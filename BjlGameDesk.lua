--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

--endregion
local HNlayer = require("src.app.newHall.HNLayer")
local scheduler = require("framework.scheduler")
local BjlGameDesk = class("BjlGameDesk",function()
    return HNlayer.new()
end)

local DESKUI_CSB = "platform/lobbyUi/desklist_Node.csb"
local DESK_ITEM_UI = "platform/lobbyUi/deskItem_Node.csb"
local BJLE_HELP_CSB = "platform/lobbyUi/bjleHelp_Node.csb"
local GAME_PAGESPROMPT_PATH = "platform/common/yuandian1.png"
local WIN_ZHUANG = "platform/lobbyUi/res/lobby/zhuang.png"
local WIN_XIAN = "platform/lobbyUi/res/lobby/xian.png"
local WIN_HE = "platform/lobbyUi/res/lobby/he.png"
local WIN_ZHUANGDUI = "platform/lobbyUi/res/lobby/zhuangdui.png"
local WIN_XIANDUI = "platform/lobbyUi/res/lobby/xiandui.png"

local pageViewDesksSize = cc.size(1280, 500)

function BjlGameDesk:ctor(_roomInfo)
    
    self:myInit(_roomInfo)
end

function BjlGameDesk:resetRoomList(_roomInfo)
    scheduler.unscheduleGlobal(self._timeText)
    scheduler.unscheduleGlobal(self._timeProgress)
    self._node:removeFromParent()
    self:myInit(_roomInfo)
end

function BjlGameDesk:resetDesk()
end

function BjlGameDesk:resetTimeLeft(time,index)
    self._itime[index] = time
    self._ftime[index] = time
end

function BjlGameDesk:resetFreeState(index)
    self._Text_betTime[index]:setVisible(true)
    self._Text_betTime[index]:setString("空闲中")
end

function BjlGameDesk:resetStartState(index)
    self._Text_betTime[index]:setVisible(true)
    self._Text_betTime[index]:setString("下注中")
end

function BjlGameDesk:resetOpenCardState(index)
    self._Text_betTime[index]:setVisible(true)
    self._Text_betTime[index]:setString("空闲中")
end

function BjlGameDesk:resetEndState(index)
    self._Text_betTime[index]:setVisible(true)
    self._Text_betTime[index]:setString("结算中")
end

function BjlGameDesk:myInit(_roomInfo)
    self._currentSelectedDesk = nil
	self._deskNO = 0
	self._pageLen = 0
	self._canCreate = true
	self._pageEven = false
	self._isTouch = true
    self._iGameCount = {}
    self._roomItemNode  = {}
    self._itemPanel = {}
    self._Text_betTime = {}
    self._AtlasLabel_time = {}
    self._Text_over = {}
    self._LoadingBar_time = {}
    self._leftRecordLayout = {}
    self._Text_zhuangNum = {}
    self._Text_xianNum = {}
    self._Text_heNum = {}
    self._Button_inter = {}
    self._Panel_inter = {}
    self._listViewCirRecord = {}
    self._listViewBallRecord = {}
    self._listViewLineRecord = {}
    self._listViewConRecord = {}
    self._img_cir_D = {}
    self._img_ball_D = {}
    self._img_line_D = {}
    self._img_cir_T = {}
    self._img_ball_T = {}
    self._img_line_T = {}
    self._curTrenLine = {}
	self._iTrendLine = {}
	self._circleLine = {}
	self._curCircleCol = {}
	self._ballLine = {}
	self._curBallCol = {}
	self._lineLine = {}
	self._curLineCol = {}
    self._lineArea = {}
    self._ballPanel = {}
    self._circlePanel = {}
    self._linePanel = {}
    self._arrResult = {}
	self._arrCircle = {}
	self._arrLine = {}
	self._arrBall = {}

	self._ftime = {}
	self._itime = {}
    self._izhuangCount = {}
	self._ixianCount = {}
	self._iheCount = {}
    self._allDeskUI = {}
    self._roomLogic = nil
    self._roomList = _roomInfo
    self._deskCount = table.nums(self._roomList)

    local x1,x2 = math.modf(self._deskCount,6)

    self._pageEven = false
	self._pageLen = x1
    if(x2 == 0) then
        self._pageEven = false
    else
        self._pageEven = true
    end

    self._winSize = cc.Director:getInstance():getWinSize()

	self:createDeskList()
end

function BjlGameDesk:onExit()
    g_GameController:gameQuitRoomReq()
end

function BjlGameDesk:onReturn(sender, eventType)
    if(eventType == ccui.TouchEventType.ended)then
        g_GameController:reqUserLeftGameServer()
        self:close()
    end
    
end

function BjlGameDesk:createDeskList()
--    local scalex = 1280 / self._winSize.width
--    local scaley = 720 / self._winSize.height
    local scalex =1/display.scaleX
	local scaley =1/display.scaleY
    self._node = UIAdapter:createNode(DESKUI_CSB)
    self._node:setPosition(self._winSize.width/2, self._winSize.height/2)
     self._node:setScale(display.scaleX,display.scaleY)
    self:addChild(self._node) 
    self._mPanelBjl = self._node:getChildByName("Panel_Bjl") 
    self._mPanelBjl:setTouchEnabled(false)

    self._layout = self._node:getChildByName("Panel_desks")
  --  self._layout:setScale(self._winSize.width / 1280, self._winSize.height / 720)
    self._layout:setTouchEnabled(true)

--    for k,child in pairs(self._layout:getChildren()) do
--        child:setScale(scalex, scaley)
--    end

    local img_top = self._layout:getChildByName("Image_top")
    img_top:setLocalZOrder(2000)
--    for k,child in pairs(img_top:getChildren()) do 
--		child:setScale(scaley, scalex); 
--	end
    local btn_return = img_top:getChildByName("Button_return")
    --btn_return:addTouchEventListener(handler(self,self.onReturn))

    local img_gold = img_top:getChildByName("Image_gold")
    self._text_gold = img_gold:getChildByName("AtlasLabel_value")
    local num_money = cc.UserDefault:getInstance():getDoubleForKey("m_playerscore")
    print("num_money:",num_money)
    self._text_gold:setString(num_money)

    local img_lottery = img_top:getChildByName("Image_lottery")
    self._text_lottery = img_lottery:getChildByName("AtlasLabel_value")
    --local num_lottery = Player:get

    self._pageViewDesks = self._layout:getChildByName("PageView_desks")
    self._pageViewDesks:setCustomScrollThreshold(15)
    --self._pageViewDesks:setCascadeOpacityEnabled(true)
	self._pageViewDesks:scrollToPage(0)
	self._pageViewDesks:removeAllPages()
    self._pageViewDesks:setTouchEnabled(false)
    --self._pageViewDesks:setBounceEnabled(false)
	--[[self._pageViewDesks:setIndicatorEnabled(true)
	self._pageViewDesks:setIndicatorIndexNodesTexture(GAME_PAGESPROMPT_PATH)
	self._pageViewDesks:setIndicatorIndexNodesScale(0.85)
	self._pageViewDesks:setIndicatorSelectedIndexColor(255, 255, 255)
	self._pageViewDesks:setIndicatorPosition(cc.p(self._pageViewDesks:getContentSize().width * 0.5, self._pageViewDesks:getContentSize().height * 0.04))]]--

    local HelpNode = UIAdapter:createNode(BJLE_HELP_CSB)
    HelpNode:setPosition(self._winSize.width / 2, self._winSize.height / 2)
    self:addChild(HelpNode)
    HelpNode:setVisible(false)
    
    local panelHelp = HelpNode:getChildByName("Panel_help")
    local Btn_rule = HelpNode:getChildByName("Button_rule0")
    local Btn_ludan = HelpNode:getChildByName("Button_rule1")
    local Btn_close = HelpNode:getChildByName("Button_close")
    local List_rule = HelpNode:getChildByName("ListView_rule")
    local List_ludan = HelpNode:getChildByName("ListView_ludan")

    Btn_close:addTouchEventListener(function()
        HelpNode:setVisible(false)
    end)
    Btn_rule:addTouchEventListener(function()
        Btn_rule:setEnabled(false)
        Btn_ludan:setEnabled(true)
        List_rule:setVisible(true)
        List_ludan:setVisible(false)
    end)
    Btn_ludan:addTouchEventListener(function()
        Btn_rule:setEnabled(true)
        Btn_ludan:setEnabled(false)
        List_rule:setVisible(false)
        List_ludan:setVisible(true)
    end)

    for k,child in pairs(self._mPanelBjl:getChildren()) do
        print(child:getName())
    end

    local Img_bg = self._mPanelBjl:getChildByName("Image_bg")
    --Img_bg:setLocalZOrder(2)
    local Img_change = self._mPanelBjl:getChildByName("Image_change")
    Img_change:setLocalZOrder(2)
    Img_change:setOpacity(255)
    local btnHelp = self._mPanelBjl:getChildByName("Button_help")
    btnHelp:setLocalZOrder(2)
    local btn_back = self._mPanelBjl:getChildByName("Button_back")
    btn_back:setLocalZOrder(3)
    local Img_titleBg = self._mPanelBjl:getChildByName("Image_titleBg")
    Img_titleBg:setLocalZOrder(2)
    Img_titleBg:setOpacity(255)
    local Atlas_money = Img_titleBg:getChildByName("AtlasLabel_money")
    local btn_noMi = self._mPanelBjl:getChildByName("Button_noMi")
    btn_noMi:setLocalZOrder(2)
    local btn_batch = ccui.Helper:seekWidgetByName(self._mPanelBjl,"Button_Batchbet")
    btn_noMi:setEnabled(false)
    self._bjlRoomListView = self._mPanelBjl:getChildByName("ListView_Room")
    self._bjlRoomListView:setTouchEnabled(true)
    --self._bjlRoomListView:setDirection(1)
    self._bjlRoomListView:setPropagateTouchEvents(true)
    self._bjlRoomListView:setSwallowTouches(true)
    self._bjlRoomListView:removeAllItems()
    self._bjlRoomListView:setItemsMargin(5)
    self._bjlRoomListView:setLocalZOrder(1)
    --self._bjlRoomListView:setTouchPriority(-1)
    local posx, posy = self._bjlRoomListView:getPosition()
    self._bjlRoomListView:setPosition(cc.p(posx, posy - 50))

    btn_back:addTouchEventListener(handler(self,self.onReturn))

    btnHelp:addTouchEventListener(function()
        HelpNode:setVisible(true)
    end)

    Atlas_money:setString(num_money)

    self._mPanelBjl:setVisible(true)
	self._layout:setVisible(false)
	--self:createDeskListPage()
	local roomCount = table.nums(self._roomList)
	
    self._roomItemNode = {}
	for i = 1,self._deskCount do
        self._iGameCount[i] = table.nums(self._roomList[i].m_history)
		self._ftime[i] = self._roomList[i].m_leftTime
		self._itime[i] = self._roomList[i].m_leftTime
		self:creatBjlRoomItem(self._roomList[i], i)
		if (self._roomItemNode[i]) then
			local layerNode = ccui.Layout:create()
			layerNode:setContentSize(cc.size(720, 320))
            layerNode:setScale(scaley, scalex);
            layerNode:setTouchEnabled(true)
            layerNode:setSwallowTouches(false)
			layerNode:addChild(self._roomItemNode[i], 2)
			self._roomItemNode[i]:setPosition(cc.p(layerNode:getContentSize().width/ 2, layerNode:getContentSize().height *display.scaleX/ 2))
			self._bjlRoomListView:pushBackCustomItem(layerNode)
            self:enterRoomEventCallBack(i)
			self:initTimeCountText(self._roomList[i].m_leftTime, i)
		end
	end
--    for k,child in pairs(self._bjlRoomListView:getChildren()) do
--        if child:getName()~= "Particle_1" then
--		    child:setScale(scaley, scalex);
--        end
--	end
    self._timeText = scheduler.scheduleGlobal(handler(self,self.setTimeCountText), 1.0)
    self._timeProgress = scheduler.scheduleGlobal(handler(self,self.setTimeCountProgress), 0.01)
    --self._timeRoomResult = scheduler.scheduleGlobal(handler(self,self.setBjlRoomResult), 1.0)
end

function BjlGameDesk:createDeskPage()
	if (not self._canCreate) then
        return
    end

	if (self._pageLen > 0) then
		--_deskinfos.clear()
		for i = 1,6 do
			self:createDeskPageInfo()
		end
		self:createDeskListPage()
		self._pageLen = self._pageLen - 1
		if (self._pageEven) and (0 == self._pageLen) then
			self._canCreate = false
		end
	end

	if (0 == self._pageLen) and (not self._pageEven) then
        local num1,num2 = math.modf(self._deskCount,6)

		for i = 1,num2 do
			self:createDeskPageInfo()
        end
		self:createDeskListPage()
		self._canCreate = false
	end

	self:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function()
		self:createDeskPage()
	end), nil))
end

-- 创建牌桌页面数据
function BjlGameDesk:createDeskPageInfo()

end

function BjlGameDesk:removeFromParent()
    self.close()
end

-- 创建牌桌列表page
function BjlGameDesk:createDeskListPage()
	--创建房间列表子页面
	local deskListLayout = ccui.Layout:create()
	deskListLayout:setName("page")
	deskListLayout:setContentSize(self._pageViewDesks:getContentSize())
	deskListLayout:setPassFocusToChild(true)
    --deskListLayout:setTouchEnabled(true)

	local pageWidth = self._pageViewDesks:getContentSize().width
	local pageHeight = self._pageViewDesks:getContentSize().height

	local idx = 0
    for k,child in pairs(self._roomList) do
        idx = idx + 1
        local deskNode = self.addDesk(child)
		local button = deskNode:getChildByName("Button_desk")
		button:removeFromParent()

        local x1,x2 = math.modf(idx,2)
        local posX = 0
        if (x2==1) then
            posX = 0.26
        else
            posX = 0.74
        end
		local posY = 0
		if (idx <= 2) then
            posY = 0.83
		elseif (idx > 2 and idx <= 4) then
            posY = 0.54
		else 
            posY = 0.25
        end

		button:setPosition(cc.p(pageWidth * posX, pageHeight * posY))
		deskListLayout:addChild(button, 1000)

		self._allDeskUI[idx] = button
    end

	--添加子页面进入列表中
	--self._pageViewDesks:addPage(deskListLayout)
end

-- 添加桌子
function BjlGameDesk:addDesk(deskInfo)
	local deskItemNode = UIAdapter:createNode(DESK_ITEM_UI)
	if (deskItemNode) then
        
    else
        return nil
    end

	local deskItem = deskItemNode:getChildByName("Button_desk")
	if (deskItem) then
        
    else
        return nil
    end

	deskItem:setScale(0.9)
	--deskItem:setUserData(deskInfo)
	deskItem:setPropagateTouchEvents(true)
	deskItem:addTouchEventListener(handler(self, self.enterTableEventCallBack))

	-- 桌名
	local text_deskNo = ccui.Helper:seekWidgetByName(deskItem, "AtlasLabel_deskNo")
	text_deskNo:setString("")

	-- 桌上人数
	local textAtlas_people = ccui.Helper:seekWidgetByName(deskItem, "AtlasLabel_count")
	textAtlas_people:setString("")

	-- 人数进度条
	local count =  100
	local progress = ccui.Helper:seekWidgetByName(deskItem, "LoadingBar_count")
	progress:setPercent(count)

	-- 房间人数
	local text_PeopleCount = ccui.Helper:seekWidgetByName(deskItem, "Text_count")
	text_PeopleCount:setString("人在玩")

	-- 金币限制
	local text_limit = ccui.Helper:seekWidgetByName(deskItem, "Text_limit")
	if (text_limit) then
		local str = ""

		--[[if (0 == deskInfo.goldMin) then
			str = GBKToUtf8("无限制")
		elseif (0 == deskInfo.goldMax) then
			if (deskInfo.goldMin <= 10000) then
				str = StringUtils::format(GBKToUtf8("%d准入"), deskInfo.goldMin)
			elseif (deskInfo->goldMin >= 10000 && deskInfo.goldMin < 100000000) then
				str = StringUtils::format(GBKToUtf8("%.2f万+"), deskInfo.goldMin / 10000.f)
			else
				str = StringUtils::format(GBKToUtf8("%.2f亿+"), deskInfo.goldMin / 100000000.f)
			end
		else
			if (deskInfo.goldMax < 10000) then
				str = StringUtils::format(GBKToUtf8("%d-%d准入"), deskInfo.goldMin, deskInfo.goldMax)
			elseif (deskInfo.goldMax >= 10000 && deskInfo.goldMax < 100000000) then
				if (deskInfo.goldMin < 10000) then
					str = StringUtils::format(GBKToUtf8("%d-%.2f万"), deskInfo.goldMin, deskInfo.goldMax / 10000.f)
				else
					str = StringUtils::format(GBKToUtf8("%.2f万-%.2f万"), deskInfo.goldMin / 10000.f, deskInfo.goldMax / 10000.f)
				end
			else
				if (deskInfo.goldMin < 10000) then
					str = StringUtils::format(GBKToUtf8("%d-%.2f亿"), deskInfo.goldMin, deskInfo.goldMax / 100000000.f)
				elseif (deskInfo.goldMin >= 10000 && deskInfo.goldMin < 100000000) then
					str = StringUtils::format(GBKToUtf8("%.2f万-%.2f亿"), deskInfo.goldMin / 10000.f, deskInfo.goldMax / 100000000.f)
				else
					str = StringUtils::format(GBKToUtf8("%.2f亿-%.2f亿"), deskInfo.goldMin / 100000000.f, deskInfo.goldMax / 100000000.f)
				end
			end
		end]]--
		text_limit:setString(str)
	end
	return deskItemNode
end

-- 进入游戏桌点击回调
function BjlGameDesk:enterTableEventCallBack(sender, eventType)
	if (not self._isTouch) then
        return
    end

	self._currentSelectedDesk = sender

    if eventType==ccui.TouchEventType.began then
        self._currentSelectedDesk:setColor(cc.c3b(170, 170, 170))
    elseif eventType==ccui.TouchEventType.canceled then
        self._currentSelectedDesk:setColor(cc.c3b(255, 255, 255))
    elseif eventType==ccui.TouchEventType.ended then
        self._isTouch = false
		self:runAction(cc.Sequence:create(cc.DelayTime:create(15.0), cc.CallFunc:create(function()
			self._isTouch = true
		end)))

	    self._currentSelectedDesk:setColor(cc.c3b(255, 255, 255))

	    --HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON)

		--[[local deskInfo = static_cast<DeskInfo*>(_currentSelectedDesk->getUserData())

		-- 参数校验
		--CCAssert(nullptr != deskInfo, "desk is nullptr!")
		if (nullptr == deskInfo)
		{
			_isTouch = true
			return
		}

		if (deskInfo->peopleValue == deskInfo->peopleMax)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("桌子人数已满。"))
			_isTouch = true
			return
		}

		// 密码桌子
		if(deskInfo->isLocked)
		{
			GamePasswordInput* layer = GamePasswordInput::create()
			layer->setPosition(_winSize / 2)
			this->addChild(layer, 1000)
			layer->onEnterCallback = CC_CALLBACK_1(GameDesk::onEnterPasswordCallback, this)
			_isTouch = true
			return
		}

		_deskLogic->requestQuickSit(deskInfo->deskID)]]--
    end
end

function BjlGameDesk:updateLockDesk()
	--[[for(auto &deskInfo: _allDeskInfo)
	{
		deskInfo->isLocked = RoomLogic()->deskStation.bDeskLock[deskInfo->deskID]
	}]]--
end

--[[function BjlGameDesk:onDeskSitCallback(bool success, const std::string& message, UINT roomID, BYTE deskNo, BYTE seatNo)
	_isTouch = true

	if(success)
	{
		ComRoomInfo* pRoomInfo = RoomLogic()->getSelectedRoom()
		if(!pRoomInfo)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("查找房间失败。"))
		}
		bool bRet = GameCreator()->startGameClient(pRoomInfo->uNameID, deskNo, true)
		if (!bRet)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("游戏启动失败。"))
		}
		else
		{
			_deskLogic->stop()
		}
	}
	else
	{
		GamePromptLayer::create()->showPrompt(message)
	}
end

function BjlGameDesk:onUpdateVirtualDesk(BYTE deskNo, bool isVirtual)
	updateDeskPeopleCount(deskNo, isVirtual)
end

function BjlGameDesk:onRoomDeskUserCountChanged(BYTE deskNo)
	updateLockDesk()

	updateDeskPeopleCount(deskNo, false)
end

--更新游戏人数
function BjlGameDesk:updateDeskPeopleCount(BYTE deskNo, bool isLock)
	if (!_pageViewDesks || _allDeskUI.empty()) return

	if (deskNo > _allDeskUI.size() - 1) return

	DeskInfo* info = static_cast<DeskInfo*>(_allDeskUI[deskNo]->getUserData())

	if (isLock)
	{
		info->peopleValue = info->peopleMax
	}
	else
	{
		info->peopleValue = RoomLogic()->deskStation.bUserCount[info->deskID]
	}

	// 桌上人数
	auto textAtlas_people = (TextAtlas*)Helper::seekWidgetByName(_allDeskUI[deskNo], "AtlasLabel_count")
	textAtlas_people->setString(StringUtils::format("%d/%d", info->peopleValue, info->peopleMax))

	// 人数进度条
	float count = info->peopleValue * 100.f / info->peopleMax
	auto progress = (LoadingBar*)Helper::seekWidgetByName(_allDeskUI[deskNo], "LoadingBar_count")
	progress->setPercent(count)

	// 房间人数
	auto text_PeopleCount = (Text*)Helper::seekWidgetByName(_allDeskUI[deskNo], "Text_count")
	text_PeopleCount->setString(StringUtils::format(GBKToUtf8("%d人在玩"), info->peopleValue))
end

-- 更新桌子状态（是否游戏中）
function BjlGameDesk:onUpdateDeskState(BYTE deskNo, bool isPlaying)

end]]--


function BjlGameDesk:refresh()
	--[[_roomInfo = RoomLogic():getSelectedRoom()

	_deskNO = 0
	_canCreate = true
	_deskinfos.clear()
	_allDeskUI.clear()
	_allDeskInfo.clear()
	_pageViewDesks:removeAllPages()

	_pageEven = ((_roomInfo:uDeskCount % 6) == 0)
	_pageLen = _roomInfo:uDeskCount / 6

	createDeskPage()]]--
end

function BjlGameDesk:createBjleRoomPage(pages, index)

end

function BjlGameDesk:setTimeCountProgress(time)
    if self._deskCount == nil then
        return
    end
    for i=1,self._deskCount do
        self._ftime[i] = self._ftime[i] - 0.017
	    local processNumber = (self._ftime[i] * 100) / 20
	    if (self._ftime[i] <= 0) then
		    self._ftime[i] = 0
		    self._LoadingBar_time[i]:setVisible(false)
	    else
		    self._LoadingBar_time[i]:setPercent(processNumber)
		    self._LoadingBar_time[i]:setVisible(true)
	    end
    end
end

function BjlGameDesk:creatBjlRoomItem(roomInfo,index)
    dump(roomInfo, "roomInfo")
    print(index)
    self._roomItemNode[index] = UIAdapter:createNode(DESK_ITEM_UI)
    --self._roomItemNode[index]:setTouchEnabled(true)

    self._itemPanel[index] = self._roomItemNode[index]:getChildByName("Panel_Bjl")
	self._itemPanel[index]:setTouchEnabled(false)
	local Image_titlebg = self._itemPanel[index]:getChildByName("Image_titlebg")
	local Text_RoomNum = Image_titlebg:getChildByName("Text_RoomNum")
	Text_RoomNum:setString(roomInfo.m_id)

	local ListView_left = self._itemPanel[index]:getChildByName("ListView_left")
	ListView_left:setTouchEnabled(false)

	local Text_last = Image_titlebg:getChildByName("Text_last")
	Text_last:setString(roomInfo.m_leftTime / 100)
	self._Text_betTime[index] = Image_titlebg:getChildByName("Text_betTime")
	self._Text_betTime[index]:setVisible(false)
	self._AtlasLabel_time[index] = Image_titlebg:getChildByName("AtlasLabel_time")
	self._AtlasLabel_time[index]:setString(roomInfo.m_leftTime)
	self._AtlasLabel_time[index]:setVisible(false)
	self._Text_over[index] = Image_titlebg:getChildByName("Text_over")
	self._Text_over[index]:setVisible(false)

	local Image_middle = self._itemPanel[index]:getChildByName("Image_middle")
	self._LoadingBar_time[index] = Image_middle:getChildByName("LoadingBar_time")
	self._LoadingBar_time[index]:setVisible(false)

	local leftList = self._itemPanel[index]:getChildByName("ListView_left")
	leftList:setClippingEnabled(true)
	leftList:setTouchEnabled(true)
    leftList:setSwallowTouches(false)
	--leftList:setScrollBarEnabled(false)
	self._leftRecordLayout[index] = ccui.Layout:create()
	self._leftRecordLayout[index]:setContentSize(cc.size(288,216))
	leftList:pushBackCustomItem(self._leftRecordLayout[index])
	local Image_bottom = self._itemPanel[index]:getChildByName("Image_bottom")
	self._Text_zhuangNum[index] = Image_bottom:getChildByName("Text_zhuangNum")
	self._Text_xianNum[index] = Image_bottom:getChildByName("Text_xianNum")
	self._Text_heNum[index] = Image_bottom:getChildByName("Text_heNum")
	self._Button_inter[index] = Image_bottom:getChildByName("Button_inter")
	self._Panel_inter[index] = self._itemPanel[index]:getChildByName("Panel_click")
	self._Panel_inter[index]:setTouchEnabled(true)
    self._Panel_inter[index]:setSwallowTouches(false)
	--self._Panel_inter[index]:setLocalZOrder(2000)

	self._listViewCirRecord[index] = self._itemPanel[index]:getChildByName("ListView_little1")
	self._listViewCirRecord[index]:removeAllItems()
	self._listViewCirRecord[index]:setTouchEnabled(true)
    self._listViewCirRecord[index]:setSwallowTouches(false)
	--self._listViewCirRecord[index]:setScrollBarEnabled(false)
	self._listViewCirRecord[index]:setPositionX(self._listViewCirRecord[index]:getPositionX() + 2)

	self._listViewBallRecord[index] = self._itemPanel[index]:getChildByName("ListView_little2")
	self._listViewBallRecord[index]:removeAllItems()
	self._listViewBallRecord[index]:setTouchEnabled(true)
    self._listViewBallRecord[index]:setSwallowTouches(false)
	self._listViewBallRecord[index]:setPositionX(self._listViewBallRecord[index]:getPositionX() + 2)
	--self._listViewBallRecord[index]:setScrollBarEnabled(false)

	self._listViewLineRecord[index] = self._itemPanel[index]:getChildByName("ListView_little3")
	self._listViewLineRecord[index]:removeAllItems()
	self._listViewLineRecord[index]:setTouchEnabled(true)
    self._listViewLineRecord[index]:setSwallowTouches(false)
	--self._listViewLineRecord[index]:setScrollBarEnabled(false)
	self._listViewLineRecord[index]:setPositionX(self._listViewLineRecord[index]:getPositionX() + 2)

	self._listViewConRecord[index] = self._itemPanel[index]:getChildByName("ListView_top")
	self._listViewConRecord[index]:removeAllItems()
	self._listViewConRecord[index]:setTouchEnabled(true)
    self._listViewConRecord[index]:setSwallowTouches(false)
	--self._listViewConRecord[index]:setScrollBarEnabled(false)
	self._listViewConRecord[index]:setPositionX(self._listViewConRecord[index]:getPositionX() + 2)

	local trendBg = self._itemPanel[index]:getChildByName("Image_middle")

	trendBg:setPosition(cc.p(trendBg:getPositionX() + 2, trendBg:getPositionY()))

	self._img_cir_D[index] = trendBg:getChildByName("Img_nextZhuang1")
	self._img_ball_D[index] = trendBg:getChildByName("Img_nextZhuang2")
	self._img_line_D[index] = trendBg:getChildByName("Img_nextZhuang3")

	self._img_cir_T[index] = trendBg:getChildByName("Img_nextXian1")
	self._img_ball_T[index] = trendBg:getChildByName("Img_nextXian2")
	self._img_line_T[index] = trendBg:getChildByName("Img_nextXian3")

	self._curTrenLine[index] = 0
	self._iTrendLine[index] = 0

	self._circleLine[index] = 0
	self._curCircleCol[index] = 0

	self._ballLine[index] = 0
	self._curBallCol[index] = 0

	self._lineLine[index] = 0
	self._curLineCol[index] = 0

    self._lineArea[index] = {}
	for i = 0,31 do
		self._lineArea[index][i] = ccui.Layout:create()
		self._lineArea[index][i]:setAnchorPoint(cc.p(0.5,1))
		self._lineArea[index][i]:setBackGroundImage("platform/lobbyUi/res/lobby/dl_bg.png")
		self._lineArea[index][i]:setContentSize(cc.size(18, 108))

		self._listViewConRecord[index]:pushBackCustomItem(self._lineArea[index][i])
		self._curTrenLine[index] = self._curTrenLine[index] + 1
	end

    self._circlePanel[index] = {}
    self._linePanel[index] = {}
	for i = 0,15 do
		self._circlePanel[index][i] = ccui.Layout:create()
		self._linePanel[index][i] = ccui.Layout:create()

		self._circlePanel[index][i]:setAnchorPoint(cc.p(0.5,1))
		self._linePanel[index][i]:setAnchorPoint(cc.p(0.5,1))

		self._circlePanel[index][i]:setBackGroundImage("platform/lobbyUi/res/lobby/xsl_bg.png")
		self._linePanel[index][i]:setBackGroundImage("platform/lobbyUi/res/lobby/xsl_bg.png")

		self._circlePanel[index][i]:setContentSize(cc.size(18, 54))
		self._linePanel[index][i]:setContentSize(cc.size(18, 54))

		self._listViewCirRecord[index]:pushBackCustomItem(self._circlePanel[index][i])
		self._listViewLineRecord[index]:pushBackCustomItem(self._linePanel[index][i])

		self._curCircleCol[index] = self._curCircleCol[index]+1

		self._curLineCol[index] = self._curLineCol[index]+1
	end

    self._ballPanel[index] = {}
	for i = 0,15 do
		self._ballPanel[index][i] = ccui.Layout:create()
		self._ballPanel[index][i]:setAnchorPoint(cc.p(0.5,1))
		self._ballPanel[index][i]:setBackGroundImage("platform/lobbyUi/res/lobby/xsl_bg.png")
		self._ballPanel[index][i]:setContentSize(cc.size(18, 54))
		self._listViewBallRecord[index]:pushBackCustomItem(self._ballPanel[index][i])
		self._curBallCol[index] = self._curBallCol[index]+1
	end

	

	self:updateGameCount(roomInfo.m_history, roomInfo.m_leftTime,index)
	--[[self.initConTrend(pages.iResultInfo, pages.igamecount - 1, index)
	self.initBall(pages.iyuceinfo1, pages.igamecount - 1, index)
	self.initCircle(pages.iyuceinfo, pages.igamecount - 1, index)
	self.initLine(pages.iyuceinfo2, pages.igamecount - 1, index)]]--
     
	self:initConTrend(roomInfo.m_history, self._iGameCount[index], index)
    self:initCircle(roomInfo.m_dyzlu, index)
	self:initBall(roomInfo.m_xlu, index)
	self:initLine(roomInfo.m_xqlu, index)
end

function BjlGameDesk:updateGameCount(data, time, index)
	local iResultInfo = {}
    for i = 1,72 do
        iResultInfo[i] = {}
        for j = 1,5 do
            iResultInfo[i][j] = 0
        end
    end

	local izhuang = 0
	local ixian = 0
	local ihe = 0
	local gamecount = 0

    local leftSpVec = {}
	--vector<Sprite*>leftSpVec
	--leftSpVec.clear()

	gamecount = self._iGameCount[index] 
	if (gamecount > 48) then
        local x1 = math.modf(gamecount/6)
        local x2 = math.fmod(gamecount,6)
		local x = x1 + 1
		local xSize = 36 * x
		self._leftRecordLayout[index]:setContentSize(cc.size(xSize, 216))
	end

	for i = 1,gamecount do
        if data[i].m_bankerVal > data[i].m_xianVal then
            iResultInfo[i][1] = 1
        elseif data[i].m_xianVal > data[i].m_bankerVal then
            iResultInfo[i][2] = 1
        else
            iResultInfo[i][3] = 1
        end

        if data[i].m_bankerPair == 1 then
            iResultInfo[i][4] = 1
        end

        if data[i].m_xianPair == 1 then
            iResultInfo[i][5] = 1
        end
	end

	for i = 0,gamecount-1 do
		local leftSp = cc.Sprite:create(WIN_ZHUANG)
		local duiSp = cc.Sprite:create(WIN_ZHUANGDUI)

		if (1 == iResultInfo[i+1][4]) then
			duiSp:setTexture(WIN_ZHUANGDUI)
		elseif (1 == iResultInfo[i+1][5]) then
			duiSp:setTexture(WIN_XIANDUI)
		else
			duiSp:setVisible(false)
		end

		if (1 == iResultInfo[i+1][1]) then
			leftSp:setTexture(WIN_ZHUANG)
			leftSp:setTag(0)
		elseif (1 == iResultInfo[i+1][2]) then
			leftSp:setTexture(WIN_XIAN)
			leftSp:setTag(1)
		elseif (1 == iResultInfo[i+1][3]) then
			leftSp:setTexture(WIN_HE)
			leftSp:setTag(2)
		end

		duiSp:setPosition(cc.p(30, 5))
        local y1 = math.modf(i/6)
        local y2 = math.fmod(i,6)
		leftSp:setPosition(cc.p(18 + (y1 * 36), 198 - (y2 * 36)))
		leftSp:setName("leftSp")
		self._leftRecordLayout[index]:addChild(leftSp, 2)
		leftSp:addChild(duiSp)
		leftSpVec[i] = leftSp
    end

	for i = 0,table.nums(leftSpVec)-1 do
        local vTag = leftSpVec[i]:getTag()
		if (vTag == 0) then
			izhuang = izhuang + 1
		elseif (vTag == 1) then
			ixian = ixian + 1
		elseif (vTag == 2) then
			ihe = ihe + 1
		else
			
		end
	end

	self._izhuangCount[index] = izhuang
	self._ixianCount[index] = ixian
	self._iheCount[index] = ihe
	self._Text_zhuangNum[index]:setString(izhuang)
	self._Text_xianNum[index]:setString(ixian)
	self._Text_heNum[index]:setString(ihe)
end

--点击进入房间消息发送
function BjlGameDesk:enterRoomEventCallBack(index)
    local function pOnTouchFunc2(sender,eventType)
        if sender then
            if eventType ~= ccui.TouchEventType.ended then
                return
            else
                ConnectManager:send2GameServer( 161001,"CS_C2G_Baccarat_EnterRoom_Req", { self._roomList[index].m_id })
            end
        end
    end  
    self._Button_inter[index]:addTouchEventListener(pOnTouchFunc2)
    self._itemPanel[index]:addTouchEventListener(pOnTouchFunc2)
    self._Panel_inter[index]:addTouchEventListener(pOnTouchFunc2)
    
end

function BjlGameDesk:initTimeCountText(time,index)
	if (time <= 0) then
		self._Text_betTime[index]:setVisible(true)
		self._AtlasLabel_time[index]:setVisible(false)
		self._Text_over[index]:setVisible(false)
	else
		self._Text_over[index]:setVisible(false)
		self._Text_betTime[index]:setVisible(true)
		self._AtlasLabel_time[index]:setVisible(true)
	end
end

function BjlGameDesk:showWinkAction(winArea,index)
	local cWinArea = {}
    for i = 1,5 do
        cWinArea[i] = winArea[i]
    end

	local gamecount = 0
	if (1 >= self._iGameCount[index]) then
		self._leftRecordLayout[index]:removeAllChildren()
	end
	gamecount = self._iGameCount[index] 
	if (gamecount > 48) then
        local x1,x2 = math.modf(gamecount,6)
		local x = x1 + 1
		local xSize = 36 * x
		self._leftRecordLayout[index]:setContentSize(cc.size(xSize, 216))
	end
	local leftSp = cc.Sprite:create(WIN_ZHUANG)
	local duiSp = cc.Sprite:create(WIN_ZHUANGDUI)
	if (1 == cWinArea[4]) then
		duiSp:setTexture(WIN_ZHUANGDUI)
	elseif (1 == cWinArea[5]) then
		duiSp:setTexture(WIN_XIANDUI)
	else
		duiSp:setVisible(false)
	end
	if (1 == cWinArea[1]) then
		leftSp:setTexture(WIN_ZHUANG)
		self._izhuangCount[index] = self._izhuangCount[index] + 1
	elseif (1 == cWinArea[2]) then
		leftSp:setTexture(WIN_XIAN)
		self._ixianCount[index] = self._ixianCount[index] + 1
	elseif (1 == cWinArea[3]) then
		leftSp:setTexture(WIN_HE)
		self._iheCount[index] = self._iheCount[index] + 1
    else

	end
	duiSp:setPosition(cc.p(30, 5))
    local x3,x4 = math.modf(gamecount-1,6)
	leftSp:setPosition(cc.p((18 + (x3 * 36)), (198 - (x4 * 36))))
	self._leftRecordLayout[index]:addChild(leftSp, 2)
	leftSp:addChild(duiSp)
	local act1 = cc.FadeIn:create(0.8)
	local act2 = cc.FadeOut:create(0.8)
	if (leftSp) then
		leftSp:setOpacity(255)
		leftSp:runAction(cc.Sequence:create(act2, act1, nil))
	end
	self._Text_zhuangNum[index]:setString(self._izhuangCount[index])
	self._Text_xianNum[index]:setString(self._ixianCount[index])
	self._Text_heNum[index]:setString(self._iheCount[index])
end

function BjlGameDesk:setBjlRoomResult(dt)
	--memcpy(bjlInfo, RoomLogic():bjlinfo.bjlinfo, sizeof(bjlInfo))
	for i = 1,3 do
		--[[self.bjlInfo[i] = RoomLogic():bjlinfo.bjlinfo[i]
		if (bjlInfo[i].igamecount <= 1)
		{
			clearData(i)
		}


		for (int j = 0 j < bjlInfo[i].igamecount j++)
		{
			_arrLine[i][j] = bjlInfo[i].iyuceinfo2[j]
			_arrCircle[i][j] = bjlInfo[i].iyuceinfo[j]
			_arrBall[i][j] = bjlInfo[i].iyuceinfo1[j]
		}
		
		if (bjlInfo[i].iResultInfo[_iGameCount[i]]--[[[0] != 0)
		{
			_iGameCount[i]++
			int gamecount = 0
			for (int j = 0 j < 72 j++)
			{
				if (bjlInfo[i].iResultInfo[j][0] != 0)
				{
					gamecount++
				}
			}
			if (_iGameCount[i] > gamecount)
			{
				_iGameCount[i] = 0
			}
			_ftime[i] = 20
			_itime[i] = 20
			showWinkAction(bjlInfo[i].iResultInfo[_iGameCount[i] -1], i)
			/*showTrend(bjlInfo[i].iResultInfo, bjlInfo[i].igamecount - 1, i)
			showBall(bjlInfo[i].igamecount - 1, i)
			showCircle(bjlInfo[i].igamecount - 1, i)
			showLine(bjlInfo[i].igamecount - 1, i)
			falshTrendImg(bjlInfo[i].igamecount - 1, i)*/
			showTrend(bjlInfo[i].iResultInfo, _iGameCount[i], i)
			showBall(_iGameCount[i] - 1, i)
			showCircle(_iGameCount[i] - 1, i)
			showLine(_iGameCount[i] - 1, i)
			falshTrendImg(_iGameCount[i] - 1, i)
			switch (i)
			{
			case 0:
				unschedule(schedule_selector(GameDesk::setTimeCountText0))
				unschedule(schedule_selector(GameDesk::setTimeCountProgress0))

				_Text_over[0]:setVisible(false)
				_Text_betTime[0]:setVisible(true)
				_AtlasLabel_time[0]:setVisible(true)
				_AtlasLabel_time[0]:setString(StringUtils::format("%d", _itime[0]))

				schedule(schedule_selector(GameDesk::setTimeCountText0), 1.0f)
				schedule(schedule_selector(GameDesk::setTimeCountProgress0), 0.01)
				break
			case 1:
				unschedule(schedule_selector(GameDesk::setTimeCountText1))
				unschedule(schedule_selector(GameDesk::setTimeCountProgress1))

				_Text_over[1]:setVisible(false)
				_Text_betTime[1]:setVisible(true)
				_AtlasLabel_time[1]:setVisible(true)
				_AtlasLabel_time[1]:setString(StringUtils::format("%d", _itime[1]))

				schedule(schedule_selector(GameDesk::setTimeCountText1), 1.0f)
				schedule(schedule_selector(GameDesk::setTimeCountProgress1), 0.01)
				break
			case 2:
				unschedule(schedule_selector(GameDesk::setTimeCountText2))
				unschedule(schedule_selector(GameDesk::setTimeCountProgress2))

				_Text_over[2]:setVisible(false)
				_Text_betTime[2]:setVisible(true)
				_AtlasLabel_time[2]:setVisible(true)
				_AtlasLabel_time[2]:setString(StringUtils::format("%d", _itime[2]))
		
				schedule(schedule_selector(GameDesk::setTimeCountText2), 1.0f)
				schedule(schedule_selector(GameDesk::setTimeCountProgress2), 0.01)
				break
			default:
				break
			}
		end]]--
	end
end

function BjlGameDesk:setTimeCountText(time)
    if self._deskCount == nil then
        return
    end
    for i = 1,self._deskCount do
        self._itime[i] = self._itime[i] - 1
        if (self._itime[i] <= 0) then
            self._itime[i] = 0
		    --self._Text_betTime[i]:setVisible(false)
		    self._AtlasLabel_time[i]:setVisible(false)
		    --self._Text_over[i]:setVisible(true)
	    else
	        --self._Text_over[i]:setVisible(false)
	        --self._Text_betTime[i]:setVisible(true)
	        self._AtlasLabel_time[i]:setVisible(true)
	        self._AtlasLabel_time[i]:setString(self._itime[i])
	    end
    end
end


function BjlGameDesk:showTrend(arrData, gameCount, index)
	local equal_count = 0
	self._iTrendLine[index] = 1
    
	--存储所有游戏结果  0: 和  1:龙  2:虎
	for i = 1,gameCount do
		for j = 1,3 do
			if (arrData[i][j] == 1) then
				self._arrResult[index][i] = j
			end
		end
	end
	--计算本局结果所在行数
	for i = 2,gameCount do
		if (i < gameCount) then
			if (self._arrResult[index][i] ~= self._arrResult[index][i - 1]) then
				self._iTrendLine[index] = self._iTrendLine[index] + 1
			end
		end
	end


	local index1 = 0
	for i = gameCount,2 do
		if (self._arrResult[index][i] == self._arrResult[index][i - 1]) then
			index1 = index1 + 1
		else
			break
		end
	end

	--判断是否需要添加列
	if (self._iTrendLine[index] > self._curTrenLine[index]) then
		for i = self._curTrenLine[index],self._iTrendLine[index]-1 do
			self._lineArea[index][i] = ccui.Layout:create()
			self._lineArea[index][i]:setAnchorPoint(cc.p(0.5,1))
			self._lineArea[index][i]:setBackGroundImage("platform/lobbyUi/res/lobby/dl_bg.png")
			self._lineArea[index][i]:setContentSize(cc.size(18, 108))

			self._listViewConRecord[index]:pushBackCustomItem(self._lineArea[index][i])
			self._curTrenLine[index] = self._curTrenLine[index] + 1
		end
	end

	local Record = cc.Sprite:create()
	if (self._arrResult[index][gameCount - 1] == 0) then
		Record = cc.Sprite:create("platform/lobbyUi/res/lobby/corner_circle_blue.png")
	elseif (self._arrResult[index][gameCount - 1] == 1) then
		Record = cc.Sprite:create("platform/lobbyUi/res/lobby/corner_circle_red.png")
	elseif (self._arrResult[index][gameCount - 1] == 2) then
		Record = cc.Sprite:create("platform/lobbyUi/res/lobby/corner_circle_green.png")
	end

	if (Record) then
		if (self._iTrendLine[index] == 1) then
			self._lineArea[index][0]:addChild(Record)
		else
			self._lineArea[index][self._iTrendLine[index] - 1]:addChild(Record)
		end
	
		Record:setPosition(cc.p(8, 101 - 18 * index1))
		local pFadeIn = cc.FadeIn:create(0.5)
		local pFadeOut = cc.FadeOut:create(0.5)
		Record:setOpacity(0)
		Record:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), pFadeIn, pFadeOut, pFadeIn, nil))
	end
end

function BjlGameDesk:initConTrend(arrData,gameCount,index)
	local equal_count = 0
	self._iTrendLine[index] = 0
    self._arrResult[index] = {}
    if table.nums(arrData)==0 or arrData==nil then
        return
    end
	--存储所有游戏结果  0: 和  1:龙  2:虎
	for i = 0,gameCount-1 do
		if arrData[i+1].m_xianVal>arrData[i+1].m_bankerVal then
            self._arrResult[index][i] = 1
        elseif  arrData[i+1].m_xianVal<arrData[i+1].m_bankerVal then
            self._arrResult[index][i] = 0
        else
            self._arrResult[index][i] = 2
        end
	end
	for i = 1,gameCount-1 do
		if i < gameCount then 
			if (self._arrResult[index][i] ~= self._arrResult[i-1]) then
				self._iTrendLine[index] = self._iTrendLine[index]+1
			end
		end
	end
	--判断是否需要添加列
	if (self._iTrendLine[index] + 1 > self._curTrenLine[index]) then
		for i = self._curTrenLine[index],self._iTrendLine[index]+1 do
			self._lineArea[index][i] = ccui.Layout:create()
			self._lineArea[index][i]:setAnchorPoint(cc.p(0.5,1))
			self._lineArea[index][i]:setBackGroundImage("platform/lobbyUi/res/lobby/dl_bg.png")
			self._lineArea[index][i]:setContentSize(cc.size(18, 108))

			self._listViewConRecord[index]:pushBackCustomItem(self._lineArea[index][i])
			self._curTrenLine[index] = self._curTrenLine[index] + 1
		end
	end
	local fir = display.newSprite()
	if (self._arrResult[index][0] == 0) then
		fir = display.newSprite("platform/lobbyUi/res/lobby/corner_circle_blue.png")
	elseif (self._arrResult[index][0] == 1) then
		fir = display.newSprite("platform/lobbyUi/res/lobby/corner_circle_red.png")
	elseif (self._arrResult[index][0] == 2) then
		fir = display.newSprite("platform/lobbyUi/res/lobby/corner_circle_green.png")
	end

	if (fir) then
		if (gameCount > 0 and self._lineArea[index][0]) then
			self._lineArea[index][0]:addChild(fir)
			fir:setPosition(cc.p(8, 101))
		end
	end

	local lineIndex = 1

	local index1 = 0
	local isInd = true
	for i = 1,gameCount-1 do
		if (i < gameCount) then
			if (self._arrResult[index][i] ~= self._arrResult[index][i-1]) then
				lineIndex = lineIndex + 1
			end

			if (self._arrResult[index][1] == self._arrResult[index][0]) and isInd then
				index1 = index1 + 1
				isInd = false
			end
			if (i > 1) then
				if (self._arrResult[index][i] == self._arrResult[index][i-1]) then
					index1 = index1 + 1
				else
					index1 = 0
				end
			end


			if (i < gameCount) then
				local Record = display.newSprite()
				if (self._arrResult[index][i] == 0) then
					Record = display.newSprite("platform/lobbyUi/res/lobby/corner_circle_blue.png")
				elseif (self._arrResult[index][i] == 1) then
					Record = display.newSprite("platform/lobbyUi/res/lobby/corner_circle_red.png")
				elseif (self._arrResult[index][i] == 2) then
					Record = display.newSprite("platform/lobbyUi/res/lobby/corner_circle_green.png")
				end
				if (Record) then
					if (lineIndex == 0) then
						if (self._lineArea[index][0]) then
							self._lineArea[index][0]:addChild(Record)
						end
					else
						if (self._lineArea[index][lineIndex - 1]) then
							self._lineArea[index][lineIndex - 1]:addChild(Record)
						end
					end
					Record:setPosition(cc.p(8, 101 - 18 * index1))
				end
			end
		end
	end
end

function BjlGameDesk:showCircle(gameCount, index)
	self._circleLine[index] = 1
	local changeCol = 0
    
	--计算本局结果所在行数
	for i = 2, gameCount do
		if (i < gameCount) then
			if (self._arrCircle[index][i] ~= self._arrCircle[index][i - 1]) then
				changeCol = changeCol + 1
				if (changeCol == 2) then
					self._circleLine[index] = self._circleLine[index] + 1
					changeCol = 0
				end
			end
		end
	end

	local index1 = 0
	for i = gameCount, 2 do
		if (self._arrCircle[index][i] == self._arrCircle[index][i - 1]) then
			index1 = index1 + 1
		else
			break
		end
	end

	--判断是否需要添加列
	if (self._circleLine[index] > self._curCircleCol[index]) then
		for i = self._curCircleCol[index], self._circleLine[index]-1 do
			self._circlePanel[index][i] = ccui.Layout:create()
			self._circlePanel[index][i]:setAnchorPoint(cc.p(0.5,1))
			self._circlePanel[index][i]:setBackGroundImage("platform/lobbyUi/res/lobby/xsl_bg.png")
			self._circlePanel[index][i]:setContentSize(cc.size(18, 54))

			self._listViewCirRecord[index]:pushBackCustomItem(self._circlePanel[index][i])
			self._curCircleCol[index] = self._curCircleCol[index] + 1
		end
	end

	local Record = cc.Sprite:create()
	if (self._arrCircle[index][gameCount - 1] == 0) then
		Record = cc.Sprite:create("platform/lobbyUi/res/lobby/corner_circle_blue.png")
	elseif (self._arrCircle[index][gameCount - 1] == 1) then
		Record = cc.Sprite:create("platform/lobbyUi/res/lobby/corner_circle_red.png")
	end
	if (Record) then
		if (self._circleLine[index] == 1) then
			self._circlePanel[index][1]:addChild(Record)
		else
			self._circlePanel[index][self._circleLine[index] - 1]:addChild(Record)
		end
	
	
		Record:setScale(0.5)
	
		if (changeCol == 0) then
			Record:setPosition(cc.p(4, 51 - 9 * index1))
		else
			Record:setPosition(cc.p(12, 51 - 9 * index1))
		end
	
		local pFadeIn = cc.FadeIn:create(0.5)
		local pFadeOut = cc.FadeOut:create(0.5)
		Record:setOpacity(0)
		Record:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), pFadeIn, pFadeOut, pFadeIn, nil))
	end
end

function BjlGameDesk:initCircle(data, index)
    if table.nums(data)==0 or data==nil then
        return
    end
	self._circleLine[index] = math.ceil(#data/2)

	--判断是否需要添加列
	if (self._circleLine[index] + 1 > self._curCircleCol[index]) then
		for i = self._curCircleCol[index],self._circleLine[index] do
			self._circlePanel[index][i] = ccui.Layout:create()
			self._circlePanel[index][i]:setAnchorPoint(cc.p(0.5,1))
			self._circlePanel[index][i]:setBackGroundImage("platform/lobbyUi/res/lobby/xsl_bg.png")
			self._circlePanel[index][i]:setContentSize(cc.size(18, 54))

			self._listViewCirRecord[index]:pushBackCustomItem(self._circlePanel[index][i])
			self._curCircleCol[index] = self._curCircleCol[index] + 1
		end
	end
	local fir = display.newSprite()
    for k,v in pairs(data) do 
        for i =1,v.m_num do 
            if v.m_color == 1 then 
	            fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_red.png");
            elseif  v.m_color == 0 then 
	            fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_blue.png");
            end
             fir:setScale(0.5);
            if k%2 == 1 then
                fir:setPosition(4, 51 - 9 * (i-1));
            else
                fir:setPosition(12, 51 - 9 * (i-1));
            end
            local index2 = math.ceil(k/2)
            self._circlePanel[index][index2-1]:addChild(fir); 
        end
    end
end

function BjlGameDesk:showBall(gameCount, index)
	self._ballLine[index] = 1
	local changeCol = 0
    
	--计算本局结果所在行数
	for i = 2,gameCount do
		if (i < gameCount) then
			if (self._arrBall[index][i] ~= self._arrBall[index][i - 1]) then
				changeCol = changeCol + 1
				if (changeCol == 2) then
					self._ballLine[index] = self._ballLine[index] + 1
					changeCol = 0
				end
			end
		end
	end


	local index1 = 0
	for i = gameCount,2 do
		if (self._arrBall[index][i] == self._arrBall[index][i - 1]) then
			index1 = index1 + 1
		else
			break
		end
	end

	--判断是否需要添加列
	if (self._ballLine[index] > self._curBallCol[index]) then
		for i = self._curBallCol[index], self._ballLine[index] do
			self._ballPanel[index][i] = ccui.Layout:create()
			self._ballPanel[index][i]:setAnchorPoint(cc.p(0.5,1))
			self._ballPanel[index][i]:setBackGroundImage("platform/lobbyUi/res/lobby/xsl_bg.png")
			self._ballPanel[index][i]:setContentSize(cc.size(18, 54))

			self._listViewBallRecord[index]:pushBackCustomItem(self._ballPanel[index][i])
			self._curBallCol[index] = self._curBallCol[index] + 1
		end
	end

	local Record = cc.Sprite:create()
	if (self._arrBall[index][gameCount - 1] == 0) then
		Record = cc.Sprite:create("platform/lobbyUi/res/lobby/corner_ball_blue.png")
	elseif (self._arrBall[index][gameCount - 1] == 1) then
		Record = cc.Sprite:create("platform/lobbyUi/res/lobby/corner_ball_red.png")
	end
	if (Record) then
		if (self._ballLine[index] == 1) then
			self._ballPanel[index][1]:addChild(Record)
		else
			self._ballPanel[index][self._ballLine[index] - 1]:addChild(Record)
		end
		Record:setScale(0.6)
	
	
		if (changeCol == 0) then
			Record:setPosition(cc.p(4, 51 - 9 * index1))
		else
			Record:setPosition(cc.p(12, 51 - 9 * index1))
		end
		local pFadeIn = cc.FadeIn:create(0.5)
		local pFadeOut = cc.FadeOut:create(0.5)
		Record:setOpacity(0)
		Record:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), pFadeIn, pFadeOut, pFadeIn, nil))
	end
end

function BjlGameDesk:initBall(data,index)
    if table.nums(data)==0 or data==nil then
        return
    end
	self._ballLine[index] = math.ceil(#data/2)
	--判断是否需要添加列
	if (self._ballLine[index] + 1 > self._curBallCol[index]) then
		for i = self._curBallCol[index], self._ballLine[index] do
			self._ballPanel[index][i] = ccui.Layout:create()
			self._ballPanel[index][i]:setAnchorPoint(cc.p(0.5, 1))
			self._ballPanel[index][i]:setBackGroundImage("platform/lobbyUi/res/lobby/xsl_bg.png")
			self._ballPanel[index][i]:setContentSize(cc.size(18, 54))

			self._listViewBallRecord[index]:pushBackCustomItem(self._ballPanel[index][i])
			self._curBallCol[index] = self._curBallCol[index] + 1
		end
	end
	local fir = display.newSprite()
    fir:setScale(0.5)
	for k,v in pairs(data) do 
        for i =1,v.m_num do 
            if v.m_color == 1 then 
	            fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_ball_red.png")
            elseif  v.m_color == 0 then 
	            fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_ball_blue.png")
            end
            if k%2 == 1 then
                fir:setPosition(4, 51 - 9 * (i-1))
            else
                fir:setPosition(12, 51 - 9 * (i-1))
            end
              local index2 = math.ceil(k/2)
            self._ballPanel[index][index2-1]:addChild(fir)
        end
    end
end

function BjlGameDesk:showLine(gameCount, index)
	self._lineLine[index] = 1
	local changeCol = 0
    
	--计算本局结果所在行数
	for i = 1, gameCount-1 do
		if (i < gameCount) then
			if (self._arrLine[index][i] ~= self._arrLine[index][i-1]) then
				changeCol = changeCol+1
				if (changeCol == 2) then
					self._lineLine[index] = self._lineLine[index] + 1
					changeCol = 0
				end
			end
		end
	end

	local index1 = 0
	for i = gameCount, 2 do
		if (self._arrLine[index][i] == self._arrLine[index][i - 1]) then
			index1=index1+1
		else
			break
		end
	end

	--判断是否需要添加列
	if (self._lineLine[index] > self._curLineCol[index]) then
		for i = self._curLineCol[index], self._lineLine[index] do
			self._linePanel[index][i] = ccui.Layout:create()
			self._linePanel[index][i]:setAnchorPoint(cc.p(0.5, 1))
			self._linePanel[index][i]:setBackGroundImage("platform/lobbyUi/res/lobby/xsl_bg.png")
			self._linePanel[index][i]:setContentSize(cc.size(18, 54))

			self._listViewLineRecord[index]:pushBackCustomItem(self._linePanel[index][i])
			self._curLineCol[index] = self._curLineCol[index] + 1
		end
	end

	local Record = cc.Sprite:create()
	if (self._arrLine[index][gameCount] == 0) then
		Record = cc.Sprite:create("platform/lobbyUi/res/lobby/corner_line_blue.png")
	elseif (self._arrLine[index][gameCount] == 1) then
		Record = cc.Sprite:create("platform/lobbyUi/res/lobby/corner_line_red.png")
	end

	if (Record) then
		if (self._lineLine[index] == 1) then
			self._linePanel[index][1]:addChild(Record)
		else
			self._linePanel[index][_lineLine[index] - 1]:addChild(Record)
		end
		Record:setScale(0.6)
		if (changeCol == 0) then
			Record:setPosition(cc.p(4, 51 - 9 * index1))
		else
			Record:setPosition(cc.p(12, 51 - 9 * index1))
		end
		local pFadeIn = cc.FadeIn:create(0.5)
		local pFadeOut = cc.FadeOut:create(0.5)
		Record:setOpacity(0)
		Record:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), pFadeIn, pFadeOut, pFadeIn, nil))
	end
end

function BjlGameDesk:initLine(data, index)
    if table.nums(data)==0 or data==nil then
        return
    end
	self._lineLine[index] = math.ceil(#data/2)

	--判断是否需要添加列
	if (self._lineLine[index] + 1 > self._curLineCol[index]) then
		for i = self._curLineCol[index], self._lineLine[index] do
			self._linePanel[index][i] = ccui.Layout:create()
			self._linePanel[index][i]:setAnchorPoint(cc.p(0.5,1))
			self._linePanel[index][i]:setBackGroundImage("platform/lobbyUi/res/lobby/xsl_bg.png")
			self._linePanel[index][i]:setContentSize(cc.size(18, 54))

			self._listViewLineRecord[index]:pushBackCustomItem(self._linePanel[index][i])
			self._curLineCol[index] = self._curLineCol[index] + 1
		end
	end
	local fir = display.newSprite()
    fir:setScale(0.5)
	for k,v in pairs(data) do 
        for i =1,v.m_num do 
            if v.m_color == 1 then 
	            fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_line_red.png")
            elseif  v.m_color == 0 then 
	            fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_line_blue.png")
            end
            if k%2 == 1 then
                fir:setPosition(4, 51 - 9 * (i-1))
            else
                fir:setPosition(12, 51 - 9 * (i-1))
            end
             local index2 = math.ceil(k/2)
            self._linePanel[index][index2-1]:addChild(fir)
        end
    end
end

function BjlGameDesk:falshTrendImg(gameCount, index)
    math.randomseed(os.time())
    local nRand_cir = math.random(0,1)
    local nRand_ball = math.random(0,1)
    local nRand_line = math.random(0,1)

    if (nRand_cir == 0) then
		self._img_cir_D[index]:loadTexture("platform/lobbyUi/res/lobby/corner_circle_blue.png")
		self._img_cir_T[index]:loadTexture("platform/lobbyUi/res/lobby/corner_circle_red.png")
	else
		self._img_cir_T[index]:loadTexture("platform/lobbyUi/res/lobby/corner_circle_blue.png")
		self._img_cir_D[index]:loadTexture("platform/lobbyUi/res/lobby/corner_circle_red.png")
    end

	if (nRand_ball == 0) then
		self._img_ball_D[index]:loadTexture("platform/lobbyUi/res/lobby/corner_ball_blue.png")
		self._img_ball_T[index]:loadTexture("platform/lobbyUi/res/lobby/corner_ball_red.png")
	else
		self._img_ball_T[index]:loadTexture("platform/lobbyUi/res/lobby/corner_ball_blue.png")
		self._img_ball_D[index]:loadTexture("platform/lobbyUi/res/lobby/corner_ball_red.png")
	end

	if (nRand_line == 0) then
		self._img_line_D[index]:loadTexture("platform/lobbyUi/res/lobby/corner_line_blue.png")
		if (self._img_line_T[index]) then
			self._img_line_T[index]:loadTexture("platform/lobbyUi/res/lobby/corner_line_red.png")
		end
	else
		if (self._img_line_T[index]) then
			self._img_line_T[index]:loadTexture("platform/lobbyUi/res/lobby/corner_line_blue.png")
		end
		
		self._img_line_D[index]:loadTexture("platform/lobbyUi/res/lobby/corner_line_red.png")
	end
end

function BjlGameDesk:clearData(index)
    self._listViewConRecord[index]:removeAllChildrenWithCleanup(true)
    self._listViewCirRecord[index]:removeAllChildrenWithCleanup(true)
    self._listViewBallRecord[index]:removeAllChildrenWithCleanup(true)
    self._listViewLineRecord[index]:removeAllChildrenWithCleanup(true)

    self._curTrenLine[index] = 0
	self._iTrendLine[index] = 0

	self._circleLine[index] = 0
	self._curCircleCol[index] = 0

	self._ballLine[index] = 0
	self._curBallCol[index] = 0

	self._lineLine[index] = 0
	self._curLineCol[index] = 0

    for i = 0, 31 do
        self._lineArea[index][i] = ccui.Layout:create()
		self._lineArea[index][i]:setAnchorPoint(cc.p(0.5,1))
		self._lineArea[index][i]:setBackGroundImage("platform/lobbyUi/res/lobby/dl_bg.png")
		self._lineArea[index][i]:setContentSize(cc.size(18, 108))

		self._listViewConRecord[index]:pushBackCustomItem(self._lineArea[index][i])
		self._curTrenLine[index] = self._curTrenLine[index] + 1
    end
	for j = 0, 15 do
		self._circlePanel[index][i] = ccui.Layout:create()
		self._linePanel[index][i] = ccui.Layout:create()

		self._circlePanel[index][i]:setAnchorPoint(cc.p(0.5,1))
		self._linePanel[index][i]:setAnchorPoint(cc.p(0.5,1))

		self._circlePanel[index][i]:setBackGroundImage("platform/lobbyUi/res/lobby/xsl_bg.png")
		self._linePanel[index][i]:setBackGroundImage("platform/lobbyUi/res/lobby/xsl_bg.png")

		self._circlePanel[index][i]:setContentSize(cc.size(18, 54))
		self._linePanel[index][i]:setContentSize(cc.size(18, 54))

		self._listViewLineRecord[index]:pushBackCustomItem(self._linePanel[index][i])

		self._curCircleCol[index] = self._curCircleCol[index] + 1

		self._curLineCol[index] = self._curLineCol[index] + 1
	end
	for k = 0, 15 do
		self._ballPanel[index][i] = ccui.Layout:create()
		self._ballPanel[index][i]:setAnchorPoint(cc.p(0.5,1))
		self._ballPanel[index][i]:setBackGroundImage("platform/lobbyUi/res/lobby/xsl_bg.png")
		self._ballPanel[index][i]:setContentSize(cc.size(18, 54))
		self._listViewBallRecord[index]:pushBackCustomItem(self._ballPanel[index][i])
		self._curBallCol[index] = self._curBallCol[index] + 1
    end
end


return BjlGameDesk