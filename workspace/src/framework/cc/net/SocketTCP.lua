--[[
For quick-cocos2d-x
SocketTCP lua
@author zrong (zengrong.net)
Creation: 2013-11-12
Last Modification: 2013-12-05
@see http://cn.quick-x.com/?topic=quickkydsocketfzl
]]
local SOCKET_TICK_TIME = 0.02 			-- check socket data interval
local SOCKET_RECONNECT_TIME = 3			-- socket reconnect try interval
local SOCKET_CONNECT_TIME = 0.3			-- connect socket interval
local SOCKET_CONNECT_FAIL_TIMEOUT = 6	-- socket failure timeout

local STATUS_CLOSED = "closed"
local STATUS_NOT_CONNECTED = "Socket is not connected"
local STATUS_ALREADY_CONNECTED = "already connected"
local STATUS_ALREADY_IN_PROGRESS = "Operation already in progress"
local STATUS_TIMEOUT = "timeout"

local scheduler = require("framework.scheduler")
local socket = require "socket"

local SocketTCP = class("SocketTCP")

SocketTCP.EVENT_DATA = "SOCKET_TCP_DATA"
SocketTCP.EVENT_CLOSE = "SOCKET_TCP_CLOSE"
SocketTCP.EVENT_CLOSED = "SOCKET_TCP_CLOSED"
SocketTCP.EVENT_CONNECTED = "SOCKET_TCP_CONNECTED"
SocketTCP.EVENT_CONNECT_FAILURE = "SOCKET_TCP_CONNECT_FAILURE"
SocketTCP.EVENT_WAIT_DATA = "SOCKET_TCP_WAIT_DATA" --缓存消息,等待下一帧触发

SocketTCP._VERSION = socket._VERSION
SocketTCP._DEBUG = socket._DEBUG

function SocketTCP.getTime()
	return socket.gettime()
end

function SocketTCP:ctor(__host, __port, __retryConnectWhenFailure)
	cc(self):addComponent("components.behavior.EventProtocol"):exportMethods()
	print("SocketTCP:ctor")
    self.host = __host
    self.port = __port
	self.tickScheduler = nil			-- timer for data
	self.reconnectScheduler = nil		-- timer for reconnect
	self.connectTimeTickScheduler = nil	-- timer for connect timeout
	self.name = 'SocketTCP'
	self.tcp = nil
	--self.isRetryConnect = __retryConnectWhenFailure
	self.isConnected = false
	self.ipaddr = ""
end

function SocketTCP:setName( __name )
	self.name = __name
	return self
end

function SocketTCP:setTickTime(__time)
	SOCKET_TICK_TIME = __time
	return self
end

function SocketTCP:setReconnTime(__time)
	SOCKET_RECONNECT_TIME = __time
	return self
end

function SocketTCP:setConnFailTime(__time)
	SOCKET_CONNECT_FAIL_TIMEOUT = __time
	return self
end

function SocketTCP:connect(__host, __port, __retryConnectWhenFailure,isGameServer)
	if __host then self.host = __host end
	if __port then self.port = __port end
	--if __retryConnectWhenFailure ~= nil then self.isRetryConnect = __retryConnectWhenFailure end
	assert(self.host or self.port, "Host and port are necessary!")
	printInfo("SocketTCP = %s.connect(%s, %d)", self.name, self.host, self.port)
	-- print(self.ipaddr)
	if not isGameServer then
		local function getaddrinfo(_host)
			local addrinfo, err
			local isipv6_only = false
			for i=1,3 do
				addrinfo, err = socket.dns.getaddrinfo(_host)
				if addrinfo and #addrinfo>0 then
					for i,v in ipairs(addrinfo) do
						if v.family == "inet6" then
							isipv6_only = true
								break
						end
					end
				else
					print("err = ",err)
				end
			end
			if not addrinfo or #addrinfo==0 and err then
				--qka.BuglyUtil:reportException("DNS failed","err:"..err.."\n".."ip:"..QkaPhoneInfoUtil:getPhoneIP().."\n"..debug.traceback())
				if _host == GlobalConf.LOGIN_SERVER_IP then
					addrinfo = {{addr = "60.205.231.7"},{addr = "59.110.137.132"}}
				end
			end
			return addrinfo, err, isipv6_only
		end

		-- 增加ipv6判断
		if device.platform == "ios" then
			local addrinfo, err, isipv6_only = getaddrinfo(self.host)
			print("ios 增加ipv6判断")
			print("isipv6_only", isipv6_only)
			
			if isipv6_only then
				self.tcp = socket.tcp6()
			else
				self.tcp = socket.tcp()	
			end
		else
			--保存为IP
			if not self.ipaddr or string.len(self.ipaddr) == 0 then
				local addrinfo, err, isipv6_only = getaddrinfo(self.host)
				if addrinfo and #addrinfo>0 then
					local randomIndex = math.random(100)%#addrinfo+1
					self.ipaddr = addrinfo[randomIndex].addr
				end
			end
			self.tcp = socket.tcp()
		end
	else
		-- 游戏服采用旧流程
		-- 增加ipv6判断
		if device.platform == "ios" then
			print("ios 增加ipv6判断")
			local isipv6_only = false
			local addrinfo, err = socket.dns.getaddrinfo(self.host)
			dump(addrinfo)
			print("err = ",err)
			for i,v in ipairs(addrinfo) do
				if v.family == "inet6" then
					isipv6_only = true
						break
				end
			end

			print("isipv6_only", isipv6_only)
			
			if isipv6_only then
				self.tcp = socket.tcp6()
			else
				self.tcp = socket.tcp()	
			end
		else
			self.tcp = socket.tcp()
		end
	end
	self.tcp:settimeout(0.2)
	local function __checkConnect()
		local __succ = self:_connect()
		if __succ then
			self:_onConnected()
		end
		return __succ
	end

	if not __checkConnect() then
		-- check whether connection is success
		-- the connection is failure if socket isn't connected after SOCKET_CONNECT_FAIL_TIMEOUT seconds
		local __connectTimeTick = function () 
			if self.isConnected then return end
			self.waitConnect = self.waitConnect or 0
			self.waitConnect = self.waitConnect + SOCKET_CONNECT_TIME
			if self.waitConnect >= SOCKET_CONNECT_FAIL_TIMEOUT then
                print("__checkConnect", self.host, self.port)
				--if g_GameController then
                --    g_GameController:onDestory()
	            --    g_GameController:atuoClearGameNetData()
                --    g_GameController:releaseInstance() 
                --   g_GameController = nil 
                --end
                --ToolKit:returnToLoginScene()
                --ToolKit:removeLoadingDialog()
				self:close(false)
				self.waitConnect = nil
				self:_connectFailure()
				--self.isConnected = false
				--if self.connectTimeTickScheduler then 
				--	scheduler.unscheduleGlobal(self.connectTimeTickScheduler)
				--	self.connectTimeTickScheduler = nil
				--end
				return
			end
			__checkConnect()
		end
		self.connectTimeTickScheduler = scheduler.scheduleGlobal(__connectTimeTick, SOCKET_CONNECT_TIME)
	end
end

function SocketTCP:send(__data)
	assert(self.isConnected, self.name .. " is not connected.")
	local ret,desc,sz = self.tcp:send(__data)
	if not ret then
		print("SocketTCP:send Err Desc:", desc)
		--if self.tickScheduler then 
		--	scheduler.unscheduleGlobal(self.tickScheduler)
		--	self.tickScheduler = nil
		--end
		--self:_onDisconnect()
		return -1
	else
		return 0
	end
end

function SocketTCP:close(bDispatch)
	print("close", self.name, bDispatch)
	self.isConnected = false
	self.tcp:close();
	-- self.ipaddr = ""
	self:_unregTimerCall()
	
	if bDispatch then
		self:dispatchEvent({name=SocketTCP.EVENT_CLOSE})
	end
end

function SocketTCP:_unregTimerCall()
	if self.connectTimeTickScheduler then 
		scheduler.unscheduleGlobal(self.connectTimeTickScheduler)
		self.connectTimeTickScheduler = nil
	end
	
	if self.tickScheduler then 
		scheduler.unscheduleGlobal(self.tickScheduler)
		self.tickScheduler = nil
	end
end

-- disconnect on user's own initiative.
function SocketTCP:disconnect()
	print("SocketTCP:disconnect")
	self:_disconnect()
	--self.isRetryConnect = false -- initiative to disconnect, no reconnect.
end

--------------------
-- private
--------------------

--- When connect a connected socket server, it will return "already connected"
-- @see: http://lua-users.org/lists/lua-l/2009-10/msg00584.html
function SocketTCP:_connect()
	print("SocketTCP:_connect():",self.ipaddr)
	local __succ, __status
	if self.ipaddr and string.len(self.ipaddr) > 0 then
		-- print("aaaaa:",self.ipaddr)
		__succ, __status = self.tcp:connect(self.ipaddr, self.port)
	else
		-- print("bbbbb:",self.host)
		__succ, __status = self.tcp:connect(self.host, self.port)
	end
	print("SocketTCP._connect:", __succ, __status, self.host, self.port, STATUS_ALREADY_CONNECTED)
	return __succ == 1 or __status == STATUS_ALREADY_CONNECTED
end

function SocketTCP:_disconnect()
	print("_disconnect", self.name)
	self.isConnected = false
	self.tcp:shutdown()
	self:dispatchEvent({name=SocketTCP.EVENT_CLOSE})
end

function SocketTCP:_onDisconnect()
	print("_onDisConnect", self.name);
	self.isConnected = false
	self:dispatchEvent({name=SocketTCP.EVENT_CLOSED})
	--self:_reconnect();
end

-- connecte success, cancel the connection timerout timer
function SocketTCP:_onConnected()
	print("SocketTCP:_onConnected")
	self.tcp:settimeout(0)
	self.isConnected = true
	self:dispatchEvent({name=SocketTCP.EVENT_CONNECTED})
	
	if self.connectTimeTickScheduler then
		scheduler.unscheduleGlobal(self.connectTimeTickScheduler)
		self.connectTimeTickScheduler = nil
	end
	
	if self.reconnectScheduler then 
		scheduler.unscheduleGlobal(self.reconnectScheduler)
		self.reconnectScheduler = nil
	end
	
	print("_onConnected", self.host, self.port)

	local __tick = function()
		while self.isConnected do
			-- if use "*l" pattern, some buffer will be discarded, why?
			local __body, __status, __partial = self.tcp:receive("*a")	-- read the package body
			--print("body:", __body, "__status:", __status, "__partial:", __partial)
    	    if __status == STATUS_CLOSED or __status == STATUS_NOT_CONNECTED then
		    	print("__tick", self.host, self.port, __status, STATUS_CLOSED, STATUS_NOT_CONNECTED)
				
				if self.tickScheduler then 
					scheduler.unscheduleGlobal(self.tickScheduler)
					self.tickScheduler = nil
				end
				
		    	--if self.isConnected then
		    	self:_onDisconnect()
		    	--else
		    	--	self:_connectFailure()
		    	--end
				--print("body:", __body, "__status:", __status, "__partial:", __partial)
		   		return
	    	end
			--print("body:", __body, "__status:", __status, "__partial:", __partial)
		    if 	(__body and string.len(__body) == 0) or (__partial and string.len(__partial) == 0) then
				self:dispatchEvent({name=SocketTCP.EVENT_WAIT_DATA})
				return
			end
			if __body and __partial then __body = __body .. __partial end
			self:dispatchEvent({name=SocketTCP.EVENT_DATA, data=(__partial or __body), partial=__partial, body=__body})
		end
	end

	-- start to read TCP data
	self.tickScheduler = scheduler.scheduleGlobal(__tick, SOCKET_TICK_TIME)
end

function SocketTCP:_connectFailure(status)
	printInfo("%s._connectFailure", self.name);
	self:dispatchEvent({name=SocketTCP.EVENT_CONNECT_FAILURE})
	-- self:_reconnect()
end

-- if connection is initiative, do not reconnect
function SocketTCP:_reconnect(__immediately)
	--if not self.isRetryConnect then return end
	print("_reconnect", self.name)
	if __immediately then self:connect() return end
	if self.reconnectScheduler then 
		scheduler.unscheduleGlobal(self.reconnectScheduler)
		self.reconnectScheduler = nil
	end
	local __doReConnect = function ()
		self:connect()
	end
	self.reconnectScheduler = scheduler.performWithDelayGlobal(__doReConnect, SOCKET_RECONNECT_TIME)
end

return SocketTCP
