module(..., package.seeall)


-- 游戏状态通知，不同状态表现不同， 广播或者上线后下发
-- 预备 等待接龙者发送包阶段, 结算到开始发放红包阶段
-- 抢红包阶段, 显示两只手托举的供抢红包的画面
CS_M2C_RedEnvelopes_GameState_Nty = 
{
	{ 1,	1, 'm_state'		 	 ,  'UINT'						, 1		, '状态 1-发红包阶段 2-抢红包阶段 3-结算阶段 4-空转状态(刚进去没人的时候)'},
	{ 2,	1, 'm_countDownTime'	 ,  'UINT'						, 1		, '如果是抢红包阶段, 这里显示为倒计时时间, 其他为0, 17s'},
	{ 3,	1, 'm_playerInfo'        ,  'PstRedEnvPlayerInfo'       , 8     , '桌面玩家, 不包括自己' },
	{ 4,	1, 'm_curGiverId'		 ,  'UINT'						, 1		, '当前接龙者玩家椅子ID'},
	{ 5,	1, 'm_leftNum'		 	 ,  'UINT'						, 1		, '剩余红包注, 抢红包阶段有效'},
	{ 6,	1, 'm_curCoin'		 	 ,  'UINT'						, 1		, '自己当前金币'},
	{ 7,	1, 'm_selfChairID'		 ,  'UINT'						, 1		, '自己的椅子号'},
	{ 8,	1, 'm_hasGrabEnv'		 ,  'UINT'						, 1		, '本局是否已经抢了红包 1-是, 0-否'},
	{ 9,	1, 'm_baseScore'		 ,  'UINT'						, 1		, '房间底分, 发红包金额, 10元即1000'},
	{ 10,	1, 'm_recordId'       	 ,  'STRING'       				, 1     , '牌局编号' },
}

-- 发红包, 发放红包者, 立即扣除房间底分金币
CS_C2M_RedEnvelopes_Give_Req = 
{
	
}

--如果一段时间玩家没有点击发红包, 由系统自动触发
CS_M2C_RedEnvelopes_Give_Nty = 
{
	{ 1,	1, 'm_ret'	 	 		 , 'INT'						, 1		, '发红包结果'},
	{ 2,	1, 'm_chairId'	 	 	 , 'UBYTE'						, 1		, '发红包玩家椅子号'},
	{ 3,	1, 'm_curCoin'	 	 	 , 'UINT'						, 1		, '发红包者金币'},
	{ 4,	1, 'm_countDownTime'	 ,  'UINT'						, 1		, '如果是发/抢红包阶段, 这里显示为倒计时时间, 其他为0, 17s'},
}

-- 抢红包
CS_C2M_RedEnvelopes_Grab_Req = 
{

}

-- 自己抢了红包, 到结算时才能看到红包金额(待确认)
-- 如果抢红包失败, 这里只发给抢红包玩家, 成功则通知所有玩家
CS_M2C_RedEnvelopes_Grab_Nty = 
{
	{ 1,	1, 'm_ret'	 	 		 , 'INT'						, 1		, '抢红包结果'},
	{ 2,	1, 'm_chairId'	 	 	 , 'UBYTE'						, 1		, '谁抢的椅子ID, 这里要播放飞红包效果, 自己的也要播放效果'},
	{ 3,	1, 'm_leftCount'	 	 , 'UINT'						, 1		, '剩余红包数量'},
}
 
--下发结算结果
CS_M2C_RedEnvelopes_GameBalance_Nty = 
{
	{ 1,	1, 'm_nick'		 		 ,  'STRING'					, 1		, '发放者昵称'},
	{ 2,	1, 'm_vecBalance'		 ,  'PstRedEnvBalanceInfo'		, 8		, '抢红包玩家信息'},
	{ 3,	1, 'm_nextId'		 	 ,  'UBYTE'						, 1		, '下一个发放红包玩家椅子ID'},
}

--开始
CS_M2C_RedEnvelopes_Start_Nty = 
{
	{ 1,	1, 'm_playerInfo'		 ,  'PstRedEnvPlayerInfo'		, 1		, '发放者信息'},
	{ 2,	1, 'm_countDownTime'	 ,  'UINT'						, 1		, '发红包阶段, 这里显示为倒计时时间, 其他为0, 17s'},
	{ 3,	1, 'm_recordId'       	 ,  'STRING'       				, 1     , '牌局编号' },
}

-- 新玩家加入
CS_M2C_RedEnvelopes_PlayerIn_Nty = 
{
	{ 1,	1, 'm_playerInfo'        ,  'PstRedEnvPlayerInfo'       , 1     , '玩家信息' },
}

-- 玩家退出
CS_M2C_RedEnvelopes_PlayerOut_Nty = 
{
	{ 1,	1, 'm_chairId'	 	 	, 'UBYTE'						, 1		, '退出玩家椅子ID'},
}

CS_C2M_RedEnvelopes_ForceExit_Req =
{
	
}

CS_M2C_RedEnvelopes_ForceExit_Ack =
{
	{ 1,	1, 'm_ret'			 , 'INT'						, 1		, '退出结果'},
}

CS_M2C_RedEnvelopes_Exit_Nty =
{
	{ 1,	1, 'm_type'			 , 'INT'						, 1		, '1-房间维护结算退出 2-你已经被系统踢出房间,请稍后重试 3-超过限定局数未操作'},
}

CS_C2M_RedEnvelopes_Background_Req =
{
	{ 1,	1, 'm_type'			 , 'INT'						, 1		, '1-切到后台 2-切回游戏'},
}

CS_M2C_RedEnvelopes_Background_Ack =
{
	{ 1,	1, 'm_type'			 , 'INT'						, 1		, '1-切到后台 2-切回游戏'},
	{ 2,	1, 'm_ret'			 , 'INT'						, 1		, '退出结果 0-表示成功'},
}