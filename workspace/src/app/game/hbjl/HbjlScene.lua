--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--
-- HbjlScene
-- Author: chenzhanming
-- Date: 2018-10-24 10:00:00
-- 百家乐主场景
--
local Scheduler           = require("framework.scheduler") 
local CCGameSceneBase     = require("src.app.game.common.main.CCGameSceneBase") 
local HbjlMainLayer     = import(".HbjlMainLayer")
local DlgAlert = require("app.hall.base.ui.MessageBox")
    
	
local HbjlScene= class("HbjlScene", function()
        return CCGameSceneBase.new()
    end)


function HbjlScene:ctor()  
    self.m_HbjlMainLayer         = nil 
    self.m_HbjlExitGameLayer     = nil
    self.m_HbjlRuleLayer         = nil
    self.m_HbjlMusicSetLayer     = nil 
    self:myInit()
end

-- 游戏场景初始化
function HbjlScene:myInit() 
 -- self:loadResources()
  --HbjlRoomController:getInstance():setInGame( true )  
  -- 主ui
 
  self:initHbjlGameMainLayer()


  self:registBackClickHandler(handler(self, self.onBackButtonClicked)) -- Android & Windows注册返回按钮
  addMsgCallBack(self, MSG_ENTER_FOREGROUND, handler(self, self.onEnterForeground)) -- 转前台
  addMsgCallBack(self, MSG_ENTER_BACKGROUND, handler(self, self.onEnterBackground)) -- 转后台 
  --addMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT, handler(self, self.socketState))
  --addMsgCallBack(self, POPSCENE_ACK,handler(self,self.showEndGameTip))
end

--[[
function HbjlScene:showEndGameTip()
	local dlg = DlgAlert.showTipsAlert({title = "提示", tip = "游戏已结束", tip_size = 34})
    dlg:setSingleBtn("确定", function ()
		dlg:closeDialog()
		g_GameController:releaseInstance()
    end)
    dlg:setBackBtnEnable(false)
    dlg:enableTouch(false)
end
--]]

---- 进入场景
function HbjlScene:onEnter()
   print("-----------HbjlScene:onEnter()-----------------")
   ToolKit:setGameFPS(1/60.0) 

end


-- 初始化主ui
function HbjlScene:initHbjlGameMainLayer()
    self.m_HbjlMainLayer = HbjlMainLayer.new()
    self:addChild(self.m_HbjlMainLayer)
end

function HbjlScene:getMainLayer()
    return self.m_HbjlMainLayer 
end

-- 显示游戏退出界面
-- @params msgName( string ) 消息名称
-- @params __info( table )   退出相关信息
-- 显示游戏退出界面
--[[
function HbjlScene:showExitGameLayer()
    if self._Scheduler1 then
        Scheduler.unscheduleGlobal(self._Scheduler1)	
        self._Scheduler1 = nil
    end 
    UIAdapter:popScene()
    g_GameController.gameScene = nil
end
--]]

function HbjlScene:onEnter()
	print("--------------HbjlScene:onEnter begin-----------------------")
	print("--------------HbjlScene:onEnter end-----------------------")
end
 
-- 退出场景
function HbjlScene:onExit()
    print("--------------HbjlScene:onExit begin-----------------------")
	
	if self.m_HbjlMainLayer then
		self.m_HbjlMainLayer:onExit()
		self.m_HbjlMainLayer = nil
	end

    removeMsgCallBack(self, MSG_ENTER_FOREGROUND)
    removeMsgCallBack(self, MSG_ENTER_BACKGROUND)
    --removeMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT)
    --removeMsgCallBack(self, POPSCENE_ACK)
	
--    HbjlGlobal.m_isNeedReconectGameServer = false
--    HbjlRoomController:getInstance():setInGame( false )
--    HbjlGameController:getInstance():onDestory()
 --   self:RemoveResources()
	print("--------------HbjlScene:onExit end-----------------------")
	
end

-- 响应返回按钮事件
function HbjlScene:onBackButtonClicked()
	if not g_GameController.m_canOut then
		TOAST("您处于游戏阶段，退出失败")
		return
    end
    g_GameController:rewhbjlForceExit()  
end

-- 从后台切换到前台
function HbjlScene:onEnterForeground()
	print("从后台切换到前台")  
	g_GameController:gameBackgroundReq(2)
end

-- 从前台切换到后台
function HbjlScene:onEnterBackground()
   print("从前台切换到后台,游戏线程挂起!")
   g_GameController:gameBackgroundReq(1)
   g_GameController.m_BackGroudFlag=true
end

function HbjlScene:clearView()
   print("HbjlScene:clearView()")
end
-- 清理数据
function HbjlScene:clearData()
   
end

--退出游戏处理
--[[
function HbjlScene:exitGame()   
    if self.m_HbjlMainLayer._Scheduler1 then
        Scheduler.unscheduleGlobal(self.m_HbjlMainLayer._Scheduler1)	
        self.m_HbjlMainLayer._Scheduler1 = nil
    end 
      if self.m_HbjlMainLayer._Scheduler2 then
            Scheduler.unscheduleGlobal(self.m_HbjlMainLayer._Scheduler2)	
            self.m_HbjlMainLayer._Scheduler2 = nil 
        end 
    UIAdapter:popScene()
    g_GameController.gameScene = nil
    g_GameController:onDestory()
end
--]]

return HbjlScene
