--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
local HNLayer= require("src.app.newHall.HNLayer") 
--endregion
local HbjlAccountLayer = class("HbjlAccountLayer",function()
    return HNLayer.new();
end)

  
function HbjlAccountLayer:ctor()   
    local root = UIAdapter:createNode("DragonTiger/HbjlRank.csb");
	self:addChild(root);  
     UIAdapter:praseNode(root,self)
      local center = root:getChildByName("Layer") 
     local diffY = (display.size.height - 750) / 2
    root:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)

    --center:setLocalZOrder(1)

end 
function HbjlAccountLayer:show(info)
    
    self.richtext = ccui.RichText:create()
    local textLabel1 = ccui.RichElementText:create(1, display.COLOR_RED, 255, info.m_nick,   "ttf/jcy.TTF", 28)
    local textLabel2 = ccui.RichElementText:create(2, display.COLOR_WHITE, 255, "发的红包",   "ttf/jcy.TTF", 28) 
    self.richtext:pushBackElement(textLabel1)
    self.richtext:pushBackElement(textLabel2)   
    self.Node_2:removeAllChildren()
    self.Node_2:addChild(self.richtext)
    self.nextId = info.m_nextId 
    if info.m_nextId == g_GameController.m_selfChairID then
        self.tjdwc_tanchu_bg_heitao_3:setTexture("DragonTiger/image/final_fail.png")
        g_GameController.m_canOut = false
    else
         g_GameController.m_canOut = true
         self.tjdwc_tanchu_bg_heitao_3:setTexture("DragonTiger/image/final_win.png")
    end
    self.listView_contentList:setClippingEnabled(true)
    self.listView_contentList:removeAllItems()
    for k,v in pairs(info.m_vecBalance) do 
		local panel = self:getOneItem(v,k); 
		self.listView_contentList:pushBackCustomItem(panel);
    end

    if self.m_pJieSuanEffect ~= nil then
        self.m_pJieSuanEffect:resetSystem()
    else
        self.m_pJieSuanEffect = cc.ParticleSystemQuad:create("DragonTiger/effect/jiesuan/jiangjinyu_lizi4.plist") 
        --self.m_pJieSuanEffect:setAutoRemoveOnFinish(true)
        --self.m_pJieSuanEffect:setPosition(cc.p(viewSize.width/2, -50))
        self.m_pJieSuanEffect:setPosition(cc.p(0,0))
        self.m_pJieSuanNode:addChild(self.m_pJieSuanEffect, 0)
    end

    self.image_root:setScale(0)
    self.image_root:runAction(cc.Sequence:create(cc.DelayTime:create(0.3), cc.ScaleTo:create(0.2, 1.0), nil));

end

function HbjlAccountLayer:getOneItem(userInfo,index)
    
    local csbNode =self.panel_itemModel:clone()
    local image_avatar = csbNode:getChildByName("image_avatar")
    local head = ToolKit:getHead( userInfo.m_faceId)
     image_avatar:loadTexture(head,1)
     image_avatar:setScale(0.5)
     local text_name =csbNode:getChildByName("text_name")
     text_name:setString(userInfo.m_nickname)
      local text_money =csbNode:getChildByName("text_money")
     text_money:setString(userInfo.m_grabCoin*0.01)
     if userInfo.m_chairId == self.nextId then
        local image_jielong=csbNode:getChildByName("image_jielong")
        image_jielong:setVisible(true)
        image_jielong:setScale(0.8)
     end
     if g_GameController.m_selfChairID == userInfo.m_chairId then
        local line =csbNode:getChildByName("line")
        line:setVisible(true)
    end
    return csbNode
end  

return HbjlAccountLayer