local CommonRoom = import("app.newHall.layer.CommonRoom")
local HbjlRoomLayer = class("HbjlRoomLayer", CommonRoom)

local PREFIX        = "game/RedEnvelopGambling/"
local PATH_ROOM_CSB = "game/RedEnvelopGambling/RedModeChooseDialog.csb"

local CONFIG = {
	[1] = {
        RoomScore   = 1,
		AnimName 	= PREFIX .. "effect/325_hbsl_hongbao/325_hbsl_hongbao",
	},
	[2] = {
        RoomScore   = 1000,
		AnimName 	= PREFIX .. "effect/325_hbsl_caishen/325_hbsl_caishen",
	},
    [3] = {
        RoomScore   = 10000,
		AnimName 	= PREFIX .. "effect/325_hbsl_hbjl/hbjl",
	},
}

function HbjlRoomLayer:ctor(roomList)
    self:init(roomList)
end

function HbjlRoomLayer:init(roomList)
    self._roomList = roomList
    self:initCSB()
end

function HbjlRoomLayer:initCSB()
    --init root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    --init csb
    self.m_pathUI = cc.CSLoader:createNode(PATH_ROOM_CSB)
    self.m_rootUI:addChild(self.m_pathUI)

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("ModeChooseDialog")
    local diffY = (display.size.height - 750) / 2
     self.m_pathUI:setPosition(cc.p(0,diffY)) 
    local diffX = 145-(1624-display.size.width)/2 
    self.m_pNodeRoot:setPositionX(diffX)
    self.m_pNodeRootUI   = self.m_pNodeRoot:getChildByName("Image_bg")
    self.m_pBtnMode1     = self.m_pNodeRootUI:getChildByName("BTN_mode_1")
    self.m_pBtnMode1:setPositionY(self.m_pBtnMode1:getPositionY()-10)
    self.m_pBtnMode2     = self.m_pNodeRootUI:getChildByName("BTN_mode_2")
    self.m_pBtnMode2:setPositionY(self.m_pBtnMode2:getPositionY()-10)
    self.m_pBtnMode3     = self.m_pNodeRootUI:getChildByName("BTN_mode_3")
    self.m_pBtnMode3:setPositionY(self.m_pBtnMode3:getPositionY()-10)

    self.m_pNodeTop      = self.m_pNodeRoot:getChildByName("Node_Top")
    self.m_pImgTitle     = self.m_pNodeTop:getChildByName("Image_title")
    --self.m_pImgTitle:setVisible(false)
    self.m_pBtnClose     = self.m_pNodeTop:getChildByName("Button_Close")

    self.m_pPanelInfo    = self.m_pNodeRoot:getChildByName("Panel_Info")
    self.m_pImgHead      = self.m_pPanelInfo:getChildByName("Image_Head")
    self.m_pImgFrame     = self.m_pPanelInfo:getChildByName("Image_Frame")
    self.m_pTextName     = self.m_pPanelInfo:getChildByName("Text_Name")
    self.m_pSpVip        = self.m_pPanelInfo:getChildByName("Sprite_Vip")
    self.m_pTextGold     = self.m_pPanelInfo:getChildByName("Image_GoldBg"):getChildByName("FontLabel")
    self.m_pBtnAdd       = self.m_pPanelInfo:getChildByName("Image_GoldBg"):getChildByName("Button")
    self.m_pBtnAdd:setVisible(false)
    self.m_pBtnQuick     = self.m_pPanelInfo:getChildByName("Button_Quick")

    self.m_pBtnClose:addTouchEventListener(handler(self,self.onReturnClicked))

    self.m_pBtnMode1:addTouchEventListener(function(sender)
        self:onModeItemClicked(1)
    end)
    local skeNode1 = sp.SkeletonAnimation:createWithBinaryFile(CONFIG[1].AnimName .. ".skel", CONFIG[1].AnimName .. ".atlas", 1)
    if skeNode1 then
        skeNode1:setPosition(cc.p(0 ,-10))
        skeNode1:setAnimation(0, "animation", true)
        skeNode1:addTo(self.m_pBtnMode1:getChildByName("Node_Spine"))
    end

    self.m_pBtnMode2:addTouchEventListener(function(sender)
        self:onModeItemClicked(2)
    end)
    local skeNode2 = sp.SkeletonAnimation:createWithBinaryFile(CONFIG[2].AnimName .. ".skel", CONFIG[2].AnimName .. ".atlas", 1)
    if skeNode2 then
        skeNode2:setPosition(cc.p(0 ,-30))
        skeNode2:setAnimation(0, "animation", true)
        skeNode2:addTo(self.m_pBtnMode2:getChildByName("Node_Spine"))
    end

    self.m_pBtnMode3:addTouchEventListener(function(sender)
        self:onModeItemClicked(3)
    end)
    local skeNode3 = sp.SkeletonAnimation:createWithJsonFile(CONFIG[3].AnimName .. ".json", CONFIG[3].AnimName .. ".atlas", 1)
    if skeNode3 then
        skeNode3:setPosition(cc.p(60 ,-30))
        skeNode3:setAnimation(0, "animation", true)
        skeNode3:addTo(self.m_pBtnMode3:getChildByName("Node_Spine"))
    end

    self.m_pBtnQuick:addTouchEventListener(function(sender)
        g_GameMusicUtil:playSound("public/sound/sound-close.mp3")
        self:quickClicked()
    end)

    --适配分辨率
--    print("display.size.width:",display.size.width)
--    local diffX = (display.size.width - 1624)/2
--    self.m_pNodeRootUI:setPositionX(self.m_pNodeRootUI:getPositionX() + diffX)
--    self.m_pImgTitle:setPositionX(self.m_pImgTitle:getPositionX() + diffX)
--    self.m_pPanelInfo:setPositionX(self.m_pPanelInfo:getPositionX() +diffX)
--    self.m_pImgTitle:setPositionX(self.m_pImgTitle:getPositionX() + diffX)

--    local diffY = (display.height - 750) / 2
--    self.m_pImgTitle:setPositionY(self.m_pImgTitle:getPositionY() + diffY)
--    self.m_pBtnClose:setPositionY(self.m_pBtnClose:getPositionY() + diffY)

    self:onMsgUpdateUserInfo()
    self:flyIn()
end

function HbjlRoomLayer:onMsgUpdateUserInfo()
    --头像
    local index = Player:getFaceID()
	local str =ToolKit:getHead(index)
	self.m_pImgHead:loadTexture(str, ccui.TextureResType.plistType)

    --名字
    local strName = Player:getNickName()
    self.m_pTextName:setString(strName)

    --金币
    local goldNum = Player:getGoldCoin()
    self.m_pTextGold:setString(goldNum)
end

function HbjlRoomLayer:flyIn()

    --tile
    self.m_pNodeTop:setPositionY(self.m_pNodeTop:getPositionY() + 200)
    local action = cc.EaseBackOut:create(cc.MoveBy:create(0.4, cc.p(0, -200)))
    local seq = cc.Sequence:create( cc.DelayTime:create(0.2), action )
    self.m_pNodeTop:runAction(seq)

    --bottom
    self.m_pPanelInfo:setPositionY(self.m_pPanelInfo:getPositionY()-150)
    local action = cc.MoveBy:create(0.25, cc.p(0, 150))
    local seq = cc.Sequence:create( action )
    self.m_pPanelInfo:runAction(seq)

    --图标
    self:runInAction(self.m_pBtnMode1, -600, 0, 0.4)
    self:runInAction(self.m_pBtnMode2, 600, 0, 0.4)
    self:runInAction(self.m_pBtnMode3, 1800, 0, 0.4)
end

function HbjlRoomLayer:runInAction(node, x, y, rt, dt)
    node:setPositionX(node:getPositionX()+x)
    node:setPositionY(node:getPositionY()+y)
    local action = cc.EaseBackOut:create(cc.MoveBy:create(rt, cc.p(-x, -y)))
    node:runAction(action)
end

--选择模式
function HbjlRoomLayer:onModeItemClicked(tag)
    g_GameMusicUtil:playSound("public/sound/sound-button.mp3")
    if g_GameController then
		g_GameController:releaseInstance()
	end
    g_GameController = require(self._roomList[tag].controllerPath):getInstance()
    g_GameController:bindGameTypeId(self._roomList[tag].gameId)
    ConnectManager:send2SceneServer(self._roomList[tag].gameId, "CS_C2M_Enter_Req", {self._roomList[tag].gameId, "" })
end

function HbjlRoomLayer:quickClicked(tag)
if g_GameController then
	g_GameController:releaseInstance()
end
    g_GameController = require(self._roomList[1].controllerPath):getInstance()
    g_GameController:bindGameTypeId(self._roomList[1].gameId)
    ConnectManager:send2SceneServer(self._roomList[1].gameId, "CS_C2M_Enter_Req", {self._roomList[1].gameId, "" })
end

function HbjlRoomLayer:onReturnClicked(sender,eventType)
    if eventType~=ccui.TouchEventType.ended then
        return
    end
    --g_GameMusicUtil:playSound("public/sound/sound-close.mp3")

    local delayTime = 0.12
    if self.m_pNodeListTop ~= nil then
        for i, v in pairs(self.m_pNodeListTop) do
            v:runAction( cc.MoveBy:create(delayTime, cc.p(0, 200)))
        end
    end
    if self.m_pNodeListBar ~= nil then
        for i, v in pairs(self.m_pNodeListBar) do
            v:runAction( cc.MoveBy:create(delayTime, cc.p(0, -200)))
        end
    end
    if self.m_pNodeListGame ~= nil then
        for i, v in pairs(self.m_pNodeListGame) do
            v:runAction( cc.FadeOut:create( delayTime ))
        end
    end
    if self.m_pNodeMM ~= nil then
        local moveAct = cc.MoveBy:create(delayTime, cc.p(-300, 0))
        local spawnAct = cc.Spawn:create(cc.FadeOut:create(delayTime), moveAct)        
        self.m_pNodeMM:runAction( spawnAct )
    end
    local callback = cc.CallFunc:create(function ()
        self:close()
        --g_GameMusicUtil:playBGMusic()
    end)
    local seq = cc.Sequence:create( cc.DelayTime:create(delayTime) ,callback)
    self:runAction(seq)
end

return HbjlRoomLayer
