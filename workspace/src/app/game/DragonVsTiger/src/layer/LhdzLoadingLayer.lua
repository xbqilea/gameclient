--region LhdzLoadingLayer.lua
--Date
--Auther Ace
--Desc [[龙虎斗loading界面]]
--此文件由[BabeLua]插件自动生成


local CommonLoading = require("src.app.newHall.layer.CommonLoading")
local LhdzLoadingLayer = class("LhdzLoadingLayer", function ()
    return CommonLoading.new()
end)
   
local LhdzDataMgr = import("..manager.LhdzDataMgr")
local Lhdz_Res = import("..scene.Lhdz_Res")
local Lhdz_Const = import("..scene.Lhdz_Const")
local Lhdz_Events = import("..manager.Lhdz_Events")
local ParticalPool = import("..bean.ParticalPool")
local PATH_CSB = "hall/csb/CommonLoading.csb"
local PATH_BG = "game/longhudazhan/image/gui-bg-load.jpg"
local PATH_LOGO1 = "game/longhudazhan/image/gui-logo-load-1.png"
local PATH_LOGO2 = "game/longhudazhan/image/gui-logo-load-2.png"

function LhdzLoadingLayer.loading()
    return LhdzLoadingLayer.new(true)
end

function LhdzLoadingLayer.reload()
    return LhdzLoadingLayer.new(false)
end

function LhdzLoadingLayer:ctor(bBool)
    self:setNodeEventEnabled(true)
    self.bLoad = bBool
    self:init()
end

function LhdzLoadingLayer:init() 
  --  self.super:init(self)
    self:initCSB()
    self:initCommonLoad()
     if cc.exports.g_SchulerOfLoading then
        scheduler.unscheduleGlobal(cc.exports.g_SchulerOfLoading)
        cc.exports.g_SchulerOfLoading = nil
    end

  --  local self = self.m_child
 --   if self.bLoad then
        self:startLoading()
end

--function LhdzLoadingLayer:onEnter()
--    self.super:onEnter()
--end

--function LhdzLoadingLayer:onExit()
--    self.super:onExit()
--end

function LhdzLoadingLayer:initCSB()

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    --ccb
    self.m_pathUI = cc.CSLoader:createNode(PATH_CSB)
    self.m_pathUI:setPositionX((display.width - 1624) / 2)
    self.m_pathUI:addTo(self.m_rootUI)

    --node
    self.m_pNodeBase = self.m_pathUI:getChildByName("Layer_base")
    self.m_pNodeBg   = self.m_pNodeBase:getChildByName("Node_bg")
    self.m_pNodeLoad = self.m_pNodeBase:getChildByName("Node_load")
    self.m_pNodeText = self.m_pNodeBase:getChildByName("Node_text")

    --bar
    self.m_pLoadingBar = self.m_pNodeLoad:getChildByName("LoadingBar")

    --text
    self.m_pLabelPercent = self.m_pNodeText:getChildByName("Text_percent")
    self.m_pLabelWord    = self.m_pNodeText:getChildByName("Text_word")

    --image
    self.m_pImageLogo = self.m_pNodeBg:getChildByName("Image_logo")
    self.m_pImageBg   = self.m_pNodeBg:getChildByName("Image_bg")
    self.m_pImageBg:loadTexture(PATH_BG, ccui.TextureResType.localType)

    --百度推广需求
--    if CommonUtils:getInstance():getIsBaiduCheck() then
--        self.m_pImageLogo:setVisible(false)
--    end
--    if ClientConfig.getInstance():getIsMainChannel() then
        self.m_pImageLogo:loadTexture(PATH_LOGO1, ccui.TextureResType.localType)
        self.m_pImageLogo:setContentSize(cc.size(910, 579))
--    else
--        self.m_pImageLogo:loadTexture(PATH_LOGO2, ccui.TextureResType.localType)
--        self.m_pImageLogo:setContentSize(cc.size(910, 579))
--    end
end

function LhdzLoadingLayer:initCommonLoad()
    
    -------------------------------------------------------
    --设置界面ui
    self:setLabelPercent(self.m_pLabelPercent) --百分比文字
--    self:setLabelWord(self.m_pLabelWord)       --提示文字
    self:setBarPercent(self.m_pLoadingBar)     --进度条
    -------------------------------------------------------

    -- load animation
    self.m_pListTable = {}
    for _,strArmName in pairs(Lhdz_Res.vecAnim) do
        local strPath = string.format("%s%s/%s.ExportJson", Lhdz_Res.strAnimPath, strArmName, strArmName)
        table.insert(self.m_pListTable, strPath)
    end 
    table.insert(self.m_pListTable, Lhdz_Res.ANI_OF_WINNER.FILEPATH)
    table.insert(self.m_pListTable, Lhdz_Res.ANI_OF_TOPWINEFFECT.FILEPATH)
    table.insert(self.m_pListTable, Lhdz_Res.ANI_OF_CHANGEUSER.FILEPATH)
    table.insert(self.m_pListTable, Lhdz_Res.ANI_OF_LAOHUTEXIAO.FILEPATH)

    --创建粒子效果
    ParticalPool.getInstance():setResName(Lhdz_Res.PLIST_OF_BAGUA)
    local fun = function()
        ParticalPool.getInstance():addNewPartical()
    end

    local funList = {}
    for i = 1, Lhdz_Const.GAME_DOWN_COUNT * Lhdz_Const.FLYPARTICAL_COUNT do
        table.insert( funList, fun )
    end

    table.insert( funList, function()
        ParticalPool.getInstance():createRichWinEffect()
    end )

    table.insert( funList, function()
        ParticalPool.getInstance():createLuckyWinEffect()
    end )

    for i = 1, 2 do
        table.insert( funList, function()
            ParticalPool.getInstance():createBGEffect(i)
        end )
        table.insert( funList, function()
            ParticalPool.getInstance():createLanternEffect(i)
        end )
        table.insert( funList, function()
            ParticalPool.getInstance():createAnimalEffect(i)
        end )
    end
    -------------------------------------------------------
    --音效/音乐/骨骼/动画/动画/碎图/大图/其他
    self:addLoadingList(self.m_pListTable,        self.TYPE_EFFECT)
    self:addLoadingList(funList,                  self.TYPE_OTHER)
    self:addLoadingList(Lhdz_Res.vecPlist,        self.TYPE_PLIST)
    self:addLoadingList(Lhdz_Res.vecReleaseImg,   self.TYPE_PNG)
    self:addLoadingList(Lhdz_Res.vecLoadingSound, self.TYPE_SOUND)
    -------------------------------------------------------
end

return LhdzLoadingLayer
--endregion
