--region LhdzMainLayer.lua
--Date
--Auther Ace
--Des [[龙虎斗主界面]]
--此文件由[BabeLua]插件自动生成
local HNLayer= require("src.app.newHall.HNLayer") 
local LhdzMainLayer = class("LhdzMainLayer", function()
    return HNLayer.new()
end)
 
local LhdzDataMgr = import("..manager.LhdzDataMgr")
local Lhdz_Res = import("..scene.Lhdz_Res")
local Lhdz_Const = import("..scene.Lhdz_Const")
local Lhdz_Events = import("..manager.Lhdz_Events")
local ParticalPool = import("..bean.ParticalPool")

local ChipFlyManager = import("..bean.ChipFlyManager")
local LhdzRuleLayer = import(".LhdzRuleLayer")
local LhdzRankLayer = import(".LhdzRankLayer")
local LhdzTrendLayer = import(".LhdzTrendLayer")
local LhdzUserInfoLayer = import(".LhdzUserInfoLayer")
local CBetManager = require("src.app.game.common.util.CBetManager")
local GameRecordLayer= require("src.app.newHall.childLayer.GameRecordLayer")
local GameSetLayer= require("src.app.newHall.childLayer.SetLayer") 
local ChatLayer = require("src.app.newHall.chat.ChatLayer") 
local AudioManager = cc.exports.AudioManager
local scheduler = require("framework.scheduler")

local Chip_EnableOpacity = 255
local Chip_DisableOpacity = 180
local Game_Chip_Times = 1

local function LOG_PRINT(...) if true then printf(...) end end 
function LhdzMainLayer:ctor()
    -- 进入游戏重置设计分辨率
  --  LuaUtils.setIphoneXDesignResolution()
  --  self:setNodeEventEnabled(true)
    self.event_ = {}
    self.handle_ = {}
    self:init() 
    math.randomseed(tostring(os.time()):reverse():sub(1, 7))
end

function LhdzMainLayer:onEnter()
    LOG_PRINT(" - LhdzMainLayer onEnter - ")
    
end

function LhdzMainLayer:onExit()
    self:clear()
    if self._Scheduler1 then
		scheduler.unscheduleGlobal(self._Scheduler1)	
        self._Scheduler1 = nil
    end
    LOG_PRINT(" - LhdzMainLayer onExit - ")
end

-- 初始化变量
function LhdzMainLayer:initAllVar_()
    self.m_rootUI = nil             -- 根节点
    -- node.
    self.m_pNodeRoot = nil
    self.m_pNodeTable = nil
    self.m_pNodeBetArea = nil
    self.m_pNodeBetValue = nil
    self.m_pNodeHistory = nil
    self.m_pNodeTableView = nil
    self.m_pNodeBanker = nil
    self.m_pNodeCountTime = nil

    self.m_pNodeButton = nil
    self.m_pNodeMenu = nil

    self.m_pNodeJetton = nil
    self.m_pNodeUserInfo = nil
    self.m_pNodePlayer = nil
    self.m_pNodeOnliePlayer = nil
    self.m_pNodeShowPlayer = {}
    self.m_pNodeChip = nil
    self.m_pNodeArmature = nil
    self.m_pNodeBackground = nil
    self.m_pNodeLantern = nil
    self.m_pNodeStar = nil
    self.m_pNodeUserTips = nil
    self.m_pNodeAnimVS = nil
    self.m_pNodeTips = nil
    -- btn
    self.m_pBtnExit = nil
    self.m_pBtnMenuPush = nil
    self.m_pBtnMenuPop = nil
    self.m_pBtnTrend = nil
    self.m_pBtnMenuClose = nil
    self.m_pBtnContinue = nil
    self.m_pBtnRule = nil
    self.m_pBtnEffect = nil
    self.m_pBtnMusic = nil
    self.m_pBtnBank = nil
    self.m_pBtnOnliePlayer = nil
    self.m_pBtnJetton = {}
    self.m_pBtnBetArea = {}
    self.m_pBtnUserInfo = {}
    --
    self.m_pSpCountTime = nil
    self.m_pSpStauts = nil
    self.m_pSpHistoryNew = nil
    self.m_pSpHistoryLight = nil
    self.m_pSpBankerTips = nil
    self.m_pNodeTablePlayer = {}
    self.m_pSpHeadIcon = {}
    self.m_pSpNoPlayer = {}
    self.m_pSpHistory = {}
    self.m_pSpStar = {}
    self.m_pSpWinArea = {}
    self.m_pParticleStar = {}

    self.m_pLbUserName = nil
    self.m_pLbUserGold = nil
    self.m_pLbResultGold = nil
    self.m_pLbCountTime = nil
    self.m_pLbBankerName = nil
    self.m_pLbBankerGold = nil
    self.m_pLbCondition = nil
    self.m_pLbBankerCount = nil
    self.m_pLbOnlieCount = nil
    self.m_pLbAllBetValue = {}
    self.m_pLbMyBetValue = {}
    self.m_pLbShowPlayerName = {}
    self.m_pLbShowPlayerGold = {}
    self.m_pLbTableResultGold = {}
    self.m_pImgPlayerVip = {}

    self.lightStar_bagua = {} --八卦光效
    self.flyStar_bagua = {} --八卦

    ----------------
    -- 自己的下注筹码
    self.m_vecSelfJetton = {}
    -- 上桌玩家的下注筹码
    self.m_vecPlayerJetton = {}
    for i = 1, Lhdz_Const.TABEL_USER_COUNT do
        self.m_vecPlayerJetton[i] = {}
    end
    -- anim
    self.m_pAnimWaitNext = nil
    self.m_pAnimCard = {}
    self.m_bIsShowStar = {false, false, false}
    --self.m_areaWinEffect = {}

    -- 变量
    self.m_bIsMenuMove = false
    self.m_bIsPlayVSAnim = false
    self.m_bIsRequestRank = false
    self.m_isPlayBetSound = false
    self.m_nIndexChoose = 0
    self.m_tmLastClicked = 0
    self.m_iLastPlayerGold = 0
    self.m_getChipEffect = {}
    self.m_userBtnVec = {}
    self.m_userBtnResPosVec = {}
    self.m_particalvec = {}
    self.m_bEnterBackground = false
    self.m_changeUserAniVec = {}            --上桌玩家切换时动画

    --神算子和大富豪中奖特效
    self.m_luckyWinEffect = nil
    self.m_richWinEffect = nil

    self.m_dNo1AllBet = {0,0,0} -- 神算子的投注
    self.m_flyJobVec = {} --飞行筹码任务清空
    self.m_chipMgr = nil
end
---------------------------- constructor --------------------------------------

---------------------------------- init view start ----------------------------------
function LhdzMainLayer:init() --onEnter:init
    local _do = {
        { func = self.initVar,      log = "初始化变量",  },
        { func = self.initCCB,      log = "加载界面",    },
        { func = self.initNode,     log = "初始化界面",  },
        { func = self.initChipMgr,  log = "初始飞筹码",  },
        { func = self.initEvent,    log = "注册监听",    },
        { func = self.initGame,     log = "初始化游戏",  },
    }
    for i, init in pairs(_do) do
        LOG_PRINT(" - [%d] - [%s] - %s", i, init.func(self) or "ok",  init.log)
    end
end

function LhdzMainLayer:clear() --onExit:clear
    local _do = {
        { func = self.cleanEvent,   log = "取消监听",   },
        { func = self.cleanGame,    log = "清理游戏",   },
    }
    for i, clean in pairs(_do) do
        LOG_PRINT(" - [%d] - [%s] - %s", i, clean.func(self) or "ok", clean.log)
    end
end

function LhdzMainLayer:initVar()
    self:initAllVar_()
end

-- 加载界面
function LhdzMainLayer:initCCB()
    LOG_PRINT("LhdzMainLayer:initCSB111")
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)
    -- load csb.
    self.m_rootWidget = cc.CSLoader:createNode(Lhdz_Res.CSB_OF_MAINSCENE)
    local diffY = (display.size.height - 750) / 2
    -- 屏幕适配，截取可视区域
    local shap = cc.DrawNode:create()
    local pointArr = {cc.p(0,0), cc.p(1624, 0), cc.p(1624, 750), cc.p(0, 750)}
    shap:drawPolygon(pointArr, 4, cc.c4f(255, 255, 255, 0), 2, cc.c4f(255, 255, 255, 0))
    self.m_pClipping = cc.ClippingNode:create(shap)
    self.m_pClipping:setPosition(cc.p(0,diffY))
    self.m_pClipping:addChild(self.m_rootWidget)
    self.m_rootUI:addChild(self.m_pClipping)
    
    local diffX = 145 - (1624-display.size.width)/2 
    self.m_pNodeRoot = self.m_rootWidget:getChildByName("Panel_root")
    self.m_pNodeRoot:setPositionX(diffX)
    -----node-----------
    self.m_pNodeBackground = self.m_pNodeRoot:getChildByName("Node_background")
    -- 灯笼
    self.m_pNodeLantern = self.m_pNodeRoot:getChildByName("Node_lantern")
    -- 桌子节点
    self.m_pNodeTable = self.m_pNodeRoot:getChildByName("Node_table")
    -- 下注区域节点
    self.m_pNodeBetArea = self.m_pNodeTable:getChildByName("Node_betArea")
    -- 下注数值节点
    self.m_pNodeBetValue = self.m_pNodeTable:getChildByName("Node_betValue")
    -- 历史记录节点
    self.m_pNodeHistory = self.m_pNodeTable:getChildByName("Node_history")
    -- 历史记录TableView节点
    self.m_pNodeTableView = self.m_pNodeHistory:getChildByName("Panel_table")
    -- 庄家节点
    self.m_pNodeBanker = self.m_pNodeTable:getChildByName("Node_banker")
    -- 倒计时节点
    self.m_pNodeCountTime = self.m_pNodeTable:getChildByName("Node_countTime")
    -- 神算子星
    self.m_pNodeStar = self.m_pNodeTable:getChildByName("Node_star")
    
    self.m_pNodeButton = self.m_pNodeRoot:getChildByName("Node_button")
    self.m_pNodeMenu = self.m_pNodeButton:getChildByName("Node_menu")

    self.m_pNodeJetton = self.m_pNodeRoot:getChildByName("Node_jetton")
    self.m_pNodeUserInfo = self.m_pNodeRoot:getChildByName("Node_userInfo")
    
    self.m_pNodePlayer = self.m_pNodeRoot:getChildByName("Node_player")
    self.m_pNodeOnliePlayer = self.m_pNodePlayer:getChildByName("Node_onliePlayer")
    self.m_pBtnChat = self.m_pNodePlayer:getChildByName("Button_chat")
    self.m_pSpChat = self.m_pBtnChat:getChildByName("Image_1")
    self.m_pSpChat:setOpacity(255*0.3)

    -- 消息动画
    local jsonName = "public/effect/chatanimation/GetNewMessage.ExportJson"
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(jsonName)
    local sz = self.m_pSpChat:getContentSize()
    self.m_pMsgArmature = ccs.Armature:create("GetNewMessage")
    self.m_pMsgArmature:getAnimation():play("Animation1",-1,1)
    self.m_pMsgArmature:setPosition(cc.p(sz.width/2,sz.height/2))
    self.m_pMsgArmature:setName("NewMsgAnimation")
    self.m_pMsgArmature:addTo(self.m_pSpChat)
    self.m_pMsgArmature:setScale(0.7)
    self.m_pMsgArmature:setVisible(false)

    -- local animationEvent = function(armatureBack, movementType, movementID)
    --     if movementType == ccs.MovementEventType.complete then
    --         if movementID == Lhdz_Res.ANI_OF_CHANGEUSER.ANILIST.BEGIN.NAME then
    --             local faceid = armatureBack:getTag()
    --             self:replaceUserHeadSprite(armatureBack, Lhdz_Res.ANI_OF_CHANGEUSER.ANILIST.BEGIN, faceid)
    --             armatureBack:getAnimation():play(Lhdz_Res.ANI_OF_CHANGEUSER.ANILIST.END.NAME)
    --         elseif movementID == Lhdz_Res.ANI_OF_CHANGEUSER.ANILIST.END.NAME then

    --         end
    --     end
    -- end

    for i=1, Lhdz_Const.TABEL_USER_COUNT do
        self.m_pNodeTablePlayer[i] = self.m_pNodePlayer:getChildByName(string.format("Node_tablePlayer%d",i))
        self.m_pSpNoPlayer[i] = self.m_pNodeTablePlayer[i]:getChildByName("Sprite_noBody")
        self.m_pNodeShowPlayer[i] = self.m_pNodeTablePlayer[i]:getChildByName("Node_showPlayer")
        self.m_pLbTableResultGold[i] = self.m_pNodeTablePlayer[i]:getChildByName("BitmapFontLabel_result")
        self.m_pLbTableResultGold[i]:setVisible(false)
        --self.m_pLbTableResultGold[i]:setScale(0.5)
        self.m_userBtnVec[i] = self.m_pNodeShowPlayer[i]:getChildByName("Button_userInfo")
        self.m_userBtnResPosVec[i] = cc.p(self.m_userBtnVec[i]:getPosition())
        self.m_pSpHeadIcon[i] = self.m_userBtnVec[i]:getChildByName("Sprite_head")
        self.m_pSpHeadIcon[i]:setVisible(false)
        self.m_pLbShowPlayerName[i] = self.m_pNodeShowPlayer[i]:getChildByName("Text_showPlayer_name")
        self.m_pLbShowPlayerGold[i] = self.m_pNodeShowPlayer[i]:getChildByName("BitmapFontLabel_showPlayer_gold")
        self.m_pBtnUserInfo[i] = self.m_pNodeShowPlayer[i]:getChildByName("Button_userInfo")
        self.m_pImgPlayerVip[i] = self.m_pNodeShowPlayer[i]:getChildByName("Image_vip")

        self.m_changeUserAniVec[i] = ccs.Armature:create(Lhdz_Res.ANI_OF_CHANGEUSER.FILENAME)
        self.m_changeUserAniVec[i]:addTo(self.m_userBtnVec[i]:getChildByName("Node_ani"))
        --self.m_changeUserAniVec[i]:getAnimation():setMovementEventCallFunc(animationEvent)
        self.m_changeUserAniVec[i]:setTag(255)
    end

    self.m_pNodeChip = self.m_pNodeRoot:getChildByName("Node_chip")
    self.m_pNodeUserTips = self.m_pNodeRoot:getChildByName("Node_userTips")
    self.m_pNodeArmature = self.m_pNodeRoot:getChildByName("Node_anim")
    self.m_pNodeAnimVS = self.m_pNodeRoot:getChildByName("Panel_animVS")
    self.m_pNodeTips = self.m_pNodeRoot:getChildByName("Node_tips")
    -- self.m_pNodeTips:setPosition(cc.p(667, 245))
    
    -------------------
    self.m_pSpMenuBg = self.m_pNodeMenu:getChildByName("Sprite_menu_bg")
    ----- Button --------
    self.m_pBtnExit = self.m_pNodeButton:getChildByName("Button_exit")
    self.m_pBtnMenuPush = self.m_pNodeButton:getChildByName("Button_push")
    self.m_pBtnMenuPop = self.m_pNodeButton:getChildByName("Button_pop")
    self.m_pBtnTrend = self.m_pNodeButton:getChildByName("Button_trend")
    self.m_pBtnTrend2 = self.m_pNodeButton:getChildByName("Button_trend2")
    self.m_pBtnContinue = self.m_pNodeButton:getChildByName("Button_continue")
    self.m_pBtnMenuClose = self.m_pSpMenuBg:getChildByName("Button_menu_close")
    self.m_pBtnRule = self.m_pSpMenuBg:getChildByName("Button_rule")
    self.m_pBtnEffect = self.m_pSpMenuBg:getChildByName("Button_effect")
    self.m_pBtnMusic = self.m_pSpMenuBg:getChildByName("Button_music")
    -- 银行保险柜
    self.m_pBtnStrongBox = self.m_pSpMenuBg:getChildByName("Button_strongBox")
    self.m_pBtnBank = self.m_pNodeUserInfo:getChildByName("Button_bank")
    self.m_pUserBg = self.m_pNodeUserInfo:getChildByName("Sprite_bg")
    --其他在线玩家
    self.m_pBtnOnliePlayer = self.m_pNodeButton:getChildByName("Button_chat")
    -- 上庄 下庄
    self.m_pBtnUpBanker = self.m_pNodeBanker:getChildByName("Button_upBanker")
    self.m_pBtnDownBanker = self.m_pNodeBanker:getChildByName("Button_downBanker")
    -- 下注筹码
    for i =1, Lhdz_Const.JETTON_ITEM_COUNT do
        self.m_pBtnJetton[i] = self.m_pNodeJetton:getChildByName(string.format("Button_jetton%d", i))
    end
    -- 下注区域
    for i = 1, Lhdz_Const.GAME_DOWN_COUNT do
        self.m_pBtnBetArea[i] = self.m_pNodeBetArea:getChildByName(string.format("Button_betArea_%d", i))
        --self.m_pSp
    end
    ------sprite------
    -- 倒计时
    self.m_pSpCountTime = self.m_pNodeCountTime:getChildByName("Sprite_countTime_bg")
    -- 状态
    self.m_pSpStauts = self.m_pNodeCountTime:getChildByName("Sprite_status")

    -- 历史记录
    for i=1, Lhdz_Const.MAX_HISTORY_COUNT-5 do
        self.m_pSpHistory[i] = self.m_pNodeHistory:getChildByName(string.format("Sprite_history_%d", i))
    end
    self.m_pSpHistoryNew = self.m_pNodeHistory:getChildByName("Sprite_history_new")
    self.m_pSpHistoryLight = self.m_pNodeHistory:getChildByName("Sprite_history_light")
    -- 星星
    for i=1, Lhdz_Const.GAME_DOWN_COUNT do
        self.m_pSpStar[i] = self.m_pNodeStar:getChildByName(string.format("Sprite_star%d", i))
        self.m_pSpStar[i]:setLocalZOrder(100)
        self.m_pSpWinArea[i] = self.m_pNodeBetArea:getChildByName(string.format("Sprite_winArea_%d", i))
    end
    ------label---------
    self.m_pSpUserBanker = self.m_pNodeBanker:getChildByName("Sprite_banker_bg")
    -- 庄家名称
    self.m_pLbBankerName = self.m_pSpUserBanker:getChildByName("Text_banker_name")
    -- 庄家金币
    self.m_pLbBankerGold = self.m_pSpUserBanker:getChildByName("BitmapFontLabel_banker_gold")
    -- 庄家vip
    self.m_pImgBankerVip = self.m_pSpUserBanker:getChildByName("Image_vip")
    -- 上庄条件
    self.m_pLbCondition = self.m_pNodeBanker:getChildByName("Text_condition")
    -- 上庄人数
    self.m_pLbBankerCount = self.m_pNodeBanker:getChildByName("Text_banker_count")
    -- 庄家输赢
    self.m_pLbBankerResultGold = self.m_pNodeBanker:getChildByName("BitmapFontLabel_bankResult")
    self.m_pLbBankerResultGold:setLocalZOrder(10)
    self.m_pLbBankerResultGold:setVisible(false)
    self.m_pLbBankerResultGold:setAnchorPoint(0.5, 0.5)
    -- 其他玩家数量
    self.m_pLbOnlieCount = self.m_pNodeOnliePlayer:getChildByName("BitmapFontLabel_online")
    -- 倒计时数字
    self.m_pLbCountTime = self.m_pSpCountTime:getChildByName("BitmapFontLabel_countTime")
    self.m_pLbCountTime:setPositionX(self.m_pSpCountTime:getContentSize().width/2-3)

    --上庄列表
    self.m_pBtnBankList = self.m_pNodeBanker:getChildByName("btn_bank_list")
    self.m_pBankListNode = self.m_pNodeRoot:getChildByName("banklisgNode")
    self.m_pBankListNode:setVisible(false)
    self.m_pBankListBG = self.m_pBankListNode:getChildByName("banklisgbg")
    self.m_pBankListBG:setVisible(false)
    self.m_pBankNameList = self.m_pBankListBG:getChildByName("bankname_list")
--    self.m_pBankNameList:setScrollBarEnabled(false)
    self.m_pClickMoveListBtn = self.m_pNodeRoot:getChildByName("Button_removebanklist")
    self.m_pClickMoveListBtn:setVisible(false)
    -- 玩家下注数
    for i=1, Lhdz_Const.GAME_DOWN_COUNT do
        self.m_pLbAllBetValue[i] = self.m_pNodeBetValue:getChildByName(string.format("BitmapFontLabel_allBetValue_%d", i))
        self.m_pLbMyBetValue[i] = self.m_pNodeBetValue:getChildByName(string.format("Text_myBetValue_%d", i))
    end
    -- 玩家昵称
    self.m_pLbUserName = self.m_pNodeUserInfo:getChildByName("Text_user_name")
    -- vip等级
    self.m_pImgUserVip = self.m_pNodeUserInfo:getChildByName("Image_vip")
    -- 玩家金币
    self.m_pLbUserGold = self.m_pNodeUserInfo:getChildByName("Node_score"):getChildByName("BitmapFontLabel_user_gold")
    self.m_pLbResultGold = self.m_pNodeUserInfo:getChildByName("BitmapFontLabel_userResult")

    self.m_pNo1BetPre = self.m_pNodeTable:getChildByName("Node_shengsuanzi")
    self.m_pNo1BetPreNum = self.m_pNo1BetPre:getChildByName("BitmapFontLabel_shengshuanzi")
    self.m_pNo1BetPreNum:setString("")
    self.m_pNo1BetPreBar1 = self.m_pNo1BetPre:getChildByName("Image_bar_lift")
    self.m_pNo1BetPreBar2 = self.m_pNo1BetPre:getChildByName("Image_bar_right")
    self.m_pNo1BetPreBar3 = self.m_pNo1BetPre:getChildByName("Image_bar_mid")

    self.mTextRecord = UIAdapter:CreateRecord(nil, 24)
    self.m_pNodeButton:addChild(self.mTextRecord)
    self.mTextRecord:setAnchorPoint(cc.p(0, 1))

	local buttonsize = self.m_pBtnExit:getContentSize()
	local buttonpoint = cc.p(self.m_pBtnExit:getPosition())
    self.mTextRecord:setPosition(cc.p(buttonpoint.x + buttonsize.width / 2, buttonpoint.y + buttonsize.height / 2))
    self:setRecordId(LhdzDataMgr.getInstance():getRecordId())
end

-- 初始化界面
function LhdzMainLayer:initNode()
    LOG_PRINT(" LhdzMainLayer:initNode")
    self.m_pBtnMenuPop:setVisible(false)
    self.m_pSpMenuBg:setVisible(false)
    local shap = cc.DrawNode:create()
    local pointArr = {cc.p(1080,300), cc.p(1320, 300), cc.p(1320, 700), cc.p(1080, 700)}
    shap:drawPolygon(pointArr, 4, cc.c4f(255, 255, 255, 255), 2, cc.c4f(255, 255, 255, 255))
    self.m_pClippingMenu = cc.ClippingNode:create(shap)
    self.m_pClippingMenu:addTo(self.m_pNodeMenu)
    self.m_pSpMenuBg:removeFromParent()
    self.m_pSpMenuBg:addTo(self.m_pClippingMenu)
    self.m_pClippingMenu:setPosition(cc.p(0,0))

    self.m_pLbCountTime:setVisible(false)

    AudioManager:getInstance():stopMusic()

    --print(cc.exports.gettime())
    --print("self:showBackgroundAnim()")
    self:showBackgroundAnim() --耗时
    -- 初始化按纽响应
    --print(cc.exports.gettime())
    --print("self:initBtnClickEvent()")
    self:initBtnClickEvent()
    --print(cc.exports.gettime())
    --print("self:updateButtonOfSound()")
    self:updateButtonOfSound()
    --print(cc.exports.gettime())
    --print("self:updateButtonOfMusic()")
    self:updateButtonOfMusic()
    --print(cc.exports.gettime())
    --print("self:updateOnliePlayerCount()")
    --初始玩家人数显示以服务器发送场景消息中的玩家人数为准
    self.m_pLbOnlieCount:setString(LhdzDataMgr.getInstance():getUserCountAtBegin())
    --3秒后再开始同步实际玩家人数
--    self:doSomethingLater( function()
--        self:updateOnliePlayerCount()
--    end, 3)
    
    --print(cc.exports.gettime())
    --print("self:clearNo1BetStar()")
    self:clearNo1BetStar()
    --print(cc.exports.gettime())
    --print("self:initChipEffect()")
    self:initChipEffect()
    --print(cc.exports.gettime())
    --print("self:initGetChipEffect()")
    self:initGetChipEffect() --耗时
    --print(cc.exports.gettime())
    --print("self:updateLuckerInfo()")
    self:updateLuckerInfo()
    --print(cc.exports.gettime())
    --print("self:initTopWinEffect()")
    self:initTopWinEffect()
    --print(cc.exports.gettime())
    --self:initAreaWinEffect()
    --self:addTestBtn()
    self:tryLayoutforIphoneX()

    -- local strUserName = LuaUtils.getDisplayNickName(Player:getNickName(), 13, true)
    local strName = Player:getNickName()
    local name_str = LuaUtils.getDisplayTwoString(strName,187,22)
    self.m_pLbUserName:setString(name_str)

--    local vipLevel = PlayerInfo.getInstance():getVipLevel()
--    local vipPath = string.format("hall/plist/vip/img-vip%d.png", vipLevel)
--    self.m_pImgUserVip:loadTexture(vipPath, ccui.TextureResType.plistType)

--    --房间号
--    local rooms = GameListManager.getInstance():getStructRoomByKindID(PlayerInfo.getInstance():getKindID(), PlayerInfo.getInstance():getBaseScore())
--    if #rooms > 1 then
--        local strRoomNo = string.format(LuaUtils.getLocalString("LHDZ_7"), PlayerInfo.getInstance():getCurrentRoomNo())
--        local m_pLbRoomNo = cc.Label:createWithBMFont(Lhdz_Res.FONT_ROOM, strRoomNo)
--        m_pLbRoomNo:setAnchorPoint(cc.p(0.5, 0.5))
--        m_pLbRoomNo:setPosition(cc.p(140, self.m_pBtnExit:getContentSize().height/2))
--        self.m_pBtnExit:addChild(m_pLbRoomNo)
--    end

    local scoreVal = LhdzDataMgr.getInstance():getTempSelfScore()
    print("当前玩家金币:" .. scoreVal)
--    if 0 == scoreVal and LhdzDataMgr.getInstance():getBankerId() == PlayerInfo.getInstance():getChairID() then
--        --自己是庄家，且分数为0，则可能是正好输完，或者退出重进过，则以庄家分数为准
--        scoreVal = LhdzDataMgr.getInstance():getBankerScore()
--    end

    --local strUsrGold = LuaUtils.getFormatGoldAndNumber(scoreVal)
    self.m_pLbUserGold:setString(scoreVal)

    for i = 1, Lhdz_Const.GAME_DOWN_COUNT do
        self.m_particalvec[i] = {}
        for j = 1, Lhdz_Const.FLYPARTICAL_COUNT do
            self.m_particalvec[i][j] = ParticalPool.getInstance():takePartical()
            self.m_particalvec[i][j]:setPosition(Lhdz_Const.FLYSTAR.STAR_BEGINPOS)
            self.m_pNodeStar:addChild(self.m_particalvec[i][j])
        end
    end
    self:clearFlyStatus()

    --创建系统庄家动画
--    self.m_sysBankerAni = Lhdz_Res.CreateSpineAni(Lhdz_Res.SPINE_OF_SYSBANKER.FILEPATH)
--    self.m_sysBankerAni:setAnimation(0, Lhdz_Res.SPINE_OF_SYSBANKER.ANILIST.DEFAULT.NAME, true)
--    self.m_sysBankerAni:setToSetupPose()
--    self.m_sysBankerAni:setAnchorPoint(0.5, 0.5)
--    self.m_sysBankerAni:update(0)
--    self.m_sysBankerAni:setPosition(cc.p(222, 510))
--    self.m_sysBankerAni:addTo(self.m_pNodeBanker)
--    self.m_sysBankerAni:setVisible(false)


    --创建聊天界面
--    self.m_newChatLayer = ChatLayer:create()
--    self.m_newChatLayer:initData({3})
--    self.m_newChatLayer:setBtnVisible(false,false,false,false)
--    local diffX = 145 - (1624-display.size.width)/2 
--    self.m_newChatLayer:setNodeAllOffset(-diffX,0)
--    self.m_newChatLayer:setLeftBtnOffsetX(self.m_pBtnExit:getPositionX()-self.m_newChatLayer:getLeftBtnPosX())
--    self.m_newChatLayer:setRightBtnOffsetX(self.m_pBtnMenuPush:getPositionX()-self.m_newChatLayer:getRightBtnPosX())
--    self.m_pNodeRoot:addChild(self.m_newChatLayer)

--    --银商喇叭
--    self.m_rollMsgObj = RollMsg.create()
--    self.m_rollMsgObj:addTo(self.m_pNodeRoot)
--    self.m_rollMsgObj:startRoll()
end

function LhdzMainLayer:tryLayoutforIphoneX()
    if LuaUtils.isIphoneXDesignResolution() then
        local offset = 80
        self.m_pBtnExit:setPositionX(self.m_pBtnExit:getPositionX() - offset)
        self.m_pBtnMenuPush:setPositionX(self.m_pBtnMenuPush:getPositionX() + offset)
        self.m_pBtnMenuPop:setPositionX(self.m_pBtnMenuPop:getPositionX() + offset)
        self.m_pClippingMenu:setPositionX(self.m_pClippingMenu:getPositionX() + offset - 20)
    end
end

function LhdzMainLayer:initChipMgr()
    self.m_chipMgr = ChipFlyManager.getInstance()
    local data = {}
    data.chipNode = self.m_pNodeChip
    data.chipAreaVec = {}
    data.bankerPos = cc.p(667, 460)
    data.selfPos = cc.p(52, 24)
    data.otherPos = cc.p(68, 125)
    data.tableUserPos = {}
    data.chipPosVec = {}
    data.getChipEffect = self.m_getChipEffect

    for i = 1, 3 do
        local _node = self.m_pNodeChip:getChildByName("Panel_" .. i)
        local posx, posy = _node:getPosition()
        local sz = _node:getContentSize()
        _node:removeFromParent()
        data.chipAreaVec[i] = cc.rect(posx, posy, sz.width, sz.height )
    end

    for i = 1, 6 do
        data.tableUserPos[i] = cc.p(self.m_pNodeTablePlayer[i]:getPosition())
    end

    for i = 1, 5 do
        data.chipPosVec[i] = cc.p(self.m_pBtnJetton[i]:getPosition())
        data.chipPosVec[i].y = data.chipPosVec[i].y + 14
    end

    self.m_chipMgr:setData(data)
end

-- 初始化按钮监听器
function LhdzMainLayer:initBtnClickEvent()
    self.m_pBtnExit:addTouchEventListener(handler(self, self.onBtnExitClicked))
    self.m_pBtnMenuPush:addTouchEventListener(handler(self, self.onBtnMenuPushClicked))
    self.m_pBtnMenuPop:addTouchEventListener(handler(self, self.onBtnMenuPopClicked))
    self.m_pBtnTrend:addTouchEventListener(handler(self, self.onBtnTrendClicked))
    self.m_pBtnTrend2:addTouchEventListener(handler(self, self.onBtnTrendClicked))
    self.m_pBtnMenuClose:addTouchEventListener(handler(self, self.onBtnMenuPopClicked))
    self.m_pBtnContinue:addTouchEventListener(handler(self, self.onBtnContinueClicked))
    self.m_pBtnRule:addTouchEventListener(handler(self, self.onBtnRuleClicked))
    self.m_pBtnEffect:addTouchEventListener(handler(self, self.onBtnEffectClicked))
    self.m_pBtnMusic:addTouchEventListener(handler(self, self.onBtnMusicClicked))
    self.m_pBtnStrongBox:addTouchEventListener(handler(self, self.onBtnRecordClicked)) 
    self.m_pBtnOnliePlayer:addTouchEventListener(handler(self, self.onBtnOtherPlayerClicked))
    self.m_pBtnBankList:addTouchEventListener(handler(self, self.ClickBankList))
    self.m_pClickMoveListBtn:addTouchEventListener(handler(self, self.ClickRemoveBankList))
    for i =1, Lhdz_Const.JETTON_ITEM_COUNT do
        self.m_pBtnJetton[i]:addTouchEventListener(handler(self, self.onBtnJettonNumClicked))
    end
    for i = 1, Lhdz_Const.GAME_DOWN_COUNT do
        self.m_pBtnBetArea[i]:addTouchEventListener(handler(self, self.onBtnAreaClicked))
    end
    self.m_pBtnUpBanker:addTouchEventListener(handler(self, self.onBtnUpBankerClicked))
    self.m_pBtnDownBanker:addTouchEventListener(handler(self, self.onBtnDownBankerClicked))
    for i=1, Lhdz_Const.TABEL_USER_COUNT do
        self.m_pBtnUserInfo[i]:setTag(i)
        self.m_pBtnUserInfo[i]:addTouchEventListener(handler(self, self.onBtnUserInfoClicked))
    end
    self.m_pBtnChat:addTouchEventListener(handler(self,self.onBtnChatClicked))
end

-- 初始化事件
function LhdzMainLayer:initEvent() 
       
        
        --游戏事件
        addMsgCallBack(self,Lhdz_Events.MSG_LHDZ_GAME_SCENE_INIT,handler(self,self.event_GameInit)) 
        addMsgCallBack(self,Lhdz_Events.MSG_LHDZ_GAME_STATUS,handler(self,self.event_GameStatus))--        log = "游戏状态",       debug = true, },
        addMsgCallBack(self,Lhdz_Events.MSG_LHDZ_GAME_RESULT,handler(self,self.event_GameResult))--        log = "游戏结算",       debug = true, },
        addMsgCallBack(self,Lhdz_Events.MSG_LHDZ_GAME_CHIP,handler(self,self.event_Chip))--              log = "玩家下注",       debug = true, },
        addMsgCallBack(self,Lhdz_Events.MSG_LHDZ_GAME_CANCEL,handler(self,self.event_Cancel))--            log = "取消下注",       debug = true, },
        addMsgCallBack(self,Lhdz_Events.MSG_LHDZ_GAME_CONTINUE,handler(self,self.event_Continue))--          log = "玩家续投",       debug = true, }, 
        addMsgCallBack(self,Lhdz_Events.MSG_LHDZ_GAME_COUNT_TIME,handler(self,self.event_CountTime))--         log = "倒计时更新",     debug = true, },
        addMsgCallBack(self,Lhdz_Events.MSG_LHDZ_GAME_TABLE_USER,handler(self,self.updateOnliePlayerCount))--         log = "更新上桌玩家",   debug = true, },
        addMsgCallBack(self,Lhdz_Events.MSG_LHDZ_GAME_RANK,handler(self,self.event_Rank))--              log = "更新排行榜",     debug = true, }, 
         
         
end

-- 清理事件监听
function LhdzMainLayer:cleanEvent()
    removeMsgCallBack(self,Lhdz_Events.MSG_LHDZ_GAME_SCENE_INIT) 
    removeMsgCallBack(self,Lhdz_Events.MSG_LHDZ_GAME_STATUS)--        log = "游戏状态",       debug = true, },
    removeMsgCallBack(self,Lhdz_Events.MSG_LHDZ_GAME_RESULT)--        log = "游戏结算",       debug = true, },
    removeMsgCallBack(self,Lhdz_Events.MSG_LHDZ_GAME_CHIP)--              log = "玩家下注",       debug = true, },
    removeMsgCallBack(self,Lhdz_Events.MSG_LHDZ_GAME_CANCEL)--            log = "取消下注",       debug = true, },
    removeMsgCallBack(self,Lhdz_Events.MSG_LHDZ_GAME_CONTINUE)--          log = "玩家续投",       debug = true, }, 
    removeMsgCallBack(self,Lhdz_Events.MSG_LHDZ_GAME_COUNT_TIME)--         log = "倒计时更新",     debug = true, },
    removeMsgCallBack(self,Lhdz_Events.MSG_LHDZ_GAME_TABLE_USER)--         log = "更新上桌玩家",   debug = true, },
    removeMsgCallBack(self,Lhdz_Events.MSG_LHDZ_GAME_RANK)--              log = "更新排行榜",     debug = true, }, 
end

-- 清理游戏
function LhdzMainLayer:cleanGame()
    AudioManager:getInstance():stopAllSounds()
    AudioManager:getInstance():stopMusic()
    self.m_rootUI:stopAllActions()
    if self.m_bEnterBackground == false then
        --清理游戏数据
        
        LhdzDataMgr.getInstance():clear()
        LhdzDataMgr.releaseInstance()
        CBetManager.getInstance():clearUserChip()
        ParticalPool.getInstance():clearPool()
        ParticalPool.getInstance():releaseInstance()

        -- 释放动画
        for _, strPathName in pairs(Lhdz_Res.vecAnim) do
            local strJsonName = string.format("%s%s/%s.ExportJson", Lhdz_Res.strAnimPath, strPathName, strPathName)
            ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo(strJsonName)
        end
        -- 释放整图
        for _, strPathName in pairs(Lhdz_Res.vecPlist) do
            display.removeSpriteFrames(strPathName[1], strPathName[2])
        end
        -- 释放背景图
        for _, strFileName in pairs(Lhdz_Res.vecReleaseImg) do
            display.removeImage(strFileName)
        end
        -- 释放音频
        for _, strFileName in pairs(Lhdz_Res.vecSound) do
            AudioManager.getInstance():unloadEffect(strFileName)
        end
        -- 释放背景音乐
--        for _, strFileName in pairs(Lhdz_Res.vecLoadingMusic) do
--            AudioManager.getInstance():unloadEffect(strFileName)
--        end
    end

    self.m_chipMgr:clearAllChip()
    self.m_chipMgr:releaseInstance()
    self.m_chipMgr = nil
   -- PlayerInfo.getInstance():setSitSuc(false)

    --LhdzDataMgr.getInstance():clearAllUserChip()

    self:removeAllChildren()
end

-- 进入游戏初始化
function LhdzMainLayer:initGame()
    --重置下注区域
    self.m_chipMgr:resetChipPosArea()
    -- 更新历史记录
    self:updateGameRecord()
    -- 更新庄家信息
  --  self:updatebankerInfo()
    -- 更新筹码面值
    self:updateJettonValues()
    -- 更新游戏状态
    self:updateGameStatus()
    -- 更新上桌玩家信息
    self:updateTableUserInfo()
    -- 更新筹码
    self:onBtnJettonNumClicked(self.m_pBtnJetton[1],ccui.TouchEventType.ended,false)
    self:updateJettonStatus()
    self:showWaitNextAnim()
    self:showSendCardAnim(false)
    self:updateContinueStatus(false) 
    if LhdzDataMgr.getInstance():getGameStatus() ~= Lhdz_Const.STATUS.GAME_SCENE_FREE then
        -- 初始化刚进游戏已经下注的筹码
        self:initAlreadyJettion()
        -- 是否是神算子下注区域
        self:playFlyStarAnim(true)
     
    end

--    if LhdzDataMgr.getInstance():getGameStatus() == Lhdz_Const.STATUS.GAME_SCENE_RESULT then
--        self:updateBankStatus(false)
--    end
    -- 更新下注区域的数值
    self:updateBetAreaValue()

    --初始化庄家列表
  --  self:initBankListTable()

    --请求rank数据 更新神算子胜率
--    CMsgLhdz.getInstance():sendMsgRank(1)
end

-- 初始筹码
function LhdzMainLayer:initAlreadyJettion()
    local chipCount = LhdzDataMgr.getInstance():getAllUserChipCount()
    for i=1, chipCount do
        local chip = LhdzDataMgr.getInstance():getAllUserChipByIndex(i)
        if chip then
            self.m_chipMgr:createStaticChip(chip.wJettonIndex, chip.wChipIndex, chip.dwUserID)
        end
    end
end

---------------------------------- init view end ----------------------------------

---------------------------------- btn click event start ----------------------------------
function LhdzMainLayer:setRecordId(id)
    if id and id ~= "" then
        self.mTextRecord:setString("牌局ID:"..id)
    end
end
-- 退出游戏
function LhdzMainLayer:onBtnExitClicked(sender,eventType)
 if eventType ~=ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_BUTTON)

--    if(LhdzDataMgr.getInstance():isBanker())then
--        if(LhdzDataMgr.getInstance():getAllOtherBetValue() > 0)then
--            --我是庄家，且有人下注
--            FloatMessage.getInstance():pushMessage("LHDZ_1");
--            return
--        end
--    end 
    if g_GameController then

        g_GameController:rewDragonVsTigerForceExit()
        g_GameController:destroyCountDownShedule()
		g_GameController:releaseInstance()
    end
end

function LhdzMainLayer:showMessageBox(msg) --打开确认框
    SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_MESSAGEBOX, msg)
end

-- 菜单下拉
function LhdzMainLayer:onBtnMenuPushClicked(sender,eventType)
    if eventType ~=ccui.TouchEventType.ended then
        return
    end
    if self.m_bIsMenuMove then return end
    self.m_bIsMenuMove = true

    self.m_pBtnMenuClose:setVisible(true)
    self.m_pSpMenuBg:setPositionY(955)
    local callBack = cc.CallFunc:create(function()
        self.m_pBtnMenuPop:setVisible(true)
        self.m_pBtnMenuPush:setVisible(false)
    end)
    local callBack2 = cc.CallFunc:create(function()
        self.m_bIsMenuMove = false
    end)
    self:showMenuPop(self.m_pSpMenuBg, callBack, callBack2, self.m_pSpMenuBg:getPositionX(), 500)
end
function LhdzMainLayer:showMenuPop(node, callback, callback2, x, y)
    
    if node==nil or callback==nil or callback2==nil then 
        return
    end
    AudioManager.getInstance():playSound("res/public/sound/sound-button.mp3")
    node:stopAllActions()
    local aTime = 0.3
    local desX = x or 0
    local desY = y or 0
    node:setOpacity(0)
    local moveTo = cc.MoveTo:create(aTime, cc.p(desX, desY))
    local show = cc.Show:create()
    local sp = cc.Spawn:create(cc.EaseBackOut:create(moveTo), cc.FadeIn:create(aTime))
    local seq = cc.Sequence:create(show, sp, callback, cc.DelayTime:create(0.1), callback2)
    node:runAction(seq)
end

function LhdzMainLayer:showMenuPush(node, callback, callback2, x, y)

    if node==nil or callback==nil or callback2==nil then 
        return
    end
    AudioManager.getInstance():playSound("res/public/sound/sound-close.mp3")
    node:stopAllActions()
    local aTime = 0.25
    local desX = x or 0
    local desY = y or 0
    local moveTo = cc.MoveTo:create(aTime, cc.p(desX, desY))
    local sp = cc.Spawn:create(cc.EaseBackIn:create(moveTo), cc.FadeOut:create(aTime))
    local hide = cc.Hide:create()
    local seq = cc.Sequence:create(sp, callback, hide, cc.DelayTime:create(0.1), callback2)
    node:runAction(seq)
end
-- 菜单上移
function LhdzMainLayer:onBtnMenuPopClicked(sender,eventType)
   if eventType ~=ccui.TouchEventType.ended then
        return
    end
    if self.m_bIsMenuMove then return end
    self.m_bIsMenuMove = true

    self.m_pBtnMenuClose:setVisible(false)
    local callBack = cc.CallFunc:create(function()
        self.m_pBtnMenuPop:setVisible(false)
        self.m_pBtnMenuPush:setVisible(true)
    end)
    local callBack2 = cc.CallFunc:create(function()
        self.m_bIsMenuMove = false
    end)
    self:showMenuPush(self.m_pSpMenuBg, callBack, callBack2, self.m_pSpMenuBg:getPositionX(), 955)
end

-- 续投
function LhdzMainLayer:onBtnContinueClicked(sender,eventType)
 if eventType ~=ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_BUTTON)
--    if self:isDelayHandleEvent() then return end
--    if (LhdzDataMgr.getInstance():getGameStatus() ~= Lhdz_Const.STATUS.GAME_SCENE_BET)then
--        FloatMessage.getInstance():pushMessage("STRING_026");
--        return;
--    end

    -- 庄家不能续投
--    if (LhdzDataMgr.getInstance():isBanker())then
--        FloatMessage.getInstance():pushMessage("LHDZ_4");
--        return;
--    end
    -- 上局没有投注
--    local count = LhdzDataMgr.getInstance():getContinueChipSize();
--    if (count <= 0)then
--        FloatMessage.getInstance():pushMessage("STRING_027");
--        return;
--    end
--    if (LhdzDataMgr.getInstance():getIsContinued())then
--        FloatMessage.getInstance():pushMessage("STRING_004");
--        return;
--    end
--    local continueChip = {};
--    for i=1,tonumber(Lhdz_Const.GAME_DOWN_COUNT) do
--        continueChip[i] = 0;
--    end
--    local llSumScore = 0;
--    for i=1,tonumber(count),1 do
--        local chip = LhdzDataMgr.getInstance():getContinueChipByIndex(i);
--        local baseVale = CBetManager.getInstance():getLotteryBaseScoreByIndex(chip.wJettonIndex);
--        continueChip[chip.wChipIndex] = continueChip[chip.wChipIndex] + baseVale;
--        llSumScore = llSumScore + baseVale;
--    end

--    if (PlayerInfo.getInstance():getUserScore() < llSumScore) then
--        --金钱不够
--        self:showMessageBox("go-recharge")
--        return;
--    end
   -- sender:setEnabled(false)
    self:updateContinueStatus(false)
    g_GameController:reqDragonVsTigerContinueBet() 
   -- CMsgLhdz.getInstance():sendMsgContinueChip(continueChip);
end

-- 音效
function LhdzMainLayer:onBtnEffectClicked(sender,eventType)
 if eventType ~=ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_BUTTON)
    local bEnable = false
    if (AudioManager.getInstance():getSoundOn())then -- 音效开着
        AudioManager.getInstance():setSoundOn(false)
    else
        bEnable = true
        AudioManager.getInstance():setSoundOn(true)
    end
    self:updateButtonOfSound(bEnable)
end

-- 音乐
function LhdzMainLayer:onBtnMusicClicked(sender,eventType)
   if eventType ~=ccui.TouchEventType.ended then
        return
    end
    local layer = GameSetLayer.new(151);
	self:addChild(layer); 
    layer:setLocalZOrder(100)
end

-- 银行
function LhdzMainLayer:onBtnRecordClicked(sender,eventType)
    if eventType ~=ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_BUTTON)
    
   local GameRecordLayer = GameRecordLayer.new(2,151)
    self:addChild(GameRecordLayer,100)    
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", {151})

end
 
-- 选取筹码
function LhdzMainLayer:onBtnJettonNumClicked(sender,eventType, bool)
    if eventType ~=ccui.TouchEventType.ended then
        return
    end
    local nIndex = sender:getTag()
    local score = CBetManager.getInstance():getJettonScore(nIndex)
    local UserScore  = LhdzDataMgr.getInstance():getTempSelfScore()
    if LhdzDataMgr.getInstance():getGameStatus() ~= Lhdz_Const.STATUS.GAME_SCENE_BET or UserScore < score  then
--        self.m_pBtnJetton[nIndex]:setColor(cc.c3b(160,160,160))
    --    self.m_pBtnJetton[nIndex]:setOpacity(Chip_DisableOpacity)
    --    self.m_chipEffect:setVisible(false)
        return
    else
        self.m_pBtnJetton[nIndex]:setColor(cc.c3b(255,255,255))
        self.m_pBtnJetton[nIndex]:setOpacity(Chip_EnableOpacity)
    end

    self.m_chipEffect:setColor(Lhdz_Const.BTNEFFCRT_COLOR[nIndex])
    self.m_chipEffect:setPosition(cc.p(self.m_pBtnJetton[nIndex]:getPositionX(), self.m_pBtnJetton[nIndex]:getPositionY() + 3))
    self.m_chipEffect:setVisible(Chip_EnableOpacity == self.m_pBtnJetton[nIndex]:getOpacity())
    
    CBetManager.getInstance():setLotteryBaseScore(score); 
    if (self.m_nIndexChoose ~= nIndex)then
        AudioManager.getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_JETTON)
    end
    
    -- 让先前选中的变成原始位置
    local fNormalY = 53
    local fSelectY = 67
    if (self.m_nIndexChoose > 0)then
        local posNowX = self.m_pBtnJetton[self.m_nIndexChoose]:getPositionX();
        self.m_pBtnJetton[self.m_nIndexChoose]:setPosition(cc.p(posNowX, fNormalY));
    end

    local posNewX = self.m_pBtnJetton[nIndex]:getPositionX();
    self.m_pBtnJetton[nIndex]:setPosition(cc.p(posNewX, fSelectY));
    self.m_chipEffect:setPosition(cc.p(self.m_pBtnJetton[nIndex]:getPositionX(), self.m_pBtnJetton[nIndex]:getPositionY() + 5))
    self.m_nIndexChoose = nIndex;
end

-- 下注
function LhdzMainLayer:onBtnAreaClicked(sender,eventType)
     if eventType ~=ccui.TouchEventType.ended then
        return
    end
    local index = sender:getTag();
    -- 未到下注时间不能下注
    if LhdzDataMgr.getInstance():getGameStatus() ~= Lhdz_Const.STATUS.GAME_SCENE_BET then
       TOAST("未到下注时间不能下注")
        return
    end

    -- 庄家不能下注
--    if (LhdzDataMgr.getInstance():isBanker())then
--        FloatMessage.getInstance():pushMessage("LHDZ_4")
--        return;
--    end

    -- 金币不够不能下注
--    if PlayerInfo.getInstance():getUserScore() < CBetManager.getInstance():getLotteryBaseScore() then
--        self:showMessageBox("go-recharge")
--        --FloatMessage.getInstance():pushMessage("LHDZ_3")
--        return
--    end
    local coin = CBetManager.getInstance():getLotteryBaseScore()
    g_GameController:reqDragonVsTigerBet({index+1, coin*100})
  --  CMsgLhdz.getInstance():sendMsgUserChip(index, CBetManager.getInstance():getLotteryBaseScoreIndex());
end

-- 其他玩家排行
function LhdzMainLayer:onBtnOtherPlayerClicked(sender,eventType)
     if eventType ~=ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_BUTTON)
--    if self:isDelayHandleEvent() then return end
--    if not self.m_bIsRequestRank then
--        self.m_bIsRequestRank = true
--        g_GameController:reqDragonVsTigerPlayerOnlineList()
--    end
--    local pLhdzRankLayer = self.m_rootUI:getChildByName("LhdzRankLayer")
--    if not pLhdzRankLayer then
--        pLhdzRankLayer = LhdzRankLayer.new()
--        pLhdzRankLayer:setName("LhdzRankLayer")
--        self.m_rootUI:addChild(pLhdzRankLayer)
--        pLhdzRankLayer:updateTableView()
--    end
    local ChatLayer = ChatLayer.new()
    self:addChild(ChatLayer);
end

function LhdzMainLayer:ClickRemoveBankList()
    AudioManager.getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_BUTTON)
    self.m_pClickMoveListBtn:setVisible(false)
    if self.m_bIsBankListMove then return end
    self.m_bIsBankListMove = true
    self.m_pSpMenuBg:stopAllActions()
    local moveTo = cc.MoveTo:create(0.2, cc.p(0, 200))
    local callBack = cc.CallFunc:create(function()
            self.m_bIsBankListMove = false
            self.m_pBankListBG:setVisible(false)
            self.m_pBankListNode:setVisible(false)
        end)
    local seq = cc.Sequence:create(moveTo, callBack)
    self.m_pBankListBG:runAction(seq)
end

function LhdzMainLayer:onBtnChatClicked()
    AudioManager.getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_BUTTON)
    self.m_newChatLayer:onOutRightMsgClicked()
     if self.m_pMsgArmature then
        self.m_pMsgArmature:hide()
    end
    self.m_pSpChat:setOpacity(255*0.3)
end

function LhdzMainLayer:ClickBankList()
    self.m_pBankListNode:setVisible(true)
    if self.m_pBankListBG:isVisible() then
        AudioManager.getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_BUTTON)
        if self.m_bIsBankListMove then return end
        self.m_pClickMoveListBtn:setVisible(false)
        self.m_bIsBankListMove = true
        self.m_pSpMenuBg:stopAllActions()
        local moveTo = cc.MoveTo:create(0.2, cc.p(0, 200))
        local callBack = cc.CallFunc:create(function()
                self.m_bIsBankListMove = false
                self.m_pBankListBG:setVisible(false)
            end)
        local seq = cc.Sequence:create(moveTo, callBack)
        self.m_pBankListBG:runAction(seq)
    else
        AudioManager.getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_BUTTON)
        if self.m_bIsBankListMove then return end
        self.m_bIsBankListMove = true

        self.m_pBankListBG:setVisible(true)
        self.m_pBankListBG:stopAllActions()
        self.m_pBankListBG:setPositionY(200)
        local moveTo = cc.MoveTo:create(0.2, cc.p(0, 0))
        local callBack = cc.CallFunc:create(function()
                self.m_bIsBankListMove = false
                self.m_pClickMoveListBtn:setVisible(true)
            end)
        local seq = cc.Sequence:create(moveTo, callBack)
        self.m_pBankListBG:runAction(seq)

        if self.m_pTableView then 
            self.m_pTableView:reloadData()
        end
    end
end 
function LhdzMainLayer:initBankListTable() --庄家列表

    local size = cc.size(230,146)
    local function initTableViewCell(cell, idx)
        local list = LhdzDataMgr.getInstance():getBankerList()
        local tableID = PlayerInfo:getInstance():getTableID()
        local userInfo = CUserManager:getInstance():getUserInfoByChairID(tableID, list[idx+1])

        local pLbNick = nil
        if PlayerInfo.getInstance():getChairID() == list[idx+1] then
            pLbNick = cc.Label:createWithSystemFont(userInfo.szNickName, "Helvetica", 20)
        else
            pLbNick = cc.Label:createWithSystemFont(LuaUtils.replaceWXNickName(userInfo.szNickName), "Helvetica", 20)
        end


        pLbNick:setAnchorPoint(cc.p(0.5,0.5))
        pLbNick:setPosition(cc.p(size.width/2,20))
        pLbNick:setColor(cc.c3b(191,178,248))
        pLbNick:addTo(cell)

        local spLine = cc.Sprite:createWithSpriteFrameName(Lhdz_Res.PNG_OF_BANKER_SPLINE)
        spLine:setPosition(cc.p(size.width/2,5))
        spLine:addTo(cell)
    end
    local function tableCellTouched(table, cell)
        --printf("%d \n", cell:getIdx())
    end
    local function tableCellAtIndex(table, idx)
        local cell = table:cellAtIndex(idx)
        if not cell then
            cell = cc.TableViewCell:new()
        else
            cell:removeAllChildren()
        end
        initTableViewCell(cell, idx)
        return cell
    end
    local function cellSizeForTable(table, idx)
        return size.width, 32
    end
    local function numberOfCellsInTableView(table)
         return  LhdzDataMgr.getInstance():getBankerListSize()
    end
    
    if not self.m_pTableView then
        self.m_pTableView = cc.TableView:create(size)
        self.m_pTableView:setIgnoreAnchorPointForPosition(false)
        self.m_pTableView:setAnchorPoint(cc.p(0,0))
        self.m_pTableView:setPosition(cc.p(12, 15))
        self.m_pTableView:setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL)
        self.m_pTableView:setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN)
        self.m_pTableView:setTouchEnabled(true)
        self.m_pTableView:setDelegate()
        self.m_pTableView:registerScriptHandler(tableCellTouched, cc.TABLECELL_TOUCHED)
        self.m_pTableView:registerScriptHandler(tableCellAtIndex, cc.TABLECELL_SIZE_AT_INDEX)
        self.m_pTableView:registerScriptHandler(cellSizeForTable, cc.TABLECELL_SIZE_FOR_INDEX)
        self.m_pTableView:registerScriptHandler(numberOfCellsInTableView, cc.NUMBER_OF_CELLS_IN_TABLEVIEW)
        self.m_pTableView:addTo(self.m_pBankListBG)
    end
end

-- 上桌玩家信息
function LhdzMainLayer:onBtnUserInfoClicked(sender,eventType)
    if eventType ~=ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_BUTTON)
    local index = sender:getTag()
    if self.m_pNodeUserTips:getChildByTag(100+index) then
        return
    end
    local userInfo = LhdzDataMgr:getInstance():getTableUserInfo(index)
    local dialog = LhdzUserInfoLayer.create()
    local posX, posY = self.m_pNodeTablePlayer[index]:getPosition()
    if index == 2 or index == 3 or index == 4 then
        posX = posX + 150
    else
        posX = posX - 150
    end
    dialog:setPosition(cc.p(posX, posY - 20))
    dialog:setTag(100+index)
    dialog:updateUserInfoByInfo(userInfo, index)
    self.m_pNodeUserTips:addChild(dialog)
end

-- 走势界面
function LhdzMainLayer:onBtnTrendClicked(sender,eventType)
     if eventType ~=ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_BUTTON)
    self:updateTrendLayer(true)
end

-- 规则界面
function LhdzMainLayer:onBtnRuleClicked(sender,eventType)
       if eventType ~=ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_BUTTON)
    local pLhdzRuleLayer = LhdzRuleLayer.new()
    pLhdzRuleLayer:setName("LhdzRulelayer")
    self.m_rootUI:addChild(pLhdzRuleLayer)
end
---------------------------------- btn click event end ----------------------------------

---------------------------------- on msg event start ---------------------------------- 
 

 

-- 游戏初始化
function LhdzMainLayer:event_GameInit(msg)
    self:initGame()
end
function LhdzMainLayer:event_UpdateScore(msg)
    if self.m_pLbUserGold and self.m_pLbUserGold:getTag() ~= -1 then
        --缓存新的金币金额
        local newscore = LhdzDataMgr.getInstance():getTempSelfScore() -- delete_score

      --  LhdzDataMgr.getInstance():setTempSelfScore(newscore)
        newscore = newscore > 0 and newscore or 0
        local scoreStr = self:dealPlayerCoin(newscore)
        local idx = LhdzDataMgr.getInstance():checkUserIsOnTable()
        if idx > 0 then
            LhdzDataMgr.getInstance():getTableUserInfo(idx).llUserScore = newscore --+delete_score
            self.m_pLbShowPlayerGold[idx]:setString(scoreStr)
        end
        self.m_pLbUserGold:setString(self:dealPlayerCoin((newscore)))
    end
    self:updateJettonStatus()
end
-- 游戏状态
function LhdzMainLayer:event_GameStatus(msg)
    local status = LhdzDataMgr.getInstance():getGameStatus()
    self:setRecordId(LhdzDataMgr.getInstance():getRecordId())
    --下注状态和空闲状态时，确保重置了翻牌扑克动画的状态，避免显示为正面牌型
    if status == Lhdz_Const.STATUS.GAME_SCENE_BET then 
        LhdzDataMgr.getInstance():clearMyBetValue()
        self:updateJettonStatus()
        self:clearFlyStatus()
        self:resetCardAnim()
        self:updateLuckerInfo()
        self:event_UpdateScore()
        self.m_pLbUserGold:setTag(1)
        self:clearWaitNextPlay()
        self.m_pNodeCountTime:setVisible(true)
        --self:updateContinueStatus(true)
        self:goldContinueStatus()
       -- self:showGameStatusAnim()
--        self:updatebankerInfo()
    --    self:updateBankStatus(true)
        self:updateTempGameRecord()
    elseif status == Lhdz_Const.STATUS.GAME_SCENE_RESULT then
        --开奖阶段停止星星倒计时动画特效
--        for i = 1, Lhdz_Const.GAME_DOWN_COUNT do
--            self.m_pSpStar[i]:stopAllActions()
--            self.m_pSpStar[i]:setScale(1)
--        end
        LhdzDataMgr.getInstance():setMyTotalBet()
        self:updateJettonStatus()
        self.m_bIsPlayVSAnim = false
        self.m_pNodeCountTime:setVisible(true)
      --  self:updateBankStatus(false)
        self:clearWaitNextPlay()
     --   self:showGameStatusAnim()
        --LhdzDataMgr.getInstance():clearMyBetValue()
    elseif status == Lhdz_Const.STATUS.GAME_SCENE_FREE then
        self:clearFlyStatus()
        self.m_bIsRequestRank = false
        self.m_pNodeCountTime:setVisible(false)
        --self:showSendCardAnim(false)
        self:resetCardAnim()
        self:updateBetAreaValue()
        self:clearWaitNextPlay()
        self.m_chipMgr:clearAllChip()
        self:clearNo1BetStar()
        self:hideTableUserWinAnim()
        self:hideWinAreaAnim()
        self:updateBetAreaValue()
   --     self:showGameVSAnim()
        self.m_chipMgr:resetChipPosArea()
        self:updateTempGameRecord()
        self.m_pLbUserGold:setString(LhdzDataMgr.getInstance():getTempSelfScore())
--        self:doSomethingLater(function()
--            self:showBankerTips(false)
--        end, 1.5)
    end
    self:updateGameStatus()
    --self:updateJettonStatus()
end

function LhdzMainLayer:goldContinueStatus()
    if LhdzDataMgr.getInstance():getContinueBet() ==0 then 
        self:updateContinueStatus(false)
    else
        self:updateContinueStatus(true)
    end
end
-- 游戏结算
function LhdzMainLayer:event_GameResult(msg)
    self.m_pLbUserGold:setTag(-1)
    self.m_iLastPlayerGold =LhdzDataMgr.getInstance():getTempSelfScore()
    self:updateContinueStatus(false)
    self:doSomethingLater(function( ... )
        self:showSendCardAnim(true)
    end, 1)
--    CMsgLhdz.getInstance():sendMsgRank(1)
end

-- 玩家下注
function LhdzMainLayer:event_Chip(event,msg)
    self:handleUserChip(msg)
end

--处理下注信息
function LhdzMainLayer:handleUserChip(msg)
    local delayTs = 0--msg.wChairID == PlayerInfo.getInstance():getChairID() and 0 or math.random(0, 8)/10

    -- 更新下注筹码数值
    self:updateBetAreaValue()
--    -- 更新筹码状态
--    self:updateJettonStatus()
    -- 神算子下注
    self:playFlyStarAnim()
    -- 筹码飞向下注区
    local nSize = LhdzDataMgr.getInstance():getAllUserChipCount();
    local lastChip = nil
    if nSize > 0 then
        lastChip = LhdzDataMgr.getInstance():getAllUserChipByIndex(nSize)
        local userIndex = LhdzDataMgr.getInstance():getUserByChirId(lastChip.wChairID)
        userIndex = (0 == userIndex) and LhdzDataMgr.getInstance():checkUserIsOnTable() or userIndex
        self:doSomethingLater(function()
            self.m_chipMgr:playFlyJettonToBetArea(lastChip.wJettonIndex, lastChip.wChipIndex, lastChip.wChairID)
            --如果是界面显示玩家投注，且不是自己投注，则进行头像抖动动画
            if userIndex > 0 and LhdzDataMgr.getInstance():getTableUserInfo(userIndex).dwUserID ~= Player:getAccountID() then
                self:playTableUserChipAni(userIndex)
            end

            if LhdzDataMgr.getInstance():getIsNo1BetByChairID(lastChip.wChairID) then
                self:SetNo1BetPercentage(lastChip)
            end
        end, delayTs)
    else
        return
    end
    --self:doSomethingLater(function()
        if msg == "myBetSuccess" then
            
            local score = CBetManager.getInstance():getJettonScore(lastChip.wJettonIndex)
            self.m_pLbUserGold:setString(LhdzDataMgr.getInstance():getTempSelfScore())
            local _curtGold = tonumber(LhdzDataMgr.getInstance():getTempSelfScore())
            self:updateCurtJettonStatus(_curtGold)
            if score >= 10000 then -- 分投注级别播放音效 金额大于100块的播放高价值音效
                AudioManager:getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_JETTON_HIGH)
            else
                AudioManager:getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_JETTON_LOW)
            end
--            self:updateContinueStatus(false)
        else
            if not self.m_isPlayBetSound then
                local score = CBetManager.getInstance():getJettonScore(lastChip.wJettonIndex)
                if score >= 10000 then -- 分投注级别播放音效 金额大于100块的播放高价值音效
                    AudioManager:getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_JETTON_HIGH)
                else
                    AudioManager:getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_JETTON_LOW)
                end
                -- 尽量避免重复播放
                self:doSomethingLater(function() self.m_isPlayBetSound  = false end, math.random(2,4)/10)
                self.m_isPlayBetSound = true
            end
        end
    --end, delayTs)

    --更新上桌玩家分数
    self:updateTableUserScore()
end

-- 实时更新筹码状态(每次自己下注成功调用)
function LhdzMainLayer:updateCurtJettonStatus(gold)
    if gold > CBetManager.getInstance():getJettonScore(Lhdz_Const.JETTON_ITEM_COUNT) then return end
    for i = 1, Lhdz_Const.JETTON_ITEM_COUNT do
        local bStatus = LhdzDataMgr.getInstance():getCurtJettonEnableStatus(i,gold)
        local color = bStatus and cc.c3b(255,255,255) or cc.c3b(160,160,160)
        local opacity = bStatus and Chip_EnableOpacity or Chip_DisableOpacity
        self.m_pBtnJetton[i]:setEnabled(bStatus)
        self.m_pBtnJetton[i]:setColor(color)
        self.m_pBtnJetton[i]:setOpacity(opacity)
    end
--    local newSelIndex = LhdzDataMgr.getInstance():getCurtJettonSelAdjust(gold);
--    if (newSelIndex > 0) then
--        print("***************** newSelIndex *************",newSelIndex)
--        self:onBtnJettonNumClicked(self.m_pBtnJetton[newSelIndex],ccui.TouchEventType.ended,true);
--    else
--        self.m_nIndexChoose = self.m_nIndexChoose<1 and 1 or self.m_nIndexChoose
--        self:onBtnJettonNumClicked(self.m_pBtnJetton[self.m_nIndexChoose],ccui.TouchEventType.ended,true);
--    end
end

function LhdzMainLayer:SetNo1BetPercentage(msg)
    self.m_pNo1BetPreBar3:setPositionX(661)
    local betToNum = {1,10,50,100,500}
    self.m_dNo1AllBet[msg.wChipIndex] = self.m_dNo1AllBet[msg.wChipIndex] + betToNum[msg.wJettonIndex]

    local preNum1 =  self.m_pNo1BetPreBar1:getChildByName("Text_prenum")
    local preNum2 =  self.m_pNo1BetPreBar2:getChildByName("Text_prenum")
    local preNum3 =  self.m_pNo1BetPreBar3:getChildByName("Text_prenum")
    local all = self.m_dNo1AllBet[1] + self.m_dNo1AllBet[2] + self.m_dNo1AllBet[3]
    if all ~= 0 then
        local offsetX = 551
        local pre1 = self.m_dNo1AllBet[1]/all
        local pre2 = self.m_dNo1AllBet[2]/all
        local pre3 = self.m_dNo1AllBet[3]/all
        local lostPre = 0
        self.m_pNo1BetPreBar1:setVisible(pre1>0)
        if pre1>0 then
            local w1 = math.floor(220*pre1)
            if w1 < 40 then
                w1 = 40
            end
            w1 = (w1>180 and pre1~=1) and 180 or w1
            offsetX = offsetX + w1
            self.m_pNo1BetPreBar1:setContentSize(cc.size(w1, self.m_pNo1BetPreBar1:getContentSize().height))
            lostPre = math.ceil(pre1*100)>1 and math.floor(pre1*100) or 1
            preNum1:setString(lostPre.."%")
        end
        self.m_pNo1BetPreBar2:setVisible(pre2>0)
        if pre2>0 then
            local pre = math.ceil(pre2*100)>1 and math.floor(pre2*100) or 1
            if pre3 > 0 then
                preNum2:setString(pre.."%")
            else
                preNum2:setString((100-lostPre).."%")
            end
            lostPre = lostPre + pre
            local w2 = math.floor(220*pre2)
            if w2<40 then
                w2 = 40
            end
            w2 = (w2>180 and pre2~=1) and 180 or w2
            offsetX = offsetX + w2
            self.m_pNo1BetPreBar2:setContentSize(cc.size(w2, self.m_pNo1BetPreBar2:getContentSize().height))
            preNum2:setPositionX(w2-15)
        end
        self.m_pNo1BetPreBar3:setVisible(pre3>0)
        if pre3>0 then
            preNum3:setString((100-lostPre).."%")
            local w3 = 220+551-offsetX
            if w3<40 then
                w3 = 40
            end
            self.m_pNo1BetPreBar3:setContentSize(cc.size(w3, self.m_pNo1BetPreBar3:getContentSize().height))
            self.m_pNo1BetPreBar3:setPositionX(self.m_pNo1BetPreBar1:getContentSize().width+551)
            preNum3:setPositionX(w3/2)
        end
        --print (" ------------------------------------------------------00000---------- ",lostPre,100-lostPre)
    end
end

-- 取消下注
function LhdzMainLayer:event_Cancel(msg)
    -- 更新下注筹码数值
    self:updateBetAreaValue()
    -- 更新筹码状态
    self:updateJettonStatus()
    if not msg then
        self:updateContinueStatus(true)
    end
end

-- 玩家续投
function LhdzMainLayer:event_Continue(msg)
    self:updateBetAreaValue()
    --self:updateJettonStatus()
    -- 神算子下注
    self:playFlyStarAnim()
    local betHigh = false
    if msg and msg == "MyContinueSuc" then
        self.m_pLbUserGold:setString(LhdzDataMgr.getInstance():getTempSelfScore())
        for i=1,Lhdz_Const.GAME_DOWN_COUNT do
            local downTotal = LhdzDataMgr.getInstance():getMyBetValue(i)
            while downTotal > 0 do
                local userChip = {}
                userChip.wChairID = PlayerInfo.getInstance():getChairID()
                userChip.wChipIndex = i
                local index = LhdzDataMgr.getInstance():GetJettonMaxIndexByValue(downTotal)
                if index > 0 then
                    userChip.wJettonIndex = index
                    downTotal = downTotal - CBetManager.getInstance():getJettonScore(index)
                end
                local beginPos = self:getUserPosition(userChip.wChairID, true, userChip.wJettonIndex)
                if LhdzDataMgr.getInstance():getIsNo1BetByChairID(userChip.wChairID) then
                    self:SetNo1BetPercentage(userChip)
                end
                self.m_chipMgr:playFlyJettonToBetArea(userChip.wJettonIndex, userChip.wChipIndex, userChip.wChairID)
                if downTotal>1000 then
                    betHigh = true
                end
            end
        end

        self:updateContinueStatus(false)
        local _curtGold = tonumber(LhdzDataMgr.getInstance():getTempSelfScore())
        self:updateCurtJettonStatus(_curtGold)
    else
        local lOtherContinueCount = LhdzDataMgr.getInstance():getOtherContinueChipSize()
        for i = 1, lOtherContinueCount do
            local chip = LhdzDataMgr.getInstance():getOtherContinueChipByIndex(i)
            local beginPos = self:getUserPosition(chip.wChairID, true, chip.wJettonIndex)
            if LhdzDataMgr.getInstance():getIsNo1BetByChairID(chip.wChairID) then
                self:SetNo1BetPercentage(chip)
            end
            self.m_chipMgr:playFlyJettonToBetArea(chip.wJettonIndex, chip.wChipIndex, chip.wChairID)
            local score = CBetManager.getInstance():getJettonScore(chip.wJettonIndex)
            if score > 1000 then
                betHigh = true
            end
        end
        LhdzDataMgr.getInstance():clearOtherContinueChip()
    end
    local soundPath = betHigh and Lhdz_Res.vecSound.SOUND_OF_JETTON_HIGH or Lhdz_Res.vecSound.SOUND_OF_JETTON_LOW
    AudioManager:getInstance():playSound(soundPath)

    --更新上桌玩家分数
    self:updateTableUserScore()
end

-- 更新庄家列表
function LhdzMainLayer:event_BankerList(msg)
    -- 自己是否上庄
    local bIsBanker =  LhdzDataMgr.getInstance():isBanker() 
    local bInList = LhdzDataMgr.getInstance():isInBankerList()
    self.m_pBtnUpBanker:setVisible(not(bIsBanker or bInList ~= -1))
    self.m_pBtnDownBanker:setVisible(bIsBanker or bInList ~= -1)
    -- 限制条件
    local strBankerMinGold = LhdzDataMgr.getInstance():getMinBankerScore()
    self.m_pLbCondition:setString(LuaUtils.getGoldNumberNounZH(strBankerMinGold))
    -- 上庄人数
    local nBankerCount = LhdzDataMgr.getInstance():getBankerListSize()
    local strNum = string.format("%d", nBankerCount)
    self.m_pLbBankerCount:setString(strNum)
end

-- 更新庄家
function LhdzMainLayer:event_BankerInfo(msg)
    self:updatebankerInfo()
     if(msg)then
        --//change banker
        local str = msg.attachment
        if(str == "CHANGEBANKER")then
            self:doSomethingLater(function()
                AudioManager.getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_BANKER)
                self:showBankerTips(true)
            end, 1.5)
        end
    end
end

-- 倒计时更新
function LhdzMainLayer:event_CountTime(msg)
    self.m_pLbCountTime:setVisible(true)
    self:updateCountTime()
end

-- 更新上桌玩家
function LhdzMainLayer:event_TableUser(msg)
    --self:updateTableUserInfo()
end

function LhdzMainLayer:event_Rank(msg)
    self.m_bIsRequestRank = false
    local pLhdzRankLayer = self.m_rootUI:getChildByName("LhdzRankLayer")
    if pLhdzRankLayer then
        pLhdzRankLayer:updateTableView()
    end
end

function LhdzMainLayer:event_BankerScore(msg)
    self:updatebankerInfo()
end

function LhdzMainLayer:onMsgEnterNetWorkFail()
    --相当于进入后台
    self:onMsgEnterBackGround()
end

function LhdzMainLayer:onMsgEnterBackGround()
    --停止在线人数的刷新
    self.m_pLbOnlieCount:stopAllActions()

    --进入后台返回时,不删资源
    self.m_bEnterBackground = true
end

function LhdzMainLayer:onMsgEnterForeGround()
end

function LhdzMainLayer:onMsgReloginSuccess()
end

---------------------------------- on msg event end ----------------------------------

---------------------------------- update view start ----------------------------------
-- 更新倒计时
function LhdzMainLayer:updateCountTime()
   local count = CBetManager.getInstance():getTimeCount();
    if self.m_pLbCountTime then
        if count < 1 then return end
        count = count < 1 and 1 or count
        count = math.floor(count)
        self.m_pLbCountTime:setString(count)
        if LhdzDataMgr.getInstance():getGameStatus() == Lhdz_Const.STATUS.GAME_SCENE_BET then
            if count > 0 and count < 4 then
                self:showCountDownAnim(count)
                AudioManager.getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_RING);
            end
        end
    end
end

-- 更新音效图标
function LhdzMainLayer:updateButtonOfSound(enable)
    local bEnable = enable or AudioManager:getInstance():getSoundOn()
    local path = bEnable and Lhdz_Res.PNG_OF_SOUND_ON or Lhdz_Res.PNG_OF_SOUND_OFF
    self.m_pBtnEffect:loadTextureNormal(path, ccui.TextureResType.plistType)
end

-- 更新音乐图标
function LhdzMainLayer:updateButtonOfMusic(enable)
    local bEnable = enable or AudioManager:getInstance():getMusicOn()
    local path = bEnable and Lhdz_Res.PNG_OF_MUSIC_ON or Lhdz_Res.PNG_OF_MUSIC_OFF
    self.m_pBtnMusic:loadTextureNormal(path, ccui.TextureResType.plistType)
    if bEnable then
        --播放音乐
        AudioManager.getInstance():playMusic(Lhdz_Res.vecLoadingMusic.MUSIC_OF_BGM)
    end
end

-- 更新游戏状态
function LhdzMainLayer:updateGameStatus()
    local status = LhdzDataMgr.getInstance():getGameStatus()
    local pStatusPath = status == Lhdz_Const.STATUS.GAME_SCENE_BET and Lhdz_Res.PNG_OF_STATUS_BET or Lhdz_Res.PNG_OF_STATUS_LOTTERY
    self.m_pSpStauts:setSpriteFrame(pStatusPath)
end

-- 更新神算子胜率
function LhdzMainLayer:updateLuckerInfo()
    local tableUser = LhdzDataMgr.getInstance():getTableUserInfo(1)
    if tableUser then
        self.m_pNo1BetPreNum:setString(tableUser.cbWinCount.."%")
    end
end

-- 更新筹码面值
function LhdzMainLayer:updateJettonValues()
    for i=1, Lhdz_Const.JETTON_ITEM_COUNT do
        local needSocre = CBetManager.getInstance():getJettonScore(i)
        if needSocre < 100 then return end
        local str1 =  string.format(Lhdz_Res.PNG_OF_JETTON, needSocre)
        self.m_pBtnJetton[i]:loadTextureNormal(str1,ccui.TextureResType.plistType)
        self.m_pBtnJetton[i]:loadTexturePressed(str1,ccui.TextureResType.plistType)
        self.m_pBtnJetton[i]:loadTextureDisabled(str1,ccui.TextureResType.plistType)
    end
end

-- 更新庄家信息
function LhdzMainLayer:updatebankerInfo()
    local isBanker = false
    if LhdzDataMgr.getInstance():isSystemBanker() then
        self.m_pSpUserBanker:setVisible(false)
        self.m_sysBankerAni:setVisible(true)
    else
        self.m_pSpUserBanker:setVisible(true)
        self.m_sysBankerAni:setVisible(false)
        -- 庄家名称
        local strBankerName = LhdzDataMgr.getInstance():getBankerName()
        self.m_pLbBankerName:setString(LuaUtils.getDisplayNickName(strBankerName, 8, true))
        -- 庄家金币
        local strBankerGold = LhdzDataMgr.getInstance():getBankerScore()
        self.m_pLbBankerGold:setString(self:dealPlayerCoin(strBankerGold))
        -- 庄家vip
        local bankerId = LhdzDataMgr.getInstance():getBankerUserID()
        local userInfo = CUserManager:getInstance():getUserInfoByUserID(bankerId)
        local vipPath = string.format("hall/plist/vip/img-vip%d.png",userInfo.nVipLev)
        self.m_pImgBankerVip:loadTexture(vipPath, ccui.TextureResType.plistType)
    end

    -- 限制条件
    local strBankerMinGold = LhdzDataMgr.getInstance():getMinBankerScore()
    self.m_pLbCondition:setString(LuaUtils.getGoldNumberNounZH(strBankerMinGold))
    -- 上庄人数
    local nBankerCount = LhdzDataMgr.getInstance():getBankerListSize()
    local strNum = string.format("%d", nBankerCount)
    self.m_pLbBankerCount:setString(strNum)

    -- 自己是否上庄
    local bIsBanker =  LhdzDataMgr.getInstance():isBanker() 
    local bInList = LhdzDataMgr.getInstance():isInBankerList()
    self.m_pBtnUpBanker:setVisible(not(bIsBanker or bInList ~= -1))
    self.m_pBtnDownBanker:setVisible(bIsBanker or bInList ~= -1)
end

-- 更新上桌玩家信息
function LhdzMainLayer:updateTableUserInfo()
    local isChangeUser = false
    for i=1, Lhdz_Const.TABEL_USER_COUNT do
        local pTableUser = LhdzDataMgr.getInstance():getTableUserInfo(i)
        if (not pTableUser) or (pTableUser.dwUserID == G_CONSTANTS.INVALID_CHAIR) then
            self.m_pSpNoPlayer[i]:setVisible(true)
            self.m_pNodeShowPlayer[i]:setVisible(false)
            LhdzDataMgr.getInstance():setTableUserIdLast(i, G_CONSTANTS.INVALID_CHAIR)
        else
            --换人播放动画
            local newuid = pTableUser.dwUserID
            local lastuid = LhdzDataMgr.getInstance():getTableUserIdLast(i)

            local func = function(armatureBack, movementType, movementID)
                if movementType == ccs.MovementEventType.complete then
                    if movementID == Lhdz_Res.ANI_OF_CHANGEUSER.ANILIST.BEGIN.NAME then
                        local tagData = armatureBack:getTag()
                        print("Lhdz_Res.ANI_OF_CHANGEUSER.ANILIST.BEGIN.NAME tagData:",tagData)
                        local faceID = tagData%100
                        local headFrame = math.floor(tagData/100)
                        self:replaceUserHeadSprite(armatureBack, Lhdz_Res.ANI_OF_CHANGEUSER.ANILIST.BEGIN, faceID, headFrame)
                        armatureBack:getAnimation():play(Lhdz_Res.ANI_OF_CHANGEUSER.ANILIST.END.NAME)
                    elseif movementID == Lhdz_Res.ANI_OF_CHANGEUSER.ANILIST.END.NAME then
                        self.m_pSpNoPlayer[i]:setVisible(false)
                        self.m_pNodeShowPlayer[i]:setVisible(true)
                        local name_str = LuaUtils.getDisplayTwoString(pTableUser.szNickName,80,20)
                        self.m_pLbShowPlayerName[i]:setString(name_str)
                        local viewscore = pTableUser.llUserScore - pTableUser.totalbet
                        viewscore = viewscore > 0 and viewscore or 0
                        self.m_pLbShowPlayerGold[i]:setString(self:dealPlayerCoin(viewscore))

                        --更新vip等级
                        local vipLevel = self:getVipByUserID(pTableUser.dwUserID)
                        vipLevel = math.min(vipLevel, VIP_LEVEL_MAX)
                        local vipPath = string.format("hall/plist/vip/img-vip%d.png", vipLevel)
                        self.m_pImgPlayerVip[i]:loadTexture(vipPath, ccui.TextureResType.plistType)
                    end
                end
            end

            if newuid ~= lastuid then
                LhdzDataMgr.getInstance():setTableUserIdLast(i, newuid)
                --新用户和老用户都是有效用户，则播放转动动画
                if lastuid ~= G_CONSTANTS.INVALID_CHAIR and newuid ~= G_CONSTANTS.INVALID_CHAIR then
                    local frameIndex = self:getHeadFrameIndex(pTableUser.dwUserID)
                    local faceID = pTableUser.wFaceID % G_CONSTANTS.FACE_NUM
                    local tagData = frameIndex *100 + faceID
                    self.m_changeUserAniVec[i]:setTag(tagData)
                    self.m_changeUserAniVec[i]:getAnimation():play(Lhdz_Res.ANI_OF_CHANGEUSER.ANILIST.BEGIN.NAME)
                    self.m_changeUserAniVec[i]:getAnimation():setMovementEventCallFunc(func)
                    isChangeUser = true
                else
                    self:replaceUserHeadSprite(self.m_changeUserAniVec[i], Lhdz_Res.ANI_OF_CHANGEUSER.ANILIST.BEGIN, pTableUser.wFaceID, self:getHeadFrameIndex(pTableUser.dwUserID))
                    self.m_pSpNoPlayer[i]:setVisible(false)
                    self.m_pNodeShowPlayer[i]:setVisible(true)
                    local name_str = LuaUtils.getDisplayTwoString(pTableUser.szNickName,80,20)
                    self.m_pLbShowPlayerName[i]:setString(name_str)
                    local viewscore = pTableUser.llUserScore - pTableUser.totalbet
                    viewscore = viewscore > 0 and viewscore or 0
                    self.m_pLbShowPlayerGold[i]:setString(self:dealPlayerCoin(viewscore))

                    --更新vip等级
                    local vipLevel = self:getVipByUserID(pTableUser.dwUserID)
                    vipLevel = math.min(vipLevel, VIP_LEVEL_MAX)
                    local vipPath = string.format("hall/plist/vip/img-vip%d.png", vipLevel)
                    self.m_pImgPlayerVip[i]:loadTexture(vipPath, ccui.TextureResType.plistType)
                end
            else
                self:replaceUserHeadSprite(self.m_changeUserAniVec[i], Lhdz_Res.ANI_OF_CHANGEUSER.ANILIST.BEGIN, pTableUser.wFaceID)
                self.m_pSpNoPlayer[i]:setVisible(false)
                self.m_pNodeShowPlayer[i]:setVisible(true)
                local name_str = LuaUtils.getDisplayTwoString(pTableUser.szNickName,80,20)
                self.m_pLbShowPlayerName[i]:setString(name_str)
                local viewscore = pTableUser.llUserScore
                viewscore = viewscore > 0 and viewscore or 0
                if i < 3 and  Player:getAccountID() == pTableUser.dwUserID then 
                    local G = LhdzDataMgr.getInstance():getTempSelfScore()
                    self.m_pLbShowPlayerGold[i]:setString(self:dealPlayerCoin(G))
                else
                    self.m_pLbShowPlayerGold[i]:setString(self:dealPlayerCoin(viewscore))
                end

                --更新vip等级
--                local vipLevel = self:getVipByUserID(pTableUser.dwUserID)
--                vipLevel = math.min(vipLevel, VIP_LEVEL_MAX)
--                local vipPath = string.format("hall/plist/vip/img-vip%d.png", vipLevel)
--                self.m_pImgPlayerVip[i]:loadTexture(vipPath, ccui.TextureResType.plistType)
            end
        end
    end

    if isChangeUser then
        AudioManager:getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_CHANGEUSER)
    end
end

-- 更新上桌玩家分数
function LhdzMainLayer:updateTableUserScore()
    for i=1, Lhdz_Const.TABEL_USER_COUNT do
        local pTableUser = LhdzDataMgr.getInstance():getTableUserInfo(i)
        if pTableUser and (pTableUser.dwUserID ~= G_CONSTANTS.INVALID_CHAIR) then
            local viewscore = pTableUser.llUserScore - pTableUser.totalbet
            viewscore = viewscore > 0 and viewscore or 0
            self.m_pLbShowPlayerGold[i]:setString(self:dealPlayerCoin(viewscore))
        end
    end
end

-- 更新在线玩家数量
function LhdzMainLayer:updateOnliePlayerCount( )
    self.m_pLbOnlieCount:setString(LhdzDataMgr.getInstance():getUserCountAtBegin())
end

-- 更新历史记录
function LhdzMainLayer:updateGameRecord(isAnimo)
    local nSize = LhdzDataMgr.getInstance():getHistoryListSize()
    if nSize > Lhdz_Const.MAX_HISTORY_COUNT-5 then
        nSize = Lhdz_Const.MAX_HISTORY_COUNT-5
    end
    --for i = nSize, 1, -1 do
    for i = 1, nSize do
        local pHistory = LhdzDataMgr.getInstance():getHistoryByIndex(nSize-i+1)
        local path = Lhdz_Res.PNG_OF_ICON_DRAW
        if pHistory.cbAreaType == Lhdz_Const.AREA.DRAGON then
            path = Lhdz_Res.PNG_OF_ICON_DRAGON
        elseif pHistory.cbAreaType == Lhdz_Const.AREA.TIGER then
            path = Lhdz_Res.PNG_OF_ICON_TIGER
        end
        self.m_pSpHistory[i]:setVisible(true)
        self.m_pSpHistory[i]:setSpriteFrame(path)

        if i == nSize then
            if isAnimo then
                self.m_pSpHistory[i]:setVisible(false)
                self.m_pSpHistoryNew:setVisible(false)
                self.m_pSpHistoryLight:setVisible(false)
                self.m_pSpHistory[i]:runAction(cc.Sequence:create(cc.DelayTime:create(2),cc.CallFunc:create(function ()
                    self.m_pSpHistory[i]:setVisible(true)
                    self.m_pSpHistoryNew:setVisible(true)
                    self.m_pSpHistoryNew:setOpacity(0)
                    self.m_pSpHistoryNew:runAction(cc.FadeIn:create(2))
                    self.m_pSpHistoryNew:setPosition(cc.p(self.m_pSpHistory[i]:getPositionX()-4.7, self.m_pSpHistory[i]:getPositionY()+5.5))
                    self.m_pSpHistoryLight:stopAllActions()
                    self.m_pSpHistoryLight:setVisible(true)
                    self.m_pSpHistoryLight:setOpacity(0)
                    self.m_pSpHistoryLight:setPosition(cc.p(self.m_pSpHistory[i]:getPositionX(), self.m_pSpHistory[i]:getPositionY()))
                    self.m_pSpHistoryLight:runAction(cc.Repeat:create(cc.Sequence:create(cc.FadeOut:create(0.5), cc.FadeIn:create(0.5)), 5))
                end) ) )
            else
                self.m_pSpHistory[i]:setVisible(true)
                self.m_pSpHistoryNew:setVisible(true)
                self.m_pSpHistoryNew:setPosition(cc.p(self.m_pSpHistory[i]:getPositionX()-4.7, self.m_pSpHistory[i]:getPositionY()+5.5))
                self.m_pSpHistoryLight:stopAllActions()
                self.m_pSpHistoryLight:setVisible(true)
                self.m_pSpHistoryLight:setPosition(cc.p(self.m_pSpHistory[i]:getPositionX(), self.m_pSpHistory[i]:getPositionY()))
                self.m_pSpHistoryLight:runAction(cc.Repeat:create(cc.Sequence:create(cc.FadeOut:create(0.5), cc.FadeIn:create(0.5)), 5))
            end
   
        end
        
    end
end

--更新显示临时历史记录
function LhdzMainLayer:updateTempGameRecord()
    if 0 == LhdzDataMgr.getInstance():synTempHistory() then
        return
    end
    self:updateGameRecord(true)
    self:updateTrendLayer(false)
end

-- 更新筹码状态
function LhdzMainLayer:updateJettonStatus()
    for i = 1, Lhdz_Const.JETTON_ITEM_COUNT do
        local bStatus = LhdzDataMgr.getInstance():getJettonEnableStatus(i)
        local color = bStatus and cc.c3b(255,255,255) or cc.c3b(160,160,160)
        local opacity = bStatus and Chip_EnableOpacity or Chip_DisableOpacity
        self.m_pBtnJetton[i]:setEnabled(bStatus)
        self.m_pBtnJetton[i]:setColor(color)
        self.m_pBtnJetton[i]:setOpacity(opacity)
    end
--    local newSelIndex = LhdzDataMgr.getInstance():getJettonSelAdjust();
--    if (newSelIndex > 0) then
--        self:onBtnJettonNumClicked(self.m_pBtnJetton[newSelIndex],ccui.TouchEventType.ended,false);
--    else
--        self.m_nIndexChoose = self.m_nIndexChoose<1 and 1 or self.m_nIndexChoose
--        self:onBtnJettonNumClicked(self.m_pBtnJetton[self.m_nIndexChoose],ccui.TouchEventType.ended,false);
--    end
end


---- 更新筹码状态(进入游戏调用，断线重连后PlayerInfo.getInstance():getUserScore() 会变为下注后的值
---- 导致筹码显示出错，故单独列出首次初始化筹码函数)
--function LhdzMainLayer:updateJettonStatusFrist()
--    for i = 1, Lhdz_Const.JETTON_ITEM_COUNT do
--        local bStatus = LhdzDataMgr.getInstance():getJettonEnableStatusFrist(i)
--        local color = bStatus and cc.c3b(255,255,255) or cc.c3b(160,160,160)
--        local opacity = bStatus and Chip_EnableOpacity or Chip_DisableOpacity
--        self.m_pBtnJetton[i]:setEnabled(bStatus)
--        self.m_pBtnJetton[i]:setColor(color)
--        self.m_pBtnJetton[i]:setOpacity(opacity)
--    end
--    local newSelIndex = LhdzDataMgr.getInstance():getJettonSelAdjustFrist();
--    if (newSelIndex > 0) then
--        self:onBtnJettonNumClickedFrist(self.m_pBtnJetton[newSelIndex]);
--    else
--        self.m_nIndexChoose = self.m_nIndexChoose<1 and 1 or self.m_nIndexChoose
--        self:onBtnJettonNumClickedFrist(self.m_pBtnJetton[self.m_nIndexChoose]);
--    end
--end
-- 更新续投按钮状态
function LhdzMainLayer:updateContinueStatus(enable)
   
     -- 庄家不能续投
--    if LhdzDataMgr.getInstance():isBanker() then
--        enable = false
--    end

    self.m_pBtnContinue:setEnabled(enable)
    local color = enable and cc.c3b(255,255,255) or cc.c3b(160,160,160)
    self.m_pBtnContinue:setColor(color)

end

-- 更新银行状态
function LhdzMainLayer:updateBankStatus(bEnabled)
    self.m_pBtnBank:setEnabled(bEnabled)
    local color = bEnabled and cc.c3b(255,255,255) or cc.c3b(160,160,160)
    --self.m_pBtnBank:setColor(color)
    self.m_pBtnStrongBox:setEnabled(bEnabled)
    self.m_pUserBg:setEnabled(bEnabled)
    
    local status = bEnabled and "1" or "0"
    --设置银行按钮状态
    local _event = {
        name = Public_Events.MSG_BANK_STATUS,
        packet = status,
    }
    SLFacade:dispatchCustomEvent(Public_Events.MSG_BANK_STATUS, _event)
end

-- 更新下注区域的下注数
function LhdzMainLayer:updateBetAreaValue()
    for i=1,tonumber(Lhdz_Const.GAME_DOWN_COUNT) do
        local llMyValue = LhdzDataMgr.getInstance():getMyBetValue(i)/Game_Chip_Times;
        local llOtherValue = LhdzDataMgr.getInstance():getOtherBetValue(i)/Game_Chip_Times;

        local new_llMyValue = LuaUtils.getFormatGoldAndNumberAndZi(llMyValue)
        local new_llOtherValue = LuaUtils.getFormatGoldAndNumberAndZi(llMyValue+llOtherValue)

        self.m_pLbMyBetValue[i]:setString("下注:" .. new_llMyValue)
        self.m_pLbAllBetValue[i]:setString(new_llOtherValue)

        self.m_pLbMyBetValue[i]:setVisible(llMyValue > 0)
        self.m_pLbAllBetValue[i]:setVisible((llMyValue + llOtherValue) > 0)
    end
end

-- 更新走势界面
function LhdzMainLayer:updateTrendLayer(bOpen)
    local pLhdzTrendLayer = self.m_rootUI:getChildByName("LhdzTrendLayer")
    if not pLhdzTrendLayer and bOpen then
        pLhdzTrendLayer = LhdzTrendLayer.new()
        pLhdzTrendLayer:setName("LhdzTrendLayer")
        self.m_rootUI:addChild(pLhdzTrendLayer)
    end
    if pLhdzTrendLayer then
        pLhdzTrendLayer:updateInfo()
    end
end

-- 清除等待下局
function LhdzMainLayer:clearWaitNextPlay()
    if self.m_pAnimWaitNext then
        self.m_pAnimWaitNext:removeFromParent()
        self.m_pAnimWaitNext = nil
    end
end

function LhdzMainLayer:clearNo1BetStar()
    for i=1, Lhdz_Const.GAME_DOWN_COUNT do
        self.m_pSpStar[i]:setVisible(false)
        self.m_bIsShowStar[i] = false
    end
    self.m_dNo1AllBet = {0,0,0}
    self.m_pNo1BetPreBar1:setVisible(false)
    self.m_pNo1BetPreBar2:setVisible(false)
    self.m_pNo1BetPreBar3:setVisible(false)
    self.m_pNo1BetPreBar1:setContentSize(cc.size(0, self.m_pNo1BetPreBar1:getContentSize().height))
    self.m_pNo1BetPreBar2:setContentSize(cc.size(0, self.m_pNo1BetPreBar2:getContentSize().height))
    self.m_pNo1BetPreBar3:setContentSize(cc.size(0, self.m_pNo1BetPreBar3:getContentSize().height))
end
---------------------------------- update view end ----------------------------------

---------------------------------- play animation start ----------------------------------
-- 背景动画
function LhdzMainLayer:showBackgroundAnim()
    for i = 1, 2 do
    -- 背景
        local background = self.m_pNodeBackground:getChildByName("background"..i)
        if not background then
--            local strJson = string.format(Lhdz_Res.ANI_OF_BACKGROUND, "json")
--            local strAtlas = string.format(Lhdz_Res.ANI_OF_BACKGROUND, "atlas")
--            background = sp.SkeletonAnimation:createWithJsonFile(strJson, strAtlas)
            background = ParticalPool.getInstance():getBGEffect(i)
            background:setPosition(cc.p(667, 375))
            background:setName("background"..i)
            background:setAnimation(0, "animation"..i, true)
            self.m_pNodeBackground:addChild(background, 0)
        end
        -- 灯笼
        local lantern = self.m_pNodeLantern:getChildByName("Lantern"..i)
        if not lantern then
--            local strJson = string.format(Lhdz_Res.ANI_OF_LANTERN, "json")
--            local strAtlas = string.format(Lhdz_Res.ANI_OF_LANTERN, "atlas")
--            lantern = sp.SkeletonAnimation:createWithJsonFile(strJson, strAtlas)
            lantern = ParticalPool.getInstance():getLanternEffect(i)
            local pos = i == 1 and cc.p(147, 530) or cc.p(1182, 530)
            if i == 2 then
                lantern:setScaleX(-1)
            end
            lantern:setPosition(pos)
            lantern:setName("Lantern"..i)
            lantern:setAnimation(0, "animation", true)
            self.m_pNodeLantern:addChild(lantern, 0)
        end
        
        -- 动物
        local animal = self.m_pNodeLantern:getChildByName("Animal"..i)
        if not animal then
--            local strJson = string.format(Lhdz_Res.ANI_OF_ANIMAL, "json")
--            local strAtlas = string.format(Lhdz_Res.ANI_OF_ANIMAL, "atlas")
--            animal = sp.SkeletonAnimation:createWithJsonFile(strJson, strAtlas)
            animal = ParticalPool.getInstance():getAnimalEffect(i)
            local pos = i == 1 and cc.p(391, 660) or cc.p(1016, 690)
            animal:setPosition(pos)
            animal:setName("Animal"..i)
            animal:setAnimation(0, "animation"..i, true)
            animal:update(0)
            self.m_pNodeLantern:addChild(animal, 0)
        end
    end
end

--等待下局开始
function LhdzMainLayer:showWaitNextAnim()
    if LhdzDataMgr.getInstance():getGameStatus() ~= Lhdz_Const.STATUS.GAME_SCENE_BET then
        if not self.m_pAnimWaitNext then
            local strJson = string.format(Lhdz_Res.ANI_OF_NEXT_GAME, "json")
            local strAtlas = string.format(Lhdz_Res.ANI_OF_NEXT_GAME, "atlas")
            self.m_pAnimWaitNext = sp.SkeletonAnimation:createWithJsonFile(strJson, strAtlas)
            self.m_pAnimWaitNext:setPosition(cc.p(667, 375))
            self.m_pAnimWaitNext:setAnimation(0, "animation", true)
            self.m_pNodeArmature:addChild(self.m_pAnimWaitNext, 0)
        end
        self.m_pAnimWaitNext:setVisible(true)
        self.m_pNodeCountTime:setVisible(false)
    else
        self:clearWaitNextPlay()
    end
end

--重置扑克动画状态
function LhdzMainLayer:resetCardAnim()
    for i = 1, Lhdz_Const.CARD_COUNT do
        if self.m_pAnimCard[i] then
            self.m_pAnimCard[i]:stopAllActions()
            self.m_pAnimCard[i]:getAnimation():play("Animation2")
            self.m_pAnimCard[i]:getAnimation():setMovementEventCallFunc(function()
                return nil
            end)
        end

        local pokerAnim = self.m_pNodeArmature:getChildByName("PokerAnimation"..i)
        if pokerAnim then
            pokerAnim:setVisible(true)
            pokerAnim:stopAllActions()
            pokerAnim:setAnimation(0, "animation1", true)
        end
    end
end

--发牌动画
function LhdzMainLayer:showSendCardAnim(isResule)
    local cards = LhdzDataMgr.getInstance():getCards()
    for i = 1, Lhdz_Const.CARD_COUNT do
        if not self.m_pAnimCard[i] then
            self.m_pAnimCard[i] = ccs.Armature:create(Lhdz_Res.vecAnim.SendCard)
            self.m_pAnimCard[i]:setScale(1.2)
            self.m_pAnimCard[i]:update(0)
            local pos = i == 1 and cc.p(576, 670)or cc.p(756, 670)
            self.m_pAnimCard[i]:setPosition(pos)
            self.m_pNodeArmature:addChild(self.m_pAnimCard[i])
        end
        if isResule then
            if i == 1 then
                AudioManager:getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_FLOP_CARD)
                self:replaceArmatureSprite(self.m_pAnimCard[i], cards[i])
                self.m_pAnimCard[i]:getAnimation():play("Animation1")
            end
            local animationEvent = function(armatureBack, movementType, movementID)
                 if movementType == ccs.MovementEventType.complete then
                    if i == 1 then
                        local value, color = LhdzDataMgr.getInstance():getCardValueAndColor(cards[i])
                        AudioManager:getInstance():playSound(string.format(Lhdz_Res.vecSound.SOUND_OF_VALUE_LEFT, value))
                        self:doSomethingLater(function( ... )
                            AudioManager:getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_FLOP_CARD)
                            self:replaceArmatureSprite(self.m_pAnimCard[i+1], cards[i+1])
                            self.m_pAnimCard[i+1]:getAnimation():play("Animation1")
                        end, 0.5)
                    else
                        local v1, _ = LhdzDataMgr.getInstance():getCardValueAndColor(cards[1])
                        local value, color = LhdzDataMgr.getInstance():getCardValueAndColor(cards[i])
                        if value > v1 then
                            AudioManager:getInstance():playSound(string.format(Lhdz_Res.vecSound.SOUND_OF_VALUE_RIGHTW, value))
                        else
                            AudioManager:getInstance():playSound(string.format(Lhdz_Res.vecSound.SOUND_OF_VALUE_RIGHTL, value))
                        end
                        self:doSomethingLater(function( ... )
                            local areaType = LhdzDataMgr:getInstance():getAreaType()
                            local areaSound = Lhdz_Res.vecSound.SOUND_OF_DRAW
                            if areaType == Lhdz_Const.AREA.DRAGON then
                                areaSound = Lhdz_Res.vecSound.SOUND_OF_DRAGON
                            elseif areaType == Lhdz_Const.AREA.TIGER then
                                areaSound = Lhdz_Res.vecSound.SOUND_OF_TIGER
                            end
                            AudioManager:getInstance():playSound(areaSound)
                            self.m_chipMgr:flychipex(function()
                                 self:playUserScoreEffect()
                            end)
                            self:updateTempGameRecord()
                            self:showWinAreaAnim()
                        end, 1)
                    end
                 end
            end
            self.m_pAnimCard[i]:getAnimation():setMovementEventCallFunc(animationEvent)
        else
            self.m_pAnimCard[i]:getAnimation():play("Animation2")
            self.m_pAnimCard[i]:getAnimation():setMovementEventCallFunc(function()
                return nil
            end)
        end
    end
    self:showPokerAmin(isResule)
end

-- 播放牌的特效
function LhdzMainLayer:showPokerAmin(isResule)
    for i=1, 2 do
        local pokerAnim = self.m_pNodeArmature:getChildByName("PokerAnimation"..i)
        if not pokerAnim then
            local strJson = string.format(Lhdz_Res.ANI_OF_PAIXIAO, "json")
            local strAtlas = string.format(Lhdz_Res.ANI_OF_PAIXIAO, "atlas")
            pokerAnim = sp.SkeletonAnimation:createWithJsonFile(strJson, strAtlas)
            pokerAnim:setScale(1.1)
            local pos = i == 1 and cc.p(576, 670)or cc.p(756, 670)
            pokerAnim:setPosition(pos)
            pokerAnim:setName("PokerAnimation"..i)
            self.m_pNodeArmature:addChild(pokerAnim, 0)
        end
        local animation = isResule and "animation2" or "animation1"
        pokerAnim:setAnimation(0, animation, true)
        pokerAnim:update(0)
        if animation == "animation2" then
            pokerAnim:setPositionY(672)
        else
            pokerAnim:setPositionY(665)
        end
        pokerAnim:setVisible(not isResule)
        if isResule then
            local func = cc.CallFunc:create(function()
                local areaType = LhdzDataMgr.getInstance():getAreaType()
                if i == Lhdz_Const.AREA.DRAGON then
                    pokerAnim:setVisible(areaType~=2 or (not isResule))
                else
                    pokerAnim:setVisible(areaType~=1 or (not isResule))
                end
            end)
            pokerAnim:runAction(cc.Sequence:create(cc.DelayTime:create(2.5), func))
        end
    end
end

-- 开始下注or结算下注
function LhdzMainLayer:showGameStatusAnim()
    if not self.m_pAnimStatus then
        local strJson = string.format(Lhdz_Res.ANI_OF_GAME_TIPS, "json")
        local strAtlas = string.format(Lhdz_Res.ANI_OF_GAME_TIPS, "atlas")
        self.m_pAnimStatus = sp.SkeletonAnimation:createWithJsonFile(strJson, strAtlas)
        self.m_pAnimStatus:setPosition(cc.p(667, 375))
        self.m_pNodeArmature:addChild(self.m_pAnimStatus, 0)
    end

    self.m_pAnimStatus:setToSetupPose()
    if LhdzDataMgr.getInstance():getGameStatus() == Lhdz_Const.STATUS.GAME_SCENE_BET then
        AudioManager.getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_START_BET)
        self.m_pAnimStatus:setAnimation(0, "animation1", false)
        self:updateTableUserInfo()
    elseif LhdzDataMgr.getInstance():getGameStatus() == Lhdz_Const.STATUS.GAME_SCENE_RESULT then
        AudioManager.getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_STOP_BET)
        self.m_pAnimStatus:setAnimation(0, "animation2", false)
    end
end

-- VS动画
function LhdzMainLayer:showGameVSAnim()
    if LhdzDataMgr.getInstance():getGameStatus() == Lhdz_Const.STATUS.GAME_SCENE_FREE then
        AudioManager.getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_BATTLE)
        if not self.m_pAnimVs then
            local strJson = string.format(Lhdz_Res.ANI_OF_ANIMAL, "json")
            local strAtlas = string.format(Lhdz_Res.ANI_OF_ANIMAL, "atlas")
            self.m_pAnimVs = sp.SkeletonAnimation:createWithJsonFile(strJson, strAtlas)
            self.m_pAnimVs:setPosition(cc.p(637+self.diffX, 375))
            self:addChild(self.m_pAnimVs, 0)
        end
        self.m_pAnimVs:setToSetupPose()
        self.m_pAnimVs:setAnimation(0, "animation3", false)
--        self.m_pNodeAnimVS:setVisible(true)
--        self.m_pAnimVs:registerSpineEventHandler(function(event)
--            self.m_pNodeAnimVS:setVisible(false)
--        end, sp.EventType.ANIMATION_COMPLETE)
    end
end

-- 神算子下注飘星
function LhdzMainLayer:playFlyStarAnim(isInitGame)
    for i=1, Lhdz_Const.GAME_DOWN_COUNT do
        local isNo1Bet = LhdzDataMgr.getInstance():getIsNo1BetByIndex(i)
        if isNo1Bet and (not self.m_bIsShowStar[i]) then
            self.m_bIsShowStar[i] = true
            if isInitGame then
                self.m_pSpStar[i]:setVisible(true)
            else
                self:playstar(i)
            end
        end
    end
end

-- 结算飘数字动画
--[[
    弹分规则：
    1 宽屏显示下，一律与头像居中显示
    2 非宽屏显示，数值展示宽度小于指定值，则居中显示(锚点x=0.5)，否则按左右对齐显示(右侧显示锚点x=1 左侧显示锚点x=0)
]]--
function LhdzMainLayer:playUserScoreEffect()
    local llAwardValue = 0 
        llAwardValue = LhdzDataMgr.getInstance():getMyResult()  
    local data =  LhdzDataMgr.getInstance():getResult()  
    print("playUserScoreEffect")
    dump(data)
     for  k,v in pairs(data)do   
	    if (v.m_accountId == Player:getAccountID()) then  
            
             local fntfile = (v.m_netProfit * 0.01 >= 0) and Lhdz_Res.FNT_OF_RESULT_WIN or Lhdz_Res.FNT_OF_RESULT_LOSE
            self.m_pLbResultGold:setFntFile(fntfile)
            local fuhaoStr = (v.m_netProfit * 0.01 >= 0) and "+" or "-"
            local goldStr = string.format("%s%s",fuhaoStr,self:dealPlayerCoin(v.m_netProfit * 0.01/Game_Chip_Times)) 
            self.m_pLbResultGold:setString(goldStr)  
             self.m_pLbUserGold:setString(v.m_curScore*0.01)
                    LhdzDataMgr.getInstance():setTempSelfScore(v.m_curScore*0.01)
            local pos = cc.p(self.m_pLbResultGold:getPosition())
            local action = cc.Sequence:create(
                cc.Show:create(),
                cc.EaseBounceOut:create(cc.MoveBy:create(0.6, cc.p(0,25))),
                cc.DelayTime:create(1.8),
                cc.FadeOut:create(0.5),
                cc.CallFunc:create(function ()
                    self.m_pLbResultGold:setVisible(false)
                    self.m_pLbResultGold:setPosition(pos)
                    self.m_pLbResultGold:stopAllActions()
                   
            end))
            if LhdzDataMgr.getInstance():getAllMyBetValue() > 0 or bBanker then
                self.m_pLbResultGold:setOpacity(255)
                self.m_pLbResultGold:setVisible(true)
                self.m_pLbResultGold:runAction(action)
            end
        end
        for i = 1,6 do  
			 local tableUser = LhdzDataMgr.getInstance():getTableUserInfo(i)
            if tableUser and tableUser.dwUserID==v.m_accountId then
                local tableUserResult = LhdzDataMgr.getInstance():getTableResult(i) 
                if LhdzDataMgr.getInstance():getTableBetValue(i) > 0 and --有投注行为
                    not ((LhdzDataMgr.getInstance():getAreaType() == Lhdz_Const.AREA.DRAW) and
                         (0 == LhdzDataMgr.getInstance():getTableAreaBetValue(i, Lhdz_Const.AREA.DRAW))) --非开和没下和
                    then
                    --local tableUser = LhdzDataMgr.getInstance():getTableUserInfo(i)
                    --if tableUser and tableUser.dwUserID then
                        --local tableUserResult = LhdzDataMgr.getInstance():getTableResult(i)
                        if tableUserResult > 0 then
                            self:showTableUserWinAnim(i)
                            self.m_getChipEffect[i]:setVisible(true)
                            self.m_getChipEffect[i]:setAnimation(0, "animation", true)
                            self:doSomethingLater(function()
                                self.m_getChipEffect[i]:setToSetupPose()
                                self.m_getChipEffect[i]:setVisible(false)
                            end, 2.2)
                            --tableUser.llUserScore = tableUser.llUserScore + tableUser.totalbet + tableUserResult
                        end
                        local fntfile = (tableUserResult >= 0) and Lhdz_Res.FNT_OF_RESULT_WIN or Lhdz_Res.FNT_OF_RESULT_LOSE
                        self.m_pLbTableResultGold[i]:setFntFile(fntfile)
                
                        --设置正确显示的位置
                        local scoreWidth = self:getScoreWidth(tableUserResult)
                        local anchor, posx = self:getScoreViewPos(i, scoreWidth)
                        self.m_pLbTableResultGold[i]:setAnchorPoint(anchor, 0.5)
                        self.m_pLbTableResultGold[i]:setPosition(posx, 0)
                
                        local fuhaoStr = ((tableUserResult >= 0)) and "+" or "-"
                        self.m_pLbTableResultGold[i]:setString(string.format("%s%s",fuhaoStr,self:dealPlayerCoin(v.m_netProfit * 0.01)))
                        self.m_pLbShowPlayerGold[i]:setString(self:dealPlayerCoin((v.m_curScore*0.01)))
                        -- local _,decimal = math.modf(tableUserResult/Game_Chip_Times)
                        -- if math.abs(decimal)==0 then
                        --     local goldStr = string.format("%s%s",fuhaoStr,tableUserResult/Game_Chip_Times)
                        --     self.m_pLbTableResultGold[i]:setString(goldStr)
                        -- else
                        --     self.m_pLbTableResultGold[i]:setString(string.format("%s%s元",fuhaoStr,LuaUtils.getFormatGoldAndNumber(tableUserResult)))
                        -- end
                    
                        self.m_pLbTableResultGold[i]:setPosition(cc.p(self.m_pLbTableResultGold[i]:getPositionX(), -30))
                        local action = cc.Sequence:create(
                            cc.Show:create(),
                            cc.EaseBounceOut:create(cc.MoveBy:create(0.6, cc.p(0, 50))), 
                            cc.DelayTime:create(1.8), 
                            cc.FadeOut:create(0.5),
                            cc.CallFunc:create(function()
                                self.m_pLbTableResultGold[i]:stopAllActions()
                                
                        end))
                        self.m_pLbTableResultGold[i]:setOpacity(255)
                        self.m_pLbTableResultGold[i]:runAction(action)
                    --end
                end
            end
		end
    end 
     

    --local stateBanker = (LhdzDataMgr.getInstance():getAllOtherBetValue() > 0) and bBanker
    --if not bBanker or stateBanker then
    if LhdzDataMgr.getInstance():getAllMyBetValue() > 0 and --有投注行为
        not ((LhdzDataMgr.getInstance():getAreaType() == Lhdz_Const.AREA.DRAW) and
             (0 == LhdzDataMgr.getInstance():getMyBetValue(Lhdz_Const.AREA.DRAW))) --非开和没下和
        then 
        print ("我结算金额："..llAwardValue)
        local fntfile = (llAwardValue >= 0) and Lhdz_Res.FNT_OF_RESULT_WIN or Lhdz_Res.FNT_OF_RESULT_LOSE
        self.m_pLbResultGold:setFntFile(fntfile)
        local fuhaoStr = (llAwardValue >= 0) and "+" or "-"
        local goldStr = string.format("%s%s",fuhaoStr,self:dealPlayerCoin(llAwardValue/Game_Chip_Times)) 
        self.m_pLbResultGold:setString(goldStr)  
        local pos = cc.p(self.m_pLbResultGold:getPosition())
        local action = cc.Sequence:create(
            cc.Show:create(),
            cc.EaseBounceOut:create(cc.MoveBy:create(0.6, cc.p(0,25))),
            cc.DelayTime:create(1.8),
            cc.FadeOut:create(0.5),
            cc.CallFunc:create(function ()
                self.m_pLbResultGold:setVisible(false)
                self.m_pLbResultGold:setPosition(pos)
                self.m_pLbResultGold:stopAllActions()
        end))
        if LhdzDataMgr.getInstance():getAllMyBetValue() > 0 or bBanker then
            self.m_pLbResultGold:setOpacity(255)
            self.m_pLbResultGold:setVisible(true)
            self.m_pLbResultGold:runAction(action)
        end
    end

        --庄家飘数字
--        self.m_pLbBankerResultGold:setVisible(true)
--        self.m_pLbBankerResultGold:setOpacity(255)
--        --local posBankGold = cc.p(self.m_pLbBankerResultGold:getPosition())
--        local posBankGold = cc.p(335, 553)
--        self.m_pLbBankerResultGold:setPosition(posBankGold)
--        local action1 = cc.Sequence:create(
--            cc.Show:create(),
--            cc.EaseBounceOut:create(cc.MoveBy:create(0.6, cc.p(0,25))),
--            cc.DelayTime:create(1.8),
--            cc.FadeOut:create(0.5),
--            cc.CallFunc:create(function ()
--                self.m_pLbBankerResultGold:setVisible(false)
--                self.m_pLbBankerResultGold:setPosition(posBankGold)
--                self.m_pLbBankerResultGold:stopAllActions()
--        end))
--        local bankscore = LhdzDataMgr.getInstance():getBankerResult()
--        local fntfile = (bankscore >= 0) and Lhdz_Res.FNT_OF_RESULT_WIN or Lhdz_Res.FNT_OF_RESULT_LOSE
--        self.m_pLbBankerResultGold:setFntFile(fntfile)
--        local fuhaoStr =  ((bankscore >= 0)) and "+" or "-"
--        local goldStr = string.format("%s%s",fuhaoStr,self:dealPlayerCoin(bankscore))
--        -- local _,decimal = math.modf(bankscore/100)
--        -- if math.abs(decimal)==0 then
--        --     goldStr = string.format("%s%s元",fuhaoStr,bankscore/100)
--        -- end
--        self.m_pLbBankerResultGold:setString(goldStr)
--        self.m_pLbBankerResultGold:runAction(action1)
    --end


    
    if not bBanker and LhdzDataMgr.getInstance():getAllMyBetValue() == 0 then
        TOAST("本局您没有下注!");
    end
     
    
    LhdzDataMgr.getInstance():clearMyBetValue()
   -- self:updatebankerInfo()
    AudioManager.getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_POPGOLD)
end

-- 上桌玩家获胜动画
function LhdzMainLayer:showTableUserWinAnim(index)

    --神算子和富豪的特效
    if 1 == index then
        self.m_luckyWinEffect:getAnimation():play(Lhdz_Res.ANI_OF_TOPWINEFFECT.ANILIST.LUCKYPLAY.NAME, -1, -1)
    elseif 2 == index then
        self.m_richWinEffect:getAnimation():play(Lhdz_Res.ANI_OF_TOPWINEFFECT.ANILIST.RICHPLAY.NAME, -1, -1)
    end

    local userInfoP = self.m_pNodeShowPlayer[index]:getChildByName("Button_userInfo")
    local tableUser = userInfoP:getChildByName("Node_winAnim")
    local userWin = tableUser:getChildByName("UserWin"..index)
    if not userWin then
        userWin = ccs.Armature:create(Lhdz_Res.ANI_OF_WINNER.FILENAME)
        userWin:getAnimation():playWithIndex(0,-1,-1)
        userWin:getAnimation():setSpeedScale(0.8)
        userWin:setPosition(cc.p(0, 0))
        userWin:setName("UserWin"..index)
        userWin:setVisible(false)
        tableUser:addChild(userWin, 0)
    end
    userWin:setVisible(true)
end

-- 隐藏上桌玩家获胜动画
function LhdzMainLayer:hideTableUserWinAnim()
    --神算子和富豪的特效
    self.m_luckyWinEffect:getAnimation():play(Lhdz_Res.ANI_OF_TOPWINEFFECT.ANILIST.LUCKYNORMAL.NAME, -1, 0)
    self.m_richWinEffect:getAnimation():play(Lhdz_Res.ANI_OF_TOPWINEFFECT.ANILIST.RICHNORMAL.NAME, -1, 0)

    for i = 1, Lhdz_Const.TABEL_USER_COUNT do
        local userInfoP = self.m_pNodeShowPlayer[i]:getChildByName("Button_userInfo")
        local tableUser = userInfoP:getChildByName("Node_winAnim")
        local userWin = tableUser:getChildByName("UserWin"..i)
        if userWin then
            userWin:setVisible(false)
        end
    end
end

-- 获胜区域动画
function LhdzMainLayer:showWinAreaAnim()
    local areaType = LhdzDataMgr.getInstance():getAreaType()
    if not self.m_pSpWinArea[areaType] then
        return
    end
    self.m_pSpWinArea[areaType]:setVisible(true)
    self.m_pSpWinArea[areaType]:runAction(cc.Repeat:create(cc.Sequence:create(cc.FadeIn:create(0.5), cc.FadeOut:create(0.5)), 5))

    --todo
    local soundPath = Lhdz_Res.vecSound.SOUND_OF_ANIDRAGONWIN
    local strEfc = "Animation1"
    --local strEfc = "Animation2"
    if areaType == 2 then
        strEfc = "Animation3"
        --strEfc = "Animation2"
        soundPath = Lhdz_Res.vecSound.SOUND_OF_ANITIGERWIN
    end
    if areaType == 3 then 
        strEfc = "Animation2"
        soundPath = nil
    end
    if not self._longhuTeXiao then
        local animationEvent = function (armatureBack, movementType, movementID)
            if movementType == ccs.MovementEventType.complete or movementType == ccs.MovementEventType.loopComplete then
                self._longhuTeXiao:setVisible(false)
            end
        end
        self._longhuTeXiao = ccs.Armature:create(Lhdz_Res.ANI_OF_LAOHUTEXIAO.FILENAME)
        self._longhuTeXiao:setPosition(cc.p(962, 371))
        self.m_pNodeArmature:addChild(self._longhuTeXiao)
        self._longhuTeXiao:getAnimation():setMovementEventCallFunc(animationEvent)
        self._longhuTeXiao:getAnimation():setSpeedScale(0.8)
        self._longhuTeXiao:setScale(2)
    end
    if self._longhuTeXiao then
        self._longhuTeXiao:setVisible(true)
        self._longhuTeXiao:getAnimation():play(strEfc)
        if soundPath then
            AudioManager:getInstance():playSound(soundPath)
        end
    end

end

-- 隐藏获胜区域动画
function LhdzMainLayer:hideWinAreaAnim()
    for i = 1, Lhdz_Const.GAME_DOWN_COUNT do
        self.m_pSpWinArea[i]:setVisible(false)
        self.m_pSpWinArea[i]:stopAllActions()
    end
end

-- 倒计时动画
function LhdzMainLayer:showCountDownAnim(val)
    local scaleTo = cc.ScaleTo:create(0.15, 1.4)
    local scaleBack = cc.ScaleTo:create(0.15, 1)
    local seq = cc.Sequence:create(scaleTo, scaleBack)
    self.m_pLbCountTime:runAction(seq)

    local fntstr = Lhdz_Res.FNT_OF_COUNTDOWN
    -- 数字扩张发散动画
    local aniscale = 1.5
    local aniscaletime = 0.3
    local fadetotime = 0.3
    local lb1 = cc.Label:createWithBMFont(fntstr, string.format("%d", val))
    lb1:setPosition(cc.p( self.m_pLbCountTime:getPosition()))
    lb1:setAnchorPoint(cc.p(0.5, 0.5))
    lb1:setOpacity(0)
    lb1:addTo(self.m_pSpCountTime)

    local seq1 = cc.Sequence:create(cc.Spawn:create(cc.FadeTo:create(fadetotime, 70),
                                                    cc.ScaleTo:create(fadetotime, aniscale)),
                                    cc.Spawn:create(cc.FadeOut:create(0.5),
                                                    cc.ScaleTo:create(0.3, 2.5)),
                                    cc.RemoveSelf:create())
    lb1:runAction(seq1)
end

function LhdzMainLayer:clearFlyStatus()
    for i=1,Lhdz_Const.GAME_DOWN_COUNT do
        if not self.flyStar_bagua[i] then
            self.flyStar_bagua[i] = cc.Sprite:createWithSpriteFrameName(Lhdz_Res.PNG_OF_BAGUAFLY)
            self.flyStar_bagua[i]:addTo(self.m_pNodeStar)
        end
        self.flyStar_bagua[i]:setVisible(false)
        self.flyStar_bagua[i]:stopAllActions()
        self.flyStar_bagua[i]:setPosition(Lhdz_Const.FLYSTAR.STAR_BEGINPOS)

        if not self.lightStar_bagua[i] then
            self.lightStar_bagua[i] = cc.Sprite:createWithSpriteFrameName(Lhdz_Res.PNG_OF_BAGUAEFFECT)
            self.lightStar_bagua[i]:addTo(self.m_pNodeStar)
        end
        self.lightStar_bagua[i]:stopAllActions()
        self.lightStar_bagua[i]:setPosition(Lhdz_Const.FLYSTAR.STAR_ENDPOS[i])
        self.lightStar_bagua[i]:setOpacity(0)

        for j = 1, Lhdz_Const.FLYPARTICAL_COUNT do
            self.m_particalvec[i][j]:stopAllActions()
            self.m_particalvec[i][j]:setPosition(Lhdz_Const.FLYSTAR.STAR_BEGINPOS)
            self.m_particalvec[i][j]:setVisible(false)
            self.m_particalvec[i][j]:resetSystem()
        end
    end
end

--神算子飞星
function LhdzMainLayer:playstar(i)
--[[
    1 光效八卦在飞星到位后，从透明度0递增到255同时旋转720度，时间35帧，然后10帧渐隐消失
    2 飞行八卦在飞星到位后，从透明度255递减到0同时旋转720度消失
    3 显示八卦在飞星到位后，从透明度0递增到255同时旋转720度保持显示
]]--

    self.m_pSpStar[i]:setVisible(false)
    for j = 1, Lhdz_Const.FLYPARTICAL_COUNT do
        self.m_particalvec[i][j]:stopAllActions()
        self.m_particalvec[i][j]:setPosition(Lhdz_Const.FLYSTAR.STAR_BEGINPOS)
        self.m_particalvec[i][j]:setVisible(true)
        self.m_particalvec[i][j]:resetSystem()
    end

    --飞行八卦
    self.flyStar_bagua[i]:setVisible(true)
    self.flyStar_bagua[i]:setOpacity(255)
    self.flyStar_bagua[i]:setPosition(Lhdz_Const.FLYSTAR.STAR_BEGINPOS)
    self.flyStar_bagua[i]:setLocalZOrder(101)
    --self.flyStar_bagua:addTo(self.m_pNodeStar)

    local func = cc.CallFunc:create(function()
        --光效
        self.lightStar_bagua[i]:setBlendFunc(gl.ONE, gl.ONE) 
        self.lightStar_bagua[i]:setPosition(Lhdz_Const.FLYSTAR.STAR_ENDPOS[i])
        self.lightStar_bagua[i]:setOpacity(0)
        self.lightStar_bagua[i]:setLocalZOrder(102)
        --self.lightStar_bagua[i]:addTo(self.m_pNodeStar)
        self.lightStar_bagua[i]:runAction(cc.Sequence:create(
                                cc.EaseQuadraticActionIn:create(cc.Spawn:create(cc.RotateBy:create(35/60, 720), cc.FadeTo:create(35/60, 255))),
                                cc.FadeTo:create(10/60, 0)
                                --cc.RemoveSelf:create()
                                ))


        --显示八卦
        self.m_pSpStar[i]:setVisible(true)
        self.m_pSpStar[i]:setRotation(0)
        self.m_pSpStar[i]:setOpacity(0)
        self.m_pSpStar[i]:setPosition(Lhdz_Const.FLYSTAR.STAR_ENDPOS[i])
        self.m_pSpStar[i]:runAction(cc.EaseQuadraticActionIn:create(cc.Spawn:create(cc.RotateBy:create(35/60, 720), cc.FadeTo:create(35/60, 255))))
    end)

    local bezier = {
        Lhdz_Const.FLYSTAR.BEZIERPOS[i][1],
        Lhdz_Const.FLYSTAR.BEZIERPOS[i][2],
        Lhdz_Const.FLYSTAR.STAR_ENDPOS[i],
    }

    self.flyStar_bagua[i]:runAction(cc.Sequence:create(
                         cc.BezierTo:create(Lhdz_Const.FLYSTAR.FLY_TIME[i], bezier),
                         func,
                         cc.EaseQuadraticActionIn:create(cc.Spawn:create(cc.RotateBy:create(35/60, 720), cc.FadeTo:create(35/60, 0)))
                         --cc.RemoveSelf:create()
                         )
                     )

    for j = 1, Lhdz_Const.FLYPARTICAL_COUNT do
        self:doSomethingLater(function()
            self.m_particalvec[i][j]:runAction(
                cc.BezierTo:create(Lhdz_Const.FLYSTAR.FLY_TIME[i], bezier)
            )                
        end, 1/60 * j)
    end
    AudioManager.getInstance():playSound(Lhdz_Res.vecSound.SOUND_OF_STAR)
end

--筹码光效动画
function LhdzMainLayer:initChipEffect()
    self.m_chipEffect = cc.Sprite:createWithSpriteFrameName(Lhdz_Res.PNG_OF_CHIP_EFFECT)
    self.m_chipEffect:setAnchorPoint(0.5, 0.5)
    self.m_chipEffect:addTo(self.m_pNodeJetton)
    self.m_chipEffect:setVisible(false)
    self.m_chipEffect:setBlendFunc(gl.ONE, gl.ONE) 
    self.m_chipEffect:runAction(
        cc.RepeatForever:create(
            cc.Sequence:create(
                cc.FadeTo:create(0.5, 100),
                cc.FadeTo:create(0.5, 255)
            )
        )
    )
end

--头像获取筹码动画
function LhdzMainLayer:initGetChipEffect()
    local strJson = string.format(Lhdz_Res.ANI_OF_GETCHIP2, "json")
    local strAtlas = string.format(Lhdz_Res.ANI_OF_GETCHIP2, "atlas")
    for i = 1, 6 do
        local userInfoP = self.m_pNodeShowPlayer[i]:getChildByName("Button_userInfo")
        local node = userInfoP:getChildByName("Node_winAnim")
        self.m_getChipEffect[i] = sp.SkeletonAnimation:createWithJsonFile(strJson, strAtlas)
        self.m_getChipEffect[i]:setScale(0.7)
        self.m_getChipEffect[i]:setPositionY(-14)
        self.m_getChipEffect[i]:setVisible(false)
        self.m_getChipEffect[i]:addTo(node)
    end
end

--初始化大富豪和神算子获胜特效
function LhdzMainLayer:initTopWinEffect()
    --神算子
    self.m_luckyWinEffect = ParticalPool.getInstance():getLuckyWinEffect()
    self.m_luckyWinEffect:setPosition(cc.p(58, 111.5))
    self.m_luckyWinEffect:addTo(self.m_pNodeShowPlayer[1]:getChildByName("Button_userInfo"))

    --大富豪
    self.m_richWinEffect = ParticalPool.getInstance():getRichWinEffect()
    self.m_richWinEffect:setPosition(cc.p(59.5, 110))
    self.m_richWinEffect:addTo(self.m_pNodeShowPlayer[2]:getChildByName("Button_userInfo"))
end

--初始化获胜区域动画
function LhdzMainLayer:initAreaWinEffect()
    self.m_areaWinEffect[2] = ccs.Armature:create(Lhdz_Res.ANI_OF_WINHU.FILENAME)
    self.m_areaWinEffect[2]:setPosition(cc.p(971.81, 248.20))
    self.m_areaWinEffect[2]:setVisible(false)
    self.m_pNodeArmature:addChild(self.m_areaWinEffect[2], 0)

    local animationEvent = function(armatureBack, movementType, movementID)
        if movementType == ccs.MovementEventType.complete then
            self.m_areaWinEffect[2]:setVisible(false)
        end
    end
    self.m_areaWinEffect[2]:getAnimation():setMovementEventCallFunc(animationEvent)

end

---------------------------------- play animation end ----------------------------------

---------------------------------- help func start ----------------------------------
-- 获取玩家的位置
function LhdzMainLayer:getUserPosition(wChairID, isBet, wJettonIndex)
    local userIndex = LhdzDataMgr.getInstance():getUserByChirId(wChairID)
    local pos = cc.p(0, 0)
    if userIndex == 0 then
        -- 玩家自己
        if isBet then
            pos.x = self.m_pBtnJetton[wJettonIndex]:getPositionX()
            pos.y = self.m_pBtnJetton[wJettonIndex]:getPositionY()
        else
            pos.x = 52
            pos.y = 24
        end
    elseif userIndex == -1 then
        -- 其他玩家
        pos.x = 47
        pos.y = 120
    else
        -- 上桌玩家
        pos.x = self.m_pNodeTablePlayer[userIndex]:getPositionX()
        pos.y = self.m_pNodeTablePlayer[userIndex]:getPositionY()
    end
    return pos
end

function LhdzMainLayer:replaceArmatureSprite(armature, card)
    local value, color = LhdzDataMgr.getInstance():getCardValueAndColor(card)
    local pBone = armature:getBone("jiedian")
        if pBone then
            local pNode = display.newNode()
            local pSpValue1 = cc.Sprite:createWithSpriteFrameName(string.format(Lhdz_Res.PNG_OF_CARD_VALUE, value, color%2))
            pSpValue1:setPosition(cc.p(-24, 33))
            pNode:addChild(pSpValue1)
            local pSpColor1 = cc.Sprite:createWithSpriteFrameName(string.format(Lhdz_Res.PNG_OF_CARD_COLOR, color))
            pSpColor1:setPosition(cc.p(-24, 4))
            pNode:addChild(pSpColor1)
            local pSpColor2 = cc.Sprite:createWithSpriteFrameName(string.format(Lhdz_Res.PNG_OF_CARD_BIGCOLOR, color))
            pSpColor2:setPosition(cc.p(18, -30))
            pNode:addChild(pSpColor2)
            pBone:addDisplay(pNode, 0)
            pBone:changeDisplayWithIndex(0, true)
        end
end

--切换用户头像
function LhdzMainLayer:replaceUserHeadSprite(armature, cfg, faceId, frameId)
    if not cfg then
        return
    end

    local pBone = armature:getBone(cfg.NODES.HEAD)
    if pBone then
        local pNode = display.newNode()
        local head = ToolKit:getHead(faceId)
        local pSpValue1 = cc.Sprite:createWithSpriteFrameName(head)

        pNode:addChild(pSpValue1)
        --增加vip等级对应的头像框
       -- local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", frameId)
--        local pSpValue2 = cc.Sprite:createWithSpriteFrameName(framePath)
--        pSpValue2:setScale(1.2)
--        pSpValue2:setAnchorPoint(cc.p(0.5, 0.5))
--        pSpValue2:setPosition(cc.p(0, 0))
--        pNode:addChild(pSpValue2)

        pBone:addDisplay(pNode, 0)
        pBone:changeDisplayWithIndex(0, true)
    end
end

-- 根据用户获取头像框索引(其他用户根据vip等级来，自己根据自己的选择来)
function LhdzMainLayer:getHeadFrameIndex(wUserID)
    local headFrame = CUserManager.getInstance():getHeadFrameByUserID(wUserID)
    if headFrame > 10 then return 10 end
    if headFrame <  0 then return  0 end
    return headFrame
end

function LhdzMainLayer:getVipByUserID(userID)
    
    local vipLevel = CUserManager.getInstance():getVipLvByUserID(userID)

    return vipLevel > VIP_LEVEL_MAX and VIP_LEVEL_MAX or vipLevel
end

-- 请勿频繁点击
function LhdzMainLayer:isDelayHandleEvent()

    --//2秒钟只能点击一次
    local tmNow = os.time()
    local defaultCDtime = 1.5

    if (tmNow - self.m_tmLastClicked <= defaultCDtime)then
        return true;
    end

    self.m_tmLastClicked = tmNow; 
    return false;
end

function LhdzMainLayer:shedule(node , callback ,  time)
    node:runAction(cc.RepeatForever:create(cc.Sequence:create(cc.DelayTime:create(time),cc.CallFunc:create(
        callback
     ))))
end

---------------------------------- help func end ----------------------------------

---------------------------------- resource start ----------------------------------
--延时播放音效
function LhdzMainLayer:doSoundPlayLater(strSound, delay)
    if delay <= 0 then
        AudioManager:getInstance():playSound(strSound)
    else
        self:doSomethingLater(function()
            AudioManager:getInstance():playSound(strSound)
        end, delay)
    end
end

function LhdzMainLayer:doSomethingLater(call, delay, ...)
    self.m_rootUI:runAction(cc.Sequence:create(cc.DelayTime:create(delay), cc.CallFunc:create(call, {...})))
end
---------------------------------- resource end ----------------------------------

--region 动画

--上桌玩家投注时头像抖动动画
function LhdzMainLayer:playTableUserChipAni(idx)
    --[[
        桌子玩家位置索引
        2 1
        3 5
        4 6
        1是神算子 2-6富豪榜1-5名
    ]]--
    local offsetX = { -30, 30, 30, 30, -30, -30 }
    local ts = 0.1

    self.m_userBtnVec[idx]:stopAllActions()
    self.m_userBtnVec[idx]:setPosition(self.m_userBtnResPosVec[idx])

    local moveTo = cc.MoveTo:create(ts, cc.p(self.m_userBtnResPosVec[idx].x + offsetX[idx], self.m_userBtnResPosVec[idx].y))
    local moveBack = cc.MoveTo:create(ts, self.m_userBtnResPosVec[idx])
    local seq = cc.Sequence:create(moveTo, moveBack)

    self.m_userBtnVec[idx]:runAction(seq)
end

--endregion

--region 辅助方法

function LhdzMainLayer:showRecharge()
--    local pMessageBox = MessageBoxNew.create(Lhdz_Res.MESSAGEBOX_IMG.RECHARGE, MsgBoxPreBiz.PreDefineallBack.CB_RECHARGE)
--    self:addChild(pMessageBox, 1000)
--    pMessageBox:setPositionY(pMessageBox:getPositionY()-((display.height - 750) / 2))
    self:showMessageBox("go-recharge")
end

--获取弹分的宽度
function LhdzMainLayer:getScoreWidth(score)
    if 0 == score then
        return 0
    end

    local retLen = Lhdz_Const.SCOREWIDTH.PRE + 34
    local v1, v2 = math.modf(score / 100)
    if v2 > 0 then
        retLen = retLen + Lhdz_Const.SCOREWIDTH.DOT + 2*Lhdz_Const.SCOREWIDTH.NUM
    end

    retLen = retLen + #tostring(v1) * Lhdz_Const.SCOREWIDTH.NUM
    return retLen
end

--根据上桌玩家索引和数值显示宽度获取显示锚点X和坐标X
function LhdzMainLayer:getScoreViewPos(index, scoreWidth)
    if LuaUtils.isIphoneXDesignResolution() then
        return 0.5, 0
    end

    local isLeft = false
    if 2 == index or 3 == index or 4 == index then
        isLeft = true
    end

    local retAnchor = 0.5
    local retX = 0
    if scoreWidth > Lhdz_Const.SCOREWIDTH.MAXWIDTH then
        retAnchor = isLeft and 0 or 1
        retX = isLeft and -78 or 75
    end

    return retAnchor, retX
end

--endregion

--飞星星贝塞尔测试按钮
function LhdzMainLayer:addTestBtn()
    self.m_p1 = cc.p(1000, 590)
    self.m_p2 = cc.p(510, 487)
    local btnp1 = ccui.Button:create()
    btnp1:setTitleText("测试点1")
    btnp1:setTitleFontSize(30)
    btnp1:setContentSize(cc.size(150, 40))
    btnp1:setColor(cc.c3b(241,14,99))
    local labelp1 = cc.Label:createWithSystemFont("", "", 24)
    labelp1:setPosition(cc.p(0, 40))
    labelp1:setString("测试点1")
    labelp1:setColor(cc.c3b(241,14,99))
    btnp1:addChild(labelp1)
    btnp1:setPosition(cc.p(1000, 590))
    self.m_pNodeStar:addChild(btnp1)

    btnp1:addTouchEventListener(function (sender,eventType)
        if eventType == ccui.TouchEventType.moved then
            local posMove = sender:getTouchMovePosition()
            btnp1:setPositionX(posMove.x - 145)
            btnp1:setPositionY(posMove.y)
            labelp1:setString( string.format("x:%d, y:%d", posMove.x - 145, posMove.y) )
            self.m_p1 = cc.p(posMove.x - 145, posMove.y)
        end
    end)

    local btnp2 = ccui.Button:create()
    btnp2:setTitleText("测试点2")
    btnp2:setTitleFontSize(30)
    btnp2:setColor(cc.c3b(241,14,99))
    btnp2:setContentSize(cc.size(150, 40))
    local labelp2 = cc.Label:createWithSystemFont("", "", 24)
    labelp2:setPosition(cc.p(0, 40))
    labelp2:setString("测试点2")
    labelp2:setColor(cc.c3b(241,14,99))
    btnp2:addChild(labelp2)
    btnp2:setPosition(cc.p(510, 487))
    self.m_pNodeStar:addChild(btnp2)

    btnp2:addTouchEventListener(function (sender,eventType)
        if eventType == ccui.TouchEventType.moved then
            local posMove = sender:getTouchMovePosition()
            btnp2:setPositionX(posMove.x - 145)
            btnp2:setPositionY(posMove.y)
            labelp2:setString( string.format("x:%d, y:%d", posMove.x - 145, posMove.y) )
            self.m_p2 = cc.p(posMove.x - 145, posMove.y)
        end
    end)
end

function LhdzMainLayer:dealPlayerCoin(coin_num)
    return LuaUtils.getFormatGoldAndNumberAndZi(math.abs(coin_num))
end

-- 更换庄家提示
function LhdzMainLayer:showBankerTips(isUpBanker)
    local strTips = "";
    local cur_obj = nil
    local bSystemBanker = LhdzDataMgr.getInstance():isSystemBanker()

    if not self.m_pBankerTipsBg then
        self.m_pBankerTipsBg = cc.Sprite:createWithSpriteFrameName(Lhdz_Res.PNG_OF_BANKER_BG)
        self.m_pBankerTipsBg:hide()
        self.m_pNodeTips:addChild(self.m_pBankerTipsBg)
    end

    if (isUpBanker)then
    --// 庄家轮换
        strTips = Lhdz_Res.PNG_OF_BANKER
        if not self.m_pSpBankerTips then
            self.m_pSpBankerTips = cc.Sprite:createWithSpriteFrameName(strTips);
            self.m_pSpBankerTips:move(self.m_pBankerTipsBg:getContentSize().width/2,self.m_pBankerTipsBg:getContentSize().height/2)
            self.m_pBankerTipsBg:addChild(self.m_pSpBankerTips);
        else
            self.m_pSpBankerTips:setSpriteFrame(strTips);
        end
        self.m_pSpBankerTips:setVisible(true)
        if self.m_pLbBankerTips then
            self.m_pLbBankerTips:setVisible(false)
        end
    else
    --// 玩家庄家 连庄
        local times = LhdzDataMgr.getInstance():getBankerTimes();
        if(times <= 1 or times >10)then
            return;
        end

        if bSystemBanker then
            return 
        end
    
        strTips = string.format("连庄%d局",times)
        if not self.m_pLbBankerTips then
            self.m_pLbBankerTips = cc.Label:createWithBMFont("game/longhudazhan/font/lhd_lz_num.fnt",strTips)
            self.m_pLbBankerTips:move(self.m_pBankerTipsBg:getContentSize().width/2,self.m_pBankerTipsBg:getContentSize().height/2)
            self.m_pBankerTipsBg:addChild(self.m_pLbBankerTips)
        else
            self.m_pLbBankerTips:setString(strTips)
        end
        if self.m_pSpBankerTips then
            self.m_pSpBankerTips:setVisible(false)
        end
        self.m_pLbBankerTips:setVisible(true)
    end

    self.m_pNodeTips:setVisible(false);
    self.m_pNodeTips:setOpacity(255)
    self.m_pNodeTips:stopAllActions()
    local callback1 = cc.CallFunc:create(function ()
        self.m_pNodeTips:setVisible(true);
        self.m_pBankerTipsBg:show()
    end)

    local callback = cc.CallFunc:create(function ()
        self.m_pNodeTips:setVisible(false)
        self.m_pBankerTipsBg:hide()
    end)
    local seq = cc.Sequence:create(cc.DelayTime:create(0.5), callback1, cc.DelayTime:create(1.0), cc.FadeOut:create(0.2), callback);
    self.m_pNodeTips:runAction(seq);
end

function LhdzMainLayer:event_update_chat(msg)
    if self.m_pMsgArmature then
        self.m_pMsgArmature:show()
    end
    self.m_pSpChat:setOpacity(255)
    
end

return LhdzMainLayer
--endregion
