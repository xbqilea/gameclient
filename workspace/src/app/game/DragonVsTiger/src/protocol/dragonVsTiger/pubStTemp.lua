module(..., package.seeall)

PstDvtHistoryInfo = 
{
	{1,	1,	'm_luckyArea', 	   'UBYTE'				, 1			, '赢家区域 1-和 2-龙 3-虎'},
	{2,	1,	'm_cards',    	   'UBYTE'				, 2			, '第一张牌为龙点数, 第二张牌为虎点数'},
}

PstDvtPlayerInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2, 	1, 'm_index'			, 'UINT'				, 1		, '索引 1-5 显示金币做多玩家 其他为0' },
	{ 3, 	1, 'm_faceId'			, 'UINT'				, 1 	, '头像ID' },
	{ 4, 	1, 'm_nickname'			, 'STRING'				, 1 	, '昵称' },
	{ 5, 	1, 'm_score'			, 'UINT'				, 1 	, '金币' },
	{ 6, 	1, 'm_areaBet'			, 'UINT'				, 3 	, '下个区域各下注多少 第一个是和, 第二个是龙, 第三个是虎' },
}

PstDvtOnlinePlayerInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2, 	1, 'm_faceId'			, 'UINT'				, 1 	, '头像ID' },
	{ 3, 	1, 'm_nickname'			, 'STRING'				, 1 	, '昵称' },
	{ 4, 	1, 'm_score'			, 'UINT'				, 1 	, '金币' },
	{ 5,    1, 'm_recentBet'    	, 'UINT'               	, 1     , '最近下注金币数'},
	{ 6,    1, 'm_recentWin'    	, 'UINT'               	, 1     , '最近胜数'},
	{ 7,    1, 'm_recentPlay'    	, 'UINT'               	, 1     , '最近游戏胜数'},
}

PstDvtBalanceInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2,	1, 'm_netProfit'		, 'INT'					, 1		, '本轮净盈利'},
	--{ 3,	1, 'm_betId'			, 'UBYTE'				, 1		, '涓鍖哄煙ID 涓嶇敤鍖哄垎'},
	{ 3,	1, 'm_curScore'			, 'UINT'				, 1		, '当前金币'},
}

PstDvtBetSumInfo =
{
	{ 1,	1, 'm_betId'			 , 'UBYTE'				, 1		, '下注区域 1-和 2-龙 3-虎'},
	{ 2,	1, 'm_betValue'			 , 'INT'				, 1		, '下注额'},
}

PstDvtDayWeekRankInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2, 	1, 'm_faceId'			, 'UINT'				, 1 	, '头像ID' },
	{ 3, 	1, 'm_nickname'			, 'STRING'				, 1 	, '昵称' },
	{ 4,    1, 'm_sex'              , 'UINT'                , 1     , '1男0女'},
	{ 5,    1, 'm_winGold'          , 'UINT'                , 1     , '赢取金币数量'},
	{ 6,    1, 'm_rank'          	, 'UINT'                , 1     , '名次'},
}

PstDvtLuDan =
{
	{ 1		, 1		, 'm_num'			, 'UINT'				, 1    , '数量'},
	{ 2		, 2		, 'm_color'			, 'UBYTE'				, 1	   , '有无规则 1-表示有规则 0-表示无规则'},
}

PstDvtBet =
{
	{ 1,	1, 'm_betId'			 , 'UBYTE'						, 1		, '下注区域ID'},
	{ 2,	1, 'm_betValue'			 , 'UINT'						, 1		, '下注额'},
}

PstDvtOneBet =
{
	{ 1,	1, 'm_accountId'		 , 'UINT'						, 1		, '当前谁下注'},
	{ 2,	1, 'm_coin'		 		 , 'UINT'						, 1		, '下注者金币'},
	{ 3,	1, 'm_bets'				 , 'PstDvtBet'					, 100	, '下注区域ID'},
	{ 4,	1, 'm_isSSz'		 	 , 'UBYTE'						, 1		, '是否神算子 1-是 0-否'},
}