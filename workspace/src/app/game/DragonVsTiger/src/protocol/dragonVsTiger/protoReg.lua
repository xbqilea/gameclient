if not LUA_VERSION or LUA_VERSION ~= "5.3" then
	 module(..., package.seeall)
end
require(g_protocolPath.."dragonVsTiger/protoID")

-- 公共结构定义文件注册
netLoaderCfg_Templates_common = {
	g_protocolPath.."dragonVsTiger/pubStTemp",
}

-- 协议定义文件注册
netLoaderCfg_Templates	=	{	
	g_protocolPath.."dragonVsTiger/dragonVsTiger",
}


-------------------------注册协议-----------------------------
-- 公共结构协议注册
netLoaderCfg_Regs_common = 
{
	PstDvtHistoryInfo 			= 	PSTID_DVTHISTORYINFO,
	PstDvtPlayerInfo 			= 	PSTID_DVTPLAYERINFO,
	PstDvtOnlinePlayerInfo		=	PSTID_DVTONLINEPLAYERINFO,
	PstDvtBalanceInfo			=	PSTID_DVTBALANCEINFO,
	PstDvtBetSumInfo			=	PSTID_DVTBETSUMINFO,
	PstDvtDayWeekRankInfo		=	PSTID_DVTDAYWEEKRANKINFO,
	PstDvtLuDan					=	PSTID_DVTLUDAN,
	PstDvtBet					=	PSTID_DVTBET,
	PstDvtOneBet				=	PSTID_DVTONEBET,
}

-- 协议注册
netLoaderCfg_Regs	=	
{
	CS_C2M_DragonVsTiger_Get_Req				=	CS_C2M_DRAGONVSTIGER_GET_REQ,
	CS_M2C_DragonVsTiger_GameState_Nty			=	CS_M2C_DRAGONVSTIGER_GAMESTATE_NTY,
	CS_C2M_DragonVsTiger_Bet_Req				=	CS_C2M_DRAGONVSTIGER_BET_REQ,
	CS_M2C_DragonVsTiger_Bet_Ack				=	CS_M2C_DRAGONVSTIGER_BET_ACK,
	CS_M2C_DragonVsTiger_EachUserBet_Nty		=	CS_M2C_DRAGONVSTIGER_EACHUSERBET_NTY,
	CS_M2C_DragonVsTiger_Bet_Nty				=	CS_M2C_DRAGONVSTIGER_BET_NTY,
	CS_M2C_DragonVsTiger_Bet_Sum_Nty			=	CS_M2C_DRAGONVSTIGER_BET_SUM_NTY,
	CS_M2C_DragonVsTiger_History_Nty			=	CS_M2C_DRAGONVSTIGER_HISTORY_NTY,
	CS_M2C_DragonVsTiger_GameBalance_Nty		=	CS_M2C_DRAGONVSTIGER_GAMEBALANCE_NTY,
	--CS_C2M_DragonVsTiger_Rank_Req				=	CS_C2M_DRAGONVSTIGER_RANK_REQ,
	--CS_M2C_DragonVsTiger_Rank_Ack				=	CS_M2C_DRAGONVSTIGER_RANK_ACK,
	CS_C2M_DragonVsTiger_PlayerOnlineList_Req	=	CS_C2M_DRAGONVSTIGER_PLAYERONLINELIST_REQ,
	CS_M2C_DragonVsTiger_PlayerOnlineList_Ack	=	CS_M2C_DRAGONVSTIGER_PLAYERONLINELIST_ACK,
	CS_M2C_DragonVsTiger_PlayerShow_Nty			=	CS_M2C_DRAGONVSTIGER_PLAYERSHOW_NTY,
	CS_M2C_DragonVsTiger_PlayerInfo_Nty			=	CS_M2C_DRAGONVSTIGER_PLAYERINFO_NTY,
	CS_C2M_DragonVsTiger_ForceExit_Req			=	CS_C2M_DRAGONVSTIGER_FORCEEXIT_REQ,
	CS_M2C_DragonVsTiger_ForceExit_Ack			=	CS_M2C_DRAGONVSTIGER_FORCEEXIT_ACK,
	CS_M2C_DragonVsTiger_Exit_Nty				=	CS_M2C_DRAGONVSTIGER_EXIT_NTY,
	CS_C2M_DragonVsTiger_Background_Req			=	CS_C2M_DRAGONVSTIGER_BACKGROUND_REQ,
	CS_M2C_DragonVsTiger_Background_Ack			=	CS_M2C_DRAGONVSTIGER_BACKGROUND_ACK,
	CS_C2M_DragonVsTiger_Continue_Req			=	CS_C2M_DRAGONVSTIGER_CONTINUE_REQ,
	CS_M2C_DragonVsTiger_Continue_Ack			=	CS_M2C_DRAGONVSTIGER_CONTINUE_ACK,
	CS_M2C_DragonVsTiger_WaitNext_Nty			=	CS_M2C_DRAGONVSTIGER_WAITNEXT_NTY,
	CS_M2C_DragonVsTiger_UpdatePlayerNum_Nty	=	CS_M2C_DRAGONVSTIGER_UPDATEPLAYERNUM_NTY,
	CS_M2C_DragonVsTiger_StartBet_Nty			=	CS_M2C_DRAGONVSTIGER_STARTBET_NTY,
}
--return netLoaderCfg_Regs

if LUA_VERSION  and LUA_VERSION == "5.3" then
	return netLoaderCfg_Regs
end

