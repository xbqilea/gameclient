module(..., package.seeall)


-- 玩家收到EnterScene后，客户端请求信息
--上线需要发送信息
--[[
CS_G2C_DragonVsTiger_GameState_Nty	 		游戏状态
CS_G2C_DragonVsTiger_Bet_Nty  				自己的下注汇总信息
CS_M2C_DragonVsTiger_Bet_Sum_Nty 			所有区域的下注汇总信息
CS_G2C_DragonVsTiger_BallHistory_Nty 		历史结算信息
--]]
CS_C2M_DragonVsTiger_Get_Req =
{
	
}

-- 游戏状态通知，不同状态表现不同， 广播或者上线后下发
CS_M2C_DragonVsTiger_GameState_Nty = 
{
	{ 1,	1, 'm_roundId'			 ,  'UINT'						, 1		, '轮次ID'},
	{ 2,	1, 'm_state'		 	 ,  'UINT'						, 1		, '状态 1-预备阶段 2-下注阶段 3-结算阶段'},
	{ 3,	1, 'm_confTime'			 ,  'UINT'						, 1		, '本阶段预留时间置 单位s'},
	{ 4,	1, 'm_leftTime'		 	 ,  'UINT'						, 1		, '本阶段剩余时间 单位s'},
	{ 5,	1, 'm_playerCount'       ,  'UINT'       				, 1     , '玩家人数' },
	{ 6,	1, 'm_dragonCard'		 ,  'UBYTE'						, 1		, '龙牌面值, 在比牌和结算时有效， 其他阶段为0'},
	{ 7,	1, 'm_tigerCard'		 ,  'UBYTE'						, 1		, '虎牌面值, 在比牌和结算时有效， 其他阶段为0'},
	{ 8,	1, 'm_userBetLimit'		 ,  'UINT'						, 1		, '玩家最大下注限制'},
	{ 9,	1, 'm_winType'		 	 ,  'UINT'						, 1		, '输赢类型, 1:和, 2:龙, 3：虎'},
	{ 10,	1, 'm_vecBetList'		 , 'PstDvtBetSumInfo'			, 3		, '玩家自己下注信息'},
	{ 11,	1, 'm_vecBetSumList'	 , 'PstDvtBetSumInfo'			, 4		, '所有下注汇总信息'},
	{ 12,	1, 'm_mostWinBetArea' 	 ,  'UBYTE'						, 3		, '神算子下注区域'},
	{ 13, 	1, 'm_mostCoinplayerList'	, 'PstDvtPlayerInfo'  		, 5		, '最多5个玩家, 金币最多的5个玩家' },
	{ 14, 	1, 'm_mostWinRatePlayer'	, 'PstDvtPlayerInfo'  		, 1		, '1个玩家，胜率最高的玩家, 神算子' },
	{ 15, 	1, 'm_rate'		 			, 'UINT'  					, 1		, '神算子胜率, 50% 这里下发的是50' },
	{ 16,	1, 'm_vecBallHistory'	 ,  'PstDvtHistoryInfo'			, 20	, '游戏历史中奖区域信息, 最近20局'},
	{ 17, 	1, 'm_self'		 		 , 'PstDvtPlayerInfo'  			, 1		, '自己的信息' },
	{ 18, 	1, 'm_minScore'		 	 , 'UINT'  						, 1		, '游戏最低金币限制' },
	{ 19, 	1, 'm_canContinueBet'	 , 'UBYTE'  					, 1		, '是否能够续压 1:是 0:否' },
	{ 20,	1, 'm_recordId'       	 ,  'STRING'       				, 1     , '牌局编号' },
}

--下注
CS_C2M_DragonVsTiger_Bet_Req = 
{
	{ 1,	1, 'm_betId'			 , 'UBYTE'						, 1		, '下注区域ID 1-和 2-龙 3-虎'},
	{ 2,	1, 'm_betValue'			 , 'UINT'						, 1		, '下注额'},
}

--下注结果，自己下注各区域加减走这个协议
CS_M2C_DragonVsTiger_Bet_Ack = 
{
	{ 1,	1, 'm_betId'			 , 'UBYTE'						, 1		, '下注区域ID'},
	{ 2,	1, 'm_betValue'			 , 'UINT'						, 1		, '下注额'}, 
	{ 3,	1, 'm_ret'			 	 , 'INT'						, 1		, '下注结果(0：成功 其他参考错误码表;)'},
}

--续压
CS_C2M_DragonVsTiger_Continue_Req = 
{

}

CS_M2C_DragonVsTiger_Continue_Ack = 
{
	{ 1,	1, 'm_ret'			 	 , 'INT'						, 1		, '下注结果(0：成功 其他参考错误码表;)'},
}

--下注飞筹码效果, 各区域加金币走这个协议
CS_M2C_DragonVsTiger_EachUserBet_Nty = 
{
	{ 1,	1, 'm_playerBets'			, 'PstDvtOneBet'			, 100		, '玩家下注区域ID'},
} 
 
 
--下发结算结果, 全部以追加的方式
CS_M2C_DragonVsTiger_GameBalance_Nty = 
{
	{ 1,	1, 'm_roundId'			 ,  'UINT'						, 1		, '轮次ID'},
	{ 2,	1, 'm_confTime'		 	 ,  'UINT'						, 1		, '状态时间'},
	{ 3,	1, 'm_leftTime'		 	 ,  'UINT'						, 1		, '剩余时间'},
	{ 4,	1, 'm_vecBalance'		 ,  'PstDvtBalanceInfo'			, 1024	, '结算信息， 这里面包含玩家信息做飞结算的效果'},
	{ 5,	1, 'm_lastWin'		 	 ,  'PstDvtHistoryInfo'			, 1		, '最新一局信息'}, 
}
 

-- 在线玩家列表
CS_C2M_DragonVsTiger_PlayerOnlineList_Req =
{
	{ 1,	1, 'm_startIndex'        , 'UINT'       				, 1     , '开始索引(从1开始)' },
	{ 2,	1, 'm_endIndex'          , 'UINT'       				, 1     , '结束索引' },
}

CS_M2C_DragonVsTiger_PlayerOnlineList_Ack = 
{
	{ 1,	1, 'm_startIndex'        , 'UINT'       				, 1     , '开始索引(从1开始)' },
	{ 2,	1, 'm_endIndex'          , 'UINT'       				, 1     , '结束索引' },
	--{ 3,	1, 'm_maxPlayerCount'    , 'UINT'       				, 1     , '玩家总数' },
	{ 3, 	1, 'm_playerInfo'		 , 'PstDvtOnlinePlayerInfo'  	, 64	, '在线玩家信息， 按照最近20局下注数排序，这个再发最多64个玩家，多的不发送' },	
}

-- 主界面玩家信息，分布在两侧的， 每局结束时如果有变化刷新一遍
CS_M2C_DragonVsTiger_PlayerShow_Nty = 
{
	{ 1, 	1, 'm_mostCoinplayerList'		 , 'PstDvtPlayerInfo'  			, 5		, '最多5个玩家, 金币最多的5个玩家' },
	{ 2, 	1, 'm_mostWinRatePlayer'		 , 'PstDvtPlayerInfo'  			, 1		, '1个玩家，胜率最高的玩家' },
	{ 3, 	1, 'm_rate'		 				 , 'UINT'  						, 1		, '神算子胜率, 50% 这里下发的是50' },
}
 
CS_C2M_DragonVsTiger_ForceExit_Req =
{
	
}

CS_M2C_DragonVsTiger_ForceExit_Ack =
{
	{ 1,	1, 'm_ret'			 , 'INT'						, 1		, '退出结果'},
}

CS_M2C_DragonVsTiger_Exit_Nty =
{
	{ 1,	1, 'm_type'			 , 'INT'						, 1		, '1-房间维护结算退出 2-你已经被系统踢出房间,请稍后重试'},
}

CS_C2M_DragonVsTiger_Background_Req =
{
	{ 1,	1, 'm_type'			 , 'INT'						, 1		, '1-切到后台 2-切回游戏'},
}

CS_M2C_DragonVsTiger_Background_Ack =
{
	{ 1,	1, 'm_type'			 , 'INT'						, 1		, '1-切到后台 2-切回游戏'},
	{ 2,	1, 'm_ret'			 , 'INT'						, 1		, '退出结果 0-表示成功'},
}

CS_M2C_DragonVsTiger_WaitNext_Nty =
{
	{ 1,	1, 'm_roundId'			 ,  'UINT'						, 1		, '轮次ID'},
	{ 2,	1, 'm_confTime'		 	 ,  'UINT'						, 1		, '状态时间'},
	{ 3,	1, 'm_leftTime'		 	 ,  'UINT'						, 1		, '剩余时间'},
}

CS_M2C_DragonVsTiger_StartBet_Nty =
{
	{ 1,	1, 'm_roundId'			 ,  'UINT'						, 1		, '轮次ID'},
	{ 2,	1, 'm_confTime'		 	 ,  'UINT'						, 1		, '状态时间'},
	{ 3,	1, 'm_leftTime'		 	 ,  'UINT'						, 1		, '剩余时间'},
	{ 4,	1, 'm_canContinueBet'    ,  'UBYTE'						, 1		, '是否可以续压'},
	{ 5,	1, 'm_recordId'       	 ,  'STRING'       				, 1     , '牌局编号' },
}

CS_M2C_DragonVsTiger_UpdatePlayerNum_Nty =
{
	{ 1,	1, 'm_playNum'			 ,  'UINT'						, 1		, '玩家人数'},
}