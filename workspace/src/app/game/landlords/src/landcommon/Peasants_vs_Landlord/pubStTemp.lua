module(..., package.seeall)


-----------------------------------------------------------自由房--------------------------------------------------------
--自由房房间大厅椅子上玩家信息
PstChairUser = {
	{1,	1,	'm_accountId',  'UINT', 1, '玩家账户ID'},
	{2,	1,	'm_faceId',     'UINT', 1, '玩家头像ID'},
	{3, 1,  'm_nickname',   'STRING', 1, '玩家昵称'},
}

--房间椅子属性
PstChairAttr = {
	{1,	1,	'm_chairId', 'UINT', 1, '椅子编号'},
	{2,	1,	'm_accountId', 'UINT', 1, '玩家账户ID, 0:椅子空闲, >0:玩家账户ID'},
}
	
-- 自由房房间大厅桌子属性
PstTableAttr = {
	{1  ,	1	,'m_tableId'			, 'UINT'			, 1		, '桌子编号'},
	{2	, 	1	,'m_minScore'			, 'UINT'			, 1 	, '底分'},
	{3	, 	1	,'m_validateType'		, 'USHORT'			, 1 	, '验证类型，0:不需要验证，1:询问桌主验证,2:密码验证'},
	{4  , 	1	,'m_chairArr'			, 'PstChairAttr'	, 4096	, '椅子数组'},
	{5	, 	1	,'m_chairUserArr'		, 'PstChairUser'	, 4096	, '椅子上玩家信息数组'},
}

-- 自由房房间大厅属性
PstRoomAttr = {
	{1,	1,	'm_roomId', 'UINT', 1, '房间编号(自由房填服务器生成的编号，其他填0)'},
	{2, 1, 'm_tableArr', 'PstTableAttr', 4096, '桌子数组'},
}

--自由房顶部房间编号属性
PstRoomIdAttr = {
	{1,	1,	'm_roomId', 'UINT' , 1, '房间编号(自由房填服务器生成的编号，其他填0)'},
	{2,	1,	'm_isFull', 'SHORT', 1, '房间是否爆满。 1： 爆满， 0：空闲'},
}

--自由房大厅内玩家信息属性
PstFreePlayerAttr = {
	{1		, 1		,  'm_accountId'	    , 'UINT'	, 1	, '玩家账户ID'},
	{2		, 1		,  'm_faceId'			, 'UINT'    , 1 , '玩家头像ID'},
	{3		, 1		,  'm_nickname'			, 'STRING'  , 1 , '玩家昵称'},
	{4		, 1		,  'm_goldCoin'			, 'UINT'    , 1 , '金币'},
}

--自由房大厅内进入/退出的玩家
PstFreePlayerInOutAttr = {
	{1		, 1		,  'm_type'	    		, 'SHORT'				, 1	, '类型,0：进入,1：退出'},
	{2		, 1		,  'm_accountId'	    , 'UINT'				, 1	, '玩家账户ID'},
}

--自由房大厅内当天赢取金币排行榜属性
PstFreeTodayRankAttr = {
	{1		, 1		,  'm_nickname'			, 'STRING'  , 1 , '玩家昵称'},
	{2		, 1		,  'm_goldCoin'			, 'UINT'    , 1 , '当天赢取总金币'},
}

--自由房场景通知游戏客户端桌子有人进/出(开始游戏前)
PstFreeBeforeGameChair = {
	{ 1		, 1		, 'm_chairId'			, 'UINT'				, 1 	, '椅子编号'},
	{ 2		, 1		, 'm_accountId'			, 'UINT'				, 1 	, '玩家账户ID'},
}

--自由房场景通知游戏客户端桌子有人进/出(开始游戏前)
PstFreeBeforeGameUser = {
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1 	, '玩家账户ID'},
	{ 2		, 1		, 'm_faceId'			, 'UINT'				, 1 	, '头像ID'},
	{ 3		, 1		, 'm_nickname'			, 'STRING'				, 1 	, '昵称'},
	{ 4		, 1		, 'm_goldCoin'			, 'UINT'				, 1 	, '金币数量'},
	{ 5		, 1		, 'm_level'				, 'UINT'				, 1 	, '等级'},
}

--自由房恶意占位处理单元
PstHoldSeatAttr = {
	{1,	1,	'm_roomId'		  , 'UINT', 1, '房间编号(自由房填服务器生成的编号，其他填0)'},
	{2,	1,	'm_tableId'       , 'UINT', 1, '桌子编号'},
	{3,	1,	'm_accountId'     , 'UINT', 1, '玩家账户ID'},
}


-----------------------------------------------------------斗地主--------------------------------------------------------

-- 场景通知斗地主初始化数据
PstGameDataEx =
{
	{ 1		, 1		, 'm_sPlayerData'				, 'PstGameData'		, 1		, '玩家数据'},
	{ 2		, 1		, 'm_gameAtomTypeId'			, 'UINT'			, 1	   	, '游戏序号'},
	{ 3		, 1		, 'm_isPlatformBalance'			, 'UBYTE'			, 1		, '是否平台结算，0:否，1：是'},
	{ 4		, 1		, 'm_taxPercent'				, 'UINT'			, 1		, '游戏结束后，胜利玩家扣税百分比,如：20即20%(m_isPlatformBalance=1,才生效)'},
	{ 5		, 1		, 'm_score'						, 'INT'				, 1	   	, '积分，比如比赛的初始积分'},
	{ 6		, 1		, 'm_nBaseCent'					, 'UINT'			, 1		, '底分'},
	{ 7		, 1		, 'm_loseTop'					, 'UINT'			, 1		, '输分封顶, 0:不封顶， >0 :封顶值'},
	{ 8		, 1		, 'm_strBusId'					, 'STRING'			, 1		, '玩家业务id(运营日志用到)'},
	{ 9		, 1		, 'm_yyParam'					, 'STRING'			, 1		, '运营参数(比赛id,其他填空)'},
}

PstLandlord_CalcDataEx =
{
	{ 1		, 1		, 'm_nAccount'					, 'UINT'		, 1	, '玩家账户ID'},
	{ 2		, 1		, 'm_nCent'						, 'INT'			, 1	, '积分(正数：表示+多少，负数：表示-多少)'},
}

PstLandlord_LandlordLoseTop =
{
	{ 1		, 1		, 'm_isLoseTop'					, 'UINT'		, 1	, '是否输分封顶, 0:否，1：是'},
	{ 2		, 1		, 'm_loseTop'					, 'UINT'		, 1	, '输分封顶值'},
}

-- 斗地主通知场景玩家积分
PstLandLordCalculateAccountData =
{
	{ 1		, 1		, 'm_nAccount'					, 'UINT'		, 1	, '玩家账户ID'},
	{ 2		, 1		, 'm_nCent'						, 'INT'			, 1	, '积分(正数：表示+多少，负数：表示-多少)'},
	{ 3		, 1		, 'm_nPun'						, 'UINT'		, 1	, '是否托管惩罚 0 没有  1 有'},
	{ 4		, 1		, 'm_calcDataEx'				, 'PstLandlord_CalcDataEx'		, 1024	, '结算数据'},
}

-- 场景通知斗地主初始化数据
PstLandLordInitGameData =
{
	{ 1		, 1		, 'm_nAccountId'				, 'UINT'		, 1		, '玩家账户ID'},
	{ 2		, 1		, 'm_nChairId'					, 'UINT'		, 1		, '椅子编号'},
	{ 3		, 1		, 'm_isPlatformBalance'			, 'UBYTE'		, 1		, '是否平台结算，0:否，1：是'},
	{ 4		, 1		, 'm_taxPercent'				, 'UINT'		, 1		, '游戏结束后，胜利玩家扣税百分比,如：20即20%(m_isPlatformBalance=1,才生效)'},
}

-- 斗地主通知场景初始化结果
PstLandLordInitResult = {
	{ 1		, 1		, 'm_nAccountId'				, 'UINT'		, 1		, '玩家账户ID'},
	{ 2		, 1		, 'm_nResult'					, 'INT'			, 1		, '初始化结果，0:成功，-X:失败'},
}

-- 斗地主游戏结算数据
--[[PstLandLordCalculateChairData =
{
	{ 1		, 1		, 'm_nAccount'					, 'UINT'		, 1	, '玩家账户ID **此项以后会被优化，目前只做调试用'},
	{ 2		, 1		, 'm_nChairId'					, 'UINT'		, 1	, '椅子编号(优先以玩家账户ID为准。如果椅子号为0，使用玩家账户ID处理)'},
	{ 3		, 1		, 'm_nValue'					, 'INT'			, 1	, '结算值(正数：表示+多少，负数：表示-多少)'},
}--]]


---------------------------------------------------------比赛---------------------------------------------------------------------
--比赛场景通知显示游戏客户端(开始游戏前)
PstMatchBeforeGameChair = {
	{ 1		, 1		, 'm_chairId'			, 'UINT'				, 1 	, '椅子编号'},
	{ 2		, 1		, 'm_accountId'			, 'UINT'				, 1 	, '玩家账户ID'},
}

--报名页面已报名玩家信息
PstMatchUser = 
{
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1 	, '玩家账户ID'},
	{ 2		, 1		, 'm_faceId'			, 'UINT'				, 1 	, '头像ID'},
	{ 3		, 1		, 'm_nickname'			, 'STRING'				, 1 	, '昵称'},
}

--比赛场景通知显示游戏客户端(开始游戏前)
PstMatchBeforeGameUser = {
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1 	, '玩家账户ID'},
	{ 2		, 1		, 'm_faceId'			, 'UINT'				, 1 	, '头像ID'},
	{ 3		, 1		, 'm_nickname'			, 'STRING'				, 1 	, '昵称'},
	{ 4		, 1		, 'm_level'				, 'UINT'				, 1 	, '等级'},
	{ 5		, 1		, 'm_gameScore'			, 'INT'					, 1 	, '当前积分(该积分为每轮开始前的初始积分、断线重连的积分，有可能为负数)'},
	{ 6		, 1		, 'm_chairId'			, 'UINT'				, 1 	, '椅子编号'},
}

PstMatchRankReward =
{
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1 	, '玩家账户ID'},
	{ 2		, 1		, 'm_faceId'			, 'UINT'				, 1 	, '头像ID'},
	{ 3		, 1		, 'm_nickname'			, 'STRING'				, 1 	, '昵称'},
	{ 4		, 1		, 'm_rank'				, 'INT'					, 1 	, '名次'},
	{ 5		, 1		, 'm_score'				, 'INT'					, 1 	, '名次积分'},
	{ 6		, 1		, 'm_award'				, 'INT'					, 1 	, '名次对应奖励'},
}

--比赛奖励道具
PstMatchRewardItem = {
	{ 1		, 1		, 'm_itemId'			, 'UINT'				, 1 	, '道具id'},
	{ 2		, 1		, 'm_num'				, 'UINT'				, 1 	, '道具数量'},
}

---------------------------------------------------------系统房-----------------------------------------------------------------------
--系统房场景通知显示游戏客户端(开始游戏前)
PstSysBeforeGameChair = {
	{ 1		, 1		, 'm_chairId'			, 'UINT'				, 1 	, '椅子编号'},
	{ 2		, 1		, 'm_accountId'			, 'UINT'				, 1 	, '玩家账户ID'},
}

--系统房场景通知显示游戏客户端(开始游戏前)
PstSysBeforeGameUser = {
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1 	, '玩家账户ID'},
	{ 2		, 1		, 'm_faceId'			, 'UINT'				, 1 	, '头像ID'},
	{ 3		, 1		, 'm_nickname'			, 'STRING'				, 1 	, '昵称'},
	{ 4		, 1		, 'm_level'				, 'UINT'				, 1 	, '等级'},
	{ 5		, 1		, 'm_goldCoin'			, 'UINT'				, 1 	, '金币'},
}


---------------------------------------------------------牌友房----------------------------------------------------------------

--房间内座位成员数据
PstLandVipRoomChairData = 
{
	{ 1		, 1		, 'm_chairId'			, 'UINT'				, 1 	, '椅子编号'},
	{ 2		, 1		, 'm_accountId'			, 'UINT'				, 1 	, '玩家账户ID'},
	{ 3		, 1		, 'm_faceId'			, 'UINT'				, 1  	, '头像id'},
	{ 4		, 1		, 'm_nickname'			, 'STRING'				, 1  	, '昵称'},
	{ 5		, 1		, 'm_offine'			, 'UINT'				, 1  	, '是否离线 0 没有离线  1 离线'},
	{ 6		, 1		, 'm_score'				, 'INT'					, 1  	, '积分'},
}

-- 房间列表数据
PstLandVipRoomListData = 
{
	{ 1		, 1		, 'm_roomId'			, 'UINT'				, 1 	, '房间内容'},
	{ 2		, 1		, 'm_playerNum'			, 'UINT'				, 1 	, '人数'},
	{ 3		, 1		, 'm_isDouble'			, 'UINT'				, 1 	, '是否加倍 0 1'},
	{ 4		, 1		, 'm_limitBomb'			, 'UINT'				, 1 	, '炸弹限制 0 不限制'},
	{ 5		, 1		, 'm_roundNum'			, 'UINT'				, 1 	, '局数'},
}

-- 解散结算界面
PstLandVipRoomDismissData = 
{
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1 	, '玩家账户ID'},
	{ 2		, 1		, 'm_isResult'	        , 'UBYTE'	            , 1	    , ' 0发起解散  1同意解散  2拒绝解散  3超时未选  4等待选择'},
	{ 3		, 1		, 'm_isLandNum'	        , 'UBYTE'	            , 1	    , '地主次数'},
	{ 4		, 1		, 'm_score'	        	, 'INT'	            	, 1	    , '分数'},
}

-- 游戏结束结算界面
PstLandVipRoomGameOverData = 
{
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1 	, '玩家账户ID'},
	{ 2		, 1		, 'm_isLandNum'	        , 'UBYTE'	            , 1	    , '地主次数'},
	{ 3		, 1		, 'm_score'	        	, 'INT'	            	, 1	    , '分数'},
}

-- 房间信息
PstLandVipRoomSingleData = 
{
	{ 1		, 1		, 'm_roomId'			, 'UINT'				, 1 	, '房间id'},
	{ 2		, 1		, 'm_acc'				, 'UINT'				, 3 	, '玩家账号列表'},
	
}

---------------------------------------------------------定点赛----------------------------------------------------------------

--房间内座位成员数据
PstLandlordAppointmentMatchCharacterData = 
{
	{ 1		, 1		, 'm_nPosId'			, 'UINT'				, 1 	, '椅子编号'},
	{ 2		, 1		, 'm_nAccount'			, 'UINT'				, 1 	, '玩家账户ID'},
	{ 3		, 1		, 'm_nFaceId'			, 'UINT'				, 1  	, '头像id'},
	{ 4		, 1		, 'm_strNickName'		, 'STRING'				, 1  	, '昵称'},
	{ 5		, 1		, 'm_nCent'			, 'INT'					, 1 	, '积分'},
	{ 6		, 1		, 'm_nLevel'			, 'UINT'				, 1 	, '等级'},
}

