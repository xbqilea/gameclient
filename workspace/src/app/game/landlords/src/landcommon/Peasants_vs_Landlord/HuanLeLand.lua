module(..., package.seeall)


-- 欢乐斗地主协议<客户端-服务器>


CS_G2C_HLLand_LoginData_Nty		=
{
    { 1     , 1		, 'm_eRoundState'       , 'UINT'		, 1     , '游戏状态[1]等待[2]抢地主[3]游戏中[4]结束[5]翻倍[7]明牌状态[8]地主明牌状态'},
    { 2     , 1		, 'm_nOperateChair'     , 'UINT'		, 1     , 'm_eRoundState(2|3 当前操作的玩家座位号[1-3])'},
    { 3     , 1		, 'm_vecValue'          , 'INT'			, 3     , 'm_eRoundState(1 等待状态[0|1准备])(2 叫分状态[-1 没操作 0 不叫 1 叫 2 不抢 3 抢])(3 牌数[1-20])(4 计分)(5 翻倍[0还没叫 1不翻倍 其他数字表示翻的倍数])(7 明牌状态[0没点 1点了])'},
    { 4     , 1		, 'm_nOperateTime'      , 'UINT'		, 1     , 'm_eRoundState(1|2|3 操作倒计时(单位：秒)'},
    { 5     , 1		, 'm_nLastOutChair'     , 'UINT'		, 1    	, 'm_eRoundState(3 上一手牌玩家[0第一手|1-3])'},
    { 6     , 1		, 'm_vecLastOutPokers'  , 'UINT'		, 20    , 'm_eRoundState(3 上一手牌[0-53])'},
    { 7     , 1		, 'm_nLordChairIdx'     , 'UINT'		, 1     , 'm_eRoundState(3 地主座位号[1-3])'},
    { 8     , 1		, 'm_nLordBelordCent'   , 'UINT'		, 1     , 'm_eRoundState(3 地主叫分[1-3])'},
    { 9     , 1		, 'm_nTotalMultiple'    , 'UINT'		, 1     , 'm_eRoundState(3|4 已产生的总倍数)'},
    { 10    , 1		, 'm_nBombs'			, 'UINT'		, 1     , 'm_eRoundState(3|4 已产生炸弹个数)'},
	{ 11	, 1		, 'm_bSpring'			, 'UINT'		, 1		, '春天 [0]否[1]是'},
    { 12    , 1		, 'm_vecTrusteeShip'    , 'UINT'		, 3     , 'm_eRoundState(2|3 托管状态[0|1])'},
    { 13    , 1		, 'm_vecUnderPoker'     , 'UINT'		, 3     , 'm_eRoundState(1|2|3|4 底牌[0-53]黑红花片(3-KA2)小王大王)'},
    { 14    , 1		, 'm_vec1ChairCards'    , 'UINT'		, 20    , 'm_eRoundState(2|3|4 手牌[0-53]黑红花片(3-KA2)小王大王)'},
    { 15    , 1		, 'm_vec2ChairCards'    , 'UINT'		, 20    , 'm_eRoundState(2|3|4 手牌[0-53]黑红花片(3-KA2)小王大王)'},
    { 16    , 1		, 'm_vec3ChairCards'    , 'UINT'		, 20    , 'm_eRoundState(2|3|4 手牌[0-53]黑红花片(3-KA2)小王大王)'},
	{ 17    , 1		, 'm_vecLandOpenCards'  , 'UINT'		, 20    , 'm_eRoundState(3|4|5 地主明牌)'},
	{ 18    , 1		, 'm_vecFarmerOpenCards', 'UINT'		, 20    , 'm_eRoundState(3|4|5 农民对家明牌)'},
    { 19    , 1		, 'm_vecOutCards'       , 'UINT'		, 51    , 'm_eRoundState(3 已经打出去的牌[0-53]黑红花片(3-KA2)小王大王)'},
	{ 20    , 1		, 'm_vecDouStatus'    	, 'UINT'		, 3     , '是否加倍 0 1 不加倍 2 加倍'},
	{ 21	, 1		, 'm_nOpenTime'			, 'UINT'		, 4		, '明牌时间，对应倍数的时间，毫秒'},
	{ 22	, 1		, 'm_nOpenVal'			, 'UINT'		, 4		, '明牌倍数 8 6 4 2 啥的'},
	{ 23	, 1		, 'm_nDouTime'			, 'UINT'		, 1		, '加倍时间 秒'},
	{ 24	, 1		, 'm_nUnderMul'			, 'UINT'		, 1		, '底牌倍数'},
	
}

CS_G2C_HLLand_Begin_Nty		=
{
	{ 1		, 1		, 'm_vecGetCards'		, 	'UINT' ,	17	, '[0-53]黑红花片(3-KA2)小王大王'},
	{ 2		, 1		, 'm_nCurRound'			,	'UINT'	, 1		, '当前轮数'},
	{ 3		, 1		, 'm_nTotalRound'		,	'UINT'	, 1		, '总轮数'},
	{ 4		, 1		, 'm_nOpenTime'			,	'UINT'	, 4		, '明牌时间，对应倍数的时间，毫秒'},
	{ 5		, 1		, 'm_nOpenVal'			,	'UINT'	, 4		, '明牌倍数 8 6 4 2 啥的'},
}

CS_C2G_HLLand_OpenPoker_Req		=
{
	{ 1		, 1		, 'm_nOpenMul'		,'UBYTE'	, 1	, '明牌倍数'},
}

CS_G2C_HLLand_OpenPoker_Nty		=
{
	{ 1		, 1		, 'm_nOpenPosition' 	,	'UINT'	, 1	, '操作明牌座位号[1-3]'},
	{ 2		, 1		, 'm_vecChairCards'		, 	'UINT'	, 20, '明牌玩家手牌'},
	{ 3		, 1		, 'm_vecLastCards'		, 	'UINT'	, 3 , '底牌'},
	{ 4		, 1		, 'm_nOpenVal'			, 	'UINT'	, 1	, '明牌 0 不明牌，1 明牌'},
	{ 5		, 1		, 'm_nNextPosition'		, 	'UINT'	, 1	, '下一个操作座位号[1-3]'},
	{ 6		, 1		, 'm_vecFarmerCards'	, 	'UINT'	, 17, '农民对家明牌'},
	{ 7		, 1		, 'm_nNextTime'			, 	'UINT'	, 1, '下个阶段操作时间(秒) 下个阶段是 加倍 或者 叫分'},
	{ 8		, 1		, 'm_nOpenMul'			, 	'UINT'	, 1, '明牌倍数'},
	{ 9		, 1		, 'm_nLastCardsMul'		, 	'UINT'	, 1, '底牌倍数'},
}

-- CS_C2G_HLLand_BeLord_Req		=
-- {
	-- { 1		, 1		, 'm_nCall'				, 'UBYTE'	, 1	, '[0]不叫[1]叫地主  [0]不抢[1]抢地主'},
-- }

-- CS_G2C_HLLand_BeLord_Nty		=
-- {
	-- { 1		, 1		, 'm_nCallPosition'		, 'UBYTE'	, 1	, '叫分座位号[1-3]'},
	-- { 2		, 1		, 'm_nIsCall'			, 'UBYTE'	, 1	, '0不叫(显示叫地主ui) 1叫地主(显示抢地主ui)'},
	-- { 3		, 1		, 'm_nNextPosition'		, 'UBYTE'	, 1	, '下一个叫分座位号[1-3]'},
-- }

-- CS_G2C_HLLand_BeLordResult_Nty		=
-- {
	-- { 1		, 1		, 'm_nLordPosition'		, 'UBYTE'	, 1	, '地主座位号[1-3]'},
	-- { 2		, 1		, 'm_vecCards'			, 'UBYTE'	, 3	, '[0-53]黑红花片(3-KA2)小王大王'},
	-- { 3		, 1		, 'm_nIsLandOpen'		, 'UINT'	, 1	, '是否有地主明牌操作'},
-- }

CS_C2G_HLLand_LandOpenPoker_Req		=
{
	{ 1		, 1		, 'm_nIsOpenCards'		,'UBYTE'	, 1	, '地主是否明牌  0 不明牌  1 明牌'},
}

CS_G2C_HLLand_LandOpenPoker_Nty		=
{
	{ 1		, 1		, 'm_vecChairCards'		, 	'UINT'	, 20, '地主明牌 内容为空表示地主不明牌'},
	{ 2		, 1		, 'm_vecFarmerCards'	, 	'UINT'	, 17, '农民对家明牌'},
	{ 3		, 1		, 'm_nNextTime'			, 	'UINT'	, 1, '下个阶段操作时间(秒) 下个阶段是 加倍'},
	{ 4		, 1		, 'm_nOpenMul'			, 	'UINT'	, 1, '明牌倍数'},
}

CS_G2C_HLLand_Result_Nty		=
{
	{ 1		, 1		, 'm_bSpring'			,		'UBYTE'		, 1		, '[0]否[1]是'},
	{ 2		, 1		, 'm_nTotalMultiple'	,		'UINT'		, 1		, '总倍数'},
	{ 3		, 1		, 'm_nTotalBombs'		,		'UINT'		, 1		, '炸弹个数'},
	{ 4		, 1		, 'm_vecScore'			,		'INT'		, 3		, '记分(单位:金币)'},
	{ 5		, 1		, 'm_vec1ChairCards'	, 		'UINT'		, 20	, '剩余手牌[0-53]黑红花片(3-KA2)小王大王'},
	{ 6		, 1		, 'm_vec2ChairCards'	, 		'UINT'		, 20	, '剩余手牌[0-53]黑红花片(3-KA2)小王大王'},
	{ 7		, 1		, 'm_vec3ChairCards'	, 		'UINT'		, 20	, '剩余手牌[0-53]黑红花片(3-KA2)小王大王'},	
	{ 8		, 1		, 'm_nCurRound'			,		'UINT'		, 1		, '当前轮数'},
	{ 9		, 1		, 'm_nTotalRound'		,		'UINT'		, 1		, '总轮数'},
	{ 10	, 1		, 'm_nLandPos'			,		'UINT'		, 1		, '地主位置 1 2 3'},
	
	{ 11	, 1		, 'm_nOpenMul'			,		'UINT'		, 1		, '明牌倍数 0 表示无'},
	{ 12	, 1		, 'm_nCallMul'			,		'UINT'		, 1		, '抢地主倍数 0 或者 1 表示无'},
	{ 13	, 1		, 'm_nLastCardsMul'		,		'UINT'		, 1		, '底牌倍数 0 表示无'},
	{ 14	, 1		, 'm_nDoubleMul'		,		'UINT'		, 1		, '加倍操作的倍数 1 表示无'},
	{ 15	, 1		, 'm_nPunResult'		,		'UINT'		, 1		, '惩罚结果 0 没有惩罚  1 有惩罚'},
	{ 16	, 1		, 'm_nEndPos'			,		'UINT'		, 1		, '最后一手出牌的位置 1 2 3'},
	-- { 17	, 1		, 'm_nPunResultVal'		,		'UINT'		, 1		, '惩罚结果是否影响结算 0 没有  1 有  这个客户端自己算，更据 m_nPunResult 和 m_vecScore'},
}

