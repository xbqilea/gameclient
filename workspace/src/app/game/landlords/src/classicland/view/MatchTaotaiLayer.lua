 
local scheduler              =  require("framework.scheduler") 
local HNLayer= require("src.app.newHall.HNLayer") 
local MatchTaotaiLayer = class("MatchTaotaiLayer",function ()
     return HNLayer.new()
end)

function MatchTaotaiLayer:ctor()
    self:myInit()
    self:setupView()
    ToolKit:registDistructor(self, handler(self, self.onDestory));
end

function MatchTaotaiLayer:onDestory()
    if self.m_scheduleOutTime then
        scheduler.unscheduleGlobal(self.m_scheduleOutTime);
    end
    self.m_scheduleOutTime = nil;
end


-- function MatchTaotaiLayer:onTouchCallback(sender)
--     local name = sender:getName() 
-- end
function MatchTaotaiLayer:setupView() 
    local node = UIAdapter:createNode("land_match_cs/land_match_lose.csb")  
    -- UIAdapter:praseNode(node,self) 
    self:addChild(node)
    local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY)) 
    local diffX = 145-(1624-display.size.width)/2 
    local pCenter = node:getChildByName("center");
    pCenter:setPositionX(diffX);

    local bg = pCenter:getChildByName("bg2");
    self.m_pRankNumText = bg:getChildByName("rank_num_text");
    
    local close_button = pCenter:getChildByName("button_close");
    --close_button:addTouchEventListener(handler(self, self.onCloseClick));

    close_button:setVisible(false);

    local jixu_button = pCenter:getChildByName("Button_1");
    jixu_button:addTouchEventListener(handler(self, self.onJiXuClick));

    self.m_textTimeOut = pCenter:getChildByName("text_time_out");
    -- jixu_button:setVisible(false);


end 
function MatchTaotaiLayer:setData(info) 
    -- self["rank"]:setString(info.m_curRank or info.m_showRank)
    -- local function callback()
    --    UIAdapter:popScene()
    -- end

    self.m_pRankNumText:setString(string.format( "%d",info.m_curRank ));
    --"10秒后若没点击继续比赛，将退出比赛返回大厅"
    self.m_textTimeOut:setString("3秒后若没点击继续比赛，将退出比赛返回大厅");
    self.m_nTime = 3;
    if self.m_scheduleOutTime then
        scheduler.unscheduleGlobal(self.m_scheduleOutTime);
    end
    self.m_scheduleOutTime = scheduler.scheduleGlobal(function()
        self.m_nTime = self.m_nTime - 1;
        if self.m_nTime <= 0 then
            self:onCloseClick();
            return;
        end
        self.m_textTimeOut:setString(string.format( "%d秒后若没点击继续比赛，将退出比赛返回大厅", self.m_nTime ));
    end, 1);

end

function MatchTaotaiLayer:onCloseClick()
    -- UIAdapter:popScene();
    --g_GameController:reqMatchExitGame();
    if self.m_scheduleOutTime then
        scheduler.unscheduleGlobal(self.m_scheduleOutTime);
    end
    self.m_scheduleOutTime = nil;
    g_GameController:releaseInstance();
end

function MatchTaotaiLayer:onJiXuClick()
    if g_GameController then
        local nGameId = g_GameController:getGameAtomTypeId();
        -- UIAdapter:popScene();
        self:onCloseClick();
        local MatchController = require("src/app/hall/MatchGameList/control/MatchController");
        -- --MatchController.getInstance():openView("MatchGameListView", nil, 0.2);
        MatchController.getInstance():startGame(nGameId, 0.3);
    end
end

return MatchTaotaiLayer