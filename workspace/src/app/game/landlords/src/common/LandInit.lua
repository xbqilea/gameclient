--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 斗地主初始化加载全局公共文件 
require("app.game.landlords.src.common.LandResourcesKit")
require("app.game.landlords.src.common.LandKit")
require("app.game.landlords.src.common.CardKit")
require("app.game.landlords.src.common.GameTypeKit")
require("app.game.landlords.src.common.LandFrameKit")
require("app.game.landlords.src.common.LandTestKit")
require("app.game.landlords.src.common.SceneKit") 