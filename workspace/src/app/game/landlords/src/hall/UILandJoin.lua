-- lhj
-- 2018/7/17 14:24:25
-- 创建房间
local LandGlobalDefine = require("app.game.landlords.src.landcommon.data.LandGlobalDefine")
local DDSRoomController =  require("src.app.game.landlords.src.classicland.contorller.DingDianSaiRoomController")
local FriendRoomController  = require("src.app.game.landlords.src.classicland.contorller.FriendRoomController")
local EventManager = require("app.game.landlords.src.common.EventManager")
local UILandJoin = class("UILandJoin", function()
	return display.newLayer()
end)


UILandJoin.ClOSE_PANEL_TAG   = 10001    --关闭



function UILandJoin:ctor( hall )
	self.hall = hall
	self.mCurPo = 1
	self.mIsSend = false
	self.mRoomNuber = {}
	self:initUI()
	LAND_LOAD_OPEN_EFFECT(self.layout_bg)
	EventManager:getInstance():addEventListener("UILandJoin.clear",handler(self, self.clear))
end

function UILandJoin:initUI()
	local function closeCallback()
		self:onClose()
	end
	local size = cc.Director:getInstance():getWinSize()
	local item = cc.MenuItemImage:create()
	item:setContentSize(cc.size(size.width, size.height))
	item:registerScriptTapHandler(closeCallback)
	self.backMenu = cc.Menu:create(item)
	self:addChild(self.backMenu)

	self.mRoot = cc.CSLoader:createNode("friend_land_cs/land_join_in.csb")
	UIAdapter:adapter(self.mRoot, handler(self, self.onTouchCallback))
	self:addChild(self.mRoot)

	self.layout_bg = self.mRoot:getChildByName("layout_bg")



	--关闭按钮
	self.closeButton = self.mRoot:getChildByName("btn_close")
	self.closeButton:setTag(UILandJoin.ClOSE_PANEL_TAG)
	self.closeButton:addTouchEventListener(handler(self,self.onTouchButtonCallBack))

	self.closeLayout = self.mRoot:getChildByName("btn_layout")
	self.closeLayout:setTag(UILandJoin.ClOSE_PANEL_TAG)
	self.closeLayout:addTouchEventListener(handler(self, self.onTouchButtonCallBack))

	self.layout_room_number = self.mRoot:getChildByName("layout_room_number")
	for i=1,6 do
		self["TextNumber"..i] = self.layout_room_number:getChildByName("text_"..i)
		self["TextNumber"..i]:setString("")
	end

	self.layout_button = self.mRoot:getChildByName("layout_button")

	for i=1,12 do
		self["ButtonNumber"..(i-1)] = self.layout_button:getChildByName("Button_"..(i-1))
		self["ButtonNumber"..(i-1)]:setTag(i-1)
		self["ButtonNumber"..(i-1)]:addTouchEventListener(handler(self,self.onButtonTouchCallback))
		self["ButtonNumberText"..(i-1)] = self["ButtonNumber"..(i-1)]:getChildByName("text_number")
	end
end

function UILandJoin:setNumber(tag)
	table.insert(self.mRoomNuber, tag)
	if self.mCurPo == 1  then
		self["TextNumber"..(self.mCurPo)]:setString(tostring(tag))
		self.mCurPo = self.mCurPo + 1
	elseif self.mCurPo == 2 then
		self["TextNumber"..(self.mCurPo)]:setString(tostring(tag))
		self.mCurPo = self.mCurPo + 1
	elseif self.mCurPo == 3 then
		self["TextNumber"..(self.mCurPo)]:setString(tostring(tag))
		self.mCurPo = self.mCurPo + 1
	elseif self.mCurPo == 4 then
		self["TextNumber"..(self.mCurPo)]:setString(tostring(tag))
		self.mCurPo = self.mCurPo + 1
	elseif self.mCurPo == 5 then
		self["TextNumber"..(self.mCurPo)]:setString(tostring(tag))
		self.mCurPo = self.mCurPo + 1
	elseif self.mCurPo == 6 then
		if self.mIsSend == false then
			self["TextNumber"..(self.mCurPo)]:setString(tostring(tag))
			self.mIsSend = true
			self:sendJoinInRoom()
		end
	end
end

function UILandJoin:sendJoinInRoom()
	local inputVal = table.concat(self.mRoomNuber)
	self.tempInPutVal = inputVal -- 这个值是在被房主T了再加入时,提示用的
	LogINFO("发送加入房间协议,inputVal is:",inputVal)

	if self.mCurPo < 6 then
		TOAST("房间密码是6位数！")
		return
	end
	
	ConnectManager:send2SceneServer(LandGlobalDefine.FRIEND_ROOM_GAME_ID, "CS_C2M_EnterLandVipRoom_Req", { LandGlobalDefine.FRIEND_ROOM_GAME_ID, tonumber(inputVal) } )
end

function UILandJoin:deleteNumber()
	table.remove(self.mRoomNuber, #self.mRoomNuber)
	if self.mCurPo == 6 and self.mIsSend == true then
		self["TextNumber"..(self.mCurPo)]:setString("")
	elseif self.mCurPo == 6 and  self.mIsSend == false then
		self["TextNumber"..(self.mCurPo-1)]:setString("")
		self.mCurPo = self.mCurPo - 1
	elseif self.mCurPo == 5 then
		self["TextNumber"..(self.mCurPo-1)]:setString("")
		self.mCurPo = self.mCurPo - 1
	elseif self.mCurPo == 4 then
		self["TextNumber"..(self.mCurPo-1)]:setString("")
		self.mCurPo = self.mCurPo - 1
	elseif self.mCurPo == 3 then
		self["TextNumber"..(self.mCurPo-1)]:setString("")
		self.mCurPo = self.mCurPo - 1
	elseif self.mCurPo == 2 then
		self["TextNumber"..(self.mCurPo-1)]:setString("")
		self.mCurPo = self.mCurPo - 1
	elseif self.mCurPo == 1 then
		print("没得删了")
	end
	self.mIsSend = false
end

function UILandJoin:clear()
	for i=1,6 do
		if self["TextNumber"..i] then
			self["TextNumber"..i]:setString("")
		end
	end
	self.mCurPo = 1
	self.mIsSend = false
	self.mRoomNuber = {}
end

function UILandJoin:onClose( ... )
	DOHALL("removeDialog")
	DOHALL("showDefaultView")
end

function UILandJoin:onTouchButtonCallBack(sender,eventType)
	if sender and sender:getTag() then
		local tag = sender:getTag()
		if eventType == ccui.TouchEventType.ended then
		   if tag == UILandJoin.ClOSE_PANEL_TAG then     --关闭
				self:onClose()
			end
		end
	end
end  

function UILandJoin:onButtonTouchCallback(sender,eventType)
	if sender and sender:getTag() then
		local tag = sender:getTag()
		if eventType == ccui.TouchEventType.ended then
			 print("tag is "..tag)
			if tag == 10 then
				self:clear()
			elseif tag == 11 then
				self:deleteNumber()
			else
				self:setNumber(tag)
			end
		end
	end
end

function UILandJoin:onTouchCallback( sender )
	local name = sender:getName()
	print("name: ", name)
end

return UILandJoin
