-- UILandHallSingleHall
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 斗地主牌友房版本
local ProtalController = require("app.hall.main.control.GameNodeController")
local FriendRoomController =  require("src.app.game.landlords.src.classicland.contorller.FriendRoomController")
local LandGameConf = require("app.game.landlords.src.common.LandGameConf")
local LandAnimationManager = require("src.app.game.landlords.src.landcommon.animation.LandAnimationManager")

local UILandHallSingleHall = class("UILandHallSingleHall", function () 
	return UIAdapter:createNode("land_common_cs/land_center_single_hall.csb")
end)

function UILandHallSingleHall:ctor()
	UIAdapter:adapter(self, handler(self, self.onTouchCallback))
	self:initBtn()
	self:updatePaiYouFang()
	self:updateJoinBtn()
	--self:checkIOS()
end

function UILandHallSingleHall:initBtn()
	for i=1,5 do
		local function onClick( sender, eventType  )
			if eventType == ccui.TouchEventType.ended then
				self:onClickBtn( i )
			end
		end
		local btn = self:getChildByName("btn_"..i)
		btn:addTouchEventListener( onClick )
		if i ~= 5 then
			LandAnimationManager:getInstance():PlayAnimation(LandGameConf.AnimationType[i+5], btn:getChildByName("node"))
		end
	end
end

function UILandHallSingleHall:updatePaiYouFang()
	local tbl = ProtalController:getInstance():getChildrenByPortalId( RoomData.LANDLORD )
	local havePaiYouFang = false
	for k,v in pairs( tbl ) do
		if v.name == "牌友房" then
			havePaiYouFang = true
		end
	end
	
	if not havePaiYouFang then
		for i=1,2 do
			local btn = self:getChildByName("btn_"..i)
			btn:setVisible(false)
		end
	end
end

function UILandHallSingleHall:onClickBtn( i )
	if i == 3  or i == 5 then
		REQ_ENTER_CLASSIC_LORD()
	elseif i == 4 then 
		REQ_ENTER_HAPPY_LORD()
	elseif i == 1 then
		
		DOHALL("onClickCreateRoomBigBtn")
	elseif i == 2 then
		local isFangZhu = FriendRoomController:getInstance():checkMyselfIsFangzhu()
		if isFangZhu then
			FriendRoomController:getInstance():backJionin()
		else
			DOHALL("onClickJoinRoomBigBtn")
		end
	else
		TOAST(STR(62, 1))
	end
end

function UILandHallSingleHall:updateJoinBtn()
	local img = self:getChildByName("btn_2"):getChildByName("back_img")
	local isFangZhu = FriendRoomController:getInstance():checkMyselfIsFangzhu()
	img:setVisible( isFangZhu )
end

--[[
function UILandHallSingleHall:checkIOS()
	local tbl = {[5]=1}
	if GlobalConf.IS_IOS_TS == true then
		tbl = {[3]=1,[4]=1}
	end

	for i=1,5 do
		local btn = self:getChildByName("btn_"..i)
		if tbl[i] then
			btn:setVisible(false)
		end
	end
end
--]]

function UILandHallSingleHall:goBack()
	POP_LORD_SCENE()
end

function UILandHallSingleHall:onTouchCallback( sender )
    local name = sender:getName()
    LogINFO("UILandHallSingleHall: ", name)
end

return UILandHallSingleHall