-- UILandHallDoor
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 斗地主房间入口界面
local LandGameConf = require("app.game.landlords.src.common.LandGameConf")
local LandAnimationManager = require("src.app.game.landlords.src.landcommon.animation.LandAnimationManager")

local UILandHallDoor = class("UILandHallDoor", function () 
	return UIAdapter:createNode("land_common_cs/land_center_door.csb")
end)

function UILandHallDoor:ctor()
	UIAdapter:adapter(self, handler(self, self.onTouchCallback))
	self:initBtn()
	--self:checkIOS()
end

function UILandHallDoor:initBtn()
	for i=1,5 do
		local function onClick( sender, eventType  )
			if eventType == ccui.TouchEventType.ended then
				self:onClickBtn( i )
			end
		end
		local btn = self:getChildByName("btn_"..i)
		local size = btn:getContentSize()
		local unopenIMG = self:createUnopenImg( i , size )
		if unopenIMG then
			btn:addChild( unopenIMG )
		end
		btn:addTouchEventListener( onClick )
		LandAnimationManager:getInstance():PlayAnimation(LandGameConf.AnimationType[i], btn:getChildByName("node"))
	end
end

--[[
function UILandHallDoor:checkIOS()
	if GlobalConf.IS_IOS_TS == true then
		for i=2,5 do
			local btn = self:getChildByName("btn_"..i)
			btn:setVisible(false)
		end
	end
end
--]]

function UILandHallDoor:onClickBtn( i )
	if i == 1  then
		REQ_ENTER_CLASSIC_LORD()
	elseif i == 2 then 
		REQ_ENTER_HAPPY_LORD()
	elseif i == 3 then
		DOHALL("showPaiYouFang")
	else
		TOAST(STR(62, 1))
	end
end

function UILandHallDoor:createUnopenImg( i , size )
	if i ~= 4 and i ~= 5 then return end
	local name = "#lord_dt_icon_unopened_l.png"
	local anch = cc.p(1,1)
	local x = size.width
	local y = size.height-40
	if i == 4 then y = size.height-43 end
	if i == 5 then
		name = "#lord_dt_icon_unopened_r.png"
		x    = 0
		anch = cc.p(0,1)
		y = size.height-43
	end
	if i == 5 then x = 0 end

	local pos  = cc.p( x , y )
	local img = display.newSprite(name)--cc.Sprite:create( name )
	img:setPosition( pos )
	img:setAnchorPoint( anch )
	return img
end

function UILandHallDoor:goBack()
	POP_LORD_SCENE()
end

function UILandHallDoor:onTouchCallback( sender )
    local name = sender:getName()
    LogINFO("UILandHallDoor: ", name)
end

return UILandHallDoor