-- UILandEntranceLoading
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 斗地主入口loading条

local GameLoadingLayer = require("app.game.common.util.GameLoadingLayer")
local LandGameResources = require("app.game.landlords.src.hall.LandGameResources")
local LandRoomResources = require("app.game.landlords.src.hall.LandRoomResources")
local FastRoomController    =  require("src.app.game.landlords.src.classicland.contorller.FastRoomController")

local UILandEntranceLoading = class("UILandEntranceLoading", function ( __params )
	return GameLoadingLayer.new(__params)
end)

function UILandEntranceLoading:ctor(__params)
	self.gameInfo = __params._info
	self.params = __params
	self:initCSB()
	self:startLoadRes()
end

function UILandEntranceLoading:setParentScene(scene)
	self.m_ParentScene = scene
end

function UILandEntranceLoading:initCSB()
	self.root = UIAdapter:createNode("land_common_cs/land_loading.csb")
	self:addChild( self.root )
	UIAdapter:adapter(self.root, handler(self, self.onTouchCallback))
end

function UILandEntranceLoading:startLoadRes()
	local allResourcesTb =  {}
	local tRes = { 
					{ list = LandRoomResources, name = "房间资源加载中，请稍后..." },
					{ list = LandGameResources, name = "游戏资源加载中，请稍后..." },
				}
	for k,v in pairs(tRes) do
		local list = v.list[v.list.TableName]
		for i=1,#list do
			allResourcesTb[ #allResourcesTb +1 ] = list[i]
		end
	end
	self.m_tRes = {TableName = "allResources", allResources = allResourcesTb, name = "斗地主所有资源"}
	self:preloadResources( self.m_tRes, handler(self, self.onLoadCompleted), handler(self, self.onLoading) )
    --self:setPercent( 0 )
end

function UILandEntranceLoading:onLoadCompleted()

	if  self.gameInfo ~= nil  and self.gameInfo.reconnect then -- 此处是重连游戏, 不进入下一个界面, 直接调用回调
		self.gameInfo.callback()
	else
		local function f()
			self.m_ParentScene:showHall()
		end
		DO_ON_FRAME( GET_CUR_FRAME()+1 , f )

		if self.params and self.params.funcId and IS_FAST_GAME( self.params.funcId ) then
			local function f()
				FastRoomController:getInstance():onClickAgainGame( self.params.funcId , 0 )
			end
			DO_ON_FRAME( GET_CUR_FRAME()+2 , f )
		end
	end
end

function UILandEntranceLoading:onLoading(__sum, __index, __name )
	print("全部" .. __sum .. "个文件需加载, 当前正在加载第" .. __index .. "个文件" .. __name)
    self:setPercent(__index/__sum*100)
    if __sum == __index + 1 then
		self:setPercent(100)
    end
end

function UILandEntranceLoading:setPercent(__index)
	local loadingBar_1 = self.root:getChildByName("LoadingBar_1")
	if loadingBar_1 then
		loadingBar_1:setPercent(math.floor(__index))
	end
	local text_percent = self.root:getChildByName("text_percent")
	if text_percent then
		text_percent:setString(math.floor(__index) .. "%")
	end
end

-- 点击事件回调
function UILandEntranceLoading:onTouchCallback( sender )
    local name = sender:getName()
    print("UILandEntranceLoading: ", name)
end

return UILandEntranceLoading