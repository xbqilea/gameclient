--
-- Author: lhj
-- Date: 2018-06-20 11:08:34
--
module(..., package.seeall)
TableName = "LandRoomResources"
LandRoomResources = {
	[1] = "csb/resouces/landhalltip.plist",
	[2] = "csb/resouces/landhall_single_door.plist",
	[3] = "csb/resouces/landhall_protal_happy.plist",
	[4] = "csb/resouces/landhall_protal_friend.plist",
	[5] = "csb/resouces/landhall_protal_classic.plist",
	[6] = "csb/resouces/landhall_door.plist",
	[7] = "csb/resouces/landhall.plist",
	[8] = "csb/resouces/land_loading.plist",
	[9] = "csb/resouces/friendroom.plist", 
	[10] = "csb/resouces/landhall.plist",
}