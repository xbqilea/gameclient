--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 主界面菜单项

local ViewBase = require("app.hall.base.ui.ViewBase")
local EventManager = require("app.game.landlords.src.common.EventManager")
local LandItemMenu = class("LandItemMenu", function ()
    return ViewBase.new()
end)

function LandItemMenu:ctor( __funcId )
    self:myInit(__funcId)

    self:setupViews()
end

-- 初始化成员变量
function LandItemMenu:myInit( __funcId )
    self.funcId = __funcId
end

-- 初始化界面
function LandItemMenu:setupViews()
    self.root = UIAdapter:createNode("land_common_cs/land_menu_item.csb")
    self:addChild(self.root)

    self.btn = self.root:getChildByName("btn_top_menu")
    
    UIAdapter:adapter(self.root, handler(self, self.onTouchCallback))

    if self.funcId ~= nil then
        local data = fromFunction(self.funcId)
        if data.id then
            self.btn:loadTextures(data.iconName, data.iconName, data.iconName, 1)
        end
        
        ToolKit:setBtnStatusByFuncStatus( self.btn, self.funcId )
    end
end


-- 点击事件回调
function LandItemMenu:onTouchCallback( sender )
    local name = sender:getName()

    if name == "btn_top_menu" then
        local data = fromFunction(self.funcId)
        print("self.funcid",self.funcId)
        dump(data, "data>>>>>>>>>")
        if data then
            if data.id == 1303 then
                DOHALL("showGameListForReplay")
            elseif data.id == 1302 then
                DOHALL("showFriendRoomListLayer")
            elseif data.id == 1304 then
                DOHALL("showFriendRuleLayer")
            elseif data.id == 1305 or data.id ==1202 or data.id == 1102  then
                if not g_functionDlg then         
                    g_functionDlg = require("app.hall.base.ui.FunctionLayer").new( {funcId = self.funcId})--GlobalDefine.FUNC_ID.MORE_FUNC} )
                    g_functionDlg:showDialog() 
                end
            elseif data.id == 1055 then
                local stackLayer = data.mobileFunction

                if stackLayer and string.len(stackLayer) > 0 then
                    print(stackLayer)
                    local level = 1
                    TotalController:onGotoCurSceneStackLayer(MSG_GOTO_STACK_LAYER, {layer = stackLayer, name = data.name, direction = DIRECTION.HORIZONTAL, funcId = self.funcId , dataTable = {level = level}})
                end
                -- else
                --     TOAST(STR(62, 1))
                -- end
            end
        end

    end
end

return LandItemMenu
