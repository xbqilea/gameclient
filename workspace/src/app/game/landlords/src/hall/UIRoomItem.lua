-- UIRoomItem
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 斗地主单个房间按钮
local DDSRoomController   =  require("src.app.game.landlords.src.classicland.contorller.DingDianSaiRoomController")
local FastRoomController  =  require("src.app.game.landlords.src.classicland.contorller.FastRoomController")
local UIRoomItem = class("UIRoomItem", function ()
	return UIAdapter:createNode("land_common_cs/land_room_item.csb")
end)

function UIRoomItem:ctor( param , pageCount )
	self.father_pageCount = pageCount

	print("param.m_portalId-------->", param.m_portalId)

	self.gwPhoneData      = RoomData:getGameDataById( param.m_portalId )
	self.roomSettingData  = RoomData:getRoomDataById( self.gwPhoneData.funcId )
	UIAdapter:adapter(self, handler(self, self.onTouchCallback))
	self:initTouch()
	self:initFlag()
	self:setPeopleCount( param )
	self:updateUI()
	self:sendQueryToServer()	

	self:getChildByName("text_room_name"):enableOutline(cc.c4b(110, 55, 31, 255), 2)
	self:getChildByName("text_desc"):enableOutline(cc.c4b(110, 55, 31, 255), 2)
end

function UIRoomItem:sendQueryToServer()
	local atom = self:getAtom()
	if not IS_DING_DIAN_SAI( atom ) then return end
	local function f()
		DDSRoomController:getInstance():queryDDS( atom )
	end
	DO_ON_FRAME( GET_CUR_FRAME()+10 , f )
end

function UIRoomItem:initFlag()
	self.flag_tbl = {}
	local tbl = {"applied","doing","maintain"}
	for k,v in pairs( tbl ) do
		self.flag_tbl[v] = self:getChildByName("lord_dt_icon_"..v)
	end
	self:hideAllFlag()
end

function UIRoomItem:hideAllFlag()
	for k,v in pairs( self.flag_tbl ) do
		v:setVisible(false)
	end
end

function UIRoomItem:updateFlag()
	self:hideAllFlag()
	local status = 0
	
	if IS_DING_DIAN_SAI( self:getAtom() ) then
		status = DDSRoomController:getInstance():getSignUpStatus( self:getAtom() )
	end

	if status == 1 then
		self.flag_tbl["applied"]:setVisible(true)
	elseif status == 2 then
		self.flag_tbl["doing"]:setVisible(true)
	elseif status == 4 then
		self.flag_tbl["maintain"]:setVisible(true)
	end
end

function UIRoomItem:getAtom()
	return self.roomSettingData.gameAtomTypeId
end

function UIRoomItem:updateUI()
	self:setRoomName()
	self:updateDesc()
	self:updatePeopleCount()
	self:setRoomIcon()
	self:updateFlag()
end

function UIRoomItem:setRoomName()
	local label = self:getChildByName("text_room_name")
	label:setString( self.roomSettingData.phoneGameName )
end

function UIRoomItem:updateDesc()
	local label = self:getChildByName("text_desc")
	local str = self.roomSettingData.roomLeftDownTips
	if IS_DING_DIAN_SAI( self:getAtom() ) then
		str = DDSRoomController:getInstance():getDDSOpenTimeStr( self:getAtom() ).."开赛"
	end
	label:setString( str )
	--label:setFontSize(25)
end

function UIRoomItem:updatePeopleCount()
	local num = self:getPeopleCount()
	local label = self:getChildByName("text_people_count")
	label:setString( PEPLE_COUNT_FORMAT(num) )
	--label:setFontSize(25)
end

function UIRoomItem:setRoomIcon()
	local bg = self:getChildByName("icon_bg")
	local size = bg:getContentSize()
	local sprite = display.newSprite(self.gwPhoneData.iconName)
	sprite:setScale(0.8)
	sprite:setPosition(size.width/2,size.height/2+3)
	bg:addChild( sprite )
end

function UIRoomItem:setPeopleCount( info )
	if type( info.m_attrList1 ) ~= "table" then return end
	for k,v in pairs( info.m_attrList1 ) do
		if v.m_pid == 1 then
			self.people_num = v.m_value
		end
	end

	local atom = self.roomSettingData.gameAtomTypeId

	if IS_FAST_GAME( atom ) then
		FastRoomController:getInstance():setFastGameCnt( atom , self.people_num )
	elseif IS_DING_DIAN_SAI( self.roomSettingData.gameAtomTypeId ) then
		DDSRoomController:getInstance():setDDSPeopleCnt( atom , self.people_num )
	end
end

function UIRoomItem:getPeopleCount()
	return self.people_num
end

function UIRoomItem:initTouch()
	self.start_x = 0
	self.end_x   = 0
	local btn = self:getChildByName("room_button")
	btn:setSwallowTouches(false)
	local  listenner = cc.EventListenerTouchOneByOne:create()
	local  function onTouchBegan( touch, event )
		local touchPoint = touch:getLocation()
		if btn:getCascadeBoundingBox():containsPoint( touchPoint ) then
			self.start_x = touchPoint.x
			self.end_x   = 0
		end
		return true
	end

	local  function onTouchMove( touch, event )
	end

	local  function onTouchEnd( touch, event )
		local touchPoint = touch:getLocation()
		if self.start_x > 0 and btn:getCascadeBoundingBox():containsPoint( touchPoint ) then
			self.end_x = touchPoint.x
			local gap = math.abs(self.end_x - self.start_x)
			if gap < 30 or self.father_pageCount <= 1 then
				self:onClickRoom()
			end
		end
		self.start_x = 0
	end

	listenner:registerScriptHandler(onTouchBegan , cc.Handler.EVENT_TOUCH_BEGAN )
	listenner:registerScriptHandler(onTouchMove  , cc.Handler.EVENT_TOUCH_MOVED )
	listenner:registerScriptHandler(onTouchEnd   , cc.Handler.EVENT_TOUCH_ENDED )
	
    local eventDispatcher = cc.Director:getInstance():getEventDispatcher()
    eventDispatcher:addEventListenerWithSceneGraphPriority(listenner, btn)
end

function UIRoomItem:onClickRoom()
	local atomID = self:getAtom()
	LogINFO("判定为点击了房间",atomID)
	if self:clickLocked( atomID ) then return end
	if IS_DING_DIAN_SAI( atomID ) then
		DDSRoomController:getInstance():onClickDDS( atomID )
	else
		REQ_ENTER_SCENE( atomID )
	end
end

function UIRoomItem:clickLocked(  atomID )
	if not IS_FREE_ROOM( atomID ) then return end
	if not self.last_click then self.last_click = 0 end
	if os.time() - self.last_click < 2 then return true end
	self.last_click = os.time()
end

function UIRoomItem:onTouchCallback( sender )
    local name = sender:getName()
    LogINFO("UIRoomItem: ", name)
end

return UIRoomItem