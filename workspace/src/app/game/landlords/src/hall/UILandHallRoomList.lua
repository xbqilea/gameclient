-- UILandHallRoomList
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 斗地主具体房间列表
local scheduler             = require("framework.scheduler")
local DDSRoomController     =  require("src.app.game.landlords.src.classicland.contorller.DingDianSaiRoomController")
local MatchSignUpController =  require("src.app.game.landlords.src.classicland.contorller.MatchSignUpController")
local UILandHallRoomList = class("UILandHallRoomList", function ()
	return UIAdapter:createNode("land_common_cs/land_center_room_list.csb")
end)

function UILandHallRoomList:ctor( param )
	UIAdapter:adapter(self, handler(self, self.onTouchCallback))
	self:initPagePoint()
	self:initPageView()
	self:updatePageView( "" , param , true )
	addMsgCallBack(self,MSG_ENTER_GAME_PROTAL_NTY,handler(self,self.updatePageView))

	if table.nums( param.m_portalList ) > 6 then
		self:showSecondPage( param )
	end
	ToolKit:registDistructor( self, handler(self, self.onDestory) )
end

function UILandHallRoomList:onDestory()
	LogINFO("斗地主房间列表UI界面被摧毁")
	removeMsgCallBack(self, MSG_ENTER_GAME_PROTAL_NTY)
end

function UILandHallRoomList:showSecondPage( param )
	local function f()
		if self and self.updatePageView then
			self:updatePageView( "" , param )
		end
	end
	DO_ON_FRAME( GET_CUR_FRAME()+11 , f )
end

function UILandHallRoomList:initPagePoint()
	self.page_point_tbl = {}
	for i=1,2 do
		self.page_point_tbl[i] = self:getChildByName("pagePoint_"..i)
	end
end

function UILandHallRoomList:initPageView()
	self.list_view = self:getChildByName("page")

	self.list_view:setContentSize(display.width, 540.00)
	self.list_view:setCustomScrollThreshold(100)
	local  function onClick( sender, eventType )
		if eventType ~= ccui.PageViewEventType.turning then return end
		self:updatePagePoint()
	end
	self.list_view:addEventListener( onClick )
end

function UILandHallRoomList:hidePagePoint()
	for k,v in pairs( self.page_point_tbl ) do
		v:setVisible( false )
	end
end

function UILandHallRoomList:updatePagePoint()
	if type( self.page_point_tbl ) ~= "table" then return end
	self:hidePagePoint()
	local pages = self.list_view:getPages()
	if #pages < 2 then return end
	local curPage = self.list_view:getCurPageIndex()+1
	for k,v in pairs( self.page_point_tbl ) do
		v:setVisible( true )
		local dot = v:getChildByName("show")
		if k == curPage then
			dot:setVisible(true)
		else
			dot:setVisible(false)
		end
	end
end

function UILandHallRoomList:updateRoomSignUpFlag( atomID )
	LogINFO("更新单个房间UI显示",atomID)
	if type( self.room_item_tbl ) ~= "table" then return end
	for k,v in pairs( self.room_item_tbl ) do
		if v and v.getAtom and v:getAtom() == atomID then
			v:updateFlag()
			v:updateDesc()
		end
	end
	self:updateSignUpDialog()
end

function UILandHallRoomList:updatePageView( msgName, msgInfo , _showFirstPageOnly )
	if not IS_LAND_LORD_PROTAL( msgInfo.m_portalId ) then return end
	if not self or not self.list_view then return end
	if not self.last_update_page then self.last_update_page = 0 end
	if GET_CUR_FRAME() - self.last_update_page <= 10 then return end
	self.last_update_page = GET_CUR_FRAME()
	self.list_view:removeAllPages()
	self.room_item_tbl = {}

	local roomList = self:sortRoom( msgInfo.m_portalList )
	local page1 = self:createOnePage( roomList , 1)
	self.list_view:addPage( page1 )
	
	if _showFirstPageOnly then return end
	
	if table.nums( roomList ) > 6 then
		local page2 = self:createOnePage( roomList , 7)
		self.list_view:addPage( page2 )
	end
	self:updatePagePoint()
end

function UILandHallRoomList:sortRoom( portalList )
	local ret = {}
	for k,v in pairs( portalList ) do
		v.openTime = self:getOpenTime( v.m_portalId )
		table.insert( ret , v )
	end

	local function f(a,b)
		if a.m_sort == b.m_sort then
			return a.openTime < b.openTime
		else
			return a.m_sort < b.m_sort
		end
	end
	table.sort( ret , f )
	return ret
end

function UILandHallRoomList:getOpenTime( protalID )
	local ret = 0
	local gwData = RoomData:getGameDataById( protalID )
	ret = DDSRoomController:getInstance():getDDSOpenTime( gwData.funcId ) or 0
	return ret
end

function UILandHallRoomList:createOnePage( tbl , from )
	local page = ccui.Layout:create()
	page:setContentSize(self.list_view:getContentSize().width,self.list_view:getContentSize().height)
	local conSize = page:getContentSize()
    local width = conSize.width
    local height = conSize.height
    local grap = 0 
    if display.width/display.height == 1480/720 then
    	grap = 0.02
    end
    
    local grap1 = 16

    local tem1 = 0.34 + grap
    local tem2 = 0.73 - grap
	local ItemPos = {
        [1] = cc.p(width*tem1,     height/6*4.9 - grap1 ),
        [2] = cc.p(width*tem2,   height/6*4.9 - grap1 ),
        [3] = cc.p(width*tem1,     height/6*3),
        [4] = cc.p(width*tem2,   height/6*3),
        [5] = cc.p(width*tem1,     height/6*1.1 + grap1),
        [6] = cc.p(width*tem2,   height/6*1.1 + grap1),
    }
    local key = 0
	local to = from + 5
	for i=from,to do
		local roomItem = self:createOneRoomItem( tbl[i] , #tbl )
		if roomItem then
			key = key + 1
			roomItem:setPosition( ItemPos[key] )
			page:addChild( roomItem )
			self.room_item_tbl[i] = roomItem
		end
	end	
	return page
end

function UILandHallRoomList:createOneRoomItem( info , roomListCount )
	if not info then return end
	local item = RequireEX("app.game.landlords.src.hall.UIRoomItem").new( info , roomListCount/6 )
	return item
end

function UILandHallRoomList:showSignUpGameDialog(  atomID , status )
	if not self.dialogLayer or not self.dialogLayer.showTuiSaiBtn then
		local path = "src.app.game.landlords.src.landcommon.view.match.SignUpGameNewDialog"
		self:popDialog( path , atomID )
	end
	if status and status == 1 then
		self.dialogLayer:showTuiSaiBtn()
		self.dialogLayer:showCountDownTimer()
	else
		self.dialogLayer:showBaoMingBtn()
		self.dialogLayer:showBaoMingNum()
	end
	self.dialogLayer:updateOpenLabel()
end

function UILandHallRoomList:updateSignUpDialog()
	if self.dialogLayer and self.dialogLayer.updateOpenLabel then
		self.dialogLayer:updateOpenLabel()
	end
end

function UILandHallRoomList:showChongQiangMatch(atomID )
	if not self.dialogLayer then
		local path = "src.app.game.landlords.src.landcommon.view.match.SignUpGameNewDialog"
		self:popDialog( path , atomID )
	end
	self.dialogLayer:showSignUpTips(200)
end

function UILandHallRoomList:onBaoMingSuccess( atom )
	if self:haveBaoMingDiaLog() then
		TOAST("报名成功!")
		self.dialogLayer:showTuiSaiBtn()
		self.dialogLayer:updateBaoMingNum()
		self.dialogLayer:showCountDownTimer()
	else
		self:showSignUpGameDialog( atom , 1 )
	end
end

function UILandHallRoomList:haveBaoMingDiaLog()
	if self.dialogLayer and self.dialogLayer.showTuiSaiBtn then return true end
end

function UILandHallRoomList:onCancelBaoMingSuccess( atom )
	TOAST("退赛成功!")
	if self.dialogLayer and self.dialogLayer.showBaoMingBtn then
		self.dialogLayer:showBaoMingBtn()
		self.dialogLayer:showBaoMingNum()
	end
end

function UILandHallRoomList:popDialog( path , ... )
	self:removeDialog()
	self.dialogLayer = RequireEX( path ).new( ... )
	self.dialogLayer:setAnchorPoint(0.5,0.5)
	self:addChild( self.dialogLayer , 2 )
end

function UILandHallRoomList:removeDialog()
	if self.dialogLayer then 
		self:removeChild( self.dialogLayer )
		self.dialogLayer = nil 
	end
end

function UILandHallRoomList:goBack()
	print("--------------->返回")
	--DOHALL("roomListGoBack")
	POP_LORD_SCENE()
end

function UILandHallRoomList:onTouchCallback( sender )
    local name = sender:getName()
    LogINFO("UILandHallRoomList: ", name)
end
return UILandHallRoomList