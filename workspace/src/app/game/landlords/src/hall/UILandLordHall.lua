
-- Author: lhj
-- Date: 2018-07-16
-- 斗地主大厅
local StackLayer            = require("app.hall.base.ui.StackLayer")
local LandItemMenu          = require("app.game.landlords.src.hall.LandItemMenu")
local LandGlobalDefine      = require("src.app.game.landlords.src.landcommon.data.LandGlobalDefine")
local MatchSignUpController = require("src.app.game.landlords.src.classicland.contorller.MatchSignUpController")
local FastRoomController    = require("src.app.game.landlords.src.classicland.contorller.FastRoomController")
local FriendRoomController =  require("src.app.game.landlords.src.classicland.contorller.FriendRoomController")

local UILandLordHall = class("UILandLordHall", function () return StackLayer.new() end)

local BTN_FUNC_TYPE = {
	CLASSIC         =          1, --经典斗地主
	HAPPY           =          2, --欢乐斗地主
	CREATE_ROOM     =          3, --牌友房创建房间
	JOIN_ROOM       =          4, --牌友房加入房间
}

function UILandLordHall:ctor( param )
	self:myInit()
	self:initCSB()
	self:addNetWorkLayer()
	self:showBtnState()
    self:showLandHallDoor()
	self:initGameRoomBG()
end

function UILandLordHall:myInit()
	ToolKit:registDistructor( self, handler(self, self.onDestory) )
	addMsgCallBack(self, MSG_ENTER_GAME_PROTAL, handler(self, self.onEnterGameProtal))
	addMsgCallBack(self, MSG_GAME_SHOW_MESSAGE, handler(self, self.onGameShowMessage))
	addMsgCallBack(self, MSG_SHOW_HIDE_LOBBY, handler(self, self.toggleLobby))
	addMsgCallBack(self, PublicGameMsg.MSG_C2H_ENTER_SCENE_ACK, handler(self, self.onEnterScene))
end

function UILandLordHall:onDestory()
	LogINFO("斗地主大厅Layer被摧毁")
	removeMsgCallBack(self, MSG_ENTER_GAME_PROTAL)
	removeMsgCallBack(self, MSG_GAME_SHOW_MESSAGE)
	removeMsgCallBack(self, MSG_SHOW_HIDE_LOBBY)
	removeMsgCallBack(self, PublicGameMsg.MSG_C2H_ENTER_SCENE_ACK)
end

function UILandLordHall:initCSB()
	self.root = UIAdapter:createNode("land_common_cs/land_lord_hall.csb")
    self:addChild(self.root)
    UIAdapter:adapter(self.root, handler(self, self.onTouchCallback))

    self.center_bg = self.root:getChildByName("layout_center")
    self.center_bg:setLocalZOrder(1)
    self.func_Type = BTN_FUNC_TYPE.CLASSIC --默认经典斗地主
end

function UILandLordHall:hideAllChildNodesForIosTs()
	self.root:getChildByName("layout_left_top"):setVisible(false)
    self.root:getChildByName("layout_botton"):setVisible(false)
    self.root:getChildByName("layout_top"):setVisible(false)
    self.root:getChildByName("layout_center_top"):setVisible(false)
end

function UILandLordHall:showLandHallRoomsForIosTs( _info )
	local path = "app.game.landlords.src.hall.UILandHallRoomsForTs"
	self.center_layer = RequireEX(path).new( _info )
	self:addChild( self.center_layer )
	--self:updateCenter( path , _info )
end


function UILandLordHall:addNetWorkLayer()
	local layer = RequireEX("app.game.landlords.src.landcommon.view.LandNetWorkLayer").new()
	self:addChild( layer , -1 )
end

function UILandLordHall:showBtnState()
	for i=1, 4 do
		local btn = self.root:getChildByName(string.format("btn_land_%d", i))
		local text1 = btn:getChildByName("text_1")
		local text2 = btn:getChildByName("text_2")
		if self.func_Type == i then
			btn:loadTextureNormal("ddz_xuanzhong.png", 1)
			btn:loadTexturePressed("ddz_xuanzhong.png", 1)
			btn:loadTextureDisabled("ddz_xuanzhong.png", 1)
						
			text1:setColor(cc.c4b(244, 90, 28, 255))
			text2:setColor(cc.c4b(244, 90, 28, 255))
		else
			btn:loadTextureNormal("ddz_changtai.png", 1)
			btn:loadTexturePressed("ddz_changtai.png", 1)
			btn:loadTextureDisabled("ddz_changtai.png", 1)

			text1:setColor(cc.c4b(84, 70, 53, 255))
			text2:setColor(cc.c4b(84, 70, 53, 255))
		end
	end
end

function UILandLordHall:showLandHallDoor()

	if self.func_Type == BTN_FUNC_TYPE.CLASSIC then
		if GET_LORD_HALL() == nil then
			REQ_ENTER_CLASSIC_LORD(handler(self, self.showLandHallPortal))
		else
			REQ_ENTER_CLASSIC_LORD()
		end
		
	elseif self.func_Type == BTN_FUNC_TYPE.HAPPY then
		REQ_ENTER_HAPPY_LORD()
	elseif self.func_Type == BTN_FUNC_TYPE.CREATE_ROOM then
		local isFangZhu = FriendRoomController:getInstance():checkMyselfIsFangzhu()
		if isFangZhu then
			TOAST("您已经有房间,无法再创建")
			self:showDefaultView()
			return
		end
		DOHALL("onClickCreateRoomBigBtn")
	elseif self.func_Type == BTN_FUNC_TYPE.JOIN_ROOM then
		local isFangZhu = FriendRoomController:getInstance():checkMyselfIsFangzhu()
		if isFangZhu then
			self:showDefaultView()
			FriendRoomController:getInstance():backJionin()
		else
			DOHALL("onClickJoinRoomBigBtn")
		end
	end
end

function UILandLordHall:showDefaultView()
    self.func_Type = BTN_FUNC_TYPE.CLASSIC
	self:showBtnState()
	self:showLandHallDoor()
end

function UILandLordHall:showLandHallPortal( _info )
	self.protal_info = _info or self.protal_info
	local path = "app.game.landlords.src.hall.UILandHallPortal"
	self:updateCenter( path , self.protal_info )
end

function UILandLordHall:showPaiYouFang()
	local path = "app.game.landlords.src.hall.UILandHallPaiYouFang"
	self:updateCenter( path )
end

function UILandLordHall:showLandHallRoomList( _info )

	print("刷新UI")

	local path = "app.game.landlords.src.hall.UILandHallRoomList"

	self:updateCenter( path , _info )

	local listName = ""
	local imgTailPath = nil
	if _info then
		if not _info.m_portalList or not _info.m_portalList[1] then
			return
		end

		if IS_HAPPY_LAND_PROTAL( _info.m_portalId ) then
			if HAPPY_ROOM_TYPE( _info.m_portalList[1].m_portalId ) == 1 then
				imgTailPath  = "diz_hl_jdddz.png"
				listName = "自\n由\n场"
			elseif HAPPY_ROOM_TYPE( _info.m_portalList[1].m_portalId ) == 2 then
				imgTailPath  = "diz_hl_jdksc.png"
				listName = "快\n速\n场"
			end
		elseif IS_LAND_LORD_PROTAL( _info.m_portalId ) then
			if CLASSIC_ROOM_TYPE( _info.m_portalList[1].m_portalId ) ==1 then
				imgTailPath  = "diz_zi_jdddz.png"
				listName = "自\n由\n场"
			elseif CLASSIC_ROOM_TYPE( _info.m_portalList[1].m_portalId ) == 2 then
				imgTailPath  = "diz_zi_jdksc.png"
				listName = "快\n速\n场"
			end
		end

	end

	self:setMenuStateShow(true, listName)

	self:setTitleImage(imgTailPath )
end

function UILandLordHall:setMenuStateShow(state, listName)
	
	print("-------------setMenuStateShow----------------")

	local menu_node = self.root:getChildByName("menu_node")
	local list_node = self.root:getChildByName("list_node")

	--menu_node:setVisible(not state)

--[[	for i=1, 4 do
		local btn_land = menu_node:getChildByName(string.format("btn_land_%d", i))
		btn_land:setEnabled(not state)
	end--]]


	self.root:getChildByName("layout_left_top"):setLocalZOrder(66666)

	for i=1, 4 do
		local btn_land = menu_node:getChildByName(string.format("btn_land_%d", i))
		if self.func_Type == i then
			btn_land:setEnabled(false)
		else
			btn_land:setEnabled(true)
		end
	end



	--list_node:setVisible(state)

	--[[if state then
		local list_name = list_node:getChildByName("list_name")
		list_name:setString(listName)
	end--]]

	--self.root:getChildByName("layout_botton"):setVisible(not state)

	local center_top = self.root:getChildByName("layout_center_top")
	center_top:setVisible(state)
end

function UILandLordHall:setTitleImage(imgTailPath)
	local title_spr = self.root:getChildByName("title_spr")
	title_spr:setSpriteFrame(display.newSpriteFrame(imgTailPath))
end

function UILandLordHall:roomListGoBack()
	self:setMenuStateShow(false)
	if self.func_Type == BTN_FUNC_TYPE.CLASSIC then
		REQ_ENTER_CLASSIC_LORD()
	elseif self.func_Type == BTN_FUNC_TYPE.HAPPY then
		REQ_ENTER_HAPPY_LORD()
	end
	
end

function UILandLordHall:updateCenter( path , _info )
	self.center_bg:stopAllActions()
	self:removeDialog()
	self.center_bg:removeAllChildren()
	self.center_layer = RequireEX(path).new( _info )
	self.center_bg:addChild( self.center_layer )
	self:showEffect()
end

function UILandLordHall:showEffect()
	self.center_layer:setScale(0.7)
	self.center_layer:runAction( cc.ScaleTo:create(0.15,1,1) )
end

function UILandLordHall:hideTitleImage()
	local bg = self.root:getChildByName("layout_title")
	bg:setVisible(false)
end

function UILandLordHall:createImgLayout( head , tail )
	local layout = ccui.Layout:create()
	layout:setAnchorPoint(0.5,0.5)

	local imgHead = display.newSprite(head)
	local imgHeadSize = imgHead:getContentSize()
	imgHead:setAnchorPoint(0,0)
	layout:addChild( imgHead )

	local imgTailSize = cc.size(0,0)
	if tail ~= "" then
		local imgTail = display.newSprite(tail)
		imgTailSize = imgTail:getContentSize()
		imgTail:setAnchorPoint(0,0)
		imgTail:setPositionX(imgHeadSize.width)
		layout:addChild( imgTail )
	end
	
	local layoutWidth = imgHeadSize.width + imgTailSize.width
	layout:setContentSize( cc.size(layoutWidth, imgHeadSize.height) )
	return layout
end

function UILandLordHall:onClickBackBtn()
	if self.gameRoomBG:isVisible() then return end
	if not tolua.isnull( self.center_layer ) then
	    self.center_layer:goBack()
	end
end

-------------------------------------游戏房背景开始------------------------------------------
function UILandLordHall:initGameRoomBG()
	self.gameRoomBG = RequireEX( "src.app.game.landlords.src.landcommon.view.LandWaitOtherPlayerLayer" ).new()
	self:addChild( self.gameRoomBG ,100 )
	self.gameRoomBG:setVisible(false)
end

function UILandLordHall:showGameRoomBG(game_atom)
	if not self.gameRoomBG then return end
	self:removeDialog()
	self.gameRoomBG:initBg(game_atom)
	self.gameRoomBG:setVisible(true)
end

function UILandLordHall:hideGameRoomBG()
	if not self.gameRoomBG then return end
	self.gameRoomBG:hideEveryThing()
	self.gameRoomBG:setVisible(false)
end
-------------------------------------游戏房背景结束------------------------------------------

-------------------------------------弹窗代码开始------------------------------------------
function UILandLordHall:updateGameListLayer()
	local scene = GET_LORD_SCENE()
	if not scene then return end
	local layer = scene:getChildByName("GameListForReplayLayer")
	if layer and layer.updateListView then
		layer:updateListView()
	end
end

function UILandLordHall:disMissFriendRoom( id )
	if self.dialogLayer and self.dialogLayer.removeRoom then
		self.dialogLayer:removeRoom( id )
	end
end

function UILandLordHall:showGameListForReplay()
	local path = "app.game.landlords.src.hall.GameListForReplayLayer"
	self:popDialog( path )
end

function UILandLordHall:showFriendRoomListLayer()
	local path = "app.game.landlords.src.friendland.view.FriendRoomListLayer"
	self:popDialog( path )
end

function UILandLordHall:showFriendRuleLayer()
	local path = "app.game.landlords.src.friendland.view.FriendRule"
	self:popDialog( path )
end

function UILandLordHall:onClickJoinRoomBigBtn()
	--[[if device.platform ~= "windows" and not ToolKit:checkBindWx() then
		self:showBindWxDialog()
	else--]]
		self.center_bg:removeAllChildren()
		self:showJoinRoomDialog()
	--[[end--]]
end

function UILandLordHall:onClickCreateRoomBigBtn()
	--[[if device.platform ~= "windows" and not ToolKit:checkBindWx() then
		self:showBindWxDialog()
	else--]]
		self.center_bg:removeAllChildren()
		self:showCreateRoomDialog()
	--[[end--]]
end

function UILandLordHall:showBindWxDialog()
	local dlg = RequireEX("app.game.landlords.src.landcommon.view.LandDiaLog").new()
	local content = "为了您更好地与好友进行游戏,请登录微信"
	dlg:setContent( content , 26 )

	local function f()
		local function bindRet( code )
			dlg:closeDialog()
			if code == 0 then
				self:showCreateRoomDialog()
			elseif code == 1 then
				self:showUseWxDialog()
			end
		end
		ToolKit:bindingWx( bindRet )
	end
	dlg:showSingleBtn("打开微信",f)
end

--[[
function UILandLordHall:showUseWxDialog()
	local dlg = RequireEX("app.game.landlords.src.landcommon.view.LandDiaLog").new()
	local content = "检测到此微信号已有绑定帐号,是否使用此微信帐号登录"
	dlg:setContent( content , 26 )

	local function f()
		local function loginRet( code )
			LogINFO("loginRet",code)
			dlg:closeDialog()
		end
		ToolKit:loginByWx( loginRet )
	end
	dlg:showYesBtn("是",f)

	local function no()
		dlg:closeDialog()
	end
	dlg:showNoBtn("否",no)
end
--]]

function UILandLordHall:showCreateRoomDialog()
	local path = "app.game.landlords.src.hall.UILandCreate"
	self:popDialog( path )
end

function UILandLordHall:showJoinRoomDialog()
	local path = "app.game.landlords.src.hall.UILandJoin"
	self:popDialog( path )
end

function UILandLordHall:createUICreateSsful(param)
	local path = "app.game.landlords.src.hall.UILandCreateSsful"
	self:popDialog( path , param )
end

function UILandLordHall:popDialog( path , ... )
	self:removeDialog()
	self.dialogLayer = RequireEX( path ).new( ... )
	self:addChild( self.dialogLayer , 2 )
end

function UILandLordHall:removeDialog()
	if self.dialogLayer then 
		self:removeChild( self.dialogLayer )
		self.dialogLayer = nil 
	end
	DOHALL_CENTER("removeDialog")
	self:clearGameMsgDialog()
end

-------------------------------------弹窗代码结束------------------------------------------

-------------------------------------监听触发型代码开始------------------------------------------
function UILandLordHall:onGameShowMessage( msgName, msgObj , msgInfo )
    if not IN_LORD_SCENE() then return end
    if msgInfo.enterGameId and IS_DING_DIAN_SAI( msgInfo.enterGameId )  then
    	if self and self.onGameStart then
    		self:onGameStart( msgInfo.enterGameId )
    	end
        return
    end

    local alertType = msgInfo.type
    if alertType == 1 then
        self:showGameMsgOnTop( msgInfo )
    else
        self:popGameMsgDialog( msgInfo )
    end
end


function UILandLordHall:onGameStart( atom )
	LogINFO("在斗地主大厅接收到游戏已经开赛通知",atom,self.gameRoomBG:isVisible())
	if self.gameRoomBG:isVisible() then return end
	if IS_LAND_LORD( atom ) then
		LogINFO("目标游戏是斗地主自己的游戏,不需要退出到一部大厅")
		REQ_ENTER_SCENE( atom )
	else
		LogINFO("目标游戏是其他产品的游戏,强行退出到一部大厅")
		--RoomTotalController:getInstance():forceEnterGame( RoomData.LANDLORD , atom )
	end
end

function UILandLordHall:showGameMsgOnTop( info )
    local enterGameId = info.enterGameId
    if enterGameId then
    	LogINFO("斗地主大厅接收到最后一次开赛提醒,并且目标是其他产品",enterGameId)
        RoomTotalController:getInstance():forceEnterGame( RoomData.LANDLORD , enterGameId )
        return
    end

    if not info.cancelSignUpId then
    	LogINFO("斗地主大厅接收到第一次开赛提醒")
    	local notice =  require("src.app.game.landlords.src.landcommon.view.LandRollNotice").new(info.message, 1, 10)
        ToolKit:addBeginGameNotice(notice, 10)
        return
    end
end

function UILandLordHall:clearGameMsgDialog()
	if self.gameMsgDialog and self.gameMsgDialog.closeDialog then
		self.gameMsgDialog:closeDialog()
		self.gameMsgDialog = nil
	end
end

function UILandLordHall:popGameMsgDialog( info )
	self:hideGameRoomBG()
	self:clearGameMsgDialog()
    local old_atom = info.cancelSignUpId
    local new_atom = info.signUpId
    local enterGameId = info.enterGameId
    local condition   = info.condition or 0

    self.gameMsgDialog = RequireEX("app.game.landlords.src.landcommon.view.LandDiaLog").new()
    self.gameMsgDialog:setTitle("比赛提示")
	self.gameMsgDialog:setContent( info.message , 26 )
	
    if IS_DING_DIAN_SAI( old_atom ) and IS_FAST_GAME( new_atom ) then
        LogINFO("报名满人赛时候,服务器提示和定点赛冲突")
        local function f()
        	if DOHALL_CENTER("haveBaoMingDiaLog") then
            	MatchSignUpController:getInstance():reqSignUp( new_atom , condition , 1 )
            else
            	MatchSignUpController:getInstance():reqSignUp( new_atom , condition , 1 , true )
            end
            self:clearGameMsgDialog()
        end
        self.gameMsgDialog:showSingleBtn("先玩着",f)
        
        local function onClose()
        	FastRoomController:getInstance():reqMatchCloseSignupPage()
        	self:clearGameMsgDialog()
        end
        self.gameMsgDialog:setCloseBtnFun( onClose )
        
        return
    end

    if IS_FAST_GAME( old_atom ) and IS_FREE_ROOM( enterGameId ) then
        LogINFO("请求进入金币房时候,服务器提示和满人赛冲突")
        local function f()
            MatchSignUpController:getInstance():reqCancelSignUp( old_atom  , enterGameId )
            self:clearGameMsgDialog()
        end
        self.gameMsgDialog:showSingleBtn("继续进入",f)
        return
    end

    if old_atom and new_atom then
        local function f()
            MatchSignUpController:getInstance():reqCancelSignUp( old_atom  , new_atom )
            self:clearGameMsgDialog()
        end
        self.gameMsgDialog:showSingleBtn("继续报名",f)
        return
    end

    if new_atom and IS_DING_DIAN_SAI( new_atom ) then
    	LogINFO("报名定点赛时候,服务器提示和另外一个定点赛时间相近")
    	local function f()
    		MatchSignUpController:getInstance():reqSignUp( new_atom , condition , 1 )
            self:clearGameMsgDialog()
        end
        self.gameMsgDialog:showSingleBtn("继续报名",f)
        return
    end

    local function f()
    	self:hideGameRoomBG()
        self:clearGameMsgDialog()
    end
    self.gameMsgDialog:setTitle("提示")
    self.gameMsgDialog:showSingleBtn("确定",f)
    self.gameMsgDialog:setCloseBtnFun( f )
end

function UILandLordHall:onEnterGameProtal( msgName , __info )
	if #__info.m_portalList == 0 then
        TOAST("敬请期待")
        return
    end
    local gameData = fromFunction( __info.m_portalId )
	if gameData.entryType == 4 then
		if self and self.showLandHallRoomList then
			self:showLandHallRoomList( __info )
	    end
    end
end

function UILandLordHall:onEnterScene( _msgType , __info )
	if not IS_LAND_LORD( __info.m_gameAtomTypeId ) then return end
	ToolKit:removeLoadingDialog()
    if __info.m_ret == 0 then
        LogINFO("进入斗地主类型游戏场景成功")
    else
    	if self and self.hideGameRoomBG then
    	   self:hideGameRoomBG()
    	end
        if __info.m_ret == -705 then
    		local function f()
    			if self and self.showChongQiangFree then
    				self:showChongQiangFree( __info )
    			end
    		end
    		DO_ON_FRAME(GET_CUR_FRAME()+2,f)
        else
        	 ToolKit:showErrorTip( __info.m_ret )
        	 if IS_DING_DIAN_SAI( __info.m_gameAtomTypeId ) then
        	 	local content = __info.m_gameAtomTypeId.."#"..__info.m_ret
        	 	--qka.BuglyUtil:reportException("landlordsDDSEnterSceneFail",content)
        	 end
        end
    end
end

function UILandLordHall:showChongQiangFree( _info)
	local dlg = RequireEX("app.game.landlords.src.landcommon.view.LandDiaLog").new()
	local function f()
		if getFuncOpenStatus(1013) == 1 then
			TOAST("商城维护中!")
			return
		end
		local data = fromFunction(GlobalDefine.FUNC_ID.GOLD_RECHARGE)
        local stackLayer = data.mobileFunction
        if stackLayer and string.len(stackLayer) > 0 then
            --TotalController:onGotoCurSceneStackLayer(MSG_GOTO_STACK_LAYER, {layer = stackLayer, name = data.name, direction = DIRECTION.HORIZONTAL,})
            sendMsg(MSG_GOTO_STACK_LAYER, {layer = stackLayer, name = data.name, funcId = GlobalDefine.FUNC_ID.GOLD_RECHARGE})
        end
        dlg:closeDialog()
    end
    local mRoomData  = RoomData:getRoomDataById( _info.m_gameAtomTypeId )
	local jsonSTR = mRoomData["roomMinScore"]
	local tbl = require("cjson").decode( jsonSTR )
	local name = ""
	local loseTopMoney = tostring(mRoomData.loseTopMoney*2).."金币"
	-- if tbl and type(tbl[loseTopMoney]) == "table" then
	-- 	local info = tbl[loseTopMoney]
	-- 	name = info.min.."金币"
	-- end
    local content = "金币不足,加入房间至少需要"..loseTopMoney
    dlg:setContent( content , 26 )
    dlg:showSingleBtn("充值",f)

    local function onClose()
    	HIDE_GAME_ROOM_BG()
    	dlg:closeDialog()
    end
    dlg:setCloseBtnFun( onClose )
end

function UILandLordHall:toggleLobby()
	self.center_bg:setVisible( not self.center_bg:isVisible() )
	self.layout_bottom:setVisible( not self.layout_bottom:isVisible() )
end
-------------------------------------监听触发型代码结束------------------------------------------

function UILandLordHall:onTouchCallback( sender )
    local name = sender:getName()
    LogINFO("UILandLordHall: ", name)
    if name == "btn_back" then
    	self:onClickBackBtn()
    elseif string.sub(name, 1, #name -1) == "btn_land_" then

    	print("点击按钮")

    	self.func_Type = tonumber(string.sub(name, -1))
    	self:showBtnState()
    	self:showLandHallDoor()
    elseif name == "fast_btn" then
    	local atomID = nil
    	if self.func_Type == BTN_FUNC_TYPE.CLASSIC then
    		atomID = LandGlobalDefine.YI_YUAN_CLASSIC_ROOM_ID
    	elseif self.func_Type == BTN_FUNC_TYPE.HAPPY then
    		atomID = LandGlobalDefine.YI_YUAN_HAPPY_ROOM_ID
    	end
		FastRoomController:getInstance():onClickAgainGame(atomID )
	elseif name == "room_btn" then
		self:showFriendRoomListLayer()
	elseif name == "playback_btn" then
		self:showGameListForReplay()
    end
end

return UILandLordHall