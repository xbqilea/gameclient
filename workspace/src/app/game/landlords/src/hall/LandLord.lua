-- LandLord
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 斗地主界面入口
local LandHallMsgController = require("src.app.game.landlords.src.common.LandHallMsgController"):getInstance()
local FastRoomController    =  require("src.app.game.landlords.src.classicland.contorller.FastRoomController") 

local LandLord = class("LandLord", function ( __params )
	return GameEntranceScene.new(__params)
end)

function LandLord:ctor( __params )
	LogINFO("进入斗地主!!")
	self:myInit()

	self:startLoading(__params)

	--[[local function f()
		self:showHall()
	end

	DO_ON_FRAME( GET_CUR_FRAME()+1 , f )

	if __params and __params.funcId and IS_FAST_GAME( __params.funcId ) then
		local function f()
			FastRoomController:getInstance():onClickAgainGame( __params.funcId , 0 )
		end
		DO_ON_FRAME( GET_CUR_FRAME()+2 , f )
	end--]]
end

function LandLord:myInit()
	ToolKit:registDistructor( self, handler(self, self.onDestory) )
	self:registBackClickHandler(handler(self, self.onClickSystemBackButton))
end

function LandLord:onDestory()
    LogINFO("斗地主大厅Scene被摧毁")
    local LandRoomController = require("src.app.game.landlords.src.common.LandRoomController"):getInstance()
    LandRoomController:onDestory()
end

function LandLord:startLoading(__params)
	local layer = RequireEX("app.game.landlords.src.hall.UILandEntranceLoading").new(__params)
	layer:setParentScene(self)
	self:addChild( layer , -1 )
end

function LandLord:showHall()
	local path = "app.game.landlords.src.hall.UILandLordHall"
	RequireEX( path )
	self.hall = self:getStackLayerManager():pushStackLayer( path )
end

function LandLord:onClickSystemBackButton()
	local layer = self:getStackLayerManager():getCurrentLayer()
	LogINFO("斗地主 点击了系统返回键",layer)
	if not layer then return end
	if layer.class and layer.class.__cname == "UILandLordHall" then
		DOHALL("onClickBackBtn")
	else
		self:getStackLayerManager():popStackLayer()
	end
end

return LandLord
