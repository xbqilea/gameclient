-- UILandHallRoomsForTs
-- Author:
-- 斗地主ios审核用
local scheduler             =  require("framework.scheduler")
local UserCenterHeadIcon    =  require("app.hall.userinfo.view.UserHeadIcon")

local UILandHallRoomsForTs = class("UILandHallRoomsForTs", function ()
	 return display.newLayer()
end)

function UILandHallRoomsForTs:ctor( param )
	print("UILandHallRoomsForTs:ctor")
	dump( param )
	self.portalId = param.m_portalId
    --保存节点信息
    self.fuctionItemDatas = {}
    for k,v in pairs( param.m_portalList ) do
		  local data = RoomData:getGameDataById(v.m_portalId)
	      data.m_attrList1 = v.m_attrList1
	      self.fuctionItemDatas[v.m_sort] = data
    end
	self:init()
end

function UILandHallRoomsForTs:init()
	self:loadCsbFile()
	self:initRoomNodes()
	self:initPlayerInfoNode()
	self:onPlayerInfoUpdated()
	self:initButtons()
	addMsgCallBack(self,MSG_ENTER_GAME_PROTAL_NTY,handler(self,self.updateRoomNodes))
    addMsgCallBack(self,  MSG_PLAYER_UPDATE_SUCCESS, handler(self, self.onPlayerInfoUpdated))
	ToolKit:registDistructor( self, handler(self, self.onDestory) )
end

-- 加载csb 文件
function UILandHallRoomsForTs:loadCsbFile()
	self.m_pMainNode = cc.CSLoader:createNode("land_common_cs/land_hall_for_ts_layer.csb")
    self:addChild(self.m_pMainNode)
    self.m_pMainNode:getChildByName("bg_Image"):setVisible( false )

    local temp = self.m_pMainNode:getChildren()
    for i=1,#temp do
        temp[i]:setPositionX(temp[i]:getPositionX()*display.scaleX)
        temp[i]:setPositionY(temp[i]:getPositionY()*display.scaleY)
    end 
end

function UILandHallRoomsForTs:initRoomNodes()
	self.contentNode = self.m_pMainNode:getChildByName("content_node")
	self.rooNodes = {}
    for i=1,4 do
    	self.rooNodes[i] = {}
    	local rooNodeName = string.format("room_node_%d",i)
        self.rooNodes[i].roomNode = self.contentNode:getChildByName(rooNodeName)
        self.rooNodes[i].roomNode:setVisible( false )
        self.rooNodes[i].roomBtn = self.rooNodes[i].roomNode:getChildByName("room_btn")
        self.rooNodes[i].roomBtn:addTouchEventListener( handler(self ,self.OnBtnOperationCallBack) )
        self.rooNodes[i].personNumLabel = self.rooNodes[i].roomNode:getChildByName("person_num_label")
        self.rooNodes[i].baseScoreLabel = self.rooNodes[i].roomNode:getChildByName("base_score_label")
        local fuctionItemData = self.fuctionItemDatas[i]
        if fuctionItemData then
        	self.rooNodes[i].roomNode:setVisible( true )
            self.rooNodes[i].roomBtn:setTag( fuctionItemData.funcId )
            local personNum = self:getPersonNum( fuctionItemData.m_attrList1  )
            self.rooNodes[i].personNumLabel:setString( personNum )
            local roomSettingData  = RoomData:getRoomDataById( fuctionItemData.funcId )
            local baseScore = self:getCellSocre( roomSettingData.roomMinScore )
            self.rooNodes[i].baseScoreLabel:setString( baseScore )
        end
    end  
end

function  UILandHallRoomsForTs:updateRoomNodes( msgName , __info )
    if __info.m_portalId ~= 110100 then 
       return 
    end
    self.fuctionItemDatas = {}
    for k,v in pairs( __info.m_portalList ) do
        local data = RoomData:getGameDataById(v.m_portalId)
        data.m_attrList1 = v.m_attrList1
        self.fuctionItemDatas[v.m_sort] = data
    end

    for i=1,4 do
       local fuctionItemData = self.fuctionItemDatas[i]
       if fuctionItemData then
           self.rooNodes[i].roomNode:setVisible( true )
           self.rooNodes[i].roomBtn:setTag( fuctionItemData.funcId )
           local personNum = self:getPersonNum( fuctionItemData.m_attrList1  )
           self.rooNodes[i].personNumLabel:setString( personNum )
           local roomSettingData  = RoomData:getRoomDataById( fuctionItemData.funcId )
           local baseScore = self:getCellSocre( roomSettingData.roomMinScore )
           self.rooNodes[i].baseScoreLabel:setString( baseScore )
       else
           self.rooNodes[i].roomNode:setVisible( false )
       end
    end
    
end

function  UILandHallRoomsForTs:OnBtnOperationCallBack( sender, eventType)
	if sender and sender:getTag()then
	    local tag = sender:getTag()
	    if eventType == ccui.TouchEventType.ended then
            REQ_ENTER_SCENE( tag )
	    end
	end
end

function UILandHallRoomsForTs:getCellSocre( roomMinScore)
    local  cellScore = 1
    local josn = json.decode( roomMinScore )
    -- dump(josn)
    for k,v in pairs(josn) do
        cellScore = k 
    end
    return cellScore
end

function UILandHallRoomsForTs:getPersonNum( attrList1 )
	local onlineNum = 0
	for k,v in pairs( attrList1 ) do
		if v.m_pid == 1 then
           onlineNum = v.m_value
           break
		end
	end
    return onlineNum
end

function UILandHallRoomsForTs:initPlayerInfoNode()
	local bottomNode = self.m_pMainNode:getChildByName("bottom_node")
    self.headBg = bottomNode:getChildByName("head_bg")
    local _head_bg_size = self.headBg:getContentSize()
    local faceSize = cc.size( 90,90 )
    self._img_head = UserCenterHeadIcon.new( { _size = faceSize })
    self._img_head:setAnchorPoint(cc.p(0.5,0.5))
  
    local ClipperRound = require("app.hall.base.ui.HeadIconClipperArea")  
    local _clipNode  = ClipperRound:clipperHead("#land_dt_tx_zz.png",  self._img_head)
    _clipNode:setScale( 0.99)

    self.headBg:addChild(_clipNode)
    _clipNode:setPosition(_head_bg_size.width*0.5,_head_bg_size.height*0.5 + 1)
    
    -- 金币数量
    self.goldNumLabel = bottomNode:getChildByName("gold_num_label")
    self.goldNumLabel:setString( 0 )
    
    -- 名字
    self.palyerNameLabel = bottomNode:getChildByName("username_label")
    self.palyerNameLabel:setFontName("Helvetica")
    
    local addButton = bottomNode:getChildByName("add_panel")
    addButton:addTouchEventListener(handler(self ,self.OnBtnNameCallBack) )
end

function  UILandHallRoomsForTs:onPlayerInfoUpdated()
    self.palyerNameLabel:setString( ToolKit:shorterString(Player:getNickName(),7))
    self.goldNumLabel:setString(Player:getGoldCoin())
    self._img_head:updateTexture(Player:getFaceID())
end

function UILandHallRoomsForTs:initButtons()
	-- 商城
    local bottomRightNode = self.m_pMainNode:getChildByName("bottom_right_node")
    local shopBtn = bottomRightNode:getChildByName("shop_btn")
    shopBtn:addTouchEventListener(handler(self ,self.OnBtnNameCallBack) )

    -- 设置
    local topRightNode = self.m_pMainNode:getChildByName("top_right_node")
    topRightNode:setVisible(false)
    local setBtn = topRightNode:getChildByName("set_btn")
    setBtn:addTouchEventListener(handler(self ,self.OnBtnNameCallBack) )
end
function  UILandHallRoomsForTs:OnBtnNameCallBack( sender, eventType)
    if sender and sender:getName()then
	    local name = sender:getName()
	    if eventType == ccui.TouchEventType.ended then
           if name == "add_panel" then
               sendMsg(MSG_GOTO_STACK_LAYER, {layer = "app.hall.mall.view.MallMainLayer",  funcId = GlobalDefine.FUNC_ID.GOLD_RECHARGE })
           elseif name == "set_btn" then
              sendMsg(MSG_GOTO_STACK_LAYER, {layer = "app.hall.set.view.NewSettingLayer" })
           elseif name == "shop_btn" then
               sendMsg(MSG_GOTO_STACK_LAYER, {layer = "app.hall.mall.view.MallMainLayer",  funcId = GlobalDefine.FUNC_ID.GOLD_RECHARGE })
           end
	    end
	end
end


function UILandHallRoomsForTs:goBack()
   print("UILandHallRoomsForTs:goBack()")
   ToolKit:ShowExitGame()
end

function UILandHallRoomsForTs:onDestory()
	removeMsgCallBack(self, MSG_ENTER_GAME_PROTAL_NTY)
	removeMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS )
	
end

return UILandHallRoomsForTs