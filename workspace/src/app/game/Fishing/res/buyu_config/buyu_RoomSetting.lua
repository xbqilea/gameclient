local config ={
 [106001]={nMinScore=1,nMaxScore=57,arrayScoreSection={1,10000},nRoomType=4,nAcer=1,isNewHand=1, minLimitGold = 0 },
 [106002]={nMinScore=25,nMaxScore=57,arrayScoreSection={100,10000},nRoomType=4,nAcer=1,isNewHand=0,minLimitGold = 500000},
 [106003]={nMinScore=41,nMaxScore=57,arrayScoreSection={1000,10000},nRoomType=4,nAcer=1,isNewHand=0,minLimitGold = 1000000},
 [106004]={nMinScore=1,nMaxScore=57,arrayScoreSection={1000,100000},nRoomType=4,nAcer=1,isNewHand=1,minLimitGold = 2000000},
 [106090]={nMinScore=1,nMaxScore=57,arrayScoreSection={1,10000},nRoomType=7,nAcer=1,isNewHand=1,minLimitGold = 0},
 [106101]={nMinScore=1,nMaxScore=57,arrayScoreSection={1,10000},nRoomType=5,nAcer=0,isNewHand=0,minLimitGold = 0},
 [106102]={nMinScore=1,nMaxScore=57,arrayScoreSection={1,10000},nRoomType=8,nAcer=0,isNewHand=0,minLimitGold = 0},
 }
return config