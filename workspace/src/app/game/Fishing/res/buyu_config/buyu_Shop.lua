local config ={
 {nIndex=1,nItemId=3123,name="恶魔之炎(10000倍)",validTime="3天",comment="恶魔的黑炎吞噬万物，发炮频率每秒5.5发，炮弹飞行速度飞快",szItemSource="fish_icon_evil.png",nCount=1,nCoin=1,nPrice=50000,},
 {nIndex=2,nItemId=3120,name="天使之怒(5000倍)",validTime="3天",comment="天使的怒火净化一切，发炮频率每秒5.5发，炮弹飞行速度飞快",szItemSource="fish_icon_angell.png",nCount=1,nCoin=1,nPrice=30000,},
 {nIndex=3,nItemId=3117,name="财神炮(1000倍)",validTime="3天",comment="财神的眷顾永伴吾身，发炮频率每秒5发，炮弹飞行速度较快",szItemSource="fish_icon_mammon.png",nCount=1,nCoin=1,nPrice=10000,},
 {nIndex=4,nItemId=3114,name="烈焰金刚(500倍)",validTime="3天",comment="烈火雄雄，焚尽海域，发炮频率每秒4.5发，炮弹飞行速度较快",szItemSource="fish_icon_vajra.png",nCount=1,nCoin=1,nPrice=8000,},
 {nIndex=5,nItemId=3111,name="魅影极光(100倍)",validTime="3天",comment="通体翠绿，仿佛鬼魅般神秘，发炮频率每秒4.5发，炮弹飞行速度略快",szItemSource="fish_icon_phantom.png",nCount=1,nCoin=1,nPrice=5000,},
 {nIndex=6,nItemId=3108,name="雷神炮(70倍)",validTime="3天",comment="发炮时如雷霆震怒，发炮频率每秒4发，炮弹飞行速度略快",szItemSource="fish_icon_thor.png",nCount=1,nCoin=1,nPrice=3000,},
 {nIndex=7,nItemId=3105,name="风神炮(40倍)",validTime="3天",comment="传说中风神铸造的炮台，发炮频率每秒4发，炮弹飞行速度一般",szItemSource="fish_icon_aeolus.png",nCount=1,nCoin=1,nPrice=2000,},
 {nIndex=8,nItemId=3998,name="冰冻弹X1",validTime="永久",comment="可使用冰冻技能，对场上的鱼造成全屏冰冻的效果，持续8秒",szItemSource="fish_icon_frozen.png",nCount=1,nCoin=1,nPrice=5000,},
 {nIndex=9,nItemId=3998,name="冰冻弹X10",validTime="永久",comment="可使用冰冻技能，对场上的鱼造成全屏冰冻的效果，持续8秒",szItemSource="fish_icon_frozen.png",nCount=10,nCoin=1,nPrice=50000,},
 {nIndex=10,nItemId=3998,name="冰冻弹X100",validTime="永久",comment="可使用冰冻技能，对场上的鱼造成全屏冰冻的效果，持续8秒",szItemSource="fish_icon_frozen.png",nCount=100,nCoin=1,nPrice=500000,},
 {nIndex=11,nItemId=3997,name="瞄准镜X1",validTime="永久",comment="可使用锁定技能，无视阻挡自动对选中的鱼发炮，持续20秒",szItemSource="fish_icon_aim.png",nCount=1,nCoin=1,nPrice=5000,},
 {nIndex=12,nItemId=3997,name="瞄准镜X10",validTime="永久",comment="可使用锁定技能，无视阻挡自动对选中的鱼发炮，持续20秒",szItemSource="fish_icon_aim.png",nCount=10,nCoin=1,nPrice=50000,},
 {nIndex=13,nItemId=3997,name="瞄准镜X100",validTime="永久",comment="可使用锁定技能，无视阻挡自动对选中的鱼发炮，持续20秒",szItemSource="fish_icon_aim.png",nCount=100,nCoin=1,nPrice=500000,},
 {nIndex=14,nItemId=3996,name="狂暴X1",validTime="永久",comment="可使用狂暴技能，发射炮弹速度增加，并可能造成暴击效果，持续15秒",szItemSource="fish_icon_wild.png",nCount=1,nCoin=1,nPrice=5000,},
 {nIndex=15,nItemId=3996,name="狂暴X10",validTime="永久",comment="可使用狂暴技能，发射炮弹速度增加，并可能造成暴击效果，持续15秒",szItemSource="fish_icon_wild.png",nCount=10,nCoin=1,nPrice=50000,},
 {nIndex=16,nItemId=3996,name="狂暴X100",validTime="永久",comment="可使用狂暴技能，发射炮弹速度增加，并可能造成暴击效果，持续15秒",szItemSource="fish_icon_wild.png",nCount=100,nCoin=1,nPrice=500000,},
 }
return config