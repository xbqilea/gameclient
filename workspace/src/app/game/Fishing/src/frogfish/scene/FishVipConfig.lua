--捕鱼炮台的配置文件
--by xiaoyema

local FishRes       = require("game.frogfish.scene.FishSceneRes")
local FishVipConfig = {}

FishVipConfig.Pao_List = {
	[0] = {
		gun_weapon = {
	        m_id = 41000, --炮台ID
	        m_name = "默认炮台",  --炮台名称
	        m_bullet_icon = "zidan00.png", --炮台名称
	        m_bullet_kb_icon = 0.6,  --狂暴子弹
		},
        gun_tag_icon_res = "hall/plist/vip/icon_big_VIP0.png", --fishGunCellView左上角图标
        gun_name_res = "vip-text-lev0.png", --炮的名称
        gun_get_tip = "初始默认获得", --炮获得途径
        gun_normal_res = "hall/plist/vip/gui-image-pao-0.png", --炮解锁后的资源
        gun_dark_res = "vip-pao0-dark.png", --炮解锁前的资源
        gun_scale = 1.0, --炮的缩放比例
        gun_spine_res = FishRes.SPINE_OF_PAO_0, --炮的spine动画
        gun_net_spine_res = FishRes.SPINE_OF_NET_0, --炮的网动画
        gun_sound_res = FishRes.SOUND_OF_FIRE_VIP0, --炮的音效
        gun_describe = "祖传捕鱼炮，便宜而又结实耐用", --炮的描述
        gun_open_func = nil, --是否有特殊开启条件
    },
    [1] = {
    	gun_weapon = {
	        m_id = 41001, --炮台ID
	        m_name = "极光新星",  --炮台名称
	        m_bullet_icon = "zidan01.png", --炮台名称
	        m_bullet_kb_icon = 0.75,  --狂暴子弹
		},
        gun_tag_icon_res = "hall/plist/vip/icon_big_VIP1.png",
        gun_name_res = "vip-text-lev1.png",
        gun_get_tip = "VIP1解锁",
        gun_normal_res = "hall/plist/vip/gui-image-pao-1.png",
        gun_dark_res = "vip-pao1-dark.png",
        gun_scale = 1.0,
        gun_spine_res = FishRes.SPINE_OF_PAO_1,
        gun_net_spine_res = FishRes.SPINE_OF_NET_1,
        gun_sound_res = FishRes.SOUND_OF_FIRE_VIP1,
        gun_describe = "捕鱼协会的收藏品，很多捕鱼达人都用过他", 
        gun_open_func = nil,
    },
    [2] = {
    	gun_weapon = {
	        m_id = 41002, --炮台ID
	        m_name = "能量脉冲",  --炮台名称
	        m_bullet_icon = "zidan02.png", --炮台名称
	        m_bullet_kb_icon = 0.8,  --狂暴子弹
		},
        gun_tag_icon_res = "hall/plist/vip/icon_big_VIP2.png",
        gun_name_res = "vip-text-lev2.png",
        gun_get_tip = "VIP2解锁",
        gun_normal_res = "hall/plist/vip/gui-image-pao-2.png", 
        gun_dark_res = "vip-pao2-dark.png",
        gun_scale = 1.0,
        gun_spine_res = FishRes.SPINE_OF_PAO_2,
        gun_net_spine_res = FishRes.SPINE_OF_NET_2,
        gun_sound_res = FishRes.SOUND_OF_FIRE_VIP2,
        gun_describe = "利用高能脉冲发射炮弹，不受深海环境影响", 
        gun_open_func = nil,
    },
    [3] = {
    	gun_weapon = {
	        m_id = 41003, --炮台ID
	        m_name = "狂暴紫电",  --炮台名称
	        m_bullet_icon = "zidan03.png", --炮台名称
	        m_bullet_kb_icon = 0.9,  --狂暴子弹
		},
        gun_tag_icon_res = "hall/plist/vip/icon_big_VIP3.png",
        gun_name_res = "vip-text-lev3.png",
        gun_get_tip = "VIP3解锁",
        gun_normal_res = "hall/plist/vip/gui-image-pao-3.png",
        gun_dark_res = "vip-pao3-dark.png",
        gun_scale = 1.0,
        gun_spine_res = FishRes.SPINE_OF_PAO_3,
        gun_net_spine_res = FishRes.SPINE_OF_NET_3,
        gun_sound_res = FishRes.SOUND_OF_FIRE_VIP3,
        gun_describe = "大发明家特斯拉全力打造的超级电磁炮", 
        gun_open_func = nil,
    },
    [4] = {
    	gun_weapon = {
	        m_id = 41004, --炮台ID
	        m_name = "重装矩阵",  --炮台名称
	        m_bullet_icon = "zidan04.png", --炮台名称
	        m_bullet_kb_icon = 0.9,  --狂暴子弹
		},
        gun_tag_icon_res = "hall/plist/vip/icon_big_VIP4.png",
        gun_name_res = "vip-text-lev4.png",
        gun_get_tip = "VIP4解锁",
        gun_normal_res = "hall/plist/vip/gui-image-pao-4.png",
        gun_dark_res = "vip-pao4-dark.png",
        gun_scale = 0.89,
        gun_spine_res = FishRes.SPINE_OF_PAO_4,
        gun_net_spine_res = FishRes.SPINE_OF_NET_4,
        gun_sound_res = FishRes.SOUND_OF_FIRE_VIP4,
        gun_describe = "据说是由变形金刚领袖擎天柱的手枪改造而成", 
        gun_open_func = nil,
    },
    [5] = {
    	gun_weapon = {
	        m_id = 41005, --炮台ID
	        m_name = "爆裂熔岩",  --炮台名称
	        m_bullet_icon = "zidan05.png", --炮台名称
	        m_bullet_kb_icon = 0.95,  --狂暴子弹
		},
        gun_tag_icon_res = "hall/plist/vip/icon_big_VIP5.png",
        gun_name_res = "vip-text-lev5.png",
        gun_get_tip = "VIP5解锁",
        gun_normal_res = "hall/plist/vip/gui-image-pao-5.png",
        gun_dark_res = "vip-pao5-dark.png",
        gun_scale = 0.89,
        gun_spine_res = FishRes.SPINE_OF_PAO_5,
        gun_net_spine_res = FishRes.SPINE_OF_NET_5,
        gun_sound_res = FishRes.SOUND_OF_FIRE_VIP5,
        gun_describe = "大炮发射来自地心的炙热岩浆，寸草不生", 
        gun_open_func = nil,
    },
    [6] = {
    	gun_weapon = {
	        m_id = 41006, --炮台ID
	        m_name = "幻影幽灵",  --炮台名称
	        m_bullet_icon = "zidan06.png", --炮台名称
	        m_bullet_kb_icon = 1.0,  --狂暴子弹
		},
        gun_tag_icon_res = "hall/plist/vip/icon_big_VIP6.png",
        gun_name_res = "vip-text-lev6.png",
        gun_get_tip = "VIP6解锁",
        gun_normal_res = "hall/plist/vip/gui-image-pao-6.png",
        gun_dark_res = "vip-pao6-dark.png",
        gun_scale = 0.89,
        gun_spine_res = FishRes.SPINE_OF_PAO_6, 
        gun_net_spine_res = FishRes.SPINE_OF_NET_6,
        gun_sound_res = FishRes.SOUND_OF_FIRE_VIP6,
        gun_describe = "深海幽灵船的主炮，填装了可怕的鬼魂炮弹", 
        gun_open_func = nil,
    },
    [7] = {
    	gun_weapon = {
	        m_id = 41007, --炮台ID
	        m_name = "苍穹之翼",  --炮台名称
	        m_bullet_icon = "zidan07.png", --炮台名称
	        m_bullet_kb_icon = 1.0,  --狂暴子弹
		},
        gun_tag_icon_res = "hall/plist/vip/icon_big_VIP7.png",
        gun_name_res = "vip-text-lev7.png",
        gun_get_tip = "VIP7解锁",
        gun_normal_res = "hall/plist/vip/gui-image-pao-7.png",
        gun_dark_res = "vip-pao7-dark.png",
        gun_scale = 0.88,
        gun_spine_res = FishRes.SPINE_OF_PAO_7,
        gun_net_spine_res = FishRes.SPINE_OF_NET_7,
        gun_sound_res = FishRes.SOUND_OF_FIRE_VIP7,
        gun_describe = "浮空战舰身为天空的主宰，舰炮捕鱼也很犀利", 
        gun_open_func = nil,
    },
    [8] = {
    	gun_weapon = {
	        m_id = 41008, --炮台ID
	        m_name = "烈焰凤凰",  --炮台名称
	        m_bullet_icon = "zidan08.png", --炮台名称
	        m_bullet_kb_icon = 1.0,  --狂暴子弹
		},
        gun_tag_icon_res = "hall/plist/vip/icon_big_VIP8.png",
        gun_name_res = "vip-text-lev8.png",
        gun_get_tip = "VIP8解锁",
        gun_normal_res = "hall/plist/vip/gui-image-pao-8.png",
        gun_dark_res = "vip-pao8-dark.png",
        gun_scale = 0.82,
        gun_spine_res = FishRes.SPINE_OF_PAO_8,
        gun_net_spine_res = FishRes.SPINE_OF_NET_8,
        gun_sound_res = FishRes.SOUND_OF_FIRE_VIP8,
        gun_describe = "装饰有凤凰羽毛的大炮，能够发射凤凰真火", 
        gun_open_func = nil,
    },
    [9] = {
    	gun_weapon = {
	        m_id = 41009, --炮台ID
	        m_name = "地狱魔龙",  --炮台名称
	        m_bullet_icon = "zidan09.png", --炮台名称
	        m_bullet_kb_icon = 1.0,  --狂暴子弹
		},
        gun_tag_icon_res = "hall/plist/vip/icon_big_VIP9.png",
        gun_name_res = "vip-text-lev9.png",
        gun_get_tip = "VIP9解锁",
        gun_normal_res = "hall/plist/vip/gui-image-pao-9.png",
        gun_dark_res = "vip-pao9-dark.png",
        gun_scale = 0.82,
        gun_spine_res = FishRes.SPINE_OF_PAO_9,
        gun_net_spine_res = FishRes.SPINE_OF_NET_9,
        gun_sound_res = FishRes.SOUND_OF_FIRE_VIP9,
        gun_describe = "猛烈的炮火如同来自地狱的魔龙吐息..", 
        gun_open_func = nil,
    },
    [10] = {
    	gun_weapon = {
	        m_id = 41010, --炮台ID
	        m_name = "神圣天使",  --炮台名称
	        m_bullet_icon = "zidan10.png", --炮台名称
	        m_bullet_kb_icon = 1.0,  --狂暴子弹
		},
        gun_tag_icon_res = "hall/plist/vip/icon_big_VIP10.png",
        gun_name_res = "vip-text-lev10.png",
        gun_get_tip = "VIP10解锁",
        gun_normal_res = "hall/plist/vip/gui-image-pao-10.png",
        gun_dark_res = "vip-pao10-dark.png",
        gun_scale = 0.82,
        gun_spine_res = FishRes.SPINE_OF_PAO_10,
        gun_net_spine_res = FishRes.SPINE_OF_NET_10,
        gun_sound_res = FishRes.SOUND_OF_FIRE_VIP10,
        gun_describe = "传说中的神器大炮，蕴含着天使的审判之力", 
        gun_open_func = nil,
    },
    --周卡炮
    [11] = {
    	gun_weapon = {
	        m_id = 41011, --炮台ID
	        m_name = "流星射手",  --炮台名称
	        m_bullet_icon = "zidan_zhouka.png", --炮台名称
	        m_bullet_kb_icon = 1.0,  --狂暴子弹
		},
        gun_tag_icon_res = "hall/plist/vip/icon_big_zhouka.png", --fishGunCellView左上角图标
        gun_name_res = "vip-text-zhouka.png", --炮的名称
        gun_get_tip = "购买周卡解锁7天", --炮获得途径
        gun_normal_res = "hall/plist/vip/gui-image-pao-zhouka.png", --炮解锁后的资源
        gun_dark_res = "vip-pao-dark-zhouka.png", --炮解锁前的资源
        gun_scale = 1.0,
        gun_spine_res = FishRes.SPINE_OF_PAO_ZHOUKA,
        gun_net_spine_res = FishRes.SPINE_OF_NET_ZHOUKA,
        gun_sound_res = FishRes.SOUND_OF_FIRE_ZHOUKA,
        gun_describe = "周卡玩家的限时特权捕鱼炮", 
        gun_open_func = function ()
        	return PlayerInfo.getInstance():get7DayIsOpen()
        end,
    },
    --月卡炮
    [12] = {
    	gun_weapon = {
	        m_id = 41012, --炮台ID
	        m_name = "银月圣枪",  --炮台名称
	        m_bullet_icon = "zidan_yueka.png", --炮台名称
	        m_bullet_kb_icon = 1.0,  --狂暴子弹
		},
        gun_tag_icon_res = "hall/plist/vip/icon_big_yueka.png", --fishGunCellView左上角图标
        gun_name_res = "vip-text-yueka.png", --炮的名称
        gun_get_tip = "购买月卡解锁30天", --炮获得途径
        gun_normal_res = "hall/plist/vip/gui-image-pao-yueka.png", --炮解锁后的资源
        gun_dark_res = "vip-pao-dark-yueka.png", --炮解锁前的资源
        gun_scale = 1.0,
        gun_spine_res = FishRes.SPINE_OF_PAO_YUEKA,
        gun_net_spine_res = FishRes.SPINE_OF_NET_YUEKA,
        gun_sound_res = FishRes.SOUND_OF_FIRE_YUEKA,
        gun_describe = "月卡玩家的限时特权捕鱼炮", 
        gun_open_func = function ()
        	return PlayerInfo.getInstance():get30DayIsOpen()
        end,

    },
}

return FishVipConfig
