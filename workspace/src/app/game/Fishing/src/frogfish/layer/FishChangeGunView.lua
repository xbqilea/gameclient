--region *.lua
--Date
--
--endregion

local FishRes           = require("game.frogfish.scene.FishSceneRes")
local FishDataMgr       = require("game.frogfish.manager.FishDataMgr")
local VipLayer          = require("hall.layer.VipLayer")              --会员界面
local CMsgFish          = require("game.frogfish.proxy.CMsgFish")
local FishVipConfig     = require("game.frogfish.scene.FishVipConfig")

local FishChangeGunView = class("FishChangeGunView", cc.Layer)
local FishGunCellView = require("game.frogfish.layer.FishGunCellView")

function FishChangeGunView:ctor()
    self:enableNodeEvents()
    self.m_pParentUILayer = nil
    self.CELL_WIDTH = 244
    self:init()
end

function FishChangeGunView:init()
    self:initCSB()
end 

function FishChangeGunView:onEnter()
    onOpenLayer(self.m_rootNode, self.m_shadow)
end

function FishChangeGunView:onExit()
    
end

function FishChangeGunView:initCSB()
    self.m_rootUI       = display.newNode()
    self.m_rootUI:addTo(self)
    self.m_pathUI       = cc.CSLoader:createNode("game/frogfish/csb/gui-fish-ChageGunLayer.csb")
    self.m_pathUI:addTo(self.m_rootUI)

    self.m_shadow = self.m_pathUI:getChildByName("Panel_color")
    self.m_rootNode     = self.m_pathUI:getChildByName("Panel_1")
    self.m_pBtnClose    = self.m_rootNode:getChildByName("Button_close")
    self.m_pButtonLeft = self.m_rootNode:getChildByName("Button_left")
    self.m_pButtonRight= self.m_rootNode:getChildByName("Button_right")

    self.m_pScrollViewNode       = self.m_rootNode:getChildByName("Panel_gun_scrollview")
    self.m_pNodeUsingGun         = self.m_rootNode:getChildByName("Node_using_gun")
    self.m_pTextGunDescrib       = self.m_pNodeUsingGun:getChildByName("Text_gun_describ")
    self.m_pSpriteUsingGun       = self.m_pNodeUsingGun:getChildByName("Sprite_using_gun")
    self.m_pSpriteUsingVip       = self.m_pNodeUsingGun:getChildByName("Sprite_name_bg"):getChildByName("Sprite_using_vip")
    self.m_pSpriteUsingName      = self.m_pNodeUsingGun:getChildByName("Sprite_name_bg"):getChildByName("Sprite_using_name")
    self.m_pTextMonth = self.m_rootNode:getChildByName("month_txt")

    self.m_pButtonSwallow = self.m_pathUI:getChildByName("ButtonSwallow")
    self.m_pButtonSwallow:addClickEventListener(handler(self, self.onReturnClicked))

    self.m_pBtnClose:addClickEventListener(handler(self, self.onReturnClicked))
    self.m_pButtonLeft:addClickEventListener(handler(self, self.onButtonLeftClicked))
    self.m_pButtonRight:addClickEventListener(handler(self, self.onButtonRightClicked))

    self.m_pTableView = cc.TableView:create(cc.size(self.m_pScrollViewNode:getContentSize().width, self.m_pScrollViewNode:getContentSize().height))
    self.m_pTableView:setIgnoreAnchorPointForPosition(false)
    self.m_pTableView:setAnchorPoint(cc.p(0,0))
    self.m_pTableView:setPosition(cc.p(0, 0))
    self.m_pTableView:setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN)
    self.m_pTableView:setDirection(cc.SCROLLVIEW_DIRECTION_HORIZONTAL)
    self.m_pTableView:setDelegate()
    self.m_pTableView:registerScriptHandler(handler(self,self.scrollViewDidScroll), CCTableView.kTableViewScroll)
    self.m_pTableView:registerScriptHandler(handler(self,self.cellSizeForTable), CCTableView.kTableCellSizeForIndex)
    self.m_pTableView:registerScriptHandler(handler(self,self.tableCellAtIndex), CCTableView.kTableCellSizeAtIndex)
    self.m_pTableView:registerScriptHandler(handler(self,self.numberOfCellsInTableView), CCTableView.kNumberOfCellsInTableView)
    self.m_pTableView:registerScriptHandler(handler(self,self.tableCellTouched), CCTableView.kTableCellTouched)
    self.m_pScrollViewNode:addChild(self.m_pTableView)
    self.m_pTableView:reloadData()
     
    self.m_pTableView:setContentSize( self.m_pTableView:getContentSize().width + 30, self.m_pTableView:getContentSize().height )
    self.m_maxScrollWidth = self.m_pTableView:getContentSize().width  --最大滑动宽度
    self.m_viewWidth = self.m_pScrollViewNode:getContentSize().width  --滑动框宽度

    local seq = cc.Sequence:create(cc.MoveBy:create(0.4, cc.p(-8,0)),cc.MoveBy:create(0.4, cc.p(8,0)))
    self.m_pButtonLeft:runAction(cc.RepeatForever:create(seq))

    local seq2 = cc.Sequence:create(cc.MoveBy:create(0.4, cc.p(8,0)),cc.MoveBy:create(0.4, cc.p(-8,0)))
    self.m_pButtonRight:runAction(cc.RepeatForever:create(seq2))

    self:initMyUsingGun()
end

function FishChangeGunView:scrollViewDidScroll(pView)
    local offset = self.m_pTableView:getContentOffset()
    if offset.x >= 0 then
        self.m_pButtonLeft:setVisible(false)
        self.m_pButtonRight:setVisible(true)
    elseif offset.x <= self.m_viewWidth - self.m_maxScrollWidth then
        self.m_pButtonLeft:setVisible(true)
        self.m_pButtonRight:setVisible(false)
    end
   -- print( string.format( "_____scrollViewDidScroll: (x: %d , y: %d)", offset.x, offset.y ))
end

function FishChangeGunView:cellSizeForTable(table, idx)
    return self.CELL_WIDTH, 350
end

function FishChangeGunView:tableCellAtIndex(table, idx)
    local cell = table:cellAtIndex(idx)
    if not cell then
        cell = cc.TableViewCell:new()
    else
        cell:removeAllChildren()
    end
    
    local item = FishGunCellView.new()
    item:initCellData( idx, self )
    item:setPosition(cc.p(20, -13))
    item:addTo(cell)

    return cell
end

function FishChangeGunView:numberOfCellsInTableView(tables)
    local list_len = table.nums(FishVipConfig.Pao_List)
    return list_len
end

function FishChangeGunView:tableCellTouched(table, cell)
end

--初始化自己使用的炮台
function FishChangeGunView:initMyUsingGun()
    local myVip = PlayerInfo.getInstance():getVipLevel()
    local myUseVip = FishDataMgr:getInstance():getWeaponIDByIndex( PlayerInfo.getInstance():getChairID() ) - cc.exports.GUN_ID_MIN
    if myUseVip < 0 then
        myUseVip = 0
    end
    if self.m_pTextGunDescrib then
        self.m_pTextGunDescrib:setString(FishVipConfig.Pao_List[myUseVip].gun_describe)
    end

    if self.m_pSpriteUsingGun then
       self.m_pSpriteUsingGun:setSpriteFrame(FishVipConfig.Pao_List[myUseVip].gun_normal_res)
    end

    if self.m_pSpriteUsingVip then
        self.m_pSpriteUsingVip:setSpriteFrame(FishVipConfig.Pao_List[myUseVip].gun_tag_icon_res)
    end

    if self.m_pSpriteUsingName then
        self.m_pSpriteUsingName:setSpriteFrame(FishVipConfig.Pao_List[myUseVip].gun_name_res)
    end

    self:JudgeMonthGun(myUseVip)

    local offset = self.m_pTableView:getContentOffset()
    local size = self.m_pTableView:getContentSize()
    local sizeMove = self.CELL_WIDTH * myUseVip
    local sizeView = self.m_pScrollViewNode:getContentSize().width
    if 0 - (offset.x - sizeMove - sizeView) < size.width then
        offset.x = offset.x - sizeMove
    else
        offset.x = sizeView - size.width
    end
    self.m_pTableView:setContentOffset(offset, false)
end

function FishChangeGunView:JudgeMonthGun(gun_index)
    self.m_pTextMonth:show()
    if gun_index == 11 then
        local leave_str = LuaUtils.getCardDateStr(PlayerInfo.getInstance():get7DayLeaveTime(),false,"到期")
        self.m_pTextMonth:setString(leave_str)
    elseif gun_index == 12 then
        local leave_str = LuaUtils.getCardDateStr(PlayerInfo.getInstance():get30DayLeaveTime(),false,"到期")
        self.m_pTextMonth:setString(leave_str)
    else
        self.m_pTextMonth:hide()
    end
end



function FishChangeGunView:onReturnClicked()
    AudioManager.getInstance():playSound(FishRes.SOUND_OF_CLOSE)

    onCloseLayer(self.m_rootNode, self.m_shadow, self)
end

function FishChangeGunView:onButtonLeftClicked()
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")

    local offset = self.m_pTableView:getContentOffset()
    local sizeMove = self.CELL_WIDTH

    if offset.x + sizeMove < 0 then
        offset.x = offset.x + sizeMove
    else
        offset.x = 0
    end

    self.m_pTableView:setContentOffset(offset, true)
end

function FishChangeGunView:onButtonRightClicked()
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")

    local offset = self.m_pTableView:getContentOffset()
    local size = self.m_pTableView:getContentSize()
    local sizeMove = self.CELL_WIDTH
    local sizeView = self.m_pScrollViewNode:getContentSize().width
    if 0 - (offset.x - sizeMove - sizeView) < size.width then
        offset.x = offset.x - sizeMove
    else
        offset.x = sizeView - size.width
    end
    self.m_pTableView:setContentOffset(offset, true)
end


function FishChangeGunView:onButtonChangeGunClicked( gunIndex )
    if gunIndex == nil then
        return
    end
    if self.m_pParentUILayer ~= nil then 
        AudioManager.getInstance():playSound(FishRes.SOUND_OF_CHANGEGUN) 
        local gunID = cc.exports.GUN_ID_MIN + gunIndex      
        self.m_pParentUILayer:gunChangeToCurGun(gunID)
        --保存当前使用的炮台vip等级
        cc.UserDefault:getInstance():setStringForKey("using_vip_level" .. PlayerInfo.getInstance():getUserID(), gunIndex )
        cc.UserDefault:getInstance():flush()
        CMsgFish.getInstance():sendChangeGun( gunIndex )
    end
    onCloseLayer(self.m_rootNode, self.m_shadow, self)
end


function FishChangeGunView:onButtonGetGunClicked( gunIndex )
   AudioManager.getInstance():playSound(FishRes.SOUND_OF_BUTTON)
    --print("onButtonGetGunClicked:" .. gunIndex )  
    if self.m_pParentUILayer ~= nil then  
        local pVipLayer = VipLayer.new()
        pVipLayer:setName("VipLayer")
        pVipLayer:updatePageViewbyIndex(gunIndex - 1)
        pVipLayer:addTo(self.m_pParentUILayer:getParent():getParent(), Z_ORDER_TOP)
    end
end
return  FishChangeGunView
