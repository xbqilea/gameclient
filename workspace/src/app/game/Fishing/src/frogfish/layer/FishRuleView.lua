-- region FishRuleVie.lua
-- Date 2017.11.15
-- Auther JackXu.
-- Desc 游戏规则弹框 Layer.

local FishRuleVie = class("FishRuleVie", cc.exports.FixLayer)

function FishRuleVie:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()
    self:init()
end

function FishRuleVie:init()
    self:initCSB()
end

function FishRuleVie:onEnter()
    self.super:onEnter()

    self:setTargetShowHideStyle(self, self.SHOW_DLG_BIG, self.HIDE_DLG_BIG)
    self:showWithStyle()
end

function FishRuleVie:onExit()
    self.super:onExit()
end

function FishRuleVie:initCSB()

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    --csb
    self.m_pUiLayer = cc.CSLoader:createNode("game/frogfish/csb/gui-fish-ruleLayer.csb")
    self.m_pUiLayer:addTo(self.m_rootUI, Z_ORDER_TOP)
    self.m_pLayerBase = self.m_pUiLayer:getChildByName("Layer_base")
    self.m_pScrollView = self.m_pLayerBase:getChildByName("ScrollView")
    self.m_pBtnClose = self.m_pLayerBase:getChildByName("Button_close")
    self.m_pBtnRule1 = self.m_pLayerBase:getChildByName("Button_1")
    self.m_pBtnRule2 = self.m_pLayerBase:getChildByName("Button_2")
    self.m_pBtnRule3 = self.m_pLayerBase:getChildByName("Button_3")
    self.m_pRule1 = self.m_pLayerBase:getChildByName("ScrollView_1")
    self.m_pRule2 = self.m_pLayerBase:getChildByName("ScrollView_2")
    self.m_pRule3 = self.m_pLayerBase:getChildByName("ScrollView_3")
    self.m_pTouch = self.m_pLayerBase:getChildByName("Panel_touch")

    --滚动条
    self.m_pRule1:setScrollBarEnabled(false)
    self.m_pRule2:setScrollBarEnabled(false)
    self.m_pRule3:setScrollBarEnabled(false)
    --关闭按纽
    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pTouch:addClickEventListener(handler(self, self.onCloseClicked))
    --切换按钮
    self.m_pBtnRule1:addClickEventListener(handler(self, self.onRule1Clicked))
    self.m_pBtnRule2:addClickEventListener(handler(self, self.onRule2Clicked))
    self.m_pBtnRule3:addClickEventListener(handler(self, self.onRule3Clicked))

    self:onSwitchLayer(1)
end

function FishRuleVie:onCloseClicked()
    AudioManager.getInstance():playSound("public/sound/sound-close.mp3")
    self:onMoveExitView()
end

function FishRuleVie:onRule1Clicked()
    AudioManager.getInstance():playSound("public/sound/sound-close.mp3")

    self:onSwitchLayer(1)
end

function FishRuleVie:onRule2Clicked()
    AudioManager.getInstance():playSound("public/sound/sound-close.mp3")

    self:onSwitchLayer(2)
end

function FishRuleVie:onRule3Clicked()
    AudioManager.getInstance():playSound("public/sound/sound-close.mp3")

    self:onSwitchLayer(3)
end

function FishRuleVie:onSwitchLayer(index)
    
    self.m_pBtnRule1:setEnabled(index ~= 1)
    self.m_pBtnRule2:setEnabled(index ~= 2)
    self.m_pBtnRule3:setEnabled(index ~= 3)

    self.m_pBtnRule1:setHighlighted(index == 1)
    self.m_pBtnRule2:setHighlighted(index == 2)
    self.m_pBtnRule3:setHighlighted(index == 3)

    self.m_pRule1:setVisible(index == 1)
    self.m_pRule2:setVisible(index == 2)
    self.m_pRule3:setVisible(index == 3)
end

return FishRuleVie
