-- region *.lua
-- Date
-- Desc loading 过渡页面 layer.


local FishRes         = require("game.frogfish.scene.FishSceneRes")
local FishTraceMgr    = require("game.frogfish.manager.FishTraceMgr")
local FishSceneMgr    = require("game.frogfish.manager.FishSceneMgr")    
local ResourceManager = require("game.frogfish.manager.ResourceManager")

local PATH_CSB = "game/frogfish/csb/gui-fish-loadLayer.csb"

local CommonLoading = require("common.layer.CommonLoading")
local FishLoadingView = class("FishLoadingView", CommonLoading)

function FishLoadingView.loading()
    return FishLoadingView.new(true)
end

function FishLoadingView.reload()
    return FishLoadingView.new(false)
end

function FishLoadingView:ctor(bBool)
    self:enableNodeEvents()
    self.bLoad = bBool
    self:init()
end

function FishLoadingView:init()
    self.super:init(self)
    self:initCSB()
    self:initCommonLoad()
end

function FishLoadingView:onEnter()
    self.super:onEnter()
end

function FishLoadingView:onExit()
    self.super:onExit()
end

function FishLoadingView:initCSB()

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    --ccb
    self.m_pathUI = cc.CSLoader:createNode(PATH_CSB)
    self.m_pathUI:setPositionX((display.width - 1624) / 2)
    self.m_pathUI:addTo(self.m_rootUI)

    --node
    self.m_pNodeBase = self.m_pathUI:getChildByName("Layer_base")
    self.m_pNodeBg   = self.m_pNodeBase:getChildByName("Node_bg")
    self.m_pNodeLoad = self.m_pNodeBase:getChildByName("Node_load")
    self.m_pNodeText = self.m_pNodeBase:getChildByName("Node_text")

    --bar
    self.m_pLoadingBar = self.m_pNodeLoad:getChildByName("LoadingBar")

    --text
    self.m_pLabelPercent = self.m_pNodeText:getChildByName("Text_percent")
    self.m_pLabelWord    = self.m_pNodeText:getChildByName("Text_word")
end

--新的加载方式
function FishLoadingView:initCommonLoad()
    
    -------------------------------------------------------
    --设置界面ui
    self:setLabelPercent(self.m_pLabelPercent) --百分比文字
    self:setLabelWord(self.m_pLabelWord)       --提示文字
    self:setBarPercent(self.m_pLoadingBar)     --进度条
    -------------------------------------------------------
    --其他加载项
    local func = {}
    -------------------------------------------------------
    --基础鱼阵
    for i = 1, 16 do
        table.insert(func, function()
            FishSceneMgr.getInstance():prepare_path2(i)
        end)
    end
    --配置鱼阵
    for i = 0, 7 do
        table.insert(func, function()
            FishSceneMgr.getInstance():loadOneSceneFileByIndex(i)
        end)
    end
    --0号路径
    table.insert(func, function()
        FishTraceMgr:getInstance():loadNumZeroTrace()
    end)
    --401号路径
    table.insert(func, function()
        FishTraceMgr:getInstance():createSpecailTrace()
    end)
    --基础路径
    for i = 1, 12 do
        table.insert(func, function()
            FishTraceMgr:getInstance():loadAllPathFile(i)
        end)
    end
    -------------------------------------------------------
    --音效/音乐/骨骼/动画/动画/碎图/大图/其他
    self:addLoadingList(FishRes.vecReleaseSound, self.TYPE_SOUND)
    self:addLoadingList(FishRes.vecReleaseMusic, self.TYPE_MUSIC)
    self:addLoadingList(func,                    self.TYPE_OTHER)
    self:addLoadingList(FishRes.vecReleaseAnim,  self.TYPE_EFFECT)
    self:addLoadingList(FishRes.vecReleasePlist, self.TYPE_PLIST)
    self:addLoadingList(FishRes.vecReleaseImg,   self.TYPE_PNG)
    -------------------------------------------------------
end

return FishLoadingView

-- endregion
