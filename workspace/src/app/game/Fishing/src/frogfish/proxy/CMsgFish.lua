
local FishEvent   = require("game.frogfish.scene.FishEvent")
local FishDefine  = require("game.frogfish.scene.FishDefine")
local FishDataMgr = require("game.frogfish.manager.FishDataMgr")

local Public_Events = cc.exports.Public_Events
local SLFacade      = cc.exports.SLFacade
local PlayerInfo    = cc.exports.PlayerInfo

local CMsgFish = class("CMsgFish", require("common.app.CMsgGame"))

CMsgFish.instance_ = nil
function CMsgFish:getInstance()
    if CMsgFish.instance_ == nil then
        CMsgFish.instance_ = CMsgFish.new()
    end
    return CMsgFish.instance_
end
function CMsgFish:ctor()

    self.func_game_ =
    {
        --100 框架命令 覆盖
        [G_C_CMD.MDM_GF_FRAME] = {
            [G_C_CMD.SUB_GF_GAME_SCENE]             = { func = self.loadGameSceneData,      log = "场景信息", debug = false, }, --101
        },

        --200 游戏命令
        [G_C_CMD.MDM_GF_GAME] = {
            [G_C_CMD.SUB_S_SCENE_FISH]              = { func = self.onSceneFish,             log="大闹天宫捕鱼",     debug = false, }, --100
            [G_C_CMD.SUB_S_EXCHANGE_FISHSCORE]      = { func = self.getExchangeFishScore,    log="捕鱼获得金币",     debug = false, }, --102
            [G_C_CMD.SUB_S_USER_FIRE]               = { func = self.getUserFire,             log="玩家开炮",         debug = false, }, --103
            [G_C_CMD.SUB_S_BULLET_DOUBLE_TIMEOUT]   = { func = self.onBulletDoubleTimeOut,   log="子弹超时",         debug = false, }, --104
            [G_C_CMD.SUB_S_DISTRIBUTE_FISH]         = { func = self.onDistributeFish,        log="创建鱼",           debug = false, }, --105
            [G_C_CMD.SUB_S_SWITCH_SCENE]            = { func = self.getSwitchScene,          log="切换场景",         debug = false, }, --106
            [G_C_CMD.SUB_S_CATCH_CHAIN]             = { func = self.onCatchFishChain,        log="玩家捕中鱼",       debug = false, }, --107
            [G_C_CMD.SUB_S_CATCH_FISH_GROUP]        = { func = self.onCatchFishGroup,        log="玩家捕中鱼群",     debug = false, }, --108
            [G_C_CMD.SUB_S_DISTRIBUTE_FISH_TEAM]    = { func = self.onDistributeFishTeam,    log="创建鱼群",         debug = false, }, --113
            [G_C_CMD.SUB_S_DISTRIBUTE_FISH_CIRCLE]  = { func = self.onDistributeFishCircle,  log="创建圆圈型鱼群",   debug = false, }, --114
            [G_C_CMD.SUB_S_RETURN_BULLET_SCORE]     = { func = self.onReturnBulletGold,      log="返回玩家子弹金币", debug = false, }, --115
            [G_C_CMD.SUB_S_USER_FISH_SYNC]          = { func = self.onReturnFishSync,        log="返回同步场景鱼数据", debug = false, }, --116
            [G_C_CMD.SUB_S_USER_SCORE_SYNC]         = { func = self.onUserScoreSync,         log="返回同步玩家金币",  debug = false, }, --117
            [G_C_CMD.SUB_S_USER_FIRE_ERROR]         = { func = self.onUserFireError,         log="发射子弹错误",      debug = false, }, --118
            [G_C_CMD.SUB_S_CHANGE_GUN]              = { func = self.onChangeGun,             log="修改炮台",          debug = false, }, --121
        },
    }
end

-- 游戏场景-100-101
function CMsgFish:loadGameSceneData(pData)

    --清理全局定义
    reloadLua("game.frogfish.scene.FishDefine")
    
    local datalen = pData:getReadableSize()
    local _buffer = pData:readData(datalen)

    --数据
    local pGameScene = {}
    pGameScene.is_special_scene = _buffer:readBoolean()
    pGameScene.special_sceene_waited_time = _buffer:readFloat()
    pGameScene.tick_count = _buffer:readUInt()
    pGameScene.game_config = {}
    pGameScene.game_config.exchange_ratio_userscore = _buffer:readInt() 
    pGameScene.game_config.exchange_ratio_fishscore = _buffer:readInt() 
    pGameScene.game_config.exchange_count = _buffer:readInt() 
    pGameScene.game_config.min_bullet_multiple = _buffer:readInt() 
    pGameScene.game_config.max_bullet_multiple = _buffer:readInt() 
    pGameScene.game_config.fish_speed = {}
    for i = 1, FishKind.FISH_KIND_COUNT do 
        pGameScene.game_config.fish_speed[i] = _buffer:readFloat() 
    end
    pGameScene.game_config.fish_bounding_radius = {}
    for i = 1, FishKind.FISH_KIND_COUNT do 
        pGameScene.game_config.fish_bounding_radius[i] = _buffer:readFloat() 
    end
    pGameScene.game_config.fish_bounding_count = {}
    for i = 1, FishKind.FISH_KIND_COUNT do 
        pGameScene.game_config.fish_bounding_count[i] = _buffer:readInt() 
    end
    pGameScene.game_config.bullet_speed = {}
    for i = 1,BulletKind.BULLET_KIND_COUNT do 
        pGameScene.game_config.bullet_speed[i] = _buffer:readFloat() 
    end 
    pGameScene.game_config.bullet_bounding_radius = {}
    for i = 1,BulletKind.BULLET_KIND_COUNT do 
        pGameScene.game_config.bullet_bounding_radius[i] = _buffer:readFloat() 
    end 
    pGameScene.fish_score = {}
    for i = 1,GAME_PLAYER_FISH do          
            pGameScene.fish_score[i] = _buffer:readLongLong() 
    end 
    pGameScene.exchange_fish_score = {}
    for i = 1,GAME_PLAYER_FISH do          
        pGameScene.exchange_fish_score[i] = _buffer:readLongLong() 
    end 
    
    pGameScene.use_vip_level = {}
    for i = 1,GAME_PLAYER_FISH do          
        pGameScene.use_vip_level[i] = _buffer:readUShort() 
    end 
    --保存
    FishDataMgr:getInstance():processGameScene(pGameScene)
    self:enterGameScene()

    local vipLev = FishDataMgr:getInstance():getSelfUseGun()
    self:sendChangeGun( vipLev )
    --日志
    local ret = ""
    return ret
end

-- 大闹天宫捕鱼==========================
-- CMD_S_SceneFrogFish
function CMsgFish:onSceneFish(pData) --200-100
end 

-- CMD_S_DistributeFrogFish   sizeof = 56
function CMsgFish:onDistributeFish(pData) --200-105

    local datalen = pData:getReadableSize()
    if datalen % 56 > 0 then
        return "error 200-105"
    end
    local _buffer = pData:readData(datalen)

    --数据/保存
    local fishCount = datalen / 56
    --print("onDistributeFis================h fish count:",tostring(fishCount))
    for i = 1, fishCount do
        local pDistributeFish = {}
        pDistributeFish.fish_kind = _buffer:readInt()
        pDistributeFish.fish_id = _buffer:readInt()
        pDistributeFish.tag = _buffer:readInt()
        pDistributeFish.fish_mulriple = _buffer:readUInt()
        pDistributeFish.path_index = _buffer:readInt()
        pDistributeFish.fOffestX = _buffer:readFloat()
        pDistributeFish.fOffestY = _buffer:readFloat()
        pDistributeFish.fDir = _buffer:readFloat()
        pDistributeFish.fDelay = _buffer:readFloat()
        pDistributeFish.dwServerTick = _buffer:readInt()
        pDistributeFish.FishSpeed = _buffer:readFloat()
        pDistributeFish.FisType = _buffer:readInt()
        pDistributeFish.nTroop = _buffer:readInt()
        pDistributeFish.nRefershID = _buffer:readInt()

        FishDataMgr:getInstance():onDistributeFish(pDistributeFish, false)
    end

    --日志
    local ret = "" --string.format("新鱼[%d]", fishCount)

    return ret
end 
function CMsgFish:onReturnFishSync(pData)
    local datalen = pData:getReadableSize()
    if datalen % 56 > 0 then
        return "error 200-105"
    end
    local _buffer = pData:readData(datalen)

    --数据/保存
    local fishCount = datalen / 56
    FishDataMgr:getInstance().m_nSyncFishNum = fishCount
    FishDataMgr:getInstance().m_nSyncFishNum = 0
    --print("onReturnFishSync================h fish count:",tostring(fishCount))
    for i = 1, fishCount do
        local pDistributeFish = {}
        pDistributeFish.fish_kind = _buffer:readInt()
        pDistributeFish.fish_id = _buffer:readInt()
        pDistributeFish.tag = _buffer:readInt()
        pDistributeFish.fish_mulriple = _buffer:readUInt()
        pDistributeFish.path_index = _buffer:readInt()
        pDistributeFish.fOffestX = _buffer:readFloat()
        pDistributeFish.fOffestY = _buffer:readFloat()
        pDistributeFish.fDir = _buffer:readFloat()
        pDistributeFish.fDelay = _buffer:readFloat()
        pDistributeFish.dwServerTick = _buffer:readInt()
        pDistributeFish.FishSpeed = _buffer:readFloat()
        pDistributeFish.FisType = _buffer:readInt()
        pDistributeFish.nTroop = _buffer:readInt()
        pDistributeFish.nRefershID = _buffer:readInt()

        FishDataMgr:getInstance():onDistributeFish(pDistributeFish, true)
    end

    --日志
    local ret = string.format("同步鱼[%d]", fishCount)

    return ret
end

function CMsgFish:onUserScoreSync( pData )
    local datalen = pData:getReadableSize()
    if 20 ~= datalen then
        return "error 200-117"
    end
    local _buffer = pData:readData(datalen)

    local	wChairid    = _buffer:readUShort()
	local	wType       = _buffer:readUShort()
	local	dwData1     = _buffer:readUInt()
	local   dwData2     = _buffer:readUInt()
	local	llCurScore  = _buffer:readLongLong()

    --通知
    local _event = {
        name = FishEvent.MSG_FISH_SCORE_SYNC,
        chairId = wChairid,
        syncType = wType,
        Data1 = dwData1,
        Data2 = dwData2,
        CurScore = llCurScore,
    }
    SLFacade:dispatchCustomEvent(FishEvent.MSG_FISH_SCORE_SYNC, _event)

    local ret = "UserScoreSync:" .. llCurScore
    return ret
end


function CMsgFish:onUserFireError( pData )
    local datalen = pData:getReadableSize()
    if 14 ~= datalen then
        return "error 200-118"
    end
    local _buffer = pData:readData(datalen)
    local	wErrorType    = _buffer:readUShort()
    local   dwBulletMul   = _buffer:readUInt()
	local	llCurScore    = _buffer:readLongLong()

    local nChairId = PlayerInfo:getInstance():getChairID()
    FishDataMgr:getInstance():exchangeFishScore(nChairId, llCurScore)

    local ret = "onUserFireError:" .. llCurScore
    return ret
end
function CMsgFish:onChangeGun( pData )
    local datalen = pData:getReadableSize()
    if 4 ~= datalen then
        return "error 200-121"
    end
    local _buffer = pData:readData(datalen)
    local wChairID = _buffer:readUShort()
    local wUseVipLevel = _buffer:readUShort()
    
    local gunID = cc.exports.GUN_ID_MIN + wUseVipLevel
    FishDataMgr:getInstance():setWeaponIDByIndex(wChairID, gunID)

        --通知
    local _event = {
        name = FishEvent.MSG_FISH_CHANGE_GUN,
        chairId = wChairID,
        useVip = wUseVipLevel
    }

    SLFacade:dispatchCustomEvent(FishEvent.MSG_FISH_CHANGE_GUN, _event)
end
-- CMD_S_CatchFrogFishGroup  77
function CMsgFish:onCatchFishGroup(pData)--200-108

    local datalen = pData:getReadableSize()
    if datalen % 77 ~= 0 then
        return "error 200-108"
    end
    local _buffer = pData:readData(datalen)

    --数据/保存
    local count = datalen / 77
    local total = 0
    local totalScore = 0
    local chairID = 0
    local bombKindID = 0
    local bombFishID = 0

    for j = 1, count do
        local pCatchFishGroup = {}
        pCatchFishGroup.tick_count = _buffer:readUInt()
        pCatchFishGroup.chair_id = _buffer:readUShort()
        if j == 1 then -- 这里一定要取第一个chairID,后面可能有空的结构体
            chairID = pCatchFishGroup.chair_id
        end
        pCatchFishGroup.fish_count = _buffer:readInt()
        total = total + pCatchFishGroup.fish_count
        pCatchFishGroup.catch_fish = {}
        for i = 1, kMaxCatchFishCount do
            pCatchFishGroup.catch_fish[i] = {}
            pCatchFishGroup.catch_fish[i].fish_id       =  _buffer:readInt()
            pCatchFishGroup.catch_fish[i].fish_kind     =  _buffer:readInt()
            pCatchFishGroup.catch_fish[i].fish_score    =  _buffer:readLongLong()
            pCatchFishGroup.catch_fish[i].bullet_double =  _buffer:readBoolean()
            pCatchFishGroup.catch_fish[i].link_fish_id  =  _buffer:readInt()
            
            pCatchFishGroup.catch_fish[i].dead_index = (j - 1) * 3 + i  --鱼的死亡索引
            if i <= pCatchFishGroup.fish_count and pCatchFishGroup.catch_fish[i].fish_score > 0 then -- 计算总分
                totalScore = totalScore + pCatchFishGroup.catch_fish[i].fish_score
            end
            if j==1 and i==1 then
                if pCatchFishGroup.catch_fish[i].fish_kind == FishKind.FISH_FOSHOU 
                or pCatchFishGroup.catch_fish[i].fish_kind == FishKind.FISH_BGLU 
                or pCatchFishGroup.catch_fish[i].fish_kind == FishKind.FISH_DNTG 
                or pCatchFishGroup.catch_fish[i].fish_kind == FishKind.FISH_YJSD 
                or pCatchFishGroup.catch_fish[i].fish_kind == FishKind.FISH_YSSN 
                or pCatchFishGroup.catch_fish[i].fish_kind == FishKind.FISH_PIECE then
                    bombKindID = pCatchFishGroup.catch_fish[i].fish_kind -- 炸弹ID
                    bombFishID = pCatchFishGroup.catch_fish[i].fish_id
                    if bombKindID == FishKind.FISH_BGLU then
                        local fish = FishDataMgr:getInstance():getFishByID(bombFishID)
                        if fish ~= nil and fish:getFishTag() == 0 then -- 0表示忠义堂，不显示炸弹结算动画
                            bombKindID = 0
                            bombFishID = 0
                        end
                    end
                end
              end            
         end
        pCatchFishGroup.bullet_id = _buffer:readInt()
        FishDataMgr:getInstance():getCatchFishGroup(pCatchFishGroup)
    end
     
    if totalScore > 0 then
        FishDataMgr:getInstance():exchangeAddFishScore(chairID, totalScore)
    end
    if bombKindID > 0 and totalScore > 0 then
       local _event = {
            name = FishEvent.MSG_FISH_BOMB_EFFECT,
            ChairId = chairID,
            BombFishID = bombFishID,
            BombKindID = bombKindID,
            TotalScore = totalScore,
        }
        SLFacade:dispatchCustomEvent(FishEvent.MSG_FISH_BOMB_EFFECT, _event)
    end
    --日志
    local ret = "" --string.format("新鱼群[%d]", datalen / 77)
    if total > 1 then
        print(">>>>>>>>>>>>>>>>>>> fishgroup: " .. total )
    end

    return ret
end

-- CMD_S_BulletDoubleTimeout 2
function CMsgFish:onBulletDoubleTimeOut(pData)--200-104
    local datalen = pData:getReadableSize()
    if 2 ~= datalen then
        return "error 200-104"
    end
    local _buffer = pData:readData(datalen)

    --数据
    local nChairId = _buffer:readUShort()

    --日志
    local ret = string.format("%d", nChairId)

    --保存
    local bulletKind = FishDataMgr:getInstance():getbulletKindByIndex(nChairId)
    if bulletKind >= BulletKind.BULLET_KIND_1_ION then
        local kind = BulletKind[bulletKind-3+1]
        FishDataMgr:getInstance():setBulletKindByIndex(nChairId, kind)
    end

    --通知
    local _event = {
        name = FishEvent.MSG_FISH_DEL_BUFF,
        chairId = nChairId,
    }
    SLFacade:dispatchCustomEvent(FishEvent.MSG_FISH_DEL_BUFF, _event)

    return ret
end 

-- CMD_S_CatchFrogChain -- size 136
function CMsgFish:onCatchFishChain(pData) --200-107

    local datalen = pData:getReadableSize()
    if 136 ~= datalen then
        return "error 200-107"
    end
    local _buffer = pData:readData(datalen)

    --数据
    local pCatchChain = {}
    pCatchChain.chair_id = _buffer:readUShort()
    pCatchChain.fish_count = _buffer:readInt()
    pCatchChain.catch_fish = {}
    for i = 1, kMaxChainFishCount do
        pCatchChain.catch_fish[i] = {}
        pCatchChain.catch_fish[i].fish_id = _buffer:readInt()
        pCatchChain.catch_fish[i].fish_kind = _buffer:readInt()
        pCatchChain.catch_fish[i].fish_score = _buffer:readLongLong()
        pCatchChain.catch_fish[i].bullet_double = _buffer:readBoolean()
        pCatchChain.catch_fish[i].link_fish_id = _buffer:readInt()
    end
    pCatchChain.bullet_id = _buffer:readInt()

    --日志
    local ret = string.format("id[%d]fish[%d]", pCatchChain.chair_id, pCatchChain.fish_count)

    --保存
    FishDataMgr:getInstance():onCatchFishChain(pCatchChain)

    return ret
end 

-- CMD_S_ExchangeFishScore size 18
function CMsgFish:getExchangeFishScore(pData) --200-102

    local datalen = pData:getReadableSize()
    if datalen ~= 18 then
        return "error 200-102"
    end
    local _buffer = pData:readData(datalen)

    --数据
    local pExchangeFishScore = {}
    pExchangeFishScore.chair_id = _buffer:readUShort()
    pExchangeFishScore.swap_fish_score = _buffer:readLongLong()
    pExchangeFishScore.exchange_fish_score = _buffer:readLongLong()

    --日志
    local nChairId = PlayerInfo:getInstance():getChairID()
    local ret = string.format("id[%d] score[%d]", nChairId, pExchangeFishScore.swap_fish_score)

    --保存
    FishDataMgr:getInstance():exchangeFishScore(pExchangeFishScore.chair_id, pExchangeFishScore.swap_fish_score)

    --通知
    local nChairId = PlayerInfo:getInstance():getChairID()
    if pExchangeFishScore.chair_id ~= nChairId and pExchangeFishScore.swap_fish_score ~= 0 then
        -- 上分，表示有玩家进入
        FishDataMgr:getInstance():setMulripleByIndex(pExchangeFishScore.chair_id, FishDataMgr:getInstance():getMinBulletMultiple()) -- 对刚进入游戏的玩家，炮倍率设置为最小值
        FishDataMgr:getInstance():setBulletKindByIndex(pExchangeFishScore.chair_id, BulletKind.BULLET_KIND_1_NORMAL)
        SLFacade:dispatchCustomEvent(FishEvent.MSG_FISH_ADD_PLAYER,  pExchangeFishScore.chair_id)
    end

    return ret
end 

-- struct CMD_S_UserFire size 27
function CMsgFish:getUserFire(pData) --200-103

    local datalen = pData:getReadableSize()
    if datalen ~= 31 then
        return "error 200-103"
    end
    local _buffer = pData:readData(datalen)

    --数据
    local pUserFire = {}
    pUserFire.tick_count = _buffer:readUInt()
    pUserFire.chair_id = _buffer:readUShort()
    pUserFire.bullet_id = _buffer:readInt()
    pUserFire.angle = _buffer:readFloat()
    pUserFire.bullet_double = _buffer:readBoolean()
    pUserFire.bullet_mulriple = _buffer:readInt()
    pUserFire.lock_fishid = _buffer:readInt()
    pUserFire.bullet_temp_id = _buffer:readInt()
    pUserFire.gun_id = _buffer:readInt()
    --日志
    local ret = 1 -- string.format("chair[%d]", pUserFire.chair_id)

    --保存
    FishDataMgr:getInstance():getUserFire(pUserFire)

    return ret
end 
  
-- struct CMD_S_SwitchScene size 8
function CMsgFish:getSwitchScene(pData) --200-106

    local datalen = pData:getReadableSize()
    if 8 ~= datalen then
        return "error 200-106"
    end
    local _buffer = pData:readData(datalen)

    --数据
    local pswitchscene = {}
    pswitchscene.next_scene = _buffer:readInt()
    pswitchscene.tick_count = _buffer:readUInt()
    pswitchscene.create_time = cc.exports.gettime()  --创建鱼阵的时间
    --保存
    FishDataMgr.getInstance():loadSwitchScene(pswitchscene)

    --日志
    local ret = ""
    return ret
end 

-- struct CMD_DistributeFrogFishTeam size 300
function CMsgFish:onDistributeFishTeam(pData) --200-113
 
    local datalen = pData:getReadableSize()
    if 300 ~= datalen then
        return "error 200-113"
    end
    local _buffer = pData:readData(datalen)

    --数据
    local pDistributeFishTeam = {}
    pDistributeFishTeam.fish_team_type = _buffer:readInt()
    pDistributeFishTeam.fish_count = _buffer:readInt()
    pDistributeFishTeam.path_index = _buffer:readInt()
    pDistributeFishTeam.fish_kind = {}
    for i = 1, MAX_FISH_TEAM do
        pDistributeFishTeam.fish_kind[i] = _buffer:readInt()
    end
    pDistributeFishTeam.fish_id = {}
    for i = 1, MAX_FISH_TEAM do
        pDistributeFishTeam.fish_id[i] = _buffer:readInt()
    end
    pDistributeFishTeam.tag = {}
    for i = 1, MAX_FISH_TEAM do
        pDistributeFishTeam.tag[i] = _buffer:readInt()
    end
    pDistributeFishTeam.xOffset = {}
    for i = 1, MAX_FISH_TEAM do
        pDistributeFishTeam.xOffset[i] = _buffer:readInt()
    end
    --union 联合体，共用内存
    --pDistributeFishTeam.delayStep = {}
    --for i = 1, MAX_FISH_TEAM do
    --    pDistributeFishTeam.delayStep[i] = _buffer:readInt()
    --end
    pDistributeFishTeam.yOffset = {}
    for i = 1, MAX_FISH_TEAM do
        pDistributeFishTeam.yOffset[i] = _buffer:readInt()
    end

    --保存
    FishDataMgr:getInstance():onDistributeFishTeam(pDistributeFishTeam)

    --日志
    local ret = string.format("fihs[%d]", pDistributeFishTeam.fish_count)

    return ret
end 

-- struct CMD_DistributeFrogFishCircle size 492
function CMsgFish:onDistributeFishCircle(pData) --200-114
    local datalen = pData:getReadableSize()
    if 492 ~= datalen then
        return "error 200-114"
    end
    local _buffer = pData:readData(datalen)

    --数据
    local pDistributeFishCircle = {}
    pDistributeFishCircle.fish_count = _buffer:readInt()-- 鱼的类型
    pDistributeFishCircle.xOffset = _buffer:readInt()-- 出生点的偏移
    pDistributeFishCircle.yOffset = _buffer:readInt()-- 出生点的偏移
    pDistributeFishCircle.fish_kind = {} -- 鱼群的类型
    for i = 1, MAX_FISH_CIRCLE do
        pDistributeFishCircle.fish_kind[i] = _buffer:readInt()
    end
    pDistributeFishCircle.fish_id = {} -- 鱼的ID
    for i = 1, MAX_FISH_CIRCLE do
        pDistributeFishCircle.fish_id[i] = _buffer:readInt()
    end
    pDistributeFishCircle.tag = {}
    for i = 1, MAX_FISH_CIRCLE do
        pDistributeFishCircle.tag[i] = _buffer:readInt()
    end
    pDistributeFishCircle.path_index = {} -- 鱼的路径ID
    for i = 1, MAX_FISH_CIRCLE do
        pDistributeFishCircle.path_index[i] = _buffer:readInt()
    end
    pDistributeFishCircle.fish_mulriple = {} -- 鱼的倍数 
    for i = 1, MAX_FISH_CIRCLE do
        pDistributeFishCircle.fish_mulriple[i] = _buffer:readInt()
    end

    --保存
    FishDataMgr:getInstance():onDistributeFishCircle(pDistributeFishCircle)

    --日志
    local ret = string.format("fish[%d]", pDistributeFishCircle.fish_count)
    return ret
end 

-- struct CMD_S_ReturnBulletScore size 12
function CMsgFish:onReturnBulletGold(pData) --200-115
    local datalen = pData:getReadableSize()
    if 12 ~= datalen then
        return "error 200-115"
    end
    local _buffer = pData:readData(datalen)

    --数据
    local pReturnBulletScore = {}
    pReturnBulletScore.chairid = _buffer:readUShort()
    pReturnBulletScore.wReason = _buffer:readUShort()
    pReturnBulletScore.llReturn = _buffer:readLongLong()

    --保存
    local chairID = pReturnBulletScore.chairid
    local score = pReturnBulletScore.llReturn
    if chairID ~= INVALID_CHAIR and score > 0 then
        -- 给玩家返回金币
        local curFishScore = FishDataMgr:getInstance():getFishScoreByIndex(chairID)
        FishDataMgr:getInstance():exchangeFishScore(chairID, curFishScore + score)
    end

    return ""
end 

-- 发送 捕到鱼 消息 给服务器
function CMsgFish:sendCatchFish(tick_count,fish_id,bullet_id,bullet_temp_id)

    local wb = WWBuffer:create()
    wb:writeUInt(tick_count)
    wb:writeInt(fish_id)
    wb:writeInt(bullet_id)
    wb:writeInt(bullet_temp_id)

    self:sendData(G_C_CMD.MDM_GF_GAME, G_C_CMD.SUB_C_CATCH_FISH, wb)
end

--玩家发炮请求
function CMsgFish:sendUserFire(angle,bullet_mulriple,lock_fishid,tick_count,bulletTempId,gunID)
    local wb = WWBuffer:create()
    wb:writeUInt(tick_count)
    wb:writeFloat(angle)
    wb:writeInt(lock_fishid)
    wb:writeInt(bullet_mulriple)
    wb:writeInt(bulletTempId)
    wb:writeInt(gunID)
    self:sendData(G_C_CMD.MDM_GF_GAME, G_C_CMD.SUB_C_USER_FIRE, wb)
end

-- 发送初始化金币 请求
function CMsgFish:sendExchangeFishScore(value)
    local wb = WWBuffer:create()
    wb:writeLongLong(value)

    self:sendData(G_C_CMD.MDM_GF_GAME, G_C_CMD.SUB_C_EXCHANGE_FISHSCORE, wb)
end

-- 发送同步场景鱼 请求
function CMsgFish:sendFishSync()
    local wb = WWBuffer:create()
    self:sendData(G_C_CMD.MDM_GF_GAME, G_C_CMD.SUB_C_USER_FISH_SYNC, wb)
end
-- 发送修改炮台 请求
function CMsgFish:sendChangeGun( changeVip )
    local nChairId = PlayerInfo:getInstance():getChairID()
    local wb = WWBuffer:create()    
    wb:writeUShort( nChairId )
    wb:writeUShort( changeVip )
    self:sendData(G_C_CMD.MDM_GF_GAME, G_C_CMD.SUB_C_CHANGE_GUN, wb)
end

return CMsgFish
-- endregion