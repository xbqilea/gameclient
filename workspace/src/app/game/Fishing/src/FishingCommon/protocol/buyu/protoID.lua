
BEGIN_MODULEID("捕鱼公共结构ID定义",130000) --[130000 -- 140000]

--捕鱼与场景通讯数据
PSTID_BUYU_PLAYERDATA       =	GENPID(1, '游戏过程数据')
PSTID_BUYU_FISHDATA         =	GENPID(2, '鱼的数据')
PSTID_BUYU_INITGAMEDATA     =	GENPID(3, '返回场景初始化结果')
PSTID_BUYU_ITEMDATA         =   GENPID(4, '物品表') 
PSTID_BUYU_CHAIRUSER        =   GENPID(5, '物品表') 
PSTID_BUYU_CHAIRATTR        =   GENPID(6, '物品表') 
PSDID_BUYU_ROOMATTR         =   GENPID(7, '物品表') 
PSTID_BUYU_ROOMIDATTR       =   GENPID(8, '物品表') 
PSTID_BUYU_PLAYERATTR       =   GENPID(9, '物品表') 
PSTID_BUYU_PLAYERINOUTATTR  =   GENPID(10, '物品表') 
PSTID_BUYU_TABLEATTR        =   GENPID(11, '物品表') 
PSTID_BUYU_BEFOREGAMECHAIR  =   GENPID(12, '物品表') 
PSTID_BUYU_BEFOREGAMEUSER   =   GENPID(13, '物品表') 
PSTID_BUYU_DROPITEM         =   GENPID(14, '掉落物品表') 
PSTID_BUYU_SKILLCDDATA		=	GENPID(15, '技能cd数据')
PSTID_BUYU_BUFFDATA			=	GENPID(17, '捕鱼的buff鱼特效')
PSTID_BUYU_POWERINFO		=   GENPID(19, '炮台的信息类型')
PSTID_BUYU_YYTABLEDATA      =   GENPID(20, '鱼友房管理页面')
PSTID_BUYU_VOTEDATA         =   GENPID(22, '鱼友投票')
PSTID_BUYU_BALANCEDATA      =   GENPID(23, '结算')
PSTID_BUYU_YYPLAYERDATA     =   GENPID(25, '鱼友数据信息')
PSTID_BUYU_YYSYNDATA        =   GENPID(26, '同步服务器数据')
PSTID_BUYU_YYQIANGZHUANG    =   GENPID(27, '玩家强庄状态')
PSTID_BUYU_ACTIVEAWARD      =   GENPID(28, '活动奖励')
PSTID_BUYUGAMEDATAEX		=	GENPID(29, '捕鱼游戏玩家数据')
-- : [捕鱼
BEGIN_MODULEID("捕鱼<客户端-服务器>",130100) --[130100 -- 130200]
CS_G2C_BUYU_PLAYERDATA_NTY	       =	GENPID(1	, '更新玩家数据')
CS_C2G_BUYU_CHANGE_LEVEL_NTY	   =	GENPID(2	, '换等级')
CS_G2C_BUYU_CHANGE_LEVEL_NTY	   =	GENPID(3	, '换等级返回状态')
CS_C2G_BUYU_FIRE_NTY	           =	GENPID(4	, '发炮')
CS_G2C_BUYU_FIRE_NTY	           =	GENPID(5	, '发炮广播')
CS_G2C_BUYU_MAKE_NTY	           =	GENPID(6	, '出鱼')
CS_C2G_BUYU_GET_NTY	               =	GENPID(7	, '试图抓哪些鱼')
CS_G2C_BUYU_GET_NTY	               =	GENPID(8	, '广播谁抓住了哪些鱼')
CS_C2G_BUYU_ACTIVE_TASK_AWARD_REQ  =    GENPID(10	, '获取玩家的任务活动奖励列表')
CS_G2C_BUYU_ACTIVE_TASK_AWARD_ACK  =    GENPID(11	, '返回玩家的任务活动奖励列表')
CS_G2C_BUYU_UPDATE_BAG_NTY         =    GENPID(12	, '刷新捕鱼包裹')
CS_G2C_BUYU_BROADCAST_NTY          =    GENPID(13	, '广播捕鱼事件')
CS_C2G_BUYU_GM_NTY                 =    GENPID(14	, '捕鱼GM')
CS_G2C_BUYU_GM_NTY                 =    GENPID(15	, '捕鱼GM返回，主要用于diy广播')
CS_C2G_BUYU_REQUEST_NTY            =    GENPID(16	, '客户端加载好，请求服务器数据')
CS_G2C_BUYU_FISHDATA_NTY	       =	GENPID(17	, '发送所有鱼的信息')
CS_G2C_BUYU_EXPEL_NTY              =    GENPID(18	, '驱赶鱼，准备出鱼阵')
CS_G2C_BUYU_ARMY_OVER_NTY          =    GENPID(19	, '鱼阵结束')
CS_G2C_BUYU_TASKID_NTY			   =	GENPID(20	, '返回当前正在做的任务信息')
CS_C2G_BUYU_SKILL_REQ			   =	GENPID(21	, '请求使用技能')
CS_G2C_BUYU_SKILL_ACK			   =	GENPID(22	, '返回使用技能的结果')
CS_C2G_BUYU_BUYITEM_REQ			   =	GENPID(23	, '请求购买商品')
CS_G2C_BUYU_BUYITEM_ACK			   =	GENPID(24	, '返回购买的结果')
CS_C2G_BUYU_CHAT_NTY			   =	GENPID(25	, '聊天请求')
CS_G2C_BUYU_CHAT_NTY			   =	GENPID(26	, '聊天的内容发返回给其他用户')
CS_C2G_BUYU_GAMEINFO_NTY		   =	GENPID(27	, '请求获取技能cd信息')
CS_G2C_BUYU_SKILLINFO_NTY		   =	GENPID(28	, '返回技能cd信息')
CS_C2G_BUYU_UI_STATIC_NTY          =    GENPID(29	, 'ui统计')
CS_G2C_BUYU_KICK_GAMEOUT_NTY	   =	GENPID(30	, '游戏退出通知,如金币不足房间的最低需求,被vip踢出房间,房主退出其他人也退出')
CS_C2G_BUYU_KICKGAME_REQ		   =	GENPID(31	, 'vip功能,踢玩家出游戏')
CS_G2C_BUYU_KICKGAME_ACK		   =	GENPID(32	, '响应踢出的结果,收到代表踢出游戏成功')
CS_G2C_BUYU_POWERSCORE_NTY		   =	GENPID(33	, '推送拥有的炮倍数给客户端')
CS_G2C_BUYU_SCENE_INFO_NTY         =	GENPID(34	, '推送背景')
CS_C2G_BUYU_BUFF_GET_REQ	       =	GENPID(35	, 'buff鱼的特效碰撞请求')
CS_G2C_BUYU_BUFF_GET_ACK	       =	GENPID(36	, 'buff鱼的特效反回结果')
CS_G2C_BUYU_BUFF_LIST_NTY	       =	GENPID(37	, '开局时推送其他玩家的特效')
CS_C2G_BUYU_GET_ACTIVE_TASK_AWARD_REQ  =    GENPID(38	, '领取活动任务奖励')
CS_G2C_BUYU_GET_ACTIVE_TASK_AWARD_ACK  =    GENPID(39	, '领取活动任务奖励返回')
CS_C2G_BUYU_DISABLE_MSG_NTY		   =	GENPID(40	, '屏蔽和开启协议收发')
CS_G2C_BUYU_NOTICE_MSG_NTY		   =	GENPID(41	, '提示通知协议.')

CS_C2G_BUYU_DISMISS_YY_TABLE_REQ       =   GENPID(42	, '解散鱼友房请求')
CS_G2C_BUYU_DISMISS_YY_TABLE_ACK       =   GENPID(43	, '解散鱼友房返回')
CS_C2G_BUYU_QIANGZHUANG_YY_TABLE_REQ   =   GENPID(44	, '抢庄请求')
CS_G2C_BUYU_QIANGZHUANG_YY_TABLE_ACK   =   GENPID(45	, '抢庄返回')
CS_C2G_BUYU_BEGIN_YY_TABLE_REQ         =   GENPID(46	, '手动开局请求')
CS_C2G_BUYU_CHECK_YY_TABLE_REQ         =   GENPID(47	, '确定玩家身份请求')
CS_G2C_BUYU_CHECK_YY_TABLE_ACK         =   GENPID(48	, '确定玩家身份返回')
CS_C2G_BUYU_VOTE_YY_TABLE_REQ          =   GENPID(49	, '投票解散请求')
CS_G2C_BUYU_VOTE_YY_TABLE_ACK          =   GENPID(50	, '投票解散返回')
CS_C2G_BUYU_FIRE_YY_NTY	               =   GENPID(51	, 'YY发炮')
CS_G2C_BUYU_FIRE_YY_NTY	               =   GENPID(52	, 'YY发炮广播')
CS_C2G_BUYU_GET_YY_NTY	               =   GENPID(53	, 'YY发炮')
CS_G2C_BUYU_GET_YY_NTY	               =   GENPID(54	, 'YY发炮广播')
CS_G2C_BUYU_STATE_YY_NTY	           =   GENPID(55	, 'YY桌子状态更新')
CS_G2C_BUYU_BALANCE_YY_NTY             =   GENPID(56	, 'YY桌子结算')
CS_C2G_BUYU_KICK_YY_REQ                =   GENPID(57	, 'YY桌子踢人')
CS_G2C_BUYU_KICK_YY_ACK                =   GENPID(58	, 'YY桌子踢人')
CS_G2C_BUYU_PLAYERDATA_YY_NTY	       =   GENPID(59	, '更新玩家数据')
CS_C2G_BUYU_CHANGE_YY_REQ              =   GENPID(60	, '换炮')
CS_G2C_BUYU_CHANGE_YY_ACK              =   GENPID(61	, '换炮返回')
CS_G2C_BUYU_MAKE_HOST_YY_NTY           =   GENPID(62	, '产生庄家') 
CS_G2C_BUYU_SEND_GAMES_YY_NTY          =   GENPID(63	, '发送局数')
CS_G2C_BUYU_UPDATE_OWER_YY_NTY         =   GENPID(64	, '更换房主') 
CS_C2G_BUYU_VOTE_YY_START_REQ          =   GENPID(65	, '投票发起')
CS_G2C_BUYU_VOTE_YY_START_ACK          =   GENPID(66	, '投票发起返回')
CS_G2C_BUYU_GAMEMAINTENANCE_NTY		   =   GENPID(67	, '推送维护公告给客户端')
CS_G2C_BUYU_SYN_YY_NTY                 =   GENPID(68	, '同步服务器的金币和字段给客户端')
CS_G2C_BUYU_BEGIN_YY_TABLE_ACK		   =   GENPID(69	, '开局结果返回')
CS_G2C_BUYU_SCENEINIT_YY_NTY		   =   GENPID(70    , '初始化场景信息')
CS_C2G_BUYU_ASK_PLAYERDATA_YY_REQ      =   GENPID(71    , '请求玩家信息')
CS_G2C_BUYU_ASK_PLAYERDATA_YY_ACK      =   GENPID(72    , '请求玩家信息返回')
CS_G2C_BUYU_TUNPAO_RETURNBACK_NTY      =   GENPID(73    , '吞炮加金币')
CS_C2G_BUYU_LOCKFISHOPER_REQ			=   GENPID(74    , '请求锁定操作')
CS_G2C_BUYU_LOCKFISHOPER_ACK			=   GENPID(75    , '请求锁定操作返回')
CS_G2C_BUYU_PLAYERLOCKFISHOPER_NTY		= 	GENPID(76    , '有玩家锁定操作')
CS_C2G_BUYU_CHANGESPEED4CANNON_REQ		= 	GENPID(77    , '切换炮速')
CS_G2C_BUYU_CHANGESPEED4CANNON_ACK		=	GENPID(78    , '切换炮速')
CS_G2C_BUYU_PLAYERCHANGESPEED_NTY		=	GENPID(79    , '有玩家切换炮速')
CS_G2C_BUYU_WHODOROBOTCHECK_NTY			=	GENPID(80    , '指定玩家做机器人的碰撞检测')

BEGIN_MODULEID("捕鱼<客户端-服务场景>",130401) --[130401 -- 130500]
CS_M2C_BUYU_SCENEPERSONALINFO_NTY      = GENPID(1	, '玩家场景信息')
CS_M2C_BUYU_ENTERDOMROOM_NTY           = GENPID(2, '响应进入房间大厅')
CS_M2C_BUYU_ENTERDOMROOMPHONE_NTY      = GENPID(3, '响应进入房间大厅 手机')
CS_M2C_BUYU_SCENEPLAYERLIST_NTY        = GENPID(4, '推送特定场景内所有玩家信息')
CS_M2C_BUYU_SCENEPLAYERINOUT_NTY       = GENPID(5, '推送特定场景内玩家进入/退出通知')
CS_M2C_BUYU_UPDAYEROOMIDLIST_NTY       = GENPID(6, '更新大厅编号列表')
CS_M2C_BUYU_APPLYTABLECHAIR_ACK        = GENPID(7, '选桌椅(PC)')
CS_M2C_BUYU_BEFOREGAMETABLE_NTY        = GENPID(8, '推送场景通知游戏客户端桌子有人进/出(开始游戏前)(PC、手机)')
CS_M2C_BUYU_UPDATEROOMTABLE_NTY        = GENPID(9, '更新大厅房间桌子信息(PC)')
CS_M2C_BUYU_ENTERSPECIFICROOMHALL_ACK  = GENPID(10, '进入指定的自由房房间大厅(PC)')
CS_M2C_BUYU_EXITROOMHALL_ACK           = GENPID(11, '强退房间大厅(PC、手机)')
CS_M2C_BUYU_FASTENTERTABLE_ACK         = GENPID(12, '响应快速加入桌子(进入当前房间)(PC)')
CS_M2C_BUYU_FASTENTERTABLEPHONE_ACK    = GENPID(13, '快速加入桌子(手机)')
CS_M2C_BUYU_ENTERTABLEVERIFY_ACK       = GENPID(14, '响应申请入桌验证(PC)')
CS_M2C_BUYU_EXITGAME_ACK               = GENPID(15, '退出游戏客户端，点游戏客户端X 、游戏中退出按钮(PC、手机)')

CS_C2M_BUYU_APPLYTABLECHAIR_REQ			= GENPID(16, '请求选桌椅(PC)')
CS_C2M_BUYU_ENTERTABLEVERIFY_REQ		= GENPID(17, '申请入桌验证(PC)')
CS_C2M_BUYU_ENTERSPECIFICROOMHALL_REQ	= GENPID(18, '进入指定的自由房房间大厅(PC)')
CS_C2M_BUYU_FASTENTERTABLE_REQ			= GENPID(19, '响应快速加入桌子(进入当前房间)(PC)')
CS_C2M_BUYU_FASTENTERTABLEPHONE_REQ		= GENPID(20, '快速加入桌子(手机)')
CS_C2M_BUYU_EXITROOMHALL_REQ			= GENPID(21, '强退房间大厅(PC、手机)')
CS_C2M_BUYU_EXITGAME_REQ              	= GENPID(22, '退出游戏客户端，点游戏客户端X 、游戏中退出按钮(PC、手机)')

CS_C2M_BUYU_REFRESH_NEXT_TABLE_REQ      = GENPID(29, '请求下一页的桌子')
CS_M2C_BUYU_REFRESH_NEXT_TABLE_ACK		= GENPID(30, '推送下一页的桌子')
CS_C2M_BUYU_GETTASKID_REQ				= GENPID(33	, '请求获取任务id')
CS_M2C_BUYU_GETTASKID_ACK				= GENPID(34	, '返回任务id')

CS_C2M_BUYU_CREATE_YY_TABLE_REQ         = GENPID(35	, '创建鱼友房')
CS_M2C_BUYU_CREATE_YY_TABLE_ACK         = GENPID(36	, '创建鱼友房')
CS_C2M_BUYU_ENTER_YY_TABLE_REQ          = GENPID(37	, '进入鱼友房')
CS_M2C_BUYU_ENTER_YY_TABLE_ACK          = GENPID(38	, '进入鱼友房')
CS_C2M_BUYU_MGR_YY_TABLE_REQ            = GENPID(39	, '进入鱼友房管理界面')
CS_M2C_BUYU_MGR_YY_TABLE_ACK            = GENPID(40	, '进入鱼友房管理界面返回')
CS_C2M_BUYU_DISMISS_YY_TABLE_REQ        = GENPID(41	, '解散鱼友房')
CS_M2C_BUYU_DISMISS_YY_TABLE_ACK        = GENPID(42	, '解散鱼友房')

CS_C2M_BUYU_GETSTATUS_YY_REQ   		    = GENPID(43 , '解散鱼友房')
CS_M2C_BUYU_GETSTATUS_YY_ACK     	    = GENPID(44	, '解散鱼友房')

CS_C2M_BUYU_CLIENTCHAT_NTY				= GENPID(45 , '客户端聊天')
CS_M2C_BUYU_CLIENTCHAT_NTY				= GENPID(46 , '客户端聊天转发')

CS_C2M_BUYU_REFRESHTABLE_REQ			= GENPID(47, '选桌进入方式下刷新玩家可见桌子信息（手机）')
CS_M2C_BUYU_REFRESHTABLE_ACK			= GENPID(48, '选桌进入方式下刷新玩家可见桌子信息响应（手机）')
CS_M2C_BUYU_KICK_CLIENT_NTY				= GENPID(49, '通知客户端退出捕鱼')