module(..., package.seeall)

-- 捕鱼协议<客户端-服务器>
CS_G2C_BuYu_Playerdata_Nty =
{
	{ 1     , 1     , 'm_vecPlayerData'     , 'PstBuYuPlayData'             , 6     , '本桌其他玩家的基本资料'},
}

-- 游戏切换回来，需要重新发送游戏信息，如技能cd，鱼池信息
CS_C2G_BuYu_GameInfo_Nty =
{
}

CS_G2C_BuYu_Fishdata_Nty =
{
	{1     , 1     , 'm_vecFishData'       , 'PstBuYuFishData'             , 500   , '本桌鱼的数据'},
	{2		, 1		, "nTime"				, "UINT"			,  1	, '服务端时间'},
}

CS_G2C_BuYu_SkillInfo_Nty =
{
	{1, 1, 'm_vecSkillCdData'	,	'PstBuYuSkillCdData', 3,'技能的数据'},
	{2, 1, 'm_nTime'			, 	'UINT'				, 1,'服务端时间'},
}

CS_G2C_BuYu_Broadcast_Nty = 
{
	{1, 1, 'm_szName',     		'STRING', 1, '人名'},
	{2, 1, 'm_nEventType', 		'UINT',   1, '事件类型'},
	{3, 1, 'm_nBroadcastType',  'UINT',   1, '广播类型 1 本桌 2 房间 3 全服'},
	{4, 1, 'm_nParam1',    		'UINT',   1, '奖励金币'},
	{5, 1, 'm_nParam2',    		'UINT',   1, '鱼的类型'}, 
	{6, 1, 'm_nParam3',			'UINT',   1, '物品id'},
	{7, 1, 'm_nParam4',			'UINT',   1, '物品数量'},
}

CS_G2C_BuYu_Scene_Info_Nty = 
{
	{1, 1, 'm_nSceneIndex', 'UINT', 1, '场景索引'},
	{2, 1, 'm_nLastTime', 'UINT', 1, '已经持续时间'}
}

CS_G2C_BuYu_Expel_Nty = 
{
}

CS_C2G_BuYu_Change_Level_Nty	=
{
	{1		, 1		, 'm_nLevel'	, 'UINT'	, 1		, '等级'},
} 

CS_G2C_BuYu_Change_Level_Nty = 
{
	{1		, 1     , 'm_nAccountID'  ,'UINT'	, 1		,'谁换了炮'},
	{2		, 1		, 'm_nLevel'	, 'UINT'	, 1		,'等级'},
	{3		, 1		, 'm_nPItemID'	, 'UINT'	, 1		,'炮的物品i'}
}

CS_C2G_BuYu_Fire_Nty = 
{
	{1		, 1		, 'm_nPItemID'	  ,'UINT'	, 1		, '炮的物品id'},
	{2		, 1		, 'm_nLevel'	  ,'UINT'	, 1		, '炮的等级'},
	{3		, 1		, 'm_nX'	      ,'UINT'	, 1		, 'x'},
	{4		, 1		, 'm_nY'	      ,'UINT'	, 1		, 'y'},
	{5		, 1		, 'm_nSourceFish' ,'UINT'	, 1		, '瞄准的目标鱼'},
}

CS_G2C_BuYu_Fire_Nty = 
{
	{1		, 1     , 'm_nAccountID'  ,'UINT'	, 1		, '谁发射了炮'},
	{2		, 1		, 'm_nCoin'	      ,'UINT'	, 1		, '刷新打炮金币数量'},
	{3		, 1		, 'm_nPItemID'    ,'UINT'	, 1		, '炮的ID'},
	{4		, 1		, 'm_nLevel'	  ,'UINT'	, 1		, '炮使用的等级'},
	{5		, 1		, 'm_nX'	      ,'UINT'	, 1		, 'x'},
	{6		, 1		, 'm_nY'	      ,'UINT'	, 1		, 'y'},
	{7		, 1		, 'm_nStep'       ,'UINT'	, 1		, '步长，大于1为狂暴状态'},
	{8		, 1		, 'm_nSourceFish' ,'UINT'	, 1		, '瞄准的目标鱼'},
	{9		, 1		, 'm_nTime'		  ,'UINT'	, 1		, '服务端时间'},
}

CS_C2G_BuYu_Fire_YY_Nty = 
{
	{1		, 1		, 'm_nPowerScore' ,'UINT'	, 1		, '炮的分数'},
	{2		, 1		, 'm_nX'	      ,'UINT'	, 1		, 'x'},
	{3		, 1		, 'm_nY'	      ,'UINT'	, 1		, 'y'},
}

CS_G2C_BuYu_Fire_YY_Nty = 
{
	{1		, 1     , 'm_nAccountID'  ,'UINT'	, 1		, '谁发射了炮'},
	{2		, 1		, 'm_nCoin'	      ,'INT'	, 1		, '刷新开炮人的金币数量'},
	{3		, 1		, 'm_nHostCoin'	  ,'INT'	, 1		, '刷新庄家的金币数量'},
	{4		, 1		, 'm_nPowers'	  ,'INT'	, 1		, '刷新庄家的子弹数'},
	{5		, 1		, 'm_nPowerScore' ,'UINT'	, 1		, '炮的分数'},
	{6		, 1		, 'm_nX'	      ,'UINT'	, 1		, 'x'},
	{7		, 1		, 'm_nY'	      ,'UINT'	, 1		, 'y'},
	{8		, 1		, 'm_nSourceFish' ,'UINT'	, 1		, '瞄准的目标鱼'},
	{9		, 1		, 'm_nTime'		  ,'UINT'	, 1		, '服务端时间'},
}


CS_G2C_BuYu_Make_Nty = 
{
	{1		, 1		, "m_vecNewFish"	, "PstBuYuFishData"		, 100	, '定期产鱼的量'},
}

CS_G2C_BuYu_Army_Over_Nty = 
{

}

CS_C2G_BuYu_Get_Nty = 
{
	{1		, 1     , "m_nIndex"		,"UINT"	, 1		, '渔网碰撞到的鱼ID'},
	{2		, 1		, 'm_nPItemID'		,'UINT'	, 1		, '炮的ID'},
	{3		, 1		, 'm_nPowerScore'	,'UINT'	, 1		, '炮的倍数'},
	{4		, 1		, 'm_nStep'			,'UINT' , 1		, '狂暴的步数'},
	{5		, 1		, 'm_nX'			,'UINT' , 1		, '鱼的位置x'},
	{6		, 1		, 'm_nY'			,'UINT' , 1		, '鱼的位置y'},
	{7		, 1     , "m_nRobotId"		,"UINT"	, 1		, '机器人ID, 如果是自己填0'},
}

CS_G2C_BuYu_Get_Nty = 
{
	{1		, 1		, "m_nAccountID"	,"UINT"				,1		, '帐号id'},
	{2		, 1		, "m_nIndex"	    ,"UINT"				,1		, '渔网碰撞到的鱼ID'},
	{3		, 1		, "m_nTimes"	    ,"UINT"				,1		, '暴击次数，保底一次'},
	{4		, 1		, "m_nAddCoin"	    ,"UINT"				,1		, '获得金币数量'},
	{5		, 1		, "m_nCoin"	        ,"UINT"				,1		, '金币数量'},
	{6		, 1     , "m_nDiamon"       ,"UINT"     		,1     	, '钻石数量'},
	{7      , 1     , "m_vecItem"       ,"PstBuYuDropItem"  ,3      , '掉落物品'},
	{8      , 1     , "m_nEffectIndex"  ,"UINT" 			,1      , '特效的index'},
	{9      , 1     , "m_nEffectType"   ,"UINT"  			,1      , '特效类型 1 毒气  2 雷电  3 喷刺 4 轮盘'},
	{10     , 1     , "m_nRandScore"    ,"UINT"  			,1      , '轮盘鱼随机的倍数'},
}

-- 玩家请求锁定操作鱼
CS_C2G_BuYu_LockFishOper_Req =
{
	{1, 1, "m_nIndex", "UINT", 1, '渔网碰撞到的鱼ID'},
}

-- 玩家请求锁定鱼操作返回
CS_G2C_BuYu_LockFishOper_Ack =
{
	{1, 1, 'm_nResult',	'INT', 	1,'返回的错误码,0:成功'},
	{2, 1, "m_nIndex", "UINT", 1, '渔网碰撞到的鱼ID'},
}

-- 有玩家锁定鱼操作通知
CS_G2C_BuYu_PlayerLockFishOper_Nty =
{
	{1, 1, "m_nAccountID", "UINT", 1, '帐号id'},
	{2, 1, "m_nIndex", "UINT", 1, '渔网碰撞到的鱼ID'},
}

-- 请求切换炮速
CS_C2G_BuYu_ChangeSpeed4Cannon_Req = 
{
	{1, 1, "m_nSeepType", "UINT", 1, '速度,0:慢炮，1:快炮'},
}

-- 切换炮速返回
CS_G2C_BuYu_ChangeSpeed4Cannon_Ack =
{
	{1, 1, "m_nRet", "INT", 1, '返回的错误码,0:成功'},
	{2, 1, "m_nSeepType", "UINT", 1, '速度,0:慢炮，1:快炮'},
}

-- 有玩家切换炮速
CS_G2C_BuYu_PlayerChangeSpeed_Nty = 
{
	{1, 1, "m_nAccountID", "UINT", 1, '帐号id'},
	{2, 1, "m_nSeepType", "UINT", 1, '速度,0:慢炮，1:快炮'},
}

CS_C2G_BuYu_Request_Nty = 
{

}

CS_G2C_BuYu_Update_Bag_Nty = 
{
	{1		, 1		, "m_vecItem"	, "PstBuYuItemData"		, 100	, '背包物品'},
	{2		, 1		, "m_nServerTime" ,		"UINT"	,1	,'服务器时间'},
}

CS_C2G_BuYu_Gm_Nty = 
{
	{1, 1, 'm_szGM',     'STRING', 1, '命令 superman addCoin  addItem setShipLevel broadCast'},
	{2, 1, 'm_nParam1',  'UINT',   1, '参数1'},
	{3, 1, 'm_nParam2',  'UINT',   1, '参数2'},
	{4, 1, 'm_nParam3',  'UINT',   1, '参数3'},
	{5, 1, 'm_szParam4', 'STRING', 1, '参数4'},
}


CS_C2G_BuYu_Ui_Static_Nty = 
{
	{1, 1, 'm_nIndex',		'UINT', 1 ,'ui索引'},
}

CS_G2C_BuYu_Gm_Nty = 
{
	{1, 1, 'm_szInfo',     'STRING', 1, '广播信息'},
}

CS_G2C_BuYu_TaskID_Nty =
{
	{1, 1, 'm_nTaskID',      'UINT', 1, '正在执行的任务'},
	{2, 1, 'm_nTaskProcess', 'UINT', 1, '任务进度'},
}


CS_C2G_BuYu_Skill_Req =
{
	{1, 1, 'm_nIndex',		'UINT', 1 ,'技能索引'},
}

CS_G2C_BuYu_Skill_Ack =
{
	{1, 1, 'm_nIndex',		'UINT', 1,'技能索引'},
	{2, 1, 'm_nTimeOut',	'UINT', 1,'技能CD时间'},
	{3, 1, 'm_nAccountID',	'UINT',	1,'技能使用者的账号id'},
	{4, 1, 'm_nResult',		'INT', 	1,'返回的错误码'},
	{5, 1, 'm_nNum',		'UINT', 1,'返回道具的数量'},
}


CS_C2G_BuYu_BuyItem_Req =
{
	{1, 1, 'm_nIndex',		'UINT', 1 ,'物品索引'},
}
CS_G2C_BuYu_BuyItem_Ack =
{
	{1, 1, 'm_nIndex',		'UINT', 1 ,'物品索引'},
	{2, 1, 'm_nAccountID',	'UINT', 1 ,'购买的用户'},
	{3, 1, 'm_nResult',		'INT', 1 ,'购买的结果0成功。1物品不存在。2金币不够'},
	{4, 1, 'm_nCoin',		'UINT', 1 ,'更新当前用户的金币数量'},
	{5, 1, 'm_nItemCoin',	'UINT', 1 ,'购买消耗的金币'},
}

CS_C2G_BuYu_Chat_Nty =
{
	{1, 1, 'm_nChannel',	'UINT', 1 ,'聊天频道1本桌2本房间'},
	{2, 1, 'm_szMsg',		'STRING', 1 ,'聊天的内容'},
}

CS_G2C_BuYu_Chat_Nty =
{
	{1, 1, 'm_nChannel',	'UINT',	  1 ,'聊天频道'},
	{2, 1, 'm_nAccountID',	'UINT',	  1 ,'账号id'},
	{3,	1, 'm_szNick',		'STRING', 1 ,'发送消息的玩家昵称'},
	{4, 1, 'm_szMsg',		'STRING', 1 ,'聊天的内容'},
}

CS_G2C_BuYu_Kick_GameOut_Nty =
{
	{1, 1, 'm_nOutType',			'UINT', 1 ,'退出游戏的通知,1 炮台过期导致没有可用的炮台踢出房间 2 维护替人 3-子弹超速踢人 4-超时不开火踢人'},
}

CS_C2G_BuYu_KickGame_Req =
{
	{1, 1, 'm_nSourceChairID',		'UINT', 1 ,'被踢的目标座位'},
}

CS_G2C_BuYu_KickGame_Ack =
{
	
}

CS_G2C_BuYu_PowerScore_Nty =
{
	{1, 1, 'm_vecPowerInfoList',			'PstBuYuPowerInfo', 100,'拥有的所有炮'},
}


CS_C2G_BuYu_Buff_Get_Req =
{
	{1		, 1     , "m_vecIndex"			,"SHORT"	, 200	, '鱼唯一id'},
	{2		, 1		, 'm_nEffectIndex'		,'UINT'	, 1		, 'buff效果的索引值'},
	{3		, 1     , "m_nRobotId"			,"UINT"	, 1		, '机器人ID, 如果是自己填0'},
}

CS_G2C_BuYu_Buff_Get_Ack =
{
	{1		, 1     , "m_vecIndex"			,"UINT"	, 100	, '鱼唯一id'},
	{2		, 1     , "m_nResult"			,"INT"	, 1		, '1为还可以继续使用特效0为生效积分已完成,可以关闭特效了'},
	{3		, 1     , "m_nEffectIndex"		,"UINT"	, 1		, 'buff效果的索引值'},
	{4		, 1     , "m_vecAddCoin"		,"UINT"	, 100	, '增加金币'},
	{5		, 1     , "m_nCoin"				,"UINT"	, 1		, '用户身上的金币'},
	{6		, 1     , "m_nAccountID"		,"UINT"	, 1		, '用户ID'},
}

CS_G2C_BuYu_Buff_List_Nty =
{
	{1		, 1     , "m_vecBuffList"		,"PstBuYuBuffData"	, 100		, 'buff的效果列表'},
}


CS_C2G_BuYu_Disable_Msg_Nty =
{
	{1		, 1     , "m_nStatus"			,"UINT"				, 1			, '0 开放协议发送,1 关闭协议发送'},
}

CS_G2C_BuYu_Notice_Msg_Nty =
{
	{1		, 1     , "m_nType"				,"UINT"				, 1			, '0 炮台过期'},
	{2		, 1     , "m_nItemID"			,"UINT"				, 1			, '物品id'},
	{3		, 1     , "m_nParam1"			,"UINT"				, 1			, '扩展参数'},
}


CS_C2G_BuYu_Active_Task_Award_Req = 
{

}

CS_G2C_BuYu_Active_Task_Award_Ack =   
{
	{1		, 1     , "m_vecAwardList"		,"PstBuyuActiveAward"	, 100		, '活动奖励列表'},
}
                                     
CS_C2G_BuYu_Get_Active_Task_Award_Req = 
{
	{1		, 1     , "m_nAwardID"			,"SHORT"	, 1			, '奖励id'},
}

CS_G2C_BuYu_Get_Active_Task_Award_Ack = 
{
	{1		, 1     , "m_nAwardID"			,"SHORT"	, 1		, '奖励id'},
	{2		, 1     , "m_nRet"			    ,"SHORT"	, 1			, '0成功1失败'},
}

CS_C2G_BuYu_Dismiss_YY_Table_Req = 
{
}

CS_G2C_BuYu_Dismiss_YY_Table_Ack = 
{
	{1		, 1		, "m_nAccountID"	,"UINT"				, 1		, '帐号id,'},
	{2		, 1		, "m_nReason"		,"SHORT"			, 1		, '解散的原因,'},
}
 	
CS_C2G_BuYu_Qiangzhuang_YY_Table_Req = 
{
	{1		, 1     , "m_nState"		    ,"SHORT"	, 1		, '1 强庄 2 不抢'},
}

CS_G2C_BuYu_Qiangzhuang_YY_Table_Ack = 
{
	{1		, 1     , "m_nState"		    ,"SHORT"	, 1		, '1 强庄 2 不抢'},
	{2		, 1     , "m_nPose"		        ,"SHORT"	, 1		     , '玩家位置id'},
}

CS_C2G_BuYu_Begin_YY_Table_Req = 
{
}

CS_G2C_BuYu_Begin_YY_Table_Ack = 
{
	{1		, 1     , "m_nResult"		    ,"SHORT"	, 1		, '开局结果0为成功 其他为失败'},
}

CS_G2C_BuYu_State_YY_Nty = 
{
	{1		, 1     , "m_nState"		,"UINT"	, 1		, '状态'},
	{2		, 1     , "m_nTime"		    ,"UINT"	, 1		, '倒计时 0表示这个状态没倒计时，比如暂停状态'},
}

CS_C2G_BuYu_Check_YY_Table_Req = 
{
	{1		, 1     , "m_nPose"		        ,"UINT"	, 1		, '被审核的玩家位置id'},
	{2		, 1     , "m_nType"		        ,"UINT"	, 1		, '1通过 2拒绝'},
}
		
CS_G2C_BuYu_Check_YY_Table_Ack = 
{
	{1		, 1     , "m_nPose"		        ,"UINT"	, 1		, '被审核的玩家位置id'},
	{2		, 1     , "m_nType"		        ,"UINT"	, 1		, '1通过 2拒绝'},
}
		
CS_C2G_BuYu_Vote_YY_Table_Req = 
{
	{1		, 1     , "m_nAgree"		,"UINT"	, 1		, '1同意解散 2反对解散'},
}
		
CS_G2C_BuYu_Vote_YY_Table_Ack = 
{
	{1		, 1     , "m_vecVote"		,"PstBuYuVoteData"	, 6		, '投票列表'},
	{2		, 1     , "m_nType"		    ,"UINT"	            , 1		, '1激活 2返回结果 3结束'},
	{3		, 1     , "m_nResult"		,"UINT"	            , 1		, '0不用理会 1解散'},
	{4		, 1     , "m_nLastTime"		,"UINT"	            , 1		, '剩余时间'},
}

CS_C2G_BuYu_Vote_YY_Start_Req =
{
}
              
CS_G2C_BuYu_Vote_YY_Start_Ack =
{
	{1		, 1     , "m_nRet"		,"INT"	, 1		, '发起结果0 成功 -1 重复发起 -2 发起时段不对'},
	{2		, 1     , "m_nPose"		,"INT"	, 1		, '发起解散者的座位'},
}

CS_C2G_BuYu_Get_YY_Nty =
{
	{1		, 1     , "m_nIndex"		,"UINT"	, 1		, '渔网碰撞到的鱼ID'},
	{2		, 1		, 'm_nPowerScore'	,'UINT'	, 1		, '炮的倍数'},
}

CS_G2C_BuYu_Get_YY_Nty =
{
	{1		, 1		, "m_nAccountID"	,"UINT"				, 1		, '帐号id'},
	{2		, 1		, "m_nIndex"	    ,"UINT"				, 1		, '渔网碰撞到的鱼ID'},
	{3		, 1		, 'm_nCoin'	        ,'INT'	            , 1		, '刷新开炮人的金币数量'},
	{4		, 1		, 'm_nHostCoin'	    ,'INT'	            , 1		, '刷新庄家的金币数量'},
	{5		, 1		, 'm_nAward'	    ,'INT'	            , 1		, '闲家获得奖励'},
}

CS_G2C_BuYu_Balance_YY_Nty = 
{
	{1		, 1     , "m_vecBalance"		,"PstBuYuBalanceData"	, 6		, '投票列表'},
	{2		, 1		, 'm_nEndTime'	    	,'INT'	            	, 1		, '结束时间'},
}


CS_C2G_BuYu_Kick_YY_Req =
{
	{ 1		, 1		, 'm_nPose'		,		'UINT'		, 1		, '位置id'},
}

CS_G2C_BuYu_Kick_YY_Ack =
{
	{ 1		, 1		, 'm_nPose'		,		'UINT'		, 1		, '位置id'},
}	

CS_G2C_BuYu_SceneInit_YY_Nty = 
{
	{1		, 1     , "m_nState"		,"SHORT", 1		, '状态'},
	{2		, 1     , "m_nLastTime"		,"UINT"	, 1		, '剩余时间'},
	{3		, 1     , "m_nTotalTimes"	,"UINT"	, 1		, '总局数'},
	{4		, 1     , "m_nCurTimes"		,"UINT"	, 1		, '当前局'},
	{5		, 1     , "m_nType"		    ,"SHORT", 1		, '桌子类型1-3 1-5'},
	{6		, 1     , "m_nPS"		    ,"UINT"	, 1		, '密码'},
	{7		, 1     , "m_nJoin"		    ,"SHORT", 1	    , '是否中途加入 1允许2不允许'},
	{8		, 1     , "m_nPose"		    ,"SHORT", 1		, '庄家位置'},
	{9		, 1     , "m_nCreativeID"   ,"UINT" , 1		, '房间创建者'},
}

CS_G2C_BuYu_Playerdata_YY_Nty =
{
	{1		, 1     , "m_vecPlayers"	,"PstBuyuYYPlayerData"	, 6		, '玩家数据'},
	{2		, 1     , "m_nOwerID"		,"UINT"					, 1		, '房主'},
}

CS_G2C_BuYu_Make_Host_YY_Nty =
{
	{1		, 1     , "m_nPose"		,"UINT"	, 1		, '庄家位置'},
	{2		, 1     , "m_nPowers"	,"UINT"	, 1		, '子弹数'},
	{3		, 1     , "m_vecData"	,"PstBuyuYYQiangzhuang"	, 6		, '玩家强庄状态'},
}

CS_C2G_BuYu_Change_YY_Req =
{
	{1		, 1     , "m_nPowerScore"		,"UINT"	, 1		, '炮倍数'},
}

CS_G2C_BuYu_Change_YY_Ack =
{
	{1		, 1		, "m_nAccountID"	,"UINT"				, 1		, '帐号id'},
	{2		, 1     , "m_nPowerScore"	,"UINT"				, 1		, '炮倍数'},
}

CS_G2C_BuYu_Send_Games_YY_Nty = 
{
	{1		, 1		, "m_nGames"	,"UINT"				, 1		, '第几局'},
}

CS_G2C_BuYu_Update_Ower_YY_Nty = 
{
	{1		, 1		, "m_nOwerID"	,"UINT"				, 1		, '新房主id'},
}

CS_G2C_BuYu_GameMaintenance_Nty =
{
	{ 1		, 1		, 'm_nOption'			, 'SHORT'				, 1	   , '1-维护 2-取消维护，如果场景重启会将此维护状态同步给场景（如果是维护状态）'},
	{ 2		, 1		, 'm_nStartTime'		, 'UINT'				, 1	   , '维护开始时间'},
	{ 3		, 1		, 'm_nEndTime'			, 'UINT'				, 1	   , '维护结束时间'},
}

CS_G2C_BuYu_Syn_YY_Nty = 
{
	{1		, 1     , "m_vecData"	,"PstBuyuYYSynData"	, 6		, '玩家数据'},
}

CS_C2G_BuYu_Ask_PlayerData_YY_Req =
{
	{1		, 1		, "m_nAccountID"	,"UINT"				, 1		, '对方的账号id'},
}

CS_G2C_BuYu_ASK_PLayerData_YY_Ack =
{
	{1		, 1		, "m_nAccountID"	,"UINT"				, 1		, '对方的账号id'},
	{2		, 1		, "m_nHostTime"	    ,"UINT"				, 1		, '做庄次数'},
	{3		, 1		, "m_nCurScore"	    ,"INT"				, 1		, '当前积分'},
}

CS_G2C_BuYu_Tunpao_Returnback_Nty = 
{
	{1		, 1		, "m_nAccountID"	,"UINT"				, 1		, '更改账号id'},
	{2		, 1		, "m_nCoin"	        ,"UINT"				, 1		, '返还的金币'},
}


-- 客户端到服务器场景
CS_M2C_BuYu_Scenepersonalinfo_Nty = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1     , '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_faceId'			, 'UINT'				, 1 	, '头像ID'},
	{ 3		, 1		, 'm_nickname'			, 'STRING'				, 1 	, '昵称'},
	{ 4		, 1		, 'm_goldCoin'			, 'UINT'				, 1 	, '金币数量'},
	{ 5		, 1		, 'm_level'				, 'UINT'				, 1 	, '等级'},
}

CS_M2C_BuYu_EnterdomRoom_Nty = 
{
	{ 1		, 1     , 'm_gameAtomTypeId'  	, 'UINT'        		, 1     , '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_result'			, 'SHORT'				, 1	  	, '0:成功, -1:已在游戏中，不允许同时玩多款游戏, -2:不在规定时间段内,-3:不在规定时刻,-4:参数非法'},
	{ 3		, 1		, 'm_roomInfo'			, 'PstBuYuRoomAttr'		, 1	  	, '当前房间信息'},
    { 4		, 1     , 'm_roomIdArr'			, 'PstBuYuRoomIdAttr' 	, 4096 	, '房间编号数组'},
 	{ 5		, 1		, 'm_faceId'			, 'UINT'				, 1 	, '头像ID'},
	{ 6		, 1		, 'm_nickname'			, 'STRING'				, 1 	, '昵称'},
	{ 7		, 1		, 'm_goldCoin'			, 'UINT'				, 1 	, '金币数量'},
	{ 8		, 1		, 'm_level'				, 'UINT'				, 1 	, '等级'},
}

CS_M2C_BuYu_EnterdomRoomPhone_Nty = 
{
	{ 1		, 1     , 'm_gameAtomTypeId'  	, 'UINT'        		, 1     , '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_result'			, 'SHORT'				, 1	    , '0:成功, -1:已在游戏中，不允许同时玩多款游戏, -2:不在规定时间段内,-3:不在规定时刻,-4:参数非法'},
	{ 3		, 1		, 'm_roomId'			, 'UINT'				, 1 	, '房间编号(自由房填服务器生成的编号，其他填0)'},
	{ 4		, 1		, 'm_type'				, 'UBYTE'				, 1 	, '0:正常进入，1：断线重连 2: 通知快速加入（由EnterScene中带入标记）'},
	{ 5		, 1		, 'm_goldCoin'			, 'UINT'				, 1 	, '金币值'},
}

CS_M2C_BuYu_ScenePlayerList_Nty = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1     , '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_playerArr'			, 'PstBuYuPlayerAttr'	, 4096  , '自由房场景内玩家列表'},
}

CS_M2C_BuYu_ScenePlayerInout_Nty = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'					, 1        , '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_playerInOut'		, 'PstBuYuPlayerInOutAttr'	, 4096     , '进入/退出玩家'},
	{ 3		, 1		, 'm_playerArr'			, 'PstBuYuPlayerAttr'	    , 4096     , '进入玩家的属性(备注：只有进入玩家才需要读取该属性)'},
}

CS_M2C_BuYu_UpdateRoomIDList_Nty  = 
{
	{ 1		, 1     ,'m_gameAtomTypeId'  	, 'UINT'        		, 1     , '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_roomInfo'			, 'PstBuYuRoomAttr'			, 1		, '自由房房间大厅信息'},
	{ 3		, 1     , 'm_roomIdArr'			, 'PstBuYuRoomIdAttr' 		, 4096 , '房间编号数组(自由房填服务器生成的编号，其他填0)'}, 	
}

CS_M2C_BuYu_ApplyTableChair_Ack = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'					, 1		, '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_result'			, 'SHORT'					, 1		, '0:成功，-1:桌子已满人,-2:桌子不满，但椅子上已有人, -3:金币不足,-4:道具不足'},
	{ 3		, 1		, 'm_roomId'			, 'UINT'					, 1 	, '房间编号(自由房填服务器生成的编号，其他填0)'},
	{ 4		, 1		, 'm_tableInfo'			, 'PstBuYuTableAttr'			, 1		, '桌子信息'},	
}

CS_M2C_BuYu_BeforeGameTable_Nty = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'		, 'UINT'								, 1     , '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_roomId'				, 'UINT'								, 1     , '房间编号'},
	{ 3		, 1		, 'm_tableId'				, 'UINT'								, 1     , '桌子编号'},
	{ 4		, 1		, 'm_minScore'				, 'UINT'								, 1     , '底分'},
	{ 5		, 1		, 'm_moveType'				, 'SHORT'								, 1 	, '进入/退出类型,0:进入，1：退出,2:断线重连'},
	{ 6		, 1		, 'm_moveChairId'			, 'UINT'								, 1     , '进入/退出的椅子编号'},	
	{ 7		, 1		, 'm_beforeGameChair'		, 'PstBuYuBeforeGameChair'				, 1024  , '椅子数组'},
	{ 8		, 1		, 'm_beforeGameUser'		, 'PstBuYuBeforeGameUser'				, 1024  , '玩家数组'},
}

CS_M2C_BuYu_UpdateRoomTable_Nty = 
{
	{ 1		, 1     ,'m_gameAtomTypeId'  	, 'UINT'        		, 1     , '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_roomInfo'			, 'PstBuYuRoomAttr'			, 1		, '自由房房间大厅信息'},
	{ 3		, 1     , 'm_roomIdArr'			, 'PstBuYuRoomIdAttr' 		, 4096 , '房间编号数组(自由房填服务器生成的编号，其他填0)'}, 	
}

CS_M2C_BuYu_EnterSpecificRoomHall_Ack = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'	, 1	, '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_result'			, 'SHORT'	, 1	, '0:成功, -1:游戏中，不能进入其他房间'},
	{ 3		, 1		, 'm_roomInfo'			, 'PstBuYuRoomAttr'	, 1	   , '当前房间信息'},
	{ 4		, 1     , 'm_roomIdArr'			, 'PstBuYuRoomIdAttr' 		, 4096 , '房间编号数组'},
}

CS_M2C_BuYu_ExitRoomHall_Ack = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'	, 1	   , '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_roomId'			, 'UINT'	, 1    , '房间编号(自由房填服务器生成的编号，其他填0)'},
	{ 3		, 1		, 'm_result'			, 'SHORT'	, 1		, '0:成功, -1:不在房间中'},
	{ 4		, 1		, 'm_type'				, 'SHORT'	, 1		, '退出原因 1-进入场景失败 2-分配桌子失败 3-游戏中强退 4-选桌界面直接离开场景'},
	{ 5		, 1		, 'm_goldCoin'			, 'UINT'	, 1		, '金币， 针对退出游戏情况下有用，其它情况为0'},
	{ 6		, 1		, 'm_exitType'			, 'SHORT'	, 1		, '1-游戏强退 2-其它为退出场景'},
}

CS_M2C_BuYu_FastEnterTable_Ack = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1		, '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_result'			, 'SHORT'				, 1		, '0:成功，-1:场景玩家信息不存在,-2:背包没有符合进该房间的炮,-3:该玩家已经在游戏中,-4:座位已经坐满'},
	{ 3		, 1		, 'm_roomId'			, 'UINT'				, 1 	, '房间编号(自由房填服务器生成的编号，其他填0)'},
	{ 4		, 1		, 'm_tableInfo'			, 'PstBuYuTableAttr'	, 1		, '桌子信息'},	
}

CS_M2C_BuYu_FastEnterTablePhone_Ack = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'		, 'UINT'						, 1		, '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_result'				, 'SHORT'						, 1		, '0:成功，-1:房间已满，请进入其他房间大厅,-2:金币不足'},
	{ 3		, 1		, 'm_roomId'				, 'UINT'						, 1 	, '房间编号(自由房填服务器生成的编号，其他填0)'},
	{ 4		, 1		, 'm_tableId'				, 'UINT'						, 1 	, '桌子编号'},
	{ 5		, 1		, 'm_minScore'				, 'UINT'						, 1     , '底分'},
	{ 6		, 1		, 'm_beforeGameChair'		, 'PstBuYuBeforeGameChair'		, 1024  , '椅子数组'},
	{ 7		, 1		, 'm_beforeGameUser'		, 'PstBuYuBeforeGameUser'		, 1024  , '玩家数组'},
}

CS_M2C_BuYu_EnterTableVerify_Ack = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'	, 1	, '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_result'			, 'SHORT'	, 1	, '0:验证通过，-1：密码不对, -2:桌主不同意,-3:桌子已满人'},	
	{ 3		, 1		, 'm_roomId'			, 'UINT'	, 1 , '房间编号(自由房填服务器生成的编号，其他填0)'},
	{ 4		, 1		, 'm_tableId'			, 'UINT'	, 1 , '桌子编号'},
	{ 5		, 1		, 'm_masterNickName'	, 'STRING'	, 1 , '桌主昵称'},
}

CS_M2C_BuYu_ExitGame_Ack = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'		, 'UINT'								, 1     , '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_result'				, 'SHORT'								, 1		, '0:成功, -x:失败'},
}

CS_C2M_BuYu_ApplyTableChair_Req = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'	, 1	, '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_roomId'			, 'UINT'	, 1 , '房间编号(自由房填服务器生成的编号，其他填0)'},
	{ 3		, 1		, 'm_tableId'			, 'UINT'	, 1 , '桌子编号'},
	{ 4		, 1		, 'm_chairId'			, 'UINT'	, 1 , '椅子编号'},
	{ 5		, 1		, 'm_minScore'			, 'UINT'	, 1 , '底分(桌主设置)'},
	{ 6		, 1		, 'm_validateType'		, 'USHORT'	, 1 , '验证类型，0:不需要验证，1:询问桌主验证,2:密码验证(桌主设置)'},
	{ 7		, 1		, 'm_key'				, 'STRING'	, 1 , '进桌密钥(仅当验证类型是密码验证时，桌主设置)'},
}

CS_C2M_BuYu_EnterTableVerify_Req = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'	, 1	, '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_roomId'			, 'UINT'	, 1 , '房间编号(自由房填服务器生成的编号，其他填0)'},
	{ 3		, 1		, 'm_tableId'			, 'UINT'	, 1 , '桌子编号'},
	{ 4		, 1		, 'm_key'				, 'STRING'	, 1 , '进桌密钥(仅当验证类型是密码验证时,需要填写)'},
	{ 5		, 1		, 'm_minScore'			, 'UINT'	, 1 , '底分(桌主设置)'},
}

CS_C2M_BuYu_EnterSpecificRoomHall_Req = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'	, 1	, '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_roomId'			, 'UINT'	, 1 , '房间编号(自由房填服务器生成的编号，其他填0)'},
}

CS_C2M_BuYu_FastEnterTable_Req = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'	, 1	, '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_roomId'			, 'UINT'	, 1 , '房间编号(自由房填服务器生成的编号，其他填0)'},
} 

CS_C2M_BuYu_FastEnterTablePhone_Req = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'	, 1	, '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_roomId'			, 'UINT'	, 1 , '房间编号(自由房填服务器生成的编号，其他填0)'},
}

CS_C2M_BuYu_ExitRoomHall_Req = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'	, 1	   , '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_roomId'			, 'UINT'	, 1    , '房间编号(自由房填服务器生成的编号，其他填0)'},
	{ 3		, 1		, 'm_nOutType'			, 'UINT'	, 1 	,'退出游戏的通知,0-游戏中強退 1 炮台过期导致没有可用的炮台踢出房间 2 维护替人 3-子弹超速踢人 4-超时不开火踢人'},
} 

CS_C2M_BuYu_ExitGame_Req = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'		, 'UINT'								, 1     , '游戏最小配置类型ID'},
}

CS_C2M_BuYu_Refresh_Next_Table_Req =
{
	{ 1		, 1		, 'm_nStart'			, 'UINT'				, 1		, '桌子的开始id'},
}

CS_M2C_BuYu_Refresh_Next_Table_Ack =
{
	{ 1		, 1		, 'm_nStart'			, 'UINT'				, 1		, '桌子的开始id'},
	{ 2		, 1		, 'm_roomInfo'			, 'PstBuYuRoomAttr'		, 1	  	, '当前房间信息'},
}


CS_C2M_BuYu_GetTaskID_Req =
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1		, '游戏最小配置类型ID'},
}

CS_M2C_BuYu_GetTaskID_Ack =
{
	{1		, 1     , "m_nTaskID"			,"UINT"				, 1			, '任务id,用来获取到当前已解锁炮台的'},
}

CS_C2M_BuYu_Create_YY_Table_Req = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'	, 1	, '游戏最小配置类型ID'},
	{ 2  	, 1		, 'm_nTimesIndex'	    , 'SHORT'	, 1	, '房间配置索引'},
	{ 3  	, 1		, 'm_nJoin'	            , 'SHORT'	, 1	, '设置加入方式 0 允许中途加入 其他是不允许'},
	{ 4  	, 1		, 'm_nHelp'	            , 'SHORT'	, 1	, '设置帮助开房 0 帮助他人开房 其他是给自己创建房间'},
}

CS_M2C_BuYu_Create_YY_Table_Ack = 
{
	{ 1		, 1		, 'm_nPassword'	    , 'UINT'	, 1	, '进房暗号'},
	{ 2		, 1		, 'm_nResult'	    , 'INT'	, 1	, '返回结果0成功,其他失败'},
	{ 3		, 1		, 'm_nHelp'	   		, 'UINT'	, 1	, '设置帮助开放 0 帮助他人开放 其他是给自己创建房间'},
	{ 4  	, 1		, 'm_nTimesIndex'	    , 'SHORT'	, 1	, '房间配置索引'},
}
 
CS_C2M_BuYu_Enter_YY_Table_Req = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'	, 1	, '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_nPassword'	        , 'UINT'	, 1	, '六位密码'},
}
 
CS_M2C_BuYu_Enter_YY_Table_Ack = 
{
	{ 1		, 1		, 'm_nResult'	        , 'INT'	, 1	, '结果码0成功 -1-2-3各种失败码'},
}
  
CS_C2M_BuYu_Mgr_YY_Table_Req = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'	, 1	, '游戏最小配置类型ID'},
}
    
CS_M2C_BuYu_Mgr_YY_Table_Ack = 
{
	{ 1		, 1		, 'm_vecYYTableList'	, 'PstBuYuYYTableData'	, 20	, 'YY桌子管理页面'},
}
    
CS_C2M_BuYu_Dismiss_YY_Table_Req = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'	, 1	, '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_nTableID'			, 'UINT'	, 1 , '桌子编号'},
}

CS_M2C_BuYu_Dismiss_YY_Table_Ack = 
{
	{ 1		, 1		, 'm_nResult'	        , 'INT'		, 1	, '结果码0成功 -1-2-3各种失败码'},
	{ 2		, 1		, 'm_nTableID'			, 'UINT'	, 1 , '桌子编号'},
	{ 3		, 1		, 'm_nPassword'			, 'UINT'	, 1 , '房间号'},
}

CS_C2M_BuYu_GetStatus_YY_Req =
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'	, 1	, '游戏最小配置类型ID'},
}

CS_M2C_BuYu_GetStatus_YY_Ack =
{
	{ 1		, 1		, 'm_nResult'			, 'INT'	, 1 , '返回结果 0 不在房间 其他 已在房间中'},
	{ 2		, 1		, 'm_nPassword'	   		, 'UINT', 1	, '进房暗号'},
}

CS_C2M_BuYu_ClientChat_Nty = 
{
	{ 1,	1, 'm_nMsgType'		, 'UINT'				, 1 	, '0表情 1快速聊天 2互动动画 3文字 4语音'},
	{ 2,	1, 'm_nOption'		, 'UINT'				, 1 	, '设置0，1，2表示索引值，3表示颜色 4无意义'},
	{ 3,	1, 'm_nAccountId'	,'UINT'					, 1		, '玩家Id'},
	{ 4,	1, 'm_nLength' 		, 'UINT' 				, 1  	, '数据长度（文字长度 或者语音长度）'},
	{ 5,	1, 'm_szData' 		, 'STRING' 				, 1  	, '二进制数据'},
}
CS_M2C_BuYu_ClientChat_Nty = 
{
	{ 1,	1, 'm_nMsgType'		, 'UINT'				, 1 	, '0表情 1快速聊天 2互动动画 3文字 4语音'},
	{ 2,	1, 'm_nOption'		, 'UINT'				, 1 	, '设置0，1，2表示索引值，3表示颜色 4无意义'},
	{ 3,	1, 'm_nAccountId'	,'UINT'					, 1		, '玩家Id'},
	{ 4,	1, 'm_nLength' 		, 'UINT' 				, 1  	, '数据长度（文字长度 或者语音长度）'},
	{ 5,	1, 'm_szData' 		, 'STRING' 				, 1  	, '二进制数据'},
}

CS_M2C_BuYu_KickClient_Nty =
{
	{1, 1, 'm_nOutType',			'UINT', 1 ,'退出游戏的通知,1 炮台过期导致没有可用的炮台踢出房间 2 维护替人 3-子弹超速踢人 4-超时不开火踢人'},
}

---------------------------------------------捕鱼选桌相关协议 add by peanut begin---------------------------------------------------
CS_C2M_BuYu_RefreshTable_Req =
{

}

CS_M2C_BuYu_RefreshTable_Ack =
{
	{ 1		, 1		, 'm_tableArr'			, 'PstBuYuTableAttr'	, 4096	, '桌子数组'},
	{ 2		, 1		, 'm_nCount'			, 'INT'					, 1 	, '游戏中人数'},
	{ 3		, 1		, 'm_nType'				, 'SHORT'				, 1 	, '推送方式 1-主动请求全推 2-有变化部分推'},
	{ 4		, 1		, 'm_nResult'			, 'INT'					, 1 	, '结果 0成功 其他失败'},
}

---------------------------------------------捕鱼选桌相关协议 add by peanut end-----------------------------------------------------