--
-- FishingDownloadManager
-- Author: chenzhanming
-- Date: 2017-12-08 12:55
-- 捕鱼资源下载管理器
--
local FishingUtil            =   require("src.app.game.Fishing.src.FishingCommon.utils.FishingUtil")


local FishingDownloadManager = class("FishingDownloadManager")

FishingDownloadManager.instance = nil

FishingDownloadManager.nodeType = 
{
   sprite = 1, -- 精灵
   image  = 2, -- 图片
   button = 3, -- 按钮
}

-- 获捕鱼资源下载管理器实例
function FishingDownloadManager:getInstance()
  if FishingDownloadManager.instance == nil then
    FishingDownloadManager.instance = FishingDownloadManager.new()
  end
    return FishingDownloadManager.instance
end

-- 账号id         accountId  
-- 头像id         faceId     
-- 图片路径       headPath
-- 节点类型       nodeType
-- 节点大小       nodeSize
-- 节点           node
-- 下载           isDown
-- 回调           callBack
-- self.userHeadPathInfo = { [params.accountId"and"faceId] = {accountId , faceId , headPath , callBack} }
-- local params2 = { m_nAccountId = info.m_nAccountId , m_faceId = info.m_faceID, m_callBack =  handler(self,self.showHeadView ) , m_nodeType  = 3,
--                                               m_node      = btn,
--                                               m_nodeSize  = cc.size(80,80),}
-- FishingDownloadManager:getInstance():loadFacePath( params2 )


function FishingDownloadManager:ctor()
   self.pathIndex = 0
   self.userHeadPathInfo = { }
   self.winFacePaths = {}
end


function FishingDownloadManager:loadFacePath( params )
   --params.m_nAccountId params.m_faceId  params.m_callBack
   if not params.m_faceId or not params.m_nAccountId then
      return
   end
   --dump( params )
   self.pathIndex = self.pathIndex + 1
   local pathKey = "fishingHeadKey"..self.pathIndex
   local _faceId = tonumber( params.m_faceId )%7
 --  if _faceId > 0 and _faceId <= GlobalDefine.SystemHeadMax then
       self.userHeadPathInfo[ pathKey ] = {  
                                              accountId = params.m_nAccountId , 
                                              faceId    = _faceId,
                                              headPath  = ToolKit:getHead(_faceId),
                                              callBack  = params.m_callBack ,
                                              textureResType = 0 ,
                                              nodeType  = params.m_nodeType,
                                              node      = params.m_node,
                                              nodeSize  = params.m_nodeSize,
                                          }
--    elseif _faceId >= GlobalDefine.UserDefineHeadMin then 
--        self.userHeadPathInfo[ pathKey ] = {   accountId = params.m_nAccountId ,   faceId    = params.m_faceId, callBack  = params.m_callBack ,
--                                                      nodeType = params.m_nodeType,  node =  params.m_node ,nodeSize  = params.m_nodeSize,}
--        local isExist = false
--        if device.platform == "windows" then 
--            local url = "http://image.milaichess.com/" .. AliYunUtil.headPath .. params.m_nAccountId .. "/" .. params.m_faceId..".jpg"
--            url = url .. "?x-oss-process=image/format,jpg"
--            local picPath = device.writablePath .. "netSprite/" .. crypto.md5(url) .. ".jpg" -- 文件保存路径
--            if UpdateFunctions.isFileExist( picPath ) then
--                isExist = true
--                self.userHeadPathInfo[ pathKey ].headPath = picPath
--                self.userHeadPathInfo[ pathKey ].textureResType = ccui.TextureResType.localType 
--            end
--        else 
--           local headPath = device.writablePath .. params.m_nAccountId .. "/" .. _faceId .. ".jpg"
--            if UpdateFunctions.isFileExist( headPath ) then
--                isExist = true 
--                self.userHeadPathInfo[ pathKey ].headPath = headPath
--                self.userHeadPathInfo[ pathKey ].textureResType = ccui.TextureResType.localType 
--            end
--        end
--        if not isExist  then
--           self.userHeadPathInfo[ pathKey ].headPath = g_SystemAvatarData[ 1 ].pngFileName  
--           self.userHeadPathInfo[ pathKey ].textureResType = ccui.TextureResType.plistType
--           params.m_pathKey = pathKey                        
--           self:downloadPlayerHeads( params )
--        end
--    end
    self:dealWithCallBack( pathKey )
end



function FishingDownloadManager:dealWithCallBack( pathKey )
    --- print("FishingDownloadManager:dealWithCallBack")
    -- print("pathKey",pathKey)
    local userHeadPathInfoData = self.userHeadPathInfo[ pathKey ]
    if userHeadPathInfoData  then
        if not tolua.isnull( userHeadPathInfoData.node )then
           if userHeadPathInfoData.nodeType == FishingDownloadManager.nodeType.sprite then
                if userHeadPathInfoData.textureResType == ccui.TextureResType.plistType then
                   userHeadPathInfoData.node:setSpriteFrame( userHeadPathInfoData.headPath )
                elseif userHeadPathInfoData.textureResType == ccui.TextureResType.localType then
                    userHeadPathInfoData.node:setTexture( userHeadPathInfoData.headPath )
                end
           elseif userHeadPathInfoData.nodeType == FishingDownloadManager.nodeType.image then
                userHeadPathInfoData.node:loadTexture( userHeadPathInfoData.headPath, 1)
           elseif userHeadPathInfoData.nodeType == FishingDownloadManager.nodeType.button then
                userHeadPathInfoData.node:loadTextures( userHeadPathInfoData.headPath ,userHeadPathInfoData.headPath,userHeadPathInfoData.headPath, userHeadPathInfoData.textureResType )
           end
           local _nodeSize = userHeadPathInfoData.node:getContentSize()
           userHeadPathInfoData.node:setScaleX( userHeadPathInfoData.nodeSize.width/_nodeSize.width  )
           userHeadPathInfoData.node:setScaleY( userHeadPathInfoData.nodeSize.height/_nodeSize.height )
        end

        if userHeadPathInfoData.callBack  then
            local _params = { 
                               m_nAccountId = userHeadPathInfoData.accountId,
                               m_faceId     = userHeadPathInfoData.faceId,
                               m_headPath   = userHeadPathInfoData.headPath,
                            }
            userHeadPathInfoData.callBack( _params )
        end
    end
end


function FishingDownloadManager:downloadPlayerHeads( params )
      -- 下载图片
      UpdateFunctions.mkDir(device.writablePath ..params.m_nAccountId ) -- 创建文件夹
      if device.platform == "windows" then -- windows平台, 用NetSprite下载
          local url = "http://image.milaichess.com/" .. AliYunUtil.headPath .. params.m_nAccountId .. "/" .. params.m_faceId..".jpg"
          url = url .. "?x-oss-process=image/format,jpg"
          local NetSprite = require("app.hall.base.ui.UrlImage")
          local netSprite = NetSprite.new(url, true, handler(self, self.win32NetSpriteCallBack), false)
          netSprite:retain()
          local isExist,fileName = netSprite:getUrlMd5()
          self.winFacePaths[ fileName ] = {  _pathKey = params.m_pathKey , _netSprite = netSprite   }
      else
          if  AliYunUtil:getAliyunState(false) then 
              local headPath = device.writablePath .. params.m_nAccountId .. "/" .. params.m_faceId .. ".jpg"
              local disPath = AliYunUtil.headPath .. params.m_nAccountId .. "/" .. params.m_faceId .. ".jpg"
              FishingUtil:getInstance():downloadFileEx( tostring(params.m_pathKey) , disPath, headPath , handler(self ,self.isDownFileBack ))
          end
      end
end


function FishingDownloadManager:win32NetSpriteCallBack(result, fileName )
    if result then
       --print("-----------fileName=",fileName)
      -- dump( self.winFacePaths )
        if self.winFacePaths[ fileName ] then
           -- print("self.winFacePaths[ fileName ]")
           local pathKey = self.winFacePaths[ fileName ]._pathKey
          -- print("pathKey=",pathKey)
           if pathKey then
              --dump(self.userHeadPathInfo)
              if self.userHeadPathInfo[ pathKey ] then
                  print("self.userHeadPathInfo[ pathKey ]")
                  self.userHeadPathInfo[ pathKey ].headPath = fileName
                  self.userHeadPathInfo[ pathKey ].textureResType = ccui.TextureResType.localType
                  self:dealWithCallBack( pathKey )
                  --self.winFacePaths[ fileName ]._netSprite:release()
              end
           end
        end
    end
end

function FishingDownloadManager:isDownFileBack( args )
    local strlist = string.split(args, ";")
    if tonumber( strlist[1] ) == 1 then
       if  self.userHeadPathInfo[ strlist[2] ]  then
            self.userHeadPathInfo[ strlist[2] ].headPath = strlist[3]
            self.userHeadPathInfo[ strlist[2] ].textureResType = ccui.TextureResType.localType
            self:dealWithCallBack( strlist[2] )
       end
    else
      print("下载头像失败!")
    end
end

function FishingDownloadManager:clearAllTextures()
    for k,v in pairs( self.userHeadPathInfo ) do
        if v.isDown == ccui.TextureResType.localType then
           cc.Director:getInstance():getTextureCache():removeTextureForKey( v.headPath )
        end
    end
    self.pathIndex = 0
    self.userHeadPathInfo = { }
    self.winFacePaths = {}
end


-- 销毁捕鱼资源下载管理器
function FishingDownloadManager:onDestory()
    FishingDownloadManager.instance = nil
end


return FishingDownloadManager