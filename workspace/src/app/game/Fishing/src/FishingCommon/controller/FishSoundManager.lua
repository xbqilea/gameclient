--
-- FishSoundManager
-- Author: chenzhanming
-- Date: 2017-05-16 16:02:38
-- 捕鱼声音管理器
-- 

local FishGlobal              = require("src.app.game.Fishing.src.FishGlobal")
local FishGameDataController  = require("src.app.game.Fishing.src.FishingCommon.controller.FishGameDataController")
local FishSoundManager = class("FishSoundManager")
 
FishSoundManager.FishSoundOnOrOff =  "FISH_SOUND_ON_OFF"  -- 声音开关的键
--------------------------------------------------------------------------
FishSoundManager.FishHallBgS          =  "bgm_hall.mp3"        -- 捕鱼大厅音效
FishSoundManager.WaveEnter            =  "wave_enter.mp3"      -- 波浪音效
FishSoundManager.NetOpen              =  "NetOpen.mp3"         -- 撒网音效
FishSoundManager.CoinFly              =  "fish_SilverCoin.wav" -- 获取金币音效
FishSoundManager.SwitchSilo           =  "gun_switch.mp3"      -- 切换枪音效
FishSoundManager.NoBullet             =  "gun_nobullet.mp3"    -- 没有子弹音效
FishSoundManager.ButtonPressed        =  "gun_nobullet.mp3"    -- 按钮点击音效
FishSoundManager.PowerUnlock          =  "gun_unlock.mp3"      -- 炮台解锁音效
FishSoundManager.BuySuccess           =  "buy_success.mp3"     -- 购买商品成功音效
FishSoundManager.EffectBoom           =  "fish_SuperBomb.mp3"  -- 爆炸音效
FishSoundManager.EffectDuwu           =  "effect_duwu.mp3"     -- 毒雾音效
FishSoundManager.EffectElectric       =  "effect_electric.mp3" -- 电球音效
FishSoundManager.EffectZhuanpan       =  "effect_zhuanpan.mp3" -- 转盘音效
FishSoundManager.EffectCitun          =  "effect_citun.mp3"    -- 刺豚音效
FishSoundManager.ItemDrop             =  "item_drop.mp3"       -- 物品掉落音效
FishSoundManager.SkillCrit            =  "skill_crit.mp3"      -- 释放暴击技能
FishSoundManager.SkillIce             =  "skill_ice.mp3"       -- 释放冰冻技能
FishSoundManager.SkillLock            =  "skill_lock.mp3"      -- 释放锁定技能
FishSoundManager.Tips                 =  "tips.mp3"            -- 提示音效
FishSoundManager.ChatPaopao           =  "chat_paopao.mp3"     -- 聊天泡泡音效
FishSoundManager.EnterRoom            =  "enter_room.mp3"      -- 点击房间图片音效
FishSoundManager.ShopOpen             =  "shop_open.mp3"       -- 打开商城窗口音效
FishSoundManager.WindowsOpen          =  "windows_open.mp3"    -- 窗口弹出音效
FishSoundManager.EffectChooseBank     =  "effect_choose_bank.mp3" --选庄音效




FishSoundManager.instance = nil

-- 获取捕鱼声音管理器实例
function FishSoundManager:getInstance()
	if FishSoundManager.instance == nil then
		FishSoundManager.instance = FishSoundManager.new()
	end
    return FishSoundManager.instance
end

function FishSoundManager:ctor()
   
end


---- 取得背景音乐开关状态
--function FishSoundManager:getFishGameMusicStatus()
--	--return g_GameMusicUtil:getGameMusicStatus( FishSoundManager.FishSoundOnOrOff )
--	return g_GameMusicUtil:getMusicStatus()
--end

---- 获取背景音乐开关状态
--function FishSoundManager:setFishGameMusicStatus( isOpen )
--	--g_GameMusicUtil:setGameMusicStatus( FishSoundManager.FishSoundOnOrOff, isOpen)
--	g_GameMusicUtil:setMusicStatus( isOpen )
--end

---- 取得音效开关状态
--function FishSoundManager:getFishGameSoundStatus()
--	--return g_GameMusicUtil:getGameSoundStatus( FishSoundManager.FishSoundOnOrOff )
--    return g_GameMusicUtil:getSoundStatus()
--end

----设置音效开关状态
--function FishSoundManager:setFishGameSoundStatus( isOpen )
--	--g_GameMusicUtil:setGameSoundStatus( FishSoundManager.FishSoundOnOrOff , isOpen)
--	g_GameMusicUtil:setSoundStatus( isOpen )
--end


function FishSoundManager:stopBGMusic()
	 audio.stopMusic(true)
end

function FishSoundManager:stopAllSound()
	 audio.stopAllSounds()
end


--播放音乐和音效
function FishSoundManager:playEffect( effect_type )
	--print("---FishSoundManager:playEffect---")
	--if g_GameMusicUtil:getGameSoundStatus( FishSoundManager.FishSoundOnOrOff ) then
	    local soundPath = FishGameDataController:getInstance():getSoundPath(effect_type)
	    if soundPath then
	       if cc.FileUtils:getInstance():isFileExist( soundPath  ) then
	       	  --print("播放音乐和音效",soundPath)
	          g_AudioPlayer:playEffect(soundPath, false)
	       else
              print("找不到声音资源路径",soundPath)
	       end
	    end
	--end
end


-- 播放背景音乐
function FishSoundManager:playBgMusic( music_type )
	local soundPath = FishGameDataController:getInstance():getSoundPath( music_type )
	--if not audio.isMusicPlaying() then
		--if g_GameMusicUtil:getGameMusicStatus( FishSoundManager.FishSoundOnOrOff ) then 
		   if soundPath then
		   	  if cc.FileUtils:getInstance():isFileExist( soundPath  ) then 
		   	  	 print("播放背景音乐",soundPath)
		         g_AudioPlayer:playMusic(soundPath, true)
		      else
                 print("找不到声音资源路径",soundPath)
		      end
		   end
		--end
	--end
end

function FishSoundManager:onDestory()
    FishSoundManager.instance = nil
end

return FishSoundManager