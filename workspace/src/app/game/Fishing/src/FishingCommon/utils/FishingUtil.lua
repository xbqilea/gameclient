--
-- FishingUtil
-- Author: chenzhanming
-- Date: 2017-07-12 11:55:38
-- 捕鱼工具类
--

local FishGameDataController  = require("src.app.game.Fishing.src.FishingCommon.controller.FishGameDataController")
local DlgAlert                = require("app.hall.base.ui.MessageBox")

local FishingUtil = class("FishingUtil")

FishingUtil.instance = nil

function FishingUtil:ctor()
   
end

-- 获取捕鱼工具类实例
function FishingUtil:getInstance()
	if FishingUtil.instance == nil then
		FishingUtil.instance = FishingUtil.new()
	end
    return FishingUtil.instance
end


--上传文件扩展
function FishingUtil:uploadFileEx(_id,_disPath, _localPath, _callback)
    if not AliYunUtil:getAliyunState(true) then
        print("[ERROR]: upload file failed, aliyun oss is nil !")
        AliYunUtil:askAliyunKey()
        return 
    end
    if device.platform == "android" then
        local javaClassName = "com/milai/aliyun/AliYunClient"
        local javaMethodName = "uploadFileEx"
        local javaParams = {_id,_disPath, _localPath, _callback}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V"
        local ok = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
    elseif device.platform == "ios" then
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AliyunHelp"
        local args = { id = _id, dis = _disPath, loc = _localPath,  callback = _callback }
        local ok = luaoc.callStaticMethod(className,"uploadFileEx",args)
        if not ok then
        end
    end
end

--下载文件扩展
function FishingUtil:downloadFileEx(_id,_disPath, _localPath, _callback)
    if not AliYunUtil:getAliyunState(false) then
       print("[ERROR]: download file failed, aliyun oss is nil !")
       AliYunUtil:askAliyunKey()
       return
    end
    if device.platform == "android" then
        local javaClassName = "com/milai/aliyun/AliYunClient"
        local javaMethodName = "downloadFileEx"
        local javaParams = {_id,_disPath, _localPath, _callback}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V"
        local ok = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
    elseif device.platform == "ios" then
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AliyunHelp"
        local args = { id = _id, dis = _disPath, loc = _localPath,  callback = _callback }
        local ok = luaoc.callStaticMethod(className,"downloadFileEx",args)
        if not ok then
        end
    end
end

--开启网络检测
function FishingUtil:startNetCheck( _callback  )
    if device.platform == "android" then
        local javaClassName = "com/milai/util/NetWorkUtil"
        local javaMethodName = "sartCheckNet"
        local javaParams = {_callback}
        local javaMethodSig = "(I)V"
        local ok = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
    elseif device.platform == "ios" then
         
    end
end

--开启网络检测
function FishingUtil:stopNetCheck()
    if device.platform == "android" then
        local javaClassName = "com/milai/util/NetWorkUtil"
        local javaMethodName = "stopCheckNet"
        local javaParams = {}
        local javaMethodSig = "()V"
        local ok = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
    elseif device.platform == "ios" then
         
    end
end


function FishingUtil:showErroCodeTipsByErrorCode( errorCode )
    local tips =  FishGameDataController:getInstance():getErrorTipsByerrorcode( errorCode )
    TOAST( tips )
end

-- 格式化时间 00:00
function FishingUtil:formatTime( times )
   local timeStr = "00:00"
   if times then
      if times >= 0 and times <= 3600 then
         local min =  math.floor(times/ 60)
         local second = math.fmod( times,60)   
         local minStr = ""
         if min <= 9 and min >=0 then
            minStr = "0"..min
         else
            minStr = min
         end
         local secondStr = ""
         if second <= 9 and second >= 0 then
            secondStr = "0"..second
         else
            secondStr = second
         end
         timeStr = minStr..":"..secondStr
      end
   end
   return timeStr
end

function FishingUtil:isExsitScene( __scene )
    
end

-- numerator 分子，denominator 分母 
function FishingUtil:randomOK( numerator, denominator )
      math.randomseed(tostring(os.time()):reverse():sub(1, 7)) --设置时间种子
      local value =  math.random( 1,denominator )              --产生1到100之间的随机数
      local isOK = false
      if value <= numerator then
         isOK = true
      end
      return isOK
end 

--[[
function FishingUtil:checkWx(callback)
    if device.platform == "windows"  then 
         callback()
    else
        if ToolKit:checkBindWx() then
           callback()
        else
            local function bindBack(code)
                if code == 0 then
                    callback()
                elseif code == 1 then
                    local message = "检测到此微信已有绑定账号，是否使用此微信号登录"
                    local data = { message = message,leftStr="是",rightStr="否" }
                    local dlg = DlgAlert.new()
                    dlg:TowSubmitAlert(data, function ()
                        ToolKit:loginByWx( function(m_code) if m_code== 0 then callback() end end )
                    end)
                    dlg:showDialog()
                end
            end 
            local message = "为了您更好的体验渔友房，请绑定微信"
            local data = { message = message,leftStr="前往微信",rightStr="取消",isGray=true }
            local dlg = DlgAlert.new()
            dlg:TowSubmitAlert(data, function ()
                ToolKit:bindingWx( bindBack)
            end)
            dlg:showDialog()
        end
    end
end
--]]

function FishingUtil:captureScreen( fileName ,callback)
    local size = cc.Director:getInstance():getWinSize()
    --获取屏幕尺寸，初始化一个空的渲染纹理对象
    local renderTexture = cc.RenderTexture:create(size.width, size.height, cc.TEXTURE2_D_PIXEL_FORMAT_RGB_A8888 ,gl.DEPTH24_STENCIL8_OES)
    --清空并开始获取
    renderTexture:beginWithClear(0.0, 0.0, 0.0, 0.0)
    renderTexture:ignoreAnchorPointForPosition(true)
    renderTexture:setAnchorPoint(cc.p(0,0))
    --遍历场景节点对象，填充纹理到RenderTexture中
    local scene = cc.Director:getInstance():getRunningScene()
    scene:visit()
    --结束获取
    renderTexture:endToLua() 
    --保存文件
    renderTexture:saveToFile(fileName , cc.IMAGE_FORMAT_JPEG )
    --使用schedule在下一帧中调用callback函数
    local fullPath = cc.FileUtils:getInstance():getWritablePath().. fileName
    local scheduler   = require("framework.scheduler")
    local function handerBack()
        callback( fullPath )
    end
    scheduler.performWithDelayGlobal( handerBack , 0.5)
end


function FishingUtil:selectTips( tipsKey , tipValue  )
    cc.UserDefault:getInstance():setStringForKey( tipsKey , tipValue)
end


function FishingUtil:checkIsSelectEx( tipsKey )
   local tipsValue =  cc.UserDefault:getInstance():getStringForKey( tipsKey , "")
   local isSelect = false
   if tipsValue == "isHide" then
      isSelect = true
   end
   print("tipsValue",tipsValue)
   print("isSelect=",isSelect)
   return isSelect
end

--本日不在
function FishingUtil:checkIsSelect( tipsKey )
   local tipsValue =  cc.UserDefault:getInstance():getStringForKey( tipsKey , "")
   local nowData =  os.date("%x", os.time())
   local isSelect = false
   if tipsValue == nowData then
      isSelect = true
   end
   print("tipsValue",tipsValue)
   print("isSelect=",isSelect)
   return isSelect
end

function FishingUtil:getCellSocre( roomMinScore)
    local  cellScore = 1
    local josn = json.decode( roomMinScore )
    -- dump(josn)
    for k,v in pairs(josn) do
        cellScore = k 
    end
    return cellScore
end

function FishingUtil:saveClassicPowerItemId( powerItemId )
   local powerItemIdKey= "oldFishPowerItemId"..Player.getAccountID()
   cc.UserDefault:getInstance():setStringForKey( powerItemIdKey , powerItemId )
end

function FishingUtil:getClassicPowerItemId()
   local powerItemIdKey= "oldFishPowerItemId"..Player.getAccountID()
   local powerItemId = cc.UserDefault:getInstance():getStringForKey( powerItemIdKey , "0")
   return  tonumber( powerItemId )
end

-- 检查是否是AI的 id 
function FishingUtil:checkAIuserid(_userId)

    if not _userId   then
        print("没有输入uid")
        return false
    end

    if tonumber(_userId ) >= GlobalDefine.AI_User_id[1]  and tonumber(_userId ) <= GlobalDefine.AI_User_id[2]  then
        return true
    end

    return false
   
end

-- 销毁捕鱼游戏数据管理器
function FishingUtil:onDestory()
    FishingUtil.instance = nil
end



return FishingUtil