--
-- FishingArmature
-- Author: chenzhanming
-- Date: 2017-06-02 10:00:00
-- 骨骼动画封装
-- 
local FishingArmatureResource = require ("src.app.game.Fishing.src.FishingCommon.data.FishingArmatureResource")

	
local FishingArmature = class("FishingArmature" ,function()
	  return display.newNode()
end)

FishingArmature._movementType = ccs.MovementEventType.complete  --动画类型（非循环/循环)

function FishingArmature:ctor( resourceById ,animationLayer, _handlerBack)
     self.m_animationLayer = animationLayer
     self.handlerBack = _handlerBack
     self.armatureResourceInfo  = FishingArmatureResource.getArmatureResourceById( resourceById )
     self:loadAnimation()
end

--加载骨骼动画数据
function FishingArmature:loadAnimation()
  --dump(self.armatureResourceInfo)
	ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( self.armatureResourceInfo.configFilePath )
  local isFileExist = cc.FileUtils:getInstance():isFileExist( self.armatureResourceInfo.configFilePath )
	self.armature = ccs.Armature:create(self.armatureResourceInfo.armatureName)
	--动画播放回调
	self.armature:getAnimation():setMovementEventCallFunc(handler(self,self.animationEvent))
	self:addChild( self.armature )
end

--播放动画
function FishingArmature:playAnimation()
	self.armature:getAnimation():play( self.armatureResourceInfo.animationName )
end

--动画播放回调
function FishingArmature:animationEvent( armatureBack, movementType, movementID )
    --非循环播放一次
    if movementType == ccs.MovementEventType.complete then
       if movementID == self.armatureResourceInfo.animationName then
            armatureBack:stopAllActions()
            if self.handlerBack then
               self.handlerBack()
            end
            self:clearArmatureFileInfo()
            self:removeFromParent()  
       end
    end
 end

--清除骨骼数据
function  FishingArmature:clearArmatureFileInfo()
	 ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo( self.armatureResourceInfo.configFilePath )
end

--停止动画
function  FishingArmature:stopAllActionsEx()
    if not tolua.isnull( self.armature ) then
       self.armature:stopAllActions()
    end
end


return  FishingArmature
