--
-- Author: chenzhanming
-- Date: 2017-04-27 16:02:38
-- 捕鱼技能管理器
-- 锁定技能，冰封万里，狂暴技能

local FishGlobal = require("src.app.game.Fishing.src.FishGlobal")
local FishSkillManager = class("FishSkillManager")

FishSkillManager.instance = nil

--技能类型
FishSkillManager.skillType = {    
                                  FROZENSKILL = 1,  -- 冰冻技能
                                  LOCKSKILL   = 2,  -- 锁定技能
                                  CRITSKILL   = 3,  -- 暴击技能
                             }
--技能种类个数
FishSkillManager.skillTypeNum = 3

-- 获取捕鱼技能管理器实例
function FishSkillManager:getInstance()
	if FishSkillManager.instance == nil then
		FishSkillManager.instance = FishSkillManager.new()
	end
    return FishSkillManager.instance
end

function FishSkillManager:ctor()
   self:initData()
end

-- 初始化技能相关数据
function FishSkillManager:initData()
	-- 技能数据列表
    self.skillDataList = {}
    for k,v in pairs( FishSkillManager.skillType ) do
    	self.skillDataList[v] = { }
    	for i=1,FishGlobal.PlayerCount do
    		self.skillDataList[v][i] = {   
		                                    m_isOpen    = false,        -- 技能是否开启
		                                    m_skillType = v    ,        -- 技能类型
		                                    m_skillCD   = 0    ,        -- 技能CD时间
                                       }
    	end
     end
     self.m_currentLockTarget = nil      -- 当前锁定的目标 
     self.m_lcockTargets = {}            -- 锁定的目标
end


function FishSkillManager:getSkillDataBySkillType( __skillType , __viewChair )
	local skillData = self.skillDataList[ __skillType ][ __viewChair ]
	return skillData
end

function FishSkillManager:setSkillDataBySkillType( __skillType , __viewChair , __skillData )
	if self.skillDataList[ __skillType ] then
       self.skillDataList[ __skillType ][__viewChair] = {     
			                                                 m_isOpen    = __skillData.m_isOpen    ,
			                                                 m_skillType = __skillData.m_skillType ,
			                                                 m_skillCD   = __skillData.m_skillCD   ,
                                                        }
	end
end


function FishSkillManager:setSkillDataIsOpen(__skillType ,__viewChair, __isOpen )
	self.skillDataList[ __skillType ][ __viewChair ].m_isOpen = __isOpen
end

function FishSkillManager:getOtherFrozenSkillOpen( __viewChair )
	local skillDatas =   self.skillDataList[ FishSkillManager.skillType.FROZENSKILL ]
	local isOpen = false
	for i = 1, FishGlobal.PlayerCount do
		if i ~=  __viewChair then
			if skillDatas[ i ] then
	          if skillDatas[ i ].isOpen then
	             isOpen = true
	             break
	          end  
	        end
		end
	end
    return isOpen
end


-- 设置锁定目标
function FishSkillManager:setCurrentLockTarget( lockTarget )
	self.m_currentLockTarget = lockTarget
end

-- 获取锁定目标
function FishSkillManager:getCurrentLockTarget()
	return  self.m_currentLockTarget 
end


function FishSkillManager:getSkillDatas()
	return  self.skillDataList
end

function FishSkillManager:clearData()
	self.skillDataList = {}
    for k,v in pairs( FishSkillManager.skillType ) do
    	self.skillDataList[v] = { }
    	for i=1,FishGlobal.PlayerCount do
    		self.skillDataList[v][i] = {   
		                                    m_isOpen    = false,        -- 技能是否开启
		                                    m_skillType = v    ,        -- 技能类型
		                                    m_skillCD   = 0    ,        -- 技能CD时间
                                       }
    	end
     end
     self.m_currentLockTarget = nil      -- 当前锁定的目标 
end

function FishSkillManager:onDestory()
    FishSkillManager.instance = nil
end 


return FishSkillManager