---------------------------------
-- Author: pang
-- Date: 2017-03-22 14:00:55
-- 生成一些效果动画
-- modify by chengzhanming 
-- Date 2017-05-02
-----------------------------------
local FishSoundManager           = require("src.app.game.Fishing.src.FishingCommon.controller.FishSoundManager")
local FishGlobal                 = require("src.app.game.Fishing.src.FishGlobal")
local FishingAnimationManager    = require("src.app.game.Fishing.src.FishingCommon.controller.FishingAnimationManager")
local FishGameDataController     = require("src.app.game.Fishing.src.FishingCommon.controller.FishGameDataController") 
local FishingUtil                = require("src.app.game.Fishing.src.FishingCommon.utils.FishingUtil")


local effect_info = {
  	["coin"]         = { play_once=false, pic="fish_img_coinG_00000.png",    name="fish_img_coinG_%05d.png",frames=11,  firstFrame=0, dt=0.04},
    ["coinSamll"]    = { play_once=false, pic="fish_small_coin_00000.png",   name="fish_small_coin_%05d.png",frames= 8,  firstFrame=0, dt=0.1},
    ["coinS"]        = { play_once=false, pic="fish_img_coinS_00000.png",    name="fish_img_coinS_%05d.png",frames=4,  firstFrame=1, dt=0.02},
    ["zhuanpanGlow"] = { play_once=false, pic="zhuanpan_glow_00000.png",     name="zhuanpan_glow_%05d.png", frames=9, firstFrame=0, dt=0.08},
    ["toxicSmog"]    = { play_once=false, pic="buyu_e_poison_00000.png",     name="buyu_e_poison_%05d.png", frames= 9, firstFrame=0, dt=0.10},
    ["dianqiu"]      = { play_once=false, pic="dianqiu_00000.png",           name="dianqiu_%05d.png",       frames= 15,firstFrame=1, dt=0.10},
    ["lightning"]    = { play_once=false, pic="fish_lightning_chain_00000.png", name="fish_lightning_chain_%05d.png",  frames= 4, firstFrame=0, dt=0.10 },
    ["wave"]         = { play_once=false, pic="buyu_e_wave_00001.png",       name="buyu_e_wave_%05d.png",   frames= 8, firstFrame=1, dt=0.11},
    ["lockTargetN"]  = { play_once=false, pic="buyu_target_n_00000.png",     name="buyu_target_n_%05d.png", frames= 6, firstFrame=0, dt=0.10},
    ["lockTargetW"]  = { play_once=false, pic="buyu_target_w_00000.png",     name="buyu_target_w_%05d.png", frames= 6, firstFrame=0, dt=0.10},
    ["critAni"]      = { play_once=false, pic="glowquan_00000.png",          name="glowquan_%05d.png",      frames= 4, firstFrame=0, dt=0.10},
    ["fishDeadGlow"] = { play_once=true,  pic="fish_glow_00000.png",         name="fish_glow_%05d.png",     frames= 8, firstFrame=0, dt=0.07},
    ["friendPaoPao"] = { play_once=false, pic="pao_00000.png",               name="pao_%05d.png",           frames= 15, firstFrame=0, dt=0.1},
    ["bossboom"] =     { play_once= true, pic="buyu_e_bossboom_00000.png",   name="buyu_e_bossboom_%05d.png",frames= 8, firstFrame=0, dt=0.08},
    ["elightStart"] =  { play_once= true, pic="buyu_e_lightning_start_00000.png",   name="buyu_e_lightning_start_%05d.png", frames= 5, firstFrame=0, dt=0.08},
    ["elightMain"] =   { play_once= true, pic="buyu_e_lightningmain_00000.png",    name="buyu_e_lightningmain_%05d.png",  frames= 4, firstFrame=0, dt=0.08},
    ["elightEnd"] =    { play_once= true, pic="buyu_e_lightning_end_00000.png",     name="buyu_e_lightning_end_%05d.png", frames= 3, firstFrame=0, dt=0.08},
    ["ciStart"] =      { play_once= true, pic="buyu_e_ci_start_00000.png",          name="buyu_e_ci_start_%05d.png", frames= 6, firstFrame=0, dt=0.1},
    ["ciMain"] =       { play_once= true, pic="buyu_e_ci_main_00000.png",           name="buyu_e_ci_main_%05d.png",  frames= 6, firstFrame=0, dt=0.08 },
    ["ciEnd"] =        { play_once= true, pic="buyu_e_ci_end_00000.png",            name="buyu_e_ci_end_%05d.png",   frames= 3, firstFrame=0, dt=0.08},
    ["fishNet"] =      { play_once= true, pic="fish_net00000.png",                  name="fish_net%05d.png",   frames= 2, firstFrame=0, dt=0.12},
    ["fishbomb"] =     { play_once= true, pic="fish_bomb_00000.png",                name="fish_bomb_%05d.png",  frames= 16, firstFrame=0, dt=0.1},
    ["fishbombteam"] = { play_once= true, pic="fish_bomb_team_00005.png",           name="fish_bomb_team_%05d.png",  frames= 16, firstFrame=0, dt=0.1},
    ["fishWater"]    = { play_once= false, pic="caustic_00001.png",               name="caustic_%05d.png",  frames= 16, firstFrame=1, dt=0.1},

}


local effect_plist = 
{
    ["coin"] = { "buyu_p_plist/p_buyu_commom_new_1.plist", },
    --["coinS"] = { "buyu_p_plist/p_buyu_common_ui.plist", },
    ["fishbomb"] = {"buyu_p_plist/p_buyu_bomb.plist"},
    ["fishbombteam"] = {"buyu_p_plist/p_buyu_bomb_team.plist"},
    ["fishWater"] = {"buyu_p_plist/p_buyu_wave_1.plist","buyu_p_plist/p_buyu_wave_2.plist"},  
}


local fish_plist = 
{
  ["fish01"] ={"buyu_fishFrame/p_buyu_fishs_1.plist",},
  ["fish02"] ={"buyu_fishFrame/p_buyu_fishs_1.plist",},
  ["fish03"] ={"buyu_fishFrame/p_buyu_fishs_1.plist",},
  ["fish04"] ={"buyu_fishFrame/p_buyu_fishs_1.plist",},
  ["fish05"] ={"buyu_fishFrame/p_buyu_fishs_1.plist",},
  ["fish06"] ={"buyu_fishFrame/p_buyu_fishs_1.plist",},
  ["fish07"] ={"buyu_fishFrame/p_buyu_fishs_1.plist",},
  ["fish08"] ={"buyu_fishFrame/p_buyu_fishs_1.plist",},
  ["fish09"] ={"buyu_fishFrame/p_buyu_fishs_1.plist",},
  ["fish10"] ={"buyu_fishFrame/p_buyu_fishs_2.plist",},
  ["fish11"] ={"buyu_fishFrame/p_buyu_fishs_2.plist",},
  ["fish12"] ={"buyu_fishFrame/p_buyu_fishs_2.plist",},
  ["fish13"] ={"buyu_fishFrame/p_buyu_fishs_2.plist",},
  ["fish14"] ={"buyu_fishFrame/p_buyu_fishs_3.plist","buyu_fishFrame/p_buyu_fishs_4.plist"},
  ["fish15"] ={"buyu_fishFrame/p_buyu_fishs_3.plist","buyu_fishFrame/p_buyu_fishs_4.plist"},
  ["fish16"] ={"buyu_fishFrame/p_buyu_fishs_4.plist"},
  ["fish17"] ={"buyu_fishFrame/p_buyu_fishs_5.plist"},
  ["fish18"] ={"buyu_fishFrame/p_buyu_fishs_6.plist","buyu_fishFrame/p_buyu_fishs_7.plist","buyu_fishFrame/p_buyu_fishs_8.plist"},
  ["fish19"] ={"buyu_fishFrame/p_buyu_fishs_9.plist"},
  ["fish20"] ={"buyu_fishFrame/p_buyu_fishs_10.plist","buyu_fishFrame/p_buyu_fishs_11.plist"},
  ["fish21"] ={"buyu_fishFrame/p_buyu_fishs_11.plist",},
  ["fish22"] ={"buyu_fishFrame/p_buyu_fishs_11.plist",},
  ["fish23"] ={"buyu_fishFrame/p_buyu_fishs_11.plist","buyu_fishFrame/p_buyu_fishs_12.plist",},
}

local EffectManager = {}

EffectManager.critAnis   = {}
EffectManager.isfrozen   = false
EffectManager.frozenType = 0
EffectManager.frozenSpr  = nil
EffectManager.listener   = nil
EffectManager.frameDisplayeListener =   nil  
EffectManager.needBlendSprs = {} 
EffectManager.redPackets = {}

EffectManager.resourceList_1 =
{
  -- [1] = "buyu_p_plist/buyu_e_lightning_fullscreen_0.plist",
  -- [2] = "buyu_p_plist/buyu_e_lightning_fullscreen_1.plist",
  -- [3] = "buyu_p_plist/buyu_e_lightning_fullscreen_2.plist",
  -- [4] = "buyu_p_plist/buyu_e_lightning_fullscreen_3.plist",
  -- [5] = "buyu_p_plist/buyu_e_lightning_fullscreen_4.plist",
  -- [6] = "buyu_p_plist/buyu_e_ci_fullscreen_0.plist",
  -- [7] = "buyu_p_plist/buyu_e_ci_fullscreen_1.plist",
  -- [8] = "buyu_p_plist/buyu_e_ci_fullscreen_2.plist",
  -- [9] = "buyu_p_plist/buyu_e_ci_fullscreen_3.plist",
  -- [10] = "buyu_p_plist/buyu_e_ci_fullscreen_4.plist",
  -- [11] = "buyu_p_plist/buyu_e_ci_fullscreen_5.plist",
  -- [12] = "buyu_p_plist/buyu_e_ci_fullscreen_6.plist",
  -- [1] = "buyu_fishFrame/p_buyu_fishs_13.plist",
  -- [2] = "buyu_fishFrame/p_buyu_fishs_14.plist",
  -- [3] = "buyu_fishFrame/p_buyu_fishs_15.plist",
  -- [4] = "buyu_fishFrame/p_buyu_fishs_16.plist",
  -- [5] = "buyu_fishFrame/p_buyu_fishs_19.plist",
}



function EffectManager.init() 
   EffectManager.addEventListenerForNeedBlendSprs()
   EffectManager.loadResource()
   EffectManager.redPackets = {}
   for i = 1,FishGlobal.PlayerCount do
       EffectManager.redPackets[i] = {}
   end
end


function EffectManager.loadResource()
    cc.Texture2D:setDefaultAlphaPixelFormat( cc.TEXTURE2_D_PIXEL_FORMAT_RGB_A4444 )
    for i=1,#EffectManager.resourceList_1 do
       local prefix, suffix = ToolKit:getPrefixAndSuffix(EffectManager.resourceList_1[i])
       cc.SpriteFrameCache:getInstance():addSpriteFrames(prefix..".plist",prefix..".png")
    end 
    cc.Texture2D:setDefaultAlphaPixelFormat( cc.TEXTURE2_D_PIXEL_FORMAT_RGB_A8888 )
end

function EffectManager.removeResource()
    for i=1,#EffectManager.resourceList_1 do
        local prefix, suffix = ToolKit:getPrefixAndSuffix(EffectManager.resourceList_1[i])
        display.removeSpriteFramesWithFile(prefix..".plist",prefix..".png")
    end 
end

function EffectManager.loadFishResourceByName( fishName )
   if fish_plist[ fishName ] then
      for k,v in pairs( fish_plist[ fishName ] )  do
         local prefix, suffix = ToolKit:getPrefixAndSuffix( v )
         cc.SpriteFrameCache:getInstance():addSpriteFrames(prefix..".plist",prefix..".png")
      end
   end
end

function EffectManager.removeAllFishResources()
   for k,fishList in pairs( fish_plist ) do
       for k,v in pairs( fishList ) do
           local prefix, suffix = ToolKit:getPrefixAndSuffix( v )
           cc.SpriteFrameCache:getInstance():addSpriteFrames(prefix..".plist",prefix..".png")
       end
   end
end


function EffectManager.addEventListenerForNeedBlendSprs()
    EffectManager.needBlendSprs = {} 
    -- EffectManager.frameDisplayeListener  = cc.EventListenerCustom:create(cc.ANIMATION_FRAME_DISPLAYED_NOTIFICATION,
    --                      function ( event )
    --                         local name = event:getEventName()
    --                         TOAST("EffectManager.frameDisplayeListener")
    --                         print("EffectManager.frameDisplayeListener")
    --                         --dump( EffectManager.needBlendSprs )
    --                         -- for k,needBlendSpr in pairs( EffectManager.needBlendSprs ) do
    --                         --     --print("needBlendSpr",needBlendSpr)
    --                         --     if not tolua.isnull( needBlendSpr ) then
    --                         --        if needBlendSpr:isVisible() then
    --                         --           needBlendSpr:setBlendFunc(gl.ONE, gl.ONE) 
    --                         --        else
    --                         --           EffectManager.needBlendSprs[k] = nil
    --                         --        end
    --                         --     else
    --                         --        EffectManager.needBlendSprs[k] = nil
    --                         --     end
    --                         -- end
    --                      end)
    -- cc.Director:getInstance():getEventDispatcher():addEventListenerWithFixedPriority(EffectManager.frameDisplayeListener, -1)
end

function EffectManager.removeEventListenerForNeedBlendSprs()
    -- if not tolua.isnull(EffectManager.frameDisplayeListener) then
    --    cc.Director:getInstance():getEventDispatcher():removeEventListener( EffectManager.frameDisplayeListener )
    --    EffectManager.frameDisplayeListener = nil
    -- end
    EffectManager.needBlendSprs = {} 
end

function EffectManager.blendSprs()
      for k,needBlendSpr in pairs( EffectManager.needBlendSprs ) do
          --print("needBlendSpr",needBlendSpr)
          if not tolua.isnull( needBlendSpr ) then
             if needBlendSpr:isVisible() then
                needBlendSpr:setBlendFunc(gl.ONE, gl.ONE) 
             else
                EffectManager.needBlendSprs[k] = nil
             end
          else
             EffectManager.needBlendSprs[k] = nil
          end
      end
end


function EffectManager.loadResourceByName( name )
   if effect_plist[ name ] then
     if name ~= "wave" then
        cc.Texture2D:setDefaultAlphaPixelFormat( cc.TEXTURE2_D_PIXEL_FORMAT_RGB_A4444 )
     end
     for k,v in pairs( effect_plist[ name ] ) do
         local prefix, suffix = ToolKit:getPrefixAndSuffix( v )
         cc.SpriteFrameCache:getInstance():addSpriteFrames(prefix..".plist",prefix..".png")
     end
     if name ~= "wave" then
        cc.Texture2D:setDefaultAlphaPixelFormat( cc.TEXTURE2_D_PIXEL_FORMAT_RGB_A8888 )
     end
   end
end

function EffectManager.removeResourceEx()
  for k,v in pairs( effect_plist ) do
     for h,j in pairs(v ) do
        local prefix, suffix = ToolKit:getPrefixAndSuffix( j )
        display.removeSpriteFramesWithFile(prefix..".plist",prefix..".png")
     end
  end
end

function   EffectManager.createEffectAnimation( name )
  EffectManager.loadResourceByName( name )
	local frames = display.newFrames(effect_info[name].name,effect_info[name].firstFrame,effect_info[name].frames)
  local anim = nil
  if frames then
	   anim = display.newAnimation(frames,effect_info[name].dt)
	   cc.AnimationCache:getInstance():addAnimation(anim,name)
  end
	return anim
end

function EffectManager.removeAllAnimtion()
    EffectManager.removeResourceEx()
    for k,v in pairs( effect_info ) do
        cc.AnimationCache:getInstance():removeAnimation( k )
    end
end

function EffectManager.createEffectAnimationEx( name )
    EffectManager.loadResourceByName( name )
    local animFrames = {}
    for i=1, effect_info[name].frames+1 do
        local frameStrName = string.format( effect_info[name].name ,i-1)
        local frame =  display.newSpriteFrame( frameStrName )
        if frame then
          local animationFrame = cc.AnimationFrame:create( frame, 1.0, { } )
          table.insert(animFrames, animationFrame)
        end
    end
    local animation = cc.Animation:create(animFrames , effect_info[name].dt)
    cc.AnimationCache:getInstance():addAnimation(animation,name)
    return animation
end

-- 全屏闪电特效
function EffectManager.playlightEffect( params )
      -- 闪电开始阶段
      local animationStart = cc.AnimationCache:getInstance():getAnimation("elightStart")
      if not animationStart then
         animationStart = EffectManager.createEffectAnimationEx("elightStart")
      end
      local elightStartAni = cc.Animate:create( animationStart)

      -- 闪电中期
      local animationMian = cc.AnimationCache:getInstance():getAnimation("elightMain")
      if not animationMian then
         animationMian = EffectManager.createEffectAnimationEx("elightMain")
      end
      local elightMainAni = cc.Animate:create( animationMian )

      -- -- 闪电后期
      -- local animationEnd = cc.AnimationCache:getInstance():getAnimation("elightEnd")
      -- if not animationEnd then
      --    animationEnd = EffectManager.createEffectAnimationEx("elightEnd")
      -- end
      -- local elightEndAni = cc.Animate:create( animationEnd )

      local targetNodeScale = 1.4286 * display.standardScale
      params.targetNode:setScale( targetNodeScale )

      local elightStartSpr = display.newSprite()
      local elightMainSpr = display.newSprite()
      --local elightEndSpr =  display.newSprite()
      params.targetNode:addChild( elightStartSpr ,11 )
      params.targetNode:addChild( elightMainSpr , 10 )
      --params.targetNode:addChild( elightEndSpr , 11 )
      
      local function hideElightStartSpr()
         elightStartSpr:setVisible( false )
      end
      local callFunc_1 = cc.CallFunc:create( hideElightStartSpr )

      local function hideElightMainSpr()
         elightMainSpr:setVisible( false )
         params.targetNode:removeFromParent()
      end
      local callFunc_2 = cc.CallFunc:create( hideElightMainSpr )

      elightStartSpr:setBlendFunc(gl.ONE, gl.ONE) 
      elightMainSpr:setBlendFunc(gl.ONE, gl.ONE) 
      --elightEndSpr:setBlendFunc(gl.ONE, gl.ONE) 
      table.insert(EffectManager.needBlendSprs,  elightStartSpr)
      table.insert(EffectManager.needBlendSprs,  elightMainSpr)
      --table.insert(EffectManager.needBlendSprs,  elightEndSpr)
      
      -- local listener  = cc.EventListenerCustom:create(cc.ANIMATION_FRAME_DISPLAYED_NOTIFICATION,
      --                    function ( event )
      --                       local name = event:getEventName()
      --                       if not tolua.isnull( elightStartSpr ) then
      --                           elightStartSpr:setBlendFunc(gl.ONE, gl.ONE) 
      --                           elightMainSpr:setBlendFunc(gl.ONE, gl.ONE) 
      --                           elightEndSpr:setBlendFunc(gl.ONE, gl.ONE) 
      --                       end
      --                    end)

      -- cc.Director:getInstance():getEventDispatcher():addEventListenerWithFixedPriority(listener, -1)
      -- local function hideElightEndSpr()
      --    elightEndSpr:setVisible(false)
      --    params.targetNode:setVisible( false )
      --   -- cc.Director:getInstance():getEventDispatcher():removeEventListener( listener )
      -- end
      -- local callFunc_3 = cc.CallFunc:create( hideElightEndSpr )

      elightStartSpr:runAction( cc.Sequence:create(elightStartAni , callFunc_1, nil) )
      elightMainSpr:runAction(  cc.Sequence:create( cc.DelayTime:create( 0.2 ) ,elightMainAni,elightMainAni,cc.FadeOut:create(0.15) ,callFunc_2, nil) )
      --elightEndSpr:runAction(  cc.Sequence:create(cc.DelayTime:create( 1.02 ) ,elightEndAni,callFunc_3, nil) )
end

-- 刺的全屏特效
function EffectManager.playCiEffect( params )
    -- 开始阶段
    local animationStart = cc.AnimationCache:getInstance():getAnimation("ciStart")

    if not animationStart then
       animationStart = EffectManager.createEffectAnimation("ciStart")
    end
    local ciStartAni = cc.Animate:create( animationStart)

    -- 中期
    local animationMian = cc.AnimationCache:getInstance():getAnimation("ciMain")
    if not animationMian then
       animationMian = EffectManager.createEffectAnimation("ciMain")
    end
    local ciMainAni = cc.Animate:create( animationMian )

    -- -- 后期
    -- local animationEnd = cc.AnimationCache:getInstance():getAnimation("ciEnd")
    -- if not animationEnd then
    --    animationEnd = EffectManager.createEffectAnimationEx("ciEnd")
    -- end
    -- local ciEndAni = cc.Animate:create( animationEnd )
   
    local targetNodeScale = 1.4286 * display.standardScale
    params.targetNode:setScale( targetNodeScale )

    --table.insert(EffectManager.needBlendSprs,  params.targetNode)
    
    -- local listener  = cc.EventListenerCustom:create(cc.ANIMATION_FRAME_DISPLAYED_NOTIFICATION,
    --                  function ( event )
    --                     local name = event:getEventName()
    --                     if not tolua.isnull( params.targetNode ) then
    --                        params.targetNode:setBlendFunc(gl.ONE, gl.ONE)  
    --                     end
    --                  end)

    -- cc.Director:getInstance():getEventDispatcher():addEventListenerWithFixedPriority(listener, -1)

    local function hideElightEndSpr()
       params.targetNode:removeFromParent()
       --params.targetNode:setVisible( false )
       --cc.Director:getInstance():getEventDispatcher():removeEventListener( listener )
    end
    local callFunc_3 = cc.CallFunc:create( hideElightEndSpr )

    params.targetNode:runAction(  cc.Sequence:create(ciStartAni ,ciMainAni,cc.FadeOut:create(0.15),callFunc_3, nil) )
      
end


function EffectManager.playAnimation( params )
    local animation = cc.AnimationCache:getInstance():getAnimation(params.name)
    if not animation then
       animation = EffectManager.createEffectAnimation(params.name)
    end
    local frame =  cc.SpriteFrameCache:getInstance():getSpriteFrame(effect_info[params.name].pic)
    if frame then
       params.node:setSpriteFrame( frame )
    end
    if animation then
        if effect_info[params.name].play_once then
            params.node:playAnimationOnce(animation,true)
        else
            params.node:playAnimationForever(animation)  
        end
    end
    --params.node:setPosition(cc.p(params.x,params.y))
end

function EffectManager.getEffect(params)
	local animation = cc.AnimationCache:getInstance():getAnimation(params.name)
	--没有动画再创建
	if not animation then
		animation = EffectManager.createEffectAnimation(params.name)
	end
	local effect = cc.Sprite:create()
  local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(effect_info[params.name].pic)
  if frame then
     effect:setSpriteFrame( frame )
  end
  if animation then
  	if effect_info[params.name].play_once then
  		effect:playAnimationOnce(animation,true)
  	else
  		effect:playAnimationForever(animation)	  
  	end
  end
	effect:setPosition(cc.p(params.x,params.y))
	return effect
end


function EffectManager.createMoveUpNumber( params )
    local  moveUpNumbeLabelAtlas = nil
    if params.count > 0 then
        --FishSoundManager:getInstance():playEffect( params.scoreSound  )
        local atlasPic = "buyu_number/fish_num_yellow.png"
        moveUpNumbeLabelAtlas = ccui.TextAtlas:create("0",atlasPic,33,38,".")
        moveUpNumbeLabelAtlas:setAnchorPoint(0.5,0.5)
        local callfunc = cc.CallFunc:create(function() moveUpNumbeLabelAtlas:removeFromParent() end)
        local str =  params.count
        moveUpNumbeLabelAtlas:setScale( 0.9 )
        moveUpNumbeLabelAtlas:setString( str )
        math.randomseed(tostring( os.time()  ):reverse():sub(1, 7)) -- 设置时间种子 
        local posTab = { cc.p(-20,20) , cc.p( 20,20) }
        local posIndex = math.random( 1,2 ) 
        local pos = posTab[ posIndex ]
        local moveTo = cc.MoveBy:create(0.5, pos )
        local scaleTo = cc.ScaleTo:create(0.5,1 )
        local spawn =  cc.Spawn:create(moveTo, scaleTo) 
        local scaleTo_1 = cc.ScaleTo:create(0.5,0.9)
        local posIndex_1 = math.random( 1,2 ) 
        local pos_1 = posTab[ posIndex ]
        local moveTo_1 = cc.MoveBy:create(0.5 , pos_1 )
        local spawn_1 =  cc.Spawn:create( moveTo_1 , scaleTo_1) 
        local action =  cc.Sequence:create( spawn, spawn_1, callfunc ,nil)
        moveUpNumbeLabelAtlas:setPosition(cc.p(params.x,params.y))
        moveUpNumbeLabelAtlas:runAction( action )
    end
    return moveUpNumbeLabelAtlas
end



function EffectManager.createMoveUpNumberOld( params )
    local  moveUpNumbeLabelAtlas = nil
    if params.count > 0 then
        local atlasPic = "buyu_number/fish_num_silver.png"
        if params._isOwer then
           atlasPic = "buyu_number/fish_num_yellow.png"
        end
        moveUpNumbeLabelAtlas = ccui.TextAtlas:create("0",atlasPic,33,38,".")
        moveUpNumbeLabelAtlas:setAnchorPoint(0.5,0.5)
        local callfunc = cc.CallFunc:create(function() moveUpNumbeLabelAtlas:removeFromParent() end)
        local str = ":".. params.count
        local moveTo = cc.MoveBy:create(0.6,cc.p(0,100))
        local action =  cc.Sequence:create(moveTo, callfunc ,nil)
        if params.ctriTimes > 1 and params.ctriTimes <=5 then
           local coinNum = params.count/ params.ctriTimes
           str = ":"..coinNum..";"..params.ctriTimes
           local scaleTo = cc.ScaleTo:create(0.06,1.5)
           action =  cc.Sequence:create( scaleTo , moveTo , callfunc ,nil)
        elseif params.ctriTimes > 5 and params.ctriTimes <= 10 then
           local coinNum = params.count/ params.ctriTimes
           str = ":"..coinNum..";"..params.ctriTimes
           local scaleTo = cc.ScaleTo:create(0.06,2)
           action =  cc.Sequence:create( scaleTo , moveTo , callfunc ,nil)
        elseif  params.ctriTimes > 10 then
           local coinNum = params.count/ params.ctriTimes
           str = ":"..coinNum..";"..params.ctriTimes
           local scaleTo = cc.ScaleTo:create(0.06,2.5)
           action =  cc.Sequence:create( scaleTo , moveTo , callfunc ,nil)
        end
        moveUpNumbeLabelAtlas:setString( str )
        moveUpNumbeLabelAtlas:setPosition(cc.p(params.x,params.y))
        moveUpNumbeLabelAtlas:runAction( action )
    end
    return moveUpNumbeLabelAtlas
end

function EffectManager.createBaoJi(params)
      local word = cc.Label:create()
      word:setString("暴击")
      word:setSystemFontSize(64)
      word:setColor(cc.c3b(255,0,0))
      word:runAction(cc.Sequence:create(cc.ScaleTo:create(0.1,4.0),cc.ScaleTo:create(0.1,2.0),cc.FadeOut:create(0.5),cc.CallFunc:create(
        function() word:removeFromParent() end),nil))
      word:setPosition(cc.p(params.x,params.y))
      return word
end


function EffectManager.createNet( params )
    local web = cc.Sprite:create()
    local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame("web_00003.png")
    if frame then
       web:setSpriteFrame( frame )
    else
       print("找不到","web_00003.png")
    end
    web:runAction(cc.Sequence:create(
      cc.ScaleTo:create(0.04, 1.1, 1.1),
      cc.ScaleTo:create(0.04, 0.8, 0.8),
      cc.FadeOut:create(0.5),
      cc.CallFunc:create(function()
        web:removeFromParent()
      end),nil))
    web:setPosition(cc.p(params.x,params.y))
    -- 撒网音效
    FishSoundManager:getInstance():playEffect( FishSoundManager.NetOpen  )
    return web
end

-- 转盘效果
function EffectManager.playTurnDisc( params )
    --params.prarentNode params.pos params.fishPngName   params.count params.isPhonefare
    cc.SpriteFrameCache:getInstance():addSpriteFrames("buyu_p_plist/p_buyu_commom_new_1.plist","buyu_p_plist/p_buyu_commom_new_1.png")
    local rootNode = cc.Node:create()
    rootNode:setPosition( params.pos )
    params.prarentNode:addChild( rootNode , FishGlobal.UILevel + 10 )

    FishSoundManager:getInstance():playEffect( FishSoundManager.EffectZhuanpan  )

    local zhuanpanbgSpr = display.newSprite("#fish_rewardFrame.png")
    rootNode:addChild(zhuanpanbgSpr,10 )
    zhuanpanbgSpr:runAction( cc.RepeatForever:create(cc.RotateBy:create(0.7, 360)) )
   

    --金币数量
    local countAtlas =   ccui.TextAtlas:create("0","buyu_number/fish_num_yellow.png",33,38,".")
    countAtlas:setAnchorPoint( cc.p(0.5,0.5 ) )
    --countAtlas:setPosition(cc.p(0,0) )
    rootNode:addChild( countAtlas ,15  )
    countAtlas:setString( params.count )
   
    local rotateTo_1 = cc.RotateBy:create(0.1, 25)
    local rotateTo_2 = cc.RotateBy:create(0.2, -50)
    local rotateTo_3 = cc.RotateBy:create(0.1, 25)

    countAtlas:runAction( cc.RepeatForever:create( cc.Sequence:create(rotateTo_1 , rotateTo_2 ,rotateTo_3)))
    local function removeRootNode()
        rootNode:removeFromParent()
    end 
    rootNode:runAction( cc.Sequence:create( cc.DelayTime:create( 5 ) ,  cc.CallFunc:create( removeRootNode  ),nil) )

end

-- 转盘效果
function EffectManager.playNewTurnDisc( params )
    --params.prarentNode params.pos params.fishPngName   params.count params.isPhonefare
    cc.SpriteFrameCache:getInstance():addSpriteFrames("buyu_p_plist/p_buyu_common_ui_1.plist","buyu_p_plist/p_buyu_common_ui_1.png")
    local rootNode = cc.Node:create()
    rootNode:setPosition( params.pos )
    params.prarentNode:addChild( rootNode , FishGlobal.UILevel + 10 )

    local _params = {
                        name = "zhuanpanGlow",
                        x    =  0,
                        y    =  0,
                     }
    -- 转盘背景
    -- local zhuanpanGlow = EffectManager.getEffect( _params )
    -- rootNode:addChild( zhuanpanGlow, 10)
    local zhuanpanbgSpr = display.newSprite("#fish_img_ray.png")
    rootNode:addChild(zhuanpanbgSpr,10 )
    zhuanpanbgSpr:setRotation( 0 ) 
    local  angleTotal = 360 * 3
    zhuanpanbgSpr:runAction( cc.RotateTo:create(5, angleTotal) )
    -- -- 转盘
    -- local disSpr = display.newSprite( "#fish_bg_dial.png" )
    -- rootNode:addChild( disSpr, 11)

   -- local rotateTo = cc.RotateTo:create(0.35, 180)
   -- local rotateTo_1 = cc.RotateTo:create(0.35, 360)
   -- disSpr:runAction( cc.RepeatForever:create(cc.Sequence:create(rotateTo, rotateTo_1,nil)))
    --disSpr:setRotation(0)  
    --local  angleTotal = 360 * 11
    -- 开始旋转动作  使用EaseExponentialOut(迅速加速，然后慢慢减速)  
    ---disSpr:runAction(cc.EaseExponentialOut:create(cc.RotateBy:create( 5,angleTotal )))
    --disSpr:runAction( cc.RotateTo:create(5, angleTotal) )

    --金币数量
    local countAtlas =   ccui.TextAtlas:create("0","buyu_number/fish_num_yellow.png",33,38,".")
    countAtlas:setAnchorPoint( cc.p(0.5,0.5 ) )
    countAtlas:setPosition(cc.p(0,-50) )
    rootNode:addChild( countAtlas ,15  )
    countAtlas:setString( params.count )
    local function removeRootNode()
        rootNode:removeFromParent()
    end 
    rootNode:runAction( cc.Sequence:create( cc.DelayTime:create( 5 ) ,  cc.CallFunc:create( removeRootNode  ),nil) )

end

-- 暴击效果
function EffectManager.playCritEffect( params )
    local rootNode = cc.Node:create()
    rootNode:setPosition( params.pos )
    params.prarentNode:addChild( rootNode , FishGlobal.LogicLevel + 10 )
    
    -- 暴击效果背景
    local critbgSpr = display.newSprite( "#fish_bg_guang_blue.png" )
    critbgSpr:setAnchorPoint(cc.p(0.5,0.5))
    rootNode:addChild( critbgSpr , 10 )
    local rotateTo = cc.RotateTo:create(0.5, 180)
    local rotateTo_1 = cc.RotateTo:create(0.5, 360)
    critbgSpr:runAction( cc.RepeatForever:create(cc.Sequence:create(rotateTo, rotateTo_1,nil)))
      
end


-- 金蟾转盘
function EffectManager.playToadTurnDisc( params  )
   --params.prarentNode params.pos params.targetPos,params.times ,params.isReversal ,params.coin
   -- 转盘弹出来的效果
   local rootNode = cc.Node:create()
   rootNode:setPosition( params.pos )
   params.prarentNode:addChild( rootNode , FishGlobal.UILevel + 10 )
   local toadPopAmture = FishingAnimationManager:getInstance():playArmatureAnimation(rootNode , 1005,cc.p(0,80))
   local moveTo = cc.MoveTo:create( 0.5, params.targetPos )
   
   -- print("playToadTurnDisc")
   FishGameDataController:getInstance():setCoinEffectIsFinished( params.chair )
   -- print("params.chair",params.chair)
   local coinIndex =  FishGameDataController:getInstance():getCoinEffectIndex( params.chair )
   rootNode:setTag( coinIndex )
   
   local turnNode = cc.Node:create()
   rootNode:addChild( turnNode )
   turnNode:setVisible( false )
   -- 数字背景
   local turnDiscBgSpr = display.newSprite("#fish_bg_lunpan.png")
   turnNode:addChild( turnDiscBgSpr )
   turnDiscBgSpr:setAnchorPoint( cc.p(0.5,0))

   
   -- 倍数转盘
   local turnDiscSpr = display.newSprite("#fish_txt_num.png")
   turnDiscSpr:setAnchorPoint( cc.p(0.5,0.5) )
   turnNode:addChild( turnDiscSpr )

   -- 转盘外壳
   local turnShellSpr = display.newSprite("#fish_img_lunpanb.png")
   turnShellSpr:setAnchorPoint( cc.p(0.5,0) )
   turnNode:addChild( turnShellSpr  )
   
   
   -- 箭头
   local arrowSpr = display.newSprite("#fish_img_jiantou.png")
   arrowSpr:setPosition( cc.p(0,180) )
   arrowSpr:setAnchorPoint( cc.p(0.5,0.5) )
   turnNode:addChild( arrowSpr )

   -- 黄标
   local yellowTagSpr = display.newSprite("#fish_img_xuanzhong.png")
   yellowTagSpr:setAnchorPoint(cc.p(0.5,0.5))
   yellowTagSpr:setPosition(cc.p(0,116) )
   yellowTagSpr:setVisible( false )
   turnNode:addChild( yellowTagSpr  )
   

   local function randomScore()
        FishingAnimationManager:getInstance():stopAndClearArmatureAnimation( toadPopAmture )
        turnNode:setVisible( true )
   end
   local moveTo   = cc.MoveTo:create( 0.9, params.targetPos )
   local callFunc = cc.CallFunc:create( randomScore )
   local angeleS = { [40] = 0, [50] = 315,[60] = 270, [70] = 225, [80] = 180,[90] = 135,[100] = 90,[110] = 45 }
   local angleTotal =  8 * 360 + angeleS[ params.times ]
   local easeExponentialOut = cc.EaseExponentialOut:create(cc.RotateBy:create( 3.5 ,angleTotal ))

   local  function reversal()
      if params.isReversal then
           rootNode:setRotation( 180 )
      end
   end
   local callFunc_2 = cc.CallFunc:create( reversal )
   
   local function showArrowSpr()
      yellowTagSpr:setVisible( true )
   end
   local callFunc_3 = cc.CallFunc:create( showArrowSpr )
  
   local function showZhanPangSound()
       FishSoundManager:getInstance():playEffect( FishSoundManager.EffectZhuanpan  )
   end

   local callFunc_4 = cc.CallFunc:create( showZhanPangSound )
   local  function removeRootNode( node )
      --print("removeRootNode")
      local tagIndex = node:getTag()
      FishGameDataController:getInstance():clearCoinEffectIsFinished( params.chair , tagIndex)
      local nativeCoin = g_GameController:getNativeCoinByChair(  params.chair )
      nativeCoin = nativeCoin + params.addCoin
      g_GameController:setNativeCoinByChair( params.chair , nativeCoin )

      if FishGameDataController:getInstance():coinEffectIsFinished( params.chair ) then
          local isEqual , coin = g_GameController:checkNativeWithServerCoin( params.chair )
          if not isEqual then
             nativeCoin = coin
          end
      end    

      params.fishMainLayer:updatePlayerGoldView( params.viewChair  , params.coin )
      rootNode:removeFromParent()
   end
   local callFunc_5 = cc.CallFunc:create( removeRootNode )
   local turnDiscSprAction =  cc.TargetedAction:create( turnDiscSpr,cc.Sequence:create(callFunc_4,easeExponentialOut,callFunc_3,nil))
   rootNode:runAction( cc.Sequence:create( cc.DelayTime:create( 0.7),moveTo,callFunc_2,cc.DelayTime:create(0.4),callFunc ,turnDiscSprAction,cc.DelayTime:create(1),callFunc_5,nil) )
end


-- 播放获取金币的特效
function  EffectManager.playNewCoinEffect( params )
      local function addscore( node )
          local tagIndex = node:getTag()
          FishGameDataController:getInstance():clearCoinEffectIsFinished( params.chair , tagIndex)
          local nativeCoin = g_GameController:getNativeCoinByChair(  params.chair )
          print("nativeCoin=",nativeCoin)
          nativeCoin = nativeCoin + params.addCoin
          g_GameController:setNativeCoinByChair( params.chair , nativeCoin )

          if FishGameDataController:getInstance():coinEffectIsFinished( params.chair ) then
              local isEqual , coin = g_GameController:checkNativeWithServerCoin( params.chair )
              if not isEqual then
                 nativeCoin = coin
              end
          end
          params.prarentNode.fishMainLayer:updatePlayerGoldView( params.viewChair  ,  params.coin )
      end
      
      if params.chair == FishGlobal.myChair then
         -- 播放获取金币声音
         FishSoundManager:getInstance():playEffect( FishSoundManager.CoinFly  )
      end

      local widthNumLimit = 5
      if params.count < 10 then
         widthNumLimit = 3
      end

      for i = 1,params.count do
          local row = math.floor( ( i - 1 )/ widthNumLimit )
          local fromPos = cc.p( params.x , params.y )
          local addWith = 0
          if  math.fmod( row , 2 ) == 0 then
              addWith = 38
          end
          fromPos.x = fromPos.x + (i- row * widthNumLimit -1) * 45  + addWith
          fromPos.y = fromPos.y + row * 40   - 100
          
          local coinEffect = EffectManager.getEffect( params )
          coinEffect:setPosition( fromPos )
          params.prarentNode:addChild( coinEffect , FishGlobal.LogicLevel )
          local value = 100
          local controlPoint_1 = cc.p(fromPos.x ,fromPos.y + value)
          local jumpTo =  cc.JumpTo:create(0.4, controlPoint_1 , value , 1)

          local toPos = cc.p( params.toX , params.toY )
          local distance = cc.pGetDistance( controlPoint_1, toPos ) + i*10
          local  time =  distance / 800
         -- local moveToAction = cc.MoveTo:create( time , toPos )
          
          local midPoint = cc.p( (controlPoint_1.x + toPos.x)/2 , (controlPoint_1.y +  toPos.y)/2)
          
          local controlPoint_2X = 0
          if toPos.x >= controlPoint_1.x then
             controlPoint_2X = controlPoint_1.x + 100
          else
             controlPoint_2X = controlPoint_1.x - 100
          end
          local controlPoint_2Y = 0
          if toPos.y >=  controlPoint_1.y then
             controlPoint_2Y = controlPoint_1.y - 100
          else
             controlPoint_2Y = controlPoint_1.y + 100
          end
          local ccBezierConfig = { controlPoint_1, cc.p(controlPoint_2X, controlPoint_2Y  ) , toPos }
          local bezierTo = cc.BezierTo:create(time, ccBezierConfig )

          local callfunc = cc.CallFunc:create(function() 
                coinEffect:removeFromParent() 
                end) 

          if  i == 1 and params.coin > 0 then
              local callfun_addscore = cc.CallFunc:create( addscore )
              FishGameDataController:getInstance():setCoinEffectIsFinished( params.chair )
              local coinIndex =  FishGameDataController:getInstance():getCoinEffectIndex( params.chair )
              coinEffect:setTag( coinIndex )
              coinEffect:runAction(cc.Sequence:create(jumpTo, bezierTo ,callfun_addscore, callfunc ,nil))
          else
              coinEffect:runAction(cc.Sequence:create(jumpTo, bezierTo , callfunc, nil ))
          end
      end
      if params.m_nFishScore ~= nil and params.m_nFishScore >= 30 then
         local spineAnim = sp.SkeletonAnimation:createWithJsonFile("app/game/Fishing/res/buyu_p_plist/boss_gold.json", "app/game/Fishing/res/buyu_p_plist/boss_gold.atlas")
         spineAnim:setPosition(cc.p(params.x, params.y))
         spineAnim:setAnimation( 0, "animation", false)
         params.prarentNode.fishMainLayer:addChild(spineAnim, 3, 11000)
         params.prarentNode.fishMainLayer:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), cc.CallFunc:create(function()
               spineAnim:removeFromParent()
         end)))   
      end
--[[
      local spineAnim = sp.SkeletonAnimation:createWithJsonFile("app/game/Fishing/res/buyu_p_plist/boss_gold.json", "app/game/Fishing/res/buyu_p_plist/boss_gold.atlas")
        spineAnim:setPosition(cc.p(params.x, params.y))
        spineAnim:setAnimation( 0, "animation", false)
        params.prarentNode.fishMainLayer:addChild(spineAnim, 3, 11000)
        params.prarentNode.fishMainLayer:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), cc.CallFunc:create(function()
            spineAnim:removeFromParent()
        end)))
        ]]
end

-- 播放获取金币的特效
function  EffectManager.playCoinEffect( params )
       --cc.SpriteFrameCache:getInstance():addSpriteFrames("buyu_p_plist/p_buyu_commom_new_1.plist","buyu_p_plist/p_buyu_commom_new_1.png")
      local function addscore( node )
          local tagIndex = node:getTag()
          FishGameDataController:getInstance():clearCoinEffectIsFinished( params.chair , tagIndex)
          local nativeCoin = g_GameController:getNativeCoinByChair(  params.chair )
          print("nativeCoin=",nativeCoin)
          nativeCoin = nativeCoin + params.addCoin
          g_GameController:setNativeCoinByChair( params.chair , nativeCoin )

          if FishGameDataController:getInstance():coinEffectIsFinished( params.chair ) then
              local isEqual , coin = g_GameController:checkNativeWithServerCoin( params.chair )
              if not isEqual then
                 nativeCoin = coin
              end
          end
          params.prarentNode.fishMainLayer:updatePlayerGoldView( params.viewChair  ,  params.coin )
      end
      -- local callfunc = cc.CallFunc:create(function() 
      --             FishSoundManager:getInstance():playEffect( FishSoundManager.CoinFly  )
      --           end) 
      -- local sequence =  cc.Sequence:create( callfunc ,cc.DelayTime:create( 1.2 ) ,callfunc,cc.DelayTime:create( 1.2 ), callfunc)
      -- local itemSpr = display.newSprite()
      -- itemSpr:runAction( sequence )
      -- params.prarentNode:addChild( itemSpr )
      FishSoundManager:getInstance():playEffect( FishSoundManager.CoinFly  )
      math.randomseed(tostring( os.time()  ):reverse():sub(1, 7)) -- 设置时间种子
      --local coinFlyTabl = { "fish_SilverCoin_1.mp3","fish_SilverCoin_2.mp3","fish_SilverCoin_3.mp3" ,"fish_SilverCoin_4.mp3" ,"fish_SilverCoin_5.mp3" }
      --local coinFlyIndex = 0
      for i=1,params.count do
         --coinFlyIndex = coinFlyIndex + 1
         -- if coinFlyIndex == 5 then
         --    coinFlyIndex = 1
         -- end
         local randomX =  math.random( 0,5)    
         local randomX = randomX * 20
         --print("randomX",randomX)
         local fromPos = cc.p( params.x + randomX , params.y )
         local coinEffect = EffectManager.getEffect( params )
         coinEffect:setPosition( fromPos )
         coinEffect:setVisible(false)
         coinEffect:setScale( 1.5)
         params.prarentNode:addChild( coinEffect , FishGlobal.LogicLevel )
      
         local delayTimeValue = (i-1) * 0.1
         if delayTimeValue > 2 then
            delayTimeValue = 2
         end
         
         local delayTime =  cc.DelayTime:create( delayTimeValue )
         local randomY =  math.random( 60,100 ) 
         --print(" randomY", randomY )
         local jumpPos = cc.p( fromPos.x , fromPos.y + randomY )
         local jumpTo =  cc.JumpTo:create(0.2, jumpPos , randomY , 1)
         local delayTime_1 =  cc.DelayTime:create( 1.2 )
         local toPos = cc.p( params.toX , params.toY )
         local distance = cc.pGetDistance( jumpPos, toPos ) 
         local  time =  distance / 450
         local moveToAction = cc.MoveTo:create( time , toPos )

         local callfunc = cc.CallFunc:create(function() 
                coinEffect:removeFromParent() 
                end) 
        
         local callfunc_2 = cc.CallFunc:create(function() 
                   --FishSoundManager:getInstance():playEffect( FishSoundManager.CoinFly  )
                   coinEffect:setVisible(true)
                end) 
         -- local callfunc_3 = cc.CallFunc:create(function() 
         --          FishSoundManager:getInstance():playEffect( FishSoundManager.CoinFly  )
         --        end) 
         
         if  i == 1 and params.coin > 0 then
              local callfun_addscore = cc.CallFunc:create( addscore )
              FishGameDataController:getInstance():setCoinEffectIsFinished( params.chair )
              local coinIndex =  FishGameDataController:getInstance():getCoinEffectIndex( params.chair )
              coinEffect:setTag( coinIndex )
              coinEffect:runAction( cc.Sequence:create(delayTime ,callfunc_2,jumpTo,delayTime_1 ,moveToAction,callfun_addscore, callfunc ,nil) )
          else
              coinEffect:runAction( cc.Sequence:create(delayTime ,callfunc_2,jumpTo, delayTime_1,moveToAction, callfunc ,nil) )
          end
      end

      if params.m_nFishScore ~= nil and params.m_nFishScore >= 30 then
         local spineAnim = sp.SkeletonAnimation:createWithJsonFile("app/game/Fishing/res/buyu_p_plist/boss_gold.json", "app/game/Fishing/res/buyu_p_plist/boss_gold.atlas")
         spineAnim:setPosition(cc.p(params.x, params.y))
         spineAnim:setAnimation( 0, "animation", false)
         params.prarentNode.fishMainLayer:addChild(spineAnim, 3, 11000)
         params.prarentNode.fishMainLayer:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), cc.CallFunc:create(function()
               spineAnim:removeFromParent()
         end)))   
      end
--[[
      local spineAnim = sp.SkeletonAnimation:createWithJsonFile("app/game/Fishing/res/buyu_p_plist/boss_gold.json", "app/game/Fishing/res/buyu_p_plist/boss_gold.atlas")
        spineAnim:setPosition(cc.p(params.x, params.y))
        spineAnim:setAnimation( 0, "animation", false)
        params.prarentNode.fishMainLayer:addChild(spineAnim, 3, 11000)
        params.prarentNode.fishMainLayer:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), cc.CallFunc:create(function()
            spineAnim:removeFromParent()
        end)))
        ]]
end

-- 金币四处飞
function EffectManager.coineExistEverywhere( params )
   cc.SpriteFrameCache:getInstance():addSpriteFrames("buyu_p_plist/p_buyu_commom_new_1.plist","buyu_p_plist/p_buyu_commom_new_1.png")
   local rotationTab = { 
                        { 0 , 30 , 60 , 100, 90 ,130 ,150, 190, 210 ,230 ,260 ,290 ,323 ,360 },
                        { 10 , 20 , 55 , 80, 90 ,110 ,140, 180, 220 ,250 ,270 ,290 ,300 ,355 },
                        { 5 , 10 , 35 , 70, 85 ,115 ,130, 160, 200 ,245 ,270 ,280 ,310 ,345 }
                     }
   local moveToTime = { 0.8, 1.0 ,1.1 , 1.2,1.5 }
   local radius = 500
   local pos = cc.p(params.x, params.y )
   local _params = { name = "coinSamll", x    =  0,  y    =  0,  }
   math.randomseed(tostring( os.time()  ):reverse():sub(1, 7)) -- 设置时间种子
   for i=1,3 do
      local delayTime = ( i - 1) * 0.3
      for k,v in pairs( rotationTab[i] ) do
      local coinEffect = EffectManager.getEffect( _params )
      coinEffect:setScale( 1.5 )
      coinEffect:setPosition( pos )
      coinEffect:setVisible(false)
      params.prarentNode:addChild( coinEffect , FishGlobal.LogicLevel )
      local delay = cc.DelayTime:create( delayTime )
      -- local rotation = math.random(10,20 )
      local rad = math.rad( v )
      local x = math.cos(rad) * radius
      local y = math.sin(rad) * radius
      local moveToPos = cc.p( pos.x + x , pos.y + y )
      local moveToTimeIndex =  math.random( 1,5 ) 
      local moveTo = cc.MoveTo:create( moveToTime[ moveToTimeIndex  ] , moveToPos)
      local callfunc = cc.CallFunc:create(function() 
               coinEffect:setVisible( true ) 
               end) 
      local callfunc_1 = cc.CallFunc:create(function() 
               coinEffect:removeFromParent() 
               end) 
      coinEffect:runAction( cc.Sequence:create( delay ,callfunc ,moveTo , callfunc_1, nil ))
   end
   end
   
   if params.m_nFishScore ~= nil and params.m_nFishScore >= 30 then
      local spineAnim = sp.SkeletonAnimation:createWithJsonFile("app/game/Fishing/res/buyu_p_plist/boss_gold.json", "app/game/Fishing/res/buyu_p_plist/boss_gold.atlas")
      spineAnim:setPosition(cc.p(params.x, params.y))
      spineAnim:setAnimation( 0, "animation", false)
      params.prarentNode.fishMainLayer:addChild(spineAnim, 3, 11000)
      params.prarentNode.fishMainLayer:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), cc.CallFunc:create(function()
            spineAnim:removeFromParent()
      end)))   
   end
--[[
   local spineAnim = sp.SkeletonAnimation:createWithJsonFile("app/game/Fishing/res/buyu_p_plist/boss_gold.json", "app/game/Fishing/res/buyu_p_plist/boss_gold.atlas")
     spineAnim:setPosition(cc.p(params.x, params.y))
     spineAnim:setAnimation( 0, "animation", false)
     params.prarentNode.fishMainLayer:addChild(spineAnim, 3, 11000)
     params.prarentNode.fishMainLayer:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), cc.CallFunc:create(function()
         spineAnim:removeFromParent()
     end)))
     ]]
end


-- 播放获取金币的特效
function  EffectManager.playClassicCoinEffect( params )
      local function addscore( node )
          local tagIndex = node:getTag()
          FishGameDataController:getInstance():clearCoinEffectIsFinished( params.chair , tagIndex)
          local nativeCoin = Classicg_GameController:getNativeCoinByChair(  params.chair )
          print("nativeCoin=",nativeCoin)
          nativeCoin = nativeCoin + params.addCoin
          Classicg_GameController:setNativeCoinByChair( params.chair , nativeCoin )

          if FishGameDataController:getInstance():coinEffectIsFinished( params.chair ) then
              local isEqual , coin = Classicg_GameController:checkNativeWithServerCoin( params.chair )
              if not isEqual then
                 nativeCoin = coin
              end
          end
          params.prarentNode.fishMainLayer:updatePlayerGoldView( params.viewChair  ,  params.coin )
      end
      
      if params.chair == FishGlobal.myChair then
         -- 播放获取金币声音
         FishSoundManager:getInstance():playEffect( FishSoundManager.CoinFly  )
      end

      local widthNumLimit = 5
      if params.count < 10 then
         widthNumLimit = 3
      end

      for i = 1,params.count do
          local row = math.floor( ( i - 1 )/ widthNumLimit )
          local fromPos = cc.p( params.x , params.y )
          local addWith = 0
          if  math.fmod( row , 2 ) == 0 then
              addWith = 38
          end
          fromPos.x = fromPos.x + (i- row * widthNumLimit -1) * 45  + addWith
          fromPos.y = fromPos.y + row * 40   - 100
          
          local coinEffect = EffectManager.getEffect( params )
          coinEffect:setPosition( fromPos )
          params.prarentNode:addChild( coinEffect , FishGlobal.LogicLevel )
          local value = 100
          local controlPoint_1 = cc.p(fromPos.x ,fromPos.y + value)
          local jumpTo =  cc.JumpTo:create(0.4, controlPoint_1 , value , 1)

          local toPos = cc.p( params.toX , params.toY )
          local distance = cc.pGetDistance( controlPoint_1, toPos ) + i*10
          local  time =  distance / 800
         -- local moveToAction = cc.MoveTo:create( time , toPos )
          
          local midPoint = cc.p( (controlPoint_1.x + toPos.x)/2 , (controlPoint_1.y +  toPos.y)/2)
          
          local controlPoint_2X = 0
          if toPos.x >= controlPoint_1.x then
             controlPoint_2X = controlPoint_1.x + 100
          else
             controlPoint_2X = controlPoint_1.x - 100
          end
          local controlPoint_2Y = 0
          if toPos.y >=  controlPoint_1.y then
             controlPoint_2Y = controlPoint_1.y - 100
          else
             controlPoint_2Y = controlPoint_1.y + 100
          end
          local ccBezierConfig = { controlPoint_1, cc.p(controlPoint_2X, controlPoint_2Y  ) , toPos }
          local bezierTo = cc.BezierTo:create(time, ccBezierConfig )

          local callfunc = cc.CallFunc:create(function() 
                coinEffect:removeFromParent() 
                end) 

          if  i == 1 and params.coin > 0 then
              local callfun_addscore = cc.CallFunc:create( addscore )
              FishGameDataController:getInstance():setCoinEffectIsFinished( params.chair )
              local coinIndex =  FishGameDataController:getInstance():getCoinEffectIndex( params.chair )
              coinEffect:setTag( coinIndex )
              coinEffect:runAction(cc.Sequence:create(jumpTo, bezierTo ,callfun_addscore, callfunc ,nil))
          else
              coinEffect:runAction(cc.Sequence:create(jumpTo, bezierTo , callfunc, nil ))
          end
      end

      if params.m_nFishScore ~= nil and params.m_nFishScore >= 30 then
         local spineAnim = sp.SkeletonAnimation:createWithJsonFile("app/game/Fishing/res/buyu_p_plist/boss_gold.json", "app/game/Fishing/res/buyu_p_plist/boss_gold.atlas")
         spineAnim:setPosition(cc.p(params.x, params.y))
         spineAnim:setAnimation( 0, "animation", false)
         params.prarentNode.fishMainLayer:addChild(spineAnim, 3, 11000)
         params.prarentNode.fishMainLayer:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), cc.CallFunc:create(function()
               spineAnim:removeFromParent()
         end)))   
      end
--[[
      local spineAnim = sp.SkeletonAnimation:createWithJsonFile("app/game/Fishing/res/buyu_p_plist/boss_gold.json", "app/game/Fishing/res/buyu_p_plist/boss_gold.atlas")
        spineAnim:setPosition(cc.p(params.x, params.y))
        spineAnim:setAnimation( 0, "animation", false)
        params.prarentNode.fishMainLayer:addChild(spineAnim, 3, 11000)
        params.prarentNode.fishMainLayer:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), cc.CallFunc:create(function()
            spineAnim:removeFromParent()
        end)))
        ]]
end

-- 播放获取金币的特效
function  EffectManager.playCoinEffectEx( params )
      if params.chair == FishGlobal.myChair then
         -- 播放获取金币声音
         if FishingUtil:getInstance():randomOK(10,100  ) then
            FishSoundManager:getInstance():playEffect( FishSoundManager.CoinFly  )
         end
      end

      local widthNumLimit = 5
      if params.count < 10 then
         widthNumLimit = 3
      end

      for i = 1,params.count do
          local row = math.floor( ( i - 1 )/ widthNumLimit )
          local fromPos = cc.p( params.x , params.y )
          local addWith = 0
          if  math.fmod( row , 2 ) == 0 then
              addWith = 38
          end
          fromPos.x = fromPos.x + (i- row * widthNumLimit -1) * 45  + addWith
          fromPos.y = fromPos.y + row * 40   - 100
          
          local coinEffect = EffectManager.getEffect( params )
          coinEffect:setPosition( fromPos )
          params.prarentNode:addChild( coinEffect , FishGlobal.LogicLevel )
          local value = 100
          local controlPoint_1 = cc.p(fromPos.x ,fromPos.y + value)
          local jumpTo =  cc.JumpTo:create(0.4, controlPoint_1 , value , 1)
          
          local fadeOut = cc.FadeOut:create(0.8)

          local callfunc = cc.CallFunc:create(function() 
                coinEffect:removeFromParent() 
                end) 
          coinEffect:runAction(cc.Sequence:create(jumpTo, cc.DelayTime:create( 0.5 ),fadeOut , callfunc, nil ))
      end

      if params.m_nFishScore ~= nil and params.m_nFishScore >= 30 then
         local spineAnim = sp.SkeletonAnimation:createWithJsonFile("app/game/Fishing/res/buyu_p_plist/boss_gold.json", "app/game/Fishing/res/buyu_p_plist/boss_gold.atlas")
         spineAnim:setPosition(cc.p(params.x, params.y))
         spineAnim:setAnimation( 0, "animation", false)
         params.prarentNode.fishMainLayer:addChild(spineAnim, 3, 11000)
         params.prarentNode.fishMainLayer:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), cc.CallFunc:create(function()
               spineAnim:removeFromParent()
         end)))   
      end
--[[
      local spineAnim = sp.SkeletonAnimation:createWithJsonFile("app/game/Fishing/res/buyu_p_plist/boss_gold.json", "app/game/Fishing/res/buyu_p_plist/boss_gold.atlas")
        spineAnim:setPosition(cc.p(params.x, params.y))
        spineAnim:setAnimation( 0, "animation", false)
        params.prarentNode.fishMainLayer:addChild(spineAnim, 3, 11000)
        params.prarentNode.fishMainLayer:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), cc.CallFunc:create(function()
            spineAnim:removeFromParent()
        end)))
        ]]
end

 

-- 播放获取金币的特效
function  EffectManager.playCoinEffectYY( params )
      local function addscore( node )
          local tagIndex = node:getTag()
          FishGameDataController:getInstance():clearCoinEffectIsFinished( params.chair , tagIndex)
          local nativeCoin = FishFriendsGameController:getInstance():getNativeScoreByChair(  params.chair )
          nativeCoin = nativeCoin + params.addCoin
          FishFriendsGameController:getInstance():setNativeScoreByChair( params.chair , nativeCoin )

          if FishGameDataController:getInstance():coinEffectIsFinished( params.chair ) then
              local isEqual , coin = FishFriendsGameController:getInstance():checkNativeWithServerScore( params.chair )
              if not isEqual then
                 nativeCoin = coin
              end
          end
          params.prarentNode.fishFriendsMainLayer:updatePlayerScoreView( params.viewChair  ,  params.coin )
      end
      
      if params.chair == FishGlobal.myChair then
         -- 播放获取金币声音
         FishSoundManager:getInstance():playEffect( FishSoundManager.CoinFly  )
      end

      local widthNumLimit = 5
      if params.count < 10 then
         widthNumLimit = 3
      end

      for i = 1,params.count do
          local row = math.floor( ( i - 1 )/ widthNumLimit )
          local fromPos = cc.p( params.x , params.y )
          local addWith = 0
          if  math.fmod( row , 2 ) == 0 then
              addWith = 38
          end
          fromPos.x = fromPos.x + (i- row * widthNumLimit -1) * 45  + addWith
          fromPos.y = fromPos.y + row * 40   - 100
          
          local coinEffect = EffectManager.getEffect( params )
          coinEffect:setPosition( fromPos )
          params.prarentNode:addChild( coinEffect , FishGlobal.LogicLevel )
          local value = 100
          local controlPoint_1 = cc.p(fromPos.x ,fromPos.y + value)
          local jumpTo =  cc.JumpTo:create(0.4, controlPoint_1 , value , 1)

          local toPos = cc.p( params.toX , params.toY )
          local distance = cc.pGetDistance( controlPoint_1, toPos ) + i*10
          local  time =  distance / 800
         -- local moveToAction = cc.MoveTo:create( time , toPos )
          
          local midPoint = cc.p( (controlPoint_1.x + toPos.x)/2 , (controlPoint_1.y +  toPos.y)/2)
          
          local controlPoint_2X = 0
          if toPos.x >= controlPoint_1.x then
             controlPoint_2X = controlPoint_1.x + 100
          else
             controlPoint_2X = controlPoint_1.x - 100
          end
          local controlPoint_2Y = 0
          if toPos.y >=  controlPoint_1.y then
             controlPoint_2Y = controlPoint_1.y - 100
          else
             controlPoint_2Y = controlPoint_1.y + 100
          end
          local ccBezierConfig = { controlPoint_1, cc.p(controlPoint_2X, controlPoint_2Y  ) , toPos }
          local bezierTo = cc.BezierTo:create(time, ccBezierConfig )

          local callfunc = cc.CallFunc:create(function() 
                coinEffect:removeFromParent() 
                end) 

          if  i == 1 and params.coin > 0 then
              local callfun_addscore = cc.CallFunc:create( addscore )
              FishGameDataController:getInstance():setCoinEffectIsFinished( params.chair )
              local coinIndex =  FishGameDataController:getInstance():getCoinEffectIndex( params.chair )
              coinEffect:setTag( coinIndex )
              coinEffect:runAction(cc.Sequence:create(jumpTo, bezierTo ,callfun_addscore, callfunc ,nil))
          else
              coinEffect:runAction(cc.Sequence:create(jumpTo, bezierTo , callfunc, nil ))
          end
      end

      if params.m_nFishScore ~= nil and params.m_nFishScore >= 30 then
         local spineAnim = sp.SkeletonAnimation:createWithJsonFile("app/game/Fishing/res/buyu_p_plist/boss_gold.json", "app/game/Fishing/res/buyu_p_plist/boss_gold.atlas")
         spineAnim:setPosition(cc.p(params.x, params.y))
         spineAnim:setAnimation( 0, "animation", false)
         params.prarentNode.fishMainLayer:addChild(spineAnim, 3, 11000)
         params.prarentNode.fishMainLayer:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), cc.CallFunc:create(function()
               spineAnim:removeFromParent()
         end)))   
      end
--[[
      local spineAnim = sp.SkeletonAnimation:createWithJsonFile("app/game/Fishing/res/buyu_p_plist/boss_gold.json", "app/game/Fishing/res/buyu_p_plist/boss_gold.atlas")
        spineAnim:setPosition(cc.p(params.x, params.y))
        spineAnim:setAnimation( 0, "animation", false)
        params.prarentNode.fishMainLayer:addChild(spineAnim, 3, 11000)
        params.prarentNode.fishMainLayer:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), cc.CallFunc:create(function()
            spineAnim:removeFromParent()
        end)))
        ]]
end

-- 道具掉落
function EffectManager.itemDropOut( params )
    -- params.m_vecItem params.pos  params.toPos
--    cc.SpriteFrameCache:getInstance():addSpriteFrames("buyu_p_plist/p_buyu_room_common_1.plist","buyu_p_plist/p_buyu_room_common_1.png")
    local toPos = params.toPos
    local i = 0
    if params.chair == FishGlobal.myChair then
        -- 播放获取掉落声音
        FishSoundManager:getInstance():playEffect( FishSoundManager.ItemDrop  )
    end

    for k,v in pairs( params.m_vecItem) do
          local itemData = FishGameDataController:getInstance():getItemByIndex( v.m_nItemID )
          if itemData then
             local itemPos = params.pos
             itemPos.x = itemPos.x + i * 40 
             itemPos.y = itemPos.y + math.random(2,3) * 5
             local itemNode = cc.Node:create()
             itemNode:setPosition( itemPos )
             params.prarentNode:addChild( itemNode , FishGlobal.LogicLevel + 10 )
            
             
             -- 物品图标
             local itemIconSpr = display.newSprite( "#"..itemData.itemIcon )
             itemIconSpr:setAnchorPoint( cc.p(0.5,0.5) )
             itemNode:addChild( itemIconSpr )
             itemIconSpr:setPosition( cc.p(0,0) )
             if itemData.nType ~= 7 then 
               -- 物品个数
               local itemCountLabel = display.newTTFLabel({
                text = tostring( v.m_nCount),
                font = "ttf/jcy.TTF",
                size = 45,
                color = cc.c3b(237, 231, 18),
              })
               local contentSize =  itemIconSpr:getContentSize()
               itemCountLabel:setAnchorPoint( cc.p( 1,0 ) )
               itemCountLabel:setPosition( cc.p( contentSize.width/2-15,-contentSize.height/2-10) )
               itemNode:addChild( itemCountLabel,10 )
            end
             --itemCountLabel:enableOutline(cc.c4b(255,255,255,255), 2)
            itemNode:setScale( 0.05 )
            local scaleTo_2  = cc.ScaleTo:create(0.4,0.5)
            local distance = cc.pGetDistance( itemPos, toPos ) + i*10
            local time = distance/800
            local controlPoint_2X = 0
            if toPos.x >= itemPos.x then
               controlPoint_2X = itemPos.x + 100
            else
               controlPoint_2X = itemPos.x - 100
            end
            local controlPoint_2Y = 0
            if toPos.y >=  itemPos.y then
               controlPoint_2Y = itemPos.y - 100
            else
               controlPoint_2Y = itemPos.y + 100
            end
            local ccBezierConfig = { itemPos , cc.p(controlPoint_2X, controlPoint_2Y  ) , toPos }
            local bezierTo = cc.BezierTo:create(time , ccBezierConfig )
            local callfunc = nil
            if itemData.nType == 6 or itemData.nType == 8 then -- 元宝类型/元宝碎片
               FishGameDataController:getInstance():setGoldEffectIsFinished( params.chair )
               local goldIndex =  FishGameDataController:getInstance():getGoldEffectIndex( params.chair )
               itemNode:setTag( goldIndex )
               itemNode:setVisible( params.isShow )
               local addGoldCount = 0
               if itemData.nType == 6 then
                  addGoldCount = v.m_nCount
               elseif itemData.nType == 8 then
                  addGoldCount = v.m_nCount/10
               end
               -- local function showItem( node )
               --    node:setVisible(  params.isShow )
               -- end  
               -- local  callfunc_1 = cc.CallFunc:create( showItem ) 

               local function addGold( node )
                    local goldIndex = node:getTag()
                    FishGameDataController:getInstance():clearGoldEffectIsFinished( params.chair , goldIndex)
                    local nativeGold = g_GameController:getNativeGoldByChair(  params.chair )
                    nativeGold= nativeGold + addGoldCount
                    g_GameController:setNativeGoldByChair( params.chair , nativeGold )
                   
                    if FishGameDataController:getInstance():goldEffectIsFinished( params.chair ) then
                        local isEqual , gold = g_GameController:checkNativeWithServerGold( params.chair )
                        if not isEqual then
                           nativeGold = gold
                        end
                    end
                    --TOAST("掉落元宝")
                    --元宝界面刷新
                    params.prarentNode.fishMainLayer:updatePlayerYbView( params.viewChair  ,  nativeGold )
                    itemNode:removeFromParent() 
               end
               callfunc = cc.CallFunc:create( addGold ) 

               itemNode:runAction(cc.Sequence:create( scaleTo_2,cc.DelayTime:create(0.4), bezierTo, callfunc ,nil))
            else
               callfunc = cc.CallFunc:create(function() 
                          itemNode:removeFromParent() 
                              if itemData.nType == 7  then
                                  EffectManager.redPacketsOperation(  params )
                              end
                          end) 
               itemNode:runAction(cc.Sequence:create(scaleTo_2,cc.DelayTime:create(0.4), bezierTo, callfunc ,nil))
            end  
        i = i + 1
      end
    end

    if params.m_nFishScore ~= nil and params.m_nFishScore >= 30 then
      local spineAnim = sp.SkeletonAnimation:createWithJsonFile("app/game/Fishing/res/buyu_p_plist/boss_gold.json", "app/game/Fishing/res/buyu_p_plist/boss_gold.atlas")
      spineAnim:setPosition(cc.p(params.x, params.y))
      spineAnim:setAnimation( 0, "animation", false)
      params.prarentNode.fishMainLayer:addChild(spineAnim, 3, 11000)
      params.prarentNode.fishMainLayer:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), cc.CallFunc:create(function()
            spineAnim:removeFromParent()
      end)))   
   end
--[[
   local spineAnim = sp.SkeletonAnimation:createWithJsonFile("app/game/Fishing/res/buyu_p_plist/boss_gold.json", "app/game/Fishing/res/buyu_p_plist/boss_gold.atlas")
     spineAnim:setPosition(cc.p(params.x, params.y))
     spineAnim:setAnimation( 0, "animation", false)
     params.prarentNode.fishMainLayer:addChild(spineAnim, 3, 11000)
     params.prarentNode.fishMainLayer:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), cc.CallFunc:create(function()
         spineAnim:removeFromParent()
     end)))
     ]]
end


-- 道具掉落
function EffectManager.classicItemDropOut( params )
    -- params.m_vecItem params.pos  params.toPos
    cc.SpriteFrameCache:getInstance():addSpriteFrames("buyu_p_plist/p_buyu_room_common_1.plist","buyu_p_plist/p_buyu_room_common_1.png")
    local toPos = params.toPos
    local i = 0
    if params.chair == FishGlobal.myChair then
        -- 播放获取掉落声音
        FishSoundManager:getInstance():playEffect( FishSoundManager.ItemDrop  )
    end

    for k,v in pairs( params.m_vecItem) do
          local itemData = FishGameDataController:getInstance():getClassicItemByIndex( v.m_nItemID )
          if itemData then
             local itemPos = params.pos
             itemPos.x = itemPos.x + i * 40 
             itemPos.y = itemPos.y + math.random(2,3) * 5
             local itemNode = cc.Node:create()
             itemNode:setPosition( itemPos )
             params.prarentNode:addChild( itemNode , FishGlobal.LogicLevel + 10 )
            
             
             -- 物品图标
             local itemIconSpr = display.newSprite( "#"..itemData.itemIcon )
             itemIconSpr:setAnchorPoint( cc.p(0.5,0.5) )
             itemNode:addChild( itemIconSpr )
             itemIconSpr:setPosition( cc.p(0,0) )
             if itemData.nType ~= 7 then 
               -- 物品个数
               local itemCountLabel = display.newTTFLabel({
                text = tostring( v.m_nCount),
                font = "ttf/jcy.TTF",
                size = 45,
                color = cc.c3b(237, 231, 18),
              })
               local contentSize =  itemIconSpr:getContentSize()
               itemCountLabel:setAnchorPoint( cc.p( 1,0 ) )
               itemCountLabel:setPosition( cc.p( contentSize.width/2-15,-contentSize.height/2-10) )
               itemNode:addChild( itemCountLabel,10 )
            end
             --itemCountLabel:enableOutline(cc.c4b(255,255,255,255), 2)
            itemNode:setScale( 0.05 )
            local scaleTo_2  = cc.ScaleTo:create(0.4,0.5)
            local distance = cc.pGetDistance( itemPos, toPos ) + i*10
            local time = distance/800
            local controlPoint_2X = 0
            if toPos.x >= itemPos.x then
               controlPoint_2X = itemPos.x + 100
            else
               controlPoint_2X = itemPos.x - 100
            end
            local controlPoint_2Y = 0
            if toPos.y >=  itemPos.y then
               controlPoint_2Y = itemPos.y - 100
            else
               controlPoint_2Y = itemPos.y + 100
            end
            local ccBezierConfig = { itemPos , cc.p(controlPoint_2X, controlPoint_2Y  ) , toPos }
            local bezierTo = cc.BezierTo:create(time , ccBezierConfig )
            local callfunc = nil
            if itemData.nType == 6 then -- 元宝类型
               FishGameDataController:getInstance():setGoldEffectIsFinished( params.chair )
               local goldIndex =  FishGameDataController:getInstance():getGoldEffectIndex( params.chair )
               itemNode:setTag( goldIndex )
               itemNode:setVisible( params.isShow )
               -- local function showItem( node )
               --    node:setVisible(  params.isShow )
               -- end  
               -- local  callfunc_1 = cc.CallFunc:create( showItem ) 

               local function addGold( node )
                    local goldIndex = node:getTag()
                    FishGameDataController:getInstance():clearGoldEffectIsFinished( params.chair , goldIndex)
                    local nativeGold = g_GameController:getNativeGoldByChair(  params.chair )
                    nativeGold= nativeGold + params.addGold
                    g_GameController:setNativeGoldByChair( params.chair , nativeGold )
                   
                    if FishGameDataController:getInstance():goldEffectIsFinished( params.chair ) then
                        local isEqual , gold = g_GameController:checkNativeWithServerGold( params.chair )
                        if not isEqual then
                           nativeGold = gold
                        end
                    end
                    --TOAST("掉落元宝")
                    --元宝界面刷新
                    params.prarentNode.fishMainLayer:updatePlayerYbView( params.viewChair  ,  nativeGold )
                    itemNode:removeFromParent() 
               end
               callfunc = cc.CallFunc:create( addGold ) 

               itemNode:runAction(cc.Sequence:create( scaleTo_2,cc.DelayTime:create(0.4), bezierTo, callfunc ,nil))
            else
               callfunc = cc.CallFunc:create(function() 
                          itemNode:removeFromParent() 
                              if itemData.nType == 7  then
                                  EffectManager.redPacketsOperation(  params )
                              end
                          end) 
               itemNode:runAction(cc.Sequence:create(scaleTo_2,cc.DelayTime:create(0.4), bezierTo, callfunc ,nil))
            end  
        i = i + 1
      end
    end
end


-- 狂暴特效
function EffectManager.playCritAni( params )
  -- params.prarentNode  params.pos  params.viewChair 
   if EffectManager.critAnis[ params.viewChair ] then
      EffectManager.critAnis[ params.viewChair ]:setVisible( true )
   else
       local critSpr = display.newSprite()
       EffectManager.critAnis[ params.viewChair ] = critSpr
       critSpr:setAnchorPoint( cc.p( 0.5,0.5 ) )
       critSpr:setPosition( cc.p(params.pos.x , params.pos.y + 6))
       if params.viewChair == 4 or params.viewChair == 5 or params.viewChair == 6 then
          critSpr:setRotation( 180 )
          critSpr:setPosition( cc.p(params.pos.x , params.pos.y  ))
       end
       params.prarentNode:addChild( critSpr , FishGlobal.LogicLevel - 10  )
       EffectManager.playAnimation( { node = critSpr,  name = "critAni" } )
   end
   
end

function EffectManager.hideCritAni( params )
    if EffectManager.critAnis[ params.viewChair ] then
       EffectManager.critAnis[ params.viewChair ]:setVisible( false )
    end
end


function EffectManager.hideAllCritAnis()
    for k,v in pairs( EffectManager.critAnis ) do
        v:setVisible( false )
    end
end

function EffectManager.clearAllCritAni()
    for k,v in pairs( EffectManager.critAnis ) do
        v:removeFromParent()
    end
    EffectManager.critAnis = {}
end

--冰冻效果
function EffectManager.playfrozen( params )
    if not EffectManager.isfrozen  then
       -- if not tolua.isnull(EffectManager.listener) then
       --     --print("EffectManager.unfreeze2")
       --     cc.Director:getInstance():getEventDispatcher():removeEventListener( EffectManager.listener )
       --     EffectManager.listener = nil
       --  end
        EffectManager.isfrozen = true
        local frozenSpr = cc.Sprite:create("buyu_p_texture/buyu_e_iceage_00002.png")
        if frozenSpr == nil then
            return
        end
        EffectManager.frozenSpr = frozenSpr
        frozenSpr:setScaleY( display.scaleY  )
        frozenSpr:setScaleX( display.scaleX  )
        frozenSpr:setBlendFunc(gl.ONE, gl.ONE) 
        frozenSpr:setAnchorPoint( cc.p( 0.5,0.5 ))
        frozenSpr:setPosition(cc.p( display.width/2,display.height/2 )  )
        params.prarentNode:addChild( frozenSpr , FishGlobal.LogicLevel + 10 )
        params.prarentNode.m_background:pauseBackGround()
        print("params.prarentNode.m_background:pauseBackGround")
        EffectManager.frozenType = params._type
        if params.isAnimal then
            local animationFrames = {}
            for i=1,3 do
                local iceagePng = string.format("buyu_p_texture/buyu_e_iceage_%05d.png",i-1)
                local texture =  cc.Director:getInstance():getTextureCache():addImage( iceagePng )
                local size = texture:getContentSize()
                local frame = cc.SpriteFrame:createWithTexture(texture, size)
                local animationFrame = cc.AnimationFrame:create( frame, 0.2, { } )
                table.insert(animationFrames, animationFrame)
            end
            local animation = cc.Animation:create(animationFrames,0.2)
            local animate = cc.Animate:create( animation )
            frozenSpr:runAction( animate )
            -- local listener  = cc.EventListenerCustom:create(cc.ANIMATION_FRAME_DISPLAYED_NOTIFICATION,
            --            function ( event )
            --               if not tolua.isnull( frozenSpr ) then
            --                 local name =  event:getEventName()
            --                 frozenSpr:setBlendFunc(gl.ONE, gl.ONE) 
            --               end
            --            end)
            -- EffectManager.listener = listener
            -- cc.Director:getInstance():getEventDispatcher():addEventListenerWithFixedPriority(listener, -1)
            table.insert(EffectManager.needBlendSprs,  frozenSpr)
        end
    end
end

function EffectManager.unfreeze( _frozenType , prarentNode )
    if EffectManager.frozenType == _frozenType then
        print("EffectManager.unfreeze")
        EffectManager.unfreezeView(  prarentNode )
        -- if not tolua.isnull(EffectManager.listener) then
        --    cc.Director:getInstance():getEventDispatcher():removeEventListener( EffectManager.listener )
        --    EffectManager.listener = nil
        -- end
    end
end

function EffectManager.unfreezeView( prarentNode )
   if EffectManager.isfrozen then
       EffectManager.isfrozen = false
       print("prarentNode.m_background:resumeBackGround()")
       if prarentNode then
          prarentNode.m_background:resumeBackGround()
       end
       if not tolua.isnull( EffectManager.frozenSpr ) then
            EffectManager.frozenSpr:removeFromParent()
            EffectManager.frozenSpr= nil
       end
    end
end

function EffectManager.clearFreeze()
    EffectManager.unfreezeView()
    -- if not tolua.isnull(EffectManager.listener) then
    --    cc.Director:getInstance():getEventDispatcher():removeEventListener( EffectManager.listener )
    --    EffectManager.listener = nil
    -- end
end


-- 刺豚动画效果
function EffectManager.playAgoutiAni( params )
    local agoutiSpr = cc.Sprite:create("buyu_p_texture/buyu_e_ci_all_00000.png")
    agoutiSpr:setAnchorPoint( cc.p( 0.5,0.5 ))
    agoutiSpr:setScaleY( display.scaleY  )
    agoutiSpr:setPosition(cc.p( display.width/2,display.height/2 )  )
    params.prarentNode:addChild( agoutiSpr , FishGlobal.LogicLevel + 10 )
    local animationFrames = {}
    for i=1,4 do
        local iceagePng = string.format("buyu_p_texture/buyu_e_ci_all_%05d.png",i-1)
        local texture =  cc.Director:getInstance():getTextureCache():addImage( iceagePng )
        local size = texture:getContentSize()
        local frame = cc.SpriteFrame:createWithTexture(texture, size)
        local animationFrame = cc.AnimationFrame:create( frame, 0.2, { } )
        table.insert(animationFrames, animationFrame)
    end
    local animation = cc.Animation:create(animationFrames,0.8)
    local animate = cc.Animate:create( animation )
    local callfunc = cc.CallFunc:create(function() 
              agoutiSpr:removeFromParent() 
              end) 
    agoutiSpr:runAction(cc.Sequence:create( animate , callfunc ,nil))
end


-- 显示鱼潮来袭的效果
function EffectManager.showFishTideEffect( params )

    local fishTideBgSpr = display.newSprite("buyu_p_texture/fish_bg_yc.png")
    fishTideBgSpr:setAnchorPoint( cc.p(0.5,0.5) )
    local bgContenSize = fishTideBgSpr:getContentSize()
    fishTideBgSpr:setPosition( -bgContenSize.width/2 , display.height/2 )
    params.prarentNode:addChild( fishTideBgSpr , FishGlobal.LogicLevel + 10 )
    local moveTo = cc.MoveTo:create( 0.5, cc.p( display.width/2,display.height/2) )
    local fadeOut = cc.FadeOut:create(1)
    local delayTime =  cc.DelayTime:create(0.5)
    local callfunc = cc.CallFunc:create(function() 
              fishTideBgSpr:removeFromParent() 
              end) 
    fishTideBgSpr:runAction(cc.Sequence:create( moveTo ,fadeOut,delayTime,callfunc))

    local fishTideTipsSpr = display.newSprite("#fish_img_yc.png")
    fishTideTipsSpr:setAnchorPoint( cc.p(0.5,0.5) )
    local fishTipContentSize = fishTideTipsSpr:getContentSize()
    fishTideTipsSpr:setPosition( display.width + fishTipContentSize.width/2,display.height/2 )
    params.prarentNode:addChild( fishTideTipsSpr , FishGlobal.LogicLevel + 10 )
    local moveTo_1 = cc.MoveTo:create( 0.5, cc.p( display.width/2,display.height/2) )
    local fadeOut_1 = cc.FadeOut:create(1)
    local delayTime_1 =  cc.DelayTime:create(0.5)
    local callfunc_1 = cc.CallFunc:create(function() 
              fishTideTipsSpr:removeFromParent() 
              end) 

    fishTideTipsSpr:runAction( cc.Sequence:create( moveTo_1 ,delayTime_1,fadeOut_1,callfunc_1) )
end

function EffectManager.redPacketsOperation( params )

   if next( EffectManager.redPackets[params.chair]  ) then
       table.insert( EffectManager.redPackets[params.chair] ,params )
    else
       table.insert( EffectManager.redPackets[params.chair] ,params )
       EffectManager.redPacketsDropOutNew( params )
    end
end

-- 红包掉落的动画效果
function EffectManager.redPacketsDropOut( params )
      cc.SpriteFrameCache:getInstance():addSpriteFrames("buyu_p_plist/p_buyu_friend_balance_pao.plist","buyu_p_plist/p_buyu_friend_balance_pao.png")
      local rootNode = cc.Node:create()
      rootNode:setPosition( cc.p( display.width/2,display.height/2) )
      params.prarentNode:addChild( rootNode , FishGlobal.UILevel + 10 )

      local bgSpr = display.newSprite("#fish_dt_f_bg_gongxihuode.png")
      bgSpr:setAnchorPoint( cc.p(0.5,0.5) )
       bgSpr:setBlendFunc(gl.ONE, gl.ONE ) --gl.DST_ALPHA)
      rootNode:addChild( bgSpr )
    
      local function dropOutEnd()
         if next( EffectManager.redPackets  ) then
            table.remove(EffectManager.redPackets , 1  )
         end
         rootNode:removeFromParent() 

         if next( EffectManager.redPackets  ) then
            if EffectManager.redPackets[1] then
               EffectManager.redPacketsDropOut( EffectManager.redPackets[1] )
            end
         end
      end 
      local callfunc = cc.CallFunc:create( dropOutEnd  )  
    
      --- 大星星
      local bigXingSpr = display.newSprite("#fish_dt_f_img_bigxing.png")
      bigXingSpr:setAnchorPoint( cc.p(0.5,0.5) )
      rootNode:addChild( bigXingSpr )
      bigXingSpr:setVisible(false)
      bigXingSpr:setBlendFunc(gl.ONE, gl.ONE )
      
      -- 散落的星星
      local cxpos = { cc.p( -400,50 ) , cc.p( -230,-100) ,cc.p( 200,270)  }
      local  cxSprS = {}
      for i=1,3 do
          local cxSpr = display.newSprite("#fish_dt_f_img_cx.png")
          cxSpr:setAnchorPoint( cc.p(0.5,0.5) )
          cxSpr:setPosition( cxpos[i] ) 
          cxSpr:setVisible(false)
          rootNode:addChild( cxSpr )
          local fadeTo_1 =  cc.FadeTo:create(1.2, 80)
          local scaleTo_1 =  cc.ScaleTo:create(1.2,0.9)
          local spawn_1 =  cc.Spawn:create( fadeTo_1 , scaleTo_1 ) 

          local scaleTo_2 =  cc.ScaleTo:create(1.2,1)
          local fadeTo_2 = cc.FadeIn:create(1.2 )
          local spawn_2 =  cc.Spawn:create( fadeTo_2 , scaleTo_2 ) 
          cxSpr:setBlendFunc(gl.ONE, gl.ONE )
          cxSprS[i] = cxSpr
          cxSpr:runAction( cc.RepeatForever:create( cc.Sequence:create(spawn_1,spawn_2,nil )))
      end
       

      local yxpos = { cc.p( 300,-10 ) ,cc.p( -260,230) ,cc.p( 180,-250)  }
      local  yxSprS = {}
      for i=1,3 do
          local yxSpr = display.newSprite("#fish_dt_f_img_yx.png")
          yxSpr:setAnchorPoint( cc.p(0.5,0.5) )
          yxSpr:setPosition( yxpos[i] )
          yxSpr:setVisible(false)
          rootNode:addChild( yxSpr )
          local fadeTo_1 =  cc.FadeTo:create(1.2, 80)
          local scaleTo_1 =  cc.ScaleTo:create(1.2,0.9)
          local spawn_1 =  cc.Spawn:create( fadeTo_1 , scaleTo_1 ) 

          local scaleTo_2 =  cc.ScaleTo:create(1.2,1)
          local fadeTo_2 = cc.FadeIn:create(1.2 )
          local spawn_2 =  cc.Spawn:create( fadeTo_2 , scaleTo_2 ) 
          yxSpr:setBlendFunc(gl.ONE, gl.ONE )
          yxSprS[i] = yxSpr
          yxSpr:runAction( cc.RepeatForever:create( cc.Sequence:create(spawn_1,spawn_2,nil )))
      end
      

      -- 恭喜获得999元红包提示
      local rootTipNode = cc.Node:create()
      rootNode:addChild( rootTipNode )
      local tipWidth = 0
      local tipNode =  cc.Node:create()
      tipNode:setAnchorPoint(0,0.5)
      local gxhdSpr = display.newSprite("#fish_txt_gxhd.png")
      gxhdSpr:setAnchorPoint( cc.p(0,0.5) )
      local gxhdSize =  gxhdSpr:getContentSize()
      tipNode:addChild( gxhdSpr )
      gxhdSpr:setPosition( cc.p(0,0) )

      --math.randomseed(tostring( os.time()  ):reverse():sub(1, 7)) -- 设置时间种子
      --local redNum =   math.random( 1,1000 )
      
      tipWidth = tipWidth + gxhdSize.width
      local redPacketAtlas = cc.LabelAtlas:_create(0,"buyu_number/fish_num_gxhd.png",67,96,48)
      redPacketAtlas:setPosition( cc.p(tipWidth , 0 )  )
      redPacketAtlas:setAnchorPoint( cc.p(0,0.5) )
      redPacketAtlas:setString( params.redCoin )
      tipNode:addChild( redPacketAtlas  )
      local redPacketAtlasSize = redPacketAtlas:getContentSize()
      tipWidth = tipWidth + redPacketAtlasSize.width 
      
      local yhbSpr = display.newSprite("#fish_txt_yhb.png")
      yhbSpr:setAnchorPoint( cc.p(0,0.5) )
      local gxhdSize =  yhbSpr:getContentSize()
      yhbSpr:setPosition( cc.p( tipWidth ,0 ) )
      tipNode:addChild( yhbSpr )
      tipWidth = tipWidth + gxhdSize.width

      tipNode:setPosition( cc.p( -tipWidth/2,0 ) )
      rootTipNode:addChild( tipNode  )
      rootTipNode:setPosition( cc.p( 0,10) )
      rootTipNode:setScale( 0.05 )
      rootTipNode:setVisible(false)
      -- 红包
      local redPacketImg =   ccui.ImageView:create("fish_img_hbclose.png",ccui.TextureResType.plistType )
      redPacketImg:setAnchorPoint( cc.p(0.5,0.75)  )
      redPacketImg:setVisible( false )
      rootNode:addChild( redPacketImg )
      local function showXing()
          bigXingSpr:setVisible(  true )
          for i=1,3 do
             cxSprS[i]:setVisible( true  )
             yxSprS[i]:setVisible( true )
          end
          redPacketImg:setVisible( true )
      end 
      local callfunc_1 = cc.CallFunc:create( showXing ) 
      bgSpr:setRotation( 0 ) 
      local  angleTotal = 360 * 1.5
      bgSpr:runAction( cc.Sequence:create( cc.ScaleTo:create(0.05,3.5),callfunc_1, cc.RotateTo:create(1, angleTotal),callfunc ) )
       
      local function openRedPacket()
          redPacketImg:loadTexture( "fish_img_hbopen.png" , ccui.TextureResType.plistType)
          tipNode:setVisible( true )
          local fadeIn_3 = cc.FadeIn:create(0.1 )
          local scaleTo_3 =  cc.ScaleTo:create(0.15,0.9)
          local moveTo_3 = cc.MoveTo:create( 0.15,cc.p( 0,180 ) )
          local spawn_3 =  cc.Spawn:create( moveTo_3 , scaleTo_3 )
          rootTipNode:setVisible( true )
          rootTipNode:runAction( cc.Sequence:create( spawn_3 ) )
      end
      local callfunc_2 = cc.CallFunc:create( openRedPacket ) 
      redPacketImg:runAction( cc.Sequence:create( cc.DelayTime:create( 0.1 ),callfunc_2 ) )
    
end


function EffectManager.redPacketsDropOutNew( params )
     cc.SpriteFrameCache:getInstance():addSpriteFrames("buyu_p_plist/p_buyu_friend_balance_pao.plist","buyu_p_plist/p_buyu_friend_balance_pao.png")
      local rootNode = cc.Node:create()
      rootNode:setPosition( params.redPos )
      params.prarentNode:addChild( rootNode , FishGlobal.UILevel -1 )
      
      local bgSpr = display.newSprite("#fish_dt_f_bg_gongxihuode.png")
      bgSpr:setAnchorPoint( cc.p(0.5,0.5) )
      --bgSpr:setBlendFunc(gl.ONE, gl.ONE ) --gl.DST_ALPHA)
      rootNode:addChild( bgSpr )
      bgSpr:setVisible( false )

      local redPacketImg =   ccui.ImageView:create("fish_img_hbclose.png",ccui.TextureResType.plistType )
      redPacketImg:setAnchorPoint( cc.p(0.5,0.5)  )
      rootNode:addChild( redPacketImg )

      local rootValueNode = cc.Node:create()
      rootValueNode:setVisible(false)
      rootNode:addChild( rootValueNode )
      local valueWidth = 0
      local valueNode =  cc.Node:create()
      valueNode:setAnchorPoint(0,0.5)
      
      local redPacketAtlas = cc.LabelAtlas:_create(0,"buyu_number/fish_num_gxhd.png",30,38,48)
      redPacketAtlas:setPosition( cc.p(valueWidth , 0 )  )
      redPacketAtlas:setAnchorPoint( cc.p(0,0.5) )
      --x%1 表示x的小数部分，x-x%1 表示x的整数部分。
      local  redRmb = params.redCoin/100
      local  integer = redRmb - redRmb%1
      local  decimalPoint = redRmb%1 * 100
      local decimalPointStr = decimalPoint
      if decimalPoint > 0 and decimalPoint <= 9 then
         decimalPointStr = "0"..decimalPointStr
      end

      redPacketAtlas:setString( integer )
      valueNode:addChild( redPacketAtlas  )
      local redPacketAtlasSize = redPacketAtlas:getContentSize()
      valueWidth = valueWidth + redPacketAtlasSize.width 
      if decimalPoint > 0 then
          local pointSpr = display.newSprite("#fish_num_point.png")
          pointSpr:setAnchorPoint( cc.p(0.5,1) )
          local pointSprSize =  pointSpr:getContentSize()
          pointSpr:setPosition( cc.p( valueWidth + pointSprSize.width/2 , 0) )
          valueNode:addChild( pointSpr )
          valueWidth = valueWidth + pointSprSize.width
          
          local pointPacketAtlas = cc.LabelAtlas:_create(0,"buyu_number/fish_num_gxhd.png",30,38,48)
          pointPacketAtlas:setPosition( cc.p( valueWidth , 0 )  )
          pointPacketAtlas:setAnchorPoint( cc.p(0,0.5) )
          pointPacketAtlas:setString(  decimalPointStr )
          valueNode:addChild( pointPacketAtlas  )
          local pointPacketAtlasSize = pointPacketAtlas:getContentSize()
          valueWidth = valueWidth + pointPacketAtlasSize.width
      end

      local fenSpr = display.newSprite("#fish_img_hbopen_fen.png")
      fenSpr:setAnchorPoint( cc.p(0,0.5) )
      local fenSprSize =  fenSpr:getContentSize()
      fenSpr:setPosition( cc.p( valueWidth ,-3 ) )
      valueNode:addChild( fenSpr )
      valueWidth = valueWidth + fenSprSize.width

      valueNode:setPosition( cc.p( -valueWidth/2,0 ) )
      rootValueNode:addChild( valueNode  )
      rootValueNode:setPosition( cc.p( 0,-35) )

      local function openRedPacket()
          redPacketImg:loadTexture( "fish_img_hbopen.png" , ccui.TextureResType.plistType)
          rootValueNode:setVisible(true) 
          bgSpr:setVisible( true )
          local  angleTotal = 360 *1.5
          bgSpr:runAction( cc.Sequence:create( cc.ScaleTo:create(0.1,2), cc.RotateTo:create(2.2, angleTotal)) )     
      end

      local function dropOutEnd()
         if next( EffectManager.redPackets[params.chair]  ) then
            table.remove(EffectManager.redPackets[ params.chair ] , 1  )
         end
         rootNode:removeFromParent() 

         if next( EffectManager.redPackets[ params.chair ]  ) then
            if EffectManager.redPackets[params.chair][1] then
               EffectManager.redPacketsDropOutNew( EffectManager.redPackets[params.chair][1] )
            end
         end
      end 
      
      local callfunc_1 = cc.CallFunc:create( openRedPacket ) 
      local callfunc_2 = cc.CallFunc:create( dropOutEnd ) 
      redPacketImg:runAction( cc.Sequence:create( cc.DelayTime:create( 0.3 ),callfunc_1 ,cc.DelayTime:create( 2 ) ,callfunc_2) )

end


return EffectManager