--
-- Author:pang
-- Date: 2017-03-16 10:30:50
--   
local FishGlobal              = require("src.app.game.Fishing.src.FishGlobal")
local FishModel               = require("src.app.game.Fishing.src.FishingGame.model.FishModel")
local FishGameDataController  = require("src.app.game.Fishing.src.FishingCommon.controller.FishGameDataController")
local  EffectManager          = require("src.app.game.Fishing.src.FishingGame.controller.EffectManager")

local FishManager = class("FishManager")

FishManager.troopMaxCount   = 20   -- 鱼阵最大数量
FishManager.STATE_GL_LIST   = {}   
FishManager.lightTexture2D  = 1
FishManager.normalTexture2D = 2
FishManager.shodow          = 3 



function FishManager:ctor( __roomType )
	print("-----------roomType-----------",__roomType)
	-- 初始化鱼阵数据
	self.troopDataList = {}
	for i=1,FishManager.troopMaxCount do
		local troopPath = string.format("src.app.game.Fishing.res.troops.trp%d",i)
        if ToolKit:isLuaExist( troopPath ) then
	       local troopData = require(troopPath)
	       self.troopDataList[ i ] = troopData
	    end
	end
  self.roomType = __roomType
  if self.roomType == FishGlobal.RoomType.System  or self.roomType == FishGlobal.RoomType.GoldIngot or self.roomType == FishGlobal.RoomType.ClubCity then
      self.fishConfigDataMap = FishGameDataController:getInstance():getFishConfigDataMap()
  elseif self.roomType == FishGlobal.RoomType.Friends or self.roomType == FishGlobal.RoomType.ClubFriend then
      self.fishConfigDataMap = FishGameDataController:getInstance():getFriendFishConfigDataMap()
  end
	-- 初始化鱼的轨迹数据
    qka.QKAPathManager:getInstance()
    self:initGlList()
end

-- 通过索引在指定鱼阵中获取鱼的位置及种类信息
function FishManager:getTroopItemDataByIndex( troopId, index)
	local troopData = self.troopDataList[ troopId ]
	local troopItemData = nil
	if troopData then
       for k,v in pairs( troopData ) do
       	  if v.index == index then
               troopItemData = v
               break
       	  end
       end
	end
	return troopItemData
end


-- 生成鱼阵中鱼
function FishManager:createTroopFish(troopId , troopFishInfo , __time , parentNode)
   local fish = nil
   local troopItemData = self:getTroopItemDataByIndex( troopId, troopFishInfo.m_nID)
   --local fishDirec = self:getFishDirectByTroopId( troopId ) 
   if troopItemData then 
        local fishConfigDataItem = self.fishConfigDataMap[ troopFishInfo.m_nType ]    
        if fishConfigDataItem then
           -- 创建鱼的模型
           fish = FishModel.new()
           local spriteFrameName = string.format("%s_%05d.png",fishConfigDataItem.fishName,0)
           local frame =  cc.SpriteFrameCache:getInstance():getSpriteFrame( spriteFrameName )
		   if frame then
	           fish:setSpriteFrame( display.newSpriteFrame (  spriteFrameName ) )
		   else
               EffectManager.loadFishResourceByName( fishConfigDataItem.fishName )
               local frame =  cc.SpriteFrameCache:getInstance():getSpriteFrame( spriteFrameName )
               if frame then
                  fish:setSpriteFrame( display.newSpriteFrame (  spriteFrameName ) )
               end
		   end
           -- 获取鱼的动画
           local animation = cc.AnimationCache:getInstance():getAnimation(string.format("%s_swim",fishConfigDataItem.fishName))
		   if  animation then
		   	   fish:runAction(cc.RepeatForever:create(cc.Animate:create(animation)))
		   else
		   	    self:createFishAnimationByName( fishConfigDataItem.fishName )
				local animation = cc.AnimationCache:getInstance():getAnimation(string.format("%s_swim",fishConfigDataItem.fishName))
				fish:runAction(cc.RepeatForever:create(cc.Animate:create(animation)))
                --print("没有该鱼的动画")
		   end
           -- 设置鱼的属性
           fish:setFishID( troopFishInfo.m_nIndex )                   -- 鱼的唯一id
	       fish:setFishType( troopFishInfo.m_nType )                  -- 鱼的类型
	       fish:setFishName( fishConfigDataItem.fishName )            -- 鱼的名称
		   fish:setSpeed( troopItemData.speed )                       -- 鱼游走的速度
		   fish:setFishScore( troopFishInfo.m_nType )                 -- 鱼的倍数
		   fish:setTroop( true )                                      -- 是否是鱼阵中的鱼
		   fish:setDeathSound( fishConfigDataItem.soundEffect )       -- 鱼死亡的音效
		   fish:setFingerling( fishConfigDataItem.nType )             -- 设置鱼种 
		   fish:setFishBone( fishConfigDataItem.dianFishImage  )      -- 设置鱼的骨头          
		   --fish:setScale(0.9) 
		   -- 宝箱对话框   
           fish:createChestDialog( fishConfigDataItem.szDiaImage )  
           fish:setScoreSound( fishConfigDataItem.scoreSound  )

           local viewOrientation = troopItemData.orientation          -- 鱼的视图朝向，统一默认向左 
           local fishPositionX =    troopItemData.x 
	       if  troopItemData.orientation == FishGlobal.fishOrientation.left  then  
	       	   -- 鱼的出生朝向左
	       	   viewOrientation = -1  
               fishPositionX = fishPositionX + display.width
	       elseif troopItemData.orientation == FishGlobal.fishOrientation.right  then
	       	   -- 鱼的出生朝向右
	       	   viewOrientation = 1 
	       	   fishPositionX = fishPositionX - display.width
	       end 
	       local moveDistance =  troopItemData.speed * g_GameController:getFishLastTime(troopFishInfo.m_nClockTime,  troopFishInfo.m_nLastTime)    -- 鱼已经游走的路程  
	       fish:setPathDistance( moveDistance )
	       local fishPositionX =  fishPositionX  + ( moveDistance * viewOrientation )
	       -- 对于鱼的位置进行转换
	       local glPos = cc.Director:getInstance():convertToGL( cc.p( fishPositionX * display.scaleX , troopItemData.y * display.scaleY ) )
	       local fishViewPosition = FishGlobal.convertFishPostion( { x = glPos.x , y = glPos.y } )
	       --dump( fishViewPosition )
	       fish:setPosition( fishViewPosition )  
	       fish:setAnchorPoint(cc.p(0.5,0.5))
	      
           -- 游戏场景的视图是否发生翻转
           local isFlipped =  FishGlobal.isConvertSenceFlipped()
           --print("isFlipped=",isFlipped)
           if isFlipped then
              viewOrientation = viewOrientation * -1
           end
           fish:setOrientation( viewOrientation ) 
           -- 鱼的视图朝向向右则进行翻转
           if fish:getOrientation() == 1 then  
           	  --print("鱼的视图朝向向右则进行翻转")
           	  if fish:getFingerling() ~= FishModel.FINGERLING.ITEM_FISH_TYPE  then
                 fish:setFlippedX( true )
              end
           end   
           fish:showFishShadow()
            if __time > 0 then
	        	local leftTime = troopFishInfo.m_nIceTime - __time
	        	if leftTime > 0 then
                   EffectManager.playfrozen( { prarentNode = parentNode , isAnimal = false , _type = 0 } )
	               parentNode:frozenFish( fish , leftTime )
	        	end
            end
        else
            print("没有鱼的配置信息!")
        end          
    else
    	print("没有鱼阵信息")
    end
    return fish
end


   

--生成走轨迹的鱼
function FishManager:createLineFish( parentNode , outFishInfo ,__time)
	local fish = nil
	local fishConfigDataItem = self.fishConfigDataMap[ outFishInfo.m_nType ]
	--print("FishManager:createLineFish")
	--print("outFishInfo.m_nType=",outFishInfo.m_nType)
	if fishConfigDataItem then
	    fish = FishModel.new()
        --fish:setBlendFunc(gl.ZERO, gl.ONE_MINUS_SRC_COLOR) -- 浮雕样式
	    fish:setFishID( outFishInfo.m_nIndex )               -- 设置鱼的唯一id
	    fish:setFishType( outFishInfo.m_nType )              -- 设置鱼的类型
	    fish:setSpeed( fishConfigDataItem.moveSpeed )        -- 设置鱼的速度
	    fish:setTag(outFishInfo.m_nIndex)
	    fish:setFishName(fishConfigDataItem.fishName)        -- 鱼的名称
	    fish:setFishScore( outFishInfo.m_nType )             -- 鱼的倍数
	    fish:setTroop(false)                           
	    fish:setDeathSound( fishConfigDataItem.soundEffect ) -- 鱼死亡的音效
	    fish:setFingerling( fishConfigDataItem.nType )       -- 设置鱼种  
	    fish:setFishBone( fishConfigDataItem.dianFishImage  )-- 设置鱼的骨头     
	    fish:setScoreSound( fishConfigDataItem.scoreSound  )    
	    --fish:setScale(0.9) 
	    -- 宝箱对话框   
	    fish:createChestDialog( fishConfigDataItem.szDiaImage )           
		-- 获取鱼的起始位置
	    local orginalPosition = qka.QKAPathManager:getInstance():getPoint( outFishInfo.m_nLine , 0 )
	    local isFlipped =  FishGlobal.isConvertSenceFlipped()
	    local isFlippedY = false
	    local isFlippedX = false
        -- 鱼的视图朝向
        if orginalPosition.x <   display.width / 2 then
           fish:setFlippedY( true ) 
           isFlippedY = true
           if fish:getFingerling() == FishModel.FINGERLING.ITEM_FISH_TYPE or fish:getFingerling() ==  FishModel.FINGERLING.REDPACKET_FISH_TYPE  then
               fish:setFlippedX( true )
               isFlippedX = true
           end
        end 

        if isFlipped then
           if (fish:getFingerling() ~= FishModel.FINGERLING.ITEM_FISH_TYPE ) and ( fish:getFingerling() ~=  FishModel.FINGERLING.REDPACKET_FISH_TYPE )  then 
              isFlippedX = true
              fish:setFlippedX( true )
           end
        end
        print("isFlippedY",isFlippedY)
        fish:initFishappearance(isFlippedX , isFlippedY )
        local spriteFrameName = string.format("%s_%05d.png",fishConfigDataItem.fishName,0)
	    local frame =  cc.SpriteFrameCache:getInstance():getSpriteFrame( spriteFrameName )
	    if frame then
            fish:setSpriteFrameEx( frame )
	    else 
            EffectManager.loadFishResourceByName( fishConfigDataItem.fishName )
           -- print("fishConfigDataItem.fishName=",fishConfigDataItem.fishName)
            local frame =  cc.SpriteFrameCache:getInstance():getSpriteFrame( spriteFrameName )
            if frame then
               fish:setSpriteFrameEx( display.newSpriteFrame (  spriteFrameName ) )
            end
	    end
		local animation = cc.AnimationCache:getInstance():getAnimation(string.format("%s_swim",fishConfigDataItem.fishName))
		if animation then
		   fish:runActionEx( animation )
		else
            --print("没有该鱼的动画")
            self:createFishAnimationByName( fishConfigDataItem.fishName )
			local animation = cc.AnimationCache:getInstance():getAnimation(string.format("%s_swim",fishConfigDataItem.fishName))
			fish:runActionEx( animation )
		end

	    -- 鱼的路径信息
        local moveDistance = (fishConfigDataItem.moveSpeed * g_GameController:getFishLastTime(outFishInfo.m_nClockTime, outFishInfo.m_nLastTime) ) + ( fishConfigDataItem.moveSpeed  * outFishInfo.m_nDelay/1000.0)
        fish:setPathInfo({index  =outFishInfo.m_nLine , distance = moveDistance})

        local currentPos = qka.QKAPathManager:getInstance():getPoint( outFishInfo.m_nLine , moveDistance )

        local glPos = cc.Director:getInstance():convertToGL( cc.p(currentPos.x * display.scaleX ,currentPos.y * display.scaleY )  )
        local angle  = qka.QKAPathManager:getInstance():getAngle( outFishInfo.m_nLine , moveDistance)
        local currentViewPos =  FishGlobal.convertFishPostion( glPos ) 
        fish:setPosition( currentViewPos )
        fish:setAnchorPoint(cc.p(0.5,0.5))
        if ( fish:getFingerling() ~= FishModel.FINGERLING.CHESS_FISH_TYPE )  then 
            fish:setRotation( 180*angle/math.pi + 180  )
        end
        fish:showFishShadow()
        if __time > 0 then
        	local leftTime = outFishInfo.m_nIceTime - __time
        	if leftTime > 0 then
        	   EffectManager.playfrozen( { prarentNode = parentNode , isAnimal = false , _type = 0 } )
               parentNode:frozenFish( fish , leftTime )
        	end
        end
	else
		 print("没有鱼的配置信息!")
	end
	return fish
end

   

--生成走轨迹的鱼
function FishManager:createLineFishYY( parentNode , outFishInfo ,__time)
	local fish = nil
	local fishConfigDataItem = self.fishConfigDataMap[ outFishInfo.m_nType ]
	if fishConfigDataItem then
	    fish = FishModel.new()
	    local spriteFrameName = string.format("%s_%05d.png",fishConfigDataItem.fishName,0)
	    local frame =  cc.SpriteFrameCache:getInstance():getSpriteFrame( spriteFrameName )
	    if frame then
            fish:setSpriteFrame( frame )
	    else
            EffectManager.loadFishResourceByName( fishConfigDataItem.fishName )
            local spriteFrameName = string.format("%s_%05d.png",fishConfigDataItem.fishName,0)
            fish:setSpriteFrame( display.newSpriteFrame (  spriteFrameName ) )
	    end
		local animation = cc.AnimationCache:getInstance():getAnimation(string.format("%s_swim",fishConfigDataItem.fishName))
		if animation then
		   fish:runAction(cc.RepeatForever:create(cc.Animate:create(animation)))
		else
			self:createFishAnimationByName( fishConfigDataItem.fishName )
			local animation = cc.AnimationCache:getInstance():getAnimation(string.format("%s_swim",fishConfigDataItem.fishName))
			fish:runAction(cc.RepeatForever:create(cc.Animate:create(animation)))
            --print("没有该鱼的动画")
		end
        --fish:setBlendFunc(gl.ZERO, gl.ONE_MINUS_SRC_COLOR) -- 浮雕样式
	    fish:setFishID( outFishInfo.m_nIndex )               -- 设置鱼的唯一id
	    fish:setFishType( outFishInfo.m_nType )              -- 设置鱼的类型
	    fish:setSpeed( fishConfigDataItem.moveSpeed )        -- 设置鱼的速度
	    fish:setTag(outFishInfo.m_nIndex)
	    fish:setFishName(fishConfigDataItem.fishName)        -- 鱼的名称
	    fish:setFishScore( outFishInfo.m_nType )             -- 鱼的倍数
	    fish:setTroop(false)                           
	    fish:setDeathSound( fishConfigDataItem.soundEffect ) -- 鱼死亡的音效
	    fish:setFingerling( fishConfigDataItem.nType )       -- 设置鱼种  
	    fish:setFishBone( fishConfigDataItem.dianFishImage  )-- 设置鱼的骨头         
	    --fish:setScale(0.9)               
		-- 获取鱼的起始位置

	    local orginalPosition = qka.QKAPathManager:getInstance():getPoint( outFishInfo.m_nLine , 0 )
	    local isFlipped =  FishGlobal.isConvertSenceFriendFlipped()
        -- 鱼的视图朝向
        if orginalPosition.x <   display.width / 2 then
           fish:setFlippedY( true ) 
           if fish:getFingerling() == FishModel.FINGERLING.ITEM_FISH_TYPE   then
               fish:setFlippedX( true )
           end
        end 
	    -- 鱼的路径信息
        local moveDistance = (fishConfigDataItem.moveSpeed * g_GameController:getFishLastTime(outFishInfo.m_nClockTime, outFishInfo.m_nLastTime) ) + ( fishConfigDataItem.moveSpeed  * outFishInfo.m_nDelay/1000.0)
        fish:setPathInfo({index  =outFishInfo.m_nLine , distance = moveDistance})

        local currentPos = qka.QKAPathManager:getInstance():getPoint( outFishInfo.m_nLine , moveDistance )

        local glPos = cc.Director:getInstance():convertToGL( cc.p(currentPos.x * display.scaleX ,currentPos.y * display.scaleY )  )
        local angle  = qka.QKAPathManager:getInstance():getAngle( outFishInfo.m_nLine , moveDistance)
        local currentViewPos =  FishGlobal.convertFishFriendPostion( glPos ) 
        fish:setPosition( currentViewPos )
        fish:setAnchorPoint(cc.p(0.5,0.5))
        local rotation = 180*angle/math.pi + 180
        if isFlipped then
           rotation = 360 - rotation
        end

        fish:setRotation( rotation  )
        fish:showFishShadow()
	else
		 print("没有鱼的配置信息!",outFishInfo.m_nType)
	end
	return fish
end

function FishManager:initGlList()
    self:loadGl("buyu_fishShader/lightTexture2D.vsh", "buyu_fishShader/lightTexture2D.fsh","lightTexture2DKey")
	--self:loadGl("buyu_fishShader/normalTexture2D.vsh", "buyu_fishShader/normalTexture2D.fsh","normalTexture2DKey" )
end


function FishManager:loadGl(vshFile, fshFile , key  )
	local fileUtiles = cc.FileUtils:getInstance() 
    local pProgram = nil
	if fileUtiles:isFileExist( vshFile ) and fileUtiles:isFileExist( fshFile ) then
        pProgram = cc.GLProgram:createWithFilenames( vshFile , fshFile )  
        cc.GLProgramCache:getInstance():addGLProgram( pProgram , key )
    else
        print("文件不存在",vshFile,fshFile)
    end
    return pProgram
end

-- 缓存所有鱼动画数据
function FishManager:createAllFishAnimation()
	-- local spriteCache = cc.SpriteFrameCache:getInstance()
	-- for k,v in pairs( self.fishConfigDataMap ) do
	--     --游动帧
	-- 	local animation_swim = cc.Animation:create()
	-- 	animation_swim:setDelayPerUnit(v.swimFrameDelay)
	-- 	for sw,swim in pairs(v.swimRes) do
	-- 		local spriteFrameName = string.format("%s_%05d.png",v.fishName,swim)
	-- 		local spriteFrame= spriteCache:getSpriteFrame( spriteFrameName )
	-- 		if spriteFrame then
	-- 		   animation_swim:addSpriteFrame( spriteFrame )
	-- 		else
 --               print("找不到精灵帧",spriteFrameName)
	-- 		end
	-- 	end
	-- 	cc.AnimationCache:getInstance():addAnimation(animation_swim,v.fishName.."_swim")
	--     --挣扎帧
	--     if #v.struggleRes > 1 then
	-- 		local animation_struggle = cc.Animation:create()
	-- 		animation_struggle:setDelayPerUnit(v.struggleFrameDelay)
	-- 		for st,struggle in pairs(v.struggleRes) do
	-- 			local spriteFrameName = string.format("%s_%05d.png",v.fishName,struggle)
	-- 			local spriteFrame= spriteCache:getSpriteFrame( spriteFrameName )
	-- 			if spriteFrame then
	-- 			   animation_struggle:addSpriteFrame( spriteFrame )
	-- 			else
	--                print("找不到精灵帧",spriteFrameName)
	-- 			end
	-- 		end
	-- 		cc.AnimationCache:getInstance():addAnimation(animation_struggle,v.fishName.."_struggle")
	-- 	end

	-- 	--死亡帧
	-- 	local animation_death = cc.Animation:create()
	-- 	animation_death:setDelayPerUnit(v.deathFrameDelay)
	-- 	for st,death in pairs(v.deathRes) do
	-- 		local spriteFrameName = string.format("%s_%05d.png",v.fishName,death)
	-- 		local spriteFrame= spriteCache:getSpriteFrame( spriteFrameName )
	-- 		if spriteFrame then
	-- 		   animation_death:addSpriteFrame( spriteFrame )
	-- 		else
 --               print("找不到精灵帧",spriteFrameName)
	-- 		end
	-- 	end
	-- 	cc.AnimationCache:getInstance():addAnimation(animation_death,v.fishName.."_death")
	-- end
end

-- 清除所有鱼的动画数据
function FishManager:clearAllFishAnimation()
	for k,v in pairs( self.fishConfigDataMap ) do
	    cc.AnimationCache:getInstance():removeAnimation( v.fishName.."_swim" )
        --cc.AnimationCache:getInstance():removeAnimation( v.fishName.."_struggle" )
        cc.AnimationCache:getInstance():removeAnimation( v.fishName.."_death" )
	end
end


-- 缓存所有鱼动画数据
function FishManager:createFishAnimationByName( fishName )
	local spriteCache = cc.SpriteFrameCache:getInstance()
	for k,v in pairs( self.fishConfigDataMap ) do
		if v.fishName == fishName then 
			    --游动帧
				local animation_swim = cc.Animation:create()
				animation_swim:setDelayPerUnit(v.swimFrameDelay)
				for sw,swim in pairs(v.swimRes) do
					local spriteFrameName = string.format("%s_%05d.png",v.fishName,swim)
					local spriteFrame= spriteCache:getSpriteFrame( spriteFrameName )
					if spriteFrame then
					   spriteFrame:retain()
					   animation_swim:addSpriteFrame( spriteFrame )
					else
		               print("找不到精灵帧",spriteFrameName)
					end
				end
				cc.AnimationCache:getInstance():addAnimation(animation_swim,v.fishName.."_swim")

				--死亡帧
				local animation_death = cc.Animation:create()
				animation_death:setDelayPerUnit(v.deathFrameDelay)
				for st,death in pairs(v.deathRes) do
					local spriteFrameName = string.format("%s_d_%05d.png",v.fishName,death)
					local spriteFrame= spriteCache:getSpriteFrame( spriteFrameName )
					if spriteFrame then
					   spriteFrame:retain()
					   animation_death:addSpriteFrame( spriteFrame )
					else
		               print("找不到精灵帧",spriteFrameName)
					end
				end
				cc.AnimationCache:getInstance():addAnimation(animation_death,v.fishName.."_death")
	           break
		end
	end
end


 return FishManager
