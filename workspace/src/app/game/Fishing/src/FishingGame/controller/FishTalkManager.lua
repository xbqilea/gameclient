-- 
-- FishTalkManager
-- Author: chenzhanming
-- Date: 2017-08-29 11:51:38
-- 聊天控制器
--

local FishGlobal              =  require("src.app.game.Fishing.src.FishGlobal")
local TalkVoiceLayer          =  require("app.game.common.chat.ChatSystemLayer") 

local FishTalkManager = class("FishTalkManager")


-- 获取聊天控制器实例
function FishTalkManager:getInstance()
  if FishTalkManager.instance == nil then
     FishTalkManager.instance = FishTalkManager.new()
  end
    return FishTalkManager.instance
end

function FishTalkManager:ctor()
	 self.talkVoiceLayer= nil
end

--初始化语音组件
function FishTalkManager:initTalkVoiceLayer( __params )
    if tolua.isnull( self.talkVoiceLayer) then
        local gameAtomTypeId = __params.gameAtomTypeId
        
        local params = 
        {
          --resourcePath = "",-- 资源路径
          --gameUIConfig = "", --游戏配置
          gameAtomTypeId = gameAtomTypeId,
          serverReq = "CS_C2M_BuYu_ClientChat_Nty",
          serverAck = "CS_M2C_BuYu_ClientChat_Nty",
        }
        self.talkVoiceLayer = TalkVoiceLayer.new( params )  
        __params.parentNode:addChild(self.talkVoiceLayer, FishGlobal.UILevel + 1)
        
        if self.talkVoiceLayer.messageButton and self.talkVoiceLayer.talkButton then
          -- 设置聊天按钮和语音按钮的位置
          local m_pos = cc.p(self.talkVoiceLayer.messageButton:getPosition())
          local t_pos = cc.p(self.talkVoiceLayer.talkButton:getPosition())
          m_pos.y = m_pos.y + 60
          t_pos.y = t_pos.y + 20
          self.talkVoiceLayer:setMessageAndTalkButtonPos(m_pos, t_pos )
          self.talkVoiceLayer.messageButton:setVisible( false )
          self.talkVoiceLayer.talkButton:setVisible(true)
        end
    end
end


-- 参数一表示玩家的用户id
-- 参数二表示玩家的位置
--  聊天的位置
-- 参数三表示是否播放表情的时候需要水平方向翻转
function FishTalkManager:setUserIdPos(_userId,_pos,_pos2,_isFlipX,_isFlipY)
    if not tolua.isnull( self.talkVoiceLayer ) then
        local params = 
        {    
          userID = _userId,        -- 玩家id
          messagepos = _pos,       -- 播放快捷聊天位置/语音的位置
          expressionPos= _pos2,    -- 表情的位置
          isFlippedX = _isFlipX ,  -- 播放快捷聊天背景/语音背景
          isFlippedY = _isFlipY ,  -- 播放快捷聊天背景/语音背景
          gender = 2               -- 性别：0.未知1.男 2.女.
        }
        self.talkVoiceLayer:setUserIDPos(params)
    end
end

-- 设置语音按钮的位置
function FishTalkManager:setTalkBtnPosY( _posY )
    if not tolua.isnull( self.talkVoiceLayer ) then
       self.talkVoiceLayer.talkButton:setPositionY( _posY )
    end
end

function FishTalkManager:setTalkBtnVisble( isVisble )
    if not tolua.isnull( self.talkVoiceLayer ) then
       self.talkVoiceLayer.talkButton:setVisible( isVisble )
    end
end


-- 销毁聊天控制器
function FishTalkManager:onDestory()
    FishTalkManager.instance = nil
end

return FishTalkManager