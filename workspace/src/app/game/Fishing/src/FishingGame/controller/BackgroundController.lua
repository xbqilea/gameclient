--
-- BackgroundController
-- Author: chenzhanming
-- Date: 2017-05-22 15:57:38
-- 捕鱼场景控制器(场景波浪,场景移动)
--
local FishGlobal                 = require("src.app.game.Fishing.src.FishGlobal")
local FishSoundManager           = require("src.app.game.Fishing.src.FishingCommon.controller.FishSoundManager")
local FishingAnimationManager    = require("src.app.game.Fishing.src.FishingCommon.controller.FishingAnimationManager")
local FishGameDataController     = require("src.app.game.Fishing.src.FishingCommon.controller.FishGameDataController") 
local EffectManager              = require("src.app.game.Fishing.src.FishingGame.controller.EffectManager")

local BackgroundController = class("BackgroundController", function()
  return display.newLayer()
end)


BackgroundController.frameTimeLimit = 1

function BackgroundController:ctor( roomType )
    self.roomType = roomType
	--背景编号
    if self.roomType == FishGlobal.RoomType.System or self.roomType == FishGlobal.RoomType.GoldIngot or self.roomType == FishGlobal.RoomType.ClubCity then
    	self.bgIndex = g_GameController:getGameSceneIndex()
    elseif self.roomType == FishGlobal.RoomType.Friends or self.roomType == FishGlobal.RoomType.ClubFriend then
        self.bgIndex = FishFriendsGameController:getInstance():getGameSceneIndex()
        self:createFishFriendInfoNode()
    end
    self.bgData  =  FishGameDataController:getInstance():getSceneDataByIndex( self.bgIndex )
	self.isJumping = false
	self:setTouchEnabled(false)
	self.m_state = false
	self.speed = 5
    self.frameTimes = 0
	self:init()
    self:setNodeEventEnabled( true )
end

function BackgroundController:addWaveAnima()
    local wavesName = string.format("lang_%05d.png",0)
    cc.SpriteFrameCache:getInstance():addSpriteFrames("buyu_p_plist/lang.plist","buyu_p_plist/lang.png")
    local frames = display.newFrames("lang_%05d.png",0,5)
    if frames then
       local waves = display.newAnimation(frames,0.1)
       cc.AnimationCache:getInstance():addAnimation(waves,"waves")
    end
end

function BackgroundController:init()
    self:addWaveAnima()
	self.BgNode = cc.Node:create()
    self.BgNode:setPosition(cc.p(display.cx,display.cy))
    self:addChild( self.BgNode,3 )
    
    
    self.bgSpr_1 =  display.newSprite( self.bgData.szSceneRouteMbc[ 1 ]) --背景1
    self.bgSpr_1:setAnchorPoint( cc.p( 0.5 , 0.5 ))
    self.bgSpr_1:setPosition(cc.p(display.cx,display.cy))
    self.bgSpr_1:setScaleX( display.scaleX )
    self.bgSpr_1:setScaleY( display.scaleY )
    self:addChild(self.bgSpr_1,2)
    
    --气泡特效
    local system = cc.ParticleSystemQuad:create("buyu_particle/bubble.plist") 
    system:setPosition(cc.p(display.width/3, display.height/2))  
    self.bgSpr_1:addChild(system,10)

    --  --气泡特效
    -- local system_3 = cc.ParticleSystemQuad:create("buyu_particle/bubble.plist") 
    -- system:setPosition(cc.p(display.width*0.8, display.height*0.3))  
    -- self.bgSpr_1:addChild(system_3,10)

     --气泡特效
    local system_1 = cc.ParticleSystemQuad:create("buyu_particle/bubble02.plist") 
    system:setPosition(cc.p(display.width*0.75, display.height*0.35))  
    self.bgSpr_1:addChild(system_1,10)


    --  --气泡特效
    -- local system_2 = cc.ParticleSystemQuad:create("buyu_particle/bubble02.plist") 
    -- system:setPosition(cc.p(display.width*0.5, display.height/2))  
    -- self.bgSpr_1:addChild(system_2,10)


    -- self.bgSpr_2 =  display.newSprite( self.bgData.szSceneRouteMbc[ 2 ]) --背景2
    -- self.bgSpr_2:setAnchorPoint( cc.p( 0.5 , 0.5 ))
    -- self.bgSpr_2:setPosition(cc.p(display.cx + display.width ,display.cy))
    -- self.bgSpr_2:setScaleX( display.scaleX + 0.001 )
    -- self.bgSpr_2:setScaleY( display.scaleY )
    -- self:addChild(self.bgSpr_2,2 )

      --气泡特效
    -- local system = cc.ParticleSystemQuad:create("buyu_particle/bubble.plist") 
    -- system:setPosition(cc.p(display.width/3, display.height/2))  
    -- self.bgSpr_2:addChild(system,10)

     --气泡特效
    -- local system_1 = cc.ParticleSystemQuad:create("buyu_particle/bubble02.plist") 
    -- system:setPosition(cc.p(display.width*0.75, display.height*0.35))  
    -- self.bgSpr_2:addChild(system_1,10)


    --  --气泡特效
    -- local system_2 = cc.ParticleSystemQuad:create("buyu_particle/bubble02.plist") 
    -- system:setPosition(cc.p(display.width*0.5, display.height/2))  
    -- self.bgSpr_2:addChild(system_2,10)

    -- self.clippingNode = cc.ClippingNode:create()
    -- self.clippingNode:setAnchorPoint( cc.p(0.5,0.5))
    -- self.clippingNode:setPosition( cc.p(display.cx,display.cy))
    -- self.clippingNode:setInverted(true)
    -- self.clippingNode:setAlphaThreshold(0.1)
    -- self.bgSpr_1:addChild( self.clippingNode )
    -- local wavesAmature =  FishingAnimationManager:getInstance():playArmatureAnimation(self.clippingNode ,  1005, cc.p(0,-10) )
    -- self.clippingNode:setCascadeOpacityEnabled(true)
    -- wavesAmature:setOpacity( 25 )
    -- local  waveShaowStel =  display.newSprite("buyu_p_texture/buyu_e_waveshadow_01.png")
    -- waveShaowStel:setAnchorPoint(cc.p(0.5,0.5))
    -- self.clippingNode:setStencil(waveShaowStel) 

    -- local fadeOut =  cc.FadeOut:create(3)
    -- local backOut = fadeOut:reverse()
    -- wavesAmature:runAction( cc.RepeatForever:create( cc.Sequence:create(fadeOut,backOut,nil )))
    -- FishSoundManager:getInstance():playBgMusic( self.bgData.szBgm )

    -- 帧事件回调
    --self:addNodeEventListener(cc.NODE_ENTER_FRAME_EVENT,handler(self,self.update))
    if self.roomType == FishGlobal.RoomType.System or self.roomType == FishGlobal.RoomType.GoldIngot or self.roomType == FishGlobal.RoomType.ClubCity then
         --self:scheduleUpdate()
    elseif self.roomType == FishGlobal.RoomType.Friends or self.roomType == FishGlobal.RoomType.ClubFriend then
        
    end
    -- 停止定时器 
    --self:unscheduleUpdate()
end

function BackgroundController:removeRes()
    display.removeSpriteFramesWithFile("buyu_p_plist/lang.plist","buyu_p_plist/lang.png")
    cc.AnimationCache:getInstance():removeAnimation( "waves" )
end

--水波纹效果
function BackgroundController:crateWal()
    -- body
end


function BackgroundController:isChanging()
    return self.m_state
end


function BackgroundController:getBackMusicEx()
    --print("getBackMusicEx()")
    --dump( self.bgData )
    return  self.bgData.szBgm
end



function BackgroundController:update( dt )
    -- self.frameTimes = self.frameTimes + 1
    -- if self.frameTimes <= BackgroundController.frameTimeLimit then
    --     return 
    -- end
    
    -- self.frameTimes = 0

   --  local posX1 = self.bgSpr_1:getPositionX()
   --  --local posX2 = self.bgSpr_2:getPositionX()
   --  --print("self.speed=",dt * self.speed)
    
   --  -- 两张地图向左滚动（两张地图是相邻的，所以要一起滚动，否则会出现空隙）
   --  posX1 = posX1 - dt * self.speed
   -- -- posX2 = posX2 - dt * self.speed
   
   --  --当第1个地图完全离开屏幕时，第2个地图刚好完全出现在屏幕上，这时候就让第1个地图紧贴在第2个地图后面 
   --  if posX1 < -display.width/2 then
   --     posX1 = display.width + display.width/2
   --  end
   --  --  同理，当第2个地图完全离开屏幕时，第1个地图刚好完全出现在屏幕上，这时候就让第2个地图紧贴在第1个地图后面 */
   --  -- if  posX2  < -display.width/2 then
   --  --     posX2 = display.width + display.width/2
   --  -- end

   --  self.bgSpr_1:setPositionX( posX1 )
   --  self.bgSpr_2:setPositionX( posX2 )
end

function BackgroundController:setBG( index )
    self.bgIndex = index 
    self.bgData =  FishGameDataController:getInstance():getSceneDataByIndex( self.bgIndex )
    if  not self.bgData then
        self.bgIndex =  1
        self.bgData =  FishGameDataController:getInstance():getSceneDataByIndex( self.bgIndex )
        print("背景索引不正确!")
    end
    if self.roomType == FishGlobal.RoomType.System  or self.roomType == FishGlobal.RoomType.GoldIngot or self.roomType == FishGlobal.ClubCity then
       if not g_GameController:isNeedWave() then
           self:setRollBg()
           if not EffectManager.isfrozen then
              -- self:scheduleUpdate()
           end
           FishSoundManager:getInstance():playBgMusic( self.bgData.szBgm )
           self:getParent():setMusicPath( self.bgData.szBgm )
        else
           self:changeBg()
        end
    elseif self.roomType == FishGlobal.RoomType.Friends or self.roomType == FishGlobal.RoomType.ClubFriend then
         if not FishFriendsGameController:getInstance():isNeedWave() then
           self:setRollBg()
           if not EffectManager.isfrozen then
               --self:scheduleUpdate()
           end
           FishSoundManager:getInstance():playBgMusic( self.bgData.szBgm )
           self:getParent():setMusicPath( self.bgData.szBgm )
        else
           self:changeBgYY()
        end
    end
   
end


function BackgroundController:setRollBg()
	local textureName_1 = self.bgData.szSceneRouteMbc[ 1 ]
	local texture_1 =   cc.Director:getInstance():getTextureCache():addImage( textureName_1 )
	self.bgSpr_1:setTexture( texture_1 ) 
    self.bgSpr_1:setPositionX( display.cx )

	-- local textureName_2 = self.bgData.szSceneRouteMbc[ 2 ]
	-- local texture_2 =   cc.Director:getInstance():getTextureCache():addImage( textureName_2 )
	-- self.bgSpr_2:setTexture( texture_2 ) 
 --    self.bgSpr_2:setPositionX( display.cx + display.width )
end

function BackgroundController:setBackgroundIndex( index )
    self.bgIndex = index
    self.bgData =  FishGameDataController:getInstance():getSceneDataByIndex( self.bgIndex )
    if  not self.bgData then
        self.bgIndex =  1
        self.bgData =  FishGameDataController:getInstance():getSceneDataByIndex( self.bgIndex )
        print("背景索引不正确!")
    end
end

function BackgroundController:changeBgYY()
    self:runAction(cc.Sequence:create(cc.DelayTime:create(0.2),cc.CallFunc:create(function() self:changing() end),nil))
    self.m_state = true
end


--根据场景ID设置背景
function BackgroundController:changeBg()
	self:runAction(cc.Sequence:create(cc.DelayTime:create(2.5),cc.CallFunc:create(function() self:changing() end),nil))
	self.m_state = true
end

--改变场景
function BackgroundController:changing()
    print("-------------BackgroundController:changing-------------")
    self.BgNode:setLocalZOrder( 3 )
    self.bgSpr_1:setLocalZOrder( 1 )
    --self.bgSpr_2:setLocalZOrder( 1 )
    -- 停止定时器 
    --self:unscheduleUpdate()
    local bg_1 = self.BgNode:getChildByTag(self.bgIndex)
    local childs_ = self.BgNode:getChildren()
    if #childs_ > 0 then
      for i=1,#childs_ do
        childs_[i]:setLocalZOrder(0)
        childs_[i]:setPercentage(100)
      end
    end
    -- 播放海浪的声音
    FishSoundManager:getInstance():playEffect( FishSoundManager.WaveEnter  )
    if not bg_1 then
        --print("新场景"..self.bgIndex)
       
        local bg=cc.Sprite:create( self.bgData.szSceneRouteMbc[ 1 ] )
        local progress_bg_2 = cc.ProgressTimer:create(bg)
        progress_bg_2:setType(cc.PROGRESS_TIMER_TYPE_BAR)
        progress_bg_2:setMidpoint(cc.p(1,0))
        progress_bg_2:setBarChangeRate(cc.p(1,0))
        progress_bg_2:setPercentage(1.0)
        progress_bg_2:setScaleX( display.scaleX )
        progress_bg_2:setScaleY( display.scaleY )
        progress_bg_2:runAction(cc.Sequence:create(cc.ProgressTo:create(2.5,100)))
        progress_bg_2:setTag(self.bgIndex)
        self.BgNode:addChild(progress_bg_2,2)
    else
        --print("重置旧场景"..self.bgIndex)
        bg_1:setType(cc.PROGRESS_TIMER_TYPE_BAR)
        bg_1:setMidpoint(cc.p(1,0))
        bg_1:setBarChangeRate(cc.p(1,0))
        bg_1:setPercentage(1.0)
        bg_1:setLocalZOrder(2)
        bg_1:runAction(cc.Sequence:create(cc.ProgressTo:create(2.5,100)))
    end
    local wave_to = cc.Sprite:create()
    local cf=cc.CallFunc:create(function() 
        wave_to:removeFromParent()
        self:setRollBg()
        --self:scheduleUpdate()
        FishSoundManager:getInstance():playBgMusic( self.bgData.szBgm  )
        self:getParent():setMusicPath( self.bgData.szBgm )
        self.BgNode:setLocalZOrder( 1 )
        self.bgSpr_1:setLocalZOrder( 2 )
        --self.bgSpr_2:setLocalZOrder( 2 )
        self.m_state = false
      end)
    local wavesAni = cc.AnimationCache:getInstance():getAnimation("waves")
    if not wavesAni then
       self:addWaveAnima()
       wavesAni = cc.AnimationCache:getInstance():getAnimation("waves")
    end
    wave_to:playAnimationForever( wavesAni )
    wave_to:setPosition(display.right+wave_to:getContentSize().width,display.cy)
    wave_to:runAction(cc.Sequence:create(cc.MoveTo:create(2.5,cc.p(-wave_to:getContentSize().width,display.cy)),cf,nil))
    --wave_to:setScaleX(display.scaleX*1.8)
    wave_to:setScaleY(display.scaleY*1.5)
    self:addChild(wave_to,10)
end

--背景颤动
function BackgroundController:setJump()
	if not self.isJumping then
       self.isJumping = true
	   local j1 = cc.JumpBy:create(1,cc.p(0,0),10,6)
	   self:runAction(cc.Sequence:create(j1,cc.CallFunc:create(function() self.isJumping=false end),nil))
	end
end

--背景强劲颤动
function BackgroundController:setStrongJumps()
    if not self.isJumping then
       self.isJumping = true
       local j1 = cc.JumpBy:create(1.5,cc.p(0,0),25,10)
       self:runAction(cc.Sequence:create(j1,cc.CallFunc:create(function() self.isJumping=false end),nil))
    end
end

-- 暂停背景移动
function BackgroundController:pauseBackGround()
    self:unscheduleUpdate()
end

-- 恢复背景移动
function BackgroundController:resumeBackGround()
    --self:scheduleUpdate()
end

function BackgroundController:createFishFriendInfoNode()
    local rootNode = cc.Node:create()
    self.roomNode = rootNode
    rootNode:setPosition( cc.p(display.scaleX * 656 , display.scaleY * 430 ))
    self:addChild( rootNode , 5 )
--    cc.SpriteFrameCache:getInstance():addSpriteFrames("buyu_p_plist/p_buyu_common_ui.plist","buyu_p_plist/p_buyu_common_ui.png")
    self.bgImg =  ccui.ImageView:create("fish_bg_fjh.png",ccui.TextureResType.plistType )
    self.bgImg:setPosition( cc.p(-3.5,2 ) )
    self.bgImg:setScale9Enabled( true )
    self.bgImg:setCapInsets( cc.rect(18,18,10,10) )
    self.bgImg:ignoreContentAdaptWithSize( true )
    self.bgImg:setContentSize( cc.size( 400 , 50 ) )
    rootNode:addChild(self.bgImg ,1 )
    
    -- 房间号
    self.roomTitle =  ccui.Text:create("房间号:","ttf/jcy.TTF", 18)
    self.roomTitle:setTextColor( cc.c4b(121, 214, 212 ,255) )
    self.roomTitle:setPosition( cc.p( -150, 2) )
    rootNode:addChild( self.roomTitle,2 )

    self.roomNumber = ccui.Text:create("123636","ttf/jcy.TTF", 18)
    self.roomNumber:setTextColor( cc.c4b(121, 214, 212 ,255) )
    self.roomNumber:setPosition( cc.p( -118, 2) )
    self.roomNumber:setAnchorPoint( cc.p(0,0.5 ) )
    rootNode:addChild( self.roomNumber,2 )

    -- 局数
    self.gamenumberTitle =  ccui.Text:create("局数:","ttf/jcy.TTF", 18)
    self.gamenumberTitle:setTextColor( cc.c4b(121, 214, 212 ,255) )
    self.gamenumberTitle:setPosition( cc.p( -21, 2) )
    rootNode:addChild( self.gamenumberTitle,2 )

    self.gamenumber =  ccui.Text:create("0/6","ttf/jcy.TTF", 18)
    self.gamenumber:setTextColor( cc.c4b(121, 214, 212 ,255) )
    self.gamenumber:setPosition( cc.p( 6, 2) )
    self.gamenumber:setAnchorPoint( cc.p(0,0.5 ) )
    rootNode:addChild( self.gamenumber,2 )

    -- 炮台倍数
    self.powerScoreTitle =  ccui.Text:create("炮台倍数:","ttf/jcy.TTF", 18)
    self.powerScoreTitle:setTextColor( cc.c4b(121, 214, 212 ,255) )
    self.powerScoreTitle:setPosition( cc.p( 105, 2) )
    rootNode:addChild( self.powerScoreTitle,2 )

    self.powerScore =  ccui.Text:create("1-5","ttf/jcy.TTF", 18)
    self.powerScore:setTextColor( cc.c4b(121, 214, 212 ,255) )
    self.powerScore:setPosition( cc.p(145, 2) )
    self.powerScore:setAnchorPoint( cc.p(0,0.5 ) )
    rootNode:addChild( self.powerScore,2 )
end

-- 设置节点透明度
function BackgroundController:setRoomNodeOpacity( opacity  )
    self.roomNode:setCascadeOpacityEnabled( true )
    self.roomNode:setOpacity( opacity )
end


-- 设置房间号
function BackgroundController:setRoomNumber( roomNumber )
    self.roomNumber:setString( tostring( roomNumber ) )
end

--设置局数 1/6
function BackgroundController:setGameNumber( gamenumber )
    self.gamenumber:setString(   gamenumber  )
end

--设置炮台倍数 1-5
function BackgroundController:setPowerScore( powerScore )
    self.powerScore:setString(  powerScore  )
end

function BackgroundController:adjustRoomNode()
    self.bgImg:setContentSize( cc.size( 300 , 50 ))
    self.roomTitle:setPositionX( -108 )
    self.roomNumber:setPositionX( -72)
    self.gamenumberTitle:setVisible( false )
    self.gamenumber:setVisible( false )
    self.powerScoreTitle:setPositionX( 60 )
    self.powerScore:setPositionX( 100 )
end

function BackgroundController:onExit()
    print("BackgroundController:onExit")
    self:removeRes()
end

return BackgroundController