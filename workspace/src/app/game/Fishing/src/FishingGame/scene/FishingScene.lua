---------------------------------------
-- FishingScene
-- Author: chengzhanming 
-- 捕鱼游戏主场景
-- Date 2017-04-01 
----------------------------------------
local scheduler            = require("framework.scheduler")
local FishGlobal            = require("src.app.game.Fishing.src.FishGlobal")
local EffectManager        = require("src.app.game.Fishing.src.FishingGame.controller.EffectManager")
local CCGameSceneBase    = require("src.app.game.common.main.CCGameSceneBase")
local NoticeLayer        = require("src.app.game.Fishing.src.FishingGame.layers.NoticeLayer")
local BackgroundController = require("src.app.game.Fishing.src.FishingGame.controller.BackgroundController")
local FishManager        = require("src.app.game.Fishing.src.FishingGame.controller.FishManager")
local FishingPlayer        = require("src.app.game.Fishing.src.FishingGame.model.FishingPlayer")
--local FishGameController    = require("src.app.game.Fishing.src.FishingGame.controller.FishGameController") 
local FishMainLayer        = require("src.app.game.Fishing.src.FishingGame.layers.FishMainLayer")
local FishSkillManager    = require("src.app.game.Fishing.src.FishingGame.controller.FishSkillManager")
local FishExitGameLayer    = require("src.app.game.Fishing.src.FishingGame.layers.FishExitGameLayer")
local FishMusicSetLayer    = require("src.app.newHall.childLayer.SetLayer")
local FishSoundManager    = require("src.app.game.Fishing.src.FishingCommon.controller.FishSoundManager")
local FishResourceList    = require("src.app.game.Fishing.res.buyu_config.FishResourceList")
local FishingLoadResAsync = require("src.app.game.Fishing.src.FishingRoom.FishingLoadResAsync")
local FishBuff            = require("src.app.game.Fishing.src.FishingGame.model.FishBuff")
local FishModel            = require("src.app.game.Fishing.src.FishingGame.model.FishModel")
local FishIllustratedLayer = require("src.app.game.Fishing.src.FishingGame.layers.FishIllustratedLayer")
local FishChatSystemLayer = require("src.app.game.Fishing.src.FishingGame.layers.FishChatSystemLayer")
local FishPlayerInfoLayer = require("src.app.game.Fishing.src.FishingGame.layers.FishPlayerInfoLayer")
local FishGameDataController = require("src.app.game.Fishing.src.FishingCommon.controller.FishGameDataController")
local FishingUtil            = require("src.app.game.Fishing.src.FishingCommon.utils.FishingUtil")
local DlgAlert                = require("app.hall.base.ui.MessageBox")
local FishPromptLayer        = require("src.app.game.Fishing.src.FishingGame.layers.FishPromptLayer")

local FishLodingLayer        = require("src.app.game.Fishing.src.FishingGame.layers.FishLodingLayer")

local FishRes = require("app.game.Fishing.src.FishingGame.scene.FishRes")

local AliveKeyForFishList    = {}     -- 当前场景鱼表
local InScreenFishList        = {}     -- 屏幕内的鱼 
local FrameClick                = 0
local UpdateCollisionBoxIndex = 1
local AliveKeyForBulletList    = {}
local CollisionBoxList        = {}
local CollisionBulletBoxList    = {}
local CONST_LINE_INTERVAL = 60


local FishingScene = class("FishingScene", function()
    return CCGameSceneBase.new()
end)

FishingScene.prickMaxCount = 20  --毒刺的数量
FishingScene.prickDistance = 10  --毒刺半直径

function FishingScene:ctor(__gameTable)
    -- cc.Texture2D:setDefaultAlphaPixelFormat( cc.TEXTURE2_D_PIXEL_FORMAT_RGB_A8888 )
    FishGlobal.setGameState(FishGlobal.GameStateCtor)
    self.gameAtomTypeId = g_GameController:getGameAtomTypeId()
    self.roomType = FishGlobal.RoomType.System
    self.m_roomData = FishGameDataController:getInstance():getbuyuRoomSettingItemById(self.gameAtomTypeId)
    self.m_Acer = FishGlobal.noDropAcer
    if self.m_roomData then
        self.roomType = self.m_roomData.nRoomType
        self.m_Acer = self.m_roomData.nAcer
    end

    self.m_background            = nil
    self.m_fishManager            = nil
    self.m_fishExitGameLayer    = nil
    self.m_noticeLayer            = nil
    self.m_fishMusicSetLayer    = nil
    self.m_fishIllustratedLayer    = nil
    self.m_fishChatSystemLatyer    = nil
    self.m_fishPlayerInfoLayer    = nil
    self.m_fishShopLayer        = nil
    self.m_fishShopBuyConfirmLayer = nil
    self.m_fishTaskActivityLayer = nil
    self.m_fishPromptLayer        = nil
    self.m_fishExchangeGoldLayer = nil
    self.fishColorFrames = {}
    self.m_toxicSmogs    = {}     -- 超级炸弹
    self.m_electrics    = {}     -- 电球
    self.m_partBombs    = {}     -- 炸弹
    self.m_pricks        = {}     -- 刺
    self.m_fishBuffs    = {}     -- 用于存放所有buff
    self.m_leftBullets = {}     -- 左边屏幕的子弹
    self.m_rightBullets = {}     -- 右边屏幕的子弹
    self.m_leftFishList = {}     -- 左边屏幕的鱼
    self.m_rightFishList = {}     -- 右边屏幕的鱼
    self.m_nCoinRreshTime = 0     -- 金币刷新时间  
    self.m_lockTargets = {}     -- 锁定目标
    self.m_bIsInit    = false
    self.m_fActualLockFishTime = 0
    self:myInit(__gameTable)

    if FishGlobal.NoNetTest then
        local filename = ".\\troops\\" .. "lifeTime.lua"
        self.lifeTimeFile = io.open(filename, "w+")
        self.pathIndex = 0
        self.fishIndex = 0
    end
end

-- 场景初始化
function FishingScene:myInit(__gameTable)
    
    self.m_noticeLayer = NoticeLayer.new()
    self:addChild(self.m_noticeLayer, FishGlobal.NoticeLevel)
    --背景层
    self.m_background = BackgroundController.new(self.roomType)
    self:addChild(self.m_background, FishGlobal.BGLevel)
    local soundPath = FishGameDataController:getInstance():getSoundPath(self.m_background:getBackMusicEx())
    if soundPath then
        self:setMusicPath(soundPath)
        --self:onMusicStatusChanged()
    end
    --鱼儿管理器
    self.m_fishManager = FishManager.new(self.roomType)
    self.m_fishManager:createAllFishAnimation()

    self:registBackClickHandler(handler(self, self.onBackButtonClicked)) -- Android & Windows注册返回按钮
    addMsgCallBack(self, MSG_ENTER_FOREGROUND, handler(self, self.onEnterForeground)) -- 转前台
    addMsgCallBack(self, MSG_ENTER_BACKGROUND, handler(self, self.onEnterBackground)) -- 转后台
    --addMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT, handler(self, self.socketState))
    --addMsgCallBack(self, POPSCENE_ACK,handler(self,self.showEndGameTip))
    --addMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT, handler(self,self.onSocketEventMsgRecived))
    addMsgCallBack(self, UPDATE_GAME_RESOURCE, handler(self, self.onLodingCall))
    
    -- self:initFishScene()

    self.m_pLodingLayer = FishLodingLayer.new()
    self:addChild(self.m_pLodingLayer, 10000)
end

function FishingScene:onLodingCall()
    self:initFishScene()
    self.m_pLodingLayer:closeView();
    self.m_pLodingLayer = nil
    --self:removeChild(self.m_pLodingLayer)
    g_GameController:onSceneInitMsgCall();
end

function FishingScene:initFishScene()
    -- --FishingUtil:getInstance():startNetCheck( handler( self, self.showNetState))
    -- --print("清除缓存信息!")
    -- --self.m_roomData = __gameTable.roomData
    -- -- 加载资源
    -- -- if not FishingRoomController:getInstance():getLoadResByLoadView() then 
    -- -- 资源加载器   
    -- --self.loadResAsync = FishingLoadResAsync.new()
    -- -- 加载资源               
    -- --self:loadResources()                        
    -- -- end
    -- --EffectManager.init() 
    -- --FishingRoomController:getInstance():setNeedReconectGameServer( true )
    -- --FishingRoomController:getInstance():setInFishingGame(  true )
    -- --消息层
    -- self.m_noticeLayer = NoticeLayer.new()
    -- self:addChild(self.m_noticeLayer, FishGlobal.NoticeLevel)
    -- --背景层
    -- self.m_background = BackgroundController.new(self.roomType)
    -- self:addChild(self.m_background, FishGlobal.BGLevel)
    -- local soundPath = FishGameDataController:getInstance():getSoundPath(self.m_background:getBackMusicEx())
    -- if soundPath then
    --     self:setMusicPath(soundPath)
    --     --self:onMusicStatusChanged()
    -- end
    --self.m_background:setBG( 1 )
    -- --鱼儿管理器
    -- self.m_fishManager = FishManager.new(self.roomType)
    -- self.m_fishManager:createAllFishAnimation()

    -- 主ui
    self:initFishGameMainLayer()

    --self:initWaterEx()
    self:initWaterNewEx()
    -- 聊天界面
    --self:showChatSystem()
    -- 创建锁定标志
    self:createLockLineAndAim()

    -- self:registBackClickHandler(handler(self, self.onBackButtonClicked)) -- Android & Windows注册返回按钮
    -- addMsgCallBack(self, MSG_ENTER_FOREGROUND, handler(self, self.onEnterForeground)) -- 转前台
    -- addMsgCallBack(self, MSG_ENTER_BACKGROUND, handler(self, self.onEnterBackground)) -- 转后台
    -- --addMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT, handler(self, self.socketState))
    -- --addMsgCallBack(self, POPSCENE_ACK,handler(self,self.showEndGameTip))
    -- --addMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT, handler(self,self.onSocketEventMsgRecived))
    -- addMsgCallBack(self, UPDATE_GAME_RESOURCE, handler(self, self.onLodingCall))
   
    -- 帧事件回调
    self:addNodeEventListener(cc.NODE_ENTER_FRAME_EVENT, handler(self, self.update))
    self:scheduleUpdate()

    self.m_bIsInit = true
    --sendMsg("fish_msg_MSG_GAME_INIT")
    
end



-- 加载游戏界面所用的资源
function FishingScene:loadResources()
    print("-------------loadResources----------")
    local key = FishResourceList.TableName
    local resList = FishResourceList[key]
    for k, v in pairs(resList) do
        --print("-----",k,v)
        self.loadResAsync:load(v)
    end
end

-- 移除游戏界面所用的资源
function FishingScene:RemoveResources()
    print("FishingScene::RemoveResources()")
    local key = FishResourceList.TableName
    local resList = FishResourceList[key]
    for k, v in pairs(resList) do
        if self.loadResAsync then
            --print("-----",k,v)
            self.loadResAsync:remove(k, v)
        end
    end

    AudioManager:getInstance():stopAllSounds()
    AudioManager:getInstance():stopMusic()

    CacheManager:removeAllExamples();

    -- 释放动画
    for _, strPathName in pairs(FishRes.vecReleaseAnim) do
        --local strJsonName = string.format("%s%s/%s.ExportJson", Lhdz_Res.strAnimPath, strPathName, strPathName)
        ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo(strPathName)
    end
    -- 释放整图
    for _, strPathName in pairs(FishRes.vecReleasePlist) do
        display.removeSpriteFrames(strPathName[1], strPathName[2])
    end
    -- 释放背景图
    for _, strFileName in pairs(FishRes.vecReleaseImg) do
        display.removeImage(strFileName)
    end
    -- 释放音频
    for _, strFileName in pairs(FishRes.vecReleaseSound) do
        AudioManager.getInstance():unloadEffect(strFileName)
    end


end

-- 场景进入
function FishingScene:onEnter()
    print("------------FishingScene:onEnter begin------------")
    ToolKit:setGameFPS(1 / 60)
    -- 准备好之后请求游戏初始化信息，不然服务器不发送初始化信息
    g_GameController:notifyViewDataAlready()

    -- 请求任务完成的进度
    -- g_GameController:reqActiveTaskAward( {} )
    print("------------FishingScene:onEnter end------------")
end

-- 每帧更新的函数
-- @params dt( number ) 每帧时间间隔
function FishingScene:update(dt)

    -- 减少一些不必要的碰撞边框运算
    if next(AliveKeyForBulletList) ~= nil or next(self.m_fishBuffs) or FishGlobal.TestCollisionBox then
        FrameClick = FrameClick + 1
        if FrameClick > 3 then
            FrameClick = 0
            UpdateCollisionBoxIndex = UpdateCollisionBoxIndex + 1
            if UpdateCollisionBoxIndex > 10000000 then
                UpdateCollisionBoxIndex = 1
            end
        end
    end

    -----------------------------------------------------------------------
    self:fishMove(dt)
    --self:autoLoackMaxSCoreFish()
    self:updateBullet(dt)

    -- 子弹与鱼的碰撞
    self:collisionFishBulletEx()
    self:collisionLockBullet()

    -- local timeBegin2 = cc.net.SocketTCP.getTime()
    -- 超级炸弹
    self:toxicSmogFish()
    -- 电 
    self:electricFish()
    -- 局部炸弹
    self:partBombFish()
    -- 刺
    --self:prickFish()
    self:actualLockFish(dt)

    -- 刷新金币显示
    self.m_nCoinRreshTime = self.m_nCoinRreshTime + dt
    if self.m_nCoinRreshTime >= 1 then
        self.m_nCoinRreshTime = 0

        --     g_GameController:synPlayersCoin()
        -- if self.m_Acer ==  FishGlobal.isDropAcer then
        --   -- g_GameController:synPlayersGold()
        -- end
    end

    -- 测试碰撞边框
    if FishGlobal.TestCollisionBox then self:TestCollisionBox()    end
    -- 测试出鱼
    if FishGlobal.NoNetTest then self:TestOutFish(dt) end
    --if self.m_bIsInit == true then
    --    g_GameController:popNetMsg()
    --end
end

-- 初始化波浪 -- 暂时未使用
function FishingScene:initWater()
    self.waterSpr = display.newSprite("others/water1.png")
    self:addChild(self.waterSpr, FishGlobal.UILevel - 1)
    self.waterSpr:setScaleY(display.scaleY * 1.5)
    self.waterSpr:setScaleX(display.scaleX * 1.8)
    self.waterSpr:setAnchorPoint(cc.p(0.5, 0.5))
    self.waterSpr:setPosition(cc.p(display.cx, display.cy))
    local fadeOut = cc.FadeOut:create(3)
    local backOut = fadeOut:reverse()
    self.waterSpr:runAction(cc.RepeatForever:create(cc.Sequence:create(fadeOut, backOut, nil)))
end

-- 初始化波浪扩展
function FishingScene:initWaterEx()
    local waveNode = display.newNode()
    waveNode:setPosition(cc.p(0, 0))
    waveNode:setScaleX(display.scaleX)
    waveNode:setScaleY(display.scaleY)
    self:addChild(waveNode, FishGlobal.LogicLevel + 100)
    local width = 256
    local height = 0
    for i = 1, 5 do
        local waveSpr = display.newSprite()
        local _params = {
            name = "wave",
            node = waveSpr,
            x    = 128 + width * (i - 1),
            y    = height,
        }
        EffectManager.playAnimation(_params)
        waveSpr:setBlendFunc(gl.ONE, gl.ONE)
        waveSpr:setAnchorPoint(cc.p(0.5, 0))
        waveSpr:setPosition(cc.p(_params.x, _params.y))
        waveSpr:setScaleX(1.0001)
        waveNode:addChild(waveSpr)
    end
end

function FishingScene:initWaterNewEx()
    local waveNode = display.newNode()
    waveNode:setPosition(cc.p(display.width / 2, display.height / 2))
    waveNode:setScaleX(display.scaleX)
    waveNode:setScaleY(display.scaleY)
    self:addChild(waveNode, FishGlobal.LogicLevel + 100)
    local width = 256
    local height = 256
    for i = 1, 3 do
        local y = (2 - i) * height
        for j = 1, 7 do
            local x = (j - 4) * width
            local waveSpr = display.newSprite()
            local _params = {
                name = "fishWater",
                node = waveSpr,
            }
            EffectManager.playAnimation(_params)
            waveSpr:setBlendFunc(gl.ONE, gl.ONE)
            waveSpr:setOpacity(100)
            waveSpr:setAnchorPoint(cc.p(0.5, 0.5))
            waveSpr:setPosition(cc.p(x, y))
            waveNode:addChild(waveSpr)
            waveSpr:setScale(1.0012)
            table.insert(EffectManager.needBlendSprs, waveSpr)
        end
    end
end

-- 初始化捕鱼主界面
function FishingScene:initFishGameMainLayer()
    self.fishMainLayer = FishMainLayer.new(self.roomType, self.m_Acer)
    self:addChild(self.fishMainLayer, FishGlobal.UILevel)
end

-- 显示游戏退出界面
function FishingScene:showFishExitGameLayer()
    FishSoundManager:getInstance():playEffect(FishSoundManager.WindowsOpen)
    self:unscheduleUpdate()
    g_GameController:reqExitGame()
    g_GameController:releaseInstance()
end

-- 显示游戏退出界面扩展函数
-- @params  params( string )  游戏退出相关参数
function FishingScene:showFishExitGameLayerEx(params)
    FishSoundManager:getInstance():playEffect(FishSoundManager.WindowsOpen)
    if tolua.isnull(self.m_fishExitGameLayer) then
        self.m_fishExitGameLayer = FishExitGameLayer.new(self)
        self:addChild(self.m_fishExitGameLayer, FishGlobal.UILevel + 11)
    else
        self.m_fishExitGameLayer:setVisible(true)
    end
    if params.bunko == 1 then
        self.m_fishExitGameLayer:rerfeshExChnageInfoWin(params)
    elseif params.bunko == 0 then
        self.m_fishExitGameLayer:rerfeshExChnageInfoLose(params)
    end
end

-- 显示游戏设置界面
function FishingScene:showMusicSetLayer()
    FishSoundManager:getInstance():playEffect(FishSoundManager.WindowsOpen)
    if tolua.isnull(self.m_fishMusicSetLayer) then
        self.m_fishMusicSetLayer = FishMusicSetLayer.new(self)
        self:addChild(self.m_fishMusicSetLayer, FishGlobal.UILevel + 11)
    else
        self.m_fishMusicSetLayer:setVisible(true)
    end
end

-- 显示鱼种界面
function FishingScene:showFishIllustrateLayer()
    FishSoundManager:getInstance():playEffect(FishSoundManager.WindowsOpen)
    if tolua.isnull(self.m_fishIllustratedLayer) then
        self.m_fishIllustratedLayer = FishIllustratedLayer.new(self.roomType)
        self:addChild(self.m_fishIllustratedLayer, FishGlobal.UILevel + 11)
    else
        self.m_fishIllustratedLayer:setVisible(true)
    end
end

-- 显示聊天界面
function FishingScene:showChatSystem()
    --FishSoundManager:getInstance():playEffect( FishSoundManager.WindowsOpen )
    if tolua.isnull(self.m_fishChatSystemLatyer) then
        self.m_fishChatSystemLatyer = FishChatSystemLayer.new()
        self:addChild(self.m_fishChatSystemLatyer, FishGlobal.UILevel + 11)
    else
        self.m_fishChatSystemLatyer:setVisible(true)
    end
end

-- 显示玩家详细信息界面
function FishingScene:showPlayerInfoLayer()
    FishSoundManager:getInstance():playEffect(FishSoundManager.WindowsOpen)
    if tolua.isnull(self.m_fishPlayerInfoLayer) then
        self.m_fishPlayerInfoLayer = FishPlayerInfoLayer.new(self.roomType)
        self:addChild(self.m_fishPlayerInfoLayer, FishGlobal.UILevel + 11)
    else
        self.m_fishPlayerInfoLayer:setVisible(true)
    end
end

-- 显示商城界面
function FishingScene:showShopLayer()
    FishSoundManager:getInstance():playEffect(FishSoundManager.ShopOpen)
    if tolua.isnull(self.m_fishShopLayer) then
        self.m_fishShopLayer = FishShopLayer.new()
        self:addChild(self.m_fishShopLayer, FishGlobal.UILevel + 11)
    else
        self.m_fishShopLayer:setVisible(true)
    end
end

-- 商城中物品位置定位
-- @params itemId( number ) 物品id
function FishingScene:shopJumpToPercentHorizontal(itemId)
    if not tolua.isnull(self.m_fishShopLayer) then
        self.m_fishShopLayer:jumpToPercentHorizontal(itemId)
    end
end

-- 显示商品购买确认界面
function FishingScene:showShopBuyConfirmLayer(__type, __index)
    if tolua.isnull(self.m_fishShopBuyConfirmLayer) then
        self.m_fishShopBuyConfirmLayer = FishShopBuyConfirmLayer.new()
        self:addChild(self.m_fishShopBuyConfirmLayer, FishGlobal.UILevel + 11)
    end
    self.m_fishShopBuyConfirmLayer:setBuyType(__type)
    self.m_fishShopBuyConfirmLayer:refreashView(__index)
    self.m_fishShopBuyConfirmLayer:setVisible(true)
end

-- 显示活动任务奖励
function FishingScene:showTaskActiveAwardsLayer()
    FishSoundManager:getInstance():playEffect(FishSoundManager.WindowsOpen)
    if tolua.isnull(self.m_fishTaskActivityLayer) then
        self.m_fishTaskActivityLayer = FishTaskActivityLayer.new()
        self:addChild(self.m_fishTaskActivityLayer, FishGlobal.UILevel + 11)
    else
        self.m_fishTaskActivityLayer:setVisible(true)
    end
end

-- 显示提示界面
-- @params params( table ) 提示界面信息参数
function FishingScene:showFishPromptLayer(params)
    FishSoundManager:getInstance():playEffect(FishSoundManager.WindowsOpen)
    if tolua.isnull(self.m_fishPromptLayer) then
        self.m_fishPromptLayer = FishPromptLayer.new()
        self.m_fishPromptLayer:setVisible(false)
        self:addChild(self.m_fishPromptLayer, FishGlobal.UILevel + 12)
    end
    self.m_fishPromptLayer:rerfeshView(params)
end

-- 显示元宝结算界面
-- @prams __info( string ) 元宝结算数据
function FishingScene:showFishExchangeGoldLayer(__info)
    FishSoundManager:getInstance():playEffect(FishSoundManager.WindowsOpen)
    if tolua.isnull(self.m_fishExchangeGoldLayer) then
        self.m_fishExchangeGoldLayer = FishExchangeGoldLayer.new()
        self:addChild(self.m_fishExchangeGoldLayer, FishGlobal.UILevel + 11)
    else
        self.m_fishExchangeGoldLayer:setVisible(true)
    end
    self.m_fishExchangeGoldLayer:refreshView(__info)
end

-- 炮台升级提示
-- @params  taskId( number ) 任务id
function FishingScene:showPowerUpLevelTips(taskId)
    local taskData = FishGameDataController:getInstance():getTakeDataByTaskId(taskId)
    if not taskData then
        return
    end
    local rootNode = display.newNode()
    rootNode:setScale(0.05)
    rootNode:setPosition(cc.p(display.width / 2, display.height / 2 + 50))
    self:addChild(rootNode, FishGlobal.UILevel)
    local upLevlBgImg = ccui.ImageView:create("fish_bg_ptsj.png", ccui.TextureResType.plistType)
    upLevlBgImg:setAnchorPoint(cc.p(0.5, 0.5))
    upLevlBgImg:setPosition(cc.p(0, 0))
    upLevlBgImg:setScale9Enabled(true)
    upLevlBgImg:setCapInsets(cc.rect(220, 1, 100, 80))
    upLevlBgImg:ignoreContentAdaptWithSize(true)
    upLevlBgImg:setContentSize(cc.size(671, 200))
    upLevlBgImg:setCascadeOpacityEnabled(true)
    upLevlBgImg:setOpacity(130)
    rootNode:addChild(upLevlBgImg)

    local tipsSpr = display.newSprite("#fish_txt_ptsj.png")
    tipsSpr:setAnchorPoint(cc.p(0.5, 0.5))
    tipsSpr:setPosition(cc.p(0, 35))
    rootNode:addChild(tipsSpr, FishGlobal.UILevel)


    local width = 0
    local height = 0
    local itemRootNode = display.newNode()
    rootNode:addChild(itemRootNode)
    if taskData.nCoin > 0 then
        local params = { goodPng = "#public_img_jinbi_01.png", count = taskData.nCoin }
        local itemNode, totalWith = self:createGoodItem(params)
        itemNode:setPosition(cc.p(width, 0))
        itemRootNode:addChild(itemNode)
        width = width + totalWith
    end

    if taskData.nDiamon > 0 then
        local params = { goodPng = "#public_img_diamond.png", count = taskData.nDiamon }
        local itemNode, totalWith = self:createGoodItem(params)
        itemNode:setPosition(cc.p(width, 0))
        itemRootNode:addChild(itemNode)
        width = width + totalWith
    end

    if next(taskData.tItem) then
        for i, v in ipairs(taskData.tItem) do
            local goodItemData = FishGameDataController:getInstance():getItemByIndex(taskData.tItem[i])
            local params = { goodPng = "#" .. goodItemData.itemIcon, count = taskData.tItemCount[i] }
            local itemNode, totalWith = self:createGoodItem(params)
            itemNode:setPosition(cc.p(width, 0))
            itemRootNode:addChild(itemNode)
            width = width + totalWith
        end
    end
    itemRootNode:setAnchorPoint(0, 0.5)
    itemRootNode:setPosition(cc.p(-width / 2, -35))

    local scaleTo = cc.ScaleTo:create(0.5, 1)
    local function remveTipsSpr()
        rootNode:stopAllActions()
        rootNode:removeFromParent()
        -- if self.roomType == FishGlobal.RoomType.System then
        --    if not FishingUtil:getInstance():checkIsSelect( FishPromptLayer.TipTypeKey.powerLvUp ) then
        --       self:showGOToHighScoreRoomDailog(taskData.nLevel )
        --    end
        -- end
        -- self:showGOToHighScoreRoomTips( taskData.nLevel ,"您现在可前往更高级房间进行游戏哦!")
    end
    local callFunc = cc.CallFunc:create(remveTipsSpr)
    rootNode:runAction(cc.Sequence:create(scaleTo, cc.DelayTime:create(3), callFunc, nil))

    self.fishMainLayer:spreadUpradPower()
end

--
function FishingScene:createGoodItem(params)
    local itemNode = display.newNode()
    itemNode:setAnchorPoint(0, 0.5)

    local goodSpr = display.newSprite(params.goodPng)
    goodSpr:setAnchorPoint(0, 0.5)
    goodSpr:setPosition(cc.p(0, 0))
    goodSpr:setScale(0.7)
    local goodSize = goodSpr:getContentSize()
    itemNode:addChild(goodSpr)

    local countAtlas = ccui.TextAtlas:create("0", "buyu_number/fish_num_yellow.png", 33, 38, ".")
    countAtlas:setAnchorPoint(cc.p(0, 0.5))
    countAtlas:setPosition(cc.p(goodSize.width - 35, 0))
    countAtlas:setString(";" .. params.count)
    itemNode:addChild(countAtlas)

    local countAtlasSize = countAtlas:getContentSize()

    local totalWith = goodSize.width + countAtlasSize.width - 20

    return itemNode, totalWith
end


-- 获取鱼表
-- @param AliveKeyForFishList( table ) 鱼表
function FishingScene:getFishList()
    return AliveKeyForFishList
end

--获取鱼
function FishingScene:getFishByFishIndex(fishIndex)
    return AliveKeyForFishList[fishIndex]
end

-- 玩家刚进来，场景鱼初始化
-- @params __info( table ) 鱼的数据
function FishingScene:initSceneFishMsg(__info)
    -- self.m_noticeLayer:playMoveUpInfo("初始化场景鱼")
    if FishGlobal.NoNetTest then return end
    local start_time = os.clock();
    local time = __info.nTime
    for k, v in pairs(__info.m_vecFishData) do
        local fish = nil
        v.m_nClockTime = __info.m_nClockTime;
        if v.m_nArrayID == 0 then
            fish = self.m_fishManager:createLineFish(self, v, time)
        else
            fish = self.m_fishManager:createTroopFish(v.m_nArrayID, v, time, self)
        end
        if fish then
            -- print("fish",v.m_nIndex)
            if tolua.isnull(AliveKeyForFishList[v.m_nIndex]) then

                self:addChild(fish, FishGlobal.LogicLevel)
                AliveKeyForFishList[v.m_nIndex] = fish
            else
                print("鱼的索引相同!")
            end
        else
            --TOAST(string.format("create fish%d failed!",v.m_nType))
        end
    end
    print("初始化场景的鱼---",os.clock() - start_time, "\n");
end


-- 出鱼
-- @params __info( table ) 鱼的数据
function FishingScene:outFishs(__info)
    if FishGlobal.NoNetTest then return end
    local time = 0
    for k, v in pairs(__info.m_vecNewFish) do
        local fish = nil
        v.m_nClockTime = __info.m_nClockTime;
        if v.m_nArrayID == 0 then
            fish = self.m_fishManager:createLineFish(self, v, time)
        else
            fish = self.m_fishManager:createTroopFish(v.m_nArrayID, v, time, self)
        end
        if fish then
            if tolua.isnull(AliveKeyForFishList[v.m_nIndex]) then
                self:addChild(fish, FishGlobal.LogicLevel)
                AliveKeyForFishList[v.m_nIndex] = fish
            else
                print("鱼的索引相同!")
            end
        else
            --TOAST(string.format("create fish%d failed!",v.m_nType))
        end
    end
end



-- 抓到鱼
-- @params __info( table ) 被抓到鱼的数据，及金币
function FishingScene:killFishMsg(__info)
    --self.m_noticeLayer:playMoveUpInfo("收到中鱼消息")
    if not tolua.isnull(AliveKeyForFishList[__info.m_nIndex]) then
        local struggle = AliveKeyForFishList[__info.m_nIndex]
        struggle:setCatch(true)
        AliveKeyForFishList[__info.m_nIndex] = nil
        InScreenFishList[__info.m_nIndex]    = nil
        self.m_leftFishList[__info.m_nIndex] = nil
        self.m_rightFishList[__info.m_nIndex] = nil
        struggle:hideChestDialog()
        local _callFunc = cc.CallFunc:create(handler(self, self.kilFishEffect), { info = __info })
        local animation = cc.AnimationCache:getInstance():getAnimation(string.format("%s_death", struggle:getFishName()))
        if not animation then
            EffectManager.loadFishResourceByName(struggle:getFishName())
            self.m_fishManager:createFishAnimationByName(struggle:getFishName())
            animation = cc.AnimationCache:getInstance():getAnimation(string.format("%s_death", struggle:getFishName()))
        end
        if animation then
            struggle:stopAllActions()
            struggle:runStruggleAction(animation, _callFunc)
            --struggle:setBlendFunc(gl.ONE_MINUS_SRC_ALPHA, gl.ONE)
            struggle:setGLProgramState(cc.GLProgramState:getOrCreateWithGLProgramName("ShaderPositionTextureColor_noMVP"))
        end

        self:unLockByStruggleFish(struggle)
        if __info.m_nEffectType > 0 then
            local __pos = cc.p(struggle:getPositionX(), struggle:getPositionY())
            local __fishType = 0
            local buffData = FishGameDataController:getInstance():getBuffDataItemByIndex(struggle:getFishType())
            if buffData then
                __fishType = buffData.nRelationFish
            end
            local buffParams = {
                accountID = __info.m_nAccountID,
                buffIndex = __info.m_nEffectIndex,
                buffType = __info.m_nEffectType,
                fishIndex = __info.m_nIndex,
                pos    = __pos,
                coin    = __info.m_nCoin * 0.01,
                addCoin = __info.m_nAddCoin * 0.01,
                --randScore = __info.m_nRandScore,
                isVisible = false,
                fishType = __fishType,
            }
            self:playFishBuff(buffParams)
        end
        local fishPlayer = g_GameController:getPlayerInfoByAccountId(__info.m_nAccountID)
        local _viewChair = 0
        local _chair = 0
        if fishPlayer then
            _viewChair = fishPlayer:getViewChair()
            _chair    = fishPlayer:getChair()
        end

        local cannonPos = self.fishMainLayer:getCannonPos(_viewChair)
        local fishType = struggle:getFishType()
        local fishItem = FishGameDataController:getInstance():getFishDataByType(fishType)

        -- if struggle:getFingerling() == FishModel.FINGERLING.BOSS_FISH_TYPE or struggle:getFingerling() == FishModel.FINGERLING.YUANBAO_FISH_TYPE then
        --    struggle:setPosition( cc.p( display.width/2,display.height/2 ) )
        --    struggle:setLocalZOrder( FishGlobal.LogicLevel + 1 )
        -- end
        if struggle:getFingerling() == FishModel.FINGERLING.YUANBAO_FISH_TYPE then
            struggle:setScale(4 * display.standardScale)
            --struggle:setBlendFunc(gl.ONE, gl.ONE) 
            self.m_background:setStrongJumps()
        end
        if __info.m_nAccountID == Player.getAccountID() then
            -- 播放鱼被击中的音效
            if struggle:getFingerling() == FishModel.FINGERLING.COMMON_FISH_TYPE then
                if self:randomOK(15, 100) then
                    FishSoundManager:getInstance():playEffect(struggle:getDeathSound())
                end
            elseif struggle:getFingerling() == FishModel.FINGERLING.REWARD_FISH_TYPE then
                if self:randomOK(50, 100) then
                    FishSoundManager:getInstance():playEffect(struggle:getDeathSound())
                end
            else
                FishSoundManager:getInstance():playEffect(struggle:getDeathSound())
            end
        end

        -- 金币效果
        if fishItem.coinCount > 0 then
            local coinEffectName = "coin"
            if _viewChair == FishGlobal.myViewChair then
                coinEffectName = "coin"
            end
            local params = {
                prarentNode = self,
                accountID = __info.m_nAccountID,    
                viewChair = _viewChair,
                chair    = _chair,
                name    = coinEffectName,
                count    = fishItem.coinCount, -- 金币个数
                coin    = __info.m_nCoin * 0.01, -- 金币数量
                x        = struggle:getPositionX(),
                y        = struggle:getPositionY(),
                toX        = cannonPos.x,
                toY        = cannonPos.y,
                addCoin    = __info.m_nAddCoin * 0.01,
                m_nFishScore = struggle:getFishScore() * 100,
            }
            EffectManager.playCoinEffect(params)

            --EffectManager.coineExistEverywhere( params )
        end



        --  物品掉落
        if #__info.m_vecItem > 0 then
            local isShow = true
            if struggle:getFingerling() == FishModel.FINGERLING.YUANBAO_FISH_TYPE then
                isShow = false
            end
            local _pos = cc.p(cannonPos.x, cannonPos.y + 180)
            if _viewChair == 4 or _viewChair == 5 or _viewChair == 6 then
                _pos = cc.p(cannonPos.x, cannonPos.y - 180)
            end
            local itemParams = {
                prarentNode = self,
                m_vecItem = __info.m_vecItem,
                pos = cc.p(struggle:getPositionX(), struggle:getPositionY()),
                toPos = cannonPos,
                chair = _chair,
                --addGold = __info.m_nAcer,
                isShow = isShow,
                redCoin = __info.m_nRedCoin,
                redPos = _pos,
                m_nFishScore = struggle:getFishScore() * 100,
            }
            EffectManager.itemDropOut(itemParams)
            if g_GameController:getClubId() == 0 then
                if self.roomType == FishGlobal.RoomType.System then
                    if __info.m_nAccountID == Player.getAccountID() then
                        if not FishingUtil:getInstance():checkIsSelectEx(FishPromptLayer.TipTypeKey.huafei) then
                            self:showHuafeiDailog(__info.m_vecItem)
                        end

                        if not FishingUtil:getInstance():checkIsSelectEx(FishPromptLayer.TipTypeKey.redPacket) then
                            self:showRedPacketDailog(__info.m_vecItem)
                        end

                        if self.m_Acer == FishGlobal.isDropAcer then
                            if not FishingUtil:getInstance():checkIsSelectEx(FishPromptLayer.TipTypeKey.yunbao) then
                                self:showYunBaoDailog(__info.m_vecItem)
                            end
                        end
                    end
                end
            end
        end

        if __info.m_nEffectType ~= FishBuff.BUFFTYPE.WHEEL_TYPE then    -- 除转盘鱼外，都显示金币数字提示
            -- 数字提示
            if __info.m_nAddCoin > 0 then
                local isOwer = false
                if _viewChair == FishGlobal.myViewChair then
                    isOwer = true
                end
                local params_1 = {
                _isOwer = isOwer,
                x    = struggle:getPositionX(),
                y    = struggle:getPositionY(),
                count    = __info.m_nAddCoin * 0.01,
                ctriTimes = __info.m_nTimes,
                scoreSound = struggle:getScoreSound()
                }
                local text = EffectManager.createMoveUpNumber(params_1)
                if text then
                    self:addChild(text, FishGlobal.LogicLevel)
                end
            end

        end
        if fishItem.nBoomType == 1 then
            local params = {
                prarentNode = self,
                x        = struggle:getPositionX(),
                y        = struggle:getPositionY(),
                scoreSound = struggle:getScoreSound(),
            }
            EffectManager.coineExistEverywhere(params)
        elseif fishItem.nBoomType == 2 then  -- 组队鱼
            --self.m_background:setJump()
            local fishGlowSpr = display.newSprite()
            fishGlowSpr:setAnchorPoint(cc.p(0.5, 0.5))
            fishGlowSpr:setScale(2.5)
            fishGlowSpr:setPosition(cc.p(struggle:getPosition()))
            self:addChild(fishGlowSpr, FishGlobal.LogicLevel + 2)
            FishSoundManager:getInstance():playEffect(FishSoundManager.EffectBoom)
            EffectManager.playAnimation({ node = fishGlowSpr, name = "fishbombteam" })
            -- fishGlowSpr:setScale( 5 )
        elseif fishItem.nBoomType == 3 then  --超级炸弹
            self.m_background:setJump()
            local fishGlowSpr = display.newSprite()
            fishGlowSpr:setScale(3)
            fishGlowSpr:setAnchorPoint(cc.p(0.5, 0.5))
            fishGlowSpr:setPosition(cc.p(struggle:getPosition()))
            self:addChild(fishGlowSpr, FishGlobal.LogicLevel + 2)
            FishSoundManager:getInstance():playEffect(FishSoundManager.EffectBoom)
            EffectManager.playAnimation({ node = fishGlowSpr, name = "fishbomb" })
        elseif fishItem.nBoomType == 4 then  --局部炸弹
            local fishGlowSpr = display.newSprite()
            fishGlowSpr:setAnchorPoint(cc.p(0.5, 0.5))
            fishGlowSpr:setScale(2)
            fishGlowSpr:setPosition(cc.p(struggle:getPosition()))
            self:addChild(fishGlowSpr, FishGlobal.LogicLevel + 2)
            FishSoundManager:getInstance():playEffect(FishSoundManager.EffectBoom)
            EffectManager.playAnimation({ node = fishGlowSpr, name = "fishbomb" })
        elseif fishItem.nBoomType == 5 then  --boss
            self.m_background:setStrongJumps()
            local params = {
                prarentNode = self,
                x        = struggle:getPositionX(),
                y        = struggle:getPositionY(),
                scoreSound = struggle:getScoreSound(),
            }
            EffectManager.coineExistEverywhere(params)
        end
    end
end




-- 捕到鱼效果
-- @params struggle( table )  鱼
-- @params __params( table )  捕到鱼数据信息
function FishingScene:kilFishEffect(struggle, __params)
   if self.m_bIsInit ~= true then return end
    local __info = __params.info
    local fishPlayer = g_GameController:getPlayerInfoByAccountId(__info.m_nAccountID)
    local _viewChair = 0
    local _chair = 0
    if fishPlayer then
        _viewChair = fishPlayer:getViewChair()
        _chair = fishPlayer:getChair()
    end
    local cannonPos = self.fishMainLayer:getCannonPos(_viewChair)
    local fishType = struggle:getFishType()
    local fishItem = FishGameDataController:getInstance():getFishDataByType(fishType)


    if fishItem.scoreBar == 1 then
        if __info.m_nAddCoin > 0 then
            local _pos = cc.p(cannonPos.x, cannonPos.y + 180)
            if _viewChair == 4 or _viewChair == 5 or _viewChair == 6 then
                _pos = cc.p(cannonPos.x, cannonPos.y - 180)
            end
            local spriteFrameName = "#" .. fishItem.scoreFishImage
            local _params = {
                prarentNode = self,
                pos        = _pos,
                fishPngName = spriteFrameName,
                count = __info.m_nAddCoin * 0.01,
                isPhonefare = false,
            }
            EffectManager.playTurnDisc(_params)
        end
    elseif fishItem.scoreBar == 2 then
        if #__info.m_vecItem >= 0 then
            local _pos = cc.p(cannonPos.x, cannonPos.y + 180)
            if _viewChair == 4 or _viewChair == 5 or _viewChair == 6 then
                _pos = cc.p(cannonPos.x, cannonPos.y - 180)
            end
            local spriteFrameName = "#" .. fishItem.scoreFishImage
            local _count = 0
            for k, v in pairs(__info.m_vecItem) do
                local itemData = FishGameDataController:getInstance():getItemByIndex(v.m_nItemID)
                if itemData then
                    if itemData.nType == 6 then
                        _count = v.m_nCount
                        break
                    end
                end
            end
            local _params = {
                prarentNode = self,
                pos        = _pos,
                fishPngName = spriteFrameName,
                count = _count,
                isPhonefare = false,
            }
            EffectManager.playTurnDisc(_params)
        end
    end


    struggle:stopAllActions()
    struggle:removeFromParent()

    -- -- 释放buff
    -- if __info.m_nEffectType > 0 then  
    --     local __pos = cc.p( struggle:getPositionX() , struggle:getPositionY() )
    --     local buffParams = {  
    --                          accountID = __info.m_nAccountID,
    --                          buffIndex = __info.m_nEffectIndex,    
    --                          buffType  = __info.m_nEffectType,
    --                          fishIndex = __info.m_nIndex,
    --                          pos       = __pos,
    --                          coin      = __info.m_nCoin,
    --                          addCoin   = __info.m_nAddCoin,
    --                          randScore = __info.m_nRandScore,
    --                       }
    --      self:playFishBuffEx( buffParams )
    -- end
end

-- buff 杀鱼获的总金和转圈面板显示
function FishingScene:showBuffTurnDisc(__info)
    --print("FishingScene:showBuffTurnDisc( __info )")
    --dump( __info )
    local fishBuff = self.m_fishBuffs[__info.m_nEffectIndex]
    if not tolua.isnull(fishBuff) then
        local fishPlayer = g_GameController:getPlayerInfoByAccountId(__info.m_nAccountID)
        local _viewChair = 0
        if fishPlayer then
            _viewChair = fishPlayer:getViewChair()
        end
        local cannonPos = self.fishMainLayer:getCannonPos(_viewChair)
        local _pos = cc.p(cannonPos.x, cannonPos.y + 180)
        if _viewChair == 4 or _viewChair == 5 or _viewChair == 6 then
            _pos = cc.p(cannonPos.x, cannonPos.y - 180)
        end
        local _params = {
            prarentNode = self,
            pos        = _pos,
            count = __info.m_totalCoin * 0.01,
        }
        EffectManager.playTurnDisc(_params)

        if __info.m_totalCoin > 0 then
            local isOwer = false
            if _viewChair == FishGlobal.myViewChair then
                isOwer = true
            end
            local params_1 = {
            _isOwer = isOwer,
            x    = fishBuff:getPositionX(),
            y    = fishBuff:getPositionY(),
            count    = __info.m_totalCoin * 0.01,
            ctriTimes = 1,
            }
            local text = EffectManager.createMoveUpNumber(params_1)
            if text then
                self:addChild(text, FishGlobal.LogicLevel)
            end
        end
        if __info.m_tatalCount > 1 and fishBuff:getBuffType() == FishBuff.BUFFTYPE.ELECTRIC_TYPE then
            FishSoundManager:getInstance():playEffect(FishSoundManager.EffectElectric)
        end
    end
end

-- buff 杀鱼
-- @params  __info( table ) buff 相关数据
function FishingScene:buffKillFishMsg(__info)
    --self.m_noticeLayer:playMoveUpInfo("收到中鱼消息")
    if not tolua.isnull(AliveKeyForFishList[__info.m_nIndex]) then
        local fish = AliveKeyForFishList[__info.m_nIndex]
        fish:setCatch(true)
        if __info.m_nAccountID == Player.getAccountID() then
            -- 播放鱼被击中的音效
            if fish:getFingerling() == FishModel.FINGERLING.COMMON_FISH_TYPE then
                if self:randomOK(5, 100) then
                    FishSoundManager:getInstance():playEffect(fish:getDeathSound())
                end
            elseif fish:getFingerling() == FishModel.FINGERLING.REWARD_FISH_TYPE then
                if self:randomOK(10, 100) then
                    FishSoundManager:getInstance():playEffect(fish:getDeathSound())
                end
            else
                FishSoundManager:getInstance():playEffect(fish:getDeathSound())
            end
        end
        local function kilFishEffect()
            fish:stopAllActions()
            fish:removeFromParent()
        end
        local _callFunc = cc.CallFunc:create(kilFishEffect)
        local animation = cc.AnimationCache:getInstance():getAnimation(string.format("%s_death", fish:getFishName()))

        if not animation then
            EffectManager.loadFishResourceByName(fish:getFishName())
            self.m_fishManager:createFishAnimationByName(fish:getFishName())
            animation = cc.AnimationCache:getInstance():getAnimation(string.format("%s_death", fish:getFishName()))
        end

        if animation then
            fish:stopAllActions()
            fish:runAction(cc.Sequence:create(cc.Animate:create(animation), cc.DelayTime:create(0.5), _callFunc, nil))
        end

        --金币效果
        local fishPlayer = g_GameController:getPlayerInfoByAccountId(__info.m_nAccountID)
        local _viewChair = 0
        local _chair = 0
        if fishPlayer then
            _viewChair = fishPlayer:getViewChair()
            _chair    = fishPlayer:getChair()
        end
        local fishType = fish:getFishType()
        local fishItem = FishGameDataController:getInstance():getFishDataByType(fishType)
        local fishBuff = self.m_fishBuffs[__info.m_nEffectIndex]

        if not tolua.isnull(fishBuff) then
            if fishBuff:getBuffType() == FishBuff.BUFFTYPE.SUPER_BOMB_TYPE or fishBuff:getBuffType() == FishBuff.BUFFTYPE.PART_BOMB_TYPE then         -- 毒
                -- 金币效果
                if fishItem.coinCount > 0 then
                    local coinEffectName = "coin"
                    local isOwer = false
                    if _viewChair == FishGlobal.myViewChair then
                        isOwer = true
                    end
                    local cannonPos = self.fishMainLayer:getCannonPos(_viewChair)
                    local params = {
                        prarentNode = self,
                        accountID = __info.m_nAccountID,
                        viewChair = _viewChair,
                        chair    = _chair,
                        name    = coinEffectName,
                        count    = fishItem.coinCount, -- 金币个数
                        coin    = __info.m_nCoin * 0.01, -- 金币数量
                        x        = fish:getPositionX(),
                        y        = fish:getPositionY(),
                        toX        = cannonPos.x,
                        toY        = cannonPos.y,
                        addCoin    = __info.m_nAddCoin * 0.01,
                        m_nFishScore = fish:getFishScore() * 100,
                    }
                    EffectManager.playCoinEffect(params)
                end
            elseif fishBuff:getBuffType() == FishBuff.BUFFTYPE.ELECTRIC_TYPE then
                -- 金币效果
                if fishItem.coinCount > 0 then
                    local coinEffectName = "coin"
                    local isOwer = false
                    if _viewChair == FishGlobal.myViewChair then
                        isOwer = true
                    end
                    local cannonPos = self.fishMainLayer:getCannonPos(_viewChair)
                    local params = {
                        prarentNode = self,
                        accountID = __info.m_nAccountID,
                        viewChair = _viewChair,
                        chair    = _chair,
                        name    = coinEffectName,
                        count    = fishItem.coinCount, -- 金币个数
                        coin    = __info.m_nCoin * 0.01, -- 金币数量
                        x        = fish:getPositionX(),
                        y        = fish:getPositionY(),
                        toX        = cannonPos.x,
                        toY        = cannonPos.y,
                        addCoin    = __info.m_nAddCoin * 0.01,
                        m_nFishScore = fish:getFishScore() * 100,
                    }
                    EffectManager.playCoinEffect(params)
                end
                --if __info.m_nAccountID ~= Player.getAccountID() then
                local electri = self.m_electrics[__info.m_nEffectIndex]
                fish:setElectri(electri:getBuffIndex(), true)
                local electriX, electriY = electri:getPosition()
                local fishX, fishY = fish:getPosition()
                local temp_y = fishY - electriY
                local temp_x = fishX - electriX
                local angle = math.atan2(temp_y, temp_x) / math.pi * 180
                local distance = math.sqrt(math.pow((temp_y), 2) + math.pow((temp_x), 2))
                local scaleY = distance / 230

                local light = display.newSprite()
                light:setScaleY(scaleY)
                light:setRotation(90 - angle)
                light:setPosition(cc.p(electriX, electriY))
                light:setAnchorPoint(cc.p(0.5, 0))
                self:addChild(light, FishGlobal.LogicLevel + 2)
                local _params = {
                    name = "lightning",
                    node = light,
                    x    = 0,
                    y    = 0,
                }
                EffectManager.playAnimation(_params)
                --light:setBlendFunc(gl.ONE, gl.ONE) 
                local function removelight()
                    light:stopAllActions()
                    light:removeFromParent()
                end
                local callfunc2 = cc.CallFunc:create(removelight)
                light:runAction(cc.Sequence:create(cc.DelayTime:create(0.6), callfunc2, nil))
                --end
            end
        end
        -- 数字提示
        if __info.m_nAddCoin > 0 then
            local isOwer = false
            if _viewChair == FishGlobal.myViewChair then
                isOwer = true
            end
            local params_1 = {
            _isOwer = isOwer,
            x    = fish:getPositionX(),
            y    = fish:getPositionY(),
            count    = __info.m_nAddCoin * 0.01,
            ctriTimes = 0,
            }
            local text = EffectManager.createMoveUpNumber(params_1)
            if text then
                self:addChild(text, FishGlobal.LogicLevel)
            end
        end
        AliveKeyForFishList[__info.m_nIndex] = nil
        InScreenFishList[__info.m_nIndex] = nil
        self.m_leftFishList[__info.m_nIndex] = nil
        self.m_rightFishList[__info.m_nIndex] = nil
    end
end


--查询客户端鱼的数量
function FishingScene:getFishCount()
    --self.m_noticeLayer:playMoveUpInfo("消息返回:CS_C2G_BuYu_Report_Nty")
    local count = 0
    for k, v in pairs(AliveKeyForFishList) do
        if not tolua.isnull(v) then
            count = count + 1
        end
    end
    return count
end

-- 查询子弹的数量
-- @params accountID( number ) 账户id
function FishingScene:getBulletByAccountID(accountID)
    local count = 0
    for k, v in pairs(AliveKeyForBulletList) do
        if not tolua.isnull(v) then
            if v:getPlayerId() == accountID then
                count = count + 1
            end
        end
    end
    return count
end

-- 玩家发炮消息
-- @params  __info( table )     炮弹相关信息
-- @params  isNative( boolean ) 是否本地发炮
-- @params  currentLockTarget( cc.node ) 目标鱼
function FishingScene:netFireMsg(__info, isNative, currentLockTarget)
    if not tolua.isnull(self.fishMainLayer) then
        local bullet = self.fishMainLayer:netFire(__info, isNative, currentLockTarget)
        if bullet then
            self:addChild(bullet, FishGlobal.LogicLevel + 12)
            table.insert(AliveKeyForBulletList, bullet)
        end
    else
        -- TOAST("change cannon error! UILayer is null !")
    end
end


-- 子弹移动逻辑
-- @params dt( numbr )  帧时间间隔
function FishingScene:updateBullet(dt)
    if next(AliveKeyForBulletList) == nil then
        return
    end

    for k, bullet in pairs(AliveKeyForBulletList) do
        if not tolua.isnull(bullet) then
            -- 当子弹有锁定目标时,子弹自动修正和目标的方向
            local lockTarget = bullet:getLockTarget()
            if not tolua.isnull(lockTarget) then
                if not lockTarget:getCatch() then
                    local posi = cc.p(bullet:getPositionX(), bullet:getPositionY())
                    local targetPosX, targetPosY = lockTarget:getPosition()
                    local temp_y = targetPosY - posi.y
                    local temp_x = targetPosX - posi.x
                    local angle = math.atan2(temp_y, temp_x) / math.pi * 180
                    local nangle = 90 - angle
                    bullet:setRotation(nangle)

                    local target_factor = math.rad(angle)
                    local target_move_factorx = math.cos(target_factor) * bullet:getMoveSpeed()
                    local target_move_factory = math.sin(target_factor) * bullet:getMoveSpeed()
                    bullet:setMoveFactory(cc.p(target_move_factorx, target_move_factory))
                else
                    bullet:setLockTarget(nil)
                end
            end

            local orientation = bullet:getRotation()
            local posi = cc.p(bullet:getPositionX(), bullet:getPositionY())
            local rect = cc.Director:getInstance():getOpenGLView():getVisibleRect()
            local factory = bullet:getMoveFactory()
            posi.x = posi.x + factory.x * dt
            posi.y = posi.y + factory.y * dt

            if not cc.rectContainsPoint(rect, posi) then    
                --进行反弹操作
                local change = bullet:getContentSize().width / 2
                if posi.x < change then--左
                    posi.x = change
                    orientation = 360 - orientation    
                elseif posi.x > (rect.width - change) then--右
                    posi.x = rect.width - change
                    orientation = 360 - orientation
                elseif posi.y < change then--上 
                    posi.y = change
                    orientation = 180 - orientation
                elseif posi.y > (rect.height - change) then--下
                    posi.y = rect.height - change
                    orientation = 180 - orientation
                end

                bullet:setLockTarget(nil)
                local factor = math.rad(90 - orientation)
                local move_factorx = math.cos(factor) * bullet:getMoveSpeed()
                local move_factory = math.sin(factor) * bullet:getMoveSpeed()

                bullet:setMoveFactory(cc.p(move_factorx, move_factory))
            end
            bullet:setPosition(posi)
            bullet:setRotation(orientation)
            bullet:updateBulltRect()
            if tolua.isnull(lockTarget) then
                if posi.x <= display.width / 2 + 6 then
                    self.m_leftBullets[k] = bullet
                    self.m_rightBullets[k] = nil
                else
                    self.m_rightBullets[k] = bullet
                    self.m_leftBullets[k] = nil
                end
            end
        else
            AliveKeyForBulletList[k] = nil
            self.m_leftBullets[k] = nil
            self.m_rightBullets[k] = nil
        end
    end
end

-- 获取最大倍数鱼
-- @return maxFish( number ) 最大倍数的鱼
function FishingScene:getMaxScoreFish(minFishType)
    local maxFish = nil
    minFishType = minFishType or FishGlobal.minCannonFishType
    for k, fish in pairs(InScreenFishList) do
        if not tolua.isnull(fish) then
            if not maxFish then
                if (fish:getFishType() >= minFishType) then
                    maxFish = fish
                end
            else
                if (fish:getFrozenCd() < maxFish:getFrozenCd() and  fish:getFishType() > maxFish:getFishType()) then
                    maxFish = fish
                end
            end
        end
    end

    return maxFish
end

function FishingScene:isFishScreen(fish)
    -- 判判断鱼是否在屏幕内
    if (tolua.isnull(fish)) then
        return false
    end
    local positionX, positionY = fish:getPosition()
    if (positionX > 0 and positionY > 0 and
        positionX < display.width and positionY < display.height) then
        return true
    end
    return false
end


-- 获取最大倍数鱼
-- @return maxFish( number ) 最大倍数的鱼
function FishingScene:getMaxTypeFish(minFishType)
    local maxFish = nil
    minFishType = minFishType or FishGlobal.minCannonFishType + 1
    for k, fish in pairs(InScreenFishList) do
        if not tolua.isnull(fish) and self:isFishScreen(fish) then
            if not maxFish then
                if (fish:getFishType() >= minFishType) then
                    maxFish = fish
                end
            else
                if (math.floor(fish:getFrozenCd()) <= math.floor(maxFish:getFrozenCd()) and fish:getFishType() >= maxFish:getFishType()) then
                    maxFish = fish
                end
            end
        end
    end

    return maxFish
end



-- 创建锁定动画
function FishingScene:createLockFishAniSign()
    self.lockTargetNode = cc.Node:create()
    self:addChild(self.lockTargetNode, FishGlobal.LogicLevel + 1)
    self.lockTargetNode:setPosition(cc.p(display.cx, display.cy))
    self.lockTargetNode:setVisible(false)
    -- 内侧锁定动画
    local lockTargetNSpr = display.newSprite()
    lockTargetNSpr:setAnchorPoint(cc.p(0.5, 0.5))
    self.lockTargetNode:addChild(lockTargetNSpr)
    EffectManager.playAnimation({ node = lockTargetNSpr, name = "lockTargetN" })
    lockTargetNSpr:runAction(cc.RepeatForever:create(cc.RotateBy:create(8, -360)))

    -- 外侧锁定动画
    local lockTargetWSpr = display.newSprite()
    lockTargetWSpr:setAnchorPoint(cc.p(0.5, 0.5))
    self.lockTargetNode:addChild(lockTargetWSpr)
    EffectManager.playAnimation({ node = lockTargetWSpr, name = "lockTargetW" })
    lockTargetWSpr:runAction(cc.RepeatForever:create(cc.RotateBy:create(8, 360)))
end

-- 创建线和瞄准镜
function FishingScene:createLockLineAndAim()
    -- cc.SpriteFrameCache:getInstance():addSpriteFrames("buyu_p_plist/p_buyu_commom_new_1.plist", "buyu_p_plist/p_buyu_commom_new_1.png")
    -- self.lockLineImgs = {}
    -- self.lockAimSprs = {}
    -- for i = 1, FishGlobal.PlayerCount do
    --     self.lockLineImgs[i] = cc.Sprite:create("buyu_p_texture/fish_lockline.png")  --display.newSprite("buyu_p_texture/fish_lockline.png") --ccui.ImageView:create("fish_lockline.png",ccui.TextureResType.plistType )
    --     self.lockLineImgs[i]:setVisible(false)
    --     self.lockLineImgs[i]:setAnchorPoint(cc.p(0, 0.5))
    --     --self.lockLineImgs[i]:setScale9Enabled( true )
    --     local viewChair = FishGlobal.getViewChair(i)
    --     local cannonPos = self.fishMainLayer:getCannonPos(viewChair)
    --     self.lockLineImgs[i]:setPosition(cannonPos)
    --     self.lockLineImgs[i]:setColor(cc.c3b(220, 226, 28))
    --     self.lockLineImgs[i]:setScaleY(0.6)
    --     self:addChild(self.lockLineImgs[i], FishGlobal.LogicLevel + 10)

    --     self.lockAimSprs[i] = display.newSprite("#fish_aim.png")
    --     self.lockAimSprs[i]:setAnchorPoint(cc.p(0.5, 0.5))
    --     self.lockAimSprs[i]:setColor(FishGlobal.getNetAndAimColorByChair(i))
    --     self:addChild(self.lockAimSprs[i], FishGlobal.LogicLevel + 10)
    --     self.lockAimSprs[i]:setVisible(false)
    -- end

    self.m_LockLine = {}
    local distance = cc.pGetDistance(cc.p(0,0), cc.p(1624, 750));
    local lineCircleCount = distance / CONST_LINE_INTERVAL + 10;
    for index = 1, FishGlobal.PlayerCount do
        local lockLines = {}
        for i = 1, lineCircleCount do
            local circle = display.newSprite("buyu_p_texture/fish-lock-dian.png")
            circle:setScale(0.5)
            table.insert(lockLines, circle)
            self:addChild(circle, FishGlobal.LogicLevel + 10)
            circle:setVisible(false)
        end
        local lockAni = CacheManager:addDragonBonesTo(self, FishRes.Ani.LOCK_FISH.animtureName, FishGlobal.LogicLevel + 10)
        lockAni:getAnimation():play(FishRes.Ani.LOCK_FISH.animationName, -1, -1)
        lockAni:setScale(2)
        self.m_LockLine[index] = {
            mIsVisible = false,
            mLockAni = lockAni,
            mLines = lockLines
        }
    end
end

function FishingScene:setLockLineAndAimVisibleByChair(chair, isVisible)
    -- self.lockLineImgs[chair]:setVisible(isVisible)
    -- self.lockAimSprs[chair]:setVisible(isVisible)

    self.m_LockLine[chair].mIsVisible = isVisible
    self.m_LockLine[chair].mLockAni:setVisible(isVisible)
    for index = 1, #self.m_LockLine[chair].mLines do
        self.m_LockLine[chair].mLines[index]:setVisible(isVisible)
    end

end

function FishingScene:hideAllLineAndAim()
    for i = 1, FishGlobal.PlayerCount do
        self:setLockLineAndAimVisibleByChair(1, false)
        self.m_lockTargets[i] = nil
    end
end

-- 通过椅子号设置锁定目标
function FishingScene:setLockTargetByChair(chair, lockTarget)
    self.m_lockTargets[chair] = lockTarget
end

-- 通过椅子号获取锁定目标
function FishingScene:getLockTargetByChair(chair)
    return self.m_lockTargets[chair]
end
-- 获取所有锁定目标
function FishingScene:getLockTargets()
    return self.m_lockTargets
end


function FishingScene:actualLockFish(dt)
    
    if (not tolua.isnull(self.m_lockTargets[FishGlobal.myChair])) then
        if (not self:isFishScreen(self.m_lockTargets[FishGlobal.myChair])) then
            self.m_lockTargets[FishGlobal.myChair] = nil
        end
    end
    for i = 1, FishGlobal.PlayerCount do
        if not tolua.isnull(self.m_lockTargets[i]) then
            local fishPosX, fishPosY = self.m_lockTargets[i]:getPosition()
            local viewChair = FishGlobal.getViewChair(i)
            local cannonPos = self.fishMainLayer:getCannonPos(viewChair)
            local temp_y = fishPosY - cannonPos.y
            local temp_x = fishPosX - cannonPos.x
            -- local angle = math.atan2(temp_y, temp_x) / math.pi * 180
            -- local nangle = 90 - angle
            -- self.fishMainLayer:setCannonRotation(viewChair, nangle)
            local distance = cc.pGetDistance(cannonPos, cc.p(fishPosX, fishPosY))
            -- self:setLockLineAndAimVisibleByChair(i, true)
            -- self.lockLineImgs[i]:setRotation(360 - angle)
            -- --self.lockLineImgs[i]:setContentSize( cc.size( distance , 8 ) )
            -- self.lockLineImgs[i]:setScaleX(distance / 8)
            -- self.lockLineImgs[i]:setPosition(cannonPos)
            -- self.lockAimSprs[i]:setPosition(cc.p(fishPosX, fishPosY))

            if (not self.m_LockLine[i].mLockAni:isVisible()) then
                self.m_LockLine[i].mLockAni:getAnimation():play(FishRes.Ani.LOCK_FISH.animationName, -1, -1)
            end
            self.m_LockLine[i].mLockAni:setVisible(true)
            self.m_LockLine[i].mLockAni:setPosition(cc.p(fishPosX, fishPosY))
            local lists = self.m_LockLine[i].mLines
            local count = distance / CONST_LINE_INTERVAL + 1
            for index = 1, #lists do
                if (index <= count) then
                    lists[index]:setVisible(true)
                    local t = ((index - 1) * CONST_LINE_INTERVAL / math.max(distance, 1))
                    local x = t * temp_x + cannonPos.x
                    local y = t * temp_y + cannonPos.y
                    lists[index]:setPosition(cc.p(x, y))
                else
                    lists[index]:setVisible(false)
                end
            end

            temp_y = fishPosY - cannonPos.y
            temp_x = fishPosX - cannonPos.x
            local angle = math.atan2(temp_y,temp_x)/math.pi * 180.0
            local nangle = 90 - angle
            self.fishMainLayer:setCannonRotation(viewChair, nangle )
        else
            self.m_lockTargets[i] = nil
            self:setLockLineAndAimVisibleByChair(i, false)
            -- if (i == FishGlobal.myChair and isLock) then
            --     self.fishMainLayer:updateFindLockFish()
            -- end
        end
    end

    if (tolua.isnull(self.m_lockTargets[FishGlobal.myChair])) then
        self.m_fActualLockFishTime = self.m_fActualLockFishTime - dt
        if (self.m_fActualLockFishTime <= 0) then
            self.m_fActualLockFishTime = 0.2
            self.fishMainLayer:updateFindLockFish()
        end
    end

end


function FishingScene:unLockByStruggleFish(struggleFish)
    for i = 1, FishGlobal.PlayerCount do
        if not tolua.isnull(self.m_lockTargets[i]) then
            if self.m_lockTargets[i] == struggleFish then
                self:setLockLineAndAimVisibleByChair(i, false)
                self.m_lockTargets[i] = nil
                if i == FishGlobal.myChair then
                    self.fishMainLayer:autoLockFireEnd()
                    g_GameController:reqLockFishOper({ 0 })
                end
            end
        end
    end
end

function FishingScene:unLockByOutFish(outFish)
    for i = 1, FishGlobal.PlayerCount do
        if not tolua.isnull(self.m_lockTargets[i]) then
            if self.m_lockTargets[i] == outFish then
                self:setLockLineAndAimVisibleByChair(i, false)
                self.m_lockTargets[i] = nil
                if i == FishGlobal.myChair then
                    self.fishMainLayer:autoLockFireEnd()
                    g_GameController:reqLockFishOper({ 0 })
                end
            end
        end
    end
end



-- 设置锁定动画的位置
-- @params pos( cc.p(x,y) ) 坐标点
function FishingScene:setLockFishSignPos(pos)
    self.lockTargetNode:setPosition(pos)
end

-- 设置锁定状态的显示状态
-- @params isVisible( boolean ) 锁定状态的显示状态
function FishingScene:setLockFishVisible(isVisible)
    if isVisible then
        self.lockTargetNode:setScale(10)
        self.lockTargetNode:setVisible(isVisible)
        self.lockTargetNode:runAction(cc.ScaleTo:create(0.8, 1))
    else
        self.lockTargetNode:setVisible(isVisible)
    end

end

-- 自动锁定最大倍数鱼
function FishingScene:autoLoackMaxSCoreFish()
    local lockSkillData = FishSkillManager:getInstance():getSkillDataBySkillType(FishSkillManager.skillType.LOCKSKILL, FishGlobal.myViewChair)
    local currentLockTarget = FishSkillManager:getInstance():getCurrentLockTarget()
    if tolua.isnull(currentLockTarget) and lockSkillData then
        if lockSkillData.m_isOpen then
            local maxFish = self:getMaxScoreFish()
            if not tolua.isnull(maxFish) then
                self:setFishsLockedSign()
                maxFish:setIsLocked(true)
                self:setLockFishVisible(true)
                FishSkillManager:getInstance():setCurrentLockTarget(maxFish)
                self:setLockFishSignPos(cc.p(maxFish:getPosition()))
            end
        end
    end
end



-- 解锁所有鱼
function FishingScene:setFishsLockedSign()
    for k, fish in pairs(InScreenFishList) do
        if not tolua.isnull(fish) then
            fish:setIsLocked(false)
        end
    end
end

-- 鱼移动逻辑
-- @params dt( numbr )  帧时间间隔
function FishingScene:fishMove(dt)
    local fishsDel = {}
    for k, fish in pairs(AliveKeyForFishList) do

        if not tolua.isnull(fish) then
            -- 被击中的变亮的鱼，明亮度恢复正常
            local fishColorIndex = self.fishColorFrames[fish:getFishID()]
            if fishColorIndex then
                fishColorIndex = fishColorIndex + 1
                self.fishColorFrames[fish:getFishID()] = fishColorIndex
                if fishColorIndex >= 10 then
                    self.fishColorFrames[fish:getFishID()] = nil
                    --fish:setBlendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA)
                    fish:setGLProgramState(cc.GLProgramState:getOrCreateWithGLProgramName("ShaderPositionTextureColor_noMVP"))
                end
            end

            -- 宝箱对话框显示
            fish:showChestDialog(dt)
            fish:setFrozenCd(fish:getFrozenCd() + dt)
            -- 鱼的游走
            if not fish:getIsFrozen() then
                if fish:getTroop() then  -- 走鱼阵的鱼
                    local positionX, positionY = fish:getPosition()
                    positionX = positionX + (fish:getSpeed() * dt * fish:getOrientation() * display.scaleX)
                    local fishSzie = fish:geRealWithAndHeight()
                    if fish:getOrientation() == FishGlobal.fishOrientation.left and positionX < -fishSzie[1] / 2 then
                        table.insert(fishsDel, k)
                    elseif fish:getOrientation() == FishGlobal.fishOrientation.right and positionX > (display.width + fishSzie[1] / 2) then
                        table.insert(fishsDel, k)
                    else
                        fish:setPositionX(positionX)
                        fish:updateFishShadowPos(true)
                        if not tolua.isnull(fish.m_pNetItem) then
                            fish.m_pNetItem:setPosition(cc.p(fish:getPosition()));
                        end
                        -- if fish:getIsLocked() then
                        --    self:setLockFishSignPos( cc.p( positionX , positionY ) )
                        -- end
                        -- 判判断鱼是否移动到屏幕内
                        if fish:getOrientation() == FishGlobal.fishOrientation.left then      --朝左游
                            if positionX <= display.width + fishSzie[1] / 2 then
                                fish:computeFishBox(UpdateCollisionBoxIndex)
                                if not InScreenFishList[k] then

                                    InScreenFishList[k] = fish
                                end
                                if positionX < display.width / 2 + fishSzie[1] / 2 then
                                    self.m_leftFishList[k] = fish
                                else
                                    self.m_leftFishList[k] = nil
                                end

                                if positionX > display.width / 2 - fishSzie[1] / 2 then
                                    self.m_rightFishList[k] = fish
                                else
                                    self.m_rightFishList[k] = nil
                                end
                            end

                        elseif fish:getOrientation() == FishGlobal.fishOrientation.right then --朝右游
                            fish:computeFishBox(UpdateCollisionBoxIndex)
                            if positionX >= -fishSzie[1] / 2 then
                                if not InScreenFishList[k] then

                                    InScreenFishList[k] = fish
                                end

                                if positionX < display.width / 2 + fishSzie[1] / 2 then
                                    self.m_leftFishList[k] = fish
                                else
                                    self.m_leftFishList[k] = nil
                                end

                                if positionX > display.width / 2 - fishSzie[1] / 2 then
                                    self.m_rightFishList[k] = fish
                                else
                                    self.m_rightFishList[k] = nil
                                end

                            end
                        end
                    end
                else

                    -- 走轨迹的鱼
                    local pathindex, distance = fish:getPathInfo()
                    distance = distance + fish:getSpeed() * dt
                    fish:setPathDistance(distance)
                    local newPosition = qka.QKAPathManager:getInstance():getPoint(pathindex, distance)
                    local newAngle = qka.QKAPathManager:getInstance():getAngle(pathindex, distance)

                    if newPosition.x == 0 and newPosition.y == 0 then  -- 走完轨迹
                        if FishGlobal.NoNetTest then
                            self.lifeTimeFile:write('\n')
                            local nIndex = fish:getFishType() * 1000 + pathindex
                            print("nIndex=", nIndex)
                            self.lifeTimeFile:write(nIndex .. " " .. fish:getStayTime())
                        end
                        table.insert(fishsDel, k)
                    else
                        local glPos = cc.Director:getInstance():convertToGL(cc.p(newPosition.x * display.scaleX, newPosition.y * display.scaleY))
                        local viewPostion = FishGlobal.convertFishPostion(glPos)
                        fish:setPosition(viewPostion)
                        if not tolua.isnull(fish.m_pNetItem) then
                            fish.m_pNetItem:setPosition(cc.p(fish:getPosition()));
                        end
                        if (fish:getFingerling() ~= FishModel.FINGERLING.CHESS_FISH_TYPE) then
                            fish:setRotation(180 * newAngle / math.pi + 180)
                        end
                        -- if fish:getIsLocked() then
                        --    self:setLockFishSignPos( viewPostion )
                        -- end
                        local isVisible = true
                        -- 当鱼的高度游走到屏幕上方的位置,则隐藏鱼的阴影
                        if viewPostion.y > display.height * 0.75 then
                            isVisible = false
                        end
                        fish:updateFishShadowPos(isVisible)

                        local fishSzie = fish:geRealWithAndHeight()
                        -- 判断鱼是否移动到屏幕内
                        if (viewPostion.x > -fishSzie[1] / 2 and viewPostion.x < display.width + fishSzie[1] / 2)
                        and (viewPostion.y > -fishSzie[1] / 2 and viewPostion.y < display.height + fishSzie[1] / 2) then
                            fish:computeFishBox(UpdateCollisionBoxIndex)
                            InScreenFishList[k] = fish
                            if viewPostion.x < display.width / 2 + fishSzie[1] / 2 then
                                self.m_leftFishList[k] = fish
                            else
                                self.m_leftFishList[k] = nil
                            end

                            if viewPostion.x > display.width / 2 - fishSzie[1] / 2 then
                                self.m_rightFishList[k] = fish
                            else
                                self.m_rightFishList[k] = nil
                            end

                        else
                            InScreenFishList[k] = nil
                            self.m_leftFishList[k] = nil
                            self.m_rightFishList[k] = nil
                        end

                    end
                end
                local positionX, positionY = fish:getPosition()
                if positionX < -10 or positionX > display.width + 10 then
                    self:unLockByOutFish(fish)
                end
            else
                local fishPos = cc.p(fish:getPosition())
                local fishSzie = fish:geRealWithAndHeight()
                -- 判断鱼是否移动到屏幕内
                if (fishPos.x > -fishSzie[1] / 2 and fishPos.x < display.width + fishSzie[1] / 2)
                and (fishPos.y > -fishSzie[1] / 2 and fishPos.y < display.height + fishSzie[1] / 2) then
                    fish:computeFishBox(UpdateCollisionBoxIndex)
                    InScreenFishList[k] = fish

                    if fishPos.x < display.width / 2 + fishSzie[1] / 2 then
                        self.m_leftFishList[k] = fish
                    else
                        self.m_leftFishList[k] = nil
                    end

                    if fishPos.x > display.width / 2 - fishSzie[1] / 2 then
                        self.m_rightFishList[k] = fish
                    else
                        self.m_rightFishList[k] = nil
                    end

                end
                local frozenCd = fish:getFrozenCd() - dt
                fish:setFrozenCd(frozenCd)
                if frozenCd <= 0 then
                    self:unfreezeFish(fish)
                end
            end

        else
            AliveKeyForFishList[k] = nil
            InScreenFishList[k] = nil
        end
    end
    -- -- 清除游出界面或者走完轨迹的鱼
    for k, v in pairs(fishsDel) do
        self:unLockByOutFish(AliveKeyForFishList[v])
        AliveKeyForFishList[v]:stopAllActions()
        AliveKeyForFishList[v]:removeFromParent()
        AliveKeyForFishList[v] = nil
        InScreenFishList[v] = nil
        self.m_leftFishList[k] = nil
        self.m_rightFishList[k] = nil
    end

    --fishsDel = {}
end

-- 过场景鱼加速
-- @params multiple( number ) 加速的倍数
function FishingScene:speedUpFish(multiple)
    for k, fish in pairs(AliveKeyForFishList) do
        if not tolua.isnull(fish) then
            fish:setSpeed(fish:getSpeed() * multiple)
        end
    end
end

--子弹与鱼碰撞
function FishingScene:collisionFishBullet()
    for k, fish in pairs(AliveKeyForFishList) do
        if (tolua.isnull(fish)) then
            AliveKeyForFishList[k] = nil
        else
            fish:getPosition()
            --计算碰撞框
            fish:computeFishBox(UpdateCollisionBoxIndex)
            for index, bullet in pairs(AliveKeyForBulletList) do
                if not tolua.isnull(bullet) then
                    if tolua.isnull(bullet:getLockTarget()) then
                        if fish:isCollision(bullet:getBulltRect()) then
                            self:collisionFishBulletEffect(bullet, fish, index)
                        end
                    end
                end
            end
        end
    end
end

-- --子弹与鱼碰撞
-- function FishingScene:collisionFishBulletEx()
--    for k,fish in pairs( InScreenFishList ) do
--         if not tolua.isnull( fish ) then
--             local fishPos = cc.p( fish:getPosition())
--             for index , bullet in pairs(AliveKeyForBulletList) do
--               if not tolua.isnull( bullet ) then
--                  if tolua.isnull( bullet:getLockTarget() ) then
--                     local bulletPos = cc.p( bullet:getPosition() )
--                     if cc.pGetDistance( fishPos, bulletPos )  <= 300 then
--                         if fish:isCollision( bullet:getBulltRect() ) then
--                             self:collisionFishBulletEffect( bullet , fish , index )
--                         end
--                     end
--                  end
--               end
--             end
--         end
--     end
-- end
-- 子弹与鱼的碰撞检测
function FishingScene:collisionFishBulletEx()
    --print("collisionFishBulletEx()")
    for index, bullet in pairs(self.m_leftBullets) do
        if not tolua.isnull(bullet) then
            local bulletPos = cc.p(bullet:getPosition())
            for k, fish in pairs(self.m_leftFishList) do
                if not tolua:isnull(fish) then
                    local fishPos = cc.p(fish:getPosition())
                    if fish:isCollision(bullet:getBulltRect()) then
                        self:collisionFishBulletEffect(bullet, fish, index)
                        break
                    end
                end
            end
        end
    end

    for index, bullet in pairs(self.m_rightBullets) do
        if not tolua.isnull(bullet) then
            local bulletPos = cc.p(bullet:getPosition())
            for k, fish in pairs(self.m_rightFishList) do
                if not tolua:isnull(fish) then
                    local fishPos = cc.p(fish:getPosition())
                    if fish:isCollision(bullet:getBulltRect()) then
                        self:collisionFishBulletEffect(bullet, fish, index)
                        break
                    end
                end
            end
        end
    end
end

-- 锁定子弹的碰撞检测
function FishingScene:collisionLockBullet()
    for index, bullet in pairs(AliveKeyForBulletList) do
        if not tolua.isnull(bullet) then
            local fish = bullet:getLockTarget()
            if not tolua.isnull(fish) then
                if not fish:getCatch() then
                    if fish:isCollision(bullet:getBulltRect()) then
                        self:collisionFishBulletEffect(bullet, fish, index)
                    end
                else
                    bullet:setLockTarget(nil)
                end
            end
        end
    end
end

-- 子弹与鱼碰撞效果
-- @params  bullet( BulletModel ) 子弹模型
-- @params  fish( FishModel )     鱼的模型
-- @params  index( number )       鱼的索引
function FishingScene:collisionFishBulletEffect(bullet, fish, index)
    --生成网效果
    local isLock = false
    local lockFish = bullet:getLockTarget()
    if lockFish ~= nil and lockFish == fish then
        isLock = true
    end
    
    local netItem = self.fishMainLayer.m_bulletManager:playNetEffect(bullet, self, cc.p(fish:getPositionX(), fish:getPositionY()), isLock);

    if isLock then
        fish.m_pNetItem = netItem;
    end


    -- if isLock then
    --     local size = fish:getContentSize();
    --     self.fishMainLayer.m_bulletManager:playNetEffect(bullet, fish, cc.p(size.width/2, size.height/2), isLock);
    -- else
    --     self.fishMainLayer.m_bulletManager:playNetEffect(bullet, self, cc.p(fish:getPositionX(), fish:getPositionY()), isLock);        
    -- end
    
    -- 只要自己子弹碰撞到鱼，才向服务器请求捕鱼
    if bullet:isOwn() then
        local poweScore = FishGameDataController:getInstance():getPowerScoreByLevel(bullet:getPowerlevel())
        local fixPos = FishGlobal.convertFishPostion(cc.p(fish:getPositionX() / display.scaleX, fish:getPositionY() / display.scaleY))
        local UIpos = cc.Director:getInstance():convertToUI(fixPos)
        g_GameController:reqGetFish({ fish:getFishID(), bullet:getPowerItemId(), poweScore, bullet:getStep(), UIpos.x, UIpos.y, 0 })
    else -- 其他人的子弹
        if g_GameController:getAiDriveAccountID() == Player.getAccountID() then -- ai 驱动端
            if bullet:isRobot() then -- 机器人子弹
                local poweScore = FishGameDataController:getInstance():getPowerScoreByLevel(bullet:getPowerlevel())
                local fixPos = FishGlobal.convertFishPostion(cc.p(fish:getPositionX() / display.scaleX, fish:getPositionY() / display.scaleY))
                local UIpos = cc.Director:getInstance():convertToUI(fixPos)
                g_GameController:reqGetFish({ fish:getFishID(), bullet:getPowerItemId(), poweScore, bullet:getStep(), UIpos.x, UIpos.y, bullet:getPlayerId() })
            end
        end
    end


    self.fishColorFrames[fish:getFishID()] = 0
    --fish:setBlendFunc(gl.ONE, gl.ONE)  --原图被蒙上一层白色
    fish:setGLProgramState(cc.GLProgramState:getOrCreateWithGLProgramName("lightTexture2DKey"))
    --鱼被击中挣扎效果
    if not fish:getIsFrozen() then
        -- if self:randomOK( 30, 100 ) then
        --       local function  resumeNormal()
        --            local swimAnimation = cc.AnimationCache:getInstance():getAnimation(string.format("%s_swim",fish:getFishName()))
        --            fish:stopAllActions() 
        --            if  swimAnimation then
        --                 fish:runAction(cc.RepeatForever:create(cc.Animate:create( swimAnimation )))
        --            end
        --       end
        --       local callfunc = cc.CallFunc:create( resumeNormal ) 
        --       local animation = cc.AnimationCache:getInstance():getAnimation(string.format("%s_struggle",fish:getFishName()))
        --       if animation then
        --           fish:stopAllActions() 
        --           fish:runAction( cc.Sequence:create( cc.Animate:create( animation ) , callfunc ,  nil ) )
        --       end
        -- end
    end
    bullet:stopAllActions()
    bullet:removeFromParent()
    AliveKeyForBulletList[index] = nil
    self.m_leftBullets[index] = nil
    self.m_rightBullets[index] = nil
end



-- 随机概率
-- @params numerator( number ) 分子
-- @params denominator( number ) 分母 
function FishingScene:randomOK(numerator, denominator)
    math.randomseed(tostring(os.time()):reverse():sub(1, 7)) --设置时间种子
    local value = math.random(1, denominator)              --产生1到100之间的随机数
    local isOK = false
    if value <= numerator then
        isOK = true
    end
    return isOK
end


--显示碰撞边框-----------------------------------------------------
function FishingScene:TestCollisionBox()
    for i, v in pairs(CollisionBoxList) do
        if (tolua.isnull(v) == false) then
            v:stopAllActions()
            v:removeFromParent()
            CollisionBoxList[i] = nil
        end
    end
    CollisionBoxList = {}
    for k, fish in pairs(AliveKeyForFishList) do
        if (tolua.isnull(fish) == false) then
            --fish:computeFishBox(UpdateCollisionBoxIndex)
            for k, rect in pairs(fish:getFishBoxTable()) do
                local drawNode1 = cc.DrawNode:create()
                local points = {
                    cc.p(rect.x, rect.y),
                    cc.p(rect.x + rect.width, rect.y),
                    cc.p(rect.x + rect.width, rect.y + rect.height),
                    cc.p(rect.x, rect.y + rect.height)
                }
                drawNode1:drawPolygon1(points, { fillColor = cc.c4f(1, 1, 1, 0), borderWidth = 0.5, borderColor = cc.c4f(1, 0, 0, 1) })
                self:addChild(drawNode1, 10000000)
                table.insert(CollisionBoxList, drawNode1)
            end
        end
    end
    self:TestCollisionBulletBoxEx()
end

-- 显示子弹的碰撞盒
function FishingScene:TestCollisionBulletBox()
    for i, v in ipairs(CollisionBulletBoxList) do
        if (tolua.isnull(v) == false) then
            v:stopAllActions()
            v:removeFromParent()
            CollisionBulletBoxList[i] = nil
        end
    end
    CollisionBulletBoxList = {}
    for index, bullet in pairs(AliveKeyForBulletList) do
        if not tolua.isnull(bullet) then
            local drawNode = cc.DrawNode:create()
            local pos = cc.p(bullet:getPosition())
            --drawNode:drawDot(pos , 20,  cc.c4f(1, 0, 0, 1))
            local angle = 90 * math.pi / 180
            drawNode:drawCircle(pos, 20, angle, 20, false, 1, 1, cc.c4f(1, 0, 0, 1))
            self:addChild(drawNode, 10000000)
            table.insert(CollisionBulletBoxList, drawNode)
        end
    end
end


-- 显示子弹的碰撞盒
function FishingScene:TestCollisionBulletBoxEx()
    for i, v in ipairs(CollisionBulletBoxList) do
        if (tolua.isnull(v) == false) then
            v:stopAllActions()
            v:removeFromParent()
            CollisionBulletBoxList[i] = nil
        end
    end
    CollisionBulletBoxList = {}
    for index, bullet in pairs(AliveKeyForBulletList) do
        if not tolua.isnull(bullet) then
            local drawNode1 = cc.DrawNode:create()
            local pos = cc.p(bullet:getPosition())
            local rect = bullet:getBulltRect()
            local points = {
                cc.p(rect.x, rect.y),
                cc.p(rect.x + rect.width, rect.y),
                cc.p(rect.x + rect.width, rect.y + rect.height),
                cc.p(rect.x, rect.y + rect.height)
            }
            drawNode1:drawPolygon1(points, { fillColor = cc.c4f(1, 1, 1, 0), borderWidth = 0.5, borderColor = cc.c4f(1, 0, 0, 1) })
            self:addChild(drawNode1, 10000000)
            table.insert(CollisionBulletBoxList, drawNode1)
        end
    end
end


-- 测试出鱼数据
-- @params dt( numbr )  帧时间间隔
function FishingScene:TestOutFish(dt)
    if (FishGlobal.OutFishDelay <= FishGlobal.CurFishDelay and self.m_background:isChanging() == false) then
        self.pathIndex = self.pathIndex + 1
        if self.pathIndex > 418 then
            TOAST("时间生成成功")
            print("-------时间生成成功------")
            FishGlobal.CurFishDelay = 0
            return
        end
        local orginalPosition = qka.QKAPathManager:getInstance():getPoint(self.pathIndex, 0)
        if orginalPosition.x == 0 and orginalPosition.y == 0 then
            print("orginalPosition.x == 0self.pathIndex=", self.pathIndex)
            return
        end
        FishGlobal.CurFishDelay = 0
        local outFishInfo = {}
        outFishInfo.m_nLine = self.pathIndex
        outFishInfo.m_nLastTime = 0
        outFishInfo.m_nDelay = 0
        outFishInfo.m_nIceTime = 0
        outFishInfo.m_nClockTime = os.clock();
        local fishConfigDataMap = FishGameDataController:getInstance():getFishConfigDataMap()
        for fishType, fishConfig in pairs(fishConfigDataMap) do
            local fishMake = FishGameDataController:getInstance():getFishMakeItemByIndex(fishType)
            if fishMake then
                if self.pathIndex >= fishMake.nPathBegin and self.pathIndex <= fishMake.nPathEnd then
                    -- print("self.pathIndex=",self.pathIndex)
                    print("fishType=", fishType)
                    self.fishIndex = self.fishIndex + 1
                    outFishInfo.m_nIndex = self.fishIndex
                    outFishInfo.m_nType = fishType
                    local fish = self.m_fishManager:createLineFish(self, outFishInfo, 0)
                    self:addChild(fish, FishGlobal.LogicLevel)
                    AliveKeyForFishList[self.fishIndex] = fish
                else
                    local buyuLines = FishGameDataController:getInstance():getBuyuLineList()
                    for k, buyuLineItem in ipairs(buyuLines) do
                        if self.pathIndex >= buyuLineItem.nPathBegin and self.pathIndex <= buyuLineItem.nPathEnd then
                            for k, tFishGroupItem in pairs(buyuLineItem.tFishGroup) do
                                if tFishGroupItem[1] and tFishGroupItem[1] == fishType then
                                    self.fishIndex = self.fishIndex + 1
                                    outFishInfo.m_nIndex = self.fishIndex
                                    outFishInfo.m_nType = fishType
                                    local fish = self.m_fishManager:createLineFish(self, outFishInfo, 0)
                                    self:addChild(fish, FishGlobal.LogicLevel)
                                    AliveKeyForFishList[self.fishIndex] = fish
                                end
                            end
                        end
                    end
                end
            end
        end
    else
        FishGlobal.CurFishDelay = FishGlobal.CurFishDelay + dt
    end
end

-- 玩家退出
-- @params __chair( number ) 椅子号
-- @params __info( table )   玩家信息
function FishingScene:onUserLeave(__chair, __info)
   if self.m_bIsInit ~= true then return end
    --self.m_noticeLayer:playMoveUpInfo("玩家离开")
    self.fishMainLayer:onUserLeave(__chair, __info)
    -- local skillData =  FishSkillManager:getInstance():getSkillDataBySkillType( FishSkillManager.skillType.CRITSKILL ,__chair)
    -- if skillData then
    --    if skillData.m_isOpen then
    --       local _viewChair = FishGlobal.getViewChair( chair )
    --       skillData.m_skillCD = 0
    --       EffectManager.hideCritAni( { viewChair = _viewChair } )
    --    end
    -- end
end

-- 玩家进入
-- @params __chair( number ) 椅子号
-- @params __info( table )   玩家信息
function FishingScene:onUserEnter(__chair, __info)
   if self.m_bIsInit ~= true then return end
    --self.m_noticeLayer:playMoveUpInfo("玩家进来")
    self.fishMainLayer:onUserEnter(__chair, __info)
end


-- 切换场景
-- @params __info( table )   背景信息
function FishingScene:onChangeBackground(__info)
    if not tolua.isnull(self.m_background) then
        --self.m_background:changeBg()
        self:speedUpFish(5)
    else
        TOAST("场景转换")
    end
end

--  驱赶鱼
--  @params __info( table )驱赶鱼的信息
function FishingScene:onDriveFish(__info)
    if FishGlobal.NoNetTest then return end
    local params = {
        prarentNode = self
    }
    EffectManager.showFishTideEffect(params)
    self:speedUpFish(6)
end


-- 冰冻鱼的方法
-- @params __skillCD( number ) 技能cd
function FishingScene:frozenFishs(__skillCD)
    for k, fish in pairs(AliveKeyForFishList) do
        self:frozenFish(fish, __skillCD)
    end
end

-- 冰冻一条鱼
-- @params fish( FishModel ) 鱼
-- @params __skillCD( number ) 技能cd
function FishingScene:frozenFish(fish, __skillCD)
    if not tolua.isnull(fish) then
        if not fish:getIsFrozen() then
            fish:setIsFrozen(true)
            fish:setFrozenCd(__skillCD)
            fish:stopAllActions()
        else
            -- 刷新cd时间
            fish:setFrozenCd(__skillCD)
        end
    end
end


-- 解冻鱼
-- @params fish( FishModel ) 鱼
function FishingScene:unfreezeFish(fish)
    EffectManager.unfreeze(0, self)
    if not tolua.isnull(fish) then
        if fish:getIsFrozen() then
            fish:setIsFrozen(false)
            fish:setFrozenCd(0)
            local animation = cc.AnimationCache:getInstance():getAnimation(string.format("%s_swim", fish:getFishName()))
            if not animation then
                EffectManager.loadFishResourceByName(fish:getFishName())
                self.m_fishManager:createFishAnimationByName(fish:getFishName())
                animation = cc.AnimationCache:getInstance():getAnimation(string.format("%s_swim", fish:getFishName()))
            end
            if animation then
                fish:runAction(cc.RepeatForever:create(cc.Animate:create(animation)))
            end
        end
    end
end


-- 功能鱼buff 效果,目前分四种
-- 气球鱼 死亡时释放的毒雾影响到全屏，普通鱼触碰到毒雾后死亡
-- 轮盘金蟾 死亡时触发转轮盘事件，通过轮盘的转动获得最终得奖分数
-- 电鳗 死亡时化作一个电球影响到全屏，自动对普通鱼释放电击
-- 刺鲀 死亡时身上的刺向外飞射，普通鱼被刺中后死亡，若刺没有打中鱼，在飞到屏幕边缘时会像子弹一样反弹
-- @params params( table ) buff 数据信息
function FishingScene:playFishBuff(params)
    -- params.buffIndex params.buffType , params.fishIndex ,params.pos , params.accountID
    if params.buffType == FishBuff.BUFFTYPE.SUPER_BOMB_TYPE then    -- 超级炸弹
        local fishBuff = FishBuff.new()
        local fishPlayer = g_GameController:getPlayerInfoByAccountId(params.accountID)
        local _isOwer = false
        local _isRobt = false
        if fishPlayer then
            if fishPlayer:getViewChair() == FishGlobal.myViewChair then
                _isOwer = true
            end
            _isRobt = fishPlayer:isRobot()
        end
        fishBuff:setAnchorPoint(cc.p(0.5, 0.5))
        fishBuff:setPosition(params.pos)
        --fishBuff:setVisible( params.isVisible )
        self:addChild(fishBuff, FishGlobal.LogicLevel + 1)
        local info = {
            buffIndex = params.buffIndex,
            fishIndex = params.fishIndex,
            buffType = params.buffType,
            rectWith = 1280,
            rectHeight = 1280,
            moveSpeed = 0,
            isOwer    = _isOwer,
            AccountId = params.accountID,
            isRobt    = _isRobt,
        }
        fishBuff:initInfo(info)
        -- local _params = {
        --                    name = "toxicSmog",
        --                    node = fishBuff,
        --                    x    = 0,
        --                    y    = 0,
        --                 }
        -- EffectManager.playAnimation( _params )
        --FishSoundManager:getInstance():playEffect( FishSoundManager.EffectDuwu  )
        fishBuff:computeCollisionBox()
        self.m_toxicSmogs[params.buffIndex] = fishBuff
        self.m_fishBuffs[params.buffIndex] = fishBuff
    elseif params.buffType == FishBuff.BUFFTYPE.PART_BOMB_TYPE then    -- 局部炸弹
        local fishBuff = FishBuff.new()
        local fishPlayer = g_GameController:getPlayerInfoByAccountId(params.accountID)
        local _isOwer = false
        local _isRobt = false
        if fishPlayer then
            if fishPlayer:getViewChair() == FishGlobal.myViewChair then
                _isOwer = true
            end
            _isRobt = fishPlayer:isRobot()
        end
        fishBuff:setAnchorPoint(cc.p(0.5, 0.5))
        fishBuff:setPosition(params.pos)
        --fishBuff:setVisible( params.isVisible )
        self:addChild(fishBuff, FishGlobal.LogicLevel + 1)
        local info = {
            buffIndex = params.buffIndex,
            fishIndex = params.fishIndex,
            buffType = params.buffType,
            rectWith = 400,
            rectHeight = 400,
            moveSpeed = 0,
            isOwer    = _isOwer,
            AccountId = params.accountID,
            isRobt    = _isRobt,
        }
        fishBuff:initInfo(info)
        fishBuff:computeCollisionBox()
        self.m_partBombs[params.buffIndex] = fishBuff
        self.m_fishBuffs[params.buffIndex] = fishBuff
    elseif params.buffType == FishBuff.BUFFTYPE.ELECTRIC_TYPE then    -- 电球
        local fishBuff = FishBuff.new()
        local fishPlayer = g_GameController:getPlayerInfoByAccountId(params.accountID)
        local _isOwer = false
        local _isRobt = false
        if fishPlayer then
            if fishPlayer:getViewChair() == FishGlobal.myViewChair then
                _isOwer = true
            end
            _isRobt = fishPlayer:isRobot()
        end
        fishBuff:setAnchorPoint(cc.p(0.5, 0.5))
        fishBuff:setPosition(cc.p(display.cx, display.cy))
        fishBuff:setVisible(true)
        self:addChild(fishBuff, FishGlobal.LogicLevel + 10)
        local info = {
            buffIndex = params.buffIndex,
            fishIndex = params.fishIndex,
            buffType = params.buffType,
            rectWith = 1280,
            rectHeight = 1280,
            moveSpeed = 0,
            isOwer    = _isOwer,
            fishType = params.fishType,
            AccountId = params.accountID,
            isRobt    = _isRobt,
        }
        fishBuff:initInfo(info)
        fishBuff:setPosition(params.pos)
        -- local _params = {
        --                    targetNode = fishBuff,
        --                 }
        --EffectManager.playlightEffect( _params )
        -- cc.SpriteFrameCache:getInstance():addSpriteFrames("buyu_p_plist/p_buyu_commom_new_1.plist","buyu_p_plist/p_buyu_commom_new_1.png")
        -- fishBuff:setSpriteFrame( display.newSpriteFrame (  "fish_a_hundred shots.png" ) ) 
        fishBuff:computeCollisionBox()
        local function removeElectric()
            fishBuff:stopAllActions()
            fishBuff:removeFromParent()
        end
        local callfunc = cc.CallFunc:create(removeElectric)
        fishBuff:runAction(cc.Sequence:create(cc.DelayTime:create(1), callfunc, nil))
        self.m_electrics[params.buffIndex] = fishBuff
        self.m_fishBuffs[params.buffIndex] = fishBuff
    elseif params.buffType == FishBuff.BUFFTYPE.PRICK_TYPE then    --刺
        local fishBuff = FishBuff.new()
        local fishPlayer = g_GameController:getPlayerInfoByAccountId(params.accountID)
        local _isOwer = false
        local _isRobt = false
        if fishPlayer then
            if fishPlayer:getViewChair() == FishGlobal.myViewChair then
                _isOwer = true
            end
            _isRobt = fishPlayer:isRobot()
        end
        fishBuff:setAnchorPoint(cc.p(0.5, 0.5))
        fishBuff:setPosition(cc.p(display.cx, display.cy))
        fishBuff:setVisible(true)
        self:addChild(fishBuff, FishGlobal.LogicLevel + 10)
        local info = {
            buffIndex = params.buffIndex,
            fishIndex = params.fishIndex,
            buffType = params.buffType,
            rectWith = 1280,
            rectHeight = 1280,
            moveSpeed = 0,
            isOwer    = _isOwer,
            AccountId = params.accountID,
            isRobt    = _isRobt,
        }
        fishBuff:initInfo(info)
        local _params = {
            targetNode = fishBuff,
        }
        EffectManager.playCiEffect(_params)
        FishSoundManager:getInstance():playEffect(FishSoundManager.EffectCitun)
        self.m_pricks[params.buffIndex] = fishBuff
        self.m_fishBuffs[params.buffIndex] = fishBuff
    elseif params.buffType == FishBuff.BUFFTYPE.WHEEL_TYPE then
        local fishPlayer = g_GameController:getPlayerInfoByAccountId(params.accountID)
        local _viewChair = 0
        local _chair = 0
        if fishPlayer then
            _viewChair = fishPlayer:getViewChair()
            _chair    = fishPlayer:getChair()
        end
        local cannonPos = self.fishMainLayer:getCannonPos(_viewChair)
        local _pos = cc.p(cannonPos.x, cannonPos.y - 60)
        local _isReversal = false
        if _viewChair == 4 or _viewChair == 5 or _viewChair == 6 then
            _pos = cc.p(cannonPos.x, cannonPos.y + 60)
            _isReversal = true
        end
        local _params = {
            prarentNode = self,
            pos        = params.pos,
            targetPos    = _pos,
            --times         = params.randScore,
            isReversal    = _isReversal,
            coin        = params.coin,
            addCoin    = params.addCoin,
            viewChair    = _viewChair,
            fishMainLayer = self.fishMainLayer,
            chair        = _chair,
        }
        EffectManager.playToadTurnDisc(_params)
    end
end

function FishingScene:playFishBuffEx(params)
    if params.buffType == FishBuff.BUFFTYPE.SURPER_BOMB_TYPE then    -- 超级炸弹
        if self.m_fishBuffs[params.buffIndex] then
            self.m_fishBuffs[params.buffIndex]:setVisible(true)
            --FishSoundManager:getInstance():playEffect( FishSoundManager.EffectDuwu  )
        end
    elseif params.buffType == FishBuff.BUFFTYPE.ELECTRIC_TYPE then    -- 电球

    elseif params.buffType == FishBuff.BUFFTYPE.PRICK_TYPE then       -- 刺

    end
end

-- 停止buff 播放效果   
function FishingScene:stopBuffEffect(params)
    local fishBuff = self.m_fishBuffs[params.m_nEffectIndex]
    if not tolua.isnull(fishBuff) then
        if fishBuff:getBuffType() == FishBuff.BUFFTYPE.TOXIC_SMOG_TYPE then      -- 超级炸弹
            if not tolua.isnull(self.m_toxicSmogs[params.m_nEffectIndex]) then
                self.m_toxicSmogs[params.m_nEffectIndex]:stopAllActions()
                self.m_toxicSmogs[params.m_nEffectIndex]:removeFromParent()
                self.m_toxicSmogs[params.m_nEffectIndex] = nil
                self.m_fishBuffs[params.m_nEffectIndex] = nil
            else
                self.m_toxicSmogs[params.m_nEffectIndex] = nil
                self.m_fishBuffs[params.m_nEffectIndex] = nil
            end
        elseif fishBuff:getBuffType() == FishBuff.BUFFTYPE.PART_BOMB_TYPE then -- 局部炸弹
            if not tolua.isnull(self.m_partBombs[params.m_nEffectIndex]) then
                self.m_partBombs[params.m_nEffectIndex]:stopAllActions()
                self.m_partBombs[params.m_nEffectIndex]:removeFromParent()
                self.m_partBombs[params.m_nEffectIndex] = nil
                self.m_fishBuffs[params.m_nEffectIndex] = nil
            else
                self.m_partBombs[params.m_nEffectIndex] = nil
                self.m_fishBuffs[params.m_nEffectIndex] = nil
            end
        elseif fishBuff:getBuffType() == FishBuff.BUFFTYPE.ELECTRIC_TYPE then    -- 电球
            if not tolua.isnull(self.m_electrics[params.m_nEffectIndex]) then
                self.m_electrics[params.m_nEffectIndex]:stopAllActions()
                self.m_electrics[params.m_nEffectIndex]:removeFromParent()
                self.m_electrics[params.m_nEffectIndex] = nil
                self.m_fishBuffs[params.m_nEffectIndex] = nil
            else
                self.m_electrics[params.m_nEffectIndex] = nil
                self.m_fishBuffs[params.m_nEffectIndex] = nil
            end
        end
    end
end

-- 超级炸弹
function FishingScene:toxicSmogFish()
    for k, toxicSmogs in pairs(self.m_toxicSmogs) do
        if not tolua.isnull(toxicSmogs) and toxicSmogs:isVisible() and toxicSmogs:getFirstReq() then
            if toxicSmogs:isOwer() then
                local fishIDs = {}
                for k, fish in pairs(InScreenFishList) do
                    if not tolua.isnull(fish) then --fish:getFishType() <  50  then
                        if toxicSmogs:isCollision(fish:getFishBoxTable()) then
                            if not fish:isPoisoning(toxicSmogs:getBuffIndex()) then
                                fish:setPoisoning(toxicSmogs:getBuffIndex(), true)
                                table.insert(fishIDs, fish:getFishID())
                            end
                        end
                    end
                end
                toxicSmogs:setFirstReq(false)
                g_GameController:reqBuffGet({ fishIDs, toxicSmogs:getBuffIndex(), 0 })
            else
                if g_GameController:getAiDriveAccountID() == Player.getAccountID() then
                    if toxicSmogs:isRobt() then
                        local fishIDs = {}
                        for k, fish in pairs(InScreenFishList) do
                            if not tolua.isnull(fish) then --fish:getFishType() <  50  then
                                if toxicSmogs:isCollision(fish:getFishBoxTable()) then
                                    if not fish:isPoisoning(toxicSmogs:getBuffIndex()) then
                                        fish:setPoisoning(toxicSmogs:getBuffIndex(), true)
                                        table.insert(fishIDs, fish:getFishID())
                                    end
                                end
                            end
                        end
                        toxicSmogs:setFirstReq(false)
                        g_GameController:reqBuffGet({ fishIDs, toxicSmogs:getBuffIndex(), toxicSmogs:getAccountId() })
                    end
                end
            end
        end
    end
end

-- 局部炸弹
function FishingScene:partBombFish()
    for k, partBomb in pairs(self.m_partBombs) do
        if not tolua.isnull(partBomb) and partBomb:isVisible() and partBomb:getFirstReq() then
            if partBomb:isOwer() then
                local fishIDs = {}
                for k, fish in pairs(InScreenFishList) do
                    if not tolua.isnull(fish) then --fish:getFishType() <  50  then
                        if partBomb:isCollision(fish:getFishBoxTable()) then
                            if not fish:isPoisoning(partBomb:getBuffIndex()) then
                                fish:setPoisoning(partBomb:getBuffIndex(), true)
                                table.insert(fishIDs, fish:getFishID())
                            end
                        end
                    end
                end
                partBomb:setFirstReq(false)
                g_GameController:reqBuffGet({ fishIDs, partBomb:getBuffIndex(), 0 })
            else
                if g_GameController:getAiDriveAccountID() == Player.getAccountID() then
                    if partBomb:isRobt() then
                        local fishIDs = {}
                        for k, fish in pairs(InScreenFishList) do
                            if not tolua.isnull(fish) then --fish:getFishType() <  50  then
                                if partBomb:isCollision(fish:getFishBoxTable()) then
                                    if not fish:isPoisoning(partBomb:getBuffIndex()) then
                                        fish:setPoisoning(partBomb:getBuffIndex(), true)
                                        table.insert(fishIDs, fish:getFishID())
                                    end
                                end
                            end
                        end
                        partBomb:setFirstReq(false)
                        g_GameController:reqBuffGet({ fishIDs, partBomb:getBuffIndex(), Player.getAccountID() })
                    end
                end
            end
        end
    end
end


-- 雷霆万钧
function FishingScene:electricFishOld()
    for k, electri in pairs(self.m_electrics) do
        if not tolua.isnull(electri) and electri:isOwer() and electri:isVisible() then
            local fishIDs = {}
            local fishNum = 0
            for k, fish in pairs(InScreenFishList) do
                if not tolua.isnull(fish) then
                    if fish:getFishType() < 50 and not fish:isElectri(electri:getBuffIndex()) then
                        fish:setElectri(electri:getBuffIndex(), true)
                        table.insert(fishIDs, fish:getFishID())
                        fishNum = fishNum + 1
                        if fishNum >= 50 then
                            break
                        end
                    end
                end
            end
            if next(fishIDs) then
                g_GameController:reqBuffGet({ fishIDs, electri:getBuffIndex() })
            end
        end
    end
end

-- 雷霆万钧
function FishingScene:electricFish()
    for k, electri in pairs(self.m_electrics) do
        if not tolua.isnull(electri) and electri:isVisible() and electri:getFirstReq() then
            if electri:isOwer() then
                local fishIDs = {}
                for k, fish in pairs(InScreenFishList) do
                    if not tolua.isnull(fish) and fish:getFishType() == electri:getFishType() then
                        if electri:isCollision(fish:getFishBoxTable()) then
                            if not fish:isElectri(electri:getBuffIndex()) then
                                --  FishSoundManager:getInstance():playEffect( FishSoundManager.EffectElectric  )
                                fish:setElectri(electri:getBuffIndex(), true)
                                --  local electriX,electriY = electri:getPosition()
                                --  local fishX,fishY = fish:getPosition()
                                --  local temp_y = fishY - electriY
                                --  local temp_x = fishX - electriX
                                --  local angle = math.atan2(temp_y,temp_x)/math.pi * 180.0
                                --  local distance = math.sqrt(math.pow((temp_y),2)+math.pow((temp_x),2))
                                --  local scaleY = distance / 230
                                --  local light = display.newSprite()
                                --  light:setScaleY( scaleY )
                                --  light:setRotation( 90 - angle  )
                                --  light:setPosition( cc.p(electriX,electriY ) )
                                --  light:setAnchorPoint( cc.p( 0.5,0) )
                                --  self:addChild( light, FishGlobal.LogicLevel+1 )
                                --  local _params = { 
                                --          name = "lightning",
                                --          node = light,
                                --          x    = 0,
                                --          y    = 0,
                                --   }
                                --  EffectManager.playAnimation( _params )
                                --  --light:setBlendFunc(gl.ONE, gl.ONE) 
                                -- local function fishEffect()
                                --     if not tolua.isnull( fish ) then
                                --        local hitEffcet = display.newSprite( "#"..fish:getFishBone() )
                                --        local contSize = fish:getContentSize()
                                --        hitEffcet:setTag(1000)
                                --        hitEffcet:setPosition(cc.p(contSize.width/2,contSize.height/2))
                                --        hitEffcet:setAnchorPoint(cc.p(0.5,0.5))
                                --        fish:addChild( hitEffcet )
                                --     end
                                --  end
                                --  local  callfunc1 = cc.CallFunc:create(fishEffect)
                                --  local  function removelight()
                                --     light:removeFromParent()
                                --     if not tolua.isnull( fish ) then
                                --        fish:removeChildByTag(1000)
                                --     end
                                --  end
                                --  local  callfunc2 = cc.CallFunc:create(removelight)
                                --  light:runAction(cc.Sequence:create( cc.DelayTime:create( 0.5 ) , callfunc2 ,nil))
                                table.insert(fishIDs, fish:getFishID())
                                --g_GameController:reqBuffGet({ fish:getFishID(), electri:getBuffIndex() } )
                                --break
                            end
                        end
                    end
                end
                electri:setFirstReq(false)
                g_GameController:reqBuffGet({ fishIDs, electri:getBuffIndex(), 0 })
            else
                if g_GameController:getAiDriveAccountID() == Player.getAccountID() then
                    if electri:isRobt() then
                        local fishIDs = {}
                        for k, fish in pairs(InScreenFishList) do
                            if not tolua.isnull(fish) and fish:getFishType() == electri:getFishType() then
                                if electri:isCollision(fish:getFishBoxTable()) then
                                    if not fish:isElectri(electri:getBuffIndex()) then
                                        fish:setElectri(electri:getBuffIndex(), true)
                                        table.insert(fishIDs, fish:getFishID())
                                    end
                                end
                            end
                        end
                        electri:setFirstReq(false)
                        g_GameController:reqBuffGet({ fishIDs, electri:getBuffIndex(), electri:getAccountId() })
                    end
                end
            end
        end
    end
end

--万箭齐发(刺豚)
function FishingScene:prickFish()
    for k, prick in pairs(self.m_pricks) do
        if not tolua.isnull(prick) and prick:isOwer() and prick:isVisible() then
            local fishIDs = {}
            local fishNum = 0
            for k, fish in pairs(InScreenFishList) do
                if not tolua.isnull(fish) then
                    if fish:getFishType() < 50 and not fish:isCi(prick:getBuffIndex()) then
                        fish:setCi(prick:getBuffIndex(), true)
                        table.insert(fishIDs, fish:getFishID())
                        fishNum = fishNum + 1
                        if fishNum >= 50 then
                            break
                        end
                    end
                end
            end
            if next(fishIDs) then
                g_GameController:reqBuffGet({ fishIDs, prick:getBuffIndex() })
            end
        end
    end
end


-- 获取场景中所有普通鱼id
function FishingScene:getCommonFishIds()
    local fishIDs = {}
    for h, fish in pairs(InScreenFishList) do
        if not tolua.isnull(fish) then
            if fish:getFingerling() == FishModel.FINGERLING.COMMON_FISH_TYPE then
                table.insert(fishIDs, fish:getFishID())
            end
        end
    end
    return fishIDs
end


-- 清除渔场中的鱼
function FishingScene:clearFishpond()
    for k, fish in pairs(AliveKeyForFishList) do
        if not tolua.isnull(fish) then
            fish:stopAllActions()
            fish:removeFromParent()
        end
    end
    AliveKeyForFishList = {}
    InScreenFishList = {}
    self.m_leftFishList = {}     -- 左边屏幕的鱼
    self.m_rightFishList = {}     -- 右边屏幕的鱼
end

-- 清除渔场中的子弹
function FishingScene:clearBullets()
    for k, bullet in pairs(AliveKeyForBulletList) do
        if not tolua.isnull(bullet) then
            bullet:stopAllActions()
            bullet:removeFromParent()
        end
    end
    AliveKeyForBulletList = {}
    self.m_leftBullets = {}     -- 左边屏幕的子弹
    self.m_rightBullets = {}     -- 右边屏幕的子弹
end

-- 清除渔场中buff
function FishingScene:clearBuffs()
    for k, fishBuff in pairs(self.m_fishBuffs) do
        if not tolua.isnull(fishBuff) then
            if fishBuff:getBuffType() == FishBuff.BUFFTYPE.TOXIC_SMOG_TYPE then     --超级炸弹
                if not tolua.isnull(self.m_toxicSmogs[k]) then
                    self.m_toxicSmogs[k]:stopAllActions()
                    self.m_toxicSmogs[k]:removeFromParent()
                    self.m_toxicSmogs[k] = nil
                    self.m_fishBuffs[k] = nil
                end
            elseif fishBuff:getBuffType() == FishBuff.BUFFTYPE.PART_BOMB_TYPE then  --局部炸弹
                if not tolua.isnull(self.m_partBombs[k]) then
                    self.m_partBombs[k]:stopAllActions()
                    self.m_partBombs[k]:removeFromParent()
                    self.m_partBombs[k] = nil
                    self.m_fishBuffs[k] = nil
                end
            elseif fishBuff:getBuffType() == FishBuff.BUFFTYPE.ELECTRIC_TYPE then    -- 电球
                if not tolua.isnull(self.m_electrics[k]) then
                    self.m_electrics[k]:stopAllActions()
                    self.m_electrics[k]:removeFromParent()
                    self.m_electrics[k] = nil
                    self.m_fishBuffs[k] = nil
                end
            end
        end
    end

end

-- 清除渔场
function FishingScene:clearFishFishery()
   if self.m_bIsInit ~= true then return end
   
    -- 清除渔场中的鱼
    self:clearFishpond()
    -- 清除渔场中的子弹
    self:clearBullets()
    -- 清除渔场中buff
    self:clearBuffs()

    self.fishMainLayer:clearView()
    EffectManager.clearFreeze()
    EffectManager.clearAllCritAni()
    FishSkillManager:getInstance():clearData()
end

function FishingScene:onExit()
    print("------------FishingScene:onExit begin------------")

    EffectManager.clearFreeze()
    EffectManager.removeEventListenerForNeedBlendSprs()
    EffectManager.removeResource()
    EffectManager.clearAllCritAni()
    EffectManager.removeAllAnimtion()
    self.m_fishManager:clearAllFishAnimation()
    self.fishMainLayer:onExit()
    EffectManager.removeAllFishResources()
    removeMsgCallBack(self, MSG_ENTER_FOREGROUND)
    removeMsgCallBack(self, MSG_ENTER_BACKGROUND)
    removeMsgCallBack(self, UPDATE_GAME_RESOURCE)
    --FishGameDataController:getInstance():clearData()
    --removeMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT)
    --removeMsgCallBack(self, POPSCENE_ACK)
    --removeMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT)
    -- FishingRoomController:getInstance():setNeedReconectGameServer( false )
    --  FishingRoomController:getInstance():setInFishingGame(  false )
    FishSkillManager:getInstance():onDestory()
    --FishSoundManager:getInstance():stopBGMusic()
    FishSoundManager:getInstance():stopAllSound()
    FishSoundManager:getInstance():onDestory()
    --g_GameController:onDestory()
    --if not FishingRoomController:getInstance():getLoadResByLoadView() then
    self:RemoveResources()
    self.m_bIsInit = false
    -- ends
    print("------------FishingScene:onExit end------------")
end

function FishingScene:hideAllDialog()
    -- 设置 
    if not tolua.isnull(self.m_fishMusicSetLayer) then
        self.m_fishMusicSetLayer:setVisible(false)
    end

    -- 鱼种界面
    if not tolua.isnull(self.m_fishIllustratedLayer) then
        self.m_fishIllustratedLayer:setVisible(false)
    end


    -- 显示玩家详细信息界面
    if not tolua.isnull(self.m_fishPlayerInfoLayer) then
        self.m_fishPlayerInfoLayer:setVisible(false)
    end

    -- 显示商城界面
    if not tolua.isnull(self.m_fishShopLayer) then
        self.m_fishShopLayer:setVisible(false)
    end

    if not tolua.isnull(self.m_fishShopBuyConfirmLayer) then
        self.m_fishShopBuyConfirmLayer:setVisible(false)
    end

end


function FishingScene:onBackButtonClicked()
    self:hideAllDialog()
    self:showFishExitGameLayer()
    -- if self.m_Acer == FishGlobal.isDropAcer then
    --    self.fishMainLayer:stopAutoFire()
    --    g_GameController:reqExchangeInfo( {} )
    -- else
    --   if  getFuncOpenStatus( FishGlobal.buyuRobHongBaoId ) == 1 then 
    --       self:showFishExitGameLayer()
    --   elseif getFuncOpenStatus( FishGlobal.buyuRobHongBaoId ) == 0 then
    --       self.fishMainLayer:stopAutoFire()
    --       g_GameController:reqExchangeInfo( {} )
    --   end
    -- end
end

function FishingScene:onEnterForeground()
    print("从后台切换到前台")
    g_GameController:setNeedWave(false)
    --FishingUtil:getInstance():startNetCheck( handler( self, self.showNetState))
    g_GameController:notifyDisableGameMsg(0)
end

-- 从前台切换到后台
function FishingScene:onEnterBackground()
    print("从前台切换到后台,游戏线程挂起!")
    g_GameController.m_BackGroudFlag = true

    self:clearFishpond()
    self:clearBullets()
    self:clearBuffs()
    EffectManager.unfreezeView(self)
    --FishingUtil:getInstance():stopNetCheck()
    self:hideAllLineAndAim()
    self:setLockTargetByChair(FishGlobal.myChair, nil)
    self:setLockLineAndAimVisibleByChair(FishGlobal.myChair, false)
    --  g_GameController:reqLockFishOper( { 0 }  )
    self.fishMainLayer:autoLockFireEnd()
    g_GameController:notifyDisableGameMsg(1)
end


function FishingScene:clearData(outType)
    --if FishGlobal.isShowDesk then      
    --sendMsg( FishGlobal.GameMsg.FISH_TOAD_ENTER_ROOM_MSG, __info  )   
    --end
    --g_GameController:exitGame( outType )
end

--[[function FishingScene:onExitGameScene( msgName )
    self:exitGame()
end

--退出游戏处理
function FishingScene:exitGame( outType )
    FishSoundManager:getInstance():stopBGMusic()
 --   g_GameController:reqLockFishOper( { 0 }  )
    self:clearData( outType )
    self:onExitGame()
end

function FishingScene:exitGameHall( outType )
    FishSoundManager:getInstance():stopBGMusic()
	g_GameController:reqExitGame( outType )
    --sendMsg( FishGlobal.GameMsg.FISH_EXIT_ROOM_HALL_MSG, {} )
    self:onExitGame()
end
--]]

return FishingScene