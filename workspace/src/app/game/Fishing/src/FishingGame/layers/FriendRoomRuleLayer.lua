--
-- FriendRoomRuleLayer
-- Author: phl
-- Date: 2017-08-21 21:10:37
--游戏规则

local FriendRoomRuleLayer = class("FriendRoomRuleLayer", function ()
    return display.newLayer()
end)

function FriendRoomRuleLayer:ctor()
    self:loadCsbFile()
end

function FriendRoomRuleLayer:loadCsbFile()

    local maskLayer = require("src.app.game.Fishing.src.FishingGame.layers.FishMaskLayer").new( function ()
        self:setVisible(false)
    end )
    self:addChild(maskLayer, -1 )

    self.node = cc.CSLoader:createNode("buyu_cs_game/buyu_game_rule_layer.csb")
    self:addChild(self.node)
    self.roomNode = self.node:getChildByName("buyu_system_set_panel")
    self.roomNode:setPositionY( self.roomNode:getPositionY() * display.scaleY )
    self.roomNode:setPositionX( self.roomNode:getPositionX() * display.scaleX )
    self:initVariableNode()
    self.roomNode:getChildByName("close_button"):addTouchEventListener(function()
        -- body
        self:setVisible(false)
    end)
    self.titleLabel = self.roomNode:getChildByName("title_label")
    self.titleLabel:enableOutline(cc.c4b(30, 70, 142, 255),2)
end

function FriendRoomRuleLayer:initVariableNode()
    local listView = self.roomNode:getChildByName("rule_scrollView")
    local label = listView:getChildByName("content_label")
    label:setVisible( false )
    local size = listView:getInnerContainerSize()
    local rule = require("src.app.Fishing.res.buyu_config.buyu_firendRoomRule")
    local ruleContentLabel = display.newTTFLabel( {
        text = rule, 
        font = "ttf/jcy.TTF",
        size = 26, 
        color = cc.c3b(186, 75, 26),
        dimensions = cc.size(900, 0),
    } )
    local ruleContentLabelSize = ruleContentLabel:getContentSize()
    listView:setInnerContainerSize( cc.size(920,  ruleContentLabelSize.height))
    ruleContentLabel:setPosition( 10, ruleContentLabelSize.height )
    ruleContentLabel:setAnchorPoint( cc.p(0,1) )
    listView:addChild( ruleContentLabel )

end

return FriendRoomRuleLayer