--
-- FishShopLayer
-- Author: chenzhanming
-- Date: 2017-06-14 19:08:37
-- 捕鱼商城界面
--
local  FishGlobal              =  require("src.app.game.Fishing.src.FishGlobal") 
local  FishGameDataController  =  require("src.app.game.Fishing.src.FishingCommon.controller.FishGameDataController")
local  FishSoundManager        =  require("src.app.game.Fishing.src.FishingCommon.controller.FishSoundManager")
local  FishMaskLayer           =  require("src.app.game.Fishing.src.FishingGame.layers.FishMaskLayer")


local FishShopLayer = class("FishShopLayer", function()
    return display.newLayer()
end)

FishShopLayer.closeBtnTAG    = 10001 -- 关闭按钮
FishShopLayer.goldShopTabTag = 10002 -- 充值金币
FishShopLayer.itemShopTabTag = 10003 -- 道具商城


function FishShopLayer:ctor()
    self.m_clickPosition = cc.p(0,0)
    self.isMove = false 
    self:init()
end

function FishShopLayer:init()
    self:setNodeEventEnabled( true )
    self:loadCsbFile()
    self:initVariableNodes()
    self:registerTouchOneByOneListener()
    ToolKit:registDistructor( self, handler(self, self.onDestory) )
end

-- 加载csb 文件
function FishShopLayer:loadCsbFile()
    local maskLayer = FishMaskLayer.new( function ()
        if not cc.rectContainsPoint(self.rect,self.m_clickPosition) then
           self:setVisible( false )  
        end  
    end )
    self:addChild(maskLayer, -1 )
    --item:setSwallowTouches( false )
    self.rootNode = cc.CSLoader:createNode("buyu_cs_game/buyu_shop_layer.csb")
    self:addChild( self.rootNode )
    local temp = self.rootNode:getChildren()
    for i=1,#temp do
        temp[i]:setPositionX(temp[i]:getPositionX()*display.scaleX)
        temp[i]:setPositionY(temp[i]:getPositionY()*display.scaleY)
    end
end


function FishShopLayer:initVariableNodes()
    ---cc.SpriteFrameCache:getInstance():addSpriteFrames("buyu_p_plist/p_buyu_room_common_1.plist","buyu_p_plist/p_buyu_room_common_1.png")
    self.shopPanel = self.rootNode:getChildByName( "buyu_shop_panel" )
    self.shopPanel:setSwallowTouches( false )
    self.shopPanelSize = self.shopPanel:getContentSize()
    self.rect = cc.rect(display.width/2-self.shopPanelSize.width/2 , display.height/2- self.shopPanelSize.height/2  , self.shopPanelSize.width , self.shopPanelSize.height)
    -- 关闭按钮
    self.closeBtn = self.shopPanel:getChildByName("close_button")
    self.closeBtn:setTag( FishShopLayer.closeBtnTAG )
    self.closeBtn:addTouchEventListener( handler( self, self.onTouchButtonCallBack ) )


    -- -- 充值金币
    -- self.buyGoldBtn = self.shopPanel:getChildByName("buy_gold_btn")
    -- self.buyGoldBtn:setTag( FishShopLayer.goldShopTabTag )
    -- self.buyGoldBtn:addTouchEventListener( handler( self, self.onTouchButtonCallBack ) )
    -- self.buyGoldBtn:setVisible(false)

    -- local buyGoldLabel = self.rootNode:getChildByName("but_gold_btn_label")
    -- buyGoldLabel:setVisible(false)
    

    -- -- 道具商城
    -- self.buyItemBtn = self.shopPanel:getChildByName("buy_item_btn")
    -- self.buyItemBtn:setTag( FishShopLayer.itemShopTabTag )
    -- self.buyItemBtn:addTouchEventListener( handler( self, self.onTouchButtonCallBack ) )
    -- self.buyItemBtn:setEnable( false )
    -- self.buyItemBtn:setGrayDisableMode( false )
    -- self.buyItemBtn:setVisible(false)

    -- local buyItemLabel = self.shopPanel:getChildByName("but_item_btn_label")
    -- buyItemLabel:setVisible(false)

    -- vip 
    self.vipNode = self.shopPanel:getChildByName("vip_node")
    self.vipNode:setVisible( false )

    -- 可以充值的金币列表
    self.rechargeScrollview = self.shopPanel:getChildByName("recharge_scrollview")
    self.rechargeScrollview:setVisible( false )

    -- 道具列表
    self.goodScrollview = self.shopPanel:getChildByName("goods_scrollView")
    local goodItemNode = self.goodScrollview:getChildByName("item_node")
    goodItemNode:setVisible( false )
    
    self.titleLabel = self.shopPanel:getChildByName("title_label")
    self.titleLabel:enableOutline(cc.c4b(30, 70, 142, 255),2)

    self:initItemListView()
end


-- 初始化道具列表
function  FishShopLayer:initItemListView()
    local buyuShop = FishGameDataController:getInstance():getBuyuShopDatas()
    local goodsItemtRootNode = display.newNode()
    self.goodScrollview:addChild( goodsItemtRootNode )
    self.goodScrollviewSize = self.goodScrollview:getContentSize()
    self.goodScrollview:setSwallowTouches( false )
    goodsItemtRootNode:setPosition( cc.p(120,0))
    local columnWidth = 232
    local rowHeight   = 0
    local column = 0
    for i,goodItem in ipairs( buyuShop ) do
        local width  = columnWidth * ( i -1)
        local height = rowHeight
        local itemNode = display.newNode()
        itemNode:setPosition( cc.p( width , height ) )
        goodsItemtRootNode:addChild( itemNode )
  
        -- 背景
        local itemBgBtn  = ccui.Button:create( "fish_shop_bg_bar.png", "fish_shop_bg_bar.png" ,"fish_shop_bg_bar.png",ccui.TextureResType.plistType) 
        itemBgBtn:setAnchorPoint(cc.p(0.5,0.5))
        itemBgBtn:setTag( goodItem.nIndex )
        itemBgBtn:setSwallowTouches( false )
        itemBgBtn:setPosition( cc.p(0,190) )
        self:onTouchBtnEvent( itemBgBtn )
        itemNode:addChild( itemBgBtn )

        -- 商品名称
        local goodItemNameLabel = display.newTTFLabel({
                text = tostring( goodItem.name ),
                font = "ttf/jcy.TTF",
                size = 24,
                color = cc.c3b(24,109, 175),
        })
        goodItemNameLabel:setAnchorPoint( cc.p(0.5,0.5) )
        goodItemNameLabel:setPosition( cc.p(0 , 353  ) )
        itemNode:addChild( goodItemNameLabel )

        -- 商品图标
        local goodsSpr = display.newSprite("#"..goodItem.szItemSource)
        goodsSpr:setAnchorPoint( cc.p(0.5,0.5) )
        goodsSpr:setPosition( cc.p( 0,200 ) )
        itemNode:addChild(  goodsSpr )
        
        if "永久" ~= goodItem.validTime then
            -- 限时背景
            local limtTimeSpr = display.newSprite("#fish_shop_img_date.png")
            limtTimeSpr:setAnchorPoint(cc.p(0.5,0.5))
            limtTimeSpr:setPosition( cc.p( 75 ,285 ) )
            itemNode:addChild( limtTimeSpr )
           
            -- 限时文本
            local limitTimeLabel = display.newTTFLabel({
                    text = tostring( goodItem.validTime ),
                    font = "ttf/jcy.TTF",
                    size = 26,
                    color = cc.c3b(255,255, 255),
            })
            limitTimeLabel:setAnchorPoint( cc.p(0.5,0.5) )
            limitTimeLabel:setPosition( cc.p( 73 ,292 ) )
            itemNode:addChild( limitTimeLabel )
        end
        
        -- 金币标志
        local goldInconSpr = display.newSprite("#buyu_dt_gold.png")
        goldInconSpr:setAnchorPoint( cc.p(0,0.5) )
        goldInconSpr:setPosition( cc.p( -46,45 ) )
        goldInconSpr:setScale( 0.75 )
        itemNode:addChild(  goldInconSpr )
        local  goldInconSprSize = goldInconSpr:getContentSize()

        -- 金币数量
        local goldNumLabel = display.newTTFLabel({
                text = tostring( goodItem.nPrice  ),
                font = "ttf/jcy.TTF",
                size = 40,
                color = cc.c3b(167,68, 5),
        })
        goldNumLabel:setAnchorPoint( cc.p(0,0.5) )
        goldNumLabel:setPosition( cc.p(-29 , 45) )
        itemNode:addChild( goldNumLabel )
        local goldNumLabelSize = goldNumLabel:getContentSize()

        local totalWidth =  goldInconSprSize.width* 0.75 + goldNumLabelSize.width
        goldInconSpr:setPositionX( -totalWidth/2 )
        goldNumLabel:setPositionX( -totalWidth/2 + goldInconSprSize.width* 0.75  )

        column = i
    end 

    local scrollHeight = rowHeight
    local scrollwidth =  columnWidth * column 
    self.goodScrollview:setInnerContainerSize(cc.size(scrollwidth, scrollHeight))
    
end

function FishShopLayer:jumpToPercentHorizontal( itemId  )
    local goodItem =  FishGameDataController:getInstance():getGoodsItemDataByItemId( itemId  )
    local buyuShops = FishGameDataController:getInstance():getBuyuShopDatas()
    if goodItem then
       local percent = (goodItem.nIndex + 1.7)/table.nums( buyuShops ) * 100
       self.goodScrollview:jumpToPercentHorizontal( percent)
    end
end

-- 触摸事件监听
function FishShopLayer:registerTouchOneByOneListener()
    local listener = cc.EventListenerTouchOneByOne:create()
    listener:registerScriptHandler(handler(self,self.onTouchMoved),cc.Handler.EVENT_TOUCH_MOVED)
    listener:registerScriptHandler(handler(self,self.onTouchBegan),cc.Handler.EVENT_TOUCH_BEGAN)
    listener:registerScriptHandler(handler(self,self.onTouchEnded),cc.Handler.EVENT_TOUCH_ENDED)
    listener:registerScriptHandler(handler(self,self.onTouchCancelled),cc.Handler.EVENT_TOUCH_CANCELLED)
    self:getEventDispatcher():addEventListenerWithSceneGraphPriority(listener,self.shopPanel)
end

function FishShopLayer:onTouchBegan(touch, evnt)
      local touchPos = touch:getLocation()
      -- 触摸点击的位置
      self.m_clickPosition = touchPos
      -- print(":onTouchBegan")
      -- dump( self.m_clickPosition )
end

function  FishShopLayer:onTouchMoved(touch,event)
      local touchPos = touch:getLocation()
      self.m_clickPosition = touchPos
      -- print("FishMainLayer:onTouchMoved")
      -- dump( self.m_clickPosition  )
end

function  FishShopLayer:onTouchEnded(touch, evnt)
    local touchPos = touch:getLocation()
    self.m_clickPosition = touchPos
    -- print("FishMainLayer:onTouchEnded")
    -- dump( self.m_clickPosition )
end


function  FishShopLayer:onTouchCancelled(touch, evnt)

end

function FishShopLayer:onTouchShopItemButtonCallBack(sender,eventType)
    if sender and sender:getTag() then
        --wordPos display.width/2 -  self.goodScrollviewSize.width/2             display.width/2 + self.goodScrollviewSize.width/2
        -- local size = sender:self:getContentSize()
        -- wordPos + size.width/2
        if self.m_clickPosition.x >= (display.width/2 -self.goodScrollviewSize.width/2  ) and self.m_clickPosition.x <=  (self.goodScrollviewSize.width/2 + display.width/2) then
            local tag = sender:getTag()
            print("local tag = sender:getTag()")
            if eventType == ccui.TouchEventType.began then
                print("eventType == ccui.TouchEventType.began")
                FishSoundManager:getInstance():playEffect( FishSoundManager.ButtonPressed )
                self.isMove = false 
            elseif eventType == ccui.TouchEventType.moved then
                self.isMove  = true
                print("ccui.TouchEventType.moved")
            elseif  eventType == ccui.TouchEventType.ended  then
                print("eventType == ccui.TouchEventType.ended")
                print("self.isMove=",self.isMove)
                if self.isMove == false then
                   self:showShopBuyConfirmLayer(  tag )    
                end
                self.isMove = false 
            end
        end
    end
end

function FishShopLayer:onTouchBtnEvent( btn )
    btn:setSwallowTouches(false)
    local listenner = cc.EventListenerTouchOneByOne:create()
    listenner:setSwallowTouches(false)
    listenner:registerScriptHandler(function(touch, event)
        if btn:getCascadeBoundingBox():containsPoint(touch:getLocation()) then
            btn._begin_point = touch:getLocation()
            btn._move_len = 0
            return true
        end
        return false
    end,cc.Handler.EVENT_TOUCH_BEGAN )
    listenner:registerScriptHandler(function(touch, event)
        if btn._begin_point ~= nil then
            local pos = touch:getLocation()
            btn._move_len = ToolKit:distance(btn._begin_point, pos)
        end
    end,cc.Handler.EVENT_TOUCH_MOVED)
    listenner:registerScriptHandler(function(touch, event)
        if btn._move_len ~= nil and btn._move_len < 5 then
            local target = event:getCurrentTarget()
            self:onTouchCallback( target )
        end
        btn._begin_point = nil
        btn._move_len = nil
    end,cc.Handler.EVENT_TOUCH_ENDED )
    local _eventDispatcher = cc.Director:getInstance():getEventDispatcher()
    _eventDispatcher:addEventListenerWithSceneGraphPriority(listenner, btn)
end

-- 点击事件回调
function  FishShopLayer:onTouchCallback( sender )
    if sender and sender:getTag()  then
       if self:isVisible() then
       print("FishShopLayer:onTouchCallback")
         if self.m_clickPosition.x >= (display.width/2 -self.goodScrollviewSize.width/2  ) and self.m_clickPosition.x <=  (self.goodScrollviewSize.width/2 + display.width/2) then
            local tag = sender:getTag()
            FishSoundManager:getInstance():playEffect( FishSoundManager.ButtonPressed )
            self:showShopBuyConfirmLayer(  tag )    
         end
       end
    end
end


function FishShopLayer:showShopBuyConfirmLayer(  index )
    self:getParent():showShopBuyConfirmLayer( FishGlobal.BUYTYPE.normal ,index )
end


function FishShopLayer:onTouchButtonCallBack(sender,eventType)
    if sender and sender:getTag() then
        local tag = sender:getTag()
        if tag == FishShopLayer.closeBtnTAG then
           self:setVisible( false )
        elseif tag == FishShopLayer.goldShopTabTag then
            --self.buyGoldBtn:setEnable( false )
            --self.buyGoldBtn:setGrayDisableMode( false )   
            self.buyItemBtn:setEnable( true )
            --self.rechargeScrollview:setVisible(true)
            self.goodScrollview:setVisible(false)
            TOAST("暂未开放!")
        elseif tag == FishShopLayer.itemShopTabTag then
            self.buyGoldBtn:setEnable( true )
            self.buyItemBtn:setEnable( false )
            self.buyItemBtn:setGrayDisableMode( false )   
            self.goodScrollview:setVisible( true )
        end
    end
end


function FishShopLayer:onExit()
   
end

function FishShopLayer:onDestory()
     print("FishShopBuyConfirmLayer:onDestory()")
end


function FishShopLayer:onExit()
  
end



return FishShopLayer