--
-- FishChatSystemLayer
-- Author: chenzhanming
-- Date: 2017-06-10 14:08:37
-- 捕鱼聊天系统界面
--
local  FishGlobal              =  require("src.app.game.Fishing.src.FishGlobal")
local  scheduler               =  require("framework.scheduler")
--local  FishGameController      =  require("src.app.game.Fishing.src.FishingGame.controller.FishGameController")
local  FishGameDataController  =  require("src.app.game.Fishing.src.FishingCommon.controller.FishGameDataController")
local  FishSoundManager      = require("src.app.game.Fishing.src.FishingCommon.controller.FishSoundManager")


local FishChatSystemLayer = class("FishChatSystemLayer", function()
    return display.newLayer()
end)

local ani_list = 
{
    ["boom"] = {name = "common_exp_boom",dy1=0,dy2=0 , soundPath = "fish_magic_boom.mp3"},
    ["chicken"] = {name = "common_exp_chicken",dy1=-20,dy2=0 , soundPath = "fish_magic_chicken.mp3" },
    ["diutuoxie"] = {name = "common_exp_slipper",dy1=0,dy2=0 , soundPath = "fish_magic_hitface.mp3" },
    ["jidan"] = {name = "common_exp_egg",dy1=20,dy2=20 , },
    ["poshui"] = {name = "common_exp_splash",dy1=-50,dy2=0 ,  },
    ["songchaopiao"] = {name = "common_exp_money",dy1=-120,dy2=0 ,  },
    ["xianhua"] = {name = "common_exp_flower",dy1=0,dy2=0 , soundPath = "fish_magic_folwer.mp3" },
}
local armature_path = "src/app/game/common/chat/expression/"
local suffix = ".ExportJson"
local __nameList = {}


FishChatSystemLayer.dialogBoxCount = 6

function FishChatSystemLayer:ctor()
    self:init()
end

function FishChatSystemLayer:init()
    self:setNodeEventEnabled( true )
    self.chatContenHideTimer = {}
    self.isMove = false 
    self.usersTable = {}
    self:loadCsbFile()
    self:initPlayerChatView()
    self:initChatContentList()
end

-- 加载csb 文件
function FishChatSystemLayer:loadCsbFile()
     local function closeCallback()
        if self.chatBoxNode then
           self.chatBoxNode:setVisible( false )
           self.menu:setVisible( false )
        end
    end
    local item = cc.MenuItemImage:create()
    item:setContentSize(cc.size(display.width, display.height))
    item:registerScriptTapHandler(closeCallback)
    self.menu = cc.Menu:create(item)
    self:addChild( self.menu )
    self.menu:setVisible( false )

    self.rootNode = cc.CSLoader:createNode("buyu_cs_game/buyu_game_chat_layer.csb")
    self:addChild( self.rootNode )
    local temp = self.rootNode:getChildren()
    for i=1,#temp do
        temp[i]:setPositionX(temp[i]:getPositionX()*display.scaleX)
        temp[i]:setPositionY(temp[i]:getPositionY()*display.scaleY)
    end
end



-- 初始化玩家各个玩家对话内容框
function  FishChatSystemLayer:initPlayerChatView()
   self.playChatNodes = {}
   for i = 1,FishChatSystemLayer.dialogBoxCount do
       self.playChatNodes[i] = {}
       local chatNodeNameStr = string.format("dialogue_frame_node%d",i)
       self.playChatNodes[i].chatNode      = self.rootNode:getChildByName( chatNodeNameStr )
       self.playChatNodes[i].dialogueBg    = self.playChatNodes[i].chatNode:getChildByName("dialogue_bg")
       self.playChatNodes[i].dialogueLabel = self.playChatNodes[i].chatNode:getChildByName("dialogue_label")
       self.playChatNodes[i].chatNode:setVisible( false )
        if i <= 3 then
             self.playChatNodes[i].chatNode:setPositionY( self.playChatNodes[i].chatNode:getPositionY()/display.scaleY )
        end
   end
end


-- 初始化聊天内容列表
function  FishChatSystemLayer:initChatContentList()
    self.chatBoxNode = self.rootNode:getChildByName("chat_box_node")
    self.chatBoxNode:setVisible( false )
    self.chatBoxScrollView = self.chatBoxNode:getChildByName("chat_box_scrollView")
    local dialogueBg = self.chatBoxScrollView:getChildByName("dialogue_bg")
    dialogueBg:setVisible(false)
    local chatContentRootNode = display.newNode()
    --chatContentRootNode:setPosition( cc.p(0,530) )
    self.chatBoxScrollView:addChild( chatContentRootNode )
    
    local columnWidth = 160
    local rowHeight   = 68
    local row = 0
    local buyuChat = FishGameDataController:getInstance():getBuyuChatDatas()
    for i,chatData in ipairs( buyuChat ) do
        local width  = columnWidth
        local height = -34 + (i-1) * rowHeight * (-1)
        local itemNode = display.newNode()
        itemNode:setPosition( cc.p( width , height ) )
        chatContentRootNode:addChild( itemNode )
        
        -- 文字背景
        local messageBtn  = ccui.Button:create( "fish_btn_lt.png", "fish_btn_lt.png" ,"fish_btn_lt.png",ccui.TextureResType.plistType) 
        messageBtn:setScale9Enabled( true )
        messageBtn:setCapInsets(cc.rect(20,20,20,20))
        messageBtn:setContentSize(cc.size(320,62))
        messageBtn:ignoreContentAdaptWithSize( true )
        messageBtn:setAnchorPoint(cc.p(0.5,0.5))
        itemNode:addChild( messageBtn )
        messageBtn:setTag( chatData.nIndex )
        messageBtn:setSwallowTouches( false )
        --messageBtn:addTouchEventListener(  handler(self,self.onTouchButtonCallBack))
        self:onTouchBtnEvent( messageBtn )

        -- 聊天内容
         local chatContentLabel = display.newTTFLabel({
                text = tostring( chatData.szChatContent ),
                font = "ttf/jcy.TTF",
                size = 24,
                color = cc.c3b(255, 255, 255),
            })
        chatContentLabel:setAnchorPoint( cc.p(0,0.5) )
        chatContentLabel:setPositionX( -145 )
        itemNode:addChild( chatContentLabel )
        row = i
    end
    
    local scrollHeight = row * rowHeight + 34
    local scrollwidth = 338
    self.chatBoxScrollView:setInnerContainerSize(cc.size(scrollwidth, scrollHeight))
    chatContentRootNode:setPosition( cc.p(0,scrollHeight) )
    
end


function FishChatSystemLayer:onTouchButtonCallBack(sender,eventType)
    if sender and sender:getTag() then
        local tag = sender:getTag()
        if eventType == ccui.TouchEventType.began then
            FishSoundManager:getInstance():playEffect( FishSoundManager.ButtonPressed )
            self.isMove = false 
        elseif eventType == ccui.TouchEventType.moved then
            self.isMove = true
        elseif  eventType == ccui.TouchEventType.ended  then
           if not self.isMove then
              self:showMessage( FishGlobal.myViewChair, tag )
              g_GameController:reqChat({ 1 , FishGlobal.chatType.text , tag } )
              self:setChatBoxVisble( false )
           end
        end
    end
end


function FishChatSystemLayer:onTouchBtnEvent( btn )
    btn:setSwallowTouches(false)
    local listenner = cc.EventListenerTouchOneByOne:create()
    listenner:setSwallowTouches(false)
    listenner:registerScriptHandler(function(touch, event)
        if btn:getCascadeBoundingBox():containsPoint(touch:getLocation()) then
            btn._begin_point = touch:getLocation()
            btn._move_len = 0
            return true
        end
        return false
    end,cc.Handler.EVENT_TOUCH_BEGAN )
    listenner:registerScriptHandler(function(touch, event)
        if btn._begin_point ~= nil then
            local pos = touch:getLocation()
            btn._move_len = ToolKit:distance(btn._begin_point, pos)
        end
    end,cc.Handler.EVENT_TOUCH_MOVED)
    listenner:registerScriptHandler(function(touch, event)
        if btn._move_len ~= nil and btn._move_len < 5 then
            local target = event:getCurrentTarget()
            self:onTouchCallback( target )
        end
        btn._begin_point = nil
        btn._move_len = nil
    end,cc.Handler.EVENT_TOUCH_ENDED )
    local _eventDispatcher = cc.Director:getInstance():getEventDispatcher()
    _eventDispatcher:addEventListenerWithSceneGraphPriority(listenner, btn)
end

-- 点击事件回调
function  FishChatSystemLayer:onTouchCallback( sender )
    if sender and sender:getTag() then
        if self.chatBoxNode:isVisible()  then
          FishSoundManager:getInstance():playEffect( FishSoundManager.ButtonPressed )
          local tag = sender:getTag()
          self:showMessage( FishGlobal.myViewChair, tag )
          g_GameController:reqChat({ 1 ,1, tag } )
          self:setChatBoxVisble( false )
        end
    end
end


function FishChatSystemLayer:showMessage( viewChair, content )
    local contentIndex = tonumber( content )
    if viewChair >= 1 and viewChair <= 6 then
       FishSoundManager:getInstance():playEffect( FishSoundManager.ChatPaopao )
       self.playChatNodes[ viewChair ].chatNode:setVisible( true )
       local _content =  FishGameDataController:getInstance():getChatContentByIndex( contentIndex )
       self.playChatNodes[ viewChair ].dialogueLabel:setString( _content )
    end
   local function  hideMessage()
      self.playChatNodes[ viewChair ].chatNode:setVisible( false )
   end
   self:hideContentBegin( viewChair , hideMessage )
end

function FishChatSystemLayer:hideContentBegin( viewChair , handler )
    if self.chatContenHideTimer[ viewChair ] then
       scheduler.unscheduleGlobal( self.chatContenHideTimer[ viewChair ] )
      self.chatContenHideTimer[ viewChair ] = nil
    end

    self.chatContenHideTimer[ viewChair ] = scheduler.scheduleGlobal(  handler  , 2 )
end

function FishChatSystemLayer:hideContentEnd( viewChair )
    if not self.chatContenHideTimer[ viewChair ] then
        return
    end
    scheduler.unscheduleGlobal( self.chatContenHideTimer[ viewChair ] )
    self.chatContenHideTimer[ viewChair ] = nil
end

function FishChatSystemLayer:setChatBoxVisble( isVisible )
    self.chatBoxNode:setVisible( isVisible )
    self.menu:setVisible( isVisible )
end

function  FishChatSystemLayer:onHandleAmusingByServer( strdata )
    local strlist = string.split(strdata, ',')
    self:showAmusingAniToUser(strlist[1],tonumber(strlist[2]),tonumber(strlist[3]))
end

function  FishChatSystemLayer:onReciveAmusing( data)
    local tag = data.tag
    local send_userId = data.send_userId
    local recive_userId = data.recive_userId
    self:showAmusingAniToUser(tag,send_userId,recive_userId)
    --local strdata = tag .. "," .. tostring(send_userId) .. "," .. tostring(recive_userId)
    --local strdata_len = string.len(strdata)
    --self:sendToServer(ChatSystemLayer.CLICK_HANDLER_AMUSING_TYPE,4,self.userId,strdata_len,strdata)
end


--加载骨骼动画数据
function  FishChatSystemLayer:loadArmature(name)
    if __nameList[name] then return end
    local a_path = armature_path..name.."/"..name..suffix
    print("load armature:",name..suffix)
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(a_path)
    __nameList[name]= true
end

--清除骨骼数据
function   FishChatSystemLayer:clearArmature(name)
    local a_path = armature_path..name.."/"..name..suffix
    print("clear armature:",name..suffix)
    ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo(a_path)
    __nameList[name]= nil
end

function FishChatSystemLayer:unLoadAll()
    local temp = clone(__nameList)
    for name,v in pairs(temp) do
      self:clearArmature(name)
    end
    __nameList = {}
end

--动画播放回调
function  FishChatSystemLayer:animationEvent( armature, movementType, movementID )
    --非循环播放一次
    if movementType == ccs.MovementEventType.complete or movementType == ccs.MovementEventType.loopComplete then
    
    if not armature.isLoop then
      armature:removeFromParent()
    end 
    if armature.callback then
      armature.callback(aramture,"complete")
    end
    end
end

--播放动画
function  FishChatSystemLayer:playArmature(name,animationName,callback,isLoop)
    print("play armature:",name)
    self:loadArmature(name)
    local armature = ccs.Armature:create(name)
    if armature then 
          local animationData = armature:getAnimation():getAnimationData()
          local data = animationData:getMovement(animationName)
          if data then
          armature:getAnimation():play(animationName,-1,1)
          else
              armature:getAnimation():playWithIndex(0,-1,1)
          end
      armature:getAnimation():setMovementEventCallFunc(handler(self,self.animationEvent))
      armature.callback = callback
          armature.isLoop = isLoop
      return armature
    end
end


function  FishChatSystemLayer:showAmusingAniToUser(tag,send_userId,recive_userId)
    local send_user = self.usersTable[send_userId]
    local recive_user = self.usersTable[recive_userId]
    
     if send_user == nil or recive_user == nil then
          print("self.usersTable[send_userId] == nil:", send_userId)
          print("self.usersTable[recive_userId] == nil:", recive_userId)
          return
      end
    
    if send_user == recive_user then
      print("can't send to myself")
      return 
    end
    

    local data = ani_list[tag]
    if not data then 
      print("don't have ani :", tag)
      return 
    end
    local send_pos = clone(send_user.expressionPos)
    local recive_pos =clone(recive_user.expressionPos)
    
    send_pos.y = send_pos.y + data.dy1
    recive_pos.y = recive_pos.y + data.dy1
    local name = data.name
    local ani = self:playArmature(name,"move",nil,true)
    self:addChild(ani)
    ani:setPosition(send_pos.x,send_pos.y)
    
    if tag=="diutuoxie" then
      if send_pos.y>recive_pos.y then
        ani:setScaleY(-1)
      end
      if send_pos.x>recive_pos.x then
        ani:setScaleX(-1)
      end
    end
    
    FishSoundManager:getInstance():playEffect( data.soundPath )
    local function ani_end()
      ani.isLoop = false
      local animationName = "end"
      local animationData = ani:getAnimation():getAnimationData()
      local ani_data = animationData:getMovement(animationName)
          if ani_data then
          ani:getAnimation():play(animationName,-1,1)
          else
              ani:getAnimation():playWithIndex(0,-1,1)
          end
      local recive_pos =clone(recive_user.expressionPos)
      recive_pos.y = recive_pos.y + data.dy2
      ani:setPositionY(recive_pos.y)
      ani:setScaleY(1)
      ani:setScaleX(1)
    end
    local dis = cc.pGetDistance(send_pos,recive_pos)
    local t = 0.5*dis/1000
    local act = {}
    act[1] = cc.MoveTo:create(t,recive_pos)
    act[2] = cc.CallFunc:create(ani_end)
    local action = cc.Sequence:create(act)
    ani:runAction(action)
end

-- 设置一个玩家显示表情
function FishChatSystemLayer:setUserIDPos( params )
     self.usersTable[params.userID] = {
           expressionPos = params.expressionPos or  cc.p(0,0) 
     }
end

function FishChatSystemLayer:onExit()
    for i=1,FishChatSystemLayer.dialogBoxCount do
        self:hideContentEnd( i )
    end
    self:unLoadAll()
end



return FishChatSystemLayer