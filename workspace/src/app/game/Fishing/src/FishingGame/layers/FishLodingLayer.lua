local CommonLoading = require("src.app.newHall.layer.CommonLoading")

local FishLodingLayer = class("FishLodingLayer", function()
    return CommonLoading.new()
end)

local FishRes = require("app.game.Fishing.src.FishingGame.scene.FishRes")

local PATH_CSB = "hall/csb/CommonLoading.csb"
local PATH_BG = "buyu_p_texture/gui-image-room.jpg"
local PATH_LOGO1 = "buyu_p_texture/logo-1.png"
local PATH_LOGO2 = "buyu_p_texture/logo-2.png"

function FishLodingLayer.loading()
    return FishLodingLayer.new(true)
end

function FishLodingLayer.reload()
    return FishLodingLayer.new(false)
end

function FishLodingLayer:ctor(bBool)
    self:setNodeEventEnabled(true)
    self.bLoad = bBool
    self:init()
end

function FishLodingLayer:init()
    --self.super:init(self)
    self:initCSB()
    self:initCommonLoad()
    if cc.exports.g_SchulerOfLoading then
        scheduler.unscheduleGlobal(cc.exports.g_SchulerOfLoading)
        cc.exports.g_SchulerOfLoading = nil
    end

    self:startLoading()
end


function FishLodingLayer:initCSB()

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    --ccb
    self.m_pathUI = cc.CSLoader:createNode(PATH_CSB)
    self.m_pathUI:setPositionX((display.width - 1624) / 2)
    self.m_pathUI:addTo(self.m_rootUI)

    --node
    self.m_pNodeBase = self.m_pathUI:getChildByName("Layer_base")
    self.m_pNodeBg   = self.m_pNodeBase:getChildByName("Node_bg")
    self.m_pNodeLoad = self.m_pNodeBase:getChildByName("Node_load")
    self.m_pNodeText = self.m_pNodeBase:getChildByName("Node_text")

    --bar
    self.m_pLoadingBar = self.m_pNodeLoad:getChildByName("LoadingBar")

    --text
    self.m_pLabelPercent = self.m_pNodeText:getChildByName("Text_percent")
    self.m_pLabelWord    = self.m_pNodeText:getChildByName("Text_word")

    --image
    self.m_pImageLogo = self.m_pNodeBg:getChildByName("Image_logo")
    self.m_pImageBg   = self.m_pNodeBg:getChildByName("Image_bg")
    self.m_pImageBg:loadTexture(PATH_BG, ccui.TextureResType.localType)

    self.m_pImageLogo:loadTexture(PATH_LOGO1, ccui.TextureResType.localType)
    self.m_pImageLogo:setContentSize(cc.size(413, 395))
end

function FishLodingLayer:initCommonLoad()
    
    -------------------------------------------------------
    --设置界面ui
    self:setLabelPercent(self.m_pLabelPercent) --百分比文字
    --self:setLabelWord(self.m_pLabelWord)       --提示文字
    self:setBarPercent(self.m_pLoadingBar)     --进度条
    -------------------------------------------------------
    local func = {};
    table.insert(func, function()
        CacheManager:putCSB("buyu_cs_game/buyu_game_main_layer.csb")
        -- CacheManager:putCSB("game/frogfish/csb/gui-fish-main.csb")
        -- CacheManager:putCSB("game/frogfish/csb/gui-fish-pao.csb")

        -- CacheManager:putCSB("game/frogfish/csb/gui-fish-roomChoose.csb")
        -- CacheManager:putCSB("game/frogfish/csb/gui-fish-roomLayer.csb")
    end)

    --播放背景音
    table.insert(func, function()
        -- AudioManager:getInstance():playMusic("game/frogfish/sound/fish-bg-01.mp3");
    end);
    
    --音效/音乐/骨骼/动画/动画/碎图/大图/其他
    self:addLoadingList(FishRes.vecLoadingPlist,  self.TYPE_PLIST)
    self:addLoadingList(FishRes.vecLoadingImage,  self.TYPE_PNG)
    self:addLoadingList(FishRes.vecLoadingAnim,   self.TYPE_EFFECT)
    self:addLoadingList(FishRes.vecLoadingMusic,  self.TYPE_MUSIC)
    self:addLoadingList(FishRes.vecLoadingSound,  self.TYPE_SOUND)
    self:addLoadingList(func,                     self.TYPE_OTHER)
    -------------------------------------------------------
end


function FishLodingLayer:closeView()
    self.m_pathUI:runAction(
        cc.Sequence:create(
            cc.DelayTime:create(0.2),
            cc.FadeOut:create(0.5),
            cc.CallFunc:create(function()
                self:getParent():removeChild(self);
            end),
        nil)
    )
end

return FishLodingLayer