--
-- FishPlayerInfoLayer
-- Author: chenzhanming
-- Date: 2017-06-10 14:08:37
-- 捕鱼玩家详细信息界面
--
local  FishGlobal           =  require("src.app.game.Fishing.src.FishGlobal")
local  scheduler            =  require("framework.scheduler")
local  FishSoundManager     =  require("src.app.game.Fishing.src.FishingCommon.controller.FishSoundManager")
local  FishMaskLayer        =  require("src.app.game.Fishing.src.FishingGame.layers.FishMaskLayer") 
local  FishGameDataController       =  require("src.app.game.Fishing.src.FishingCommon.controller.FishGameDataController")

local FishPlayerInfoLayer = class("FishPlayerInfoLayer", function()
    return display.newLayer()
end)


function FishPlayerInfoLayer:ctor(  roomType )
    self.roomType = roomType
    self.operationCool = false
    self:init()
end

function FishPlayerInfoLayer:init()
    self:setNodeEventEnabled( true )
    self:loadCsbFile()
    self:initPlayerInfoView()
    self.accoundId = 0 
    self.leftTime = 0
    self.freeTimes = 0
    self.emojiCoin = 0
    if self.roomType == FishGlobal.RoomType.System  or self.roomType == FishGlobal.RoomType.GoldIngot or self.roomType == FishGlobal.RoomType.ClubCity then
       self.freeTimes = g_GameController.m_emojiFreeTimes
       self.emojiCoin = g_GameController.m_emojiCoin
       self.attrData =  FishGameDataController:getInstance():getAttrData()
    elseif self.roomType == FishGlobal.RoomType.Friends or self.roomType == FishGlobal.RoomType.ClubFriend  then
       self.freeTimes = FishFriendsGameController:getInstance().m_emojiFreeTimes
       self.emojiCoin = FishFriendsGameController:getInstance().m_emojiCoin
       self.attrData =  FishGameDataController:getInstance():getAttrFriendData()
    end
    if self.attrData then
       self.freeTimeLabel1:setString( self.attrData.nMagicTime )
    end
    self.goldCountLabel:setString( self.emojiCoin )
    self:updateEnjoyAinView( self.freeTimes )
end

-- 加载csb 文件
function FishPlayerInfoLayer:loadCsbFile()
    local maskLayer = FishMaskLayer.new( function ()
        self:setVisible( false )
    end )
    self:addChild(maskLayer, -1 )

    self.rootNode = cc.CSLoader:createNode("buyu_cs_game/buyu_player_message_layer.csb")
    self:addChild( self.rootNode )
    local temp = self.rootNode:getChildren()
    for i=1,#temp do
        temp[i]:setPositionX(temp[i]:getPositionX()*display.scaleX)
        temp[i]:setPositionY(temp[i]:getPositionY()*display.scaleY)
    end
end

function FishPlayerInfoLayer:playAniTimerBegin()
    if self.playAniTimer then
        scheduler.unscheduleGlobal( self.playAniTimer )
        self.playAniTimer = nil 
    end
    self.leftTime = 3
    self.playAniTimer = scheduler.scheduleGlobal(handler(self,self.aniTimeCountDown), 1 )
end

function FishPlayerInfoLayer:playAniTimerEnd()
    if not self.playAniTimer then
        return
    end
    self.leftTime = 0 
    scheduler.unscheduleGlobal(self.playAniTimer)
    self.playAniTimer = nil
end

function FishPlayerInfoLayer:aniTimeCountDown()
     self.leftTime =  self.leftTime  - 1 
     if self.leftTime <= 0 then
        self:playAniTimerEnd()
     end
end

-- 初始玩家详细信息
function  FishPlayerInfoLayer:initPlayerInfoView()
   local buyuPlayerInfoPanel = self.rootNode:getChildByName("buyu_player_message_panel")
   local playerHeadNode = buyuPlayerInfoPanel:getChildByName("player_head_node")
   -- 玩家头像背景
   self.headBg = playerHeadNode:getChildByName("head_bg")

   --玩家性别
   self.sexImg = playerHeadNode:getChildByName("sex_image")

   -- 玩家账号id
   self.accountLabel = playerHeadNode:getChildByName("id_number_label")
   self.accountLabel:setVisible(false)

   self.idLabel = playerHeadNode:getChildByName("id_label")
   self.idLabel:setVisible( false )
   
   local playerMessageNode = buyuPlayerInfoPanel:getChildByName("player_message_node")
   -- 玩家昵称
   self.nickNameLabel = playerMessageNode:getChildByName("name_label")
   self.nickNameLabel:setFontName("Helvetica")
   --vip
   self.vipLabel = playerMessageNode:getChildByName("vip_level_label")
   self.vipLabel:setVisible( false )
  
   self.vipTitleLabel = playerMessageNode:getChildByName("vip_title_label")
   self.vipTitleLabel:setVisible( false )

   self.powerTitleLabel = playerMessageNode:getChildByName("power_title_label")
   self.goldTitleLabel = playerMessageNode:getChildByName("gold_title_label")

   -- 炮台最高倍数
   self.cannonsMaxScoreLabel = playerMessageNode:getChildByName("power_score_label")

   -- 拥有的金币数量
   self.playGoldLabel = playerMessageNode:getChildByName("gold_score_label")

    -- 关闭按钮
    self.closeBtn =  buyuPlayerInfoPanel:getChildByName("close_button")
    self.closeBtn:addTouchEventListener( handler(self,self.onTouchButtonCallBack)  )

   -- 魔法表情
   self.magicExpressionNode = buyuPlayerInfoPanel:getChildByName("magic_expression_node")
   self.tipsNode_1 = self.magicExpressionNode:getChildByName("tips_node1")
   -- 消耗金币数量
   self.goldCountLabel = self.tipsNode_1:getChildByName("gold_count_label")
   self.tipsNode_1:setVisible(false)

   self.tipsNode_2 = self.magicExpressionNode:getChildByName("tips_node2")
   self.freeTimeLabel1 = self.tipsNode_2:getChildByName("free_count_label1")
   -- 免费次数
   self.freeTimeLabel2 = self.tipsNode_2:getChildByName("free_count_label2")
  
   --表情图标
   self.expressionNode = buyuPlayerInfoPanel:getChildByName("expression_node")
   self.expressionButton = { }
   self.expression_img = {}
   local expressionNodeNameTable = { "xianhua" ,"boom","diutuoxie","chicken" }
   for i,v in ipairs( expressionNodeNameTable ) do
      local expressionButtonStr = string.format("expression_button%d",i)
      self.expressionButton[i] = self.expressionNode:getChildByName( expressionButtonStr )
      self.expressionButton[i]:setName( v )
      --self:onTouchBtnEvent( self.expressionButton[i] )
      self.expressionButton[i]:addTouchEventListener( handler(self,self.onTouchMagicButtonCallBack)  )
      local expressionImgStr = string.format("expression_img%d",i)
      self.expression_img[i] = self.expressionNode:getChildByName( expressionImgStr )
   end
   self.tips_node_3 = self.magicExpressionNode:getChildByName("tips_node3")
   self.tips_node_3:setVisible( false )
   
   self.titleLabel = buyuPlayerInfoPanel:getChildByName("title_label")
   self.titleLabel:enableOutline(cc.c4b(30, 70, 142, 255),2)

 end

function FishPlayerInfoLayer:updatePlayerHeadView( paras )
  --   cc.SpriteFrameCache:getInstance():addSpriteFrames("ui/p_head_icon.plist","ui/p_head_icon.png")
     local textureResType = ccui.TextureResType.localType
     if paras.faceId < GlobalDefine.UserDefineHeadMin then
          textureResType = ccui.TextureResType.plistType
     end
     -- 头像模板
     local  faceStel =  display.newSprite("#fish_bg_wjheadd.png")
     faceStel:setAnchorPoint(cc.p(0.5,0.5))
     local faceStelSize = faceStel:getContentSize()
     if self.headIconImg then 
        self.headIconImg:loadTexture( paras.fileName , textureResType)   
        self.headIconSize = self.headIconImg:getContentSize()
        self.headIconImg:setScaleX( faceStelSize.width / self.headIconSize.width )
        self.headIconImg:setScaleY( faceStelSize.height/self.headIconSize.height ) 
     else
          local clippingNode = cc.ClippingNode:create()
          clippingNode:setAnchorPoint( cc.p(0.5,0.5))
          clippingNode:setAlphaThreshold(0.1)
          self.headBg:addChild( clippingNode )
          local bgContenSize =  self.headBg:getContentSize()
          clippingNode:setPosition(cc.p( bgContenSize.width/2-2, bgContenSize.height/2 + 5 ))
          clippingNode:setStencil( faceStel ) 

          -- 头像
          self.headIconImg =   ccui.ImageView:create( paras.fileName ,textureResType)
          self.headIconImg:setAnchorPoint(cc.p(0.5,0.5))
          self.headIconSize = self.headIconImg:getContentSize()
          self.headIconImg:setScaleX( faceStelSize.width / self.headIconSize.width )
          self.headIconImg:setScaleY( faceStelSize.height/self.headIconSize.height )
          clippingNode:addChild( self.headIconImg )
     end
end

function FishPlayerInfoLayer:updateFriendPlayerInfoView( paras )
   print("FishPlayerInfoLayer:updateFriendPlayerInfoView")
   dump(paras)
     -- 性别 
   local sexImg = "fish_img_man.png"
   if paras.sex == 1 then
      sexImg = "fish_img_girl.png"
   end
   self.accoundId = paras.accoundId
   self.sexImg:loadTexture( sexImg ,  ccui.TextureResType.plistType)
   self.sexImg:setVisible(false)

   --玩家昵称
   self.nickNameLabel:setString( ToolKit:shorterString( paras.niceName, 7 ) )
   self.powerTitleLabel:setString("坐庄次数:")
   -- 坐庄次数
   self.cannonsMaxScoreLabel:setString( paras.score )
   self.cannonsMaxScoreLabel:setPositionX( -20 )
   -- 当前积分
   self.goldTitleLabel:setString("当前积分:")
   self.playGoldLabel:setString( paras.gold )
   self.playGoldLabel:setPositionX( -20)
   if self.accoundId == Player:getAccountID() then
      for i=1,4 do
         self.expressionButton[i]:setColor(cc.c3b(147, 157, 165))
         self.expressionButton[i]:setTouchEnabled(false)
         self.expression_img[i]:setColor(cc.c3b(147, 157, 165))
      end
   else
      for i=1,4 do
         self.expressionButton[i]:setColor(cc.c3b(255, 255, 255))
         self.expressionButton[i]:setTouchEnabled( true )
         self.expression_img[i]:setColor(cc.c3b(255, 255, 255))
      end
   end
end

function FishPlayerInfoLayer:updatePlayerInfoView( paras )
   -- 性别 
   local sexImg = "fish_img_man.png"
   if paras.sex == 1 then
      sexImg = "fish_img_girl.png"
   end
   self.sexImg:loadTexture( sexImg ,  ccui.TextureResType.plistType)
   self.sexImg:setVisible(false)
   -- 玩家账号id
   self.accountLabel:setString(paras.accoundId)
   self.accoundId = paras.accoundId
   --玩家昵称
   self.nickNameLabel:setString( ToolKit:shorterString( paras.niceName, 7) )
   --VIP
   self.vipLabel:setString( 0 )
   -- 炮台最高倍数
   self.cannonsMaxScoreLabel:setString( paras.score )
   -- 拥有的金币数量
   self.playGoldLabel:setString( paras.gold )
   if self.accoundId == Player:getAccountID() then
      for i=1,4 do
         self.expressionButton[i]:setColor(cc.c3b(147, 157, 165))
         self.expressionButton[i]:setTouchEnabled(false)
         self.expression_img[i]:setColor(cc.c3b(147, 157, 165))
      end
   else
      for i=1,4 do
         self.expressionButton[i]:setColor(cc.c3b(255, 255, 255))
         self.expressionButton[i]:setTouchEnabled( true )
         self.expression_img[i]:setColor(cc.c3b(255, 255, 255))
      end
   end
end

function FishPlayerInfoLayer:updateEnjoyAinView( freeTimes )
      if  freeTimes > 0 then
        self.tipsNode_1:setVisible(false)
        self.tipsNode_2:setVisible(true)
        local timesValue = string.format("%d/%d",freeTimes , self.attrData.nMagicTime)
        self.freeTimeLabel2:setString( timesValue )
      else
        self.tipsNode_1:setVisible(true)
        self.tipsNode_2:setVisible(false)
      end
end

function FishPlayerInfoLayer:onTouchBtnEvent( btn )
    btn:setSwallowTouches(false)
    local listenner = cc.EventListenerTouchOneByOne:create()
    listenner:setSwallowTouches(false)
    listenner:registerScriptHandler(function(touch, event)
        if btn:getCascadeBoundingBox():containsPoint(touch:getLocation()) then
            btn._begin_point = touch:getLocation()
            btn._move_len = 0
            return true
        end
        return false
    end,cc.Handler.EVENT_TOUCH_BEGAN )
    listenner:registerScriptHandler(function(touch, event)
        if btn._begin_point ~= nil then
            local pos = touch:getLocation()
            btn._move_len = ToolKit:distance(btn._begin_point, pos)
        end
    end,cc.Handler.EVENT_TOUCH_MOVED)
    listenner:registerScriptHandler(function(touch, event)
        if btn._move_len ~= nil and btn._move_len < 5 then
            local target = event:getCurrentTarget()
            self:onTouchCallback( target )
        end
        btn._begin_point = nil
        btn._move_len = nil
    end,cc.Handler.EVENT_TOUCH_ENDED )
    local _eventDispatcher = cc.Director:getInstance():getEventDispatcher()
    _eventDispatcher:addEventListenerWithSceneGraphPriority(listenner, btn)
end

-- 点击事件回调
function  FishPlayerInfoLayer:onTouchCallback( sender )
    if sender and sender:getTag() then
        if self:isVisible() then
          if self.accoundId ~= Player:getAccountID() then
             if self.leftTime <= 0 then
                 self:setVisible(false)
                 self:playAniTimerBegin()
                 FishSoundManager:getInstance():playEffect( FishSoundManager.ButtonPressed )
                 local name = sender:getName()
                 -- local data = { tag = name , send_userId =Player:getAccountID() , recive_userId = self.accoundId }
                 -- self:getParent().m_fishChatSystemLatyer:onReciveAmusing( data)
                 local strdata = name .. "," .. tostring(Player:getAccountID()) .. "," .. tostring(self.accoundId)
                if self.roomType == FishGlobal.RoomType.System or self.roomType == FishGlobal.RoomType.GoldIngot or self.roomType == FishGlobal.RoomType.ClubCity then
                    print("FishPlayerInfoLayer:onTouchCallback")
                    g_GameController:reqChat({ 1 ,FishGlobal.chatType.magicAni, strdata } )
                elseif self.roomType == FishGlobal.RoomType.Friends or self.roomType == FishGlobal.RoomType.ClubFriend then
                    FishFriendsGameController:getInstance():reqChat({ 1 ,FishGlobal.chatType.magicAni, strdata } ) 
                end
                 
             else
                if not self.operationCool then
                   self.operationCool = true
                   TOAST("操作过于频繁，请歇一会", function()
                       self.operationCool = false
                   end)
                end
             end
          end
        end
    end
end

function FishPlayerInfoLayer:onTouchMagicButtonCallBack( sender,eventType )
   if sender and sender:getTag() then
        local tag = sender:getTag()
        if eventType == ccui.TouchEventType.began then
            FishSoundManager:getInstance():playEffect( FishSoundManager.ButtonPressed )
        elseif eventType == ccui.TouchEventType.moved then

        elseif  eventType == ccui.TouchEventType.ended  then
            if self.accoundId ~= Player:getAccountID() then
               if self.leftTime <= 0 then
                   self:setVisible(false)
                   self:playAniTimerBegin()
                   local name = sender:getName()
                   -- local data = { tag = name , send_userId =Player:getAccountID() , recive_userId = self.accoundId }
                   -- self:getParent().m_fishChatSystemLatyer:onReciveAmusing( data)
                   local strdata = name .. "," .. tostring(Player:getAccountID()) .. "," .. tostring(self.accoundId)
                  if self.roomType == FishGlobal.RoomType.System  or self.roomType == FishGlobal.RoomType.GoldIngot or self.roomType == FishGlobal.RoomType.ClubCity then
                      local playerData = g_GameController:getChairByAccountId( self.accoundId )
                      if playerData then
                         g_GameController:reqChat({ 1 ,FishGlobal.chatType.magicAni, strdata } )
                      end
                  elseif self.roomType == FishGlobal.RoomType.Friends or self.roomType == FishGlobal.RoomType.ClubFriend then
                      local playerData = FishFriendsGameController:getInstance():getChairByAccountId( self.accoundId )
                      if playerData then
                         FishFriendsGameController:getInstance():reqChat({ 1 ,FishGlobal.chatType.magicAni, strdata } ) 
                      end
                  end
               else
                  if not self.operationCool then
                     self.operationCool = true
                     TOAST("操作过于频繁，请歇一会", function()
                         self.operationCool = false
                     end)
                  end
               end
           end
        end
    end
end

function FishPlayerInfoLayer:onTouchButtonCallBack(sender,eventType)
    if sender and sender:getTag() then
        local tag = sender:getTag()
        if eventType == ccui.TouchEventType.began then
            FishSoundManager:getInstance():playEffect( FishSoundManager.ButtonPressed )
        elseif eventType == ccui.TouchEventType.moved then
        elseif  eventType == ccui.TouchEventType.ended  then
           self:setVisible( false )
        end
    end
end


function FishPlayerInfoLayer:onExit()
    self:playAniTimerEnd()   
end



return FishPlayerInfoLayer