--
-- FishMainLayer
-- Author: chenzhanming
-- Date: 2017-04-19 14:00:00
-- 捕鱼主界面UI
--

local  FishGlobal             =  require("src.app.game.Fishing.src.FishGlobal")
local  scheduler              =  require("framework.scheduler")
local  BulletManager          =  require("src.app.game.Fishing.src.FishingGame.controller.BulletManager")
--local  FishGameController     =  require("src.app.game.Fishing.src.FishingGame.controller.FishGameController")
local  FishSkillManager       =  require("src.app.game.Fishing.src.FishingGame.controller.FishSkillManager")
local  BuyuSkillButton        =  require("src.app.game.Fishing.src.FishingGame.model.BuyuSkillButton")
local  FishSoundManager       =  require("src.app.game.Fishing.src.FishingCommon.controller.FishSoundManager")
local  EffectManager          =  require("src.app.game.Fishing.src.FishingGame.controller.EffectManager")
local  FishGameDataController =  require("src.app.game.Fishing.src.FishingCommon.controller.FishGameDataController")
local  FishingUtil            =  require("src.app.game.Fishing.src.FishingCommon.utils.FishingUtil")
local  FishBuff               =  require("src.app.game.Fishing.src.FishingGame.model.FishBuff")
local  DlgAlert               =  require("app.hall.base.ui.MessageBox") 
local GameRecordLayer= require("src.app.newHall.childLayer.GameRecordLayer")
local ChatLayer = require("src.app.newHall.chat.ChatLayer")
local HNLayer= require("src.app.newHall.HNLayer")  
local FishMainLayer = class("FishMainLayer", function()
    return HNLayer.new()
end)

FishMainLayer.MoveDistance        = 100
FishMainLayer.ClickTimeLimit      = 0.25          -- 手动点击时间间隔
FishMainLayer.autoTimeLimit       = 0.25          -- 自动开炮时间间隔
FishMainLayer.pressTimeLimit      = 0.25          -- 长按屏幕时间间隔
FishMainLayer.lockTargetTimeLimit = 0.25          -- 锁定目标时间间隔
FishMainLayer.addSpeed            = 1.5           -- 子弹加速度
FishMainLayer.addfireFrequency    = 2             -- 发炮频率


function FishMainLayer:ctor( __roomType , __Acer)
    self.roomType = __roomType
    self.m_Acer = __Acer
	-- 玩家view数组信息
    self.m_playerNodeViews = {}
    for i=1, FishGlobal.PlayerCount do
        self.m_playerNodeViews[i] = {}
    end
    -- 炮的开火变量
    self.cannonMoveBacks = {}
    self.cannonMoveFronts = {} 
    self.cannonAnchorpointY = {}
    for i = 1,FishGlobal.PlayerCount do
        self.cannonMoveBacks[i] = 0
        self.cannonMoveFronts[i] = 0
        self.cannonAnchorpointY[i] = 0.3
    end
    -- 主舰升级操作面板
    self.upGradMainShipPanel = nil
    self.upgradBtn = nil
    self.isUnfolding = false -- 是否收缩

    self.isSetOpen = false  -- 功能设置面板是否打开
    self.isSwithSetPaneled = true
    self.setPanelTime  = 0

    self.m_clickPosition = cc.p(display.cx,display.cy)
    self.m_pressClick = 0

    -- 玩家头像
    self.playerFaces = {}
    for i=1, FishGlobal.PlayerCount do
        self.playerFaces[i] = {} 
        self.playerFaces[i].faceId = nil
        self.playerFaces[i].facePic = nil
        self.playerFaces[i].needRemoveTextrue = false
    end

    self.facePaths = {}

    -- 玩家头像按钮
    self.playerFaceBtns = {}

    -- 子弹管理器
    self.m_bulletManager = BulletManager.new()

    self.clickTime = 0
    self.isFirstBulltLimiltTips = true

    --炮的世界坐标点
    self.powerworldPos = {}
    self.taskPanelTime = 0
    self.isSwithTaskPaneled = true
    self.timeLimit = 0.25
    self.fireOutTime = 0   --超时时间
    self.isShowTimeOutTips = false
    self.isPowerTipsVisble = false          -- 炮弹超上限提示
    self.faceWorldPos = {}

    self.lastClickFish =  nil
    self.crurentTime = socket.gettime()
    self.lastTime = socket.gettime()
    self.m_LockFireFishType = FishGlobal.minCannonFishType + 1
    self.m_bIsLockFish = false

    self:init()
    --用于连发的发炮延时
    --self.m_delayFireSchedule = self:schedule(handler(self,self.delayFire),0.4)
    --帧事件回调
    self:addNodeEventListener(cc.NODE_ENTER_FRAME_EVENT,handler(self,self.update))
    self:scheduleUpdate()
    self:registerTouchOneByOneListener()
    self:setNodeEventEnabled( true )
    
    addMsgCallBack(self,  MSG_FUNCSWITCH_UPDATE, handler(self, self.refreshFuctionNodes))
end


function FishMainLayer:init()
    local start_time = os.clock();
    local end_time = start_time;
    local all_time = start_time;

    self:loadMainCsbFile()
    end_time = os.clock();
    print("初始化 loadMainCsbFile-----", end_time - start_time, "\n");
    start_time = end_time;

    self:initFishPlayersView()
    end_time = os.clock();
    print("初始化 initFishPlayersView-----", end_time - start_time, "\n");
    start_time = end_time;
    -- self:initUpgradMainShipPanel()
    --self:initOtherButtons()
    -- self:initFutionsNode()
    self:initSetNode()
    end_time = os.clock();
    print("初始化 initSetNode-----", end_time - start_time, "\n");
    start_time = end_time;
    --self:createSkillButtons()
    self:refreshFuctionNodes()
    end_time = os.clock();
    print("初始化 refreshFuctionNodes-----", end_time - start_time, "\n");
    start_time = end_time;

    self:initFuntionNode()
    end_time = os.clock();
    print("初始化 initFuntionNode-----", end_time - start_time, "\n");
    start_time = end_time;

    print("初始化界面总耗时-----", end_time - all_time, "\n");
    --self:createLockLineSpr()
end

-- 触摸事件监听
function FishMainLayer:registerTouchOneByOneListener()
    local listener = cc.EventListenerTouchOneByOne:create()
    listener:registerScriptHandler(handler(self,self.onTouchMoved),cc.Handler.EVENT_TOUCH_MOVED)
    listener:registerScriptHandler(handler(self,self.onTouchBegan),cc.Handler.EVENT_TOUCH_BEGAN)
    listener:registerScriptHandler(handler(self,self.onTouchEnded),cc.Handler.EVENT_TOUCH_ENDED)
    listener:registerScriptHandler(handler(self,self.onTouchCancelled),cc.Handler.EVENT_TOUCH_CANCELLED)
    self:getEventDispatcher():addEventListenerWithSceneGraphPriority(listener,self)
end

-- 加载csb 文件
function FishMainLayer:loadMainCsbFile()
    self.m_pMainNode = CacheManager:addCSBTo(self, "buyu_cs_game/buyu_game_main_layer.csb", 0) -- cc.CSLoader:createNode("buyu_cs_game/buyu_game_main_layer.csb") 
    -- self:addChild(self.m_pMainNode)
    self.m_pMainNode:getChildByName("buyu_game_main_layer"):getChildByName("bg_image"):setVisible(false)
    local center = self.m_pMainNode:getChildByName("buyu_game_main_layer")
    local diffY = (display.size.height - 750) / 2
    self.m_pMainNode:setPosition(cc.p(0, diffY))

    local diffX = 145 - (1624 - display.size.width) / 2
    center:setPositionX(diffX)
    --    local temp = self.m_pMainNode:getChildren()
    --    for i=1,#temp do
    --        temp[i]:setPositionX(temp[i]:getPositionX()*display.scaleX)
    --        temp[i]:setPositionY(temp[i]:getPositionY()*display.scaleY)
    --    end
    local centerSize = center:getContentSize()
    self.mTextRecord = UIAdapter:CreateRecord()
    center:addChild(self.mTextRecord)
    self.mTextRecord:setAnchorPoint(cc.p(0.5, 0.5))
    self.mTextRecord:setPosition(cc.p(centerSize.width / 2, centerSize.height - 50))
end


-- 初始化玩家ui
function FishMainLayer:initFishPlayersView()
	for i = 1, FishGlobal.PlayerCount do
		local  playerNodeName = string.format("player_node_%d", i)
		local  playerNode = self.m_pMainNode:getChildByName( playerNodeName )
        self.m_playerNodeViews[i].playerNode = playerNode
        --self.m_playerNodeViews[i].playerNode:setVisible( false )
        if i <= 2 then
             playerNode:setPositionY( playerNode:getPositionY()/display.scaleY )
        end
        local attriNode = playerNode:getChildByName("attri_node")
        attriNode:setVisible( false )
        self.m_playerNodeViews[i].attriNode = attriNode
        local goldNode = attriNode:getChildByName("gold_node")
        local shipScoreNode = attriNode:getChildByName("ship_score_node")
        -- 金币数量  
        self.m_playerNodeViews[i].goldValueLabel = goldNode:getChildByName("gold_value_label")
        tolua.cast( self.m_playerNodeViews[i].goldValueLabel,"ccui.Text" )
        self.m_playerNodeViews[i].goldValueLabel:setVisible(false)
        local goldValueLabelPos = cc.p( self.m_playerNodeViews[i].goldValueLabel:getPosition()  )
        self.m_playerNodeViews[i].goldValueAtlas = ccui.TextAtlas:create(".","buyu_number/fish_bullet_num.png",12,16,".")
        self.m_playerNodeViews[i].goldValueAtlas:setScale( 1.3 )
        self.m_playerNodeViews[i].goldValueAtlas:setAnchorPoint( cc.p( 0 , 0.5) )
         self.m_playerNodeViews[i].goldValueAtlas:setPosition( goldValueLabelPos )
        goldNode:addChild( self.m_playerNodeViews[i].goldValueAtlas  )
        
        -- 金币背景
        self.m_playerNodeViews[i].goldSprite = goldNode:getChildByName("gold_sprite")
        -- 椅子号
        self.m_playerNodeViews[i].chairLabel = attriNode:getChildByName("chair_label")
        local chairLabelPos = cc.p( self.m_playerNodeViews[i].chairLabel:getPosition() )
        self.m_playerNodeViews[i].chairLabel:setVisible( false )
        --self.m_playerNodeViews[i].chairAtlas = cc.LabelAtlas:_create(0,"buyu_number/fish_f_zph_num_yljb.png",19,27,48)
        --self.m_playerNodeViews[i].chairAtlas:setAnchorPoint( cc.p( 0.5,0.5 ) )
        --self.m_playerNodeViews[i].chairAtlas:setPosition( chairLabelPos )
        --attriNode:addChild( self.m_playerNodeViews[i].chairAtlas )

        -- 玩家名称 
        self.m_playerNodeViews[i].nameLabel = attriNode:getChildByName("name_label")
        self.m_playerNodeViews[i].nameLabel:enableOutline(cc.c4b(30, 70, 142, 255),2)
        --self.m_playerNodeViews[i].nameLabel:setFontName("Helvetica")

        -- -- 元宝数量
        -- self.m_playerNodeViews[i].yuanBaoNode = attriNode:getChildByName("yuanbao_node")
        -- self.m_playerNodeViews[i].yuanBaoBg = self.m_playerNodeViews[i].yuanBaoNode:getChildByName("yuanbao_bg_img")
        -- self.m_playerNodeViews[i].yuanBaoBg:setContentSize( cc.size(50,24))
        -- self.m_playerNodeViews[i].yuanbaoNumLabel = self.m_playerNodeViews[i].yuanBaoNode:getChildByName("yuanbao_num_label")
        -- self.m_playerNodeViews[i].yuanbaoNumLabel:setString("0")
        -- -- if self.m_Acer == FishGlobal.isDropAcer then
        -- --     self.m_playerNodeViews[i].yuanBaoNode:setVisible( true )
        -- -- else
        --     self.m_playerNodeViews[i].yuanBaoNode:setVisible( false )
        -- --end
        -- 炮的倍数
        self.m_playerNodeViews[i].shipScoreLabel = shipScoreNode:getChildByName("ship_score_label")
        tolua.cast( self.m_playerNodeViews[i].shipScoreLabel,"ccui.Text" )
        self.m_playerNodeViews[i].shipScoreLabel:setVisible(false)
        local scoreLabelPos = cc.p( self.m_playerNodeViews[i].shipScoreLabel:getPosition() )

        self.m_playerNodeViews[i].shipScoreAtlas = ccui.TextAtlas:create("0","buyu_number/fish_paotaishuzi.png",18,32,".")
        self.m_playerNodeViews[i].shipScoreAtlas:setAnchorPoint( cc.p( 0.5 ,0.5 ) )
        shipScoreNode:addChild( self.m_playerNodeViews[i].shipScoreAtlas )
        self.m_playerNodeViews[i].shipScoreAtlas:setPosition( scoreLabelPos  )
        -- if i <= 3 then
        --     --用来显示船的倍数列表
        --     self.m_playerNodeViews[i].shipScoresPanel = attriNode:getChildByName("shop_scores_panel")
        --     self.m_playerNodeViews[i].shipScoresPanel:setVisible( false )
        -- end
        -- --炮倍数背景
        -- self.m_playerNodeViews[i].cannonScoreImg = shipScoreNode:getChildByName("bg_image")
        -- self.m_playerNodeViews[i].cannonScoreImg:setVisible(false)

        -- 玩家头像
--        self.m_playerNodeViews[i].faceSpr = shipScoreNode:getChildByName("face_spr")
--        tolua.cast( self.m_playerNodeViews[i].faceSpr,"cc.Sprite" )
        --shipScoreNode:getChildByName("face_spr"):setVisible(false)
        self.m_playerNodeViews[i].faceSpr = shipScoreNode:getChildByName("face_panel")
       -- self.m_playerNodeViews[i].facePanel:addTouchEventListener( handler(self ,self.OnPlayerHeadCallBack ) )
        -- 头像坐标
        self.faceWorldPos[i] = cc.p( shipScoreNode:convertToWorldSpace( cc.p( self.m_playerNodeViews[i].faceSpr:getPosition() ) ))
        local shipNode = playerNode:getChildByName("ship_node")
        shipNode:setVisible( false )
        self.m_playerNodeViews[i].shipNode = shipNode
        -- -- 切换低等级的炮
        -- self.m_playerNodeViews[i].decreaseBtn = shipScoreNode:getChildByName("decrease_btn")
        -- tolua.cast( self.m_playerNodeViews[i].decreaseBtn,"ccui.Button" )
        -- self.m_playerNodeViews[i].decreaseBtn:addTouchEventListener(handler(self,self.OnBtnOperationCallBack))
        -- self.m_playerNodeViews[i].decreaseBtn:setVisible( false )
        -- self.m_playerNodeViews[i].decreaseBtn:setTag( FishGlobal.OPERATION_CANNON_DEC )
        -- -- 切换高等级的炮
        -- self.m_playerNodeViews[i].addBtn = shipScoreNode:getChildByName("add_btn")
        -- tolua.cast( self.m_playerNodeViews[i].addBtn,"ccui.Button" )
        -- self.m_playerNodeViews[i].addBtn:setVisible( false )
        -- self.m_playerNodeViews[i].addBtn:addTouchEventListener(handler(self,self.OnBtnOperationCallBack))
        -- self.m_playerNodeViews[i].addBtn:setTag( FishGlobal.OPERATION_CANNON_ADD )
     
        -- -- 船的模型
        -- self.m_playerNodeViews[i].shipSpr = shipNode:getChildByName("ship_spr")
        -- tolua.cast( self.m_playerNodeViews[i].shipSpr,"cc.Sprite" )
        -- 炮的模型
        self.m_playerNodeViews[i].cannonSpr = shipNode:getChildByName("cannon_spr")
        self.m_playerNodeViews[i].bg_ship = shipNode:getChildByName("bg_ship") 
        tolua.cast( self.m_playerNodeViews[i].cannonSpr,"cc.Sprite" )
        self.m_playerNodeViews[i].cannonSpr:setAnchorPoint( cc.p(0.5 , 0.3) )
        local rotation = 0
        if i > 2 then
            rotation = 180
        end
        self.m_playerNodeViews[i].cannonSpr:setRotation( rotation )

        -- 破产标志
        self.m_playerNodeViews[i].bankruptImg = attriNode:getChildByName("image_bankruptcy")
        self.m_playerNodeViews[i].bankruptImg:setVisible(false)

        -- 等待加入
        self.m_playerNodeViews[i].waitJoinImg = playerNode:getChildByName("wait_join")

        --位置提示
        self.m_playerNodeViews[i].tipNode = playerNode:getChildByName("tips_node")
        self.m_playerNodeViews[i].tipNode:setVisible( false )

       
	end
end

-- 初始化升级主舰view
function FishMainLayer:initUpgradMainShipPanel()
    self.upgradNode = self.m_pMainNode:getChildByName("upgrad_node")
    -- self.upgradNode:setVisible(false)
    
    self.upgradImageBg = self.upgradNode:getChildByName("image_bg")
    self.upgradImageCsbBgSize = self.upgradImageBg:getContentSize()
    self.upgradImageBg:setVisible(false)
    -- 升级按钮
    self.upgradBtn = self.upgradNode:getChildByName("upgrad_btn")
    tolua.cast( self.upgradBtn,"ccui.Button" )
    self.upgradBtn:setTag( FishGlobal.OPERATION_CANNON_UPFOLDING )
    self.upgradBtn:addTouchEventListener(handler(self,self.OnBtnOperationCallBack))
    self.upgradBtn:setVisible(false)
	self.upPanel = self.upgradNode:getChildByName("up_panel")
    self.upPanel:setVisible(false)
	self.upPanel:addTouchEventListener(handler(self,self.OnBtnOperationCallBack))

    self.upPanel:getChildByName("task_cond"):setVisible(false)
  	-- 解锁多少倍的炮
  	self.paoScoreLevel = self.upPanel:getChildByName("ship_level")
  	tolua.cast( self.shipLevel,"ccui.Text" )
    self.paoScoreLevel:setVisible( false )

    self.paoScoreLabelAtlas = cc.LabelAtlas:_create(0,"buyu_number/fish_num_beishu.png",19,23,48)
    self.paoScoreLabelAtlas:setAnchorPoint( cc.p(0.5,0.5) )
    local paoScorePos = cc.p( self.paoScoreLevel:getPosition() )
    self.paoScoreLabelAtlas:setPosition( paoScorePos )
    self.paoScoreLabelAtlas:setString( 0 )
    self.upPanel:addChild( self.paoScoreLabelAtlas )

    -- 任务名称
    self.taslNameLabel =self.upPanel:getChildByName("task_name_label") 
    tolua.cast( self.taslNameLabel,"ccui.Text" )

  	-- 主舰升级进度条
  	self.shipLevelProBar = self.upPanel:getChildByName("levelbar")
    --self.upPanel:setSwallowTouches( false )

    -- 任务进度文字提示
    local progessNode = self.upPanel:getChildByName("progess_node")
    
    -- 鱼的图片
    self.fishTargetImage = self.upPanel:getChildByName("fish_target_image")
    
    -- 任务进度
    self.perTaskLabel =  progessNode:getChildByName("per_label")
    self.perTaskLabel:setString( 0 )

    -- 总任务个数
    self.totalTaskLabel = progessNode:getChildByName("total_label")
    self.totalTaskLabel:setString( 0 )

    --self.upgradImageBg:setContentSize(cc.size(248,130))
   

    self.upgrad_panel = self.upgradNode:getChildByName("upgrad_panel")
    self.upgrad_panel:setSwallowTouches(false)

    self.shopButton = self.m_pMainNode:getChildByName("shop_button")
    self.shopButton:setTag( FishGlobal.OPERATION_SHOP )
    self.shopButton:addTouchEventListener(handler(self,self.OnBtnOperationCallBack))
    self.shopButton:setVisible(false)
    -- -- 活动按钮
    -- self.activityButton = self.m_pMainNode:getChildByName("activity_button")
    -- self.activityButton:setTag( FishGlobal.OPERATION_ACTIVITY )
    -- self.activityButton:addTouchEventListener(handler(self,self.OnBtnOperationCallBack))
    -- self.activityButton:setVisible(false)
    
    -- -- 活动小红点
    -- self.redSprite = self.activityButton:getChildByName("read_sprite")
    -- self.redSprite:setVisible(false)
    
end

function FishMainLayer:setRedSpriteVisble( isVisble )
     self.redSprite:setVisible( isVisble )
end

function FishMainLayer:refreshFuctionNodes()
    -- 公测活动开关
    -- if   getFuncOpenStatus( FishGlobal.buyuOpenBetaActivityId ) == 1 then 
    --     self.activityButton:setVisible( false )
    -- elseif getFuncOpenStatus( FishGlobal.buyuOpenBetaActivityId ) == 0 then
    --     self.activityButton:setVisible( true )
    -- end
end

function FishMainLayer:setUpgradNodeVisble( isVisble)
   self.upgradNode:setVisible( isVisble )
end

function FishMainLayer:setPaoLevel( score )
   self.paoScoreLabelAtlas:setString(  tostring( score ) )
end


function FishMainLayer:setTaskName( taskName )
    self.taslNameLabel:setString( taskName )
end

function FishMainLayer:refreshTaskView( __info )
    self:setPaoLevel( __info.score )
    self:setTaskName( __info.taskName )
    self.shipLevelProBar:setPercent( __info.percen )
    self.perTaskLabel:setString( __info.taskPro )
    self.totalTaskLabel:setString( __info.totalTask )
    if __info.taskType == 1 then
        local fishData =  FishGameDataController:getInstance():getFishDataByType( __info.fishType )
        if fishData then
           self.fishTargetImage:setVisible( true )
           dump( fishData.tjFishImage )
           local frame =  cc.SpriteFrameCache:getInstance():getSpriteFrame( fishData.tjFishImage )
           if frame then
              self.fishTargetImage:loadTexture( fishData.tjFishImage , ccui.TextureResType.plistType)
           end
        else
           self.fishTargetImage:setVisible(false)
        end
    else
        self.fishTargetImage:setVisible(false)
    end
end

function FishMainLayer:setTaskName( taskName )
    self.taslNameLabel:setString( taskName )
end

function FishMainLayer:swithUpgradShiBegin()
    if self.swithUpgradShiTimer then
        scheduler.unscheduleGlobal( self.swithUpgradShiTimer )
        self.swithUpgradShiTimer = nil 
    end
    -- self.upgradBtn:setTouchEnabled( false )
    self.swithUpgradShiTimer = scheduler.scheduleGlobal(handler(self,self.swithUpgradShip),0.0001)
end

function FishMainLayer:swithUpgradShiEnd()
    if not self.swithUpgradShiTimer then
        return
    end
    scheduler.unscheduleGlobal(self.swithUpgradShiTimer)
    self.swithUpgradShiTimer = nil
end



-- 升级主舰面板的切换
function FishMainLayer:swithUpgradShip( dt )
    if self.isUnfolding then 
        self.upPanel:setVisible( false )
        local  upgradImageBgSize = self.upgradImageBg:getContentSize()
        local upgradImageBgWidth = upgradImageBgSize.width - 1200 * dt
        if upgradImageBgWidth <= 20 then
           self.isUnfolding = false
           self.isSwithTaskPaneled = true
           self.upgradImageBg:setContentSize(cc.size(20,self.upgradImageCsbBgSize.height))
           self.upgradImageBg:setVisible(false)
           self:swithUpgradShiEnd()
          -- self.upgradBtn:setTouchEnabled( true )
        else
           self.upgradImageBg:setContentSize(cc.size(upgradImageBgWidth,self.upgradImageCsbBgSize.height))
        end
    else
        self.upgradImageBg:setVisible(true)
        local  upgradImageBgSize = self.upgradImageBg:getContentSize()
        local upgradImageBgWidth = upgradImageBgSize.width + 1200 * dt
        if upgradImageBgWidth >= 220 then
            self.taskPanelTime = 0
            self.isUnfolding = true
            self.isSwithTaskPaneled = true
            self.upPanel:setVisible( true )
            self.upgradImageBg:setContentSize( self.upgradImageCsbBgSize)
            self:swithUpgradShiEnd()
            -- self.upgradBtn:setTouchEnabled( true )
        else
            self.upgradImageBg:setContentSize(cc.size(upgradImageBgWidth,self.upgradImageCsbBgSize.height))
        end
    end
end

function FishMainLayer:spreadUpradPower()
    -- 控制升级面板的收缩
    if not self.isUnfolding then
        if self.isSwithTaskPaneled then
           self.isSwithTaskPaneled = false
           self:swithUpgradShiBegin()
           FishSoundManager:getInstance():playEffect( FishSoundManager.PowerUnlock  )
        end
    end
end

function FishMainLayer:initOtherButtons()
    -- 房间号
    self.roomNode = self.m_pMainNode:getChildByName("room_node")
    self.roomNode:setVisible( false )

    self.roomTitle = self.roomNode:getChildByName("room_title")
    self.roomTitle:setString("网速:")

    self.roomNumLabel = self.roomNode:getChildByName("room_number")

    -- 踢出房间
    self.controlNode = self.m_pMainNode:getChildByName("control_node")
    self.controlNode:setVisible(false)

end


function FishMainLayer:setNetSpeed( speed )
    self.roomNumLabel:setString( speed )
end

function FishMainLayer:initFutionsNode()
    self.funcionNode = self.m_pMainNode:getChildByName("funtion_panel")
    self.funcionNodeX,self.funcionNodeY = self.funcionNode:getPosition()
    
    self.skillBtnPosList = { }
    -- 锁定目标
    self.lockBtn = self.funcionNode:getChildByName("lock_button")
    self.lockBtn:setTag( FishGlobal.OPERATION_LOCK_FISH ) 
    self.lockBtn:addTouchEventListener(handler(self,self.OnBtnOperationCallBack))
    self.lockBtn:setVisible(false)

    self.lockButtonPos = cc.p( self.lockBtn:getPositionX(), self.lockBtn:getPositionY() )
    self.skillBtnPosList[ FishSkillManager.skillType.LOCKSKILL ] =  self.lockButtonPos

    -- 冰封万里
    self.frozenSkillBtn = self.funcionNode:getChildByName("frozen_button")
    self.frozenButtonPos = cc.p( self.frozenSkillBtn:getPositionX(),self.frozenSkillBtn:getPositionY())
    self.skillBtnPosList[ FishSkillManager.skillType.FROZENSKILL ] = self.frozenButtonPos
    self.frozenSkillBtn:setVisible( false ) 

    -- 狂暴技能
    self.critSkillBtn = self.funcionNode:getChildByName("crit_button")
    self.critSkillBtnPos = cc.p(self.critSkillBtn:getPositionX(), self.critSkillBtn:getPositionY() )
    self.skillBtnPosList[ FishSkillManager.skillType.CRITSKILL ] = self.critSkillBtnPos
    self.critSkillBtn:setVisible(false)

end


function FishMainLayer:createSkillButtons()
    self.skillBtns = {} 
    local skillDataList =  FishGameDataController:getInstance():getBuyuSkillDataList() 
    for k,skillData in pairs( skillDataList ) do
       local  param = {  
                         skillType            = skillData.nType,
                         cdTime               = skillData.nTime , 
                         stencilFileName      = "#dt_gameroom_btn_tra1.png" ,
                         buttonNormalName     =  skillData.szSkillIcon,
                         buttonClickName      =  skillData.szSkillIcon,
                         buttonTextureResType =  ccui.TextureResType.plistType,
                         progressSprTexture   =  "#"..skillData.szSkillIcon,
                      } 
       self.skillBtns[ skillData.nType ] =  BuyuSkillButton.new( param )
       self.skillBtns[ skillData.nType ]:setPosition( self.skillBtnPosList[ skillData.nType ] )
       self.funcionNode:addChild( self.skillBtns[ skillData.nType ] , FishGlobal.UILevel)
       self.skillBtns[ skillData.nType ]:setSkillClickCallHandler( handler(self,self.reqUseSkill ) )
       self.skillBtns[ skillData.nType ]:setSkillCoolDownCallHandler( handler(self, self.skillEnd ) )
       self.skillBtns[ skillData.nType ]:setVisible(false)
    end

end

function FishMainLayer:setSkilItemCountBySkillType( skillType , itemCount )
    -- if self.skillBtns[ skillType ] then
    --    self.skillBtns[ skillType ]:setSkilItemCount( itemCount )
    -- end
end


function FishMainLayer:pressLockSkill()
    self:reqAutoLockFire()
end

function FishMainLayer:initFuntionNode()
    --self.funtionNode = self.m_pMainNode:getChildByName("funtion_node")
    
    local nWinOffsetX = (display.size.width - 1334 ) / 2
 
    local pAutoNode = self.m_pMainNode:getChildByName("auto_node")
    --加炮
    local addBtn = pAutoNode:getChildByName("add_btn")
    addBtn:addTouchEventListener(handler(self, self.OnBtnOperationCallBack))
    addBtn:setTag(FishGlobal.OPERATION_CANNON_ADD)
    --减炮
    local decreaseBtn = pAutoNode:getChildByName("decrease_btn")
    decreaseBtn:addTouchEventListener(handler(self, self.OnBtnOperationCallBack))
    decreaseBtn:setTag(FishGlobal.OPERATION_CANNON_DEC)
    --pAutoNode:setVisible(false)

    local pFuntionNode = self.m_pMainNode:getChildByName("funtion_node")
    local nFuntionNodePointX = pFuntionNode:getPositionX()
    if nWinOffsetX > 0 then
        pFuntionNode:setPositionX(nFuntionNodePointX + nWinOffsetX / 2)
    else
        pFuntionNode:setPositionX(nFuntionNodePointX + nWinOffsetX)
    end

    --自动开火
    self.autoFireCheckBox = pFuntionNode:getChildByName("autofire_checkbox")
    self.autoFireCheckBox:addEventListener(handler(self, self.onAutoFireCheckBox))

    -- 锁定
    self.lockCheckbox = pFuntionNode:getChildByName("lock_checkbox")
    self.lockCheckbox:addEventListener(handler(self, self.onAutoFireCheckBox))
    self.lockCheckbox:setSelected(false)

    -- 切换高等级的炮
    local btn_chat = pFuntionNode:getChildByName("btn_chat")
    btn_chat:addTouchEventListener(handler(self, self.onChat))
    btn_chat:setTag(-1)
end
 
 function FishMainLayer:onChat( sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        local chatLayer= ChatLayer.new()
        self:addChild(chatLayer)
    end
 end
-- 初始化设置面板
function FishMainLayer:initSetNode()

    local nWinOffsetX = (display.size.width - 1334 ) / 2

    local pSettingNode = self.m_pMainNode:getChildByName("set_node")
    local nSettingNodePointX = pSettingNode:getPositionX()
    if nWinOffsetX > 0 then
        pSettingNode:setPositionX(nSettingNodePointX - nWinOffsetX / 2)
    else
        pSettingNode:setPositionX(nSettingNodePointX - nWinOffsetX)
    end

    -- 退出游戏
    self.backBtn = pSettingNode:getChildByName("btn_leave")
    tolua.cast( self.backBtn,"ccui.Button" )
    self.backBtn:setTag( FishGlobal.OPERATION_EXIT_GAME )
    self.backBtn:addTouchEventListener(handler(self,self.OnBtnOperationCallBack))
    self.backBtnPos = cc.p(self.backBtn:getPosition())
    self.backBtn:setPosition(cc.p(0, 0))
    self.backBtn:setVisible(false)
    
    -- 自动开火选择框
    self.speedCheckbox = pSettingNode:getChildByName("speed_checkbox")
    self.speedCheckbox:addEventListener(handler(self, self.onAutoFireCheckBox))
    
    -- 显示鱼种列表
    self.fishListBtn = pSettingNode:getChildByName("btn_fishdis")
    tolua.cast( self.fishListBtn,"ccui.Button" )
    self.fishListBtn:setTag( FishGlobal.OPERATION_FISH_LIST )
    self.fishListBtn:addTouchEventListener(handler(self,self.OnBtnOperationCallBack))
    self.fishListBtnPos = cc.p(self.fishListBtn:getPosition())
    self.fishListBtn:setPosition(cc.p(0, 0))
    self.fishListBtn:setVisible(false)
    -- 设置
    self.setBtn = pSettingNode:getChildByName("btn_set")
    tolua.cast( self.setBtn,"ccui.Button" )
    self.setBtn:setTag( FishGlobal.OPERATION_MUST_SET )
    self.setBtn:addTouchEventListener(handler(self,self.OnBtnOperationCallBack))
    self.setBtnPos = cc.p(self.setBtn:getPosition())
    self.setBtn:setPosition(cc.p(0, 0))
    self.setBtn:setVisible(false)
    -- -- 语音聊天
    -- self.chatBtn = self.setNode:getChildByName("btn_chat")
    -- tolua.cast( self.setBtn,"ccui.Button" )
    -- self.chatBtn:setTag( FishGlobal.OPERATION_CHAT )
    -- self.chatBtn:addTouchEventListener(handler(self,self.OnBtnOperationCallBack))
    
    -- 更多
    self.moreBtn = pSettingNode:getChildByName("btn_more")
    tolua.cast( self.setBtn,"ccui.Button" )
    self.moreBtn:addTouchEventListener(handler(self,self.OnBtnOperationCallBack))
    self.moreBtn:setTag( FishGlobal.OPERATION_SET_MORE )
    --self.arrowSpr = pSettingNode:getChildByName("arrow_spr")
    
    -- local frame =  cc.SpriteFrameCache:getInstance():getSpriteFrame( "buyu_btn_more1.png" )
    -- if frame then
    --    --self.moreBtn:loadTextures("buyu_btn_more1.png","buyu_btn_more1.png","buyu_btn_more1.png",ccui.TextureResType.plistType)
    -- end
    --self.moreBtn:setRotation( 0 )
end

function  FishMainLayer:swithSetPanel()
    local moveTime = 0.1
    local delayTime = 0.06
    if self.isSetOpen  then
        self.isSetOpen = false
        self.backBtn:setEnabled(false)
        self.fishListBtn:setEnabled(false)
        self.setBtn:setEnabled(false)
        self.moreBtn:setEnabled(false)

        self.backBtn:runAction(cc.Sequence:create(
            cc.Spawn:create(cc.MoveTo:create(moveTime, cc.p(0,0)), cc.ScaleTo:create(moveTime, 0.1)),
            cc.Hide:create(), 
            cc.CallFunc:create(function()
                self.moreBtn:setEnabled(true)
                --self.moreBtn:setRotation(0)
                self.isSwithSetPaneled = true
                self.setPanelTime = 0
                self.moreBtn:setScaleX(1)
            end), 
        nil))
        self.fishListBtn:runAction(
            cc.Sequence:create(cc.DelayTime:create(delayTime), 
                cc.Spawn:create(cc.MoveTo:create(moveTime, cc.p(0,0)), cc.ScaleTo:create(moveTime, 0.1)),
                cc.Hide:create(), 
            nil))
        self.setBtn:runAction(cc.Sequence:create(cc.DelayTime:create(delayTime*2.0), 
            cc.Spawn:create(cc.MoveTo:create(moveTime, cc.p(0,0)), cc.ScaleTo:create(moveTime, 0.1)), 
            cc.Hide:create(), nil))

        --[[
        --self.moreBtn:setTouchEnabled(false)
        local moveOut =  cc.MoveBy:create(0.35,cc.p( FishMainLayer.MoveDistance ,0))
        local callFunc = cc.CallFunc:create( function()
             self.arrowSpr:setRotation( 0 )
             --local frame =  cc.SpriteFrameCache:getInstance():getSpriteFrame( "buyu_btn_more1.png" )
             -- if frame then
             --    self.moreBtn:loadTextures("buyu_btn_more1.png","buyu_btn_more1.png","buyu_btn_more1.png",ccui.TextureResType.plistType)
             -- end
             --self.moreBtn:setTouchEnabled( true )
             self.isSwithSetPaneled = true
             self.setPanelTime = 0 
        end )
        self.setNode:runAction( cc.Sequence:create( moveOut , callFunc ) )
        --local moveIn =  cc.MoveTo:create(0.35,cc.p( display.width  ,self.funcionNodeY))
        --self.funcionNode:runAction( cc.Sequence:create( moveIn ))
        ]]
    else
        self.isSetOpen = true
        
        self.backBtn:runAction(cc.Sequence:create(cc.Show:create(), 
            cc.Spawn:create(cc.MoveTo:create(moveTime, self.backBtnPos), cc.ScaleTo:create(moveTime, 1)),
            cc.CallFunc:create(function()
                self.moreBtn:setEnabled(true)
                self.backBtn:setEnabled(true)
                --self.moreBtn:setRotation(180)
                self.isSwithSetPaneled = true
                self.setPanelTime = 0
                self.moreBtn:setScaleX(-1)
            end), 
        nil))
        self.fishListBtn:runAction(cc.Sequence:create(cc.DelayTime:create(delayTime), cc.Show:create(), 
            cc.Spawn:create(cc.MoveTo:create(moveTime, self.fishListBtnPos), cc.ScaleTo:create(moveTime, 1)), 
            cc.CallFunc:create(function() 
            self.fishListBtn:setEnabled(true)
        end), nil))
        self.setBtn:runAction(cc.Sequence:create(cc.DelayTime:create(delayTime*2.0), cc.Show:create(), 
            cc.Spawn:create(cc.MoveTo:create(moveTime, self.setBtnPos), cc.ScaleTo:create(moveTime, 1)), 
            cc.CallFunc:create(function() 
            self.setBtn:setEnabled(true)
        end), nil))

        --[[
        -- self.moreBtn:setTouchEnabled( false )
        local moveIn =  cc.MoveBy:create(0.35,cc.p( -FishMainLayer.MoveDistance,0))
        local callFunc = cc.CallFunc:create( function()
             self.arrowSpr:setRotation( 180 )
             -- local frame =  cc.SpriteFrameCache:getInstance():getSpriteFrame( "buyu_btn_more2.png" )
             -- if frame then
             --    self.moreBtn:loadTextures("buyu_btn_more2.png","buyu_btn_more2.png","buyu_btn_more2.png",ccui.TextureResType.plistType)
             -- end
             --self.moreBtn:setTouchEnabled( true )
             self.isSwithSetPaneled = true
             self.setPanelTime = 0 
        end )
        self.setNode:runAction( cc.Sequence:create( moveIn , callFunc ) )
        --local moveOut =  cc.MoveTo:create(0.35,cc.p( display.width + FishMainLayer.MoveDistance ,self.funcionNodeY))
        --self.funcionNode:runAction( cc.Sequence:create( moveOut ))
        ]]
    end
end

-- 玩家进入
function FishMainLayer:onUserEnter( chair, info)
    print("玩家进入")
    dump(info)
    local viewChair = FishGlobal.getViewChair( chair)
    self.m_playerNodeViews[ viewChair ].shipNode:setVisible( true )
    self.m_playerNodeViews[ viewChair ].attriNode:setVisible( true )
    self.m_playerNodeViews[ viewChair ].waitJoinImg:setVisible( false )
    self:updatePlayerGoldView( viewChair , info.m_nCoin )
    local score = FishGameDataController:getInstance():getPowerScoreByLevel( info.m_nPowerLevel )
    self:showOpenLevel( viewChair, score )
    self:changeCannon( viewChair, info.m_nVipLevel ,false )
    self:setPlayFacePath( info )
 --   self:showPlayerFace( chair )
    if chair == FishGlobal.myChair then
       --self.m_playerNodeViews[ viewChair ].addBtn:setVisible( true )
       --self.m_playerNodeViews[ viewChair ].decreaseBtn:setVisible( true )
       self.m_playerNodeViews[ viewChair ].tipNode:setVisible( true )
    end
    local _expressionPos= self.faceWorldPos[ viewChair ]
    if viewChair == 1 or viewChair == 2  then
       _expressionPos = cc.p( _expressionPos.x ,_expressionPos.y + 30)
    else
       _expressionPos = cc.p( _expressionPos.x ,_expressionPos.y - 30 )
    end
    local params = { userID = info.m_nAccountId , expressionPos = _expressionPos  }
    --self:getParent().m_fishChatSystemLatyer:setUserIDPos( params )
    local goldBgPng = FishGlobal.getGoldBgPngByChair( chair )
    -- 金币背景 
    --cc.SpriteFrameCache:getInstance():addSpriteFrames("buyu_p_plist/p_buyu_commom_new_1.plist","buyu_p_plist/p_buyu_commom_new_1.png")
    --self.m_playerNodeViews[viewChair].goldSprite:setSpriteFrame( display.newSpriteFrame (  goldBgPng ) )
    -- 椅子号
    --self.m_playerNodeViews[viewChair].chairAtlas:setString( tostring(chair) )
    -- 玩家名称 
    self.m_playerNodeViews[viewChair].nameLabel:setString( info.m_nickName )
    if chair == FishGlobal.myChair then
       if info.m_nSeepType == FishGlobal.SeepType.fast then
          self.speedCheckbox:setSelected( false )
       else
          self.speedCheckbox:setSelected( true )
       end
    end

    if info.m_nAccountId == Player:getAccountID() then
        local viewPosX, viewPosY = self.m_playerNodeViews[viewChair].playerNode:getPosition()
        local pAutoNode = self.m_pMainNode:getChildByName("auto_node")
        if viewChair == 1 or viewChair == 2 then
            pAutoNode:setPosition(cc.p(viewPosX, viewPosY + 42))
        else
            pAutoNode:setPosition(cc.p(viewPosX, viewPosY - 42))
        end
    end
    
end
-- 玩家离开
function FishMainLayer:onUserLeave( chair, info)
    local viewChair = FishGlobal.getViewChair( chair )
    self.m_playerNodeViews[ viewChair ].shipNode:setVisible( false )
    self.m_playerNodeViews[ viewChair ].attriNode:setVisible( false )
    self.m_playerNodeViews[ viewChair ].waitJoinImg:setVisible( true )
    if self.playerFaces[viewChair].needRemoveTextrue then
       self.playerFaces[viewChair].needRemoveTextrue = false
       if self.playerFaces[viewChair].faceId  >= GlobalDefine.UserDefineHeadMin then
          cc.Director:getInstance():getTextureCache():removeTextureForKey( self.playerFaces[viewChair].facePic )
       end
    end
    self.playerFaces[viewChair].faceId = nil
    self.playerFaces[viewChair].facePic = nil
    EffectManager.hideCritAni( { viewChair = viewChair } )
end

function FishMainLayer:refreshUserView( info )
    local score = FishGameDataController:getInstance():getPowerScoreByLevel( info.m_nPowerLevel )
    self:showOpenLevel( info.m_viewChairId , score )
    self.m_playerNodeViews[ info.m_viewChairId ].shipNode:setVisible( true )
    self.m_playerNodeViews[ info.m_viewChairId ].attriNode:setVisible( true )
    self.m_playerNodeViews[ info.m_viewChairId ].waitJoinImg:setVisible( false )
    self:changeCannon( info.m_viewChairId , info.m_nVipLevel ,false )
end

function FishMainLayer:setPlayFacePath( info )
    local faceId = tonumber( info.m_faceID )%7 
    if faceId then
      -- if faceId > 0 and faceId <= GlobalDefine.SystemHeadMax then
          self.playerFaces[ info.m_viewChairId ].faceId  = faceId
          self.playerFaces[ info.m_viewChairId ].facePic = ToolKit:getHead(faceId)
      
    end
end

function FishMainLayer:downloadPlayerHeads( info )
    local viewChair = info.m_viewChairId
    local headPath = device.writablePath .. info.m_nAccountId .. "/" .. info.m_faceID .. ".jpg"
    if FishingUtil:checkAIuserid( info.m_nAccountId ) then 
       headPath = device.writablePath .. info.m_nAccountId .. "/" .. info.m_faceID .. ".png"
    end
   
    --判断图片是否存在，存在则不去下载
    if UpdateFunctions.isFileExist( headPath ) then
        self.playerFaces[ viewChair ].faceId  = info.m_faceID
        self.playerFaces[ viewChair ].facePic = headPath
        self.playerFaces[ viewChair ].needRemoveTextrue = true
    else
        -- 下载图片
        UpdateFunctions.mkDir(device.writablePath ..info.m_nAccountId ) -- 创建文件夹
        if device.platform == "windows"  then -- windows平台, 用NetSprite下载
            local url = "http://image.milaichess.com/" .. AliYunUtil.headPath .. info.m_nAccountId .. "/" .. info.m_faceID..".jpg"
            local picPath = device.writablePath .. "netSprite/" .. crypto.md5(url) .. ".jpg" -- 文件保存路径
            if FishingUtil:checkAIuserid( info.m_nAccountId ) then 
               url = "http://image.milaichess.com/newPlatform/image/head/AI/" .. info.m_faceID .. ".png"
               picPath = device.writablePath.."netSprite/" .. crypto.md5(url) .. ".png"
            end
            url = url .. "?x-oss-process=image/format,jpg"
            if UpdateFunctions.isFileExist( picPath ) then
               self.playerFaces[ viewChair ].faceId  = info.m_faceID
               self.playerFaces[ viewChair ].facePic = picPath
               self.playerFaces[ viewChair ].needRemoveTextrue = true
            else
                local NetSprite = require("app.hall.base.ui.UrlImage")
                local netSprite = NetSprite.new(url, true, handler(self, self.win32NetSpriteCallBack), true)
                self:addChild( netSprite )
                netSprite:setVisible(false)
                local isExist,fileName = netSprite:getUrlMd5()
                self.facePaths[ fileName ] = { _viewChair = viewChair , _faceId = info.m_faceID , _chair = info.m_chairId }
            end  
        else
            if  AliYunUtil:getAliyunState(false) then 
                local disPath = AliYunUtil.headPath .. info.m_nAccountId .. "/" .. info.m_faceID .. ".jpg"
                if FishingUtil:checkAIuserid( info.m_nAccountId ) then 
                    print("FishingUtil:checkAIuserid( info.m_nAccountId )=",info.m_faceID)
                    disPath = "newPlatform/image/head/AI/" .. info.m_faceID .. ".png"          
                end
                FishingUtil:getInstance():downloadFileEx( tostring(info.m_nAccountId) , disPath  ,headPath, handler(self ,self.isDownFileBack ))
            end
        end
    end
end

function FishMainLayer:win32NetSpriteCallBack(result, fileName )
    if result then
        if self.facePaths[ fileName ] then
            local viewChair = self.facePaths[ fileName ]._viewChair
            local chair = self.facePaths[ fileName ]._chair
            self.playerFaces[ viewChair ].facePic = fileName
            self.playerFaces[ viewChair ].faceId = self.facePaths[ fileName ]._faceId
            self.playerFaces[ viewChair ].needRemoveTextrue = true
     --       self:showPlayerFace(  chair )
        end
    end
end

function FishMainLayer:isDownFileBack( args )
    --print(args)
    local strlist = string.split(args, ";")
    if tonumber( strlist[1] ) == 1 then
        local fishPlayer =  g_GameController:getPlayerInfoByAccountId(tonumber( strlist[2] ) )
        if fishPlayer then
           local viewChair = fishPlayer:getViewChair()
           self.playerFaces[ viewChair ].faceId = fishPlayer:getFaceID()
           self.playerFaces[ viewChair ].facePic = strlist[3]
           self.playerFaces[ viewChair ].needRemoveTextrue = true
           local chair = fishPlayer:getChair()
       --    self:showPlayerFace(  chair )
        end
    else
      print("下载头像失败!")
    end
end



-- 显示玩家头像
function FishMainLayer:showPlayerFace(  chair )
 --   cc.SpriteFrameCache:getInstance():addSpriteFrames("ui/p_head_icon.plist","ui/p_head_icon.png")
    local viewChair = FishGlobal.getViewChair( chair)
   -- self.m_playerNodeViews[ viewChair ].facePanel:setTag( chair )
    textureResType = 0

    -- 头像模板
    local  faceStel =  display.newSprite("#fish_img_head.png")
    faceStel:setAnchorPoint(cc.p(0.5,0.5))
    local faceStelSize = faceStel:getContentSize()

    if self.playerFaceBtns[ viewChair ] then
        self.playerFaceBtns[ viewChair ]:loadTexture( self.playerFaces[viewChair].facePic , 1 )
        self.playerFaceBtns[ viewChair ]:setVisible(false)
        local playerFaceBtnSize = self.playerFaceBtns[ viewChair ]:getContentSize()
        self.playerFaceBtns[ viewChair ]:setScaleX(faceStelSize.width/playerFaceBtnSize.width  )
        self.playerFaceBtns[ viewChair ]:setScaleY( faceStelSize.height/playerFaceBtnSize.height )
    else
        local faceSprContenSize = self.m_playerNodeViews[ viewChair ].faceSpr:getContentSize()
        local clippingNode = cc.ClippingNode:create()
        clippingNode:setAnchorPoint( cc.p(0.5,0.5))
        clippingNode:setAlphaThreshold(0.1)
        self.m_playerNodeViews[ viewChair ].faceSpr:addChild( clippingNode )
        if viewChair == 1 or viewChair == 2  then
           clippingNode:setPosition( cc.p( faceSprContenSize.width/2,faceSprContenSize.height/2 + 6 ))
        else
           clippingNode:setPosition( cc.p( faceSprContenSize.width/2,faceSprContenSize.height/2 - 6 ))

        end 
        clippingNode:setStencil( faceStel )  
        -- 头像
        self.playerFaceBtns[ viewChair ]  = ccui.ImageView:create( self.playerFaces[viewChair].facePic, 1) 
         self.playerFaceBtns[ viewChair ]:setVisible(false)
        self.playerFaceBtns[ viewChair ]:setAnchorPoint(cc.p(0.5,0.5))
        local playerFaceBtnSize = self.playerFaceBtns[ viewChair ]:getContentSize()
        self.playerFaceBtns[ viewChair ]:setScaleX(faceStelSize.width/playerFaceBtnSize.width  )
        self.playerFaceBtns[ viewChair ]:setScaleY( faceStelSize.height/playerFaceBtnSize.height )
        clippingNode:addChild( self.playerFaceBtns[ viewChair ] ) 
    end
end

--按钮操作回调
function  FishMainLayer:OnPlayerHeadCallBack( sender, eventType)
    if sender and sender:getTag()then
        local tag = sender:getTag()
        if eventType == ccui.TouchEventType.began then
            self:showPlayerInfo( tag )
        end
    end
end


function FishMainLayer:showPlayerInfo( chair )
    local viewChair = FishGlobal.getViewChair( chair)
    self:getParent():showPlayerInfoLayer()
    local  playFace = self.playerFaces[ viewChair ]
    local _paras = { faceId = playFace.faceId , fileName = playFace.facePic  }
    self:getParent().m_fishPlayerInfoLayer:updatePlayerHeadView( _paras )
    local fishPlayer =  g_GameController:getPlayerInfoByChairId( chair )
    if not fishPlayer then
        --print("not fishPlayer")
       return
    end
    local paras = { sex = fishPlayer:getSex(), accoundId = fishPlayer:getAccount(), niceName = fishPlayer:getNickName(), score  = fishPlayer:getPowerMaxScore() , gold = fishPlayer:getCoin()  }
    self:getParent().m_fishPlayerInfoLayer:updatePlayerInfoView( paras )
end


--设置角度
function FishMainLayer:setCannonRotation( viewCharirId , rotation )
    if self.m_playerNodeViews[ viewCharirId ] then
       self.m_playerNodeViews[ viewCharirId ].cannonSpr:setRotation( rotation )
       self.m_playerNodeViews[viewCharirId].shipNode.m_pPaoTa:setRotation(rotation)
       --self.m_playerNodeViews[ viewCharirId ].cannonScoreImg:setRotation( rotation )
       --self.m_playerNodeViews[ viewCharirId ].shipScoreLabel:setRotation( rotation )
    end
end

function FishMainLayer:fire( viewCharirId )
    if viewCharirId then
       if viewCharirId > 0 and viewCharirId <=  FishGlobal.PlayerCount then
          self.cannonMoveBacks[ viewCharirId ] = 1
          self.cannonMoveFronts[ viewCharirId ] = 0
          if self.m_playerNodeViews[viewCharirId].m_bIsFire ~= true then
            self.m_playerNodeViews[viewCharirId].m_bIsFire = true
            self.m_playerNodeViews[viewCharirId].shipNode.m_pPaoTa:setAnimation(0, "open fire", true)
          end
          self.m_playerNodeViews[viewCharirId].m_fFireTime = 0.25
       end
    end
end

function FishMainLayer:update( dt )
   for i = 1,FishGlobal.PlayerCount  do
       if self.cannonMoveBacks[ i ] == 1 then
          if self.cannonAnchorpointY[ i ] > 0.4 then
             self.cannonMoveFronts[ i ] = 1
             self.cannonMoveBacks[ i ]  = 0
             self.cannonAnchorpointY[ i ] = 0.4
             self.m_playerNodeViews[ i ].cannonSpr:setAnchorPoint( cc.p( 0.5 , 0.4 ) )
          else
             self.cannonAnchorpointY[ i ] = self.cannonAnchorpointY[ i ] + dt
             self.m_playerNodeViews[ i ].cannonSpr:setAnchorPoint( cc.p( 0.5 , self.cannonAnchorpointY[ i ] ) )
             
          end
       elseif self.cannonMoveFronts[ i ] == 1 then
            if self.cannonAnchorpointY[ i ] < 0.3 then
               self.cannonMoveFronts[ i ] = 0
               self.cannonMoveBacks[ i ] = 0
               self.cannonAnchorpointY[ i ] = 0.3
               self.m_playerNodeViews[ i ].cannonSpr:setAnchorPoint(cc.p(0.5,0.3))
            else
               self.cannonAnchorpointY[ i ] = self.cannonAnchorpointY[ i ] - dt
               self.m_playerNodeViews[ i ].cannonSpr:setAnchorPoint( cc.p( 0.5,self.cannonAnchorpointY[ i ] ) )
            end
       end
       if self.m_playerNodeViews[i].m_bIsFire == true then
            if self.m_playerNodeViews[i].m_fFireTime ~= nil then
                self.m_playerNodeViews[i].m_fFireTime = self.m_playerNodeViews[i].m_fFireTime - dt
                if self.m_playerNodeViews[i].m_fFireTime <= 0 then
                    self.m_playerNodeViews[i].m_fFireTime = 0
                    self.m_playerNodeViews[i].m_bIsFire = false
                    self.m_playerNodeViews[i].shipNode.m_pPaoTa:setAnimation(0, "standby", true)
                end
            end
        end

   end
   self.clickTime = self.clickTime + dt
   
   -- 暴击技能
   for i = 1,FishGlobal.PlayerCount  do
      local skillData =  FishSkillManager:getInstance():getSkillDataBySkillType( FishSkillManager.skillType.CRITSKILL ,i)
      if skillData then
         if skillData.m_isOpen then
            skillData.m_skillCD = skillData.m_skillCD - dt
            if skillData.m_skillCD <= 0 then
               skillData.m_isOpen = false
               EffectManager.hideCritAni( { viewChair = i } )
            end
         end
      end
   
   end
   -- 冰冻技能
   if EffectManager.isfrozen then  
       local frozenCount = 0
       for i = 1,FishGlobal.PlayerCount  do
          local frozenSkillData =  FishSkillManager:getInstance():getSkillDataBySkillType( FishSkillManager.skillType.FROZENSKILL ,i)
          if frozenSkillData then
            if frozenSkillData.m_isOpen then
                frozenSkillData.m_skillCD = frozenSkillData.m_skillCD - dt
                if frozenSkillData.m_skillCD <= 0 then
                   frozenSkillData.m_isOpen = false
                   frozenCount = frozenCount + 1
                end
            else
                frozenCount = frozenCount + 1
            end
          end
       end
       if frozenCount == FishGlobal.PlayerCount then
          EffectManager.unfreeze( 1 , self:getParent() )
       end
    end

   if self.isUnfolding then
      self.taskPanelTime = self.taskPanelTime + dt
      if self.taskPanelTime > 10 then
         if self.isSwithTaskPaneled then
            self.taskPanelTime = 0
            self.isSwithTaskPaneled = false
            self:swithUpgradShiBegin()
         end
      end
   end
   
   if self.isSetOpen then
      self.setPanelTime = self.setPanelTime + dt
      if  self.setPanelTime  > 10 then
         if self.isSwithSetPaneled then
            self.isSwithSetPaneled = false
            self.setPanelTime = 0 
            self:swithSetPanel()
         end
      end
   end
   if not self.isShowTimeOutTips then
       self.fireOutTime = self.fireOutTime + dt 
       if self.fireOutTime >= 60  then
          TOAST("2分钟无发炮将被请离出房间哦")
          self.fireOutTime = 0
          self.isShowTimeOutTips = true
       end
   end
   -- 设置叠加模式
   EffectManager.blendSprs()
end


-- 切换炮台
function FishMainLayer:changeCannon(viewCharirId, vipLevel , isplay )
    --print("切换炮台")
    if viewCharirId then
       if viewCharirId > 0 and viewCharirId <=  FishGlobal.PlayerCount then
           if isplay then
              if viewCharirId == FishGlobal.myViewChair then
                 FishSoundManager:getInstance():playEffect( FishSoundManager.SwitchSilo )
              end
           end

           local pPaoTa = self.m_playerNodeViews[ viewCharirId ].shipNode.m_pPaoTa
           if pPaoTa ~= nil then
                self.m_playerNodeViews[ viewCharirId ].shipNode:removeChild(pPaoTa)
           end
           local pataPath = string.format("buyu_p_plist/paotai/paotai%02d/paotai%02d", vipLevel, vipLevel)
           pPaoTa = sp.SkeletonAnimation:createWithJsonFile(pataPath..".json", pataPath..".atlas")
           self.m_playerNodeViews[ viewCharirId ].shipNode:addChild(pPaoTa)
           pPaoTa:setAnimation(0,"standby", true)
           self.m_playerNodeViews[ viewCharirId ].shipNode.m_pPaoTa = pPaoTa
           local cannonPos = cc.p(self.m_playerNodeViews[ viewCharirId ].cannonSpr:getPosition())
           self.m_playerNodeViews[viewCharirId].shipNode.m_pPaoTa:setPosition(cannonPos)
           pPaoTa:setRotation(self.m_playerNodeViews[viewCharirId].cannonSpr:getRotation())
        --   local powerItem =  FishGameDataController:getInstance():getCannonByItemId( itemId ) 
            local path = string.format("cannon/cannon_gun_%d.png",vipLevel) 
            local path1 = string.format("cannon/cannon_gun_%d_bg.png",vipLevel) 
                    -- powerPic = "buyu_textures/"..powerPic
                    -- local texture =   cc.Director:getInstance():getTextureCache():addImage( powerPic )
            self.m_playerNodeViews[ viewCharirId ].cannonSpr:setTexture( path ) 
            --self.m_playerNodeViews[ viewCharirId ].bg_ship:loadTexture( path1 )  
            --self.m_playerNodeViews[ viewCharirId ].cannonScoreImg:loadTexture( powerItem.szPowerScoreSource, 1 )
            self.m_playerNodeViews[ viewCharirId ].cannonSpr:setVisible(false)
            --self.m_playerNodeViews[ viewCharirId ].cannonSpr:setPositionY(-25)
            
        end
        if viewCharirId == FishGlobal.myViewChair then
           self:setFireTimeLimit()
        end
    end
end

function FishMainLayer:showOpenLevel(viewCharirId, score )
    if viewCharirId then
       if viewCharirId > 0 and viewCharirId <=  FishGlobal.PlayerCount then 
          local sooreStr = string.format("%d",score)
          --self.m_playerNodeViews[viewCharirId].shipScoreLabel:setString( sooreStr )
          self.m_playerNodeViews[viewCharirId].shipScoreAtlas:setString( score*0.01 )
      end
    end
end


-- 获取炮台的世界坐标位置
function FishMainLayer:getCannonPos( viewCharirId )
    local   worldPos = cc.p(0,0)    
    if viewCharirId then
       if viewCharirId > 0 and viewCharirId <=  FishGlobal.PlayerCount then
            if not self.powerworldPos[ viewCharirId ] then
               local posX,posY = self.m_playerNodeViews[ viewCharirId ].cannonSpr:getPosition()
               local  worldPosX, worldPosY = self.m_playerNodeViews[ viewCharirId ].shipNode:convertToWorldSpace( cc.p( posX , posY ))
               worldPos = cc.p(worldPosX, worldPosY )
               self.powerworldPos[ viewCharirId ] = worldPos
            else
               worldPos = self.powerworldPos[ viewCharirId ]
            end
       end
    end
    return worldPos
end

-- 获取炮台的转角
function FishMainLayer:getCannonRotation( viewCharirId )
    local  rotation = 0
    if viewCharirId then
       if viewCharirId > 0 and viewCharirId <=  FishGlobal.PlayerCount then
           rotation = self.m_playerNodeViews[ viewCharirId ].cannonSpr:getRotation()
       end
    end
    return rotation
end

-- 刷新玩家金币
function FishMainLayer:updatePlayerGoldView( viewCharirId , goldNum )
    if self.m_playerNodeViews[viewCharirId] then
       self.m_playerNodeViews[viewCharirId].goldValueAtlas:setString( goldNum )
       local _goldNum = tonumber( goldNum ) 
       
       if _goldNum <= 0 then
          self.m_playerNodeViews[viewCharirId].bankruptImg:setVisible( true )

       else
          self.m_playerNodeViews[viewCharirId].bankruptImg:setVisible( false )
       end
    end
end

function FishMainLayer:updatePlayerYbView(viewCharirId , ybNum )
    if self.m_playerNodeViews[viewCharirId] then   
        self.m_playerNodeViews[viewCharirId].yuanbaoNumLabel:setString( ybNum )
        local yuanbaoLabelContentSize = self.m_playerNodeViews[viewCharirId].yuanbaoNumLabel:getContentSize()
        self.m_playerNodeViews[viewCharirId].yuanBaoBg:setContentSize( cc.size(40 + yuanbaoLabelContentSize.width,24))
    end
end
      

function FishMainLayer:setFireTimeLimit()
    local fishPlayer = g_GameController:getPlayerInfoByChairId( FishGlobal.myChair )
    local powerItem = FishGameDataController:getInstance():getCannonByItemId( fishPlayer.m_nPowerItemID )
    if powerItem then
      if fishPlayer:getSeepType() == FishGlobal.SeepType.fast then
          self.timeLimit = (1/ powerItem.nShootSpeed)/FishMainLayer.addfireFrequency
       else
          self.timeLimit = 1/ powerItem.nShootSpeed
       end
       --print("self.timeLimit",self.timeLimit)
    end
end

--切换战舰皮肤
function FishMainLayer:changeShipModel( index )
    
end

function FishMainLayer:onTouchBegan(touch, evnt)
      local touchPos = touch:getLocation()
      -- 触摸点击的位置
      self.m_clickPosition = touchPos
      local lockSkillData = FishSkillManager:getInstance():getSkillDataBySkillType(FishSkillManager.skillType.LOCKSKILL, FishGlobal.myViewChair )
      -- print("lockSkillData")
      -- dump( lockSkillData )
      --if not lockSkillData.m_isOpen then
    if (not self.lockCheckbox:isSelected()) then
        -- 炮台的位置
        local cannonPos = self:getCannonPos( FishGlobal.myViewChair )
        if cannonPos then
            local temp_y = touchPos.y - cannonPos.y
            local temp_x = touchPos.x - cannonPos.x
            local angle = math.atan2(temp_y,temp_x)/math.pi * 180.0
            local nangle = 90 - angle
            self:setCannonRotation(FishGlobal.myViewChair, nangle )
        end
      end
     
      local isTouchFish = false
      local touchedFish = nil
      if self.lockCheckbox:isSelected() then
          local fishList = self:getParent():getFishList()
          for k,fish in pairs( fishList ) do
              if not tolua.isnull( fish ) then
                  -- local fishContenSize = fish:getContentSize()
                  -- local fishPos = fish:convertToNodeSpace( self.m_clickPosition )
                  -- if cc.rectContainsPoint(cc.rect(0, 0, fishContenSize.width, fishContenSize.height), fishPos) then
                  --    isTouchFish = true
                  --    self:getParent():setLockFishSignPos(cc.p(fish:getPositionX() ,fish:getPositionY()))
                  --    self:getParent():setLockFishVisible( true )
                  --    FishSkillManager:getInstance():setCurrentLockTarget( fish )
                  --    self:getParent():setFishsLockedSign()
                  --    fish:setIsLocked( true )
                  -- end
                 if fish:getFishType() > FishGlobal.minCannonFishType  and fish:isCollisionPoint( self.m_clickPosition ) then
                    self.m_LockFireFishType = fish:getFishType()
                     isTouchFish = true
                     touchedFish = fish
                     -- self:getParent():setLockFishSignPos(cc.p(fish:getPositionX() ,fish:getPositionY()))
                     -- self:getParent():setLockFishVisible( true )
                     -- FishSkillManager:getInstance():setCurrentLockTarget( fish )
                     -- self:getParent():setFishsLockedSign()
                     -- fish:setIsLocked( true )
                     break
                 end
              end
          end
      end
     
      local isCancelLock = false
      local currentLockTarget = self:getParent():getLockTargetByChair( FishGlobal.myChair )
    if (isTouchFish) then
        if (not tolua.isnull(touchedFish) and (touchedFish ~= currentLockTarget or tolua.isnull(currentLockTarget))) then
            self:LockFish(touchedFish)
        end
    end


    --   if isTouchFish then
    --      if tolua.isnull( self.lastClickFish ) then -- 上一次没有点中鱼
    --         self.lastClickFish =  touchedFish
    --         self.lastTime = socket.gettime()
    --         -- 取消锁定
    --         isCancelLock = true
    --      else
    --          self.crurentTime = socket.gettime()
    --          local timeInterval = self.crurentTime - self.lastTime
    --          self.lastTime = self.crurentTime
    --          if timeInterval <= 0.3 then
    --             if self.lastClickFish ==  touchedFish then
    --                -- 请求锁定
    --                if not tolua.isnull(currentLockTarget) and currentLockTarget ~= touchedFish then
    --                   self:getParent():setLockLineAndAimVisibleByChair( FishGlobal.myChair ,  true )
    --                   self:getParent():setLockTargetByChair( FishGlobal.myChair, touchedFish  )
    --                   self.lastClickFish = touchedFish
    --                   g_GameController:reqLockFishOper({ touchedFish:getFishID() }  )
    --                   self:autoLockFireBegin()
    --                else
    --                   if tolua.isnull( currentLockTarget ) then
    --                        self:getParent():setLockLineAndAimVisibleByChair( FishGlobal.myChair ,  true )
    --                        self:getParent():setLockTargetByChair( FishGlobal.myChair, touchedFish  )
    --                        self.lastClickFish = touchedFish
    --                        g_GameController:reqLockFishOper({ touchedFish:getFishID() } )
    --                        self:autoLockFireBegin()
    --                   else
    --                        self.lastClickFish = touchedFish
    --                        -- 取消锁定
    --                        --isCancelLock = true
    --                   end
    --                end
    --             else
    --                 self.lastClickFish = touchedFish
    --                 -- 取消锁定
    --                 isCancelLock = true
    --             end
    --          else
                
    --             if not tolua:isnull(currentLockTarget) and currentLockTarget == touchedFish then
    --                 self.lastClickFish = touchedFish
    --                 -- 取消锁定
    --                 isCancelLock = true
    --             else
    --                 self.lastClickFish = touchedFish
    --                 -- 取消锁定
    --                 isCancelLock = true
    --             end
    --          end
    --      end
    --   else
    --      self.lastClickFish = nil
    --      self.lastTime = socket.gettime()
    --      -- 取消锁定鱼
    --      isCancelLock = true
    --   end

    --   if isCancelLock and not tolua.isnull( currentLockTarget ) and self.lockCheckbox:isSelected() then
    --     -- 请求取消锁定
    --     self:getParent():setLockTargetByChair( FishGlobal.myChair, nil  )
    --     self:getParent():setLockLineAndAimVisibleByChair( FishGlobal.myChair ,  false )
    --     g_GameController:reqLockFishOper( { 0 }  )
    --     self:autoLockFireEnd()
    --   end

      if not  self.autoFireCheckBox:isSelected() and not( not tolua.isnull( currentLockTarget ) and self.lockCheckbox:isSelected() )  then
          self:longPressFireBegin()
          if self.clickTime >= (self.timeLimit - 0.1 )  then
              self.clickTime = 0
              self:myFire( self.m_clickPosition )
          end
      end
      self.m_pressClick = self.m_pressClick + 1

      if self.m_playerNodeViews[ FishGlobal.myViewChair ] then
         if self.m_playerNodeViews[ FishGlobal.myViewChair ].tipNode:isVisible() then
            self.m_playerNodeViews[ FishGlobal.myViewChair ].tipNode:setVisible( false )
         end
      end
      
     if self.isUnfolding then
        if self.isSwithTaskPaneled then
            self.isSwithTaskPaneled = false
            self:swithUpgradShiBegin()
        end
     end
   
     if self.isSetOpen then
        if self.isSwithSetPaneled then
           self.isSwithSetPaneled = false
           self:swithSetPanel()
        end
     end


      return true
end

function FishMainLayer:onTouchMoved(touch,event)
      local touchPos = touch:getLocation()
      self.m_clickPosition = touchPos
      local cannonPos = self:getCannonPos( FishGlobal.myViewChair )
      if cannonPos then
         local temp_y = touchPos.y - cannonPos.y
         local temp_x = touchPos.x - cannonPos.x
         local angle = math.atan2(temp_y,temp_x)/math.pi * 180.0
         local nangle = 90 - angle
         self:setCannonRotation(FishGlobal.myViewChair, nangle )
      end
end


function FishMainLayer:onTouchEnded(touch, evnt)
     self.m_pressClick = self.m_pressClick - 1
     if not  self.autoFireCheckBox:isSelected() then
          self:longPressFireEnd()
     end
end

function FishMainLayer:onTouchCancelled(touch, evnt)
     self.m_pressClick = self.m_pressClick - 1
     --TOAST(string.format("点击取消:%d",self.m_pressClick))
     if not  self.autoFireCheckBox:isSelected() then
          self:longPressFireEnd()
     end
end

--按钮操作回调
function  FishMainLayer:OnBtnOperationCallBack( sender, eventType)
    if sender and sender:getTag()then
        local tag = sender:getTag()
        if eventType == ccui.TouchEventType.ended then
             FishSoundManager:getInstance():playEffect( FishSoundManager.ButtonPressed )
             if tag ==  FishGlobal.OPERATION_CANNON_ADD then
              -- 切换高等级的炮
                local isChange =  g_GameController:addPowerLevelIndex()
                if isChange then
                    local openLevel =  g_GameController:getOpenLevel()
                    if openLevel ~= 0  then
                       g_GameController:reqChangePower(   openLevel )
                    end
                end
             elseif tag == FishGlobal.OPERATION_CANNON_DEC then
              -- 切换低等级的炮
                local isChange = g_GameController:decPowerLevelIndex()
                if isChange then
                    local openLevel =  g_GameController:getOpenLevel()
                    if openLevel ~= 0 then
                       g_GameController:reqChangePower(   openLevel )
                    end 
                end
             elseif tag == FishGlobal.OPERATION_CANNON_UPFOLDING then
                -- 控制升级面板的收缩
                if self.isSwithTaskPaneled then
                   self.isSwithTaskPaneled = false
                   self:swithUpgradShiBegin()
                end

             elseif tag == FishGlobal.OPERATION_CANNON_UPSHIPlEVEL then
               -- 升级主舰

             elseif tag == FishGlobal.OPERATION_SET_MORE  then
                -- 显示更多
                if self.isSwithSetPaneled then
                    self.isSwithSetPaneled = false
                    self:swithSetPanel()
                end
             elseif tag == FishGlobal.OPERATION_EXIT_GAME then
                -- 退出游戏
                self:unscheduleUpdate()
                removeMsgCallBack(self, MSG_FUNCSWITCH_UPDATE)
                self:stopAllTimer() 
                self.m_bulletManager:removeAlBulletAnimation()
                -- 移除子弹资源
                self.m_bulletManager:removeBulletResouce()
                self:getParent():showFishExitGameLayer()
                
             elseif tag == FishGlobal.OPERATION_LOCK_FISH then

             elseif tag == FishGlobal.OPERATION_MUST_SET then
                -- 设置
                self:getParent():showMusicSetLayer()
             elseif tag == FishGlobal.OPERATION_FISH_LIST then
                -- 鱼种
                --TOAST("敬请期待")
                self:getParent():showFishIllustrateLayer()

             elseif tag == FishGlobal.OPERATION_CHAT then
                -- 聊天
                self:getParent().m_fishChatSystemLatyer:setChatBoxVisble( true )

                --self:getParent().m_background:setJump()
             elseif tag == FishGlobal.OPERATION_SHOP then
                -- 商城
                self:getParent():showShopLayer()
                --cc.Director:getInstance():purgeCachedData()
                -- local params = { prarentNode = self:getParent() }
                -- EffectManager.redPacketsDropOutNew( params )
             elseif tag == FishGlobal.OPERATION_ACTIVITY then
                -- 活动
                self:getParent():showTaskActiveAwardsLayer()
                g_GameController:reqActiveTaskAward( {} )
                --cc.Director:getInstance():purgeCachedData()
            elseif tag == -1 then
                 local GameRecordLayer = GameRecordLayer.new(2)
            self:addChild(GameRecordLayer)   
            GameRecordLayer:setScaleX(display.scaleX) 
             ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", {106})
             end
        end    
    end
end


function  FishMainLayer:onAutoFireCheckBox( sender, eventType)
    if sender then
        FishSoundManager:getInstance():playEffect( FishSoundManager.ButtonPressed )
        local name = sender:getName()
       if eventType ==  ccui.CheckBoxEventType.selected then
            if name == "autofire_checkbox" then
               self:autoFireBegin()
            elseif name == "speed_checkbox" then
               --self.timeLimit = self.timeLimit/2
               g_GameController:reqChangeSpeed4Cannon( { FishGlobal.SeepType.slow } )
            elseif name == "lock_checkbox" then
                -- if (not self.autoFindLockFishSchedule) then
                --     self.autoFindLockFishSchedule = scheduler.scheduleGlobal(handler(self, self.updateFindLockFish), 1)
                -- end
                self.m_bIsLockFish = true
            end
       elseif eventType == ccui.CheckBoxEventType.unselected then
            if name == "autofire_checkbox" then
               self:autoFireEnd() 
            elseif name == "speed_checkbox" then
               --self.timeLimit = self.timeLimit * 2
                g_GameController:reqChangeSpeed4Cannon( { FishGlobal.SeepType.fast } )
            elseif name == "lock_checkbox" then
                self.m_bIsLockFish = false
                local lockTarget = self:getParent():getLockTargetByChair( FishGlobal.myChair )
                if not tolua.isnull( lockTarget ) then 
                   self:getParent():setLockTargetByChair( FishGlobal.myChair, nil  )
                   self:getParent():setLockLineAndAimVisibleByChair( FishGlobal.myChair ,  false )
                   g_GameController:reqLockFishOper( { 0 } )
                   self:autoLockFireEnd()
                end
            end
       end
    end
end

-- 设置发炮频率
function FishMainLayer:setFirePowerFrequency( nSeepType )
    if nSeepType == FishGlobal.SeepType.slow then
       self:setFireTimeLimit()
       if  self.lockCheckbox:isSelected() then
           self:autoLockFireBegin()
       end
       if self.autoFireCheckBox:isSelected() then
          self:autoFireBegin()
       end
    elseif nSeepType == FishGlobal.SeepType.fast then
       self:setFireTimeLimit()
       if  self.lockCheckbox:isSelected() then
           self:autoLockFireBegin()
       end
       if self.autoFireCheckBox:isSelected() then
          self:autoFireBegin()
       end
    end
end

-- 自动开火处理
function FishMainLayer:autoFireBegin()
    if self.autoFireTimer then
        scheduler.unscheduleGlobal( self.autoFireTimer )
        self.autoFireTimer = nil 
    end
 
    self.autoFireTimer = scheduler.scheduleGlobal(handler( self, self.autotFire ), self.timeLimit )
end

function FishMainLayer:autoFireEnd()
    if not self.autoFireTimer then
        return
    end
    scheduler.unscheduleGlobal(self.autoFireTimer)
    self.autoFireTimer = nil
end

-- 用于处理连续发炮
function FishMainLayer:autotFire()
    --local lockSkillData = FishSkillManager:getInstance():getSkillDataBySkillType(FishSkillManager.skillType.LOCKSKILL,FishGlobal.myViewChair )
    local currentLockTarget = self:getParent():getLockTargetByChair( FishGlobal.myChair )
    if  self.autoFireCheckBox and  self.autoFireCheckBox:isSelected() and not ( not tolua.isnull( currentLockTarget ) and self.lockCheckbox:isSelected() )  then
        self:myFire( self.m_clickPosition )
    end
end

-- 长按屏幕开火处理
function FishMainLayer:longPressFireBegin()
    if self.longPressFireTimer then
        scheduler.unscheduleGlobal( self.longPressFireTimer )
        self.longPressFireTimer = nil 
    end
    self.longPressFireTimer = scheduler.scheduleGlobal(handler(self,self.longPressFire), self.timeLimit )
    --print("longPressFireBegin()")
end

function FishMainLayer:longPressFireEnd()
    --print("FishMainLayer:longPressFireEnd()")
    if not self.longPressFireTimer then
        return
    end
    scheduler.unscheduleGlobal(self.longPressFireTimer)
    self.longPressFireTimer = nil
end

function FishMainLayer:longPressFire()
     self:myFire( self.m_clickPosition )
     --print("FishMainLayer:longPressFire()")
end

function FishMainLayer:reqUseSkill( __skillType )
    local fishPlayer =  g_GameController:getPlayerInfoByChairId( FishGlobal.myChair )
    local skillData = FishGameDataController:getInstance():getSkillDataByType( __skillType )
    if fishPlayer then
        if fishPlayer:getCoin() > 0 then
            if skillData then
               local itemCount =  g_GameController:getSkillItemCountByType( skillData.nType )
               if itemCount <= 0 then
                  self:getParent():showShopLayer()
                  self:getParent():shopJumpToPercentHorizontal( skillData.nItemId )
               else 
                  g_GameController:reqUseSkill( skillData.nId )
               end
            end
        else
          -- TOAST("您的金币不足，无法继续开炮!")
          self:showBuyGoldTips()
        end
    end
end

function FishMainLayer:showBuyGoldTips()
    --if g_GameController:getClubId() > 0 then
       TOAST("您的金币不足,请前往商城购买金币!")
    -- else
    --     if self.bugGoldDlg then
    --        return
    --     end
    --     self.bugGoldDlg = DlgAlert.new()
    --     self.bugGoldDlg:TowSubmitAlert({title="提示",message="您的金币不足,是否前往商城购买金币?"})
    --     self.bugGoldDlg:setBtnAndCallBack("确定","取消",handler(self, self.onExitGame),handler(self, self.onCloseBuyGoldTips))
    --     self.bugGoldDlg:showDialog()
    --     self.bugGoldDlg:enableTouch(false)
    --     self.bugGoldDlg:setBackBtnEnable(false)
    -- end
end
function FishMainLayer:onCloseBuyGoldTips()
     self.bugGoldDlg:closeDialog()
     self.bugGoldDlg = nil
end

--[[
function FishMainLayer:onExitGame()
    self:getParent():exitGame()
    self.bugGoldDlg:closeDialog()
    self.bugGoldDlg = nil 
end
--]]

function FishMainLayer:skill( skillType ,viewChair )
      local skillData = FishSkillManager:getInstance():getSkillDataBySkillType( skillType,viewChair )
      if skillType == FishSkillManager.skillType.LOCKSKILL then
         if viewChair == FishGlobal.myViewChair then
             self.skillBtns[ skillType ]:skillProgress(0,skillData.m_skillCD )
             self:autoLockFireBegin()
             FishSoundManager:getInstance():playEffect( FishSoundManager.SkillLock )
         end
      elseif skillType == FishSkillManager.skillType.FROZENSKILL then
         if viewChair == FishGlobal.myViewChair then
             self.skillBtns[ skillType ]:skillProgress(0,skillData.m_skillCD )
             self:frozenFishsAni()
         end
         FishSoundManager:getInstance():playEffect( FishSoundManager.SkillIce )
         self:frozenFishs( skillData.m_skillCD)
      elseif skillType == FishSkillManager.skillType.CRITSKILL then
         if viewChair == FishGlobal.myViewChair then
             self.skillBtns[ skillType ]:skillProgress(0, skillData.m_skillCD )
         end
         FishSoundManager:getInstance():playEffect( FishSoundManager.SkillCrit )
         local crtlSprPos =  self:getCannonPos( viewChair )
         EffectManager.playCritAni( { prarentNode = self:getParent() , pos = crtlSprPos ,viewChair = viewChair  } )
      end
end

function FishMainLayer:skillProgress( skillType , per , skillCD )
   --self.skillBtns[ skillType ]:skillProgress(per, skillCD )
end


function FishMainLayer:skillEnd( __skillType )
   if __skillType == FishSkillManager.skillType.LOCKSKILL then
      print("self:autoLockFireEnd()")
      self:autoLockFireEnd()
      --FishSkillManager:getInstance():setSkillDataIsOpen( FishSkillManager.skillType.LOCKSKILL , FishGlobal.myViewChair , false )
   elseif __skillType == FishSkillManager.skillType.FROZENSKILL then
      FishSkillManager:getInstance():setSkillDataIsOpen( FishSkillManager.skillType.FROZENSKILL , FishGlobal.myViewChair , false )
   elseif __skillType == FishSkillManager.skillType.CRITSKILL then
      FishSkillManager:getInstance():setSkillDataIsOpen(  FishSkillManager.skillType.CRITSKILL , FishGlobal.myViewChair , false )
      EffectManager.hideCritAni( {viewChair = FishGlobal.myViewChair} )
      self:critSkillEnd()
   end
end

function FishMainLayer:autoLockFireBegin()
    --FishSkillManager:getInstance():setSkillDataIsOpen( FishSkillManager.skillType.LOCKSKILL,FishGlobal.myViewChair, true )
    if self.autoLockFireTimer then
        scheduler.unscheduleGlobal( self.autoLockFireTimer )
        self.autoLockFireTimer = nil 
    end
    self.autoLockFireTimer = scheduler.scheduleGlobal(handler(self,self.autoLockFire), self.timeLimit )
end

function FishMainLayer:createLockLineSpr()
    cc.SpriteFrameCache:getInstance():addSpriteFrames("buyu_p_plist/p_buyu_commom_new_1.plist","buyu_p_plist/p_buyu_commom_new_1.png")
    self.locklineImg =  ccui.ImageView:create("fish_lockline.png",ccui.TextureResType.plistType )
    self.locklineImg:setAnchorPoint( cc.p( 0,0.5 ) )
    self.locklineImg:setScale9Enabled( true )
    local lineImgSize =   self.locklineImg:getContentSize()
    self.locklineImg:ignoreContentAdaptWithSize( true )
    self.locklineImg:setContentSize( cc.size( 300 , 8 ) )
    self.locklineImg:setVisible( false )
    self:addChild( self.locklineImg )
    local cannonPos = self:getCannonPos( FishGlobal.myViewChair )
    self.locklineImg:setPosition( cannonPos )
end

-- 锁定技能开火
function FishMainLayer:autoLockFire()
    local currentLockTarget = self:getParent():getLockTargetByChair( FishGlobal.myChair )
    if not tolua.isnull( currentLockTarget ) and self.lockCheckbox:isSelected() then
       local fishPosX,fishPosY = currentLockTarget:getPosition()
       local cannonPos = self:getCannonPos( FishGlobal.myViewChair )
       local pos = cc.p( fishPosX , fishPosY )
       self:myFire(pos, currentLockTarget )
    end
end


function FishMainLayer:autoLockFireEnd()
    print("FishMainLayer:autoLockFireEnd1")
    if not self.autoLockFireTimer then
        return
    end
    print("FishMainLayer:autoLockFireEnd2")
    self:getParent():setLockLineAndAimVisibleByChair( FishGlobal.myChair , false )
    self:getParent():setLockTargetByChair( FishGlobal.myChair , nil )
    scheduler.unscheduleGlobal( self.autoLockFireTimer )
    self.autoLockFireTimer = nil

    if (self.autoFindLockFishSchedule) then
        scheduler.unscheduleGlobal(self.autoFindLockFishSchedule)
    end
    self.autoFindLockFishSchedule = nil
end

function FishMainLayer:isLockFire()
    --return self.lockCheckbox:isSelected()
    return self.m_bIsLockFish
end

function FishMainLayer:LockFish(fish)
    if (not self:isLockFire()) then
        return
    end
    if (tolua.isnull(fish)) then
        return
    end
    self:getParent():setLockLineAndAimVisibleByChair( FishGlobal.myChair ,  false )
    self:getParent():setLockTargetByChair( FishGlobal.myChair, fish  )
    self.lastClickFish = fish
    g_GameController:reqLockFishOper({ fish:getFishID() } )
    self:autoLockFireBegin()
end


function FishMainLayer:updateFindLockFish(dt)
    --自动锁定鱼
    if (not self:isLockFire()) then
        return
    end
    local maxFish = self:getParent():getMaxTypeFish(self.m_LockFireFishType)
    if (tolua.isnull(maxFish) and self.m_LockFireFishType > (FishGlobal.minCannonFishType + 1)) then
        maxFish = self:getParent():getMaxTypeFish(FishGlobal.minCannonFishType + 1)
    end
    self:LockFish(maxFish)
end


-- 全屏冰冻特效
function FishMainLayer:frozenFishsAni()
     FishSkillManager:getInstance():setSkillDataIsOpen( FishSkillManager.skillType.FROZENSKILL , FishGlobal.myViewChair , true )
end

-- 冰冻方法
function FishMainLayer:frozenFishs( __skillCD )
    EffectManager.playfrozen( { prarentNode = self:getParent(),isAnimal = true , _type = 1 } )
    self:getParent():frozenFishs( __skillCD )
end

-- 解冻方法
function FishMainLayer:unfreezeFishs()
    self:getParent():unfreezeFishs()
end


-- 狂暴技能开启
function FishMainLayer:critSkillBegin()
     FishSkillManager:getInstance():setSkillDataIsOpen( FishSkillManager.skillType.CRITSKILL , FishGlobal.myViewChair , true )
end


function FishMainLayer:critSkillEnd()
     --FishSkillManager:getInstance():setSkillDataIsOpen( FishSkillManager.skillType.CRITSKILL , FishGlobal.myViewChair , false )
end

--终端玩家发炮
function FishMainLayer:myFire( pos , currentLockTarget )
    if g_GameController == nil then
        return
    end
    local fishPlayer =  g_GameController:getPlayerInfoByAccountId( Player.getAccountID() )
    if not fishPlayer then
       return
    end
    self.fireOutTime = 0
    self.isShowTimeOutTips = false
    -- 隐藏“我在这里”
    if self.m_playerNodeViews[ FishGlobal.myViewChair ] then
        if self.m_playerNodeViews[ FishGlobal.myViewChair ].tipNode:isVisible() then
           self.m_playerNodeViews[ FishGlobal.myViewChair ].tipNode:setVisible( false )
        end
    end

    local step = 1
    local critSkillData = FishSkillManager:getInstance():getSkillDataBySkillType( FishSkillManager.skillType.CRITSKILL, FishGlobal.myViewChair )
    if critSkillData and critSkillData.m_isOpen then
        local skillData = FishGameDataController:getInstance():getSkillDataByType( FishSkillManager.skillType.CRITSKILL )
        step = skillData.nValue1
    end
    local powerScore = FishGameDataController:getInstance():getPowerScoreByLevel( fishPlayer.m_nPowerLevel )
    local _coin =  powerScore * step*0.01
    --判断金币数量是否等于0,则不允许开炮
    if fishPlayer:getCoin() <  _coin then
       --self:getParent():showShopLayer()
       --TOAST("您的金币不足，无法继续开炮!")
       self:showBuyGoldTips()
       if self.autoFireCheckBox:isSelected() then
           self.autoFireCheckBox:setSelected( false )
           self.m_bIsLockFish = false
           self:autoFireEnd()
       end
       -- local lockSkillData = FishSkillManager:getInstance():getSkillDataBySkillType(FishSkillManager.skillType.LOCKSKILL,FishGlobal.myViewChair )
       if self.lockCheckbox:isSelected() then
          self:autoLockFireEnd()
       end
       FishSoundManager:getInstance():playEffect( FishSoundManager.NoBullet   )
       return
    end 
    if self:getParent():getBulletByAccountID( Player.getAccountID() ) >= 10 then
        if self.isFirstBulltLimiltTips then
           self.isFirstBulltLimiltTips = false
           if not self.isPowerTipsVisble then
              self.isPowerTipsVisble = true
              TOAST("炮弹达到上限!",function ()
                 self.isPowerTipsVisble = false
              end)
           end
        end
        return
    else
        self.isFirstBulltLimiltTips = true
    end

    local pItemID = fishPlayer:getPowerItemID()
    local plevel = fishPlayer:getPowerLevel()
   
    local outPos = cc.Director:getInstance():convertToUI( pos )
    local fishIndex = 0
    if not tolua.isnull( currentLockTarget )then
       fishIndex = currentLockTarget:getFishID()
    end
    local info  =  {
                      m_nAccountID = Player.getAccountID(), 
                      m_nPItemID   = pItemID,
                      m_nLevel     = plevel,
                      m_nX         = outPos.x / display.scaleX,
                      m_nY         = outPos.y / display.scaleY,
                      m_nStep      = step,
                      m_nSourceFish = fishIndex,
                   }
    -- 调用此父节点接口是为了统一流程            
    self:getParent():netFireMsg( info , true , currentLockTarget)
    local pos  = self.m_clickPosition
    if not tolua.isnull( currentLockTarget ) then
        pos = cc.p(currentLockTarget:getPosition())
    end
    local currentLockTargetPox = 1
    local fishPlayer =  g_GameController:getPlayerInfoByAccountId( Player.getAccountID() )
    if fishPlayer then
       g_GameController:reqFire( { pItemID, plevel, outPos.x / display.scaleX, outPos.y / display.scaleY , fishIndex} )
    end

end


--网络发炮消息
function FishMainLayer:netFire( info,isNative ,currentLockTarget)
      if g_GameController == nil then
        return
    end
    local bullet = nil
    if info.m_nAccountID == Player.getAccountID() and not isNative then -- 如果是终端发炮用户消息，则return
       return bullet
    end
    local fishPlayer =  g_GameController:getPlayerInfoByAccountId( info.m_nAccountID )
    if not  fishPlayer then
       --TOAST("获取玩家信息失败")
       return bullet
    end
    local chair = fishPlayer:getChair()
    local viewChair = fishPlayer:getViewChair()
    local curCoin    = info.m_nCoin                  -- 金币数量
    local outPos = cc.Director:getInstance():convertToGL( cc.p( info.m_nX * display.scaleX , info.m_nY * display.scaleY ) )
    local point = FishGlobal.convertToMatchViewChair({ wChairID = chair ,x = outPos.x ,y = outPos.y })
    local cannonPos = self:getCannonPos( viewChair )
    local temp_y = point.y - cannonPos.y
    local temp_x = point.x - cannonPos.x
    local angle = math.atan2(temp_y,temp_x)/math.pi * 180.0
    local nangle = 90 - angle
    self:setCannonRotation( viewChair, nangle )
    self:fire( viewChair )
    if self.m_bulletManager then
        -- 播放开炮的声音
        local powerItem =  FishGameDataController:getInstance():getCannonByItemId( info.m_nPItemID )
        local speed = 1000
        local isCrit = false
        if powerItem then
            speed =  powerItem.nBulletSpeed 
            if info.m_nStep > 1 then
              isCrit = true
              local critSkillData = FishGameDataController:getInstance():getSkillDataByType( FishSkillManager.skillType.CRITSKILL )
              if critSkillData then
                 speed =  speed * critSkillData.nValue2
              end
            end
            if fishPlayer:getSeepType() == FishGlobal.SeepType.fast then
               speed = speed * FishMainLayer.addSpeed
            end
        end
        bullet = self.m_bulletManager:createBullet( powerItem , isCrit , chair, fishPlayer.m_nVipLevel )
        local rotation = self:getCannonRotation( viewChair )
        -- if  rotation == 0 or rotation == 180 then
        --     rotation = rotation + 1
        -- end
        bullet:setRotation( rotation )
        bullet:setAnchorPoint( cc.p(0.5,0.5))
        
        local rad = math.rad(90 - bullet:getRotation())   
        local move_x = math.cos(rad) * speed
        local move_y = math.sin(rad) * speed
        local bulltInfo = { 
                        powerlevel   = info.m_nLevel ,   
                        powerItemId  = info.m_nPItemID,  
                        valid        = true,       
                        rebound      = true,
                        move_speed   = speed,
                        move_factorx = move_x,
                        move_factory = move_y,
                        accountID    = info.m_nAccountID,
                        bullet_id    = 10000,
                        chairId      = viewChair,
                        step         = info.m_nStep
                     }
        bullet:setInfo( bulltInfo )

        if info.m_nAccountID == Player.getAccountID() then
           bullet:setOwner( true )
        else
           if fishPlayer:isRobot() then
              bullet:setRobot( true )
           end
           if info.m_nSourceFish > 0 then
              local fishList =  self:getParent():getFishList()
              currentLockTarget = fishList[ info.m_nSourceFish ]
           end
        end
        if not tolua.isnull( currentLockTarget )then
           bullet:setLockTarget( currentLockTarget )
        end

       

        --设置生成位置
        bullet:setPosition(cc.p(cannonPos.x + math.cos(rad) * 45,cannonPos.y + math.sin(rad) * 45))

        if info.m_nAccountID == Player.getAccountID() then 
            -- 播放开炮的声音
            if powerItem then
               FishSoundManager:getInstance():playEffect( powerItem.szSoundSource )
            end
        end
       
    end
    return bullet
end

function FishMainLayer:clearView()
    -- 玩家头像
    for i=1, FishGlobal.PlayerCount do
        if i ~= FishGlobal.myViewChair then
            if self.playerFaces[i].needRemoveTextrue then
               if self.playerFaces[i].faceId  >= GlobalDefine.UserDefineHeadMin then
                  cc.Director:getInstance():getTextureCache():removeTextureForKey( self.playerFaces[i].facePic )
               end
            end
        end
    end

    self.playerFaces = {}
    for i=1,FishGlobal.PlayerCount do
        self.playerFaces[i] = {} 
        self.playerFaces[i].faceId = nil
        self.playerFaces[i].facePic = nil
        self.playerFaces[i].needRemoveTextrue = false
    end
    
    for i=1,FishGlobal.PlayerCount do
        self.m_playerNodeViews[ i ].shipNode:setVisible( false)
        self.m_playerNodeViews[ i ].attriNode:setVisible( false )
        --self.m_playerNodeViews[ i ].addBtn:setVisible( false )
        --self.m_playerNodeViews[ i ].decreaseBtn:setVisible( false )
        self.m_playerNodeViews[ i ].tipNode:setVisible( false )
    end
    self.autoFireCheckBox:setSelected( false )
    self.m_bIsLockFish = false
end

function FishMainLayer:stopAutoFire()
    if self.autoFireCheckBox:isSelected() then
       self.autoFireCheckBox:setSelected( false )
       self.m_bIsLockFish = false
       self:autoFireEnd()
    end
end

function FishMainLayer:stopAllTimer()
    -- 停止自动开火定时器
     self:autoFireEnd()
     -- 关闭长按定时器
     self:longPressFireEnd()
     -- 关闭自动锁定
     self:autoLockFireEnd()
    
     self:swithUpgradShiEnd()
end

--退出当前layer的时候调用，在这里都是做一些清除工作
function FishMainLayer:onExit()
     print("-----FishMainLayer:onExit()----")
     removeMsgCallBack(self, MSG_FUNCSWITCH_UPDATE)
     self:stopAllTimer() 
     self.m_bulletManager:removeAlBulletAnimation()
     -- 移除子弹资源
     self.m_bulletManager:removeBulletResouce()

     if (self.autoFindLockFishSchedule) then
        scheduler.unscheduleGlobal(self.autoFindLockFishSchedule)
     end
     self.autoFindLockFishSchedule = nil
end

function FishMainLayer:setRecordID(id)
    if id and id ~= "" then
        self.mTextRecord:setString("牌局ID:"..id)
    end
end

return  FishMainLayer
