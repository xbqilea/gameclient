--
-- Author: Your Name
-- Date: 2017-03-01 12:02:09
-- 捕鱼玩家
--
local FishingPlayer = class("FishingPlayer")

function FishingPlayer:ctor()
	self.m_chairId      	 = 0       -- 椅子号
	self.m_viewChairId       = 0       -- 视图号
    self.m_nAccountId        = 0       -- 玩家账户
	self.m_isHost       	 = false   -- 是否是主玩家
	self.m_faceID        	 = 1       -- 头像id
	self.m_sex               = 0       -- 性别 0 男 1女
	self.m_nickName          = ""      -- 昵称
    self.m_nCoin             = 0       -- 金币数量
    self.m_nVipLevel         = 0       -- vip等级
	self.m_nPowerItemID      = 3116    -- 炮的物品id"
	self.m_nPowerScore       = 0       -- 炮的威力
    self.m_nPowerLevel       = 0       -- 炮使用的等级
    self.m_powerMaxScore     = 0       -- 炮台最高倍数
    self.m_score             = 0       -- 积分
    self.m_powersNumer       = 0       -- 庄家的炮弹数
    self.m_degree            = 0       -- 玩家身份，1庄家,2普通玩家,3观众
    self.m_roomHost          = false   -- 是否是房主
    self.m_online            = 0
    self.m_facePic           = nil
    self.m_showFaceID        = nil
    self.m_waitTime          = 20      -- 旁观者等待时间
    self.m_goldIngot         = 0       -- 玩家每局获得的元宝
    self.m_nLockFishId       = 0       -- 锁定鱼的id,0代表没锁定
    self.m_nSeepType         = 0       -- 速度,0:慢炮，1:快炮
    self.m_bRobot            = 0       -- 是否机器人,0:普通玩家,1机器人
end


function FishingPlayer:initFishPlayer( _info )
	self.m_chairId      = _info.m_nChair           -- 椅子号
    self.m_nAccountId   = _info.m_nAccountId       -- 玩家账户
	self.m_faceID       = _info.m_nIcon            -- 头像id
    self.m_sex          = _info.m_nSex             -- 性别 0 男 1女 
	self.m_nickName     = _info.m_szName           -- 昵称
    self.m_nCoin        = _info.m_nCoin*0.01            -- 金币数量
  --  self.m_nVipLevel    = _info.m_nVipLevel        -- vip等级
	self.m_nPowerItemID = _info.m_nItemID          -- 炮的物品id"
    self.m_nPowerLevel  = _info.m_nLevel           -- 炮使用的等级
    self.m_powerMaxScore= _info.m_nMaxPower*0.01        -- 炮台最高倍数
    self.m_nLockFishId  = _info.m_nLockFishId      -- 锁定鱼的id,0代表没锁定
    self.m_nSeepType    = _info.m_nSeepType        -- 速度,0:慢炮，1:快炮
    self.m_bRobot       = _info.m_bRobot           -- 是否机器人,0:普通玩家,1机器人
    --self.m_goldIngot    = _info.m_nGoldIngot/10    -- 元宝数量
end

function FishingPlayer:initFishFriendPlayer( _info )
	self.m_chairId      = _info.m_nChair           -- 椅子号
    self.m_nAccountId   = _info.m_nAccountId       -- 玩家账户
	self.m_faceID       = _info.m_nIcon            -- 头像id
    self.m_sex          = _info.m_nSex             -- 性别 0 男 1女 
	self.m_nickName     = _info.m_szName           -- 昵称
    self.m_score        = _info.m_nCoin *0.01            -- 积分
    self.m_powersNumer  = _info.m_nPowers          -- 炮弹个数
    self.m_nPowerScore  = _info.m_nPowerScore*0.01      -- 炮弹倍数
    self.m_degree       = _info.m_nDegree          -- 玩家身份
    self.m_online       = _info.m_nOnline          -- 在线/下线
    self.m_waitTime     = _info.m_nWaitTime         -- 旁观者等待时间
end

function FishingPlayer:initClassicFishPlayer( _info )
	self.m_chairId      = _info.m_nChair           -- 椅子号
    self.m_nAccountId   = _info.m_nAccountId       -- 玩家账户
	self.m_faceID       = _info.m_nIcon            -- 头像id
    self.m_sex          = _info.m_nSex             -- 性别 0 男 1女 
	self.m_nickName     = _info.m_szName           -- 昵称
    self.m_nCoin        = _info.m_nCoin     *0.01        -- 金币数量
  --  self.m_nVipLevel    = _info.m_nVipLevel        -- vip等级
	self.m_nPowerItemID = _info.m_nItemID          -- 炮的物品id"
    self.m_powerMaxScore= _info.m_nMaxPower *0.01       -- 炮台最高倍数
    self.m_goldIngot    = _info.m_nGoldIngot/10    -- 元宝数量
    self.m_degree       = _info.m_nDegree          -- 玩家身份
    self.m_waitTime     = _info.m_nValidTime        -- 旁观者等待时间
end

function FishingPlayer:setAccount(m_nAccountId)
	self.m_nAccountId = m_nAccountId
end
function FishingPlayer:getAccount()
	return self.m_nAccountId
end

function FishingPlayer:setFaceID(face_id)
	self.m_faceID = face_id
end

function FishingPlayer:getFaceID()
	return self.m_faceID
end

function FishingPlayer:getCoin()
	return self.m_nCoin
end
function FishingPlayer:setnCoin( coin )
	self.m_nCoin = coin
end

function FishingPlayer:setNickName(nickname)
	self.m_nickName = nickname
end

function FishingPlayer:getNickName()
	return self.m_nickName
end

function FishingPlayer:isHost()
	return self.m_isHost
end

function FishingPlayer:setHost(b)
	self.m_isHost = b
end

function FishingPlayer:setChair(chair)
	self.m_chairId = chair
end

function FishingPlayer:getChair()
	return self.m_chairId
end
function FishingPlayer:getViewChair()
	return self.m_viewChairId
end

function FishingPlayer:setViewChair(v_chair)
	self.m_viewChairId = v_chair
end

function FishingPlayer:getVipLevel()
	return self.m_nVipLevel
end

function FishingPlayer:setVipLevel( vipLevel )
	self.m_nVipLevel = 0
end

function FishingPlayer:getPowerItemID()
	return self.m_nPowerItemID
end

function FishingPlayer:setPowerItemID( powerItemID )
	self.m_nPowerItemID = powerItemID
end

function FishingPlayer:getPowerScore()
    return self.m_nPowerScore
end

function FishingPlayer:setPowerScore( powerScore )
    self.m_nPowerScore = powerScore
end

function  FishingPlayer:setPowerLevel( powerLevel )
	 self.m_nPowerLevel  = powerLevel
end

function FishingPlayer:getPowerLevel()
	return self.m_nPowerLevel
end

function FishingPlayer:getPowerMaxScore()
    return	self.m_powerMaxScore
end

function FishingPlayer:setPowerMaxScore( powerMaxScore )
    self.m_powerMaxScore = powerMaxScore*0.01
end

function FishingPlayer:getSex()
	return self.m_sex 
end

function FishingPlayer:setSex( sex )
	self.m_sex  = sex
end

function FishingPlayer:getScore()
	 return self.m_score
end

function FishingPlayer:setScore( score )
	 self.m_score = score
end

function FishingPlayer:isRoomHost()
	return self.m_roomHost
end

function FishingPlayer:setRoomHost( isRoomHost )
	  self.m_roomHost = isRoomHost
end

function FishingPlayer:getDegree()
	return  self.m_degree
end

function FishingPlayer:setDegree( degree )
	  self.m_degree = degree
end

function FishingPlayer:getOnline()
	return  self.m_online
end

function FishingPlayer:isOnline( online )
	  self.m_online = online
end

function  FishingPlayer:setPowerNumber( powersNumer )
	self.m_powersNumer = powersNumer
end

-- 获取头像路径
function FishingPlayer:getFacePic()
   return  self.m_facePic
end

function FishingPlayer:setFacePic( facePic)
	self.m_facePic = facePic
end

function FishingPlayer:setShowFaceId( showFaceId )
    self.m_showFaceID = showFaceId 
end

function FishingPlayer:setGoldIngot( goldIngot )
	self.m_goldIngot = goldIngot
end

function FishingPlayer:getGoldIngot()
	return  self.m_goldIngot
end

function FishingPlayer:setLockFishId( lockFishId )
	self.m_nLockFishId = lockFishId
end

function FishingPlayer:getLockFishId()
	return self.m_nLockFishId 
end

function FishingPlayer:setSeepType( nSeepType )
    self.m_nSeepType = nSeepType
end

function FishingPlayer:getSeepType()
	return self.m_nSeepType
end

function FishingPlayer:isRobot()
	local _isRobot = false
	if self.m_bRobot == 1 then
	   _isRobot = true
	end
	return _isRobot
end



return FishingPlayer  