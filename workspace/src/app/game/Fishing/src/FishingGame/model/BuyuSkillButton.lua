--
-- BuyuSkillButton
-- Author: chengzhanming
-- Date: 2017-05-02 10:24:29
-- 技能按钮组件
-- 
local  scheduler     =  require("framework.scheduler")

local BuyuSkillButton = class("BuyuSkillButton",function()
    return display.newNode()
end)

BuyuSkillButton.isSkillCoolTipsVisble = false

function BuyuSkillButton:ctor( param )
    self.skillType            = param.skillType                 -- 技能类型
    self.cdTime               = param.cdTime                    -- 技能cd时间
    self.stencilFileName      = param.stencilFileName           -- 半透明蒙版名称
    self.buttonNormalName     = param.buttonNormalName          -- 技能按钮图片
    self.buttonClickName      = param.buttonClickName           -- 技能按钮被点击状态
    self.buttonTextureResType = param.buttonTextureResType      -- 按钮图片类型
    self.progressSprTexture   = param.progressSprTexture        -- 进度条精灵图片
    self.itemCount            = 0                               -- 物品数量
    self.isProgress           = false                           -- 技能是否正在冷却中

    self:initBuyuSkillButton()
end

function BuyuSkillButton:initBuyuSkillButton()
	-- 技能按钮
    self.m_SkillButton = ccui.Button:create( self.buttonNormalName, self.buttonClickName ,"",ccui.TextureResType.plistType) 
    --self.m_SkillButton:setAnchorPoint(cc.p(0.5,0.5))
   -- self.m_SkillButton:setPosition( cc.p( 0 , 0) )
    self:addChild( self.m_SkillButton,-100 )
    self.m_SkillButton:addTouchEventListener(  handler(self,self.skillClickCallBack))

    -- 阴影模版
    self.m_StencilSpr = display.newSprite( self.stencilFileName )
    self.m_StencilSpr:setVisible( false )
    --self.m_StencilSpr:setAnchorPoint( cc.p( 0 , 0 ))
    --self.m_StencilSpr:setAnchorPoint( cc.p(0.5 , 0.5) )
    self:addChild( self.m_StencilSpr,0 )
    

    -- 旋转进度条精灵
    local progressSpr = display.newSprite( self.progressSprTexture  )
    self.m_ProgressTimer = cc.ProgressTimer:create( progressSpr ) 
    self.m_ProgressTimer:setType( cc.PROGRESS_TIMER_TYPE_RADIAL )
    self.m_ProgressTimer:setVisible(false)

    --self.m_ProgressTimer:setAnchorPoint(cc.p(0.5,0.5))
    --self.m_ProgressTimer:setPosition( cc.p(0,0))
    self:addChild( self.m_ProgressTimer,100 )

    self.countBg  = display.newScale9Sprite("#fish_bg_jns.png" ,0,-50,cc.size(55,20),cc.rect(10,10,10,10))
    self:addChild( self.countBg,101 )

    -- 物品个数
    self.itemCountLabel = display.newTTFLabel({
        text = tostring(self.itemCount),
        font = "ttf/jcy.TTF",
        size = 20,
        color = cc.c3b(237, 231, 18),
        })
    self:addChild( self.itemCountLabel ,102 )
    self.itemCountLabel:align(display.CENTER, 0, -50)

    self.timeLabelAtlas = cc.LabelAtlas:_create(0,"buyu_number/fish_js_num_orange.png",24,32,48)
    self.timeLabelAtlas:setAnchorPoint(cc.p(0.5,0.5))
    self.timeLabelAtlas:setVisible(false)
    self:addChild( self.timeLabelAtlas ,103 )
end

function BuyuSkillButton:setSkilItemCount( itemCount )
     self.itemCount  = itemCount
     self.itemCountLabel:setString( tostring( itemCount ) )
     local size =  self.itemCountLabel:getContentSize()
     if size.width > 55 then
        self.countBg:setContentSize(cc.size(size.width ,20 ))
     end
end


function BuyuSkillButton:setSkillClickCallHandler( _skillClickCallHandler  )
	self.skillClickCallHandler = _skillClickCallHandler
end

function BuyuSkillButton:setSkillCoolDownCallHandler( _skillCoolDownCallHandler )
	self.skillCoolDownCallHandler = _skillCoolDownCallHandler
end



function BuyuSkillButton:skillClickCallBack( sender, eventType )
    if eventType == ccui.TouchEventType.ended then
		 -- 冷却计时，即时状态技能按钮不可点击
		 --self.m_SkillButton:setTouchEnabled( false )
         if self.isProgress then
             if not BuyuSkillButton.isSkillCoolTipsVisble then
                 BuyuSkillButton.isSkillCoolTipsVisble = true
                 TOAST("技能冷却中!", function()
                     BuyuSkillButton.isSkillCoolTipsVisble = false
                 end)
             end
         else
            if self.skillClickCallHandler then
                self.skillClickCallHandler( self.skillType )
            end
         end
    end
end


function BuyuSkillButton:skillProgress(percentage, _cdTime )
     self.isProgress = true
     self.m_ProgressTimer:stopAllActions()
     self.cdTime = _cdTime
     self.timeLabelAtlas:setString( self.cdTime )
     self.timeLabelAtlas:setVisible( true )
     --self.m_SkillButton:setTouchEnabled( false )
     --  模版可见
     self.m_StencilSpr:setVisible( true)
     -- 设置精灵进度条为顺时针
     self.m_ProgressTimer:setVisible( true )
     self.m_ProgressTimer:setPercentage( percentage  )
     
     -- 逐渐覆盖半透模板形成冷却效果
     local actionProgressTo = cc.ProgressTo:create(self.cdTime, 100)
     local callFunc = cc.CallFunc:create(  handler( self, self.skillCoolDownCallBack))
     self.m_ProgressTimer:runAction( cc.Sequence:create( actionProgressTo, callFunc ))
     self:skillCoolDownTimeBegin()
end

function BuyuSkillButton:setPercentage( percent )
     self.m_ProgressTimer:setPercentage( percent )
end

-- 技能冷却完成回调
function BuyuSkillButton:skillCoolDownCallBack()
      self.isProgress = false
      self.timeLabelAtlas:setVisible(false)
      -- 按钮置为可用 
	  --self.m_SkillButton:setTouchEnabled( true )
	   -- 设置蒙板不可见 
	  self.m_StencilSpr:setVisible( false )
	   -- 进度条技能不可见
	  self.m_ProgressTimer:setVisible( false )
      self:skillCoolDownTimeEnd()
	  if self.skillCoolDownCallHandler then
         self.skillCoolDownCallHandler( self.skillType )
	  end
end


function BuyuSkillButton:skillCoolDownTimeBegin()
    if self.skillCoolDownTimer then
        scheduler.unscheduleGlobal( self.skillCoolDownTimer )
        self.skillCoolDownTimer = nil 
    end
    self.skillCoolDownTimer = scheduler.scheduleGlobal(handler(self,self.showskillCoolDownLabel),  1 )
end

function BuyuSkillButton:showskillCoolDownLabel()
     self.cdTime = self.cdTime - 1
     self.timeLabelAtlas:setString( self.cdTime )
end

function BuyuSkillButton:skillCoolDownTimeEnd()
    if not self.skillCoolDownTimer then
        return
    end
    scheduler.unscheduleGlobal( self.skillCoolDownTimer )
    self.skillCoolDownTimer = nil
end



return BuyuSkillButton