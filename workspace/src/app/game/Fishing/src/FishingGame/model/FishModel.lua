--
-- Author: Your Name
-- Date: 
--

--正方形碰撞边框
local boundbox = {
					["fish01"]={{80,24},{37},{40,37}},
                    ["fish02"]={{56,25},{34},{11,32}},
                    ["fish03"]={{84,50},{55},{36,62}},
                    ["fish04"]={{107,60},{65},{17,65}},
                    ["fish05"]={{109,45}, {66},{23,66}},
                    ["fish06"]={{108,74}, {85},{18,89}},
                    ["fish07"]={{117,49},{64},{20,70}},
                    ["fish08"]={{128,73},{107},{19,120}},
                    ["fish09"]={{201,95},{115},{69,125}},
                    ["fish10"]={{214,84},{115},{53,125}},
                    ["fish11"]={{135,107},{116},{12,134}},
                    ["fish12"]={{168,168},{146},{45,200}},
                    ["fish13"]={{225,95},{106},{22,116},{128,116}},
                    ["fish14"]={{287,91},{122},{40,126},{162,126}},
                    ["fish15"]={{210,222},{200},{20,237}},
                    ["fish16"]={{298,141},{160},{10,170},{170,170}},
                    ["fish17"]={{295,129},{143},{24,150},{167,150}},
                    ["fish18"]={{520,222},{267},{11,285},{278,285}},
                    ["fish19"]={{256,256},{255},{0,282}},
                    ["fish20"]={{483,141},{166},{23,190},{189,200},{ 355,200 }},
                    ["fish21"]={{164,164}, {164},{6,165}},
                    ["fish22"]={{193,193}, {193},{15,205}},
                    ["fish23"]={{483,141},{166},{23,190},{189,200},{ 355,200 }},
		        }
	        

local FishGlobal = require("src.app.game.Fishing.src.FishGlobal")


local FishModel = class("FishModel", function()
		return display.newSprite()
	end)

-- 鱼种
FishModel.FINGERLING = {
                          COMMON_FISH_TYPE     = 1 ,  -- 普通鱼
                          REWARD_FISH_TYPE     = 2 ,  -- 奖金鱼
                          BUFF_FISH_TYPE       = 3 ,  -- buff鱼
                          BOSS_FISH_TYPE       = 4 ,  -- Boss鱼
                          TEAM_THREE_FISH_TYPE = 5 ,  -- 三组队
                          TEAM_FOROUR_FISH_TYPE= 6,   -- 四组队
                          SAME_FISH_TYPE       = 7,   -- 同类鱼圈
                          SURPER_BOMB_TYPE     = 8,   -- 超级炸弹
                          PART_BOMB_YPE        = 9,   -- 局部炸弹
                       }


function FishModel:ctor()
	self.m_state        = FishGlobal.ObjectStateAlive  -- 当前状态(游走swim，挣扎struggle，逃跑escape)
	self.m_iValue       = 1000                       
	self.m_iID          = 0                            -- 鱼的id
	self.m_szName       = "fish"                       -- 鱼的名称
	self.m_iPathIndex   = 1                            -- 鱼的路径id
	self.m_iDistance    = 0                            -- 鱼的走的距离
	self.m_iType        = 1                            -- 鱼的种类
	self.m_bTroop       = false                        -- 鱼阵中的鱼
	self.m_fishScore    = 0                            -- 鱼的倍数 
	self.m_bDelayClear  = false              
	self.m_fSpeed       = 100.0                        -- 鱼的速度
	self.m_Picture      = ""
	self.m_isIn         = false             
	self.m_isOut        = false
	self.m_orientation  = 1                            -- 鱼的朝向, 1代表向左，-1代表向右
	self.m_delayGo      = false
	self.m_troopMoveToX = 0
	self.m_collisionBox = {}                           -- 鱼的碰撞框
	self.m_moveDir      = 1
	self.m_update_index = -1                           -- 刷新频度控制
	self.m_time         = os.time()                    
	self.m_originalPostion = cc.p(0,0)                 -- 鱼的出生位置
	self.m_isLocked     = false                        -- 鱼是否被锁定
	self.m_isFrozen     = false                        -- 鱼是否被冰冻
	self.m_frozenCd     = 0                            -- 鱼被冻结的时间
	self.m_beCatch      = false                        -- 鱼是否被抓
	self.m_deathSound   = ""                           -- 鱼被抓的音效
	self.m_isCoundboxDataInit  = false                 -- 碰撞盒数据是否初始化
	self.selfBoundboxData      = {}                    -- 自己碰撞盒数据
	self.m_fingerling   = 0                            -- 鱼种
	self.m_isPoisoning  = {}                           -- 鱼是否已经中毒
	self.m_isElectri    = {}                           -- 鱼是否被电击
	self.m_fishBone     = ""                           -- 鱼的骨头，被闪电击中的效果
	self.m_dialogShowTime = 0                          -- 宝箱显示时间
	self.m_dialoghideTime = 0                          -- 宝箱隐藏时间
	self.m_chestDialogVisible = false
	self.m_isCi    = {}                                -- 鱼是否刺中    
	self.m_scoreSound = ""                             -- 显示分数的声音      
             
end

function FishModel:start()
	
end

-- function FishModel:update(dt)
-- 	-- body
--       self.m_iDistance = self.m_iDistance + self.m_fSpeed*dt

--       local position_ = qka.QKAPathManager:getInstance():getPoint(self.m_iPathIndex,self.m_iDistance)
--       local angle_ = qka.QKAPathManager:getInstance():getAngle(self.m_iPathIndex,self.m_iDistance)

--       if position_.x == 0 and position_.y == 0 then--游出界面  
--         self:removeFromParent()
--       else
--         self:setPosition(cc.p(position_.x,position_.y))
--         --  fish:setRotation(180*angle_/math.pi)
--         self:setRotation(180*angle_/math.pi+180)
--       end
-- end

-- 初始化鱼的外形
function FishModel:initFishappearance( isFlippedX , isFlippedY )
	--print("self.m_iType=",self.m_iType)
	--print("isFlippedY=",isFlippedY)
	cc.SpriteFrameCache:getInstance():addSpriteFrames("buyu_p_plist/p_buyu_commom_new_1.plist","buyu_p_plist/p_buyu_commom_new_1.png")
	if self.m_fingerling == FishModel.FINGERLING.TEAM_THREE_FISH_TYPE then -- 三组队鱼
		--print("三组队鱼")
		self.chassisSpr ={}
		self.fishSpr = {}
		local rad = math.rad(60)   
        for i = 1 , 3 do
         	self.chassisSpr[i] = display.newSprite("#fish_team_back.png")
         	self.fishSpr[i] = display.newSprite()
         	local pos = cc.p( 0,0 )
         	local contentSize = self.chassisSpr[i]:getContentSize()
         	local x = contentSize.width/2 / math.tan( rad )
            local x1 = contentSize.width/2 /math.sin(rad)
         	if i == 1 then
	            pos = cc.p(-x1, 0  )
	        elseif i == 2 then
	            pos = cc.p( x,contentSize.width/2  )
	        elseif i == 3 then
	            pos = cc.p(x,-contentSize.width/2) 
	        end
	        self.chassisSpr[i]:setPosition( pos )
	        self:addChild( self.chassisSpr[i] )
            self.chassisSpr[i]:runAction( cc.RepeatForever:create(cc.RotateBy:create(3.5, 360)) )

            self.fishSpr[i]:setPosition( pos )
            self:addChild( self.fishSpr[i] )
            self.fishSpr[i]:setFlippedX( isFlippedX ) 
            self.fishSpr[i]:setFlippedY( isFlippedY )
        end  

        -- local aimSpr =   display.newSprite("#fish_aim.png")
        -- local aimSprC3b = cc.c3b(42, 211, 160)
        -- aimSpr:setColor( aimSprC3b )
        -- self:addChild( aimSpr )
	elseif self.m_fingerling ==  FishModel.FINGERLING.TEAM_FOROUR_FISH_TYPE  then -- 四组队鱼
		self.chassisSpr ={}
		self.fishSpr = {}
		for i=1,4 do
			self.chassisSpr[i] = display.newSprite("#fish_team_back.png")
         	self.fishSpr[i] = display.newSprite()
         	local pos = cc.p( 0,0 )
         	local contentSize = self.chassisSpr[i]:getContentSize()
         	if i == 1 then
               pos = cc.p(-contentSize.width/2+10 ,contentSize.height/2-10 )
         	elseif i == 2 then
         	   pos = cc.p(contentSize.width/2-10 ,contentSize.height/2-10 )
         	elseif i == 3 then
               pos = cc.p(-contentSize.width/2+10 ,-contentSize.height/2+10 )
         	elseif i == 4 then
         	   pos = cc.p( contentSize.width/2-10 ,-contentSize.height/2+10 )
         	end
         	self.chassisSpr[i]:setPosition( pos )
	        self:addChild( self.chassisSpr[i] )
            self.chassisSpr[i]:runAction( cc.RepeatForever:create(cc.RotateBy:create(3.5, 360)) )

            self.fishSpr[i]:setPosition( pos )
            self:addChild( self.fishSpr[i] )
            self.fishSpr[i]:setFlippedX( isFlippedX ) 
            self.fishSpr[i]:setFlippedY( isFlippedY )
		end
        
	elseif self.m_fingerling == FishModel.FINGERLING.SAME_FISH_TYPE   then  -- 同类鱼
		self.chassisSpr ={}
        self.chassisSpr[1] = display.newSprite("#fish_a_hundred shots.png")
        self:addChild( self.chassisSpr[1] )
        self.chassisSpr[1]:runAction( cc.RepeatForever:create(cc.RotateBy:create(3.5, 360)) )

        self.fishSpr = {}
        self.fishSpr[1] = display.newSprite()
        self:addChild( self.fishSpr[1] )
        self.fishSpr[1]:setFlippedX( isFlippedX ) 
        self.fishSpr[1]:setFlippedY( isFlippedY )
	else

	end
end

function FishModel:setSpriteFrameEx( spriteFrame )
	if self.m_fingerling == FishModel.FINGERLING.TEAM_THREE_FISH_TYPE then -- 三组队鱼
		for i=1, 3 do
		   self.fishSpr[i]:setSpriteFrame( spriteFrame  )
		end
	elseif self.m_fingerling ==  FishModel.FINGERLING.TEAM_FOROUR_FISH_TYPE  then -- 四组队鱼
        for i=1, 4 do
           self.fishSpr[i]:setSpriteFrame( spriteFrame  )
        end
	elseif self.m_fingerling == FishModel.FINGERLING.SAME_FISH_TYPE   then  -- 同类鱼
       self.fishSpr[1]:setSpriteFrame( spriteFrame  )
	else
       self:setSpriteFrame( spriteFrame  )
	end
end

function FishModel:runActionEx( animation )
	if self.m_fingerling == FishModel.FINGERLING.TEAM_THREE_FISH_TYPE then -- 三组队鱼
		for i=1, 3 do
		   self.fishSpr[i]:runAction( cc.RepeatForever:create(cc.Animate:create( animation)) )
		end
	elseif self.m_fingerling ==  FishModel.FINGERLING.TEAM_FOROUR_FISH_TYPE  then -- 四组队鱼
        for i=1, 4 do
           self.fishSpr[i]:runAction( cc.RepeatForever:create(cc.Animate:create( animation)) )
        end
	elseif self.m_fingerling == FishModel.FINGERLING.SAME_FISH_TYPE   then  -- 同类鱼
       self.fishSpr[1]:runAction( cc.RepeatForever:create(cc.Animate:create( animation)) )
	else 
   --     self:setZOrder(100)
       self:runAction( cc.RepeatForever:create(cc.Animate:create( animation)) )
	end
end


function FishModel:runStruggleAction( animation , _callFunc  )
    if self.m_fingerling == FishModel.FINGERLING.TEAM_THREE_FISH_TYPE then -- 三组队鱼
		for i=1, 3 do
		   self.fishSpr[i]:runAction( cc.Animate:create( animation ) )
		end
		self:runAction(cc.Sequence:create( cc.DelayTime:create( 1 ) , _callFunc , nil))
	elseif self.m_fingerling ==  FishModel.FINGERLING.TEAM_FOROUR_FISH_TYPE  then -- 四组队鱼
        for i=1, 4 do
           self.fishSpr[i]:runAction( cc.Animate:create( animation ) )
        end
        self:runAction(cc.Sequence:create( cc.DelayTime:create( 1 ) , _callFunc , nil))
	elseif self.m_fingerling == FishModel.FINGERLING.SAME_FISH_TYPE   then  -- 同类鱼
        self.fishSpr[1]:runAction( cc.Sequence:create( cc.Animate:create( animation ) ,nil))
        self:runAction(cc.Sequence:create( cc.DelayTime:create( 1.2 ) , _callFunc , nil))
	else
        self:runAction( cc.Sequence:create( cc.Animate:create( animation ), _callFunc , nil) )
	end
end


--计算碰撞边框
function FishModel:computeFishBoxOld(index)
	if(self.m_update_index == index)then return end
	self.m_update_index = index
	self.sideLength = 0
	for k,v in pairs(boundbox[self.m_szName]) do
		if k ~= 1 then
           if k == 2 then
			 self.sideLength = v[1]
			else
				local p = self:convertToWorldSpace(cc.p(v[1] + self.sideLength/2,v[2] - self.sideLength/2))
				local rect = cc.rect(p.x-self.sideLength/2 , p.y - self.sideLength/2 , self.sideLength , self.sideLength)
				self.m_collisionBox[k] = rect
			end
		end
		
	end
end


--显示鱼的阴影
function FishModel:showFishShadow()
    self.fishShadow = display.newSprite("buyu_p_texture/fish_img_show.png")
    self.fishShadow:setAnchorPoint( cc.p(0.5,0.5) )
    self.fishShadow:setPosition(cc.p(0,-20))
    self:addChild( self.fishShadow )
    if boundbox[self.m_szName] then
	    local fishSize = boundbox[self.m_szName][1]
	    local fishShadowSize = self.fishShadow:getContentSize()
	    local scaleX = fishSize[1]/fishShadowSize.width
	    local scaleY = fishSize[2]/fishShadowSize.height
	    self.fishShadow:setScaleX( scaleX  )
	    self.fishShadow:setScaleY( scaleY  )
	    self.fishShadow:setOpacity( 80 )
	end
end

-- 刷新阴影的位置
function FishModel:updateFishShadowPos( isVisible )
	local fishPos =  cc.p(self:getPosition())
	if  boundbox[self.m_szName] then
		local fishSize = boundbox[self.m_szName][1]
		local nativePos = self:convertToNodeSpace( cc.p(fishPos.x , fishPos.y - (fishSize[2]/2)- 50 ) )
		self.fishShadow:setPosition( nativePos )
		self.fishShadow:setVisible( isVisible )
    end
end

-- 获取的鱼的实际宽度和高度
function FishModel:geRealWithAndHeight()
	local realWithAndHeight = {50,50}
	if self.m_fingerling == FishModel.FINGERLING.TEAM_THREE_FISH_TYPE then -- 三组队鱼
		realWithAndHeight = { 255 , 284 }
	elseif self.m_fingerling ==  FishModel.FINGERLING.TEAM_FOROUR_FISH_TYPE  then -- 四组队鱼
        realWithAndHeight = { 284 ,284 }
	elseif self.m_fingerling == FishModel.FINGERLING.SAME_FISH_TYPE   then  -- 同类鱼
        realWithAndHeight = { 210 , 210 }
	else
        if boundbox[self.m_szName] then
	       realWithAndHeight = boundbox[self.m_szName][1]
	    else
           realWithAndHeight = {50,50}
	    end
	end
	--print("FishModel:geRealWithAndHeight")
	--dump( realWithAndHeight )
	return realWithAndHeight
end


-- 创建宝箱鱼的对话框
function FishModel:createChestDialog(   szDiaImage )
	 --print(" FishModel:createChestDialog(   szDiaImage )")
	if self.m_fingerling == FishModel.FINGERLING.CHESS_FISH_TYPE then
         --print("self.m_fingerling",self.m_fingerling)
		 self.chestDialog = display.newSprite( "#"..szDiaImage )
		 self.chestDialog:setAnchorPoint( cc.p(0.5,0) )
	     self.chestDialog:setPosition(cc.p(155,186))
	     self.chestDialog:setVisible(true)
	     self:addChild( self.chestDialog )
	end
end

function FishModel:showChestDialog( dt )
	if self.m_fingerling == FishModel.FINGERLING.CHESS_FISH_TYPE then
		if self.m_chestDialogVisible then
		    self.m_dialoghideTime = self.m_dialoghideTime + dt
	        if self.m_dialoghideTime > 2 then
	           self.m_dialoghideTime = 0
	           self.m_chestDialogVisible = false
	           self.chestDialog:setVisible(false)
	        end 
		else
			self.m_dialogShowTime = self.m_dialogShowTime  + dt
			if self.m_dialogShowTime >= 3 then
	           self.m_dialogShowTime = 0
	           self.m_chestDialogVisible = true
	           self.chestDialog:setVisible(true)
			end
		end
	end
end

function FishModel:hideChestDialog()
	if self.m_fingerling == FishModel.FINGERLING.CHESS_FISH_TYPE then
		 self.chestDialog:setVisible(false)
	end
end


function FishModel:computeFishBox( index )
	if(self.m_update_index == index)then return end
	self.m_update_index = index
	self.m_collisionBox = {}
	if self.m_fingerling == FishModel.FINGERLING.TEAM_THREE_FISH_TYPE then -- 三组队鱼
       self:computeFishBoxTeamThree()
	elseif self.m_fingerling ==  FishModel.FINGERLING.TEAM_FOROUR_FISH_TYPE  then -- 四组队鱼
       self:computeFishBoxTeamFour()
	elseif self.m_fingerling == FishModel.FINGERLING.SAME_FISH_TYPE   then  -- 同类鱼
       self:computeFishBoxSame()
	else
       self:computeFishBoxNormal()
	end

end

--计算碰撞边框
function FishModel:computeFishBoxNormal()
	if  not self.m_isCoundboxDataInit then
		self.m_isCoundboxDataInit = true
		self.sideLength = 0
		local contenSize = self:getContentSize()
		local isFlippedX = self:isFlippedX()
		local isFlippedY = self:isFlippedY()
		local scale = self:getScale()
		if boundbox[self.m_szName] then
			for k,v in pairs(boundbox[self.m_szName]) do
				if k ~= 1 then
		           if k == 2 then
					 self.sideLength = v[1] * scale
					else
						local posX = v[1] * scale + self.sideLength/2 
						if isFlippedX then
						   posX = contenSize.width * scale -  posX 
					    end 
					    local posY = v[2] * scale  - self.sideLength/2
					    if isFlippedY then
		                   posY = contenSize.height * scale - posY 
					    end
					    self.selfBoundboxData[k] = {posX, posY }
						local p = self:convertToWorldSpace(cc.p( posX , posY ))
						local rect = cc.rect(p.x-self.sideLength/2 , p.y - self.sideLength/2 , self.sideLength , self.sideLength)
						self.m_collisionBox[k] = rect
					end
				end
				
			end
		else
	        print("没有碰撞盒",self.m_szName)
		end
	else
       for k,v in pairs( self.selfBoundboxData ) do 
           local p = self:convertToWorldSpace(cc.p( v[1] , v[2] ))
		   local rect = cc.rect(p.x-self.sideLength/2 , p.y - self.sideLength/2 , self.sideLength , self.sideLength)
		   self.m_collisionBox[k] = rect
       end
	end
end

function FishModel:computeFishBoxSame()
	self.sideLength = 210
	local pos = cc.p(self:getPosition())
	local rect = cc.rect( pos.x -self.sideLength/2 , pos.y - self.sideLength/2 , self.sideLength , self.sideLength)
	self.m_collisionBox[1] = rect
end


-- 生成三组队鱼碰撞盒
function FishModel:computeFishBoxTeamThree()
	self.sideLength = 142
	for i=1,3 do
		  if not tolua.isnull( self.fishSpr[i] ) then
			  local pos = cc.p( self.fishSpr[i]:getPosition() )
			  local p = self:convertToWorldSpace( pos )
		      local rect = cc.rect( p.x -self.sideLength/2 , p.y - self.sideLength/2 , self.sideLength , self.sideLength)
		      self.m_collisionBox[i] = rect
		  end
	end
end

-- 生成三组队鱼碰撞盒
function FishModel:computeFishBoxTeamFour()
	self.sideLength = 142
	for i=1,4 do
		if not tolua.isnull( self.fishSpr[i] ) then
		    local pos = cc.p( self.fishSpr[i]:getPosition() )
		    local p = self:convertToWorldSpace( pos )
		    local rect = cc.rect( p.x -self.sideLength/2 , p.y - self.sideLength/2 , self.sideLength , self.sideLength)
		    self.m_collisionBox[i] = rect
		 end
	end
end

function FishModel:collision_RectWithCircle( rect,  p,  r)
     --获取矩形信息
     --左下角坐标：( lx , ly )
     --右上角坐标：( rx , ry )
        local lx = rect.x
        local ly = rect.y
        local rx = rect.x + rect.width
        local ry = rect.y + rect.height
 
     --计算圆心到四个顶点的距离

        local  d1 = cc.pGetDistance( p, cc.p(lx, ly))
        local  d2 = cc.pGetDistance( p, cc.p(lx, ry))
        local  d3 = cc.pGetDistance( p, cc.p(rx, ly))
        local  d4 = cc.pGetDistance( p, cc.p(rx, ry))
 
     --判断是否碰撞
     --判断距离是否小于半径
        if  d1 < r or d2 < r or d3 < r or d4 < r  then return true end
        --是否在圆角矩形的，横向矩形内
        if( p.x > (lx-r) and p.x < (rx+r) and p.y > ly and p.y < ry ) then return true end
        --是否在圆角矩形的，纵向矩形内
        if( p.x > lx and p.x < rx and p.y > (ly-r) and p.y < (ry+r) ) then return true end
 
    --不发生碰撞
        return false
 end


function FishModel:getFishBoxTable()
	return self.m_collisionBox
end

function FishModel:isCollision( rect )
	local isCollision = false
	for k,v in pairs(self.m_collisionBox) do
		if cc.rectIntersectsRect(v, rect) then
			isCollision = true
            break
		end
	end
	return isCollision
end

function FishModel:isCollisionPoint(point)
	local isCollision = false
	for k,v in pairs(self.m_collisionBox) do
		if cc.rectContainsPoint(v,point) then
			isCollision = true
            break
		end
	end
	return isCollision
end


function FishModel:isCollisionNew( point )
	for k,v in pairs( self.m_collisionBox ) do
		if self:collision_RectWithCircle( v, point, 15 ) then
			return true
		end
	end
	return false
end

function FishModel:getMoveDir()
	return self.m_moveDir
end

function FishModel:setMoveDir(dir)
	self.m_moveDir = dir
end

function FishModel:isIn()
	return self.m_isIn
end

function FishModel:getTroopMoveToX()
	return self.m_troopMoveToX
end

function FishModel:setTroopMoveToX(x)
	self.m_troopMoveToX = x
end

function FishModel:isGoing()
	return self.m_delayGo
end

function FishModel:setGoing()
	self:setVisible(true)
	self.m_delayGo = true
end

function FishModel:getOrientation()
	return self.m_orientation
end

function FishModel:setOrientation(orientation)
	self.m_orientation = orientation
end

function FishModel:getOriginalPostion()
	return self.m_originalPostion
end

function FishModel:getOriginalPostion( pos )
	self.m_originalPostion = cc.p( pos.x, pos.y )
end

function FishModel:isOut()
	return self.m_isOut
end

function FishModel:setIn(b)
	self.m_isIn = b
end

function FishModel:setOut(b)
	self.m_isOut = b
end

function FishModel:setDead()
	-- body
	--self:setPosition()
end

function FishModel:setPic(name)
	self.m_Picture = name
end

function FishModel:isPic(name)
	return self.m_Picture==name
end

function FishModel:setDelayClear(b)
	self.m_bDelayClear = b
end

function FishModel:getDelayClear()
	return self.m_bDelayClear
end

function FishModel:setSpeed(speed)
	self.m_fSpeed = speed
end

function FishModel:getSpeed()
	return self.m_fSpeed
end

function FishModel:setPathInfo(params)
	self.m_iPathIndex = params.index
	self.m_iDistance = params.distance
end

function FishModel:setPath(path)
	self.move_path = path
end

-- function FishModel:fishMove(dt)
-- 	-- body
-- 	local info={x=0,y=0,r=0}
-- 	self.m_iDistance=self.m_iDistance+1
-- 	if(self.m_iDistance<#self.move_path)then
-- 		info.x=self.move_path[self.m_iDistance].x
-- 		info.y=self.move_path[self.m_iDistance].y
-- 		info.r=self.move_path[self.m_iDistance].r
-- 	end
-- 	print("鱼的坐标")
-- 	-- dump(info)
-- 	self:setPositionX(info.x)
-- 	self:setPositionY(info.y)
-- 	self:setRotation(info.r)
-- end

function FishModel:getPoint()
	-- body
	local info={x=0,y=0,r=0}
	self.m_iDistance=self.m_iDistance+1
	if(self.m_iDistance<#self.move_path)then
		info.x=self.move_path[self.m_iDistance].x
		info.y=self.move_path[self.m_iDistance].y
		info.r=self.move_path[self.m_iDistance].r
	end
	return info
end

function FishModel:setPathDistance(distance)
	self.m_iDistance = distance
end

function FishModel:getPathDistance()
	return self.m_iDistance
end

function FishModel:getPathInfo()
	return self.m_iPathIndex,self.m_iDistance
end
function FishModel:TestFishName(name)
	return self.m_szName==name
end

function FishModel:setValue(value)
	self.m_iValue = value
end

function FishModel:getValue()
	return self.m_iValue
end

function FishModel:setFishType(kind)
	self.m_iType = kind
end

function FishModel:getFishType()
	return self.m_iType
end

function FishModel:getFishScore()
	return self.m_fishScore
end

function FishModel:setFishScore( fishScore )
	self.m_fishScore = fishScore*0.01
end

function FishModel:setFishName(name)
	self.m_szName = name
end

function FishModel:getFishName()
	return self.m_szName
end

function FishModel:setTroop(b)
	self.m_bTroop = b
end

function FishModel:getTroop()
	return self.m_bTroop
end

function FishModel:setFishID(id)
	self.m_iID = id
end

function FishModel:getFishID()
	return self.m_iID
end

function FishModel:setState(state)
	self.m_state = state
end

function FishModel:getState()
	
	return self.m_state
end

function FishModel:isState(state)
	
	return self.m_state==state
end

function FishModel:getIsFrozen()
	return self.m_isFrozen
end

function FishModel:setIsFrozen( isFrozen )
	self.m_isFrozen = isFrozen
end

function FishModel:getIsLocked()
	return self.m_isLocked
end

function FishModel:setIsLocked( isLocked )
	self.m_isLocked = isLocked
end

function FishModel:setFrozenCd( __frozenCd  ) 
	self.m_frozenCd = __frozenCd
end

function FishModel:setCatch( beCath   )
	self.m_beCatch = beCath
end

function FishModel:getCatch() 
    return self.m_beCatch
end

function FishModel:getFrozenCd()
    return self.m_frozenCd
end

function FishModel:setDeathSound( deathSound )
	self.m_deathSound = deathSound
end
function FishModel:getDeathSound()
	return self.m_deathSound
end

function FishModel:setFingerling( fingerling ) 
	self.m_fingerling = fingerling
end

function FishModel:getFingerling()
	return self.m_fingerling
end

function FishModel:isPoisoning( index )
    return self.m_isPoisoning[ index ]
end

function FishModel:setPoisoning(index , isPoisoning )
	self.m_isPoisoning[index] = isPoisoning
end

function FishModel:isElectri( index )
   return self.m_isElectri[ index ] 
end

function FishModel:setElectri(index, isElectri )
    self.m_isElectri[index] = isElectri
end

function FishModel:isCi( index )
   return self.m_isCi[ index ] 
end

function FishModel:setCi(index, isCi )
    self.m_isCi[index] = isCi
end


function FishModel:getFishBone()
	return self.m_fishBone
end

function FishModel:setFishBone( fishBone  )
	 self.m_fishBone = fishBone
end

function FishModel:getStayTime()
	return os.time()-self.m_time
end

function FishModel:setScoreSound( scoreSound  )
	self.m_scoreSound = scoreSound
end

function FishModel:getScoreSound()
	return self.m_scoreSound
end
return FishModel