
--
-- Author: Your Name
-- Date: 2017-03-13 15:23:55
--
local FishGlobal = {}

--层级

FishGlobal.UILevel      = 400
FishGlobal.NoticeLevel  = 300
FishGlobal.LogicLevel   = 200
FishGlobal.BGLevel      = 100

--对象状态
FishGlobal.ObjectStateAlive    = 1
FishGlobal.ObjectStateStruggle = 2
FishGlobal.ObjectStateDead     = 3
--游戏状态
FishGlobal.GameStateCtor  = 100
FishGlobal.GameStateInit  = 101
FishGlobal.GameStateWait  = 102
FishGlobal.GameStatePlay  = 103
FishGlobal.GameStatePause = 104
FishGlobal.GameStateStop  = 105
FishGlobal.GameStateOver  = 106

FishGlobal.isLoadResByLoadView = true --是否加载了资源
FishGlobal.PlayerCount = 4

FishGlobal.CurrentGameState = FishGlobal.GameStateInit

FishGlobal.NoNetTest = false --"单机测试"
--出鱼定时
FishGlobal.OutFishDelay = 2
--时间流逝
FishGlobal.CurFishDelay = 0

FishGlobal.TestCollisionBox = false   --"碰撞边框测试"
FishGlobal.IsOpenPlayMoveUpInfo = false
FishGlobal.autoFireCheckBoxIsVisble = false -- 渔友自动开炮按钮

FishGlobal.buyuErroCodeBegin = -10000
FishGlobal.minCannonFishType =  10        --最小可以瞄准的鱼

--一些按钮命令
FishGlobal.cmdCannonLevelUp   = "CannonLevelUp"   -- 炮台升级(影响命中)
FishGlobal.cmdCannonLevelDown = "CannonLevelDown" -- 炮台降级
FishGlobal.cmdCannonTips      = "CannonTips"      -- 炮台说明
FishGlobal.cmdShipLevelUp     = "ShipLevelUp"     -- 提高倍数(影响收益)
FishGlobal.cmdChangeShipSkin  = "ChangeShipSkin"  -- 切换船皮肤
FishGlobal.cmdChangeShipScore = "ChangeShipScore" -- 换船的倍数

-- 玩家操作按钮
FishGlobal.OPERATION_CANNON_ADD         = 10001 -- 切换高等级的炮
FishGlobal.OPERATION_CANNON_DEC         = 10002 -- 切换低等级的炮
FishGlobal.OPERATION_CANNON_UPFOLDING   = 10003 -- 展开升级面板
FishGlobal.OPERATION_CANNON_UPSHIPlEVEL = 10004 -- 升级主舰
FishGlobal.OPERATION_SET_MORE           = 10005 -- 更多
FishGlobal.OPERATION_EXIT_GAME          = 10006 -- 退出游戏
FishGlobal.OPERATION_LOCK_FISH          = 10007 -- 瞄准鱼
FishGlobal.OPERATION_MUST_SET           = 10008 -- 设置
FishGlobal.OPERATION_FISH_LIST          = 10009 -- 鱼种
FishGlobal.OPERATION_CHAT               = 10010 -- 聊天
FishGlobal.OPERATION_SHOP               = 10011 -- 商城
FishGlobal.OPERATION_DISSOLVEB          = 10012 -- 游戏没有开始前的解散
FishGlobal.OPERATION_VOICE              = 10013 -- 语音
FishGlobal.OPERATION_RULE               = 10014 -- 规则
FishGlobal.OPERATION_DISSOLVEA          = 10015 -- 游戏没有开始后解散
FishGlobal.OPERATION_INVITE             = 10016 -- 邀请好友按钮
FishGlobal.OPERATION_STARTGAME          = 10017 -- 开局
FishGlobal.OPERATION_ACTIVITY           = 10018 -- 活动按钮
FishGlobal.OPERATION_CLUB_INVITE        = 10019 -- 俱乐部邀请好友按钮
FishGlobal.OPERATION_COPY               = 10020 -- 复制房间号码
FishGlobal.OPERATION_LOCK_VIP_ROOM      = 10021 -- 锁定贵宾房

FishGlobal.RoomFriend_id  = 210000 --渔友房节点
FishGlobal.RoomClassic_id = 220000 --系统房节点
FishGlobal.RoomOldFish_id = 230000 --老捕鱼节点



FishGlobal.gameAtomTypeId4Function  = 106001                    -- 在未进场景服前，用于请求功能相关数据(例:排行榜)
FishGlobal.middleRoomId             = 106002                    -- 深海巨鲨(50-500倍炮入场)
FishGlobal.highRoomId               = 106003                    -- 龙宫宝藏(500倍及以上炮入场)
FishGlobal.goldIngotRoomId          = 106004                    -- 神龟海域(10倍及以上炮入场，专属BOSS:元宝龟)
FishGlobal.CityRoomId               = 106090                    -- 电玩城房间id

FishGlobal.gameAtomTypeIdFriendRoom = 106101                    -- 普通渔友房
FishGlobal.clubFriendRoomId         = 106102                    -- 俱乐部渔友房

 
FishGlobal.gameAtomTypeId4NewHand  =  135001                    -- 新手场（0金币入场，底分1）
FishGlobal.gameAtomTypeId4Mans     =  135002                    -- 达人场（1万金币入场，底分10）
FishGlobal.gameAtomTypeId4Expert   =  135003                    -- 专家场（10万金币入场，底分100）
FishGlobal.gameAtomTypeId4master   =  135004                    -- 大师场（100万金币入场，底分1000）

--服务器提供的椅子号从1开始
FishGlobal.InvalidChair = 65535
FishGlobal.myChair = 1
FishGlobal.myViewChair = 1

-- 鱼的游走方向
FishGlobal.fishOrientation = { left = -1 , right = 1 }


FishGlobal.GameMsg =    -- 捕鱼消息
{ 
   FISH_GOLD_RANK_DATAS_MSG  = "FISH_GOLD_RANK_DATA_MSG",    -- 金币排行榜
   FISH_GAIN_RANK_DATAS_MSG  = "FISH_GAIN_RANK_DATAS_MSG",   -- 盈亏排行榜
   FISH_BUYU_ITEM_ACK_MSG    = "FISH_BUYU_ITEM_ACK_MSG",     -- 购买物品返回消息  
   FISH_BUYU_EXIT_GAME_MSG   = "FISH_BUYU_EXIT_GAME_MSG",    -- 退出游戏场景
   FISH_GAMEDATA_NTY_MSG     = "FISH_GAMEDATA_NTY_MSG" ,     -- 捕鱼游戏数据刷新
   FISH_ROOMMANAGER_MSG		 = "FISH_ROOMMANAGER_MSG",	     -- 房间管理页面刷新
   FISH_JOINBTN_UPDATE_MSG	 = "FISH_JOINBTN_UPDATE_MSG",    -- 更新按钮状态
   FISH_ROOM_DISMISS_MSG	 = "FISH_ROOM_DISMISS_MSG",      -- 解散房间
   FISH_HIDE_JOINPANNEL_MSG  = "FISH_HIDE_JOINPANNEL_MSG",   -- 隐藏加入界面
   FISH_INVITEFIREND_MSG	 = "FISH_INVITEFIREND_MSG",	     -- 激活创建时邀请页面弹窗
   FISH_MANAGERKICK_MSG		 = "FISH_MANAGERKICK_MSG",	     -- 管理员踢人
   FISH_UPDATETABLEINFO_MSG	 = "FISH_UPDATETABLEINFO_MSG" ,  -- 更新桌子信息
   FISH_BYDR_ENTER_ROOM_MSG	 = "FISH_BYDR_ENTER_ROOM_MSG" ,  -- 进入房间(经典捕鱼)
   FISH_BYDR_UPDATE_TABLE_MSG= "FISH_BYDR_UPDATE_TABLE_MSG", -- 更新房间内桌子信息(经典捕鱼)
   FISH_UPDATE_GOLD_NUM_MSG  = "FISH_UPDATE_GOLD_NUM_MSG",   -- 刷新选桌界面金币
   FISH_EXIT_ROOM_HALL_MSG   = "FISH_EXIT_ROOM_HALL_MSG" ,   -- 退出选桌界面
   FISH_SHOW_PROMPET_MSG     = "FISH_SHOW_PROMPET_MSG",
   FISH_TOAD_ENTER_ROOM_MSG  = "FISH_TOAD_ENTER_ROOM_MSG",   -- 进入金蟾捕鱼选桌界面
   FISH_TOAD_UPDATE_ONLINEPERS_MSG = " FISH_TOAD_UPDATE_ONLINEPERS_MSG" -- 刷新在线玩家
}

FishGlobal.ClubMsg =    -- 俱乐部相关消息
{ 
   FISH_CLUB_ROOMLIST_MSG	= "FISH_CLUB_ROOMLIST_MSG",	   --房间列表消息
}

FishGlobal.BUYTYPE = { 
                        normal = 1 ,  -- 正常
                        expire  = 2, -- 过期
                      }


FishGlobal.RoomType = 
{
    Free       = 1,      -- 1.自由桌
    MatchFull  = 2,      -- 2.比赛房：满人开赛
    MatchTimer = 3,      -- 3.比赛房：定时开赛
    System     = 4,      -- 4.系统匹配房
    Friends    = 5,      -- 5.渔友房
    GoldIngot  = 6,      -- 6.元宝房
    ClubCity   = 7,      -- 7.电玩城
    ClubFriend = 8,      -- 8.牌友圈
}


FishGlobal.FriendRoomStage =   -- 渔友房游戏阶段
{
	unknowStage           = 0 , -- 未知的状态
	waitStage       	  = 1 , -- 等待阶段(准备)
	pickABankStage        = 2 , -- 选庄阶段
    gamePlayStage         = 3 , -- 游戏阶段( 正常出鱼 )
    gameStopStage   	  = 4 , -- 游戏暂停
    gameVoteStage         = 5 , -- 投票阶段
    gameClearStage        = 6 , -- 游戏清场
    gameVoteShowStage     = 7 , -- 投票结果观看阶段
    gameEndStage    	  = 8 , -- 游戏结束阶段
}

FishGlobal.degreeType =  --玩家身份
{
    roomHost = 1  , -- 庄家
    normal   = 2  , -- 普通玩家
    audience = 3  , -- 观众
}

FishGlobal.TaskAwardType =  -- 活动任务奖励的状态
{
    canGet = 0 , -- 可领取
    hasGet = 1 , -- 已领取
    noGet  = 2 , -- 未达成
}

FishGlobal.RichElementType = 
{
	TEXT   = 0, --文本
    IMAGE  = 1, --图片
    CUSTOM = 2, --自定义
}

FishGlobal.chatType = 
{
   text = 1 ,     --文字
   magicAni = 2,  -- 魔法表情

}

-- 桌子类型
FishGlobal.chairType = 
{
    EN_CHAIR_4 = 0,		--4人桌
	EN_CHAIR_6 = 1,		--6人桌
}

-- 游戏入口类型
FishGlobal.entranceType = 
{
	normal    = 1  , -- 普通模式
	clubHall  = 2  , -- 俱乐部模式
}

-- 物品类型
FishGlobal.itemType = 
{
	POER_TYPE       = 1,  -- 炮
	CURRENCY_TYPE   = 2,  -- 货币
	SKILL_ITEM_TYPE = 3,  -- 技能道具  
	HUIFEI_TYPE     = 4,  -- 话费卡
	IPHONE_TYPE     = 5,  -- iphone 碎片
	YUNBAO_TYPE     = 6,  -- 元宝类型
	REDPACKET_TYPE  = 7,  -- 红包类型
}

-- 是否掉元宝
FishGlobal.noDropAcer = 0  --不掉落元宝
FishGlobal.isDropAcer = 1  --掉落元宝

FishGlobal.chairTypeValue = FishGlobal.chairType.EN_CHAIR_4


-- 捕鱼功能渠道开关
FishGlobal.buyuTotalFunctionId      = 2001    -- 捕鱼功能总开关
FishGlobal.buyuFunctionListId       = 2002    -- 功能列表开关
FishGlobal.buyuStoreFunctionId      = 2004    -- 商城
FishGlobal.buyuBagFunctionId        = 2005    -- 背包
FishGlobal.buyuUserCenterFunctionId = 2012    -- 个人中心
FishGlobal.buyuBuyGoldFunctionId    = 2013    -- 购买金币
FishGlobal.buyuBuyFKFunctionId      = 2014    -- 购买房卡
FishGlobal.buyuBuyQuickFunctionId   = 2015    -- 快速开始
FishGlobal.buyuQuickEnterId         = 2015    -- 快速开始(目前用于更多游戏开关控制)
FishGlobal.buyuOpenBetaActivityId   = 2100    -- 公测活动
FishGlobal.buyuVoiceId              = 2101    -- 捕鱼语音开关
FishGlobal.buyuMoreGameId           = 2102    -- 更多游戏
FishGlobal.buyuFriendShareId        = 2103    -- 渔友房结算分享开关
FishGlobal.buyuHongBaoId            = 2106    -- 红包开关
FishGlobal.buyuRobHongBaoId         = 2107    -- 抢红包
FishGlobal.buyuNoYBANDHONGBAOId     = 2108    -- 无元宝和红包产出
FishGlobal.buyuRankId               = 2109    -- 排行榜

-- 游戏类型
FishGlobal.ThousandsGunsKindTypeId = 106       -- 千炮捕鱼
FishGlobal.ClassicKindTypeId       = 135       -- 经典捕鱼

FishGlobal.TableStatus = 
{
     normal = 0,       -- 0:普通 
     vipRoom =1,       -- 1:贵宾非续租 
     vipRoomRelet = 2, -- 2:贵宾续租
}


FishGlobal.RankType =
{
   Gold  =  1, -- 金币
   Huafei=  2, -- 话费
}

FishGlobal.DateType =
{
   day  =  1,  -- 日
   month=  2,  -- 月
}

--'0:锁定，1:取消锁定'
FishGlobal.lockType = 
{
   lock   = 0, --0:锁定
   unLock = 1, --1:取消锁定
}

--速度,0:慢炮，1:快炮
FishGlobal.SeepType = 
{
   slow = 0, -- 0:慢炮
   fast = 1, -- 1:快炮
}

-- 爆炸类型
FishGlobal.bombType = 
{
   

}

-- 玩家类型
FishGlobal.playerType = 
{
   normal = 0, -- 普通玩家
   robot  = 1, -- 机器人
}

FishGlobal.gameKindHead = 3        -- 用于排行榜
FishGlobal.gameMonthKingdHead = 11 -- 话费累计排行榜

FishGlobal.netAndAimColor = { 
                               [1] = cc.c3b(220, 226, 28) , [2] = cc.c3b(195,25,223) ,[3] = cc.c3b(49,52,182),
                               [4] = cc.c3b(60, 213 ,36) ,  [5] = cc.c3b(206,2,0) ,   [6] = cc.c3b(206,124,44) 
                            }

FishGlobal.goldBgPng      = { 
                               [1] = "fish_coin_bg_5.png" , [2] = "fish_coin_bg_4.png" ,[3]="fish_coin_bg_1.png" ,
                               [4] = "fish_coin_bg_2.png" , [5] = "fish_coin_bg_6.png" ,[6]="fish_coin_bg_3.png" 
                            }
FishGlobal.bulltPng       = {
                               [1] = "fish_bullt_01.png",  [2] = "fish_bullt_02.png" , [3] = "fish_bullt_01.png",
                               [4] = "fish_bullt_01.png",  [5] = "fish_bullt_02.png" , [6] = "fish_bullt_01.png",
                            }
                            

 
function FishGlobal.getViewChair( chair )
	if chair~=FishGlobal.InvalidChair then
		local view_chair = chair
		--如果我的椅子号大于3，则所有人的要调整
		if FishGlobal.myChair > 2 then
		   view_chair = (view_chair + 1)%FishGlobal.PlayerCount + 1
		end
		--print("chair=%d->getViewChair:",chair,view_chair)
		return view_chair
	end
	return FishGlobal.InvalidChair
end

-- 渔友房椅子视图转换
function FishGlobal.getFishFriendViewChair( chair )
     local viewChair = 1
     local viewChairTable = {}
     local viewChairFlipYTable = {}
     if FishGlobal.chairTypeValue == FishGlobal.chairType.EN_CHAIR_4 then
     	  viewChairTable = { [1] = 1, [2] = 6 , [3] = 2, [4] = 5 , [5] = 3 , [6] = 4  }
          viewChairFlipYTable = { [1] = 6, [2] = 1 , [3] = 5, [4] = 2 , [5] = 4 , [6] = 3  }
     elseif FishGlobal.chairTypeValue == FishGlobal.chairType.EN_CHAIR_6 then
          viewChairTable = { [1] = 2, [2] = 5 , [3] = 1, [4] = 6 , [5] = 3 , [6] = 4  }
          viewChairFlipYTable = { [1] = 5, [2] = 2 , [3] = 6, [4] = 1 , [5] = 4 , [6] = 3  }
     end
	 if chair >= 1  and chair <= 6 then
        if FishGlobal.myChair == 1 or FishGlobal.myChair == 3 or FishGlobal.myChair == 5 then
            viewChair = viewChairTable[ chair ]
        else
            viewChair = viewChairFlipYTable[ chair ]
        end
	 end 
	 return viewChair
end


-- 用于鼠标点击位置的转换
function FishGlobal.convertToMatchViewChair(params)
	local myChair=FishGlobal.myChair
    local point=cc.p(params.x,params.y)
	if (myChair~=FishGlobal.INVALID_CHAIR)then
	      -- 如果自己椅子号是一、二 三 ，则椅子号是四、 五 、六的点击坐标的位置要进行转换
		if (myChair==1 or myChair==2 or myChair==3)then
			if (params.wChairID==4 or params.wChairID==5 or params.wChairID==6)then
			 	point.x = display.right - point.x
				point.y = display.top - point.y
			end
		else
		    --如果自己椅子号是三、四、五，则椅子号是一、二、三的点击坐标位置要进行转换
			if (params.wChairID==1 or params.wChairID==2 or params.wChairID==3)then
			 	point.x = display.right - point.x
				point.y = display.top - point.y
			end
		end
	end
	return point
end

-- 用于鼠标点击位置的转换
function FishGlobal.convertToMatchFriendViewChair(params)
	local myChair=FishGlobal.myChair
    local point=cc.p(params.x,params.y)
	if (myChair~=FishGlobal.INVALID_CHAIR)then
	      -- 如果自己椅子号是1、3 、5 ，则椅子号是4、 2 、6 的点击坐标的位置要进行转换
		if (myChair==1 or myChair==3 or myChair==5)then
			if (params.wChairID==4 or params.wChairID== 2 or params.wChairID==6)then
			 	--point.x = display.right - point.x
				point.y = display.top - point.y
			end
		else
		    --如果自己椅子号是4、2、6，则椅子号是1、3、5的点击坐标位置要进行转换
			if (params.wChairID==1 or params.wChairID== 3 or params.wChairID== 5 )then
			 	--point.x = display.right - point.x
				point.y = display.top - point.y
			end
		end
	end
	return point
end



-- 当自己椅子号是四、五、六的时候,自己视图发生转换,鱼的视图位置也要进行转换
function FishGlobal.convertFishPostion( params )
	local myChair=FishGlobal.myChair
	local point = cc.p(params.x, params.y)
	if myChair ~= FishGlobal.INVALID_CHAIR then
       if myChair == 4 or myChair == 5 or myChair == 6 then
		 	point.x = display.right - point.x
			point.y = display.top - point.y
       end
	end
	return point
end

-- 当自己椅子号是4、2、6的时候,自己视图发生转换,鱼的视图位置也要进行转换
function FishGlobal.convertFishFriendPostion( params )
	local myChair=FishGlobal.myChair
	local point = cc.p(params.x, params.y)
	if myChair ~= FishGlobal.INVALID_CHAIR then
       if myChair == 4 or myChair == 2 or myChair == 6 then
		 	--point.x = display.right - point.x
			point.y = display.top - point.y
       end
	end
	return point
end

-- 针对终端玩家游戏场景是否发生翻转
function FishGlobal.isConvertSenceFlipped()
    local myChair = FishGlobal.myChair
	local  isFlipped = false
	if myChair ~= FishGlobal.INVALID_CHAIR then
       if myChair == 4 or myChair == 5 or myChair == 6 then
          isFlipped = true
       end
	end
	return isFlipped
end

-- 针对终端玩家游戏场景是否发生翻转
function FishGlobal.isConvertSenceFriendFlipped()
    local myChair = FishGlobal.myChair
	local  isFlipped = false
	if myChair ~= FishGlobal.INVALID_CHAIR then
       if myChair == 4 or myChair == 2 or myChair == 6 then
          isFlipped = true
       end
	end
	return isFlipped
end


function FishGlobal.getPlayerCount()
	return FishGlobal.PlayerCount
end

function FishGlobal.isGameState(__state)
	return FishGlobal.CurrentGameState == __state
end

function FishGlobal.setGameState(__state)
	FishGlobal.CurrentGameState = __state
end

function FishGlobal.getNetAndAimColorByChair( chair )
	return FishGlobal.netAndAimColor[chair]
end

function FishGlobal.getGoldBgPngByChair( chair )
	return FishGlobal.goldBgPng[chair]
end

function FishGlobal.getBulltPngByChair( chair  )
    return string.format( "buyu_p_texture/gui-zidan/zidan%02d.png", chair)
	--return FishGlobal.bulltPng[chair]
end

FishGlobal.isShowDesk = false   -- 是否显示桌子 

return FishGlobal