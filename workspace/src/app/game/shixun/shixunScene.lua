--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
local Scheduler           = require("framework.scheduler") 
local CCGameSceneBase     = require("src.app.game.common.main.CCGameSceneBase") 
local shixunGameLayer     = require("app.game.shixun.shixunGameLayer")
local DlgAlert = require("app.hall.base.ui.MessageBox")

local shixunGameScene= class("shixunGameScene", function()
        return CCGameSceneBase.new()
end)

function shixunGameScene:ctor()  
    self.m_shixunMainLayer         = nil 
    self.m_shixunExitGameLayer     = nil
    self.m_shixunRuleLayer         = nil
    self.m_shixunMusicSetLayer     = nil 
    self:myInit()
end

-- 游戏场景初始化
function shixunGameScene:myInit() 
    -- 主ui 
	self:initshixunGameMainLayer()

	self:registBackClickHandler(handler(self, self.onBackButtonClicked)) -- Android & Windows注册返回按钮
	addMsgCallBack(self, MSG_ENTER_FOREGROUND, handler(self, self.onEnterForeground)) -- 转前台
	addMsgCallBack(self, MSG_ENTER_BACKGROUND, handler(self, self.onEnterBackground)) -- 转后台 
end

---- 进入场景
function shixunGameScene:onEnter()
   print("-----------shixunGameScene:onEnter()-----------------")
   ToolKit:setGameFPS(1/60.0) 
end

-- 初始化主ui
function shixunGameScene:initshixunGameMainLayer()
    self.m_shixunMainLayer = shixunGameLayer.new()
    self:addChild(self.m_shixunMainLayer)
end

function shixunGameScene:getMainLayer()
    return self.m_shixunMainLayer 
end

function shixunGameScene:onEnter()
	print("------shixunGameScene:onEnter begin--------")
	print("------shixunGameScene:onEnter end--------")
end
   
-- 退出场景
function shixunGameScene:onExit()
    print("------shixunGameScene:onExit begin--------") 
	
	if self.m_shixunMainLayer._Scheduler1 then
        Scheduler.unscheduleGlobal(self.m_shixunMainLayer._Scheduler1)	
        self.m_shixunMainLayer._Scheduler1 = nil
    end 
     self.m_shixunMainLayer:onCleanup()
    removeMsgCallBack(self, MSG_ENTER_FOREGROUND)
    removeMsgCallBack(self, MSG_ENTER_BACKGROUND)
    --removeMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT)
    --removeMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT)
--    shixunGlobal.m_isNeedReconectGameServer = false
--    shixunRoomController:getInstance():setInGame( false )
--    shixunGameController:getInstance():onDestory()
 --   self:RemoveResources()
	print("------shixunGameScene:onExit end--------")
end

-- 响应返回按钮事件
function shixunGameScene:onBackButtonClicked() 
--     UIAdapter:popScene()   
end

-- 从后台切换到前台
function shixunGameScene:onEnterForeground()
	print("从后台切换到前台")
end

-- 从前台切换到后台
function shixunGameScene:onEnterBackground()
   print("从前台切换到后台,游戏线程挂起!")
end

function shixunGameScene:clearView()
   print("shixunGameScene:clearView()")
end

-- 清理数据
function shixunGameScene:clearData()
   
end 

return shixunGameScene

