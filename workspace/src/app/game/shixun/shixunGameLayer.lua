local HNLayer = require("src.app.newHall.HNLayer")
local GameRecordLayer = require("src.app.newHall.childLayer.GameRecordLayer")
local GameSetLayer = require("src.app.newHall.childLayer.SetLayer")
local shixunGameLayer = class("shixunGameLayer", function()
    return HNLayer.new();
end)
local scheduler = require("framework.scheduler")
local DlgAlert = require("app.hall.base.ui.MessageBox")
local platformUtils = require("app.hall.base.util.platform")
local ChatLayer = import("app.newHall.chat.ChatLayer")

local SearchPath = "app/game/shixun"

function shixunGameLayer:onCleanup()
    platform.closeGameWebView()
    -- MY_CONFIG_SCREEN_WIDTH = 1334
    -- MY_CONFIG_SCREEN_HEIGHT = 750
    -- display.resetDisplay(true)
end

function shixunGameLayer:ctor()
    self:init()
end

function shixunGameLayer:init()
    -- 背景
    --    local layer = ccui.Layout:create()    
    --    layer:setContentSize(display.width, display.height)     
    --    layer:setBackGroundColorType(ccui.LayoutBackGroundColorType.solid) --设置颜色    
    --    layer:setBackGroundColor(cc.c3b(151, 151, 151))        
    --    layer:setBackGroundColorOpacity(180)    --设置透明
    --    layer:addTo(self)
    -- local img_bg = ccui.ImageView:create("app/game/shixun/res/img_bg.jpg")
    -- img_bg:addTo(self)
    -- img_bg:setPosition(display.width / 2, display.height / 2)
    -- img_bg:ignoreContentAdaptWithSize(false)
    -- img_bg:setContentSize(cc.size(display.width, display.height))
    -- 网页
    --     if device.platform == "platform" then
    --         print("shixunGameLayer:init ...............")
    --         self.m_pWebView = ccexp.WebView:create()
    --         self.m_pWebView:setPosition(cc.p(0,0))
    --         self.m_pWebView:setAnchorPoint(cc.p(0,0))
    --         self.m_pWebView:setContentSize(cc.size(display.width, display.height))
    --         self.m_pWebView:setScalesPageToFit(true)
    -- --        self.m_pWebView:setJavascriptInterfaceScheme("lua")
    -- --        self.m_pWebView:setOnJSCallback(function(_, url)
    -- --            print('lua js callback:', url)
    -- --            self:onGameExit(url)
    -- --        end)
    --         self.m_pWebView:setOnShouldStartLoading(function(sender, url)        
    --             print("setOnShouldStartLoading, url is ", url)
    --             -- return self:onWebViewShouldStartLoading(sender, url)
    --         end)
    --         self.m_pWebView:setOnDidFinishLoading(function(sender, url)
    --             print("setOnDidFinishLoading, url is ", url)
    --             -- self:onWebViewDidFinishLoading(sender, url)
    --         end)
    --         self.m_pWebView:setOnDidFailLoading(function(sender, url)
    --             print("setOnDidFailLoading, url is ", url)
    --             -- self:onWebViewDidFailLoading(sender, url)
    --         end)
    --         self.m_pWebView:addTo(self)
    --         self.m_pWebView:setVisible(false)
    --         -- 
    -- --        local strUrl = ""
    -- --        self.m_pWebView:loadURL(strUrl)  
    -- --        self.m_pWebView:reload()
    --     end
    local img_bg = ccui.ImageView:create("app/game/shixun/res/img_bg.jpg")
    img_bg:addTo(self)
    img_bg:setPosition(display.width / 2, display.height / 2)
    img_bg:ignoreContentAdaptWithSize(false)
    img_bg:setContentSize(cc.size(display.width, display.height))
    self.image_bg = img_bg
    self:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function()
        -- MY_CONFIG_SCREEN_WIDTH = 1334
        -- MY_CONFIG_SCREEN_HEIGHT = 750
        -- display.resetDisplay(true)
        -- 关闭按钮
        if device.platform == "windows" then
            

            self.m_pBtnClose = ccui.Button:create(SearchPath .. "/res/btn_return.png")
            self.m_pBtnClose:addTouchEventListener(handler(self, self.onReturnClicked))
            self.m_pBtnClose:setPosition(50, display.height - 50)
            self.m_pBtnClose:addTo(self)
            self.m_pBtnClose:setLocalZOrder(9)

            self.m_pBtnChat = ccui.Button:create(SearchPath .. "/res/btn_return.png")
            self.m_pBtnChat:addTouchEventListener(function()
                self:onNativeCall(-101)
            end)
            self.m_pBtnChat:setPosition(200, display.height - 50)
            self.m_pBtnChat:addTo(self)
            self.m_pBtnChat:setLocalZOrder(9)
        end
    end), nil));
end

function shixunGameLayer:loadUrl(gameid, controller)
    --A:
    --百家樂:A01-03、05
    --极速百家樂:A06-07
    --龍虎:A08
    --輪盤:A09
    --骰宝:A10
    --B:
    --共咪百家樂:B01-03
    --多彩百家樂:B05
    --百家樂:B06
    --牛牛:B07
    --龍虎:B08
    --炸金花:B10
    if (gameid == nil) then
        gameid = 210
    end
    -- 210-215: 百家乐 扎金花 龙虎斗 牛牛 古堡 轮盘；
    -- 百家乐：0x17f, 383; 0x1f, 0x2f, 0x4f, 0x10f,
    -- 扎金花：0x200f, 8207;
    -- 龙虎斗：0x20f, 527;
    -- 牛牛：0x100f, 4111;
    -- 古堡: 0x40f, 1039;
    -- 轮盘：0x80f, 2063;
    local tableNo = {
        -- [210] = 16,
        -- [211] = 8192,
        -- [212] = 512,
        -- [213] = 4096,
        -- [214] = 1024,
        -- [215] = 2048,
        [210] = 383,
        [211] = 8207,
        [212] = 527,
        [213] = 4111,
        [214] = 1039,
        [215] = 2063,
    }

    -- 判断登录后的本地存储地址获取
    local sGameid = "gameid" .. gameid
    local cacheUrl = g_BGController:getShixunCacheUrl();
    if cacheUrl then
        local gType = tableNo[gameid]
        local cType = string.format("&cui=576&type=%d", gType)
        local tempUrl = cacheUrl .. cType
        print("shixunGameLayer:loadUrl url(from cache): ", tempUrl)
        if (tempUrl and tempUrl ~= "") then
            --            TOAST("资源加载中")
            if (self.m_pWebView) then
                self.m_pWebView:setVisible(true)
                self.m_pWebView:loadURL(tempUrl)
            end
            if device.platform == "android" or device.platform == "ios" then
                -- platformUtils.showBGUrl(tempUrl, "", handler(self, self.onGameExit))
                self.image_bg:setVisible(false)
                platform.openWebView(tempUrl, "", 0, handler(self, self.onNativeCall), 0)
            end
            return;
        end
    end

    local table = {
        [1] = "A01", [210] = "A06", [212] = "A08", [215] = "A09", [214] = "A10",
        [6] = "B01", [7] = "B05", [8] = "B06", [213] = "B07", [10] = "B08",
        [211] = "B10",
    }
    self.tableID = table[gameid]
    self.controller = controller
    if (self.tableID == nil) then
        self:onGameExit()
        return
    end
    local ditchNumber = GlobalConf.CHANNEL_ID
    local isMobileUrl = 1
    if (device.platform == "android" or device.platform == "ios") then
        isMobileUrl = 1
    end

    -- local url = string.format("http://%s/api/itf/bg/openGame", "172.16.11.116:9999")
    local url = string.format("%sapi/itf/bg/openGame", GlobalConf.DOMAIN)
    local _pass = g_LoginController.password or ""
    local _isWx = 0
    if g_LoginController._currentLogonType ~= "qka" then
        _isWx = 1
    end
    local t_data = {
        channelId = ditchNumber,
        isMobileUrl = isMobileUrl,
        isHttpsUrl = 1,
        userId = "" .. Player:getAccountID(),
        --table=self.tableID,
        password = _pass,
        isWx = _isWx
    }
    --    t_data = {
    --        channelId="1003",  
    --        isMobileUrl=1,
    --        isHttpsUrl=1,
    --        userId="1000052",
    --        table="B07",
    --    }
    local s_data = json.encode(t_data)
    local xhr = cc.XMLHttpRequest:new()
    xhr:setRequestHeader("Content-Type", "application/json")
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON
    xhr:open("POST", url)
    print(url, s_data)
    local function onReadyStateChange()
        print("Http Status Code:" .. xhr.status .. xhr.status .. xhr.statusText)
        print("xhr.response", xhr.response)
        if xhr.readyState == 4 and (xhr.status >= 200 and xhr.status < 207) then
            local response = xhr.response
            local output = json.decode(response)
            if (output.code == 0 and output.url) then
                --[[                local i,j = string.find(output.url, "&table=")
                if i then
                    if i>0 then
                        output.url = string.sub(output.url,0,i-1)
                        print(output.url)
                    end
                end
				
                local nKindid = g_BGController:getCurrentGameId()
				print(nKindid)
                local tableNo =  {
                    [210] = 16,
                    [211] = 8192,
                    [212] = 512,
                    [213] = 4096,
                    [214] = 1024,
                    [215] = 2048,
                }
				--]]
                -- 判断登录后的本地存储地址
                --cc.UserDefault:getInstance():setStringForKey( "shixunUrl", output.url);
                g_BGController:setShixunCacheUrl(output.url)

                local gType = tableNo[gameid]
                local cType = string.format("&cui=576&type=%d", gType)
                output.url = output.url .. cType
                print(output.url)

                TOAST("资源加载中")
                if (self.m_pWebView) then
                    self.m_pWebView:setVisible(true)
                    self.m_pWebView:loadURL(output.url)
                end
                if device.platform == "android" or device.platform == "ios" then
                    -- platformUtils.showBGUrl(output.url, "", handler(self, self.onGameExit))
                    self.image_bg:setVisible(false)
                    platform.openWebView(output.url, "", 0, handler(self, self.onNativeCall), 0)
                end

                -- 判断登录后的本地存储地址\
                --[[                local shixunUrl = cc.UserDefault:getInstance():getStringForKey( "shixunUrl", "");
                local t_json =  {}
                if (shixunUrl ~= "") then
                    t_json = json.decode(shixunUrl)
                end
                t_json[sGameid] = output.url
                local s_json = json.encode(t_json)
                cc.UserDefault:getInstance():setStringForKey( "shixunUrl", s_json);
				--]]
            else
                TOAST(output.msg)
                --                local dlg = DlgAlert.showTipsAlert({title = "", tip = output.msg, tip_size = 34})
                --				dlg:setSingleBtn(STR(37, 5), function ()
                scheduler.performWithDelayGlobal(function()
                    self:onGameExit()
                end, 1.5)
                --			    end)
            end
        end
    end
    xhr:registerScriptHandler(onReadyStateChange)
    xhr:send(s_data)
end

-- function shixunGameLayer:onWebViewShouldStartLoading(sender, url)
--     if device.platform == "android" then
--         if string.find(url, ".apk") then --跳转
--             LuaUpdateNativeBridge.getInstance():openURL(url)
--             return false
--         end
--     end
--     return true
-- end
-- function shixunGameLayer:onWebViewDidFinishLoading(sender, url)
--     local undefindUrl = "/undefined"
--     local startPos, endPos = string.find(url, undefindUrl)
--     if (startPos and startPos > 0) then
-- --        scheduler.performWithDelayGlobal(function()
--             self:onGameExit(sender)
-- --        end, 1.5)
--     end
-- end
-- function shixunGameLayer:onWebViewDidFailLoading(sender, url)
-- end
function shixunGameLayer:onReturnClicked(pSender, enventType)
    if (enventType ~= 2) then
        return
    end
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")

    if self.m_isBackTime == nil then
        self.m_isBackTime = true
        self:runAction(cc.Sequence:create(cc.DelayTime:create(0.05), cc.CallFunc:create(function()
            self.m_isBackTime = nil
            if self.ChatLayer and self.ChatLayer:isVisible() then
                self.ChatLayer:setVisible(false)
                platform.hideGameChatView()
            else
                self:onNativeCall(-100)
            end
        end)))
    end

    -- self:onGameExit()
    --    self:setVisible(false)
    --    LuaNativeBridge.getInstance():setScreenOrientation(0)
    --    local function funcFi()
    --        self:removeFromParent()
    --    end
    --    self:runAction(cc.Sequence:create(cc.DelayTime:create(0.05),cc.CallFunc:create(funcFi)))
end

function shixunGameLayer:onNativeCall(_result)
    self:runAction(cc.Sequence:create(cc.DelayTime:create(0.05), cc.CallFunc:create(function()
        self:onGameExit(_result)
    end)))
end

function shixunGameLayer:onGameExit(sender)

    if sender and sender == -101 then
        --聊天界面
        if (self.ChatLayer) then
            self.ChatLayer:setVisible(true)
        else
            -- 聊天界面
            self.ChatLayer = ChatLayer.new()
            self:addChild(self.ChatLayer, 999)
            self.ChatLayer:setExitEvent(function(instance)
                -- self.ChatLayer:setVisible(false)
                -- self.ChatLayer:removeFromParent()
                self.ChatLayer = nil
                platform.hideGameChatView()
            end)
            self.ChatLayer:setVisible(true)
        end
    else
        if (self.limitTime) then
            return
        end
        local function showReady(args)
            self.limitTime = nil
            self._Scheduler1 = nil
        end
        self.limitTime = true
        self._Scheduler1 = scheduler.performWithDelayGlobal(showReady, 5)

        local tempSender = sender
        if (tempSender == nil) then
            tempSender = self.m_pWebView;
        end
        if (tempSender and type(tempSender) ~= "number") then
            --        self:close()
            tempSender:setVisible(false)
            tempSender:setOnShouldStartLoading(function(sender, url) end)
            tempSender:setOnDidFinishLoading(function(sender, url) end)
            tempSender:setOnDidFailLoading(function(sender, url) end)
            --        tempSender:removeFromParent()
            --        tempSender = nil
        end
        --    print("正在退出")
        --    TOAST("正在退出")
        local scene = display.getRunningScene()
        local tDlgAlert = scene:getChildByName("DlgAlert")
        if (tDlgAlert) then
            tDlgAlert:closeDialog()
        end

        --    self.controller:gameExitBgReq()
        UIAdapter:popScene()

        if g_isNetworkInFail then
            if (g_BGController) then
                g_BGController:releaseInstance()
            end
            ToolKit:returnToLoginScene()
        end
        --
        --    -- 判断连接
        --    if (g_BGController) then
        --        g_BGController:onShixunConnect()
        --    end
    end
end

return shixunGameLayer

