local Scheduler = require("framework.scheduler")
local SearchPath = "app/game/caipiao"
local ThirdPartyGameContntroller = class("thirdPartyGameContntroller")

function ThirdPartyGameContntroller:getInstance()
    if nil == g_ThirdPartyContntroller then
        g_ThirdPartyContntroller = ThirdPartyGameContntroller.new()
    end
    return g_ThirdPartyContntroller
end

function ThirdPartyGameContntroller:releaseInstance()
    if g_ThirdPartyContntroller then
        g_ThirdPartyContntroller:onDestory()
    end
    g_ThirdPartyContntroller = nil
end

function ThirdPartyGameContntroller:ctor()
    print("ThirdPartyGameContntroller:ctor()")
    self.mDOMAIN = "http://xbqp.xb336.com/"

    self.THIRD_TYPE = {
        CAI_PIAO = 1,
        ZHEN_REN = 2,
        DIAN_ZI = 3,
        TI_YU = 4,
        BU_YU = 5
    }

    self.m_NetMsgHandlerSwitch = {}
    self.m_NetMsgHandlerSwitch["CS_H2C_EnterCP_Ack"] = handler(self, self.on_EnterCP_Ack)
    self.m_NetMsgHandlerSwitch["CS_H2C_ExitCP_Ack"] = handler(self, self.on_ExitCP_Ack)
    self.m_NetMsgHandlerSwitch["CS_H2C_EnterThirdPart_Ack"] = handler(self, self.on_EnterThirdPart_Ack)
    self.m_NetMsgHandlerSwitch["CS_H2C_ExitThirdPart_Ack"] = handler(self, self.on_ExitThirdPart_Ack)
end

function ThirdPartyGameContntroller:onDestory()
    print("----------ThirdPartyGameContntroller:onDestory begin--------------")
--    TotalController:removeNetMsgCallback(self, Protocol.SceneServer, "CS_H2C_HandleMsg_Ack")
    -- removeMsgCallBack(self, MSG_RECONNECT_LOBBY_SERVER)
    self.m_NetMsgHandlerSwitch = {}
    self.mEnterCPInfo = nil
    self.mEnterThirdPartInfo = nil

	if self.mGameScene then
		UIAdapter:popScene()
		self.mGameScene = nil
	end
--	self:onBaseDestory()
	print("----------ThirdPartyGameContntroller:onDestory end--------------")
end

function ThirdPartyGameContntroller:setThirdType(thirdType)
    self.mThirdType = thirdType
end

function ThirdPartyGameContntroller:setEventHandler(_handler)
    self.mMessageHandler = _handler
end

function ThirdPartyGameContntroller:getEnterCPInfo()
    return self.mEnterCPInfo
end

function ThirdPartyGameContntroller:getEnterThirdPartInfo()
    return self.mEnterThirdPartInfo
end


function ThirdPartyGameContntroller:sendEnterReq()
    -- { 1		, 1		, 'm_nThirdType' , 'INT'	, 1		, '第三方类型 彩票：1，真人:2，电子：3， 体育:4， 捕鱼：5'},
    print("ThirdPartyGameContntroller:sendEnterReq [ThirdType: "..self.mThirdType.."]")
    ConnectManager:send2Server(Protocol.LobbyServer,"CS_C2H_EnterCP_Req", {self.mThirdType})
end

function ThirdPartyGameContntroller:sendExitReq()
    ConnectManager:send2Server(Protocol.LobbyServer,"CS_C2H_ExitCP_Req", {})
end

function ThirdPartyGameContntroller:sendEnterThirdPartReq(sPlayCode, nPlatType, nGameID)
    -- { 1		, 1		, 'm_nThirdType'    , 'INT'		, 1		, '第三方类型 彩票：1，真人:2，电子：3， 体育:4， 捕鱼：5'},
    -- { 2		, 1		, 'm_nPlayCode'	    , 'STRING'	, 1		, '真人code'},
    -- { 3		, 1		, 'm_nPlatType'	    , 'INT'		, 1		, '真人平台type'},
    -- { 4		, 1		, 'm_gameId'	    , 'STRING'	, 1		, '游戏ID'},
    print("ThirdPartyGameContntroller:sendEnterThirdPartReq [ThirdType: "..self.mThirdType.."]")
    self.sPlayCode = sPlayCode
    self.nPlatType = nPlatType
    self.nGameID = nGameID
    ConnectManager:send2Server(Protocol.LobbyServer,"CS_C2H_EnterThirdPart_Req", {self.mThirdType, sPlayCode, nPlatType, nGameID})
end

function ThirdPartyGameContntroller:sendExitThirdPartReq(sPlayCode, nPlatType)
	-- { 1		, 1		, 'm_nThirdType'	, 'INT'		, 1		, '第三方类型 彩票：1，真人:2，电子：3， 体育:4， 捕鱼：5'},
	-- { 2		, 1		, 'm_nPlayCode'		, 'STRING'	, 1		, '真人code'},
    -- { 3		, 1		, 'm_nPlatType'		, 'INT'		, 1		, '真人平台type'},
    print("ThirdPartyGameContntroller:sendExitThirdPartReq [ThirdType: "..self.mThirdType.."]")
    self.sPlayCode = nil
    self.nPlatType = nil
    self.nGameID = nil
    ConnectManager:send2Server(Protocol.LobbyServer,"CS_C2H_ExitThirdPart_Req", {self.mThirdType, sPlayCode, nPlatType})
end

function ThirdPartyGameContntroller:callNetMsgHandler(_id, _cmd)
    local _handler = self.m_NetMsgHandlerSwitch[_id]
    if _handler then
        _handler(_cmd)
    else

    end
end

function ThirdPartyGameContntroller:on_EnterCP_Ack(_cmd)
    -- { 1		, 1		, 'm_nThirdType'	, 'INT'		, 1		, '第三方类型 彩票：1，真人:2，电子：3， 体育:4， 捕鱼：5'},
	-- { 2		, 1		, 'm_nRet'			, 'INT'		, 1		, '错误码'},	
	-- { 3		, 1		, 'm_webErrMsg'		, 'STRING'	, 1		, '后台错误信息(m_result !=0  && m_webErrCode != 0时有效)'},
	-- { 4		, 1		, 'm_accessToken'	, 'STRING'	, 1		, 'accessToken'},
    -- { 5		, 1		, 'm_domain'		, 'STRING'	, 1		, '跳转链接'},
    print("ThirdPartyGameContntroller:on_EnterCP_Ack")
    dump(_cmd)
    if _cmd.m_nRet ~= 0 then
        self:errorMessage("EnterCP_Ack", _cmd.m_nRet, _cmd.m_webErrMsg)
    else
        self.mEnterCPInfo = _cmd
    end
    -- if _cmd.m_nThirdType == self.THIRD_TYPE.CAI_PIAO then
    --     if _cmd.m_nRet == 0 then
            
    --         if self.mGameScene == nil then
    --             self.mGameScene = UIAdapter:pushScene("src.app.game.caipiao.caipiaoScene")
    --             return
    --         end
    --     end
    -- end

    if self.mMessageHandler then
        self.mMessageHandler("EnterCP_Ack", _cmd)
    end
end

function ThirdPartyGameContntroller:on_ExitCP_Ack(_cmd)
    -- { 1		, 1		, 'm_nRet'		, 'INT'		, 1		, '错误码'},	
    -- { 2		, 1		, 'm_webErrMsg'	, 'STRING'	, 1		, '后台错误信息(m_result !=0  && m_webErrCode != 0时有效)'},
    print("ThirdPartyGameContntroller:on_ExitCP_Ack")
    dump(_cmd)
    if _cmd.m_nRet ~= 0 then
        self:errorMessage("ExitCP_Ack", _cmd.m_nRet, _cmd.m_webErrMsg)
    else
       
    end
    g_ThirdPartyContntroller:releaseInstance()
    if self.mMessageHandler then
        self.mMessageHandler("ExitCP_Ack", _cmd)
    end
end

function ThirdPartyGameContntroller:on_EnterThirdPart_Ack(_cmd)
    -- { 1		, 1		, 'm_nThirdType'	, 'INT'		, 1		, '第三方类型 彩票：1，真人:2，电子：3， 体育:4， 捕鱼：5'},
	-- { 2		, 1		, 'm_nPlayCode'		, 'STRING'	, 1		, '真人code'},
	-- { 3		, 1		, 'm_nPlatType'		, 'INT'		, 1		, '真人平台type'},
	-- { 4		, 1		, 'm_gameId'		, 'STRING'	, 1		, '游戏ID'},
	-- { 5		, 1		, 'm_nRet'			, 'INT'		, 1		, '错误码'},
	-- { 6		, 1		, 'm_webErrMsg'		, 'STRING'	, 1		, '后台错误信息(m_result !=0  && m_webErrCode != 0时有效)'},
	-- { 7		, 1		, 'm_accessToken'	, 'STRING'	, 1		, 'accessToken'},
    -- { 8		, 1		, 'm_domain'		, 'STRING'	, 1		, '跳转链接'},
    print("ThirdPartyGameContntroller:on_EnterThirdPart_Ack")
    dump(_cmd)
    if _cmd.m_nRet ~= 0 then
        self:errorMessage("EnterThirdPart_Ack", _cmd.m_nRet, _cmd.m_webErrMsg)
    else
        self.mEnterThirdPartInfo = _cmd
        if self.mGameScene == nil then
            self.mGameScene = UIAdapter:pushScene("src.app.game.caipiao.src.thirdScene")
            return
        end
    end

    if self.mMessageHandler then
        self.mMessageHandler("EnterThirdPart_Ack", _cmd)
    end
end

function ThirdPartyGameContntroller:on_ExitThirdPart_Ack(_cmd)
    -- { 1		, 1		, 'm_nRet'		, 'INT'		, 1		, '错误码'},
	-- { 2		, 1		, 'm_webErrMsg'	, 'STRING'	, 1		, '后台错误信息(m_result !=0  && m_webErrCode != 0时有效)'},
    -- { 3		, 1		, 'm_domain'	, 'STRING'	, 1		, '跳转链接'},
    print("ThirdPartyGameContntroller:on_ExitThirdPart_Ack")
    dump(_cmd)
    self.mEnterThirdPartInfo = nil
    if _cmd.m_nRet ~= 0 then
        self:errorMessage("ExitThirdPart_Ack", _cmd.m_nRet, _cmd.m_webErrMsg)
    else
        if self.mGameScene then
            UIAdapter:popScene()
            self.mGameScene = nil
        end
        if self.mEnterCPInfo == nil then
            self:sendExitReq()
        end
    end

    if self.mMessageHandler then
        self.mMessageHandler("ExitThirdPart_Ack", _cmd)
    end
end

function ThirdPartyGameContntroller:errorMessage(_id, _result, _webErrMsg)
    if _webErrMsg and _webErrMsg ~= "" then
        TOAST(_webErrMsg)
    else
        if "EnterCP_Ack" == _id then
        elseif "ExitCP_Ack" == _id then
        elseif "EnterThirdPart_Ack" == _id then
        elseif "ExitThirdPart_Ack" == _id then
        end
        TOAST(_id.." ERROR: code ".._result)
    end
end


return ThirdPartyGameContntroller