local scheduler = require("framework.scheduler")
local ChatLayer = import("app.newHall.chat.ChatLayer")
local ChatLayerEx = import("app.newHall.chat.newChat.ChatLayerEx")
local SearchPath = "app/game/caipiao"
local ThirdGameLayer = class("ThirdGameLayer", function()
    return display.newLayer()
end)

function ThirdGameLayer:onCleanup()
    platform.closeGameWebView()
end

function ThirdGameLayer:ctor()
    self:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function()
        -- MY_CONFIG_SCREEN_WIDTH = 750
        -- MY_CONFIG_SCREEN_HEIGHT = 1334
        -- display.resetDisplay(false)
        -- 关闭按钮
        if device.platform == "windows" then
            self.m_pBtnClose = ccui.Button:create(SearchPath .. "/res/btn_return.png")
            self.m_pBtnClose:addTouchEventListener(handler(self, self.onReturnClicked))
            self.m_pBtnClose:setPosition(50, display.height - 50)
            self.m_pBtnClose:addTo(self)
            self.m_pBtnClose:setLocalZOrder(9)

            self.m_pBtnChat = ccui.Button:create(SearchPath .. "/res/btn_return.png")
            self.m_pBtnChat:addTouchEventListener(function()
                self:onNativeCall(-101)
            end)
            self.m_pBtnChat:setPosition(200, display.height - 50)
            self.m_pBtnChat:addTo(self)
            self.m_pBtnChat:setLocalZOrder(9)

        end
    end), nil));

    local info = g_ThirdPartyContntroller:getEnterThirdPartInfo()
    if info == nil then
        info = g_ThirdPartyContntroller:getEnterCPInfo()
    end
    if info.m_accessToken ~= "" then
        local s_data = "SESSION=" .. info.m_accessToken
        platform.openWebView(info.m_domain, s_data, 1, handler(self, self._onNativeCall), 0)
    else
        platform.openWebView(info.m_domain, "", 0, handler(self, self._onNativeCall), 0)
    end
end

function ThirdGameLayer:DaleyTime(dealy, callfunc)
    self:runAction(cc.Sequence:create(cc.DelayTime:create(dealy), cc.CallFunc:create(function()
        callfunc()
    end), nil))
end

function ThirdGameLayer:onReturnClick(pSender, enventType)
    if (enventType ~= 2) then
        return
    end
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    self:onNativeCall(-100)
end

function ThirdGameLayer:_onNativeCall(_result)
    self:runAction(cc.Sequence:create(cc.DelayTime:create(0.05), cc.CallFunc:create(function()
        self:onNativeCall(_result)
    end)))
end

function ThirdGameLayer:onNativeCall(_result)
    if _result and _result == -101 then
        --聊天界面
        if (self.ChatLayer) then
            self.ChatLayer:setVisible(true)
        else
            -- 聊天界面
            self.ChatLayer = ChatLayer.new()
            self:addChild(self.ChatLayer, 999)
            self.ChatLayer:setExitEvent(function(instance)
                self.ChatLayer:setVisible(false)
                self:removeChild(self.ChatLayer)
                self.ChatLayer = nil
                platform.hideGameChatView()
            end)
            self.ChatLayer:setVisible(true)
        end
    else
        print("caipiaoGameController:onNativeCall:" .. (_result or "nil"))
        if (self.limitTime) then
            return
        end

        self.limitTime = true
        self:DelayTime(5, function()
            self.limitTime = nil
        end)

        local sPlayCode = g_ThirdPartyContntroller.sPlayCode
        local nPlatType = g_ThirdPartyContntroller.nPlatType
        g_ThirdPartyContntroller:sendExitThirdPartReq(sPlayCode, nPlatType)

        if g_isNetworkInFail then
            if (g_ThirdPartyContntroller) then
                g_ThirdPartyContntroller:releaseInstance()
            end
            -- self:closeGameWebView()
            ToolKit:returnToLoginScene()
        end
    end
end


return ThirdGameLayer