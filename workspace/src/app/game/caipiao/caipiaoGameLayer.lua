local HNLayer = require("src.app.newHall.HNLayer")
local GameRecordLayer = require("src.app.newHall.childLayer.GameRecordLayer")
local GameSetLayer = require("src.app.newHall.childLayer.SetLayer")
local caipiaoGameController = class("caipiaoGameController", function()
    return HNLayer.new();
end)
local scheduler = require("framework.scheduler")
local DlgAlert = require("app.hall.base.ui.MessageBox")
local platformUtils = require("app.hall.base.util.platform")
-- local ChatLayer = import("app.newHall.chat.ChatLayer")
local ChatLayer = import("app.newHall.chat.newChat.ChatLayerEx")
local SearchPath = "app/game/caipiao"

function caipiaoGameController:ctor()
    self:init()
end

function caipiaoGameController:onCleanup()
    platform.closeGameWebView()
    MY_CONFIG_SCREEN_WIDTH = 1334
    MY_CONFIG_SCREEN_HEIGHT = 750
    display.resetDisplay(true)
end

function caipiaoGameController:onBackButtonClicked()
    if self.m_isBackTime == nil then
        self.m_isBackTime = true
        self:runAction(cc.Sequence:create(cc.DelayTime:create(0.05), cc.CallFunc:create(function()
            self.m_isBackTime = nil
            if self.ChatLayer:isVisible() then
                self.ChatLayer:setVisible(false)
                platform.hideGameChatView()
            else
                self:onNativeCall(-100)
            end
        end)))
    end
end

function caipiaoGameController:init()
    -- 背景
    -- local layer = ccui.Layout:create()    z
    -- layer:setContentSize(display.width, display.height)     
    -- layer:setBackGroundColorType(ccui.LayoutBackGroundColorType.solid) --设置颜色    
    -- layer:setBackGroundColor(cc.c3b(151, 151, 151))        
    -- layer:setBackGroundColorOpacity(180)    --设置透明
    -- layer:addTo(self)
    -- 背景
    -- local img_bg = ccui.ImageView:create(SearchPath .. "/res/img_bg.jpg")
    -- img_bg:addTo(self)
    -- img_bg:setPosition(display.width/2, display.height/2)
    -- img_bg:ignoreContentAdaptWithSize(false)
    -- img_bg:setContentSize(cc.size(display.width, display.height))
    g_CPController:initNetMsgHandlerSwitchData()
    g_CPController:setGameEnterAck(function(instance, info)
        if (not tolua.isnull(self)) then
            self:loadUrl(info, self)
        end
    end)
    local info = g_CPController:getGameUrlInfo()
    self:loadUrl(info, self)

    self:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function()
        MY_CONFIG_SCREEN_WIDTH = 750
        MY_CONFIG_SCREEN_HEIGHT = 1334
        display.resetDisplay(false)
        -- 关闭按钮
        if device.platform == "windows" then
            self.m_pBtnClose = ccui.Button:create(SearchPath .. "/res/btn_return.png")
            self.m_pBtnClose:addTouchEventListener(handler(self, self.onReturnClicked))
            self.m_pBtnClose:setPosition(50, display.height - 50)
            self.m_pBtnClose:addTo(self)
            self.m_pBtnClose:setLocalZOrder(9)

            self.m_pBtnChat = ccui.Button:create(SearchPath .. "/res/btn_return.png")
            self.m_pBtnChat:addTouchEventListener(function()
                self:onNativeCall(-101)
            end)
            self.m_pBtnChat:setPosition(200, display.height - 50)
            self.m_pBtnChat:addTo(self)
            self.m_pBtnChat:setLocalZOrder(9)

        end
    end), nil));
end

function caipiaoGameController:onReturnClicked(pSender, enventType)
    if (enventType ~= 2) then
        return
    end
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    self:onNativeCall()
end

function caipiaoGameController:loadUrl(info, controller)
    --
    dump(info, "loadUrlloadUrl")
    local url = info.m_domain
    if url == nil then
        url = "https://b00201.appxb.me"
    end
    self.controller = controller
    --
    -- local t_data = {
    --     cookies = {{
    --         -- "id": "" .. Player:getAccountID(),
    --         httpOnly = false,
    --         maxAge = -1,
    --         name = Player:getNickName(),
    --         secure = false,
    --         value = info.m_accessToken,
    --         version = 0
    --     }}
    -- }
    -- local s_data = json.encode(t_data)
    local s_data = "SESSION=" .. info.m_accessToken
    --
    dump(info, "caipiaoGameController_loadUrl")
    platform.openWebView(url, s_data, 1, handler(self, self._onNativeCall), 1)
end

function caipiaoGameController:_onNativeCall(_result)
    self:runAction(cc.Sequence:create(cc.DelayTime:create(0.05), cc.CallFunc:create(function()
        self:onNativeCall(_result)
    end)))
end

function caipiaoGameController:onNativeCall(_result)
    if _result and _result == -101 then
        --聊天界面
        if (self.ChatLayer) then
            self.ChatLayer:setVisible(true)
        else
            -- 聊天界面
            self.ChatLayer = ChatLayer.new()
            self:addChild(self.ChatLayer, 999)
            self.ChatLayer:setExitEvent(function(instance)
                self.ChatLayer:setVisible(false)
                self.ChatLayer:removeFromParent()
                self.ChatLayer = nil
                platform.hideGameChatView()
            end)
            self.ChatLayer:setVisible(true)
        end
    else
        print("caipiaoGameController:onNativeCall:" .. (_result or "nil"))
        if (self.limitTime) then
            return
        end
        local function showReady(args)
            if (not tolua.isnull(self)) then
                self.limitTime = nil
                self._Scheduler1 = nil
            end
        end
        self.limitTime = true
        self._Scheduler1 = scheduler.performWithDelayGlobal(showReady, 5)
        --
        local scene = display.getRunningScene()
        local tDlgAlert = scene:getChildByName("DlgAlert")
        if (tDlgAlert) then
            tDlgAlert:closeDialog()
        end

        g_CPController:gameExitReq()
        g_CPController:setGameExitAck(function(...)
            -- if (tolua.isnull(self)) then
            --     self:closeGameWebView()
            -- end
            UIAdapter:popScene()
        end)

        if g_isNetworkInFail then
            if (g_CPController) then
                g_CPController:releaseInstance()
            end
            -- self:closeGameWebView()
            ToolKit:returnToLoginScene()
        end
    end
end

return caipiaoGameController
