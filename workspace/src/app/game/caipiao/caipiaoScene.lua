--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
--endregion
local Scheduler        = require("framework.scheduler")
local CCGameSceneBase    = require("src.app.game.common.main.CCGameSceneBase")
local caipiaoGameLayer    = require("app.game.caipiao.caipiaoGameLayer")
local DlgAlert = require("app.hall.base.ui.MessageBox")
local LoginController = require("app.newHall.message.LoginController")
local caipiaoGameScene = class("caipiaoGameScene", function()
    return CCGameSceneBase.new()
end)

function caipiaoGameScene:ctor()
    self.m_gameMainLayer        = nil
    self:myInit()
end

-- 游戏场景初始化
function caipiaoGameScene:myInit()
    
    self:registBackClickHandler(handler(self, self.onBackButtonClicked)) -- Android & Windows注册返回按钮
    addMsgCallBack(self, MSG_ENTER_FOREGROUND, handler(self, self.onEnterForeground)) -- 转前台
    addMsgCallBack(self, MSG_ENTER_BACKGROUND, handler(self, self.onEnterBackground)) -- 转后台 

    -- 主ui
    self:initshixunGameMainLayer()
    AudioManager:getInstance():playMusic("app/game/caipiao/res/sound/caipiao_bg.mp3", true)

    ConnectionUtil:setCallback(function (network_state)
        if ConnectionUtil.NETWORK_DISCONNECTED_EVENT == network_state then
            self:runAction(cc.Sequence:create(cc.DelayTime:create(0.05), cc.CallFunc:create(function()
                LoginController:getInstance():onNetworkFailure()
            end)))
            
        end
    end)
end

---- 进入场景
function caipiaoGameScene:onEnter()
    print("-----------caipiaoGameScene:onEnter()-----------------")
    ToolKit:setGameFPS(1 / 60)
end

-- 初始化主ui
function caipiaoGameScene:initshixunGameMainLayer()
    self.m_gameMainLayer = caipiaoGameLayer.new()
    self:addChild(self.m_gameMainLayer)
end

function caipiaoGameScene:getMainLayer()
    return self.m_gameMainLayer
end

function caipiaoGameScene:onEnter()
    print("------caipiaoGameScene:onEnter begin--------")
    print("------caipiaoGameScene:onEnter end--------")
end

-- 退出场景
function caipiaoGameScene:onExit()
    print("------caipiaoGameScene:onExit begin--------")

    if self.m_gameMainLayer._Scheduler1 then
        Scheduler.unscheduleGlobal(self.m_gameMainLayer._Scheduler1)
        self.m_gameMainLayer._Scheduler1 = nil
    end
    self.m_gameMainLayer:onCleanup()
    removeMsgCallBack(self, MSG_ENTER_FOREGROUND)
    removeMsgCallBack(self, MSG_ENTER_BACKGROUND)
    --removeMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT)
    --removeMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT)
    --    shixunGlobal.m_isNeedReconectGameServer = false
    --    shixunRoomController:getInstance():setInGame( false )
    --    shixunGameController:getInstance():onDestory()
    --   self:RemoveResources()

    AudioManager:getInstance():stopAllSounds()
    AudioManager:getInstance():stopMusic()
    
    print("------caipiaoGameScene:onExit end--------")
end

-- 响应返回按钮事件
function caipiaoGameScene:onBackButtonClicked()
    --     UIAdapter:popScene()   
    -- if self.m_gameMainLayer then
    --     self.m_gameMainLayer:onBackButtonClicked()
    -- end
end

-- 从后台切换到前台
function caipiaoGameScene:onEnterForeground()
    print("从后台切换到前台")
end

-- 从前台切换到后台
function caipiaoGameScene:onEnterBackground()
    print("从前台切换到后台,游戏线程挂起!")
end

function caipiaoGameScene:clearView()
    print("caipiaoGameScene:clearView()")
end

-- 清理数据
function caipiaoGameScene:clearData()

end

return caipiaoGameScene