--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion

----------------------------------------------------------------------------------------------------------
-- 项目：
-- 时间: 2018-01-11
----------------------------------------------------------------------------------------------------------
 
local Scheduler = require("framework.scheduler") 
local DlgAlert = require("app.hall.base.ui.MessageBox")
local SearchPath = "app/game/caipiao"

--local BaseGameController = import("app.game.common.BaseGameController")
--local caipiaoGameController =  class("caipiaoGameController",function()
--    return BaseGameController.new()
--end) 

local caipiaoGameController =  class("caipiaoController") 

caipiaoGameController.instance = nil

-- 获取房间控制器实例
function caipiaoGameController:getInstance()
    if caipiaoGameController.instance == nil then
        caipiaoGameController.instance = caipiaoGameController.new()
    end
    return caipiaoGameController.instance
end

function caipiaoGameController:releaseInstance()
    if caipiaoGameController.instance then
		caipiaoGameController.instance:onDestory()
        caipiaoGameController.instance = nil
        g_CPController = nil
    end
end

function caipiaoGameController:ctor()
    print("caipiaoGameController:ctor()")
    self:myInit()
end

-- 初始化
function caipiaoGameController:myInit()
    print("caipiaoGameController:myInit()") 
    -- 添加搜索路径
    ToolKit:addSearchPath(SearchPath.."/res") 
    -- 加载场景协议以及游戏相关协议
     
--    Protocol.loadProtocolTemp("app.game.shixun.protoReg")
     
--    self:initNetMsgHandlerSwitchData() 
--    self:setGamePingTime( 5, 0x7FFFFFFF )--心跳包
end

function caipiaoGameController:setCurrentGameId(gameid)
    self.currentGameid = gameid
end

function caipiaoGameController:getCurrentGameId()
    return self.currentGameid
end

function caipiaoGameController:setGameCacheUrl(url)
    self.gameUrl = url
end

function caipiaoGameController:getGameCacheUrl()
    return self.gameUrl
end

function caipiaoGameController:setGameUrlInfo(info)
    self.gameInfo = info
end

function caipiaoGameController:getGameUrlInfo()
    return self.gameInfo
end

function caipiaoGameController:initNetMsgHandlerSwitchData()
    removeMsgCallBack(self, MSG_RECONNECT_LOBBY_SERVER)
    addMsgCallBack(self, MSG_RECONNECT_LOBBY_SERVER, handler(self, self.onGameReconnect))
--    self.m_netMsgHandlerSwitch = {}  
--    self.m_protocolList = {}
--    for k,v in pairs(self.m_netMsgHandlerSwitch) do
--        self.m_protocolList[#self.m_protocolList+1] = k
--    end
--    self:setNetMsgCallbackByProtocolList(self.m_protocolList, handler(self, self.netMsgHandler)) 
    
--    self.m_callBackFuncList = {}
--    TotalController:registerNetMsgCallback(self, Protocol.SceneServer, "CS_H2C_HandleMsg_Ack", handler(self, self.sceneNetMsgHandler))

--    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_EnterBg_Ack", handler(self, self.ackEnterScene))
--    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_ExitBg_Ack", handler(self, self.ackExitScene))
end 
 
function caipiaoGameController:gameEnterReq()
    ConnectManager:send2Server(Protocol.LobbyServer,"CS_C2H_EnterCP_Req", {1})
--self:ackEnterScene({m_result = 0})
end

function caipiaoGameController:gameExitReq()
    ConnectManager:send2Server(Protocol.LobbyServer,"CS_C2H_ExitCP_Req", {})
--self:ackExitScene({m_result = 0})
end

function caipiaoGameController:ackEnterScene(info)
    dump(info, "caipiaoGameController__info")
    if info.m_nRet == 0 then
        -- local scene = UIAdapter:pushScene("src.app.game.caipiao.caipiaoScene", DIRECTION.HORIZONTAL)  
        -- scene:getMainLayer():loadUrl(info, self)
        if (self.gameEnterAckFunc) then
            print("self===gameEnterAckFunc")
            self:gameEnterAckFunc(info)
        end
    else
        self:HandleError(info)
--        self:game.EnterAckFunc()
    end
end

function caipiaoGameController:ackExitScene(info)
    if info.m_nRet == 0 then
        -- UIAdapter:popScene()
        if (self.gameExitAckFunc) then
            self:gameExitAckFunc()
        end
        -- self:releaseInstance()
    else
        self:HandleError(info)
    end
end

function caipiaoGameController:setGameEnterAck(gameEnterAckFunc)
    self.gameEnterAckFunc = gameEnterAckFunc
end

function caipiaoGameController:setGameExitAck(gameExitAckFunc)
    self.gameExitAckFunc = gameExitAckFunc
end

-- 销毁龙虎斗游戏管理器
function caipiaoGameController:onDestory()
	print("----------caipiaoGameController:onDestory begin--------------")
--    TotalController:removeNetMsgCallback(self, Protocol.SceneServer, "CS_H2C_HandleMsg_Ack")
    removeMsgCallBack(self, MSG_RECONNECT_LOBBY_SERVER)
	self.m_netMsgHandlerSwitch = {}
	self.m_callBackFuncList = {} 
	if self.gameScene then
		UIAdapter:popScene()
		self.gameScene = nil
	end
--	self:onBaseDestory()
	print("----------caipiaoGameController:onDestory end--------------")
end

function caipiaoGameController:HandleError(ret) 
    -- local errorCode = -1
    -- local errorMsg = "操作失败"
    -- if (type(ret) == "table" and ret.m_result ~= 0 and ret.m_webErrCode) then
    --     -- 棋牌错误码
    --     if (ret.m_webErrCode == 0) then
    --         local errorInfo = {
    --             [-1245] = "进入视讯失败",
    --             [-1246] = "退出视讯失败",
    --             [-1247] = "金币不足进入BG最低限制",
    --             [-1248] = "您操作太频繁，请稍后再试",
    --             [-1249] = "视讯未开放",
    --         }
    --         errorCode = ret.m_result
    --         if (errorInfo[errorCode]) then
    --             errorMsg = errorInfo[errorCode]
    --         end
    --         TOAST(string.format("%s", errorMsg))
    --         return 
    --     -- 后台错误码
    --     else
    --         errorCode = ret.m_webErrCode
    --         if (type(ret.m_webErrMsg) == "string") then
    --            errorMsg = ret.m_webErrMsg
    --         end
    --         errorMsg = "游戏加载错误，请绑定手机号或联系在线客服"
    --         TOAST(string.format("[错误码：%d] %s", errorCode, errorMsg))
    --         return 
    --     end
    -- end
    --
    local errorMsg = ret.m_webErrMsg
    local errorInfo = {
        [-1253] = "进入彩票失败",
        [-1254] = "退出彩票失败",
        [-1255] = "您操作太频繁，请稍后再试",
        [-1256] = "彩票未开放",
    }
    local errorTemp = errorInfo[ret.m_nRet]
    if errorTemp then
        errorMsg = errorTemp
    end
    TOAST(string.format("%s", errorMsg))
end

function caipiaoGameController:netMsgHandler( __idStr,info )
    print("__idStr = ",__idStr) 
    if self.m_netMsgHandlerSwitch[__idStr] then
        (self.m_netMsgHandlerSwitch[__idStr])( info )
    else
        print("未找到游戏消息" .. (__idStr or ""))
    end
end

function caipiaoGameController:onGameReconnect( msgName, msgObj )
    print("caipiaoGameController:onGameReconnect: ", msgObj, g_isAllowReconnect)
    if msgObj == "start" then -- 开始重连

    elseif msgObj == "success" then --重连成功 
        self:setGameExitAck()
        self:onGameConnectSuccess()
    elseif msgObj == "fail" then -- 重连失败
        self:setGameExitAck()
        self:onGameConnectFail()
    end
end

function caipiaoGameController:onGameConnectFail()
    -- 断线重连
    local scene = display.getRunningScene()
    local tDlgAlert = scene:getChildByName("DlgAlert")
    if (tDlgAlert) then
        tDlgAlert:closeDialog()
    end
    --
    local scene = display.getRunningScene()
    if (not scene:getMainLayer()) then
        return 
    end
    --
	--if not ConnectManager:isConnectSvr(Protocol.LobbyServer) then
	--	ToolKit:removeLoadingDialog()
	--	GlobalDefine.ISLOGOUT = false
	--	g_isAllowReconnect = true
	--	TotalController:stopToSendHallPing()
	--	if g_isAllowReconnect then
	--		ConnectManager:reconnect()
	--	end
	--end
end

function caipiaoGameController:onGameConnectSuccess()
   Scheduler.performWithDelayGlobal(function()
            self:gameEnterReq()
   end, 1.0)
end

function caipiaoGameController:onGameConnect()
    -- 断线重连
    local scene = display.getRunningScene()
    local tDlgAlert = scene:getChildByName("DlgAlert")
    if (tDlgAlert) then
        tDlgAlert:closeDialog()
    end
    --
    local scene = display.getRunningScene()
    if (not scene:getMainLayer()) then
        return 
    end
    --
	if not ConnectManager:isConnectSvr(Protocol.LobbyServer) then
		ToolKit:removeLoadingDialog()
		GlobalDefine.ISLOGOUT = false
		g_isAllowReconnect = true
		TotalController:stopToSendHallPing()
		if g_isAllowReconnect then
			ConnectManager:reconnect()
		end
	end
end

return caipiaoGameController
