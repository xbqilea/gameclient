--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--
-- User: lhj
-- Date: 2018/7/14 
-- Time: 14:19 
local GameUserHead = require("src.app.newHall.rootLayer.UserHeadLayer")
local LISTITEM_CSB = "Games/NewBjle/userListUi/newUserListItem.csb";
local USER_HEAD_MASK = "platform/common/touxiangdi.png"; 
local TabCellBase = require("app.hall.base.ui.TabCellBase") 

local BjlUserListCell = class("BjlUserListCell", TabCellBase)

function BjlUserListCell:ctor()
	self:myInit()
	--self:setContentSize(cc.size(532, 95))
end

function BjlUserListCell:myInit()
	self.node = UIAdapter:createNode(LISTITEM_CSB) 
    self.node:setPosition(592,-1000) 
	self:addChild(self.node)
end

function BjlUserListCell:setMainLayer(layer)
	self.m_MainLayer = layer
end

function BjlUserListCell:initData(userInfo, index)
	--刷新数据
	local panel = self.node:getChildByName("Panel_1"); 
        panel:setTouchEnabled(false)
		local textUserNo =  panel:getChildByName("BitmapFontLabel_No");
		textUserNo:setVisible(false);
		textUserNo:setLocalZOrder(2000);
		local imgNo =  panel:getChildByName("Image_NOimg");
		imgNo:setVisible(false);
		imgNo:setLocalZOrder(2000);
		local imgUserNo = panel:getChildByName("Image_No");
		imgUserNo:setLocalZOrder(2000);
		imgUserNo:setVisible(false);
		local textUserName = panel:getChildByName("Text_userName");
		--Text* textUserMoney = (Text*)panel:getChildByName("Text_gold");
		local textUserGold = panel:getChildByName("AtlasLabel_gold");
		local textUserVip = panel:getChildByName("AtlasLabel_vip");
		--下注金钱
		local textUserMoney = panel:getChildByName("AtlasLabel_money");
		--胜利次数
		local textUserWinCount = panel:getChildByName("AtlasLabel_round");
		local fuserMoney = userInfo.m_recentBet / 100;
		textUserMoney:setString(fuserMoney);
		local Ysize = textUserMoney:getContentSize().width;
		--textUserMoney:setScale(115 / Ysize);
		textUserMoney:setPosition(415,65);
		textUserWinCount:setString(userInfo.m_recentWin);

		if (index < 9 and index ~= 0) then
		
			local NoUrl = string.format("Games/NewBjle/userListUi/userListUiRes/No_%d.png", index);
			imgUserNo:loadTexture(NoUrl);
			textUserNo:setVisible(false);
			imgUserNo:setVisible(true);
			imgNo:setVisible(false);
		
		elseif (index >= 9) then
		
			textUserNo:setString(string.format("%d", index + 1));
			textUserNo:setVisible(true);
			imgUserNo:setVisible(false);
			imgNo:setVisible(true);
		
		else 
			textUserNo:setVisible(false);
			imgUserNo:setVisible(true);
			imgNo:setVisible(false);
		end

		textUserName:setString(userInfo.m_nickname);
		--textUserName:setString(StringUtils::format("%d", userInfo.dwUserID));
		textUserGold:setString(userInfo.m_score / 100);
		textUserVip:setString(userInfo.m_vipLevel);


		--头像
		local img_headBox = panel:getChildByName("Image_userkuang");
		local img_head = panel:getChildByName("Image_userhead");
		img_head:setVisible(false);
		local userHead = GameUserHead.new(img_head, img_headBox);
		userHead:show(USER_HEAD_MASK);
		userHead:setLocalZOrder(1000);
		local headID = userInfo.m_faceId%7;
	 
		local headUrl = string.format("platform/userData/res/head/icon_%d.png", headID);
		userHead:loadTexture(headUrl);
		userHead:setScale(0.75);
		--头像框
		local headFrameID = userInfo.m_frameId;
		local headFrameUrl = string.format("platform/userData/res/frame/frame_%d.png", headFrameID);
		img_headBox:loadTexture(headFrameUrl);
		img_headBox:setScale(0.8);
		img_headBox:setLocalZOrder(1001); 
end

function BjlUserListCell:refresh(index, data)

	print("index:::::::", index)

	local page = 1

	if index % 10 == 0 then
		page = index / 10
	else
		page = math.floor(index / 10) + 1
	end

	if page > self.m_MainLayer.m_CurPage then
		self.m_MainLayer.m_CurPage = page
		self.m_MainLayer:refreshScrollView()
	end 
  
	if index >= #self.m_MainLayer.m_userList then
		self:setVisible(false)
	else
		self:setVisible(true)
		self:initData(self.m_MainLayer.m_userList[index], index)
	end
	 

end

return BjlUserListCell