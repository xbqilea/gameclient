--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--
-- Author:chenzhanming
-- Date: 2018-10-24 16:36:43
-- 异步加载资源

local scheduler = require("framework.scheduler")
local BjlLoadResAsync = class("BjlLoadResAsync")


local __ExportJson = "ExportJson"
local __plist = "plist"
local __png = "png"
local __jpg = "jpg"
local __mp3 = "mp3"


local __load = "load"
local __notLoad = "notLoad"

function BjlLoadResAsync:ctor()
	self:myInit()
end

-- 初始化
function BjlLoadResAsync:myInit()
	self._fileList = {} -- 提取的资源列表

	self._resourceList = {} -- 资源列表
	
	self._loadIndex = 1 -- 当前加载的下标
	
	self._currentSum = 0 -- 当前加载总数
	
	self.__currentLoadKey = "" -- 当前加载的路径 -- TODO: 可能会被修改, 在update里面
	
	self.loadCompleteCallback = nil -- 加载完成回调

	self.loadingCallback = nil -- 加载过程回调
end

-- 阻塞式加载
-- @params __resourcesTemp( table )  资源列表
function BjlLoadResAsync:blockLoadResources( __resourcesTemp )
	self.__currentLoadKey = __resourcesTemp.TableName
	self._resourceList[self.__currentLoadKey] = __resourcesTemp[self.__currentLoadKey]
	dump(self._resourceList[self.__currentLoadKey])
	for k, v in pairs(self._resourceList[self.__currentLoadKey]) do
		self:load(v)
	end
end

-- 预加载资源
-- @params __resourcesTemp( table )       资源列表
-- @params __callback( handler )          加载完所有资源回调
-- @params __loadingCallback( handler  )  加载完一个资源回调
function BjlLoadResAsync:preloadResources( __resourcesTemp, __callback, __loadingCallback )
	local key = __resourcesTemp.TableName
	local resList = __resourcesTemp[key]

	self.loadCompleteCallback = __callback

	self.loadingCallback = __loadingCallback

	self:loadResources(key, resList)
end

-- 释放路径里的资源
-- @params __resourcesTemp( table )  资源列表
function BjlLoadResAsync:removeResources( __resourcesTemp )
	local key = __resourcesTemp.TableName
	if self._resourceList[key] then
		for k, v in pairs(self._resourceList[key]) do
			self:remove(key, v)
		end
		self._resourceList[key] = nil
	end
end

-- 加载资源
-- @params __key( string )    资源键值
-- @params __resList( table ) 资源列表  
function BjlLoadResAsync:loadResources( __key, __resList )
	-- self:sort(__key, __resList)
	self._resourceList[__key] = __resList
	if #self._resourceList[__key] == 0 then
		-- print("There is no resource in directory: ", __key)
		if self.loadCompleteCallback then
			self.loadCompleteCallback()
		end
		return
	end
	self.__currentLoadKey = __key
	self._loadIndex = 1
	self._currentSum = #self._resourceList[__key]
	if self.timer == nil then
		local interval = cc.Director:getInstance():getAnimationInterval()
		self.timer = scheduler.scheduleGlobal(handler(self, self.update), 0.2 )
	end
end

-- 停止定时器
function BjlLoadResAsync:updateDataTimerEnd()
	if self.timer then
		scheduler.unscheduleGlobal(self.timer)
		self.timer = nil
	end
end

-- 定时器事件函数
-- @params  dt( number )  定时器时间间隔
function BjlLoadResAsync:update( dt )
	local res = self._resourceList[self.__currentLoadKey][self._loadIndex]
	self:load(res)
	
	 print(self._loadIndex .. "/" .. self._currentSum .. ": loading " .. res .. " ...")
	self._loadIndex = self._loadIndex + 1
	if self._loadIndex > self._currentSum then
		-- print("completed!!")
		if self.timer then
			scheduler.unscheduleGlobal(self.timer)
			self.timer = nil
		end

		if self.loadCompleteCallback then
			self.loadCompleteCallback()
		end

		-- self._loadIndex = 1 -- 当前加载的下标
		-- self._currentSum = 0 -- 当前加载总数
		-- self.__currentLoadKey = "" -- 当前加载的路径
		-- self.loadCompleteCallback = nil
		
	else
		if self.loadingCallback then
			self.loadingCallback(self._currentSum, self._loadIndex, res)
		end
	end
end


-- 预加载资源
-- @params __resourcesTemp( table )       资源列表
-- @params __callback( handler )          加载完所有资源回调
-- @params __loadingCallback( handler  )  加载完一个资源回调
function BjlLoadResAsync:preloadResourcesAsync( __resourcesTemp, __callback, __loadingCallback )
	local key = __resourcesTemp.TableName
	local resList = __resourcesTemp[key]

	self.loadCompleteCallback = __callback

	self.loadingCallback = __loadingCallback

	self:loadResourcesAsync(key, resList)
end

-- 加载资源
-- @params __key( string )    资源键值
-- @params __resList( table ) 资源列表 
function BjlLoadResAsync:loadResourcesAsync( __key, __resList )
	-- self:sort(__key, __resList)
	self._resourceList[__key] = __resList
	if #self._resourceList[__key] == 0 then
		-- print("There is no resource in directory: ", __key)
		if self.loadCompleteCallback then
			self.loadCompleteCallback()
		end
		return
	end
	self.__currentLoadKey = __key
	self._loadIndex = 1
	self._currentSum = #self._resourceList[__key]
	for i,v in ipairs( self._resourceList[self.__currentLoadKey] ) do
		if v then
            self:loadAsync( v )
		end
	end
end

-- 资源异步加载回调
-- @params  pert( number/string )  进度/资源
function BjlLoadResAsync:loadAsyncCallBack( pert )
	print("loadAsyncCallBack",self._loadIndex)
	print("pert=",pert)
    if self.loadingCallback then
       print("self.loadingCallback")
	   self.loadingCallback(self._currentSum, self._loadIndex )
	end
	self._loadIndex = self._loadIndex + 1
	print("self._loadIndex = self._loadIndex + 1=",self._loadIndex )
	if self._loadIndex > self._currentSum then
		if self.loadCompleteCallback then
			self.loadCompleteCallback()
		end
	end
end

-- 异步加载函数
-- @params plistFilename( string )  plist文件名
-- @parmas image( string )          图片名称
-- @parmas handler( hander )        函数回调
function BjlLoadResAsync:addSpriteFramesAsync(plistFilename ,image, handler )
    local function asyncHandler( texture )
        cc.SpriteFrameCache:getInstance():addSpriteFrames(plistFilename, image )
        if type( handler ) ==  "function" then
           handler( plistFilename )
        end
    end
    cc.Director:getInstance():getTextureCache():addImageAsync(image, asyncHandler)   
end

-- 资源异步加载函数
-- @params __res( string )  资源名称
function BjlLoadResAsync:loadAsync( __res )
	if self:isWithSuffix(__res, __plist) then -- 加载大图(.plist, .png)
		local prefix, suffix = ToolKit:getPrefixAndSuffix(__res)
		self:addSpriteFramesAsync(prefix..".plist",prefix..".png", handler(self,self.loadAsyncCallBack))
	elseif self:isWithSuffix(__res, __png) then -- 加载碎图(png)
        display.addImageAsync(__res, handler(self,self.loadAsyncCallBack))
	elseif self:isWithSuffix(__res, __jpg) then -- 加载碎图(jpg)
		display.addImageAsync(__res, handler(self,self.loadAsyncCallBack))
	elseif self:isWithSuffix(__res, __mp3) then -- 加载音效(.mp3)
		--cc.SimpleAudioEngine:getInstance():preloadEffect(__res)
	elseif self:isWithSuffix(__res, __ExportJson) then -- 加载动画(.ExportJson)
		--ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(__res)
		ccs.ArmatureDataManager:getInstance():addArmatureFileInfoAsync(__res, handler(self,self.loadAsyncCallBack) )
		--self:loadAsyncCallBack()
	end
end

-- 资源加载函数
-- @params __res( string )  资源名称
function BjlLoadResAsync:load( __res )
	if self:isWithSuffix(__res, __plist) then -- 加载大图(.plist, .png)
		local prefix, suffix = ToolKit:getPrefixAndSuffix(__res)
		cc.SpriteFrameCache:getInstance():addSpriteFrames(prefix..".plist",prefix..".png")
	elseif self:isWithSuffix(__res, __png) then -- 加载碎图(png)
		cc.Director:getInstance():getTextureCache():addImage(__res)
	elseif self:isWithSuffix(__res, __jpg) then -- 加载碎图(jpg)
		cc.Director:getInstance():getTextureCache():addImage(__res)
	elseif self:isWithSuffix(__res, __mp3) then -- 加载音效(.mp3)
		cc.SimpleAudioEngine:getInstance():preloadEffect(__res)
	elseif self:isWithSuffix(__res, __ExportJson) then -- 加载动画(.ExportJson)
		ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(__res)
	end
end

-- 内存中资源移除
-- @params __key( string )  键值
-- @params __res( string )  资源名称
function BjlLoadResAsync:remove( __key, __res )
	--print("remove: ", __key, __res)
	if self:isWithSuffix(__res, __plist) then -- 清除大图(.plist, .png)
		local prefix, suffix = ToolKit:getPrefixAndSuffix(__res)
		display.removeSpriteFramesWithFile(prefix..".plist",prefix..".png")
	elseif self:isWithSuffix(__res, __png) then -- 清除碎图(png)
		cc.Director:getInstance():getTextureCache():removeTextureForKey(__res)
	elseif self:isWithSuffix(__res, __jpg) then -- 清除碎图(jpg)
		cc.Director:getInstance():getTextureCache():removeTextureForKey(__res)
	elseif self:isWithSuffix(__res, __mp3) then -- 清除音效(.mp3)
		cc.SimpleAudioEngine:getInstance():unloadEffect(__res)
	elseif self:isWithSuffix(__res, __ExportJson) then -- 清除动画(.ExportJson)
		ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo(__res)
	end
end

-- 判断是否以此为后缀
-- @params __name( string )   文件名
-- @params __suffix( string ) 文件后缀
function BjlLoadResAsync:isWithSuffix( __name, __suffix )
	local prefix, suffix = ToolKit:getPrefixAndSuffix(__name)
	return (suffix == __suffix)
end

-- 销毁函数
function BjlLoadResAsync:onDestory()
	if self.timer then
		scheduler.unscheduleGlobal(self.timer)
		self.timer = nil
	end
end


return BjlLoadResAsync