--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion


local LISTITEM_CSB = "Games/NewBjle/recordUi/gameRecordItem.csb";
local WIN_IMG = "Games/NewBjle/recordUi/recordUiRes/win.png";
local WIN_XIAN = "Games/NewBjle/recordUi/recordUiRes/record/xian.png";
local WIN_ZHUANG = "Games/NewBjle/recordUi/recordUiRes/record/zhuang.png";
local WIN_HE = "Games/NewBjle/recordUi/recordUiRes/record/he.png";
local WIN_XIANDIAN = "Games/NewBjle/recordUi/recordUiRes/record/duizi_icon_blue.png";
local WIN_ZHUANGDIAN = "Games/NewBjle/recordUi/recordUiRes/record/duizi_icon_red.png";
local LINE_BLUE = "Games/NewBjle/recordUi/recordUiRes/record/corner_line_blue.png";
local LINE_RED = "Games/NewBjle/recordUi/recordUiRes/record/corner_line_red.png";
local HNLayer= require("src.app.newHall.HNLayer")
local BjlRecordListManager = class("BjlRecordListManager",function ()
     return HNLayer.new()
end)

function BjlRecordListManager:ctor()
    self:myInit()
    self:setupViews()
end 
function BjlRecordListManager:setupViews()
    local gamenode = UIAdapter:createNode("Games/NewBjle/recordUi/gameRecord.csb")
    self:addChild(gamenode)
    local panel = gamenode:getChildByName("Panel_1");
	local listBg = panel:getChildByName("zoushibg");

	local trendBg = listBg:getChildByName("Image_trendBg");

	trendBg:setPosition(trendBg:getPositionX() + 2,trendBg:getPositionY());

	self._img_cir_D = trendBg:getChildByName("Image_circle_dragon");
	self._img_ball_D = trendBg:getChildByName("Image_ball_dragon");
	self._img_line_D = trendBg:getChildByName("Image_line_dragon");

	self._img_cir_T =  trendBg:getChildByName("Image_circle_tiger");
	self._img_ball_T = trendBg:getChildByName("Image_ball_tiger");
	self._img_line_T = trendBg:getChildByName("Image_line_tiger");

 	self._listViewLeftRecord = listBg:getChildByName("ListView_leftRecord");
 	self._listViewLeftRecord:removeAllItems();
 	self._listViewLeftRecord:setTouchEnabled(false);
	--self._listViewLeftRecord:setScrollBarEnabled(false);

	self._textZhuangCount = listBg:getChildByName("Text_zhuangCount");
	self._textXianCount = listBg:getChildByName("Text_xianCount");
	self._textHeCount = listBg:getChildByName("Text_heCount");
	self._textZhuangdui = listBg:getChildByName("Text_zhuangduiCount");
	self._textXiandui = listBg:getChildByName("Text_xianduiCount");
	self._text89Dian = listBg:getChildByName("Text_gameCount_0");

	self._textGameCount = listBg:getChildByName("Text_gameCount");


	self._listViewCirRecord = listBg:getChildByName("ListView_3");
	self._listViewCirRecord:removeAllItems();
	self._listViewCirRecord:setTouchEnabled(true);
	--self._listViewCirRecord:setScrollBarEnabled(false);
	self._listViewCirRecord:setPositionX(self._listViewCirRecord:getPositionX() + 2);

	self._listViewBallRecord = listBg:getChildByName("ListView_4");
	self._listViewBallRecord:removeAllItems();
	self._listViewBallRecord:setTouchEnabled(true);
	self._listViewBallRecord:setPositionX(self._listViewBallRecord:getPositionX()+2);
--	self._listViewBallRecord:setScrollBarEnabled(false); 

	self._listViewLineRecord = listBg:getChildByName("ListView_6");
	self._listViewLineRecord:removeAllItems();
	self._listViewLineRecord:setTouchEnabled(true);

--	self._listViewLineRecord:setScrollBarEnabled(false);
	self._listViewLineRecord:setPositionX(self._listViewLineRecord:getPositionX() + 2); 

	self._listViewConRecord = listBg:getChildByName("ListView_2");
	self._listViewConRecord:removeAllItems();
	self._listViewConRecord:setTouchEnabled(true); 
--	self._listViewConRecord:setScrollBarEnabled(false);
	self._listViewConRecord:setPositionX(self._listViewConRecord:getPositionX() + 2);

	for  i = 0,31 do 
		self._lineArea[i] = ccui.Layout:create()
		self._lineArea[i]:setAnchorPoint(cc.p(0.5, 1.0));
		self._lineArea[i]:setBackGroundImage("Games/NewBjle/recordUi/recordUiRes/dl_bg.png");
		self._lineArea[i]:setContentSize(18, 108); 
		self._listViewConRecord:pushBackCustomItem(self._lineArea[i]);
		self._curTrenLine = self._curTrenLine+1;
    end
	for  i = 0,15 do 
		self._circlePanel[i] = ccui.Layout:create()
		self._linePanel[i] =  ccui.Layout:create()

		self._circlePanel[i]:setAnchorPoint(cc.p(0.5, 1.0));
		self._linePanel[i]:setAnchorPoint(cc.p(0.5, 1.0));

		self._circlePanel[i]:setBackGroundImage("Games/NewBjle/recordUi/recordUiRes/xsl_bg.png");
		self._linePanel[i]:setBackGroundImage("Games/NewBjle/recordUi/recordUiRes/xsl_bg.png");

		self._circlePanel[i]:setContentSize(18, 54);
		self._linePanel[i]:setContentSize(18, 54);

		self._listViewCirRecord:pushBackCustomItem(self._circlePanel[i]);
		self._listViewLineRecord:pushBackCustomItem(self._linePanel[i]);

		self._curCircleCol=self._curCircleCol+1;

		self._curLineCol = self._curLineCol+1;
	end
	for  i = 0,15 do  
		self._ballPanel[i] = ccui.Layout:create()
		self._ballPanel[i]:setAnchorPoint(cc.p(0.5, 1.0));
		self._ballPanel[i]:setBackGroundImage("Games/NewBjle/recordUi/recordUiRes/xsl_bg.png");
		self._ballPanel[i]:setContentSize(18, 54);
		self._listViewBallRecord:pushBackCustomItem(self._ballPanel[i]);
		self._curBallCol = self._curBallCol+1;
	end
         
	local Image_optionBG = listBg:getChildByName("Image_optionBG");
	self._txtxianHong = Image_optionBG:getChildByName("Text_6"); 
end
 
function BjlRecordListManager:getOneItem(oneRecord)
    local csbNode =UIAdapter:createNode(LISTITEM_CSB);
	local panel = csbNode:getChildByName("Panel_1");
	local imgResult = panel:getChildByName(string.format("Imag_results%d",0));
	if (oneRecord.win[0]) then
	
		imgResult:loadTexture(WIN_IMG);
	end

	csbNode:removeChild(panel);
	return panel;
end 

--显示左边的走势图
function BjlRecordListManager:showLeftRecord(record,gameCount)
    local csbNode =UIAdapter:createNode(LISTITEM_CSB);
	local panel = csbNode:getChildByName("Panel_1");
	for i = 1,6 do 
		local Recore = display.newSprite(WIN_ZHUANG);
		Recore:setVisible(false);
		if self._irecordCount >(gameCount) then
		 
			Recore:setVisible(false);
			 
		elseif 1 == record[i][0] then
			
			Recore:setTexture(WIN_ZHUANG);
			Recore:setTag(0);
			Recore:setVisible(true);
			
		elseif 1 == record[i][1] then
			
			Recore:setTexture(WIN_XIAN);
			Recore:setTag(1);
			Recore:setVisible(true);
			
		elseif 1 == record[i][2] then
			
			Recore:setTexture(WIN_HE);
			Recore:setTag(2);
			Recore:setVisible(true);
		end
		if (1 == record[i][3]) then
			
			local Recoredian = display.newSprite(WIN_ZHUANGDIAN);
			Recoredian:setPosition(30, 5);
			Recoredian:setName("zhuangdui");
			Recore:addChild(Recoredian);
			
		elseif (1 == record[i][4]) then
			
			local Recoredian = display.newSprite(WIN_XIANDIAN);
			Recoredian:setPosition(30, 5);
			Recoredian:setName("xiandui");
			Recore:addChild(Recoredian);
		end
		table.insert(self._vecRecord,Recore)
		Recore:setPosition(18, (198 - 36 * (i-1)));
		panel:addChild(Recore);
		self._irecordCount = self._irecordCount+1;
	end	
	csbNode:removeChild(panel);
	self._listViewLeftRecord:pushBackCustomItem(panel);
    panel:setLocalZOrder(1000);
    self._listViewLeftRecord:jumpToRight()
end
--添加新的走势
function BjlRecordListManager:addNewLeftRecord(vaule,index)
    	if index==0 or table.nums(self._vecRecord) then
		 
			return;
		end
		if (1 == vaule[0]) then
		
			self._vecRecord[index - 1]:setTexture(WIN_ZHUANG);
			self._vecRecord[index - 1]:setTag(0);
		
		elseif (1 == vaule[1]) then
		
			self._vecRecord[index - 1]:setTexture(WIN_XIAN);
			self._vecRecord[index - 1]:setTag(1);
		
		elseif (1 == vaule[2] )then
		
			self._vecRecord[index - 1]:setTexture(WIN_HE);
			self._vecRecord[index - 1]:setTag(2);
		end
		if (1 == vaule[3]) then
		
			local Recoredian = display.newSprite(WIN_ZHUANGDIAN);
			Recoredian:setPosition(30, 5);
			Recoredian:setName("zhuangdui");	
			self._vecRecord[index - 1]:addChild(Recoredian);
		
		elseif (1 == vaule[4]) then
		
			local Recoredian = display.newSprite(WIN_XIANDIAN);
			Recoredian:setPosition(30, 5);
			Recoredian:setName("xiandui");
			self._vecRecord[index - 1]:addChild(Recoredian);
		end
		self._vecRecord[index - 1]:setVisible(true);
		self._vecRecord[index - 1]:setOpacity(255);

        local act = {}
	    act[1] =cc.FadeIn:create(0.8 )
	    act[2] =  cc.FadeOut:create(0.8 )
	    local action = cc.Sequence:create(act)
	    self._vecRecord[index - 1]:runAction(action)
		 

    self._listViewLeftRecord:jumpToRight()
end
--更新一下牌局的数据 
--清空数据
function BjlRecordListManager:allClear()
    self._listViewLeftRecord:removeAllItems();
	self._listViewLeftRecord:removeAllChildren();
	self._vecRecord={}
	self._typeRecordPos={};
	self._irecordCount = 0;
	self:updateGameCountResult(0);
end
--设置record背景图
function BjlRecordListManager:setListviewBG(iseEable)
    if iseEable then
	
    	self._listViewLeftRecord:setTouchEnabled(true);
		self._listViewLeftRecord:setBackGroundImage("Games/NewBjle/recordUi/recordUiRes/corner_bg2.png");
	
	else
	
		self._listViewLeftRecord:setTouchEnabled(false);
		self._listViewLeftRecord:setBackGroundImage("Games/NewBjle/recordUi/recordUiRes/corner_bg.png");
	end
end

function BjlRecordListManager:showTrend(arrData,gameCount)
    local equal_count = 0;
    self._iTrendLine = 1;
	--存储所有游戏结果  0: 和  1:龙  2:虎
	for i = 1,gameCount do
		if arrData[i].m_xianVal> arrData[i].m_bankerVal then
            self._arrResult[i-1] = 1;
        elseif  arrData[i].m_xianVal<arrData[i].m_bankerVal then
            self._arrResult[i-1] =0;
        else
               self._arrResult[i-1] = 2;
        end
	end
	--计算本局结果所在行数
	for i = 1,gameCount-1 do
		if (i < gameCount) then
			if (self._arrResult[i] ~= self._arrResult[i - 1]) then
				self._iTrendLine = self._iTrendLine+1;
            end
        end
	end


	local index = 0;
	for i = gameCount,2,-1 do
		if (self._arrResult[i] == self._arrResult[i - 1]) then 
			index=index+1; 
		else 
			break;
		end
	end

	--判断是否需要添加列
	if (self._iTrendLine > self._curTrenLine) then
		for i = self._curTrenLine, self._iTrendLine-1 do 
			self._lineArea[i] = ccui.Layout:create();
			self._lineArea[i]:setAnchorPoint(cc.p(0.5, 1.0));
			self._lineArea[i]:setBackGroundImage("Games/NewBjle/recordUi/recordUiRes/dl_bg.png");
			self._lineArea[i]:setContentSize(18, 108);

			self._listViewConRecord:pushBackCustomItem(self._lineArea[i]);
			self._curTrenLine = self._curTrenLine+1;
		end
	end

	local Record = display.newSprite();
	if (self._arrResult[gameCount] == 0) then
		
		Record = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_blue.png");
		
	elseif (self._arrResult[gameCount] == 1) then
		
		Record = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_red.png");
		
	elseif (self._arrResult[gameCount] == 2) then
		
		Record = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_green.png");
	end

	if (self._iTrendLine == 0) then
		
		self._lineArea[0]:addChild(Record);
	else
		if self._lineArea[self._iTrendLine - 1] then
		    self._lineArea[self._iTrendLine - 1]:addChild(Record);
        end
	end

	Record:setPosition(8, 101 - 18 * index); 
	local pFadeIn =cc.FadeIn:create(0.5 )
	local pFadeOut =  cc.FadeOut:create(0.5 )
	     
	Record:setOpacity(0);
	Record:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), pFadeIn, pFadeOut, pFadeIn, nil));
end 

function BjlRecordListManager:showCircle(gameCount)
    self._circleLine = 1;
    local changeCol = 0;

	--计算本局结果所在行数
	for i = 2,gameCount do 
		if (i < gameCount) then
			
			if (self._arrCircle[i] ~= self._arrCircle[i - 1]) then
				
				changeCol=changeCol+1;
				if (changeCol == 2) then
					
					self._circleLine = self._circleLine+1;
					changeCol = 0;
				end
			end
		end
	end


	local index = 0;
	for i = gameCount,2,-1 do
		
		if (self._arrCircle[i] == self._arrCircle[i - 1]) then
			
			index=index+1;
			
		else
			
			break;
		end
	end

	--判断是否需要添加列
	if (self._circleLine > self._curCircleCol) then
		
		for i = self._curCircleCol,self._circleLine-1 do
			
			self._circlePanel[i] = ccui.Layout:create();
			self._circlePanel[i]:setAnchorPoint(cc.p(0.5, 1.0));
			self._circlePanel[i]:setBackGroundImage("Games/NewBjle/recordUi/recordUiRes/xsl_bg.png");
			self._circlePanel[i]:setContentSize(18, 54);

			self._listViewCirRecord:pushBackCustomItem(self._circlePanel[i]);
			self._curCircleCol = self._curCircleCol+1;
		end
	end

	local Record = display.newSprite();
	if (self._arrCircle[gameCount - 1] == 0) then
		
		Record = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_blue.png");
		
	elseif (self._arrCircle[gameCount - 1] == 1) then
		
		Record = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_red.png");
	end
	if (self._circleLine == 0) then
		
		self._circlePanel[0]:addChild(Record);
		
	else
		
		self._circlePanel[self._circleLine - 1]:addChild(Record);
	end

	Record:setScale(0.5);

	if (changeCol == 0) then
		
		Record:setPosition(4, 51 - 9 * index);
		
	else
		
		Record:setPosition(12, 51 - 9 * index);
	end

	local pFadeIn = cc.FadeIn:create(0.5);
	local pFadeOut = cc.FadeOut:create(0.5);
	Record:setOpacity(0);
	Record:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), pFadeIn, pFadeOut, pFadeIn, nil));
end
function BjlRecordListManager:initBall(data)
    if #data == 0 or data == nil then
        return
    end 
    self._circleLine = math.ceil(#data/2);  
	--判断是否需要添加列
	if (self._circleLine + 1 > self._curCircleCol) then 
		for i = self._curCircleCol,self._circleLine do 
			self._circlePanel[i] = ccui.Layout:create();
			self._circlePanel[i]:setAnchorPoint(cc.p(0.5, 1.0));
			self._circlePanel[i]:setBackGroundImage("Games/NewBjle/recordUi/recordUiRes/xsl_bg.png");
			self._circlePanel[i]:setContentSize(18, 54); 
			self._listViewCirRecord:pushBackCustomItem(self._circlePanel[i]);
			self._curCircleCol = self._curCircleCol+1;
		end
	end
    local fir = display.newSprite();
  --  fir:setScale(0.5);
    for k,v in pairs(data) do 
        for i =1,v.m_num do 
            if v.m_color == 1 then 
	            fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_ball_red.png");
            elseif  v.m_color == 0 then 
	            fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_ball_blue.png");
            end
            if k%2 == 1 then
                fir:setPosition(4, 51 - 9 * (i-1));
            else
                fir:setPosition(12, 51 - 9 * (i-1));
            end
              local index = math.ceil(k/2)-1
            self._circlePanel[index]:addChild(fir); 
        end
    end

--	if (self._arrCircle[0] == 0) then 
--		fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_blue.png");

--	elseif (self._arrCircle[0] == 1) then

--		fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_red.png");
--	end

--	if (gameCount > 0) then

--		self._circlePanel[0]:addChild(fir);
--	end
--	fir:setPosition(4, 51);

--	local lineIndex = 1;

--	local index = 0;
--	local isInd = true;
--	changeCol = 0;
--	for i = 2,gameCount do 
--		if (i < gameCount) then 
--			if (self._arrCircle[i] ~= self._arrCircle[i - 1]) then 
--				changeCol= changeCol+1;
--				if (changeCol == 2) then 
--					lineIndex=lineIndex+1;
--					changeCol = 0;
--				end
--			end 
--			if (self._arrCircle[1] == self._arrCircle[0] and isInd) then
--				index = index+1;
--				isInd = false;
--			end
--			if (i > 1) then
--				if (self._arrCircle[i] == self._arrCircle[i - 1]) then
--					index=index+1;
--				else
--					index = 0;
--				end
--			end


--			if (i < gameCount) then 
--				local Record = display.newSprite();
--				if (self._arrCircle[i] == 0) then 
--					Record = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_blue.png"); 
--				elseif (self._arrCircle[i] == 1) then 
--					Record = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_red.png");
--				end
--				if (lineIndex == 0) then 
--					self._circlePanel[0]:addChild(Record); 
--				else 
--					self._circlePanel[lineIndex - 1]:addChild(Record);
--				end
--				Record:setScale(0.5);
--				if (changeCol == 0) then 
--					Record:setPosition(4, 51 - 9 * index);
--				else
--					Record:setPosition(12, 51 - 9 * index);
--				end
--			end
--		end
--	end
end

function BjlRecordListManager:showBall( gameCount)
    self._ballLine = 1;
	local changeCol = 0;
	--计算本局结果所在行数
	for i = 2,gameCount do
		if (i < gameCount) then
			if (self._arrBall[i] ~= self._arrBall[i - 1]) then
				changeCol=changeCol+1;
				if (changeCol == 2) then
					
					self._ballLine = self._ballLine+1;
					changeCol = 0;
				end
			end
		end
	end


	local index = 0;
	for i = gameCount ,2,-1 do
		if (self._arrBall[i] == self._arrBall[i - 1]) then

			index=index+1; 
		else 
			break;
		end
	end

	--判断是否需要添加列
	if (self._ballLine > self._curBallCol) then 
		for i = self._curBallCol,self._ballLine-1 do 
			self._ballPanel[i] = ccui.Layout:create();
			self._ballPanel[i]:setAnchorPoint(cc.p(0.5, 1.0));
			self._ballPanel[i]:setBackGroundImage("Games/NewBjle/recordUi/recordUiRes/xsl_bg.png");
			self._ballPanel[i]:setContentSize(18, 54);

			self._listViewBallRecord:pushBackCustomItem(self._ballPanel[i]);
			self._curBallCol = self._curBallCol+1;
		end
	end

	local Record = display.newSprite();
	if (self._arrBall[gameCount - 1] == 0) then
		Record = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_ball_blue.png"); 
	elseif (self._arrBall[gameCount - 1] == 1) then 
		Record = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_ball_red.png");
	end
	if (self._ballLine == 0) then
		self._ballPanel[0]:addChild(Record); 
	else 
		self._ballPanel[self._ballLine - 1]:addChild(Record);
	end
	--Record:setScale(0.5f);

	if (changeCol == 0) then 
		Record:setPosition(4, 51 - 9 * index); 
	else
 
		Record:setPosition(12, 51 - 9 * index);
	end
	local pFadeIn = cc.FadeIn:create(0.5);
	local pFadeOut = cc.FadeOut:create(0.5);
	Record:setOpacity(0);
	Record:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), pFadeIn, pFadeOut, pFadeIn, nil));
end
function BjlRecordListManager:initCircle(data)
  if #data == 0 or data == nil then
        return
    end 
    self._ballLine = math.ceil(#data/2);  
	--判断是否需要添加列
	if (self._ballLine + 1 > self._curBallCol) then 
		for i = self._curCircleCol,self._ballLine do 
			self._ballPanel[i] = ccui.Layout:create();
			self._ballPanel[i]:setAnchorPoint(cc.p(0.5, 1.0));
			self._ballPanel[i]:setBackGroundImage("Games/NewBjle/recordUi/recordUiRes/xsl_bg.png");
			self._ballPanel[i]:setContentSize(18, 54); 
			self._listViewBallRecord:pushBackCustomItem(self._ballPanel[i]);
			self._curBallCol = self._curBallCol+1;
		end
	end
    local fir = display.newSprite();
   
    for k,v in pairs(data) do 
        for i =1,v.m_num do 
            if v.m_color == 1 then 
	            fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_red.png");
            elseif  v.m_color == 0 then 
	            fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_blue.png");
            end
             fir:setScale(0.5);
            if k%2 == 1 then
                fir:setPosition(4, 51 - 9 * (i-1));
            else
                fir:setPosition(12, 51 - 9 * (i-1));
            end
            local index = math.ceil(k/2)-1 
            self._ballPanel[index]:addChild(fir); 
        end
    end 
end

function BjlRecordListManager:showLine(gameCount)
    self._lineLine = 1;
	local changeCol = 0;
	--计算本局结果所在行数
	for i = 2,gameCount do  
		if (i < gameCount) then 
			if (self._arrLine[i] ~= self._arrLine[i - 1]) then 
				changeCol=changeCol+1;
				if (changeCol == 2) then
					self._lineLine= self._lineLine+1;
					changeCol = 0;
				end
			end
		end
	end


	local index = 0;
	for i = gameCount ,2,-1 do 
		if (self._arrLine[i] == self._arrLine[i - 1]) then 
			index=index+1; 
		else 
			break;
		end
	end

	--判断是否需要添加列
	if (self._lineLine > self._curLineCol) then
		for i = self._curLineCol,self._lineLine-1 do 
			self._linePanel[i] = ccui.Layout:create();
			self._linePanel[i]:setAnchorPoint(cc.p(0.5, 1.0));
			self._linePanel[i]:setBackGroundImage("Games/NewBjle/recordUi/recordUiRes/xsl_bg.png");
			self._linePanel[i]:setContentSize(18, 54);

			self._listViewLineRecord:pushBackCustomItem(self._linePanel[i]);
			self._curLineCol =self._curLineCol+1;
		end
	end

	local Record = display.newSprite();
	if (self._arrLine[gameCount - 1] == 0) then
		Record = display.newSprite(LINE_BLUE); 
	elseif (self._arrLine[gameCount - 1] == 1) then
		Record = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_line_red.png");
	end
	if (self._lineLine == 0) then
		self._linePanel[0]:addChild(Record); 
	else 
		self._linePanel[self._lineLine - 1]:addChild(Record);
	end
	--Record:setScale(0.5f);
	if (changeCol == 0) then
		Record:setPosition(4, 51 - 9 * index); 
	else 
		Record:setPosition(12, 51 - 9 * index);
	end
	local pFadeIn = cc.FadeIn:create(0.5);
	local pFadeOut = cc.FadeOut:create(0.5);
	Record:setOpacity(0);
	Record:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), pFadeIn, pFadeOut, pFadeIn, nil));
end
function BjlRecordListManager:initLine(data)
    if #data == 0 or data == nil then
        return
    end 
    self._lineLine = math.ceil(#data/2);  
	--判断是否需要添加列
	if (self._lineLine + 1 > self._curLineCol) then 
		for i = self._curLineCol,self._lineLine do 
			self._linePanel[i] = ccui.Layout:create();
			self._linePanel[i]:setAnchorPoint(cc.p(0.5, 1.0));
			self._linePanel[i]:setBackGroundImage("Games/NewBjle/recordUi/recordUiRes/xsl_bg.png");
			self._linePanel[i]:setContentSize(18, 54); 
			self._listViewLineRecord:pushBackCustomItem(self._linePanel[i]);
			self._curLineCol = self._curLineCol+1;
		end
	end
    local fir = display.newSprite();
  --  fir:setScale(0.5);
    for k,v in pairs(data) do 
        for i =1,v.m_num do 
            if v.m_color == 1 then 
	            fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_line_red.png");
            elseif  v.m_color == 0 then 
	            fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_line_blue.png");
            end
            if k%2 == 1 then
                fir:setPosition(4, 51 - 9 * (i-1));
            else
                fir:setPosition(12, 51 - 9 * (i-1));
            end
             local index = math.ceil(k/2)-1
            self._linePanel[index]:addChild(fir); 
        end
    end
--    self._lineLine = 0;
--	local changeCol = 0;
--	--存储所有游戏结果  0: 和  1:龙  2:虎
--	for  i = 1,72 do
--		self._arrLine[i] = data[i];
--	end
--	for i = 2,gameCount do  
--		if (i < gameCount) then 
--			if (self._arrLine[i] ~= self._arrLine[i - 1]) then 
--				changeCol=changeCol+1;
--				if (changeCol == 2) then 
--					self._lineLine= self._lineLine+1;
--					changeCol = 0;
--				end
--			end
--		end
--	end
--	--判断是否需要添加列
--	if (self._lineLine + 1 > self._curLineCol) then 
--		for i = self._curLineCol,self._lineLine do
--			self._linePanel[i] = ccui.Layout:create();
--			self._linePanel[i]:setAnchorPoint(cc.p(0.5, 1.0));
--			self._linePanel[i]:setBackGroundImage("Games/NewBjle/recordUi/recordUiRes/xsl_bg.png");
--			self._linePanel[i]:setContentSize(18, 54);

--			self._listViewLineRecord:pushBackCustomItem(self._linePanel[i]);
--			self._curLineCol = self._curLineCol+1;
--		end
--	end
--	local fir = display.newSprite();
--	if (self._arrLine[1] == 0) then
--		fir = display.newSprite(LINE_BLUE);
--	elseif (self._arrLine[1] == 1) then
--		fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_line_red.png");
--	end
--	if (gameCount > 0) then
--		self._linePanel[0]:addChild(fir);
--	end
--	--fir:setScale(0.5f);
--	fir:setPosition(4, 51);

--	local lineIndex = 1;
--	changeCol = 0;
--	local index = 0;
--	local isInd = true;
--	for i = 2,gameCount do  
--		if (i < gameCount) then 
--			if (self._arrLine[i] ~= self._arrLine[i - 1]) then
--				changeCol=changeCol+1;
--				if (changeCol == 2) then
--					lineIndex=lineIndex+1;
--					changeCol = 0;
--				end
--			end

--			if (self._arrLine[1] == self._arrLine[0] and isInd) then 
--				index=index+1;
--				isInd = false;
--			end
--			if (i > 1) then 
--				if (self._arrLine[i] == self._arrLine[i - 1]) then 
--					index=index+1; 
--				else 
--					index = 0;
--				end
--			end


--			if (i < gameCount) then 
--				local Record = display.newSprite();
--				if (self._arrLine[i] == 0) then
--					Record = display.newSprite(LINE_BLUE); 
--				elseif (self._arrLine[i] == 1) then
--					Record = display.newSprite("recordUi/recordUiRes/record/corner_line_red.png");
--				end
--				if (lineIndex == 0) then 
--					self._linePanel[0]:addChild(Record); 
--				else 
--					self._linePanel[lineIndex - 1]:addChild(Record);
--				end
--				--Record:setScale(0.5f);
--				if (changeCol == 0) then 
--					Record:setPosition(4, 51 - 9 * index); 
--				else 
--					Record:setPosition(12, 51 - 9 * index);
--				end
--			end
--		end
--	end
end

function BjlRecordListManager:falshTrendImg(gameCount)
    local nRand_cir = math.random(0,100) % 2;
	local nRand_ball = math.random(0,100) % 2;
	local nRand_line = math.random(0,100) % 2;

	if (nRand_cir == 0) then 
		self._img_cir_D:loadTexture("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_blue.png");
		self._img_cir_T:loadTexture("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_red.png"); 
	else 
		self._img_cir_T:loadTexture("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_blue.png");
		self._img_cir_D:loadTexture("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_red.png");
	end

	if (nRand_ball == 0) then 
		self._img_ball_D:loadTexture("platform/lobbyUi/res/lobby/corner_ball_blue.png");
		self._img_ball_T:loadTexture("platform/lobbyUi/res/lobby/corner_ball_red.png"); 
	else 
		self._img_ball_T:loadTexture("platform/lobbyUi/res/lobby/corner_ball_blue.png");
		self._img_ball_D:loadTexture("platform/lobbyUi/res/lobby/corner_ball_red.png");
	end 
	if (nRand_line == 0) then 
		self._img_line_D:loadTexture("platform/lobbyUi/res/lobby/corner_line_blue.png");
		self._img_line_T:loadTexture("platform/lobbyUi/res/lobby/corner_line_red.png"); 
	else 
		self._img_line_T:loadTexture("platform/lobbyUi/res/lobby/corner_line_blue.png");
		self._img_line_D:loadTexture("platform/lobbyUi/res/lobby/corner_line_red.png");
	end 
end
function BjlRecordListManager:clearData()
    self._curCircleCol = 0;
	self._curBallCol = 0;
	self._curLineCol = 0;
	self._curTrenLine = 0;
    self._arrCircle = {}
    self._arrBall = {}
    self._arrLine = {} 

	self._listViewConRecord:removeAllChildren();
	self._listViewCirRecord:removeAllChildren();
	self._listViewBallRecord:removeAllChildren();
	self._listViewLineRecord:removeAllChildren();

	for i = 0,31 do 
		self._lineArea[i] = ccui.Layout:create();
		self._lineArea[i]:setAnchorPoint(cc.p(0.5, 1.0));
		self._lineArea[i]:setBackGroundImage("Games/NewBjle/recordUi/recordUiRes/dl_bg.png");
		self._lineArea[i]:setContentSize(18, 108);

		self._listViewConRecord:pushBackCustomItem(self._lineArea[i]);
		self._curTrenLine = self._curTrenLine+1;
	end
	for i = 0,15 do  
		self._circlePanel[i] = ccui.Layout:create();
		self._linePanel[i] = ccui.Layout:create();

		self._circlePanel[i]:setAnchorPoint(cc.p(0.5, 1.0));
		self._linePanel[i]:setAnchorPoint(cc.p(0.5, 1.0));

		self._circlePanel[i]:setBackGroundImage("Games/NewBjle/recordUi/recordUiRes/xsl_bg.png");
		self._linePanel[i]:setBackGroundImage("Games/NewBjle/recordUi/recordUiRes/xsl_bg.png");

		self._circlePanel[i]:setContentSize(18, 54);
		self._linePanel[i]:setContentSize(18, 54);

		self._listViewCirRecord:pushBackCustomItem(self._circlePanel[i]);
		self._listViewLineRecord:pushBackCustomItem(self._linePanel[i]);

		self._curCircleCol=self._curCircleCol+1;

		self._curLineCol=self._curLineCol+1; 

		self._ballPanel[i] = ccui.Layout:create();
		self._ballPanel[i]:setAnchorPoint(cc.p(0.5, 1.0));
		self._ballPanel[i]:setBackGroundImage("Games/NewBjle/recordUi/recordUiRes/xsl_bg.png");
		self._ballPanel[i]:setContentSize(18, 54);
		self._listViewBallRecord:pushBackCustomItem(self._ballPanel[i]);
		self._curBallCol = self._curBallCol+1;
	end
	--清空数据  随机数重新随机
	for i = 1,72 do  
 
		self._arrCircle[i] = math.random(0,100) % 2;
		self._arrLine[i] = math.random(0,100) % 2;
		self._arrBall[i] = math.random(0,100) % 2;
	end

end
--设置限红数额
function BjlRecordListManager:setXianhongMoney(money)
    self._txtxianHong:setString( money / 100);
end 
 function BjlRecordListManager:updateGameCountResult(i89dianCount)
     self._iZhuangwin = 0;
	self._iXianwin = 0;
	self._iHeWin = 0;
	self._iZhuangdui = 0;
	self._iXiandui = 0;
	self._iAllGameCount = 0;
	self._i89Dian = i89dianCount;
	for k,v in pairs(self._vecRecord) do
		if v:isVisible() then
			
			if v:getTag() == 0 then 
				self._iZhuangwin = self._iZhuangwin+1; 
			elseif v:getTag() == 1 then 
                self._iXianwin = self._iXianwin+1;  
            elseif v:getTag() == 2 then 
                self._iHeWin = self._iHeWin+1;   
            end
        end
		local xiandui = v:getChildByName("xiandui");
		if xiandui then
				self._iXiandui= self._iXiandui+1;
		end
		local zhuangdui = v:getChildByName("zhuangdui");
		if zhuangdui then
            self._iZhuangdui= self._iZhuangdui+1;  
        end
	end

	self._iAllGameCount = self._iZhuangwin + self._iXianwin + self._iHeWin;
	self._textZhuangCount:setString(string.format("%d", self._iZhuangwin));
	self._textXianCount:setString(string.format("%d", self._iXianwin));
	self._textHeCount:setString(string.format("%d", self._iHeWin));
	self._textZhuangdui:setString(string.format("%d", self._iZhuangdui));
	self._textXiandui:setString(string.format("%d", self._iXiandui));
	self._textGameCount:setString(string.format("%d", self._iAllGameCount));
	self._text89Dian:setString(string.format("%d", self._i89Dian));
end
function BjlRecordListManager:initConTrend(arrData,gameCount)
    local equal_count = 0;
	self._iTrendLine = 0;
		--存储所有游戏结果  0: 和  1:龙  2:虎
    if #arrData == 0 then
        return
    end
    local index= 0
	for i = 0,gameCount-1 do
		 
        if arrData[i+1].m_xianVal> arrData[i+1].m_bankerVal then
            self._arrResult[i-index] = 1; 
        elseif  arrData[i+1].m_xianVal<arrData[i+1].m_bankerVal then
            self._arrResult[i-index] =0; 
        else
            index = index+1
            if self._arrResult[i-index] then
                self._arrResult[i-index] = self._arrResult[i-index]+10
            end
        end
	end
	for i = 1,table.nums(self._arrResult)-1 do 
		if (self._arrResult[i]%10 ~= self._arrResult[i - 1]%10) then 
			self._iTrendLine = self._iTrendLine+1;
		end 
	end
	--判断是否需要添加列
	if (self._iTrendLine + 1 > self._curTrenLine) then
	
		for i = self._curTrenLine,self._iTrendLine + 1 do
		
			self._lineArea[i] = ccui.Layout:create();
			self._lineArea[i]:setAnchorPoint(cc.p(0.5, 1.0));
			self._lineArea[i]:setBackGroundImage("Games/NewBjle/recordUi/recordUiRes/dl_bg.png");
			self._lineArea[i]:setContentSize(18, 108);
            
			self._listViewConRecord:pushBackCustomItem(self._lineArea[i]);
			self._curTrenLine = self._curTrenLine+1;
		end
	end
	local fir = display.newSprite();
    if self._arrResult[0]== nil then
        return
    end
	if (self._arrResult[0] == 0) then
	
		fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_red.png");
	
	elseif (self._arrResult[0] == 1) then 
		fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_blue.png"); 
    else
        local num = math.floor(self._arrResult[0]/10)
        local result = self._arrResult[0]%10
        if result ==0 then
            fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_red.png");
        elseif result == 1 then
            fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_blue.png"); 
        end
        local text = ccui.TextAtlas:create(tostring(num),"Games/NewBjle/recordUi/recordUiRes/record/num_green.png",13,18,"0") 
        fir:addChild(text)
		text:setPosition(fir:getContentSize().width/2,fir:getContentSize().height/2)
        text:setScale(0.5) 
	end
   
	if (table.nums(self._arrResult) > 0) then
	
		self._lineArea[0]:addChild(fir);
	end
	fir:setPosition(8, 101);

	local lineIndex = 1;

	local index = 0;
	local isInd = true;
	for i = 1, table.nums(self._arrResult)-1 do
		if (i < table.nums(self._arrResult)) then
			if (self._arrResult[i]%10 ~= self._arrResult[i - 1]%10) then 
                lineIndex = lineIndex+1;
			end

			if (self._arrResult[1]%10 == self._arrResult[0]%10 and isInd) then
				index=index+1;
				isInd = false;
			end
			if (i > 1) then
				if (self._arrResult[i]%10 ==self._arrResult[i - 1]%10) then
					index=index+1;
					
				else
					
					index = 0;
				end
			end 

			if (i < table.nums(self._arrResult)) then 
				local Record = display.newSprite();
				if (self._arrResult[i] == 0) then
				
					Record = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_red.png");
				
				elseif (self._arrResult[i] == 1) then
				
					Record = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_blue.png"); 
               else
                    local num = math.floor(self._arrResult[i]/10)
                    local result = self._arrResult[i]%10
                    if result ==0 then
                        Record = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_red.png");
                    elseif result == 1 then
                        Record = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_blue.png"); 
                    end
                    local text = ccui.TextAtlas:create(tostring(num),"Games/NewBjle/recordUi/recordUiRes/record/num_green.png",13,18,"0") 
                    Record:addChild(text)
				    text:setPosition(Record:getContentSize().width/2,Record:getContentSize().height/2)
                    text:setScale(0.5)
                end
               
				   
                 
                 if 101 -18 * index >0 then
				    if (lineIndex == 0) then
				
					    self._lineArea[0]:addChild(Record);
				
				    else
				        if self._lineArea[lineIndex - 1] then
					        self._lineArea[lineIndex - 1]:addChild(Record);
                        end
				    end
                     Record:setPosition(8, 101 - 18 * index);
                 else
                    if (lineIndex+index-6)<32 then
                        self._lineArea[lineIndex+index-6]:addChild(Record);
                        Record:setPosition(8, 11);
                    else
                        self._lineArea[lineIndex+index-6] = ccui.Layout:create()
		                self._lineArea[lineIndex+index-6]:setAnchorPoint(cc.p(0.5, 1.0));
		                self._lineArea[lineIndex+index-6]:setBackGroundImage("Games/NewBjle/recordUi/recordUiRes/dl_bg.png");
		                self._lineArea[lineIndex+index-6]:setContentSize(18, 108); 
		                self._listViewConRecord:pushBackCustomItem(self._lineArea[lineIndex+index-6]);
		                self._curTrenLine = self._curTrenLine+1;
                        self._lineArea[lineIndex+index-6]:addChild(Record);
                        Record:setPosition(8, 11);
                    end
                 end
			end
		end
	end
end
function BjlRecordListManager:myInit()
    self._listViewLeftRecord = nil;--左边走势图 
    self._textZhuangCount=0;--庄的局数
    self._textXianCount=0;--闲的局数
    self._textHeCount=0;--和的局数
    self._textZhuangdui=0;--庄对的局数
    self._textXiandui=0;--闲对的局数
    self._text89Dian=0;--8/9点的局数
    self._textGameCount=0;--游戏局数 
    self._iXianwin = 0;--闲赢
    self._iZhuangwin = 0;--庄赢
    self._iHeWin = 0;--和赢
    self._iZhuangdui = 0;--庄对
    self._iXiandui = 0;--闲对
    self._i89Dian = 0;--89点
    self._iAllGameCount = nil;--总的局数
    self._typeRecordPos = nil;
    self._irecordCount = 0;--记录当前有的数据个数
    self._vecRecord = {};--存储走势节点的容器 
    self._lineArea={};
    self._circlePanel={};
    self._ballPanel={};
    self._linePanel={};
    self._listViewCirRecord= nil;
    self._listViewBallRecord= nil;
    self._listViewLineRecord= nil;
    self._listViewConRecord= nil; 
    self._iTrendLine = 0;
    self._curTrenLine = 0;
    self._arrResult={};
    self._arrCircle={};
    self._circleLine = 0;
    self._curCircleCol = 0; 
    self._arrBall={};
    self._ballLine = 0;
    self._curBallCol = 0; 
    self._arrLine={};
    self._lineLine = 0;
    self._curLineCol = 0; 
    self._img_cir_D= nil; 
    self._img_ball_D= nil; 
    self._img_line_D= nil; 
    self._img_cir_T= nil; 
    self._img_ball_T= nil; 
    self._img_line_T= nil; 
    self._txt_xianHong= nil; 

end
return BjlRecordListManager