--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

--endregion
local HNlayer = require("src.app.newHall.HNLayer")
local scheduler = require("framework.scheduler")
local BjlGameDesk = class("BjlGameDesk",function()
    return HNlayer.new()
end)

local DESKUI_CSB = "app/game/bjl/res/Baccarat/BaccaratRoomScene.csb"
local DESK_ITEM_UI = "app/game/bjl/res/Baccarat/BaccaratTableNode.csb"
local WIN_ZHUANG = "app/game/bjl/res/Baccarat/image/room/zhuang.png"
local WIN_XIAN = "app/game/bjl/res/Baccarat/image/room/xian.png"
local WIN_HE = "app/game/bjl/res/Baccarat/image/room/he.png"
local WIN_ZHUANGDUI = "app/game/bjl/res/Baccarat/image/room/zhuangdui.png"
local WIN_XIANDUI = "app/game/bjl/res/Baccarat/image/room/xiandui.png"

local pageViewDesksSize = cc.size(1280, 500)

function BjlGameDesk:ctor(_roomInfo,_score)
    self:myInit(_roomInfo,_score)
end

function BjlGameDesk:clearData()
	if self._timeText then
		scheduler.unscheduleGlobal(self._timeText)
		self._timeText = nil
	end
	if self._timeProgress then
		scheduler.unscheduleGlobal(self._timeProgress)
		self._timeProgress = nil
	end
end

function BjlGameDesk:resetRoomList(_roomInfo)
    for i=1,self._deskCount do
        if self._roomList[i].m_id==_roomInfo.m_roomId then
            if #self._roomList[i].m_history == 72 then
                self._roomList[i].m_history = {}
                self._roomList[i].m_dyzlu = {}
                self._roomList[i].m_xlu = {}
                self._roomList[i].m_xqlu = {}
            end
            table.insert(self._roomList[i].m_history,_roomInfo.m_result)
            if  _roomInfo.m_dyzlu and #_roomInfo.m_dyzlu~=0 then
                if #self._roomList[i].m_dyzlu == 0 then
                    table.insert(self._roomList[i].m_dyzlu,_roomInfo.m_dyzlu[1])
                end
                if _roomInfo.m_dyzlu[1].m_color ~=self._roomList[i].m_dyzlu[#self._roomList[i].m_dyzlu].m_color then
                    table.insert(self._roomList[i].m_dyzlu, _roomInfo.m_dyzlu[1]) 
                else
                    self._roomList[i].m_dyzlu[#self._roomList[i].m_dyzlu] = _roomInfo.m_dyzlu[1]
                end
            end
            if _roomInfo.m_xlu and #_roomInfo.m_xlu~=0 then
                if #self._roomList[i].m_xlu == 0 then
                    table.insert(self._roomList[i].m_xlu,_roomInfo.m_xlu[1])
                end
                if _roomInfo.m_xlu[1].m_color~=self._roomList[i].m_xlu[#self._roomList[i].m_xlu].m_color then
                    table.insert(self._roomList[i].m_xlu,_roomInfo.m_xlu[1]) 
                else
                    self._roomList[i].m_xlu[#self._roomList[i].m_xlu] = _roomInfo.m_xlu[1]
                end
            end
            if _roomInfo.m_xqlu and #_roomInfo.m_xqlu~=0 then
                if #self._roomList[i].m_xqlu == 0 then
                    table.insert(self._roomList[i].m_xqlu,_roomInfo.m_xqlu[1])
                end
                if _roomInfo.m_xqlu[1].m_color ~=self._roomList[i].m_xqlu[#self._roomList[i].m_xqlu].m_color then
                    table.insert(self._roomList[i].m_xqlu,_roomInfo.m_xqlu[1]) 
                else
                    self._roomList[i].m_xqlu[#self._roomList[i].m_xqlu] = _roomInfo.m_xqlu[1]
                end 
            end
            self._iGameCount[i] = table.nums(self._roomList[i].m_history)
            self:resetNoTime(i)
            self:resetEndState(i)
            self:refreshGameCount(self._roomList[i].m_history,i)
            self:showTrend(self._roomList[i].m_history,self._iGameCount[i],i)
            self:showCircle(self._roomList[i].m_dyzlu, i)
	        self:showBall(self._roomList[i].m_xlu, i)
	        self:showLine(self._roomList[i].m_xqlu, i)
            self:falshTrendImg(self._iGameCount[i], i)
            return
        end
    end
end

function BjlGameDesk:reloadRoomList(_roomInfo,_score)
    local roomCount0 = self._deskCount
    print("roomCount0",roomCount0)
    local layernodecount = #self._layerNode
    print("layernodecount",layernodecount)
    self._roomList = _roomInfo
    self._score = _score
    local num_money = self._score / 100
    self._text_gold:setString(num_money)
    self._deskCount = table.nums(self._roomList)
    local y1 = math.modf(self._deskCount/2)
    local y2 = math.fmod(self._deskCount,2)

    if self._deskCount<roomCount0 then
        for i=1,roomCount0 do
            if i>self._deskCount then
                self._roomItemNode[i]:removeFromParent()
                self._roomItemNode[i]=nil
            end
        end
    end

    if y1+y2<layernodecount then
        for i=1,layernodecount do
            if i>y1+y2 then
                self._layerNode[i]:removeFromParent()
                self._layerNode[i]=nil
            end
        end
    end

    for i = 1,y1+y2 do
        if self._layerNode[i]==nil then
            self._layerNode[i] = ccui.Layout:create()
            self._layerNode[i]:setAnchorPoint(cc.p(0.5,0.5))
            if (self._winSize.width / self._winSize.height > 1.78) then 
		        self._layerNode[i]:setContentSize(cc.size(1440, 320))
            else
                self._layerNode[i]:setContentSize(cc.size(1280, 320))
            end
            self._layerNode[i]:setScaleX(1/display.scaleX);
            self._layerNode[i]:setTouchEnabled(true)
            self._layerNode[i]:setSwallowTouches(false)
            self._bjlRoomListView:pushBackCustomItem(self._layerNode[i])
        end
    end

    local scaleN = 0.75
    if (self._winSize.width / self._winSize.height > 1.78) then 
        scaleN = 0.72
    else
        scaleN = 0.75
    end

    for i=1,self._deskCount do
        if self._roomItemNode[i]==nil then
            local x1 = math.modf((i-1)/2) + 1
            local x2 = math.fmod((i-1),2)
            self._iGameCount[i] = table.nums(self._roomList[i].m_history)
		    self._ftime[i] = self._roomList[i].m_leftTime
		    self._itime[i] = self._roomList[i].m_leftTime
		    self:creatBjlRoomItem(self._roomList[i], i)
		    if (self._roomItemNode[i]) then
                print("layernode",x1)
			    self._layerNode[x1]:addChild(self._roomItemNode[i], 2)
			
                if x2 == 1 then
                    self._roomItemNode[i]:setPosition(cc.p(self._layerNode[x1]:getContentSize().width * scaleN, self._layerNode[x1]:getContentSize().height / 2))
                else
                    self._roomItemNode[i]:setPosition(cc.p(self._layerNode[x1]:getContentSize().width * (1-scaleN), self._layerNode[x1]:getContentSize().height / 2))
                end
		    end
        else
            self._iGameCount[i] = table.nums(self._roomList[i].m_history)
            self:refreshGameCount(self._roomList[i].m_history,i)
            self:showTrend(self._roomList[i].m_history,self._iGameCount[i],i)
            self:showCircle(self._roomList[i].m_dyzlu,i)
            self:showBall(self._roomList[i].m_xlu,i)
            self:showLine(self._roomList[i].m_xqlu,i)
        end
    end

    for i = 1,self._deskCount do
        self:enterRoomEventCallBack(i)
		self:initTimeCountText(self._roomList[i].m_leftTime, i)
    end

    self:clearData()

    self._timeText = scheduler.scheduleGlobal(handler(self,self.setTimeCountText), 0.95)
    self._timeProgress = scheduler.scheduleGlobal(handler(self,self.setTimeCountProgress), 0.01)
end

function BjlGameDesk:resetDesk()
end

function BjlGameDesk:resetTimeLeft(time,index)
    self._itime[index] = time
    self._ftime[index] = time
    self._AtlasLabel_time[index]:setVisible(true)
    self._AtlasLabel_time[index]:setString(20)
    self._LoadingBar_time[index]:setVisible(true)
    self._LoadingBar_time[index]:setPercent(100)
end

function BjlGameDesk:resetNoTime(index)
    self._itime[index] = 0
    self._ftime[index] = 0
    self._AtlasLabel_time[index]:setVisible(false)
    self._LoadingBar_time[index]:setVisible(false)
end

function BjlGameDesk:resetFreeState(index)
    self._Text_betTime[index]:setVisible(false)
    self._AtlasLabel_time[index]:setVisible(false)
    self._Text_over[index]:setVisible(true)
end

function BjlGameDesk:resetStartState(index)
    self._Text_betTime[index]:setVisible(true)
    self._AtlasLabel_time[index]:setVisible(true)
    self._Text_over[index]:setVisible(false)
end

function BjlGameDesk:resetOpenCardState(index)
    self._Text_betTime[index]:setVisible(true)
    self._AtlasLabel_time[index]:setVisible(true)
    self._Text_over[index]:setVisible(false)
end

function BjlGameDesk:resetEndState(index)
    self._Text_betTime[index]:setVisible(true)
    self._AtlasLabel_time[index]:setVisible(true)
    self._Text_over[index]:setVisible(false)
end

function BjlGameDesk:myInit(_roomInfo,_score)
    self._currentSelectedDesk = nil
	self._deskNO = 0
	self._pageLen = 0
	self._canCreate = true
	self._pageEven = false
	self._isTouch = true
    self._iGameCount = {}
    self._roomItemNode  = {}
    self._itemPanel = {}
    self._Text_betTime = {}
    self._AtlasLabel_time = {}
    self._Text_over = {}
    self._LoadingBar_time = {}
    self._leftRecordLayout = {}
    self._Text_zhuangNum = {}
    self._Text_xianNum = {}
    self._Text_heNum = {}
    self._Button_inter = {}
    self._Panel_inter = {}
    self._listViewCirRecord = {}
    self._listViewBallRecord = {}
    self._listViewLineRecord = {}
    self._listViewConRecord = {}
    self._img_cir_D = {}
    self._img_ball_D = {}
    self._img_line_D = {}
    self._img_cir_T = {}
    self._img_ball_T = {}
    self._img_line_T = {}
    self._curTrenLine = {}
	self._iTrendLine = {}
	self._circleLine = {}
	self._curCircleCol = {}
	self._ballLine = {}
	self._curBallCol = {}
	self._lineLine = {}
	self._curLineCol = {}
    self._lineArea = {}
    self._ballPanel = {}
    self._circlePanel = {}
    self._linePanel = {}
    self._arrResult = {}
	self._arrCircle = {}
	self._arrLine = {}
	self._arrBall = {}
    self._layerNode = {}
	self._ftime = {}
	self._itime = {}
    self._izhuangCount = {}
	self._ixianCount = {}
	self._iheCount = {}
    self._allDeskUI = {}
    self._roomLogic = nil
    self._roomList = _roomInfo
    self._score = _score
    self._deskCount = table.nums(self._roomList)

    local x1,x2 = math.modf(self._deskCount,6)

    self._pageEven = false
	self._pageLen = x1
    if(x2 == 0) then
        self._pageEven = false
    else
        self._pageEven = true
    end

    self._winSize = cc.Director:getInstance():getWinSize()

	self:createDeskList()
end

--[[
function BjlGameDesk:onExit()
    g_GameController:gameQuitRoomReq()
end
--]]

function BjlGameDesk:onReturn(sender, eventType)
    if(eventType == ccui.TouchEventType.ended)then
        g_GameController:reqUserLeftGameServer()
    end
end

function BjlGameDesk:createDeskList()
    local scalex =1/display.scaleX
	local scaley =1/display.scaleY
    self._node = UIAdapter:createNode(DESKUI_CSB)
    self._node:setAnchorPoint(cc.p(0.5,0.5))
    self._node:setPosition(self._winSize.width/2, self._winSize.height/2)
    self._node:setScale(display.scaleX,display.scaleY)
    self:addChild(self._node) 

    self._bjlRoomListView = self._node:getChildByName("listView_lvTableList")
    self._bjlRoomListView:setClippingEnabled(true)
    self._bjlRoomListView:setTouchEnabled(true)
    self._bjlRoomListView:setPropagateTouchEvents(true)
    self._bjlRoomListView:setSwallowTouches(true)
    self._bjlRoomListView:removeAllItems()

    self.btn_back = self._node:getChildByName("button_btnBack")
    self.btn_back:loadTextures("hall/image/zcm_button8.png")
    self.btn_back:addTouchEventListener(handler(self,self.onReturn))
    --self.btn_back:setLocalZOrder(1000)
    self.btn_back:setScaleX(1/display.scaleX)
    --self.btn_back:setVisible(false)

    local close_panel = self._node:getChildByName("panel_closepanel")
    close_panel:addTouchEventListener(handler(self,self.onReturn))
    close_panel:setTouchEnabled(true)
    close_panel:setSwallowTouches(true)

    local title_bg = self._node:getChildByName("Image_11")
    title_bg:setScaleX(1/display.scaleX)

    local num_money = self._score / 100
    local bg_image = self._node:getChildByName("Image_13")
    self._text_gold = bg_image:getChildByName("bmfont_myCoin")
    self._text_gold:setString(num_money)
    bg_image:setScaleX(1/display.scaleX)

	local roomCount = table.nums(self._roomList)
	
    self._roomItemNode = {}
    self._layerNode = {}
    local y1 = math.modf(self._deskCount/2)
    local y2 = math.fmod(self._deskCount,2)

    for i = 1,y1+y2 do
        self._layerNode[i] = ccui.Layout:create()
        self._layerNode[i]:setAnchorPoint(cc.p(0.5,0.5))
        if (self._winSize.width / self._winSize.height > 1.78) then 
		    self._layerNode[i]:setContentSize(cc.size(1440, 320))
        else
            self._layerNode[i]:setContentSize(cc.size(1280, 320))
        end
        self._layerNode[i]:setScaleX(1/display.scaleX);
        self._layerNode[i]:setTouchEnabled(true)
        self._layerNode[i]:setSwallowTouches(false)
        self._bjlRoomListView:pushBackCustomItem(self._layerNode[i])
    end

    local scaleN = 0.75
    if (self._winSize.width / self._winSize.height > 1.78) then 
        scaleN = 0.72
    else
        scaleN = 0.75
    end

	for i = 1,self._deskCount do
        local x1 = math.modf((i-1)/2) + 1
        local x2 = math.fmod((i-1),2)
        self._iGameCount[i] = table.nums(self._roomList[i].m_history)
		self._ftime[i] = self._roomList[i].m_leftTime
		self._itime[i] = self._roomList[i].m_leftTime
		self:creatBjlRoomItem(self._roomList[i], i)
		if (self._roomItemNode[i]) then
            print("layernode",x1)
			self._layerNode[x1]:addChild(self._roomItemNode[i], 2)
			
            if x2 == 1 then
                self._roomItemNode[i]:setPosition(cc.p(self._layerNode[x1]:getContentSize().width * scaleN, self._layerNode[x1]:getContentSize().height / 2))
            else
                self._roomItemNode[i]:setPosition(cc.p(self._layerNode[x1]:getContentSize().width * (1-scaleN), self._layerNode[x1]:getContentSize().height / 2))
            end
		end
	end

    for i = 1,self._deskCount do
        self:enterRoomEventCallBack(i)
		self:initTimeCountText(self._roomList[i].m_leftTime, i)
    end
	
	self:clearData()

    self._timeText = scheduler.scheduleGlobal(handler(self,self.setTimeCountText), 0.95)
    self._timeProgress = scheduler.scheduleGlobal(handler(self,self.setTimeCountProgress), 0.01)

    --ToolKit:removeLoadingDialog()
end

function BjlGameDesk:removeFromParent()
    self:close()
end

function BjlGameDesk:setTimeCountProgress(time)
    if self._deskCount == nil then
        return
    end
    for i=1,self._deskCount do
        self._ftime[i] = self._ftime[i] - 0.035
	    local processNumber = (self._ftime[i] * 100) / 20
	    if (self._ftime[i] <= 0) then
		    self._ftime[i] = 0
		    self._LoadingBar_time[i]:setVisible(false)
	    else
		    self._LoadingBar_time[i]:setPercent(processNumber)
		    self._LoadingBar_time[i]:setVisible(true)
	    end
    end
end

function BjlGameDesk:creatBjlRoomItem(roomInfo,index)
    self._roomItemNode[index] = UIAdapter:createNode(DESK_ITEM_UI)

    self._itemPanel[index] = self._roomItemNode[index]:getChildByName("image_imgBg")
	self._itemPanel[index]:setTouchEnabled(false)
	local panelTop = self._itemPanel[index]:getChildByName("Panel_top")
	local Text_RoomNum = panelTop:getChildByName("Image_2"):getChildByName("atlas_txtTableOrder")
	Text_RoomNum:setString(roomInfo.m_id)

	self._Text_betTime[index] = panelTop:getChildByName("image_imgBetting")
	self._Text_betTime[index]:setVisible(false)
	self._AtlasLabel_time[index] = panelTop:getChildByName("image_imgBetting"):getChildByName("atlas_txtBetCountdown")
	self._AtlasLabel_time[index]:setString(roomInfo.m_leftTime)
	self._AtlasLabel_time[index]:setVisible(false)
	self._Text_over[index] = panelTop:getChildByName("image_imgBilling")
	self._Text_over[index]:setVisible(false)

	local Image_middle = self._itemPanel[index]:getChildByName("image_proBetBg")
	self._LoadingBar_time[index] = Image_middle:getChildByName("loadingBar_proBet")
	self._LoadingBar_time[index]:setVisible(false)

	local leftList = ccui.ListView:create()
    leftList:setContentSize(248, 210)
    leftList:setAnchorPoint(cc.p(0,0))
    leftList:setPosition(cc.p(8,50))
	leftList:setClippingEnabled(true)
	leftList:setTouchEnabled(true)
    leftList:setSwallowTouches(false)
    leftList:setDirection(2)
    self._itemPanel[index]:addChild(leftList)
	self._leftRecordLayout[index] = ccui.Layout:create()
	self._leftRecordLayout[index]:setContentSize(cc.size(372,210))
	leftList:pushBackCustomItem(self._leftRecordLayout[index])
	local Image_bottom = self._itemPanel[index]:getChildByName("panel_bottom")
	self._Text_zhuangNum[index] = Image_bottom:getChildByName("Image_4"):getChildByName("atlas_txtDealerNum")
	self._Text_xianNum[index] = Image_bottom:getChildByName("Image_4_0"):getChildByName("atlas_txtPlayerNum")
	self._Text_heNum[index] = Image_bottom:getChildByName("Image_4_0_0"):getChildByName("atlas_txtDrawNum")
	self._Button_inter[index] = Image_bottom:getChildByName("button_btnEnterRoom")
	self._Panel_inter[index] = self._itemPanel[index]
	self._Panel_inter[index]:setTouchEnabled(true)
    self._Panel_inter[index]:setSwallowTouches(false)

	self._listViewCirRecord[index] = ccui.ListView:create()
    self._listViewCirRecord[index]:setContentSize(178, 50)
	self._listViewCirRecord[index]:removeAllItems()
	self._listViewCirRecord[index]:setTouchEnabled(true)
    self._listViewCirRecord[index]:setSwallowTouches(false)
	self._listViewCirRecord[index]:setPosition(cc.p(260, 110))
    self._listViewCirRecord[index]:setDirection(2)
    self._itemPanel[index]:addChild(self._listViewCirRecord[index])

	self._listViewBallRecord[index] = ccui.ListView:create()
    self._listViewBallRecord[index]:setContentSize(178, 50)
	self._listViewBallRecord[index]:removeAllItems() 
	self._listViewBallRecord[index]:setTouchEnabled(true)
    self._listViewBallRecord[index]:setSwallowTouches(false)
	self._listViewBallRecord[index]:setPosition(cc.p(438.5, 110))
    self._listViewBallRecord[index]:setDirection(2)
    self._itemPanel[index]:addChild(self._listViewBallRecord[index])

	self._listViewLineRecord[index] = ccui.ListView:create()
    self._listViewLineRecord[index]:setContentSize(178, 50)
	self._listViewLineRecord[index]:removeAllItems()
	self._listViewLineRecord[index]:setTouchEnabled(true)
    self._listViewLineRecord[index]:setSwallowTouches(false)
	self._listViewLineRecord[index]:setPosition(cc.p(260,63))
    self._listViewLineRecord[index]:setDirection(2)
    self._itemPanel[index]:addChild(self._listViewLineRecord[index])

	self._listViewConRecord[index] = ccui.ListView:create()
    self._listViewConRecord[index]:setContentSize(356, 100)
	self._listViewConRecord[index]:removeAllItems()
	self._listViewConRecord[index]:setTouchEnabled(true)
    self._listViewConRecord[index]:setSwallowTouches(false)
	self._listViewConRecord[index]:setPosition(cc.p(260, 152))
    self._listViewConRecord[index]:setDirection(2)
    self._itemPanel[index]:addChild(self._listViewConRecord[index])

	self._img_cir_D[index] = self._itemPanel[index]:getChildByName("Panel_panelZhuangWenLu"):getChildByName("image_t1WL_1")
    self._img_cir_D[index]:setVisible(true)
	self._img_ball_D[index] = self._itemPanel[index]:getChildByName("Panel_panelZhuangWenLu"):getChildByName("image_t1WL_2")
    self._img_ball_D[index]:setVisible(true)
	self._img_line_D[index] = self._itemPanel[index]:getChildByName("Panel_panelZhuangWenLu"):getChildByName("image_t1WL_3")
    self._img_line_D[index]:setVisible(true)

	self._img_cir_T[index] = self._itemPanel[index]:getChildByName("Panel_panelXianWenLu"):getChildByName("image_t2WL_1")
    self._img_cir_T[index]:setVisible(true)
	self._img_ball_T[index] = self._itemPanel[index]:getChildByName("Panel_panelXianWenLu"):getChildByName("image_t2WL_2")
    self._img_ball_T[index]:setVisible(true)
	self._img_line_T[index] = self._itemPanel[index]:getChildByName("Panel_panelXianWenLu"):getChildByName("image_t2WL_3")
    self._img_line_T[index]:setVisible(true)

	self._curTrenLine[index] = 0
	self._iTrendLine[index] = 0

	self._circleLine[index] = 0
	self._curCircleCol[index] = 0

	self._ballLine[index] = 0
	self._curBallCol[index] = 0

	self._lineLine[index] = 0
	self._curLineCol[index] = 0

    self._lineArea[index] = {}
	for i = 0,31 do
		self._lineArea[index][i] = ccui.Layout:create()
		self._lineArea[index][i]:setAnchorPoint(cc.p(0.5,1))
		self._lineArea[index][i]:setBackGroundImage("app/game/bjl/res/Baccarat/image/room/dl_bg.png")
		self._lineArea[index][i]:setContentSize(cc.size(18, 102))
        self._lineArea[index][i]:setScaleY(0.88)
		self._listViewConRecord[index]:pushBackCustomItem(self._lineArea[index][i])
		self._curTrenLine[index] = self._curTrenLine[index] + 1
	end

    self._circlePanel[index] = {}
    self._linePanel[index] = {}
	for i = 0,15 do
		self._circlePanel[index][i] = ccui.Layout:create()
		self._linePanel[index][i] = ccui.Layout:create()

		self._circlePanel[index][i]:setAnchorPoint(cc.p(0.5,1))
		self._linePanel[index][i]:setAnchorPoint(cc.p(0.5,1))

		self._circlePanel[index][i]:setBackGroundImage("app/game/bjl/res/Baccarat/image/room/xsl_bg.png")
		self._linePanel[index][i]:setBackGroundImage("app/game/bjl/res/Baccarat/image/room/xsl_bg.png")

		self._circlePanel[index][i]:setContentSize(cc.size(18, 50))
		self._linePanel[index][i]:setContentSize(cc.size(18, 50))

        self._circlePanel[index][i]:setScaleY(0.88)
        self._linePanel[index][i]:setScaleY(0.88)
		self._listViewCirRecord[index]:pushBackCustomItem(self._circlePanel[index][i])
		self._listViewLineRecord[index]:pushBackCustomItem(self._linePanel[index][i])

		self._curCircleCol[index] = self._curCircleCol[index]+1

		self._curLineCol[index] = self._curLineCol[index]+1
	end

    self._ballPanel[index] = {}
	for i = 0,15 do
		self._ballPanel[index][i] = ccui.Layout:create()
		self._ballPanel[index][i]:setAnchorPoint(cc.p(0.5,1))
		self._ballPanel[index][i]:setBackGroundImage("app/game/bjl/res/Baccarat/image/room/xsl_bg.png")
		self._ballPanel[index][i]:setContentSize(cc.size(18, 50))
        self._ballPanel[index][i]:setScaleY(0.88)
		self._listViewBallRecord[index]:pushBackCustomItem(self._ballPanel[index][i])
		self._curBallCol[index] = self._curBallCol[index]+1
	end

	

	self:updateGameCount(roomInfo.m_history,index)
	--[[self.initConTrend(pages.iResultInfo, pages.igamecount - 1, index)
	self.initBall(pages.iyuceinfo1, pages.igamecount - 1, index)
	self.initCircle(pages.iyuceinfo, pages.igamecount - 1, index)
	self.initLine(pages.iyuceinfo2, pages.igamecount - 1, index)]]--
     
	self:initConTrend(roomInfo.m_history, self._iGameCount[index], index)
    self:initCircle(roomInfo.m_dyzlu, index)
	self:initBall(roomInfo.m_xlu, index)
	self:initLine(roomInfo.m_xqlu, index)
    self:falshTrendImg(self._iGameCount[i], index)
end

function BjlGameDesk:updateGameCount(data, index)
    if #data == 0 then
        self._leftRecordLayout[index]:removeAllChildren()
        return
    end
	local iResultInfo = {}
    for i = 1,72 do
        iResultInfo[i] = {}
        for j = 1,5 do
            iResultInfo[i][j] = 0
        end
    end

	local izhuang = 0
	local ixian = 0
	local ihe = 0
	local gamecount = 0

    local leftSpVec = {}
	--vector<Sprite*>leftSpVec
	--leftSpVec.clear()

	gamecount = self._iGameCount[index] 
	if (gamecount > 48) then
        local x1 = math.modf(gamecount/6)
        local x2 = math.fmod(gamecount,6)
		local x = x1 + 1
		local xSize = 31 * x
		self._leftRecordLayout[index]:setContentSize(cc.size(xSize, 210))
	end

	for i = 1,gamecount do
        if data[i].m_bankerVal > data[i].m_xianVal then
            iResultInfo[i][1] = 1
        elseif data[i].m_xianVal > data[i].m_bankerVal then
            iResultInfo[i][2] = 1
        else
            iResultInfo[i][3] = 1
        end

        if data[i].m_bankerPair == 1 then
            iResultInfo[i][4] = 1
        end

        if data[i].m_xianPair == 1 then
            iResultInfo[i][5] = 1
        end
	end

	for i = 0,gamecount-1 do
		local leftSp = cc.Sprite:create(WIN_ZHUANG)
		local duiSp = cc.Sprite:create(WIN_ZHUANGDUI)

		if (1 == iResultInfo[i+1][4]) then
			duiSp:setTexture(WIN_ZHUANGDUI)
		elseif (1 == iResultInfo[i+1][5]) then
			duiSp:setTexture(WIN_XIANDUI)
		else
			duiSp:setVisible(false)
		end

		if (1 == iResultInfo[i+1][1]) then
			leftSp:setTexture(WIN_ZHUANG)
			leftSp:setTag(0)
		elseif (1 == iResultInfo[i+1][2]) then
			leftSp:setTexture(WIN_XIAN)
			leftSp:setTag(1)
		elseif (1 == iResultInfo[i+1][3]) then
			leftSp:setTexture(WIN_HE)
			leftSp:setTag(2)
		end

		duiSp:setPosition(cc.p(29.5, 3.5))
        local y1 = math.modf(i/6)
        local y2 = math.fmod(i,6)
		leftSp:setPosition(cc.p(18 + (y1 * 31), 188 - (y2 * 31)))
		leftSp:setName("leftSp")
        leftSp:setScale(0.92)
		self._leftRecordLayout[index]:addChild(leftSp, 2)
		leftSp:addChild(duiSp)
		leftSpVec[i] = leftSp
    end

	for i = 0,table.nums(leftSpVec)-1 do
        local vTag = leftSpVec[i]:getTag()
		if (vTag == 0) then
			izhuang = izhuang + 1
		elseif (vTag == 1) then
			ixian = ixian + 1
		elseif (vTag == 2) then
			ihe = ihe + 1
		else
			
		end
	end

	self._izhuangCount[index] = izhuang
	self._ixianCount[index] = ixian
	self._iheCount[index] = ihe
	self._Text_zhuangNum[index]:setString(izhuang)
	self._Text_xianNum[index]:setString(ixian)
	self._Text_heNum[index]:setString(ihe)
end

function BjlGameDesk:refreshGameCount(data, index)
    self._leftRecordLayout[index]:removeAllChildren()
    if #data == 0 then
        return
    end
	local iResultInfo = {}
    for i = 1,72 do
        iResultInfo[i] = {}
        for j = 1,5 do
            iResultInfo[i][j] = 0
        end
    end

	local izhuang = 0
	local ixian = 0
	local ihe = 0
	local gamecount = 0

    local leftSpVec = {}
	--vector<Sprite*>leftSpVec
	--leftSpVec.clear()

	gamecount = self._iGameCount[index] 
	if (gamecount > 48) then
        local x1 = math.modf(gamecount/6)
        local x2 = math.fmod(gamecount,6)
		local x = x1 + 1
		local xSize = 31 * x
		self._leftRecordLayout[index]:setContentSize(cc.size(xSize, 210))
	end

	for i = 1,gamecount do
        if data[i].m_bankerVal > data[i].m_xianVal then
            iResultInfo[i][1] = 1
        elseif data[i].m_xianVal > data[i].m_bankerVal then
            iResultInfo[i][2] = 1
        else
            iResultInfo[i][3] = 1
        end

        if data[i].m_bankerPair == 1 then
            iResultInfo[i][4] = 1
        end

        if data[i].m_xianPair == 1 then
            iResultInfo[i][5] = 1
        end
	end

	for i = 0,gamecount-1 do
		local leftSp = cc.Sprite:create(WIN_ZHUANG)
		local duiSp = cc.Sprite:create(WIN_ZHUANGDUI)

		if (1 == iResultInfo[i+1][4]) then
			duiSp:setTexture(WIN_ZHUANGDUI)
		elseif (1 == iResultInfo[i+1][5]) then
			duiSp:setTexture(WIN_XIANDUI)
		else
			duiSp:setVisible(false)
		end

		if (1 == iResultInfo[i+1][1]) then
			leftSp:setTexture(WIN_ZHUANG)
			leftSp:setTag(0)
		elseif (1 == iResultInfo[i+1][2]) then
			leftSp:setTexture(WIN_XIAN)
			leftSp:setTag(1)
		elseif (1 == iResultInfo[i+1][3]) then
			leftSp:setTexture(WIN_HE)
			leftSp:setTag(2)
		end

		duiSp:setPosition(cc.p(29.5, 3.5))
        local y1 = math.modf(i/6)
        local y2 = math.fmod(i,6)
		leftSp:setPosition(cc.p(18 + (y1 * 31), 188 - (y2 * 31)))
		leftSp:setName("leftSp")
		leftSp:addChild(duiSp)
		leftSpVec[i] = leftSp
        leftSp:setScale(0.92)
        self._leftRecordLayout[index]:addChild(leftSp, 2)
        if (i == gamecount - 1) then
            leftSp:runAction(cc.Blink:create(2,3))
        end
    end

	for i = 0,table.nums(leftSpVec)-1 do
        local vTag = leftSpVec[i]:getTag()
		if (vTag == 0) then
			izhuang = izhuang + 1
		elseif (vTag == 1) then
			ixian = ixian + 1
		elseif (vTag == 2) then
			ihe = ihe + 1
		else
			
		end
	end

	self._izhuangCount[index] = izhuang
	self._ixianCount[index] = ixian
	self._iheCount[index] = ihe
	self._Text_zhuangNum[index]:setString(izhuang)
	self._Text_xianNum[index]:setString(ixian)
	self._Text_heNum[index]:setString(ihe)
end

--点击进入房间消息发送
function BjlGameDesk:enterRoomEventCallBack(index)
    local function pOnTouchFunc2(sender,eventType)
        if sender then
             if eventType == ccui.TouchEventType.moved then
               self._isTouch =false
             
           elseif eventType == ccui.TouchEventType.began then
                self._isTouch =true
            else
                if self._isTouch then
                    ToolKit:addLoadingDialog(3,"正在进入游戏，请稍等......")
                    ConnectManager:send2GameServer( 161001,"CS_C2G_Baccarat_EnterRoom_Req", { self._roomList[index].m_id })
                end
            end
        end
    end  
    local function pOnTouch(sender,eventType)
        if eventType == ccui.TouchEventType.ended then
            ToolKit:addLoadingDialog(3,"正在进入游戏，请稍等......")
            ConnectManager:send2GameServer( 161001,"CS_C2G_Baccarat_EnterRoom_Req", { self._roomList[index].m_id })
        end
    end

    self._Button_inter[index]:addTouchEventListener(pOnTouch)
    self._itemPanel[index]:addTouchEventListener(pOnTouchFunc2)
    self._Panel_inter[index]:addTouchEventListener(pOnTouchFunc2)
end

function BjlGameDesk:initTimeCountText(time,index)
	if (time <= 0) then
		self._Text_betTime[index]:setVisible(true)
		self._AtlasLabel_time[index]:setVisible(false)
		self._Text_over[index]:setVisible(false)
	else
		self._Text_over[index]:setVisible(false)
		self._Text_betTime[index]:setVisible(true)
		self._AtlasLabel_time[index]:setVisible(true)
	end
end

function BjlGameDesk:showWinkAction(winArea,index)
	local cWinArea = {}
    for i = 1,5 do
        cWinArea[i] = winArea[i]
    end

	local gamecount = 0
	if (1 >= self._iGameCount[index]) then
		self._leftRecordLayout[index]:removeAllChildren()
	end
	gamecount = self._iGameCount[index] 
	if (gamecount > 48) then
        local x1,x2 = math.modf(gamecount,6)
		local x = x1 + 1
		local xSize = 36 * x
		self._leftRecordLayout[index]:setContentSize(cc.size(xSize, 216))
	end
	local leftSp = cc.Sprite:create(WIN_ZHUANG)
	local duiSp = cc.Sprite:create(WIN_ZHUANGDUI)
	if (1 == cWinArea[4]) then
		duiSp:setTexture(WIN_ZHUANGDUI)
	elseif (1 == cWinArea[5]) then
		duiSp:setTexture(WIN_XIANDUI)
	else
		duiSp:setVisible(false)
	end
	if (1 == cWinArea[1]) then
		leftSp:setTexture(WIN_ZHUANG)
		self._izhuangCount[index] = self._izhuangCount[index] + 1
	elseif (1 == cWinArea[2]) then
		leftSp:setTexture(WIN_XIAN)
		self._ixianCount[index] = self._ixianCount[index] + 1
	elseif (1 == cWinArea[3]) then
		leftSp:setTexture(WIN_HE)
		self._iheCount[index] = self._iheCount[index] + 1
    else

	end
	duiSp:setPosition(cc.p(30, 5))
    local x3,x4 = math.modf(gamecount-1,6)
	leftSp:setPosition(cc.p((18 + (x3 * 36)), (198 - (x4 * 36))))
	self._leftRecordLayout[index]:addChild(leftSp, 2)
	leftSp:addChild(duiSp)
	local act1 = cc.FadeIn:create(0.8)
	local act2 = cc.FadeOut:create(0.8)
	if (leftSp) then
		leftSp:setOpacity(255)
		leftSp:runAction(cc.Sequence:create(act2, act1, nil))
	end
	self._Text_zhuangNum[index]:setString(self._izhuangCount[index])
	self._Text_xianNum[index]:setString(self._ixianCount[index])
	self._Text_heNum[index]:setString(self._iheCount[index])
end

function BjlGameDesk:setBjlRoomResult(dt)
	--memcpy(bjlInfo, RoomLogic():bjlinfo.bjlinfo, sizeof(bjlInfo))
	for i = 1,3 do
		--[[self.bjlInfo[i] = RoomLogic():bjlinfo.bjlinfo[i]
		if (bjlInfo[i].igamecount <= 1)
		{
			clearDesk(i)
		}


		for (int j = 0 j < bjlInfo[i].igamecount j++)
		{
			_arrLine[i][j] = bjlInfo[i].iyuceinfo2[j]
			_arrCircle[i][j] = bjlInfo[i].iyuceinfo[j]
			_arrBall[i][j] = bjlInfo[i].iyuceinfo1[j]
		}
		
		if (bjlInfo[i].iResultInfo[_iGameCount[i]]--[[[0] != 0)
		{
			_iGameCount[i]++
			int gamecount = 0
			for (int j = 0 j < 72 j++)
			{
				if (bjlInfo[i].iResultInfo[j][0] != 0)
				{
					gamecount++
				}
			}
			if (_iGameCount[i] > gamecount)
			{
				_iGameCount[i] = 0
			}
			_ftime[i] = 20
			_itime[i] = 20
			showWinkAction(bjlInfo[i].iResultInfo[_iGameCount[i] -1], i)
			/*showTrend(bjlInfo[i].iResultInfo, bjlInfo[i].igamecount - 1, i)
			showBall(bjlInfo[i].igamecount - 1, i)
			showCircle(bjlInfo[i].igamecount - 1, i)
			showLine(bjlInfo[i].igamecount - 1, i)
			falshTrendImg(bjlInfo[i].igamecount - 1, i)*/
			showTrend(bjlInfo[i].iResultInfo, _iGameCount[i], i)
			showBall(_iGameCount[i] - 1, i)
			showCircle(_iGameCount[i] - 1, i)
			showLine(_iGameCount[i] - 1, i)
			falshTrendImg(_iGameCount[i] - 1, i)
			switch (i)
			{
			case 0:
				unschedule(schedule_selector(GameDesk::setTimeCountText0))
				unschedule(schedule_selector(GameDesk::setTimeCountProgress0))

				_Text_over[0]:setVisible(false)
				_Text_betTime[0]:setVisible(true)
				_AtlasLabel_time[0]:setVisible(true)
				_AtlasLabel_time[0]:setString(StringUtils::format("%d", _itime[0]))

				schedule(schedule_selector(GameDesk::setTimeCountText0), 1.0f)
				schedule(schedule_selector(GameDesk::setTimeCountProgress0), 0.01)
				break
			case 1:
				unschedule(schedule_selector(GameDesk::setTimeCountText1))
				unschedule(schedule_selector(GameDesk::setTimeCountProgress1))

				_Text_over[1]:setVisible(false)
				_Text_betTime[1]:setVisible(true)
				_AtlasLabel_time[1]:setVisible(true)
				_AtlasLabel_time[1]:setString(StringUtils::format("%d", _itime[1]))

				schedule(schedule_selector(GameDesk::setTimeCountText1), 1.0f)
				schedule(schedule_selector(GameDesk::setTimeCountProgress1), 0.01)
				break
			case 2:
				unschedule(schedule_selector(GameDesk::setTimeCountText2))
				unschedule(schedule_selector(GameDesk::setTimeCountProgress2))

				_Text_over[2]:setVisible(false)
				_Text_betTime[2]:setVisible(true)
				_AtlasLabel_time[2]:setVisible(true)
				_AtlasLabel_time[2]:setString(StringUtils::format("%d", _itime[2]))
		
				schedule(schedule_selector(GameDesk::setTimeCountText2), 1.0f)
				schedule(schedule_selector(GameDesk::setTimeCountProgress2), 0.01)
				break
			default:
				break
			}
		end]]--
	end
end

function BjlGameDesk:setTimeCountText(time)
    if self._deskCount == nil then
        return
    end
    for i = 1,self._deskCount do
        self._itime[i] = self._itime[i] - 1
        if (self._itime[i] <= 0) then
            self._itime[i] = 0
		    self._Text_betTime[i]:setVisible(false)
		    self._AtlasLabel_time[i]:setVisible(false)
            self._Text_over[i]:setVisible(true)
	    else
	        self._Text_betTime[i]:setVisible(true)
	        self._AtlasLabel_time[i]:setVisible(true)
            self._Text_over[i]:setVisible(false)
	        self._AtlasLabel_time[i]:setString(self._itime[i])
	    end
    end
end


function BjlGameDesk:showTrend(arrData, gameCount, index)
    for k,v in pairs(self._lineArea[index]) do
        v:removeAllChildren()
    end
    if gameCount == 0 then
        return
    end
	local equal_count = 0
	self._iTrendLine[index] = 0
    self._arrResult[index] = {}
    if table.nums(arrData)==0 or arrData==nil then
        return
    end
	--存储所有游戏结果  0: 和  1:龙  2:虎
    local index2 = 0
	for i = 0,gameCount-1 do
		if arrData[i+1].m_xianVal>arrData[i+1].m_bankerVal then
            self._arrResult[index][i-index2] = 1
        elseif  arrData[i+1].m_xianVal<arrData[i+1].m_bankerVal then
            self._arrResult[index][i-index2] = 0
        else
            index2 = index2+1
            if self._arrResult[index][i-index2] then
                self._arrResult[index][i-index2] = self._arrResult[index][i-index2]+10
            end
        end
	end
	for i = 1,table.nums(self._arrResult[index])-1 do
		if (self._arrResult[index][i]%10 ~= self._arrResult[index][i-1]%10) then
			self._iTrendLine[index] = self._iTrendLine[index]+1
		end
	end
	--判断是否需要添加列
	if (self._iTrendLine[index] + 1 > self._curTrenLine[index]) then
		for i = self._curTrenLine[index],self._iTrendLine[index]+1 do
			self._lineArea[index][i] = ccui.Layout:create()
			self._lineArea[index][i]:setAnchorPoint(cc.p(0.5,1))
			self._lineArea[index][i]:setBackGroundImage("app/game/bjl/res/Baccarat/image/room/dl_bg.png")
			self._lineArea[index][i]:setContentSize(cc.size(18, 102))
            self._lineArea[index][i]:setScaleY(0.88)
			self._listViewConRecord[index]:pushBackCustomItem(self._lineArea[index][i])
			self._curTrenLine[index] = self._curTrenLine[index] + 1
		end
	end
    
        local fir = display.newSprite()
        if self._arrResult[index][0]== nil then
            return
        end
	    if (self._arrResult[index][0] == 0) then
		    fir = display.newSprite("app/game/bjl/res/Baccarat/image/room/corner_circle_blue.png")
	    elseif (self._arrResult[index][0] == 1) then
		    fir = display.newSprite("app/game/bjl/res/Baccarat/image/room/corner_circle_red.png")
        else
            local num = math.floor(self._arrResult[index][0]/10)
            local result = self._arrResult[index][0]%10
            if result ==0 then
                fir = display.newSprite("app/game/bjl/res/Baccarat/image/room/corner_circle_blue.png");
            elseif result == 1 then
                fir = display.newSprite("app/game/bjl/res/Baccarat/image/room/corner_circle_red.png"); 
            end
            local text = ccui.TextAtlas:create(tostring(num),"Games/NewBjle/recordUi/recordUiRes/record/num_green.png",13,18,"0") 
            fir:addChild(text)
		    text:setPosition(fir:getContentSize().width/2,fir:getContentSize().height/2)
            text:setScale(0.5)
	    end

	    if (fir) then
		    if (table.nums(self._arrResult[index]) > 0 and self._lineArea[index][0]) then
			    self._lineArea[index][0]:addChild(fir)
			    fir:setPosition(cc.p(8, 95))
		    end
	    end
	

	local lineIndex = 1

	local index1 = 0
	local isInd = true
	for i = 1,table.nums(self._arrResult[index])-1 do
		if (i < table.nums(self._arrResult[index])) then
			if (self._arrResult[index][i]%10 ~= self._arrResult[index][i-1]%10) then
				lineIndex = lineIndex + 1
			end

			if (self._arrResult[index][1]%10 == self._arrResult[index][0]%10) and isInd then
				index1 = index1 + 1
				isInd = false
			end
			if (i > 1) then
				if (self._arrResult[index][i]%10 == self._arrResult[index][i-1]%10) then
					index1 = index1 + 1
				else
					index1 = 0
				end
			end

                if (i < table.nums(self._arrResult[index])) then
				    local Record = display.newSprite()
				    if (self._arrResult[index][i] == 0) then
					    Record = display.newSprite("app/game/bjl/res/Baccarat/image/room/corner_circle_blue.png")
				    elseif (self._arrResult[index][i] == 1) then
					    Record = display.newSprite("app/game/bjl/res/Baccarat/image/room/corner_circle_red.png")
				    else
					    local num = math.floor(self._arrResult[index][i]/10)
                        local result = self._arrResult[index][i]%10
                        if result ==0 then
                            Record = display.newSprite("app/game/bjl/res/Baccarat/image/room/corner_circle_blue.png");
                        elseif result == 1 then
                            Record = display.newSprite("app/game/bjl/res/Baccarat/image/room/corner_circle_red.png"); 
                        end
                        local text = ccui.TextAtlas:create(tostring(num),"Games/NewBjle/recordUi/recordUiRes/record/num_green.png",13,18,"0") 
                        Record:addChild(text)
				        text:setPosition(Record:getContentSize().width/2,Record:getContentSize().height/2)
                        text:setScale(0.5)
				    end
				    if (Record) then
					    if 95 -18 * index1 >0 then
                            if (lineIndex == 0) then
						        if (self._lineArea[index][0]) then
							        self._lineArea[index][0]:addChild(Record)
						        end
					        else
						        if (self._lineArea[index][lineIndex - 1]) then
							        self._lineArea[index][lineIndex - 1]:addChild(Record)
						        end
					        end
					        Record:setPosition(cc.p(8, 95 - 18 * index1))
					    else
                            if (lineIndex+index1-6)<32 then
                                self._lineArea[index][lineIndex+index1-6]:addChild(Record);
                                Record:setPosition(8, 5);
                            else
                                self._lineArea[index][lineIndex+index1-6] = ccui.Layout:create()
		                        self._lineArea[index][lineIndex+index1-6]:setAnchorPoint(cc.p(0.5,1))
		                        self._lineArea[index][lineIndex+index1-6]:setBackGroundImage("app/game/bjl/res/Baccarat/image/room/dl_bg.png")
		                        self._lineArea[index][lineIndex+index1-6]:setContentSize(cc.size(18, 108))

		                        self._listViewConRecord[index]:pushBackCustomItem(self._lineArea[index][lineIndex+index1-6])
		                        self._curTrenLine[index] = self._curTrenLine[index] + 1

                                self._lineArea[index][lineIndex+index1-6]:addChild(Record);
                                Record:setPosition(8, 5);
                            end
                            
                        end
                        if i==gameCount-1 then
                            Record:runAction(cc.Blink:create(2,3))
                        end
                        
				    end
			    end
		end
	end
end

function BjlGameDesk:initConTrend(arrData,gameCount,index)
    dump(arrData, "history=")
	local equal_count = 0
	self._iTrendLine[index] = 0
    self._arrResult[index] = {}
    if table.nums(arrData)==0 or arrData==nil then
        return
    end
	--存储所有游戏结果  0: 和  1:龙  2:虎
    local index2 = 0
	for i = 0,gameCount-1 do
		if arrData[i+1].m_xianVal>arrData[i+1].m_bankerVal then
            self._arrResult[index][i-index2] = 1
        elseif  arrData[i+1].m_xianVal<arrData[i+1].m_bankerVal then
            self._arrResult[index][i-index2] = 0
        else
            index2 = index2+1
            if self._arrResult[index][i-index2] then
                self._arrResult[index][i-index2] = self._arrResult[index][i-index2]+10
            end
        end
	end
	for i = 1,table.nums(self._arrResult[index])-1 do
		if (self._arrResult[index][i]%10 ~= self._arrResult[index][i-1]%10) then
			self._iTrendLine[index] = self._iTrendLine[index]+1
		end
	end
	--判断是否需要添加列
	if (self._iTrendLine[index] + 1 > self._curTrenLine[index]) then
		for i = self._curTrenLine[index],self._iTrendLine[index]+1 do
			self._lineArea[index][i] = ccui.Layout:create()
			self._lineArea[index][i]:setAnchorPoint(cc.p(0.5,1))
			self._lineArea[index][i]:setBackGroundImage("app/game/bjl/res/Baccarat/image/room/dl_bg.png")
			self._lineArea[index][i]:setContentSize(cc.size(18, 102))
            self._lineArea[index][i]:setScaleY(0.88)
			self._listViewConRecord[index]:pushBackCustomItem(self._lineArea[index][i])
			self._curTrenLine[index] = self._curTrenLine[index] + 1
		end
	end
	local fir = display.newSprite()
    if self._arrResult[index][0]== nil then
        return
    end
	if (self._arrResult[index][0] == 0) then
		fir = display.newSprite("app/game/bjl/res/Baccarat/image/room/corner_circle_blue.png")
	elseif (self._arrResult[index][0] == 1) then
		fir = display.newSprite("app/game/bjl/res/Baccarat/image/room/corner_circle_red.png")
    else
        local num = math.floor(self._arrResult[index][0]/10)
        local result = self._arrResult[index][0]%10
        if result ==0 then
            fir = display.newSprite("app/game/bjl/res/Baccarat/image/room/corner_circle_blue.png");
        elseif result == 1 then
            fir = display.newSprite("app/game/bjl/res/Baccarat/image/room/corner_circle_red.png"); 
        end
        local text = ccui.TextAtlas:create(tostring(num),"Games/NewBjle/recordUi/recordUiRes/record/num_green.png",13,18,"0") 
        fir:addChild(text)
		text:setPosition(fir:getContentSize().width/2,fir:getContentSize().height/2)
        text:setScale(0.5)
	end

	if (fir) then
		if (table.nums(self._arrResult[index]) > 0 and self._lineArea[index][0]) then
			self._lineArea[index][0]:addChild(fir)
			fir:setPosition(cc.p(8, 95))
		end
	end

	local lineIndex = 1

	local index1 = 0
	local isInd = true
	for i = 1,table.nums(self._arrResult[index])-1 do
		if (i < table.nums(self._arrResult[index])) then
			if (self._arrResult[index][i]%10 ~= self._arrResult[index][i-1]%10) then
				lineIndex = lineIndex + 1
			end

			if (self._arrResult[index][1]%10 == self._arrResult[index][0]%10) and isInd then
				index1 = index1 + 1
				isInd = false
			end
			if (i > 1) then
				if (self._arrResult[index][i]%10 == self._arrResult[index][i-1]%10) then
					index1 = index1 + 1
				else
					index1 = 0
				end
			end


			if (i < table.nums(self._arrResult[index])) then
				local Record = display.newSprite()
				if (self._arrResult[index][i] == 0) then
					Record = display.newSprite("app/game/bjl/res/Baccarat/image/room/corner_circle_blue.png")
				elseif (self._arrResult[index][i] == 1) then
					Record = display.newSprite("app/game/bjl/res/Baccarat/image/room/corner_circle_red.png")
				else
                    local num = math.floor(self._arrResult[index][i]/10)
                    local result = self._arrResult[index][i]%10
                    if result ==0 then
                        Record = display.newSprite("app/game/bjl/res/Baccarat/image/room/corner_circle_blue.png");
                    elseif result == 1 then
                        Record = display.newSprite("app/game/bjl/res/Baccarat/image/room/corner_circle_red.png"); 
                    end
                    local text = ccui.TextAtlas:create(tostring(num),"Games/NewBjle/recordUi/recordUiRes/record/num_green.png",13,18,"0") 
                    Record:addChild(text)
				    text:setPosition(Record:getContentSize().width/2,Record:getContentSize().height/2)
                    text:setScale(0.5)
				end
				if (Record) then
                    if 95 -18 * index1 >0 then
                        if (lineIndex == 0) then
						    if (self._lineArea[index][0]) then
							    self._lineArea[index][0]:addChild(Record)
						    end
					    else
						    if (self._lineArea[index][lineIndex - 1]) then
							    self._lineArea[index][lineIndex - 1]:addChild(Record)
						    end
					    end
					    Record:setPosition(cc.p(8, 95 - 18 * index1))
					else
                        if (lineIndex+index1-6)<32 then
                            self._lineArea[index][lineIndex+index1-6]:addChild(Record);
                            Record:setPosition(8, 5);
                        else
                            self._lineArea[index][lineIndex+index1-6] = ccui.Layout:create()
		                    self._lineArea[index][lineIndex+index1-6]:setAnchorPoint(cc.p(0.5,1))
		                    self._lineArea[index][lineIndex+index1-6]:setBackGroundImage("app/game/bjl/res/Baccarat/image/room/dl_bg.png")
		                    self._lineArea[index][lineIndex+index1-6]:setContentSize(cc.size(18, 108))

		                    self._listViewConRecord[index]:pushBackCustomItem(self._lineArea[index][lineIndex+index1-6])
		                    self._curTrenLine[index] = self._curTrenLine[index] + 1

                            self._lineArea[index][lineIndex+index1-6]:addChild(Record);
                            Record:setPosition(8, 5);
                        end
                        
                    end
				end
			end
		end
	end
end

function BjlGameDesk:showCircle(data, index)
    for k,v in pairs(self._circlePanel[index]) do
        v:removeAllChildren()
    end
    if table.nums(data)==0 or data==nil then
        return
    end
	self._circleLine[index] = math.ceil(#data/2)

	--判断是否需要添加列
	if (self._circleLine[index] + 1 > self._curCircleCol[index]) then
		for i = self._curCircleCol[index],self._circleLine[index] do
			self._circlePanel[index][i] = ccui.Layout:create()
			self._circlePanel[index][i]:setAnchorPoint(cc.p(0.5,1))
			self._circlePanel[index][i]:setBackGroundImage("app/game/bjl/res/Baccarat/image/room/xsl_bg.png")
			self._circlePanel[index][i]:setContentSize(cc.size(18, 50))
            self._circlePanel[index][i]:setScaleY(0.88)
			self._listViewCirRecord[index]:pushBackCustomItem(self._circlePanel[index][i])
			self._curCircleCol[index] = self._curCircleCol[index] + 1
		end
	end
	local fir = display.newSprite()
    for k,v in pairs(data) do 
        for i =1,v.m_num do 
            --if (i==v.m_num) and (k==#data) then
                if v.m_color == 1 then 
	                fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_red.png");
                elseif  v.m_color == 0 then 
	                fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_blue.png");
                end
                fir:setScale(0.5);
                if k%2 == 1 then
                    fir:setPosition(4, 46.5 - 9 * (i-1));
                else
                    fir:setPosition(12, 46.5 - 9 * (i-1));
                end
                local index2 = math.ceil(k/2)
                self._circlePanel[index][index2-1]:addChild(fir);
                --fir:runAction(cc.Blink:create(2,3))
            --end
        end
    end
end

function BjlGameDesk:initCircle(data, index)
    if table.nums(data)==0 or data==nil then
        return
    end
	self._circleLine[index] = math.ceil(#data/2)

	--判断是否需要添加列
	if (self._circleLine[index] + 1 > self._curCircleCol[index]) then
		for i = self._curCircleCol[index],self._circleLine[index] do
			self._circlePanel[index][i] = ccui.Layout:create()
			self._circlePanel[index][i]:setAnchorPoint(cc.p(0.5,1))
			self._circlePanel[index][i]:setBackGroundImage("app/game/bjl/res/Baccarat/image/room/xsl_bg.png")
			self._circlePanel[index][i]:setContentSize(cc.size(18, 50))
            self._circlePanel[index][i]:setScaleY(0.88)
			self._listViewCirRecord[index]:pushBackCustomItem(self._circlePanel[index][i])
			self._curCircleCol[index] = self._curCircleCol[index] + 1
		end
	end
	local fir = display.newSprite()
    for k,v in pairs(data) do 
        for i =1,v.m_num do 
            if v.m_color == 1 then 
	            fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_red.png");
            elseif  v.m_color == 0 then 
	            fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_circle_blue.png");
            end
             fir:setScale(0.5);
            if k%2 == 1 then
                fir:setPosition(4, 46.5 - 9 * (i-1));
            else
                fir:setPosition(12, 46.5 - 9 * (i-1));
            end
            local index2 = math.ceil(k/2)
            self._circlePanel[index][index2-1]:addChild(fir); 
        end
    end
end

function BjlGameDesk:showBall(data,index)
    for k,v in pairs(self._ballPanel[index]) do
        v:removeAllChildren()
    end
    if table.nums(data)==0 or data==nil then
        return
    end

	self._ballLine[index] = math.ceil(#data/2)
	--判断是否需要添加列
	if (self._ballLine[index] + 1 > self._curBallCol[index]) then
		for i = self._curBallCol[index], self._ballLine[index] do
			self._ballPanel[index][i] = ccui.Layout:create()
			self._ballPanel[index][i]:setAnchorPoint(cc.p(0.5, 1))
			self._ballPanel[index][i]:setBackGroundImage("app/game/bjl/res/Baccarat/image/room/xsl_bg.png")
			self._ballPanel[index][i]:setContentSize(cc.size(18, 50))
            self._ballPanel[index][i]:setScaleY(0.88)
			self._listViewBallRecord[index]:pushBackCustomItem(self._ballPanel[index][i])
			self._curBallCol[index] = self._curBallCol[index] + 1
		end
	end
	local fir = display.newSprite()
    fir:setScale(0.5)
	for k,v in pairs(data) do 
        for i =1,v.m_num do 
            --if (i==v.m_num) and (k==#data) then
                if v.m_color == 1 then 
	                fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_ball_red.png")
                elseif  v.m_color == 0 then 
	                fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_ball_blue.png")
                end
                if k%2 == 1 then
                    fir:setPosition(4, 46.5 - 9 * (i-1))
                else
                    fir:setPosition(12, 46.5 - 9 * (i-1))
                end
                local index2 = math.ceil(k/2)
                self._ballPanel[index][index2-1]:addChild(fir)
                --fir:runAction(cc.Blink:create(2,3))
            --end
        end
    end
end

function BjlGameDesk:initBall(data,index)
    if table.nums(data)==0 or data==nil then
        return
    end
	self._ballLine[index] = math.ceil(#data/2)
	--判断是否需要添加列
	if (self._ballLine[index] + 1 > self._curBallCol[index]) then
		for i = self._curBallCol[index], self._ballLine[index] do
			self._ballPanel[index][i] = ccui.Layout:create()
			self._ballPanel[index][i]:setAnchorPoint(cc.p(0.5, 1))
			self._ballPanel[index][i]:setBackGroundImage("app/game/bjl/res/Baccarat/image/room/xsl_bg.png")
			self._ballPanel[index][i]:setContentSize(cc.size(18, 50))
            self._ballPanel[index][i]:setScaleY(0.88)
			self._listViewBallRecord[index]:pushBackCustomItem(self._ballPanel[index][i])
			self._curBallCol[index] = self._curBallCol[index] + 1
		end
	end
	local fir = display.newSprite()
    fir:setScale(0.5)
	for k,v in pairs(data) do 
        for i =1,v.m_num do 
            if v.m_color == 1 then 
	            fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_ball_red.png")
            elseif  v.m_color == 0 then 
	            fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_ball_blue.png")
            end
            if k%2 == 1 then
                fir:setPosition(4, 46.5 - 9 * (i-1))
            else
                fir:setPosition(12, 46.5 - 9 * (i-1))
            end
              local index2 = math.ceil(k/2)
            self._ballPanel[index][index2-1]:addChild(fir)
        end
    end
end

function BjlGameDesk:showLine(data, index)
    for k,v in pairs(self._linePanel[index]) do
        v:removeAllChildren()
    end
    if table.nums(data)==0 or data==nil then
        return
    end
	self._lineLine[index] = math.ceil(#data/2)

	--判断是否需要添加列
	if (self._lineLine[index] + 1 > self._curLineCol[index]) then
		for i = self._curLineCol[index], self._lineLine[index] do
			self._linePanel[index][i] = ccui.Layout:create()
			self._linePanel[index][i]:setAnchorPoint(cc.p(0.5,1))
			self._linePanel[index][i]:setBackGroundImage("app/game/bjl/res/Baccarat/image/room/xsl_bg.png")
			self._linePanel[index][i]:setContentSize(cc.size(18, 50))
            self._linePanel[index][i]:setScaleY(0.88)
			self._listViewLineRecord[index]:pushBackCustomItem(self._linePanel[index][i])
			self._curLineCol[index] = self._curLineCol[index] + 1
		end
	end
	local fir = display.newSprite()
    fir:setScale(0.5)
	for k,v in pairs(data) do 
        for i =1,v.m_num do 
            --if (i==v.m_num) and (k==#data) then
                if v.m_color == 1 then 
	                fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_line_red.png")
                elseif  v.m_color == 0 then 
	                fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_line_blue.png")
                end
                if k%2 == 1 then
                    fir:setPosition(4, 46.5 - 9 * (i-1))
                else
                    fir:setPosition(12, 46.5 - 9 * (i-1))
                end
                local index2 = math.ceil(k/2)
                self._linePanel[index][index2-1]:addChild(fir)
                --fir:runAction(cc.Blink:create(2,3))
            --end
        end
    end
end

function BjlGameDesk:initLine(data, index)
    if table.nums(data)==0 or data==nil then
        return
    end
	self._lineLine[index] = math.ceil(#data/2)

	--判断是否需要添加列
	if (self._lineLine[index] + 1 > self._curLineCol[index]) then
		for i = self._curLineCol[index], self._lineLine[index] do
			self._linePanel[index][i] = ccui.Layout:create()
			self._linePanel[index][i]:setAnchorPoint(cc.p(0.5,1))
			self._linePanel[index][i]:setBackGroundImage("app/game/bjl/res/Baccarat/image/room/xsl_bg.png")
			self._linePanel[index][i]:setContentSize(cc.size(18, 50))
            self._linePanel[index][i]:setScaleY(0.88)
			self._listViewLineRecord[index]:pushBackCustomItem(self._linePanel[index][i])
			self._curLineCol[index] = self._curLineCol[index] + 1
		end
	end
	local fir = display.newSprite()
    fir:setScale(0.5)
	for k,v in pairs(data) do 
        for i =1,v.m_num do 
            if v.m_color == 1 then 
	            fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_line_red.png")
            elseif  v.m_color == 0 then 
	            fir = display.newSprite("Games/NewBjle/recordUi/recordUiRes/record/corner_line_blue.png")
            end
            if k%2 == 1 then
                fir:setPosition(4, 46.5 - 9 * (i-1))
            else
                fir:setPosition(12, 46.5 - 9 * (i-1))
            end
             local index2 = math.ceil(k/2)
            self._linePanel[index][index2-1]:addChild(fir)
        end
    end
end

function BjlGameDesk:falshTrendImg(gameCount, index)
    math.randomseed(os.time())
    local nRand_cir = math.random(0,1)
    local nRand_ball = math.random(0,1)
    local nRand_line = math.random(0,1)

    if (nRand_cir == 0) then
		self._img_cir_D[index]:loadTexture("app/game/bjl/res/Baccarat/image/room/corner_circle_blue.png")
		self._img_cir_T[index]:loadTexture("app/game/bjl/res/Baccarat/image/room/corner_circle_red.png")
	else
		self._img_cir_T[index]:loadTexture("app/game/bjl/res/Baccarat/image/room/corner_circle_blue.png")
		self._img_cir_D[index]:loadTexture("app/game/bjl/res/Baccarat/image/room/corner_circle_red.png")
    end

	if (nRand_ball == 0) then
		self._img_ball_D[index]:loadTexture("app/game/bjl/res/Baccarat/image/room/corner_ball_blue.png")
		self._img_ball_T[index]:loadTexture("app/game/bjl/res/Baccarat/image/room/corner_ball_red.png")
	else
		self._img_ball_T[index]:loadTexture("app/game/bjl/res/Baccarat/image/room/corner_ball_blue.png")
		self._img_ball_D[index]:loadTexture("app/game/bjl/res/Baccarat/image/room/corner_ball_red.png")
	end

	if (nRand_line == 0) then
		self._img_line_D[index]:loadTexture("app/game/bjl/res/Baccarat/image/room/corner_line_blue.png")
		self._img_line_T[index]:loadTexture("app/game/bjl/res/Baccarat/image/room/corner_line_red.png")
	else
		self._img_line_T[index]:loadTexture("app/game/bjl/res/Baccarat/image/room/corner_line_blue.png")
		self._img_line_D[index]:loadTexture("app/game/bjl/res/Baccarat/image/room/corner_line_red.png")
	end
end

function BjlGameDesk:clearDesk(index)
    self._listViewConRecord[index]:removeAllChildrenWithCleanup(true)
    self._listViewCirRecord[index]:removeAllChildrenWithCleanup(true)
    self._listViewBallRecord[index]:removeAllChildrenWithCleanup(true)
    self._listViewLineRecord[index]:removeAllChildrenWithCleanup(true)

    self._curTrenLine[index] = 0
	self._iTrendLine[index] = 0

	self._circleLine[index] = 0
	self._curCircleCol[index] = 0

	self._ballLine[index] = 0
	self._curBallCol[index] = 0

	self._lineLine[index] = 0
	self._curLineCol[index] = 0

    for i = 0, 31 do
        self._lineArea[index][i] = ccui.Layout:create()
		self._lineArea[index][i]:setAnchorPoint(cc.p(0.5,1))
		self._lineArea[index][i]:setBackGroundImage("app/game/bjl/res/Baccarat/image/room/dl_bg.png")
		self._lineArea[index][i]:setContentSize(cc.size(18, 108))

		self._listViewConRecord[index]:pushBackCustomItem(self._lineArea[index][i])
		self._curTrenLine[index] = self._curTrenLine[index] + 1
    end
	
	for j = 0, 15 do
		self._circlePanel[index][i] = ccui.Layout:create()
		self._linePanel[index][i] = ccui.Layout:create()

		self._circlePanel[index][i]:setAnchorPoint(cc.p(0.5,1))
		self._linePanel[index][i]:setAnchorPoint(cc.p(0.5,1))

		self._circlePanel[index][i]:setBackGroundImage("app/game/bjl/res/Baccarat/image/room/xsl_bg.png")
		self._linePanel[index][i]:setBackGroundImage("app/game/bjl/res/Baccarat/image/room/xsl_bg.png")

		self._circlePanel[index][i]:setContentSize(cc.size(18, 54))
		self._linePanel[index][i]:setContentSize(cc.size(18, 54))

		self._listViewLineRecord[index]:pushBackCustomItem(self._linePanel[index][i])

		self._curCircleCol[index] = self._curCircleCol[index] + 1

		self._curLineCol[index] = self._curLineCol[index] + 1
	end
	for k = 0, 15 do
		self._ballPanel[index][i] = ccui.Layout:create()
		self._ballPanel[index][i]:setAnchorPoint(cc.p(0.5,1))
		self._ballPanel[index][i]:setBackGroundImage("app/game/bjl/res/Baccarat/image/room/xsl_bg.png")
		self._ballPanel[index][i]:setContentSize(cc.size(18, 54))
		self._listViewBallRecord[index]:pushBackCustomItem(self._ballPanel[index][i])
		self._curBallCol[index] = self._curBallCol[index] + 1
    end
end

return BjlGameDesk