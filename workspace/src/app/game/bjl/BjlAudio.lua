 
	-- 获取单例
local BjlAudio = class("BjlAudio")
BjlAudio.instance = nil
function BjlAudio:getInstance()
    if BjlAudio.instance == nil then
		BjlAudio.instance = BjlAudio.new()
	end
    return BjlAudio.instance
end
		-- 销毁单例
function BjlAudio:destroyInstance()
    BjlAudio.instance = nil
end
-- 背景音乐
function BjlAudio:playBgm()
    	g_AudioPlayer:stopMusic()
		local str = string.format("Games/NewBjle/audio/PlayGame.mp3");
		g_AudioPlayer:playMusic(str, true)
end 
-- 游戏开始音效
function BjlAudio:playStartEffect()
    g_AudioPlayer:playEffect("Games/NewBjle/audio/gamestart.mp3")
end

function BjlAudio:playStopEffect()
    g_AudioPlayer:playEffect("Games/NewBjle/audio/stop.mp3")
end
-- 发牌音效
function BjlAudio:playSendCardEffect()
    g_AudioPlayer:playEffect("Games/NewBjle/audio/fapai.mp3")
end
function BjlAudio:playTimeEffect()
    g_AudioPlayer:playEffect("Games/NewBjle/audio/countdown.mp3")
end

--按钮点击音效
function BjlAudio:playBtnClick()
     g_AudioPlayer:playEffect("Games/NewBjle/audio/btnClick.mp3")
end  
--牌型音效
function BjlAudio:playCardTypeEffect(Cardtype)
 
	if Cardtype == 2 then
			g_AudioPlayer:playEffect("Games/NewBjle/audio/single.mp3"); 
	elseif Cardtype == 3 then
			g_AudioPlayer:playEffect("Games/NewBjle/audio/pairs.mp3"); 
	elseif Cardtype == 4 then
		g_AudioPlayer:playEffect("Games/NewBjle/audio/straight.mp3"); 
	elseif Cardtype == 5 then
		g_AudioPlayer:playEffect("Games/NewBjle/audio/gold_flower.mp3"); 
	elseif Cardtype == 6 then
		g_AudioPlayer:playEffect("Games/NewBjle/audio/straight_gold.mp3"); 
	elseif Cardtype == 7 then
		g_AudioPlayer:playEffect("Games/NewBjle/audio/panther.mp3");  
    end 
end
--播放庄家
function BjlAudio:playBanker()
     g_AudioPlayer:playEffect("Games/NewBjle/audio/banker.mp3");
end
--播放闲家
function BjlAudio:playXian()
     g_AudioPlayer:playEffect("Games/NewBjle/audio/player.mp3");
end
--播放庄家赢
function BjlAudio:playBankerWin()
     g_AudioPlayer:playEffect("Games/NewBjle/audio/banker_win.mp3");
end
--播放闲家赢
function BjlAudio:playXianWin()
     g_AudioPlayer:playEffect("Games/NewBjle/audio/player_win.mp3");
end
--播放和
function BjlAudio:playHeEffect()
     g_AudioPlayer:playEffect("Games/NewBjle/audio/tie.mp3");
end
--播放庄家第三张
function BjlAudio:playBankerAdd()
     g_AudioPlayer:playEffect("Games/NewBjle/audio/banker_add.mp3");
end
--播放闲家第三张
function BjlAudio:playXianAdd()
     g_AudioPlayer:playEffect("Games/NewBjle/audio/player_add.mp3");
end
--播放庄家对子
function BjlAudio:playBankerDouble()
     g_AudioPlayer:playEffect("Games/NewBjle/audio/banker_pair.mp3");
end
--播放闲家对子
function BjlAudio:playXianDouble()
     g_AudioPlayer:playEffect("Games/NewBjle/audio/player_pair.mp3");
end
--闲家点数
function BjlAudio:playXianPoint(num)
	local music =string.format("Games/NewBjle/audio/xj%d.mp3", num);
	 g_AudioPlayer:playEffect(music);
end
--庄家点数
function BjlAudio:playZhuangPoint(num)
    local music =string.format("Games/NewBjle/audio/zj%d.mp3", num);
	 g_AudioPlayer:playEffect(music);
end
--播放最后结算显示音效
function BjlAudio:playGameFinishEffect()
    
    g_AudioPlayer:playEffect("Games/NewBjle/audio/papa.mp3");
end
--播放闲家天牌
function BjlAudio:playTianXian(point) 
     g_AudioPlayer:playEffect(string.format("Games/NewBjle/audio/player_%d.mp3",point));
end
--播放庄家天牌
function BjlAudio:playTianBanker(point)
     g_AudioPlayer:playEffect(string.format("Games/NewBjle/audio/banker_%d.mp3",point));
end
--播放筹码回来的音效
function BjlAudio:playCoinBackEffect()
    g_AudioPlayer:playEffect(string.format("Games/NewBjle/audio/on_bet.mp3"));
end
function BjlAudio:playCoinBet()
    g_AudioPlayer:playEffect(string.format("Games/NewBjle/audio/bet.mp3"));
end
return BjlAudio