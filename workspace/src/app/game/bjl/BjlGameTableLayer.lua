--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local GAMERESULTS_CSB = "Games/NewBjle/resultsUi/gameResults.csb";
local TOP_WIN_HONG = "Games/NewBjle/recordUi/recordUiRes/red_polocal.png";
local TOP_WIN_HEI = "Games/NewBjle/recordUi/recordUiRes/black_polocal.png";
local TOP_WIN_LONG = "Games/NewBjle/recordUi/recordUiRes/dragon_d.png";
local TOP_WIN_HU = "Games/NewBjle/recordUi/recordUiRes/tiger_d.png";
local TOP_WIN_HE = "Games/NewBjle/recordUi/recordUiRes/tie_d.png";
local  Scheduler              =  require("framework.scheduler")
local Audio = import(".BjlAudio") 
import(".BjlGlobal")
local GameSetLayer= require("src.app.newHall.childLayer.SetLayer")
local BjlRuleLayer = import(".BjlRuleLayer")
local HNLayer= require("src.app.newHall.HNLayer")
local PlayerHeadLayer =import("..common.util.PlayerHeadLayer")
local HandCard = import(".BjlGameHandCard") 
local BjlUserListManger= require("src.app.game.common.util.PlayerListLayer")
local NewBjleRecordListManager= import(".BjlRecordListManager") 
local BigWinnerLayer = import("..common.util.BigWinnerLayer")
local GameRecordLayer= require("src.app.newHall.childLayer.GameRecordLayer")
local ChatLayer = require("src.app.newHall.chat.ChatLayer") 
--endregion
local BjlGameTableLayer = class("BjlGameTableLayer",function()
    return HNLayer.new();
end)

function BjlGameTableLayer:ctor()
	print("BjlGameTableLayer:ctor")
	self:myInit()
    self:setupViews();
  --  self.tableLogic:sendGameInfo()
end 

function BjlGameTableLayer:myInit()
	print("BjlGameTableLayer:myInit")
	self._tableLayOut = nil;
	self._xiaZhuBtn= {};   --下注金额按钮
	self._xiaZhuBtnLiangBox={}; --下注金额按钮边上的亮框
	self._xiaZhuTxt ={};   --下注金额文本
	self._gameStateBg = nil;                  --游戏状态背景
	self._img_state =nil;                    --游戏状态
	self._clockLoading= nil; 
	self._clockProcess= nil;                 --时钟进度条
	self._clockTimeTxt= nil;                 --时间文本
	self._areaAllBetTxt = {}; --总下注多少的文本
	self._img_myBet={};
	self._areaMyBetTxt={}; --我下注多少的文本
	self._cardPile=nil;                     --牌堆
	self._xiaZhuArea={};   --下注的区域
	self._tipBox = nil;                       --提示框
     
		self._userListLayer=nil;              --玩家列表
		self._recordLayer=nil;              --历史胜负情况
	self._areaTotalValue = {0,0,0,0,0}	            --各区域下注量
    self._myareaTotalValue = {0,0,0,0,0}	            --我的各区域下注量
	self._ruleLayer = nil;                     --牌型规则
	self._recordNode=nil;                   --历史胜负
	self._Btn_RuleClose=nil;				--规则关闭按钮 
	self._winOrLoseArea={};--四个闲家的输赢情况
	self._userListLayerState = 1;       --记录玩家列表的伸缩状态
	self._zhuangListLayerState = 1;     --记录庄家列表的伸缩状态
	self._xiaZhuType = 0;               --下注类型
	self._allTime =0;                      --总时间
	self._nowTime =0;                      --剩余时间
	self._fnowTime =0;--进度条剩余时间
	self._willXiaZhuMoney = 0;          --选中的金币数
	self._xiaZhuMoney = {}; --下注金额
	self._xiaZhuMaxMoney = nil; --房间当前最大下注金额
	self._areaNowMoney = nil; --当前房间还可以下注的金额
	--self._recordList;                   --本地保存之前的胜负情况
	self._ntInfo = nil;                       --庄家
	self._myInfo = nil;                       --本方
	self._otherInfo = {};				--其他玩家
	self._otherNode={};
	self._handCard={};    --手牌
	self._gameResultPanel=nil;			--手牌结算区域
	self._imgWlocalip=nil;					--哪家赢的标识
	self._textXianPolocal=nil;				--闲家的点数
	self._textZhuangPonit=nil;			--庄家的点数
	self._zhuangSeat=nil;                   --庄家的座位号
	self._foreground = nil;
	self._resultNode = nil;         --结算背景
	self._gameState=nil;                    --当前游戏的状态
	self._sendCardSpace=nil;                --发单张牌的间隔
	self._kaiCardSpace=nil;                 --开牌的间隔
	self._sendCardNum = 0;              --已经发了几张牌了
	self._pai={{},{}};  --牌数据  
	self._myHaveBet = false;            --是否已经下注  
	self._canBetMoney = 0;                  --还可以押多少金币
	self._userListNode = nil;--在线列表节点
	self._btn_upZhuang=nil;	--上庄按钮
	--self._btn_downZhuang=nil;--下庄按钮 
	self._btn_online=nil;
	self._winAreaImg={};--赢的时候显示的框
	self._Particle_frame=nil;
	self._Particle_star=nil;
	self._Other_frame={};
	self._Other_star={}; 
	self._otherOther_Lost={};
	self._myInfo_Lost = nil;
	self._other_Win={};
	self._myInfo_Win = nil; 
	--存放桌子上筹码的五个容器，庄0，闲1，和2，庄对3，闲对4 
    self._spMoneyTab = {};
    for i=1,5 do 
        self._spMoneyTab[i] = {}
    end
	self._otherPos = {};--0-3其他玩家的位置,4在线玩家位置
	self._iGameCount = 0;--记录游戏当前局数
	self._iitemCount = 0;--记录当前显示的列数

	self._curMoney = 0;
	self._data={};
	--存储规则界面复选框的容器
	self._ruleSelectCheckBoxs = {};
	--进度条
	self._progressTimer = nil;
	self._MyPos = nil;
    self._Scheduler1 = nil
    self._backFlag = true
end

function BjlGameTableLayer:clearData()
	print("BjlGameTableLayer:clearData")
	if self._Scheduler1 then
		Scheduler.unscheduleGlobal(self._Scheduler1)	
        self._Scheduler1 = nil
    end
end

function  BjlGameTableLayer:onTouchCallback( sender)
    local name = sender:getName()   
    if name == "button_setting" then  -- 设置  
        local layer = GameSetLayer.new();
		self:addChild(layer);
        layer:setScale(1/display.scaleX, 1/display.scaleY);
    elseif name == "button_helpX" then -- 规则 
        local layer = BjlRuleLayer.new()
        self:addChild(layer);
    elseif name == "button_exit" then -- 退出  
        self:clearData()
        g_GameController:gameQuitRoomReq()
        UIAdapter:popScene()
        g_GameController.gameScene = nil
    elseif name == "Btn_qtwj" then -- 退出          
        --g_GameController:gamePlayerOnlineListReq()
        local ChatLayer = ChatLayer.new()
        self:addChild(ChatLayer);
    elseif name == "button_back" then
        local move = cc.MoveTo:create(0.5, cc.p(self.posX+self.backWidth, self.image_settingPanel:getPositionY()))
        local rmove = cc.MoveTo:create(0.5, cc.p(-self.posX-self.backWidth, self.image_settingPanel:getPositionY()));
    --  local rmove = move:reverse()
        local img = sender:getChildByName("Image_5")
        local RotateBy= cc.RotateBy:create(0.5,-90)
        if  self._backFlag then  
            img:runAction(RotateBy);  
            self.image_settingPanel:runAction(move)
            self._backFlag = false
            self.panel_backMenu:setVisible(true)         
        else
            img:runAction(RotateBy:reverse());   
            self.image_settingPanel:runAction(cc.Sequence:create(rmove,cc.CallFunc:create(function()   self.panel_backMenu:setVisible(false)
            end),nil))
             self._backFlag = true
        end 
    elseif name == "button_bigWinBtn" then
       local ChatLayer = ChatLayer.new()
        self:addChild(ChatLayer);
       -- g_GameController:gamePlayerOnlineListReq()
    elseif name == "button_btnTableList" then
       local GameRecordLayer = GameRecordLayer.new(2)
        self:addChild(GameRecordLayer)  
         ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", {161})
    end 
end

function BjlGameTableLayer:updateOnlineUserList(__info)
    self._userListLayer:updateUserList(__info.m_playerInfo); 
    self._userListLayer:setVisible(true); 
end

function BjlGameTableLayer:setPlayerCount(num)
    self.text_allPlayerText:setString(num)
end
function BjlGameTableLayer:setupViews()
     
    Audio:getInstance():playBgm();
	local winSize = cc.Director:getInstance():getWinSize();
	local gameTableNode = UIAdapter:createNode("Baccarat/BaccaratScene.csb");
	self:addChild(gameTableNode); 
      local center = gameTableNode:getChildByName("Scene") 
     local diffY = (display.size.height - 750) / 2
    gameTableNode:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
     UIAdapter:adapter(gameTableNode,handler(self, self.onTouchCallback)) 
     UIAdapter:praseNode(gameTableNode,self)
     self.button_btnTableList:loadTextures("hall/image/sanguosgj.png","hall/image/sanguosgj.png","hall/image/sanguosgj.png",0)
	self._tableLayOut = gameTableNode:getChildByName("image_bg");
    self.panel_backMenu =  gameTableNode:getChildByName("panel_backMenu");
    self.panel_backMenu:setSwallowTouches(false)
    self.panel_backMenu:setVisible(false)
    self.image_settingPanel =  self.panel_backMenu:getChildByName("image_settingPanel");
    self.posX = self.image_settingPanel:getPositionX()
    self.backWidth = self.image_settingPanel:getContentSize().width
     local panel_panelPlayers =gameTableNode:getChildByName("panel_panelPlayers");
     local node_seatPos7 = panel_panelPlayers:getChildByName("node_seatPos7");
     local button_bigWinBtn= node_seatPos7:getChildByName("button_bigWinBtn");
     button_bigWinBtn:setVisible(false)
	--在线人数
	self._btn_online = node_seatPos7:getChildByName("button_allPlayersBtn"); 
	self._otherPos[7] = cc.p(node_seatPos7:getPositionX()*display.scaleX,node_seatPos7:getPositionY());
    self.text_allPlayerText = self._btn_online:getChildByName("text_allPlayerText"); 
    self.text_allPlayerText:setString("")
    --text_allPlayerText:s
	--其他玩家区域  
	for i = 1,6 do
		local nodeName = string.format("node_seatPos%d", i );
		local node_other = panel_panelPlayers:getChildByName(nodeName);
		self._otherNode[i] = node_other
		self._otherInfo[i] = PlayerHeadLayer.new(node_other,2);
		self:addChild(self._otherInfo[i]);
		self._otherPos[i] =  cc.p(node_other:getPositionX()*display.scaleX,node_other:getPositionY()); 
        self._otherOther_Lost[i] = node_other:getChildByName("atlas_loseCoinNum");
		self._other_Win[i] = node_other:getChildByName("atlas_winCoinNum");
        self._other_Win[i]:setScale(1.5) 
        self._otherOther_Lost[i]:setScale(1.5) 
	end	 

	--自己的区域
		
	local image_opBar = self._tableLayOut:getChildByName("image_opBar");
	local node_my = image_opBar:getChildByName("Node_selfInfomation"); 
	 
	
	self._myInfo = PlayerHeadLayer.new(node_my,1);
    self._myInfo_Lost = node_my:getChildByName("atlas_loseCoinNum");
     self._myInfo_Lost:setVisible(false)
	self._myInfo_Win = node_my:getChildByName("atlas_winCoinNum");  
     self._myInfo_Win:setVisible(false)
    self._myInfo_Lost:setPosition(node_my:getPositionX()+60,node_my:getPositionY()+100)
    self._myInfo_Win:setPosition(node_my:getPositionX()+60,node_my:getPositionY()+100)
	self:addChild(self._myInfo);
	self._MyPos = cc.p(node_my:getPositionX(),node_my:getPositionY());
	--下注按钮和对应的值
    local Image_10 = image_opBar:getChildByName("Image_10"); 
    local listView_betListView = Image_10:getChildByName("listView_betListView"); 
     listView_betListView:setInertiaScrollEnabled(false)
    listView_betListView:setBounceEnabled(false)
   -- local t = ccui.Widget:create();
    self.panel_itemModelBet =  gameTableNode:getChildByName("panel_itemModelBet");
	for i = 1,BJL_GAME_BET_NUMBER do 
        local panel =  self.panel_itemModelBet:clone()
        local button_btn = panel:getChildByName("button_btn")
        button_btn:loadTextures("common/game_common/chip/cm_"..BJL_ChipValue[i]..".png")
        listView_betListView:pushBackCustomItem(panel);
		self._xiaZhuBtn[i] = button_btn
		self._xiaZhuBtn[i]:addTouchEventListener(handler(self,self.xiaZhuBtnClickCallBack))
        self._xiaZhuBtn[i]:setTag(i)
		--self._xiaZhuTxt[i] = self._xiaZhuBtn[i]:getChildByName("Text_money");
		self._xiaZhuBtnLiangBox[i] = panel:getChildByName("image_sel");
        local gray = panel:getChildByName("button_gray");
        gray:setVisible(false)
        local rotate =cc.RotateBy:create(1,360)
         self._xiaZhuBtnLiangBox[i]:runAction( cc.RepeatForever:create( rotate))
          self._xiaZhuBtnLiangBox[i]:setVisible(false)
	end 
		
 
	--游戏状态和倒计时
	local Node_1 = self._tableLayOut:getChildByName("Node_1");

	self._gameStateBg = Node_1:getChildByName("image_clockStateTxt");
	self._clockTimeTxt = Node_1:getChildByName("atlas_clockNum");
	self._gameStateBg:setVisible(false);
    self._gameStateBg:loadTexture("Baccarat/image/clock_state_bet.png")
	self._clockTimeTxt:setVisible(false);
	 

	--手牌
		
	self._gameResultPanel = gameTableNode:getChildByName("Panel_gameResult");
	self._gameResultPanel:setVisible(false);
    self._gameResultPanel:setLocalZOrder(100)
	self._imgWlocalip = self._gameResultPanel:getChildByName("Image_winTip");
	self._imgWlocalip:setVisible(false);
       local image_resultXian = self._gameResultPanel:getChildByName("image_resultXian");
    local Image_11  =image_resultXian:getChildByName("Image_11");
	self._textXianPolocal = self._gameResultPanel:getChildByName("image_resultXianDian");
    local image_resultZhuang = self._gameResultPanel:getChildByName("image_resultZhuang");
    local Image_11  =image_resultZhuang:getChildByName("Image_11");
	self._textZhuangPonit = Image_11:getChildByName("image_resultZhuangDian");
--	for i = 0, 1 do

		local handNode = self._gameResultPanel:getChildByName("node_xianCards");
		self._handCard[0] = HandCard.new(handNode,1);
		self:addChild(self._handCard[0]);
        local handNode = self._gameResultPanel:getChildByName("node_zhuangCards");
		self._handCard[1] = HandCard.new(handNode,2);
		self:addChild(self._handCard[1]);
	--end
		

--	--牌堆
--	self._cardPile = self._tableLayOut:getChildByName("Image_paidui");
--	self._cardPile:setVisible(false);

	--下注的区域
		
			
	for i = 1,BJL_MAX_COUNT do
			
		self._xiaZhuArea[i] = self._tableLayOut:getChildByName(string.format("panel_betPos%d", i-1));
		--self._xiaZhuArea[i]:addTouchEventListener(handler(self,self.xiaZhuAreaClickCallBack))
        self._xiaZhuArea[i]:setTag(i)
        self._xiaZhuArea[i]:setLocalZOrder(1)
	end
		

	--区域总下注的文本显示
		
	for i = 1,BJL_MAX_COUNT do
		local button_choosebet = self._tableLayOut:getChildByName(string.format("button_choosebet%d", i-1)); 
        button_choosebet:addTouchEventListener(handler(self,self.xiaZhuAreaClickCallBack))
        button_choosebet:setTag(i)
        local Sprite = button_choosebet:getChildByName("Sprite"); 
		self._areaAllBetTxt[i] = Sprite:getChildByName("text_poolNum");
        self._areaMyBetTxt[i] = Sprite:getChildByName("text_selfBetCnt");
	end
 
		
	--玩家列表
--	self._userListNode = self._tableLayOut:getChildByName("FileNode_userList");
	self._userListLayer = BjlUserListManger.new();
	 self._userListLayer:setVisible(false)
	self:addChild(self._userListLayer);
	 self._userListLayer:setLocalZOrder(1000)

     self._userWinnerLayer = BigWinnerLayer.new();
	 self._userWinnerLayer:setVisible(false)
	self:addChild(self._userWinnerLayer);
	 self._userWinnerLayer:setLocalZOrder(1000)
	--规则牌型
	 
	--历史胜负情况
	self._recordNode = gameTableNode:getChildByName("image_bgTrendChart"); 
    self._recordNode:setVisible(false)
    local node = display:newNode()
    node:setPosition(self._recordNode:getPositionX()+self.diffX,self._recordNode:getPositionY()-self._recordNode:getContentSize().height/2)
   -- node:setAnchorPoint(0.5,0.5)
    self:addChild(node)
	self._recordLayer = NewBjleRecordListManager.new(); 
    node:addChild(self._recordLayer)
   -- self._recordLayer:setVisible(false)


   local Node_7 = self._tableLayOut:getChildByName("Node_7")
  -- Node_7:setVisible(false)
   --ToolKit:removeLoadingDialog()


	return true;
end 
 
function BjlGameTableLayer:xiaZhuBtnClickCallBack(pSender) 
	local btnName = pSender:getName();
	for i = 1,BJL_GAME_BET_NUMBER do 
		self._xiaZhuBtnLiangBox[i]:setVisible(false);
		self._xiaZhuBtn[i]:setEnabled(true);
		if (i == pSender:getTag()) then
			
			self._xiaZhuBtnLiangBox[i]:setVisible(true);
			self._xiaZhuBtn[i]:setEnabled(false);
			self._willXiaZhuMoney = self._xiaZhuMoney[i];
			self._xiaZhuType = i;
		end
	end
end 


function BjlGameTableLayer:xiaZhuAreaClickCallBack(pSender,event)
     if event == ccui.TouchEventType.ended then
	    local btnName = pSender:getName();
	    if (self._gameState ~= GS_BET) then 
		    return;
	    end
	    local money = 0 
        for k,v in pairs(self._myareaTotalValue) do
            money =v+money
        end
        local tag = pSender:getTag()
	    if self._willXiaZhuMoney +money > self._areaNowMoney then
            self:showNoticeMessage("超过单局玩家总下注限制！");
		    return;
        end

	
		if (self._willXiaZhuMoney +self._areaTotalValue[tag]> self.m_areaBetLimit) then
		
		    self:showNoticeMessage("超过区域限定下注金额！");
		    return;
	    end
	    if (self._willXiaZhuMoney == 0) then
		
		    self:showNoticeMessage("请先选择下注金额！");
		    return;
	    end 

	    if (self._willXiaZhuMoney > self._curMoney) then
		
		    self:showNoticeMessage("您的金币不足！");
		    return;
	    end
		--if (btnName == self._xiaZhuArea[tag]:getName()) then 
			g_GameController:gameBetReq(tag-1,self._willXiaZhuMoney*100 );
	--	end 
    end
end

function BjlGameTableLayer:setGameState( gamestate, time, isAni)
	
	--保存状态
	self._gameState = gamestate;

	--设置状态
	if (gamestate == GS_BET) then 
		if (isAni) then 
			ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("Games/NewBjle/biyingqipai/biyingqipai0.png",
				"Games/NewBjle/biyingqipai/biyingqipai0.plist", "Games/NewBjle/biyingqipai/biyingqipai.ExportJson");
	
			local StartXiazhu = ccs.Armature:create("biyingqipai");
			StartXiazhu:getAnimation():play("startxiazhu");
			Audio:getInstance():playStartEffect();
			StartXiazhu:setPosition(667,self._winSize.height / 2);
			StartXiazhu:setVisible(true);
			StartXiazhu:setLocalZOrder(1);
		--	StartXiazhu:setRotation(-90);
			self._tableLayOut:addChild(StartXiazhu, 1); 
			local function func1(armature_2,ntype,id) 
				if (ntype == ccs.MovementEventType.complete) then 
					armature_2:getAnimation():stop();
					armature_2:runAction(cc.Sequence:create( cca.delay(0.1),  cc.CallFunc:create(function ()   
                        armature_2:removeFromParent()
                    end)))
                end
			end
			StartXiazhu:getAnimation():setMovementEventCallFunc(handler(self,func1)); 
		end
		self:showRunTime(time);
	end
--	if (gamestate == GS_SEND_CARD and isAni) then

--		ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("Games/NewBjle/biyingqipai/biyingqipai0.png",
--				"Games/NewBjle/biyingqipai/biyingqipai0.plist", "Games/NewBjle/biyingqipai/biyingqipai.ExportJson");
--		local StopXiazhu = ccs.Armature:create("biyingqipai");
--		Audio:getInstance():playStopEffect();  
--		StopXiazhu:setPosition(667,self._winSize.height / 2);
--		StopXiazhu:setVisible(true);
--		StopXiazhu:setLocalZOrder(1);
--		StopXiazhu:setRotation(-90);
--		self._tableLayOut:addChild(StopXiazhu, 1);
--		local function func2()
--			StopXiazhu:stopAllActions();
--			StopXiazhu:removeFromParent();
--		end
--		self:runAction(cc.Sequence:create(cca.delay(1), cc.CallFunc:create(func2)));
--	end
	if (gamestate == GS_WAIT_NEXT and isAni) then 
	--	self:showRunTime(time);	
		ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("Games/NewBjle/biyingqipai/biyingqipai0.png",
				"Games/NewBjle/biyingqipai/biyingqipai0.plist", "Games/NewBjle/biyingqipai/biyingqipai.ExportJson");
		local waitArmature =ccs.Armature:create("biyingqipai");
		waitArmature:getAnimation():play("qingdengdaixiajuyouxikaishi");
		waitArmature:setPosition(667,self._winSize.height / 2-50);
		waitArmature:setVisible(true);
		waitArmature:setLocalZOrder(1);
		--waitArmature:setRotation(-90);
		self._tableLayOut:addChild(waitArmature, 1);

		local function func2()
			waitArmature:stopAllActions();
			waitArmature:removeFromParent();
		end
		self:runAction(cc.Sequence:create(cca.delay(2), cc.CallFunc:create(func2)));
	end
end

function BjlGameTableLayer:showRunTime(time)
	
	self._allTime = time;
	self._nowTime = time;
	self._fnowTime = time;
    if self._nowTime<=0 then
        self._nowTime =0
        self._clockTimeTxt:setVisible(false);
        self._gameStateBg:setVisible(false);	
    end
	self._clockTimeTxt:setString(string.format("%d",self._nowTime));
	
	if self._Scheduler1 then
		Scheduler.unscheduleGlobal(self._Scheduler1)	
        self._Scheduler1 = nil
    end
	 
	if (time <= 0) then	
		self._gameStateBg:setVisible(false); 
	else 
        self._clockTimeTxt:setVisible(true);
		self._gameStateBg:setVisible(true);
		self._Scheduler1 = Scheduler.scheduleGlobal(handler(self, self.updateGameTime),1)	
	end
end

function BjlGameTableLayer:updateGameTime(dt) 
	self._gameStateBg:setVisible(true);
		
	for i = 1,BJL_GAME_BET_NUMBER do
		
		self._xiaZhuBtn[i]:setEnabled(true);
		self._xiaZhuBtn[i]:setEnabled(true);
	end
	self:isXiazhu();
	self._nowTime = self._nowTime-1;
	if (self._nowTime>0 and self._nowTime<=5) then
		
		Audio:getInstance():playTimeEffect();
    end
	if self._nowTime<=0 then
        self._nowTime =0
    end
	self._clockTimeTxt:setString(string.format("%d",self._nowTime));
	if (self._nowTime<=0) then		
		self._nowTime = 0;
		self._gameStateBg:setVisible(false);
		for i = 1,BJL_GAME_BET_NUMBER do			
			self._xiaZhuBtn[i]:setEnabled(false);
		--	self._xiaZhuBtn[i]:setEnabled(false);
		end
        
		if self._Scheduler1 then
			Scheduler.unscheduleGlobal(self._Scheduler1)	
			self._Scheduler1 = nil
		end
	end
end

function BjlGameTableLayer:dealSendCard( data, sendCardSpace, kaiCardSpace) 
	--发牌 顺序-庄-天-地-玄-黄
    self._clockTimeTxt:setVisible(false);
    self._gameStateBg:setVisible(false);
	self._sendCardSpace = sendCardSpace;
	self._kaiCardSpace = kaiCardSpace;
	self._sendCardNum = 0;
	--self._cardPile:setVisible(true);
	self._pai = data.pai;

	self:clearHandCard();
--	self:setBetBtnEnable(false);
	self._gameResultPanel:setVisible(true);
    self._recordLayer:setVisible(false)
	self:playSendCard(0, 1, self._pai[1][1], self._sendCardSpace,data);
end

function BjlGameTableLayer:playSendCard( dir, idx, cardValue, sendCardSpace, data)
	local num= #self._pai[1] +#self._pai[2]
	self._gameResultPanel:runAction(cc.Sequence:create(cca.delay(sendCardSpace), cc.CallFunc:create(function()
		self._handCard[dir]:setHandCardVisible(true); 
		self._handCard[dir]:sendOneCard(idx, cardValue, self._sendCardSpace);
		local fdelayTime = 0.5;
		local fdelayTime1 = 2.0; 
		if (1 == dir) then
			
			self:setZhuangCardPolocal(data.iZPaiXing[idx]);
			
		elseif (0 == dir) then
			
			self:setXianCardPolocal(data.iXPaiXing[idx]);
		end 
		if (self._sendCardNum == 0 or self._sendCardNum == 2 ) then
					
			Audio:getInstance():playXian();	
		end
		if (self._sendCardNum == 1 or self._sendCardNum == 3 ) then
			
			Audio:getInstance():playBanker();
		end
		if self._sendCardNum >= 4 and dir == 0 then
			Audio:getInstance():playXianAdd();
			fdelayTime1 = 3.5;
		end
		
		if self._sendCardNum >= 4 and dir == 1 then	
			Audio:getInstance():playBankerAdd();
			fdelayTime = 1.5;
		end

		self._sendCardNum = self._sendCardNum+1;
		if (self._sendCardSpace ~= 0) then
			
			Audio:getInstance():playSendCardEffect();
		end
		if (self._sendCardNum < num) then
			
			local d = self._sendCardNum % 2;
			local index = math.floor(self._sendCardNum / 2);
			if #self._pai[1] < #self._pai[2] and self._sendCardNum == 4 then
				local t = self._pai[dir + 1][index+1]
				self:playSendCard(dir, index+1, t, self._sendCardSpace,data);
			else
				local t = self._pai[d+1][index+1]
				self:playSendCard(d, index+1, t, self._sendCardSpace,data);
			end
		else
			
			local idui = -1;
			for i = 1,3 do
				
				if (1 == data.iWinQuYu[i]) then
					
					local fun1 = cc.CallFunc:create(function()
						if (8 == data.iXPaiXing[3]) then
							
							Audio:getInstance():playTianXian(8);
							
						elseif (9 == data.iXPaiXing[3]) then
							
							Audio:getInstance():playTianXian(9);
							
						else
							
							Audio:getInstance():playXianPoint(data.iXPaiXing[3]);
						end
							
					end)
					local fun2 = cc.CallFunc:create(function()
						if (8 == data.iZPaiXing[3]) then
							
							Audio:getInstance():playTianBanker(8);
							
						elseif (9 == data.iZPaiXing[3]) then
							
							Audio:getInstance():playTianBanker(9);
							
						else
							
							Audio:getInstance():playZhuangPoint(data.iZPaiXing[3]);
						end							
					end)
					local fun3 = cc.CallFunc:create(function()
						Audio:getInstance():playGameFinishEffect();
						local isZdui = false;
						local isXdui = false;
						if (1 == data.iWinQuYu[5]) then
						
							isZdui = true;
						end
						if (1 == data.iWinQuYu[4]) then 
							isXdui = true;
						end
						self:showWlocalip(i, isZdui, isXdui);

					end)
					local delayFun  = cca.delay(fdelayTime);
					local delayFun1 = cca.delay(fdelayTime1);
					local delayFun2 = cca.delay(2.5);
					local seqAct1 = cc.Sequence:create(delayFun,fun1, delayFun1, fun2, delayFun2, fun3, nil);
					self:runAction(seqAct1);
				end
			end
		end
	end), nil));
end
 

 

--function BjlGameTableLayer:dealGameBegin(data)

--	Audio:getInstance():playStartEffect();
--	self:setXiaZhuBtnTxt(data.i64ChoumaValue);
--	--self:updateZhuangList(data.locallist);
--	self:showNtInfo(data.iNowNtStation);
--	self:showMyInfo(BjlGameController:getInstance():getMySeatNo());
--	self:setBetBtnEnable(not (BjlGameController:getInstance():getMySeatNo() == data.iNowNtStation));
--	self:setCanBetMoney(data.iMaxZhu);

--	for i = 0,BJL_GAME_BET_NUMBER-1 do

--		self._data[i] = data.i64ChoumaValue[i];
--	end

--	self:isXiazhu(self._data);
--end

function BjlGameTableLayer:dealXiaZhu( data) 
    
	self:showXiaZhuAnimation(data);
	if (data.m_accountId == Player:getAccountID()) then
        self._curMoney =self._curMoney- data.m_betValue/100;
		--自己下注要更新下自己的金币
        print("下注金额    "..data.m_betValue/100)
		self:setMyMoneyChangForBet( self._curMoney);
		--_curMoney -= data.iMoney;

		self:isXiazhu();

		self:setMyHaveBet(true);
--		local myXiaZhu = {0,0,0,0,0};
--		for i = 1,BJL_MAX_COUNT do 
--			myXiaZhu[data.m_betId+1] = data.m_betValue;
--		end
--		self:showMyBetTxt(myXiaZhu); 
        self._areaMyBetTxt[data.m_betId+1]:setString(data.m_myAreaValue / 100);
	end
	--刷新桌面区域下注筹码文本显示
	self._areaAllBetTxt[data.m_betId+1]:setString(data.m_areaTotalValue/ 100);
    self._areaTotalValue[data.m_betId+1] = data.m_areaTotalValue/ 100
    self._myareaTotalValue[data.m_betId+1] = data.m_myAreaValue
end

function BjlGameTableLayer:setMyMoneyChangForBet(money)

	self._myInfo:setUserMoney(money);
end

function BjlGameTableLayer:setMyHaveBet( haveBet)

	self._myHaveBet = haveBet;
end



function BjlGameTableLayer:setXiaZhuBtnTxt( money)

	for i = 1,BJL_GAME_BET_NUMBER do

		local nowMoney = money[i] ;
	--	self._xiaZhuTxt[i]:setString(string.format("%d", nowMoney));
		self._xiaZhuMoney[i] = nowMoney;
	end
end

function BjlGameTableLayer:setAreaMaxBetMoney( money)

	local nowMoney = money;
	self._xiaZhuMaxMoney = nowMoney*0.01;
end
 

function BjlGameTableLayer:dealGameResult( data)
    
	local stationData;
--	for i = 1,BJL_MAX_COUNT do

--		if (1 == self._winOrLoseArea[i]) then

--			self._winAreaImg[i]:setVisible(true);
--			self._winAreaImg[i]:setOpacity(0);
--			local feadIn = cc.FadeIn:create(0.5);
--			local feadOut = cc.FadeOut:create(0.5);
--			self._winAreaImg[i]:runAction(cc.Repeat:create(cc.Sequence:create(feadIn, feadOut, nil), 3));
--		end
--	end

	local seq = cc.Sequence:create(cc.CallFunc:create(function() 
		
		self:showPlayHeadflash(data.m_allResult);
			 
		self:choumaAction();
	end), cca.delay(1), cc.CallFunc:create(function()

		for i = 1,5 do

			self:hidechouma(i);
		end
	end), nil);
	self:runAction(seq);
	--self:setBetBtnEnable(false);
     

	
    
  
end

function BjlGameTableLayer:showResultLayer( data)

	if (self._resultNode ~= nil) then 
		self._resultNode:removeFromParent();
		self._resultNode = nil;
	end
	self._resultNode = UIAdapter:createNode(GAMERESULTS_CSB);
	local winSize = cc.Director:getInstance():getWinSize();
	self:addChild(self._resultNode);
	self._resultNode:setPosition(self._winSize.width / 2,self._winSize.height / 2);
	self._resultNode:setScaleX(winSize.width / 1280);
	self._resultNode:setScaleY(winSize.height / 720);

	local panel = self._resultNode:getChildByName("Panel_1");
	local spBg = panel:getChildByName("bg");
	local textBenFen = spBg:getChildByName("AtlasLabel_benfen");
	local img_win = spBg:getChildByName("Image_win");
	local img_bml = spBg:getChildByName("Image_bml");
	local img_lose = spBg:getChildByName("Image_lose");
	local img_CMwin = spBg:getChildByName("Image_CMwin");
	img_CMwin:setVisible(false);
	local img_CMlose = spBg:getChildByName("Image_CMlose");
	img_CMwin:setVisible(false);


	if (0 < data.iUserFen) then

		textBenFen:setString("+" .. data.iUserFen/100);
		img_CMwin:setVisible(true);
		img_CMlose:setVisible(false);
		img_win:setVisible(true);
		img_lose:setVisible(false);
		img_bml:setVisible(false);

	elseif (0 == data.iUserFen) then

		textBenFen:setString( data.iUserFen);
		img_CMwin:setVisible(false);
		img_CMlose:setVisible(true);
		img_win:setVisible(false);
		img_lose:setVisible(false);
		img_bml:setVisible(true);


	elseif (0 > data.iUserFen) then

		textBenFen:setString( data.iUserFen/100);
		img_CMwin:setVisible(false);
		img_CMlose:setVisible(true);
		img_win:setVisible(false);
		img_lose:setVisible(true);
		img_bml:setVisible(false);

	end
	local btnClose =spBg:getChildByName("Button_close");
	btnClose:addTouchEventListener(function()
		if (self._resultNode) then

			self._resultNode:removeFromParent();
			self._resultNode = nil;
		end
	end);
	--延迟显示
	self._resultNode:setVisible(false);
	--自动移除
	self._resultNode:runAction(cc.Sequence:create(cca.delay(4), cc.CallFunc:create(function()
		_resultNode:setVisible(true);
--		for i = 1,BJL_MAX_COUNT do

--			self._winAreaImg[i]:setOpacity(0);
--			self._winAreaImg[i]:setVisible(false);
--		end
	end),cca.delay(2), cc.CallFunc:create(function()
		if (self._resultNode) then 
			self._resultNode:removeFromParent();
			self._resultNode = nil;
		end
	end), nil));
end

function BjlGameTableLayer:showXiaZhuAnimation( data)

	--筹码类型
	local betType = 1;
	for i = 1,BJL_GAME_BET_NUMBER do

		if (data.m_betValue*0.01 == self._xiaZhuMoney[i]) then

			betType = i;
		end	
	end

	if (betType>5) then

		return;
	end
	local spMoney = cc.Sprite:create("common/game_common/chip/cm_"..BJL_ChipValue[betType]..".png");
    
	--chouma:setPosition(spMoney:getContentSize().width/2,spMoney:getContentSize().height/2);
	--chouma:setRotation(-90);
--	spMoney:addChild(chouma);
	spMoney:setScale(0.4);
	local randomNumR = math.random(0, 360);
	spMoney:setRotation(randomNumR);
    spMoney:setLocalZOrder(100)
	self._xiaZhuArea[data.m_betId+1]:addChild(spMoney);

	--下注的起点坐标
	local winSize = cc.Director:getInstance():getWinSize();

	--下注的终点坐标
	local btnArea = self._xiaZhuArea[data.m_betId+1]:getContentSize();
	local endPos;
	local randomNumX = math.random(0, 20);
	local randomNumY =  math.random(0, 20);


	endPos = cc.p((btnArea.width / 40) * randomNumX + 50, (btnArea.height / 25) * randomNumY + 20);
-- 		}
	local startPos = cc.p(0, 0);
	local bOther = true;
	if (data.m_accountId == Player:getAccountID()) then

		startPos = self._xiaZhuArea[data.m_betId+1]:convertToNodeSpace(cc.p(self._xiaZhuBtn[betType]:getPositionX(),self._xiaZhuBtn[betType]:getPositionY()));
		spMoney:setTag(255);--255自己的筹码，0-3其他玩家筹码，4在线玩家筹码
        for i = 1,6 do 
			if (self._otherInfo[i]:getTag() == Player:getAccountID()  and self._otherInfo[i]:isVisible()) then
                self._otherInfo[i]:setUserMoney(data.m_coin*0.01)
            end
        end
	else

		for i = 1,6 do

			if (self._otherInfo[i]:getTag() == data.m_accountId  and self._otherInfo[i]:isVisible()) then

				startPos = self._xiaZhuArea[data.m_betId+1]:convertToNodeSpace(self._otherPos[i]);
				bOther = false;
                if i<4 then
				    self._otherInfo[i]:showOtherRun(1);
                else
                    self._otherInfo[i]:showOtherRun(2);
                end
				spMoney:setTag(i);
                self._otherInfo[i]:setUserMoney(data.m_coin*0.01)
			end
		end
		if (bOther == true) then

			startPos = self._xiaZhuArea[data.m_betId+1]:convertToNodeSpace(self._otherPos[7]);
			spMoney:setTag(7);
		end

	end
	spMoney:setPosition(startPos);
	local actionMove = cc.MoveTo:create(0.1, endPos);
	local scaleTo1 = cc.ScaleTo:create(0.1, 0.55);
	local scaelTo2 = cc.ScaleTo:create(0.1, 0.4);
	spMoney:runAction(cc.Sequence:create(actionMove, scaleTo1, scaelTo2, nil));

	table.insert(self._spMoneyTab[data.m_betId+1],spMoney)	 
--	if area == 0 then

--		break;
--	elseif area == 1 then
--        table.insert(self._spMoney1,spMoney) 
--		break;
--	elseif area == 2 then
--        table.insert(self._spMoney2,spMoney) 
--		break;
--	elseif area == 3 then
--        table.insert(self._spMoney3,spMoney) 
--		break;
--	elseif area == 4 then
--        table.insert(self._spMoney4,spMoney) 
--		break;
--	end
	Audio:getInstance():playCoinBet(data.m_coin);
end

function BjlGameTableLayer:showPlayHeadflash(data)
    
    for  k,v in pairs(data)do   
	    if (v.m_accountId == Player:getAccountID()) then
            self._curMoney =v.m_curScore*0.01
		    if (0 < v.m_profit) then
              --   self._curMoney = self._curMoney+ v.m_profit* 0.01
                  
			    self._myInfo_Win:setString("+"..v.m_profit * 0.01);

			    local destPos = cc.p(0, 50);
			    local fadeIn = cc.FadeIn:create(1.0);
			    local fadeOut = cc.FadeOut:create(1.0);
			    local seq = cc.Sequence:create(cca.delay(1.5),cc.CallFunc:create(function()
				    self._myInfo_Win:setVisible(true);
				    self._myInfo_Win:setOpacity(0);
--			 ;					
			    end), fadeIn, fadeOut, nil);
			    self._myInfo_Win:runAction(seq); 
		    end
		    if (0 > v.m_profit) then
               
			    self._myInfo_Lost:setString(v.m_profit * 0.01);
             
			    local fadeIn = cc.FadeIn:create(1.0);
			    local fadeOut = cc.FadeOut:create(1.0);
			    local seq = cc.Sequence:create(cca.delay(1.5), cc.CallFunc:create(function()		
				    self._myInfo_Lost:setVisible(true);
				    self._myInfo_Lost:setOpacity(0);
			    end),fadeIn,fadeOut, nil);
			    self._myInfo_Lost:runAction(seq);

		    end 
	    end
		 for i = 1,6 do 
			if (self._otherInfo[i]:getTag() == v.m_accountId   and self._otherInfo[i]:isVisible()) then
				if (0 <=v.m_profit) then 
					 
					self._other_Win[i]:setString("+"..v.m_profit * 0.01); 
					local fadeIn = cc.FadeIn:create(1.0);
					local fadeOut = cc.FadeOut:create(1.0);
					local seq = cc.Sequence:create(cca.delay(1.5), cc.CallFunc:create(function()

						self._other_Win[i]:setVisible(true);
						self._other_Win[i]:setOpacity(0);
						 
					end), fadeIn, fadeOut, nil);
					self._other_Win[i]:runAction(seq);

				end
				if (0 > v.m_profit) then 
					self._otherOther_Lost[i]:setString(v.m_profit * 0.01);
                     
					local fadeIn = cc.FadeIn:create(1.0);
					local fadeOut = cc.FadeOut:create(1.0);
					local seq = cc.Sequence:create(cca.delay(1.5),  cc.CallFunc:create(function()
						self._otherOther_Lost[i]:setVisible(true);
						self._otherOther_Lost[i]:setOpacity(0);
					end), fadeIn, fadeOut, nil);
					self._otherOther_Lost[i]:runAction(seq); 
				end  
		    end 
	    end
    end
    --更新自己和庄家的金币
	 local info = {}
    info.m_nickname = Player:getNickName()
    info.m_score = self._curMoney*100
    info.m_vipLevel = Player:getVipLevel()
    info.m_faceId = Player:getFaceID()
    --info.m_frameId = Player:getFrameID()
	self._myInfo:setPlayerInfo(info); 
end



function BjlGameTableLayer:clearDesk()

	--self._cardPile:stopAllActions();
	--self._cardPile:setVisible(false);
    self:setZhuangCardPolocal(0);
    self:setXianCardPolocal(0);
	self:clearAllChip();
	self:clearHandCard();
	self:clearAreaBet();
	--self:setBetBtnEnable(false);
	self:setMyHaveBet(false); 
	self._gameResultPanel:setVisible(false);
    self._recordLayer:setVisible(true)
	self._imgWlocalip:setVisible(false); 
end

function BjlGameTableLayer:clearAllChip()
 
	for i=1,5 do
		self._spMoneyTab[i]={}
        if self._xiaZhuArea[i] then
		    self._xiaZhuArea[i]:removeAllChildren();
        end
	end
end

function BjlGameTableLayer:clearHandCard()

	for i=0,1 do

		self._handCard[i]:stopSendCardAction();
		self._handCard[i]:resettingAllCardPos();
		self._handCard[i]:setHandCardVisible(false); 
	end
end
 



function BjlGameTableLayer:updateUserList(__info) 
    for i = 1,6 do 
    	self._otherInfo[i]:showPlayer(false); 
    end
    self:showOtherInfo(__info.m_mostCoinplayerList[1], 1);
    for k=1,5 do
        self:showOtherInfo(__info.m_mostRichPlayerList[k], k+1);
    end
--	m_UserListVec={}
--	local index = 0;
--	for i = 0,3 do 
--		self._otherInfo[i]:showPlayer(false);
--		self._otherInfo[i]:setTag(244);
--	end
--	for i = 0,PLAY_COUNT-1 do 
--		local userInfo = BjlGameController:getInstance():getUserBySeatNo(i);
--		if (userInfo) then
--			table.insert(m_UserListVec,userInfo); 
--			if (i ~= BjlGameController:getInstance():getMySeatNo() and (4 > index)) then 
--				self:showOtherInfo(userInfo, index, userInfo.bDeskStation);
--				index = index+1;
--			end
--		end
--	end
end

function BjlGameTableLayer:setWinOrLoseArea( winLose)

	for i = 1,BJL_MAX_COUNT do
		self._winOrLoseArea[i] = winLose[i];
    end
end


function BjlGameTableLayer:showMyInfo(score ) 
    local info = {}
    info.m_nickname = Player:getNickName()
    info.m_score =score
    info.m_vipLevel = Player:getVipLevel()
    info.m_faceId = Player:getFaceID()
    --info.m_frameId = Player:getFrameID() 
	self._myInfo:setPlayerInfo(info);
	 self._curMoney = score *0.01
end

function BjlGameTableLayer:showAreaBetTxt( areaMoney)
    if #areaMoney==0 then
        return
    end
	for k,v in pairs(areaMoney) do  
		self._areaAllBetTxt[k]:setString( v / 100);
	end 
end

function BjlGameTableLayer:showMyBetTxt( areaMoney)
    if #areaMoney==0 then
        return
    end
	for k,v in pairs(areaMoney) do  
        if v~=0 then
		    self._areaMyBetTxt[k]:setString( v / 100);
        end
	end 
end

function BjlGameTableLayer:clearAreaBet() 
	for i = 1,BJL_MAX_COUNT do 
		self._areaAllBetTxt[i]:setString(string.format("%d", 0));
		self._areaMyBetTxt[i]:setString(string.format(""));
	end
end

function BjlGameTableLayer:setBetBtnEnable( enable)

	for i = 1,BJL_GAME_BET_NUMBER do

		self._xiaZhuBtn[i]:setEnabled(enable);
		self._xiaZhuBtn[i]:setEnabled(true);
		if (not enable) then 
			self._xiaZhuBtnLiangBox[i]:setVisible(false);
			self._willXiaZhuMoney = 0; 
		end
	end
end

function BjlGameTableLayer:leaveTableCallBack()

	GamePlatform:returnPlatform(DESKLIST);
end

function BjlGameTableLayer:showNoticeMessage(message)
    TOAST(message)
--	self._tipBox:setVisible(true);
-- 	local tipTxt = self._tipBox:getChildByName("Text_tip");
--	tipTxt:setString(message);
--	self._tipBox:runAction(cc.Sequence:create(cca.delay(0.8), cc.CallFunc:create(function()
--		self._tipBox:setVisible(false);
--	end), nil));
end

--	function BjlGameTableLayer:dealGameForEnterForeground()

--		local nowTime = socket.gettime()
--		local lastTime = Configuration:getInstance():getValue("bkTime", Value(0)).asUnsignedlocal();
--		if (0 == lastTime) return;

--		local oldTime = _nowTime;
--		local oldGameStation = _gameState;
--		-- 切后台所消耗时间
--		Ulocal useTime = nowTime - lastTime;

--		--根据状态和剩余时间来整理桌子
--		this:runAction(cc.Sequence:create(cca.delay(0.5f), cc.CallFunc:create(function()
--			if (oldGameStation ~= _gameState)
--			{
--				if (self._nowTime>useTime-oldTime)
--				{
--					self._nowTime = self._nowTime - useTime + oldTime;
--				}
--			}
--			else
--			{
--				-- 倒计时剩余时间
--				if (self._nowTime > useTime)
--				{
--					self._nowTime -= useTime;
--				}
--				else
--				{
--					self._nowTime = 0;
--				}
--			}
--			setDeskForEnterForeground();
--		}), nil));
--	}

--	function BjlGameTableLayer:setDeskForEnterForeground()
--	{
--		switch (self._gameState)
--		{
--		case GS_BET:
--			break;
--		case GS_SEND_CARD:
--			if ((self._nowTime < (0.5f*(25 - self._sendCardNum) + 8)) and (self._sendCardNum < 25))
--			{
--				if ((_nowTime <= 8))
--				{
--					self._kaiCardSpace = 0;
--				}
--				local newSpece = (self._nowTime - (1.5f*4)) / local(25 - self._sendCardNum);
--				self._sendCardSpace = newSpece;
--			}
--			break;
--		case GS_WAIT_NEXT:
--			clearDesk();
--			break;
--		case GS_PLAY_GAME:
--			clearDesk();
--			break;
--		default:
--			break;
--		}
--	}

--	--超端界面
--	function BjlGameTableLayer:showSuperLayer()
--	{
--		local superLayer = GameSuper:create();
--		addChild(superLayer);
--		superLayer:superCallBack = [=](local area){
--			BjlGameController:getInstance():sendSuperContorl(area);
--		};
--	}

--	--显示超端按钮
--	function BjlGameTableLayer:showSuperBtn()
--	{
--		local btn_super = dynamic_cast<Button*> (self._tableLayOut:getChildByName("Button_super"));
--		btn_super:setVisible(true);
--	}

function BjlGameTableLayer:showOtherInfo(userInfo, index)

    if userInfo then
	    self._otherInfo[index]:setPlayerInfo(userInfo);
	    self._otherInfo[index]:setVisible(true);
	    self._otherInfo[index]:showPlayer(true); 
        self._otherInfo[index]:setTag(userInfo.m_accountId)
    end
end

function BjlGameTableLayer:choumaAction() 
	for i = 1,BJL_MAX_COUNT do 
		local endPos;
		if (self._winOrLoseArea[i] and 0 < self._winOrLoseArea[i]) then 
            local sub =self._spMoneyTab[i]
			for j = 1,#sub do
                local obj =sub[j]
				if (obj:getTag()) == 255 then 
					endPos =obj:getParent():convertToNodeSpace(self._MyPos); 
				elseif (obj:getTag()) == 1 then 
					endPos = obj:getParent():convertToNodeSpace(self._otherPos[1]); 
				elseif (obj:getTag()) == 2 then 
					endPos = obj:getParent():convertToNodeSpace(self._otherPos[2]); 
				elseif (obj:getTag()) == 3 then 
					endPos = obj:getParent():convertToNodeSpace(self._otherPos[3]); 
				elseif (obj:getTag()) == 4 then 
					endPos = obj:getParent():convertToNodeSpace(self._otherPos[4]); 
				elseif (obj:getTag()) == 5 then 
					endPos = obj:getParent():convertToNodeSpace(self._otherPos[5]); 
                elseif (obj:getTag()) == 6 then 
					endPos = obj:getParent():convertToNodeSpace(self._otherPos[6]); 
                  elseif (obj:getTag()) == 7 then 
					endPos = obj:getParent():convertToNodeSpace(self._otherPos[7]); 
                else
                    endPos = obj:getParent():convertToNodeSpace(self._otherPos[7]); 
				end
				obj:setTag(44);
				local moveto1 =cc.MoveTo:create(0.5, endPos);
				obj:runAction(cc.Sequence:create(moveto1,cc.CallFunc:create(function()
                obj:setVisible(false)
                end),nil));
            end
			Audio:getInstance():playCoinBackEffect();

		else
			self:hidechouma(i);
		end
	end
end

function BjlGameTableLayer:hidechouma(area)

    local obj =self._spMoneyTab[area]
	for  i = 1, #obj do

		if (44 ~= obj[i]:getTag()) then

			obj[i]:setVisible(false);
		end
	end
end


function BjlGameTableLayer:setGameCount( data)

 
	if (self._iGameCount > table.nums(data)) then

		self._iitemCount = 0;
		self._recordLayer:allClear();
		self._recordLayer:clearData();
		self._recordLayer:setListviewBG(false);
	end
	self._iGameCount = table.nums(data);
end


function BjlGameTableLayer:showLeftRecorf( data) 
    self._recordLayer:allClear();
	self._recordLayer:clearData();
	local fgameCount = self._iGameCount / 6;
	local itemCount = math.ceil(fgameCount);
	self._iitemCount = itemCount;
	local  index = 0;
	--local igameCoun = 0;
--	if (self._gameState ~= GS_WAIT_NEXT) then 
--		igameCoun = self._iGameCount - 1; 
--	else 
--		igameCoun = self._iGameCount;
--	end
   
	for i = 0,itemCount-1 do  
         local tab = {{},{},{},{},{},{}}
		for  j = 1,6 do  
            
            if  data[i*6+j] then
                if data[i*6+j].m_xianVal> data[i*6+j].m_bankerVal then
                    tab[j][1] =1
                elseif  data[i*6+j].m_xianVal<data[i*6+j].m_bankerVal then
                    tab[j][0] =1
                else
                     tab[j][2] =1
                end
                if data[i*6+j].m_xianPair ==1 then
                    tab[j][4] =1
                end
                if data[i*6+j].m_bankerPair ==1 then
                    tab[j][3] =1
                end
            end
		end
        
		self._recordLayer:showLeftRecord(tab, self._iGameCount);
	end
	if (self._iGameCount >= 48) then 
		self._recordLayer:setListviewBG(true);
	end

	
    
    local index = 0 
    for k,v in pairs(data) do
        if v.m_xianVal>=8 or v.m_bankerVal>=8 then
            index = index+1
        end
    end
    self._recordLayer:initConTrend(data, self._iGameCount);
	self._recordLayer:updateGameCountResult(index);
    self._recordLayer:falshTrendImg(self._iGameCount);
end

function BjlGameTableLayer:isXiazhu()

	 
	for j = 1,BJL_GAME_BET_NUMBER do

		self._xiaZhuBtn[j]:setEnabled(true);
	end

	local i = 8; 
	if (self._curMoney < BJL_ChipValue[1]) then

		i = 1;

	elseif (self._curMoney < BJL_ChipValue[2]) then

		i = 2;

	elseif (self._curMoney < BJL_ChipValue[3]) then

		i = 3;

	elseif (self._curMoney < BJL_ChipValue[4]) then

		i = 4;

	elseif (self._curMoney < BJL_ChipValue[5]) then

		i = 5;
         
	end

	for k=i, BJL_GAME_BET_NUMBER do

		self._xiaZhuBtn[k]:setEnabled(false);
		self._xiaZhuBtnLiangBox[k]:setVisible(false);
	end

end

function BjlGameTableLayer:dealOnlineUser( data)

	self._userListLayer:setUserMoneyData(data);
end

function BjlGameTableLayer:checkRuleSelectCallback(pSender, ntype) 
	if (pSender) then 
		if (#self._ruleSelectCheckBoxs > 0) then 
			for k,v in pairs(self._ruleSelectCheckBoxs) do 
				if (pSender ~= v) then 
					v:setTouchEnabled(true);
					v:setSelected(false);

				else

					v:setTouchEnabled(false);
                end
				if (v:isSelected()) then

					local tag = v:getTag() + 1;
					local listView = self._ruleLayer:getChildByName(string.format("ListView_%d",tag));
					listView:setVisible(true);

				else

					local tag = v:getTag() + 1;
					local listView = self._ruleLayer:getChildByName(string.format("ListView_%d", tag));
					listView:scrollToTop(0.1,true);
					listView:setVisible(false);
				end
			end
		end
	end
end

function BjlGameTableLayer:showWlocalip( winArea,  zhuangdui,  xiandui)

	local wlocalipStr = "";
	if (zhuangdui) then

		Audio:getInstance():playBankerDouble();
	end
	if (xiandui) then

		Audio:getInstance():playXianDouble();
	end
    if self:getChildByTag(11) then
        self:getChildByTag(11):removeFromParent()
    end
     local spineAnim = sp.SkeletonAnimation:createWithJsonFile("Baccarat/image/result/win.json", "Baccarat/image/result/win.atlas")
     spineAnim:setTag(11)      
          
	if (winArea == GF_ZHUANG) then

		wlocalipStr = "Baccarat/image/result/result_banker.png";

        spineAnim:setAnimation( 0, "animation_boss", false)
	elseif (winArea == GF_XIAN) then

		wlocalipStr = "Baccarat/image/result/result_player.png";
        spineAnim:setAnimation( 0, "animation_free", false)
	elseif (winArea == GF_HE) then
        spineAnim:setAnimation( 0, "animation_p", false)
		wlocalipStr = "Baccarat/image/result/result_he.png";
	end
    spineAnim:setPosition(self._winSize.width/display.scaleX/2,self._winSize.height/2)
    self:addChild(spineAnim,100)
    self:runAction(cc.Sequence:create(cc.DelayTime:create(3.0), cc.CallFunc:create(function()
        spineAnim:removeFromParent()
    end)))
	self._imgWlocalip:setScale(0.01);
	self._imgWlocalip:loadTexture(wlocalipStr);
	self._imgWlocalip:setVisible(true);
	self._imgWlocalip:runAction(cc.Sequence:create(cc.ScaleTo:create(0.5, 1.0), cc.CallFunc:create(function()
		if (winArea == GF_ZHUANG) then

			Audio:getInstance():playBankerWin();


		elseif (winArea == GF_XIAN) then

			Audio:getInstance():playXianWin();

		elseif (winArea == GF_HE) then

			Audio:getInstance():playHeEffect();
		end
	end))); 
end

function BjlGameTableLayer:setRoomID( iID)
    local Image_1 =  self._tableLayOut:getChildByName("Image_1");
	local roomIDText = Image_1:getChildByName("atlas_txtTableOrder");
	roomIDText:setString(string.format("%d",iID));
end

function BjlGameTableLayer:setXianhongMoney( money,money2)

	self._recordLayer:setXianhongMoney(money); 
    self.m_areaBetLimit = money
    self._areaNowMoney = money2
end

function BjlGameTableLayer:setZhuangCardPolocal( zhuang)

	self:runAction(cc.Sequence:create(cca.delay(0.1), cc.CallFunc:create(function() 
		self._textZhuangPonit:loadTexture("Baccarat/image/result/dians_banker/"..zhuang..".png")
	end)));
end

function BjlGameTableLayer:setXianCardPolocal( xian)

	self:runAction(cc.Sequence:create(cca.delay(0.1), cc.CallFunc:create(function() 
		self._textXianPolocal:loadTexture("Baccarat/image/result/dians_player/"..xian..".png")
	end)));
end
 

function BjlGameTableLayer:dealYuceReslut( data1,data2,data3)

	--初始化走势图

	self._recordLayer:initCircle(data1);
	self._recordLayer:initBall(data2);
	self._recordLayer:initLine(data3);
end

 return  BjlGameTableLayer