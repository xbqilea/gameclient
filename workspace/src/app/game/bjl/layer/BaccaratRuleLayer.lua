-- region *.lua
-- Date
-- 此文件由[BabeLua]插件自动生成	
local HNLayer= require("src.app.newHall.HNLayer") 
local BaccaratRuleLayer = class("BaccaratRuleLayer", function()
    return HNLayer.new()
end)
local BaccaratRes               = import("..scene.BaccaratRes")


function BaccaratRuleLayer.create()
    BaccaratRuleLayer.instance_ = BaccaratRuleLayer.new()
    return BaccaratRuleLayer.instance_
end

function BaccaratRuleLayer:ctor()
--    self.super:ctor(self)
--    self:enableNodeEvents()
    self:init()
end

function BaccaratRuleLayer:init()
    self:initCSB()
end 

--function BaccaratRuleLayer:onEnter()
--    self.super:onEnter()
--    self:showWithStyle()
--end

--function BaccaratRuleLayer:onExit()
--    self.super:onExit()
--end

function BaccaratRuleLayer:initCSB()
 --   self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_NORMAL, FixLayer.HIDE_DLG_NORMAL)
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    self.m_pathUI = cc.CSLoader:createNode(BaccaratRes.CSB_GAME_RULE)
    self.m_pathUI:addTo(self.m_rootUI)

    local diffX = 145 - (1624 - display.size.width) / 2
    self.m_pathUI:setPositionX(diffX)
   
    self.m_pBtnClose      = self.m_pathUI:getChildByName("Panel_2"):getChildByName("Image_1"):getChildByName("btn_close")
--    self.m_pathUI:getChildByName("Panel_2"):getChildByName("Image_1"):getChildByName("ScrollView_1"):setScrollBarEnabled(false)

    self.m_pBtnClose:addTouchEventListener(handler(self, self.onReturnClicked))

    self.m_pathUI:getChildByName("Panel_1"):addTouchEventListener(handler(self, self.onReturnClicked))

--    if ClientConfig.getInstance():getIsOtherChannel() then
--        local sprite = self.m_pathUI:getChildByName("Panel_2"):getChildByName("Image_1"):getChildByName("ScrollView_1"):getChildByName("Sprite_1")
--        sprite:setTexture("game/baccarat/image/rule_txt-2.png")
--    end
end

function BaccaratRuleLayer:onReturnClicked(sender,eventType)
    if eventType ~=ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound(BaccaratRes.SOUND_OF_CLOSE)
    self:close()
end

return BaccaratRuleLayer