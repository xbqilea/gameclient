--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
--BaccaratLayer
--endregion

local BaccaratTrendLayer        = import(".BaccaratTrendLayer") 
local BaccaratRes               = import("..scene.BaccaratRes")
local BaccaratEvent             = import("..scene.BaccaratEvent")
local BaccaratOtherInfoLayer    = import(".BaccaratOtherInfoLayer")
local Poker                     = import(".BaccaratPoker")  
local BaccaratRuleLayer         = import(".BaccaratRuleLayer")
local BaccaratResultLayer       = import(".BaccaratResultLayer")
local BaccaratDataMgr           = import("..manager.BaccaratDataMgr") 
local GameRecordLayer= require("src.app.newHall.childLayer.GameRecordLayer")
local GameSetLayer= require("src.app.newHall.childLayer.SetLayer") 
local CBetManager               = require("src.app.game.common.util.CBetManager")
local HNLayer= require("src.app.newHall.HNLayer") 
local scheduler    = require("framework.scheduler") 
local ChatLayer = require("src.app.newHall.chat.ChatLayer") 
local AudioManager = cc.exports.AudioManager

local ChipCount          = 5 --筹码数量
local AreaCount          = 5 --投注区域数量
local ChipOffsetY        = 14 --选中筹码位移
local FlyChipScale       = 0.35 --下注筹码缩放比例

local CHIP_FLY_STEPDELAY = 0.02 --筹码连续飞行间隔
local CHIP_FLY_TIME      = 0.35 --飞筹码动画时间
local CHIP_JOBSPACETIME  = 0.35 --飞筹码任务队列间隔
local CHIP_FLY_SPLIT     = 20 -- 飞行筹码分组 分XX次飞完
local CHIP_ITEM_COUNT    = AreaCount --下注项数
local CHIP_REWARD        = {2, 9, 2, 3, 3, 33, 12, 12}
local IPHONEX_OFFSETX    = 60

local ZORDER_OF_RULE = 100
local ZORDER_OF_TREND = 100
local ZORDER_OF_MESSAGEBOX = 101
local ZORDER_OF_FLOATMESSAGE = 102

local BaccaratLayer = class("BaccaratLayer", function()
    return HNLayer.new()
end)
 

function BaccaratLayer:ctor()
    math.randomseed(os.time())
    self:setNodeEventEnabled(true)
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)
    self.m_pathUI = cc.CSLoader:createNode(BaccaratRes.CSB_GAME_MAIN)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0, diffY))
    self.m_pathUI:addTo(self.m_rootUI)
    self.m_pResultLayer = nil 
    self:init()
end

function BaccaratLayer:onEnter()
end

function BaccaratLayer:onExit()
    --注销系统事件
    self:cleanEvent()

    --注销后台切换和断线重连事件
  --  self:stopGameEvent()
end

function BaccaratLayer:init()
    --初始化主ui
    self:initUI()

    --注册按钮事件
    self:initBtnClickEvent()

    --注册系统事件
    self:initEvent()

    --注册前后台切换和断线重连事件
   -- self:startGameEvent()

    --初始化变量
    self:initConstVar()

    --初始化ui元素显示和变量
    self:initViewAndVar()

    --动画进入
    self:flyIn()
end

--初始化变量，此方法内变量在断线重连后，需要重置
function BaccaratLayer:initVar()
    self.m_vJettonOfMine    = {} --自己筹码 {pSpr = sp(筹码对象), nIdx = idx(随机坐标索引)}
    self.m_vJettonOfOther   = {} --其他筹码 {pSpr = sp(筹码对象), nIdx = idx(随机坐标索引)}
    self.m_isPlayBetSound   = false --是否正在播放筹码音效
    self.m_isPlayDaojishi   = false --是否在播放最后3秒倒计时动画
    self.m_randmChipSelf    = {} --自己的随机筹码投注区
    self.m_randmChipOthers  = {} --其他玩家的随机筹码投注区
    self.m_flyJobVec        = {} --飞行筹码任务清空
    self.m_sendCard         = {} --前2张发牌
    self.m_nSoundClock      = nil
    self.m_daluMaxCol       = 0
    self.m_zhuluMaxCol      = 0
    self.m_oldScore         = 0
    --self.m_xianPointAni     = nil --闲家点数动画
    --self.m_zhuangPointAni   = nil --庄家点数动画
    self:resetChipPosArea() --重置筹码随机投注坐标
    self.m_bMoveIn = false --是否在播放入场动画
end

--初始化变量，此方法内变量在断线重连后，不需要重置
function BaccaratLayer:initConstVar()
    self.isOpenMenu       = false
    self.m_nIndexChoose   = 0 --选中筹码的索引
    self.m_betChipPos     = {} --筹码原始坐标    
    self.m_flyChipPos     = {} --玩家下注飞行筹码起始坐标    
    for i = 1, ChipCount do
        self.m_betChipPos[i] = cc.p(self.m_pBtnBetChip[i]:getPosition())
        self.m_flyChipPos[i] = cc.p(self.m_nodeFlyChip:getChildByName("Node_pos" .. i):getPosition())
    end

    --region 处理筹码飞行层的定位用节点，处理完后，清空筹码飞行层
    self.m_selfChipPos   = cc.p(self.m_nodeFlyChip:getChildByName("Node_self"):getPosition())
    self.m_otherChipPos  = cc.p(self.m_nodeFlyChip:getChildByName("Node_other"):getPosition())
    self.m_bankerChipPos = cc.p(self.m_nodeFlyChip:getChildByName("Node_banker"):getPositionX(),self.m_nodeFlyChip:getChildByName("Node_banker"):getPositionY()-180)
    self.m_vChipArea     = {} --投注区域rect数组
    for i = 1, AreaCount do
        local _node = self.m_nodeFlyChip:getChildByName("Panel_flyarea" .. i)
        local posx, posy = _node:getPosition()
        local sz = _node:getContentSize()
        self.m_vChipArea[i] = cc.rect(posx, posy, sz.width, sz.height )
    end

    self.m_nodeFlyChip:removeAllChildren()
    --endregion

    --region 发牌动画定位用节点，处理完后，清空节点对象
    --[[ 区域显示结构

            获胜/同点平
            ___________
            |  扑克牌 |
            |         |
            |         |
            |天王/对子|
            |         |
            -----------
               点数
    ]]--

    --发牌起始坐标点
    self.m_sendCardBeginPos = cc.p(self.m_nodeCardAni:getChildByName("Node_fapaiqi"):getPosition())

    --闲家牌结束坐标点
    self.m_xianCardEndPos   = cc.p(self.m_nodeCardAni:getChildByName("Node_xiancard"):getPosition())
    --闲家获胜或平的动画坐标
    self.m_xianWinAniPos    = cc.p(self.m_xianCardEndPos.x, self.m_xianCardEndPos.y + 50)
    --闲家牌型坐标
    self.m_xianTypePos      = cc.p(self.m_xianCardEndPos.x + 50, self.m_xianCardEndPos.y)
    --闲家点数坐标
    self.m_xianPointPos     = cc.p(self.m_xianCardEndPos.x, self.m_xianCardEndPos.y - 50)

    --庄家牌结束坐标点
    self.m_zhuangCardEndPos = cc.p(self.m_nodeCardAni:getChildByName("Node_zhuangcard"):getPosition())
    --庄家获胜或平的动画坐标
    self.m_zhuangWinAniPos  = cc.p(self.m_zhuangCardEndPos.x, self.m_zhuangCardEndPos.y + 50)
    --庄家牌型坐标
    self.m_zhuangTypePos    = cc.p(self.m_zhuangCardEndPos.x + 50, self.m_zhuangCardEndPos.y)
    --庄家点数坐标
    self.m_zhuangPointPos   = cc.p(self.m_zhuangCardEndPos.x, self.m_zhuangCardEndPos.y - 50)

    self.m_nodeCardAni:removeAllChildren()
    --endregion

    --扑克配置项
    local offset = 40
    self.m_sendCardConfig = {
        beginRotate = 212, --发牌初始角度
        beginScaleVal = 0.19, --发牌初始缩放
        endScaleVal = 0.7, --发牌结束缩放
        beginPos = self.m_sendCardBeginPos, --发牌起始点
        sendTs = 0.32, --单张发牌时间
        cardNum = 2, --发牌张数
        openCardTs = 0.15, --开牌动画一半所需时间
        cardOffsetX = offset, --发牌的横向间隔距离
        cardOffsetY = -13, --补牌的纵向位移
        pointSoundDelay = 0.6, --牌点数音效延迟
        boneName = {
            VALUE = "A",
            COLOR = "heitao",
        },
        pointAniPos = {
            [BaccaratDataMgr.ePlace_Xian]   = {
                [2] = cc.p(self.m_xianCardEndPos.x + 20, self.m_xianCardEndPos.y - 75), --两张牌坐标
                [3] = cc.p(self.m_xianCardEndPos.x + 40, self.m_xianCardEndPos.y - 75), --三张牌坐标
            },
            
            [BaccaratDataMgr.ePlace_Zhuang] = {
                [2] = cc.p(self.m_zhuangCardEndPos.x + 20, self.m_zhuangCardEndPos.y - 75),
                [3] = cc.p(self.m_zhuangCardEndPos.x + 40, self.m_zhuangCardEndPos.y - 75),
            },
        },
        --前4张牌的配置 13闲家 24庄家
        [1] = {
            cardType  = BaccaratDataMgr.ePlace_Xian,
            cardIndex = 1,
            endPos    = cc.p(self.m_xianCardEndPos.x + offset/2, self.m_xianCardEndPos.y)
            },
        [2] = {
            cardType  = BaccaratDataMgr.ePlace_Zhuang,
            cardIndex = 1,
            endPos    = cc.p(self.m_zhuangCardEndPos.x + offset/2, self.m_zhuangCardEndPos.y)
            },
        [3] = {
            cardType  = BaccaratDataMgr.ePlace_Xian,
            cardIndex = 2,
            endPos    = cc.p(self.m_xianCardEndPos.x + 3*offset/2, self.m_xianCardEndPos.y)
            },
        [4] = {
            cardType  = BaccaratDataMgr.ePlace_Zhuang,
            cardIndex = 2,
            endPos    = cc.p(self.m_zhuangCardEndPos.x + 3*offset/2, self.m_zhuangCardEndPos.y)
            },
    }

    --等待下一局动画坐标点
    self.m_zcAniPos = cc.p(self.m_nodeZCAni:getChildByName("Node_anipos"):getPosition())
    self.m_nodeZCAni:removeAllChildren()
end

--清理UI组件状态(尽量还原为初始化状态) 此方法在断线重连后，需要调用
function BaccaratLayer:cleanUI()
    --停止所有动画
    self:stopAllActions()

    --清理显示的筹码
    self:cleanChip()

    --清理转场动画
    self:cleanZhuanChangAni()

    --清理扑克动画
    self:cleanCardAni()

    --清理下注金额显示
    self:cleanBetNum()

    --隐藏所有闪烁获胜动画
    self:hideAllAreaBlink()
end

--重置UI显示
function BaccaratLayer:resetUIView()
    --先清理现有UI元素
    self:cleanUI()

    self:initViewAndVar()

    --显示筹码(入场动画原因 单独显示筹码)
    self:viewBetChip()
end

function BaccaratLayer:initViewAndVar()
    --初始化数据
    self:initVar()

    --重置UI元素显示
    self:resetNodeView()

    --首次显示筹码按钮状态
    self:updateJettonStateFirst()

    --更新银行按钮
--    self:updateBankBtnState()

    --更新续投按钮状态
    self:updateBetContinueEnable()

    --显示等待动画
    self:playWaitAni()

    --通知银行可用状态
--    self:notifyBankEnable()
end

--初始化UI组件 此方法内变量在断线重连后，不需要重置
function BaccaratLayer:initUI()
    self.m_rootNode    = self.m_pathUI:getChildByName("Panel_root")

    --self.m_pNodeResult    = self.m_pathUI:getChildByName("Node_Result")

    self.m_nodeBottom  = self.m_rootNode:getChildByName("Node_bottom")
    self.m_nodeLeft    = self.m_rootNode:getChildByName("Node_left")
    self.m_nodeRight   = self.m_rootNode:getChildByName("Node_right")
    self.m_nodeClip    = self.m_rootNode:getChildByName("Panel_node_clip")
    self.m_nodeBet     = self.m_rootNode:getChildByName("Node_betarea")
    self.m_nodeFlyChip = self.m_rootNode:getChildByName("Node_flychip")
    self.m_nodeTop     = self.m_rootNode:getChildByName("Node_top")
    self.m_nodeDlg     = self.m_rootNode:getChildByName("Panel_dialog")

    --动画展示节点，包含：转场动画,发牌动画,开牌动画,等待开始动画
    self.m_nodeAni     = self.m_rootNode:getChildByName("Node_ani")
    
    --转场动画节点
    self.m_nodeZCAni   = self.m_nodeAni:getChildByName("Node_zhuanchang")

    --牌动画节点 包含：发牌动画,开牌动画,牌型动画，获胜动画
    self.m_nodeCardAni = self.m_nodeAni:getChildByName("Node_cardani")

    --桌子背景
    self.m_pImgDesk    = self.m_rootNode:getChildByName("Image_desk")

    --倒计时
    self.m_pImgTimer   = self.m_pImgDesk:getChildByName("Image_timerbg")
    self.m_pImgState   = self.m_pImgTimer:getChildByName("Image_state")
    self.m_pLbTimerNum = self.m_pImgTimer:getChildByName("Text_timernum")

    --动画闪烁区域
    self.m_nodeBlink   = self.m_pImgDesk:getChildByName("Node_blink")
    self.m_pBlink = {}
    for i = 1, AreaCount do
        self.m_pBlink[i] = self.m_nodeBlink:getChildByName("Sprite_blink" .. i)
    end

    --玩家信息
    self.m_selfPanel    = self.m_nodeLeft:getChildByName("Panel_self")
    local _Image_selfbg = self.m_selfPanel:getChildByName("Image_selfbg")
    self.m_pLbSelfName  = _Image_selfbg:getChildByName("Text_selfname")
    self.m_pImgVip      = _Image_selfbg:getChildByName("Image_vip")
    self.m_pLbSelfGold  = _Image_selfbg:getChildByName("Node_scale"):getChildByName("Text_selfgold")
    self.m_pBtnBank     = _Image_selfbg:getChildByName("Btn_bank")

    --庄家信息
    local _Image_bankbg  = self.m_nodeTop:getChildByName("Image_5")
    self.m_pSpZhuangNameBg = _Image_bankbg:getChildByName("Image_name")
    self.m_pLbBankerName = self.m_pSpZhuangNameBg:getChildByName("Text_bankname")
    self.m_pSpBankerVip  = _Image_bankbg:getChildByName("Image_vip_0")
    self.m_pSpZhuangGoldBg = _Image_bankbg:getChildByName("Image_gold")
    self.m_pLbBankerGold = self.m_pSpZhuangGoldBg:getChildByName("Text_bankgold")
    self.m_pLbApplyNum   = _Image_bankbg:getChildByName("Text_apply")
    self.m_pBtnApply     = _Image_bankbg:getChildByName("Btn_applybanker")
    self.m_pBtnDown      = _Image_bankbg:getChildByName("Btn_downbanker")

    --主界面路数展示信息
    self.m_nodeSmallroad   = self.m_nodeTop:getChildByName("Node_smallroad")
    local _Sprite_bg       = self.m_nodeSmallroad:getChildByName("Sprite_bg")
    self.m_pNodeTableWin   = _Sprite_bg:getChildByName("Node_win")
    self.m_pNodeTableRoad  = _Sprite_bg:getChildByName("Node_road")
    self.m_pLbPingNum      = _Sprite_bg:getChildByName("Text_pingnum")
    self.m_pLbZhuangNum    = _Sprite_bg:getChildByName("Text_zhuangnum")
    self.m_pLbXianNum      = _Sprite_bg:getChildByName("Text_xiannum")
    self.m_pLbZhuangDuiNum = _Sprite_bg:getChildByName("Text_zhuangduinum")
    self.m_pLbXianDuiNum   = _Sprite_bg:getChildByName("Text_xianduinum")

    --其他玩家按钮
    -- local _Panel_other   = self.m_nodeLeft:getChildByName("Panel_other")
    self.m_pBtnOthers    = self.m_nodeLeft:getChildByName("Btn_qtwj")
    -- self.m_pLbOtherNum   = _Panel_other:getChildByName("TXT_other_num")

    --六个筹码
    self.m_pBtnBetChip = {}
    local _chipbg = self.m_nodeBottom:getChildByName("Image_chipbg")
    for i = 1, ChipCount do
        self.m_pBtnBetChip[i] = _chipbg:getChildByName("Btn_chip" .. i)
    end

    --续投按钮
    self.m_pBtnContinue  = _chipbg:getChildByName("Btn_continue")

    --全屏关闭按钮
    self.m_pBtnFullClose = self.m_nodeClip:getChildByName("Btn_close")

    --退出按钮
    self.m_pBtnReturn    = self.m_nodeLeft:getChildByName("Btn_exit")

    --路数按钮
    self.m_pBtnRoad      = self.m_nodeRight:getChildByName("Btn_road")

    --菜单按钮
    self.m_pBtnMenuPop   = self.m_nodeRight:getChildByName("Btn_pop")

    --弹出菜单
    self.m_nodeMenu     = self.m_rootNode:getChildByName("Node_menu")
    self.m_menuBG       = self.m_nodeMenu:getChildByName("Image_menubg")
    self.m_pBtnSound    = self.m_menuBG:getChildByName("Btn_sound")
    self.m_pBtnMusic    = self.m_menuBG:getChildByName("Btn_music")
    self.m_pBtnMenuBank = self.m_menuBG:getChildByName("Btn_menubank")
    self.m_pBtnRule     = self.m_menuBG:getChildByName("Btn_rule")

    self.m_nodeBetnum   = self.m_pImgDesk:getChildByName("Node_betnum")

    --总下注
    self.m_pLbTotalBet  = self.m_nodeBetnum:getChildByName("Text_bettotal")
    
    --区域下注
    self.m_pLbAreaTotal = {}

    --我的下注
    self.m_pLbSelfTotal = {}

    for i = 1, AreaCount do
        self.m_pLbAreaTotal[i] = self.m_nodeBetnum:getChildByName("Text_betnum_total" .. i)
        self.m_pLbSelfTotal[i] = self.m_nodeBetnum:getChildByName("Text_betnum_self" .. i)
    end

    self.m_vChipArea = {} --记录投注区域rect 用于重置随机投注点用

    --庄家提示动画节点
    self.m_pSpbankerTip = _Image_bankbg:getChildByName("Sprite_bankertip")
    --连庄节点
    self.m_pSpLianzhuang = self.m_nodeAni:getChildByName("Sprite_zhuang")

    --分辨率自适应
    self:setNodeFixPostion()

    --初始化弹出菜单
    self:initClipNode()
    self:initMenuView()

    --初始化主界面大路
    self:initMainViewDalu()

    --初始化主界面珠路
    self:initMainViewZhulu()

    --刷新珠路显示
    self:updateMainRoadView()

--    --聊天界面
--    self.m_newChatLayer = ChatLayer:create()
--    self.m_newChatLayer:initData({1,2})
--    self.m_newChatLayer:setPlayerUpdateWay(false)
--    self.m_newChatLayer:setBtnVisible(true,true,false,false)
--    self.m_newChatLayer:setNodeAllOffset(self.m_pBtnOthers:getPositionX()-self.m_newChatLayer:getLeftBtnPosX(),0)
--    self.m_newChatLayer:setLeftBtnOffsetX(self.m_pBtnOthers:getPositionX()-self.m_newChatLayer:getLeftBtnPosX())
--    self.m_newChatLayer:setLeftBtnOffsetY(self.m_pBtnOthers:getPositionY()-self.m_newChatLayer:getLeftBtnPosY())
--    self.m_nodeLeft:addChild(self.m_newChatLayer,101)
--    self.m_pBtnOthers:hide()
--    self.m_pLbOtherNum:hide()

    --房间号
--    if PlayerInfo.getInstance():getIsShowRoomNo() then
--        local strRoomNo = string.format(LuaUtils.getLocalString("STRING_187"),PlayerInfo.getInstance():getCurrentRoomNo())
--        self.m_pLbRoomNo = cc.Label:createWithBMFont("public/font/11.fnt", strRoomNo)
--	    self.m_pLbRoomNo:setAnchorPoint(cc.p(0, 0.5))
--	    self.m_pLbRoomNo:setPosition(cc.p(28, 630))
--	    self.m_nodeLeft:addChild(self.m_pLbRoomNo)
--    end

--    --银商喇叭
--    self.m_rollMsgObj = RollMsg.create()
--    self.m_rollMsgObj:addTo(self.m_rootNode)
--    self.m_rollMsgObj:startRoll()

----    --结算UI
--    self.m_pNodeResult:setVisible(false)
--    self.m_resultBg = self.m_pNodeResult:getChildByName("Image_bg")
--    self.m_pNodeResultLabelName = self.m_resultBg:getChildByName("Text_zhuang_name")
--    self.m_pNodeResultLabelPoints = {}
--    self.m_pNodeResultLabelPoints[1] = self.m_resultBg:getChildByName("dianshu_1")
--    self.m_pNodeResultLabelPoints[2] = self.m_resultBg:getChildByName("dianshu_2")
--    self.m_pSpShuying = {}
--    self.m_pSpShuying[1] = self.m_resultBg:getChildByName("shuyingtag1")
--    self.m_pSpShuying[2] = self.m_resultBg:getChildByName("shuyingtag2")
--    self.m_pNodeResultLabelMine = {}
--    self.m_pNodeResultLabelMine[1] = self.m_resultBg:getChildByName("result_score")
--    self.m_pNodeResultLabelMine[2] = self.m_resultBg:getChildByName("jiesuanweiyazhu")
--    self.m_pNodeResultLabelMine[3] = self.m_resultBg:getChildByName("jiesuanweitouzhong")
--    self.m_pBtnCloseResult = self.m_resultBg:getChildByName("Button_close")
--    self.m_pNodeResultNodeRankName = {}
--    self.m_pNodeResultNodeRankScore = {}
--    for i=1,4 do
--        self.m_pNodeResultNodeRankName[i] = self.m_resultBg:getChildByName("Text_rankname"..i)
--        self.m_pNodeResultNodeRankScore[i] = self.m_resultBg:getChildByName("Text_rankscore"..i)
--    end

--    local pathskel = "game/bjl/effect/huanle30miaojiesuan_donghua/huanle30miaojiesuan_donghua.skel"
--    local pathAtlas = "game/bjl/effect/huanle30miaojiesuan_donghua/huanle30miaojiesuan_donghua.atlas"
--    self.jiesuanAnimo = sp.SkeletonAnimation:createWithBinaryFile(pathskel, pathAtlas)
--    self.jiesuanAnimo:setPosition(353,236)
--    self.m_resultBg:addChild(self.jiesuanAnimo)

--    --创建系统庄家动画
--    local pathskel = "game/bjl/effect_public/baccarat_zhuang/325_hl30_zhuangjia.json"
--    local pathAtlas = "game/bjl/effect_public/baccarat_zhuang/325_hl30_zhuangjia.atlas"
--    self.m_sysBankerAni = sp.SkeletonAnimation:createWithJsonFile(pathskel, pathAtlas)
--    if ClientConfig.getInstance():getIsMainChannel() then
--        self.m_sysBankerAni:setAnimation(0, "animation1", true)
--    else
--        self.m_sysBankerAni:setAnimation(0, "animation2", true)
--    end
--    self.m_sysBankerAni:setAnimation(0, "animation", true)
--    self.m_sysBankerAni:setToSetupPose()
--    self.m_sysBankerAni:setAnchorPoint(0.5, 0.5)
--    self.m_sysBankerAni:setPosition(-50, 0)
--    self.m_sysBankerAni:update(0)
--    self.m_sysBankerAni:addTo(self.m_pSpbankerTip)
--    self.m_sysBankerAni:setVisible(false)



    self.mTextRecord = UIAdapter:CreateRecord()
    self.m_nodeLeft:addChild(self.mTextRecord)
    self.mTextRecord:setAnchorPoint(cc.p(0, 1))
	local buttonsize = self.m_pBtnReturn:getContentSize()
	local buttonpoint = cc.p(self.m_pBtnReturn:getPosition())
	self.mTextRecord:setPosition(cc.p(buttonpoint.x + buttonsize.width / 2 + 3, buttonpoint.y + buttonsize.height / 2))
    if g_GameController.m_nRecordId and g_GameController.m_nRecordId ~= "" then
        self.mTextRecord:setString("牌局ID:"..g_GameController.m_nRecordId)
    end

end

--初始化ui组件的显示内容 断线重连后应重置
function BaccaratLayer:resetNodeView()
    --玩家信息显示
    self:updateUsrName()
    self:updateUsrGold()

    --庄家信息显示
  --  self:updateBankerInfo()

    --其他玩家数量
    self:updateOtherNum()

    --倒计时
    self:onTimeCountDown()

    --下注金额显示
    self:updateBetValueOfAll()

end

--初始化弹出菜单剪切区域
function BaccaratLayer:initClipNode()
    self.m_pBtnFullClose:setOpacity(0)
    self.m_pBtnFullClose:setVisible(false)
    local _posx, _ =  self.m_pBtnMenuPop:getPosition()
    local shap = cc.DrawNode:create()
    local width = self.m_menuBG:getContentSize().width
    local pointArr = {cc.p(0,300), cc.p(400, 300), cc.p(400, 690), cc.p(0, 690)}
    shap:drawPolygon(pointArr, 4, cc.c4f(255, 255, 255, 255), 2, cc.c4f(255, 255, 255, 255))
    self.m_pClippingMenu = cc.ClippingNode:create(shap)
    
    local diffX = 0
    if _posx + width/2 > display.size.width then
        diffX = display.size.width - width
    else
        diffX = _posx - width/2 + 10
    end

    self.m_pClippingMenu:setPosition(cc.p(diffX,0))
    self.m_nodeClip:addChild(self.m_pClippingMenu)
    self.m_menuBG:removeFromParent()
    self.m_pClippingMenu:addChild(self.m_menuBG)
    self.m_menuBG:setVisible(false)
end

--初始化音效音乐
function BaccaratLayer:initMenuView()
    --音乐 
    local music = AudioManager.getInstance():getMusicOn()
    if music then
        AudioManager.getInstance():setMusicOn(true)
        self.m_pBtnMusic:loadTextureNormal(BaccaratRes.IMG_MUSIC_ON, ccui.TextureResType.plistType)
    else
        AudioManager.getInstance():setMusicOn(false)
        AudioManager.getInstance():stopMusic()
        self.m_pBtnMusic:loadTextureNormal(BaccaratRes.IMG_MUSIC_OFF, ccui.TextureResType.plistType)
    end

    --音效
    local sound = AudioManager.getInstance():getSoundOn()
    if sound then
        AudioManager.getInstance():setSoundOn(true)
        self.m_pBtnSound:loadTextureNormal(BaccaratRes.IMG_SOUND_ON, ccui.TextureResType.plistType)
    else
        AudioManager.getInstance():setSoundOn(false)
        self.m_pBtnSound:loadTextureNormal(BaccaratRes.IMG_SOUND_OFF, ccui.TextureResType.plistType)
    end

    -- 播放背景音樂
    AudioManager.getInstance():stopMusic(false)
    AudioManager.getInstance():playMusic(BaccaratRes.SOUND_OF_BG)
end

--倒计时
function BaccaratLayer:initTimer()
    self.timerHandle = scheduler.scheduleGlobal(handler(self,self.onTimeCountDown), 1) 
end

--分辨率适配
function BaccaratLayer:setNodeFixPostion()
    local diffX = 145-(1624-display.size.width)/2

    -- 根容器节点
    self.m_rootNode:setPositionX(diffX)
   -- self.m_pNodeResult:setPositionX(self.m_pNodeResult:getPositionX()-(1624-display.size.width)/2)

    if LuaUtils.isIphoneXDesignResolution() then
        --左侧节点
        self.m_nodeLeft:setPositionX(self.m_nodeLeft:getPositionX() - IPHONEX_OFFSETX)
        local _node1 = self.m_nodeFlyChip:getChildByName("Node_self")
        _node1:setPositionX(_node1:getPositionX() - IPHONEX_OFFSETX)
        local _node2 = self.m_nodeFlyChip:getChildByName("Node_other")
        _node2:setPositionX(_node2:getPositionX() - IPHONEX_OFFSETX)

        --右侧节点
        self.m_nodeRight:setPositionX(self.m_nodeRight:getPositionX() + IPHONEX_OFFSETX)

        --顶部显示
        self.m_nodeSmallroad:setPositionX(self.m_nodeSmallroad:getPositionX() - IPHONEX_OFFSETX/2)

    end
end

--endregion

--region 按钮事件及绑定处理

--按钮事件
function BaccaratLayer:initBtnClickEvent()
    --绑定筹码点击
    for i = 1, ChipCount do
        self.m_pBtnBetChip[i]:addTouchEventListener(function(sender,eventType)
            --主动点击筹码才播放音效
            if eventType ~=ccui.TouchEventType.ended then
                    return
                end
            AudioManager.getInstance():playSound(BaccaratRes.SOUND_OF_JETTON)
            self:onBetNumClicked(i)
        end)
    end

    self.m_pBtnReturn:addTouchEventListener(handler(self,self.onReturnClicked)) 
    self.m_pBtnMusic:addTouchEventListener(handler(self,self.onMusicClicked)) 
    self.m_pBtnRule:addTouchEventListener(handler(self,self.onRuleClicked))
    self.m_pBtnMenuPop:addTouchEventListener(handler(self,self.onMenuClicked))
    self.m_pBtnFullClose:addTouchEventListener(handler(self,self.onMenuClicked))
    self.m_pBtnRoad:addTouchEventListener(handler(self,self.onTrendClicked))
    self.m_pBtnOthers:addTouchEventListener(handler(self,self.onOtherInfoClicked))  
    self.m_pBtnContinue:addTouchEventListener(handler(self,self.onBetContinueClicked))
    self.m_pBtnMenuBank:addTouchEventListener(handler(self,self.onRecordClicked)) 
    --self.m_pBtnCloseResult:addTouchEventListener(handler(self, self.onCloseResultClicked))

    --绑定点击投注区域
    for i = 1, 5 do
        local _node = self.m_nodeBet:getChildByName("Node_bet" .. i)
        local j = 1
        while true do
            local _betBtn = _node:getChildByTag(j)
            if not _betBtn then break end
            _betBtn:addTouchEventListener(function(sender,eventType)
                if eventType ~=ccui.TouchEventType.ended then
                    return
                end
                if self.m_bMoveIn then return end --在执行flyIn()时不响应玩家的下注操作（fix 在入场动画后会根据玩家的下注额 生成一次下注筹码）
                self:onBetClicked(i)
            end)
            j = j + 1
        end
    end
end 
function BaccaratLayer:onRecordClicked(sender,eventType)
     if eventType ~=ccui.TouchEventType.ended then
        return
    end
    local GameRecordLayer = GameRecordLayer.new(2,161)
    self:addChild(GameRecordLayer,100)    
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", {161})
end
function BaccaratLayer:onMenuClicked(sender,eventType)
     if eventType ~=ccui.TouchEventType.ended then
                    return
                end
    if self.m_bMenuMoving then
        return
    end
    self.m_bMenuMoving = true

    if self.m_menuBG:isVisible() then
        --菜单栏收起
        self.m_pBtnFullClose:setVisible(false)
        local callBack =  cc.CallFunc:create(function()
            self.m_pBtnMenuPop:loadTextureNormal(BaccaratRes.IMG_POP, ccui.TextureResType.plistType)
        end)
        local callBack2 =  cc.CallFunc:create(function()
            self.m_bMenuMoving = false
        end)
        showMenuPush(self.m_menuBG, callBack, callBack2, 2, 780)
    else
        --菜单栏放下
        self.m_pBtnFullClose:setVisible(true)
        self.m_menuBG:setPosition(cc.p(2,780))
        local callBack =  cc.CallFunc:create(function()
            self.m_pBtnMenuPop:loadTextureNormal(BaccaratRes.IMG_PUSH, ccui.TextureResType.plistType)
        end)
        local callBack2 =  cc.CallFunc:create(function()
            self.m_bMenuMoving = false
        end)
        showMenuPop(self.m_menuBG, callBack, callBack2, 2, 650)
    end
end


--其他玩家信息
function BaccaratLayer:onOtherInfoClicked(sender,eventType)
    if eventType ~=ccui.TouchEventType.ended then
                    return
                end
    AudioManager.getInstance():playSound(BaccaratRes.SOUND_OF_BUTTON)
      local ChatLayer = ChatLayer.new()
        self:addChild(ChatLayer); 
end
function BaccaratLayer:updateOnlineUserList()
    local infoLayer = BaccaratOtherInfoLayer.new()
    self.m_nodeDlg:addChild(infoLayer,4)
end
--退出
function BaccaratLayer:onReturnClicked(sender,eventType)
    if eventType ~=ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound(BaccaratRes.SOUND_OF_CLOSE)
     
    g_GameController:gameQuitRoomReq()
    UIAdapter:popScene()
    g_GameController.gameScene = nil
end

--点击音乐
function BaccaratLayer:onMusicClicked(sender,eventType)
    if eventType ~=ccui.TouchEventType.ended then
                    return
                end
    if self.m_bMenuMoving then
        return
    end
    AudioManager.getInstance():playSound(BaccaratRes.SOUND_OF_BUTTON) 
   local layer = GameSetLayer.new(161);
	self:addChild(layer); 
    layer:setLocalZOrder(100)
end
 

--筹码点击
function BaccaratLayer:onBetNumClicked(nIndex)
    if not nIndex then return end

    if self.m_nIndexChoose == nIndex then
        return
    end

    local score = BaccaratDataMgr.getInstance():getScoreByJetton(nIndex)
    if score and BaccaratDataMgr.getInstance():getUserScore() < score then
        return
    end
    
    --让先前选中的变成原始位置
    if self.m_nIndexChoose > 0 then
        --local posNowY = self.m_pBtnBetChip[self.m_nIndexChoose]:getPositionY()
        self.m_pBtnBetChip[self.m_nIndexChoose]:setPositionY(self.m_betChipPos[self.m_nIndexChoose].y)
    end
    self.m_pBtnBetChip[nIndex]:setPositionY(self.m_betChipPos[self.m_nIndexChoose].y + ChipOffsetY)

    self.m_nIndexChoose = nIndex
end

--下注区域点击
function BaccaratLayer:onBetClicked(nIndex) --点击下注

    if BaccaratDataMgr.getInstance():getGameState() ~= BaccaratDataMgr.eType_Bet then
        TOAST("非下注时间不能下注！")

    --不够下注金币
    elseif BaccaratDataMgr:getInstance():getUserScore() < BaccaratDataMgr.getInstance():getScoreByJetton(self.m_nIndexChoose) then
        --跳转充值
        TOAST("下注金币不足！")
    else --发送下注
        local betScore = BaccaratDataMgr.getInstance():getScoreByJetton(self.m_nIndexChoose)
        g_GameController:gameBetReq(nIndex, betScore*100)
    end
end

 
--点击续投
function BaccaratLayer:onBetContinueClicked(sender,eventType)
if eventType ~=ccui.TouchEventType.ended then
                    return
                end
    AudioManager.getInstance():playSound(BaccaratRes.SOUND_OF_BUTTON)

--    --庄家
--    if BaccaratDataMgr.getInstance():isBanker() then
--        self:showFloatmessage("BACCARAT_9")
--        return
--    end

    --非下注状态
    if BaccaratDataMgr.eType_Bet ~= BaccaratDataMgr.getInstance():getGameState() then
        self:showFloatmessage("BACCARAT_11")
        return
    end

    --上次未下注
--    if 0 == BaccaratDataMgr.getInstance():getContinueBetSize() then
--        self:showFloatmessage("DICE_3")
--        return
--    end

    --已续投
--    if BaccaratDataMgr.getInstance():getIsContinued() then
--        self:showFloatmessage("STRING_004")
--        return
--    end

    local chips = {}
    for i = 1, AreaCount do
        chips[i] = 0
    end

--    local totalBet = 0
    for i = 1, BaccaratDataMgr.getInstance():getContinueBetSize() do
        local bet = BaccaratDataMgr.getInstance():getContinueBetAtIndex(i)
        chips[bet.cbBetArea] = chips[bet.cbBetArea] + bet.lBetScore 
    end

--    --续投不够筹码
--    if BaccaratDataMgr.getInstance():getUserScore() < totalBet then
--        self:showMessageBox("go-recharge")
--    else --发送续投
   
    g_GameController:gameContinueBetReq() 
     self.m_pBtnContinue:setColor(cc.c3b(160,160,160))
     self.m_pBtnContinue:setEnabled(false)
      --  CMsgBaccarat.getInstance():sendMsgChipContinue(chips)
 --   end
end

 
--点击路子
function BaccaratLayer:onTrendClicked(sender,eventType)
if eventType ~=ccui.TouchEventType.ended then
                    return
                end
    AudioManager.getInstance():playSound(BaccaratRes.SOUND_OF_BUTTON)
    self:showTrendLayer()
end

--点击规则
function BaccaratLayer:onRuleClicked(sender,eventType)
if eventType ~=ccui.TouchEventType.ended then
                    return
                end
    if self.m_bMenuMoving then
        return
    end
    AudioManager.getInstance():playSound(BaccaratRes.SOUND_OF_BUTTON)

    local ruleLayer = BaccaratRuleLayer.new()
    self:addChild(ruleLayer,5)
end

--function BaccaratLayer:onCloseResultClicked()
--    AudioManager.getInstance():playSound(BaccaratRes.SOUND_OF_BUTTON)

--    local action = cc.Sequence:create(
--        cc.EaseBackIn:create(cc.ScaleTo:create(0.25, 0.0)),
--        cc.CallFunc:create(function()
--            self.m_pNodeResult:setVisible(false)
--        end),
--        cc.Hide:create())
--    self.m_resultBg:stopAllActions()
--    self.m_resultBg:runAction(action)
--end
 
--endregion

--region 系统事件注册及处理
function BaccaratLayer:initEvent()  --初始化事件
 
      
    addMsgCallBack(self,BaccaratEvent.MSG_GAME_BACCARAT_STATE,handler(self,self.event_GameState)) 
    addMsgCallBack(self,BaccaratEvent.MSG_GAME_BACCARAT_CHIP_SUCCESS,handler(self,self.event_ChipSuccess))
    addMsgCallBack(self,BaccaratEvent.MSG_GAME_BACCARAT_CHIP_CONTINUE,handler(self,self.event_ChipContinue))
    self:initTimer()

end

function BaccaratLayer:cleanEvent() --清理事件监听
   removeMsgCallBack(self,BaccaratEvent.MSG_GAME_BACCARAT_STATE) 
   removeMsgCallBack(self,BaccaratEvent.MSG_GAME_BACCARAT_CHIP_SUCCESS) 
   removeMsgCallBack(self,BaccaratEvent.MSG_GAME_BACCARAT_CHIP_CONTINUE) 
   removeMsgCallBack(self,BaccaratEvent.MSG_GAME_BACCARAT_UPDATERECORD)
    --注销倒计时
    if self.timerHandle then
        scheduler.unscheduleGlobal(self.timerHandle)
    end
end

function BaccaratLayer:startGameEvent()
    self.event_game_ = {  
        [Public_Events.MSG_GAME_NETWORK_FAILURE]  =  { func = self.onMsgEnterNetWorkFail, log = "网络中断",   },
        [Public_Events.MSG_GAME_ENTER_BACKGROUND] =  { func = self.onMsgEnterBackGround,  log = "切换至后台", },
        [Public_Events.MSG_GAME_ENTER_FOREGROUND] =  { func = self.onMsgEnterForeGround,  log = "切换至前台", },
        [Public_Events.MSG_GAME_RELOGIN_SUCCESS]  =  { func = self.onMsgReloginSuccess,   log = "重登录成功", },
        [Public_Events.MSG_GAME_EXIT]             = { func = self.event_ExitGame,         log = "退出游戏",   },
        [Public_Events.MSG_NETWORK_FAILURE]       = { func = self.event_NetFailure,       log = "游戏掉线",   },
    }
    for key, event in pairs(self.event_game_) do   --监听事件
         SLFacade:addCustomEventListener(key, handler(self, event.func), self.__cname)
    end
end

function BaccaratLayer:stopGameEvent()
    for key in pairs(self.event_game_) do   --监听事件
         SLFacade:removeCustomEventListener(key, self.__cname)
    end
    self.event_game_ = {}
end

function BaccaratLayer:onMsgEnterNetWorkFail()
    self:onMsgEnterBackGround()
end

--进入后台
function BaccaratLayer:onMsgEnterBackGround()
--    self:stopAllActions()
--    self:cleanEvent()
--    AudioManager:getInstance():stopAllSounds()
--    AudioManager.getInstance():stopMusic(true)
--    PlayerInfo.getInstance():setSitSuc(false)
end

--进入前台
function BaccaratLayer:onMsgEnterForeGround()

end

--重登录成功
function BaccaratLayer:onMsgReloginSuccess()
    self:resetUIView()
end

--退出游戏
function BaccaratLayer:event_ExitGame(msg)
    self:onMoveExitView(msg)
end 

--游戏掉线
function BaccaratLayer:event_NetFailure(msg)
    FloatMessage.getInstance():pushMessage("STRING_023")
    self:onMoveExitView()
end

--系统公告
function BaccaratLayer:event_System(msg)
    self:showFloatmessage(msg)
end

--银行数据
function BaccaratLayer:event_BankInfo(msg)
    self:updateUsrGold()    --用户分数
    self:updateJettonState()--筹码状态
end

--玩家分数
function BaccaratLayer:event_UserScore(msg)
    if BaccaratDataMgr.getInstance():getIsLoadGameSceneData() == false then
        return
    end

   if BaccaratDataMgr.getInstance():getGameState() ~= BaccaratDataMgr.eType_Award then
        self:updateUsrGold()
        self:updateJettonState()
    end
end

--游戏状态
function BaccaratLayer:event_GameState(str,msg)
    local state = BaccaratDataMgr.getInstance():getGameState()
      --更新续投
    self:updateBetContinueEnable()

    if g_GameController.m_nRecordId and g_GameController.m_nRecordId ~= "" then
        self.mTextRecord:setString("牌局ID:"..g_GameController.m_nRecordId)
    end

    if state == BaccaratDataMgr.eType_Wait then -- 空闲
        --self.m_pNodeResult:setVisible(false)
        --重置随机下注点
        self:resetChipPosArea()
        --更新历史记录
        --BaccaratDataMgr.getInstance():addGameRecordByOpen()
        self:notifyTrendRefresh()
    elseif state == BaccaratDataMgr.eType_Bet then -- 下注
        --self.m_pNodeResult:setVisible(false)
        --处理缓存的记录
        if self.m_pResultLayer and self.m_pResultLayer:isVisible() then
            self.m_pResultLayer:setVisible(false)
        end 
        if BaccaratDataMgr.getInstance():hasCacheRecord() then
            BaccaratDataMgr.getInstance():synCacheRecord()
            self:notifyTrendRefresh()
        end

        --清理UI
        self:cleanUI()

        --显示连庄
    --    self:bankerTips(false)

        --播放开始下注
        self:playBeginChipAni()

        --重置UI显示
        self:resetNodeView()
    elseif state == BaccaratDataMgr.eType_Award then -- 开奖
        --self.m_pNodeResult:setVisible(false)
        --备份一下当前用户积分
        self.m_oldScore = BaccaratDataMgr.getInstance():getUserScore()

        --清理倒计时动画
        self.m_isPlayDaojishi = false
        self:cleanZhuanChangAni()

        --播放停止下注
        self:playStopChipAni()
        self.m_pBtnContinue:setColor(cc.c3b(160,160,160))
         self.m_pBtnContinue:setEnabled(false)
    end

    --更新筹码按钮状态
    self:updateJettonState()

  

    --更新银行按钮
  --  self:updateBankBtnState()

    --通知银行可用状态
--    self:notifyBankEnable()
end

--投注成功
function BaccaratLayer:event_ChipSuccess(str,msg)
    --[[
        local msg = {}
        msg.wChairID = _buffer:readUShort()       --用户位置
        msg.cbBetArea = _buffer:readChar()        --筹码区域
        msg.lBetScore = _buffer:readLongLong()    --加注数目
        msg.cbAndroidUser = _buffer:readUChar()   --机器人标识
    ]]--

    if not msg then return end

    --更新下注金额
    self:updateBetValueOfAll()

    --更新筹码状态
    self:updateJettonState()

    --执行投注筹码动画
    self:playBetChipAni(msg.cbBetArea, msg.lBetScore, msg.wChairID)

    if msg.wChairID == Player:getAccountID() then
        --续投禁用
       -- self:updateBetContinueEnable()
        --更新自己分数
        self:updateUsrGold()
        --更新筹码状态
        self:updateJettonState()
    end

end

--下注失败
function BaccaratLayer:event_ChipFail(msg)
    FloatMessage.getInstance():pushMessage("BACCARAT_7")
end

--续投成功
function BaccaratLayer:event_ChipContinue(str,msg)
    local bets = BaccaratDataMgr.getInstance():getOtherBetting()
    for i, bet in pairs(bets) do
        local betScore = BaccaratDataMgr.getInstance():getScoreByJetton(bet.wJettonIndex)
        self:playBetChipAni(bet.wChipIndex, betScore, bet.wChairId)
        if bet.wChairId == Player:getAccountID() then
            BaccaratDataMgr.getInstance():addUserBet( { cbBetArea = bet.wChipIndex, lBetScore = betScore } )
        end
    end

    --清空续投历史
    BaccaratDataMgr.getInstance():cleanOtherBetting()

    self:updateBetValueOfAll()
    self:updateBetContinueEnable()

    if msg == string.format("%d", Player:getAccountID()) then
        FloatMessage.getInstance():pushMessage("续投成功")
    end
end

----庄家换人信息
--function BaccaratLayer:event_BankerInfo(msg)
--    self:updateBankerInfo()

--    --更换庄家
--    if msg == "changebanker" then
--        self:bankerTips(true)
--    end
--end

--function BaccaratLayer:event_BankerApply(msg)
--    if nil == msg or nil == msg.wCancelUser then
--        return
--    end

--    if msg.wCancelUser == Player:getAccountID() then
--        FloatMessage.getInstance():pushMessage(BACCARAT_12)
--    end

--    self:updateBankerInfo()
--end

--function BaccaratLayer:event_CancelBanker(msg)
--    self:updateBankerInfo()

--    --if not BaccaratDataMgr.getInstance():isBanker() and msg.wCancelUser == Player:getAccountID() then
--    --    self:showFloatmessage("HORSE_11")
--    --end
--end

--倒计时
function BaccaratLayer:onTimeCountDown()
    --每秒刷新其他玩家数量
    self:updateOtherNum()
    self.m_pImgTimer:setVisible(true)
    --游戏状态
    if BaccaratDataMgr.eType_Bet == BaccaratDataMgr.getInstance():getGameState() then
        self.m_pImgState:loadTexture(BaccaratRes.IMG_GAMECHIP,ccui.TextureResType.plistType)
    else
        self.m_pImgState:loadTexture(BaccaratRes.IMG_GAMEOPEN,ccui.TextureResType.plistType)    
    end

    --倒计时
    local ts = BaccaratDataMgr.getInstance():getStateTime()
    ts = ts < 1 and 0 or ts - 1
    BaccaratDataMgr.getInstance():setStateTime(ts)
    self.m_pLbTimerNum:setString(("%d").format(ts))

    --显示最后3秒倒计时动画
    if ts < 4 and not self.m_isPlayDaojishi and BaccaratDataMgr.eType_Bet == BaccaratDataMgr.getInstance():getGameState() then
        self.m_isPlayDaojishi = true
        self:playLastTimeAni(ts)
    end
end

--endregion

--region 动画相关

--开场动画进入
function BaccaratLayer:flyIn()
    local deskAniTime  = 0.3
    local otherAniTime = 0.4
    local offset = 15
    self.m_bMoveIn = true

    --桌子下方进入
    local posx, posy = self.m_pImgDesk:getPosition()
    self.m_pImgDesk:setPositionY(posy - 400)
    self.m_pImgDesk:setScale(1.3)
    local tableMove = cc.MoveTo:create(deskAniTime, cc.p(posx,posy))
    local tableScale = cc.ScaleTo:create(deskAniTime, 1)
    local tableSpawn = cc.Spawn:create(tableScale, tableMove)
    local callfun = cc.CallFunc:create(function()
                        self.m_bMoveIn = false
                        self:viewBetChip()
                    end)
    self.m_pImgDesk:runAction(cc.Sequence:create(tableSpawn, callfun))

    --筹码区下方进入
    posx, posy = self.m_nodeBottom:getPosition()
    self.m_nodeBottom:setPositionY(posy - 400)
    self.m_nodeBottom:setOpacity(0)
    local bottomMove = cc.MoveTo:create(otherAniTime, cc.p(posx,posy + offset))
    local bottomMove2 = cc.MoveTo:create(0.2, cc.p(posx,posy))
    local spawn = cc.Spawn:create(cc.FadeIn:create(0.2), bottomMove)
    local seq = cc.Sequence:create(spawn, bottomMove2)
    self.m_nodeBottom:runAction(seq)

    --玩家信息区域
    posx, posy = self.m_selfPanel:getPosition()
    self.m_selfPanel:setPositionY(posy - 400)
    self.m_selfPanel:setOpacity(0)
    self.m_selfPanel:runAction(seq:clone())

    --上部节点
    posx, posy = self.m_nodeTop:getPosition()
    self.m_nodeTop:setPositionY(posy + 400)
    self.m_nodeTop:setOpacity(0)
    local topMove = cc.MoveTo:create(otherAniTime, cc.p(posx,posy - offset))
    local topMove2 = cc.MoveTo:create(0.2, cc.p(posx,posy))
    spawn = cc.Spawn:create(cc.FadeIn:create(0.2), topMove)
    seq = cc.Sequence:create(spawn, topMove2)
    self.m_nodeTop:runAction(seq)

    --右侧节点
    posx, posy = self.m_nodeRight:getPosition()
    self.m_nodeRight:setPositionY(posy + 400)
    self.m_nodeRight:setOpacity(0)
    self.m_nodeRight:runAction(seq:clone())

    --退出和其他玩家按钮
    posx, posy = self.m_pBtnReturn:getPosition()
    self.m_pBtnReturn:setPositionY(posy + 400)
    self.m_pBtnReturn:setOpacity(0)
    topMove = cc.MoveTo:create(otherAniTime, cc.p(posx,posy - offset))
    topMove2 = cc.MoveTo:create(0.2, cc.p(posx,posy))
    spawn = cc.Spawn:create(cc.FadeIn:create(0.2), topMove)
    seq = cc.Sequence:create(spawn, topMove2)
    self.m_pBtnReturn:runAction(seq)

--    posx, posy = self.m_newChatLayer:getPosition()
--    self.m_newChatLayer:setPositionY(posy + 400)
--    self.m_newChatLayer:setOpacity(0)
--    topMove = cc.MoveTo:create(otherAniTime, cc.p(posx,posy - offset))
--    topMove2 = cc.MoveTo:create(0.2, cc.p(posx,posy))
--    spawn = cc.Spawn:create(cc.FadeIn:create(0.2), topMove)
--    seq = cc.Sequence:create(spawn, topMove2)
--    self.m_newChatLayer:runAction(seq)

    --房间号
--    if self.m_pLbRoomNo ~= nil then 
--        posx, posy = self.m_pLbRoomNo:getPosition()
--        self.m_pLbRoomNo:setPositionY(posy + 400)
--        self.m_pLbRoomNo:setOpacity(0)
--        topMove = cc.MoveTo:create(otherAniTime, cc.p(posx,posy - offset))
--        topMove2 = cc.MoveTo:create(0.2, cc.p(posx,posy))
--        spawn = cc.Spawn:create(cc.FadeIn:create(0.2), topMove)
--        seq = cc.Sequence:create(spawn, topMove2)
--        self.m_pLbRoomNo:runAction(seq)
--    end
end

--开始下注动画
function BaccaratLayer:playBeginChipAni()
    AudioManager.getInstance():playSound(BaccaratRes.SOUND_OF_WAGER_START)    
    local armature = ccs.Armature:create(BaccaratRes.ANI_FILE_ZHUANCHANG) 
    armature:setPosition(self.m_zcAniPos)
    self.m_nodeZCAni:addChild(armature)
    armature:getAnimation():play(BaccaratRes.ANI_NAME_BEGINCHIP , -1, -1)  
    local animationEvent = function (armatureBack, movementType, movementID)
        if movementType == ccs.MovementEventType.complete then
            armature:removeFromParent()
        end
    end
    armature:getAnimation():setMovementEventCallFunc(animationEvent)
    return armature
end

--停止下注动画
function BaccaratLayer:playStopChipAni()
    AudioManager.getInstance():playSound(BaccaratRes.SOUND_OF_WAGER_STOP)    
    local armature = ccs.Armature:create(BaccaratRes.ANI_FILE_ZHUANCHANG) 
    armature:setPosition(self.m_zcAniPos)
    self.m_nodeZCAni:addChild(armature)
    armature:getAnimation():play(BaccaratRes.ANI_NAME_STOPCHIP, -1, -1)  
    local animationEvent = function (armatureBack, movementType, movementID)
        if movementType == ccs.MovementEventType.complete then
            armature:removeFromParent()
            self:playSendCardAni()
        end
    end
    armature:getAnimation():setMovementEventCallFunc(animationEvent)
    return armature
end

--下注筹码动画
function BaccaratLayer:playBetChipAni(betArea, betScore, chairId)
    if betScore <= 0 then
        return
    end
    if BaccaratDataMgr.getInstance():getGameState() == BaccaratDataMgr.eType_Bet then
        local isSelf = Player:getAccountID() == chairId
        local beginPos = isSelf and self:getSelfChipBeginPos(betScore) or self:getOtherChipBeginPosition()
        local idx, endPos = self:getChipEndPosition(betArea, isSelf)
        local sp = self:flyJetton(beginPos, endPos, betScore, isSelf)
        sp:setTag(betScore)
        if isSelf then
            table.insert(self.m_vJettonOfMine, { pChipSpr = sp, nIdx = idx, wChipIndex = betArea})
        else
            table.insert(self.m_vJettonOfOther, { pChipSpr = sp, nIdx = idx, wChipIndex = betArea })
        end
    end
end

--筹码飞行
function BaccaratLayer:flyJetton(beginPos, endPos, betScore, isSelf)
    local sp = self:getSpriteOfJetton(betScore)
            sp:setName(betScore)
            sp:setScale(FlyChipScale+0.05)
            sp:setAnchorPoint(cc.p(0.5, 0.5))
            sp:setPosition(beginPos)
            sp:addTo(self.m_nodeFlyChip)

    local soundPath = betScore > 10 and BaccaratRes.SOUND_OF_BETHIGH or BaccaratRes.SOUND_OF_BETLOW

    local call = cc.CallFunc:create(function ()
        if isSelf then -- 自己的音效立刻播放
             AudioManager.getInstance():playSound(soundPath)
        else
            if not self.m_isPlayBetSound then
                AudioManager.getInstance():playSound(soundPath)
                -- 尽量避免重复播放
                self:doSomethingLater(function() self.m_isPlayBetSound  = false end, math.random(2,4)/10)
                self.m_isPlayBetSound = true
            end
        end
    end)

    local move_distance = cc.pGetDistance(beginPos,endPos)
    local move_time = move_distance/BR_CHIP_SPEED
    local forward = cc.MoveTo:create(move_time, endPos)
    local eMove = cc.EaseOut:create(forward,move_time+0.3)
    local scale_action = cc.ScaleTo:create(0.15,FlyChipScale)
    --local Value = isNeedDelay and ( math.random(0,10) % 100)/100.0 or 0 
    local seq = cc.Sequence:create(call, eMove,scale_action)
    sp:runAction(seq)

    return sp
end

--庄家提示信息
 
--发牌动画
function BaccaratLayer:playSendCardAni()
    self:sendCard(1)
end

--单张发牌和开牌
function BaccaratLayer:sendCard(nIndex)
    local card = BaccaratDataMgr.getInstance():getCardAtIndex(self.m_sendCardConfig[nIndex].cardType, self.m_sendCardConfig[nIndex].cardIndex)
    if not card then return end
    local poker = Poker:create()
    poker:setData(card.iColor, card.iValue)
    poker:setPosition(self.m_sendCardConfig.beginPos)
    poker:setBack()
    poker:setRotation(self.m_sendCardConfig.beginRotate)
    poker:setScale(self.m_sendCardConfig.beginScaleVal)
    poker:addTo(self.m_nodeCardAni)

    local moveTo = cc.MoveTo:create(self.m_sendCardConfig.sendTs, self.m_sendCardConfig[nIndex].endPos)
    local rotateBy = cc.RotateBy:create(self.m_sendCardConfig.sendTs, 360 - self.m_sendCardConfig.beginRotate)
    local scaleTo = cc.ScaleTo:create(self.m_sendCardConfig.sendTs, self.m_sendCardConfig.endScaleVal)
    local spawn = cc.Spawn:create(moveTo, rotateBy, scaleTo)

    local callBack1 = cc.CallFunc:create(
        function ()
            if nIndex < self.m_sendCardConfig.cardNum then
                self:sendCard(nIndex + 1)
            else
                --if 2 == nIndex then --闲家发第二张牌
                    self:xianAddCardAni(1)
--                    local point = BaccaratDataMgr.getInstance():getResultPoints(BaccaratDataMgr.ePlace_Zhuang, 2)
--                    self.m_zhuangPointAni = self:playCardPointAni(self.m_sendCardConfig.pointAniPos[BaccaratDataMgr.ePlace_Zhuang][3], point)
--                    AudioManager:getInstance():playSound( string.format( BaccaratRes.SOUND_OF_ZPOINT, point))
                --end
            end
        end
    )

    local callBack2 = cc.CallFunc:create(function ()
        poker:showCard()
    end)

    local seq = cc.Sequence:create(spawn,
        cc.ScaleTo:create(self.m_sendCardConfig.openCardTs, 0.03, self.m_sendCardConfig.endScaleVal),
        callBack2,
        cc.ScaleTo:create(self.m_sendCardConfig.openCardTs, self.m_sendCardConfig.endScaleVal, self.m_sendCardConfig.endScaleVal),
        callBack1)

    AudioManager:getInstance():playSound(BaccaratRes.SOUND_OF_CARD)
    self.m_sendCard[nIndex] = poker
    poker:runAction(seq)
end

--补牌动画
function BaccaratLayer:playAddCardAni()
    --是否显示加牌
    if BaccaratDataMgr.getInstance():getCardCount(BaccaratDataMgr.ePlace_Xian) > 2 then
        --self:doSomethingLater(function() self:xianAddCardAni(2) end, self.m_sendCardConfig.pointSoundDelay)
        self:xianAddCardAni(2)
    elseif BaccaratDataMgr.getInstance():getCardCount(BaccaratDataMgr.ePlace_Zhuang) > 2 then
        --self:doSomethingLater(function() self:zhuangAddCardAni(2) end, self.m_sendCardConfig.pointSoundDelay)
        self:zhuangAddCardAni(2)
    else
        --不需要补牌 直接播放结果动画
        --self:doSomethingLater(function() self:playCardResultAni() end, 1.2)
        self:playCardResultAni()
    end
end

--闲家补牌
function BaccaratLayer:xianAddCardAni(nIndex)
    --补的牌在发完后以牌背方式横向显示
    local poker = Poker:create()
    poker:setPosition(self.m_sendCardConfig.beginPos)
    poker:setBack()
    poker:setRotation(self.m_sendCardConfig.beginRotate)
    poker:setScale(self.m_sendCardConfig.beginScaleVal)
    poker:addTo(self.m_nodeCardAni)

    local endpos = cc.p(self.m_xianCardEndPos.x + nIndex*self.m_sendCardConfig.cardOffsetX, self.m_xianCardEndPos.y + self.m_sendCardConfig.cardOffsetY)

    local moveTo = cc.MoveTo:create(self.m_sendCardConfig.sendTs, endpos)
    local rotateBy = cc.RotateBy:create(self.m_sendCardConfig.sendTs, 90 - self.m_sendCardConfig.beginRotate)
    local scaleTo = cc.ScaleTo:create(self.m_sendCardConfig.sendTs, self.m_sendCardConfig.endScaleVal)
    local spawn = cc.Spawn:create(moveTo, rotateBy, scaleTo)

    local callBack1 = cc.CallFunc:create(
        function ()
            poker:removeFromParent()
            self:xianOpenCard(nIndex)
        end
    )

    AudioManager:getInstance():playSound(BaccaratRes.SOUND_OF_CARD)
    local seq = cc.Sequence:create(spawn, callBack1)
    poker:runAction(seq)
    if 2 == nIndex then
        self.m_sendCard[1]:runAction(cc.MoveBy:create(self.m_sendCardConfig.sendTs, cc.p(-self.m_sendCardConfig.cardOffsetX/2, 0)))
        self.m_sendCard[3]:runAction(cc.MoveBy:create(self.m_sendCardConfig.sendTs, cc.p(-self.m_sendCardConfig.cardOffsetX/2, 0)))
    end
end

--闲家开最后一张牌
function BaccaratLayer:xianOpenCard(nIndex)
    local card = BaccaratDataMgr.getInstance():getCardAtIndex(BaccaratDataMgr.ePlace_Xian, nIndex + 1)
    if not card then return end
    local armature = CacheManager:addSpineTo(self.m_nodeCardAni, "game/baccarat/effect/huanle30s_fanpai/huanle30s_fanpai", 0, ".json", 1)
    armature:setAnimation(0, "Animation1", false)
    armature:runAction(cca.seq({cca.delay(1), cca.callFunc(function()
        armature:removeFromParent()
        self:xianViewCard(nIndex)
    end), cca.removeSelf()}))
    -- local armature = ccs.Armature:create(BaccaratRes.ANI_FILE_FANPAI)
    -- if armature == nil then
    --     return
    -- end
    local pos = cc.p(self.m_xianCardEndPos.x + nIndex*self.m_sendCardConfig.cardOffsetX, self.m_xianCardEndPos.y + self.m_sendCardConfig.cardOffsetY)
    armature:setPosition(pos) 
    -- self.m_nodeCardAni:addChild(armature)

    local pathVal = string.format("value-%d-%d.png", card.iValue, card.iColor%2)
    local pathColor = string.format("color-%d.png", card.iColor), ccui.TextureResType.plistType
    -- -- self:replaceArmatureSprite(armature, self.m_sendCardConfig.boneName.VALUE, pathVal)
    -- -- self:replaceArmatureSprite(armature, self.m_sendCardConfig.boneName.COLOR, pathColor)
    -- local animationEvent = function(armatureBack, movementType, movementID)
    --     if movementType == ccs.MovementEventType.complete 
    --     or movementType == ccs.MovementEventType.loopComplete
    --     then
    --         armature:removeFromParent()
    --         self:xianViewCard(nIndex)
    --     end
    -- end
    -- armature:getAnimation():setMovementEventCallFunc(animationEvent)
    -- armature:getAnimation():playWithIndex(0)
    local node = display.newNode()
    armature:addChild(node, 1)
    node:setVisible(false)
    node:runAction(cc.Sequence:create(cc.DelayTime:create(0.6), cc.Show:create(), nil))

    local imageVal = ccui.ImageView:create(pathVal, ccui.TextureResType.plistType)
    local imageColor = ccui.ImageView:create(pathColor, ccui.TextureResType.plistType)
    node:addChild(imageVal, 1)
    node:addChild(imageColor, 1)
    imageVal:setAnchorPoint(cc.p(0.5,0.5))
    imageColor:setAnchorPoint(cc.p(0.5,0.5))
    imageVal:setPosition(cc.p(28.70, 43.63))
    imageVal:setRotation(54)
    imageColor:setPosition(cc.p(9.64, 30.61))
    imageColor:setRotation(54)
    imageVal:setScale(0.6)
    imageColor:setScale(0.6)

end

--闲家最后一张牌显示
function BaccaratLayer:xianViewCard(nIndex)
    local card = BaccaratDataMgr.getInstance():getCardAtIndex(BaccaratDataMgr.ePlace_Xian, nIndex + 1)
    if not card then return end
    local poker = Poker:create()
    poker:setData(card.iColor, card.iValue)
    poker:setPosition(cc.p(self.m_xianCardEndPos.x + (2 + nIndex)*self.m_sendCardConfig.cardOffsetX/2, self.m_xianCardEndPos.y))
    poker:showCard()
    poker:addTo(self.m_nodeCardAni)

    --牌的抖动动画
    poker:setScale(1)
    poker:setRotation(-40)

    local rotae1 = cc.RotateTo:create(3 * 0.017, -15)
    local rotae2 = cc.RotateTo:create(4 * 0.017, 10)
    local rotae3 = cc.RotateTo:create(6 * 0.017, 0)
    local scale1 = cc.ScaleTo:create(3 * 0.017, self.m_sendCardConfig.endScaleVal)
    local callBack = cc.CallFunc:create(
        function ()
            self:zhuangAddCardAni(nIndex)
        end
    )
    local seq = cc.Sequence:create(cc.Spawn:create(rotae1, scale1), rotae2, rotae3, callBack)
    poker:runAction(seq)
    if 1 == nIndex then
        self.m_sendCard[3] = poker
    end
end

--庄家补牌
function BaccaratLayer:zhuangAddCardAni(nIndex)
    if 2 == nIndex and BaccaratDataMgr.getInstance():getCardCount(BaccaratDataMgr.ePlace_Zhuang) < 3 then
        self:playCardResultAni()
        return
    end

    local poker = Poker:create()
    poker:setPosition(self.m_sendCardConfig.beginPos)
    poker:setBack()
    poker:setRotation(self.m_sendCardConfig.beginRotate)
    poker:setScale(self.m_sendCardConfig.beginScaleVal)
    poker:addTo(self.m_nodeCardAni)

    local endpos = cc.p(self.m_zhuangCardEndPos.x + nIndex*self.m_sendCardConfig.cardOffsetX, self.m_zhuangCardEndPos.y + self.m_sendCardConfig.cardOffsetY)
    local moveTo = cc.MoveTo:create(self.m_sendCardConfig.sendTs, endpos)
    local rotateBy = cc.RotateBy:create(self.m_sendCardConfig.sendTs, 90 - self.m_sendCardConfig.beginRotate)
    local scaleTo = cc.ScaleTo:create(self.m_sendCardConfig.sendTs, self.m_sendCardConfig.endScaleVal)
    local spawn = cc.Spawn:create(moveTo, rotateBy, scaleTo)

    local callBack1 = cc.CallFunc:create(
        function ()
            poker:removeFromParent()
            self:zhuangOpenCard(nIndex)
        end
    )

    AudioManager:getInstance():playSound(BaccaratRes.SOUND_OF_CARD)
    local seq = cc.Sequence:create(spawn, callBack1)
    poker:runAction(seq)
    if 2 == nIndex then
        self.m_sendCard[2]:runAction(cc.MoveBy:create(self.m_sendCardConfig.sendTs, cc.p(-self.m_sendCardConfig.cardOffsetX/2, 0)))
        self.m_sendCard[4]:runAction(cc.MoveBy:create(self.m_sendCardConfig.sendTs, cc.p(-self.m_sendCardConfig.cardOffsetX/2, 0)))
    end
end

--庄家开最后一张牌
function BaccaratLayer:zhuangOpenCard(nIndex)
    local card = BaccaratDataMgr.getInstance():getCardAtIndex(BaccaratDataMgr.ePlace_Zhuang, nIndex + 1)
    if not card then
        return
    end
    
    local armature = CacheManager:addSpineTo(self.m_nodeCardAni, "game/baccarat/effect/huanle30s_fanpai/huanle30s_fanpai", 0, ".json", 1)
    armature:setAnimation(0, "Animation1", false)
    armature:runAction(cca.seq({cca.delay(1), cca.callFunc(function()
        self:zhuangViewCard(nIndex)
    end), cca.removeSelf()}))

    -- local armature = ccs.Armature:create(BaccaratRes.ANI_FILE_FANPAI)
    -- if armature == nil then
    --     return
    -- end
    local pos = cc.p(self.m_zhuangCardEndPos.x + 2*self.m_sendCardConfig.cardOffsetX, self.m_zhuangCardEndPos.y + self.m_sendCardConfig.cardOffsetY)
    armature:setPosition(pos) 
    -- self.m_nodeCardAni:addChild(armature)


    local pathVal = string.format("value-%d-%d.png", card.iValue, card.iColor%2)
    local pathColor = string.format("color-%d.png", card.iColor), ccui.TextureResType.plistType
  --  self:replaceArmatureSprite(armature, self.m_sendCardConfig.boneName.VALUE, pathVal)
  --  self:replaceArmatureSprite(armature, self.m_sendCardConfig.boneName.COLOR, pathColor)

    -- local animationEvent = function(armatureBack, movementType, movementID)
    --     if movementType == ccs.MovementEventType.complete 
    --     or movementType == ccs.MovementEventType.loopComplete
    --     then
    --         armature:removeFromParent()
    --         self:zhuangViewCard(nIndex)
    --     end
    -- end
    -- armature:getAnimation():setMovementEventCallFunc(animationEvent)
    -- armature:getAnimation():playWithIndex(0)

    local node = display.newNode()
    armature:addChild(node, 1)
    node:setVisible(false)
    node:runAction(cc.Sequence:create(cc.DelayTime:create(0.6), cc.Show:create(), nil))

    local imageVal = ccui.ImageView:create(pathVal, ccui.TextureResType.plistType)
    local imageColor = ccui.ImageView:create(pathColor, ccui.TextureResType.plistType)
    node:addChild(imageVal, 1)
    node:addChild(imageColor, 1)
    imageVal:setAnchorPoint(cc.p(0.5,0.5))
    imageColor:setAnchorPoint(cc.p(0.5,0.5))
    imageVal:setPosition(cc.p(28.70, 43.63))
    imageVal:setRotation(54)
    imageColor:setPosition(cc.p(9.64, 30.61))
    imageColor:setRotation(54)
    imageVal:setScale(0.6)
    imageColor:setScale(0.6)
end

--庄家最后一张牌显示
function BaccaratLayer:zhuangViewCard(nIndex)
    local card = BaccaratDataMgr.getInstance():getCardAtIndex(BaccaratDataMgr.ePlace_Zhuang, nIndex + 1)
    if not card then return end
    local poker = Poker:create()
    poker:setData(card.iColor, card.iValue)
    poker:setPosition(cc.p(self.m_zhuangCardEndPos.x + (2 + nIndex)*self.m_sendCardConfig.cardOffsetX/2, self.m_zhuangCardEndPos.y))
    poker:showCard()

    poker:addTo(self.m_nodeCardAni)

    --牌的抖动动画
    poker:setScale(1)
    poker:setRotation(-40)

    local rotae1 = cc.RotateTo:create(3 * 0.017, -15)
    local rotae2 = cc.RotateTo:create(4 * 0.017, 10)
    local rotae3 = cc.RotateTo:create(6 * 0.017, 0)
    local scale1 = cc.ScaleTo:create(3 * 0.017, self.m_sendCardConfig.endScaleVal)
    local callBack = cc.CallFunc:create(
        function ()
            if 1 == nIndex then
                self:playAddCardAni()
            else
                self:playCardResultAni()
            end
        end
    )
    local seq = cc.Sequence:create(cc.Spawn:create(rotae1, scale1), rotae2, rotae3, callBack)
    poker:runAction(seq)
    if 1 == nIndex then
        self.m_sendCard[4] = poker
    end
end

--播放点数 牌型 和胜利动画
function BaccaratLayer:playCardResultAni()
    local countxian   = 0
    local countzhuang = 0
    local pointxian   = 0
    local pointzhuang = 0
    local isDuiZi     = false
    local isTianWang  = false
    local posx        = 0
    local posy        = 0
    local imgWidth    = 66 + 10

    --region 闲家
    --点数
    countxian = BaccaratDataMgr.getInstance():getCardCount(BaccaratDataMgr.ePlace_Xian)
    pointxian = BaccaratDataMgr.getInstance():getResultPoints(BaccaratDataMgr.ePlace_Xian, countxian)

    --牌型
    isDuiZi = BaccaratDataMgr.getInstance():getResultIsTwoPair(BaccaratDataMgr.ePlace_Xian)
    isTianWang = pointxian > 7

    --计算牌型动画的展示原点
    posx = self.m_xianCardEndPos.x + 2*self.m_sendCardConfig.cardOffsetX/2
    posy = self.m_xianCardEndPos.y - 10

    if isDuiZi and isTianWang then
   --     AudioManager:getInstance():playSound(BaccaratRes.SOUND_OF_TIANWANG)
        local pos1 = cc.p(posx - imgWidth/2, posy)
        local pos2 = cc.p(posx + imgWidth/2, posy)
        self:playCardTypeAni(pos1, BaccaratRes.ANI_CARDTYPE_NAME.NAME_DUIZI)
    --    self:playCardTypeAni(pos2, BaccaratRes.ANI_CARDTYPE_NAME.NAME_TIANWANG)
    elseif isDuiZi then
  --      AudioManager:getInstance():playSound(BaccaratRes.SOUND_OF_TIANWANG)
        self:playCardTypeAni(cc.p(posx, posy), BaccaratRes.ANI_CARDTYPE_NAME.NAME_DUIZI)
    elseif isTianWang then
       -- AudioManager:getInstance():playSound(BaccaratRes.SOUND_OF_TIANWANG)
     --   self:playCardTypeAni(cc.p(posx, posy), BaccaratRes.ANI_CARDTYPE_NAME.NAME_TIANWANG)
    end

    self:playCardPointAni(self.m_sendCardConfig.pointAniPos[BaccaratDataMgr.ePlace_Xian][3], pointxian)
    --endregion

    --region 庄家
    --点数
    countzhuang = BaccaratDataMgr.getInstance():getCardCount(BaccaratDataMgr.ePlace_Zhuang)
    pointzhuang = BaccaratDataMgr.getInstance():getResultPoints(BaccaratDataMgr.ePlace_Zhuang, countzhuang)

    --牌型
    isDuiZi = BaccaratDataMgr.getInstance():getResultIsTwoPair(BaccaratDataMgr.ePlace_Zhuang)
    isTianWang = pointzhuang > 7

    --计算牌型动画的展示原点
    posx = self.m_zhuangCardEndPos.x + 2*self.m_sendCardConfig.cardOffsetX/2
    posy = self.m_zhuangCardEndPos.y - 10

    if isDuiZi and isTianWang then
       -- AudioManager:getInstance():playSound(BaccaratRes.SOUND_OF_TIANWANG)
        local pos1 = cc.p(posx - imgWidth/2, posy)
        local pos2 = cc.p(posx + imgWidth/2, posy)
        self:playCardTypeAni(pos1, BaccaratRes.ANI_CARDTYPE_NAME.NAME_DUIZI)
    --    self:playCardTypeAni(pos2, BaccaratRes.ANI_CARDTYPE_NAME.NAME_TIANWANG)
    elseif isDuiZi then
     --   AudioManager:getInstance():playSound(BaccaratRes.SOUND_OF_TIANWANG)
        self:playCardTypeAni(cc.p(posx, posy), BaccaratRes.ANI_CARDTYPE_NAME.NAME_DUIZI)
    elseif isTianWang then
      --  AudioManager:getInstance():playSound(BaccaratRes.SOUND_OF_TIANWANG)
   --     self:playCardTypeAni(cc.p(posx, posy), BaccaratRes.ANI_CARDTYPE_NAME.NAME_TIANWANG)
    end
    self:playCardPointAni(self.m_sendCardConfig.pointAniPos[BaccaratDataMgr.ePlace_Zhuang][3], pointzhuang)
    --endregion

    --获胜动画
    local soundStr = ""
    if BaccaratDataMgr.getInstance():getResultIsTongDianPing() then --同点平
        posx = self.m_xianCardEndPos.x + 2*self.m_sendCardConfig.cardOffsetX/2
        posy = self.m_xianCardEndPos.y + 75
    --    self:playResultPingAni(cc.p(posx, posy), BaccaratRes.ANI_CARDTYPE_NAME.NAME_SAMEPING)

        posx = self.m_zhuangCardEndPos.x + 2*self.m_sendCardConfig.cardOffsetX/2
        posy = self.m_zhuangCardEndPos.y + 75
    --    self:playResultPingAni(cc.p(posx, posy), BaccaratRes.ANI_CARDTYPE_NAME.NAME_SAMEPING)

        soundStr = BaccaratRes.SOUND_OF_PING
        --AudioManager:getInstance():playSound(BaccaratRes.SOUND_OF_PING)
    elseif pointxian == pointzhuang then --平
        posx = self.m_xianCardEndPos.x + 2*self.m_sendCardConfig.cardOffsetX/2
        posy = self.m_xianCardEndPos.y + 75
        self:playResultPingAni(cc.p(posx, posy), BaccaratRes.ANI_CARDTYPE_NAME.NAME_PING)

        posx = self.m_zhuangCardEndPos.x + 2*self.m_sendCardConfig.cardOffsetX/2
        posy = self.m_zhuangCardEndPos.y + 75
        self:playResultPingAni(cc.p(posx, posy), BaccaratRes.ANI_CARDTYPE_NAME.NAME_PING)

        soundStr = BaccaratRes.SOUND_OF_PING
        --AudioManager:getInstance():playSound(BaccaratRes.SOUND_OF_PING)
    elseif pointxian > pointzhuang then--闲赢
        posx = self.m_xianCardEndPos.x + 2*self.m_sendCardConfig.cardOffsetX/2
        posy = self.m_xianCardEndPos.y + 75
        self:playResultWinAni(cc.p(posx, posy))
        soundStr = BaccaratRes.SOUND_OF_XIANWIN
        --AudioManager:getInstance():playSound(BaccaratRes.SOUND_OF_XIANWIN)
    else --庄赢
        posx = self.m_zhuangCardEndPos.x + 2*self.m_sendCardConfig.cardOffsetX/2
        posy = self.m_zhuangCardEndPos.y + 75
        self:playResultWinAni(cc.p(posx, posy))
        soundStr = BaccaratRes.SOUND_OF_ZHUANGWIN
        --AudioManager:getInstance():playSound(BaccaratRes.SOUND_OF_ZHUANGWIN)
    end

    --闲家点数
    AudioManager:getInstance():playSound( string.format( BaccaratRes.SOUND_OF_XPOINT, pointxian))

    --庄家点数
    self:doSomethingLater(function() AudioManager:getInstance():playSound( string.format( BaccaratRes.SOUND_OF_ZPOINT, pointzhuang)) end, 1.2)
    
    --结果音效
    self:doSomethingLater(function() AudioManager:getInstance():playSound(soundStr) end, 2.4)

    -- 0胜 1负 2平
    local winArea = {
        [1] = pointzhuang < pointxian, --闲
        [4] = pointzhuang == pointxian, --平
        [2] = pointzhuang > pointxian, --庄 
        [3] = BaccaratDataMgr.getInstance():getResultIsTwoPair(BaccaratDataMgr.ePlace_Xian), --闲对
        [5] = BaccaratDataMgr.getInstance():getResultIsTwoPair(BaccaratDataMgr.ePlace_Zhuang), --庄对
    }

    for i = 1, AreaCount do
        if winArea[i] then self:playAreaBlink(i) end
    end

    --更新历史记录
    BaccaratDataMgr.getInstance():addGameRecordByOpen()
    --self:notifyTrendRefresh()

    --延迟1秒后 播放飞筹码动画
    self:doSomethingLater(function() self:flychipex() end, 5.0) --新版325延迟

    --提示本局有无下注
    self:showResultTips()
end

--提示本局有无下注
function BaccaratLayer:showResultTips()
--    if BaccaratDataMgr.getInstance():isBanker() then
--        return
--    end

    if BaccaratDataMgr.getInstance():getMyAllBetValue() > 0 then
        return
    end

    TOAST("本局您没有下注!")
end

--显示牌点动画
function BaccaratLayer:playCardPointAni(pos, point)
    -- local armature = ccs.Armature:create(BaccaratRes.ANI_FILE_CARDTYPE)
    -- armature:setPosition(pos) 
    -- self.m_nodeCardAni:addChild(armature)
    -- -- if point < 8 then
    -- --     self:replaceArmatureSprite(armature, BaccaratRes.ANI_CARDTYPE_NAME.BONE_LOWPOINT, string.format( BaccaratRes.IMG_CARDPOINT, point))
    -- -- else
    -- --     self:replaceArmatureSprite(armature, BaccaratRes.ANI_CARDTYPE_NAME.BONE_HIGHPOINT, string.format( BaccaratRes.IMG_CARDPOINT, point))
    -- -- end
    
    -- armature:getAnimation():play(BaccaratRes.ANI_CARDTYPE_NAME[point])

    local armature = CacheManager:addSpineTo(self.m_nodeCardAni, "game/baccarat/effect/huanle30s_kaipai/huanle30s_kaipai_1", 0, ".json", 1)
    armature:setPosition(pos)
    armature:setAnimation(0, BaccaratRes.ANI_CARDTYPE_NAME[point], false)
    local dian = ccui.ImageView:create(string.format( BaccaratRes.IMG_CARDPOINT, point), ccui.TextureResType.plistType)
    armature:addChild(dian, 1)
    
    return armature
end

--显示牌型动画
function BaccaratLayer:playCardTypeAni(pos, aniName)
    -- local armature = ccs.Armature:create(BaccaratRes.ANI_FILE_CARDTYPE)
    -- armature:setPosition(pos) 
    -- self.m_nodeCardAni:addChild(armature)
    -- armature:getAnimation():play(aniName)

    local armature = CacheManager:addSpineTo(self.m_nodeCardAni, "game/baccarat/effect/huanle30s_kaipai/huanle30s_kaipai_1", 0, ".json", 1)
    armature:setPosition(pos)
    armature:setAnimation(0, aniName, false)
end

--显示平/同点平动画
function BaccaratLayer:playResultPingAni(pos, aniName)
    -- local armature = ccs.Armature:create(BaccaratRes.ANI_FILE_CARDTYPE)
    -- armature:setPosition(pos) 
    -- self.m_nodeCardAni:addChild(armature)
    -- armature:getAnimation():play(aniName)

    local armature = CacheManager:addSpineTo(self.m_nodeCardAni, "game/baccarat/effect/huanle30s_kaipai/huanle30s_kaipai_1", 0, ".json", 1)
    armature:setPosition(pos)
    armature:setAnimation(0, aniName, false)
end

--显示赢动画
function BaccaratLayer:playResultWinAni(pos)
    local armature = ccs.Armature:create(BaccaratRes.ANI_FILE_YING)
    armature:setPosition(pos) 
    self.m_nodeCardAni:addChild(armature)
    armature:getAnimation():play(BaccaratRes.ANI_YING_NAME.BEGAN)
    local animationEvent = function (armatureBack, movementType, movementID)
        if movementType == ccs.MovementEventType.complete then
            armature:getAnimation():play(BaccaratRes.ANI_YING_NAME.LOOP)
        end
    end
    armature:getAnimation():setMovementEventCallFunc(animationEvent)
end

--替换动画的节点资源
-- function BaccaratLayer:replaceArmatureSprite(armature, boneName, newName)
--     local bone = armature:getBone(boneName)
--     local manager = bone:getDisplayManager()
--     local sprite = manager:getDisplayRenderNode()
--     local vecDisplay = manager:getDecorativeDisplayList()
--     for k, v in pairs(vecDisplay) do 
--         if v then
--             local spData = v:getDisplay() 
--             if spData then 
--                 spData:setSpriteFrame(newName)
--             end
--         end
--     end
-- end

--清理所有扑克牌动画
function BaccaratLayer:cleanCardAni()
    --self.m_xianPointAni = nil
    --self.m_zhuangPointAni = nil
    self.m_sendCard = {}
    self.m_nodeCardAni:stopAllActions()
    self.m_nodeCardAni:removeAllChildren()
end

--播放等待下一局动画
function BaccaratLayer:playWaitAni()
    if BaccaratDataMgr.getInstance():getGameState() == BaccaratDataMgr.eType_Bet then
        return
    end

    local armature = ccs.Armature:create(BaccaratRes.ANI_FILE_WAIT)
    armature:setPosition(self.m_zcAniPos) 
    self.m_nodeZCAni:addChild(armature)
    armature:getAnimation():playWithIndex(0, -1, -1)
    self.m_pImgTimer:setVisible(false)
end

--清理所有转场动画
function BaccaratLayer:cleanZhuanChangAni()
    self.m_nodeZCAni:stopAllActions()
    self.m_nodeZCAni:removeAllChildren()
end

function BaccaratLayer:showResultEffect() 
    if self.m_pResultLayer == nil then
        self.m_pResultLayer = BaccaratResultLayer.new()
        self:addChild(self.m_pResultLayer,5)
    end
    if self.m_pResultLayer ~= nil then
        self.m_pResultLayer:initLayer()
    end
end

--function BaccaratLayer:showResultEffect()
--    self.m_pNodeResult:setVisible(true)
--    self.m_resultBg:setVisible(false)

--    local name = BaccaratDataMgr.getInstance():getBankerName()
--    if INVALID_CHAIR == BaccaratDataMgr.getInstance():getBankerId() then
--        name = LuaUtils.getLocalString("STRING_240")
--    end
--    local strName = LuaUtils.getDisplayNickNameInGame(name)
--    --本轮庄家
--    local strZhuang = LuaUtils.getDisplayNickName(strName, 12, true)

--    self.m_pNodeResultLabelName:setString(strZhuang)

--    local ePlace = {
--        [0] = BaccaratDataMgr.ePlace_Xian, 
--        [1] = BaccaratDataMgr.ePlace_Zhuang, 
--    }
--    local counts = {
--        [0] = BaccaratDataMgr.getInstance():getCardCount(ePlace[0]),
--        [1] = BaccaratDataMgr.getInstance():getCardCount(ePlace[1]),
--    }
--    if counts[0]==0 or counts[1] == 0 then
--        self.m_pNodeResult:setVisible(false)
--        return 
--    end
--    local points = {
--        [0] = BaccaratDataMgr.getInstance():getResultPoints(ePlace[0], counts[0]),
--        [1] = BaccaratDataMgr.getInstance():getResultPoints(ePlace[1], counts[1]),
--    }
--    for i = 0, 1 do

--        local temp1 = points[i == 1 and 1 or 0]
--        local temp2 = points[i == 0 and 1 or 0]
--        local _type = 0
--        if temp1 == temp2 then
--            _type = 1
--        elseif temp1 > temp2 then
--            _type = 2
--        elseif temp1 < temp2 then
--            _type = 3
--        end

--        self.m_pNodeResultLabelPoints[i+1]:setString(temp1.."点")
--        local picstr = {
--            "game/bjl/gui/pingtag.png",
--            "game/bjl/gui/yingtag.png",
--            "game/bjl/gui/shuletag.png",
--        }
--        print (" i = ",i,picstr[_type])
--        self.m_pSpShuying[i+1]:setTexture(picstr[_type])
--        self.m_pSpShuying[i+1]:setVisible(true)
--    end

--    --玩家输赢
--    local betValue = BaccaratDataMgr.getInstance():getMyAllBetValue()
--    local myResult = BaccaratDataMgr.getInstance():getMyResult()
--    local bBanker = BaccaratDataMgr.getInstance():isBanker()
--    local strResult = ""
--    self.m_pNodeResultLabelMine[1]:setVisible(false)
--    self.m_pNodeResultLabelMine[2]:setVisible(false)
--    self.m_pNodeResultLabelMine[3]:setVisible(false)

--    if myResult >= 0 then 
--        self.m_resultBg:loadTexture("game/bjl/image/bg1.png",ccui.TextureResType.localType)
--        self.jiesuanAnimo:setAnimation(0, "animation1", true)
--    else
--        self.m_resultBg:loadTexture("game/bjl/image/bg2.png",ccui.TextureResType.localType)
--        self.jiesuanAnimo:setAnimation(0, "animation2", true)
--    end

--    --显示金额/提示
--    if bBanker then
--        self.m_pNodeResultLabelMine[1]:setVisible(true) --输赢金额
--    else
--        if betValue > 0 and myResult > 0 then
--            self.m_pNodeResultLabelMine[1]:setVisible(true) --中奖金额
--        elseif betValue == 0 then
--            self.m_pNodeResultLabelMine[2]:setVisible(true) --未下注
--        elseif betValue > 0 and myResult <= 0 then
--            self.m_pNodeResultLabelMine[1]:setVisible(true)
--            self.m_pNodeResultLabelMine[3]:setVisible(false) --未投中
--        end 
--    end

--    --结果金额
--    if myResult >= 0 then
--        strResult = "+" .. LuaUtils.getFormatGoldAndNumberAndZi(myResult)
--    else
--        strResult = LuaUtils.getFormatGoldAndNumberAndZi(myResult)
--    end

--    --结果颜色
--    local strPath = (myResult < 0) and ("game/bjl/font/zhs-fzzyjw-48-hui.fnt") or ("game/bjl/font/zhs-fzzyjw-48.fnt")
--    self.m_pNodeResultLabelMine[1]:setFntFile(strPath)
--    self.m_pNodeResultLabelMine[1]:setString(strResult)

--    --结算排行榜
--    for i = 1, 4 do

--        if i <= BaccaratDataMgr.getInstance():getRankSize() then
--            local info = BaccaratDataMgr.getInstance():getRankByIndex(i)
--            local name = CUserManager.getInstance():getUserNickByChairID(info.wChirId)
--            local strScore = LuaUtils.getFormatGoldAndNumberAndZi(info.llScore)
--            self.m_pNodeResultNodeRankName[i]:setString(name)
--            self.m_pNodeResultNodeRankScore[i]:setString(strScore)
--        else
--            self.m_pNodeResultNodeRankName[i]:setString("")
--            self.m_pNodeResultNodeRankScore[i]:setString("")
--        end
--    end

--    --动作
--    local _call = function()
--        local _show = cc.Show:create()
--        local _scale = cc.ScaleTo:create(0.03, 1.3)
--        local _ease = cc.EaseElasticOut:create(cc.ScaleTo:create(0.22, 1.0))
--        local action = cc.Sequence:create(_show, _scale, _ease)
--        return action
--    end

--    local _call4 = function()
--        for i = 1, 4 do
--            self.m_pNodeResultNodeRankName[i]:setVisible(true)
--            self.m_pNodeResultNodeRankScore[i]:setVisible(true)
--        end
--        if BaccaratDataMgr.getInstance():getMyResult() > 0 then
--            local path = string.format("sound-bjl-cheer%d.wav", math.random(0, 2))
--            AudioManager.getInstance():playSound(path)
--        end
--    end
--    local _call5 = function()
--        self.m_pNodeResult:setVisible(false)
--    end
--    self.m_resultBg:setScale(0.0)
--    local action = cc.Sequence:create(
--        cc.Show:create(),
--        cc.EaseBackOut:create(cc.ScaleTo:create(0.2, 1.0)),
--        cc.CallFunc:create(_call4),
--        cc.DelayTime:create(5.0),
--        cc.EaseBackIn:create(cc.ScaleTo:create(0.25, 0.0)),
--        cc.CallFunc:create(_call5))
--    self.m_resultBg:runAction(action)
--end

--得分动画
function BaccaratLayer:playUserScoreEffect()
    local selfcurscore = 0 -- 自己最终得分 
  --  local othercurscore = 0 -- 其他玩家最终得分 

--    selfcurscore = self:calcCurScore()

--    --用户当前积分
 --   local llUserScore = self.m_oldScore
--    local str = ""
--    local curpath = ""

--    -- 显示庄家分数动画
----    str = LuaUtils.getFormatGoldAndNumberAndZi(bankercurscore)
----    if bankercurscore > 0 then
----        str = "+" .. str
----    end
----    curpath = bankercurscore < 0 and BaccaratRes.FNT_RESULT_LOSE or BaccaratRes.FNT_RESULT_WIN
----    BaccaratDataMgr.getInstance():setBankerScore(BaccaratDataMgr.getInstance():getBankerScore() + bankercurscore)
----    local cb = function()
----       self:showResultEffect()
----    end
    self:doSomethingLater(function() self:showResultEffect() end, 1.0)

--   -- self:updateBankerInfo()
--    --调整位置（未调整ui）
--   -- local pos_banker = cc.p(self.m_bankerChipPos.x, self.m_bankerChipPos.y + 80)
--    --self:flyNumOfGoldChange(pos_banker, curpath, str, cb)

--    -- 显示其他玩家分数动画
--    str = LuaUtils.getFormatGoldAndNumberAndZi(othercurscore)
--    if othercurscore > 0 then
--        str = "+" .. str
--    end
--    local posOther = cc.p(self:getOtherChipBeginPosition())
--    posOther.x = posOther.x - 20
--    posOther.y = posOther.y + 110
--    curpath = othercurscore < 0 and BaccaratRes.FNT_RESULT_LOSE or BaccaratRes.FNT_RESULT_WIN
    --self:flyNumOfGoldChange(posOther, curpath, str)

--    -- 显示自己分数动画
--    local selfbet = BaccaratDataMgr.getInstance():getMyAllBetValue()
--    if selfbet > 0 or isbanker then
--        str = LuaUtils.getFormatGoldAndNumberAndZi(selfcurscore)
--        if selfcurscore > 0 then
--            str = "+" .. str
--        end

--        local posSelf = cc.p(self:getSelfChipBeginPosition())
--        posSelf.y = posSelf.y + 150
--        curpath = selfcurscore < 0 and BaccaratRes.FNT_RESULT_LOSE or BaccaratRes.FNT_RESULT_WIN
--        --self:flyNumOfGoldChange(posSelf, curpath, str)
--    end

    --自己金币变化动画
    local soundPath = ""
    if BaccaratDataMgr.getInstance():getMyResult() >= 0 then 
            soundPath = BaccaratRes.SOUND_OF_WIN 
    else
        if 0 == BaccaratDataMgr.getInstance():getUserBetSize() then
            soundPath = BaccaratRes.SOUND_OF_NOBET
        else 
                soundPath = BaccaratRes.SOUND_OF_LOSE 
        end 
    end
    self.m_pLbSelfGold:setString(BaccaratDataMgr.getInstance():getUserScore())
    AudioManager.getInstance():playSound(soundPath)

end

--弹金币动画
function BaccaratLayer:flyNumOfGoldChange(beginPos, filepath, str, cb)
    local lb = cc.Label:createWithBMFont(filepath, str)
                :setAnchorPoint(0, 0.5)
                :setOpacity(255)
                :setScale(0.6)
                :setVisible(false) --新版325不显示弹分
                :setPosition(beginPos)
                :addTo(self.m_nodeTop)

    local pos = cc.p(lb:getPosition())
    local callBack = cc.CallFunc:create(function ()
        lb:setVisible(false)
        lb:setPosition(pos)
        lb:stopAllActions()
        if cb then cb() end
    end)

    local pSeq = cc.Sequence:create(
        cc.EaseBounceOut:create(cc.MoveBy:create(0.8, cc.p(0,-50))),
        cc.DelayTime:create(2.0),
        cc.FadeOut:create(0.4),
        callBack
        )

    lb:runAction(pSeq)   
end

--克隆一个赔付的筹码对象
function BaccaratLayer:clonePayoutChip(jettonIndex)
    local pos = self:getBankerChipBeginPosition()
    local pSpr = self:getSpriteOfJetton(jettonIndex)
    if not pSpr then return end
    pSpr:setScale(0.35)
    pSpr:setPosition(pos)
    pSpr:setLocalZOrder(1)
    self.m_nodeFlyChip:addChild(pSpr)
    return pSpr
end

--以队列方式进行飞筹码
function BaccaratLayer:flychipex()
    -- 处理现有的筹码对象列表
    local countXian  = BaccaratDataMgr.getInstance():getCardCount(BaccaratDataMgr.ePlace_Xian)
    local pointXian  = BaccaratDataMgr.getInstance():getResultPoints(BaccaratDataMgr.ePlace_Xian, countXian)
    local countZhuang = BaccaratDataMgr.getInstance():getCardCount(BaccaratDataMgr.ePlace_Zhuang)
    local pointZhuang = BaccaratDataMgr.getInstance():getResultPoints(BaccaratDataMgr.ePlace_Zhuang, countZhuang)


    -- 0胜 1负 2平
    local winArea = {
        [1] = pointZhuang < pointXian and 0 or 1 , --闲 
        [2] = pointZhuang > pointXian and 0 or 1 , --庄 
        [3] = BaccaratDataMgr.getInstance():getResultIsTwoPair(BaccaratDataMgr.ePlace_Xian) and 0 or 1 , --闲对
        [4] = pointZhuang == pointXian and 0 or 1 , --平
        [5] = BaccaratDataMgr.getInstance():getResultIsTwoPair(BaccaratDataMgr.ePlace_Zhuang) and 0 or 1 , --庄对
    }

    if 0 == winArea[4] then
        winArea[1] = 2
        winArea[2] = 2
    end
    dump(winArea)
    local tem = BaccaratDataMgr.getInstance():getLastGameRecord()
    if not tem then return end

    local bankerwinvec = {} -- 庄家获取的筹码
    local bankerlostvec = {} -- 庄家赔付筹码
    local selfwinvec = {} -- 我获取的筹码
    local otherwinvec = {} -- 其他人获取的筹码
    local selfvec = {} -- 我的所有区域筹码
    local othervec = {} -- 其他人的所有区域筹码
    local bankerwin = false
    local bankerlose = false
    local otherwin = false
    local selfwin = false

    --初始化并标记庄家获胜区域
    for i = 1, CHIP_ITEM_COUNT do
        bankerwinvec[i] = {}
        bankerlostvec[i] = {}
        selfwinvec[i] = {}
        otherwinvec[i] = {}
        selfvec[i] = {winmark = winArea[i], vec = {}}
        othervec[i] = {winmark = winArea[i], vec = {}}
    end

    for k, v in pairs(self.m_vJettonOfMine) do
         table.insert(selfvec[v.wChipIndex].vec, v)
    end

    for k, v in pairs(self.m_vJettonOfOther) do
        table.insert(othervec[v.wChipIndex].vec, v)
    end

    -- 遍历获胜的区域 必须用pairs方式 保证所有筹码遍历到
    local selfPos = self:getSelfChipBeginPosition()
    local bankerPos = self:getBankerChipBeginPosition()
    local otherPos = self:getOtherChipBeginPosition()
    for i = 1, CHIP_ITEM_COUNT do
        for k, v in pairs(selfvec[i].vec) do
            if v then
                if 1 == selfvec[i].winmark then
                    table.insert(bankerwinvec[i], {sp = v.pChipSpr, endpos = bankerPos})
                    bankerwin = true
                else
                    table.insert(selfwinvec[i], {sp = v.pChipSpr, endpos = selfPos, winmark = selfvec[i].winmark})
                    selfwin = true
                end
            end
        end

        for k, v in pairs(othervec[i].vec) do
            if v then
                if 1 == othervec[i].winmark then
                    table.insert(bankerwinvec[i], {sp = v.pChipSpr, endpos = bankerPos})
                    bankerwin = true
                else
                    table.insert(otherwinvec[i], {sp = v.pChipSpr, endpos = otherPos, winmark = selfvec[i].winmark})
                    otherwin = true
                end
            end
        end
    end

    -- 处理庄家需要赔付的筹码 庄家赔付的筹码是多个区域的 需要分区域处理
    -- 同时新建的赔付的筹码引用需要加在玩家获胜筹码集合中 赔付筹码大余8个 则取半
    -- 按倍率赔付筹码数量 大于一定数量的筹码 则不再增加
    -- 赔付筹码是否需要随机分配结束pos飞行？
    local countself = 0
    local countother = 0
    local rewardCount = 0
    local rewardCountMax = 30 -- 最多一个区域赔付30个筹码
    local offset = 7
    for i = 1, CHIP_ITEM_COUNT do
        countself = #selfwinvec[i] -- 后面会对vec进行插入 所以保存一个初始的索引
        rewardCount = 0
        for m = 1, CHIP_REWARD[i] - 1 do
            if rewardCount > rewardCountMax then
                break
            end
            for j = 1, countself do
                rewardCount = rewardCount + 1
                if rewardCount > rewardCountMax then
                    break
                end
                -- 取出原始的筹码对象复制
                local oldsp = selfwinvec[i][j].sp
                if oldsp and 0 == selfwinvec[i][j].winmark then
                    local newsp = self:clonePayoutChip(oldsp:getTag())
                    -- 初始点在庄家处
                    newsp:setPosition(bankerPos)
                    -- 加入赔付队列 落点坐标直接从预定义的随机落点取 赔付的筹码使用随机区域落点互换
                    local posIdx = j % #self.m_randmChipOthers[i].vec
                    posIdx = posIdx > 0 and posIdx or 1
                    table.insert( bankerlostvec[i], { sp = newsp, endpos = self.m_randmChipOthers[i].vec[posIdx]})
                    bankerlose = true
                    -- 加入自己的获胜队列
                    table.insert( selfwinvec[i], { sp = newsp, endpos = selfPos })
                end
            end
        end
        countother = #otherwinvec[i]
        countother = countother > 8 and countother/2 or countother
        for j = 1, countother do
            local oldsp = otherwinvec[i][j].sp
            if oldsp and 0 == otherwinvec[i][j].winmark then
                local newsp = self:clonePayoutChip(oldsp:getTag())
                newsp:setPosition(bankerPos)
                local randx = (math.random(-offset*100, offset*100)) / 100
                local randy = (math.random(-offset*100, offset*100)) / 100
                local posIdx = j % #self.m_randmChipSelf[i].vec
                posIdx = posIdx > 0 and posIdx or 1
                table.insert( bankerlostvec[i], { sp = newsp, endpos = self.m_randmChipSelf[i].vec[posIdx] })
                bankerlose = true
                --table.insert( bankerlostvec[i], { sp = newsp, endpos = cc.p(oldsp:getPosition()) })
                table.insert( otherwinvec[i], { sp = newsp, endpos = otherPos })
            end
        end
    end

    -- 庄家获取筹码
    local jobitem1 = {
        flytime       = CHIP_FLY_TIME,                                -- 飞行时间
        flytimedelay  = false,                                        -- 飞行时间延长随机时间(0.05~0.15)
        flysteptime   = CHIP_FLY_STEPDELAY,                           -- 筹码队列飞行间隔
        nextjobtime   = CHIP_JOBSPACETIME,                            -- 下个任务执行间隔时间
        chips         = bankerwinvec,                                 -- 筹码队列集合 二维数组[下注区域索引][筹码对象引用]
        preCB         = function()                                    -- 任务开始时执行的回调，此回调根据preCBExec控制只执行一次
                            if bankerwin then
                                AudioManager.getInstance():playSound(BaccaratRes.SOUND_OF_GETGOLD)
                            end
                        end,
        preCBExec     = false,
        --overCB        = function() print("庄家获胜筹码飞行完毕") end, -- 动画任务完成后回调
        hideAfterOver = true                                          -- 动画完成后隐藏
    }

    -- 庄家赔付筹码
    local jobitem2 = {
        flytime       = CHIP_FLY_TIME,
        flysteptime   = CHIP_FLY_STEPDELAY,
        nextjobtime   = CHIP_JOBSPACETIME,
        chips         = bankerlostvec,
        preCB         = function()
                            if bankerlose then
                                AudioManager.getInstance():playSound(BaccaratRes.SOUND_OF_GETGOLD)
                            end
                        end,
        --overCB        = function() print("庄家赔付筹码飞行完毕") end,
        hideAfterOver = false
    }

    -- 其他人赢得的筹码
    local jobitem3 = {
        flytime       = CHIP_FLY_TIME + 0.1,
        flysteptime   = CHIP_FLY_STEPDELAY,
        nextjobtime   = CHIP_JOBSPACETIME,
        chips         = otherwinvec,
        preCB         = function()
                            if otherwin then
                                AudioManager.getInstance():playSound(BaccaratRes.SOUND_OF_GETGOLD)
                            end
                        end,
        preCBExec     = false,
        --overCB        = function() print("其他人获胜筹码飞行完毕") end,
        hideAfterOver = true
    }

    -- 自己赢得的筹码
    local jobitem4 = {
        flytime       = CHIP_FLY_TIME + 0.1,
        flytimedelay  = true,                                        -- 飞行时间延长随机时间(0.05~0.15)
        flysteptime   = 0.01,
        nextjobtime   = CHIP_JOBSPACETIME,
        chips         = selfwinvec,
        preCB         = function()
                            if selfwin then
                                AudioManager.getInstance():playSound(BaccaratRes.SOUND_OF_GETGOLD)
                            end
                        end,
        preCBExec     = false,
        --overCB        = function() print("自己获胜筹码飞行完毕") end,
        hideAfterOver = true
    }

    self.m_flyJobVec = {}
    self.m_flyJobVec.nIdx = 1 -- 任务处理索引
    self.m_flyJobVec.flyIdx = 1 -- 飞行队列索引
    self.m_flyJobVec.jobVec = {} -- 任务对象
    self.m_flyJobVec.overCB = function()
        print("所有飞行任务执行完毕")
        self.m_flyJobVec = {}
        self:playUserScoreEffect()
    end

    table.insert(self.m_flyJobVec.jobVec, { jobitem1 }) -- 1 飞行庄家获取筹码
    table.insert(self.m_flyJobVec.jobVec, { jobitem2 }) -- 2 飞行庄家赔付筹码
    table.insert(self.m_flyJobVec.jobVec, { jobitem3, jobitem4 }) -- 3 其他人筹码和自己筹码一起飞行

    self:doFlyJob()
end

--执行单个的飞行任务
function BaccaratLayer:doFlyJob()
    -- 全部任务执行完成之前，被清理重置
    if nil == self.m_flyJobVec.nIdx or nil == self.m_flyJobVec.jobVec then return end

    -- 任务处理完了
    if self.m_flyJobVec.nIdx > #self.m_flyJobVec.jobVec then
        if self.m_flyJobVec.overCB then
            self.m_flyJobVec.overCB()
        end
        return
    end

    -- 取出一个当前需要处理的飞行任务
    local job = self.m_flyJobVec.jobVec[self.m_flyJobVec.nIdx]
    if not job then return end
    if 0 == #job then return end

    -- 按队列取出需要飞行的对象进行动画处理
    local flyvec = {}
    local mf = math.floor
    if self.m_flyJobVec.flyIdx <= CHIP_FLY_SPLIT then
        for i = 1, #job do
            if job[i] then
                if job[i].preCB and (not job[i].preCBExec) then
                    job[i].preCB()
                    job[i].preCBExec = true
                end
                for j = 1, #job[i].chips do
                    local segnum = mf(#job[i].chips[j] / CHIP_FLY_SPLIT) -- 计算需要分成几段
                    for m = 0, segnum do
                        local tgg = job[i].chips[j][m*CHIP_FLY_SPLIT + self.m_flyJobVec.flyIdx]
                        if tgg then
                            table.insert(flyvec, { sptg = tgg, idx = i })
                        end                    
                    end
                end
            end
        end
    end

    -- 当前队列都飞完了
    if 0 == #flyvec then
        -- 下个任务的执行
        self.m_flyJobVec.nIdx = self.m_flyJobVec.nIdx + 1
        self.m_flyJobVec.flyIdx = 1
        self:doSomethingLater(function ()
            for i = 1, #job do
                if job[i].overCB then job[i].overCB() end
            end
            self:doFlyJob()
        end , job[1].nextjobtime) -- 多个任务时 取第一个任务的时间
        return
    end

    -- 开始飞筹码
    for i = 1, #flyvec do
        local tg = flyvec[i]
        if tg and tg.sptg then
            local ts = job[tg.idx].flytimedelay and job[tg.idx].flytime + math.random(5,15) / 100 or job[tg.idx].flytime
            local mt = cc.MoveTo:create(ts, tg.sptg.endpos)
            if i == #flyvec then -- 最后一个筹码飞行完成后执行下一次的飞行回调
                self.m_flyJobVec.flyIdx = self.m_flyJobVec.flyIdx + 1
                self:doSomethingLater(function ()
                    self:doFlyJob()
                end , job[tg.idx].flysteptime)
            end

            if job[tg.idx].hideAfterOver then
                tg.sptg.sp:runAction(cc.Sequence:create(cc.Show:create(), mt, cc.Hide:create()))
            else
                tg.sptg.sp:runAction(cc.Sequence:create(cc.Show:create(), mt))
            end
        end
    end
end

--最后3秒倒计时
function BaccaratLayer:playLastTimeAni(ts)
    local armature = ccs.Armature:create(BaccaratRes.ANI_FILE_DAOJISHI)
    armature:setPosition(self.m_zcAniPos)
    armature:addTo(self.m_nodeZCAni)
    local frameIndex = math.ceil((3 -ts) * 60 + 1)
    if frameIndex > 170 then frameIndex = 170 end 
    armature:getAnimation():playWithIndex(0, -1, -1)
    armature:getAnimation():gotoAndPlay(frameIndex)

    --帧回调无效？使用延迟播放
    for i = 1, ts do
        self:doSomethingLater(function ()
            if self.m_nSoundClock then
                AudioManager.getInstance():stopSound(self.m_nSoundClock)
                self.m_nSoundClock = nil
            end
            self.m_nSoundClock = AudioManager.getInstance():playSound(BaccaratRes.SOUND_OF_COUNT)
        end , i - 1)
    end
end

--闪烁获胜区域
function BaccaratLayer:playAreaBlink(nIndex)
    local _node = self.m_pBlink[nIndex]
    if not _node then return end
    _node:runAction(cc.Sequence:create(cc.Blink:create(8, 5), cc.Show:create()))
end

--隐藏所有获胜闪烁区域
function BaccaratLayer:hideAllAreaBlink()
    for i = 1, AreaCount do
        self.m_pBlink[i]:stopAllActions()
        self.m_pBlink[i]:setVisible(false)
    end
end

--endregion

--region 其他弹出层

function BaccaratLayer:showTrendLayer() --打开路子
    BaccaratTrendLayer.new():addTo(self.m_rootUI, ZORDER_OF_TREND)
end
 
  
function BaccaratLayer:showMessageBox(msg) --打开确认框
    SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_MESSAGEBOX, msg)
end

function BaccaratLayer:showFloatmessage(msg) --打开提示信息
    FloatMessage.getInstance():pushMessage(msg)
end

--endregion

--region UI组件刷新

--更新续投按钮状态
function BaccaratLayer:updateBetContinueEnable()
    
--    --庄家直接置禁用
--    if BaccaratDataMgr.getInstance():isBanker() then
--        self.m_pBtnContinue:setEnabled(false)
--        return
--    end

--    --非下注状态直接置禁用
--    if BaccaratDataMgr.eType_Bet ~= BaccaratDataMgr.getInstance():getGameState() then
--        self.m_pBtnContinue:setEnabled(false)
--        return
--    end

--    --本局已投注直接置禁用
--    if BaccaratDataMgr.getInstance():getUserBetSize() > 0 then
--        self.m_pBtnContinue:setEnabled(false)
--        return        
--    end

--    --无上局投注记录直接置禁用
--    if 0 == BaccaratDataMgr.getInstance():getContinueBetSize() then
--        self.m_pBtnContinue:setEnabled(false)
--        return        
--    end

--    --本局已续投直接禁用
--    if BaccaratDataMgr.getInstance():getIsContinued() then
--        self.m_pBtnContinue:setEnabled(false)
--        return
--    end

--    local totalBet = 0
--    for i = 1, BaccaratDataMgr.getInstance():getContinueBetSize() do
--        local bet = BaccaratDataMgr.getInstance():getContinueBetAtIndex(i)
--        totalBet = totalBet + bet.lBetScore
--    end

--    --钱不够续投
--    if BaccaratDataMgr.getInstance():getUserScore() < totalBet then
--        self.m_pBtnContinue:setEnabled(false)
--        return
--    end
    
     if BaccaratDataMgr.getInstance():getContinueBet() ==1 then 
        self.m_pBtnContinue:setColor(cc.c3b(255,255,255))
        self.m_pBtnContinue:setEnabled(true)
    else
        self.m_pBtnContinue:setColor(cc.c3b(160,160,160))
         self.m_pBtnContinue:setEnabled(false)
    end
end

--首次初始化筹码显示
function BaccaratLayer:updateJettonStateFirst()
    for i = 1, ChipCount do
        local bState = BaccaratDataMgr.getInstance():getJettonEnableState(i)
        local color = bState and cc.c3b(255,255,255) or cc.c3b(160,160,160)
        local opacity = bState and 255 or 180
        self.m_pBtnBetChip[i]:setOpacity(opacity)
        self.m_pBtnBetChip[i]:setColor(color)
        self.m_pBtnBetChip[i]:setEnabled(bState)

        self.m_pBtnBetChip[i]:setPositionY(self.m_betChipPos[i].y)
    end

    --默认选择最小
    self.m_nIndexChoose = 1
    self.m_pBtnBetChip[self.m_nIndexChoose]:setPositionY(self.m_betChipPos[self.m_nIndexChoose].y +  ChipOffsetY)
end

--更新筹码按钮状态
function BaccaratLayer:updateJettonState()
    --设置所有筹码状态
    for i = 1, ChipCount do
        local bState = BaccaratDataMgr.getInstance():getJettonEnableState(i)
        local color = bState and cc.c3b(255,255,255) or cc.c3b(160,160,160)
        local opacity = bState and 255 or 180
        self.m_pBtnBetChip[i]:setOpacity(opacity)
        self.m_pBtnBetChip[i]:setColor(color)
        self.m_pBtnBetChip[i]:setEnabled(bState)
    end

     --计算当前筹码
    local nIndex = -1
    if self.m_nIndexChoose > 0 and BaccaratDataMgr.getInstance():getUserScore() >= BaccaratDataMgr.getInstance():getScoreByJetton(self.m_nIndexChoose) then 
        nIndex = self.m_nIndexChoose
    else
        nIndex = BaccaratDataMgr.getInstance():getJettonSelAdjust()
    end

    nIndex = nIndex < 1 and 1 or nIndex

    if nIndex == self.m_nIndexChoose then return end

    if self.m_nIndexChoose > 0 then
        self.m_pBtnBetChip[self.m_nIndexChoose]:setPositionY(self.m_betChipPos[self.m_nIndexChoose].y)
    end

    self.m_pBtnBetChip[nIndex]:setPositionY(self.m_betChipPos[nIndex].y +  ChipOffsetY)
    self.m_nIndexChoose = nIndex
end

--更新银行按钮状态
function BaccaratLayer:updateBankBtnState()
    local enable_value = BaccaratDataMgr.eType_Bet == BaccaratDataMgr.getInstance():getGameState()
    local enable_color = enable_value and cc.c3b(255,255,255) or cc.c3b(160,160,160)
    local enable_op = enable_value and 255 or 180
    self.m_pBtnBank:setEnabled(enable_value)
    self.m_pBtnBank:setColor(enable_color)
    self.m_pBtnBank:setOpacity(enable_op)
end

--其他玩家数量显示
function BaccaratLayer:updateOtherNum()
   -- local userTab = CUserManager.getInstance():getUserInfoInTable(PlayerInfo.getInstance():getTableID(),true)
    --  self.m_pLbOtherNum:setString(BaccaratDataMgr.getInstance():getOtherNum())
   -- self.m_newChatLayer:updateOutPlayerData(userTab)
end

--刷新用户昵称显示
function BaccaratLayer:updateUsrName()
    local strNick = Player:getNickName()
    local strFormatNick = LuaUtils.getDisplayNickName2(strNick, 170, "Helvetica", 24, "..")
    self.m_pLbSelfName:setString(strFormatNick)

--    local vipLevel = PlayerInfo.getInstance():getVipLevel()
--    local vipPath = string.format("hall/plist/vip/img-vip%d.png", vipLevel)
--    self.m_pImgVip:loadTexture(vipPath, ccui.TextureResType.plistType)
end

--刷新玩家金币状态
function BaccaratLayer:updateUsrGold(gold)
   -- local llGold = gold
  --  local strGold = LuaUtils.getFormatGoldAndNumberAndZi(llGold)
    self.m_pLbSelfGold:setString(BaccaratDataMgr.getInstance():getUserScore())
end

--刷新庄家信息显示
function BaccaratLayer:updateBankerInfo()--更新庄家信息
    --庄家名字
    local name = BaccaratDataMgr.getInstance():getBankerName()
    local strName = LuaUtils.getDisplayNickNameInGame(name)
    self.m_pLbBankerName:setString(strName)

--    --庄家vip
--    local bankerId = BaccaratDataMgr.getInstance():getBankerId()
--    local tableId = PlayerInfo.getInstance():getTableID()
--    local userInfo = CUserManager:getInstance():getUserInfoByChairID(tableId, bankerId)
--    local vipPath = string.format("hall/plist/vip/img-vip%d.png",userInfo.nVipLev)
--    self.m_pSpBankerVip:loadTexture(vipPath, ccui.TextureResType.plistType)

--    --庄家分数
--    local llScore = BaccaratDataMgr.getInstance():getBankerScore()
--    local strScore = LuaUtils.getFormatGoldAndNumberAndZi(llScore)
--    self.m_pLbBankerGold:setString(strScore)

--    --申请上庄人数
--    local listSize = BaccaratDataMgr.getInstance():getBankerListSize()
--    local strList = string.format(LuaUtils.getLocalString("STRING_242"), listSize)
--    local bInList = BaccaratDataMgr.getInstance():isInBankerList()
----    if bInList ~= -1 and not BaccaratDataMgr.getInstance():isBanker() then
----        strList = string.format(LuaUtils.getLocalString("STRING_241"), bInList - 1)
----    end
--    self.m_pLbApplyNum:setString(strList)

--    --上庄按钮
--    local bIsAsk = (BaccaratDataMgr.getInstance():isBanker() or (bInList ~= -1))
--    self.m_pBtnApply:setVisible(not bIsAsk)
--    self.m_pBtnDown:setVisible(bIsAsk)

--    if BaccaratDataMgr.getInstance():isSystemBanker() then
--        self.m_sysBankerAni:show()
--        self:setZhuangDataVisible(false)
--    else
--        self.m_sysBankerAni:hide()
--        self:setZhuangDataVisible(true)
--    end
end

function BaccaratLayer:setZhuangDataVisible(visible)
    self.m_pSpZhuangNameBg:setVisible(visible)
    self.m_pSpZhuangGoldBg:setVisible(visible)
    self.m_pSpBankerVip:setVisible(visible)
end

--清除下注金额显示
function BaccaratLayer:cleanBetNum()
    self.m_pLbTotalBet:setString("")
    --每个区域的下注额
    for i = 1, AreaCount do
        self.m_pLbAreaTotal[i]:setString("")
        self.m_pLbSelfTotal[i]:setString("")
    end
end

--显示已下注筹码
function BaccaratLayer:viewBetChip()
    local betValSelf = 0
    local betValOther = 0
    for i = 1, AreaCount do
        betValSelf = BaccaratDataMgr.getInstance():getMyBetValueAtIndex(i)
        betValOther = BaccaratDataMgr.getInstance():getAllBetValueAtIndex(i) - betValSelf

        --显示已下注筹码
        self:parseStaticChip(i, betValSelf, true)
        self:parseStaticChip(i, betValOther, false)
    end

    BaccaratDataMgr.getInstance():cleanOtherInitBetting()
    BaccaratDataMgr.getInstance():CleanMyInitBetting()

end

--显示静态筹码
function BaccaratLayer:parseStaticChip(betArea, totalVal, isSelf)
    ----old 
    -- for i = ChipCount, 1, -1 do
    --     local chipVal = BaccaratDataMgr.getInstance():getScoreByJetton(i)
    --     if chipVal == nil then
    --         return
    --     end
    --     while totalVal >= chipVal do
    --         totalVal = totalVal - chipVal
    --         self:placeStaticChip(betArea, chipVal, isSelf)
    --     end
    -- end
    
    ----new
    local chip_table = CBetManager.getInstance():getSplitGoldNew(totalVal)
    for k,v in pairs(chip_table) do
        self:placeStaticChip(betArea, v, isSelf)
    end

end

--放置单个静态显示筹码
function BaccaratLayer:placeStaticChip(betArea, score, isSelf)
    local idx, endPos = self:getChipEndPosition(betArea, isSelf)
    local sp = self:getSpriteOfJetton(score)
            sp:setTag(score)
            sp:setScale(FlyChipScale)
            sp:setAnchorPoint(cc.p(0.5, 0.5))
            sp:setPosition(endPos)
            sp:addTo(self.m_nodeFlyChip)

    if isSelf then
        table.insert(self.m_vJettonOfMine, { pChipSpr = sp, nIdx = idx, wChipIndex = betArea})
    else
        table.insert(self.m_vJettonOfOther, { pChipSpr = sp, nIdx = idx, wChipIndex = betArea })
    end
end

--更新下注金额
function BaccaratLayer:updateBetValue(index)
    local allBet = BaccaratDataMgr.getInstance():getAllBetValueAtIndex(index)
    local strAllBet = allBet > 0 and LuaUtils.getFormatGoldAndNumberAndZi(allBet) or ""
    self.m_pLbAreaTotal[index]:setString(strAllBet)
   
    local myBet = BaccaratDataMgr.getInstance():getMyBetValueAtIndex(index)
    local strMyBet = myBet > 0 and LuaUtils.getFormatGoldAndNumberAndZi(myBet) or ""
    self.m_pLbSelfTotal[index]:setString(strMyBet)

    return allBet
end

--更新下注金额
function BaccaratLayer:updateBetValueOfAll()
    local totalBet = 0
    --每个区域的下注额
    for i = 1, AreaCount do
        totalBet = totalBet + self:updateBetValue(i)
    end

    --总下注额
    self.m_pLbTotalBet:setString(LuaUtils.getFormatGoldAndNumberAndZi(totalBet))
end

--显示主界面上端的大路
function BaccaratLayer:initMainViewDalu()
    local rows = 5
    local cols = 7
    local border = 1
    local bgwidth = 23
    local cellwidth = border + bgwidth
    local cellheight = rows * cellwidth
    local scale = 0.7

    local cellSizeForTable = function (table, idx)
        return cellwidth, cellheight
    end

    local numberOfCellsInTableView = function (table)
        return self.m_daluMaxCol > cols and self.m_daluMaxCol or cols
    end

    local tableCellAtIndex = function (table, idx)
        local cell = table:dequeueCell()
        if not cell then
            cell = cc.TableViewCell:new()
        else
            cell:removeAllChildren()
        end

        for i = 0, rows - 1 do
            --背景方块
            local spBg = cc.Sprite:createWithSpriteFrameName(BaccaratRes.IMG_ROADSMALL_BG)
                spBg:setAnchorPoint(cc.p(0, 0))
                spBg:setPosition(cc.p(border, cellheight - (i + 1)*cellwidth))
                spBg:addTo(cell)

            local record = BaccaratDataMgr.getInstance():getDaLuRecordByPosition(i, idx)
            if record.bExist then
                local posx = bgwidth/2
                local posy = posx

                if record.cbWin == 4 then   --起始开平
                    local spBg1 =cc.Sprite:createWithSpriteFrameName(BaccaratRes.IMG_TABLE_PING)
                        spBg1:setAnchorPoint(0.5, 0.5)
                        spBg1:setPosition(posx, posy)
                        spBg1:setScale(scale)
                        spBg1:addTo(spBg)
                elseif record.cbWin >= 2 then--平
                    local spName1 = ((record.cbWin-2)==1)
                        and BaccaratRes.IMG_TABLE_ZHUANGWIN
                        or BaccaratRes.IMG_TABLE_XIANWIN

                    local spBg1=cc.Sprite:createWithSpriteFrameName(spName1)
                        spBg1:setScale(scale)
                        spBg1:setAnchorPoint(0.5, 0.5)
                        spBg1:setPosition(posx, posy)
                        spBg1:addTo(spBg)

                    local spName2 = ((record.cbWin-2)==1)
                        and BaccaratRes.IMG_TABLE_ZHUANGPING
                        or BaccaratRes.IMG_TABLE_XIANPING

                    local spBg2=cc.Sprite:createWithSpriteFrameName(spName2)
                        spBg2:setScale(scale)
                        spBg2:setAnchorPoint(0.5, 0.5)
                        spBg2:setPosition(posx, posy)
                        spBg2:addTo(spBg)
                else
                    local spName = (record.cbWin == 1)
                        and BaccaratRes.IMG_TABLE_ZHUANGWIN
                        or BaccaratRes.IMG_TABLE_XIANWIN

                    local spBg2=cc.Sprite:createWithSpriteFrameName(spName)
                        spBg2:setAnchorPoint(0.5, 0.5)
                        spBg2:setPosition(posx, posy)
                        spBg2:setScale(scale)
                        spBg2:addTo(spBg)
                end

                if record.bBankerTwoPair==1 then
                    local spBg2=cc.Sprite:createWithSpriteFrameName(BaccaratRes.IMG_TABLE_ZHUANGDUI)
                        spBg2:setScale(0.3)
                        spBg2:setAnchorPoint(0.5, 0.5)
                        spBg2:setPosition(posx - 6, posy + 6)
                        spBg2:addTo(spBg)
                end
                if record.bPlayerTwoPair==1 then
                    local spBg2=cc.Sprite:createWithSpriteFrameName(BaccaratRes.IMG_TABLE_XIANDUI)
                        spBg2:setScale(0.3)
                        spBg2:setAnchorPoint(0.5, 0.5)
                        spBg2:setPosition(posx + 6, posy - 6)
                        spBg2:addTo(spBg)
                end
            end
        end

        return cell
    end

    self.m_pDaluView = cc.TableView:create(cc.size(cellwidth * cols, cellheight))
    self.m_pDaluView:ignoreAnchorPointForPosition(false)
    self.m_pDaluView:setAnchorPoint(cc.p(0,0))
    self.m_pDaluView:setPosition(cc.p(0,0))
    self.m_pDaluView:setDirection(cc.SCROLLVIEW_DIRECTION_HORIZONTAL)
    self.m_pDaluView:setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN)
    self.m_pDaluView:setTouchEnabled(true)
    self.m_pDaluView:setDelegate()
    self.m_pNodeTableRoad:addChild(self.m_pDaluView)
    self.m_daluMaxCol = BaccaratDataMgr.getInstance():getDaLuColCount()
    self.m_daluMaxCol = self.m_daluMaxCol > cols and self.m_daluMaxCol or cols
    self.m_pDaluView:registerScriptHandler(cellSizeForTable, cc.TABLECELL_SIZE_FOR_INDEX)
    self.m_pDaluView:registerScriptHandler(tableCellAtIndex, cc.TABLECELL_SIZE_AT_INDEX)
    self.m_pDaluView:registerScriptHandler(numberOfCellsInTableView, cc.NUMBER_OF_CELLS_IN_TABLEVIEW)

end

--显示主界面上端的珠路
function BaccaratLayer:initMainViewZhulu()
    local rows = 5
    local cols = 7
    local border = 1
    local bgwidth = 23
    local cellwidth = border + bgwidth
    local cellheight = rows * cellwidth
    local scale = 0.7

    local cellSizeForTable = function (table, idx)
        return cellwidth, cellheight
    end

    local numberOfCellsInTableView = function (table)
        return self.m_zhuluMaxCol > cols and self.m_zhuluMaxCol or cols
    end

    local tableCellAtIndex = function (table, idx)
        local cell = table:dequeueCell()
        if not cell then
            cell = cc.TableViewCell:new()
        else
            cell:removeAllChildren()
        end

        for i = 0, rows - 1 do
            --背景方块
            local spBg = cc.Sprite:createWithSpriteFrameName(BaccaratRes.IMG_ROADSMALL_BG)
                spBg:setAnchorPoint(cc.p(0, 0))
                spBg:setPosition(cc.p(border, cellheight - (i + 1)*cellwidth))
                spBg:addTo(cell)

            local rIndex = idx*rows+i
            if rIndex <  BaccaratDataMgr.getInstance():getGameRecordSize() then
                local record = BaccaratDataMgr.getInstance():getGameRecordAtIndex(rIndex+1)
                local strSpName
                if record.cbPlayerCount < record.cbBankerCount then
                    --庄赢
                    strSpName = BaccaratRes.IMG_SMALL_ZHUANG
                elseif record.cbPlayerCount > record.cbBankerCount then
                    --闲赢
                    strSpName = BaccaratRes.IMG_SMALL_XIAN
                else
                    --平
                    strSpName = BaccaratRes.IMG_SMALL_PING
                end
                local spIcon = cc.Sprite:createWithSpriteFrameName(strSpName)
                                spIcon:setPosition(cc.p(bgwidth/2, bgwidth/2))
                                spIcon:setScale(scale)
                                spIcon:addTo(spBg)

                if record.bBankerTwoPair ==1 then
                   local spIcon =  cc.Sprite:createWithSpriteFrameName(BaccaratRes.IMG_TABLE_ZHUANGDUI)
                        spIcon:setScale(0.3)
                        spIcon:setAnchorPoint(0.5, 0.5)
                        spIcon:setPosition(bgwidth/2 - 6, bgwidth/2 + 6)
                        spIcon:addTo(spBg)
                end
                if record.bPlayerTwoPair==1 then
                   local spIcon =  cc.Sprite:createWithSpriteFrameName(BaccaratRes.IMG_TABLE_XIANDUI)
                        spIcon:setScale(0.3)
                        spIcon:setAnchorPoint(0.5, 0.5)
                        spIcon:setPosition(bgwidth/2 + 6, bgwidth/2 - 6)
                        spIcon:addTo(spBg)
                end
            end
        end

        return cell
    end

    self.m_pZhuluView = cc.TableView:create(cc.size(cellwidth * cols, cellheight))
    self.m_pZhuluView:ignoreAnchorPointForPosition(false)
    self.m_pZhuluView:setAnchorPoint(cc.p(0,0))
    self.m_pZhuluView:setPosition(cc.p(0,0))
    self.m_pZhuluView:setDirection(cc.SCROLLVIEW_DIRECTION_HORIZONTAL)
    self.m_pZhuluView:setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN)
    self.m_pZhuluView:setTouchEnabled(true)
    self.m_pZhuluView:setDelegate()
    self.m_pNodeTableWin:addChild(self.m_pZhuluView)
    self.m_zhuluMaxCol = math.floor( BaccaratDataMgr.getInstance():getGameRecordSize() / rows) + 1
    self.m_zhuluMaxCol = self.m_zhuluMaxCol > cols and self.m_zhuluMaxCol or cols
    self.m_pZhuluView:registerScriptHandler(cellSizeForTable, cc.TABLECELL_SIZE_FOR_INDEX)
    self.m_pZhuluView:registerScriptHandler(tableCellAtIndex, cc.TABLECELL_SIZE_AT_INDEX)
    self.m_pZhuluView:registerScriptHandler(numberOfCellsInTableView, cc.NUMBER_OF_CELLS_IN_TABLEVIEW)

end

--刷新主界面路子
function BaccaratLayer:updateMainRoadView()
    --大路刷新
    self.m_daluMaxCol = BaccaratDataMgr.getInstance():getDaLuColCount()
    self.m_pDaluView:reloadData()
    if self.m_daluMaxCol > 7 then
        self.m_pDaluView:setContentOffset(
            cc.p(self.m_pDaluView:getViewSize().width - self.m_pDaluView:getContentSize().width, 0))
    end

    --珠路刷新
    self.m_zhuluMaxCol = math.floor( BaccaratDataMgr.getInstance():getGameRecordSize() / 5) + 1
    self.m_pZhuluView:reloadData()
    if self.m_zhuluMaxCol > 7 then
        self.m_pZhuluView:setContentOffset(
            cc.p(self.m_pZhuluView:getViewSize().width - self.m_pZhuluView:getContentSize().width, 0))
    end

    --统计数据显示
    self.m_pLbZhuangNum:setString( string.format("庄:%d", BaccaratDataMgr.getInstance():getGameRecordCountByType(BaccaratDataMgr.ePlace_Zhuang)) )
    self.m_pLbXianNum:setString( string.format("闲:%d", BaccaratDataMgr.getInstance():getGameRecordCountByType(BaccaratDataMgr.ePlace_Xian)) )
    self.m_pLbPingNum:setString( string.format("平:%d", BaccaratDataMgr.getInstance():getGameRecordCountByType(BaccaratDataMgr.ePlace_Ping)) )
    self.m_pLbZhuangDuiNum:setString( string.format("庄对:%d", BaccaratDataMgr.getInstance():getGameRecordCountByType(BaccaratDataMgr.ePlace_ZhuangDui)) )
    self.m_pLbXianDuiNum:setString( string.format("闲对:%d", BaccaratDataMgr.getInstance():getGameRecordCountByType(BaccaratDataMgr.ePlace_XianDui)) )
end

--endregion

--region 辅助方法

function BaccaratLayer:getSpriteOfJetton(score)
    local name = string.format("game-jetton-%d.png", score)
    local sprite = cc.Sprite:createWithSpriteFrameName(name)
    return sprite
end

--获取投注筹码动画落点坐标
function BaccaratLayer:getChipEndPosition(areaIndex, isSelf)
    local idx = 0
    local pos = cc.p(0, 0)
    if isSelf then
        if self.m_randmChipSelf[areaIndex].idx > #self.m_randmChipSelf[areaIndex].vec then
            idx = math.random(1, #self.m_randmChipSelf[areaIndex].vec)
        else
            idx = self.m_randmChipSelf[areaIndex].idx
            self.m_randmChipSelf[areaIndex].idx = idx + 1
        end
        pos = self.m_randmChipSelf[areaIndex].vec[idx]
    else
        if self.m_randmChipOthers[areaIndex].idx > #self.m_randmChipOthers[areaIndex].vec then
            idx = math.random(1, #self.m_randmChipOthers[areaIndex].vec)
        else
            idx = self.m_randmChipOthers[areaIndex].idx
            self.m_randmChipOthers[areaIndex].idx = idx + 1
        end
        pos = self.m_randmChipOthers[areaIndex].vec[idx]
    end

    return idx, pos
end

--获取自己筹码动画起点坐标
function BaccaratLayer:getSelfChipBeginPosition()
    return self.m_selfChipPos
end

--获取其他玩家筹码动画起点坐标
function BaccaratLayer:getOtherChipBeginPosition()
    return self.m_otherChipPos
end

--获取庄家筹码动画起点坐标
function BaccaratLayer:getBankerChipBeginPosition()
    return self.m_bankerChipPos
end

--重置筹码投注区
function BaccaratLayer:resetChipPosArea()
    local offset = 7
    local cfg = {
        {row = 6, col = 9}, --庄
        {row = 5, col = 12}, --平
        {row = 6, col = 9}, --闲
        {row = 4, col = 7}, --闲天王
        {row = 4, col = 7}, --庄天王
        {row = 4, col = 4}, --同点平
        {row = 4, col = 7}, --闲对
        {row = 4, col = 7}, --庄对
    }
    for i = 1, AreaCount do
        self.m_randmChipSelf[i] = {
            idx = 1, -- pos点的索引，原则上按递增方式取点，保证首次最大化铺满区域，然后随机取点
            vec = self:getRandomChipPosVec(self.m_vChipArea[i], 30, cfg[i].row, cfg[i].col, offset)
        }
        self.m_randmChipOthers[i] = {
            idx = 1,
            vec = self:getRandomChipPosVec(self.m_vChipArea[i], 30, cfg[i].row, cfg[i].col, offset)
        }
    end
end

-- 获取区域范围内指定数量的随机下注点
-- rect cc.rect矩形范围
-- num 需要的随机点数量
-- rowNum 显示多少行筹码 以能相互覆盖到为标准
-- colNum 显示多少列筹码 以能相互覆盖到为标准
-- offset 允许的随机偏移量
-- 返回值为筹码放置对象集合
function BaccaratLayer:getRandomChipPosVec(rect, num, rowNum, colNum, offset)
    local rowStep = (math.floor(rect.height*100 / (rowNum + 1))) / 100
    local colStep = (math.floor(rect.width*100 / (colNum + 1))) / 100

    -- 计算逻辑矩形中总的筹码放置位置
    local chipPosVec = {}
    for m = 1, rowNum do
        for n = 1, colNum do
            local randx = (math.random(-offset*100, offset*100)) / 100
            local randy = (math.random(-offset*100, offset*100)) / 100
            local posx = rect.x + n*colStep + randx
            local posy = rect.y + m*rowStep + randy
            table.insert(chipPosVec, cc.p(posx, posy))
        end
    end

    -- 乱序排列
    local count = #chipPosVec

    for i = 1, count do
        local tmp = math.random(i, count)
        chipPosVec[i], chipPosVec[tmp] = chipPosVec[tmp], chipPosVec[i]
    end

    -- 获取指定数量的随机放置点
--    local takenum = (#chipPosVec > num) and (#chipPosVec - num) or 0
--    for i = 1, takenum do
--        local delIdx = math.random(1, #chipPosVec)
--        table.remove(chipPosVec, delIdx)
--    end

    return chipPosVec
end

--清理显示筹码
function BaccaratLayer:cleanChip()
    self.m_vJettonOfMine = {}
    self.m_vJettonOfOther = {}
    self.m_nodeFlyChip:removeAllChildren()
    self.m_flyJobVec = {}
end

--通知路子层刷新历史记录
function BaccaratLayer:notifyTrendRefresh()
    sendMsg(BaccaratEvent.MSG_GAME_BACCARAT_UPDATERECORD)
    --刷新主界面的路子显示
    self:updateMainRoadView()
end

--获取坐标点
function BaccaratLayer:getSubNodePos(parentNode, subNodeName)
    local _node = parentNode:getChildByName(subNodeName)
    if _node then 
        return cc.p(_node:getPosition())
    else
        return cc.p(0, 0)
    end
end

--计算最终显示的积分
function BaccaratLayer:calcCurScore()
    --[[
    假设税率为0.05
    开奖时  玩家，如果中的积分小于等于自己总押注 则返回积分为完全分
            否则返回值 = (中的积分 - 总押注) * 0.95 + 总押注
                       = 中的积分*0.95 + 总押注*0.05

            庄家，积分>0时*0.95 小于0时为实际输分

    a:庄家免税积分 b:自己实际获得免税积分 c:其他玩家免税得分
    c + b = -a
    c = -a - b
    ]]--

    local selfcurscore = 0 -- 自己最终得分
--    local othercurscore = 0 -- 其他玩家最终得分
--    local tax = 1 - BaccaratDataMgr.getInstance():getGameTax()
--   -- local isbanker = BaccaratDataMgr.getInstance():isBanker()

--    --庄家本局结算得分
--    bankercurscore = BaccaratDataMgr.getInstance():getBankerResult()
--    -- bankercurscore = bankercurscore > 0 and bankercurscore/tax or bankercurscore

--    --用户本局结算积分(下注阶段压分然后退出再进，服务端结算本局游戏返回的用户积分是0，使用本地计算)
--    --selfcurscore = BaccaratDataMgr.getInstance():getMyResult()
--    --selfcurscore = selfcurscore > 0 and selfcurscore/tax or selfcurscore

----    if isbanker then
----        selfcurscore = bankercurscore
----    else
        selfcurscore = self:caleSelfWinScore(tax)
        selfcurscore = selfcurscore > 0 and selfcurscore * tax or selfcurscore
  --  end

    --其他玩家计算积分
--    othercurscore = isbanker and -bankercurscore or -bankercurscore - selfcurscore

--    -- selfcurscore = selfcurscore > 0 and selfcurscore * tax or selfcurscore
--    othercurscore = othercurscore > 0 and othercurscore * tax or othercurscore
--    -- bankercurscore = bankercurscore > 0 and bankercurscore * tax or bankercurscore

    return selfcurscore
end

--计算玩家的获胜分数
function BaccaratLayer:caleSelfWinScore(tax)
    local _REWARD        = {2, 9, 2, 3, 3, 33, 12, 12}

    -- 处理现有的筹码对象列表
    local countXian  = BaccaratDataMgr.getInstance():getCardCount(BaccaratDataMgr.ePlace_Xian)
    local pointXian  = BaccaratDataMgr.getInstance():getResultPoints(BaccaratDataMgr.ePlace_Xian, countXian)
    local countZhuang = BaccaratDataMgr.getInstance():getCardCount(BaccaratDataMgr.ePlace_Zhuang)
    local pointZhuang = BaccaratDataMgr.getInstance():getResultPoints(BaccaratDataMgr.ePlace_Zhuang, countZhuang)

    -- 0胜 1负 2平
    local winArea = {
        [1] = pointzhuang < pointxian, --闲
        [4] = pointzhuang == pointxian, --平
        [2] = pointzhuang > pointxian, --庄 
        [3] = BaccaratDataMgr.getInstance():getResultIsTwoPair(BaccaratDataMgr.ePlace_Xian), --闲对
        [5] = BaccaratDataMgr.getInstance():getResultIsTwoPair(BaccaratDataMgr.ePlace_Zhuang), --庄对
    }

    if winArea[2] then
        _REWARD[1] = 1
        _REWARD[3] = 1
        winArea[1] = true
        winArea[3] = true
    end
    
    local myWinScore = 0
    local myBetScore = BaccaratDataMgr.getInstance():getMyAllBetValue()

    for i = 1, AreaCount do
        if winArea[i] then --胜
            myWinScore = myWinScore + _REWARD[i] * BaccaratDataMgr.getInstance():getMyBetValueAtIndex(i)
        end
    end

--    local ret = myWinScore - myBetScore 

--    return ret > 0 and ret * tax or ret

    return myWinScore - myBetScore 

end

function BaccaratLayer:doSomethingLater( call, delay )
    self.m_rootUI:runAction(cc.Sequence:create(cc.DelayTime:create(delay), cc.CallFunc:create(call)))
end

--清理游戏
function BaccaratLayer:cleanGame()
    AudioManager.getInstance():stopAllSoundEffect()
    BaccaratDataMgr.getInstance():clean()

    self:stopAllActions()
    --self:removeAllChildren()
    
    -- 释放动画
    for i, name in pairs(BaccaratRes.vecReleaseAnim) do
        ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo(name)
    end
    -- 释放整图
    for i, name in pairs(BaccaratRes.vecReleasePlist) do
        display.removeSpriteFrames(name[1], name[2])
    end
    -- 释放背景图
    for i, name in pairs(BaccaratRes.vecReleaseImg) do
        display.removeImage(name)
    end
    -- 释放音频
    for i, name in pairs(BaccaratRes.vecReleaseSound) do
        AudioManager.getInstance():unloadEffect(name)
    end
end

--主动退出
function BaccaratLayer:onMoveExitView(msg)
    if msg and tonumber(msg) ~= 1 then --发送起立
        CMsgBaccarat.getInstance():sendTableStandUp()
    end

    self:cleanGame()

    --退出游戏
    PlayerInfo.getInstance():setSitSuc(false)
    PlayerInfo.getInstance():setIsGameBackToHall(true)
    PlayerInfo:getInstance():setIsGameBackToHallSuc(false)

    --返回大厅
    SLFacade:dispatchCustomEvent(Public_Events.Load_Entry)

end

--获取玩家下注筹码起始坐标
function BaccaratLayer:getSelfChipBeginPos(score)
    local idx = BaccaratDataMgr.getInstance():getJettonIndexByValue(score)
    return cc.p(self.m_flyChipPos[idx])
end

--通知银行可用状态
function BaccaratLayer:notifyBankEnable()
    --通知银行可用状态
    local _event = {
        name = Public_Events.MSG_BANK_STATUS,
        packet = BaccaratDataMgr.eType_Bet == BaccaratDataMgr.getInstance():getGameState() and "1" or "0",
    }
    SLFacade:dispatchCustomEvent(Public_Events.MSG_BANK_STATUS, _event)
end

--银行取钱成功
function BaccaratLayer:onMsgBankOperate(_event)    
    local betScore = PlayerInfo.getInstance():getBetScore()
    local userScore = PlayerInfo.getInstance():getUserScore()-betScore
    PlayerInfo.getInstance():setUserScore(userScore,true)
    local strUsrBanlance = LuaUtils.getFormatGoldAndNumberAndZi(userScore)
    self.m_pLbSelfGold:setString(strUsrBanlance)

    self:updateJettonState()
end

--endregion

return BaccaratLayer
