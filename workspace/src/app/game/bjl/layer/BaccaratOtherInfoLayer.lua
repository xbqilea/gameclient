--region BaccaratOtherInfoLayer.lua
--Date 2018/03/16
--百人牛牛其他玩家信息界面
--endregion
local BaccaratDataMgr           = import("..manager.BaccaratDataMgr") 

local HNLayer= require("src.app.newHall.HNLayer") 
local BaccaratOtherInfoLayer   = class("BaccaratOtherInfoLayer", function()
    return HNLayer.new()
end)
local BaccaratRes               = import("..scene.BaccaratRes") 
local BaccaratEvent               = import("..scene.BaccaratEvent")
function BaccaratOtherInfoLayer.create()
    BaccaratOtherInfoLayer.instance_ = BaccaratOtherInfoLayer.new()
    return BaccaratOtherInfoLayer.instance_
end

function BaccaratOtherInfoLayer:ctor()
    self:setNodeEventEnabled(true)
    self:init()
end

function BaccaratOtherInfoLayer:init()
    self:initVar()
    self:initCSB()
    self:initView()
end 

function BaccaratOtherInfoLayer:onEnter()
end

function BaccaratOtherInfoLayer:onExit()
end

function BaccaratOtherInfoLayer:initVar()
    self.m_vecOtherInfo = {}
    --按金币大小排序
    local tab =  BaccaratDataMgr.getInstance():getOnlinePlayers() 
    self.m_vecOtherInfo = tab
end

function BaccaratOtherInfoLayer:initCSB()
    self.m_rootUI       = display.newNode()
    self.m_rootUI:addTo(self)
    self.m_pathUI       = cc.CSLoader:createNode(BaccaratRes.CSB_GAME_USERINFO)
    self.m_pathUI:addTo(self.m_rootUI)
    self.m_rootNode     = self.m_pathUI:getChildByName("Panel_1")
    self.m_pBtnClose    = self.m_pathUI:getChildByName("BTN_close")
    self.m_pBtnClose:addTouchEventListener(handler(self, self.onReturnClicked))

    if display.size.width == 1624 then
        self.m_rootNode:setPosition(cc.p(-75,0))
    else
        self.m_rootNode:setPosition(cc.p(0,0))
    end
end

function BaccaratOtherInfoLayer:initView()
    self:initTableView()
    self:initSlider()
    if #self.m_vecOtherInfo < 5 then
        local slider = tolua.cast(self.m_rootNode:getChildByTag(100), "cc.ControlSlider")
        if slider then slider:setVisible(false) end
    end
end

function BaccaratOtherInfoLayer:initSlider()
    local spBg = cc.Sprite:createWithSpriteFrameName(BaccaratRes.IMG_SCROLL_BG)
    -- //添加一个滑动条的背景
    local spBg0 = cc.Sprite:createWithSpriteFrameName(BaccaratRes.IMG_SCROLL_BG)
    spBg0:setAnchorPoint(cc.p(0, 0))
    spBg:addChild(spBg0, 20)
    
    local pgSp = cc.Sprite:createWithSpriteFrameName(BaccaratRes.IMG_NULL)
    pgSp:setOpacity(0)
    local spTub = cc.Sprite:createWithSpriteFrameName(BaccaratRes.IMG_SCROLL_BUTTON)
    local spTub1 = cc.Sprite:createWithSpriteFrameName(BaccaratRes.IMG_SCROLL_BUTTON)
    local slider = cc.ControlSlider:create(spBg, pgSp, spTub, spTub1)
    if not slider then
        return false
    end
    
    slider:setAnchorPoint(cc.p(0.5, 0.5))
    slider:setMinimumValue(0)
    slider:setMaximumValue(130)
    slider:setPosition(cc.p(446 , 371))
    slider:setRotation(90)
    slider:setValue(12)
    slider:setTag(100)
    slider:setScaleX(1.26)
    slider:setEnabled(false)
    self.m_rootNode:addChild(slider, 50)
end

function BaccaratOtherInfoLayer:onReturnClicked(sender,eventType)
    if eventType ~=ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound(BaccaratRes.SOUND_OF_CLOSE)
    self:close()
end

function BaccaratOtherInfoLayer:initTableView()
     if self.m_pTableView == nil then
        self.m_pTableView = cc.TableView:create(cc.size(335,438))
        self.m_pTableView:setAnchorPoint(cc.p(0,0))
        self.m_pTableView:setPosition(cc.p(120,152))
        self.m_pTableView:ignoreAnchorPointForPosition(false)
        self.m_pTableView:setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN)
        self.m_pTableView:setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL)
        self.m_pTableView:setDelegate()
        self.m_pTableView:registerScriptHandler(handler(self,self.scrollViewDidScroll), cc.SCROLLVIEW_SCRIPT_SCROLL)
        self.m_pTableView:registerScriptHandler(handler(self,self.cellSizeForTable), cc.TABLECELL_SIZE_FOR_INDEX)
        self.m_pTableView:registerScriptHandler(handler(self,self.tableCellAtIndex), cc.TABLECELL_SIZE_AT_INDEX)
        self.m_pTableView:registerScriptHandler(handler(self,self.numberOfCellsInTableView), cc.NUMBER_OF_CELLS_IN_TABLEVIEW)
        self.m_pTableView:registerScriptHandler(handler(self,self.tableCellTouched), cc.TABLECELL_TOUCHED)
        self.m_pTableView:reloadData()
        self.m_rootNode:addChild(self.m_pTableView)
    else
        self.m_pTableView:reloadData()
    end
end

function BaccaratOtherInfoLayer:initTableViewCell(cell, idx)
    local userinfo = self.m_vecOtherInfo[idx+1]
    if not userinfo then return end
    local node = cc.CSLoader:createNode(BaccaratRes.CSB_GAME_USERINFO_NODE)
    node:setPosition(cc.p(5,0))
    cell:addChild(node)

    local name = node:getChildByName("Text_name")
    local strName = LuaUtils.getDisplayNickNameInGame(userinfo.m_nickname)
    name:setString(strName)

    local gold = node:getChildByName("Text_gold")
    gold:setString(LuaUtils.getFormatGoldAndNumber(userinfo.m_score*0.01 or "0"))

    local wFaceID = userinfo.m_faceId or 0
    local head =ToolKit:getHead(wFaceID) 
    local icon = node:getChildByName("Image_icon")
    icon:loadTexture(head,ccui.TextureResType.plistType)
    icon:setScale(0.6)
    --vip等级
--    local level = math.min(userinfo.nVipLev, VIP_LEVEL_MAX)
--    local path = string.format("hall/plist/vip/img-vip%d.png", level)
--    local spVip = cc.Sprite:createWithSpriteFrameName(path)
--    spVip:setScale(0.75)
--    spVip:addTo(icon)

    --调整位置
--    local posX, posY = name:getPosition()
--    local size = spVip:getContentSize()
--    name:setPositionX(gold:getPositionX() + 10)
--    spVip:setPosition(posX, posY - 16)

--    --增加vip等级对应的头像框
--    local frameIndex = userinfo.nVipLev > 6 and 6 or userinfo.nVipLev
--    local framePath = string.format("hall/plist/userinfo/gui-frame-v0.png")
--    local spFrame = cc.Sprite:createWithSpriteFrameName(framePath)
--    spFrame:setScale(0.67)
--    spFrame:setAnchorPoint(cc.p(0, 0))
--    spFrame:setPosition(cc.p(-10, -10))
--    icon:addChild(spFrame)
end

function BaccaratOtherInfoLayer:cellSizeForTable(table, idx)
    return 320,107
end

function BaccaratOtherInfoLayer:tableCellAtIndex(table, idx)
    local cell = table:dequeueCell()
    if not cell then
        cell = cc.TableViewCell:new()
    else
        cell:removeAllChildren()
    end
    self:initTableViewCell(cell, idx)
    return cell
end

function BaccaratOtherInfoLayer:numberOfCellsInTableView(table)
    return #self.m_vecOtherInfo
end

function BaccaratOtherInfoLayer:tableCellTouched(table, cell)
end

function BaccaratOtherInfoLayer:scrollViewDidScroll(pView)
    local slider = tolua.cast(self.m_rootNode:getChildByTag(100), "cc.ControlSlider")
    if not slider then
        return
    end
    local max = (#self.m_vecOtherInfo) * 107 - 438
    local value = 106 + (math.floor(pView:getContentOffset().y * 106 / max) + 12)
    value = value < 12 and 12 or value
    value = value > 118 and 118 or value
    slider:setValue(value)
end

return BaccaratOtherInfoLayer
