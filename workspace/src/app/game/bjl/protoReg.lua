if not LUA_VERSION or LUA_VERSION ~= "5.3" then
	 module(..., package.seeall)
end
require("src.app.game.bjl.protoID")

-- 公共结构定义文件注册
netLoaderCfg_Templates_common = {
	"src/app/game/bjl/pubStTemp",
}

-- 协议定义文件注册
netLoaderCfg_Templates	=	{	
	"src/app/game/bjl/baccarat",
}


-------------------------注册协议-----------------------------
-- 公共结构协议注册
netLoaderCfg_Regs_common = 
{
	PstBaccaratHistory 				= 	PSTID_BACCARATHISTORY,
	PstBaccaratPlayerInfo 			= 	PSTID_BACCARATPLAYERINFO,
	PstBaccaratOnlinePlayerInfo		=	PSTID_BACCARATONLINEPLAYERINFO,
	PstBaccaratBalanceInfo			=	PSTID_BACCARATBALANCEINFO,
	PstBaccaratBetSumInfo			=	PSTID_BACCARATBETSUMINFO,
	PstBaccaratRoom					=	PSTID_BACCARATROOM,
	PstBaccaratBalanceData			=	PSTID_BACCARATBALANCEDATA,
	PstBaccaratLuDan				=	PSTID_BACCARATLUDAN,
	PstBaccaratBet					=	PSTID_BACCARATBET,
	PstBaccaratOneBet				=	PSTID_BACCARATONEBET,
}

-- 协议注册
netLoaderCfg_Regs	=	
{
	--CS_G2C_Baccarat_StateFree_Nty			=	CS_G2C_BACCARAT_STATEFREE_NTY,		
	CS_G2C_Baccarat_StatePlay_Nty			=	CS_G2C_BACCARAT_STATEPLAY_NTY,		
	CS_G2C_Baccarat_GameFree_Nty			=	CS_G2C_BACCARAT_GAMEFREE_NTY,		
	CS_G2C_Baccarat_GameStart_Nty			=	CS_G2C_BACCARAT_GAMESTART_NTY,		
	--CS_G2C_Baccarat_GameOpenCard_Nty		=	CS_G2C_BACCARAT_GAMEOPENCARD_NTY,	
	CS_G2C_Baccarat_GameEnd_Nty				=	CS_G2C_BACCARAT_GAMEEND_NTY,		
	CS_G2C_Baccarat_History_Nty				=	CS_G2C_BACCARAT_HISTORY_NTY,		
	CS_C2G_Baccarat_Bet_Req					=	CS_C2G_BACCARAT_BET_REQ,				
	CS_G2C_Baccarat_Bet_Nty					=	CS_G2C_BACCARAT_BET_NTY,			
	CS_C2G_Baccarat_ApplyBanker_Req			=	CS_C2G_BACCARAT_APPLYBANKER_REQ,	
	CS_G2C_Baccarat_ApplyBanker_Nty			=	CS_G2C_BACCARAT_APPLYBANKER_NTY,	
	CS_C2G_Baccarat_CancelBanker_Req		=	CS_C2G_BACCARAT_CANCELBANKER_REQ,	
	CS_G2C_Baccarat_CancelBanker_Nty		=	CS_G2C_BACCARAT_CANCELBANKER_NTY,	
	CS_G2C_Baccarat_ChangeBanker_Nty		=	CS_G2C_BACCARAT_CHANGEBANKER_NTY,	
	CS_C2G_Baccarat_PlayerOnlineList_Req	=	CS_C2G_BACCARAT_PLAYERONLINELIST_REQ,
	CS_G2C_Baccarat_PlayerOnlineList_Ack	=	CS_G2C_BACCARAT_PLAYERONLINELIST_ACK,
	--CS_G2C_Baccarat_PlayerShow_Nty			=	CS_G2C_BACCARAT_PLAYERSHOW_NTY,
	CS_C2G_Baccarat_Background_Req			=	CS_C2G_BACCARAT_BACKGROUND_REQ,	
	CS_G2C_Baccarat_Background_Ack			=	CS_G2C_BACCARAT_BACKGROUND_ACK,	
	CS_C2G_Baccarat_GetRoomList_Req			=	CS_C2G_BACCARAT_GETROOMLIST_REQ,
	CS_G2C_Baccarat_GetRoomList_Ack			=	CS_G2C_BACCARAT_GETROOMLIST_ACK,
	--CS_G2C_Baccarat_RoomChange_Nty			=	CS_G2C_BACCARAT_ROOMCHANGE_NTY,
	CS_C2G_Baccarat_EnterRoom_Req			=	CS_C2G_BACCARAT_ENTERROOM_REQ,
	CS_G2C_Baccarat_EnterRoom_Ack			=	CS_G2C_BACCARAT_ENTERROOM_ACK,
	CS_C2G_Baccarat_ExitRoom_Req			=	CS_C2G_BACCARAT_EXITROOM_REQ,
	CS_G2C_Baccarat_ExitRoom_Ack			=	CS_G2C_BACCARAT_EXITROOM_ACK,
	SS_M2G_Baccarat_GameCreate_Req			=	SS_M2G_BACCARAT_GAMECREATE_REQ,
	SS_G2M_Baccarat_GameCreate_Ack			=	SS_G2M_BACCARAT_GAMECREATE_ACK,
	SS_M2G_Baccarat_GameResult_Nty			=	SS_M2G_BACCARAT_GAMERESULT_NTY,
	CS_M2C_Baccarat_Exit_Nty				=	CS_M2C_BACCARAT_EXIT_NTY,
	CS_C2G_Baccarat_Continue_Req			=	CS_C2G_BACCARAT_CONTINUE_REQ,
	CS_G2C_Baccarat_Continue_Nty			=	CS_G2C_BACCARAT_CONTINUE_NTY,
}
--return netLoaderCfg_Regs

if LUA_VERSION  and LUA_VERSION == "5.3" then
	return netLoaderCfg_Regs
end

