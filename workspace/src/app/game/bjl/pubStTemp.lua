module(..., package.seeall)

-- 公共结构协议定义

PstBaccaratHistory = 
{
	{ 1, 	1, 'm_xianPair'			, 'UBYTE'				, 1		, '�Ƿ��ж��� 1-�� 0-��' },
	{ 2, 	1, 'm_bankerPair'		, 'UBYTE'				, 1		, '�Ƿ�ׯ���� 1-�� 0-��' },
	{ 3, 	1, 'm_xianVal'			, 'UBYTE'				, 1 	, '�е���' },
	{ 4, 	1, 'm_bankerVal'		, 'UBYTE'				, 1 	, 'ׯ����' },
}

PstBaccaratPlayerInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '���ID' },
	{ 2, 	1, 'm_index'			, 'UINT'				, 1		, '���� 1-3 ��ʾ���������� ����Ϊ0' },
	{ 3, 	1, 'm_nickname'			, 'STRING'				, 1 	, '�ǳ�' },
	{ 4, 	1, 'm_faceId'			, 'UINT'				, 1 	, 'ͷ��ID' },
	{ 5, 	1, 'm_frameId'			, 'UINT'				, 1 	, 'ͷ���ID' },
	{ 6, 	1, 'm_vipLevel'			, 'UINT'				, 1 	, 'vip�ȼ�' },
	{ 7, 	1, 'm_score'			, 'UINT'				, 1 	, '���' },
}

PstBaccaratOnlinePlayerInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '���ID' },
	{ 2, 	1, 'm_nickname'			, 'STRING'				, 1 	, '�ǳ�' },
	{ 3, 	1, 'm_faceId'			, 'UINT'				, 1 	, 'ͷ��ID' },
	{ 4, 	1, 'm_frameId'			, 'UINT'				, 1 	, 'ͷ���ID' },
	{ 5, 	1, 'm_vipLevel'			, 'UINT'				, 1 	, 'vip�ȼ�' },
	{ 6, 	1, 'm_score'			, 'UINT'				, 1 	, '���' },
	{ 7,    1, 'm_recentBet'    	, 'UINT'               	, 1     , '���20����ע�����'},
	{ 8,    1, 'm_recentWin'    	, 'UINT'               	, 1     , '���20��ʤ��'},
}

PstBaccaratBalanceInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '���ID' },
	{ 2,	1, 'm_nick'				, 'STRING'				, 1		, '�ǳ�'},
	{ 3,	1, 'm_profit'			, 'INT'					, 1		, '���־�ӯ��'},
	--{ 3,	1, 'm_curScore'			, 'UINT'				, 1		, '�������'},
}

PstBaccaratBetSumInfo =
{
	{ 1,	1, 'm_betId'			 , 'UBYTE'				, 1		, '��ע���� 0-ׯ 1-�� 2-�� 3-ׯ�� 4-�ж�'},
	{ 2,	1, 'm_betValue'			 , 'INT'				, 1		, '��ע��'},
}

PstBaccaratRoom =
{
	{ 1, 	1, 'm_id'				, 'UINT'				, 1		, '����ID' },
	{ 2, 	1, 'm_type'				, 'UBYTE'				, 1		, '�������� 1-������ 2-������ע' },
	{ 3, 	1, 'm_status'			, 'UBYTE'				, 1		, '״̬ 1-��ע�� 2-������' },
	{ 4, 	1, 'm_leftTime'			, 'UBYTE'				, 1		, '��ע����ʱ' },
	{ 5, 	1, 'm_history'			, 'PstBaccaratHistory'	, 72	, '��ʷ��Ϣ' },
	{ 6, 	1, 'm_selfBet'			, 'UINT'				, 5		, '�Լ�����ע����� ���������ע��Ч' },
	{ 7, 	1, 'm_dyzlu'			, 'PstBaccaratLuDan'	, 72	, '������··��' },
	{ 8, 	1, 'm_xlu'				, 'PstBaccaratLuDan'	, 72	, 'С··��' },
	{ 9, 	1, 'm_xqlu'				, 'PstBaccaratLuDan'	, 72	, '���h··��' },
}

--[[
PstBaccaratRoomChange =
{
	{ 1, 	1, 'm_id'				, 'UINT'				, 1		, '����ID' },
	{ 2, 	1, 'm_status'			, 'UBYTE'				, 1		, '״̬ 1-��ע�� 2-������' },
	{ 3, 	1, 'm_leftTime'			, 'UBYTE'				, 1		, '��ע�ص���ʱ' },
	{ 4, 	1, 'm_history'			, 'PstBaccaratHistory'	, 1		, '�¼�һ����ʷ��¼��Ϣ' },
	{ 5, 	1, 'm_forecast'			, 'UBYTE'				, 3		, 'Ԥ�������·,С·,���h·�Ƿ��й���' },
}
--]]

PstBaccaratBalanceData =
{
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1    , '���ID'},
	{ 2		, 2		, 'm_curCoin'			, 'UINT'				, 1	   , '��ǰ���'},
	{ 3		, 2		, 'm_reason'			, 'SHORT'				, 1	   , '1-�����˳� 2-ǿ�� 3-����'},
}

PstBaccaratLuDan =
{
	{ 1		, 1		, 'm_num'			, 'UINT'				, 1    , '����'},
	{ 2		, 2		, 'm_color'			, 'UBYTE'				, 1	   , '���޹��� 1-��ʾ�й��� 0-��ʾ�޹���'},
}

PstBaccaratBet =
{
	{ 1,	1, 'm_betId'			 , 'UBYTE'						, 1		, '��ע����ID'},
	{ 2,	1, 'm_betValue'			 , 'UINT'						, 1		, '��ע��'},
}

PstBaccaratOneBet =
{
	{ 1,	1, 'm_accountId'		 , 'UINT'						, 1		, '��ǰ˭��ע'},
	{ 2,	1, 'm_coin'		 		 , 'UINT'						, 1		, '��ע��ʣ����'},
	{ 3,	1, 'm_myAreaValue'	 	 , 'UINT'						, 5		, '����������Լ�����ע��'},
	{ 4,	1, 'm_bets'	 	 		 , 'PstBaccaratBet'				, 100	, '�����ע'},
}

