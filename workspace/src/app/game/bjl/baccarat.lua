module(..., package.seeall)


-- 请求房间列表
CS_C2G_Baccarat_GetRoomList_Req =
{
	{ 1,	1, 'm_type'		 	 	 ,  'UBYTE'						, 1		, '房间类型 1-无咪牌 2-批量下注'},
}

CS_G2C_Baccarat_GetRoomList_Ack =
{
	{ 1,	1, 'm_roomList'		 	 ,  'PstBaccaratRoom'			, 20	, '房间信息， 最多提供20个房间'},
	{ 2,	1, 'm_score'		 	 ,  'UINT'						, 1		, '玩家金币'},
}
--[[
CS_G2C_Baccarat_RoomChange_Nty =
{
	{ 1,	1, 'm_roomList'		 	 ,  'PstBaccaratRoomChange'		, 20	, '房间变化信息'},
}
--]]

--进入房间
CS_C2G_Baccarat_EnterRoom_Req =
{
	{ 1,	1, 'm_roomId'		 	 ,  'UINT'						, 1		, '房间ID'},
}

CS_G2C_Baccarat_EnterRoom_Ack =
{
	{ 1,	1, 'm_roomId'		 	 ,  'UINT'						, 1		, '房间ID'},
	{ 2,	1, 'm_ret'		 	 	 ,  'INT'						, 1		, '进入结果 0-成功 其他参考错误码'},
}

--退出房间
CS_C2G_Baccarat_ExitRoom_Req =
{
	{ 1,	1, 'm_roomId'		 	 ,  'UINT'						, 1		, '房间ID'},
}

CS_G2C_Baccarat_ExitRoom_Ack =
{
	{ 1,	1, 'm_roomId'		 	 ,  'UINT'						, 1		, '房间ID'},
	{ 2,	1, 'm_ret'		 	 	 ,  'INT'						, 1		, '退出结果 0-成功 其他参考错误码'},
	--{ 3,	1, 'm_extra'		 	 ,  'INT'						, 1		, '附加信息 1-未下注退出 2-当局游戏还在继续，外面可以旁观'},
}

--中间进入游戏的
-- 游戏状态通知，不同状态表现不同， 广播或者上线后下发
--[[
CS_G2C_Baccarat_StateFree_Nty = 
{
	{ 1,	1, 'm_leftTime'		 	 ,  'UINT'						, 1		, '本阶段剩余时间 单位s'},
	{ 2,	1, 'm_playerCount'       ,  'UINT'       				, 1     , '玩家人数' },
	{ 3,	1, 'm_score'       		 ,  'UINT'       				, 1     , '玩家金币' },
	
	--{ 3,	1, 'm_bankerID'      	 ,  'UINT'       				, 1     , '庄家ID' },
	--{ 4,	1, 'm_bankerCalWin'      ,  'INT'       				, 1     , '庄家累计输赢金币' },
	--{ 5,	1, 'm_bankerTimes'       ,  'UINT'       				, 1     , '庄家坐庄次数' },
	
	--{ 6,	1, 'm_enableSysBanker'   ,  'UBYTE'       				, 1     , '是否允许系统坐庄 1-允许 0-不允许' },
	--{ 7,	1, 'm_applyBankerCondition'   ,  'UINT'       			, 1     , '申请坐庄条件' },
	
	{ 4,	1, 'm_areaBetLimit'   	 ,  'UINT'       				, 1     , '区域下注限制' },
	{ 5,	1, 'm_userBetLimit'   	 ,  'UINT'       				, 1     , '单局玩家总下注限制' },
	
	--{ 6,	1, 'm_forecast'    		,  'UBYTE'       				, 3     , '预测大眼仔路,小路,曱甴路是否有规则' },
}
--]]

--下注，开牌， 结算阶段都会发送下面信息，
-- 如果是在下注，开牌时间进去的， 后面必然会收到结算消息
-- 如果实在结算阶段进入， 不再显示结算效果，各区域下注为0
CS_G2C_Baccarat_StatePlay_Nty = 
{
	{ 1,	1, 'm_state'		 	 ,  'UINT'						, 1		, '状态 1-free阶段 2-下注阶段 3-结算阶段'},
	{ 2,	1, 'm_leftTime'		 	 ,  'UINT'						, 1		, '本阶段剩余时间 单位s'},
	{ 3,	1, 'm_playerCount'       ,  'UINT'       				, 1     , '玩家人数' },
	
	{ 4,	1, 'm_score'       		 ,  'UINT'       				, 1     , '玩家当前金币' },
	
	{ 5,	1, 'm_areaBetLimit'   	 ,  'UINT'       				, 1     , '单局区域下注限制' },
	{ 6,	1, 'm_userBetLimit'   	 ,  'UINT'       				, 1     , '单局玩家总下注限制' },
	
	{ 7,	1, 'm_totalBet'       	 ,  'UINT'       				, 5     , '各区域总下注额' },
	{ 8,	1, 'm_myBet'       	 	 ,  'UINT'       				, 5     , '自己各区域总下注额' },
	
	{ 9,	1, 'm_bankerCards'       ,  'UBYTE'       				, 3     , '庄家牌' },
	{ 10,	1, 'm_xianCards'         ,  'UBYTE'       				, 3     , '闲家牌' },
	{ 11, 	1, 'm_canContinueBet'	 , 'UBYTE'  					, 1		, '是否能够续压 1:是 0:否' },
	{ 12,	1, 'm_recordId'       	 ,  'STRING'       				, 1     , '牌局编号' },
	--{ 11,	1, 'm_forecast'    		,  'UBYTE'       				, 3     , '预测大眼仔路,小路,曱甴路是否有规则' },
	
	--{ 9,	1, 'm_bankerID'      	 ,  'UINT'       				, 1     , '庄家ID' },
	--{ 10,	1, 'm_bankerCalWin'      ,  'INT'       				, 1     , '庄家累计成绩' },
	--{ 11,	1, 'm_bankerWin'      	 ,  'INT'       				, 1     , '庄家本局成绩' },
	--{ 12,	1, 'm_bankerTimes'       ,  'UINT'       				, 1     , '庄家坐庄次数' },
	
	--{ 10,	1, 'm_selfWin'         	 ,  'INT'       				, 1     , '玩家自己成绩' },
	--{ 11,	1, 'm_allPlayerWin'      ,  'INT'    					, 5     , '玩家各下注区域成绩' },
	--{ 12,	1, 'm_allResult'      	 ,  'PstBaccaratBalanceInfo'    , 200   , '所有玩家结算结果' },
	
	--{ 15,	1, 'm_enableSysBanker'   ,  'UBYTE'       				, 1     , '是否允许系统坐庄 1-允许 0-不允许' },
	--{ 16,	1, 'm_applyBankerCondition'   ,  'UINT'       			, 1     , '申请坐庄条件' },
	
}

--通知所有玩家游戏进入free状态
CS_G2C_Baccarat_GameFree_Nty = 
{
	{ 1,	1, 'm_roomId'			 ,  'UINT'						, 1		, '房间ID'},
	{ 2,	1, 'm_timeLeft'			 ,  'UINT'						, 1		, '该状态剩余时间'},
	{ 3,	1, 'm_playerCount'       ,  'UINT'       				, 1     , '玩家人数' },
}

--通知所有玩家游戏进入start状态(处于房间列表界面旁观者，也发送该消息）
CS_G2C_Baccarat_GameStart_Nty = 
{
	{ 1,	1, 'm_roomId'			 ,  'UINT'						, 1		, '房间ID'},
	{ 2,	1, 'm_timeLeft'			 ,  'UINT'						, 1		, '该状态剩余时间'},
	{ 3,	1, 'm_playerCount'       ,  'UINT'       				, 1     , '玩家人数' },
	{ 4,	1, 'm_clearHist'    	 ,  'UBYTE'       				, 1     , '删除之前历史信息 1-表示清理 0-表示不清理' },
	{ 5, 	1, 'm_canContinueBet'	 , 'UBYTE'  					, 1		, '是否能够续压 1:是 0:否' },
	{ 6,	1, 'm_recordId'       	 ,  'STRING'       				, 1     , '牌局编号' },
}


--通知所有玩家游戏进入start状态(处于房间列表界面旁观者，也发送该消息）
--[[
CS_G2C_Baccarat_GameOpenCard_Nty = 
{
	{ 1,	1, 'm_roomId'			 ,  'UINT'						, 1		, '房间ID'},
	{ 2,	1, 'm_timeLeft'			 ,  'UINT'						, 1		, '该状态剩余时间'},
	{ 3,	1, 'm_playerCount'       ,  'UINT'       				, 1     , '玩家人数' },
	{ 4,	1, 'm_bankerCards'       ,  'UBYTE'       				, 3     , '庄家牌' },
	{ 5,	1, 'm_xianCards'         ,  'UBYTE'       				, 3     , '闲家牌' },
}
--]]

--通知所有玩家游戏进入结算处状态
CS_G2C_Baccarat_GameEnd_Nty = 
{
	{ 1,	1, 'm_roomId'			 ,  'UINT'						, 1		, '房间ID'},
	{ 2,	1, 'm_timeLeft'			 ,  'UINT'						, 1		, '该状态剩余时间'},
	{ 3,	1, 'm_playerCount'       ,  'UINT'       				, 1     , '玩家人数' },
	{ 4,	1, 'm_bankerCards'       ,  'UBYTE'       				, 3     , '庄家牌' },
	{ 5,	1, 'm_xianCards'         ,  'UBYTE'       				, 3     , '闲家牌' },
	{ 6,	1, 'm_profit'         	 ,  'INT'       				, 1     , '自己结算输赢' },
	{ 7,	1, 'm_score'         	 ,  'UINT'       				, 1     , '结算后金币' },
	{ 8,	1, 'm_allResult'      	 ,  'PstBaccaratBalanceInfo'    , 5     , '前五名信息' },
	{ 9,	1, 'm_result'         	 ,  'PstBaccaratHistory'       	, 1     , '新牌型纪录' },
	{ 10, 	1, 'm_dyzlu'			 ,  'PstBaccaratLuDan'			, 2		, '大眼仔路最后一路路单' },
	{ 11, 	1, 'm_xlu'				 ,  'PstBaccaratLuDan'			, 2		, '小路最后一路路单' },
	{ 12, 	1, 'm_xqlu'				 ,  'PstBaccaratLuDan'			, 2		, '曱甴路最后一路路单' },
	--{ 5,	1, 'm_allPlayerWin'      ,  'INT'    					, 5     , '玩家各下注区域成绩' },
}

-- 历史下注信息
CS_G2C_Baccarat_History_Nty = 
{
	{ 1,	1, 'm_roomId'			 ,  'UINT'						, 1		, '房间ID'},
	{ 2,	1, 'm_history'			 , 	'PstBaccaratHistory'		, 72	, '历史记录'},
	{ 3, 	1, 'm_dyzlu'			 , 	'PstBaccaratLuDan'			, 72	, '大眼仔路路单' },
	{ 4, 	1, 'm_xlu'				 , 	'PstBaccaratLuDan'			, 72	, '小路路单' },
	{ 5, 	1, 'm_xqlu'				 , 	'PstBaccaratLuDan'			, 72	, '曱甴路路单' },
}

--下注
CS_C2G_Baccarat_Bet_Req = 
{
	{ 1,	1, 'm_roomId'			 , 'UINT'						, 1		, '房间ID， 无咪牌模式填0'},
	{ 2,	1, 'm_betId'			 , 'UBYTE'						, 1		, '下注区域ID 0-庄 1-闲 2-和 3-庄对 4-闲对'},
	{ 3,	1, 'm_betValue'			 , 'UINT'						, 1		, '下注额'},
}

--下注结果 失败发送给请求玩家， 成功广播给所有玩家
CS_G2C_Baccarat_Bet_Nty = 
{
	{ 1,	1, 'm_roomId'			 , 'UINT'						, 1		, '房间ID， 无咪牌模式填0'},
	{ 2,	1, 'm_ret'			 	 , 'INT'						, 1		, '下注结果(0：成功 其他参考错误码表)'},
	{ 3,	1, 'm_bets'		 		 , 'PstBaccaratOneBet'			, 1		, '下注者剩余金币'},
	{ 4,	1, 'm_areaTotalValue'	 , 'UINT'						, 5		, '五个区域总下注额, 如果下注失败, 这里不发'},
}

--续压
CS_C2G_Baccarat_Continue_Req = 
{
	{ 1,	1, 'm_roomId'			 , 'UINT'						, 1		, '房间ID， 无咪牌模式填0'},
}

--续压结果
CS_G2C_Baccarat_Continue_Nty = 
{
	{ 1,	1, 'm_roomId'			 , 'UINT'						, 1		, '房间ID， 无咪牌模式填0'},
	{ 2,	1, 'm_ret'			 	 , 'INT'						, 1		, '下注结果(0：成功 其他参考错误码表)'},
	{ 3,	1, 'm_bets'		 		 , 'PstBaccaratOneBet'			, 1	, '下注者剩余金币'},
	{ 4,	1, 'm_areaTotalValue'	 , 'UINT'						, 5		, '五个区域总下注额, 如果下注失败, 这里不发'},
}  

--下发结算结果
--[[
CS_G2C_Baccarat_GameBalance_Nty = 
{
	{ 1,	1, 'm_roundId'			 , 'UINT'						, 1		, '轮次ID'},
	{ 2,	1, 'm_vecBalance'		 , 'PstDvtBalanceInfo'			, 1024	, '结算信息， 这里面包含玩家信息做飞结算的效果'},
}
--]]

-- 在线玩家列表
CS_C2G_Baccarat_PlayerOnlineList_Req =
{
	{ 1,	1, 'm_startIndex'        , 'UINT'       				, 1     , '开始索引(从1开始)' },
	{ 2,	1, 'm_endIndex'          , 'UINT'       				, 1     , '结束索引' },
}

CS_G2C_Baccarat_PlayerOnlineList_Ack = 
{
	{ 1,	1, 'm_startIndex'        , 'UINT'       				, 1     , '开始索引(从1开始)' },
	{ 2,	1, 'm_endIndex'          , 'UINT'       				, 1     , '结束索引' },
	{ 3, 	1, 'm_playerInfo'		 , 'PstBaccaratOnlinePlayerInfo'  	, 64	, '在线玩家信息， 按照最近20局下注数排序，这个再发最多64个玩家，多的不发送' },	
}

-- 主界面玩家信息，分布在两侧的， 每局结束时如果有变化刷新一遍
--[[
CS_G2C_Baccarat_PlayerShow_Nty = 
{
	{ 1, 	1, 'm_mostRichPlayerList'		 , 'PstBaccaratPlayerInfo'  			, 2		, '神算子，近20局获胜次数最高的人，最多一个人，可能一个也没有，这里用vector存' },
	{ 2, 	1, 'm_mostCoinplayerList'		 , 'PstBaccaratPlayerInfo'  			, 3		, '最多3个玩家, 金币最多的3个玩家， 除了神算子外' },
}
--]]

-- 主界面玩家信息，分布在两侧的， 每局结束时如果有变化刷新一遍
--[[
CS_G2C_Baccarat_PlayerInfo_Nty = 
{
	{ 1, 	1, 'm_self'		 				 , 'PstBaccaratPlayerInfo'  	, 1		, '自己的信息' },
	{ 2, 	1, 'm_minScore'		 			 , 'UINT'  						, 1		, '游戏最低金币限制' },
}
--]]

--切到后台
CS_C2G_Baccarat_Background_Req =
{
	{ 1,	1, 'm_type'			 , 'INT'						, 1		, '1-切到后台 2-切回游戏'},
}

CS_G2C_Baccarat_Background_Ack =
{
	{ 1,	1, 'm_type'			 , 'INT'						, 1		, '1-切到后台 2-切回游戏'},
	{ 2,	1, 'm_ret'			 , 'INT'						, 1		, '退出结果 0-表示成功'},
}


CS_M2C_Baccarat_Exit_Nty =
{
	{ 1		, 1		, 'm_type'		,		'UBYTE'	, 1		, '退出， 0-正常结束 1-分配游戏服失败 2-同步游戏服失败 3-踢人 4-维护踢人'},
}