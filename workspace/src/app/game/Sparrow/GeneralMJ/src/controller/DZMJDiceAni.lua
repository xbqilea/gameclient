--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 筛子动画

local DZMJDiceAni = class("DZMJDiceAni", function()
	return display.newLayer()
end)

function DZMJDiceAni:ctor()
	-- print("===DZMJDiceAni:ctor===")
    self.ani = nil
    self.aniBone = nil
    self.dice1 = nil
    self.dice2 = nil
    self.callFunc = nil

    self.isRunAni = false
end

function DZMJDiceAni:play(arr, callFunc)
	print("===DZMJDiceAni:play===")
    local sNum1 = arr[1] or 1
    local sNum2 = arr[2] or 1

    self.callFunc = callFunc

    if self.ani == nil then
        self.ani = ToolKit:createFrameAnimationEx("tx/dice/", "ccmj_dice")
        self:addChild(self.ani,10)
    end
    self:stopAllActions()
    self.ani:setVisible(true)
    self.ani:setPosition(display.cx,display.cy-50)
    
    self.aniBone = self.ani:getBone("aniLayer")
    self.dice1 = self.ani:getBone("dice1")
    self.dice2 = self.ani:getBone("dice2")
    self.dice1:changeDisplayWithIndex(-1,true)
    self.dice2:changeDisplayWithIndex(-1,true)
    self.aniBone:getDisplayManager():setVisible(true)

    self.ani:getAnimation():playWithIndex(0)

	self:runAction(
		cc.Sequence:create(cc.DelayTime:create(0.5), 
		cc.CallFunc:create(function ()
            -- self.aniBone:changeDisplayWithIndex(-1,true)
        	self.aniBone:getDisplayManager():setVisible(false)
        	local diceNode1 = display.newSprite( "tx/dice/" .. sNum1 .. ".png" )
            local diceNode2 = display.newSprite( "tx/dice/" .. sNum2 .. ".png" )
            self.dice1:addDisplay(diceNode1,0)
            self.dice1:changeDisplayWithIndex(0, true) 
            self.dice2:addDisplay(diceNode2,0)
            self.dice2:changeDisplayWithIndex(0, true) 
    		end ),
        cc.Sequence:create(cc.DelayTime:create(0.5)), 
        cc.CallFunc:create(function ( )
            self.ani:setVisible(false)
            self:onAniFinish()
        end)
    	)
    )
	
end

function DZMJDiceAni:onAniFinish()
    print("===DZMJDiceAni:onAniFinish===")
    dump(self.callFunc)
    if self.callFunc then
        self.callFunc()
    end
end

return DZMJDiceAni