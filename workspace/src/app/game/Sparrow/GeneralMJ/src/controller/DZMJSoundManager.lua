--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将音效管理

local HU_SOUND = {
	[MJDef.eGender.male] = {
		[MJDef.HUTYPE.TREASURES+MJDef.HUTYPE.CLIP] = "ccmj_baoJia_nan.mp3",			--	宝中宝夹胡_男
		[MJDef.HUTYPE.TREASURES+MJDef.HUTYPE.DRIFT] = "ccmj_baoPiao_nan.mp3",			--	宝中宝飘胡_男
		[MJDef.HUTYPE.TREASURES+MJDef.HUTYPE.SEVEN_PAIR] = "ccmj_baoQi_nan.mp3",			--	宝中宝七小对_男
		[MJDef.HUTYPE.TREASURES] = "ccmj_baozhongbao_nan.mp3",			--	宝中宝_男
		[MJDef.HUTYPE.FIRE_GUN] = "ccmj_dianpao_nan.mp3",			--	点炮_男
		[MJDef.HUTYPE.CLIP] = "ccmj_jiahu_nan.mp3",			--	夹胡_男
		[MJDef.HUTYPE.HIS_TREASURE] = "ccmj_loubao_nan.mp3",			--	搂宝_男
		[MJDef.HUTYPE.CLIP+MJDef.HUTYPE.HIS_TREASURE] = "ccmj_louJia_nan.mp3",			--	夹胡搂宝_男
		[MJDef.HUTYPE.DRIFT+MJDef.HUTYPE.HIS_TREASURE] = "ccmj_louPiao_nan.mp3",			--	飘胡搂宝_男
		[MJDef.HUTYPE.SEVEN_PAIR+MJDef.HUTYPE.HIS_TREASURE] = "ccmj_louQi_nan.mp3",			--	七小对搂宝_男
		[MJDef.HUTYPE.DRIFT] = "ccmj_piaohu_nan.mp3",			--	飘胡_男
		[MJDef.HUTYPE.LEVEL] = "ccmj_pinghu_nan.mp3",			--	平胡_男
		[MJDef.HUTYPE.SEVEN_PAIR] = "ccmj_qixiaodui_nan.mp3",			--	七小对_男
		[MJDef.HUTYPE.SELF_PICK] = "ccmj_zimo_nan.mp3",			--	自摸_男
	},
	[MJDef.eGender.female] = {
		[MJDef.HUTYPE.TREASURES+MJDef.HUTYPE.CLIP] = "ccmj_baoJia_nv.mp3", 		--	宝中宝夹胡_女
		[MJDef.HUTYPE.TREASURES+MJDef.HUTYPE.DRIFT] = "ccmj_baoPiao_nv.mp3", 		--	宝中宝飘胡_女
		[MJDef.HUTYPE.TREASURES+MJDef.HUTYPE.SEVEN_PAIR] = "ccmj_baoQi_nv.mp3", 		--	宝中宝七小对_女
		[MJDef.HUTYPE.TREASURES] = "ccmj_baozhongbao_nv.mp3", 		--	宝中宝_女
		[MJDef.HUTYPE.FIRE_GUN] = "ccmj_dianpao_nv.mp3", 		--	点炮_女
		[MJDef.HUTYPE.CLIP] = "ccmj_jiahu_nv.mp3", 		--	夹胡_女
		[MJDef.HUTYPE.HIS_TREASURE] = "ccmj_loubao_nv.mp3", 		--	搂宝_女
		[MJDef.HUTYPE.CLIP+MJDef.HUTYPE.HIS_TREASURE] = "ccmj_louJia_nv.mp3", 		--	夹胡搂宝_女
		[MJDef.HUTYPE.DRIFT+MJDef.HUTYPE.HIS_TREASURE] = "ccmj_louPiao_nv.mp3", 		--	飘胡搂宝_女
		[MJDef.HUTYPE.SEVEN_PAIR+MJDef.HUTYPE.HIS_TREASURE] = "ccmj_louQi_nv.mp3", 		--	七小对搂宝_女
		[MJDef.HUTYPE.DRIFT] = "ccmj_piaohu_nv.mp3", 		--	飘胡_女
		[MJDef.HUTYPE.LEVEL] = "ccmj_pinghu_nv.mp3", 		--	平胡_女
		[MJDef.HUTYPE.SEVEN_PAIR] = "ccmj_qixiaodui_nv.mp3", 		--	七小对_女
		[MJDef.HUTYPE.SELF_PICK] = "ccmj_zimo_nv.mp3", 		--	自摸_女
	},
}

local OPT_SOUND = {
	[MJDef.eGender.male] = {
		[MJDef.OPERTPYE.CHI] = "ccmj_chi_nan.mp3", 		--	吃_男
		[MJDef.OPERTPYE.HU] = "ccmj_chi_hu_nan.mp3", 		--	胡_男
		[MJDef.OPERTPYE.GANG] = "ccmj_gang_nan.mp3", 		--	杠_男
		[MJDef.OPERTPYE.PENG] = "ccmj_peng_nan.mp3", 		--	碰_男
		[MJDef.OPERTPYE.QIANG_TING] = "ccmj_ting_nan.mp3", 		--	抢杠_男
		[MJDef.OPERTPYE.TING] = "ccmj_ting_nan.mp3", 		--	听牌_男
	},
	[MJDef.eGender.female] = {
		[MJDef.OPERTPYE.CHI] = "ccmj_chi_nv.mp3", 		--	吃_女
		[MJDef.OPERTPYE.HU] = "ccmj_chi_hu_nv.mp3", 		--	胡_女
		[MJDef.OPERTPYE.GANG] = "ccmj_gang_nv.mp3", 		--	杠_女
		[MJDef.OPERTPYE.PENG] = "ccmj_peng_nv.mp3", 		--	碰_女
		[MJDef.OPERTPYE.QIANG_TING] = "ccmj_ting_nv.mp3", 		--	抢杠_女
		[MJDef.OPERTPYE.TING] = "ccmj_ting_nv.mp3", 		--	听牌_女
	},
}

local CARD_SOUND = {
	[MJDef.eGender.male] = {
		[MJDef.PAI.DONG] = "ccmj_f_1_nan.mp3", 		--	东风_男
		[MJDef.PAI.NAN] = "ccmj_f_2_nan.mp3", 		--	南风_男
		[MJDef.PAI.XI] = "ccmj_f_3_nan.mp3", 		--	西风_男
		[MJDef.PAI.BEI] = "ccmj_f_4_nan.mp3", 		--	北风_男
		[MJDef.PAI.ZHONG] = "ccmj_f_5_nan.mp3", 		--	红中_男
		[MJDef.PAI.FA] = "ccmj_f_6_nan.mp3", 		--	发财_男
		[MJDef.PAI.BAI] = "ccmj_f_7_nan.mp3", 		--	白板_男
		[MJDef.PAI.SUO_1] = "ccmj_s_1_nan.mp3", 		--	1条_男
		[MJDef.PAI.SUO_2] = "ccmj_s_2_nan.mp3", 		--	2条_男
		[MJDef.PAI.SUO_3] = "ccmj_s_3_nan.mp3", 		--	3条_男
		[MJDef.PAI.SUO_4] = "ccmj_s_4_nan.mp3", 		--	4条_男
		[MJDef.PAI.SUO_5] = "ccmj_s_5_nan.mp3", 		--	5条_男
		[MJDef.PAI.SUO_6] = "ccmj_s_6_nan.mp3", 		--	6条_男
		[MJDef.PAI.SUO_7] = "ccmj_s_7_nan.mp3", 		--	7条_男
		[MJDef.PAI.SUO_8] = "ccmj_s_8_nan.mp3", 		--	8条_男
		[MJDef.PAI.SUO_9] = "ccmj_s_9_nan.mp3", 		--	9条_男
		[MJDef.PAI.TONG_1] = "ccmj_t_1_nan.mp3", 		--	1筒_男
		[MJDef.PAI.TONG_2] = "ccmj_t_2_nan.mp3", 		--	2筒_男
		[MJDef.PAI.TONG_3] = "ccmj_t_3_nan.mp3", 		--	3筒_男
		[MJDef.PAI.TONG_4] = "ccmj_t_4_nan.mp3", 		--	4筒_男
		[MJDef.PAI.TONG_5] = "ccmj_t_5_nan.mp3", 		--	5筒_男
		[MJDef.PAI.TONG_6] = "ccmj_t_6_nan.mp3", 		--	6筒_男
		[MJDef.PAI.TONG_7] = "ccmj_t_7_nan.mp3", 		--	7筒_男
		[MJDef.PAI.TONG_8] = "ccmj_t_8_nan.mp3", 		--	8筒_男
		[MJDef.PAI.TONG_9] = "ccmj_t_9_nan.mp3", 		--	9筒_男
		[MJDef.PAI.WAN_1] = "ccmj_w_1_nan.mp3", 		--	1万_男
		[MJDef.PAI.WAN_2] = "ccmj_w_2_nan.mp3", 		--	2万_男
		[MJDef.PAI.WAN_3] = "ccmj_w_3_nan.mp3", 		--	3万_男
		[MJDef.PAI.WAN_4] = "ccmj_w_4_nan.mp3", 		--	4万_男
		[MJDef.PAI.WAN_5] = "ccmj_w_5_nan.mp3", 		--	5万_男
		[MJDef.PAI.WAN_6] = "ccmj_w_6_nan.mp3", 		--	6万_男
		[MJDef.PAI.WAN_7] = "ccmj_w_7_nan.mp3", 		--	7万_男
		[MJDef.PAI.WAN_8] = "ccmj_w_8_nan.mp3", 		--	8万_男
		[MJDef.PAI.WAN_9] = "ccmj_w_9_nan.mp3", 		--	9万_男
	},
	[MJDef.eGender.female] = {
		[MJDef.PAI.DONG] = "ccmj_f_1_nv.mp3",				-- 东风_女
		[MJDef.PAI.NAN] = "ccmj_f_2_nv.mp3",				-- 南风_女
		[MJDef.PAI.XI] = "ccmj_f_3_nv.mp3",				-- 西风_女
		[MJDef.PAI.BEI] = "ccmj_f_4_nv.mp3",				-- 北风_女
		[MJDef.PAI.ZHONG] = "ccmj_f_5_nv.mp3",				-- 红中_女
		[MJDef.PAI.FA] = "ccmj_f_6_nv.mp3",				-- 发财_女
		[MJDef.PAI.BAI] = "ccmj_f_7_nv.mp3",				-- 白板_女
		[MJDef.PAI.SUO_1] = "ccmj_s_1_nv.mp3",			-- 1条_女
		[MJDef.PAI.SUO_2] = "ccmj_s_2_nv.mp3",			-- 2条_女
		[MJDef.PAI.SUO_3] = "ccmj_s_3_nv.mp3",			-- 3条_女
		[MJDef.PAI.SUO_4] = "ccmj_s_4_nv.mp3",			-- 4条_女
		[MJDef.PAI.SUO_5] = "ccmj_s_5_nv.mp3",			-- 5条_女
		[MJDef.PAI.SUO_6] = "ccmj_s_6_nv.mp3",			-- 6条_女
		[MJDef.PAI.SUO_7] = "ccmj_s_7_nv.mp3",			-- 7条_女
		[MJDef.PAI.SUO_8] = "ccmj_s_8_nv.mp3",			-- 8条_女
		[MJDef.PAI.SUO_9] = "ccmj_s_9_nv.mp3",			-- 9条_女
		[MJDef.PAI.TONG_1] = "ccmj_t_1_nv.mp3",			-- 1筒_女
		[MJDef.PAI.TONG_2] = "ccmj_t_2_nv.mp3",			-- 2筒_女
		[MJDef.PAI.TONG_3] = "ccmj_t_3_nv.mp3",			-- 3筒_女
		[MJDef.PAI.TONG_4] = "ccmj_t_4_nv.mp3",			-- 4筒_女
		[MJDef.PAI.TONG_5] = "ccmj_t_5_nv.mp3",			-- 5筒_女
		[MJDef.PAI.TONG_6] = "ccmj_t_6_nv.mp3",			-- 6筒_女
		[MJDef.PAI.TONG_7] = "ccmj_t_7_nv.mp3",			-- 7筒_女
		[MJDef.PAI.TONG_8] = "ccmj_t_8_nv.mp3",			-- 8筒_女
		[MJDef.PAI.TONG_9] = "ccmj_t_9_nv.mp3",			-- 9筒_女
		[MJDef.PAI.WAN_1] = "ccmj_w_1_nv.mp3",			-- 1万_女
		[MJDef.PAI.WAN_2] = "ccmj_w_2_nv.mp3",			-- 2万_女
		[MJDef.PAI.WAN_3] = "ccmj_w_3_nv.mp3",			-- 3万_女
		[MJDef.PAI.WAN_4] = "ccmj_w_4_nv.mp3",			-- 4万_女
		[MJDef.PAI.WAN_5] = "ccmj_w_5_nv.mp3",			-- 5万_女
		[MJDef.PAI.WAN_6] = "ccmj_w_6_nv.mp3",			-- 6万_女
		[MJDef.PAI.WAN_7] = "ccmj_w_7_nv.mp3",			-- 7万_女
		[MJDef.PAI.WAN_8] = "ccmj_w_8_nv.mp3",			-- 8万_女
		[MJDef.PAI.WAN_9] = "ccmj_w_9_nv.mp3",			-- 9万_女
	},
}

-- 
local GAME_SOUND = {
	["bg"] = "ccmj_bg.mp3", 				--	背景音乐
	["game_end"] = "ccmj_game_end.mp3", 				--	游戏结束
	["game_lose"] = "ccmj_game_lose.mp3", 				--	游戏结束-输
	["game_lost"] = "ccmj_game_lost.mp3", 				--	游戏结束-计分
	["game_start"] = "ccmj_game_start.mp3", 				--	游戏开始
	["game_warn"] = "ccmj_game_warn.mp3", 				--	时间将到
	["game_win"] = "ccmj_game_win.mp3", 				--	游戏结束-赢
	["mopai"] = "ccmj_mopai.wav", 				--	摸牌
	["outcard"] = "ccmj_outcard.mp3", 				--	出牌声
	["send_card"] = "ccmj_send_card.mp3", 				--	发牌
	["shaizi"] = "ccmj_shaizi.mp3", 				--	扔骰子
}


local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local DZMJSoundManager = class("DZMJSoundManager")

function DZMJSoundManager:ctor()
	self:myInit()
end

function DZMJSoundManager:myInit()
	
end

-- 牌音效
function DZMJSoundManager:playCardSound( card, gender )
	if gender == MJDef.eGender.male then
		if CARD_SOUND[gender][card] then
			g_GameMusicUtil:playSound("sound/nan/" .. CARD_SOUND[gender][card])
		end
	else
		if CARD_SOUND[gender][card] then
			g_GameMusicUtil:playSound("sound/nv/" .. CARD_SOUND[gender][card])
		end
	end
end

-- 操作音效
function DZMJSoundManager:playOptSound( opt, gender )
	if gender == MJDef.eGender.male then
		if OPT_SOUND[gender][opt] then
			g_GameMusicUtil:playSound("sound/nan/" .. OPT_SOUND[gender][opt])
		end
	else
		if OPT_SOUND[gender][opt] then
			g_GameMusicUtil:playSound("sound/nv/" .. OPT_SOUND[gender][opt])
		end
	end
end

-- 胡牌音效
function DZMJSoundManager:playHuSound( huType, gender )
	if gender == MJDef.eGender.male then
		if HU_SOUND[gender][huType] then
			g_GameMusicUtil:playSound("sound/nan/" .. HU_SOUND[gender][huType])
		end
	else
		if HU_SOUND[gender][huType] then
			g_GameMusicUtil:playSound("sound/nv/" .. HU_SOUND[gender][huType])
		end
	end
end

-- 游戏音效
function DZMJSoundManager:playGameSound( name, isLoop )
	-- print(name, GAME_SOUND[name])
	if GAME_SOUND[name] then
		g_GameMusicUtil:playSound("sound/" .. GAME_SOUND[name], isLoop)
	end
end

-- 背景音乐
function DZMJSoundManager:playGameBGSound( name, isLoop )
	-- print(name, GAME_SOUND[name])
	if GAME_SOUND[name] then
		g_GameMusicUtil:playBGMusic("sound/" .. GAME_SOUND[name], isLoop)
		local scene = display.getRunningScene()
		if scene.setMusicPath then
			scene:setMusicPath("sound/" .. GAME_SOUND[name])
		end
	end
end

-- 背景音乐
function DZMJSoundManager:setBGSoundPath(scene)
	-- print(name, GAME_SOUND[name])
	local name = "bg"
	if GAME_SOUND[name] then
		if scene.setMusicPath then
			scene:setMusicPath("sound/" .. GAME_SOUND[name])
		end
	end
end

return DZMJSoundManager
