--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 玩家手牌的数据操作（用来做数据校验）
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local DZMJHandLogic = class("DZMJHandLogic")
 
function DZMJHandLogic:ctor()
	self:myinit()
end

function DZMJHandLogic:myinit()

	self.canCheck = false

    self.playerManager = MJHelper:getPlayerManager()

	self.allCards = {}
	for i =0,3 do 
		local DZMJPlayerCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJPlayerCard")
		local playerCard = DZMJPlayerCard.new()
		--playerCard:setIndex(i)
		--table.insert(self.allCards,playerCard)
		self.allCards[i] = playerCard
	end

	self.optList = {
		[MJDef.OPER.CHU_PAI] = { text = "CHU_PAI", func = handler(self, self.onChuPai) },					--	出牌		ID..cid..card
		[MJDef.OPER.CHI] = { text = "CHI", func = handler(self, self.onChi) },					--	吃			ID..cid..card;
		[MJDef.OPER.PENG] = { text = "PENG", func = handler(self, self.onPeng) },				--	碰
		[MJDef.OPER.AN_GANG] = { text = "AN_GANG", func = handler(self, self.onAnGang) },				--	暗杠		（手上有4张牌杠）
		[MJDef.OPER.MING_GANG] = { text = "MING_GANG", func = handler(self, self.onMingGang) },			--	明杠		（手上有3张牌别人打出一张 拿过来杠）
		[MJDef.OPER.JIA_GANG] = { text = "JIA_GANG", func = handler(self, self.onJiaGang) },			--	加杠		（碰有三张牌，手牌有一张牌 补上去杠）

		[MJDef.OPER.YAO_GANG] = { text = "YAO_GANG", func = handler(self, self.commonGang) },			--	幺杠		ID..cid..card1..card2..card3;
		[MJDef.OPER.JIU_GANG] = { text = "JIU_GANG", func = handler(self, self.commonGang) },			--	九杠
		[MJDef.OPER.SAN_FENG_GANG] = { text = "SAN_FENG_GANG", func = handler(self, self.commonGang) },		--	三风杠
		[MJDef.OPER.SI_FENG_GANG] = { text = "SI_FENG_GANG", func = handler(self, self.commonGang) },		--	四风杠
		[MJDef.OPER.XI_GANG] = { text = "XI_GANG", func = handler(self, self.commonGang) },	
					--	喜杠
		[MJDef.OPER.FEI_DAN] = { text = "FEI_DAN", func = handler(self, self.onFeiDan) },				--	飞蛋

		[MJDef.OPER.BU_YAO_GANG] = { text = "BU_YAO_GANG", func = handler(self, self.onBuYaoGang) },			--	补幺杠		ID..cid..card;
		[MJDef.OPER.BU_JIU_GANG] = { text = "BU_JIU_GANG", func = handler(self, self.onBuJiuGang) },			--	补九杠
		[MJDef.OPER.BU_FENG_GANG] = { text = "BU_FENG_GANG", func = handler(self, self.onBuFengGang) },		--	补风杠
		[MJDef.OPER.BU_XI_GANG] = { text = "BU_XI_GANG", func = handler(self, self.onBuXiGang) },			--	补喜杠
		[MJDef.OPER.BU_FEI_DAN] = { text = "BU_FEI_DAN", func = handler(self, self.onBuFeiDan) },			--	补飞蛋

		[MJDef.OPER.CHI_TING] = { text = "CHI_TING", func = handler(self, self.onChiTing) },			--	吃听		ID..cid..card;
		[MJDef.OPER.PENG_TING] = { text = "PENG_TING", func = handler(self, self.onPengTing) },			--	碰听

		[MJDef.OPER.QG_CHI_TING] = { text = "QG_CHI_TING", func = handler(self, self.onQiangGangChiTing) },			--	抢杠吃听	（A补杠，B抢过来吃听）
		[MJDef.OPER.QG_PENG_TING] = { text = "QG_PENG_TING", func = handler(self, self.onQiangGangPengTing) },		--	抢杠碰听 	（A补杠，B抢过来碰听）
		[MJDef.OPER.QG_PENG] = { text = "QG_PENG", func = handler(self, self.onQiangGangPeng) },				--	抢杠碰		（A补杠，B抢过来碰）
		[MJDef.OPER.QG_GANG] = { text = "QG_GANG", func = handler(self, self.onQiangGangGang) },				--	抢杠杠		（A补杠，B抢过来明杠（4张牌））
		[MJDef.OPER.QG_HU] = { text = "QG_HU", func = handler(self, self.onQiangGangHu) },				--	抢杠胡		（A补杠，B抢过来胡）
		[MJDef.OPER.HU_PAI] = { text = "HU_PAI", func = handler(self, self.onHu) },				--	胡牌

		-- [MJDef.OPER.GAME_END_FLAG] = { text = "GAME_END_FLAG", func = handler(self, self.onGameEnd) },				--	胡牌
		[MJDef.OPER.FEN_ZHANG] = { text = "FEN_ZHANG", func = handler(self, self.onFengZhang), otype = 5 },			--	分张
	}

	self.myChairId =  nil

end



function DZMJHandLogic:getMyChairId()
	local myAid = Player:getAccountID()
	local myPlayerInfo = self.playerManager:getPlayerInfoByAid(myAid)
	if myPlayerInfo == nil then 
		return 
	end

	local id = 0
	id = myPlayerInfo:getChairId()
	return id

end

function DZMJHandLogic:getCardInfoByChairId(chair)
	return self.allCards[chair]
end

function DZMJHandLogic:doAnalysis(__idstr , __info)
	if self.optList[__info.m_stAction.m_iAcCode] then
		if __info.m_stAction.m_iAcCard and type(__info.m_stAction.m_iAcCard) == "table" then
			if self.optList[__info.m_stAction.m_iAcCode].func then
				-- 调用操作对应的处理函数

				local operData = self.allCards[__info.m_nOperChair] --操作者数据
				--local byOperData = self.allCards[__info.m_nByOperChair] --被操作者数据
				local byOperData = self.allCards[__info.m_stAction.m_ucByCid] --被操作者数据
				local data = __info.m_stAction  --操作数据

				self.optList[__info.m_stAction.m_iAcCode].func(operData,data,byOperData)
			end
		end
	end		
end

function DZMJHandLogic:reAnalysis(__index,__info)
	if self.optList[__info.m_stAction.m_iAcCode] then
		if __info.m_stAction.m_iAcCard and type(__info.m_stAction.m_iAcCard) == "table" then
			if self.optList[__info.m_stAction.m_iAcCode].func then
				-- 调用操作对应的处理函数

				local operData = self.allCards[__index] --操作者数据
				local byOperData = self.allCards[__index] --被操作者数据
				local data = __info.m_stAction  --操作数据(

				self.optList[__info.m_stAction.m_iAcCode].func(operData,data,byOperData)
			end
		end
	end		
end

function DZMJHandLogic:checkData( oriData,checkData )
	local data1 = nil
	local data2 = nil

	data1 = oriData:getAllHandCards()
	data2 = checkData.handCards

	local is_error = false
	if #data1 == #data2 then 
		local function compare (a,b)
			if a:getCard() == b:getCard() then
				return a:getId() < b:getId()
			else
				return a:getCard() < b:getCard()
			end
		end
		table.sort(data1,compare)
		table.sort(data2,compare)
		
		for i = 1 , #data2 do 
			if  data1[i]:getCard() == data2[i]:getCard() then 
				--什么都不做
			else
				print("牌的数量相等，牌值%s和%s不等",data1[i]:getCard(),data2[i]:getCard())
				--TOAST("牌值不相等")
				is_error = true
				break
			end
		end

		--print("牌的数量相等，牌值相等")

	else
		print("牌的数量%s不相等%s",#data1,#data2)
		--TOAST("牌的数量不相等")
		is_error = true
	end
	
	if is_error then

		if not self.is_error then
			self.is_error = true
			local message = "麻将牌校验错误：\n"
			for k,v in pairs(data2) do
				message = message .. MJDef.PAISTR[v:getCard()].."/"
			end
			local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
			local data = { message = message,leftStr="确定",rightStr="取消" }
			local dlg = MJDlgAlert.new()
			dlg:TowSubmitAlert(data)
			ToolKit:registDistructor( dlg, function ()
				display.resume()
				self.is_error = false
			end)
			dlg:showDialog()
			performWithDelay(dlg,function() display.pause() end,0.2)
		end
		
	end
end

function DZMJHandLogic:getAllCards()

	return self.allCards
end

function DZMJHandLogic:netMsgHandler(__idstr , __info)
	 
	self.myChairId =  self:getMyChairId()

	--添加摸牌到数组
	if "CS_G2C_Mj_Oper_Nty" == __idstr   then --？？
		 for k , v in pairs(__info.m_vecActions) do 
		 	  if v.m_iAcCode == MJDef.OPER.MO_PAI then   --摸牌
		 	  	local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
		 	  	local card = DZMJCard.new(v.m_iAcCard[1])
		 	  	table.insert(self.allCards[self.myChairId].handCards,card)
		 	  end
		 end
		
	--发手牌
	elseif "CS_G2C_Mj_SendHandCards_Nty" == __idstr then 
		self.canCheck = true
		self:clearData()	

		for k , v in pairs(__info.m_stHCards.m_vecCards) do 
			local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
			local card = DZMJCard.new(v)
			table.insert(self.allCards[self.myChairId].handCards,card)
		end

	--出牌（吃碰杠）
	elseif "CS_G2C_Mj_OperRst_Nty" == __idstr then

		self:doAnalysis(__idstr , __info)
	--断线重连	
	elseif "CS_G2C_Mj_ScenePlaying_Nty" == __idstr then 
		self.canCheck = true
		self:clearData()

		self:updateAllCardsInfo(__info)
	elseif "CS_G2C_Mj_GameOver_Nty" == __idstr then
		self.canCheck = false
		self:clearData()
	end

	local gameManager = MJHelper:getGameManager()
	if self.canCheck then  --发完手牌和重连之后开始可以校验

		self:checkData(gameManager:getCardInfoByChairId(self.myChairId),self:getCardInfoByChairId(self.myChairId))
	end
end

function DZMJHandLogic:clearData(  )
	-- body
	for i =0,3 do
		self.allCards[i]:clearData()
	end
	self.is_error = false
end

function DZMJHandLogic:updateAllCardsInfo(__info)
	-- body
	local DZMJWeave = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJWeave")
	local DZMJCard  = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
	

	--吃碰杠牌
	for i = 1 , #__info.m_vecAcCards do 
		for k , v in pairs(__info.m_vecAcCards[i].m_vecAction) do 
			local wrapInfo = {}
			wrapInfo.m_stAction = v
			self:reAnalysis(i - 1,wrapInfo)
		end
	end

	--其他的牌
	for i = 1 , #__info.m_vecOutCards do 
		--已经出的牌
		for k , v in pairs(__info.m_vecOutCards[i].m_vecCards) do
			local card = DZMJCard.new(v)
			table.insert(self.allCards[i - 1].outCards,card)
		end
	end

	--最后发自己手牌
	for i = 1 , #__info.m_stMyHCards.m_vecCards do 
		local card = DZMJCard.new(__info.m_stMyHCards.m_vecCards[i])		
		table.insert(self.allCards[self.myChairId].handCards,card)
	end
end

-- 删除一组手牌
function DZMJHandLogic:removeCards(operData,data)
	for k , card in pairs(data) do 
		operData:removeHandCard(card)
	end
end

-- 当前牌插入手牌列表
function DZMJHandLogic:addCurCardToHands( operData,data,byOperData)
	
end

-- 吃
function DZMJHandLogic:onChi( operData,data,byOperData)
	--从手牌中去掉吃的牌
	operData:removeHandCard(data.m_iAcCard[1])  --删除第一张牌
	operData:removeHandCard(data.m_iAcCard[3])  --删除第三章牌

	--被操作的名牌中删除吃的牌
	byOperData:removeOutCard(data.m_iAcCard[2])

	--操作牌weave的更新
	self:addWeave(operData,data)

end

--添加一组刻子
function DZMJHandLogic:addWeave(operData,data)
	-- body
	local DZMJWeave = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJWeave")
	local weave =  DZMJWeave.new()
	weave:updateData(data)

	operData:addWeave(weave)
end

-- 碰
function DZMJHandLogic:onPeng( operData,data,byOperData)
	--从手牌中去掉吃的牌
	-- 删除自己的牌
	operData:removeHandCard(data.m_iAcCard[1])
	operData:removeHandCard(data.m_iAcCard[2])
	-- 删除碰过来的牌
	byOperData:removeOutCard(data.m_iAcCard[3])
	-- 添加组合
	self:addWeave(operData, data)
end

-- 暗杠
function DZMJHandLogic:onAnGang(operData,data,byOperData)
	for i = 1, 4 do
		if data.m_iAcCard[i] == nil then
			data.m_iAcCard[i] = MJDef.PAI.NULL
		end
	end
	-- 删除自己的牌
	self:removeCards(operData, data.m_iAcCard)
	-- 添加组合
	self:addWeave(operData, data)
end

-- 明杠
function DZMJHandLogic:onMingGang(operData,data,byOperData)
	-- 杠其他玩家
	if byOperData then
		-- 删除自己的牌
		local t = { data.m_iAcCard[1], data.m_iAcCard[2], data.m_iAcCard[3] }
		self:removeCards(operData, t)
		-- 删除杠过来的牌
		byOperData:removeOutCard(data.m_iAcCard[4])
		self:addWeave(operData, data)
		
	-- 摸牌杠
	else
		self:removeCards(operData, data.m_iAcCard)
		self:addWeave(operData, data)
	end
end

-- 加杠
function DZMJHandLogic:onJiaGang( operData,data,byOperData)
	-- 删除自己的牌
	operData:removeHandCard(data.m_iAcCard[1])
	-- 更新组合
	local t = operData:getWeaveByOper(MJDef.OPER.PENG)
	local weave = nil
	for k, tWeave in pairs(t) do
		local cards = tWeave:getCards()
		if cards[1]:getCard() == data.m_iAcCard[1] then
			weave = tWeave
			break
		end
	end
	if weave then
		weave:updateData(data)
	else
		--print("[MJError] JiaGang weave not be found.")
	end
end

--常规杠
function DZMJHandLogic:commonGang( operData,data,byOperData)
	self:removeCards(operData, data.m_iAcCard)
	self:addWeave(operData, data)
end

-- 小鸡飞弹
function DZMJHandLogic:onFeiDan( operData,data,byOperData)
end

-- 补特殊杠(特殊杠只会存在一个) 无此消息，此函数被调用
function DZMJHandLogic:buGang( operData,data,bu )
	local t = operData:getWeaveByOper(bu)
	local is = false
	if #t > 0 then
		local taggetWeave = t[1]
		local card = data.m_iAcCard[1]
		-- 补杠成功
		if taggetWeave:buGang(card) then
			is = true			
		else
			-- -- 没有找到补杠的牌，替换幺鸡
			local isReplace, num = taggetWeave:replaceYaoJi(card)
			if isReplace then
				operData:setYaoJiNum(operData:getYaoJiNum() + num)
				is = true
			end
		end
	end
	return is
end

-- 补幺杠
function DZMJHandLogic:onBuYaoGang( operData,data,byOperData )
	operData:removeHandCard(data.m_iAcCard[1])
	local bu = MJDef.OPER.YAO_GANG
	self:buGang(operData, data, bu)
end

-- 补九杠
function DZMJHandLogic:onBuJiuGang( operData,data,byOperData )
	-- 删除自己的牌
	operData:removeHandCard(data.m_iAcCard[1])
	local bu = MJDef.OPER.JIU_GANG
	self:buGang(operData, data, bu)
end

-- 补风杠
function DZMJHandLogic:onBuFengGang( operData,data,byOperData )
	-- 删除自己的牌
	operData:removeHandCard(data.m_iAcCard[1])
	-- 四风杠
	local bu = MJDef.OPER.SI_FENG_GANG
	if not self:buGang(operData, data, bu) then
		-- 三风杠
		local bu = MJDef.OPER.SAN_FENG_GANG
		if not self:buGang(operData, data, bu) then
			local t = operData:getWeaveByOper(bu)
			if #t > 0 then
				local taggetWeave = t[1]
				local card = data.m_iAcCard[1]
				taggetWeave:sanFengToSiFeng(card)
			end
		end
	end
end

-- 补喜杠
function DZMJHandLogic:onBuXiGang( operData,data,byOperData)
	-- 删除自己的牌
	operData:removeHandCard(data.m_iAcCard[1])
	local bu = MJDef.OPER.XI_GANG
	self:buGang(operData, data, bu)
end

-- 补飞弹
function DZMJHandLogic:onBuFeiDan( operData,data,byOperData )
	operData:removeHandCard(data.m_iAcCard[1])
	--跟幺鸡有关
	operData:setYaoJiNum(operData:getYaoJiNum() + 1)
end

-- 出牌
function DZMJHandLogic:onChuPai( operData,data,byOperData )
	if data.m_iAcCard[1] then
		local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
		operData:addOutCard(DZMJCard.new(data.m_iAcCard[1]))
		operData:removeHandCard(data.m_iAcCard[1])
	end
end

-- 吃听
function DZMJHandLogic:onChiTing( operData,data,byOperData )
		self:onChi(operData, data,byOperData)
end

-- 碰听
function DZMJHandLogic:onPengTing( operData,data,byOperData)
	data.m_iAcCode = MJDef.OPER.PENG
	self:onPeng(operData, data,byOperData)
end

-- 抢杠吃听
function DZMJHandLogic:onQiangGangChiTing( operData,data,byOperData)
	self:onChi(operData, data,byOperData)
end

-- 抢杠碰听
function DZMJHandLogic:onQiangGangPengTing( operData,data,byOperData)
	byOperData:removeHandCard(data.m_iAcCard[3])
	-- 删除自己的牌
	operData:removeHandCard(data.m_iAcCard[1])
	operData:removeHandCard(data.m_iAcCard[2])
	-- 添加组合
	data.m_iAcCode = MJDef.OPER.PENG
	self:addWeave(operData, data)
end

-- 抢杠碰
function DZMJHandLogic:onQiangGangPeng( operData,data,byOperData )
	byOperData:removeHandCard(data.m_iAcCard[3])
	-- 删除自己的牌
	operData:removeHandCard(data.m_iAcCard[1])
	operData:removeHandCard(data.m_iAcCard[2])
	-- 添加组合
	data.m_iAcCode = MJDef.OPER.PENG
	self:addWeave(operData, data)
end

-- 抢杠杠
function DZMJHandLogic:onQiangGangGang( operData,data,byOperData )
		-- 删除被抢玩家的牌
	byOperData:removeHandCard(data.m_iAcCard[4])
	-- 删除自己的牌
	operData:removeHandCard(data.m_iAcCard[1])
	operData:removeHandCard(data.m_iAcCard[2])
	operData:removeHandCard(data.m_iAcCard[3])
	-- 添加组合
	self:addWeave(operData, data)
end

-- 抢杠胡
function DZMJHandLogic:onQiangGangHu( operData,data,byOperData )
	--胡了不校验了
	self.canCheck = false
	-- byOperData:removeHandCard(data.m_iAcCard[4])
	-- -- 删除自己的牌
	-- operData:removeHandCard(data.m_iAcCard[1])
	-- operData:removeHandCard(data.m_iAcCard[2])
	-- operData:removeHandCard(data.m_iAcCard[3])
	-- -- 添加组合
	-- self:addWeave(operData, data)
end

function DZMJHandLogic:onHu()
	self.canCheck = false
end

function DZMJHandLogic:onFengZhang(operData,data)
	self.canCheck = false
end

-- function DZMJHandLogic:onGameEnd()
-- 	self.canCheck = false
-- end

return DZMJHandLogic