--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将玩家数据

local DZMJPlayer = class("DZMJPlayer")

function DZMJPlayer:ctor()
	self:myInit()
end

-- 初始化数据
function DZMJPlayer:myInit()
	self.index = 1 				-- 显示的位置(1：下，2：右，3：上，4：左)
	self.chairId = 0 			-- 椅子ID

	self.handCards = {} 		-- 手牌
	self.outCards = {} 			-- 弃牌列表
	self.weaves = {} 			-- 组合牌列表

	self.listenState = 0 		-- 听牌状态，0：没听，1：听
	self.acCard = -1 			-- 操作的牌 

	self.m_nState = 0 			-- 状态，1：离开，2：离线			
	self.ready = false
end

-- 设置显示的位置
function DZMJPlayer:setIndex( index )
	self.index = index or self.index  
end

function DZMJPlayer:getIndex()
	return self.index
end

-- 设置椅子ID
function DZMJPlayer:setChairId( chairId )
	self.chairId = chairId or self.chairId  
end

function DZMJPlayer:getChairId()
	return self.chairId
end

-- 设置手牌列表
function DZMJPlayer:setHandCards( handCards )
	self.handCards = handCards or self.handCards
end

function DZMJPlayer:getHandCards()
	return self.handCards
end

function DZMJPlayer:addHandCard( card )
	table.insert(self.handCards, card)
end

function DZMJPlayer:removeHandCard( card )
	for i = #self.handCards, 1, -1 do
		if self.handCards[i] == card then
			table.remove(self.handCards, i)
			break
		end
	end
	print("[MJError] remove unsuccess, card : " .. card .. " did not found.")
end

-- 设置弃牌列表
function DZMJPlayer:setOutCards( outCards )
	self.outCards = outCards or self.outCards
end

function DZMJPlayer:getOutCards()
	return self.outCards
end

-- 设置组合牌列表
function DZMJPlayer:addWeave( weave )
	table.insert(self.weaves, weave)
end

function DZMJPlayer:getWeaveByOper( optCode )
	local weave = nil
	for k, v in pairs(self.weaves) do
		if v.oper == optCode then
			weave = v
			break
		end
	end
	if not weave then
		print("[MJError] none weave with optCode : ", optCode)
	end
	return weave 
end

-- 设置听牌状态
function DZMJPlayer:setListenState( listenState )
	self.listenState = listenState or self.listenState
end

function DZMJPlayer:isListenState()
	if self.listenState == 1 then
		return true
	else
		return false
	end
end

-- 操作的牌
function DZMJPlayer:setActionCard( action )
	self.acCard = action
end

function DZMJPlayer:getActionCard()
	return self.acCard
end

-- 玩家状态
function DZMJPlayer:setState( state )
	self.m_nState = state or self.m_nState
end

function DZMJPlayer:getState()
	return self.m_nState
end

-- 玩家是否准备
function DZMJPlayer:setReady( ready )
	self.ready = ready
end

function DZMJPlayer:getReady()
	return self.ready
end



return DZMJPlayer

