--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将玩家牌型

local DZMJPlayerCard = class("DZMJPlayerCard")

function DZMJPlayerCard:ctor()
	self:myinit()
end

function DZMJPlayerCard:myinit()
	self.index = 1 				-- 显示的位置(1：下，2：右，3：上，4：左)
	self.chairId = -1 			-- 椅子ID

	self.handCards = {}
	self.outCards = {}
	self.weaves = {}

	self.yaojiNum = 0 			-- 小鸡数量

end	

function DZMJPlayerCard:clearData( )
	self.handCards = {}
	self.outCards = {}
	self.weaves = {}

	self.yaojiNum = 0 			-- 小鸡数量
end

function DZMJPlayerCard:getIndex()
	return self.index
end

function DZMJPlayerCard:setIndex(index)
	self.index = index or self.chairId
end

function DZMJPlayerCard:setChairId(id )
	self.chairId = id or self.chairId
end

function DZMJPlayerCard:getChairId( )
	return self.chairId
end

function DZMJPlayerCard:setHandCards(cards)
	self.handCards = cards or self.handCards
end

function DZMJPlayerCard:addHandCard(card)
	table.insert(self.handCards,card)
end

function DZMJPlayerCard:sortHandCards(cards)
	
	-- body
end

function DZMJPlayerCard:removeHandCard(card)
	-- body
	for i = #self.handCards, 1, -1 do
		if self.handCards[i]:getCard() == card  then
			table.remove(self.handCards, i)
			return
		end
	end
	if card then
		print("[MJError] remove unsuccess, card : " .. card .. " did not found.")
	else
		print(debug.traceback())
	end

end

function DZMJPlayerCard:removeOutCard(card)
	-- body

	if #self.outCards > 0 and self.outCards[#self.outCards]:getCard() == card then
		table.remove(self.outCards, #self.outCards)
	else
		print("[MJError] the last outcard not be right card")
	end
end

function DZMJPlayerCard:addWeave(weave)
	-- body
	table.insert(self.weaves,weave)
end

function DZMJPlayerCard:getWeaveByOper( oper)
	local t = {}
	for k, tWeave in pairs(self.weaves) do
		-- 找操作码
		if tWeave:getOperCode() == oper then
			table.insert(t, tWeave)
		end
	end
	
	if #t <= 0 then
		print("[MJError] none weave with optCode : ", oper)
	end
	return t 
end

function DZMJPlayerCard:addOutCard(card )
	-- body
		table.insert(self.outCards, card)
end

-- 小鸡数量
function DZMJPlayerCard:setYaoJiNum( num )
	self.yaojiNum = num
end

function DZMJPlayerCard:getYaoJiNum()
	return self.yaojiNum
end



return DZMJPlayerCard