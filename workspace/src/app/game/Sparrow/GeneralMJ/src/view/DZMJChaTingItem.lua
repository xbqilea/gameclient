--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 查听信息

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local START_POS_X = 112
local DIS_Y = 18
 
local DZMJChaTingItem = class("DZMJChaTingItem", function ()
	return display.newNode()
end)

function DZMJChaTingItem:ctor()
	self:myInit()

	self:setupViews()

	-- self:updateViews()
end

function DZMJChaTingItem:myInit()
	self.m_vnListenCard = {}
	self.m_vnHuTime = {}
	self.m_vnLeftCount = {}
end

function DZMJChaTingItem:setupViews()
	local view = UIAdapter:createNode("csb/ccmj_cha_ting_item.csb")
    self:addChild(view)

    self.layout_bg = view:getChildByName("layout_bg")
    self.img_hu = view:getChildByName("img_hu")

    self.items = {}
    self.items[1] = self:createItem(1)
end

function DZMJChaTingItem:setData( data )
	self.m_vnListenCard = data.m_vnListenCard or {}
	self.m_vnHuTime = data.m_vnHuTime or {}
	self.m_vnLeftCount = data.m_vnLeftCount or {}

	self:updateViews()
end

-- 更新界面
function DZMJChaTingItem:updateViews()
	if self.items[1] then
		local oneSize = self.items[1]:getContentSize()
		local num = #self.m_vnListenCard
		local newSize = cc.size(0, 0)
		if num > 8 then
			newSize = cc.size(oneSize.width * 8 + START_POS_X, oneSize.height * math.ceil(num / 8) + DIS_Y * (math.ceil(num / 8) + 1))
		else
			newSize = cc.size(oneSize.width * num + START_POS_X, oneSize.height * math.ceil(num / 8) + DIS_Y * (math.ceil(num / 8) + 1))
		end

		self.layout_bg:setContentSize(newSize)
		self:setContentSize(self.layout_bg:getContentSize())

		self.img_hu:setPositionY(newSize.height/2)
	end

	for k, item in pairs(self.items) do
		if k > #self.m_vnListenCard then
			item:setVisible(false)
		else
			item:setVisible(true)
		end
	end

	for k, card in pairs(self.m_vnListenCard) do
		if self.items[k] == nil then
			self.items[k] = self:createItem(k)
		end
		local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
		local t = { card = DZMJCard.new(card), state = MJDef.eCardState.e_front_small1 }
		self.items[k].cardItem:setData(t)

		item = self.items[k]

	    local size = item:getContentSize()
		item:setPosition(START_POS_X + (math.floor((k - 1) % 8))*size.width, self:getContentSize().height - math.ceil(k / 8)*(size.height + DIS_Y) )

		if self.m_vnHuTime[k] then
			item.lebel_1:setString(self.m_vnHuTime[k] .. MJHelper:STR( 1 ))
		end

		if self.m_vnLeftCount[k] then
			item.lebel_2:setString(self.m_vnLeftCount[k] .. MJHelper:STR( 2 ))
			if self.m_vnLeftCount[k] > 0 then
				item.lebel_2:setColor(cc.c4f(174, 255, 15, 255))
			else
				item.lebel_2:setColor(cc.c4f(204, 204, 204, 255))
			end
		end
	end
end

-- 创建一项牌+倍数+张数
function DZMJChaTingItem:createItem( k )
	local item = UIAdapter:createNode("csb/ccmj_cha_ting_card_item.csb")
    self.layout_bg:addChild(item)

    item.layout = item:getChildByName("layout")
    item.lebel_1 = item:getChildByName("lebel_1")
    item.lebel_2 = item:getChildByName("lebel_2")

    local MJCardItem = MJHelper:loadClass("src.app.game.Sparrow.MjCommunal.src.model.MJCardItem")
    item.cardItem = MJCardItem.new()
    item.layout:addChild(item.cardItem)

    item:setContentSize(item.layout:getContentSize())

    return item
end

return DZMJChaTingItem