--
-- Author: 
-- Date: 2018-08-07 18:17:10
--


local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local DZMJTotalRoundEndItem = class("DZMJTotalRoundEndItem", function ()
	return display.newLayer()
end)

function DZMJTotalRoundEndItem:ctor()
	self:myInit()

	self:setupViews()

    -- ToolKit:registDistructor( self, handler(self, self.onDestory) )
end

function DZMJTotalRoundEndItem:onDestory()
    print("DZMJTotalRoundEndItem:onDestory()")
end

function DZMJTotalRoundEndItem:myInit()
	
end

function DZMJTotalRoundEndItem:setupViews()
	local view = UIAdapter:createNode("csb/ccmj_total_round_end_item.csb")
    self:addChild(view)
    UIAdapter:adapter(view)

    -- 头像
    self.head = view:getChildByName("proj_head")
	self.btn_head = self.head:getChildByName("btn_head")
	self.txt_player_state = self.head:getChildByName("txt_player_state")
	self.img_banker_icon = self.head:getChildByName("img_banker_icon")
	self.img_unlink = self.head:getChildByName("img_unlink")

	self.img_banker_icon:setVisible(false)
	self.txt_player_state:setVisible(false)
	self.img_unlink:setVisible(false)

	self.img_room_owner = self.head:getChildByName("img_room_owner")

    self.label_name = view:getChildByName("label_name")
    self.label_id = view:getChildByName("label_id")
    self.mj_game_icon_bigwin = view:getChildByName("mj_game_icon_bigwin")
    self.mj_game_icon_bestpao = view:getChildByName("mj_game_icon_bestpao")
	
	self.label_name:setFontName("")
	self.label_id:setFontName("")
	
    self.labels = {}
    self.nums = {}
    for i = 1, 5 do
    	self.labels[i] = view:getChildByName("label_" .. i)
    	self.nums[i] = view:getChildByName("num_" .. i)
    	self.labels[i]:enableOutline(cc.c4f(120, 64, 37, 255), 2)
    	self.nums[i]:enableOutline(cc.c4f(120, 64, 37, 255), 2)
    end

    -- self.label_total_score = view:getChildByName("label_total_score")
    self.node_total_score = view:getChildByName("node_total_score")
    self.totalScoreNum = cc.LabelAtlas:_create(":0", "number/mj_game_jiesuan_num.png", 27, 34, 48)
	self.totalScoreNum:setAnchorPoint(cc.p(0.5, 0.5))
	self.node_total_score:addChild(self.totalScoreNum)

    self:initHead(self.head)

    self:initHead(self.head)
end

function DZMJTotalRoundEndItem:setPlayerInfo( info )
	local playerManager = MJHelper:getPlayerManager()
	local is_this_room_owner = playerManager:isThisRoomOwner(info) 
	-- if is_this_room_owner then
	-- 	self.img_room_owner:setVisible(true)
	-- else
		self.img_room_owner:setVisible(false)
	-- end

	-- if info:getAccountId() == Player:getAccountID() then
	-- 	self.label_name:setColor(cc.c4f(255, 253, 93, 255))
	-- 	self.label_id:setColor(cc.c4f(255, 253, 93, 255))
	-- else
	-- 	self.label_name:setColor(cc.c4f(255, 255, 255, 255))
	-- 	self.label_id:setColor(cc.c4f(255, 255, 255, 255))
	-- end
	
	self.label_name:setString(info:getNickname())
	self.label_id:setString("ID：" .. info:getAccountId())
	if self.head.head_node then
		self.head.head_node:updateTexture(info:getFaceId(),info:getAccountId())
	end
end

function DZMJTotalRoundEndItem:setData( data, bigWinNum, fireMaxNum )
	local tStr = MJHelper:getSkin():getTotalRoundTexts()
	self.labels[1]:setString(tStr[1])
	self.nums[1]:setString(data.m_ucLianZhCiShi)

	self.labels[2]:setString(tStr[2])
	self.nums[2]:setString(data.m_ucHuPaiCiShi)

	self.labels[3]:setString(tStr[3])
	self.nums[3]:setString(data.m_ucDianPaoCiShi)

	self.labels[4]:setString(tStr[4])
	self.nums[4]:setString(data.m_ucLaoPaoCiShi)

	self.labels[5]:setString(tStr[5])
	self.nums[5]:setString(data.m_ucDuiBaoCiShi)

	-- self.label_total_score:setString(string.format("%+d", data.m_nZhScore) )
	if data.m_nZhScore > 0 then
		self.totalScoreNum:setString(":" .. data.m_nZhScore)
	elseif data.m_nZhScore < 0 then
		self.totalScoreNum:setString(";" .. math.abs(data.m_nZhScore))
	else
		self.totalScoreNum:setString(data.m_nZhScore)
	end

	self.mj_game_icon_bigwin:setVisible(data.m_nZhScore == bigWinNum)
	self.mj_game_icon_bestpao:setVisible(data.m_ucDianPaoCiShi == fireMaxNum)
end

function DZMJTotalRoundEndItem:initHead( root )
	if not root.head_node then
		local img_head_bg = root:getChildByName("img_head_bg")
		local MJUIHelper = MJHelper:getUIHelper()
		local _img_head = MJUIHelper:initPlayerHead(img_head_bg,false,false,"majiang_tubiao_txk.png")
	
        root.head_node = _img_head
	end 
end

return DZMJTotalRoundEndItem