--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 总结算


local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local HNLayer = require("src.app.newHall.HNLayer")
local DZMJTotalRoundEndLayer = class("DZMJTotalRoundEndLayer", function ()
	 return HNLayer.new()
end)

function DZMJTotalRoundEndLayer:ctor()
	self:myInit()

	self:setupViews()

    ToolKit:registDistructor( self, handler(self, self.onDestory) )
end

function DZMJTotalRoundEndLayer:onDestory()
    print("DZMJTotalRoundEndLayer:onDestory()")
end

function DZMJTotalRoundEndLayer:myInit()
	self.gameManager = MJHelper:getGameManager(true)
    self.playerManager = MJHelper:getPlayerManager(true)
end

function DZMJTotalRoundEndLayer:setupViews()
	local view = UIAdapter:createNode("csb/ccmj_total_round_end_layer.csb")
    self:addChild(view)
    UIAdapter:adapter( view, handler(self, self.onOneRoundEndCallBack) )

    self.img_title_bg = view:getChildByName("img_title_bg")
    self.img_title = view:getChildByName("img_title")

    self.label_time = view:getChildByName("label_time")
    self.img_logo = view:getChildByName("img_logo")
    self.layout_bg = view:getChildByName("layout_bg")

    self:setupItems(view)

    self:setupSkin()
end

function DZMJTotalRoundEndLayer:setupItems( view )
    self.layouts = {}
    self.items = {}
    for i = 1, 4 do
        self.layouts[i] = view:getChildByName("layout_" .. i)
        local DZMJTotalRoundEndItem = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.view.DZMJTotalRoundEndItem")
        self.items[i] = DZMJTotalRoundEndItem.new()
        self.layouts[i]:addChild(self.items[i])
    end
end

function DZMJTotalRoundEndLayer:setupSkin()
    -- local img = MJHelper:getSkin():getGameLogoImg()
    -- self.img_logo:loadTexture(img, 1)
end

function DZMJTotalRoundEndLayer:setData( data )
	self.data = data

	self:updateViews()
end

-- 更新界面
function DZMJTotalRoundEndLayer:updateViews()
    -- 时间
    local str = MJHelper:STR( 5 ) .. "\n" .. os.date("%Y-%m-%d %X")
    self.label_time:setString(str)

    local bigWinNum = -1
    local fireMaxNum = -1
    for k, v in pairs(self.data.m_vstLastBalance) do
        if v.m_ucDianPaoCiShi > fireMaxNum and v.m_ucDianPaoCiShi > 0 then
            fireMaxNum = v.m_ucDianPaoCiShi
        end

        if v.m_nZhScore > bigWinNum and v.m_nZhScore > 0 then
            bigWinNum = v.m_nZhScore
        end
    end

    -- 玩家信息
    for i = 1, 4 do
        local playerInfo = self.playerManager:getPlayerInfoByCid(i - 1)
        if playerInfo and self.data.m_vstLastBalance[i] then
            local cardInfo = self.gameManager:getCardInfoByChairId(playerInfo:getChairId())
            self.items[i]:setVisible(true)
            self.items[i]:setPlayerInfo(playerInfo)
            self.items[i]:setData(self.data.m_vstLastBalance[i], bigWinNum, fireMaxNum)

            if playerInfo:getAccountId() == Player:getAccountID() then
                self.layouts[i]:setBackGroundImage("majiang_js_wj_bar1.png", 1)
            else
                --if i == 1 or i == 3 then
                self.layouts[i]:setBackGroundImage("majiang_js_wj_bar2.png", 1)
                --else
                    --self.layouts[i]:removeBackGroundImage()
                --end
            end
        else
            self.items[i]:setVisible(false)
            self.layouts[i]:setVisible(false)
        end
    end

    local size = self.layout_bg:getContentSize()
    local lsize = self.layouts[1]:getContentSize()
    local dis = (size.width - lsize.width*(#self.data.m_vstLastBalance))/2
    local posX = dis
    for i = 1, 4 do
        self.layouts[i]:setPositionX(posX + (i-1)*lsize.width)
    end
end

function DZMJTotalRoundEndLayer:onOneRoundEndCallBack( sender )
END1()
	local name = sender:getName()
    print("DZMJTotalRoundEndLayer ", name)
    if name == "btn_share" then
        sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "wx_share_img")
    elseif name == "btn_exit" then 
        UIAdapter:popScene()
       ConnectManager:send2SceneServer( g_GameController.m_gameAtomTypeId, "CS_C2M_Mj_SysExitGame_Req", {} ) 
    elseif name  == "btn_continue_gold" then
         self:close()
        local playerManager = MJHelper:getPlayerManager() 
	    if playerManager then
		    playerManager:clearRoomInfo()
	    end 
         ConnectManager:send2SceneServer( g_GameController.m_gameAtomTypeId, "CS_C2M_Mj_SysContinueGame_Req", {} ) 
    end
end

return DZMJTotalRoundEndLayer