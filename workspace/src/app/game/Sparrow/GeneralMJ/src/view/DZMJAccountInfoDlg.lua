--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将玩家信息

local QkaDialog = require("app.hall.base.ui.CommonView")
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")


local DZMJAccountInfoDlg = class("DZMJAccountInfoDlg", function ()
    return QkaDialog.new()
end)

function DZMJAccountInfoDlg:ctor()
	self:myInit()

	self:setupViews()
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_GAME_OVER_NTF, handler(self, self.closeDlg))
	ToolKit:registDistructor( self,handler(self, self.onExit))
end

function DZMJAccountInfoDlg:onExit()
	removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_GAME_OVER_NTF)
end

function DZMJAccountInfoDlg:closeDlg()
	self:closeDialog()
end

function DZMJAccountInfoDlg:myInit()

end

function DZMJAccountInfoDlg:setupViews()
	-- body
	self.root = UIAdapter:createNode("csb/ccmj_account_info.csb")
    self:addChild(self.root)
    UIAdapter:adapter(self.root, handler(self, self.onCreateLayerCallBack))
	local head = self.root:getChildByName("img_head_bg")
	self:initHead(self.root)
	
	self.txt_player_name = self.root:getChildByName("txt_player_name")
	self.txt_player_uid = self.root:getChildByName("txt_player_uid")
	
	self.txt_player_name:setFontName("")
	self.txt_player_uid:setFontName("")
	
	local size = self.txt_player_name:getContentSize()
	local x = self.txt_player_name:getPositionX()
	x = x - size.width/2
	self.txt_player_name:setAnchorPoint(cc.p(0,0.5))
	self.txt_player_name:setPositionX(x-5)
	
	local size = self.txt_player_uid:getContentSize()
	local x = self.txt_player_uid:getPositionX()
	x = x - size.width/2
	self.txt_player_uid:setAnchorPoint(cc.p(0,0.5))
	self.txt_player_uid:setPositionX(x)
	
	local playerManager = MJHelper:getPlayerManager(true)
	if playerManager and playerManager:isFreeRoom() then
		self.txt_player_uid:setVisible(false)
		
		self.txt_player_name:setPositionY(self.txt_player_uid:getPositionY())
	end
end

function DZMJAccountInfoDlg:initHead( root )
	if not root.head_node then
		local img_head_bg = root:getChildByName("img_head_bg")
		local MJUIHelper = MJHelper:getUIHelper()
		local _img_head = MJUIHelper:initPlayerHead(img_head_bg)
		
		root.head_node = _img_head
	end 
end

function DZMJAccountInfoDlg:updateViews(player)
	-- body
	self.player = player
	self.txt_player_name:setString(player:getNickname())
    self.txt_player_uid:setString("Uid:"..player:getAccountId())
	if self.root.head_node then
		self.root.head_node:updateTexture(player:getFaceId(),player:getAccountId())
	end
	if player:getAccountId() == Player:getAccountID() then
		for i=1,4 do
			local btn = self.root:getChildByName("btn_face_"..i)
			btn:setEnabled(false)
			btn:setColor(cc.c4f(125, 125, 125, 255))
			
			local img = self.root:getChildByName("img_face_bg_"..i)
			img:setColor(cc.c4f(125, 125, 125, 255))
		end
	end
end

function DZMJAccountInfoDlg:updatePos(point,size)
	-- body
	local my_size = self.root:getChildByName("layout_bg"):getContentSize()
	if point.x < display.cx then
		point.x = point.x +size.width/2+0
	else
		point.x = point.x -size.width/2-my_size.width
	end
	if point.y + my_size.height+40 > display.height then
		point.y = display.height - my_size.height-40
	end
	self.root:setPosition(point)
end


function DZMJAccountInfoDlg:onCreateLayerCallBack( sender )
	local name = sender:getName() 
    print("name: ", name)
	local tag = nil
    if name == "btn_face_1" then
		tag = "diutuoxie"
	elseif name == "btn_face_2" then
		tag = "jidan"
	elseif name == "btn_face_3" then
		tag = "xianhua"
	elseif name == "btn_face_4" then
		tag = "songchaopiao"
	end

	if tag then
		local gameManager = MJHelper:getGameManager(true)
		local cd = gameManager:getAmusingCd()
		-- print("cd : ", cd)
		if cd > 20 then
			TOAST("操作过于频繁，请稍作休息")
		else
			local data = {
			}
			data.tag = tag
			data.send_userId =Player:getAccountID()
			data.recive_userId =self.player:getAccountId()
			sendMsg(PublicGameMsg.MSG_PUBLIC_CHAT_AMUSING,data)
			self:removeFromParent()
			-- cd
			gameManager:addAmusingCd()
		end		
	end
end

return DZMJAccountInfoDlg