--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将操作提示界面

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local scheduler = require("framework.scheduler")

local DZMJOptTipsLayer = class("DZMJOptTipsLayer", function ()
	return display.newLayer()
end)

local BTN_DIS = 40

function DZMJOptTipsLayer:ctor()
	self:myInit()

	self:setupViews()

	self:updateViews()

	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_OPT_TIPS, handler(self, self.showOptTips))

    ToolKit:registDistructor( self, handler(self, self.onDestory) )
end

function DZMJOptTipsLayer:onDestory()
    print("DZMJOptTipsLayer:onDestory()")
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_OPT_TIPS)
end

function DZMJOptTipsLayer:myInit()
	self.gameManager = MJHelper:getGameManager(true)
end

function DZMJOptTipsLayer:setupViews()
	local view = UIAdapter:createNode("csb/ccmj_operate_tips.csb")
    self:addChild(view)
    UIAdapter:adapter(view, handler(self, self.onOptTipsLayerCallBack))

    self.layout_opt_tips = view:getChildByName("layout_opt_tips")
    
    
    -- 吃，碰，杠
    self.btn_chi = view:getChildByName("btn_chi")
    self.btn_peng = view:getChildByName("btn_peng")
    self.btn_gang = view:getChildByName("btn_gang")
    self.btn_ting = view:getChildByName("btn_ting")
    self.btn_hu = view:getChildByName("btn_hu")
    self.btn_guo = view:getChildByName("btn_guo")
    self.btn_qiangting = view:getChildByName("btn_qiangting")
    
    -- 转圈效果
    self.img_lights = {}
    self.img_lights[1] = self.btn_chi:getChildByName("spr_light")
    self.img_lights[2] = self.btn_peng:getChildByName("spr_light")
    self.img_lights[3] = self.btn_gang:getChildByName("spr_light")
    self.img_lights[4] = self.btn_ting:getChildByName("spr_light")
    self.img_lights[5] = self.btn_hu:getChildByName("spr_light")
    self.img_lights[6] = self.btn_guo:getChildByName("spr_light")
    self.img_lights[7] = self.btn_qiangting:getChildByName("spr_light")
    for i=1,7 do
    	self.img_lights[i]:setBlendFunc( gl.ONE , gl.ONE) 
        self.img_lights[i]:runAction( cc.RepeatForever:create(cc.RotateBy:create(1.2, 360)) )
    end

    self.optTipItems = {}
end

-- 显示提示
function DZMJOptTipsLayer:showOptTips( msgStr, optTips )
	self:updateViews()
end

-- 更新界面
function DZMJOptTipsLayer:updateViews()
	local opt = self.gameManager:getOptTipsByType(operType)
	for k, optTipItem in pairs(self.optTipItems) do
		if k > #opt then
			optTipItem:setVisible(false)
		else
			optTipItem:setVisible(true)
		end
	end

	self.btn_guo:setVisible(false)
	self.btn_gang:setVisible(false)
	self.btn_peng:setVisible(false)
	self.btn_chi:setVisible(false)
	self.btn_hu:setVisible(false)
	self.btn_ting:setVisible(false)
	self.btn_qiangting:setVisible(false)

	local posX = self.btn_guo:getPositionX()
	-- 过
	local opt = self.gameManager:getOptTipsByType(MJDef.OPERTPYE.GUO)
	if #opt > 0 then
		self.btn_guo:setVisible(true)
		self.btn_guo.sendData = opt[1]
		posX = posX - BTN_DIS - self.btn_guo:getContentSize().width
	end

	-- 胡
	local opt = self.gameManager:getOptTipsByType(MJDef.OPERTPYE.HU)
	if #opt > 0 then
		self.btn_hu:setVisible(true)
		self.btn_hu:setPositionX(posX)
		posX = posX - BTN_DIS - self.btn_hu:getContentSize().width

		self.btn_hu.sendData = opt[1]
	end

	-- 抢听
	local opt = self.gameManager:getOptTipsByType(MJDef.OPERTPYE.QIANG_TING)
	if #opt > 0 then
		self.btn_qiangting:setVisible(true)
		self.btn_qiangting:setPositionX(posX)
		posX = posX - BTN_DIS - self.btn_qiangting:getContentSize().width

		self.btn_qiangting.sendData = opt[1]
	end

	-- 听
	local opt = self.gameManager:getOptTipsByType(MJDef.OPERTPYE.TING)
	if #opt > 0 then
		self.btn_ting:setVisible(true)
		self.btn_ting:setPositionX(posX)
		posX = posX - BTN_DIS - self.btn_ting:getContentSize().width

		self.btn_ting.sendData = opt[1]
	end

	-- 杠
	local opt = self.gameManager:getOptTipsByType(MJDef.OPERTPYE.GANG)
	if #opt > 0 then
		self.btn_gang:setVisible(true)
		self.btn_gang:setPositionX(posX)
		posX = posX - BTN_DIS - self.btn_gang:getContentSize().width
	end

	-- 碰
	local opt = self.gameManager:getOptTipsByType(MJDef.OPERTPYE.PENG)
	if #opt > 0 then
		self.btn_peng:setVisible(true)
		self.btn_peng:setPositionX(posX)
		posX = posX - BTN_DIS - self.btn_peng:getContentSize().width
	end

	-- 吃
	local opt = self.gameManager:getOptTipsByType(MJDef.OPERTPYE.CHI)
	if #opt > 0 then
		self.btn_chi:setVisible(true)
		self.btn_chi:setPositionX(posX)
		posX = posX - BTN_DIS - self.btn_chi:getContentSize().width
	end

end

function DZMJOptTipsLayer:showWeaves( operType )
	self.btn_gang:setVisible(false)
	self.btn_peng:setVisible(false)
	self.btn_chi:setVisible(false)
	self.btn_hu:setVisible(false)
	self.btn_ting:setVisible(false)
	self.btn_qiangting:setVisible(false)

	local opt = self.gameManager:getOptTipsByType(operType)
	for k, optTipItem in pairs(self.optTipItems) do
		if k > #opt then
			optTipItem:setVisible(false)
		else
			optTipItem:setVisible(true)
		end
	end

	local posX = self.btn_guo:getPositionX()
	posX = posX - self.btn_guo:getContentSize().width/2
	local posY = 0
	for k, v in pairs(opt) do
		if (k-1) % 4 == 0 then
			posX = self.btn_guo:getPositionX() - self.btn_guo:getContentSize().width/2
		end
		if self.optTipItems[k] == nil then
			local DZMJOptTipItem = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.view.DZMJOptTipItem")
			self.optTipItems[k] = DZMJOptTipItem.new()
			self.layout_opt_tips:addChild(self.optTipItems[k])
		end
		self.optTipItems[k]:setData(v)
		posX = posX - BTN_DIS - self.optTipItems[k]:getContentSize().width
		self.optTipItems[k]:setPositionX(posX)

		posY = self.btn_guo:getPositionY() - self.optTipItems[k]:getContentSize().height/2 + math.floor((k - 1)/4) * self.optTipItems[k]:getContentSize().height
		self.optTipItems[k]:setPositionY(posY)
	end
end

-- 点击事件
function DZMJOptTipsLayer:onOptTipsLayerCallBack( sender )
	local name = sender:getName()
	print("DZMJOptTipsLayer:onOptTipsLayerCallBack", name)

	if name == "btn_guo" then
		sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "operate", sender.sendData)
	elseif name == "btn_gang" then
		local opt = self.gameManager:getOptTipsByType(MJDef.OPERTPYE.GANG)
		if #opt == 1 then
			sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "operate", opt[1])
		else
			self:showWeaves(MJDef.OPERTPYE.GANG)
		end
	elseif name == "btn_peng" then
		local opt = self.gameManager:getOptTipsByType(MJDef.OPERTPYE.PENG)
		if #opt == 1 then
			sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "operate", opt[1])
		else
			self:showWeaves(MJDef.OPERTPYE.PENG)
		end
	elseif name == "btn_chi" then
		local opt = self.gameManager:getOptTipsByType(MJDef.OPERTPYE.CHI)
		if #opt == 1 then
			sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "operate", opt[1])
		else
			self:showWeaves(MJDef.OPERTPYE.CHI)
		end
	elseif name == "btn_hu" then
		sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "operate", sender.sendData)
	elseif name == "btn_ting" then
		sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "operate", sender.sendData)
	elseif name == "btn_qiangting" then
		-- sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "operate", sender.sendData)
		local opt = self.gameManager:getOptTipsByType(MJDef.OPERTPYE.QIANG_TING)
		if #opt == 1 then
			sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "operate", opt[1])
		else
			self:showWeaves(MJDef.OPERTPYE.QIANG_TING)
		end
	end
end

return DZMJOptTipsLayer
