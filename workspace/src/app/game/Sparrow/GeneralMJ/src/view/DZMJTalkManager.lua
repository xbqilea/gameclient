--其他人的组件
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 设置界面

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local DZMJTalkManager = class("DZMJTalkManager", function()
    return display.newLayer()
end)

function DZMJTalkManager:ctor()
    print("===DZMJTalkManager:ctor===")
	self:myInit()
	self:setupViews()
end

function DZMJTalkManager:myInit()
   
end

function DZMJTalkManager:setupViews()
    -- print("===DZMJTalkManager:setupViews===")
	self:initTalkVoiceLayer()
end

function DZMJTalkManager:onExit( )
    
end

--检测是否初始化了语音组件,如果没有初始化就初始化语音组件
function DZMJTalkManager:isCreateVoice()
--print("function DZMJTalkManager:isCreateVoice()")
    local state = false
    if true then
		if self.talkVoiceLayer == nil then
			self:initTalkVoiceLayer()
		end
		if self.talkVoiceLayer then
			state =true
		end    
    end
    
    return state
end

--初始化语音组件
function DZMJTalkManager:initTalkVoiceLayer()
--print("function DZMJTalkManager:initTalkVoiceLayer()")
    if self.talkVoiceLayer == nil then
--       myLog2("%s","stat===DZMJMainScene:initTalkVoiceLayer===")
		local TalkVoiceLayer =  MJHelper:loadClass("app.game.common.chat.ChatSystemLayer")
		local DZMJChatSystemConfig = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.view.DZMJChatSystemConfig")
		local playerManager = MJHelper:getPlayerManager()
		local gameAtomTypeId = playerManager:getGameAtomTypeId()
		
		local params = 
		{
			resourcePath = "src/app/game/Sparrow/GeneralMJ/res/dzmjtalkvoice",-- 资源路径
			gameUIConfig = DZMJChatSystemConfig, --长春麻将游戏配置
			gameAtomTypeId = gameAtomTypeId,
			serverReq = "CS_C2M_ClientChat_Nty",
			serverAck = "CS_M2C_ClientChat_Nty",
		}
		self.talkVoiceLayer = TalkVoiceLayer.new( params )  
		self:addChild(self.talkVoiceLayer)
		
		if self.talkVoiceLayer.messageButton and self.talkVoiceLayer.talkButton then
			-- 设置聊天按钮和语音按钮的位置
			local m_pos = cc.p(self.talkVoiceLayer.messageButton:getPosition())
			local t_pos = cc.p(self.talkVoiceLayer.talkButton:getPosition())
			m_pos.y = m_pos.y + 60
			t_pos.y = t_pos.y + 20
			self.talkVoiceLayer:setMessageAndTalkButtonPos(m_pos, t_pos )

			if playerManager:isVipRoom() then
				self.talkVoiceLayer.talkButton:setVisible(true)
			else
				self.talkVoiceLayer.talkButton:setVisible(false)
			end
		end
    end
end
-- 参数一表示玩家的用户id
-- 参数二表示玩家的位置
--  聊天的位置
-- 参数三表示是否播放表情的时候需要水平方向翻转
function DZMJTalkManager:setUserIdPos(_userId,_pos,_pos2,_isFlipX,_isFlipY)
    if self:isCreateVoice() then
		 local params = 
		  {    
			  userID = _userId,       --玩家id
			  messagepos = _pos, --播放快捷聊天位置/语音的位置
			  expressionPos= _pos2,-- 表情的位置
			  isFlippedX = _isFlipX ,-- 播放快捷聊天背景/语音背景
			  isFlippedY = _isFlipY ,-- 播放快捷聊天背景/语音背景
			  gender = 2              -- 性别：0.未知1.男 2.女.
		  }
        self.talkVoiceLayer:setUserIDPos(params)
    end
end


return DZMJTalkManager