--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 指示灯

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local scheduler = require("framework.scheduler")

local DZMJTurnNode = class("DZMJTurnNode", function ()
	return display.newLayer()
end)

function DZMJTurnNode:ctor()
	self:myInit()

	self:setupViews()

	self:updateTurnLight()
	self:updateLeftCardCount( )

	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_OPT_CLOCK, handler(self, self.showOptClock))
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_TABLEINFO, handler(self, self.updateTabelInfo))
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_TURN, handler(self, self.updateTurnLight))
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_LEFT_CARD_COUNT, handler(self, self.updateLeftCardCount))

    ToolKit:registDistructor( self, handler(self, self.onDestory) )
end

function DZMJTurnNode:onDestory()
    print("DZMJTurnNode:onDestory()")
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_OPT_CLOCK)
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_TABLEINFO)
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_TURN)
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_LEFT_CARD_COUNT)

    if self.pCDHandler then
	    scheduler.unscheduleGlobal(self.pCDHandler)
		self.pCDHandler = nil
	end
end

function DZMJTurnNode:myInit()
	self.gameManager = MJHelper:getGameManager(true)
end

function DZMJTurnNode:setupViews()
	local view = UIAdapter:createNode("csb/ccmj_turn_node.csb")
    self:addChild(view)
    UIAdapter:adapter(view)

    -- 指示
    self.layout_5 = view:getChildByName("layout_5")
    self.layout_turn = view:getChildByName("layout_turn")
    self.layout_turn_light = view:getChildByName("layout_turn_light")
    self.layout_turn_zi = view:getChildByName("layout_turn_zi")
    self.layout_turn_zi_light = view:getChildByName("layout_turn_zi_light")

    self.turnBgLight = {}
    self.turnZi = {}
    self.turnZiLight = {}

    for i = 1, 4 do
    	self.turnBgLight[i] = self.layout_turn_light:getChildByName("sprite_" .. i)
    	self.turnBgLight[i]:setVisible(false)
    	self.turnZi[i] = self.layout_turn_zi:getChildByName("sp_zi_" .. i)
    	self.turnZiLight[i] = self.layout_turn_zi_light:getChildByName("sp_zi_light_" .. i)
    	self.turnZiLight[i]:setVisible(false)
    end
    self.labAtlasTimeCd = cc.LabelAtlas:_create(":0", "number/mj_game_num_time.png", 19, 30, 48)
    self.labAtlasTimeCd:setPosition(self.layout_5:getContentSize().width/2, self.layout_5:getContentSize().height/2)
    self.labAtlasTimeCd:setAnchorPoint(cc.p(0.5, 0.5))
    self.labAtlasTimeCd:setVisible(false)
    self.layout_5:addChild(self.labAtlasTimeCd)

    self.labAtlasTimeCdRed = cc.LabelAtlas:_create(":0", "number/mj_game_num_time_red.png", 19, 30, 48)
    self.labAtlasTimeCdRed:setPosition(self.layout_5:getContentSize().width/2, self.layout_5:getContentSize().height/2)
    self.labAtlasTimeCdRed:setAnchorPoint(cc.p(0.5, 0.5))
    self.labAtlasTimeCdRed:setVisible(false)
    self.layout_5:addChild(self.labAtlasTimeCdRed)

    -- self.label_round = view:getChildByName("label_round")
    -- self.label_round:setString("")
    self.vip_round_node = self.layout_5:getChildByName("vip_round_node")
    self.vip_round_node:setVisible(false)
    self.free_round_node = self.layout_5:getChildByName("free_round_node")
    self.free_round_node:setVisible(false)

    local gameManager = MJHelper:getGameManager(true)
    local isVideo , cage = gameManager:getShowAllCardMode()
    local playerManager = MJHelper:getPlayerManager(true)
	if playerManager:isVipRoom() and not isVideo then  -- 
        self.vip_round_node:setVisible( true )
	    self.free_round_node:setVisible( false )
	    self.label_left_card_num = self.free_round_node:getChildByName("label_left_card_num")
		self.label_left_card_num = self.vip_round_node:getChildByName("label_left_card_num")
		self.label_left_card_num:setString( 0 )
		self.label_round = self.vip_round_node:getChildByName("label_round")
		self.label_round:setString( "0/0" )
	else
		self.vip_round_node:setVisible( false )
		self.free_round_node:setVisible( true )
		self.label_left_card_num = self.free_round_node:getChildByName("label_left_card_num")
		self.label_left_card_num:setString( 0 )
	end
   
end

-- 更新指示灯
function DZMJTurnNode:updateTurnLight()
	local rotate = {
		[0] = 0,
		[1] = -90,
		[2] = -180,
		[3] = -270,
		[-1] = 90,
		[-2] = 180,
		[-3] = 270,
	}
	local img = {
		[1] = {normal = "mj_game_turn_font_e.png", light = "mj_game_turn_font_e_1.png"},
		[2] = {normal = "mj_game_turn_font_s.png", light = "mj_game_turn_font_s_1.png"},
		[3] = {normal = "mj_game_turn_font_w.png", light = "mj_game_turn_font_w_1.png"},
		[4] = {normal = "mj_game_turn_font_n.png", light = "mj_game_turn_font_n_1.png"},
	}
	local playerManager = MJHelper:getPlayerManager(true)
	local gameManager = MJHelper:getGameManager(true)
	local myAid = Player:getAccountID()
	local bankerId = gameManager:getMaster()
	local playerInfo = playerManager:getPlayerInfoByAid(myAid)
	if not playerInfo then
		return 
	end
	local chairId = playerInfo:getChairId()
	
	if bankerId == nil then
		self:setVisible(false)
	else
		self:setVisible(true)
		local rotateIndex = gameManager:ChairIdToIndex(bankerId) - gameManager:ChairIdToIndex(chairId)
		self.layout_turn:setRotation(rotate[rotateIndex] - 90)
		self.layout_turn_light:setRotation(rotate[rotateIndex] - 90)

		-- dump(self.turnZi)
		-- dump(img)
		local index = gameManager:ChairIdToIndex(bankerId)
		local turnBgLightEIndex = 4
		self.map = {}
		-- print("index ", index)
		for i = 1, 4 do
			-- print(index)
			self.turnZi[index]:setSpriteFrame(display.newSpriteFrame(img[i].normal))
			self.turnZiLight[index]:setSpriteFrame(display.newSpriteFrame(img[i].light))

			self.map[index] = turnBgLightEIndex

			index = index + 1
			if index > 4 then
				index = 1
			end
			
			turnBgLightEIndex = turnBgLightEIndex + 1
			if turnBgLightEIndex > 4 then
				turnBgLightEIndex = 1
			end
		end
	end
	
end

-- 更新桌子信息
function DZMJTurnNode:updateTabelInfo( msgStr )
	local playerManager = MJHelper:getPlayerManager(true)	
	local gameManager = MJHelper:getGameManager(true)
	local data = playerManager:getRoomInitData()
	-- local str = MJHelper:STR(22) .. gameManager:getPlayCount() .. "/" .. playerManager:getRoundNum()
	local roundType = playerManager:getRoundUnit()
	local str = ""
	if roundType == 2 then
		str = gameManager:getPlayCount() .. "/" .. playerManager:getRoundNum()
	else
		print("gameManager:getPlayCount()", gameManager:getPlayCount())
		if data then
			str = "本圈" .. math.ceil(gameManager:getPlayCount()/data.m_nPlayerCount) .. "/" .. math.ceil(playerManager:getRoundNum()/data.m_nPlayerCount)
		end
	end
	self.label_round:setString(str)
	self.label_round:setColor(cc.c4b(255,251,115,255))
	self.label_round:enableShadow(cc.c4b(0,0,0,255), cc.size(1, -1))
end

-- 显示指示
function DZMJTurnNode:showOptClock( msgStr, paramStr )
	-- print("paramStr", paramStr)
	for i = 1, 4 do
    	self.turnBgLight[i]:setVisible(false)
    	self.turnBgLight[i]:stopAllActions()
    	self.turnZiLight[i]:setVisible(false)
    	self.turnZiLight[i]:stopAllActions()
    end
	self.labAtlasTimeCd:setVisible(true)

	if paramStr == "stop" then
		if self.pCDHandler then
		    scheduler.unscheduleGlobal(self.pCDHandler)
			self.pCDHandler = nil
		end
		self.labAtlasTimeCd:setVisible(false)
		self.labAtlasTimeCdRed:setVisible(false)
		return 
	end
    
	local function updateAction()
		local gameManager = MJHelper:getGameManager(true)
		if not gameManager then return end
		local time = math.ceil(gameManager:getOptCD())
		self.labAtlasTimeCd:setString(time)
		self.labAtlasTimeCdRed:setString(time)

		local curChairId = gameManager:getCurOperator()
		if curChairId >= 0 and curChairId <= 3 then
			local curCardInfo = gameManager:getCardInfoByChairId(curChairId)
			local index = gameManager:ChairIdToIndex(curChairId)

			self.turnBgLight[self.map[index]]:setVisible(true)
			self.turnZiLight[index]:setVisible(true)
			if time <= 3 then
				self.labAtlasTimeCdRed:setVisible(true)
				self.labAtlasTimeCd:setVisible(false)
				self:runFadeAction(self.turnBgLight[self.map[index]], true)
				self:runFadeAction(self.turnZiLight[index], true)

				if time > 0 then
					local soundManager = MJHelper:getSoundManager()
					soundManager:playGameSound("game_warn")
				end
			else
				self.labAtlasTimeCdRed:setVisible(false)
				self.labAtlasTimeCd:setVisible(true)
				self:runFadeAction(self.turnBgLight[self.map[index]], false)
				self:runFadeAction(self.turnZiLight[index], false)
			end
		end
	end
	
	updateAction()

	if self.pCDHandler == nil then
		self.pCDHandler = scheduler.scheduleGlobal(updateAction, 1)
	end
end

-- 闪烁动画
function DZMJTurnNode:runFadeAction(node, isQuickFade)
    -- print("===DZMJTurnNode:runFadeAction===")
    local blink = 0.5 
    if isQuickFade then
        blink = 0.25
    end
    
    local fadeIn = cc.FadeIn:create(blink)
    local fadeOut = cc.FadeOut:create(blink)
    node:stopAllActions()
    node:runAction(cc.RepeatForever:create(cc.Sequence:create(fadeIn, fadeOut)))
end

-- 更新剩余牌数
function DZMJTurnNode:updateLeftCardCount( msgStr )
	local gameManager = MJHelper:getGameManager(true)
	local leftCardCount = gameManager:getLeftCardCount()
	self.label_left_card_num:setString(leftCardCount)
end


return DZMJTurnNode
