--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 结算界面牌局信息项


local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local DZMJOneRoundEndItem = class("DZMJOneRoundEndItem", function ()
	return display.newNode()
end)

function DZMJOneRoundEndItem:ctor()
	self:myInit()

	self:setupViews()

	-- self:updateViews()
end

function DZMJOneRoundEndItem:myInit()
	
end

function DZMJOneRoundEndItem:setupViews()
	local view = UIAdapter:createNode("csb/ccmj_one_round_end_item.csb")
    self:addChild(view)
    UIAdapter:adapter( view )

    -- 头像
    self.head = view:getChildByName("proj_head")
	self.btn_head = self.head:getChildByName("btn_head")
	self.txt_player_state = self.head:getChildByName("txt_player_state")
	self.img_banker_icon = self.head:getChildByName("img_banker_icon")
	self.img_unlink = self.head:getChildByName("img_unlink")
	self.img_head_bg_0 = self.head:getChildByName("img_head_bg_0")
	self.img_head_bg_0:setVisible(false)

	self.img_banker_icon:setVisible(false)
	-- self.img_banker_icon:setPosition(-10, 40)
	self.txt_player_state:setVisible(false)
	self.img_unlink:setVisible(false)

	self.img_room_owner = self.head:getChildByName("img_room_owner")

	self.label_name = view:getChildByName("label_name")
	self.card_start_pos = view:getChildByName("card_start_pos")
	self.label_fan = view:getChildByName("label_fan")
	
	self.label_name:setFontName("")

	self.img_text = view:getChildByName("img_text")

	self.label_describe = view:getChildByName("label_describe")

	self.curCardItem = nil
	self.handCardItems = {}
	self.weaveItems = {}

	self.imgYaoji = view:getChildByName("img_yaoji")
	self.imgYaoji:setVisible(false)
	local layout = view:getChildByName("layout")
	-- self.yaoJiNum = cc.LabelAtlas:_create(":0", "number/mj_num_xiaoji.png", 16, 17, 48)
	-- layout:addChild(self.yaoJiNum)
	-- self.yaoJiNum:setScale(self.imgYaoji:getScale())
	-- self.yaoJiNum:setAnchorPoint(cc.p(0, 0.5))
	-- self.yaoJiNum:setPosition(cc.p(self.imgYaoji:getPositionX() + self.imgYaoji:getContentSize().width*self.imgYaoji:getScale() + 5, self.imgYaoji:getPositionY()))

	self.node_total = view:getChildByName("node_total")
	self.totalScoreNum = cc.LabelAtlas:_create("/0", "number/majiang_js_jinshu1.png", 26, 40, 47)
	self.totalScoreNum:setAnchorPoint(cc.p(0.5, 0.5))
	self.node_total:addChild(self.totalScoreNum)
	self.totalScoreNum:setVisible(false)

	self.totalScoreDecr = cc.LabelAtlas:_create("/0", "number/majiang_js_lvshu2.png", 26, 40, 47)
	self.totalScoreDecr:setAnchorPoint(cc.p(0.5, 0.5))
	self.node_total:addChild( self.totalScoreDecr )
	self.totalScoreDecr:setVisible(false)

	self.label_fan_hu = view:getChildByName("label_fan_hu")
	self.label_fan_gang = view:getChildByName("label_fan_gang")

	self.img_win_bg = view:getChildByName("img_win_bg")
	self.img_line = view:getChildByName("img_line")
	self.img_weizhi = view:getChildByName("img_weizhi")
	self.img_weizhi:setVisible(false)

	self:initHead(self.head)
end

function DZMJOneRoundEndItem:setCardInfo( cardInfo )
	local index = cardInfo:getIndex()
	local handCards = cardInfo:getHandCards()
	local weaves = cardInfo:getWeaves()
	local curCard = cardInfo:getCurCard()

	local handcard_start_pos = self.card_start_pos

	local handCardItems = self.handCardItems
	local weaveItems = self.weaveItems

	for k, pCard in pairs(handCards) do
		if handCardItems[k] == nil then
			local MJCardItem = MJHelper:loadClass("src.app.game.Sparrow.MjCommunal.src.model.MJCardItem")
			handCardItems[k] = MJCardItem.new()
			handcard_start_pos:addChild(handCardItems[k], k)
		end

		local data = { card = pCard, state = MJDef.eCardState.e_front_small2 }
		handCardItems[k]:setData(data)
	end

	for k, pWeave in pairs(weaves) do
		if weaveItems[k] == nil then
			local MJWeaveItem = MJHelper:loadClass("src.app.game.Sparrow.MjCommunal.src.model.MJWeaveItem")
			weaveItems[k] = MJWeaveItem.new()
			handcard_start_pos:addChild(weaveItems[k])
		end
		local data = { weave = pWeave, index = 7 }
		weaveItems[k]:setData(data)
	end

	if self.curCardItem == nil then
		local MJCardItem = MJHelper:loadClass("src.app.game.Sparrow.MjCommunal.src.model.MJCardItem")
		self.curCardItem = MJCardItem.new()
		handcard_start_pos:addChild(self.curCardItem)
	end

	local posX = 0
	-- 更新组合牌
	for k, item in pairs(weaveItems) do
		if k > #weaves then
			item:setVisible(false)
		else
			item:setVisible(true)
			item:setPositionX(posX)

			posX = posX + item:getContentSize().width + 7
		end
	end

	-- 更新手牌
	for k, item in pairs(handCardItems) do
		if k > #handCards then
			item:setVisible(false)
		else
			item:setVisible(true)
			item:setPositionX(posX)

			posX = posX + item:getContentSize().width
		end
	end

	-- 更新当前牌
	if curCard then
		local data = { card = curCard, state = MJDef.eCardState.e_front_small2 }
		self.curCardItem:setData(data)
		self.curCardItem:setVisible(true)
		self.curCardItem:setPositionX(posX + 7)
	else
		self.curCardItem:setVisible(false)
	end

	-- 小鸡飞弹数量
		-- 更新小鸡飞弹
	-- local yaojiNum = cardInfo:getYaoJiNum()
	-- if yaojiNum > 0 then
	-- 	self.yaoJiNum:setVisible(true)
	-- 	self.imgYaoji:setVisible(true)
	-- 	self.yaoJiNum:setString(":" .. yaojiNum)
	-- else
		-- self.yaoJiNum:setVisible(false)
		-- self.imgYaoji:setVisible(false)
	-- end
end

function DZMJOneRoundEndItem:setPlayerInfo( info )
	local playerManager = MJHelper:getPlayerManager()
	local is_this_room_owner = playerManager:isThisRoomOwner(info) 
	-- if is_this_room_owner then
	-- 	self.img_room_owner:setVisible(true)
	-- else
		self.img_room_owner:setVisible(false)
	-- end

	if info:getAccountId() == Player:getAccountID() then
		self.label_name:setColor(cc.c4f(255, 253, 93, 255))
	else
		self.label_name:setColor(cc.c4f(255, 255, 255, 255))
	end
	self.label_name:setString(info:getNickname())
	-- if self.flag == 1 then
	-- 	self.label_name:setPositionY(10)
	-- 	self.head:setScale(1.12)
	-- 	self.head:setPosition(47, 25)
	-- 	self.img_head_bg_0:loadTexture("mj_bg_head_win.png", 1)
	-- else
	-- 	self.label_name:setPositionY(16)
	-- 	self.head:setScale(0.9)
	-- 	self.head:setPosition(60, 29)
	-- 	self.img_head_bg_0:loadTexture("mj_bg_head_f.png", 1)
	-- end
     
	local gameManager = MJHelper:getGameManager(true)
	if gameManager:getBanker() == info:getChairId() then
		self.img_banker_icon:setVisible(true)
	else
		self.img_banker_icon:setVisible(false)
	end
	if self.head.head_node then
		self.head.head_node:updateTexture(info:getFaceId(),info:getAccountId())
	end

	local img = {
		[1] = {normal = "mj_game_turn_font_e.png", light = "mj_game_turn_font_e_1.png"},
		[2] = {normal = "mj_game_turn_font_s.png", light = "mj_game_turn_font_s_1.png"},
		[3] = {normal = "mj_game_turn_font_w.png", light = "mj_game_turn_font_w_1.png"},
		[4] = {normal = "mj_game_turn_font_n.png", light = "mj_game_turn_font_n_1.png"},
	}
	local bankerId = gameManager:getMaster()
	local chairId = info:getChairId()
	local i = (chairId - bankerId + 4)%4 + 1
	self.img_weizhi:loadTexture(img[i].light, 1)
end

function DZMJOneRoundEndItem:setFanStr( gang, hu )
	-- local total = gang + hu
	-- if total > 0 then
	-- 	self.label_fan:setColor(cc.c4f(246, 64, 64, 255))
	-- else
	-- 	self.label_fan:setColor(cc.c4f(24, 194, 52, 255))
	-- end
	-- local str = string.format("%3d", hu)  .. MJHelper:STR( 3 ) .. " " .. string.format("%3d", gang) .. MJHelper:STR( 4 ) .. " " .. string.format("%+3d", total)
	-- self.label_fan:setString(str)
	if self.flag == 1 then
		-- self.label_fan_hu:setColor(cc.c4f(255, 240, 0, 255))
		-- self.label_fan_hu:enableOutline(cc.c4f(132, 0, 0, 255), 2)
		-- self.label_fan_gang:setColor(cc.c4f(255, 240, 0, 255))
		-- self.label_fan_gang:enableOutline(cc.c4f(132, 0, 0, 255), 2)
		self.label_fan_hu:setColor(cc.c4f(24, 231, 255, 255))
		self.label_fan_hu:enableOutline(cc.c4f(10, 40, 97, 255), 2)
		self.label_fan_gang:setColor(cc.c4f(24, 231, 255, 255))
		self.label_fan_gang:enableOutline(cc.c4f(10, 40, 97, 255), 2)
	else
		self.label_fan_hu:setColor(cc.c4f(24, 231, 255, 255))
		self.label_fan_hu:enableOutline(cc.c4f(10, 40, 97, 255), 2)
		self.label_fan_gang:setColor(cc.c4f(24, 231, 255, 255))
		self.label_fan_gang:enableOutline(cc.c4f(10, 40, 97, 255), 2)
	end
	self.label_fan_hu:setString(MJHelper:STR( 3 ) .. string.format("%+3d", hu))
	self.label_fan_gang:setString(MJHelper:STR( 4 ) .. string.format("%+3d", gang))

	local total = gang + hu
	if total > 0 then 
		self.totalScoreNum:setVisible(true)
		self.totalScoreDecr:setVisible(false)
		self.totalScoreNum:setString("/" .. total)
	elseif total < 0 then
		self.totalScoreNum:setVisible(false)
		self.totalScoreDecr:setVisible(true)
		self.totalScoreDecr:setString("/" .. math.abs(total) )
	else
		self.totalScoreNum:setVisible(true)
		self.totalScoreDecr:setVisible(false)
		self.totalScoreNum:setString(total)
	end
end

function DZMJOneRoundEndItem:setFlag( flag )
	self.flag = flag
	if flag == 1 then
		self.img_text:setVisible(true)
		self.img_win_bg:setVisible(true)
		self.img_text:loadTexture("mj_game_jiesuan_font_hu.png", 1)

	elseif flag == 2 then
		self.img_text:setVisible(true)
		self.img_win_bg:setVisible(false)
		self.img_text:loadTexture("mj_game_jiesuan_font_pao.png", 1)
	elseif flag == 4 then
		self.img_text:setVisible(true)
		self.img_win_bg:setVisible(false)
		self.img_text:loadTexture("mj_game_jiesuan_font_liuju.png", 1)
	else
		self.img_win_bg:setVisible(false)
		self.img_text:setVisible(false)
	end
end

function DZMJOneRoundEndItem:setDescribe( str )
	if self.flag == 1 then
		-- self.label_describe:setPosition(cc.p(231, 98))
		self.label_describe:setFontSize(18)
		self.label_describe:setColor(cc.c4f(255, 240, 0, 255))
		self.label_describe:enableOutline(cc.c4f(132, 0, 0, 255), 2)
	else
		-- self.label_describe:setPosition(cc.p(231, 91))
		self.label_describe:setFontSize(24)
		self.label_describe:setColor(cc.c4f(192, 222, 231, 255))
		self.label_describe:enableOutline(cc.c4f(35, 54, 61, 255), 2)
	end

	self.label_describe:setString(str)
end

function DZMJOneRoundEndItem:initHead( root )
	if not root.head_node then
		local img_head_bg = root:getChildByName("img_head_bg")
		local MJUIHelper = MJHelper:getUIHelper()
		local _img_head = MJUIHelper:initPlayerHead(img_head_bg,false,false,"majiang_tubiao_txk.png")

        root.head_node = _img_head
	end 
end

return DZMJOneRoundEndItem