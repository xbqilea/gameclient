--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 牌友房游戏界面

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")


local DZMJVipGameLayer = class("DZMJVipGameLayer", function ()
    return display.newLayer()
end)

function DZMJVipGameLayer:ctor( scene )
    self.scene = scene

    self:myInit()

    self:setupViews()

	self:initPlayerLayer()
	
    self:initChatLayer()
	
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_LEFT_CARD_COUNT, handler(self, self.updateLeftCardCount))
	
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_GAME_SCENE_ANDROID_BACK, handler(self, self.onAndroidBackClick))
	
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_REFRESH_START, handler(self, self.reFreshStart))
	
	ToolKit:registDistructor( self, handler(self, self.onDestory) )
end

-- 初始化成员变量
function DZMJVipGameLayer:myInit(  )  
	self.is_first_init = true
	self.is_start_game = false
	self.is_show_wxyq = true
	self.is_show_test = false
end

function DZMJVipGameLayer:onDestory()
	removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_LEFT_CARD_COUNT)
	removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_GAME_SCENE_ANDROID_BACK)
end

function DZMJVipGameLayer:initChatLayer()
	--local DZMJTalkManager = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.view.DZMJTalkManager")
	--self.talkManager = DZMJTalkManager.new()
	--self:addChild(self.talkManager)
end

function DZMJVipGameLayer:setChatCallback(callback)
	self.talkManagerCallBack = callback
end

function DZMJVipGameLayer:initHead( root )
	if not root.head_node then
		local img_head_bg = root:getChildByName("img_head_bg")
		local MJUIHelper = MJHelper:getUIHelper()
		local _img_head = MJUIHelper:initPlayerHead(img_head_bg,false,false,"majiang_tubiao_txk.png")

        root.head_node = _img_head
	end 

end

function DZMJVipGameLayer:setClickCallBack(method)
    if method then
        UIAdapter:adapter(self.playerInfo,method)
    end
end
function DZMJVipGameLayer:initPlayerLayer()
	
	local view = UIAdapter:createNode("csb/ccmj_player_info_layer.csb")
	self.playerInfo = view
    self:addChild(self.playerInfo)
    UIAdapter:adapter(self.playerInfo, handler(self, self.onPlayerCallBack))
	
	local node_point_room_id = view:getChildByName("node_point_room_id")
	local roomid = MJHelper:getPlayerManager(true):getRoomId()
	self.txt_roomid = view:getChildByName("txt_roomid")
	self.txt_roomid:setString("")
	if roomid then
	   self.txt_roomid:setString(MJHelper:STR(27) .. roomid)
	end

	self.roundNumLabel = view:getChildByName("txt_room_round")
	self.play_data = {}
	for i=1,9 do
		self.play_data[i] = view:getChildByName("txt_font_wanfa_"..i)
	end	
	
	local MJCreateRoomData = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJCreateRoomData")
	local roomdata = MJCreateRoomData.new()
	local playerManager = MJHelper:getPlayerManager(true)

	self.txt_roomid_icon_0 = view:getChildByName("txt_roomid_icon_0")
	self.txt_roomid_icon_0:setVisible(false)
	local gameName = roomdata:getGameNameById(playerManager:getGameAtomTypeId())
	self.txt_roomid_icon_0:setString(gameName)

	local n =1 
	local t = {}
	local data = playerManager:getRoomInitData()
	if data  then
		local roundType = playerManager:getRoundUnit()
		local str = ""
		if roundType == 2 then
			str = MJHelper:STR(28) .. playerManager:getRoundNum() .. MJHelper:STR(29)
		else
			str = MJHelper:STR(28) .. math.ceil(playerManager:getRoundNum()/data.m_nPlayerCount) .. "圈"
		end
		self.roundNumLabel:setString(str)
		if data.m_vstSelection then
			for k,v in pairs(data.m_vstSelection) do
				if v.m_nId==MJDef.PLAYTYPE.THREE_EGG then
					local b = v.m_nId
					if v.m_nValue==0 then
						b = MJDef.PLAYTYPE.FOUR_EGG
					end
					local s = MJDef.PLAYTYPESTR[b] or ""
					table.insert(t, s)
					n = n +1
				else
					if v.m_nValue==1 then
						local s = roomdata:getDescribeById(v.m_nId) -- MJDef.PLAYTYPESTR[k] or ""
						table.insert(t, s)
						n = n +1
					end
				end
				
			end
		end
	end
	
	local txt_room_wanfa = view:getChildByName("txt_room_wanfa")
	local dis = 120
	local wanfaPosX = 440
	local itemPosX = 545
	local offsetX = 0

	if n == 2 then
		offsetX = dis * 2 / 2
	elseif n == 3 then
		offsetX = dis / 2
	end
	txt_room_wanfa:setPositionX(wanfaPosX + offsetX)

	for k, label in pairs(self.play_data) do
		if t[k] then
			label:setString(t[k])
			local i = (k - 1) % 3
			label:setPositionX(itemPosX + i*dis + offsetX)
		else
			label:setString("")
		end
	end
	
	self.player_list = {}
	for i=1,4 do
		local obj = {}
		self.player_list[i] = obj
		obj.head = view:getChildByName("proj_head_"..i)
		obj.btn_head = obj.head:getChildByName("btn_head")
		obj.txt_player_state = obj.head:getChildByName("txt_player_state")
		obj.img_banker_icon = obj.head:getChildByName("img_banker_icon")
		obj.img_unlink = obj.head:getChildByName("img_unlink")
		obj.img_room_owner = obj.head:getChildByName("img_room_owner")
		obj.img_head_bg_0 = obj.head:getChildByName("img_head_bg_0")
		obj.img_head_bg_0:setVisible(false)
		
		if i == 1 then
			--self:initHead(obj.head)
		end
		obj.node_info = view:getChildByName("node_head_info_"..i)
		
		--obj.txt_player_name = obj.node_info:getChildByName("txt_player_name_"..i)
		obj.img_score_bg = obj.node_info:getChildByName("img_score_bg_"..i)
		obj.txt_score = obj.node_info:getChildByName("txt_score_"..i)
		obj.btn_kick = obj.node_info:getChildByName("btn_kick_"..i)
		obj.btn_kick:setName("btn_kick")
		obj.txt_ip = obj.node_info:getChildByName("txt_ip_"..i)
		obj.txt_ip:setColor(cc.c4b(179,255,183,255))
		obj.txt_ip:setVisible(false)
		obj.txt_ip:setString("")
		
		obj.btn_kick:setVisible(false)
		
		obj.node_info:setVisible(false)
		obj.img_banker_icon:setVisible(false)
		obj.img_unlink:setVisible(false)
		obj.img_room_owner:setVisible(false)
	end
	
	
	local initData = playerManager:getRoomInitData()
	local maxPlayerCount = 4
	if initData then
		maxPlayerCount = initData.m_nPlayerCount or maxPlayerCount
	end

	if maxPlayerCount ==2 then
		local info = self.player_list[2]
		for k,v in pairs(info) do
			v:setVisible(false)
		end
		local info = self.player_list[4]
		for k,v in pairs(info) do
			v:setVisible(false)
		end	
	end

	self.btn_exit_room = view:getChildByName("btn_exit_room")

	
	self.btn_start = view:getChildByName("btn_start")
	self.btn_wxyq = view:getChildByName("btn_wxyq")
	self.img_wait_for_open = view:getChildByName("img_wait_for_open")
	self.layout_mid = view:getChildByName("layout_mid")

	view:getChildByName("txt_btn_start"):enableOutline(cc.c4b(149,117,29,255),3)
	view:getChildByName("txt_btn_wxyq"):enableOutline(cc.c4b(29,149,124,255),3)
	view:getChildByName("txt_btn_exit_room"):enableOutline(cc.c4b(24,101,108,255),2)
	
	
	
	
	
	self.img_wait_for_open:setVisible(false)
	
    
	self.txt_sys_time = view:getChildByName("txt_sys_time")
	self.txt_sys_time:setString(os.date("%H:%M"))
	local function update_time()
		local strTime = os.date("%H:%M")
		self.txt_sys_time:setString(strTime)
		--print(strTime)
	end
    update_time()
	schedule(self.txt_sys_time,update_time,1)

    self.img_icon_battery = view:getChildByName("img_icon_battery")
    local battery_node = view:getChildByName("battery_node")
    self.img_battery_num = {}
    for i=1,5 do
    	local img_battery_num_name = string.format("img_battery_num_%d",i)
    	self.img_battery_num[i] = battery_node:getChildByName( img_battery_num_name )
    	self.img_battery_num[i]:setVisible(false)
    end
    local function update_battery()
        local level = QkaPhoneInfoUtil:getBatteryLevel()
        level = level or 100
		if level >=80 then
			for i=1,5 do
				self.img_battery_num[i]:setVisible(true)
			end
		elseif  level >= 60 then
			for i=1,5 do
				if i == 5 then
				   self.img_battery_num[i]:setVisible(false)
				else
                   self.img_battery_num[i]:setVisible(true)
				end
			end
		elseif  level >=40 then
			for i=1,5 do
				if i >= 3 then
				   self.img_battery_num[i]:setVisible(false)
				else
                   self.img_battery_num[i]:setVisible(true)
				end
			end
		elseif  level >=20 then
			for i=1,5 do
				if i >= 2 then
				   self.img_battery_num[i]:setVisible(false)
				else
                   self.img_battery_num[i]:setVisible(true)
				end
			end
        else
           for i=1,5 do
				if i == 1 then
				   self.img_battery_num[i]:setVisible(true)
				else
                   self.img_battery_num[i]:setVisible(false)
				end
			end
		end
		--self.img_icon_battery:loadTexture(sigalName,1)
	end
    update_battery()
	schedule(self.img_icon_battery,update_battery,10)



	-- 剩余牌数
	self.label_left_count = view:getChildByName("label_left_count")
	self.label_left_count:setString(0)
	local manager = MJHelper:getPlayerManager(true)
    local index = manager:getCardBGType()
    print(index)
	self.img_left_count_bg = view:getChildByName("img_left_count_bg")
	self.img_left_count_bg:loadTexture(MJDef.PaiBG[index].MIN_4, 1)
	self.img_left_count_bg:setScale( 0.3 )

    self:initSkin(view)
end

function DZMJVipGameLayer:initSkin(view)
	local color = MJHelper:getSkin():getText5FontColor()
	self.txt_roomid:setColor(cc.c3b(179, 255, 183))
	self.roundNumLabel:setColor(cc.c3b(8, 73, 32))
	self.txt_roomid_icon_0:setColor(color)

	local colorWanfa = MJHelper:getSkin():getText5FontColorWanfa()
	local shadowColor = MJHelper:getSkin():getText5ShadowColor()
	if colorWanfa then
		local txt_room_wanfa = view:getChildByName("txt_room_wanfa")
		txt_room_wanfa:setColor(colorWanfa)
		txt_room_wanfa:enableShadow(shadowColor, cc.size(1, -1))

		for k, label in pairs(self.play_data) do
			label:setColor(colorWanfa)
			label:enableShadow(shadowColor, cc.size(1, -1))
		end
	else
		local txt_room_wanfa = view:getChildByName("txt_room_wanfa")
		txt_room_wanfa:setColor(color)
		for k, label in pairs(self.play_data) do
			label:setColor(color)
		end
	end
end

function DZMJVipGameLayer:initMenu(father)
	local view = UIAdapter:createNode("csb/ccmj_menu_layer.csb")
    father:addChild(view,130)
    UIAdapter:adapter(view, handler(self, self.onPlayerCallBack))
	
	self.img_pull_down_bg = view:getChildByName("img_pull_down_bg")
	self.btn_dismiss = view:getChildByName("btn_dismiss")
	self.btn_return = view:getChildByName("btn_return")
	self.btn_rule = view:getChildByName("btn_rule")
	
	--self.img_line_3 = view:getChildByName("img_line_3")
	
	self.btn_pull_down = view:getChildByName("btn_pull_down")
	self.layout_pull_down = view:getChildByName("layout_pull_down")
	self.layout_pull_down:setVisible(false)
	
	local playerManager = MJHelper:getPlayerManager(true)
	
	local blink = 1
	local fadeIn = cc.FadeIn:create(blink)
    local fadeOut = cc.FadeOut:create(blink)
    self.img_wait_for_open:stopAllActions()
    self.img_wait_for_open:runAction(cc.RepeatForever:create(cc.Sequence:create(fadeIn, fadeOut)))
		
	if playerManager:isFreeRoom() then
		self:startFreeGame()
	end
	
end
-- 更新剩余牌数
function DZMJVipGameLayer:updateLeftCardCount( msgStr )
	local gameManager = MJHelper:getGameManager(true)
	local leftCardCount = gameManager:getLeftCardCount()
	self.label_left_count:setString(leftCardCount)
end


function DZMJVipGameLayer:initPullDown()
	self.second_pos_y = self.btn_return:getPositionY()
	self.third_pos_y = self.btn_dismiss:getPositionY()
	self.bg_size = self.img_pull_down_bg:getContentSize()
	local playerManager = MJHelper:getPlayerManager(true)

end

function DZMJVipGameLayer:updatePullDownChange()
	local playerManager = MJHelper:getPlayerManager(true)
	local gameManager = MJHelper:getGameManager(true)
	if gameManager and not gameManager:isStartGame() then
		if playerManager:isVipRoom() then
			if playerManager:isCurRoomCreater() then
				self.btn_dismiss:setVisible(true)
				local size = clone(self.bg_size)
				self.img_pull_down_bg:setContentSize(size)
				--self.img_line_3:setVisible(true)
			else
				self.btn_dismiss:setVisible(false)
				local size = clone(self.bg_size)
				size.height = size.height-114
				self.img_pull_down_bg:setContentSize(size)
				--self.img_line_3:setVisible(false)
			end
		else
			self.btn_dismiss:setVisible(false)
			local size = clone(self.bg_size)
			size.height = size.height-114
			self.img_pull_down_bg:setContentSize(size)
			--self.img_line_3:setVisible(false)
		end
	end
	local state = playerManager:getRoomState()
	if state == 4 then
		self:updatePullDown()
	end
end

function DZMJVipGameLayer:updatePullDown()
	local playerManager = MJHelper:getPlayerManager(true)
	if playerManager:isVipRoom() then
		self.btn_return:setVisible(false)
		self.btn_dismiss:setVisible(true)
		if self.second_pos_y and self.bg_size then
			self.btn_dismiss:setPositionY(self.second_pos_y)
			local size = clone(self.bg_size)
			size.height = size.height-114
			self.img_pull_down_bg:setContentSize(size)
			--self.img_line_3:setVisible(false)
		end
	end
end

function DZMJVipGameLayer:startGame()
	
	local playerManager = MJHelper:getPlayerManager(true)
	if playerManager:isVipRoom() then
		self.btn_return:setVisible(false)
	else
        self.txt_roomid:setVisible(false)
	end
	self.btn_start:setVisible(false)
	self.btn_wxyq:setVisible(false)
	self.img_wait_for_open:setVisible(false)
	self.layout_mid:setVisible(false)
	self.btn_exit_room :setVisible(false)
	
	if not tolua.isnull( self.ani_wait_for_open_spr ) then
	   self.ani_wait_for_open_spr:removeFromParent() 
	   self.ani_wait_for_open_spr = nil
    end
	
	self:updatePullDown()
end


function DZMJVipGameLayer:onGameContinue()
    local playerManager = MJHelper:getPlayerManager(true)
    if playerManager:isVipRoom() then
        --self.layout_mid:setVisible(true)
    end
end

function DZMJVipGameLayer:startFreeGame()
	self.btn_start:setVisible(false)
	self.btn_wxyq:setVisible(false)
	self.img_wait_for_open:setVisible(false)
	self.layout_mid:setVisible(false)
	self.btn_exit_room :setVisible(false)
	self.txt_roomid:setVisible(false)
end

function DZMJVipGameLayer:onAndroidBackClick()
	local playerManager = MJHelper:getPlayerManager(true)
	if playerManager:isFreeRoom() then
		self:onPlayerCallBack(self.btn_return)
	end
end

function DZMJVipGameLayer:onPlayerCallBack(sender)
	local name = sender:getName() 
    print("namexx: ", name)
	if name == "btn_return" then
		if self.return_dlg then
			self.return_dlg:closeDialog()
			return 
		end
		self.layout_pull_down:setVisible(false)	
		local gameManager = MJHelper:getGameManager(true)
        local playerManager = MJHelper:getPlayerManager(true)
	
	    local is_room_owner = false
	    if playerManager:isCurRoomOwner() then
		    is_room_owner = true
	    end
        local message = "返回大厅将退出本房间"
        if is_room_owner then
            message = "返回大厅后可再次返回房间"
        end
		if playerManager:isFreeRoom() then
			message = "您确定要退出游戏吗？"
			if gameManager:isStartGame() then
				message = "您确定要退出游戏吗？退出后将自动托管"
				local str = "强退将暂时扣除%s金币用于本局结算，结算后自动返还剩余金币，是否退出？"
				local gameType = playerManager:getGameAtomTypeId()
				local createRoomdata = require("app.game.Sparrow.MjCommunal.src.MJConfig.Mj_RoomConfig")
				conf = createRoomdata[gameType] or {}
				local costCoin = conf.CostCoin or ""
				message = string.format(str,costCoin)
			end
			
		end
		
		if playerManager:isVipRoom() then
			if not gameManager:isStartGame() and (not is_room_owner or playerManager:isHelpCreateRoom()) then
				sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "leave_room")
				
				playerManager:setGameAtomTypeId(nil)
				playerManager:clearRoomInfo()
				sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "force_exit_room")	
				return 
			end
		end
		
        local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
        local data = { message = message,leftStr="确定",rightStr="取消" }
        local dlg = MJDlgAlert.new()
        dlg:TowSubmitAlert(data, function ()
              if playerManager:isFreeRoom() then
				local gameAtomTypeId = playerManager:getGameAtomTypeId()
				local roomId = playerManager:getRoomId()			 
				playerManager:setGameAtomTypeId(nil)
				playerManager:clearRoomInfo()
				sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "force_exit_room")	 
                ConnectManager:closeGameConnect(g_GameController.m_gameAtomTypeId)
	            UIAdapter:popScene()
            else
                sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "leave_room")
				
				playerManager:setGameAtomTypeId(nil)
				playerManager:clearRoomInfo()
				sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "force_exit_room")	
            end
            
        end)
        dlg:showDialog()
		
		self.return_dlg = dlg
		ToolKit:registDistructor( dlg, function ()
			self.return_dlg = nil
		end)
	--踢人
    elseif name == "btn_pull_down" then
    	self.layout_pull_down:setVisible(true)
	elseif name == "layout_pull_down" then
    	self.layout_pull_down:setVisible(false)	
	
	elseif name == "btn_set" then
    	local MJVipRoomSetDlg =  MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJVipRoomSetDlg")
        local Dlg = MJVipRoomSetDlg.new()
        Dlg:showDialog()
		self.layout_pull_down:setVisible(false)	
	elseif name == "btn_rule" then 
	    local MJHelpDlg =  MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJHelpDlg")
	    local Dlg = MJHelpDlg.new()
	    Dlg:showDialog()
		self.layout_pull_down:setVisible(false)		
	elseif name == "btn_dismiss" then
        local playerManager = MJHelper:getPlayerManager(true)
        if playerManager:isFreeRoom() then
          
        else       
			local gameManager = MJHelper:getGameManager(true)
			if gameManager:isStartGame() then
				sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "dis_room")
			else	
				local state = playerManager:getRoomState()
				if state == 4 then
					sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "dis_room")
				else	
					local message = "解散将清除房间并请离所有玩家"
					local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
					local data = { message = message,leftStr="确定",rightStr="取消" }
					local dlg = MJDlgAlert.new()
					dlg:TowSubmitAlert(data, function ()
						sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "dis_room")
					end)
					dlg:showDialog()
				end
			end
    	   
        end
		
    elseif name == "btn_head" then
  --   	if sender.chairId then
			
		-- 	local playerManager = MJHelper:getPlayerManager(true)
		-- 	local player = playerManager:getPlayerInfoByCid(sender.chairId)
		-- 	if player then
		-- 		local point = sender:convertToWorldSpaceAR(cc.p(0,0))
		-- 		local size = sender:getContentSize()
		-- 		print(point.x,point.y)
		-- 		local DZMJAccountInfoDlg =  MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.view.DZMJAccountInfoDlg")
		-- 		local Dlg = DZMJAccountInfoDlg.new()
		-- 		Dlg:enableAnimation(false)
		-- 		Dlg:setMountMode(0)
		-- 		Dlg:updateViews(player)
		-- 		Dlg:showDialog()
		-- 		Dlg:updatePos(point,size)
		-- 	end
		-- end	
	--踢人
    elseif name == "btn_kick" then
    	if sender.chairId then
            local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
            local data = { message = "确定要踢掉这名玩家吗？",leftStr="确定",rightStr="取消" }
            local dlg = MJDlgAlert.new()
            dlg:TowSubmitAlert(data, function ()
                sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "kick_player",sender.chairId)
            end)
            dlg:showDialog()
			
		end
	elseif name == "btn_shake" then
    	if sender.chairId then
			sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "shake",sender.chairId)
		end	
    -- 解散房间
    elseif name == "btn_exit_room" then
        local message = "解散将清除房间并请离所有玩家"
        local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
        local data = { message = message,leftStr="确定",rightStr="取消" }
        local dlg = MJDlgAlert.new()
        dlg:TowSubmitAlert(data, function ()
            sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "dis_room")
        end)
        dlg:showDialog()
    -- 准备
    elseif name == "btn_ready" then
        sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "ready")

    -- 开始游戏
    elseif name == "btn_start" then
        sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "start_game")
		
	elseif name == "btn_wxyq" then
		local MJCreateRoomData = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJCreateRoomData")
		local roomdata = MJCreateRoomData.new()

		local playerManager = MJHelper:getPlayerManager(true)
		local roomid = playerManager:getRoomId()
		local gameId = playerManager:getGameAtomTypeId()
		local gameName = roomdata:getGameNameById(playerManager:getGameAtomTypeId())
		local title = string.format(gameName .. "，房间号:%s",string.format("%06d",roomid))
		local content = "玩法："
		
		local data = playerManager:getRoomInitData()
		if data then
			if data.m_vstSelection then
				for k,v in pairs(data.m_vstSelection) do
					if v.m_nId==MJDef.PLAYTYPE.THREE_EGG then
						local b = v.m_nId
						if v.m_nValue==0 then
							b = MJDef.PLAYTYPE.FOUR_EGG
						end
						content=content..MJDef.PLAYTYPESTR[b].." "
					else
						if v.m_nValue==1 then
							content = content..roomdata:getDescribeById(v.m_nId).." "
						end
					end
				end
			end
		end
		-- print(title, content)
		local function callback(ret)
			if ret then
				print("share success!")
				local conf = MJHelper:getConfig()
				local playerManager = MJHelper:getPlayerManager(true)
				local is_room_owner = 0
				if playerManager:isCurRoomOwner() then
					is_room_owner = 1
				end
				local data = {
					RoomCode = roomid,
					UserId = Player:getAccountID(),
					UserAttributes = is_room_owner,
					GameTypeID = conf.CCMJ_VIPROOM_GAME_ID,
					SendTime = math.floor(os.time()),
				}
				sendMsg(PublicGameMsg.MSG_CCMJ_SEND_WX_INVITE,data)
			end
		end
		QkaShareUtil:wxInvite(MJHelper:getShareConf().WXInvite, roomid, content, callback,gameId)
    end
end
-- 初始化界面
function DZMJVipGameLayer:setupViews()
end

-- 设置数据
function DZMJVipGameLayer:updateViews(  )
	
end

-- 设置数据
function DZMJVipGameLayer:initInfo(params)
	
end

-- 更新手牌
function DZMJVipGameLayer:updateHandCard( msgStr, cardInfo )

end

-- 操作列表
function DZMJVipGameLayer:operateTips( msgStr, data )

end

function DZMJVipGameLayer:reFreshStart(msgStr,data)
	local gameManager = MJHelper:getGameManager(true)
	local playerManager = MJHelper:getPlayerManager(true)
	
	local is_room_owner = false
	if playerManager:isCurRoomOwner() then
		is_room_owner = true
	end	
	self.is_start_game = false
	if self.btn_start then
		gameManager:setStartGame(false)
		self.btn_start:setVisible(true)
		--self.btn_wxyq:setVisible(true)
		self.img_wait_for_open:setVisible(false)
		self.layout_mid:setVisible(true)
		if not is_room_owner then
			self.btn_start:setVisible(false)
		end
	end
end


-- 玩家信息更新
function DZMJVipGameLayer:onUpdatePlayerInfo(msgStr,players)
	
	local gameManager = MJHelper:getGameManager(true)
	local playerManager = MJHelper:getPlayerManager(true)
	
	if not gameManager then return end
		
	local is_room_owner = false
	if playerManager:isCurRoomOwner() then
		is_room_owner = true
	end
	
	if playerManager:isCurRoomCreater() and not playerManager:isHelpCreateRoom() then	
		if playerManager:isVipRoom() then
			if self.btn_return then
				local btn = self.btn_return
				btn:loadTextureNormal("majiang_anniu_xhuidat.png", ccui.TextureResType.plistType)
				btn:loadTexturePressed("majiang_anniu_xhuidat.png", ccui.TextureResType.plistType)
				btn:loadTextureDisabled("majiang_anniu_xhuidat.png", ccui.TextureResType.plistType)
			end
		end
	end
		
	if self.is_first_init then
		self.is_first_init = false
		if not is_room_owner then
			self.btn_start:setVisible(false)
			self.btn_exit_room :setVisible(false)
		end
		self:initPullDown()
		
		local myAid = Player:getAccountID()
		local myPlayerInfo = playerManager:getPlayerInfoByAid(myAid)
		if myPlayerInfo  then
			local flag = myPlayerInfo:getRechargingState()
			if flag == 1 then
				sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "continue_db_room")
			end
		end
	end
	
	self:updatePullDownChange()
	
		
	local isShowExitRoomBtn = false
	if playerManager:isCurRoomCreater() and playerManager:isVipRoom() then
		if not gameManager:isStartGame() then
			local state = playerManager:getRoomState()
			if state == 0 then
				isShowExitRoomBtn = true
			end
		end
	end
	self.btn_exit_room :setVisible(isShowExitRoomBtn)	


	local isFullMan = false
	local num = table.nums(players)	
	local initData = playerManager:getRoomInitData()
	local maxPlayerCount = 4
	if initData then
		maxPlayerCount = initData.m_nPlayerCount or maxPlayerCount
	end
	if num >= maxPlayerCount then
		isFullMan = true
	end
	
	local is_have_room_owner = false
	for k,v in pairs(players) do
		if playerManager:isThisRoomOwner(v) then
			is_have_room_owner = true
			break
		end
	end	
	
		
	if not self.is_start_game then
		if playerManager:isVipRoom() and self.is_show_wxyq then
			if isFullMan then
				self.btn_wxyq:setVisible(false)
			else
				self.btn_wxyq:setVisible(true)
			end
		end
		
		if gameManager:isStartGame() then
			self.is_start_game = true
			self:startGame()
			for i=1,4 do
				local player_info = self.player_list[i]
				player_info.btn_shake = player_info.btn_kick
				player_info.btn_shake:setName("btn_shake")
				player_info.btn_shake:setVisible(false)
				local name = "mj_game_btn_zhengd.png"
				player_info.btn_shake:loadTextures(name,name,name,1)

                player_info.txt_player_state:setVisible(false)

			end
		end 
	else

	end
	
	local isNeedToCharge =false
	for k, v in pairs(players) do
		if v:getRechargingState() > 0 then
			isNeedToCharge = true
		end
	end
	
	local isShowWaitOpen = false
	local img = nil
	if playerManager:isVipRoom() then
		if not gameManager:isStartGame() and not is_room_owner then
			isShowWaitOpen = true
			img = "mj_game_font_dengdaikaiju.png"
		end
		
		if gameManager:isStartGame() and gameManager:getGameState()==3 then
			isShowWaitOpen = true
			img = "mj_game_font_dengdaiwanjia.png"
		end
		

		
		if not gameManager:isStartGame() and isNeedToCharge then
			isShowWaitOpen = true
			img = "mj_game_font_ddwjbcdb.png"
		end
		
		if not isFullMan then
			isShowWaitOpen = false
		end
	else
        if not gameManager:isStartGame()  and not isFullMan then
           isShowWaitOpen = true
           img = "mj_game_font_dengdaiwanjia.png"
           if tolua.isnull( self.ani_wait_for_open_spr )  then
          	   self.ani_wait_for_open_spr = display.newSprite()
	       	   self:addChild( self.ani_wait_for_open_spr )
	           self.ani_wait_for_open_spr:setPosition( cc.p(display.width/2 ,display.height/2-100))
               self.ani_wait_for_open_spr:setVisible( true )
               local MJEffectManager = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJEffectManager")
               MJEffectManager.playAnimation( { node = self.ani_wait_for_open_spr ,  name = "jiazai" } )
           end
        end
	end
	self.img_wait_for_open:setVisible(isShowWaitOpen)
	if isShowWaitOpen and img then
		self.img_wait_for_open:loadTexture(img,1)
	end
    

	local isShowStartBtn = false
	if playerManager:isVipRoom() then
		if playerManager:isCurRoomOwner() and not isNeedToCharge then
			if not gameManager:isStartGame() and gameManager:getGameState()~=1 then
				isShowStartBtn = true
			end
		end
	end
	self.btn_start:setVisible(isShowStartBtn)
	if isShowStartBtn then
		if not isFullMan then
			self.btn_start:setEnable(false)
		else
			self.btn_start:setEnable(true)
		end
	end
	
	local update_list = {}	
	local str = ""
	local i = 1
    for k, v in pairs(players) do
		local chairId = v:getChairId()
		local index = gameManager:ChairIdToIndex(chairId)

		index = index or 0
		update_list[index] = true
		local player_info = self.player_list[index]
		
		if player_info then
			
			str = "已准备"
			local color  = cc.c3b(145,254,73)
			self:initHead(player_info.head)
			if player_info.head.head_node then
				player_info.head.head_node:setVisible(true)
				player_info.head.head_node:updateTexture(v:getFaceId(),v:getAccountId())
			end
			player_info.btn_head.chairId = v:getChairId()
			
			player_info.node_info:setVisible(true)
			player_info.btn_kick.chairId = v:getChairId()
            if not self.is_start_game then
			    player_info.txt_player_state:setVisible(true)
			    player_info.txt_player_state:setString(str)
			    player_info.txt_player_state:setColor(color)
				player_info.txt_player_state:enableOutline(cc.c4b(13,59,9,255),1)
			else
				player_info.txt_player_state:setVisible(false)
            end
			
			if  gameManager:getGameState()==3 then
				local s = v:getReady()
				local state = s == 1
				player_info.txt_player_state:setVisible(state)
			end
			
			local is_this_room_owner = playerManager:isThisRoomOwner(v) 
			if is_this_room_owner then
				if gameManager:isStartGame() or playerManager:isFreeRoom()  then
					player_info.img_room_owner:setVisible(false)
				else
					player_info.img_room_owner:setVisible(true)
				end
			else
				player_info.img_room_owner:setVisible(false)
			end

           if chairId== gameManager:getBanker() then
                player_info.img_banker_icon:setVisible(true)
           else
                player_info.img_banker_icon:setVisible(false)
           end
			
			if is_room_owner then
				player_info.btn_kick:setVisible(true)
			end
			
			if v:getAccountId() ==  Player:getAccountID() then
				player_info.btn_kick:setVisible(false)
			end
			
			if v:getCanKick()==0 and playerManager:getRoomState()==4 and not v:isLeaveState() then
				player_info.btn_kick:setVisible(false)
			end

			if not gameManager:isStartGame() then
				player_info.txt_score:setFontSize(16)
				player_info.txt_score:setFontName("")
				str = v:getNickname()
				--player_info.txt_player_name:setString(str)
			else
				player_info.txt_score:setFontSize(22)
				player_info.txt_score:setFontName("ttf/jcy.TTF")
				local score = v:getScore()
				score = ToolKit:shorterNumber(score)
				str = score
				if playerManager:isFreeRoom() then
					str = score
				end
			end 
			player_info.txt_score:setColor(cc.c4b(255,251,115,255))
			player_info.txt_score:setString(v:getScore()*0.01)
			local size = player_info.txt_score:getContentSize()
			size.width = size.width + 10
			player_info.img_score_bg:setContentSize(size)
			player_info.img_score_bg:setVisible(false)
            if playerManager:isVipRoom() then
               player_info.txt_ip:setVisible(true)
			   player_info.txt_ip:setString(v:getIp( ))
			end
			
			if player_info.btn_shake then
				player_info.btn_shake:setVisible(false)
			end
			
			if v:isUnlinkState() then
				player_info.img_unlink:setVisible(true)
			else
				player_info.img_unlink:setVisible(false)
			end		
			if v:isLeaveState() and index~=1 then
				if player_info.btn_shake then
					player_info.btn_shake:setVisible(true)
					local cd = v:getShakeCD()
					if cd > 0 then
						player_info.btn_shake:setTouchEnabled(false)
						player_info.btn_shake:loadTextures("mj_game_btn_zhengd_1.png", "mj_game_btn_zhengd_1.png", "mj_game_btn_zhengd_1.png", 1)
						player_info.btn_shake:setTitleText(cd .. "s")
						player_info.btn_shake:schedule(function ()
							local cd = v:getShakeCD()
							if cd > 0 then
								player_info.btn_shake:loadTextures("mj_game_btn_zhengd_1.png", "mj_game_btn_zhengd_1.png", "mj_game_btn_zhengd_1.png", 1)
								player_info.btn_shake:setTitleText(cd .. "s")
							else
								player_info.btn_shake:stopAllActions()
								player_info.btn_shake:setTouchEnabled(true)
								player_info.btn_shake:loadTextures("mj_game_btn_zhengd.png", "mj_game_btn_zhengd.png", "mj_game_btn_zhengd.png", 1)
								player_info.btn_shake:setTitleText("")
							end
						end, 1)
					else
						player_info.btn_shake:stopAllActions()
						player_info.btn_shake:setTouchEnabled(true)
						player_info.btn_shake:loadTextures("mj_game_btn_zhengd.png", "mj_game_btn_zhengd.png", "mj_game_btn_zhengd.png", 1)
						player_info.btn_shake:setTitleText("")
					end
				end
			else
				if player_info.btn_shake then
					player_info.btn_shake:setVisible(false)
				end
			end
			
			local point = player_info.btn_head:convertToWorldSpaceAR(cc.p(0,0))
			local userId = v:getAccountId()
			local messagepos = point
			local expressionPos = cc.p(point.x,point.y+20)
			local isFlippedX = false
			local isFlippedY = false
			if index ==2 or index ==3 then
				isFlippedX = true
			end
			if index == 3 then
				isFlippedY = true
			end
			if self.talkManagerCallBack then
				self.talkManagerCallBack(userId,messagepos,expressionPos,isFlippedX,isFlippedY)
			end
			
            --if self.talkManager then
			    --self.talkManager:setUserIdPos(userId,messagepos,expressionPos,isFlippedX,isFlippedY)
            --end
			
		else
			print("warning no player in game chair",i)
		end
    end
	
	for k = 1,4 do
		if not update_list[k] then
			local player_info = self.player_list[k]
			if player_info then
				player_info.btn_head.chairId = nil
				
				player_info.node_info:setVisible(false)
				player_info.img_banker_icon:setVisible(false)
				player_info.img_unlink:setVisible(false)
				player_info.img_room_owner:setVisible(false)
				if player_info.head.head_node then
					player_info.head.head_node:setVisible(false)
				end
				local color = cc.c3b(255,255,255)
				local str = "等待加入"
				player_info.txt_player_state:setVisible(true)
				player_info.txt_player_state:setString(str)
				player_info.txt_player_state:setColor(color)
				player_info.txt_player_state:enableOutline(cc.c4b(41,41,41,255),1)
			end
		end
	end
	
	if maxPlayerCount<4 then
		for i=maxPlayerCount,4-1 do
			local index = gameManager:ChairIdToIndex(3)
			local info = self.player_list[index]
			if info then
				for k,v in pairs(info) do
					v:setVisible(false)
				end
			end
		end
		
		print(gameManager.chairIdMap)
	end
    --self.label_players:setString(str)
end

return DZMJVipGameLayer
