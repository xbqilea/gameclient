--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将单局结算

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local DZMJOneRoundEndLayer = class("DZMJOneRoundEndLayer", function ()
	return display.newLayer()
end)

function DZMJOneRoundEndLayer:ctor()
	self:myInit()

	self:setupViews()

	-- self:updateViews()
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_SINGLE_RESULT, handler(self, self.updateSingleResult))

    ToolKit:registDistructor( self, handler(self, self.onDestory) )

    self:setKeypadEnabled(true)
    self:addNodeEventListener(cc.KEYPAD_EVENT, function (event)             
        if event.key == "back" then
            if self:isVisible() then
				local playerManager = MJHelper:getPlayerManager(true)
				if playerManager:isFreeRoom() then
					if self.btn_exit then
						self:onOneRoundEndCallBack( self.btn_exit )
						return true
					end
				end
			end
        end  
    end)
end

function DZMJOneRoundEndLayer:onDestory()
    print("DZMJOneRoundEndLayer:onDestory()")
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_SINGLE_RESULT)
end

function DZMJOneRoundEndLayer:myInit()
	self.gameManager = MJHelper:getGameManager(true)
    self.playerManager = MJHelper:getPlayerManager(true)
end

function DZMJOneRoundEndLayer:setupViews()
	local view = UIAdapter:createNode("csb/ccmj_one_round_end_layer.csb")
	self.view = view
    self:addChild(view,2)
    UIAdapter:adapter( view, handler(self, self.onOneRoundEndCallBack) )

    self.layouts = {}
    self.items = {}
    for i = 1, 4 do
    	self.layouts[i] = view:getChildByName("layout_" .. i)
        local DZMJOneRoundEndItem = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.view.DZMJOneRoundEndItem")
        self.items[i] = DZMJOneRoundEndItem.new()
        self.layouts[i]:addChild(self.items[i])
    end

    --self.img_title_bg = view:getChildByName("img_title_bg")
    --self.img_title = view:getChildByName("img_title")

    self.label_time = view:getChildByName("label_time")

    self.layout_baopai = view:getChildByName("layout_baopai")
    self.card_node = view:getChildByName("card_node")
    local MJCardItem = MJHelper:loadClass("src.app.game.Sparrow.MjCommunal.src.model.MJCardItem")
    self.baoCardItem = MJCardItem.new()
    self.baoCardItem:setScale(0.8)
    self.baoCardItem:setPositionY(-2)
    self.card_node:addChild(self.baoCardItem)

    self.label_hupai = view:getChildByName("label_hupai")

    self.layout_not_vip_room_btn = view:getChildByName("layout_not_vip_room_btn")
    self.layout_vip_room_btn = view:getChildByName("layout_vip_room_btn")	
	
	self.btn_exit = view:getChildByName("btn_exit")
	self.btn_exit:setVisible(false)
	self.btn_share1 = self.layout_not_vip_room_btn:getChildByName("btn_share")
	self.btn_share2 = self.layout_vip_room_btn:getChildByName("btn_share")
	self.btn_continue_vip = view:getChildByName("btn_continue_vip")
	self.btn_continue_gold = view:getChildByName("btn_continue_gold")

	self.btn_exit:enableOutline(cc.c4f(149, 52, 29, 255), 3)
	self.btn_share1:enableOutline(cc.c4f(149, 117, 29, 255), 3)
	self.btn_share2:enableOutline(cc.c4f(149, 117, 29, 255), 3)
	self.btn_continue_vip:enableOutline(cc.c4f(29, 149, 124, 255), 3)
	self.btn_continue_gold:enableOutline(cc.c4f(29, 149, 124, 255), 3)

	self.img_logo = view:getChildByName("img_logo")

	self:setupSkin()
end

function DZMJOneRoundEndLayer:setupSkin()
	local img = MJHelper:getSkin():getGameLogoImg()
	self.img_logo:loadTexture(img, 1)
end

local function matchBit(match_b,huType)
	local bb = bit.bnot(match_b)
	local l = bit.bor(bb,huType)
	if l == 0xffffffff then
		return true
	end
end

function DZMJOneRoundEndLayer:showAni(data)
	local winChair = nil 
	for i = 0, 3 do
		if self.data.m_winnerMap[i] == 1 then
			winChair = i 
			break
		end
	end
	
	local pos_table = 
	{	[1] = cc.p(display.cx,250),
		[2] = cc.p(display.width-200,display.cy+100),
		[3] = cc.p(display.cx,display.height-150),
		[4] = cc.p(200,display.cy+100),
	}
	
	local super_ani = 
	{
		MJDef.HUTYPE.TREASURES+MJDef.HUTYPE.CLIP,
		MJDef.HUTYPE.TREASURES+MJDef.HUTYPE.DRIFT,
		MJDef.HUTYPE.TREASURES+MJDef.HUTYPE.SEVEN_PAIR,
		MJDef.HUTYPE.CLIP+MJDef.HUTYPE.HIS_TREASURE,
		MJDef.HUTYPE.DRIFT+MJDef.HUTYPE.HIS_TREASURE,
		MJDef.HUTYPE.SEVEN_PAIR+MJDef.HUTYPE.HIS_TREASURE,
		MJDef.HUTYPE.QING_YI_SE+MJDef.HUTYPE.PENGPENG_HU,
		MJDef.HUTYPE.QXD_HH,		--	豪华七小对
		MJDef.HUTYPE.QXD_SHH,		--	-- 双豪华七小对
		MJDef.HUTYPE.QXD_ZZ,		--	至尊七小对
	}
	
	local spuer_ani_img = 
	{
		"mj_game_font_bzbjh.png", -- 
		"mj_game_font_bzbph.png",
		"mj_game_font_bzbqxd.png",
		"mj_game_font_jhlb.png",
		"mj_game_font_phlb.png",
		"mj_game_font_qxdlb.png",
		"mj_game_font_qyspph.png", -- 清一色碰碰胡 推到胡
		"mj_game_font_hhqxd.png",  -- 豪华七小对
		"mj_game_font_shhqxd.png", -- 双豪华七小对
		"mj_game_font_zzhhqxd.png",----	至尊七小对
	}
	
	local hight_ani = 
	{
		MJDef.HUTYPE.CLIP,
		MJDef.HUTYPE.HIS_TREASURE,
		MJDef.HUTYPE.TREASURES,
		MJDef.HUTYPE.DRIFT,	
		MJDef.HUTYPE.SEVEN_PAIR,	
		MJDef.HUTYPE.QING_YI_SE,
		MJDef.HUTYPE.GANG_KAI,
		MJDef.HUTYPE.PENGPENG_HU,

	}
	
	local hight_ani_img = 
	{
		"mj_game_font_jh.png",  --夹胡  
		"mj_game_font_lb.png",  --楼宝  
		"mj_game_font_bzb.png", --宝中宝 
		"mj_game_font_ph.png",  --飘胡  
		"mj_qxp",  --"mj_game_font_qxd.png", --七小对   推到胡
		"mj_qys",  --"mj_game_font_qys.png", --清一色   推到胡
		"mj_gskh", --"mj_game_font_gskh.png",--杠上开花 推到胡
		"mj_pph",  --"mj_game_font_pph.png", --碰碰胡   推到胡
		
	}

	local function show()
		performWithDelay(self,function()
			local gameManager = MJHelper:getGameManager(true)
			gameManager:setShowAllCardMode(true, 2)
			local cardInfos = gameManager:getCardInfos()
			for k, cardInfo in pairs(cardInfos) do
				sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_HANDCARD, cardInfo)
			end
			gameManager:setShowAllCardMode(false, nil)
		end, 0.5)

		performWithDelay(self,function()				
			self.view:setVisible(true)
			sendMsg(PublicGameMsg.MSG_CCMJ_GAME_CONTINUE)
			sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = false})
			local playerManager = MJHelper:getPlayerManager(true)
			local playerInfo = playerManager:getPlayerInfoByAid(Player:getAccountID()) 
			local myChairId = playerInfo:getChairId()
			-- print("myChairId", myChairId, winChair, winChair == myChairId)
			local soundManager = MJHelper:getSoundManager()
			if winChair == myChairId then
				soundManager:playGameSound("game_win")
			else
				soundManager:playGameSound("game_lose")
			end

			performWithDelay(self, function ()
				local soundManager = MJHelper:getSoundManager()
				soundManager:playGameBGSound("bg", true)
			end, 3)
		end, 2)
	end
	
	if winChair == nil then
		-- self.view:setVisible(true)
		-- sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = false})
		show()
	else
		local chair = winChair
		local b_chair = data.m_nFireChair
		local huType = self.playerResults[chair + 1]:getHuType()
		local playerManager = MJHelper:getPlayerManager(true)
		local playerInfo = playerManager:getPlayerInfoByCid(chair)
		local gender = 0
		if playerInfo then
			gender = playerInfo:getGender()
		end
		local b_index = self.gameManager:ChairIdToIndex(b_chair)

		-- 赢家先翻牌
		local winCardInfo =  self.gameManager:getCardInfoByChairId(chair)
		self.gameManager:setShowAllCardMode(true, 2)
		sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_HANDCARD, winCardInfo)
		self.gameManager:setShowAllCardMode(false, nil)
		
		local MJAnimation = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJAnimation")
		local soundManager = MJHelper:getSoundManager()
		local is_fire = matchBit(MJDef.HUTYPE.FIRE_GUN,huType)
		local is_self = matchBit(MJDef.HUTYPE.SELF_PICK,huType)
		
		local is_hight = false
		local is_super = false
		local key = 0
		local soundKey = 0
		for k,v in ipairs(super_ani) do
			if matchBit(v,huType) then
				is_super = true
				key = k
				soundKey = v
			end
		end 
		
		if not is_super then
			for k,v in ipairs(hight_ani) do
				if matchBit(v,huType) then
					is_hight = true
					key = k
					soundKey = v
				end
			end
		end
				
		local function next_play()
			soundManager:playHuSound(soundKey, gender)
			if is_super then
				self.superLayer = display.newColorLayer(cc.c4b(0,0,0,80))
				self:addChild(self.superLayer)
				local ani = MJAnimation:playArmature("majiang_effects","mj_qyspph",nil,true)
				self:addChild(ani)
				-- local img = spuer_ani_img[key]
				-- if img then
				-- 	MJAnimation:changeDisplay(ani,"zhi",0,img)
				-- end
				local p = cc.p(display.cx,display.cy)
				ani:setPosition(p)
				ani:setScale(0)
				local act = {}
				act[1] = cc.ScaleTo:create(0.1,1)
				local action = cc.Sequence:create(act)
				ani:runAction(action)
				performWithDelay(ani,function()
					self.superLayer:removeFromParent()
					ani:removeFromParent()
					show()
				 end, 2)
			elseif is_hight then
				local aniName = hight_ani_img[key]
				local ani = MJAnimation:playArmature("majiang_effects",aniName ,show)
				self:addChild(ani)
				
				local p = cc.p(display.cx,display.cy)
				ani:setPosition(p)
			else
				show()
			end
			self.backLayer:removeFromParent()
		end
		self.backLayer = display.newColorLayer(cc.c4b(0,0,0,150))
		self:addChild(self.backLayer)

		local isPlayedNext = false
		for chairId, isHu in pairs(self.data.m_winnerMap) do
			if isHu == 1 then
				local index = self.gameManager:ChairIdToIndex(chairId)
				local ani = nil
				local aniName = "mj_hu"
				if is_self then
				   aniName = "mj_zm"
				end
				if not isPlayedNext then
					isPlayedNext = true
					-- ani = MJAnimation:playArmature("thunder","",function ()
					-- 	performWithDelay(self, next_play, 2)
					-- end)
					ani = MJAnimation:playArmature("majiang_effects", aniName , next_play)
				else
					ani = MJAnimation:playArmature("majiang_effects", aniName )
				end
				self:addChild(ani)
				local p = pos_table[index] or cc.p(display.cx,display.cy)
				ani:setPosition(p)
				if not is_self then
					--MJAnimation:changeDisplay(ani,"zhi",0,"majiang_zi_zimo.png")
				else
		    		soundManager:playHuSound(MJDef.HUTYPE.SELF_PICK, gender)
				end
			end
		end
		
		if is_fire then
			local ani = MJAnimation:playArmature("majiang_effects","mj_dp")
			self:addChild(ani)
			local p = pos_table[b_index] or cc.p(display.cx,display.cy)
			ani:setPosition(p)
			--ani:setScale(0.6)
			local playerManager = MJHelper:getPlayerManager(true)
			local playerInfo = playerManager:getPlayerInfoByAid(Player:getAccountID()) 
			local myChairId = playerInfo:getChairId()
			-- 胡牌玩家和被胡玩家播点炮，其他玩家播平胡
			if winChair == myChairId or b_chair == myChairId then
    			soundManager:playHuSound(MJDef.HUTYPE.FIRE_GUN, gender)
    		else
    			if not is_super and not is_hight then
    				soundManager:playHuSound(MJDef.HUTYPE.LEVEL, gender)
    			end
    		end

    		local gameManager = MJHelper:getGameManager(true)
			local cardInfo, data = gameManager:getCurOutCardData()
			if cardInfo and data then
				local t = { index = cardInfo:getIndex(), card = data.m_iAcCard[1], isShow = true}
				sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_BIG_CARD, t)
				gameManager:setCurOutCardData(nil, nil)
			end
    	else
    		-- 不是点炮
    		local gameManager = MJHelper:getGameManager(true)
			local cardInfo, data = gameManager:getCurOutCardData()
			if cardInfo and data then
				sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_OUT_CARD_ANI, cardInfo, nil, 3)
				gameManager:setCurOutCardData(nil, nil)
			end
		end
	end
end

function DZMJOneRoundEndLayer:setData( data )
	-- dump(data)
	self.data = clone(data)
	self.data.m_winnerMap = {}
	local chairs = { 0x1, 0x2, 0x4, 0x8}
	for k, v in pairs(chairs) do
		if matchBit(v, self.data.m_nWinnerChairFlag) then
			self.data.m_winnerMap[k-1] = 1
		else
			self.data.m_winnerMap[k-1] = 0
		end
	end
	dump(self.data.m_winnerMap)
	self.playerResults = {}
	for k, v in pairs(self.data.m_stPlayerBalance) do
		local DZMJPlayerResult = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJPlayerResult")
		local result = DZMJPlayerResult.new()
		result:updateData(v)
		table.insert(self.playerResults, result)
	end

	self:updateViews()
	sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_SINGLE_RESULT, { name = "update_btn_continue" })
	
	self.view:setVisible(false)
	sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = true,cTime=20})

	-- performWithDelay(self,function() 
	-- 	self.view:setVisible(true)
	-- 	sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = false})
	-- end,10)
	
	-- performWithDelay(self,function() 
		self:showAni(self.data)
	-- end, 2)
	
	MJHelper:AutoTest(self.view:getChildByName("btn_continue_gold"),handler(self, self.onOneRoundEndCallBack))
end

-- 更新界面
function DZMJOneRoundEndLayer:updateViews()
	local playerinfos = clone(self.playerManager:getPlayerInfos())
	table.sort( playerinfos, function ( a, b )
    	if self.data.m_winnerMap[a:getChairId()] == self.data.m_winnerMap[b:getChairId()] then
    		-- local total_a = self.playerResults[a:getChairId()]:getGFScore()  + self.playerResults[a:getChairId()]:getHFScore()
    		-- local total_b = self.playerResults[b:getChairId()]:getGFScore()  + self.playerResults[b:getChairId()]:getHFScore()
    		-- return total_a < total_b
    		return a:getChairId() < b:getChairId()
    	else
    		return self.data.m_winnerMap[a:getChairId()] > self.data.m_winnerMap[b:getChairId()]
    	end

	end )

    for i = 1, 4 do
        local playerInfo = playerinfos[i]
        if playerInfo then
            local cardInfo = self.gameManager:getCardInfoByChairId(playerInfo:getChairId())
            self.items[i]:setDescribe("", cc.c4f(255, 240, 0, 255), cc.c4f(132, 0, 0, 255), 2)

            if self.data.m_winnerMap[playerInfo:getChairId()] == 1 then
                self.items[i]:setFlag(1)
                local str = ""
                local t = self.playerResults[playerInfo:getChairId()+1]:getHuTypeToOdd()
                for k, v in pairs(t) do
                	local huType = v.m_nType
                	local huTime = v.m_nOdd
                	if MJDef.HUTYPESTR[huType] then
                		str = str .. MJDef.HUTYPESTR[huType] .. " ×" .. huTime .. "  "
                	else
                		TOAST("胡牌类型未定义：" .. huType)
                	end
                end
                self.items[i]:setDescribe(str)
            elseif playerInfo:getChairId() == self.data.m_nFireChair then
                self.items[i]:setFlag(2)
                if cardInfo:isListenState() then
                	self.items[i]:setDescribe(MJHelper:STR(20))
                else
                	self.items[i]:setDescribe(MJHelper:STR(21))
                end
            else
                self.items[i]:setFlag(3)
                if cardInfo:isListenState() then
                	self.items[i]:setDescribe(MJHelper:STR(20))
                else
                	self.items[i]:setDescribe(MJHelper:STR(21))
                end
            end

            self.items[i]:setVisible(true)
            self.items[i]:setPlayerInfo(playerInfo)
            self.items[i]:setCardInfo(cardInfo)

            self.items[i]:setFanStr(self.playerResults[playerInfo:getChairId()+1]:getGFScore(), self.playerResults[playerInfo:getChairId()+1]:getHFScore())

        else
            self.items[i]:setVisible(false)
        end
    end

    -- 时间
    local str = MJHelper:STR( 5 ) .. "\n" .. os.date("%Y-%m-%d %X")
    self.label_time:setString(str)

    -- dump(self.data)
    if self.data.m_nBaoPai and self.data.m_nBaoPai > 0 then
    	local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
        local t = { card = DZMJCard.new(self.data.m_nBaoPai), state = MJDef.eCardState.e_front_big1 }
        self.baoCardItem:setData(t)
        self.layout_baopai:setVisible(true)
    else
        self.layout_baopai:setVisible(false)
    end

    local str = ""
    self.label_hupai:setString(str)

    -- 标题
    local isLiuJu = true
    for k, result in pairs(self.playerResults) do
    	if result:getHuType() ~= 0 then
    		isLiuJu = false
    	end
    end
    if isLiuJu then
        for i = 1, 4 do
             self.items[i]:setFlag(4)
        end
    end
	-- if isLiuJu then
	-- 	self.img_title_bg:loadTexture("mj_game_jiesuan_bg_2.png", 1)
	-- 	self.img_title_bg:setPosition(641, 650)
	-- 	self.img_title:loadTexture("mj_game_jiesuan_font_liuju.png", 1)
	-- else
	-- 	local myAid = Player:getAccountID()
	-- 	local playerInfo = self.playerManager:getPlayerInfoByAid(myAid)
	-- 	local chairId = playerInfo:getChairId()
	-- 	if self.data.m_winnerMap[chairId] == 1 then
	-- 	    self.img_title_bg:loadTexture("mj_game_jiesuan_bg_1.png", 1)
	-- 	    self.img_title_bg:setPosition(658, 650)
	-- 		self.img_title:loadTexture("mj_game_jiesuan_font_win.png", 1)
	-- 	else
	-- 		self.img_title_bg:loadTexture("mj_game_jiesuan_bg_2.png", 1)
	-- 		self.img_title_bg:setPosition(641, 650)
	-- 		self.img_title:loadTexture("mj_game_jiesuan_font_fail.png", 1)
	-- 	end
	-- end

	-- 金币房&牌友房
	if self.playerManager:isVipRoom() then
		self.layout_not_vip_room_btn:setVisible(false)
		self.layout_vip_room_btn:setVisible(true)
		self.btn_exit:setVisible(false)
		local btn_continue = self.layout_vip_room_btn:getChildByName("btn_continue_vip")
		btn_continue:setTitleText(MJHelper:STR(23))
	else
		self.layout_not_vip_room_btn:setVisible(true)
		self.layout_vip_room_btn:setVisible(false)
		self.btn_exit:setVisible(true)
	end

end

function DZMJOneRoundEndLayer:onOneRoundEndCallBack( sender )
	local name = sender:getName()
    print("DZMJOneRoundEndLayer ", name)
    if name == "btn_share" then
    	sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "wx_share_img")

    elseif name == "btn_continue_vip" then
        self:setVisible(false)
        self:stopAllActions()
        --sendMsg(PublicGameMsg.MSG_CCMJ_GAME_CONTINUE)
        sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = false})

        local gameManager = MJHelper:getGameManager(true)
		local isFinal = gameManager:isFinalGame()
		if isFinal then
			sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "show_total_result")
		else
			sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "ready")
		end
		sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "clear_all_cardinfo")

    elseif name == "btn_continue_gold" then
    	 self:setVisible(false)
		self:stopAllActions()
    	sendMsg(PublicGameMsg.MSG_CCMJ_GAME_CONTINUE)
    	sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "gold_continue")
		sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "clear_all_cardinfo") 
        local playerManager = MJHelper:getPlayerManager() 
	    if playerManager then
		    playerManager:clearRoomInfo()
	    end 
         ConnectManager:send2SceneServer( g_GameController.m_gameAtomTypeId, "CS_C2M_Mj_SysContinueGame_Req", {} ) 

    elseif name == "btn_exit" then
		local playerManager = MJHelper:getPlayerManager(true)
    	playerManager:setGameAtomTypeId(nil)
		playerManager:clearRoomInfo()
		   UIAdapter:popScene()
       ConnectManager:send2SceneServer( g_GameController.m_gameAtomTypeId, "CS_C2M_Mj_SysExitGame_Req", {} ) 
    end
end

function DZMJOneRoundEndLayer:updateSingleResult( msgStr, param )
	local name = param.name

	if name == "update_btn_continue" then
		local gameManager = MJHelper:getGameManager(true)
		local btn_continue = self.layout_vip_room_btn:getChildByName("btn_continue_vip")
		local isFinal = gameManager:isFinalGame()

		if isFinal then
			btn_continue:setTitleText(MJHelper:STR(24))
		else
			btn_continue:setTitleText(MJHelper:STR(23))
		end
	end
end

return DZMJOneRoundEndLayer
