--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 创建/加入牌友房相关测试

local TestBaseNty = require("app.game.Sparrow.GeneralMJ.src.test.TestBaseNty") 

local TestCreateAndEnter = class("TestCreateAndEnter", function (ctl)
	return TestBaseNty.new(ctl)
end)

function TestCreateAndEnter:ctor()
	-- self:addTest(handler(self, self.getCreateRoomAckTest))
	-- self:addTest(handler(self, self.getEnterRoomAckTest))
	self:addTest(handler(self, self.getEnterRoomNtyTest))
	self:addTest(handler(self, self.getPlayerReadyTest))
	self:addTest(handler(self, self.getRoomListTest))
	self:addTest(handler(self, self.getCurRoomInfo))

	
	
end

-- 创建房间返回
function TestCreateAndEnter:getCreateRoomAckTest()
	local idStr = "CS_M2C_CreateRoom_Ack"
	local data = {
		m_gameAtomTypeId = 120001,
		m_roomId = 222222,
		m_roomKey = 0,
		m_ret = 0,
	}

	return idStr, data
end

-- 进入房间返回
function TestCreateAndEnter:getEnterRoomAckTest()
	local idStr = "CS_M2C_EnterRoom_Ack"
	local data = {
		m_gameAtomTypeId = 120001,
		m_roomId = 222222,
		m_ret = 0,
	}

	return idStr, data
end

-- 玩家进入
function TestCreateAndEnter:getEnterRoomNtyTest()
	local idStr = "CS_M2C_EnterRoom_Nty"
	local data = {
		m_gameAtomTypeId = 0,
		m_roomId = 0,
		m_accountId = 0,
		m_playerInfo = {
			{
				m_accountId = Player:getAccountID(),
				m_chairId = 1,
				m_level = 0,
				m_faceId = 0,
				m_nickname = "游客游客游客",
				m_score = 0,
			},
			{
				m_accountId = 5000,
				m_chairId = 0,
				m_level = 0,
				m_faceId = 0,
				m_nickname = "游客游客游客",
				m_score = 0,
			},
			{
				m_accountId = 4000,
				m_chairId = 2,
				m_level = 0,
				m_faceId = 0,
				m_nickname = "游客游客游客",
				m_score = 0,
			},
			{
				m_accountId = 3000,
				m_chairId = 3,
				m_level = 0,
				m_faceId = 0,
				m_nickname = "游客游客游客",
				m_score = 0,
			},
			
		}
	}

	return idStr, data
end

-- 玩家准备
function TestCreateAndEnter:getPlayerReadyTest()
	local idStr = "CS_M2C_SetReady_Nty"
	local data = {
		m_accountId = 1,
		m_ready = 1,
	}

	return idStr, data
end

-- 房间列表
function TestCreateAndEnter:getRoomListTest()
	local idStr = "CS_M2C_FriendRoomIdList_Nty"
	local data = {
		m_arr_nRoomIdList = { 111111, 111112 },
	}

	return idStr, data
end

-- 初始房间信息
function TestCreateAndEnter:getCurRoomInfo()
	local idStr = "CS_M2C_FriendPlayerCurrentRoom_Nty"
	local data = {
		m_roomId = 111111,
	}

	return idStr, data
end

return TestCreateAndEnter
