--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 游戏操作提示通知测试数据

local TestBaseNty = require("app.game.Sparrow.GeneralMJ.src.test.TestBaseNty") 

local TestOptTipsNty = class("TestOptTipsNty", function (ctl)
	return TestBaseNty.new(ctl)
end)

function TestOptTipsNty:ctor()
	-- self:addTest(handler(self, self.getOperNty))
	-- self:addTest(handler(self, self.getChaTingNty))
	-- self:addTest(handler(self, self.getChuPaiResultNty))
	-- self:addTest(handler(self, self.getChaTingNty2))
	-- 吃&碰&加杠
	-- self:addTest(handler(self, self.getTest))
	-- self:addTest(handler(self, self.getChiResult))
	-- self:addTest(handler(self, self.getPengResult))
	-- self:addTest(handler(self, self.getJiaGangResult))

	-- -- -- 幺杠&补杠
	-- self:addTest(handler(self, self.getTest))
	-- self:addTest(handler(self, self.getYaoGangResult))
	-- self:addTest(handler(self, self.getBuYaoGangResult))

	-- -- -- 明杠
	-- self:addTest(handler(self, self.getTest))
	-- self:addTest(handler(self, self.getMingGangResult1))
	-- self:addTest(handler(self, self.getMingGangResult2))

	-- -- -- 抢杠吃听
	-- self:addTest(handler(self, self.getTest))
	-- self:addTest(handler(self, self.getQiangGangChiTingResult))
	-- self:addTest(handler(self, self.getQiangGangPengResult))

	-- 宝牌
	self:addTest(handler(self, self.getBaoPaiResult))
	-- 吃&碰&加杠
	-- self:addTest(handler(self, self.getTest))
	-- self:addTest(handler(self, self.getChiResult))
	-- self:addTest(handler(self, self.getPengResult))
	-- self:addTest(handler(self, self.getJiaGangResult))

	-- 九杠&补九杠
	-- self:addTest(handler(self, self.getTest))
	-- self:addTest(handler(self, self.getJiuGangResult))
	-- self:addTest(handler(self, self.getBuJiuGangResult))
	-- self:addTest(handler(self, self.getBuJiuGangResult))
	-- self:addTest(handler(self, self.getBuJiuGangResult1))
	-- self:addTest(handler(self, self.getBuJiuGangResult2))
	

	-- 风杠
	-- self:addTest(handler(self, self.getTest))
	-- self:addTest(handler(self, self.getFengGangResult))
	-- self:addTest(handler(self, self.getBuFengGangResult))
	-- self:addTest(handler(self, self.getBuFengGangResult2))
	-- self:addTest(handler(self, self.getBuFengGangResult))

	-- 分张
	-- self:addTest(handler(self, self.getFenZhang1))
	-- self:addTest(handler(self, self.getFenZhang2))
	-- self:addTest(handler(self, self.getFenZhang3))
	-- self:addTest(handler(self, self.getFenZhang4))
end

function TestOptTipsNty:getOperNty()
	local idStr = "CS_G2C_Mj_Oper_Nty"
	local data = {
		m_vecActions = { 
			{ m_iAcCode = MJDef.OPER.QG_CHI_TING, m_iAcCard = { 1, 1, 1, 1 } },
			{ m_iAcCode = MJDef.OPER.QG_CHI_TING, m_iAcCard = { 2, 2, 2, 2 } },
			{ m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 3 } },
			{ m_iAcCode = MJDef.OPER.MING_GANG, m_iAcCard = { 3, 3, 3, 5 } },
			{ m_iAcCode = MJDef.OPER.SI_FENG_GANG, m_iAcCard = { 3, 3, 3, 6 } },
			{ m_iAcCode = MJDef.OPER.AN_GANG, m_iAcCard = { 3, 3, 3, 7 } },
			{ m_iAcCode = MJDef.OPER.AN_GANG, m_iAcCard = { 3, 3, 3, 8 } },
			{ m_iAcCode = MJDef.OPER.AN_GANG, m_iAcCard = { 3, 3, 3, 9 } },
			{ m_iAcCode = MJDef.OPER.AN_GANG, m_iAcCard = { 3, 3, 3, 9 } },
			{ m_iAcCode = MJDef.OPER.AN_GANG, m_iAcCard = { 3, 3, 3, 9 } },
		},
	}

	return idStr, data
end

function TestOptTipsNty:getChaTingNty()
	local idStr = "CS_G2C_Mj_CheckListen_Nty"
	local data = {
		m_stCheckListen = { 
			{ m_nOutCard = 1, m_vnHuTime = {}, m_vnLeftCount = {1}, m_vnListenCard = {2} },
			{ m_nOutCard = 3, m_vnHuTime = {}, m_vnLeftCount = {1,1}, m_vnListenCard = {2, 3} },
			{ m_nOutCard = 5, m_vnHuTime = {}, m_vnLeftCount = {2}, m_vnListenCard = {2} },
			-- { m_nOutCard = 4, m_vnHuTime = {}, m_vnLeftCount = {1,1,1,1,1,1,1,1,1}, m_vnListenCard = {1, 2, 3, 4, 5, 7, 7, 7, 7} },
		},
	}

	return idStr, data
end

function TestOptTipsNty:getChuPaiResultNty()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
		m_nOperChair = 0,
		m_stAction = { m_iAcCode = MJDef.OPER.CHU_PAI, m_iAcCard = { 1 } },
	}
	return idStr, data
end

function TestOptTipsNty:getChaTingNty2()
	local idStr = "CS_G2C_Mj_CheckListen_Nty"
	local data = {
		m_stCheckListen = { 
			{ m_nOutCard = 0, m_vnHuTime = {2, 3}, m_vnLeftCount = {4, 4}, m_vnListenCard = {2, 3} },
		},
	}

	return idStr, data
end

function TestOptTipsNty:getTest()
	local idStr = "CS_G2C_Mj_SendHandCards_Nty"
	local data = {
		m_nBanker = 0,
		m_nDieCount = 0,
		m_stHCards = { 
			m_vecCards = { 3, 3, 3, 1, 1, 1, 2, 2, 2, 4, 5  }
		},
	}
	return idStr, data
end

-- 吃
function TestOptTipsNty:getChiResult()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
	m_nOperChair = 0,
	m_nByOperChair = 1,
	m_stAction = { 
			m_iAcCode = MJDef.OPER.CHI, 
			m_iAcCard = { 1, 3, 2 },
			m_ucByCid = 1,
		},
	}

	return idStr, data
end

-- 碰
function TestOptTipsNty:getPengResult()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
	m_nOperChair = 0,
	m_nByOperChair = 1,
	m_stAction = { 
			m_iAcCode = MJDef.OPER.PENG, 
			m_iAcCard = { 1, 1, 1 },
			m_ucByCid = 1,
		},
	}

	return idStr, data
end

-- 加杠
function TestOptTipsNty:getJiaGangResult()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
	m_nOperChair = 0,
	m_nByOperChair = 1,
	m_stAction = { 
			m_iAcCode = MJDef.OPER.JIA_GANG, 
			m_iAcCard = { 1, 1, 1, 1 },
		},
	}

	return idStr, data
end

-- 幺杠
function TestOptTipsNty:getYaoGangResult()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
	m_nOperChair = 0,
	m_stAction = { 
			m_iAcCode = MJDef.OPER.YAO_GANG, 
			m_iAcCard = { 2, 17, 2 },
		},
	}

	return idStr, data
end

-- 补幺杠
function TestOptTipsNty:getBuYaoGangResult()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
	m_nOperChair = 0,
	m_stAction = { 
			m_iAcCode = MJDef.OPER.BU_YAO_GANG, 
			m_iAcCard = { 1 },
		},
	}

	return idStr, data
end

-- 明杠自己
function TestOptTipsNty:getMingGangResult1()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
	m_nOperChair = 0,
	m_nByOperChair = 0,
	m_stAction = { 
			m_iAcCode = MJDef.OPER.MING_GANG, 
			m_iAcCard = { 2, 2, 2, 2 },
			m_ucByCid = 0,
		},
	}

	return idStr, data
end

-- 明杠其他人
function TestOptTipsNty:getMingGangResult2()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
	m_nOperChair = 0,
	m_nByOperChair = 1,
	m_stAction = { 
			m_iAcCode = MJDef.OPER.MING_GANG, 
			m_iAcCard = { 3, 3, 3, 3 },
			m_ucByCid = 1,
		},
	}

	return idStr, data
end

-- 抢杠吃听
function TestOptTipsNty:getQiangGangChiTingResult()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
	m_nOperChair = 0,
	m_nByOperChair = 1,
	m_stAction = { 
			m_iAcCode = MJDef.OPER.QG_CHI_TING, 
			m_iAcCard = { 1, 3, 2 },
			m_ucByCid = 1,
		},
	}

	return idStr, data
end

-- 抢杠碰
function TestOptTipsNty:getQiangGangPengResult()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
	m_nOperChair = 0,
	m_nByOperChair = 1,
	m_stAction = { 
			m_iAcCode = MJDef.OPER.QG_PENG, 
			m_iAcCard = { 1, 1, 1 },
			m_ucByCid = 1,
		},
	}

	return idStr, data
end


-- 宝牌
function TestOptTipsNty:getBaoPaiResult()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
	m_nOperChair = 0,
	m_nByOperChair = 0,
	m_stAction = { 
			m_iAcCode = MJDef.OPER.XUAN_BAO, 
			m_iAcCard = { 6, 1 },
		},
	}

	return idStr, data
end

-- 九杠
function TestOptTipsNty:getJiuGangResult()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
	m_nOperChair = 0,
	m_stAction = { 
			m_iAcCode = MJDef.OPER.JIU_GANG, 
			m_iAcCard = { MJDef.PAI.SUO_9, 17, 17 },
		},
	}

	return idStr, data
end

-- 补九杠
function TestOptTipsNty:getBuJiuGangResult()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
	m_nOperChair = 0,
	m_stAction = { 
			m_iAcCode = MJDef.OPER.BU_JIU_GANG, 
			m_iAcCard = { 17 },
		},
	}

	return idStr, data
end

function TestOptTipsNty:getBuJiuGangResult1()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
	m_nOperChair = 0,
	m_stAction = { 
			m_iAcCode = MJDef.OPER.BU_JIU_GANG, 
			m_iAcCard = { MJDef.PAI.WAN_9 },
		},
	}

	return idStr, data
end

function TestOptTipsNty:getBuJiuGangResult2()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
	m_nOperChair = 0,
	m_stAction = { 
			m_iAcCode = MJDef.OPER.BU_JIU_GANG, 
			m_iAcCard = { MJDef.PAI.TONG_9 },
		},
	}

	return idStr, data
end

-- 风杠
function TestOptTipsNty:getFengGangResult()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
	m_nOperChair = 0,
	m_stAction = { 
			m_iAcCode = MJDef.OPER.SAN_FENG_GANG, 
			-- m_iAcCard = { MJDef.PAI.DONG, 17, MJDef.PAI.XI },
			m_iAcCard = { MJDef.PAI.DONG, MJDef.PAI.NAN, MJDef.PAI.XI },
		},
	}

	return idStr, data
end

-- 补风杠
function TestOptTipsNty:getBuFengGangResult()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
	m_nOperChair = 0,
	m_stAction = { 
			m_iAcCode = MJDef.OPER.BU_FENG_GANG, 
			-- m_iAcCard = { MJDef.PAI.NAN },
			m_iAcCard = { 17 },
		},
	}

	return idStr, data
end

-- 补风杠2
function TestOptTipsNty:getBuFengGangResult2()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
	m_nOperChair = 0,
	m_stAction = { 
			m_iAcCode = MJDef.OPER.BU_FENG_GANG, 
			m_iAcCard = { MJDef.PAI.BEI },
		},
	}

	return idStr, data
end

-- 分张
function TestOptTipsNty:getFenZhang1()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
	m_nOperChair = 0,
	m_stAction = { 
			m_iAcCode = MJDef.OPER.FEN_ZHANG, 
			m_iAcCard = { MJDef.PAI.BEI },
		},
	}

	return idStr, data
end

-- 分张
function TestOptTipsNty:getFenZhang2()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
	m_nOperChair = 1,
	m_stAction = { 
			m_iAcCode = MJDef.OPER.FEN_ZHANG, 
			m_iAcCard = { MJDef.PAI.BEI },
		},
	}

	return idStr, data
end

-- 分张
function TestOptTipsNty:getFenZhang3()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
	m_nOperChair = 2,
	m_stAction = { 
			m_iAcCode = MJDef.OPER.FEN_ZHANG, 
			m_iAcCard = { MJDef.PAI.BEI },
		},
	}

	return idStr, data
end

-- 分张
function TestOptTipsNty:getFenZhang4()
	local idStr = "CS_G2C_Mj_OperRst_Nty"
	local data = {
	m_nOperChair = 3,
	m_stAction = { 
			m_iAcCode = MJDef.OPER.FEN_ZHANG, 
			m_iAcCard = { MJDef.PAI.BEI },
		},
	}

	return idStr, data
end



return TestOptTipsNty
