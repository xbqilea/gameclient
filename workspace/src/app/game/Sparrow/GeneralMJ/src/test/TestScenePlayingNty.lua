--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 游戏中场景消息测试数据

local TestBaseNty = require("app.game.Sparrow.GeneralMJ.src.test.TestBaseNty") 

local TestScenePlayingNty = class("TestScenePlayingNty", function (ctl)
	return TestBaseNty.new(ctl)
end)

function TestScenePlayingNty:ctor()
	self:addTest(handler(self, self.getTest))
	-- self:addTest(handler(self, self.getGameResultTDhuTest))
	-- self:addTest(handler(self, self.getGameResultTest))
	-- self:addTest(handler(self, self.getTotalGameEndTest))
end

function TestScenePlayingNty:getTest()
	local idStr = "CS_G2C_Mj_ScenePlaying_Nty"
	local data = {
		m_nBanker = 0,
		m_nCurrOper = 2,
		m_nBCard = MJDef.PAI.BAO,
		m_nLCardsCount = 50,
		m_vecHCardsCount = { 1, 13, 1, 13 },
		m_stMyHCards = { 
			-- m_vecCards = { 3, 3, 3, 1, 1, 1, 2, 2, 2, 4, 5 }
			m_vecCards = { 3, 3, 3, 3, 3, 3 }
		},
		m_vecAcCards = {
			{ 
				m_vecAction = { 
					{ m_iAcCode = MJDef.OPER.YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 2, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 3, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.AN_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.SI_FENG_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.SI_FENG_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.BU_FEI_DAN, m_iAcCard = { 17 } },

				},
			},
			{ 
				m_vecAction = { 
					-- { m_iAcCode = MJDef.OPER.YAO_GANG, m_iAcCard = { 17, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.YAO_GANG, m_iAcCard = { 17, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.SI_FENG_GANG, m_iAcCard = { 17, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 2, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 3, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.SI_FENG_GANG, m_iAcCard = { 1, 2, 3, 4 } },
				},
			},
			{ 
				m_vecAction = { 
					{ m_iAcCode = MJDef.OPER.YAO_GANG, m_iAcCard = { 17, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 2, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 3, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.AN_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.SI_FENG_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.SI_FENG_GANG, m_iAcCard = { 1, 2, 3, 4 } },
				},
			},
			{ 
				m_vecAction = { 
					-- { m_iAcCode = MJDef.OPER.AN_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },

					-- { m_iAcCode = MJDef.OPER.YAO_GANG, m_iAcCard = { 17, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.AN_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.SI_FENG_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.SI_FENG_GANG, m_iAcCard = { 1, 2, 3, 4 } },
				},
			},
		},
		m_vecOutCards = { 
			{ m_vecCards = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9} },
			{ m_vecCards = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9} },
			{ m_vecCards = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9} },
			{ m_vecCards = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9} },
		},
		m_vecSListen = { 0, 0, 0, 0 },
		m_vecTuoGuan = { 0, 0, 0, 0 },
	}
	return idStr, data
end

function TestScenePlayingNty:getGameResultTest()
	local idStr = "CS_G2C_Mj_GameOver_Nty"
	local data = {
		m_stBalance = {
			m_nWinnerChair = 0,
			m_nFireChair = 255,
			m_nHuType = MJDef.HUTYPE.SELF_PICK,
			m_vstHuTypeToOdd = { { m_nType = MJDef.HUTYPE.BANKER, m_nOdd = 2 }, { m_nType = MJDef.HUTYPE.STAND, m_nOdd = 2 } },
			m_nBaoPai = 1,
			m_nBiMen = 1,
			m_nHFScore = {0, 0, 0, 0},
			m_nGFScore = {-20, -5, 0, 0},
		},
		m_stMyHCards = { 
			-- m_vecCards = { 1, 1, 1, 2, 2, 2, 3, 3, 3, 5, 5, 5, 4 }
			{ m_vecCards = { 3, 3, 3, 5, 5, 5, 4, 4 }, },
			{ m_vecCards = { 3, 3, 3, 5, 5, 5, 4, 4 }, },
			{ m_vecCards = { 3, 3, 3, 5, 5, 5, 4, 4 }, },
			{ m_vecCards = { 3, 3, 3, 5, 5, 5, 4, 4 }, },
		},
		m_vecAcCards = {
			{ 
				m_vecAction = { 
					{ m_iAcCode = MJDef.OPER.AN_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 1 } },
					{ m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 2 } },
					{ m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 3 } },
				},
			},
			{ 
				m_vecAction = { 
					{ m_iAcCode = MJDef.OPER.YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.AN_GANG, m_iAcCard = { 1, 2, 3, 4 } },
				},
			},
			{ 
				m_vecAction = { 
					{ m_iAcCode = MJDef.OPER.AN_GANG, m_iAcCard = { 1, 2, 3, 4 } },
				},
			},
			{ 
				m_vecAction = { 
					{ m_iAcCode = MJDef.OPER.AN_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
				},
			},
		},
		m_vecOutCards = { 
			{ m_vecCards = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9} },
			{ m_vecCards = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9} },
			{ m_vecCards = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9} },
			{ m_vecCards = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9} },
		},
	}
	return idStr, data
end

function TestScenePlayingNty:getGameResultTDhuTest()
	local idStr = "CS_G2C_Mj_GameOver_Nty"
	local data = {
		m_stBalance = {
			m_nWinnerChairFlag = 3,
			m_nFireChair = 2,
			m_nBaoPai = 1,
			-- m_nHuType = MJDef.HUTYPE.SELF_PICK,
			m_stPlayerBalance = {
				{ 
					m_nHuType = MJDef.HUTYPE.QXD_HH,
					m_vstHuTypeToOdd = { { m_nType = MJDef.HUTYPE.BANKER, m_nOdd = 2 }, { m_nType = MJDef.HUTYPE.STAND, m_nOdd = 2 }, { m_nType = MJDef.HUTYPE.QXD_HH, m_nOdd = 32 }, { m_nType = MJDef.HUTYPE.QXD_SHH, m_nOdd = 16 }, { m_nType = MJDef.HUTYPE.QXD_ZZ, m_nOdd = 8 }, { m_nType = MJDef.HUTYPE.CLIP, m_nOdd = 8 }, { m_nType = MJDef.HUTYPE.STAND, m_nOdd = 2 } },
					m_nBiMen = 1,
					m_nHFScore = 12600000,
					m_nGFScore = -5,
				},
				{ 
					m_nHuType = MJDef.HUTYPE.QXD_HH,
					m_vstHuTypeToOdd = { { m_nType = MJDef.HUTYPE.BANKER, m_nOdd = 2 }, { m_nType = MJDef.HUTYPE.STAND, m_nOdd = 2 } },
					m_nBiMen = 1,
					m_nHFScore = 12600000,
					m_nGFScore = -5,
				},
				{ 
					m_nHuType = 0,
					m_vstHuTypeToOdd = { { m_nType = MJDef.HUTYPE.BANKER, m_nOdd = 2 }, { m_nType = MJDef.HUTYPE.STAND, m_nOdd = 2 } },
					m_nBiMen = 1,
					m_nHFScore = 12600000,
					m_nGFScore = -5,
				},
				{ 
					m_nHuType = 0,
					m_vstHuTypeToOdd = { { m_nType = MJDef.HUTYPE.BANKER, m_nOdd = 2 }, { m_nType = MJDef.HUTYPE.STAND, m_nOdd = 2 } },
					m_nBiMen = 1,
					m_nHFScore = 20,
					m_nGFScore = -5,
				},
			}
			
		},
		m_stMyHCards = { 
			-- m_vecCards = { 1, 1, 1, 2, 2, 2, 3, 3, 3, 5, 5, 5, 4 }
			{ m_vecCards = { 3, 3 }, },
			{ m_vecCards = { 3, 3, 3, 5, 5, 5, 4, 4 }, },
			{ m_vecCards = { 3 }, },
			{ m_vecCards = { 3, 3, 3, 5, 5, 5, 4, 4 }, },
		},
		m_vecAcCards = {
			{ 
				m_vecAction = { 
					-- { m_iAcCode = MJDef.OPER.AN_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 1 } },
					-- { m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 2 } },
					-- { m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 3 } },
					{ m_iAcCode = MJDef.OPER.YAO_GANG, m_iAcCard = { 17, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 2, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 3, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.AN_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.SI_FENG_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.SI_FENG_GANG, m_iAcCard = { 1, 2, 3, 4 } },
				},
			},
			{ 
				m_vecAction = { 
					{ m_iAcCode = MJDef.OPER.YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.AN_GANG, m_iAcCard = { 1, 2, 3, 4 } },
				},
			},
			{ 
				m_vecAction = { 
					{ m_iAcCode = MJDef.OPER.AN_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.YAO_GANG, m_iAcCard = { 17, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 2, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.BU_YAO_GANG, m_iAcCard = { 3, 2, 3, 4 } },
				},
			},
			{ 
				m_vecAction = { 
					{ m_iAcCode = MJDef.OPER.AN_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					{ m_iAcCode = MJDef.OPER.YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
					-- { m_iAcCode = MJDef.OPER.YAO_GANG, m_iAcCard = { 1, 2, 3, 4 } },
				},
			},
		},
		m_vecOutCards = { 
			{ m_vecCards = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9} },
			{ m_vecCards = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9} },
			{ m_vecCards = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9} },
			{ m_vecCards = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9} },
		},
	}
	return idStr, data
end


function TestScenePlayingNty:getTotalGameEndTest()
	local idStr = "CS_G2C_Mj_LastBalance_Nty"
	local data = {
		m_nGameRound = 4,
		m_nCurRound = 2,
		m_vstLastBalance = {
			{
				m_ucLianZhCiShi = 1,
				m_ucHuPaiCiShi = 1,
				m_ucDianPaoCiShi = 0,
				m_ucLaoPaoCiShi = 100,
				m_ucDuiBaoCiShi = 1000,
				m_nWinScore = 20,
				m_nZhScore = 0,
			},
			{
				m_ucLianZhCiShi = 1,
				m_ucHuPaiCiShi = 1,
				m_ucDianPaoCiShi = 0,
				m_ucLaoPaoCiShi = 100,
				m_ucDuiBaoCiShi = 1000,
				m_nWinScore = 20,
				m_nZhScore = 0,
			},
			{
				m_ucLianZhCiShi = 1,
				m_ucHuPaiCiShi = 1,
				m_ucDianPaoCiShi = 0,
				m_ucLaoPaoCiShi = 100,
				m_ucDuiBaoCiShi = 1000,
				m_nWinScore = 20,
				m_nZhScore = -20000,
			},
			-- {
			-- 	m_ucLianZhCiShi = 1,
			-- 	m_ucHuPaiCiShi = 1,
			-- 	m_ucDianPaoCiShi = 10,
			-- 	m_ucLaoPaoCiShi = 100,
			-- 	m_ucDuiBaoCiShi = 1000,
			-- 	m_nWinScore = 20,
			-- 	m_nZhScore = -20000,
			-- },
		}
	}
	return idStr, data
end

return TestScenePlayingNty