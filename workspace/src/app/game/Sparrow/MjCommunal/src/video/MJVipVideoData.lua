--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将录像回放

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

require("app.game.Sparrow.MjCommunal.src.MJDef")
local oper = {}
local mt = {}
setmetatable(oper,mt)
mt.__index = function(tbl,key)
	local v = MJDef.OPER[key]
	if not v then print("Error oper key:",key) end
		
	v = v or 0
	return v
end

local analyseData = {}
local function handle_user_id(playList,cardList,str)
	str = string.sub(str, 3, #str)
	local chairId = tonumber("0x"..string.sub(str,1,2))
	local m_accountId = tonumber(string.sub(str,3,#str))
	playList.m_accountId[chairId] = m_accountId
end
analyseData[oper.USER_ID] = handle_user_id

local function handle_face_id(playList,cardList,str)
	str = string.sub(str, 3, #str)
	local chairId = tonumber("0x"..string.sub(str,1,2))
	local m_faceId = tonumber(string.sub(str,3,#str))
	playList.m_faceId[chairId] = m_faceId
end
analyseData[oper.FACE_ID] = handle_face_id

local function handle_user_name(playList,cardList,str)
	str = string.sub(str, 3, #str)
	local chairId = tonumber("0x"..string.sub(str,1,2))
	local m_nickname = string.sub(str,3,#str)
	playList.m_nickname[chairId] = m_nickname
end
analyseData[oper.USER_NAME] = handle_user_name

local function handle_room_creater(playList,cardList,str)
	str = string.sub(str, 3, #str)
	local m_roomCreatorId = tonumber(string.sub(str,3,#str))
	playList.m_roomCreatorId = m_roomCreatorId
end
analyseData[oper.MASTER_ID] = handle_room_creater

local function handle_banker(playList,cardList,str)
	local m_nBanker = tonumber(string.sub(str,-2,-1))
	cardList.m_nBanker = m_nBanker
end
analyseData[oper.BANKER] = handle_banker

local function handle_card_dice(playList,cardList,str)
	local m_nDieCount = tonumber("0x"..string.sub(str,-2,-1))
	cardList.m_nDieCount = m_nDieCount
end
analyseData[oper.START_DICE] = handle_card_dice

local function handle_hand_card(playList,cardList,str)
	str = string.sub(str, 3, #str)
	local chairId = tonumber("0x"..string.sub(str,1,2))
	local length = #str/2
	local list = {}
	for i=2,length do
		local card = tonumber("0x"..string.sub(str,i*2-1,i*2))
		table.insert(list,card)	
	end
	cardList.handcard[chairId] = list
end
analyseData[oper.SHOU_PAI] = handle_hand_card

local function handle_leftHandCard_card(playList,cardList,str)
	str = string.sub(str, 3, #str)
    str = string.sub(str, 3, #str)
    local m_nLCardsCount = tonumber("0x"..string.sub(str,1,2))
    cardList.m_nLCardsCount = m_nLCardsCount
end
analyseData[oper.MJ_LEFT_CARD_CNT] = handle_leftHandCard_card

local function handle_huFen_card(playList,cardList,str)
	str = string.sub(str, 3, #str)
	local chairId = tonumber("0x"..string.sub(str,1,2))
    str = string.sub(str, 3, #str)
    local fen = tonumber(str)
    playList.balance.m_stPlayerBalance[chairId+1].m_nHFScore = fen
end
analyseData[oper.MJ_HU_FEN] = handle_huFen_card

local function handle_gangFen_card(playList,cardList,str)
	str = string.sub(str, 3, #str)
	local chairId = tonumber("0x"..string.sub(str,1,2))
    str = string.sub(str, 3, #str)
    local fen = tonumber(str)
    playList.balance.m_stPlayerBalance[chairId+1].m_nGFScore = fen
end
analyseData[oper.MJ_GANG_FEN] = handle_gangFen_card

local function handle_allFen_card(playList,cardList,str)
	str = string.sub(str, 3, #str)
	local chairId = tonumber("0x"..string.sub(str,1,2))
    str = string.sub(str, 3, #str)
    local fen = tonumber(str)
    playList.m_nScore[chairId] = fen
end
analyseData[oper.MJ_ZONG_JI_FEN] = handle_allFen_card

local function handle_vipRound_card(playList,cardList,str)
	str = string.sub(str, 3, #str)
	str = string.sub(str, 3, #str)
	local curRound = tonumber("0x"..string.sub(str,1,2))
    playList.roundInfo.curRound = curRound
	
	str = string.sub(str, 3, #str)
	local curTurn = tonumber("0x"..string.sub(str,1,2))
    playList.roundInfo.curTurn = curTurn
	
	str = string.sub(str, 3, #str)
	local maxRound = tonumber("0x"..string.sub(str,1,2))
    playList.roundInfo.maxRound = maxRound
end
analyseData[oper.MJ_VIP_ROUND] = handle_vipRound_card

local function handle_oper_no_by(playList,cardList,str)
	local m_iAcCode = tonumber("0x"..string.sub(str,1,2))
	str = string.sub(str, 3, #str)
	local chairId = tonumber("0x"..string.sub(str,1,2))
	local length = #str/2
	local list = {}
	for i=2,length do
		local card = tonumber("0x"..string.sub(str,i*2-1,i*2))
		table.insert(list,card)	
	end
	local one_oper = {
		m_nOperChair = chairId,
		m_nByOperChair = chairId,
		m_stAction = {
			m_iAcCode = m_iAcCode,
			m_iAcCard = list,
			m_ucByCid = chairId,
		}
	}
	table.insert(cardList.operList,one_oper)
end

local function handle_oper_no_by_copy_four(playList,cardList,str)
	local m_iAcCode = tonumber("0x"..string.sub(str,1,2))
	str = string.sub(str, 3, #str)
	local chairId = tonumber("0x"..string.sub(str,1,2))
	local length = #str/2
	local list = {}
	for i=2,length do
		local card = tonumber("0x"..string.sub(str,i*2-1,i*2))
		for i=1,4 do
			table.insert(list,card)	
		end
	end
	local one_oper = {
		m_nOperChair = chairId,
		m_nByOperChair = chairId,
		m_stAction = {
			m_iAcCode = m_iAcCode,
			m_iAcCard = list,
			m_ucByCid = chairId,
		}
	}
	table.insert(cardList.operList,one_oper)
end

local function handle_oper_have_by(playList,cardList,str)
	local m_iAcCode = tonumber("0x"..string.sub(str,1,2))
	str = string.sub(str, 3, #str)
	local chairId = tonumber("0x"..string.sub(str,1,2))
	str = string.sub(str, 3, #str)
	local byChairId = tonumber("0x"..string.sub(str,1,2))
	local length = #str/2
	local list = {}
	for i=2,length do
		local card = tonumber("0x"..string.sub(str,i*2-1,i*2))
		table.insert(list,card)	
	end
	local one_oper = {
		m_nOperChair = chairId,
		m_nByOperChair = byChairId,
		m_stAction = {
			m_iAcCode = m_iAcCode,
			m_iAcCard = list,
			m_ucByCid = byChairId,
		}
	}
	table.insert(cardList.operList,one_oper)
end

local function handle_oper_have_by_copy_four(playList,cardList,str)
	local m_iAcCode = tonumber("0x"..string.sub(str,1,2))
	str = string.sub(str, 3, #str)
	local chairId = tonumber("0x"..string.sub(str,1,2))
	str = string.sub(str, 3, #str)
	local byChairId = tonumber("0x"..string.sub(str,1,2))
	local length = #str/2
	local list = {}
	for i=2,length do
		local card = tonumber("0x"..string.sub(str,i*2-1,i*2))
		for i=1,4 do
			table.insert(list,card)	
		end
	end
	local one_oper = {
		m_nOperChair = chairId,
		m_nByOperChair = byChairId,
		m_stAction = {
			m_iAcCode = m_iAcCode,
			m_iAcCard = list,
			m_ucByCid = byChairId,
		}
	}
	table.insert(cardList.operList,one_oper)
end

analyseData[oper.MO_PAI] = handle_oper_no_by
analyseData[oper.CHU_PAI] = handle_oper_no_by

analyseData[oper.XUAN_BAO] = handle_oper_no_by
analyseData[oper.KAN_BAO] = handle_oper_no_by
analyseData[oper.HUAN_BAO] = handle_oper_no_by

analyseData[oper.CHI] = handle_oper_have_by
analyseData[oper.PENG] = handle_oper_have_by
analyseData[oper.AN_GANG] = handle_oper_have_by
analyseData[oper.MING_GANG] = handle_oper_have_by
analyseData[oper.JIA_GANG] = handle_oper_have_by

analyseData[oper.YAO_GANG] = handle_oper_no_by
analyseData[oper.JIU_GANG] = handle_oper_no_by
analyseData[oper.SAN_FENG_GANG] = handle_oper_no_by
analyseData[oper.SI_FENG_GANG] = handle_oper_no_by
analyseData[oper.XI_GANG] = handle_oper_no_by
analyseData[oper.FEI_DAN] = handle_oper_no_by


analyseData[oper.BU_YAO_GANG] = handle_oper_no_by
analyseData[oper.BU_JIU_GANG] = handle_oper_no_by
analyseData[oper.BU_FENG_GANG] = handle_oper_no_by
analyseData[oper.BU_XI_GANG] = handle_oper_no_by
analyseData[oper.BU_FEI_DAN] = handle_oper_no_by

analyseData[oper.TING_PAI] = handle_oper_no_by
analyseData[oper.CHEN_PAI] = handle_oper_no_by


analyseData[oper.CHI_TING] = handle_oper_have_by
analyseData[oper.PENG_TING] = handle_oper_have_by


analyseData[oper.QG_CHI_TING] = handle_oper_have_by
analyseData[oper.QG_PENG_TING] = handle_oper_have_by
analyseData[oper.QG_PENG] = handle_oper_have_by
analyseData[oper.QG_GANG] = handle_oper_have_by
analyseData[oper.QG_HU] = handle_oper_have_by

analyseData[oper.HU_PAI] = handle_oper_have_by
analyseData[oper.DUI_BAO] = handle_oper_have_by
analyseData[oper.LOU_BAO] = handle_oper_have_by

local analyseEndData = {}
local function handle_end_hand_card(playList,endCardList,str)
	str = string.sub(str, 3, #str)
	local chairId = tonumber("0x"..string.sub(str,1,2))
	local length = #str/2
	local list = {}
	for i=2,length do
		local card = tonumber("0x"..string.sub(str,i*2-1,i*2))
		table.insert(list,card)	
	end
	endCardList.handcard[chairId] = list
end
analyseEndData[oper.SHOU_PAI] = handle_end_hand_card

local function handle_end_weave_card(playList,endCardList,str)
	local m_iAcCode = tonumber("0x"..string.sub(str,1,2))
	str = string.sub(str, 3, #str)
	local chairId = tonumber("0x"..string.sub(str,1,2))
	local length = #str/2
	local list = {}
	for i=2,length do
		local card = tonumber("0x"..string.sub(str,i*2-1,i*2))
		table.insert(list,card)	
	end
	local one_oper = {
			m_iAcCode = m_iAcCode,
			m_iAcCard = list,
	}
	endCardList.weavecard[chairId] = endCardList.weavecard[chairId] or {}
	table.insert(endCardList.weavecard[chairId],one_oper)
end
analyseEndData[oper.CHI] = handle_end_weave_card


local function handle_end_biMen_card(playList,endCardList,str)
	str = string.sub(str, 3, #str)
    str = string.sub(str, 3, #str)
	
	local total = #str/2
	for i=1,total do
		local s = string.sub(str, (i-1)*2+1, i*2)
		local fen = tonumber("0x"..s)
		playList.balance.m_stPlayerBalance[i].m_nBiMen = fen
	end
end
analyseEndData[oper.MJ_END_BI_MEN] = handle_end_biMen_card

local function matchBit(match_b,huType)
	local bb = bit.bnot(match_b)
	local l = bit.bor(bb,huType)
	if l == 0xffffffff then
		return true
	end
end

local function handle_end_huType_card(playList,endCardList,str)
	str = string.sub(str, 3, #str)
	local chairId = tonumber("0x"..string.sub(str,1,2))
    str = string.sub(str, 3, #str)
	local m_type = tonumber("0x"..str)
	
	playList.balance.m_stPlayerBalance[chairId+1].m_nHuType = m_type
	
	for k,v in pairs(MJDef.HUTYPEODD) do
		if matchBit(k,m_type) then
			local obj = 
			{	
				m_nType = k,
				m_nOdd = v,
			}
			table.insert(playList.balance.m_stPlayerBalance[chairId+1].m_vstHuTypeToOdd,obj)
		end
	end
	
end
analyseEndData[oper.MJ_END_HU_TYPE] = handle_end_huType_card

local MJVipVideoData = class("MJVipVideoData")


function MJVipVideoData:ctor()
	self:myInit()
end

function MJVipVideoData:myInit()
    self.videoData = {}
    self.videoData.playList  = {m_accountId= {},m_nickname= {},m_faceId = {},m_roomCreatorId=nil,}
	self.videoData.cardList = {m_nDieCount=0,m_nBanker=0,handcard = {},operList={}}
	self.videoData.endCardList = {weavecard= {},handcard = {}}
end

function MJVipVideoData:analyzeData(data)
    self:myInit()
    local playList =  self.videoData.playList
    local cardList =  self.videoData.cardList
    local endCardList =  self.videoData.endCardList
	
	playList.roundInfo = {}
	local v = playList.roundInfo
	v.curRound = 0
	v.curTurn = 0
	v.maxRound = 0
	
    playList.m_nScore = {0,0,0,0}
    playList.balance = {}
	local v = playList.balance
	v.m_nWinnerChairFlag = 0
	v.m_nFireChair = -1
	
	v.m_nBaoPai = 0
	v.m_stPlayerBalance = {}

	
	for i=1,4 do
		local info = {}
		info.m_vstHuTypeToOdd = {}
		info.m_nHuType = 0
		info.m_nBiMen = 0
		info.m_nHFScore = 0
		info.m_nGFScore = 0
		table.insert(v.m_stPlayerBalance,info)
	end

    --data = "5000110831;5200;5001110912;5201;60ff15;610007070912131317232323252727;61010312131618192122242424293234;660129;650014;660017;700100181719;660116;650001;660001;650103;660134;650006;660013;650126;660132;650015;660006;650134;660134;650009;660015;650121;660126;950001252627;660027;650128;660128;6a000408;650009;b0000009;ff0001;9500252627;61000707090909121314232323;7001181719;610103031213212122242424;"
    data = data or "5000110831;5200;5001110840;5201;5002110841;5202;5003110842;5203;60ff46;610012141721212525252627282828;61010103040607091719212129353637;610202041112131318262729343536;610303030708141617222229313137;7b01091929;7e01353637;660117;650204;7e02353611;660234;650319;660337;650007;660026;650102;9001;660104;710201040404;660202;650311;7b03111929;660314;650033;660033;6a010606;650132;660132;650235;830235;650227;660218;700302161817;660307;650003;660017;650134;660134;650215;660215;650305;660305;b0010305;ff0101;610003071214212125252527282828;7b01091929;7e01353637;610101020306072121;7e02353611;7102040404;830235;610212131326272729;7b03111929;7003161817;610303030822223131;"
    local list = string.split(data, ";")
	local is_game_flag = true
	for k,v in pairs(list) do
		local t_oper = tonumber("0x"..string.sub(v,1,2))
		if t_oper ==0xff then
			is_game_flag = false
		end
		if is_game_flag then
			if analyseData[t_oper] then
				analyseData[t_oper](playList,cardList,v)
				local one_oper = cardList.operList[#cardList.operList]
				local card = nil
				if t_oper==oper.XUAN_BAO then
					
					card = one_oper.m_stAction.m_iAcCard[2]

				elseif	t_oper==oper.KAN_BAO then
					card = one_oper.m_stAction.m_iAcCard[1]
				elseif t_oper==oper.HUAN_BAO then
					card = one_oper.m_stAction.m_iAcCard[2]
				end
				if card then
					local v = playList.balance
					v.m_nBaoPai = card
				end
			end
			if t_oper==oper.HU_PAI then
				local chairId = tonumber("0x"..string.sub(v,3,4))
				local by_chairId = tonumber("0x"..string.sub(v,5,6))
				local v = playList.balance
				v.m_nWinnerChairFlag = v.m_nWinnerChairFlag+ bit.blshift(1,chairId)
				v.m_nFireChair = by_chairId

			elseif t_oper == oper.DUI_BAO	 then
				local chairId = tonumber("0x"..string.sub(v,3,4))
				local by_chairId = tonumber("0x"..string.sub(v,5,6))
				local v = playList.balance
				v.m_nWinnerChairFlag = v.m_nWinnerChairFlag + bit.blshift(1,chairId)
				v.m_nFireChair = by_chairId

			elseif t_oper == oper.LOU_BAO	 then
				local chairId = tonumber("0x"..string.sub(v,3,4))
				local by_chairId = tonumber("0x"..string.sub(v,5,6))
				local v = playList.balance
				v.m_nWinnerChairFlag = v.m_nWinnerChairFlag + bit.blshift(1,chairId)
				v.m_nFireChair = by_chairId
			end
		else 
			if analyseEndData[t_oper] then
				analyseEndData[t_oper](playList,endCardList,v)
			else
				if t_oper and t_oper>= oper.CHI and t_oper<oper.HU_PAI then
					analyseEndData[oper.CHI](playList,endCardList,v)
				end
			end
		end
	end

    self.videoData.my_chair = 0
    for c,v in pairs(self.videoData.playList.m_accountId) do
        if v==Player:getAccountID() then
            self.videoData.my_chair = c
        end
    end
    --dump(self.videoData,"videoData",7)
end

function MJVipVideoData:isLegal()
    local accountId = Player:getAccountID()
    local playList = self.videoData.playList
    for i,v in pairs(playList.m_accountId) do
        if v==accountId then
            return true
        end
    end
    return false
end

function MJVipVideoData:getOperNum()
    local num = table.nums(self.videoData.cardList.operList)
    return num
end

function MJVipVideoData:getOper(index)
    local list = self.videoData.cardList.operList
    return list[index]
end

function MJVipVideoData:createHandCard()
    local data = {m_nBanker=0,m_nDieCount=0,m_stHCards=nil}
    local cardList = self.videoData.cardList
    data.m_nBanker = cardList.m_nBanker
    data.m_nDieCount = cardList.m_nDieCount
    local c = self.videoData.my_chair

	data.m_nLCardsCount = cardList.m_nLCardsCount
	
    local list = {m_vecCards={}}
    local handcard = cardList.handcard[c] or {}
    for k,v in pairs(handcard) do
        table.insert(list.m_vecCards,v)
    end
    data.m_stHCards = list
    return data
end

function MJVipVideoData:getHandCard(chairId)
	local cardList = self.videoData.cardList
	local data = {}
    local handcard = cardList.handcard[chairId] or {}
    for k,v in pairs(handcard) do
        table.insert(data,v)
    end
    return data
end

function MJVipVideoData:getMyScore()
	local m_chairId = self.videoData.my_chair
	print(self.videoData.playList.balance,m_chairId)
    local balance = self.videoData.playList.balance
	local hf = balance.m_stPlayerBalance[chairId+1].m_nHFScore or 0
	local gf = balance.m_stPlayerBalance[chairId+1].m_nGFScore or 0
	local m_score =  hf + gf
	return m_score
end

function MJVipVideoData:getRoundInfo()
    local roundInfo = self.videoData.playList.roundInfo

	return roundInfo
end

function MJVipVideoData:createPlayerInfo()
    local data = {m_playerInfo = {}}
    for i=0,3 do
        local m_accountId = self.videoData.playList.m_accountId[i]
        if m_accountId then
            local m_accountId = self.videoData.playList.m_accountId[i]
            local m_nickname = self.videoData.playList.m_nickname[i]
			local m_faceId = self.videoData.playList.m_faceId[i] or 1
            local m_nRoomctorId = self.videoData.playList.m_roomCreatorId
            local m_chairId = i
            local score = self.videoData.playList.m_nScore[i] or 0
            local player = {
                m_accountId = m_accountId,
                m_chairId = m_chairId,
                m_level = 1,
                m_faceId = m_faceId,
                m_nickname = m_nickname,
                m_score = score,
                m_ready = 1,
                m_nRoomctorId = m_nRoomctorId
            }
            table.insert(data.m_playerInfo,player)
        end
    end
    return data
end

function MJVipVideoData:createRoomInitInfo()
    local data = {}
	data.m_nPlayerCount = 4
	data.m_nPlayerCount = table.nums(self.videoData.playList.m_accountId)
	data.m_nMaxRound = 8
		
    return data
end

function MJVipVideoData:createOverInfo()
	local data = {m_stBalance=nil,m_stMyHCards={},m_vecAcCards={},m_vecOutCards={}}
	local endCardList =  self.videoData.endCardList
	data.m_stBalance = self.videoData.playList.balance
	for k,v in pairs(endCardList.handcard) do
		local card = {m_vecCards=v}
		data.m_stMyHCards[k+1]=card
	end
	
	for k,v in pairs(endCardList.weavecard) do
		local card = {m_vecAction=v}
		data.m_vecAcCards[k+1]=card
	end
	return data
end
return MJVipVideoData