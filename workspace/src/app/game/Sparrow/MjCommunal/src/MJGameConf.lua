module(..., package.seeall)

LoadingLayer = "" -- loading 界面
GameRoomLayer = "app.game.Sparrow.MjCommunal.src.MJRoom.MJVipRoomLayer" -- 选择游戏界面

CCMJ_GAME_NODE = 410000	--长春麻将节点
TDH_GAME_NODE = 411000 	-- 大众麻将节点

CCMJ_VIP_ROOM_NODE = 410100 		-- vip房节点ID
-- 节点ID
CCMJ_FREE_ROOM_NODE = 410200 		-- 自由房节点ID
TDH_FREE_ROOM_NODE = 411200 		-- 推倒胡自有房即节点

-- 最小游戏类型ID
CCMJ_VIPROOM_GAME_ID = 120001
TDH_FREEROOM_GAME_ID = 120051

CCMJ_HALLCENTER_GAME_ID = 120007

-- 游戏类型ID
CCMJ_GAME_KIND_TYPE = 120 				-- 长春麻将
TDH_GAME_KIND_TYPE = 125 				-- 大众麻将

GENERALMJ_RANK_KIDE_ID = 14             -- 大众麻将排行类型

-- 游戏场景
ScenePath = "app.game.Sparrow.GeneralMJ.src.view.DZMJMainScene" -- 大众麻将
ScenePaths = {
	[CCMJ_GAME_KIND_TYPE] = "app.game.Sparrow.GeneralMJ.src.view.CCMJMainScene", -- 长春麻将
	[TDH_GAME_KIND_TYPE] = "app.game.Sparrow.GeneralMJ.src.view.DZMJMainScene", --  大众麻将
}


-- 节点路径
NodePath = {
	[CCMJ_FREE_ROOM_NODE] = "app.game.Sparrow.MjCommunal.src.MJRoom.MJFreeRoomLayer",
	[TDH_FREE_ROOM_NODE] = "app.game.Sparrow.MjCommunal.src.MJRoom.MJFreeRoomLayer",
}

NodeFreeRoomId = {
	[CCMJ_GAME_NODE] 	= CCMJ_FREE_ROOM_NODE,
	[TDH_GAME_NODE] 	= TDH_FREE_ROOM_NODE,
}

ShareConf = {
	[0] = {
		WXInvite 	= 28,
		WXShare		= 30, 
	},
	[1]= {
		WXInvite 	= 33,
		WXShare		= 34, 
	},
}