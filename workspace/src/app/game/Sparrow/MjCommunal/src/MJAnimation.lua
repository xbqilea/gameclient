-- Author: 
-- Date: 2018-08-07 18:17:10
-- 麻将动画类

local MJAnimation = class("MJAnimation")

local armature_path = "animation/"
local suffix = ".ExportJson"

local __nameList = {}

--加载骨骼动画数据
function MJAnimation:loadArmature(name)
	if __nameList[name] then return end
	local a_path = armature_path..name.."/"..name..suffix
	print("load armature:",name..suffix)
	ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(a_path)
	__nameList[name]= true
end

--清除骨骼数据
function  MJAnimation:clearArmature(name)
	local a_path = armature_path..name.."/"..name..suffix
	print("clear armature:",name..suffix)
	ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo(a_path)
	__nameList[name]= nil
end

function MJAnimation:unLoadAll()
	local temp = clone(__nameList)
	for name,v in pairs(temp) do
		self:clearArmature(name)
	end
	__nameList = {}
end

--动画播放回调
function MJAnimation:animationEvent( armature, movementType, movementID )
    --非循环播放一次
    if movementType == ccs.MovementEventType.complete or movementType == ccs.MovementEventType.loopComplete then
		
		if not armature.isLoop then
			armature:removeFromParent()
		end 
		if armature.callback then
			armature.callback(armature,"complete")
		end
    end
 end

--动画播放回调
function MJAnimation:frameEvent( bone, frameEventName, originFrameIndex,currentFrameIndex )
    local armature = bone:getArmature()
	if armature then
		if armature.callback then
			armature.callback(armature,frameEventName)
		end
	end
 end

--替换图片
function MJAnimation:changeDisplay(armature,boneName,index,after_img)
	local bone = armature:getBone(boneName)
	if bone then
		bone:addDisplay(ccs.Skin:createWithSpriteFrameName(after_img),index)
	end
end

--替换图片
function MJAnimation:replaceTexture(armature,before_img,after_img)
	local boneDic =armature:getBoneDic()
	if before_img and after_img then
		local skin = true --ccs.Skin:createWithSpriteFrameName(after_img)
		if skin then
			for k,bone in pairs(boneDic) do
				print(k)
			end
		end
	end
end


--播放动画
function MJAnimation:playArmature(name,animationName,callback,isLoop)
    print("play armature:",name)
	self:loadArmature(name)
	local armature = ccs.Armature:create(name)
	if armature then 
        local animationData = armature:getAnimation():getAnimationData()
        local data = animationData:getMovement(animationName)
        if data then
		    armature:getAnimation():play(animationName,-1,1)
        else
            armature:getAnimation():playWithIndex(0,-1,1)
        end
		armature:getAnimation():setMovementEventCallFunc(handler(self,self.animationEvent))
		armature:getAnimation():setFrameEventCallFunc(handler(self,self.frameEvent))
		armature.callback = callback
        armature.isLoop = isLoop
		return armature
	end
end

return MJAnimation