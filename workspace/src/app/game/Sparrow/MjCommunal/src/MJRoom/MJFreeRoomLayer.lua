--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 自由房列表

local StackLayer = require("app.hall.base.ui.StackLayer")
local MJHelper   = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local ROOM_LIST = { 120002, 120003, 120004, 120005 }

local MJFreeRoomLayer = class("MJFreeRoomLayer", function(  )
    return StackLayer.new()
end)

function MJFreeRoomLayer:ctor( param )
	self:myInit()

	self:setupViews()

	self:setData(param)

	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_NODE_DATA, handler(self, self.onUpdateNodeData))
    ToolKit:registDistructor( self, handler(self, self.onDestory) )
end

function MJFreeRoomLayer:onDestory()
	print("MJFreeRoomLayer:onDestory()")
	sendMsg(PublicGameMsg.MSG_CCMJ_VIP_SHOW_UI_ANI,true)
    -- 注销消息
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_NODE_DATA)
end

function MJFreeRoomLayer:myInit()
	self.roomDatas = {}
	self.playerCount = {}
end

function MJFreeRoomLayer:setupViews()
	local view = UIAdapter:createNode("csb/mahjong_gold_room_layer.csb")
	self:addChild(view)
	UIAdapter:adapter(view, handler(self, self.onGoldRoomCallBack))

	if not GlobalConf.DEBUG_MODE then
		--view:getChildByName("btn_bank"):setVisible(false)
	end
	
	local layout_mid = view:getChildByName("layout_mid")
	-- layout_mid:setTouchEnabled(true)
	local action1 = cc.ScaleTo:create(0.15, 1)
 --    local action2 = cc.ScaleTo:create(0.05, 0.9)
 --    local action3 = cc.ScaleTo:create(0.05, 1)
	-- local seq = cc.Sequence:create(action1, action2, action3)
	layout_mid:setScale(0.5)
	layout_mid:runAction(action1)

	self.items = {}
	for i = 1, 6 do
		self.items[i] = layout_mid:getChildByName("item_" .. i)
		local x, y = self.items[i]:getPosition()
		local size = self.items[i]:getContentSize()
		self.items[i]:setPosition(x + size.width/2, y + size.height/2)
		self.items[i]:setAnchorPoint(cc.p(0.5, 0.5))
		-- self.items[i]:setScale(0)
		-- performWithDelay(self.items[i], function ()
		-- 	local action1 = cc.ScaleTo:create(0.1, 1)
		    -- local action2 = cc.ScaleTo:create(0.05, 0.9)
		    -- local action3 = cc.ScaleTo:create(0.05, 1)
			-- local seq = cc.Sequence:create(action1, action2, action3)
		-- 	self.items[i]:runAction(action1)
		-- end, 0.05 * (i-1))
	end

	local layout_show = view:getChildByName("layout_show")
	local layout_1 = view:getChildByName("layout_1")
	local layout_2 = view:getChildByName("layout_2")

	local t = { layout_show, layout_1, layout_2 }
	for k, layout in pairs(t) do
		layout:setPositionY(layout:getPositionY() + layout:getContentSize().height)
		local fadeIn = cc.FadeIn:create(0.2)
		local moveIn = cc.MoveBy:create(0.2, cc.p(0, - layout:getContentSize().height))
		layout:runAction(fadeIn)
		layout:runAction(moveIn)
	end
	
end

function MJFreeRoomLayer:setData( param )
	-- dump(param, "msg", 9)
	self.param = param
	self.param.m_portalList = self.param.m_portalList or {}

	table.sort( self.param.m_portalList, function (a, b)
		if a.m_sort == b.m_sort then
			return a.m_portalId < b.m_portalId
		else
			return a.m_sort < b.m_sort
		end
	end )

	self.roomDatas = {}
	self.playerCount = {}
	for k, v in pairs(self.param.m_portalList) do
		local nodeData = RoomData:getGameDataById(v.m_portalId)
		local data = RoomData:getRoomDataById(nodeData.funcId)
		data.m_sort = v.m_sort
		table.insert(self.roomDatas, data)

		for k, att in pairs(v.m_attrList1) do
			if att.m_pid == 1 then
				self.playerCount[data.gameAtomTypeId] = att.m_value
			end
		end
		
		for k, att in pairs(v.m_attrList2) do
			if att.m_pid == 1 then
				self.playerCount[data.gameAtomTypeId] = att.m_value
			end
		end
	end
	-- dump(self.roomDatas, "data", 9)
	-- for k, v in pairs(ROOM_LIST) do
	-- 	local data = RoomData:getRoomDataById(v)
	-- 	table.insert(self.roomDatas, data)
	-- end
	-- dump(self.playerCount)
	self:updateViews()
end

function MJFreeRoomLayer:updateViews()
	for k, layout in pairs(self.items) do
		if self.roomDatas[k] then
			self:updateItem(layout, self.roomDatas[k])
			layout:setVisible(true)
		else
			layout:setVisible(false)
		end
	end
end

function MJFreeRoomLayer:onUpdateNodeData( msgStr, data )
	self:setData(data)
end

function MJFreeRoomLayer:onGoldRoomCallBack(sender)
	local name = sender:getName()

	if name == "btn_return" then
		-- sendMsg(PublicGameMsg.MSG_CCMJ_ENTER_SOME_LAYER, { name = "back"})
		local scene = display.getRunningScene()
		scene:getStackLayerManager():popStackLayer()
	elseif name == "btn_bank" then
		sendMsg(PublicGameMsg.MSG_CCMJ_VIP_LOBBY_OPT, "btn_bank", {})	
	end
end

function MJFreeRoomLayer:updateItem(layout, data)
	local item = layout:getChildByTag(165466)
	if item == nil then
		item = UIAdapter:createNode("csb/mahjong_gold_room_item.csb")
		item:setTag(165466)
		UIAdapter:adapter(item)
		layout:addChild(item)
		
		local btn = item:getChildByName("btn")
		UIAdapter:registClickCallBack( btn, function ()
			local gameData = layout.data
			local param=
			{
				id = nil,
				req = "CS_C2H_EnterScene_Req",
				ack = "CS_H2C_EnterScene_Ack",
				dataTable = {gameData.gameAtomTypeId, 0, 0, 0, 0},
			}
			local function callback(param,body)
				MJHelper:getUIHelper():removeWaitLayer()
				if not param then
					local playerManager = MJHelper:getPlayerManager(true)
					playerManager:setGameAtomTypeId(nil)
					ConnectManager:send2SceneServer(gameData.gameAtomTypeId, "CS_C2M_ExitScene_Req", {} ) 
					return 
				end
				if param and param.m_ret==0 then
					local playerManager = MJHelper:getPlayerManager(true)
					playerManager:setGameAtomTypeId(param.m_gameAtomTypeId)
					playerManager:setVipRoom(false)
					local info = 
					{
						id = param.m_gameAtomTypeId,
						req = "CS_C2M_EnterRoom_Req",
						ack = "CS_M2C_EnterRoom_Ack",
						dataTable = {1,0}
					}
					local function enter_room_callback(param,body)
						param = param or {m_ret=0}
						if param.m_ret == 0 then
							print("success enter gold room")
						else
							--不能进入房间，则退出场景返还金币
							local gameAtomTypeId = playerManager:getGameAtomTypeId()
							playerManager:setGameAtomTypeId(nil)
							ConnectManager:send2SceneServer(gameAtomTypeId, "CS_C2M_ExitScene_Req", {} )
						end
					end 
					MJBaseServer:sceneRpcCall(info,enter_room_callback)
				else
					if param.m_ret == -747 then
						return -- 系统维护, 避免重复提示, 这里直接返回
					else
						ToolKit:showErrorTip( param.m_ret )
					end
					
				end
			end
			MJHelper:getUIHelper():addWaitLayer(10,"正在进入房间...")
			MJBaseServer:sceneRpcCall(param,callback)
		end )
	end
	
	layout.data = data
	local btn = item:getChildByName("btn")
	local label_1 = item:getChildByName("label_1")
	local label_2 = item:getChildByName("label_2")
	local label_3 = item:getChildByName("label_3")
	local labelDes = item:getChildByName("label_des")

	local str = data.phoneGameName or ""
	local num = string.find(str, "（")
	num = num or (#data.phoneGameName + 1)
	local gameName = string.sub(str, 1, num - 1)
	local des = string.sub(str, num, -1)
	label_1:setString(gameName)
	labelDes:setString(des)
	local pos = cc.p(label_1:getPosition())
	local size = label_1:getContentSize()
	labelDes:setPosition(pos.x + size.width, pos.y)

	local t1 = json.decode(data.roomMinScore)
	local ante = nil
	for k, v in pairs(t1) do
		if ante == nil then
			ante = tonumber(k)
		else
			if ante > tonumber(k) then
				ante = tonumber(k)
			end
		end
	end
	label_2:setString(MJHelper:STR(18)..ante)

	local num = self.playerCount[data.gameAtomTypeId] or 0
	label_3:setString(num .. MJHelper:STR(19))

end

return MJFreeRoomLayer