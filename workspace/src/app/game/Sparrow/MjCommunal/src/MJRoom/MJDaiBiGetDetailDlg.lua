--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 麻将获得代币详情
local STRING_CONFIG = {
	-- {
	-- tirtle = "*玩游戏，得元宝:",
	-- str1 = "玩20局，得50；玩50局，得200；玩100局，得500。",
	-- str2 ="截图联系客服，获得代币奖励。"
	-- },
	{
	tirtle ="*邀玩家，获收益:",
	str1 = "1、 您邀请的玩家，每购买一张房卡，您都可以获得最多50%的返利。",
	str2 = "2、您邀请的玩家，每胡一次，你都可以获得元宝奖励。"
	},
	{
	tirtle = "*做代理，赚大钱:",
	str1 = "1、 成为代理后，您可以获得更多的收益。",
	str2 ="2、代理可以通过玩家推广的玩家获取收益。"
	}
}
local QkaDialog = require("app.hall.base.ui.CommonView")

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local MJDaiBiGetDetailDlg = class("MJDaiBiGetDetailDlg", function ()
    return QkaDialog.new()
end)

function MJDaiBiGetDetailDlg:ctor()
	self:myInit()

	self:setupViews()
end

function MJDaiBiGetDetailDlg:myInit()
	self:setTouchEnabled(true)
end

function MJDaiBiGetDetailDlg:setupViews()
	self.root = UIAdapter:createNode("csb/Sparrow_get_daibi_detail.csb")
    self:addChild(self.root)
    UIAdapter:adapter(self.root, handler(self, self.onCreateLayerCallBack))

    self.contentList = self.root:getChildByName("list_content")

    self:initContent()
end

function MJDaiBiGetDetailDlg:initContent()
	for i = 1 , #STRING_CONFIG do 
		local item = UIAdapter:createNode("csb/mahjong_get_daibi_item.csb")
		 UIAdapter:adapter(item, function()end)

		 local tirtle = item:getChildByName("txt_1")
		 local str1 = item:getChildByName("txt_2")
		 local str2 = item:getChildByName("txt_3")

		 tirtle:setString(STRING_CONFIG[i].tirtle)
		 str1:setString(STRING_CONFIG[i].str1)
		 str2:setString(STRING_CONFIG[i].str2)

		local custom_item = ccui.Layout:create()
		custom_item:setContentSize(cc.size(600,100))
		custom_item:addChild(item)

		self.contentList:pushBackCustomItem(custom_item)
	end
end

function MJDaiBiGetDetailDlg:onDestroy()           
	
end
function MJDaiBiGetDetailDlg:onCreateLayerCallBack(sender)
		local name = sender:getName()
		print(name)
		if name == "btn_close" then
			self:closeDialog()
   		end

end


return MJDaiBiGetDetailDlg