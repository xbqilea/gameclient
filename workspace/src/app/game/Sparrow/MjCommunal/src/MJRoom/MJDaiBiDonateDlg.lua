--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将房间管理

local QkaDialog = require("app.hall.base.ui.CommonView")

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local DonateData = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJDonateData")
local MJDaiBiDonateDlg = class("MJDaiBiDonateDlg", function ()
    return QkaDialog.new()
end)

function MJDaiBiDonateDlg:ctor()
	self:myInit()

	self:setupViews()
end

function MJDaiBiDonateDlg:myInit()
	self:setTouchEnabled(true)

    --玩家玩了多少局
    self.curRound = Player:getTokenRoomInning()
    --第一阶段要求达到局数
    self.round1 = DonateData:getRound1Limit()
    --第二阶段要求达到的局数
    self.round2 = DonateData:getRound2Limit()
    --达到第一阶段但是没有到第二阶段时限制预留元宝
    self.roundLimit = DonateData:getMinCoinLimit()
    --玩家的代币数量
    GlobalDefine.ITEM_ID.DaiBiCard = 9998 
    self.daiBiNum = GlobalBagController:getCountByItemID(GlobalDefine.ITEM_ID.DaiBiCard)    --代币数量

    addMsgCallBack(self, MSG_BAG_UPDATE_SUCCESS, handler(self, self.updateDaiBi))
    ToolKit:registDistructor( self, handler(self, self.onDestroy) )

end

function MJDaiBiDonateDlg:setupViews()
	self.root = UIAdapter:createNode("csb/mahjong_donate_daibi.csb")
    self:addChild(self.root)
    UIAdapter:adapter(self.root, handler(self, self.onCreateLayerCallBack))

    local y = self.root:getChildByName("layout_bg"):getPositionY()
    self.root:getChildByName("layout_bg"):setPositionY(y - 10)

    self.tf_id = self.root:getChildByName("tf_id")
    --给tf_id一个标记，后面仅id光标离开输入框请求名字
    self.tf_id.flag = "TF_ID"
    self.tf_num = self.root:getChildByName("tf_num")
    self.tf_num.flag = "TF_NUM"
    --默认设置为空
    self.tf_id:setString("")
    self.tf_num:setString("")
    self.btn_donate = self.root:getChildByName("btn_donate")
    self.txt_donate = self.root:getChildByName("txt_donate")
    self.txt_donate:enableOutline(cc.c4b(29, 149, 124, 255), 3)

    
    self.txt_user_name = self.root:getChildByName("txt_user_name")
    self.txt_user_name:setString("")

    --玩家可以赠送最大代币提示
    self.txt_max_num = self.root:getChildByName("txt_max_num")
    self.txt_max_num:setString("")
    
    self:initTextField(self.tf_id)
    self:initTextField(self.tf_num)
    self.tf_id:setInputMode(cc.EDITBOX_INPUT_MODE_PHONENUMBER)
    self.tf_num:setInputMode(cc.EDITBOX_INPUT_MODE_PHONENUMBER)

    --根据玩家玩的局数来判断玩家可以赠送的最大代币数量
    self:checkRoundLimit()
end

function MJDaiBiDonateDlg:initTextField(node)
    local layer = self
    local edit = node
    --local tips = node:getParent():getChildByName("text_tips")
    local function onEdit(textfield, event)
        if event == ccui.TextFiledEventType.attach_with_ime then
        	print("ccui.TextFiledEventType.attach_with_ime")

        elseif event == ccui.TextFiledEventType.insert_text then
            local str = edit:getString()
            print("ccui.TextFiledEventType.insert_text")
            if string.find(str, "^%d+$") then

                if edit.flag == "TF_NUM" then --赠送数量0开头去除0(赠送数量自动去掉开头的0)
                    local x  , _ = string.gsub(str,"^0(%d*)$","")
                    edit:setString(x)
                end
                   
            --输入不合法字符则去掉不合法字符 
            else
                local x  , _ = string.gsub(str,"[^0-9]","")
                edit:setString(x)
            end

        elseif event == ccui.TextFiledEventType.detach_with_ime then
        	print("ccui.TextFiledEventType.detach_with_ime")
         
            local str = edit:getString()
            if str == "" then
            else
                if string.find(str, "^[0-9]%d*$") then --or string.find(str, "^0$") then
                    --id输入框光标离开输入框时请求 以便显示玩家名称
                    if edit.flag == "TF_ID" then
                        self:onRequireUserName()
                    elseif edit.flag == "TF_NUM" then

                        self:checkCoinNum(edit:getString())
                    end
                else
                    edit:setString("")
                end
            end
        end
    end

    edit:addEventListener(onEdit)
end


function MJDaiBiDonateDlg:onCreateLayerCallBack(sender)
		local name = sender:getName()
		if name == "btn_donate"then 
            self.donate_clicked = true

            self:onRequireUserName()
            -- local MJDonateTip =  MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJDonateTipDlg")
            -- local mjDonateTip = MJDonateTip.new()
            -- mjDonateTip:setData(self.acceptData)
            -- mjDonateTip:showDialog()

		elseif name == "btn_close" then
			self:closeDialog()
   		end
end

function MJDaiBiDonateDlg:onRequireUserName()
    --不可以进入确认界面
    local dt = ToolKit:getTimeMs()
    function callBack(param)
      self:onResponseUserName(param)
    end

    local id = tonumber(self.tf_id:getString()) or 0
    local num = tonumber(self.tf_num:getString()) or 0

    --判断是不是赠送给自己
    if id == Player:getAccountID() then 
       -- TOAST("不能赠送给自己")
        self:onErrorCode(-1218)
        return
    end

    local info = 
    {
        req = "CS_C2H_DonateItem_Req",
        ack = "CS_H2C_DonateItem_Ack",
        dataTable = {id,9998,num}
    }
    dump({id,9998,num},"----------------------------->")

    local function get_history_callback(param,body)
        MJHelper:getUIHelper():removeWaitLayer()
        if not param then 
            print("服务器没有返回数据")
        end

        local t = ToolKit:getTimeMs()-dt
        if t<0.15 then
            performWithDelay(self,function() callBack(param) end,0.15-t)
        else
            callBack(param)
        end
    end
    MJHelper:getUIHelper():addWaitLayer(10,"正在获取数据...")
    MJBaseServer:sceneRpcSend(info,get_history_callback)

end

function MJDaiBiDonateDlg:onResponseUserName(param)
    if not param then print("未返回数据") return end
    local id = tonumber(self.tf_id:getString()) or 0
    local num = tonumber(self.tf_num:getString()) or 0
    --校验数据
    function checkData ()
        if param.m_toPlayer == id and param.m_itemNum == num then
            return true
        else 
            return false
        end
    end

    if param.m_result == 0  then    --请求成功
        local name  = param.m_toNickname or ""
        self.txt_user_name:setString(name)
        self.txt_user_name:setColor(cc.c3b(20, 200, 20))
        self.acceptData = param

        --点击赠送执行
        if  self.donate_clicked  then
            if num == 0 then 
                TOAST("请输入赠送数量")
                return 
            end
            local MJDonateTip =  MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJDonateTipDlg")
            local mjDonateTip = MJDonateTip.new(self.acceptData)
            mjDonateTip:showDialog()
        end
    else                            --请求失败
        print("请求赠送返回错误码: ",param.m_result)
        -- local name  = param.m_toNickname or ""
        -- self.txt_user_name:setString(name)

        ----点击赠送请求失败
        if  self.donate_clicked  then
            ToolKit:showErrorTip(param.m_result)
        --检测执行
        else
            self:onErrorCode(param.m_result)
        end

    end

    self.donate_clicked = false
  
   
end

function MJDaiBiDonateDlg:onErrorCode(_errorCode)
    local name  =  ""
    self.txt_user_name:setColor(cc.c3b(255, 0, 0))
    if _errorCode == -1218 then  --不能赠送自己
         name = "不能给自己赠送元宝"
        self.txt_user_name:setString(name)
    elseif _errorCode == -10 then   -- 没有此id
        name = "玩家不存在"
        self.txt_user_name:setString(name)
    else
        self.txt_user_name:setString(name)
    end
end


function MJDaiBiDonateDlg:checkCoinNum(_coinNum)
if  self.curRound >= self.round1 and self.curRound < self.round2 then  --达到第一阶段 
    --if true then 
        if tonumber(_coinNum) > self.daiBiNum - self.roundLimit then 
             self.tf_num:setString(self.daiBiNum - self.roundLimit)
        end
    elseif self.curRound >= self.round2 then  --达到第二阶段 
        if tonumber(_coinNum) > self.daiBiNum then 
            self.tf_num:setString(self.daiBiNum)
        end
    end
end

function MJDaiBiDonateDlg:updateDaiBi()
    --玩家的代币数量
    GlobalDefine.ITEM_ID.DaiBiCard = 9998 
    self.daiBiNum = GlobalBagController:getCountByItemID(GlobalDefine.ITEM_ID.DaiBiCard)    --代币数量

    self:checkRoundLimit()
end

function MJDaiBiDonateDlg:checkRoundLimit()
    if   self.curRound >= self.round1 and self.curRound < self.round2 then  --达到第一阶段 ，大于limit的可以赠送
    --if  true then  
        local canDonate = self.daiBiNum - self.roundLimit
        if canDonate > 0 then
            canDonate = canDonate
        else
            canDonate = 0
        end
        self.txt_max_num:setString(string.format("最多可赠送%d元宝",canDonate))
    elseif self.curRound >= self.round2 then  --达到第二阶段 ，最多可以全部赠送
        self.txt_max_num:setString(string.format("最多可赠送%d元宝",self.daiBiNum))
    end
    
end

function MJDaiBiDonateDlg:onDestroy()
        removeMsgCallBack(self, MSG_BAG_UPDATE_SUCCESS)
end




return MJDaiBiDonateDlg