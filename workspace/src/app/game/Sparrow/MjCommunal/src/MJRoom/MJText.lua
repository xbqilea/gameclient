--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 文字

local createRoomdata = require("app.game.Sparrow.MjCommunal.src.MJConfig.MJ_Text")
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local MJText = {}

function MJText:getProxyText()
	return createRoomdata[1].Describe
end

return MJText
