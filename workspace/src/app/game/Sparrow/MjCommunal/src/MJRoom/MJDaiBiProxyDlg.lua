--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 麻将获得代币详情

local QkaDialog = require("app.hall.base.ui.CommonView")

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
--local RichText = require("app.game.Sparrow.MjCommunal.src.MJRoom.RichText")

local MJDaiBiProxyDlg = class("MJDaiBiProxyDlg", function ()
    return QkaDialog.new()
end)

function MJDaiBiProxyDlg:ctor()
	self:myInit()

	self:setupViews()
end

function MJDaiBiProxyDlg:myInit()
	self:setTouchEnabled(true)
end

function MJDaiBiProxyDlg:setupViews()
	self.root = UIAdapter:createNode("csb/mahjong_get_daibi_detail.csb")
    self:addChild(self.root)
    UIAdapter:adapter(self.root, handler(self, self.onCreateLayerCallBack))

    self.contentList = self.root:getChildByName("list_content")

    self:initContent()
end

function MJDaiBiProxyDlg:initContent()

	local MJText = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJText")
	local des = MJText:getProxyText()

		local label = display.newTTFLabel({
	    text = des,
	    font = "ttf/jcy.TTF",
	    size = 30,
	    color = cc.c3b(110, 75, 71), -- 使用纯红色
	    --align = cc.TEXT_ALIGNMENT_LEFT,
	    --valign = cc.VERTICAL_TEXT_ALIGNMENT_TOP,
	    dimensions = cc.size(550, 300)
		})
		label:setAnchorPoint(cc.p(0,0))
		label:setPositionX(30)
		local custom_item = ccui.Layout:create()
		custom_item:setContentSize(cc.size(580,300))
		custom_item:addChild(label)

		self.contentList:pushBackCustomItem(custom_item)
end

function MJDaiBiProxyDlg:onDestroy()           
	
end
function MJDaiBiProxyDlg:onCreateLayerCallBack(sender)
		local name = sender:getName()
		print(name)
		if name == "btn_close" then
			self:closeDialog()
   		end

end


return MJDaiBiProxyDlg