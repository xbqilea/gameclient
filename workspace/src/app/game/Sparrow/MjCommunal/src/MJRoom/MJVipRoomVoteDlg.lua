--
-- Author: 
-- Date: 2018-08-07 18:17:10
--


local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local QkaDialog = require("app.hall.base.ui.CommonView")
local scheduler = require("framework.scheduler")

local MJVipRoomVoteDlg = class("MJVipRoomVoteDlg", function ()
    return QkaDialog.new()
end)

function MJVipRoomVoteDlg:ctor()
	self:myInit()

	self:setupViews()

    self:updateViews()

    ToolKit:registDistructor( self, handler(self, self.onDestory) )

    addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_DISBAND, handler(self, self.updateViews))

    self:enableTouch(false)
    self.dlgLayoutMain:setSwallowTouches(true)
end

function MJVipRoomVoteDlg:onDestory()
    print("MJVipRoomVoteDlg:onDestory()")
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_DISBAND)

    if self.pTimerBtn then
        scheduler.unscheduleGlobal(self.pTimerBtn)
        self.pTimerBtn = nil
    end

    if self.pTimerLabel then
        scheduler.unscheduleGlobal(self.pTimerLabel)
        self.pTimerLabel = nil
    end

    local playerManager = MJHelper:getPlayerManager(true)
    playerManager:setDisbanState(MJDef.eDisbandState.none)
    local playerInfos = playerManager:getPlayerInfos()
    for k, info in pairs(playerInfos) do
        info:setDisbandOpinion(MJDef.eDisbandVote.none)
    end
    sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = false, name = MJDef.eAnimation.JIE_SAN})
end

function MJVipRoomVoteDlg:myInit()
	self.pTimerBtn = nil
    self.pTimerLabel = nil
end

function MJVipRoomVoteDlg:setupViews()
	-- body
	self.root = UIAdapter:createNode("csb/mahjong_vote_dismiss.csb")
    self:addChild(self.root)
    UIAdapter:adapter(self.root, handler(self, self.onVoteCallBack))

    --self.img_title = self.root:getChildByName("img_title")
    self.txt_title = self.root:getChildByName("txt_title")
    self.label_des = self.root:getChildByName("label_des")
	
	self.txt_title:setFontName("")
	
    self.label_cd = self.root:getChildByName("label_cd")
    self.btn_no = self.root:getChildByName("btn_no")
    self.btn_yes = self.root:getChildByName("btn_yes")
    self.btn_confirm = self.root:getChildByName("btn_confirm")

    self.layout = {}
    for i = 1, 4 do
        self.layout[i] = self.root:getChildByName("layout_vote_" .. i)
    end

    self.label_cd:enableOutline(cc.c4f(29, 149, 124, 255), 3)
    self.btn_yes:enableOutline(cc.c4f(29, 149, 124, 255), 3)
    self.btn_confirm:enableOutline(cc.c4f(29, 149, 124, 255), 3)
    self.btn_no:enableOutline(cc.c4f(149, 29, 29, 255), 3)

    self.label_des:setString(MJHelper:STR(11))
    self.btn_yes:setTitleText(MJHelper:STR(13))
    self.btn_no:setTitleText(MJHelper:STR(14))
    self.btn_confirm:setTitleText(MJHelper:STR(15))
end

function MJVipRoomVoteDlg:updateViews()
    self.label_cd:setVisible(false)
    self.btn_no:setVisible(false)
    self.btn_confirm:setVisible(false)
    self.btn_yes:setVisible(false)

    for i = 1, 4 do
        self.layout[i]:setVisible(false)
    end

    local playerManager = MJHelper:getPlayerManager(true)
    local disbandState = playerManager:getDisbanState()
    -- print(disbandState)
    if disbandState == MJDef.eDisbandState.failure then
        --self.img_title:loadTexture("mj_tangchuang_font_jssb.png", 1)
        self.btn_confirm:setVisible(true)
    elseif disbandState == MJDef.eDisbandState.succeed then
        --self.img_title:loadTexture("mj_tangchuang_font_jscg.png", 1)
        self.btn_confirm:setVisible(true)
    elseif disbandState == MJDef.eDisbandState.doing then
        --self.img_title:loadTexture("mj_tangchuang_font_jstp.png", 1)

        local myAid = Player:getAccountID()
        local myPlayerInfo = playerManager:getPlayerInfoByAid(myAid)
        if myPlayerInfo:getDisbandOpinion() == MJDef.eDisbandVote.none then
            self.btn_yes:setVisible(true)
            self.btn_no:setVisible(true)

            self:showButtonCD()
            if self.pTimerBtn == nil then
                self.pTimerBtn = scheduler.scheduleGlobal(function ()
                    self:showButtonCD()
                    local time = playerManager:getDisbanTime()
                    if time <= 0 then
                        scheduler.unscheduleGlobal(self.pTimerBtn)
                        self.pTimerBtn = nil
                        sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "vote_dismiss_room", 1)
                    end
                end, 1)
            end
        else
            self.label_cd:setVisible(true)

            self:showLabelCD()
            if self.pTimerLabel == nil then
                self.pTimerLabel = scheduler.scheduleGlobal(function ()
                    self:showLabelCD()
                    local time = playerManager:getDisbanTime()
                    if time <= 0 then
                        scheduler.unscheduleGlobal(self.pTimerLabel)
                        self.pTimerLabel = nil
                    end
                end, 1)
            end
        end
    end

    -- 玩家&投票信息
    local playerInfos = playerManager:getPlayerInfos()
    for i = 1, 4 do
        if playerInfos[i] then
            self.layout[i]:setVisible(true)
            self:setPlayerInfo(self.layout[i], playerInfos[i])
        else
            self.layout[i]:setVisible(false)
        end
    end

    local str = ""
    local raisePlayer = playerManager:getPlayerInfoByAid(playerManager:getDissVoteRaiser())
    if raisePlayer then
        str = "【" .. raisePlayer:getNickname() .. "】" .. MJHelper:STR(25)
    end
    self.txt_title:setString(str)
end

function MJVipRoomVoteDlg:setPlayerInfo( layout, info )
    local proj_head = layout:getChildByName("proj_head")
    if layout.head == nil then
        layout.head = UIAdapter:createNode("csb/ccmj_player_head_item.csb")
        proj_head:addChild(layout.head)

        layout.btn_head = layout.head:getChildByName("btn_head")
        layout.txt_player_state = layout.head:getChildByName("txt_player_state")
        layout.img_banker_icon = layout.head:getChildByName("img_banker_icon")
        layout.img_unlink = layout.head:getChildByName("img_unlink")

        layout.img_banker_icon:setVisible(false)
        layout.txt_player_state:setVisible(false)
        layout.img_unlink:setVisible(false)

        layout.img_room_owner = layout.head:getChildByName("img_room_owner")
        layout.img_room_owner:setVisible(false)

        self:initHead(layout.head)
    end
	
	if layout.head.head_node then
		layout.head.head_node:updateTexture(info:getFaceId(),info:getAccountId())
	end
    -- 房主
    -- local playerManager = MJHelper:getPlayerManager(true)
    -- local is_this_room_owner = playerManager:isThisRoomOwner(info) 
    -- if is_this_room_owner then
    --     layout.img_room_owner:setVisible(true)
    -- else
    --     layout.img_room_owner:setVisible(false)
    -- end
    -- 昵称
    local label_name = layout:getChildByName("txt_name")
    label_name:setString(info:getNickname())
    -- 庄家
    -- local gameManager = MJHelper:getGameManager(true)
    -- if gameManager:getBanker() == info:getChairId() then
    --     layout.img_banker_icon:setVisible(true)
    -- else
    --     layout.img_banker_icon:setVisible(false)
    -- end
    -- 投票状态
    local txt_vote = layout:getChildByName("txt_vote")
    local img_vote = layout:getChildByName("img_vote")

    local playerManager = MJHelper:getPlayerManager(true)
    local disbandState = playerManager:getDisbanState()
    local opinion = info:getDisbandOpinion()
    -- print("opinion", opinion)
    if disbandState == MJDef.eDisbandState.doing then
        txt_vote:setVisible(true)
        img_vote:setVisible(false)
        -- 投票中/已投票
        if opinion == MJDef.eDisbandVote.none then
            txt_vote:setString(MJHelper:STR(16))
            txt_vote:setColor(cc.c4f(207, 26, 7, 255))
        -- else
        --     txt_vote:setString(MJHelper:STR(17))
        --     txt_vote:setColor(cc.c4f(165, 239, 119, 255))
        -- end

        elseif opinion == MJDef.eDisbandVote.agreement then
            txt_vote:setString(MJHelper:STR(13))
            txt_vote:setColor(cc.c4f(165, 239, 119, 255))
        elseif opinion == MJDef.eDisbandVote.disagreement then
            txt_vote:setString(MJHelper:STR(14))
            txt_vote:setColor(cc.c4f(207, 26, 7, 255))
        end

    else
        txt_vote:setVisible(false)
        img_vote:setVisible(false)
        -- 同意/不同意
        if opinion == MJDef.eDisbandVote.agreement then
            img_vote:setVisible(true)
            img_vote:loadTexture("mj_game_icon_gou.png", 1)
        elseif opinion == MJDef.eDisbandVote.disagreement then
            img_vote:setVisible(true)
            img_vote:loadTexture("mj_game_icon_cha.png", 1)
        end
    end
end


function MJVipRoomVoteDlg:initHead( root )
    if not root.head_node then
        local img_head_bg = root:getChildByName("img_head_bg")
        local MJUIHelper = MJHelper:getUIHelper()
		local _img_head = MJUIHelper:initPlayerHead(img_head_bg,false,false,"majiang_tubiao_txk.png")
        root.head_node = _img_head
    end 
end

function MJVipRoomVoteDlg:showButtonCD()
    local playerManager = MJHelper:getPlayerManager(true)
    local time = playerManager:getDisbanTime()
    if time > 0 then
        self.btn_yes:setTitleText(MJHelper:STR(13) .. " " .. time)
    else
        self.btn_yes:setTitleText(MJHelper:STR(13))
    end
end

function MJVipRoomVoteDlg:showLabelCD()
    local playerManager = MJHelper:getPlayerManager(true)
    local time = playerManager:getDisbanTime()
    if time > 0 then
        self.label_cd:setString(string.format(MJHelper:STR(12), time))
    else
        self.label_cd:setString(string.format(MJHelper:STR(12), 0))
    end
end

function MJVipRoomVoteDlg:onVoteCallBack( sender )
	local name = sender:getName()
    print(name) 
    if name == "btn_yes" then
        print("yes dismiss")
        local data = 1
        sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "dis_room_vote", data)
    elseif name == "btn_no" then
        print("no dismiss")
        local data = 2
        sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "dis_room_vote", data)
    elseif name == "btn_confirm" then
        self:closeDialog()
        local playerManager = MJHelper:getPlayerManager(true)
        playerManager:setDisbanState(MJDef.eDisbandState.none)
        local playerInfos = playerManager:getPlayerInfos()
        for k, info in pairs(playerInfos) do
            info:setDisbandOpinion(MJDef.eDisbandVote.none)
        end
        sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "run_msg_queue", {is = false, name = MJDef.eAnimation.JIE_SAN})
    end
end

return MJVipRoomVoteDlg