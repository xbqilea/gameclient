--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 富文本类


local RichText = class("RichText",function() 
	return ccui.Layout:create()
end)

-- 这是一段测试文本
--<color = 0xffffff>玩家 xxxx 在今日比赛中获得了</color><color = 0xffffff>99</color>个代币
function RichText:ctor(_richText , _fontSize , _contentSize)
	self.labelPool = {}
	self.nextLineChangeLine = false
 	self.testStr = ""
	self:myInit(_richText , _fontSize , _contentSize)
end

--初始化  _contentSize cc.size() or nil
function RichText:myInit(_richText , _fontSize , _contentSize)
	self:initData(_richText , _fontSize , _contentSize)
	--local strData = self:trimStr(self.labelString)
	local strData = self.labelString
	--加一个测试检验-->几行几列
	self:splitToLines(strData)

	self:_newLabels()
	self:linkLabels()  
end


--初始化数据
function RichText:initData(_richText , _fontSize , _contentSize)
	--self.labelString = _richText or ""
	self.labelString = _richText or {}

	self.labelTab = {}   --label数组清空
	self.strPiece = {}   --文字碎片
	self.fontSize = _fontSize  or 40
	self.contentSize = _contentSize or "singleLine"
end

--从父节点中移除label
function RichText:removeLabels()
	for i = 1, #self.labelTab do 
		-- self.labelTab[i].label:retain()
		-- self.labelTab[i].label:removeFromParent()
		-- labelPool[#labelPool + 1] = self.labelTab[i].label
		-- self.labelTab[i].label:release()

		self:_putLabel(self.labelTab[i].label)
	end
end

--
function RichText:splitToLines(strData)
	--dump(strData,"strData")

	for i = 1 , #strData do
		local str = strData[i].str
		local color = strData[i].color
		self:dealString(strData[i].str,strData[i].color)
	end

end

function RichText:dealString(_str,_color)
	local strTab = {}
	local len = string.len(_str)
	local left = len
	local index = 1
	--local catchIndex = 1
	local itemStr = ""
	--local nextLineChangeLine = false
	local arr  = {0, 0xc0, 0xe0, 0xf0, 0xf8, 0xfc}
	while left ~= 0 do
		local temp = string.byte(_str,-left)
		local i = #arr
		while arr[i] do 
			if temp >= arr[i] then
				left = left - i
				break
			end
				i = i - 1
		end
		
		local word = string.sub(_str , index , index + i - 1)

		self.testStr = self.testStr .. word
		if self:testLength(self.testStr) > self.contentSize.width or word == "\n" then 
			if self.nextLineChangeLine then
				self.strPiece[#self.strPiece + 1] = {str = itemStr , color = _color , cLine = true}
				self.nextLineChangeLine = true
			else
				self.strPiece[#self.strPiece + 1] = {str = itemStr , color = _color , cLine = false}
				self.nextLineChangeLine = true
			end
			
			--测试不通过，不放入当前这次字符串中，放入一下一次字符串中
			if word == "\n" then 
				self.testStr = ""
				itemStr = ""
			else
				self.testStr = word
				itemStr = word
			end
		else
			itemStr = itemStr .. word
		end
		index = index + i
	end
	if itemStr  ~= "" then
		if self.nextLineChangeLine then
			self.strPiece[#self.strPiece + 1] = {str = itemStr , color = _color , cLine = true}
			self.nextLineChangeLine = false
		else
			self.strPiece[#self.strPiece + 1] = {str = itemStr , color = _color , cLine = false}
			self.nextLineChangeLine = false
		end
	end
end

function RichText:testLength(_str)
	if not self.label then 
		self.label = display.newTTFLabel({
    	text = "",
	    --font = "jcy",
	    size = self.fontSize,
		})
	end
	self.label:setString(_str)

	return self.label:getContentSize().width
end


--label链接起来
function RichText:linkLabels()
	if #self.labelTab == 0 then
		return 
	end

	self:updateLabelPos()

	for i = 1 , #self.labelTab do
		self:addChild(self.labelTab[i].label)
	end
	
end

--更新label位置
function RichText:updateLabelPos()
	if #self.labelTab == 0 then
		return 
	end
	for i = 1 , #self.labelTab do
		local pos = nil
		local x , y = 0 , 0
		if self.labelTab[i] and self.labelTab[i].cLine == true then 
			
			x  = 0 
			local height = self.labelTab[i].label:getContentSize().height 
			if height == 0 then  height  = self:getTestLabelSize().height end

			y  = self.labelTab[i - 1].label:getPositionY() - height
			size = cc.size(0,0)
			
		elseif self.labelTab[i] and self.labelTab[i].cLine == false then
			if self.labelTab[i - 1] ~= nil then
				x , y = self.labelTab[i-1].label:getPosition()

				size = self.labelTab[i-1].label:getContentSize()
				
			else
				x,y  = 0,0
				size = cc.size(0,0)
			end
		end

		 self.labelTab[i].label:setPosition(x + size.width,y)
	end
end

--从字符串中切出用用数据段 (没用)
function RichText:trimStr(_richText)
	local strTab = {}
	local strData = {}
	for word in string.gmatch(_richText, "%<%s*color%s*=%s*%w+%>%s*[^%<]*%<%s*%/color%s*%>") do
   	 	strTab[#strTab + 1] = word
	end
	 for i = 1 , #strTab do 
	 	local color , str = string.match(strTab[i], "%<%s*color%s*=%s*(%w+)%>%s*([^%<]*)%<%s*%/color%s*%>")
	 	if strData[i] == nil then
	 		strData[i] = {}
	 	end
	 	strData[i].color = color
	 	strData[i].str = str
	 end
	return strData
end

--创建label
function RichText:_newLabels()
	for i  = 1 , #self.strPiece do
		local str =  self.strPiece[i].str
		local color = converHexToC3b(self.strPiece[i].color)
		local cLine = self.strPiece[i].cLine

		-- local label = display.newTTFLabel({
  --   	text = str,
	 --    font = "jcy",
	 --    size = self.fontSize,
	 --    color = color, -- 使用纯红色
	 --    align = cc.TEXT_ALIGNMENT_LEFT,
	 --    valign = cc.VERTICAL_TEXT_ALIGNMENT_TOP,
		-- })
		local label = self:_getLabel(str,self.fontSize,color)
		label:setAnchorPoint(cc.p(0,0.5))
		self.labelTab[#self.labelTab + 1] = {label = label , cLine = cLine}
	end
	return self.labelTab
end

function  RichText:_getLabel(_str,_fontSize,_color)
	local str = _str or ""
	local fontSize = _fontSize or 20
	local color = _color or cc.c3b(255, 255, 255)

	if  #self.labelPool >= 1 then 

		local label =  self.labelPool[#self.labelPool]
		label:setString(str)--setString(text)
		label:setSystemFontSize(fontSize)
		label:setColor(color)
		self.labelPool[#self.labelPool] = nil
		return label

	else 
		local label = display.newTTFLabel({
    	text = str ,
	    --font = "ttf/jcy.TTF",
	    size = fontSize ,
	    color = color, -- 使用纯红色
	    align = cc.TEXT_ALIGNMENT_LEFT,
	    valign = cc.VERTICAL_TEXT_ALIGNMENT_TOP,
		})

		return label
	end
end

function  RichText:_putLabel(label)

	if label:getParent() then 
		label:retain()
		label:removeFromParent()
		self.labelPool[#self.labelPool + 1] = label
		label:release()
	else 
		self.labelPool[#self.labelPool + 1] = label
	end
end



--转化十六进制为c3b格式 HhexNum
function converHexToC3b(_hexNum)
	_hexNum = "0x" .. tostring(string.format("%06x",_hexNum))
	if string.len(_hexNum) ~= 8 then 
		print("颜色数据有问题")
	end
	local index = 2
	local nums = {}
	for i = 1 , 3 do 
		local hex = string.sub(_hexNum , index+i*2-1,index+i*2)
		hex = tonumber("0x" .. hex)
		nums[#nums + 1] = hex
	end

	return cc.c3b(nums[1], nums[2],nums[3])
end

--设置label的字体size
function RichText:setFontSize(_fontSize)  

	self:removeLabels()  
	--for i = 1,  #self.labelTab do
	--	self.labelTab[i]:setSystemFontSize(_fontSize)
	--end
	--self:updateLabelPos()
	self.testStr = ""
	self.nextLineChangeLine = false
	self:myInit(self.labelString,_fontSize,self.contentSize)
end

--获得label的字体size
function RichText:getFontSize(_fontSize)
	return self.fontSize
end

--设置文字
function RichText:setString(_richText)
	self:removeLabels()
	--setStirng()时候需要清楚上一次设置字符文字
	self.testStr = ""
	self.nextLineChangeLine = false
	self:myInit(_richText,self.fontSize,self.contentSize)
end

--获得内容的大小
function RichText:getConetentSize()
	local boundingbox =  self:getCascadeBoundingBox()
	return cc.size(boundingbox.width,boundingbox.height)
end

function RichText:setContentSize(_size)
	self:removeLabels()
	--setStirng()时候需要清楚上一次设置字符文字
	self.testStr = ""
	self.nextLineChangeLine = false
	self:myInit(self.labelString,self.fontSize,_size)
end

function  RichText:getTestLabelSize()
	local label = self:_getLabel("test",self.fontSize)
	local size = label:getContentSize()
	self:_putLabel(label)
	return size
end

return RichText