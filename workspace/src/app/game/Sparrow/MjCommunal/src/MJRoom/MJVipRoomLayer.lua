--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将VIP房
local StackLayer = require("app.hall.base.ui.StackLayer")

local MJHelper   = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local scheduler  = require("framework.scheduler")

local MJVipRoomLayer = class("MJVipRoomLayer", function()
    return StackLayer.new()
end)

function MJVipRoomLayer:ctor(__params)
	print("MJVipRoomLayer:ctor")
    --dump(__params, "__params", 9)
    self.__params = __params or {}
	MJHelper:getSkin():enterPor(self.__params.m_portalId)
    --cc.SpriteFrameCache:getInstance():addSpriteFrames("ui/p_mahjong_new_card.plist","ui/p_mahjong_new_card.png")
    --cc.SpriteFrameCache:getInstance():addSpriteFrames("ui/p_mahjong_new_card_1.plist","ui/p_mahjong_new_card_1.png")
	local t = cc.net.SocketTCP.getTime()
	self:myInit()
	
	-- local bg = display.newSprite("ui/mj_dt_loading.jpg")
	-- local size = bg:getContentSize()
	-- local s = math.max(display.width / size.width, display.height / size.height)
	-- bg:setScale(s)
	-- bg:setPosition(display.cx, display.cy)
	-- self:addChild(bg)
	local bg =  ccui.ImageView:create( "ui/mj_dt_loading.jpg" ,ccui.TextureResType.localType )
    bg:setScale9Enabled( true )
    bg:setCapInsets( cc.rect(150,150,150,150) )
    bg:ignoreContentAdaptWithSize( true )
    local size = bg:getContentSize()
    bg:setContentSize( cc.size( size.width * display.scaleX , size.height * display.scaleY) )
	bg:setPosition(display.cx, display.cy)
	self:addChild( bg )

	local girlImg =  ccui.ImageView:create( "ui/majiang_ren.png" ,ccui.TextureResType.localType )
	girlImg:setAnchorPoint( cc.p(0,0) )
	girlImg:setPosition(0, 0)
	self:addChild( girlImg )
	
	local loading = UIAdapter:createNode("csb/mahjong_loading.csb")
	self.loading = loading
	self:addChild(loading)
	self.loading:setPosition(cc.p(display.cx,100))
	self.loading_bar = loading:getChildByName("lb_loading")
	--self.img_fenge = loading:getChildByName("img_fenge")
	--local begin_x,y = self.img_fenge:getPosition()
	local sizeWidth = self.loading_bar:getContentSize().width
	self.node_pos = loading:getChildByName("node_pos")
	
	self.prosessLabel = loading:getChildByName("prosessLabel")
	self.prosessLabel:setString("")
	-- local label = cc.LabelAtlas:_create(string.format(";%d:<",0),"number/mj_txt_shuzi.png",28,31,48)
	-- self.node_pos:addChild(label)
	-- local size = label:getContentSize()
	-- label:setPositionY(-size.height/2)
	self.loading_bar:setPercent(0)

	local function loadProgress(per)
		if not self.init then return end 
		print(per)
		local dis = sizeWidth-25
		self.loading_bar:setPercent(per)
		--self.img_fenge:setPositionX(begin_x+((dis*per)/100))
		--label:setString(string.format(";%d:<",per))
		-- if per>=100 then
		-- 	self.img_fenge:setVisible(false)
		-- end
		self.prosessLabel:setString(per.."%")
	end
	local function loadBack()

		if not self.init then return end 

		self:setupViews()

		self:updateViews()

		self:onPlayerInfoChanged()
		self:onBtnStateChanged()
		self:updateComeBackRoom()
		self.view:setVisible(false)
		local function delay_show()
			self.view:setVisible(true)
			if MJHelper:getReconnet() then
				self.view:setVisible(false)
			end
			
			-- performWithDelay(self.view,function() 
			-- 	if MJHelper:getReconnet() then
			-- 		MJHelper:setReconnet(false)
			-- 	end
			-- 	self.view:setVisible(true)
			-- end,0.5)
			local function showHallLayer(  )
				if MJHelper:getReconnet() then
					MJHelper:setReconnet(false)
				end
				if not tolua.isnull( self.view ) then
			       self.view:setVisible(true)
			    end
			end 
		    scheduler.performWithDelayGlobal( showHallLayer , 1.0 )
			
			local action = cc.ScaleTo:create(0.15,1)
			self.layout_mid:setScale(0.5)
			self.layout_mid:runAction(action)
		   
			-- local p = self.aniPos.layout_top.originPos
			-- local m = self.aniPos.layout_top
			-- self.layout_top:setPositionY(p.y+m.dy)
			
			-- local action = cc.MoveTo:create(0.15,p)
			-- self.layout_top:runAction(action)
			
			performWithDelay(self.view,function()
				local MJResManager = MJHelper:loadInstanceClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJResManager")
				MJResManager:loadMJAsyncLater()
				MJResManager:loadMJArmatureAsync(self)
			end,0.15)	
		end
		--delay_show()
		performWithDelay(self.view,delay_show,0.05)
		performWithDelay(self.view,function() MJHelper:setReady(true)end,0.1)
		
		
	end

	local MJResManager = MJHelper:loadInstanceClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJResManager")
	MJResManager:loadMJAsync(loadBack,loadProgress)
	
end

-- 初始化成员变量
function MJVipRoomLayer:myInit(  )
	self.init = true
    addMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS, handler(self, self.onPlayerInfoChanged))
	addMsgCallBack(self, MSG_BAG_UPDATE_SUCCESS, handler(self, self.onPlayerInfoChanged))
	
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_VIP_SHOW_UI_ANI, handler(self, self.showLayer))
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_VIP_HIDE_UI_ANI, handler(self, self.hideLayer))
	
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_COME_BACK_BTN, handler(self, self.updateComeBackRoom))
	
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_FUNC_BTN_STATUS, handler(self, self.onBtnStateChanged))
	
    ToolKit:registDistructor( self, handler(self, self.onDestory) )
    
end

function MJVipRoomLayer:onDestory()
	self.init = false
	print("MJVipRoomLayer:onDestory()")
    -- 注销消息
    removeMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS)
	removeMsgCallBack(self, MSG_BAG_UPDATE_SUCCESS)
	removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_VIP_SHOW_UI_ANI)
	removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_VIP_HIDE_UI_ANI)
	removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_COME_BACK_BTN)
	removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_FUNC_BTN_STATUS)
end

function MJVipRoomLayer:runAniAction()
	local action = cc.ScaleTo:create(0.15,1)
	self.layout_mid:setScale(0.5)
	self.layout_mid:runAction(action)
	
	-- local p = self.aniPos.layout_top.originPos
	-- local m = self.aniPos.layout_top
	-- self.layout_top:setPositionY(p.y+m.dy)
	
	-- local action = cc.MoveTo:create(0.15,p)
	-- self.layout_top:runAction(action)
end

-- 初始化界面
function MJVipRoomLayer:setupViews()
    local view = UIAdapter:createNode("csb/mahjong_main_scene.csb")
	self.view = view
	self:addChild(view)
	

    UIAdapter:adapter(view, handler(self, self.onVipRoomCallBack))
	
	local bg = view:getChildByName("bg")
	local img = MJHelper:getSkin():getDtBg()
	bg:loadTexture(img)
	
	-- local title = view:getChildByName("title_img")
	-- local img = MJHelper:getSkin():getSceneLogoImg()
	-- title:loadTexture(img,1)
	
	--self.layout_top = view:getChildByName("layout_top")
	
	local layout_up = view:getChildByName("layout_up")
	local normalImg = "majiang_js_return.png"
	local selectImg = "majiang_js_return.png"
	local disableImg = "majiang_js_return.png"
	local btn_back = ccui.Button:create(normalImg,selectImg,disableImg,1)
	layout_up:addChild(btn_back)
    local btn = view:getChildByName("rank_btn")
	local x,y = btn:getPosition()
	btn_back:setAnchorPoint(cc.p(0,0.5))
	btn_back:setPosition(cc.p( x-display.width+150, y+50))
	btn_back:setName("btn_back")
	UIAdapter:registClickCallBack( btn_back, handler(self, self.onVipRoomCallBack))

	if GlobalConf.DEBUG_MODE then
		btn_back:setVisible(false)
	end
	
	self.btn_back = btn_back
	--self.btn_more = view:getChildByName("btn_more")
	--self.btn_more:setVisible(false)
		
	
	if not GlobalConf.DEBUG_MODE then
		--self.btn_moregame = view:getChildByName("btn_moregame")
		--self.btn_moregame:setVisible(false)
		
		--self.btn_daibi_manager = view:getChildByName("btn_daibi_manager")
		--self.btn_daibi_manager:setVisible(false)
	end
	
	local state = getFuncOpenStatus(GlobalDefine.MJ_FUNC_ID.DAIBI_MANAGER)
	if state ~=0 then
		--self.btn_daibi_manager = view:getChildByName("btn_daibi_manager")
		--self.btn_daibi_manager:setVisible(false)
	end
	
	--self.btn_moregame = view:getChildByName("btn_moregame")
	
	self.layout_mid = view:getChildByName("layout_mid")
	self.layout_down = view:getChildByName("layout_down")
	
	self.layout_up = view:getChildByName("layout_up")
    self.btn_create = view:getChildByName("btn_create")
    self.btn_join = view:getChildByName("btn_join")
	self.btn_free = view:getChildByName("btn_free")
	--self.btn_match = view:getChildByName("btn_match")
	
	self.img_come_back = view:getChildByName("img_come_back")
	self.room_tip_img = view:getChildByName("room_tip_img")
	self.rank_btn = view:getChildByName("rank_btn")
	self.layout_down_right = view:getChildByName("layout_down_right")
	
	self.aniPos= {}
	self.aniPos.layout_up={dx=0,dy=110}
	self.aniPos.layout_up.originPos = cc.p(self.layout_up:getPosition())
	
	self.aniPos.btn_create={dx=-1000,dy=0}
	self.aniPos.btn_create.originPos = cc.p(self.btn_create:getPosition())
	
	self.aniPos.btn_join={dx=-1000,dy=0}
	self.aniPos.btn_join.originPos = cc.p(self.btn_join:getPosition())
	
	self.aniPos.btn_free={dx=1000,dy=0}
	self.aniPos.btn_free.originPos = cc.p(self.btn_free:getPosition())
	
	-- self.aniPos.btn_match={dx=1000,dy=0}
	-- self.aniPos.btn_match.originPos = cc.p(self.btn_match:getPosition())
	
	-- self.aniPos.layout_top={dx=0,dy=110}
	-- self.aniPos.layout_top.originPos = cc.p(self.layout_top:getPosition())
	
	self:initHead(view)
    self.labelName = view:getChildByName("txt_name")
	
	self.labelName:setFontName("")

	--ID
	self.labelId = view:getChildByName("txt_ID_value")
	
    self.labelGoldNum = view:getChildByName("txt_gold_num")
    self.labelRoomCardNum = view:getChildByName("txt_roomcard_num")
	--self.labelDiamondNum = view:getChildByName("txt_diamond_num")
	
	
	------------------------------------------------------------------
--    local view = UIAdapter:createNode("csb/mahjong_main_scene_btn.csb")
--    local scene = display.getRunningScene()
--    scene:addChild(view,50)
--    UIAdapter:adapter(view, function()end)
	MJHelper:start(self)
	
	-- local MJGmCode = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.tool.MJGmCode")
	-- MJGmCode:start(self)
end

function MJVipRoomLayer:initHead( root )
	
	local img_head_bg = root:getChildByName("img_head_bg")
    local MJUIHelper = MJHelper:getUIHelper()
	local _img_head = MJUIHelper:initPlayerHead(img_head_bg,false,false,"majiang_tubiao_txk.png")
    self._img_head = _img_head
end

-- 设置数据
function MJVipRoomLayer:updateViews(  )

end

function MJVipRoomLayer:updateComeBackRoom()
	self.playerManager = MJHelper:getPlayerManager()	
	local id = self.playerManager:getVipRoomId()
	if id then  -- 返回
		if self.img_come_back then  
			self.img_come_back:loadTexture( "mj_cc_jrfj_fanhui.png" , 1)
			self.room_tip_img:loadTexture( "mj_cc_jrfj_fanhui1.png" , 1 )
		end
	else  -- 加入
		if self.img_come_back then
			self.img_come_back:loadTexture( "mj_cc_jrfj_jiaru.png" , 1)
			self.room_tip_img:loadTexture( "mj_cc_jrfj_jiaru1.png" , 1 )
		end
	end
end

function MJVipRoomLayer:showLayer(msg,isHaveAction)
	if self.view then
		self.view:setVisible(true)
		self.layout_mid:setVisible(true)
		self.layout_down:setVisible(true)
		--self.layout_top:setVisible(true)
		if not GlobalConf.DEBUG_MODE then
			--self.btn_moregame:setVisible(false)
			self.btn_back:setVisible(true)
		else
			--self.btn_moregame:setVisible(true)
			self.btn_back:setVisible(false)
		end
		self.rank_btn:setVisible(true)
		self.layout_down_right:setVisible( true )
		--self.btn_more:setVisible(true)
		self.layout_mid:setOpacity(255)	
		if isHaveAction then
			self:runAniAction()
		end	
	end
end

function MJVipRoomLayer:hideLayer(msg,isHideAll)
	if self.view then
		--self.btn_moregame:setVisible(false)

		self.layout_mid:setOpacity(255)
		local action = cc.FadeTo:create(0.2,0)
		self.layout_mid:runAction(action)
		self.rank_btn:setVisible(false)
		self.layout_down_right:setVisible( false )
		
		if isHideAll then
			self.btn_back:setVisible(false)
			--self.btn_more:setVisible(false)
			--self.layout_top:setVisible(false)
		else
			self.layout_down:setVisible(false)
		end
	end
end

function MJVipRoomLayer:onPlayerInfoChanged( )
	if not self.view then return end
    -- self.imgHead = root:getChildByName("img_head")
    GlobalDefine.ITEM_ID.DaiBiCard = 9998 
	local _fangkaNum = GlobalBagController:getCountByItemID(GlobalDefine.ITEM_ID.RoomCard)
	local _daiBiNum = GlobalBagController:getCountByItemID(GlobalDefine.ITEM_ID.DaiBiCard)	
    self.labelName:setString(Player:getNickName())
    self.labelId:setString( Player:getAccountID() )

    self.labelGoldNum:setString(Player:getGoldCoin())
    self.labelRoomCardNum:setString(_fangkaNum)
	--self.labelDiamondNum:setString(_daiBiNum)
	
    self._img_head:updateTexture(Player:getFaceID())
end

function MJVipRoomLayer:onBtnStateChanged()
	if not self.view then return end
    local MJUIHelper = MJHelper:getUIHelper()
	local conf = MJHelper:getConfig()
	
	-- MJUIHelper:setBtnStatusByFuncStatus(self.btn_create,conf.CCMJ_VIP_ROOM_NODE,handler(self, self.onVipRoomCallBack))
	-- MJUIHelper:setBtnStatusByFuncStatus(self.btn_join,conf.CCMJ_VIP_ROOM_NODE,handler(self, self.onVipRoomCallBack))
	-- MJUIHelper:setBtnStatusByFuncStatus(self.btn_free,conf.CCMJ_FREE_ROOM_NODE,handler(self, self.onVipRoomCallBack))
end

--[[
local function checkWx(callback)
	if MJ_SELF_UPDATE then
		callback()
	else
		if ToolKit:checkBindWx() or  device.platform == "windows"  then
			callback()
		else
			local function bindBack(code)
				if code == 0 then
					callback()
				elseif code == 1 then
					local message = "检测到此微信已有绑定账号，是否使用此微信号登录"
					local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
					local data = { message = message,leftStr="是",rightStr="否" }
					local dlg = MJDlgAlert.new()
					dlg:TowSubmitAlert(data, function ()
						ToolKit:loginByWx( function(m_code) if m_code== 0 then callback() end end )
					end)
					dlg:showDialog()
				end
			end 
			local message = "为了您更好的体验牌友房，请绑定微信"
			local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
			local data = { message = message,leftStr="打开微信",rightStr="取消",isGray=true }
			local dlg = MJDlgAlert.new()
			dlg:TowSubmitAlert(data, function ()
				ToolKit:bindingWx( bindBack)
			end)
			dlg:showDialog()
		end
	end
end
--]]

-- 点击事件
function MJVipRoomLayer:onVipRoomCallBack( sender )
    local name = sender:getName() 
    print("name: ", name)
    if name == "btn_create" then
		local function create_room()
			--local MJVipRoomCreateDlg =  MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJVipRoomCreateDlg")
			--local vipRoomCreateDlg = MJVipRoomCreateDlg.new()
			--vipRoomCreateDlg:showDialog()

			--local value = fromFunction(self.__params.m_portalId) or {name="长春麻将"}
			local playerManager = MJHelper:getPlayerManager()
			if playerManager:isHaveVipRoom() then
				TOAST("您已经创建了房间,不能再创建!")
			else
                local prodId = self.__params.m_portalId
				--local MJRoomSelect =  MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJRoomSelectDlg")
				local MJCreateRoomDlg =  MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJCreateRoomDlg")
				--传递麻将类型
				local mjCreateRoomDlg = MJCreateRoomDlg.new(prodId)
				mjCreateRoomDlg:showDialog()
			end
		end
		--checkWx(create_room)
    elseif name == "btn_join" then
    	local playerManager = MJHelper:getPlayerManager()
    	if playerManager:isHaveVipRoom() then
    		local roomId = playerManager:getVipRoomId()
    		sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "enter_vip_room", roomId)
    	else
			local function join_room()
				local MJVipRoomJoinDlg =  MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJVipRoomJoinDlg")
				local vipRoomJoinDlg = MJVipRoomJoinDlg.new()
				vipRoomJoinDlg:showDialog()
			end
			--checkWx(join_room)
    	end
    elseif name == "btn_back" then
		UIAdapter:popScene()    

	elseif name == "btn_moregame" then
		
		UIAdapter:popScene()
		--self:getScene():onBackButtonClicked()
    elseif name == "btn_room" then
		local MJVipRoomManagerDlg =  MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJVipRoomManagerDlg")
        local MJVipRoomManagerDlg = MJVipRoomManagerDlg.new()
        MJVipRoomManagerDlg:showDialog()
	elseif name == "btn_set" then
		local MJVipRoomSetDlg =  MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJVipRoomSetDlg")
        local Dlg = MJVipRoomSetDlg.new()
        Dlg:showDialog()
	elseif name == "btn_video" then
		local MJVipVideoManagerDlg =  MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJVipVideoManagerDlg")
        local Dlg = MJVipVideoManagerDlg.new()
        Dlg:showDialog()
	elseif name == "btn_add_gold" then
        sendMsg(PublicGameMsg.MSG_CCMJ_VIP_LOBBY_OPT, "btn_add_gold", {})
	elseif name == "btn_add_roomcard" then
        sendMsg(PublicGameMsg.MSG_CCMJ_VIP_LOBBY_OPT, "btn_add_roomcard", {})
    --代币管理入口
    elseif name == "btn_daibi_manager" then
        local MJCreateRoomData = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJDaiBiManagerDlg")
		local daibiManager = MJCreateRoomData.new()
		daibiManager:showDialog()
    elseif name == "btn_bag" then
        sendMsg(PublicGameMsg.MSG_CCMJ_VIP_LOBBY_OPT, "btn_bag", {})	
    elseif name == "btn_mail" then
        sendMsg(PublicGameMsg.MSG_CCMJ_VIP_LOBBY_OPT, "btn_mail", {})
	elseif name == "btn_bank" then
--		local MJVipRoomBankDlg =  MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJVipRoomBankDlg")
--        local Dlg = MJVipRoomBankDlg.new()
--        Dlg:showDialog()
        sendMsg(PublicGameMsg.MSG_CCMJ_VIP_LOBBY_OPT, "btn_bank", {})	
	elseif name == "btn_head" then
		sendMsg(PublicGameMsg.MSG_CCMJ_VIP_LOBBY_OPT, "btn_head", {})
	elseif name == "btn_more" then

		if not g_functionDlg then
			g_functionDlg = MJHelper:loadClass("app.Total.view.lobby.function.MJFunctionLayer").new( {funcId = 4002} )
			g_functionDlg:showDialog() 
		end

	elseif name == "btn_free" then
		local conf = require("app.game.Sparrow.MjCommunal.src.MJGameConf") -- 麻将配置文件 
		local msgObj = { id = conf.NodeFreeRoomId[self.__params.m_portalId], lcount = 1 }
		-- local msgObj = { id = 410000, lcount = 1 }
		sendMsg(PublicGameMsg.MSG_PUBLIC_ENTER_GAME, msgObj)
		MJHelper:getUIHelper():addWaitLayer()
	elseif name == "rank_btn" then
		--TOAST("显示排行榜")
		local conf = MJHelper:getConfig()
		local RankMainLayer = MJHelper:loadClass("app.hall.rank.view.RankMainLayer")
		local rankMainLayer = RankMainLayer.new({ gameId = conf.GENERALMJ_RANK_KIDE_ID , rankType = 1 , dataType = 1  })
		rankMainLayer:showDialog()
    elseif name == "viode_btn" then -- 回放
        local MJVipVideoManagerDlg = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJVipVideoManagerDlg")
		local m_MJVipVideoManagerDlg = MJVipVideoManagerDlg.new()
		m_MJVipVideoManagerDlg:showDialog()
	elseif name == "fight_btn" then -- 战绩
        local MJBattleHistoryDlg = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJBattleHistoryDlg")
		local m_MJBattleHistoryDlg = MJBattleHistoryDlg.new()
		m_MJBattleHistoryDlg:showDialog()
	elseif name == "btn_match" then
		TOAST("功能暂未开放")
    end
end

return MJVipRoomLayer
