--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将房间管理

local QkaDialog = require("app.hall.base.ui.CommonView")

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")


local RichText =  MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.RichText")

local DonateData = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJDonateData")

local HISTORY_TEMP = {
	[1] = {
		{
			str = "您推广的用户 %s 通过充值房卡，为您带来",
			color = 0x6E4B47
		},
		{
			str = "%d",
			color = 0xff0000
		},
		{
			str = "个元宝收益。",
			color = 0x6E4B47
		}	
	},
	[2] = {
		{
			str = "您通过下载游戏，获得了",
			color = 0x6E4B47
		},
		{
			str = "%d",
			color = 0xff0000
		},
		{
			str = "个元宝奖励。",
			color = 0x6E4B47
		}	
	},
	[3] =  {
		{
			str = "玩家%s通过玩游戏共产生了%d的服务费，您获得了",
			color = 0x6E4B47
		},
		{
			str = "%d",
			color = 0xff0000
		},
		{
			str = "个元宝收益。",
			color = 0x6E4B47
		}	
	},
	[4] = {
		{
			str = "您完成了%s局元宝房，获得了%d个元宝的收益，再玩%s局，还可以再获得",
			color = 0x6E4B47
		},
		{
			str = "%d",
			color = 0xff0000
		},
		{
			str = "个元宝哦！",
			color = 0x6E4B47
		}	
	},
	[104] = {             
		{
			str = "您完成了%s局元宝房，获得了%d个元宝的收益",
			color = 0x6E4B47
		},
	},
}

local MJDaiBiManager = class("MJDaiBiManager", function ()
    return QkaDialog.new()
end)

function MJDaiBiManager:ctor()
	self:myInit()

	self:setupViews()
end

function MJDaiBiManager:myInit()
	--发展人数
	self.connection_user = Player:getPromoteNum() 

	--获得代币数量
	local data = Player:getTokenInfo()
	self.today_get_num = data.m_nDayToken or 0	--今日获得代币
	self.all_get_num = data.m_nCalcToken  or 0		--累计获得代币

	GlobalDefine.ITEM_ID.DaiBiCard = 9998 
	self.daiBiNum = GlobalBagController:getCountByItemID(GlobalDefine.ITEM_ID.DaiBiCard)	--代币数量

	--玩家玩了多少局
    self.curRound = Player:getTokenRoomInning()
    --第一阶段要求达到局数
    self.round1 = DonateData:getRound1Limit()
    --第二阶段要求达到的局数
    self.round2 = DonateData:getRound2Limit()
    --达到第一阶段但是没有到第二阶段时限制预留元宝
    self.roundLimit = DonateData:getMinCoinLimit()

    addMsgCallBack(self, MSG_BAG_UPDATE_SUCCESS, handler(self, self.updateUserData))
    ToolKit:registDistructor( self, handler(self, self.onDestroy) )
end

function MJDaiBiManager:updateData()
		--发展人数
	self.connection_user = Player:getPromoteNum() 

	--获得代币数量
	local data = Player:getTokenInfo()
	self.today_get_num = data.m_nDayToken or 0	--今日获得代币
	self.all_get_num = data.m_nCalcToken  or 0		--累计获得代币

	GlobalDefine.ITEM_ID.DaiBiCard = 9998 
	self.daiBiNum = GlobalBagController:getCountByItemID(GlobalDefine.ITEM_ID.DaiBiCard)	--代币数量

	--玩家玩了多少局
    self.curRound = Player:getTokenRoomInning()

end

function MJDaiBiManager:updateUserData()
	self:updateData()
	self.txt_connection_user:setString(string.format("已发展玩家%d人",self.connection_user))
	self.txt_today_get_num:setString(self.today_get_num)
	self.txt_all_get_num:setString(self.all_get_num)
	self.txt_daibi_num:setString(self.daiBiNum)
end

function MJDaiBiManager:updateView()
	self:updateData()

	self.txt_connection_user:setString(string.format("已发展玩家%d人",self.connection_user))
	self.txt_today_get_num:setString(self.today_get_num)
	self.txt_all_get_num:setString(self.all_get_num)
	self.txt_daibi_num:setString(self.daiBiNum)
	self:checkRoundLimit()
end

function MJDaiBiManager:setupViews()
	self.root = UIAdapter:createNode("csb/mahjong_daibi_manager.csb")
    self:addChild(self.root)
    UIAdapter:adapter(self.root, handler(self, self.onCreateLayerCallBack))

    --玩家名称
    self.txt_name = self.root:getChildByName("txt_name")
    self.txt_name:setFontName("")

    --已发展玩家x人
    self.txt_connection_user = self.root:getChildByName("txt_connection_user")
    self.txt_connection_user:setString(string.format("已发展玩家%d人",self.connection_user))

    --今日获得
    self.txt_today_get_num = self.root:getChildByName("txt_today_get_num")
    self.txt_today_get_num:setString(self.today_get_num)

    --累计获得
    self.txt_all_get_num = self.root:getChildByName("txt_all_get_num")
    self.txt_all_get_num:setString(self.all_get_num)

    --玩家代币的数量
    self.txt_daibi_num = self.root:getChildByName("txt_daibi_num")
    self.txt_daibi_num:setString(self.daiBiNum)

    --玩家获得代币的具体信息容器
    self.list_content = self.root:getChildByName("list_content")

    --获得玩家头像/放置头像的img
    self.img_header = self.root:getChildByName("img_header")

   --查看已经发展的玩家信息
    self.btn_look_up = self.root:getChildByName("btn_look_up")
    self.txt_look_up = self.root:getChildByName("txt_look_up")
    self.txt_look_up:setVisible(false)
    self.btn_look_up:setVisible(false)

    --无列表时显示标志
    self.layout_no_room = self.root:getChildByName("label_no_list")
    self.layout_no_room:setVisible(false)

    --赠送条件提示
    self.txt_donate_tips = self.root:getChildByName("txt_donate_tips")
    self.txt_donate_tips:setString("")

    --赠送按钮
    self.btn_donate_coin = self.root:getChildByName("btn_donate_coin")
    self.btn_donate_coin:getChildByName("txt_donate_coin"):enableOutline(cc.c4b(149, 117, 29, 255), 3)
    self:checkRoundLimit()

    self.btn_get_coin = self.root:getChildByName("btn_get_coin")
    self.btn_get_coin:getChildByName("txt_get_coin"):enableOutline(cc.c4b(29, 149, 124, 255), 3)
    

    self.historyList = {}
    local dt = ToolKit:getTimeMs()
    function callBack(param)
    	self:updateView()
    	--如果没有返回数据，则返回
    	if not param then print("没有返回数据") return end

    	local list = param.m_vTokenHistory
    	if not list then
    	else
	    	for i = 1 , #list do 
	    		local item = {}
	    		item.m_strId = list[i].m_strId
	    		item.m_nType = list[i].m_nType
	    		item.m_nTime =list[i].m_nTime
	    		item.m_nNum	= list[i].m_nNum
	    		item.m_strJsonParam = json.decode(list[i].m_strJsonParam)
	    		table.insert(self.historyList,item)
	    	end
	    	self:updateList()

	    end
    end

    local info = 
	{
		req = "CS_C2H_LoadTokenHistory_Req",
		ack = "CS_H2C_LoadTokenHistory_Ack",
		dataTable = {}
	}

	local function get_history_callback(param,body)
		MJHelper:getUIHelper():removeWaitLayer()
		if not param then 
			print("服务器没有返回数据")
		end

		local t = ToolKit:getTimeMs()-dt
		if t<0.15 then
			performWithDelay(self,function() callBack(param) end,0.15-t)
		else
			callBack(param)
		end
	end
	MJHelper:getUIHelper():addWaitLayer(10,"正在获取数据...")
	MJBaseServer:sceneRpcSend(info,get_history_callback)


    self:initName()
    self:initHeader()

end

function MJDaiBiManager:updateList()
	local listNum = #self.historyList

	if listNum == 0 then
		self.layout_no_room:setVisible(true)
		return 
	end
	local index = 1
	function callback()
		if index > listNum then
			return 
		else
			self:addOneItem(index)
			index = index + 1
		end
		performWithDelay(self, callback ,0.2)
	end
	
	callback()
end

function MJDaiBiManager:addOneItem(index)
		--富文本
		local colorSize = 24
		local strTab = self:adaptData(self.historyList,index)
		local richLabel = RichText.new(strTab,colorSize,cc.size(550,100))
		local size = richLabel:getConetentSize()
		richLabel:setPosition(190,size.height - colorSize/2)

		--时间标签
		local timeStr  = os.date("%m月%d日 %H:%M",self.historyList[index].m_nTime)
		local timeLabel = display.newTTFLabel({text = timeStr , size = colorSize,color = cc.c3b(0x6e, 0x4b, 0x47)})
		--local timeLabel = display.newTTFLabel({text = timeStr , size = colorSize,font="ttf/jcy.TTF",color = cc.c3b(0x6e, 0x4b, 0x47)})
		timeLabel:setPosition(85,size.height - colorSize/2)

		--列表分界线
		local lineSpt = display.newSprite("#mj_line_1.png")
		lineSpt:setPositionY(-10)
		lineSpt:setScaleX(0.8)
		lineSpt:setAnchorPoint(cc.p(0,1))


		local custom_item = ccui.Layout:create()
		custom_item:setContentSize(cc.size(740,size.height + 20))  --20是间隙
		custom_item:addChild(timeLabel)
		custom_item:addChild(richLabel)
		custom_item:addChild(lineSpt)

		self.list_content:pushBackCustomItem(custom_item)
end

--对每一中类型的文字单独处理
function MJDaiBiManager:adaptData(data,index)
	local item = data[index]
	if item.m_nType == 1 then 
		local adaptStr = clone(HISTORY_TEMP[item.m_nType])
		adaptStr[1].str = string.format(adaptStr[1].str,item.m_strJsonParam.userName)
		adaptStr[2].str = string.format(adaptStr[2].str,item.m_nNum)
		return adaptStr
	elseif item.m_nType ==  2 then
		local adaptStr = clone(HISTORY_TEMP[item.m_nType])
		adaptStr[2].str = string.format(adaptStr[2].str,item.m_nNum)
		return adaptStr
	elseif item.m_nType ==  3 then
		local adaptStr = clone(HISTORY_TEMP[item.m_nType])
		adaptStr[1].str = string.format(adaptStr[1].str,item.m_strJsonParam.userName,item.m_strJsonParam.serviceCharge)
		adaptStr[2].str = string.format(adaptStr[2].str,item.m_nNum)
		return adaptStr
	elseif item.m_nType ==  4 and  item.m_strJsonParam.round2 then
		local adaptStr = clone(HISTORY_TEMP[item.m_nType])
		adaptStr[1].str = string.format(adaptStr[1].str,item.m_strJsonParam.round1,item.m_nNum,item.m_strJsonParam.round2)
		adaptStr[2].str = string.format(adaptStr[2].str,item.m_strJsonParam.newGet)
		return adaptStr
	elseif item.m_nType ==  4 then
		local adaptStr = clone(HISTORY_TEMP[item.m_nType + 100])
		adaptStr[1].str = string.format(adaptStr[1].str,item.m_strJsonParam.round1,item.m_nNum)
		return adaptStr
	end
end

function MJDaiBiManager:initHeader()
    local MJUIHelper = MJHelper:getUIHelper()
	local _img_head = MJUIHelper:initPlayerHead(self.img_header , true , true) 
	--真正的头像   
    self._img_head = _img_head
    self._img_head:updateTexture(Player:getFaceID())
end

--初始化玩家名字
function MJDaiBiManager:initName()
	self.txt_name:setString(Player:getNickName())
end


function MJDaiBiManager:onDestroy()
		removeMsgCallBack(self, MSG_BAG_UPDATE_SUCCESS)
end

function MJDaiBiManager:checkRoundLimit()
    if  self.curRound < self.round1 then  --没有达到条件
         self.btn_donate_coin:setEnable(false)
         self.btn_donate_coin:getChildByName("txt_donate_coin"):disableEffect()
         self.txt_donate_tips:setString(string.format("再完成%d局且大于%d元宝可用",self.round1 - self.curRound,self.roundLimit))
    elseif  self.curRound >= self.round1 and self.curRound < self.round2 then  --达到第一阶段
    	self.btn_donate_coin:setEnable(true)
    	self.btn_donate_coin:getChildByName("txt_donate_coin"):enableOutline(cc.c4b(149, 117, 29, 255), 3)
    elseif self.curRound <= self.round2 then  --达到第二阶段
    	self.btn_donate_coin:setEnable(true)
    	self.btn_donate_coin:getChildByName("txt_donate_coin"):enableOutline(cc.c4b(149, 117, 29, 255), 3)

    end

end

function MJDaiBiManager:onCreateLayerCallBack(sender)
		local name = sender:getName()
		if name == "btn_donate_coin"then 
			local MJDaiBiDonateDlg = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJDaiBiDonateDlg")
			local daiBiDonate = MJDaiBiDonateDlg.new()
			daiBiDonate:showDialog()

		elseif name == "btn_get_coin"then 
			local MJDaiBiGetDlg = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJDaiBiGetDlg")
			local daiBiGet = MJDaiBiGetDlg.new()
			daiBiGet:showDialog()

		elseif name == "btn_look_up"then 
			local MJDaiBiLookupPlayerDlg = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJDaiBiLookupPlayerDlg")
			local lookupPlayer = MJDaiBiLookupPlayerDlg.new()
			lookupPlayer:showDialog()
			
		elseif name == "btn_close" then
			self:closeDialog()
   		end
end


return MJDaiBiManager