--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将牌友房解散对话框

local QkaDialog = require("app.hall.base.ui.CommonView")
local scheduler = require("framework.scheduler")

local MJVipRoomDisbandDlg = class("MJVipRoomDisbandDlg", function ()
    return QkaDialog.new()
end)

function MJVipRoomDisbandDlg:ctor(ctl)
	self.ctl = ctl

	self:myInit()

	self:setupViews()

	self:updateViews()

	ToolKit:registDistructor( self, handler(self, self.onDestory) )

	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_DISBAND, handler(self, self.updateViews))

end

function MJVipRoomDisbandDlg:myInit()
	-- body
	self.pTimer = nil
end

function MJVipRoomDisbandDlg:onDestory()
    print("MJVipRoomDisbandDlg:onDestory()")
    removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_UPDATE_DISBAND)

    if self.pTimer then
	    scheduler.unscheduleGlobal(self.pTimer)
		self.pTimer = nil
	end
end

function MJVipRoomDisbandDlg:setupViews()
	self.root = UIAdapter:createNode("csb/ccmj_vip_room_disband.csb")
    self:addChild(self.root)
    UIAdapter:adapter(self.root, handler(self, self.onDibandDlgCallBack))

    self.label_time = self.root:getChildByName("label_time")

    self.layout = {}
    for i = 1, 4 do
    	self.layout[i] = self.root:getChildByName("layout_" .. i)
    end
end

function MJVipRoomDisbandDlg:updateViews()
	-- 倒计时
	local time = self.ctl:getDisbanTime()
	if time > 0 then
		self.label_time:setString("投票倒计时：" .. time)
		if self.pTimer == nil then
			self.pTimer = scheduler.scheduleGlobal(function ()
				local time = self.ctl:getDisbanTime()
				self.label_time:setString("投票倒计时：" .. time)
				if time <= 0 then
					scheduler.unscheduleGlobal(self.pTimer)
					self.pTimer = nil
				end
			end, 1)
		end
	else
		self.label_time:setString("投票倒计时：" .. 0)
	end
	-- 玩家&投票信息
	local playerInfos = self.ctl:getPlayerInfos()
	for i = 1, 4 do
		local label_vote = self.layout[i]:getChildByName("label_vote")
		label_vote:setString("")

		local label_name = self.layout[i]:getChildByName("label_name")
		label_name:setString("")
		label_name:setFontName("")

		if playerInfos[i] then
			-- 昵称
			local name = playerInfos[i]:getNickname()
			label_name:setString(name)
			-- 同意/不同意
			local opinion = playerInfos[i]:getDisbandOpinion()
			if opinion == 0 then
				label_vote:setString("")
			elseif opinion == 1 then
				label_vote:setString("同意")
			elseif opinion == 2 then
				label_vote:setString("不同意")
			end
		end
    end
end

function MJVipRoomDisbandDlg:onDibandDlgCallBack( sender )
	local name = sender:getName()
	-- 解散房间投票
    if name == "btn_vote_yes" then
        sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "dis_room_vote", 1)
    elseif name == "btn_vote_no" then
        sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "dis_room_vote", 2)
    end
end

return MJVipRoomDisbandDlg