--
-- Author: 
-- Date: 2018-08-07 18:17:10
--长春麻将规则说明

local QkaDialog = require("app.hall.base.ui.CommonView")
local MJHelper  = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local MJConf = MJHelper:getConfig()		-- 麻将配置文件 

local MJHelpDlg = class("MJHelpDlg",function ()
	return QkaDialog.new()
end)

function MJHelpDlg:ctor()
	self:myInit()

	self:setUpViews()
end

function MJHelpDlg:myInit()
	--传递游戏类型
	local mjSkin = MJHelper:getSkin()
	local mjId = mjSkin:getPor()
	local platform_newFun = fromLua("MilShowNodeCfg")
	local gameName = platform_newFun[mjId].name

	self.mjGameNode = mjId
	self.mjTypeName = gameName .. "玩法"

	ToolKit:addSearchPath("src/app/game/Sparrow/GeneralMJ/res")

	self.mjPath = {
		[MJConf.CCMJ_GAME_NODE] = "app.game.Sparrow.MjCommunal.src.MJConfig.MJ_ccmj_rule",
		[MJConf.TDH_GAME_NODE] = "app.game.Sparrow.MjCommunal.src.MJConfig.MJ_tdh_rule"
	}

	self.rootBtnList = {}
	self.leafIndex = 0
	self.leafBtnList = {}
	self.leafLabel = {}

	self.playerManager = MJHelper:getPlayerManager()
end


function MJHelpDlg:setUpViews()
	self.root = UIAdapter:createNode("csb/mahjong_help_dlg.csb")
	self:addChild(self.root)
	UIAdapter:adapter(self.root,handler(self,self.onHelpCallBack))

	self:setCloseCallback(handler(self,self.onDestroy))

	self.imgBg = self.root:getChildByName("layout_bg3") 
	self.btnRule1  = self.root:getChildByName("btn_leaf_rule_1")
	self.btnRule2  = self.root:getChildByName("btn_leaf_rule_2")
	self.btnRule3  = self.root:getChildByName("btn_leaf_rule_3")
	
	self.textRule1 = self.root:getChildByName("text_rule_1")
	self.textRule2 = self.root:getChildByName("text_rule_2")
	self.textRule3 = self.root:getChildByName("text_rule_3")

	self.btnRule1:setCascadeOpacityEnabled(false)
	self.btnRule2:setCascadeOpacityEnabled(false)
	self.btnRule3:setCascadeOpacityEnabled(false)

	for i = 1 , 3 do
		self.leafBtnList[i] = self["btnRule" .. i]
	end

	for i = 1 , 3 do
		self.leafLabel[i] = self["textRule" .. i]
	end
	--麻将类型文字
	self.textType = self.root:getChildByName("txt_mj_type")

	--self.listRootRule = self.root:getChildByName("list_root_role")
    self.listView = self.root:getChildByName("list_help")
	self:setUpRootRule()
end

function MJHelpDlg:setUpRootRule()
	-- for i = 1 , #self.rootRuleConf do
	-- 	local rootRule = UIAdapter:createNode("csb/mahjong_checkbox_item.csb")
	-- 	UIAdapter:adapter(rootRule,handler(self,self.onHelpCallBack))
	-- 	local checkbox = rootRule:getChildByName("btn_root_rule")
	-- 	rootRule:getChildByName("text_root_rule"):setString(self.rootRuleConf[i].text)
	-- 	checkbox.rootIndex = i
	-- 	self.rootBtnList[i] = checkbox
	-- 	checkbox:addEventListener(handler(self,self.onRootClicked))

		
	-- 	local custom_item = ccui.Layout:create()
	-- 	custom_item:setContentSize(cc.size(230,90))
	-- 	custom_item:addChild(rootRule)

	-- 	self.listRootRule:pushBackCustomItem(custom_item)
	-- end

	--默认选中第一项
	-- local gameAtomTypeId = self.playerManager:getGameAtomTypeId()
	-- self.roomData = RoomData:getRoomDataById(gameAtomTypeId) -- 获取房间数据
	-- self.gameKindType = self.roomData.gameKindType
	
	-- if MJConf.CCMJ_GAME_KIND_TYPE == gameKindType then  --长春麻将
	-- 	--self:onRootClicked(self.rootBtnList[CCMJ_GAME_TYPE],nil)
	-- 	self.rootIndex = MJConf.CCMJ_GAME_KIND_TYPE
	-- 	self:onRootClicked()
		
	-- elseif MJConf.TDH_GAME_KIND_TYPE == gameKindType then   --推倒胡
	-- 	--self:onRootClicked(self.rootBtnList[TDH_GAME_TYPE],nil)
	-- 	self.rootIndex = MJConf.TDH_GAME_KIND_TYPE
	-- 	self:onRootClicked()
	-- end

	--self.rootIndex = self.gameKindType
	self:onRootClicked()

	--更新麻将玩法
	--local mjName = self.mjTypeName[self.gameKindType]
	local mjName = self.mjTypeName
	self.textType:setString(mjName)
	
end


function MJHelpDlg:onRootClicked()
	--记录rootIndex
	--self.rootIndex = item.rootIndex

	-- self.rootBtnList[item.rootIndex]:setSelected(true)
	-- self.rootBtnList[item.rootIndex]:getChildByName("img_rule_arrow"):setVisible(false)
	-- self.rootBtnList[item.rootIndex]:getChildByName("text_root_rule"):setColor(cc.c3b(255, 255, 255))
	-- self.rootIndex = item.rootIndex

	-- for i = 1 , #self.rootRuleConf do 
	-- 	if i ~= self.rootIndex then 
	-- 		self.rootBtnList[i]:setSelected(false)
	-- 		self.rootBtnList[i]:getChildByName("img_rule_arrow"):setVisible(false)
	-- 		self.rootBtnList[i]:getChildByName("text_root_rule"):setColor(cc.c3b(249, 84, 82))
	-- 	end
	-- end
	
	self:onLeafClicked(1)
end

function MJHelpDlg:onLeafClicked(index)
	self.leafIndex = index

	for i = 1 , 3 do
		if self.leafIndex == i then
			self.leafBtnList[i]:setOpacity(255)
			self.leafLabel[i]:setColor(cc.c3b(255,255,255))
			self.leafLabel[i]:enableOutline(cc.c4b(29, 149, 124, 255), 2)
		else
			self.leafBtnList[i]:setOpacity(0)
			self.leafLabel[i]:setColor(cc.c3b(68, 99, 154))
			self.leafLabel[i]:disableEffect()
		end
	end

	self:updateViews()
end


function MJHelpDlg:updateViews()
	--移除掉之前的items
	self.listView:removeAllItems()
	local helpData = nil
	if self.mjPath[self.mjGameNode] then 
		helpData = MJHelper:loadClass(self.mjPath[self.mjGameNode])
	end
	
	if helpData == nil then
		print("没有规则数据")
		return 
	end
	self.dataNum = #helpData
	self.dataIndex = 0

	local function showHelpItems()
  		self.dataIndex = self.dataIndex + 1

  		if self.dataIndex > self.dataNum then
  			return 
  		end

  		local v = helpData[self.dataIndex]
  		if v.type == self.leafIndex then
  			self:addOneHelpItem(v)
  		else
  			showHelpItems()
  		end
	end

	self:addNodeEventListener(cc.NODE_ENTER_FRAME_EVENT,function() showHelpItems() end )
	self:scheduleUpdate()

end

function MJHelpDlg:addOneHelpItem(v)
	local tips = nil
	local data = nil
	local dataType = nil
	if v then
		tips = self:getCardDataTip(v)
		data , dataType  = self:getCardData(v)
	end

	local MJUIHelper = MJHelper:getUIHelper()
	local node = nil      --牌型的节点
	local layoutBaseHeight = 0  --layout节点高度等于当前的基本高度 + label的高度
	local singleLabel = 0   -- layout是否是单一的文字，没有牌型
	if data.tHandCards and data.tWeaves and (next(data.tHandCards) or next(data.tWeaves)) then 
		node = MJUIHelper:createCardAndWeave(data)
		layoutBaseHeight = node:getCascadeBoundingBox().height
	 	singleLabel = 1
	end

	local size = self.imgBg:getContentSize()
	
	local listView = self.root:getChildByName("list_help")

    -- 测试label 用于测试文字是否放不下一行
	local testLabel = display.newTTFLabel({
		text = tips,
		size = 24,
		font = "ttf/jcy.TTF",
		})
	local lineHeight = testLabel:getContentSize().height

	local lineNum = nil   -- 文字的行数
	-- if testLabel:getContentSize().width > (size.width - 50) then 
	-- 	lineNum = 2
	-- else
	-- 	lineNum = 1
	-- end
	local labelWidth = testLabel:getContentSize().width
	local lineWidth = size.width - 50
	lineNum = math.ceil(labelWidth/lineWidth)

	local label = display.newTTFLabel({
		text = tips,
		size = 24,
		font = "ttf/jcy.TTF",
		color = cc.c3b(68, 99, 154),
		align = cc.TEXT_ALIGNMENT_LEFT,
    	dimensions = cc.size(size.width - 50,(lineHeight + 10) * lineNum )
		}):pos(20,(layoutBaseHeight) * singleLabel + lineHeight * lineNum + 30)
	label:setAnchorPoint(cc.p(0,1))

	local custom_item = ccui.Layout:create()
	custom_item:setContentSize(cc.size(size.width - 50,(layoutBaseHeight) * singleLabel + lineHeight * lineNum + 30))
	if node ~= nil then 
		node:setPosition(20,20)
		custom_item:addChild(node)
	end
	custom_item:addChild(label)

	listView:pushBackCustomItem(custom_item)

end

function MJHelpDlg:getCardDataTip(rawData)
	-- body
	local tips = nil
	if rawData.info then
		tips = rawData.info[1].tips
	end 
	return tips
end

--配置数据转化成创建麻将需要的数据
function MJHelpDlg:getCardData(rawData)
	 local data = {}

	 if rawData.info and rawData.info[2] and rawData.info[2].weave then 
	 	local i , j , k  = 0,0,0
	 	  for i = 1 , #rawData.info[2].weave do 
	 	  	if data.tWeaves  == nil then
	 	  		data.tWeaves = {}
	 	  	end
			data.tWeaves[i] = {}

	 	  	if rawData.info[2].weave[i] and rawData.info[2].weave[i][1] then
	 	  		data.tWeaves[i].m_iAcCode = rawData.info[2].weave[i][1]
	 	  	end

			data.tWeaves[i].m_iAcCard = {}
	 	  	data.tWeaves[i].m_iNums = {}

	 	  	for j = 2,#rawData.info[2].weave[i] do 	 	  		
 	  			if rawData.info[2].weave[i][j][1] then
 	  				table.insert(data.tWeaves[i].m_iAcCard,rawData.info[2].weave[i][j][1])
 	  			end
 	  			if rawData.info[2].weave[i][j][2] then 	  				
 	  				table.insert(data.tWeaves[i].m_iNums, rawData.info[2].weave[i][j][2])
 	  			end		 		 
	 	  	end
	 	  end
	 else
	 	data.tWeaves = {}
	 end

	 if rawData.info and rawData.info[3] and rawData.info[3].hand then 
	 	if data.tHandCards  == nil then
	 	  	data.tHandCards = {}
	 	end
	 	for  m = 1, #rawData.info[3].hand do 
	 		table.insert(data.tHandCards,rawData.info[3].hand[m])
	 	end
	 else 
	 	data.tHandCards = {}
	 end


	 if rawData.info and rawData.info[3] and rawData.info[3].ch then  
	 	data.pCurCard = rawData.info[3].ch
	 else
	 	data.pCurCard = nil
	 end

	 return data , rawData.type
end


function MJHelpDlg:onHelpCallBack(sender)
	-- body
	local name = sender:getName()
	if name == "btn_close" then 
		self:closeDialog()
	elseif name == "btn_leaf_rule_1" then
		self:onLeafClicked(1)
		--self.leafIndex = 1
	elseif name == "btn_leaf_rule_2" then 
		self:onLeafClicked(2)
		--self.leafIndex = 2
	elseif name == "btn_leaf_rule_3" then 
		self:onLeafClicked(3)
		--self.leafIndex = 3
	end

end

function MJHelpDlg:setCloseCallback()
	-- body
	
end

return MJHelpDlg