--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 代币房查看好友界面

local QkaDialog = require("app.hall.base.ui.CommonView")
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
	
local MJDaiBiLookupPlayer = class("MJDaiBiLookupPlayer", function ()
    return QkaDialog.new()
end)

function MJDaiBiLookupPlayer:ctor()
	self:myInit()

	self:setupViews()
end

function MJDaiBiLookupPlayer:myInit()
	self.playerNum = 25
	
end

function MJDaiBiLookupPlayer:setupViews()
	-- body
	self.root = UIAdapter:createNode("csb/mahjong_daibi_lookup_player.csb")
    self:addChild(self.root)
    UIAdapter:adapter(self.root, handler(self, self.onCreateLayerCallBack)) 

    self.list_player = self.root:getChildByName("list_player")
  
	self:initPlayerList()
end

function MJDaiBiLookupPlayer:initPlayerList()
	local index = math.ceil(self.playerNum / 4)

	for i = 1 , index do 
		local playerItems = UIAdapter:createNode("csb/mahjong_lookup_item.csb")
		local size = playerItems:getCascadeBoundingBox()

		local custom_item = ccui.Layout:create()
		custom_item:setContentSize(size)
		custom_item:addChild(playerItems)

		self.list_player:pushBackCustomItem(custom_item)
	end
	
end

function MJDaiBiLookupPlayer:onCreateLayerCallBack( sender )
	local name = sender:getName() 
    if name == "btn_close" then

		self:closeDialog()
	end
	
end

return MJDaiBiLookupPlayer