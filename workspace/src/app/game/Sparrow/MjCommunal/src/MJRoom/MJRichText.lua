--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 单行富文本类
local MJRichText = class("MJRichText",function() 
	return ccui.Layout:create()
end)

-- 这是一段测试文本
--<color = 0xffffff>玩家 xxxx 在今日比赛中获得了</color><color = 0xffffff>99</color>个代币
function MJRichText:ctor(_richText)
	self:myInit(_richText)
end

--初始化
function MJRichText:myInit(_richText)
	self:initData(_richText)
	local strData = self:trimStr(self.labelString)
	self:_newLabels(strData)
	self:linkLabels()  
end

--初始化数据
function MJRichText:initData(_richText)
	self.labelString = _richText or ""

	self.labelTab = {}   --label数组清空
	self.fontSize = self.fontSize or 30
end

--从父节点中移除label
function MJRichText:removeLabels()
	for i = 1, #self.labelTab do 
		self.labelTab[i]:removeFromParent()
	end
end

--label链接起来
function MJRichText:linkLabels()
	if #self.labelTab == 0 then
		return 
	end

	self:updateLabelPos()

	for i = 1 , #self.labelTab do
		self:addChild(self.labelTab[i])
	end
	
end

--更新label位置
function MJRichText:updateLabelPos()
	if #self.labelTab == 0 then
		return 
	end
	for i = 1 , #self.labelTab do
		local pos = nil
		local x , y = 0 , 0
		if self.labelTab[i - 1] ~= nil then
			x , y = self.labelTab[i-1]:getPosition()

			size = self.labelTab[i-1]:getContentSize()
			
		else
			x,y  = 0,0
			size = cc.size(0,0)
		end

		 self.labelTab[i]:setPosition(x + size.width,y)
	end
end

--从字符串中切出用用数据段
function MJRichText:trimStr(_richText)
	local strTab = {}
	local strData = {}
	for word in string.gmatch(_richText, "%<%s*color%s*=%s*%w+%>%s*[^%<]*%<%s*%/color%s*%>") do
   	 	strTab[#strTab + 1] = word
	end
	 for i = 1 , #strTab do 
	 	local color , str = string.match(strTab[i], "%<%s*color%s*=%s*(%w+)%>%s*([^%<]*)%<%s*%/color%s*%>")
	 	if strData[i] == nil then
	 		strData[i] = {}
	 	end
	 	strData[i].color = color
	 	strData[i].str = str
	 end
	return strData
end

--创建label
function MJRichText:_newLabels(strData)
	
	for i  = 1 , #strData do
		local str =  strData[i].str
		local color = converHexToC3b(strData[i].color)

		local label = display.newTTFLabel({
    	text = str,
	    font = "jcy",
	    size = self.fontSize,
	    color = color, -- 使用纯红色
	    align = cc.TEXT_ALIGNMENT_LEFT,
	    valign = cc.VERTICAL_TEXT_ALIGNMENT_TOP,
		})
		label:setAnchorPoint(cc.p(0,0.5))
		self.labelTab[#self.labelTab + 1] = label
	end
	return self.labelTab
end

--转化十六进制为c3b格式
function converHexToC3b(_hexNum)
	if string.len(_hexNum) ~= 8 then 
		print("颜色数据有问题")
	end
	local index = 2
	local nums = {}
	for i = 1 , 3 do 
		local hex = string.sub(_hexNum , index+i*2-1,index+i*2)
		hex = tonumber("0x" .. hex)
		nums[#nums + 1] = hex
	end

	return cc.c3b(nums[1], nums[2],nums[3])
end

--设置label的字体size
function MJRichText:setFontSize(_fontSize)
	self.fontSize = _fontSize

	for i = 1,  #self.labelTab do
		self.labelTab[i]:setSystemFontSize(_fontSize)
	end
	self:updateLabelPos()
end

--获得label的字体size
function MJRichText:getFontSize(_fontSize)
	return self.fontSize
end

--设置文字
function MJRichText:setString(_richText)
	self:removeLabels()
	self:myInit(_richText)
end

--获得内容的大小
function MJRichText:getConetentSize()

	local boundingbox =  self:getCascadeBoundingBox()
	return cc.size(boundingbox.width,boundingbox.height)
end

function MJRichText:setConetentSize(_size)
	local width = _size.width
	local height = _size.height
	self:setLayoutSize(width, height)
	return 
end

return MJRichText