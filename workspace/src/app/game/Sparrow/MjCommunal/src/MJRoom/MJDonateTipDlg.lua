--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将房间管理

local QkaDialog = require("app.hall.base.ui.CommonView")

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local RichText = require("app.game.Sparrow.MjCommunal.src.MJRoom.RichText")

local MJDonateTipDlg = class("MJDonateTipDlg", function ()
    return QkaDialog.new("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
end)

function MJDonateTipDlg:ctor(data)
	self:myInit()
    self:setData(data)
	self:setupViews()
end

function MJDonateTipDlg:myInit()
	self:setTouchEnabled(true)
end

function MJDonateTipDlg:setData(data)
	self.data = data

end

function MJDonateTipDlg:setupViews()
	self.root = UIAdapter:createNode("csb/mahjong_donate_tip.csb")
    self:addChild(self.root)
    UIAdapter:adapter(self.root, handler(self, self.onCreateLayerCallBack))

    local txt_content = self.root:getChildByName("txt_content")
    txt_content:setString("")
    local x,y = txt_content:getPosition()
    local str = {
     {
         str = "确定赠送",
         color = 0x6E4B47
     },
     {
         str = "%d",
         color = 0xff0000
     },
     {
         str = "个元宝给\n",
         color = 0x6E4B47
     },
     {
         str = "%s（id:%d)",
         color = 0xff0000
     },
      {
         str = "?",
         color = 0x6E4B47
     }

    }
    str[2].str = string.format(str[2].str,self.data.m_itemNum)
    str[4].str = string.format(str[4].str,self.data.m_toNickname,self.data.m_toPlayer)
    local richLabel = RichText.new(str,32,cc.size(400,200))

    local size = richLabel:getConetentSize()
    richLabel:setPosition(0,size.height / 2)
    txt_content:addChild(richLabel)

   -- self.txt_content = self.root:getChildByName("txt_content")
    --self.txt_content:setString(string.format("确定赠送%d个代币给\n%s（id:%d)?",self.data.m_itemNum,self.data.m_toNickname,self.data.m_toPlayer))

end

function MJDonateTipDlg:onDestroy()
	
end
function MJDonateTipDlg:onCreateLayerCallBack(sender)
		local name = sender:getName()
		if name == "btn_confirm"then 
			self:onRequireConfirm()
            --self:closeDialog()
        elseif name == "btn_think" then
            self:closeDialog()
		elseif name == "btn_close" then
			self:closeDialog()
   		end
end

--请求捐赠确认
function MJDonateTipDlg:onRequireConfirm()

    local dt = ToolKit:getTimeMs()
    function callBack(param)
      	self:onResponseConfirm(param)
    end

    local info = 
    {
        req = "CS_C2H_DonateItemConfirm_Req",
        ack = "CS_H2C_DonateItemConfirm_Ack",
        dataTable = {self.data.m_toPlayer,self.data.m_itemId,self.data.m_itemNum}
    }

    local function get_history_callback(param,body)
        --MJHelper:getUIHelper():removeWaitLayer()
        if not param then 
            print("服务器没有返回数据")
        end

        local t = ToolKit:getTimeMs()-dt
        if t<0.15 then
            performWithDelay(self,function() callBack(param) end,0.15-t)
        else
            callBack(param)
        end
    end
    --MJHelper:getUIHelper():addWaitLayer(10,"正在获取数据...")
    MJBaseServer:sceneRpcSend(info,get_history_callback)

end

function MJDonateTipDlg:onResponseConfirm(param)
	if not param then return end
    if param.m_result == 0 then
        TOAST("赠送成功")
        self:closeDialog()
    else
        print("请求赠送返回错误码: ",param.m_result)
        TOAST("赠送失败")
    end
   
end


return MJDonateTipDlg