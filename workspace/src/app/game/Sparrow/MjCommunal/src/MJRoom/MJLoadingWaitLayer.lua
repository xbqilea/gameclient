--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 麻將加載界面

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

--添加本节点之后，2秒后弹文字  10秒之后移除本节点
local ADD_TIP_TIME = 2
local REMOVE_LAYER_TIME = 5
local layerInstanceIndex = 0
local LOAD_LAYER_ZORDER = 0xffffffe
local DEFAULT_STRING = "正在加载中..."
local MJLoadingWaitLayer = class("MJLoadingWaitLayer",function()
	return cc.Layer:create()
end)

function MJLoadingWaitLayer:ctor (time , str,showNow)
	self.time = time
	self.str = str
	self.showNow = showNow
	self:myinit()
	self:setupViews()

	ToolKit:registDistructor( self, handler(self, self.onDestory))
	addMsgCallBack( self, PublicGameMsg.MSG_CCMJ_HIDE_LOADING_WAIT_LAYER, handler(self,self.onRemoveSelf) )
end

function MJLoadingWaitLayer:myinit ( )

	--点击回调函数
    self:setTouchEnabled(true)
	local function onTouch(event,x,y)
		return true
	end
	self:registerScriptTouchHandler(onTouch)
	
	if self.showNow then
		self:addTips()
	else	
		self:performWithDelay(handler(self,self.addTips),ADD_TIP_TIME)
	end

 	--时间到了移除本节点
    self:performWithDelay(handler(self,self.onRemoveSelf),self.time or REMOVE_LAYER_TIME)
	
end

function MJLoadingWaitLayer:setupViews( )
	
end

--加载提示语
function MJLoadingWaitLayer:addTips( ... )

	local img_bg_layer = display.newColorLayer(cc.c4b(0, 0, 0, 120))
	self:addChild(img_bg_layer)
	
	local label_tip = display.newTTFLabel({
    text = self.str or DEFAULT_STRING ,
    font = "ttf/jcy.TTF",
    size = 30,
    color = cc.c3b(255, 255, 255), -- 使用白色文字
    align = cc.TEXT_ALIGNMENT_LEFT,
    valign = cc.VERTICAL_TEXT_ALIGNMENT_TOP,
	})
	label_tip:setAnchorPoint(cc.p(0.5,0.5))
	img_bg_layer:addChild(label_tip,10)

	local size = label_tip:getContentSize()
    
	
	local MJAnimation = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJAnimation")
	self.sprite_ = MJAnimation:playArmature("hall_loading","",nil,true)
	

    local spriteSize = self.sprite_:getContentSize()
	
    label_tip:setPosition(display.cx ,display.cy-55 )
    self.sprite_:setPosition(display.cx ,display.cy-82)

	img_bg_layer:addChild(self.sprite_,9)
end

function MJLoadingWaitLayer:showSelf( ... )
	local scene = display.getRunningScene()
	if scene then
		scene:addChild(self, LOAD_LAYER_ZORDER)
	else
		print("没有合适Scene存在")
	end

end

function MJLoadingWaitLayer:onRemoveSelf()
	self:removeFromParent(true)
end

function MJLoadingWaitLayer:onDestory(  )
	removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_HIDE_LOADING_WAIT_LAYER)
end


function MJLoadingWaitLayer:onExit( ... )

end

return MJLoadingWaitLayer
