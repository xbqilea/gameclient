--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 长春麻将创建代币房

local QkaDialog = require("app.hall.base.ui.CommonView")
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local MJCreateRoomData = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJCreateRoomData")
local MJDaiBiRoomCreateDlg = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJDaiBiRoomCreateDlg")
local MJPaiYouRoomCreateDlg = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJPaiYouRoomCreateDlg")
local MJConf = MJHelper:getConfig()    -- 麻将配置文件 

local PAI_YOU_ROOM = 0
local YUAN_BAO_ROOM = 1
local ANI_TIME = 0.2

local MJCreateRoomDlg = class("MJCreateRoomDlg", function ()
    return QkaDialog.new()
end)


function MJCreateRoomDlg:ctor(id)
	--记录当前的麻将类型
	self._gameTypeId = id

	self:myInit()

	self:setupViews()

end

function MJCreateRoomDlg:myInit()

	--默认选择牌友房 1 牌友  2 元宝
	self.rootRoomType = YUAN_BAO_ROOM
	    										
	self.roomCreateDlg = nil

	self.playerManager = MJHelper:getPlayerManager()
end


function MJCreateRoomDlg:setupViews()
	-- body
	self.root = UIAdapter:createNode("csb/mahjong_create_room.csb")
    self:addChild(self.root)
    UIAdapter:adapter(self.root, handler(self, self.onCreateLayerCallBack))

    --创建按钮
    self.btn_create = self.root:getChildByName("btn_create")
	self.btn_create:getChildByName("txt_create"):enableOutline(cc.c4b(29, 149, 124, 255), 3)

    --切换游戏房间按钮 spirte text
   -- self.btn_switch_room_type = self.root:getChildByName("btn_switch_room_type")
    --self.spt_ani_left = self.root:getChildByName("spt_ani_left")
    --self.spt_ani_right = self.root:getChildByName("spt_ani_right")

    --元宝房views
     self.node_yuan_bao = self.root:getChildByName("node_yuan_bao")
     self.node_yuan_bao:setVisible(false)
    --牌友放views
     self.node_pai_you = self.root:getChildByName("node_pai_you")
     self.node_pai_you:setVisible(false)

    --self.yuanBaoCreateDlg = MJDaiBiRoomCreateDlg.new(self._gameTypeId,self.root,self)
	--self.paiYouCreateDlg = MJPaiYouRoomCreateDlg.new(self._gameTypeId,self.root,self)
	
    self:switchRoomType()

	-- local state = getFuncOpenStatus(GlobalDefine.MJ_FUNC_ID.DAIBI_CREATE)
 --    if  state ==0  then 
 --    	self.btn_switch_room_type:setVisible(true)
 --    	self.spt_ani_left:setVisible(true)
 --    	self.spt_ani_right:setVisible(true)
 --    else
    	--self.btn_switch_room_type:setVisible(false)
    	--self.spt_ani_left:setVisible(false)
    	--self.spt_ani_right:setVisible(false)
 --    end

end

function MJCreateRoomDlg:switchRoomType()

	self.rootRoomType = self.rootRoomType + 1
	self.rootRoomType = self.rootRoomType % 2
	-- local index = self.rootRoomType  + 1
	if self.rootRoomType == PAI_YOU_ROOM then 
		self.node_yuan_bao:setVisible(false)
		self.node_pai_you:setVisible(true)

        if self.node_pai_you:getChildrenCount() == 0 then
            local node = UIAdapter:createNode("csb/mahjong_create_room_paiyou.csb")
            UIAdapter:adapter(node, handler(self, self.onCreateLayerCallBack))
            self.node_pai_you:addChild(node)
        end

        if not self.paiYouCreateDlg then 
            self.paiYouCreateDlg = MJPaiYouRoomCreateDlg.new(self._gameTypeId,self.root,self)
        end
         self.roomCreateDlg = self.paiYouCreateDlg
		
	else
		self.node_yuan_bao:setVisible(true)
		self.node_pai_you:setVisible(false)

        if self.node_yuan_bao:getChildrenCount() == 0 then
      
            local node = UIAdapter:createNode("csb/mahjong_create_room_yuanbao.csb")
            UIAdapter:adapter(node, handler(self, self.onCreateLayerCallBack))
            self.node_yuan_bao:addChild(node)
        end

        if not self.yuanBaoCreateDlg then 
        
            self.yuanBaoCreateDlg = MJDaiBiRoomCreateDlg.new(self._gameTypeId,self.root,self)
        end
		self.roomCreateDlg = self.yuanBaoCreateDlg
	end

	--self:switchBtn()
	self.roomCreateDlg:setGameNode(self._gameTypeId) --传递类型
	self.roomCreateDlg:showRootBtn()
	self.roomCreateDlg:checkCreateBtn()

end

function MJCreateRoomDlg:setGameNode(_gameNode)
	self._gameTypeId  = _gameNode or self._gameTypeId
end

function MJCreateRoomDlg:switchBtn()
	if self.rootRoomType == YUAN_BAO_ROOM then 
        self:switchAnimation()
	else
        self:switchAnimation()
	end
end

function MJCreateRoomDlg:switchAnimation()
    local scale20 = cc.ScaleTo:create(ANI_TIME, 0 , 1)
    local scale21 = cc.ScaleTo:create(ANI_TIME, 1 , 1)
    local callback = cc.CallFunc:create(handler(self,self.changeFrame))
    local ease0 = cc.EaseSineOut:create(scale20)
    local ease1 = cc.EaseSineIn:create(scale21)
    local action20 = cc.Sequence:create(ease0,callback,ease1)
    action20:retain()
    local callback_1 = function(act)  
        self.btn_switch_room_type:runAction(act) 
     end

    self:performWithDelay(function() 
        callback_1(action20)
        action20:release()
    end,ANI_TIME)

    local llittleMove0 = cc.MoveBy:create(0.15,cc.p(30,0))
    local llittleMove1 = cc.MoveBy:create(0.15,cc.p(-30,0))
    local lfadeOut20 = cc.FadeOut:create(ANI_TIME)
    local lfadeIn21  = cc.FadeIn:create(ANI_TIME)
    local lmove20   = cc.MoveBy:create(ANI_TIME,cc.p(-150,0))
    local lmove21   = cc.MoveBy:create(ANI_TIME,cc.p(150,0))
    local lspawn1   = cc.Spawn:create(lfadeOut20,lmove20)
    local lspawn2  = cc.Spawn:create(lfadeIn21,lmove21)
    local ldelay   = cc.DelayTime:create(ANI_TIME*1.5)
    local laction20 = cc.Sequence:create(llittleMove0,lspawn1,ldelay,lspawn2,llittleMove1)
    local lactiont = cc.EaseSineOut:create(laction20)
    --self.spt_ani_left:runAction(lactiont)

    local rlittleMove0 = cc.MoveBy:create(0.15,cc.p(-30,0))
    local rlittleMove1 = cc.MoveBy:create(0.15,cc.p(30,0))
    local rfadeOut20 = cc.FadeOut:create(ANI_TIME)
    local rfadeIn21  = cc.FadeIn:create(ANI_TIME)
    local rmove20   = cc.MoveBy:create(ANI_TIME,cc.p(150,0))
    local rmove21   = cc.MoveBy:create(ANI_TIME,cc.p(-150,0))
    local rspawn1   = cc.Spawn:create(rfadeOut20,rmove20)
    local rspawn2  = cc.Spawn:create(rfadeIn21,rmove21)
    local rdelay   = cc.DelayTime:create(ANI_TIME*1.5)
    local raction20 = cc.Sequence:create(rlittleMove0,rspawn1,rdelay,rspawn2,rlittleMove1)
    local ractiont = cc.EaseSineOut:create(raction20)
    self.spt_ani_right:runAction(ractiont)
end

function MJCreateRoomDlg:changeFrame()
    if self.rootRoomType == YUAN_BAO_ROOM then 
       -- self.btn_switch_room_type:loadTextures("mj_cj_btn_card.png","mj_cj_btn_card.png","mj_cj_btn_card.png",1)
    else
       -- self.btn_switch_room_type:loadTextures("mj_cj_btn_yuanbao.png","mj_cj_btn_yuanbao.png","mj_cj_btn_yuanbao.png",1)
    end
end


--点击回掉 穿件代币房间
function MJCreateRoomDlg:onCreateLayerCallBack( sender )
	local name = sender:getName() 
   print("name: ", name)
    if name == "btn_create" then
    	
    	if self.rootRoomType == YUAN_BAO_ROOM  then 
    		if self.playerManager:isHaveVipRoom() then 
    			TOAST("你正在其他房间，不能创建元宝房")
    		else
    			self.roomCreateDlg:onCreateRoom()
    		end
    	else
    		self.roomCreateDlg:onCreateRoom()
    	end
	elseif name == "btn_switch_room_type" then
		self:switchRoomType()
	elseif name == "btn_close" then
		self:closeDialog()
    end
end

return MJCreateRoomDlg