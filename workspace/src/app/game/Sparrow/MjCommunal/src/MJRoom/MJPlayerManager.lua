--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 房间玩家数据管理类
local json = require(cc.PACKAGE_NAME .. ".json")
local MJPlayerInfo = require("app.game.Sparrow.MjCommunal.src.MJRoom.MJPlayerInfo")
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local MJConf = MJHelper:getConfig()

local MJPlayerManager = class("MJPlayerManager")

local UserDefaultClickKey = "MahjongClickKey"
local UserDefaultHelpCreateKey = "MahjongHelpCreateKey"
local UserDefaultCreateRoomSelectKey = "MahjongCreateRoomSelectKey"
local UserDefaultCoinRoomSelectKey  = "UserDefaultCoinRoomSelectKey"

function MJPlayerManager:ctor()
	print("MJPlayerManager:ctor()")
	self:myInit()
end

-- 初始化数据
function MJPlayerManager:myInit()
	self.m_gameAtomTypeId = nil 	-- 游戏最小配置类型ID
	self.m_roomId = nil 				-- 房间ID
	
	self.m_vipRoomId = nil 				-- VIP房间ID
	
	self.m_roomState = 0				--房间状态
	self.m_isVipRoom = nil				--是否牌友房
	
	self.m_roundNum = 2					--圈数

	self.m_playerInfos = {} 				-- 桌子玩家数据

	self.m_myRoomListInfo = {}			--	我的房间列表信息
	self.m_myRoomList = {} 				-- 我的房间列表

	-- 解散
	self.m_timecountdown = 0 			-- 解散倒计时
	self.m_timestamp = 0 				-- 获得倒计时的时间戳
	self.m_disbandState = MJDef.eDisbandState.none
	
	self.m_roomCreatorId = 0			--创间创建者的ID
	self.m_roomHost = 0				--房间的房主ID
	
	self.m_is_help_create_room = false
	
	local defautl_v = 2
	if device.platform == "windows" then
		defautl_v = 1
	end
	self.userDefault = cc.UserDefault:getInstance()
	local click = self.userDefault:getIntegerForKey(UserDefaultClickKey,defautl_v)

	self.m_click_type = click
	
	local helpCreate = self.userDefault:getIntegerForKey(UserDefaultHelpCreateKey,0)
	self.m_is_help_create = helpCreate==1
	
	local res = self.userDefault:getStringForKey(UserDefaultCreateRoomSelectKey, v)
	if res and string.len(res) >0 then
		self.m_createRoomSelect = json.decode(res)
	end

	local res = self.userDefault:getStringForKey(UserDefaultCoinRoomSelectKey, v)
	if res and string.len(res) >0 then
		self.m_coinRoomSelect = json.decode(res)
	end
	self.userDefault:flush()
end

function MJPlayerManager:setRoomInitData(data)
	self.m_initData = data
	local num = self.m_initData.m_nMaxRound
	if self.m_gameAtomTypeId == MJConf.CCMJ_VIPROOM_GAME_ID then 
		num  = self.m_initData.m_nMaxRound * self.m_initData.m_nPlayerCount
	end
	
	self:setRoundNum(num)
end

function MJPlayerManager:getRoomInitData()
	return self.m_initData
end

function MJPlayerManager:setRoundNum(num)
	num = num or 2
	self.m_roundNum = num	
end

function MJPlayerManager:getRoundNum()
	
	return self.m_roundNum or 2	
end

-- 创建房间选项记录
function MJPlayerManager:setCreateRoomSelect(roundIndex,play_type_list)
	self.m_createRoomSelect = self.m_createRoomSelect or {}
	self.m_createRoomSelect.roundIndex = roundIndex
	self.m_createRoomSelect.play_type_list = play_type_list
	
	local v = json.encode(self.m_createRoomSelect)
	self.userDefault:setStringForKey(UserDefaultCreateRoomSelectKey, v)
	self.userDefault:flush()
end

function MJPlayerManager:getCreateRoomSelect()
	return self.m_createRoomSelect
end

-- 单击出牌或双击出牌
function MJPlayerManager:setClickType(num)
	num = num or 1
	if self.m_click_type ~= num then
		self.m_click_type = num
		self.userDefault:setIntegerForKey(UserDefaultClickKey, num)
		self.userDefault:flush()
	end
end	

--是否单击出牌
function MJPlayerManager:isOneClickOutCard()
	return self.m_click_type == 1
end

-- 单击出牌或双击出牌
function MJPlayerManager:setHelpCreate(bool)
	
	if self.m_is_help_create ~= bool then
		local num = 0
		if bool then
			num = 1
		end
		num = num or 1

		self.userDefault:setIntegerForKey(UserDefaultHelpCreateKey, num)
		self.userDefault:flush()
	end
	self.m_is_help_create = bool
end	

--是否单击出牌
function MJPlayerManager:isHelpCreate()
	return self.m_is_help_create
end

-- 设置游戏最小配置类型ID
function MJPlayerManager:setGameAtomTypeId( gameAtomTypeId )
	print("MJPlayerManager:setGameAtomTypeId : ", gameAtomTypeId)
    self.m_gameAtomTypeId = gameAtomTypeId
end

-- 获取游戏最小配置类型ID
function MJPlayerManager:getGameAtomTypeId()
	print("MJPlayerManager:getGameAtomTypeId : ", self.m_gameAtomTypeId)
	return self.m_gameAtomTypeId
end

function MJPlayerManager:setVipRoom(bool)
	self.m_isVipRoom = bool
end

function MJPlayerManager:isVipRoom()
	return self.m_isVipRoom
end

function MJPlayerManager:isFreeRoom()
	return not self.m_isVipRoom
end

-- 设置VIP房间ID
function MJPlayerManager:setVipRoomId( roomId )
	if roomId and roomId > 0x7FFFFFFF then
		self.m_vipRoomId = nil
	else
		self.m_vipRoomId = roomId
	end
end

-- 获取VIP房间ID
function MJPlayerManager:getVipRoomId()
	return self.m_vipRoomId
end


-- 设置房间ID
function MJPlayerManager:setRoomId( roomId )
	if roomId and roomId > 0x7FFFFFFF then
		self.m_roomId = nil
	else
		self.m_roomId = roomId
	end
end

-- 获取房间ID
function MJPlayerManager:getRoomId( roomId )
	return self.m_roomId
end

-- 设置房间ID
function MJPlayerManager:setRoomState( state )
	self.m_roomState = state
end

-- 获取房间ID
function MJPlayerManager:getRoomState()
	return self.m_roomState
end

-- 设置我的房间列表
function MJPlayerManager:setRoomListInfo( roomListInfo )
	self.m_myRoomListInfo = roomListInfo
	
	self:setRoomList(roomListInfo.m_arr_nRoomIdList)
end

function MJPlayerManager:getRoomListInfo()
	return self.m_myRoomListInfo
end

-- 设置我的房间列表
function MJPlayerManager:setRoomList( roomList )
	self.m_myRoomList = roomList
end

function MJPlayerManager:getRoomList()
	return self.m_myRoomList
end

-- 获取玩家列表
function MJPlayerManager:getPlayerInfos()
	return self.m_playerInfos
end

function MJPlayerManager:isThisRoomOwner(player)
	local uid = player:getAccountId()
	if self.m_roomHost == uid then
		return true
	end
end

function MJPlayerManager:isCurRoomCreater()
	local uid = Player:getAccountID()
	if self.m_roomCreatorId == uid then
		return true
	end
end

function MJPlayerManager:setHelpCreateRoom(bool)
	self.m_is_help_create_room = bool
end

function MJPlayerManager:isHelpCreateRoom()
	return self.m_is_help_create_room
end

function MJPlayerManager:setRoomCreater(uid)
	 self.m_roomCreatorId = uid 
end

function MJPlayerManager:setRoomHoster(uid)
	 self.m_roomHost = uid 
end

function MJPlayerManager:getRoomHoster(uid)
	return self.m_roomHost 
end

-- 更新玩家信息
function MJPlayerManager:updatePlayerInfo( data ,isVideo)
	for k, v in pairs(data.m_playerInfo) do
		local b_playerInfo = self:getPlayerInfoByAid(v.m_accountId)

		local playerInfo = self:getPlayerInfoByCid(v.m_chairId)
		if not playerInfo then 
			if  b_playerInfo then
				playerInfo = b_playerInfo
			end	
		else 
			if  b_playerInfo and b_playerInfo~=playerInfo then
				self:removePlayerInfoByAid( v.m_accountId )
			end
		end

		-- dump(playerInfo)
		if playerInfo == nil then
			playerInfo = MJPlayerInfo.new()
			table.insert(self.m_playerInfos, playerInfo)
		end
		if not isVideo then
			if v.m_accountId==Player:getAccountID() then
				v.m_faceId = Player:getFaceID()
			end
		end
		playerInfo:updateFromServ(v)
        --self.m_roomCreatorId = v.m_nRoomctorId
	end
	self.m_roomCreatorId = data.m_roomCreatorId
	self.m_roomHost = data.m_roomHost
	if data.m_nIsForOthers and data.m_nIsForOthers>0 then 
		self.m_is_help_create_room = true
	else
		self.m_is_help_create_room = false	
	end
end

-- 根据玩家ID获取玩家信息
function MJPlayerManager:getPlayerInfoByAid( id )
	local playerInfo = nil
	-- dump(self.m_playerInfos)
	for k, v in pairs(self.m_playerInfos) do
		if v:getAccountId() == id then
			playerInfo = v
			break
		end
	end
	return playerInfo
end

-- 根据玩家ID删除玩家信息
function MJPlayerManager:removePlayerInfoByAid( id )
	for i = #self.m_playerInfos, 1, -1 do
		local v = self.m_playerInfos[i]
		local aid = v:getAccountId()
		if aid == id then
			table.remove(self.m_playerInfos, i)
		end
	end
	sendMsg(PublicGameMsg.MSG_CCMJ_UPDATE_PLAYERINFO, self)
end

-- 根据椅子ID获取玩家信息
function MJPlayerManager:getPlayerInfoByCid( id )
	local playerInfo = nil
	-- dump(self.m_playerInfos)
	for k, v in pairs(self.m_playerInfos) do
		if v:getChairId() == id then
			playerInfo = v
			break
		end
	end
	return playerInfo
end

-- 设置解散状态
function MJPlayerManager:setDisbanState( state )
	self.m_disbandState = state
	-- print(self.m_disbandState)
	-- print(debug.traceback())
end

-- 获取解散状态
function MJPlayerManager:getDisbanState()
	return self.m_disbandState
end

-- 设置解散倒计时
function MJPlayerManager:setDisbandTime( time )
	self.m_timecountdown = time
	self.m_timestamp = os.time()
end

-- 获取倒计时
function MJPlayerManager:getDisbanTime()
	return tonumber(self.m_timecountdown - (os.time() - self.m_timestamp))
end

-- 解散发起人
function MJPlayerManager:setDissVoteRaiser( aid )
	self.m_raiser = aid
end

function MJPlayerManager:getDissVoteRaiser()
	return self.m_raiser
end

-- 玩家当前是否有房间
function MJPlayerManager:isHaveVipRoom()
	return self.m_vipRoomId ~= nil
end

-- 玩家当前是否有房间
function MJPlayerManager:isHaveRoom()
	return self.m_roomId ~= nil
end

-- 玩家当前所在房间是否是房主
function MJPlayerManager:isCurRoomOwner()
	local uid = Player:getAccountID()
	if self.m_roomHost == uid then
		return true
	end
end


-- 清除房间数据
function MJPlayerManager:clearRoomInfo()
	print("MJPlayerManager:clearRoomInfo")
	self.m_playerInfos = {} 				-- 桌子玩家数据
	self.m_roomId = nil
	self.m_roomState = 0
	self.m_vipRoomId = nil
	
	self.m_isVipRoom = nil				--是否牌友房
	
	-- 解散
	self.m_timecountdown = 0 			-- 解散倒计时
	self.m_timestamp = 0 				-- 获得倒计时的时间戳
end

--设置代币房数据
function MJPlayerManager:setCoinRoomSelect(roundIndex,roundIndex2,play_type_list,isHelp,gameId,playerNum)
	self.m_coinRoomSelect = self.m_coinRoomSelect or {}
	self.m_coinRoomSelect[gameId] = self.m_coinRoomSelect[gameId] or {}
	self.m_coinRoomSelect[gameId].roundIndex = roundIndex or self.m_coinRoomSelect[gameId].roundIndex
	self.m_coinRoomSelect[gameId].roundIndex2 = roundIndex2 or self.m_coinRoomSelect[gameId].roundIndex2
	self.m_coinRoomSelect[gameId].play_type_list = play_type_list
	self.m_coinRoomSelect[gameId].isHelp = isHelp
	self.m_coinRoomSelect[gameId].playerNum = playerNum
	
	local v = json.encode(self.m_coinRoomSelect)
	self.userDefault:setStringForKey(UserDefaultCoinRoomSelectKey, v)
	self.userDefault:flush()
end

--获取代币房玩法选项
function MJPlayerManager:getCoinRoomSelectById(gameId)
	return self.m_coinRoomSelect[gameId]
end

--得到代币房局数
function MJPlayerManager:getCoinRoomRoundById(gameId)
	if self.m_coinRoomSelect and self.m_coinRoomSelect[gameId] then 
		return self.m_coinRoomSelect[gameId].roundIndex
	else 
		return false
	end
end

--得到代币房局数
function MJPlayerManager:getCoinRoomRound2ById(gameId)
	if self.m_coinRoomSelect and self.m_coinRoomSelect[gameId] then 
		return self.m_coinRoomSelect[gameId].roundIndex2
	else 
		return false
	end
end

--得到代币是否吃
function MJPlayerManager:getCoinRoomPlayOptById(gameId)
	if self.m_coinRoomSelect and self.m_coinRoomSelect[gameId] then 
		return self.m_coinRoomSelect[gameId].play_type_list
	else
		return false
	end
end

--得到是否帮别人开房
function MJPlayerManager:getCoinRoomIsHelpById(gameId)
	if self.m_coinRoomSelect and self.m_coinRoomSelect[gameId] then 
		return self.m_coinRoomSelect[gameId].isHelp
	else
		return false
	end
end

--得到创建房间的人数
function MJPlayerManager:getCoinRoomPlayerNumById(gameId)
	if self.m_coinRoomSelect and self.m_coinRoomSelect[gameId] then 
		return self.m_coinRoomSelect[gameId].playerNum
	else
		return false
	end
end

function MJPlayerManager:getCardBGType()
	return 2
end

--获得当前所在房间是用局还是用圈  1 -- 圈 2 -- 局
function MJPlayerManager:getRoundUnit()
	local roomInfo = self:getRoomInitData()	
	if self.m_gameAtomTypeId == MJConf.CCMJ_VIPROOM_GAME_ID then 
		if roomInfo.m_nPlayerCount == 2 then 
			return 2
		else
			return 1 
		end
	end
	return 2
end

return MJPlayerManager
