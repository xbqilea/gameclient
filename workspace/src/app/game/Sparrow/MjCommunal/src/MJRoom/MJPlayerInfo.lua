--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 房间玩家信息

local MJPlayerInfo = class("MJPlayerInfo")

function MJPlayerInfo:ctor()
	self:myInit()
end

-- 初始化数据
function MJPlayerInfo:myInit()
	self.m_accountId		= 	0 				-- '玩家账户ID'
	self.m_chairId          = 	0 				-- '椅子号,对应游戏内的椅子,在房间内则为玩家索引0~玩家个数'
	self.m_level			= 	0 				-- '等级'
	self.m_faceId			= 	0 				-- '头像ID'
	self.m_nickname			= 	"" 				-- '昵称'
	self.m_score			= 	0 				-- '积分，金币，房卡等代名词'
	self.m_ready            = 	0     			-- '准备的对应值 0未 1已'
	self.m_rechargingState  =   0				-- "充值代币状态 0未 1需要充值" 
	self.m_bCanKick		=	0				-- '0：不能踢；1：可以被踢'
	self.m_disbandOpinion 	= 	MJDef.eDisbandVote.none 				-- 0:没投票, 1:同意, 2:不同意

	self.m_nState = 0 			-- 状态，1:等待，2游戏中，3：离开，4：离线	
	self.m_sex = 0 				-- 1男，0女

	self.m_shakeCd = 0 			-- 震他CD
	self.m_shakeCdStamp = 0 	-- 时间戳
	self.m_ip               = "" --玩家ip   
end

-- 更新数据
function MJPlayerInfo:updateFromServ( data )
	self.m_accountId = data.m_accountId or self.m_accountId 				
	self.m_chairId = data.m_chairId or self.m_chairId 				
	self.m_level = data.m_level or self.m_level 				
	self.m_faceId = data.m_faceId or self.m_faceId 				
	self.m_nickname = data.m_nickname or self.m_nickname 				
	self.m_score = data.m_score or self.m_score 
	self.m_bCanKick = data.m_bCanKick or self.m_bCanKick
	
	self.m_ready = data.m_ready or self.m_ready	
	
	self.m_rechargingState = data.m_rechargingState or self.m_rechargingState	

	self.m_sex = data.m_sex or self.m_sex

	self.m_ip = data.m_ip or self.m_ip
	
	if self.m_faceId == 0 then
		self.m_faceId = 1
	end
end

function MJPlayerInfo:getGender()
	return self.m_sex
end

function MJPlayerInfo:getAccountId()
	return self.m_accountId
end

function MJPlayerInfo:getChairId()
	return self.m_chairId
end

function MJPlayerInfo:getLevel()
	return self.m_level
end

function MJPlayerInfo:getFaceId()
	return self.m_faceId
end

function MJPlayerInfo:getNickname()
	return self.m_nickname
end

function MJPlayerInfo:setScore( score )
	self.m_score = score
end

function MJPlayerInfo:getScore()
	return self.m_score
end

function MJPlayerInfo:setDisbandOpinion( opinion )
	self.m_disbandOpinion = opinion
end
function MJPlayerInfo:getDisbandOpinion()
	return self.m_disbandOpinion
end

function MJPlayerInfo:setReady( ready )
	self.m_ready = ready
end
function MJPlayerInfo:getReady()
	return self.m_ready
end

function MJPlayerInfo:setRechargingState( state )
	self.m_rechargingState = state or 0
end
function MJPlayerInfo:getRechargingState()
	return self.m_rechargingState
end

function MJPlayerInfo:setCanKick( b )
	self.m_bCanKick = b
end

function MJPlayerInfo:getCanKick()
	return self.m_bCanKick
end

-- 玩家状态
function MJPlayerInfo:setState( state )
	self.m_nState = state or self.m_nState
end

function MJPlayerInfo:getState()
	return self.m_nState
end

function MJPlayerInfo:isLeaveState()
	return self.m_nState==3
end

function MJPlayerInfo:isUnlinkState()
	return self.m_nState==4
end

-- 震他CD
function MJPlayerInfo:setShakeCD( time )
	self.m_shakeCd = time
	self.m_shakeCdStamp = os.time()
end

function MJPlayerInfo:getShakeCD()
	return self.m_shakeCd - (os.time() - self.m_shakeCdStamp)
end


function MJPlayerInfo:getIp( )
	return self.m_ip
end

function MJPlayerInfo:setIp( _ip )
	self.m_ip = _ip
end
return MJPlayerInfo
