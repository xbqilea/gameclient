--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 加入房间 - 键盘弹窗


local QkaDialog = require("app.hall.base.ui.CommonView")

local MJVipRoomJoinDlg = class("MJVipRoomJoinDlg", function ()
    return QkaDialog.new()
end)

function MJVipRoomJoinDlg:ctor()
	self:myInit()

	self:setupViews()

	self:updateViews()
end

function MJVipRoomJoinDlg:myInit()
	-- body
	self.nums = {}
	
	ToolKit:registDistructor( self,handler(self, self.onDestroy))
	addMsgCallBack(self, PublicGameMsg.MSG_CCMJ_CLOSE_JOIN_DLG, handler(self, self.onClose)) 
end

function MJVipRoomJoinDlg:setupViews()
	self.root = UIAdapter:createNode("csb/mahjong_room_join.csb")
    self:addChild(self.root)
    UIAdapter:adapter(self.root, handler(self, self.onJoinDlgCallBack))

    self.labels = {}
    for i = 1, 6 do
    	self.labels[i] = self.root:getChildByName("label_" .. i)
    end
	self.root:getChildByName("txt_room_tip"):disableEffect()
	
end

function MJVipRoomJoinDlg:onDestroy()
	removeMsgCallBack(self, PublicGameMsg.MSG_CCMJ_CLOSE_JOIN_DLG)
end

function MJVipRoomJoinDlg:onClose()
	self:closeDialog()
end

function MJVipRoomJoinDlg:updateViews()
	for i = 1, 6 do
		if self.nums[i] then
    		self.labels[i]:setString(self.nums[i])
    	else
    		self.labels[i]:setString("")
    	end
    end
end

function MJVipRoomJoinDlg:onJoinDlgCallBack( sender )
	local name = sender:getName()
	if name == "btn_reset" then
		self.nums = {}
		self:updateViews()

	elseif name == "btn_delete" then
		table.remove(self.nums, #self.nums)
		self:updateViews()
	elseif name == "btn_close" then
		self:closeDialog()
	else
		local num = string.match(name, "btn_(%d)")
		if #self.nums < 6 then
			table.insert(self.nums, num)
			self:updateViews()
			if #self.nums == 6 then
				-- 请求加入
				performWithDelay(self,function()
				local roomId = tonumber(table.concat( self.nums, ""))
				sendMsg(PublicGameMsg.MSG_CCMJ_VIP_ROOM_OPT, "enter_vip_room", roomId)

				self.nums = {}
				self:updateViews()
				end,0.2)
			end
		end
	end
end

return MJVipRoomJoinDlg