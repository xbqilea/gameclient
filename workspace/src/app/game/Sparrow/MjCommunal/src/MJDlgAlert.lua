--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 通用提示框
local QkaDialog = require("app.hall.base.ui.CommonView")

local MJDlgAlert = class("MJDlgAlert", function ()
    return QkaDialog.new()
end)

function MJDlgAlert:ctor()
    self:init()

    self:setupViews()
end

function MJDlgAlert:init()
    self._singleBtnCallBack = nil 			-- 单个按钮的回调事件
    self._btnLeftCallBack = nil
    self._btnRightCallBack = nil
end

-- 初始化界面
function MJDlgAlert:setupViews()
    self.root = UIAdapter:createNode("csb/mahjong_dlg_alter_v.csb")
    self:addChild(self.root)
    UIAdapter:adapter(self.root, handler(self, self.onTouchEvent))

    self.labelTitle = self.root:getChildByName("label_title")
    self.img_title_bg = self.root:getChildByName("img_title_bg")
    self.btnConfirm = self.root:getChildByName("btn_confirm")
    self.btnCancle = self.root:getChildByName("btn_cancle")
    self.btnOnlyOne = self.root:getChildByName("btn_only_one")
    self.btnConfirm:enableOutline({r = 29, g = 149, b = 124, a = 255}, 3)
    self.btnCancle:enableOutline({r = 149, g = 29, b = 29, a = 255}, 3)
    self.btnOnlyOne:enableOutline({r = 29, g = 149, b = 124, a = 255}, 3)

    self.label_content = self.root:getChildByName("label_content")
	local x,y = self.label_content:getPosition()
	local color =  self.label_content:getColor()
	local fontSize =  self.label_content:getFontSize()
	self.label_content:setName("label_content1")
	self.label_content:setVisible(false)
	local parent = self.label_content:getParent()
	local label = display.newTTFLabel({text = "",size = fontSize,color=color,font="ttf/jcy.TTF"})
	parent:addChild(label)
	label:setPosition(cc.p(x,y))
	self.label_content = label
	self.label_content:setName("label_content")
	self.label_content:setDimensions(520,0)
	
	local node = display.newNode()
	parent:addChild(node)
	node:setPosition(cc.p(x,y))
	self.layout_node = node
	--self.label_content:ignoreContentAdaptWithSize(true)
	--self.label_content:setTextAreaSize(cc.size(300,0))
	--self.label_content:setString("游戏已结束游戏已结束游戏已结束游戏已结束游戏已结束")
	--self.label_content:getVirtualRenderer():setDimensions(520, 0)
end

function MJDlgAlert:setFontSize(size)
	self.label_content:setSystemFontSize(size)
end

-- 设置标题
function MJDlgAlert:setContentText( str)
	str = str or ""
	local max_s = str 
	local list = string.split(str, "\n")
	if #list>1 then
		local max_l = string.len(list[1])
		local max_k = 1
		for i =2,#list do
			local l = string.len(list[i])
			if l>max_l then
				max_l = l
				max_k = i
			end
		end
		max_s = list[max_k]


	else
		max_s = list[1]
	end
	self.label_content:setDimensions(0,0)
	self.label_content:setString(max_s)
	local size = self.label_content:getContentSize()
	if size.width >520 then
		self.label_content:setDimensions(520,0)
	else
		self.label_content:setDimensions(size.width,0)
	end
	print(str)
	self.label_content:setString(str)
end

-- 设置标题
function MJDlgAlert:setTitle( str )
    if str then
		print(str)
        self.labelTitle:setString(str)
    end
end

-- 设置是否显示标题层
function MJDlgAlert:setTitleVisible( bIs )
    if bIs then
        self.labelTitle:setVisible(true)
		self.img_title_bg:setVisible(true)
    else
        self.labelTitle:setVisible(false)
		self.img_title_bg:setVisible(false)
    end
end

-- 设置内容 size: 500*128
function MJDlgAlert:setContent( layer )
    if layer then
        self.layout_node:addChild(layer)
    end
end

-- 设置单个按钮的文本和回调事件
function MJDlgAlert:setSingleBtn(sName, callback)
    self.btnConfirm:setVisible(false)
    self.btnCancle:setVisible(false)
    self.btnOnlyOne:setVisible(true)

    if sName then
        self.btnOnlyOne:setTitleText(sName)
    end
    self._singleBtnCallBack = callback
end

-- 设置两个按钮的文本和回调事件
function MJDlgAlert:setBtnAndCallBack( str1, str2, callback1, callback2 )
    self.btnConfirm:setVisible(true)
    self.btnCancle:setVisible(true)
    self.btnOnlyOne:setVisible(false)

    if str1 then
        self.btnConfirm:setTitleText(str1)
        
        if ToolKit:getUtf8StringCount(str1) > 2 then
            self.btnConfirm:setTitleFontSize(32)
        end 
    end
    
    if str2 then 
        self.btnCancle:setTitleText(str2)
        if ToolKit:getUtf8StringCount(str2) > 2 then
            self.btnConfirm:setTitleFontSize(32)
        end 
    end

    self._btnLeftCallBack = callback1
    self._btnRightCallBack = callback2
end

-- 点击事件
function MJDlgAlert:onTouchEvent(sender, eventType)
    local name = sender:getName()
    -- print(name)
    if name == "btn_confirm" then
        if self._btnLeftCallBack then
            self._btnLeftCallBack()
        end

    elseif name == "btn_cancle" then
        if self._btnRightCallBack then
            self._btnRightCallBack()
        end

    elseif name == "btn_only_one" then
        if self._singleBtnCallBack then
            self._singleBtnCallBack()
        else
            self:closeDialog()
        end

    end
end

--  展示正确弹窗
function MJDlgAlert.showRightAlert(param, callback)
    local dlg = MJDlgAlert.new()


    -- 设置文本
    local txt_tips = dlg.label_content
	
    if param.title and param.title ~= "" then
        dlg:setTitle( param.title )
    else
        dlg:setTitle( "提示" )
    end
    
    if param.tip and  param.tip ~= ""  then
        dlg:setContentText( param.tip)
    else
        txt_tips:setString("")
    end

    -- 设置按钮
    dlg:setSingleBtn(STR(92, 1))

    -- show
    dlg:showDialog()
    if callback then
        dlg:setCloseCallback(callback)
    end
    return dlg
end

--展示错误弹窗
function MJDlgAlert.showErrorAlert(param, callback)   

    MJDlgAlert.showRightAlert(param, callback)
 
end

-- 展示提示弹窗
function MJDlgAlert.showTipsAlert(param, callback)
    local dlg = MJDlgAlert.new()

    -- 设置文本
    local txt_tips = dlg.label_content

    if param.title and param.title ~= "" then
        dlg:setTitle( param.title )
    else
        dlg:setTitle( "提示" )
    end
    
    if param.tip and  param.tip ~= ""  then
        dlg:setContentText( param.tip)
    else
        txt_tips:setString("")
    end

    if param.tip_size then
        dlg:setFontSize(param.tip_size)
    end

    -- 设置按钮
    dlg:setBtnAndCallBack(STR("5", 4), STR("6", 4) )

    -- show
    dlg:showDialog()
    if callback then
        dlg:setCloseCallback(callback)
    end
    return dlg
end

-- 提示确认框
-- 支持多行
-- 如果是单行请用上面的showTipsAlert
function MJDlgAlert.customTipsAlert(param, callback)

    local dlg = MJDlgAlert.new()


    -- 设置文本
    local txt_tips = dlg.label_content

    if param.title and param.title ~= "" then
        dlg:setTitle( param.title )
    else
        dlg:setTitle( "提示" )
    end
    
	dlg:setFontSize(param.tip_size)
	dlg:setContentText( param.tip or "")
    -- 设置按钮
    dlg:setBtnAndCallBack(STR("5", 4), STR("6", 4) )

    -- show
    dlg:showDialog()
    if callback then
        dlg:setCloseCallback(callback)
    end
    return dlg
end

function MJDlgAlert:TowSubmitAlert(param, callback1,callback2)
    -- 二次确认
    if param.title then
        self:setTitle(param.title)
    else
        self:setTitle("提示")
    end

    if param.message then
		self:setContentText( param.message)       
    end
	if param.isGray then
		
	end

    self:setBtnAndCallBack( param.leftStr, param.rightStr,
        function ()
            if callback1 then
                callback1()
            end

            self:closeDialog()
        end ,

        function()
            if callback2 then
                callback2()
            end
            self:closeDialog()
        end
    )

end

return MJDlgAlert
