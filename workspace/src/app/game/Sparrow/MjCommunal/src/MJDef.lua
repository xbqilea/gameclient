--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 麻将定义

MJDef = MJDef or {}

-- 
-- 麻将定义 begin
-- 
MJDef.PAI = {}
MJDef.PAI.NULL 				= 0x00			--	空

MJDef.PAI.WAN_1				= 0x01			--	一萬
MJDef.PAI.WAN_2				= 0x02			--	二萬
MJDef.PAI.WAN_3				= 0x03			--	三萬
MJDef.PAI.WAN_4				= 0x04			--	四萬
MJDef.PAI.WAN_5				= 0x05			--	五萬
MJDef.PAI.WAN_6				= 0x06			--	六萬
MJDef.PAI.WAN_7				= 0x07			--	七萬
MJDef.PAI.WAN_8				= 0x08			--	八萬
MJDef.PAI.WAN_9				= 0x09			--	九萬

MJDef.PAI.SUO_1				= 0x11			--	一索
MJDef.PAI.SUO_2				= 0x12			--	二索
MJDef.PAI.SUO_3				= 0x13			--	三索
MJDef.PAI.SUO_4				= 0x14			--	四索
MJDef.PAI.SUO_5				= 0x15			--	五索
MJDef.PAI.SUO_6				= 0x16			--	六索
MJDef.PAI.SUO_7				= 0x17			--	七索
MJDef.PAI.SUO_8				= 0x18			--	八索
MJDef.PAI.SUO_9				= 0x19			--	九索

MJDef.PAI.TONG_1				= 0x21			--	一筒
MJDef.PAI.TONG_2				= 0x22			--	二筒
MJDef.PAI.TONG_3				= 0x23			--	三筒
MJDef.PAI.TONG_4				= 0x24			--	四筒
MJDef.PAI.TONG_5				= 0x25			--	五筒
MJDef.PAI.TONG_6				= 0x26			--	六筒
MJDef.PAI.TONG_7				= 0x27			--	七筒
MJDef.PAI.TONG_8				= 0x28			--	八筒
MJDef.PAI.TONG_9				= 0x29			--	九筒

MJDef.PAI.DONG					= 0x31			--	东风
MJDef.PAI.NAN					= 0x32 			--	南风
MJDef.PAI.XI					= 0x33  			--	西风
MJDef.PAI.BEI					= 0x34 			--	北风

MJDef.PAI.ZHONG				= 0x35			--	红中
MJDef.PAI.FA					= 0x36			--	发财
MJDef.PAI.BAI					= 0x37			--	白板

MJDef.PAI.CHUN					= 0x41			--	春
MJDef.PAI.XIA					= 0x42			--	夏
MJDef.PAI.QIU					= 0x43			--	秋
MJDef.PAI.HDONG				= 0x44			--	冬
MJDef.PAI.MEI					= 0x45			--	梅
MJDef.PAI.LAN					= 0x46			--	兰
MJDef.PAI.ZHU					= 0x47			--	竹
MJDef.PAI.JU					= 0x48			--	菊

MJDef.PAI.GAI					= 0x4A			--	盖牌（背面）
MJDef.PAI.BAO					= 0x4B			--	宝牌

-- 麻将中文解释
MJDef.PAISTR = {}
MJDef.PAISTR[MJDef.PAI.NULL] 				= "空"

MJDef.PAISTR[MJDef.PAI.WAN_1]				= "一万"
MJDef.PAISTR[MJDef.PAI.WAN_2]				= "二万"
MJDef.PAISTR[MJDef.PAI.WAN_3]				= "三万"
MJDef.PAISTR[MJDef.PAI.WAN_4]				= "四万"
MJDef.PAISTR[MJDef.PAI.WAN_5]				= "五万"
MJDef.PAISTR[MJDef.PAI.WAN_6]				= "六万"
MJDef.PAISTR[MJDef.PAI.WAN_7]				= "七万"
MJDef.PAISTR[MJDef.PAI.WAN_8]				= "八万"
MJDef.PAISTR[MJDef.PAI.WAN_9]				= "九万"

MJDef.PAISTR[MJDef.PAI.SUO_1]				= "一索"
MJDef.PAISTR[MJDef.PAI.SUO_2]				= "二索"
MJDef.PAISTR[MJDef.PAI.SUO_3]				= "三索"
MJDef.PAISTR[MJDef.PAI.SUO_4]				= "四索"
MJDef.PAISTR[MJDef.PAI.SUO_5]				= "五索"
MJDef.PAISTR[MJDef.PAI.SUO_6]				= "六索"
MJDef.PAISTR[MJDef.PAI.SUO_7]				= "七索"
MJDef.PAISTR[MJDef.PAI.SUO_8]				= "八索"
MJDef.PAISTR[MJDef.PAI.SUO_9]				= "九索"

MJDef.PAISTR[MJDef.PAI.TONG_1]				= "一筒"
MJDef.PAISTR[MJDef.PAI.TONG_2]				= "二筒"
MJDef.PAISTR[MJDef.PAI.TONG_3]				= "三筒"
MJDef.PAISTR[MJDef.PAI.TONG_4]				= "四筒"
MJDef.PAISTR[MJDef.PAI.TONG_5]				= "五筒"
MJDef.PAISTR[MJDef.PAI.TONG_6]				= "六筒"
MJDef.PAISTR[MJDef.PAI.TONG_7]				= "七筒"
MJDef.PAISTR[MJDef.PAI.TONG_8]				= "八筒"
MJDef.PAISTR[MJDef.PAI.TONG_9]				= "九筒"

MJDef.PAISTR[MJDef.PAI.DONG]					= "东风"
MJDef.PAISTR[MJDef.PAI.NAN]					= "南风"
MJDef.PAISTR[MJDef.PAI.XI]					= "西风"
MJDef.PAISTR[MJDef.PAI.BEI]					= "北风"

MJDef.PAISTR[MJDef.PAI.ZHONG]				= "红中"
MJDef.PAISTR[MJDef.PAI.FA]					= "发财"
MJDef.PAISTR[MJDef.PAI.BAI]					= "白板"

MJDef.PAISTR[MJDef.PAI.CHUN]					= "春"
MJDef.PAISTR[MJDef.PAI.XIA]					= "夏"
MJDef.PAISTR[MJDef.PAI.QIU]					= "秋"
MJDef.PAISTR[MJDef.PAI.HDONG]				= "冬"
MJDef.PAISTR[MJDef.PAI.MEI]					= "梅"
MJDef.PAISTR[MJDef.PAI.LAN]					= "兰"
MJDef.PAISTR[MJDef.PAI.ZHU]					= "竹"
MJDef.PAISTR[MJDef.PAI.JU]					= "菊"

MJDef.PAISTR[MJDef.PAI.GAI]					= "盖牌"
MJDef.PAISTR[MJDef.PAI.BAO]					= "宝牌"
--
-- 麻将定义 end
--


--
-- 胡牌类型定义 begin
--
MJDef.PLAYTYPE = {}
MJDef.PLAYTYPE.MAX_PLAY_NUM			= 	6			-- 最大玩法数量
MJDef.PLAYTYPE.FOUR_EGG				= 	0			-- 四风蛋
MJDef.PLAYTYPE.FIRE_GUN_THREE		= 	6			-- 点炮包三家
MJDef.PLAYTYPE.SHUT_DOUBLE			= 	5			-- 闭门加倍
MJDef.PLAYTYPE.EGG_STAND			= 	4			-- 下蛋算站立
MJDef.PLAYTYPE.THREE_SHUT			= 	3			-- 三家闭
MJDef.PLAYTYPE.THREE_EGG			= 	2			-- 三风蛋
MJDef.PLAYTYPE.SEVEN_PAIR			= 	1			-- 七小对


MJDef.PLAYTYPESTR = {}
MJDef.PLAYTYPESTR[MJDef.PLAYTYPE.FOUR_EGG]				=	"四风蛋"
MJDef.PLAYTYPESTR[MJDef.PLAYTYPE.FIRE_GUN_THREE]		=	"点炮包三家"
MJDef.PLAYTYPESTR[MJDef.PLAYTYPE.SHUT_DOUBLE]			=	"闭门加倍"
MJDef.PLAYTYPESTR[MJDef.PLAYTYPE.EGG_STAND]				=	"下蛋算站立"
MJDef.PLAYTYPESTR[MJDef.PLAYTYPE.THREE_SHUT]			=	"三家闭双倍"
MJDef.PLAYTYPESTR[MJDef.PLAYTYPE.THREE_EGG]				=	"三风蛋"
MJDef.PLAYTYPESTR[MJDef.PLAYTYPE.SEVEN_PAIR]			=	"七小对"


MJDef.ROUNDTYPESTR = {}
MJDef.ROUNDTYPESTR[1]	=	"东风局"
MJDef.ROUNDTYPESTR[2]	=	"南风局"
MJDef.ROUNDTYPESTR[3]	=	"西风局"
MJDef.ROUNDTYPESTR[0]	=	"北风局"


--
-- 胡牌类型定义 end
--

--
-- 胡牌类型定义 begin
--
MJDef.HUTYPE = {}
MJDef.HUTYPE.NULL				= 	0x0000			-- 
MJDef.HUTYPE.LEVEL				= 	0x0001			-- 平胡  
MJDef.HUTYPE.SELF_PICK			= 	0x0002			-- 自摸
MJDef.HUTYPE.FIRE_GUN			= 	0x0004			-- 放炮
MJDef.HUTYPE.CLIP				= 	0x0008			-- 夹胡
MJDef.HUTYPE.HIS_TREASURE		= 	0x0010			-- 搂宝
MJDef.HUTYPE.TREASURES			= 	0x0020			-- 宝中宝
MJDef.HUTYPE.DRIFT				= 	0x0040			-- 飘胡
MJDef.HUTYPE.BANKER				= 	0x0080			-- 庄胡
MJDef.HUTYPE.STAND				= 	0x0100			-- 立胡
MJDef.HUTYPE.THREE_SHUT			= 	0x0200			-- 三家闭
MJDef.HUTYPE.SEVEN_PAIR			= 	0x0400			-- 七小对
MJDef.HUTYPE.QXD_HH				=	0x00020000		--	豪华七小对
MJDef.HUTYPE.QXD_SHH			=	0x00040000		--	双豪华七小对
MJDef.HUTYPE.QXD_ZZ				=	0x00080000		--	至尊七小对

-- 推倒胡
MJDef.HUTYPE.QING_YI_SE			=		0x0800		--	清一色
MJDef.HUTYPE.SAN_SU_QUAN		=		0x1000		--	三色全
MJDef.HUTYPE.SHOU_BA_YI			=		0x2000		--	手把一
MJDef.HUTYPE.ZHUA_WU_KUI		=		0x4000		--	捉五魁
MJDef.HUTYPE.GANG_KAI			=		0x8000		--	杠上开花
MJDef.HUTYPE.PENGPENG_HU 		=		0x10000 		-- 	碰碰胡
--
-- 胡牌类型定义 end
--

--
-- 胡牌类型倍数定义 begin
--
MJDef.HUTYPEODD = {}
MJDef.HUTYPEODD[MJDef.HUTYPE.NULL]				= 	nil	--"流局"
MJDef.HUTYPEODD[MJDef.HUTYPE.LEVEL]				= 	nil	--"平胡"
MJDef.HUTYPEODD[MJDef.HUTYPE.SELF_PICK]			= 	2	--"自摸"
MJDef.HUTYPEODD[MJDef.HUTYPE.FIRE_GUN]			= 	nil	--"放炮"
MJDef.HUTYPEODD[MJDef.HUTYPE.CLIP]				= 	2	--"夹胡"
MJDef.HUTYPEODD[MJDef.HUTYPE.HIS_TREASURE]		= 	2	--"搂宝"
MJDef.HUTYPEODD[MJDef.HUTYPE.TREASURES]			= 	4	--"宝中宝"
MJDef.HUTYPEODD[MJDef.HUTYPE.DRIFT]				= 	4	--"飘胡"
MJDef.HUTYPEODD[MJDef.HUTYPE.BANKER]			= 	2	--"庄胡"
MJDef.HUTYPEODD[MJDef.HUTYPE.STAND]				= 	2	--"立胡"
MJDef.HUTYPEODD[MJDef.HUTYPE.THREE_SHUT]		= 	2	--"三家闭"
MJDef.HUTYPEODD[MJDef.HUTYPE.SEVEN_PAIR]		= 	4	--"七小对"

MJDef.HUTYPEODD[MJDef.HUTYPE.QXD_HH]			= 	16	--"豪华七小对"
MJDef.HUTYPEODD[MJDef.HUTYPE.QXD_SHH]			= 	32	--"双豪华七小对"
MJDef.HUTYPEODD[MJDef.HUTYPE.QXD_ZZ]			= 	64	--"至尊豪华七小对"

-- 推倒胡
MJDef.HUTYPEODD[MJDef.HUTYPE.QING_YI_SE]		=	4	--"清一色"
MJDef.HUTYPEODD[MJDef.HUTYPE.SAN_SU_QUAN]		=	2	--"三色全"
MJDef.HUTYPEODD[MJDef.HUTYPE.SHOU_BA_YI]		=	2	--"手把一"
MJDef.HUTYPEODD[MJDef.HUTYPE.ZHUA_WU_KUI]		=	2	--"捉五魁"
MJDef.HUTYPEODD[MJDef.HUTYPE.GANG_KAI]			=	2	--"杠上开花"
MJDef.HUTYPEODD[MJDef.HUTYPE.PENGPENG_HU]		=	4	--"碰碰胡"
--
-- 胡牌类型倍数定义 end
--

-- 
-- 胡牌中文解释 begin
--
MJDef.HUTYPESTR = {}
MJDef.HUTYPESTR[MJDef.HUTYPE.NULL]				= 	"流局"
MJDef.HUTYPESTR[MJDef.HUTYPE.LEVEL]				= 	"平胡"
MJDef.HUTYPESTR[MJDef.HUTYPE.SELF_PICK]			= 	"自摸"
MJDef.HUTYPESTR[MJDef.HUTYPE.FIRE_GUN]			= 	"放炮"
MJDef.HUTYPESTR[MJDef.HUTYPE.CLIP]				= 	"夹胡"
MJDef.HUTYPESTR[MJDef.HUTYPE.HIS_TREASURE]		= 	"搂宝"
MJDef.HUTYPESTR[MJDef.HUTYPE.TREASURES]			= 	"宝中宝"
MJDef.HUTYPESTR[MJDef.HUTYPE.DRIFT]				= 	"飘胡"
MJDef.HUTYPESTR[MJDef.HUTYPE.BANKER]				= 	"庄胡"
MJDef.HUTYPESTR[MJDef.HUTYPE.STAND]				= 	"立胡"
MJDef.HUTYPESTR[MJDef.HUTYPE.THREE_SHUT]			= 	"三家闭"
MJDef.HUTYPESTR[MJDef.HUTYPE.SEVEN_PAIR]			= 	"七小炮"
MJDef.HUTYPESTR[MJDef.HUTYPE.QXD_HH]				=	"豪华七小对"
MJDef.HUTYPESTR[MJDef.HUTYPE.QXD_SHH]			=	"双豪华七小对"
MJDef.HUTYPESTR[MJDef.HUTYPE.QXD_ZZ]				=	"至尊豪华七小对"
-- 推倒胡
MJDef.HUTYPESTR[MJDef.HUTYPE.QING_YI_SE]			=	"清一色"
MJDef.HUTYPESTR[MJDef.HUTYPE.SAN_SU_QUAN]		=	"三色全"
MJDef.HUTYPESTR[MJDef.HUTYPE.SHOU_BA_YI]			=	"手把一"
MJDef.HUTYPESTR[MJDef.HUTYPE.ZHUA_WU_KUI]		=	"捉五魁"
MJDef.HUTYPESTR[MJDef.HUTYPE.GANG_KAI]			=	"杠上开花"
MJDef.HUTYPESTR[MJDef.HUTYPE.PENGPENG_HU]			=	"碰碰胡"
--
-- 胡牌中文解释 end
--

--
-- 操作码定义 begin
--
MJDef.OPER = {}

MJDef.OPER.FACE_ID = 0x4F					--玩家头像Id	ID..cid..Id;

MJDef.OPER.USER_ID = 0x50					--	玩家ID		ID..cid..uid;
MJDef.OPER.USER_ACCOUNT = 0x51				--	玩家账号	ID..cid..account;
MJDef.OPER.USER_NAME = 0x52				--	玩家昵称	ID..cid..name;
MJDef.OPER.MASTER_ID = 0x53				--	房主ID，	ID..cid..uid;
MJDef.OPER.BANKER = 0x54					--	庄家		ID..cid;


MJDef.OPER.MJ_TABLE_ID = 0x55					--	桌子号（房间号）ID..FF..tid;

MJDef.OPER.MJ_SELECTION = 0x56			--	玩法选项	ID..FF..aabbccddeeff;
MJDef.OPER.MJ_ZONG_JI_FEN = 0x57				--	总积分		ID..cid..score;		（score 十进制，总共）
MJDef.OPER.MJ_HU_FEN = 0x58					--	胡分		ID..cid..score;		（score 十进制，单局）
MJDef.OPER.MJ_GANG_FEN = 0x59					--	杠分		ID..cid..score;		（score 十进制，单局）
MJDef.OPER.MJ_LEFT_CARD_CNT = 0x5A			--	剩余牌数	ID..FF..cnt;

MJDef.OPER.MJ_VIP_ROUND = 0x5B			--	VIP 轮数	ID..FF..局数..轮数..总轮数

MJDef.OPER.MJ_END_BI_MEN = 0xD0				--	闭门		ID..FF..bi1..bi2..b3...;
MJDef.OPER.MJ_END_TING_PAI = 0xD1				--	听牌		ID..FF..t1..t2..t3...;
MJDef.OPER.MJ_END_HU_TYPE = 0xD2				--	胡牌类型	ID..FF..type%08x;	(位与)


MJDef.OPER.START_DICE = 0x60			--	开始骰子	ID..FF..(dice1 << 4 | dice2);
MJDef.OPER.SHOU_PAI = 0x61			--	手牌		cid..card1..card2..card3......;
MJDef.OPER.DONG_HUA = 0x62			--	动画结束	（手机端收到手牌，播放动画完成后发给服务器）card: 1-开始，2-场景

MJDef.OPER.MO_PAI = 0x65				--	玩家摸牌	ID..cid..card;
MJDef.OPER.CHU_PAI = 0x66				--	出牌		ID..cid..card;
MJDef.OPER.GUO = 0x67					--	过			ID..cid;
MJDef.OPER.FEN_ZHANG = 0x68			--	分张		ID..cid..card;

MJDef.OPER.XUAN_BAO = 0x6A			--	选宝牌		ID..cid..dice..card;
MJDef.OPER.KAN_BAO = 0x6B				--	看宝牌		ID..cid..card;
MJDef.OPER.HUAN_BAO = 0x6C			--	换宝牌		ID..cid..dice..card;

MJDef.OPER.CHI = 0x70					--	吃			ID..cid..card;
MJDef.OPER.PENG = 0x71				--	碰
MJDef.OPER.AN_GANG = 0x72				--	暗杠		（手上有4张牌杠）
MJDef.OPER.MING_GANG = 0x73			--	明杠		（手上有3张牌别人打出一张 拿过来杠）
MJDef.OPER.JIA_GANG = 0x74			--	加杠		（碰有三张牌，手牌有一张牌 补上去杠）

MJDef.OPER.YAO_GANG = 0x7A			--	幺杠		ID..cid..card1..card2..card3;
MJDef.OPER.JIU_GANG = 0x7B			--	九杠
MJDef.OPER.SAN_FENG_GANG = 0x7C		--	三风杠
MJDef.OPER.SI_FENG_GANG = 0x7D		--	四风杠
MJDef.OPER.XI_GANG = 0x7E				--	喜杠
MJDef.OPER.FEI_DAN = 0x7F				--	飞蛋

MJDef.OPER.BU_YAO_GANG = 0x80			--	补幺杠		ID..cid..card;
MJDef.OPER.BU_JIU_GANG = 0x81			--	补九杠
MJDef.OPER.BU_FENG_GANG = 0x82		--	补风杠
MJDef.OPER.BU_XI_GANG = 0x83			--	补喜杠
MJDef.OPER.BU_FEI_DAN = 0x84			--	补飞蛋

MJDef.OPER.BU_GANG_TS = 0x8F		--	补杠提示  TS ti shi

MJDef.OPER.CHEN_PAI = 0x90			--	抻牌		ID..cid;
MJDef.OPER.TING_PAI = 0x91			--	听牌		ID..cid;

MJDef.OPER.CHI_TING = 0x95			--	吃听		ID..cid..card;
MJDef.OPER.PENG_TING = 0x96			--	碰听

MJDef.OPER.QG_PENG = 0x9A				--	抢杠碰		（A补杠，B抢过来碰）
MJDef.OPER.QG_CHI_TING = 0x9B			--	抢杠吃听	（A补杠，B抢过来吃听）
MJDef.OPER.QG_PENG_TING = 0x9C		--	抢杠碰听 	（A补杠，B抢过来碰听）
MJDef.OPER.QG_GANG = 0x9D				--	抢杠杠		（A补杠，B抢过来明杠（4张牌））
MJDef.OPER.QG_HU = 0x9E				--	抢杠胡		（A补杠，B抢过来胡）

MJDef.OPER.HU_PAI = 0xB0				--	胡牌		
MJDef.OPER.DUI_BAO = 0xB1				--	对宝胡牌	（宝中宝）	
MJDef.OPER.LOU_BAO = 0xB2				--	搂宝胡牌		

MJDef.OPER.HUI_GUN = 0xC0				--	回滚		（到上一操作）
MJDef.OPER.TUO_GUAN = 0xC1					--	托管

MJDef.OPER.GAME_ERROR_ACK = 0xFD				--	错误返回
MJDef.OPER.GAME_END_FLAG = 0xFF				--	游戏结束标志
--
-- 操作码定义 end
--

--
-- 操作码中文解释
--
MJDef.OPERSTR = {}

MJDef.OPERSTR[MJDef.OPER.USER_ID] = "玩家ID"		--		ID..cid..uid;
MJDef.OPERSTR[MJDef.OPER.USER_ACCOUNT] = "玩家账号"		--	ID..cid..account;
MJDef.OPERSTR[MJDef.OPER.USER_NAME] = "玩家昵称"		--	ID..cid..name;
MJDef.OPERSTR[MJDef.OPER.MASTER_ID] = "房主ID，"		--	ID..cid..uid;
MJDef.OPERSTR[MJDef.OPER.BANKER] = "庄家"		--		ID..cid;

MJDef.OPERSTR[MJDef.OPER.START_DICE] = "开始骰子"		--	ID..FF..(dice1 << 4 | dice2);
MJDef.OPERSTR[MJDef.OPER.SHOU_PAI] = "手牌"		--		cid..card1..card2..card3......;
MJDef.OPERSTR[MJDef.OPER.DONG_HUA] = "动画结束"		--	（手机端收到手牌，播放动画完成后发给服务器）

MJDef.OPERSTR[MJDef.OPER.MO_PAI] = "玩家摸牌"		--	ID..cid..card;
MJDef.OPERSTR[MJDef.OPER.CHU_PAI] = "出牌"		--		ID..cid..card;
MJDef.OPERSTR[MJDef.OPER.GUO] = "过"		--			ID..cid;

MJDef.OPERSTR[MJDef.OPER.XUAN_BAO] = "选宝牌"		--		ID..cid..dice..card;
MJDef.OPERSTR[MJDef.OPER.KAN_BAO] = "看宝牌"		--		ID..cid..card;
MJDef.OPERSTR[MJDef.OPER.HUAN_BAO] = "换宝牌"		--		ID..cid..dice..card;

MJDef.OPERSTR[MJDef.OPER.CHI] = "吃"		--			ID..cid..card;
MJDef.OPERSTR[MJDef.OPER.PENG] = "碰"		--
MJDef.OPERSTR[MJDef.OPER.AN_GANG] = "暗杠"		--		（手上有4张牌杠）
MJDef.OPERSTR[MJDef.OPER.MING_GANG] = "明杠"		--		（手上有3张牌别人打出一张 拿过来杠）
MJDef.OPERSTR[MJDef.OPER.JIA_GANG] = "加杠"		--		（碰有三张牌，手牌有一张牌 补上去杠）

MJDef.OPERSTR[MJDef.OPER.YAO_GANG] = "幺杠"		--		ID..cid..card1..card2..card3;
MJDef.OPERSTR[MJDef.OPER.JIU_GANG] = "九杠"		--
MJDef.OPERSTR[MJDef.OPER.SAN_FENG_GANG] = "三风杠"		--
MJDef.OPERSTR[MJDef.OPER.SI_FENG_GANG] = "四风杠"		--
MJDef.OPERSTR[MJDef.OPER.XI_GANG] = "喜杠"		--
MJDef.OPERSTR[MJDef.OPER.FEI_DAN] = "飞蛋"		--

MJDef.OPERSTR[MJDef.OPER.BU_YAO_GANG] = "补幺杠"		--		ID..cid..card;
MJDef.OPERSTR[MJDef.OPER.BU_JIU_GANG] = "补九杠"		--
MJDef.OPERSTR[MJDef.OPER.BU_FENG_GANG] = "补风杠"		--
MJDef.OPERSTR[MJDef.OPER.BU_XI_GANG] = "补喜杠"		--
MJDef.OPERSTR[MJDef.OPER.BU_FEI_DAN] = "补飞蛋"		--

MJDef.OPERSTR[MJDef.OPER.TING_PAI] = "听牌"		--		ID..cid;
MJDef.OPERSTR[MJDef.OPER.CHEN_PAI] = "抻牌"		--		ID..cid;

MJDef.OPERSTR[MJDef.OPER.CHI_TING] = "吃听"		--		ID..cid..card;
MJDef.OPERSTR[MJDef.OPER.PENG_TING] = "碰听"		--

MJDef.OPERSTR[MJDef.OPER.QG_CHI_TING] = "抢杠吃听"		--	（A补杠，B抢过来吃听）
MJDef.OPERSTR[MJDef.OPER.QG_PENG_TING] = "抢杠碰听"		-- 	（A补杠，B抢过来碰听）
MJDef.OPERSTR[MJDef.OPER.QG_PENG] = "抢杠碰"		--		（A补杠，B抢过来碰）
MJDef.OPERSTR[MJDef.OPER.QG_GANG] = "抢杠杠"		--		（A补杠，B抢过来明杠（4张牌））
MJDef.OPERSTR[MJDef.OPER.QG_HU] = "抢杠胡"		--		（A补杠，B抢过来胡）

MJDef.OPERSTR[MJDef.OPER.HU_PAI] = "胡牌"		--		
MJDef.OPERSTR[MJDef.OPER.DUI_BAO] = "对宝胡牌（宝中宝）"		--		
MJDef.OPERSTR[MJDef.OPER.LOU_BAO] = "搂宝胡牌"		--		


MJDef.OPERSTR[MJDef.OPER.HUI_GUN] = "回滚"		--		（到上一操作）


MJDef.OPERSTR[MJDef.OPER.GAME_END_FLAG] = "游戏结束标志"		--
--
-- 
--


--
-- 操作类型定义 begin
--
MJDef.OPERTPYE = {}

MJDef.OPERTPYE.CHI = 1
MJDef.OPERTPYE.PENG = 2
MJDef.OPERTPYE.GANG = 3
MJDef.OPERTPYE.TING = 4
MJDef.OPERTPYE.QIANG_TING = 5
MJDef.OPERTPYE.CHEN = 6
MJDef.OPERTPYE.HU = 7
MJDef.OPERTPYE.GUO = 8
--
-- 操作类型定义 end
--

MJDef.OPERMAP = {}
local function initOptMap()
	for k, v in pairs(MJDef.OPER) do
		-- 杠
		if v >= MJDef.OPER.AN_GANG and v <= MJDef.OPER.BU_FEI_DAN then
			MJDef.OPERMAP[v] = MJDef.OPERTPYE.GANG
		-- 抢听
		elseif v >= MJDef.OPER.CHI_TING and v <= MJDef.OPER.QG_PENG_TING then
			MJDef.OPERMAP[v] = MJDef.OPERTPYE.QIANG_TING
		end
	end
	-- 杠
	MJDef.OPERMAP[MJDef.OPER.QG_GANG] = MJDef.OPERTPYE.GANG
	-- 吃
	MJDef.OPERMAP[MJDef.OPER.CHI] = MJDef.OPERTPYE.CHI
	-- 碰
	MJDef.OPERMAP[MJDef.OPER.PENG] = MJDef.OPERTPYE.PENG
	MJDef.OPERMAP[MJDef.OPER.QG_PENG] = MJDef.OPERTPYE.PENG
	-- 听
	MJDef.OPERMAP[MJDef.OPER.TING_PAI] = MJDef.OPERTPYE.TING
	-- 抻
	MJDef.OPERMAP[MJDef.OPER.CHEN_PAI] = MJDef.OPERTPYE.CHEN
	-- 胡
	MJDef.OPERMAP[MJDef.OPER.HU_PAI] = MJDef.OPERTPYE.HU
	MJDef.OPERMAP[MJDef.OPER.QG_HU] = MJDef.OPERTPYE.HU
	-- 过
	MJDef.OPERMAP[MJDef.OPER.GUO] = MJDef.OPERTPYE.GUO
	-- dump(MJDef.OPERMAP)
	for k, v in pairs(MJDef.OPER) do
		if not MJDef.OPERMAP[v] then
			print("key : ", k)
		end 
	end
end
-- 计算操作map
initOptMap()

-- 牌的状态 
MJDef.eCardState = 
{
    e_none = -1, -- 无
    e_front_big1 = 0,  -- 正面大牌1（手牌）
    e_front_big2 = 1,  -- 正面大牌2（组合牌）
    e_back_big = 2,     -- 反面大牌

    e_front_small1 = 10,  -- 正面小牌1（出牌，组合牌）
    e_front_small2 = 11, -- 正面小牌2（宝牌）
    e_back_small1 = 12, -- 反面小牌1（宝牌）
    e_back_small2 = 13, -- 反面小牌2（手牌）

    e_left_stand = 21,   -- 左侧站立
    e_right_stand = 22,   -- 右侧站立
    e_left_front = 23,   -- 左侧正面
    e_right_front = 24,   -- 右侧正面
    e_left_back = 25, -- 左侧反面
    e_right_back = 26, -- 右侧反面

    e_bao = 30,         -- 宝牌
}

-- 当前操作玩家及状态
MJDef.eCurOptState = {
	MO_PAI = 1,  			-- 摸牌
	OTHER_OPT = 3,			-- 其他操作
}

-- 解散状态
MJDef.eDisbandState = {
	none = 0, 					-- 不处于解散状态
	failure = 1, 				-- 解散失败
	succeed = 2, 				-- 解散成功
	doing = 3, 					-- 解散中
}

MJDef.eDisbandVote = {
	none = 0, 			-- 未投票
	agreement = 1,	 			-- 同意
	disagreement = 2,	 			-- 不同意
}

-- 动画标识
MJDef.eAnimation = {
	BAO_PAI = "bao_pai", 		-- 宝牌
	YI_PAI = "yi_pai", 			-- 移牌
	CHA_PAI = "cha_pai", 			-- 插牌
	BEGIN_FA_PAI = "begin_fa_pai", 			-- 发牌动画
	NORMAL = "normal", 			-- 其他
	CHU_PAI_1 = "chu_pai_1",
	CHU_PAI_2 = "chu_pai_2",
	FENG_ZHANG = "FENG_ZHANG",
	JIE_SAN = "JIE_SAN", 		-- 解散
}

-- 性别
MJDef.eGender = {
	male = 1, 			-- 男性
	female = 0, 		-- 女性
}

--	麻将消息错误类型	MJ_OPER_ERROR
--	MET		msg error type
MJDef.MET = {}
MJDef.MET.NORMAL			=			0x00		--	正常操作
MJDef.MET.STATUS			=			0x01		--	游戏桌子状态错误（不在游戏中）
MJDef.MET.OTHER_OPER		=			0x02		--	非当前玩家操作错误
MJDef.MET.FING_OPER			=			0x03		--	查找操作错误（没有该操作）
MJDef.MET.OUT_CARD			=			0x04		--	不能出牌（吃碰杠弃）
MJDef.MET.FLASH				=			0x10		--	开始动画错误（已经收到）
MJDef.MET.ON_FLASH			=			0x11		--	有玩家在开始动画中（还不能操作）
MJDef.MET.OFF_LINE			=			0x12		--	断线重连（玩家在线）
MJDef.MET.PLAYER_LELF		=			0x21		--	有玩家离开

MJDef.METSTR = {}
-- MJDef.METSTR[MJDef.MET.NORMAL]			=	"正常操作"
-- MJDef.METSTR[MJDef.MET.STATUS]			=	"游戏桌子状态错误"
-- MJDef.METSTR[MJDef.MET.OTHER_OPER]		=	"非当前玩家操作状态"
-- MJDef.METSTR[MJDef.MET.FING_OPER]			=	"查找操作错误"
-- MJDef.METSTR[MJDef.MET.OUT_CARD]			=	"不能出牌（吃碰杠弃）"
-- MJDef.METSTR[MJDef.MET.FLASH]				=	"开始动画错误"
-- MJDef.METSTR[MJDef.MET.ON_FLASH]			=	"有玩家在开始动画中（还不能操作）"
-- MJDef.METSTR[MJDef.MET.OFF_LINE]			=	"断线重连（玩家在线）"
MJDef.METSTR[MJDef.MET.PLAYER_LELF]		=	"有玩家处于离线状态"

MJDef.ActTime = {
	FA_PAI = 0.5, 				-- 发牌动画时间（总时间为0.5*4）
	FAN_PAI_1 = 0.25/2, 			-- 翻牌前停顿时间（乱序正面）
	FAN_PAI_2 = 0.4/2, 			-- 翻牌停顿时间（背面）
	CHA_PAI = 0.4, 				-- 插牌动画总时间
	BIG_PAI = 0.2, 				-- 大牌停留时间
}

MJDef.PaiBG = {
	[1] = {
		BIG_1 = "board_big_1.png",
		BIG_2 = "board_big_2.png",
		BIG_3 = "board_big_3.png",
		MIN_1 = "board_min_1.png",
		MIN_2 = "board_min_2.png",
		MIN_3 = "board_min_3.png",
		MIN_4 = "board_min_4.png",
		LEFT_1 = "board_min_left_1.png",
		LEFT_2 = "board_min_left_2.png",
		LEFT_3 = "board_min_left_3.png",
		BAO = "board_bao.png",
	},
	[2] = {
	    BIG_1 = "board_big_1.png",
		BIG_2 = "board_big_2.png",
		BIG_3 = "mj_desktop_card_bg.png",
		MIN_1 = "board_min_1.png",
		MIN_2 = "board_min_2.png",
		MIN_3 = "mj_desktop_card_bg.png",
		MIN_4 = "mj_hand_card_bg.png",
		LEFT_1 = "board_min_left_1.png",
		LEFT_2 = "mj_desktop_card_right_bg.png",
		LEFT_3 = "mj_hand_card_right_bg.png",
		BAO = "board_bao.png",
	},
}

return MJDef
