--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 麻将牌
local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local MJCardItem = class("MJCardItem", function ()
	return display.newNode()
end)

function MJCardItem:ctor()
	self:myInit()

	self:setupViews()

    self:updateViews()

    -- local t_func = self.setPosition

    -- function self:setPosition(...)
    --     local mainScene = display.getRunningScene()
    --     if mainScene.vipWaitLayer then
    --         if mainScene.vipWaitLayer.cardLayer then
    --             local t = mainScene.vipWaitLayer.cardLayer.handCardItems[1]
    --             if t then
    --                 for k, v in pairs(t) do
    --                     if v == self then
    --                         print("----------------------------------------------------------------")
    --                         print(debug.traceback())
    --                     end
    --                 end
    --             end
    --         end
    --     end
    --     t_func(self,...)
    -- end
end

function MJCardItem:myInit()
    local DZMJCard = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.model.DZMJCard")
	self.m_card = DZMJCard.new(MJDef.PAI.WAN_1)
    self.m_state = MJDef.eCardState.e_front_big1
    self.m_index = 1

    self.m_endPos = cc.p(0, 0)
end

function MJCardItem:setupViews()
	self.view = UIAdapter:createNode("csb/mahjong_card_item.csb")
    self:addChild(self.view)

    self.img_bg = self.view:getChildByName("img_bg")
    self.img_card = self.view:getChildByName("img_card")
    self.layout_item = self.view:getChildByName("layout_item")

    self.imgLightCard = ccui.ImageView:create("board_mengban_3.png", 1)
    self.imgLightCard:setScale9Enabled(true)
    self.imgLightCard:setCapInsets(cc.rect(10, 10, 10, 10))
    self:addChild(self.imgLightCard)
    self:setLightCard(false)


    self.layout_item:addTouchEventListener(handler(self, self.onCardItemTouch))
    self.layout_item:setTouchEnabled(false)
end

function MJCardItem:setData( data )
	self.m_card = data.card or self.m_card
    self.m_state = data.state or self.m_state
    self.m_index = data.index or self.m_index

	self:updateViews()
end

function MJCardItem:getCardData()
    return self.m_card
end

function MJCardItem:updateViews()
    cc.SpriteFrameCache:getInstance():addSpriteFrames("ui/p_mahjong_new_card.plist","ui/p_mahjong_new_card.png")
    cc.SpriteFrameCache:getInstance():addSpriteFrames("ui/p_mahjong_new_card_1.plist","ui/p_mahjong_new_card_1.png")
    -- 刷新牌面数据
    -- dump(self.m_card)
    -- if self.m_card:getCard() > 0 then
    --     self.img_card:loadTexture("mj_card_" .. self.m_card:getCard() .. ".png", 1)
    -- end
    self.img_card:setScale(1) 
    self.img_card:setPosition(0,0)
    self.img_card:setVisible(false)
    self.img_card:setRotation(0)

    self.img_bg:setScale(1)
    self.img_card:setScale(1)

    local manager = MJHelper:getPlayerManager(true)
    local index = manager:getCardBGType()
    -- 正面大牌1（手牌）
	if self.m_state == MJDef.eCardState.e_front_big1 then
        self.imgLightCard:loadTexture("board_mengban_1.png", 1)
        if self.m_card:getCard() > 0 then 
           local paiImgName = "mj_hand_card_"..self.m_card:getCard()..".png"
           self.img_bg:loadTexture( paiImgName , 1)
        end
    -- 正面大牌2（组合牌）
    elseif self.m_state == MJDef.eCardState.e_front_big2 then
        self.imgLightCard:loadTexture("board_mengban_1.png", 1)
        if self.m_card:getCard() > 0 then 
           local paiImgName = "mj_desktop_card_"..self.m_card:getCard()..".png"
           self.img_bg:loadTexture( paiImgName , 1)
        end
    -- 反面大牌
    elseif self.m_state == MJDef.eCardState.e_back_big then
        self.img_bg:loadTexture(MJDef.PaiBG[index].BIG_3, 1)

    -- 正面小牌1（出牌，组合牌）
    elseif self.m_state == MJDef.eCardState.e_front_small1 then
        self.imgLightCard:loadTexture("board_mengban_3.png", 1)
        if self.m_card:getCard() > 0 then 
           local paiImgName = "mj_desktop_card_"..self.m_card:getCard()..".png"
           self.img_bg:loadTexture( paiImgName , 1)
           self.img_bg:setScale( 0.51 )
        end
    -- 正面小牌2（宝牌）
    elseif self.m_state == MJDef.eCardState.e_front_small2 then
         self.imgLightCard:loadTexture("board_mengban_3.png", 1)
        if self.m_card:getCard() > 0 then 
           local paiImgName = "mj_hand_card_"..self.m_card:getCard()..".png"
           self.img_bg:loadTexture( paiImgName , 1)
           self.img_bg:setScale( 0.51 )
        end
    -- 反面小牌1（宝牌）
    elseif self.m_state == MJDef.eCardState.e_back_small1 then
        self.img_bg:loadTexture(MJDef.PaiBG[index].MIN_3, 1)
        self.img_bg:setScale( 0.51 )
    -- 反面小牌2（手牌）
    elseif self.m_state == MJDef.eCardState.e_back_small2 then
        self.img_bg:loadTexture(MJDef.PaiBG[index].MIN_4, 1)
        self.img_bg:setScale( 0.51 )
    -- 左侧站立
    elseif self.m_state == MJDef.eCardState.e_left_stand then
        self.img_bg:loadTexture(MJDef.PaiBG[index].LEFT_3, 1)
        self.img_bg:setScaleX(-1)
        self.img_bg:setScale( 0.50 )
    -- 右侧站立
    elseif self.m_state == MJDef.eCardState.e_right_stand then
        self.img_bg:loadTexture(MJDef.PaiBG[index].LEFT_3, 1)
         self.img_bg:setScale( 0.50 )
    -- 左侧正面
    elseif self.m_state == MJDef.eCardState.e_left_front then
        self.imgLightCard:loadTexture("board_mengban_2.png", 1)
        if self.m_card:getCard() > 0 then 
           local paiImgName = "mj_desktop_card_left_"..self.m_card:getCard()..".png"
           self.img_bg:loadTexture( paiImgName , 1)
           self.img_bg:setScale( 0.62 )
        end
    -- 右侧正面
    elseif self.m_state == MJDef.eCardState.e_right_front then
        self.imgLightCard:loadTexture("board_mengban_2.png", 1)
        if self.m_card:getCard() > 0 then 
           local paiImgName = "mj_desktop_card_right_"..self.m_card:getCard()..".png"
           self.img_bg:loadTexture( paiImgName , 1)
           self.img_bg:setScale( 0.62 )
        end
    -- 左侧反面
    elseif self.m_state == MJDef.eCardState.e_left_back then
        self.img_bg:loadTexture(MJDef.PaiBG[index].LEFT_2, 1)
        self.img_bg:setScale( 0.48 )
    -- 右侧反面
    elseif self.m_state == MJDef.eCardState.e_right_back then
        self.img_bg:loadTexture(MJDef.PaiBG[index].LEFT_2, 1)
        self.img_bg:setScale( 0.48 )  
    -- 宝牌
    elseif self.m_state == MJDef.eCardState.e_bao then
        --self.img_bg:loadTexture(MJDef.PaiBG[index].BAO, 1)
        --self.img_card:setVisible(false)
    end


    -- 更新节点size
    local size = self.img_bg:getContentSize()
    local bgscaleX = self.img_bg:getScaleX()
    local bgscaleY = self.img_bg:getScaleY()
    self.layout_item:setContentSize(cc.size(size.width * bgscaleX, size.height * bgscaleY))
    self:setScaleSize(self:getScaleX(), self:getScaleY() )
    self.img_bg:setPosition(size.width * bgscaleX /2, size.height * bgscaleY/2)
    self.imgLightCard:setPosition(size.width * bgscaleY/2, size.height * bgscaleY/2 + 1)
    self.imgLightCard:setContentSize(cc.size(size.width * bgscaleX, size.height * bgscaleY))
end

function MJCardItem:setScaleSize( scaleX, scaleY )
    scaleY = scaleY or 1
    local size = self.img_bg:getContentSize()
    local bgscaleX = self.img_bg:getScaleX()
    local bgScaleY = self.img_bg:getScaleY()
    self:setScaleX(scaleX)
    self:setScaleY(scaleY)
    self:setContentSize(cc.size(size.width * scaleX * bgscaleX, size.height * scaleY * bgScaleY ))
end

function MJCardItem:setLayoutTouchPos( pos )
    self.m_ltPos = pos
end

function MJCardItem:getLayoutTouchPos()
    return self.m_ltPos
end

function MJCardItem:onCardItemTouch( sender, eventType )
    local parent = self:getParent()
    if eventType == ccui.TouchEventType.began then     
        local gameManager = MJHelper:getGameManager(true)  
        if gameManager:isSendOutCardMsg() then
            print("[MJCardItem MJWarnning] is sending out card msg, can not out card")
            return false
        end

        -- print("MJCardItem ccui.TouchEventType.began")
        self:setLocalZOrder(1)
        self:setLayoutTouchPos(nil)
        -- 摸牌标识
        sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_MO_PAI_ANI, "stop")

        if gameManager:isChaTingState() then
            sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_CHATING, self)
        end

    elseif eventType == ccui.TouchEventType.moved then
        local beginPos = sender:getTouchBeganPosition()
        local worldPos = sender:getTouchMovePosition()
        local localPos = parent:convertToNodeSpace(worldPos)
        local vector2 = cc.p(worldPos.x - beginPos.x, worldPos.y - beginPos.y)
        local layoutPos = self:getLayoutTouchPos()
        if layoutPos then
            vector2 = cc.p(localPos.x - layoutPos.x, localPos.y - layoutPos.y)
        end
        -- print("MJCardItem ccui.TouchEventType.moved")
        -- print("beginPos : ", beginPos.x, beginPos.y)
        -- print("worldPos : ", worldPos.x, worldPos.y)
        -- print("layoutPos : ", layoutPos.x, layoutPos.y)
        -- print(math.abs(vector2.x), math.abs(vector2.y))
        -- if math.abs(vector2.x) > 4 or math.abs(vector2.y) > 4 then
            -- self:setPosition(localPos.x - self:getContentSize().width/2, localPos.y - self:getContentSize().height/2)
            self:setPosition(vector2.x + self:getEndPos().x, vector2.y + self:getEndPos().y)
        -- end

    elseif eventType == ccui.TouchEventType.ended or eventType == ccui.TouchEventType.canceled then
        -- print("MJCardItem ccui.TouchEventType.ended and canceled")
        local beginPos = sender:getTouchBeganPosition()
        local worldPos = sender:getTouchEndPosition()
        local localPos = parent:convertToNodeSpace(worldPos)
        local vector2 = cc.p(worldPos.x - beginPos.x, worldPos.y - beginPos.y)
        local layoutPos = self:getLayoutTouchPos()
        if layoutPos then
            vector2 = cc.p(localPos.x - layoutPos.x, localPos.y - layoutPos.y)
        end
        -- print("beginPos : ", beginPos.x, beginPos.y)
        -- print("worldPos : ",worldPos.x, worldPos.y)
        -- print("localPos : ",localPos.x, localPos.y)
        -- print(math.abs(vector2.x), math.abs(vector2.y))
        -- 拖出范围
        if math.abs(vector2.x) <= 4 and math.abs(vector2.y) <= 4 then
            local playerManager = MJHelper:getPlayerManager(true)
            if playerManager:isOneClickOutCard() then
                self:oneClickCardEnd()
            else
                self:clickCardEnd()
            end
        else
            MJHelper:setSelectCard( nil )
            sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_LIGHT_CARD, { isShow = false })
            if localPos.y - self:getContentSize().height/2 >= self:getContentSize().height * 0.8 then
                -- 拖动出牌
                self:doOutCard()
            -- 取消出牌
            else
                sendMsg(PublicGameMsg.MSG_CCMJ_OUT_CARD_FAIL)
            end
        end
    end
end

function MJCardItem:doOutCard()
    sendMsg(PublicGameMsg.MSG_CCMJ_GAME_OPT, "out_card", self)
    -- sendMsg(PublicGameMsg.MSG_CCMJ_OUT_CARD_FAIL)
end

function MJCardItem:clickCardEnd()
    -- 双击出牌
    local lastSelect = MJHelper:getSelectCard()
    if lastSelect == self then
        MJHelper:setSelectCard( nil )
        sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_LIGHT_CARD, { isShow = false })
        self:doOutCard()
    else
        if lastSelect then
            local endpos = lastSelect:getEndPos()
            lastSelect:setPosition(endpos)
            lastSelect:setLocalZOrder(0)
            sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_LIGHT_CARD, { isShow = false })
        end
        local endpos = self:getEndPos()
        -- 设置新的当前点击牌
        self:setPosition(endpos.x, self:getContentSize().height*0.2)
        MJHelper:setSelectCard( self )
        sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_LIGHT_CARD, { isShow = true, card = self.m_card:getCard()})
    end
end

function MJCardItem:oneClickCardEnd()
    local gameManager = MJHelper:getGameManager(true)
    if gameManager:isChaTingState() then
        self:clickCardEnd()
    else
        self:doOutCard()
        MJHelper:setSelectCard( nil )
        sendMsg(PublicGameMsg.MSG_CCMJ_SHOW_LIGHT_CARD, { isShow = false })
    end  
end

function MJCardItem:setEndPos( pos )
    self.m_endPos = pos
end

function MJCardItem:getEndPos()
    return self.m_endPos
end

function MJCardItem:setLightCard( is )
    self.imgLightCard:setVisible(is)
end

return MJCardItem
