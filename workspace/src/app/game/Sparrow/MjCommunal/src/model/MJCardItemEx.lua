--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 带数量的麻将项

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local MJCardItem = MJHelper:loadClass("src.app.game.Sparrow.MjCommunal.src.model.MJCardItem")

local MJCardItemEx = class("MJCardItemEx", function ()
	return display.newNode()
end)

function MJCardItemEx:ctor()
	self:myInit()

	self:setupViews()
end

function MJCardItemEx:myInit()
	self.m_num = 0
	self.m_index = 1
end

function MJCardItemEx:setupViews()
	-- cardItem
	self.item = MJCardItem.new()
	self:addChild(self.item)

	-- num
	self.labelNum = cc.LabelAtlas:_create(":0", "number/mj_num_xiaoji.png", 16, 17, 48)
	self.labelNum:setAnchorPoint(cc.p(0.5, 0.5))
	self:addChild(self.labelNum)

	self.labelNumB = display.newTTFLabel({size = 24, font="ttf/jcy.TTF"})
	self.labelNumB:setColor(cc.c3b(110, 75, 71))
	self:addChild(self.labelNumB)
end

function MJCardItemEx:setData( data )
	self.item:setData(data)

	self.m_num = data.num or self.m_num
	self.m_index = data.index or self.m_index
	self:updateViews()
end	

function MJCardItemEx:updateViews()
	if self.m_num > 1 then
		local label
		if self.m_index == 6 then
			self.labelNumB:setVisible(true)
			self.labelNum:setVisible(false)
			self.labelNumB:setString("×" .. self.m_num)
			label = self.labelNumB
		else
			self.labelNumB:setVisible(false)
			self.labelNum:setVisible(true)
			self.labelNum:setString(":" .. self.m_num)
			label = self.labelNum
		end

		local size = self.item:getContentSize()
		if self.m_index == 1 then
			label:setScale(1)
			label:setRotation(0)
			label:setPosition(size.width/2, size.height + 5 + label:getContentSize().height/2)
		elseif self.m_index == 2 then
			-- label:setScale(0.7)
			label:setRotation(-90)
			label:setPosition(- 3 - label:getContentSize().height/2 * label:getScale() , size.height/2)
		elseif self.m_index == 3 or self.m_index == 5 or self.m_index == 6 or self.m_index == 7 then
			label:setScale(0.7)
			label:setRotation(0)
			label:setPosition(size.width/2, size.height + label:getContentSize().height/2)
		elseif self.m_index == 4 then
			-- label:setScale(0.7)
			label:setRotation(90)
			label:setPosition(3 + label:getContentSize().height/2 * label:getScale() + size.width , size.height/2)
		else
			print("[MJError] MJCardItemEx illegal vulue \'self.m_index\' ", self.m_index)
		end
	else
		self.labelNum:setVisible(false)
		self.labelNumB:setVisible(false)
	end
	self:setContentSize(self.item:getContentSize())
end

function MJCardItemEx:getCardData()
	return self.item:getCardData()
end

function MJCardItemEx:setLightCard( is )
	self.item:setLightCard(is)
end

return MJCardItemEx
