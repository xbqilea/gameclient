--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 麻将组合牌

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")

local MJWeaveItem = class("MJWeaveItem", function ()
	return display.newNode()
end)

function MJWeaveItem:ctor()
	self:myInit()

	self:setupViews()
end

function MJWeaveItem:myInit()
	self.m_weave = nil
    self.m_index = 1
end

function MJWeaveItem:setupViews()
	self.cardItems = {}
	local MJCardItemEx = MJHelper:loadClass("src.app.game.Sparrow.MjCommunal.src.model.MJCardItemEx")
	for i = 1, 5 do
		local item = MJCardItemEx.new()
		self:addChild(item, -i)
		table.insert(self.cardItems, item)
	end
end

function MJWeaveItem:setData( data )
	self.m_weave = data.weave
    self.m_index = data.index or self.m_index

	self:updateViews()
end

function MJWeaveItem:updateViews()
	local cards = self.m_weave:getCards()
	local states = self:getStates()
    -- dump(states)
    -- dump(cards)
	-- 设置数据
	for k, item in pairs(self.cardItems) do
		if cards[k] then
			local data = { card =cards[k], state = states[k], index = self.m_index, num = self.m_weave:getNumById(cards[k]:getId()) }
			item:setData(data)
			item:setVisible(true)
		else
			item:setVisible(false)
		end
	end

    local item = self.cardItems[1]
    local itemSize = item:getContentSize()
    self:initCardPos(itemSize)
    self:getPosIndex(itemSize)

	-- -- 设置位置
 --    if self.m_index == 1 or self.m_index == 3 or self.m_index == 5 or self.m_index == 6 then
 --    	for k, item in pairs(self.cardItems) do
 --    		item:setPosition((k - 1)*itemSize.width, 0)
 --    	end
 --    	-- 四风杠平铺
 --        local itemSize = item:getContentSize()
 --    	if self.m_weave:getOperCode() == MJDef.OPER.SI_FENG_GANG then
 --            self.cardItems[4]:setLocalZOrder(0)
 --            local newSize = cc.size(4*itemSize.width, itemSize.height)
 --    		self:setContentSize(newSize)
 --    	else
 --            local dis = 0
 --            if self.m_index == 1 then
 --                dis = 18
 --            else
 --                dis = 12
 --            end
            
 --            if self.cardItems[4]:isVisible() then
 --                self.cardItems[4]:setPosition(itemSize.width, dis)
 --                self.cardItems[4]:setLocalZOrder(100)
 --                local newSize = cc.size(3*itemSize.width, itemSize.height)
 --                self:setContentSize(newSize)
 --            else
 --                local newSize = cc.size((#cards)*itemSize.width, itemSize.height)
 --                self:setContentSize(newSize)
 --            end
 --    	end
        
 --        if self.m_index == 1 then
 --            local size = self:getContentSize()
 --            self:setScale(0.8)
 --            self:setContentSize(size.width * 0.8, size.height * 0.8)
 --        end

 --    elseif self.m_index == 2 or self.m_index == 4 then
 --        local cardHeight = 40
 --        local bgHeight = 10
 --    	for k, item in pairs(self.cardItems) do
 --    		item:setPosition(0, k*cardHeight + bgHeight)
 --    	end
 --    	-- 四风杠平铺
 --    	if self.m_weave:getOperCode() == MJDef.OPER.SI_FENG_GANG then
 --            self.cardItems[4]:setLocalZOrder(0)
 --            local newSize = cc.size(itemSize.width, 4*cardHeight + bgHeight)
 --            self:setContentSize(newSize)
 --    	else
 --    		self.cardItems[4]:setPosition(0, cardHeight*2 + bgHeight*2)
 --            self.cardItems[4]:setLocalZOrder(100)
 --            local newSize = cc.size(itemSize.width, 3*cardHeight + bgHeight)
 --            self:setContentSize(newSize)
 --    	end
 --        self:setScale(0.7)
 --        local size = self:getContentSize()
 --        self:setContentSize(size.width * 0.7, size.height * 0.7)
	-- end
end

function MJWeaveItem:initCardPos(itemSize)
    self.t_pos = {}
    -- 设置位置
    if self.m_index == 1 or self.m_index == 3 or self.m_index == 5 or self.m_index == 6 or self.m_index == 7 then
        for i = 1, 5 do
            self.t_pos[i] = cc.p((i - 1)*itemSize.width, 0)
        end
        
        local dis = 0
        if self.m_index == 1 then
            dis = 18
        else
            dis = 12
        end
        self.t_pos[6] = cc.p(itemSize.width, dis)

    elseif self.m_index == 2 or self.m_index == 4 then
        local cardHeight = 38
        local bgHeight = 12

        for i = 1, 5 do
            self.t_pos[i] = cc.p(0, i*cardHeight + bgHeight)
        end
        self.t_pos[6] = cc.p(0, cardHeight*2 + bgHeight*2)
    end
end

function MJWeaveItem:getPosIndex(itemSize)
    local cards = self.m_weave:getCards()
    local yaoji = self.m_weave:getYaoJi()
    -- 设置位置
    if self.m_index == 1 or self.m_index == 3 or self.m_index == 5 or self.m_index == 6 or self.m_index == 7 then
        for k, item in pairs(self.cardItems) do
            item:setPosition(self.t_pos[k])
        end
        -- 四风杠平铺
        if self.m_weave:getOperCode() == MJDef.OPER.SI_FENG_GANG then
            self.cardItems[4]:setLocalZOrder(-4)
            local newSize = cc.size(4*itemSize.width, itemSize.height)
            self:setContentSize(newSize)
            if yaoji then
                local newSize = cc.size(5*itemSize.width, itemSize.height)
                self:setContentSize(newSize)
            end
        else
            if self.cardItems[4]:isVisible() and not yaoji then
                self.cardItems[4]:setPosition(self.t_pos[6])
                self.cardItems[4]:setLocalZOrder(100)
                local newSize = cc.size(3*itemSize.width, itemSize.height)
                self:setContentSize(newSize)
            else
                self.cardItems[4]:setLocalZOrder(-4)
                local newSize = cc.size((#cards)*itemSize.width, itemSize.height)
                self:setContentSize(newSize)
            end
        end
        
        if self.m_index == 1 then
            local size = self:getContentSize()
            self:setScale(0.7)
            self:setContentSize(size.width * 0.7, size.height * 0.7)
        elseif self.m_index == 7 then
            local size = self:getContentSize()
            self:setScale(0.8)
            self:setContentSize(size.width * 0.8, size.height * 0.8)
        end

    elseif self.m_index == 2 or self.m_index == 4 then
        local cardHeight = 38
        local bgHeight = 12
        for k, item in pairs(self.cardItems) do
            item:setPosition(self.t_pos[k])
        end
        -- 四风杠平铺
        if self.m_weave:getOperCode() == MJDef.OPER.SI_FENG_GANG then
            self.cardItems[4]:setLocalZOrder(-4)
            local newSize = cc.size(itemSize.width, 4*cardHeight + bgHeight)
            self:setContentSize(newSize)
            if yaoji then
                local newSize = cc.size(itemSize.width, 5*cardHeight + bgHeight)
                self:setContentSize(newSize)
            end
        else
            if self.cardItems[4]:isVisible() and not yaoji then
                self.cardItems[4]:setPosition(self.t_pos[6])
                self.cardItems[4]:setLocalZOrder(100)
                local newSize = cc.size(itemSize.width, 3*cardHeight + bgHeight)
                self:setContentSize(newSize)
            else
                self.cardItems[4]:setLocalZOrder(-4)
                local newSize = cc.size(itemSize.width, (#cards)*cardHeight + bgHeight)
                self:setContentSize(newSize)
            end
        end
        self:setScale(0.7)
        local size = self:getContentSize()
        self:setContentSize(size.width * 0.7, size.height * 0.7)
    end
end

-- 组合牌状态
function MJWeaveItem:getStates()
	local states = {}
	if self.m_index == 1 then
    	if self.m_weave:getOperCode() == MJDef.OPER.AN_GANG then
    		states = {MJDef.eCardState.e_back_big, MJDef.eCardState.e_back_big, MJDef.eCardState.e_back_big, MJDef.eCardState.e_front_big2}
    	else
    		states = {MJDef.eCardState.e_front_big2, MJDef.eCardState.e_front_big2, MJDef.eCardState.e_front_big2, MJDef.eCardState.e_front_big2, MJDef.eCardState.e_front_big2}
    	end
    elseif self.m_index == 2 then
    	if self.m_weave:getOperCode() == MJDef.OPER.AN_GANG then
    		states = {MJDef.eCardState.e_right_back, MJDef.eCardState.e_right_back, MJDef.eCardState.e_right_back, MJDef.eCardState.e_right_back}
    	else
    		states = {MJDef.eCardState.e_right_front, MJDef.eCardState.e_right_front, MJDef.eCardState.e_right_front, MJDef.eCardState.e_right_front, MJDef.eCardState.e_right_front}
    	end
	elseif self.m_index == 3 then
		if self.m_weave:getOperCode() == MJDef.OPER.AN_GANG then
    		states = {MJDef.eCardState.e_back_small1, MJDef.eCardState.e_back_small1, MJDef.eCardState.e_back_small1, MJDef.eCardState.e_back_small1}
    	else
    		states = {MJDef.eCardState.e_front_small1, MJDef.eCardState.e_front_small1, MJDef.eCardState.e_front_small1, MJDef.eCardState.e_front_small1, MJDef.eCardState.e_front_small1}
    	end
    elseif self.m_index == 5 or self.m_index == 6 or self.m_index == 7 then
        if self.m_weave:getOperCode() == MJDef.OPER.AN_GANG then
            states = {MJDef.eCardState.e_back_small1, MJDef.eCardState.e_back_small1, MJDef.eCardState.e_back_small1, MJDef.eCardState.e_front_small1}
        else
            states = {MJDef.eCardState.e_front_small1, MJDef.eCardState.e_front_small1, MJDef.eCardState.e_front_small1, MJDef.eCardState.e_front_small1, MJDef.eCardState.e_front_small1}
        end
	elseif self.m_index == 4 then
		if self.m_weave:getOperCode() == MJDef.OPER.AN_GANG then
    		states = {MJDef.eCardState.e_left_back, MJDef.eCardState.e_left_back, MJDef.eCardState.e_left_back, MJDef.eCardState.e_left_back}
    	else
    		states = {MJDef.eCardState.e_left_front, MJDef.eCardState.e_left_front, MJDef.eCardState.e_left_front, MJDef.eCardState.e_left_front, MJDef.eCardState.e_left_front}
    	end
	end 
    return states
end

-- 是否是四风杠
function MJWeaveItem:isSiFengGang()
    return self.m_weave:getOperCode() == MJDef.OPER.SI_FENG_GANG
end

function MJWeaveItem:getCardItems()
    local t = {}
    for k, item in pairs(self.cardItems) do
        if item:isVisible() then
            table.insert(t, item)
        end
    end
    return t
end

return MJWeaveItem
