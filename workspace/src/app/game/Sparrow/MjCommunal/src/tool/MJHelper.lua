--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 麻将帮助类

local MJHelper = class("MJHelper")
local scheduler = require("framework.scheduler")

local __filepathList = {}
local __instancefilepathList = {}

local __gameOpenStateList = {}
local __init =false

function MJHelper:start(layer)
	if layer then
		layer:setKeypadEnabled(true)
		layer:addNodeEventListener(cc.KEYPAD_EVENT, function (event) 
				self:handleKeyEvent(event)
			end
		)
	end
end

function MJHelper:handleKeyEvent(event)
	--print(event.key,cc.KeyCode.KEY_F1,cc.KeyCode.KEY_Z,cc.KeyCode.KEY_A)
	if event.key == "back" then return end
	if event.key == "menu" then return end		
	local key = tonumber(event.key) - 3
	if key == cc.KeyCode.KEY_F1 then
		--print("fffffffff")
		for key,value in pairs(__filepathList) do
			package.loaded[key] = nil
		end
		__filepathList = {}
	elseif  key == cc.KeyCode.KEY_Z then
		local MJTestCode = self:loadClass("app.game.Sparrow.MjCommunal.src.tool.MJTestCode")
		MJTestCode.Ztest()
	elseif  key == cc.KeyCode.KEY_F4 then
		UIAdapter:popScene()
	end
	
	local filter = {}
	filter[cc.KeyCode.KEY_Z] = true
	filter[cc.KeyCode.KEY_F1] = true
	filter[cc.KeyCode.KEY_F4] = true
	
	if not filter[key]  then
		local MJGmCode = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.tool.MJGmCode")
		MJGmCode:DoKey(key)
	
		local MJTestCode = self:loadClass("app.game.Sparrow.MjCommunal.src.tool.MJTestCode")
		if MJTestCode.Keytest then
			MJTestCode.Keytest(key)
		end
	end
	
end

function MJHelper:init()
	__init = true
	for key,value in pairs(__filepathList) do
		package.loaded[key] = nil
	end
	__filepathList = {}
		
	for key,value in pairs(__instancefilepathList) do
		package.loaded[key] = nil
	end
	__instancefilepathList = {}
	__gameOpenStateList = {}
	
	self.soundManager = nil
	self.skin = nil
	--self.is_auto_test = true
end

function MJHelper:isInit()
	return __init
end

function MJHelper:setAutoTest(bool)
	self.is_auto_test = bool
end

function MJHelper:AutoTest(btn,clickFunc)
	if self.is_auto_test then
		if btn and clickFunc then
			performWithDelay(btn,function()  TOAST("AutoTest", nil) clickFunc(btn) end,5)
		end
	end
end

function MJHelper:updateGameOpenState(info)
	local list = __gameOpenStateList[info.m_portalId]
	if list then
		for k,v in pairs(list) do
			__gameOpenStateList[v] = nil 
		end
	end
	__gameOpenStateList[info.m_portalId] = {}
	list = __gameOpenStateList[info.m_portalId]
	for k,v in pairs (info.m_portalList) do
		__gameOpenStateList[v.m_portalId] = {}
		table.insert(list,v.m_portalId)
	end
end

function MJHelper:getGameOpenState(id)

	if __gameOpenStateList[id] then
		return 0
	else
		return 1
	end
end

function MJHelper:setReconnet(bool)
	self.is_reconnet = bool
end

function MJHelper:getReconnet()
	return self.is_reconnet
end

function MJHelper:setReady(bool)
	self.is_ready = bool
end

function MJHelper:getReady()
	return self.is_ready
end

function MJHelper:getShareConf()
	local obj = self:getConfig()
	local ShareConf = obj.ShareConf
	local m_type = ToolKit:checkRoomType()
	local param = obj.ShareConf[m_type]
	if not param then
		param = obj.ShareConf[0]
	end
	param = param or {}
	
	return param
end

function MJHelper:loadInstanceClass(__filepath)
	__instancefilepathList[__filepath] = true
	local obj = require(__filepath)
	return obj
end

function MJHelper:loadClass(__filepath)
	__filepathList[__filepath] = true
	local obj = require(__filepath)
	return obj
end

function MJHelper:getConfig()
	local obj = self:loadClass("app.game.Sparrow.MjCommunal.src.MJGameConf")
	return obj
end

function MJHelper:getSkin()
	self.skin = self.skin or self:loadClass("app.game.Sparrow.MjCommunal.src.MJSkin")
	return self.skin
end

function MJHelper:getUIHelper()
	local obj = self:loadClass("app.game.Sparrow.MjCommunal.src.MJUIHelper")
	return obj
end

function MJHelper:setPlayerManager(obj)
	self.playerManager = obj
end

function MJHelper:getPlayerManager(isVirtual)
    if isVirtual then
        if self.videoPlayerManager then
            return self.videoPlayerManager
        end
    end
	return self.playerManager
end

function MJHelper:setGameManager( obj )
	self.gameManager = obj
end

function MJHelper:getGameManager(isVirtual)
    if isVirtual then
        if self.videoGameManager then
            return self.videoGameManager
        end
    end
	return self.gameManager
end

function MJHelper:setVideoGameManager( obj )
	self.videoGameManager = obj
end

function MJHelper:setVideoPlayerManager(obj)
	self.videoPlayerManager = obj
end

function MJHelper:setSelectCard( card )
	self.selectCard = card
end

function MJHelper:getSelectCard()
	return self.selectCard
end

function MJHelper:STR( i )
	local t = require("app.game.Sparrow.MjCommunal.src.MJConfig.MJString")
	-- dump(t)
	if t[i] then
		return t[i]
	else
		print("[MJError] get nil STR with index ", i)
		return ""
	end
end

function MJHelper:setActionHelp( action )
	self.actionHelp = action
end

function MJHelper:getActionHelp()
	return self.actionHelp
end

function MJHelper:setOutCard( card )
	self.outCard = card
end

function MJHelper:getOutCard()
	return self.outCard
end

function MJHelper:getScreenPic( callback )
	local name = "MJGameScreen.jpg"
	
	local size = cc.Director:getInstance():getWinSize()
	--定义一个屏幕大小的渲染纹理
	local renderTexture = cc.RenderTexture:create(size.width,size.height, cc.TEXTURE2_D_PIXEL_FORMAT_RGB_A8888,gl.DEPTH24_STENCIL8_OES)
	
	renderTexture:beginWithClear(0.0,0.0,0.0,0.0)
	renderTexture:ignoreAnchorPointForPosition(true)
	renderTexture:setAnchorPoint(cc.p(0,0))
	--获得当前的场景指针
	local scene = cc.Director:getInstance():getRunningScene()
	scene:visit()
	renderTexture:endToLua()
	renderTexture:saveToFile(name, cc.IMAGE_FORMAT_JPEG)
	
	local fullPath = cc.FileUtils:getInstance():getWritablePath()..name
	local scheduler = require("framework.scheduler")
	local function saveback()
		callback(fullPath)
	end
	scheduler.performWithDelayGlobal(saveback,0.3)
end

function MJHelper:setSoundManager( manager )
	self.soundManager = manager
end

function MJHelper:getSoundManager()
	if not self.soundManager then
		local DZMJSoundManager = MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.controller.DZMJSoundManager")
		self.soundManager = DZMJSoundManager.new()
	end
	return self.soundManager
end

function MJHelper:setRecordManager( manager )
	self.recordManager = manager
end

function MJHelper:getRecordManager()
	return self.recordManager
end

return MJHelper
