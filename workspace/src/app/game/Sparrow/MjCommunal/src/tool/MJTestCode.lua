--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 麻将测试类

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local MJTestCode = {}

function MJTestCode:ctor()
	self:myInit()
end

function MJTestCode:myInit()
	
end



function MJTestCode.t(self)
	--setContentView
	local function _touchCallBack(sender)
		local name = sender:getName() 
		print("name: ", name)
	end
	local view = self--.__layout5
	local test = UIAdapter:createNode("csb/mahjong_main_scene.csb")
	view:addChild(test)
	UIAdapter:adapter(test,_touchCallBack)

		
end

local function bbb()
	local layer = cc.Layer:create()
	local scene = display.getRunningScene()
	scene:addChild(layer,CUR_GLOBAL_ZORDER)
	layer:setTouchEnabled(true)
	layer:setSwallowsTouches(false)
	
	local s_child = nil
	local e_child = nil
	local function onTouch(event,x,y)
		layer:removeFromParent()
	end
	layer:registerScriptTouchHandler(onTouch)
	
	local info = {}
	for i=1,50 do
		info[i] = i
	end
	local item_to_idx_list = {}
	local idx_to_item_list = {}
	local function delegate(listView, tag, idx)
		--print(string.format("TestUIListViewScene tag:%s, idx:%s", tostring(tag), tostring(idx)))
		if cc.ui.UIListView.COUNT_TAG == tag then
			--print(tag)
			return #info
		elseif cc.ui.UIListView.CELL_TAG == tag then
			print("xxx",tag)
			local item
			local content

			item = listView:dequeueItem()
			if not item then
				print("is new")
				item = listView:newItem()
				content = cc.ui.UILabel.new(
						{text = "item"..idx,
						size = 20,
						align = cc.ui.TEXT_ALIGN_CENTER,
						color = display.COLOR_WHITE})
				item:addContent(content)
			else
				print("is old")
				content = item:getContent()
			end
			idx_to_item_list[idx] = item
			item_to_idx_list[item] = idx
			content:setString("item:" .. info[idx])
			item:setItemSize(120, 80)

			return item
		else
		end
	end
	
	local function onMyTouch(event)
		--print(event.name)
		local listView = event.listView
		if "clicked" == event.name then
			print("async list view clicked idx:" .. event.itemPos)
			table.remove(info,event.itemPos)
			listView:reload()
			--listView:removeItem(event.item)
		end
	end
	
--    local lv = cc.ui.UIListView.new {
--        bgColor = cc.c4b(0, 0, 0, 120),
--        --bg = "sunset.png",
--        bgScale9 = true,
--        async = true, --异步加载
--        viewRect = cc.rect(200, 80, 120, 400),
--        direction = cc.ui.UIScrollView.DIRECTION_VERTICAL,
--        }
--        :onTouch(onMyTouch)
--		:addTo(layer)

--   lv:setDelegate(delegate)
--   lv:reload()
--	UIAdapter:transNode(lv.touchNode_)


	local  onLabel = display.newTTFLabel({
				text = "on",
				size = 24,
				x = 0,
				y = 0,
				color = cc.c3b(0,0,0)
			}			
		)  
	local  offLabel = display.newTTFLabel({
				text = "off",
				size = 24,
				x = 0,
				y = 0,
				color = cc.c3b(0,0,0)
			}			
		)  
    
    local maskSprite = display.newSprite("ui/mj_bg_swith2.png")
    local onSprite = display.newSprite("#mj_bg_open_swith.png")
    local offSprite = display.newSprite("#mj_bg_swith.png")
    local thumbSprite = display.newSprite("#mj_btn_swith.png")
	maskSprite:setScale(1.5)
	local size = onSprite:getContentSize()
    --设置按钮的截取范围 开关图片和显示文字以及按钮  
	local switchBtn = cc.ControlSwitch:create(maskSprite, onSprite, offSprite, thumbSprite, onLabel, offLabel);  
    layer:addChild(switchBtn)
	switchBtn:setPosition(cc.p(640,360))

--	local label = cc.LabelAtlas:_create("1234567","number/mj_game_num_time.png",17,30,49)
--	layer:addChild(label)
--	label:setPosition(cc.p(640,700))
--	label:setScale(3)
--	local snow = cc.ParticleRain:create()
--	layer:addChild(snow)


--	local spriteFrame = display.newSpriteFrame("mj_btn_free.png")

--	local texture = spriteFrame:getTexture()
--	local rect = spriteFrame:getRect()
--	snow:setTextureWithRect(texture,rect)
--	dump(rect)
--	snow:setStartSize(30)
--	snow:setEndSize(20)

--	snow:setStartSpin(80)
--	snow:setStartSpinVar(60)
--	snow:setEndSpin(10)
--	snow:setEndSpinVar(60)



end

function MJTestCode.Ztest()
	--print(_VERSION)
	--package.loaded = nil
	--local test = require("app.game.Sparrow.MjCommunal.src.MJRoom.RichText")
	--package.loaded = nil
	--print(test)

--print(utf8.char(97,98))
-- local utf8 = require("utf8")
-- print("-->",utf8)
-- -- dump(io)
-- dump(debug)
    --dlg:enableTouch(false)
-- --    dlg:setCountDownTime(10)
-- 	 local socket = require("socket")
	
-- 	-- local  soc = cc.net.SocketTCP.new("192.168.204.128", 8888)
	
-- 	-- local socket = cc.net.SocketTCP.new("192.168.204.128", 8888)
-- 	-- socket.send("hello")
-- 	-- socket.connect()
-- 	--cc.utils = require("framework.cc.utils.init")
-- -- cc.net = require("framework.cc.net.init")
-- 	--local tet = require("protobuf")
-- 	--print(os.time())
-- 	function GetAdd(hostname)
-- 		local ip, resolved = socket.dns.toip(hostname)
-- 		local ListTab = {}
-- 		for k, v in ipairs(resolved.ip) do
-- 			table.insert(ListTab, v)
-- 		end
-- 		print(ListTab)
-- 		return ListTab
-- 	end
-- 	print(socket)
-- 	local host = unpack(GetAdd(socket.dns.gethostname()))
-- 	print(socket.dns)
-- 	print(socket.dns.gethostname())
-- 	print(host)


	-- local RichText =  MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.RichText")
	 -- local mjHelpDlg =  MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJHelpDlg")
	 -- mjHelpDlg.new(120):showDialog()


	-- local tt = {
	-- 	{
	-- 		str = "aaaaa",
	-- 		color = 0x00f00f
	-- 	},
	-- 	{
	-- 		str = "中国惹大概每dafae年都的那个",
	-- 		color = 0x00ffff
	-- 	},
	-- 	{
	-- 		str = "中国惹大概每dafae年都的那个",
	-- 		color = 0x00ffff
	-- 	}
	-- print[[jciadjiagn;ajdfgi
	-- ]]
	-- local data = {}
	-- data.m_itemNum = 20 
	-- data.m_toNickname = "游客0000000"
	-- data.m_toPlayer = 11116789

	--          local MJDonateTip =  MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJRoom.MJDonateTipDlg")
 --            local mjDonateTip = MJDonateTip.new(data)
 --            mjDonateTip:showDialog()


    -- local realSize = display.size  --实际分辨率
    -- local scaley = realSize.height / MY_CONFIG_SCREEN_HEIGHT  --实际分辨率
    -- print(realSize.height,MY_CONFIG_SCREEN_HEIGHT)
    -- print(scaley)
    -- print(display.scaleY)
	--local test = display.newSprite()
	--  local pop_window = display.newColorLayer(cc.c4b(255, 255, 255, 200))       -- 半透明的黑色  
 --    pop_window:setContentSize(display.width, display.height)             -- 设置Layer的大小,全屏出现  
 --    pop_window:align(display.CENTER, display.width/2, display.height/2)  
	-- display.getRunningScene():addChild(pop_window,0x00fffffe)

	-- local label = display.newTTFLabel({
 --    	text = "你好就低昂" ,
	--     font = "ttf/jcy.TTF",
	--     size = 20 ,
	--     --color = cc.c3b(110, 75, 71), -- 使用纯红色
	--     align = cc.TEXT_ALIGNMENT_LEFT,
	--     valign = cc.VERTICAL_TEXT_ALIGNMENT_TOP,
	-- 	}):pos(200,200)
	-- label:setTextColor(cc.c4b(255,0,0,255))
	-- label:enableOutline(cc.c4b(255, 255, 255,255),2)

	-- 	dump(label:getContentSize())
	-- local len1 = label:getContentSize().width

	-- local str = display.newTTFLabel({
 --    	text = "三三丰嫁给你 三三丰嫁给你 三三丰嫁给你 三三丰嫁给你 三三丰嫁给你" ,
	--     font = "ttf/jcy.TTF",
	--     size = 20 ,
	--     color = cc.c3b(110, 75, 71), -- 使用纯红色
	--     align = cc.TEXT_ALIGNMENT_LEFT,
	--     valign = cc.VERTICAL_TEXT_ALIGNMENT_TOP,
	-- 	}):pos(400,50)
	-- 	dump(str:getContentSize())

	-- local len2 = str:getContentSize().width	

	-- local triStr = ToolKit:getMaxUtf8String("三三丰嫁给你  三三丰嫁给你 三三丰嫁给你 三三丰嫁给你给你给你给你给你给你        三丰嫁给你", 82)
	-- triStr = triStr .. "..."
	-- --local triStr = string.sub("三三丰嫁给你 三三丰嫁给你 三三丰嫁给你 三三丰嫁给你 三三丰嫁给你",1,82)
	-- local test = display.newTTFLabel({
 --    	text = triStr ,
	--     font = "ttf/jcy.TTF",
	--     size = 20 ,
	--     color = cc.c3b(110, 75, 71), -- 使用纯红色
	--     align = cc.TEXT_ALIGNMENT_LEFT,
	--     valign = cc.VERTICAL_TEXT_ALIGNMENT_TOP,
	--     dimensions = cc.size(280, 40)
	-- 	}):pos(400,200)
	-- dump(test:getContentSize())

	-- local len3 = 400

	-- print(string.len("年纪地方噶嘛你的麻烦给的价格接待费估计都没基地啊给你发面"))
	-- 	print(string.len("..."))
	-- print(string.sub("三三 丰嫁给你",1,3*3))
	-- -- display.getRunningScene():addChild(test,0x00ffffff)
	-- -- display.getRunningScene():addChild(str,0x00ffffff)	
	-- display.getRunningScene():addChild(label,0x00ffffff)
	-- if "" then 
	-- 	print(111)
	-- else
	-- 	print(222)
	-- end

	-- }
	-- local richLabel = RichText.new(tt,80,cc.size(200,100))
	-- richLabel:setPosition(300,500)
	-- --richLabel:setFontSize(30)
	-- --richLabel:setContentSize(cc.size(300,50))
-- local test = {
-- 	 ["1"] = 2,
-- 	 ["2"] = 7,
-- 	 ["7"] = 5,
-- 	 ["3"] = 7,
-- 	 ["12"] = 7,
-- 	 ["4"]  = 8
-- }

-- local cjson = require "cjson"
-- --local sampleJson = [[{"age":"23","testArray":{"array":[8,9,11,14,25]},"Himi":"himigame.com"}]];
-- --解析json字符串
-- local data = cjson.decode([[{"2"}]]);
-- print(data)
-- --打印json字符串中的age字段
-- print(data["age"]);
-- --打印数组中的第一个值(lua默认是从0开始计数)
-- print(data["testArray"]["array"][1]);  
--  dump(test)

 -- 	local _hexNum = "0x" .. tostring(string.format("%06x","0xff33ff"))
 -- print(_hexNum)
 -- print(type(_hexNum))


	--  local scene =  display.getRunningScene()
 --     scene:addChild(richLabel, 0x00ffffff)


 	 -- local test =  MJHelper:loadClass("app.Sparrow.MjCommunal.src.MJRoom.MJDonateData")
 	 -- print(test:getRound1Limit())
 	 -- print(test:getRound2Limit())
 	 -- print(test:getMinCoinLimit())
 	 -- print(tonumber(""))

 --    local test = 0x0035ba
 --    print("0x" .. tostring(string.format("%06x",test)))


	--local MJCreateRoomData = MJHelper:loadClass("app.Sparrow.MjCommunal.src.MJRoom.MJDaiBiManagerDlg")
	-- local test = MJCreateRoomData.new()
	-- test:showDialog()
	--print(string.find("chenfeng","en",5))

-- print(Player:getFaceID())

	--   local MJCreateRoomData = MJHelper:loadClass("app.Sparrow.MjCommunal.src.MJRoom.MJCreateRoomData")
	--   dump(MJCreateRoomData:getPlayerNumById(120001))
	-- -- dump(MJCreateRoomData:getLimit())
	-- print(MJCreateRoomData:getLimitDaiBiById(120010))
	-- print(MJCreateRoomData:getLimitRoundById(120010))
	-- print(MJCreateRoomData:getLimitTimesById(120010))
	-- print(MJCreateRoomData:getLimitBaseById(120010))


   

   -- local MJCreateRoomData = require("app.Sparrow.MjCommunal.src.MJRoom.MJCreateRoomData")
   -- print(MJCreateRoomData:IsPaiyouRoomById(125001))
	-- print(unpack(GetAdd('localhost')))
	-- print(unpack(GetAdd(socket.dns.gethostname())))
	
--[[	local settingDialog = require("app.Total.view.lobby.newSetting.NewSettingLayer")
                        settingDialog.new():showDialog();
	--bbb()
	 local bankDialog = require("app.Total.view.lobby.newBank.BankHomeLayer")
	 bankDialog.new():showDialog();--]]
	
	--print(device.gettime())
--[[	local MJHelper = require("app.Sparrow.MjCommunal.src.tool.MJHelper")
	local MJVipRoomController = MJHelper:loadInstanceClass("app.Sparrow.MjCommunal.src.MJRoom.MJVipRoomController")
	local roomController = MJVipRoomController:getInstance()
	roomController.gameScene.ccmjVipWaitLayer.testMain:next()--]]

end

function MJTestCode.Keytest(key)
	if key== cc.KeyCode.KEY_R then
		--doPlay()
	end
	
	if key== cc.KeyCode.KEY_T then
			local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
		local DZMJRecordManagerLayer =  MJHelper:loadClass("app.game.Sparrow.GeneralMJ.src.view.DZMJRecordManagerLayer")
		local dlg = DZMJRecordManagerLayer.new()
		dlg:showDialog()
	end
	
	if key == cc.KeyCode.KEY_F3 then
		local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
		MJHelper:setAutoTest(true)
	end
	
	if key == cc.KeyCode.KEY_F9 then
		print(cc.Director:getInstance():isPaused())
		if cc.Director:getInstance():isPaused() then
			local scheduler = require("framework.scheduler")
			scheduler.performWithDelayGlobal(function()display.pause() end,0)
			display.resume()
			--display.pause()
		else	
			display.pause()
		end
		
	end
	
	if key == cc.KeyCode.KEY_F10 then
		display.resume()
	end
	
	if key == cc.KeyCode.KEY_V then
	print(cc.FileUtils:getInstance():getSearchPaths())
	end
end


return MJTestCode
