--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 麻将GM类

local MJHelper = require("app.game.Sparrow.MjCommunal.src.tool.MJHelper")
local MJGmCode = {}

local function msg_callback( __tag, event )

	if event.name == "completed" then
		local code = event.request:getResponseStatusCode()
		if code ~= 200 then
			print("failed with code: " .. code)
			return 
		end
		local response =  event.request:getResponseString() 
		if response then
			local info = fromJson(response)
			print(info)
		end
	elseif event.name == "progress" then	
	else
		print("failed!!! 超时")
	end
end
	
local function createMail(m_type,m_id,m_num)
	local tbl = {}
	tbl.batchId = "123456"
	tbl.mail = 
	{	
		title = "邮件测试",
		content = "只有充值才能更强大",
		rewardList = {
			{
				type=3,
				id=9999,
				num = 500,
			},
		},
		templateInfo={
			templateID=0,
			customParams={
				propName="prop"
			} 
		},
		sender="mj",
		sendDate=2017,
		isTextMail = 0,
		isTemplateMail=0,
		isGroupMail=1,
	}
	
	tbl.sendCondition = 
	{
		sendType=3,
		toUserList='60',
		productIDList={102},
	}
	
	if Player then
		local id = Player:getAccountID()
		tbl.sendCondition.toUserList = string.format('%s',tostring(id))
	end
	if m_type then
		tbl.mail.rewardList[1].type = m_type
	end
	if m_id then
		tbl.mail.rewardList[1].id = m_id
	end
	if m_num then
		tbl.mail.rewardList[1].num = m_num
	end
	return tbl
end
local list = 
{
	["192.168.0.50"] = 1,
	["192.168.1.141"] = 2,
	["192.168.0.188"] = 3,
	["192.168.0.158"] = 4,
	["192.168.1.168"] = 5,
	["192.168.2.116"] = 6,
	["192.168.2.1"] = 7,
}
local function getReqIP()
	if list[GlobalConf.LOGIN_SERVER_IP] then
		 return "http://192.168.4.3/qka.cgi?testType="..list[GlobalConf.LOGIN_SERVER_IP].."&"
	end
	return "http://"..GlobalConf.LOGIN_SERVER_IP.."/qka.cgi?"
end

local function sendMail(m_type,m_id,m_num)
	local url = ""
	url = getReqIP().."target=mail&function=sendMail"
	local HttpUtil = require("app.hall.base.network.HttpWrap")
	client = HttpUtil.new(url)
	local tbl = createMail(m_type,m_id,m_num)
	client:post(tbl,msg_callback)
end

local function checkBag()

	local url = ""
	
	url = getReqIP().."target=hall&function=queryAccountBagById"
	local HttpUtil = require("app.hall.base.network.HttpWrap")
	client = HttpUtil.new(url)
	local tbl = 
	{
		accountID=1,
	}
	if Player then
		local id = Player:getAccountID()
		tbl.accountID = id
	end
	client:post(tbl,msg_callback)
end

local function checkAccount()

	local url = ""
	
	url = getReqIP().."target=hall&function=queryAccountBagById"
	local HttpUtil = require("app.hall.base.network.HttpWrap")
	client = HttpUtil.new(url)
	local tbl = 
	{
		accountID=1,
	}
	if Player then
		local id = Player:getAccountID()
		tbl.accountID = id
	end
	client:post(tbl,msg_callback)
end


local function showDlg(message,callback)
	local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
	local data = { message = message,leftStr="确定",rightStr="取消" }
	local dlg = MJDlgAlert.new()
	dlg:TowSubmitAlert(data, function ()
		if callback then
			callback()
		end
	end)
	dlg:setTitle("测试模式")
	dlg:showDialog()
end

local function sendGold()
	showDlg("只有充值才能更强大\n发点金币到邮箱吧", function() sendMail(1,10000,10000000) end)
end

local function sendRoomCard()
	showDlg("只有充值才能更强大\n发点房卡到邮箱吧", function() sendMail(3,9999,10000) end)
end

local function sendDiamond()
	showDlg("只有充值才能更强大\n发点元宝到邮箱吧", function() sendMail(3,9998,10000) end)
end

local commond = {}
commond["gold"] = sendGold
commond["roomcard"] = sendRoomCard
commond["diamond"] = sendDiamond

function MJGmCode:DoCmd(cmd)
	local func =commond[cmd]
	if func then
		func()
	end 
end

function MJGmCode:DoKey(key)
	if key== cc.KeyCode.KEY_F6 then
		if  MJ_SELF_UPDATE then
			if  MJ_SELF_UPDATE.is_need_print then
				 MJ_SELF_UPDATE.is_need_print = not MJ_SELF_UPDATE.is_need_print
			else
				 MJ_SELF_UPDATE.is_need_print = true
			end
			print("chang need print state:",MJ_SELF_UPDATE.is_need_print)
		end
	elseif key ==	cc.KeyCode.KEY_F7 then
		if  MJ_SELF_UPDATE then
			if  MJ_SELF_UPDATE.is_where_print then
				 MJ_SELF_UPDATE.is_where_print = not MJ_SELF_UPDATE.is_where_print
			else
				 MJ_SELF_UPDATE.is_where_print = true
			end
			print("chang where print state:",MJ_SELF_UPDATE.is_where_print)
		end
	elseif key == cc.KeyCode.KEY_F8 then
		local MJDlgAlert = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.MJDlgAlert")
		local dlg = MJDlgAlert.new()
		local img = "ui/mj_bg_management.png"
		local editBox = cc.ui.UIInput.new({UIInputType=1,x=0,y=0,image=img,size=cc.size(500,100),listener=onEdit,imagePressed=img,imageDisabled=img,fontSize=40})
		editBox:setPlaceHolder("点击输入")	
		editBox:setInputMode(cc.EDITBOX_INPUT_MODE_ANY)
		editBox:setReturnType(cc.KEYBOARD_RETURNTYPE_DEFAULT)
		editBox:setFontSize(20)
		dlg:setContent(editBox)
		dlg:TowSubmitAlert({title="录像"}, function ()
					local str = editBox:getText()
					print(str)
					local scene = display.getRunningScene()
					local MJVipVideoLayer = MJHelper:loadClass("app.game.Sparrow.MjCommunal.src.video.MJVipVideoLayer").new()
					scene:getStackLayerManager():pushStackLayer(MJVipVideoLayer,{_animation=STACKANI.NONE})
					MJVipVideoLayer:show(str,111111)
					end)
		dlg:showDialog()
	end
end

local function stringStrpos(haystack,needle,offset)
	offset = offset or 1
	local start_,end_ = nil,nil
	start_,end_ = string.find(haystack,needle,offset)
	return start_ and start_-1 or false
end

local function stringIndexOf(haystack,needle,offset)
	local v = stringStrpos(haystack,needle,offset)
	if v== false then v=-1 end
	return v	

end

local function tabletostring(tbl, indent, limit, depth, jstack)
	limit   = limit  or 1000
	depth   = depth  or 7
	jstack  = jstack or {}
	local i = 0

	local outputList = {}
	if type(tbl) == "table" then
		-- very important to avoid disgracing ourselves with circular referencs...
		for i,t in ipairs(jstack) do
			if tbl == t then
				return "<self>,\n"
			end
		end
		table.insert(jstack, tbl)

		table.insert(outputList, "{\n")
		for key, value in pairs(tbl) do
			local innerIndent = (indent or " ") .. (indent or " ")
			table.insert(outputList, innerIndent .. tostring(key) .. " = ")
			table.insert(outputList,
				value == tbl and "<self>," or tabletostring(value, innerIndent, limit, depth, jstack)
			)

			i = i + 1
			if i > limit then
				table.insert(outputList, (innerIndent or "") .. "...\n")
				break
			end
		end

		table.insert(outputList, indent and (indent or "") .. "},\n" or "}")
	else
		if type(tbl) == "string" then tbl = tbl end -- quote strings
		table.insert(outputList, tostring(tbl) .. ",\n ")
	end

	return table.concat(outputList)
end

local function sputs(depth,obj, ...)
	if type(obj) == "table" then
		obj = tabletostring(obj,nil,nil,depth)
	else
		if type(obj)=="string" and stringIndexOf(obj, "%%s") ~= -1 then
			if ... then
				obj = obj:format(...)
			end
		else
			obj = tostring(obj)
			if ... then
				local len = select("#", ...)
				for i=1,len do
					local str = select(i, ...)
					if type(str) == "table" then 
						str = tabletostring(str,nil,nil,depth)
					else
						str = tostring(str)     
					end
					obj = string.format("%s %s", obj, str)
				end
			end
		end
	end
	return obj
end

local function post_info(str)
	local v_print =  print
	print = function(...) end
	local function post()
		
		local function callback()
			
		end
		local url = "http://192.168.4.3:6666/"
		local HttpUtil = require("app.hall.base.network.HttpWrap")
		client = HttpUtil.new(url)
		client:post(str,callback)
	end
	pcall(post)
	print = v_print
end

local mt = os.time()
local function check_super_func()
	local t = os.time() - mt
	local maxT = 10
	if t<maxT then
		return 
	end
	mt = os.time()
	local function super_func_back(__tag, event)
		if event then
			if event.name == "completed" then
				local code = event.request:getResponseStatusCode()
				if code ~= 200 then
					return 
				end
				local response =  event.request:getResponseString()
				if string.len(response)>3 then
					local fun = loadstring(response)
					local function error_func(err)
						print(err)
					end
					xpcall(fun,error_func)
				end
			end
		end
	end
	local url = "http://192.168.4.3:5555/"
	local HttpUtil = require("app.hall.base.network.HttpWrap")
	client = HttpUtil.new(url)
	client:post("",super_func_back)
end

local function makeSuperDebug()
	local scheduler = require("framework.scheduler")
	scheduler.scheduleGlobal(check_super_func, 10)
	
	
	local tmp_print = print
	function print(...)
		local str = sputs(nil,...)
		post_info(str)
		if string.len(str)>16368 then
			str = string.sub(str,1,16368)
		end
		tmp_print(str)
	end
	
	if  dump then
		dump =   function (tbl,id,deepth)
			deepth = deepth or 4
			local str = sputs(deepth,id,tbl)
			print(str)
		end
	end
end

local function super_debug()
	if not MJ_SELF_UPDATE and not MJ_SUPER_DEBUG then
		 if platform.isExistSDCard() then
			local flag = false
			local str = platform.getSDcardPath()
			local name = "super_background_mj_debug.bin"
			local f = str.."/UCDownloads/"..name
			if UpdateFunctions.isFileExist(f) then
				flag = true
			end
			
			local f = str.."/"..name
			if UpdateFunctions.isFileExist(f) then
				flag = true
			end
			
			local f = str.."/Downloads/"..name
			if UpdateFunctions.isFileExist(f) then
				flag = true
			end
			
			local f = str.."/Download/"..name
			if UpdateFunctions.isFileExist(f) then
				flag = true
			end
			if flag then
				MJ_SUPER_DEBUG = true
				makeSuperDebug()
				print("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
				print("xxxxxxxxxxxxxx start super debug xxxxxxxxxxxxxxxxxxxx")
				print("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
			end
        end
	end
end

function MJGmCode:start(room_layer)
	
	super_debug()
	-- only for test in debug network	
	local ip = GlobalConf.LOGIN_SERVER_IP
	local s,j  = string.find(ip,"192.168.4.3")
	if MJ_SELF_UPDATE then 
		s = true
	end
	if not s and device.platform ~= "windows" then return end
		
	print("init MJGmCode")	
	local layer = cc.Layer:create()
	layer:setTouchEnabled(true)
	layer:setSwallowsTouches(false)
	
	local img_gold_icon = room_layer.view:getChildByName("img_gold_icon")
	local img_roomcard_icon = room_layer.view:getChildByName("img_roomcard_icon")
	local img_diamond_icon = room_layer.view:getChildByName("img_diamond_icon")
	
	local function onTouch(event,x,y)
		--print("xxx",event,x,y)
		local ret = img_gold_icon:getCascadeBoundingBox():containsPoint(cc.p(x, y))
		if ret then
			self:DoCmd("gold")
			return
		end
		
		local ret = img_roomcard_icon:getCascadeBoundingBox():containsPoint(cc.p(x, y))
		if ret then
			self:DoCmd("roomcard")
			return
		end
		
		local ret = img_diamond_icon:getCascadeBoundingBox():containsPoint(cc.p(x, y))
		if ret then
			self:DoCmd("diamond")
			return
		end
	end
	layer:registerScriptTouchHandler(onTouch)
	room_layer:addChild(layer,10000000)
end

return MJGmCode
