module(..., package.seeall)

---------------------------------------------麻将大厅-------------------------------------------

---------------------------------------------代币信息管理-------------------------------------------
-- 赠送代币
CS_C2A_Mj_PresentAgentGold_Req = 
{
		{1, 1, 'm_nObjAccountId', 'UINT', 1, '目标账号id'},
		{2, 1, 'm_nAgentGoldCount', 'UINT', 1, '赠送数量'},
}

CS_A2C_Mj_PresentAgentGold_Ack = 
{
		{1, 1, 'm_nRet', 'UBYTE', 1, '结果,0:成功 1:失败'},
}

-- 请求赠送代币按钮状态
CS_C2A_Mj_GetAgentGoldInfo_Req =
{
}

CS_A2C_Mj_GetAgentGoldInfo_Ack = 
{
		{1, 1, 'm_nStat', 'UBYTE', 1, '获得代币按钮状态，0：未激活 1：激活，但赠送代币要保留500代币 2:激活，无限制'},
		{2, 1, 'm_nDailyCount', 'UINT', 1, '今日获得代币数量'},
		{3, 1, 'm_nTotalCount', 'UINT', 1, '总计获得代币数量'},
		{4, 1, 'm_strRecord', 'STRING', 1024, '代币记录'},
}

-- 通知大厅代币信息变化
SS_M2A_Mj_UpdateAgentGoldInfo_Nty =
{
		{1, 1, 'm_nAccountId', 'UINT', 1, '账号id'},
		{2, 1, 'm_nHallSrvId', 'UINT', 1, '所在大厅服务id'},
}

---------------------------------------------任务子模块-------------------------------------------
-- 加载玩家任务信息
SS_M2A_Mj_LoadTaskInfo_Nty =
{
		{1, 1, 'm_nAccountId', 'UINT', 1, '账号id'},
}

-- 更新任务进度
SS_M2A_Mj_UpdateTaskProgress_Nty =
{
		{1, 1, 'm_nAccountId', 'UINT', 1, '账号id'},
		{2, 1, 'm_nHallSrvId', 'UINT', 1, '所在大厅服务id'},
		{3, 1, 'm_nTaskId', 'UBYTE', 1, '任务id'},
		{4, 1, 'm_strTaskValue', 'STRING', 1, '完成进度值，json类型'},
}

---------------------------------------------玩家游戏信息子模块-------------------------------------------
-- 加载玩家游戏信息
SS_M2A_Mj_LoadGameInfo_Req =
{
		{1, 1, 'm_nAccountId', 'UINT', 1, '账号id'},
}

SS_A2M_Mj_LoadGameInfo_Ack =
{
		{1, 1, 'm_nAccountId', 'UINT', 1, '账号id'},
		{2, 1, 'm_vstPlayCount', 'PstPlayRoundCount', 1024, '总局数'},
}

-- 更新玩家游戏信息
SS_M2A_Mj_UpdateGameInfo_Nty =
{
		{1, 1, 'm_nAccountId', 'UINT', 1, '账号id'},
		{2, 1, 'm_nGameAtomTypeId', 'UINT', 1, '游戏id'},
		{3, 1, 'm_nPlayCount', 'UBYTE', 1, '当次在房间玩了多少局'},
}

---------------------------------------------房间信息管理子模块-------------------------------------------
-- 服务器间通信协议
-- 创建房间ID请求
SS_M2A_GenRoomId_Req =
{
	{ 1, 1, 'm_nGameAtomTypeId', 'UINT', 1, 'game atom type id'},
	{ 2, 1, 'm_nAsyncTaskId', 'UINT', 1, 'async task id'},
	{ 3, 1, 'm_nRoomType', 'UINT', 1, '房间类型'},
	{ 4, 1, 'm_nAccountId', 'UINT', 1, 'accountId'},
	{ 5, 1, 'm_nIsForOtherPlayer', 'UINT', 1, '是否帮别人开房'},
	{ 6, 1, 'm_stInitData', 'PstPyInitData', 1, '玩法信息'},
}

-- 创建房间ID响应
SS_A2M_GenRoomId_Ack =
{
	{ 1, 1, 'm_nGameAtomTypeId', 'UINT', 1, 'game atom type id'},
	{ 2, 1, 'm_nAsyncTaskId', 'UINT', 1, 'async task id'},
	{ 3, 1, 'm_nRet', 'UINT', 1, '0:成功 1:没有空闲的房间号可分配 2:已经有自己的房间，不能再创建房间'},
	{ 4, 1, 'm_nRoomId', 'UINT', 1, 'id'},
	{ 5, 1, 'm_nAccountId', 'UINT', 1, 'accountId'},
	{ 6, 1, 'm_nRoomType', 'UINT', 1, '房间类型'},
}

-- 回收房间ID通知
SS_M2A_ReuseRoomId_Nty =
{
	{ 1, 1, 'm_nRoomId', 'UINT', 1, '房间ID'},
}

-- 确认使用房间ID
SS_M2A_UsedRoomId_Nty =
{
	{ 1, 1, 'm_nRoomId', 'UINT', 1, '房间ID'},
}

-- 大厅向场景获取房间列表信息请求
SS_A2M_GetFriendRoomInfo_Req = 
{
	{1,1,'m_vectRoomCreaterAcId','UINT',				1024,'房主账号ID'}, 
}

-- 大厅向场景获取房间列表信息返回
SS_M2A_GetFriendRoomInfo_Ack = 
{
	{1,1,'nRoomId','UINT',				1,'该名玩家创建的房间,简称:房间列表'}, 
	{2,1,'m_arr_stRoundInfo','PstPyTableRoundInfo',1,'游戏服告诉场景服的,场景服转发到客户端,数组对齐'},
	{3,1,'m_arr_stInitData','PstPyInitData',		1,'玩法信息,牌友房转发客户端的,金币房读取csv的'},
	{4,1,'m_nCurPlayerCount','UINT',1,'当前玩家数,最大玩家数在PstPyInitdata里'},
	{5,1,'m_nRoomStat','UINT',1,'-1:无意义  0:开放中  1:游戏中  2:解散中  3:已经消失的  4:扩展...'},
	{6,1,'m_nPlayerBrifInfo','PstPyRoomPlayerBrifInfo',4,'玩家简要信息'},
	
}

-- 场景向大厅更新房间状态信息通知
SS_M2A_UpdateFriendRoomState_Nty = 
{
	{1,1,'m_nRoomId','UINT',				1,'房间ID'}, 
	{2,1,'m_nRoundInfo','PstPyTableRoundInfo',1,'局信息'},
	{3,1,'m_nCurPlayerCount','UINT',		1,'当前玩家数'},
	{4,1,'m_nRoomStat','UINT',1,'-1:无意义  0:开放中  1:游戏中  2:解散中  3:已经消失的  4:扩展...'},
	{5,1,'m_nPlayerBrifInfo','PstPyRoomPlayerBrifInfo',4,'玩家简要信息'},
}

-- 获取玩家所在房间信息（代理客户端协议）
SS_A2M_GetCurrentRoom_Nty = 
{
	{1,1,'m_nAccountId','UINT',				1,'玩家账号ID'}, 
}

-- 场景初始化时通知
SS_M2A_SceneInit_Nty =
{
	{ 1, 1, 'm_nGameAtomTypeId', 'UINT', 1, '游戏类型ID'},
}

-- 场景通知大厅玩家进入房间(不用)
SS_M2A_PlayerEnterRoom_Nty =
{
	{1,1,'m_nAccountId','UINT',				1,'玩家账号ID'}, 
	{2,1,'m_nRoomId','UINT',				1,'房间号ID'}, 
}

-- 场景通知大厅玩家离开房间(不用)
SS_M2A_PlayerLaveRoom_Nty =
{
	{1,1,'m_nAccountId','UINT',				1,'玩家账号ID'}, 
	{2,1,'m_nRoomId','UINT',				1,'房间号ID'}, 
}

-- 麻将大厅向dbp加载房间信息请求
SS_A2D_LoadRoomInfo_Req = 
{
	{1, 1, "m_nBeginId", "UINT", 1, "房间查询开始id"},
	{2, 1, "m_nEndId", "UINT", 1, "房间查询结束id"}
}

-- 麻将大厅向dbp加载房间信息返回
SS_D2A_LoadRoomInfo_Ack = 
{
	{1, 1, "m_nRetCode", "INT", 1, "0：成功 1：失败"},
	{2, 1, "m_list", "PstDBRoomInfoItem", 1024, "加载回来房间信息列表"},
}

-- 麻将大厅向dbp更新房间信息请求
SS_A2D_UpdateRoomInfoList_Req = 
{
	{1, 1, "m_nOptType", "UINT", 1, "更新方式 0-全量更新 1-增量更新"},
	{2, 1, "m_updateList", "PstDBRoomInfoItem", 1024, "更新房间信息列表"},
}

-- 麻将大厅向dbp更新房间信息返回
SS_D2A_UpdateRoomInfoList_Ack = 
{
	{1, 1, "m_nRetCode", "INT", 1, "0：成功 1：失败"},
}


-- 服务器与客户端间通信协议
-- 获取游戏类型请求
CS_C2A_GetGameTypeID_Req =
{
	{ 1, 1, 'm_nRoomId', 'UINT', 1, '房间ID'},
}

-- 获取游戏类型响应
CS_A2C_GetGameTypeID_Ack =
{
	{ 1, 1, 'm_nRoomId', 'UINT', 1, '房间ID'},
	{ 2, 1, 'm_nGameAtomTypeId', 'UINT', 1, '游戏类型ID'},
}

-- 获取好友房间列表请求
CS_C2A_FriendRoomStateList_Req =
{

}

-- 获取玩家所在房间信息请求
CS_C2A_FriendPlayerCurrentRoom_Req =
{

}

-- 获取玩家所在房间信息返回
CS_A2C_FriendPlayerCurrentRoom_Ack =
{
	{1,1,'m_nAccountId','UINT',1,'玩家Id'},
	{2,1,'m_nRet','UINT',1,'0：失败 1：成功'},
}

-- 获取好友房间列表返回
CS_A2C_FriendRoomStateList_Ack = 
{
	{1,1,'m_arr_nRoomIdList','UINT',				1024,'该名玩家创建的房间,简称:房间列表'}, 
	{2,1,'m_arr_nGametypeID','UINT',				1024,'游戏类型'}, 
	{3,1,'m_arr_stRoundInfo','PstPyTableRoundInfo',1024,'游戏服告诉场景服的,场景服转发到客户端,数组对齐'},
	{4,1,'m_arr_stInitData','PstPyInitData',		1024,'玩法信息,牌友房转发客户端的,金币房读取csv的'},
	{5,1,'m_nCurPlayerCount','UINT',1024,'当前玩家数,最大玩家数在PstPyInitdata里'},
	{6,1,'m_nRoomStat','UINT',1024,'-1:无意义  0:开放中  1:游戏中  2:解散中  3:已经消失的  4:扩展...'},
	{7,1,'m_nCreateTimeStamp','UINT',1024,'创建房间时间戳'},
	{8,1,'m_nIsForOtherPlayer','UINT',1024,'0：自己开房 1：代别人开房'},
	{9,1,'m_nPlayerBrifInfo','PstMjRoomPlayerBrifInfo',1024,'房间玩家简要信息'},
}

-- 获取玩家所在超快赛的游戏状态请求
CS_C2A_GetFastRoomPlayerInfo_Req =
{
	{1	,1,	'm_gameKindType', 	'UINT', 	1, 		'游戏分类ID'},
}

-- 获取玩家所在超快赛的游戏状态返回
CS_A2C_GetFastRoomPlayerInfo_Ack =
{
	{1	,1,	'm_gameAtomTypeId', 	'UINT', 	1, 		'游戏类型id'},
	{2	,1,	'm_nRet',				'UINT',		1,		'0：成功 1：失败'},
}

-- 通知场景发送所在超快赛的游戏状态给客户端
SS_A2M_SendFastRoomPlayerInfo_Nty =
{
	{1	,1,	'm_gameKindType', 	'UINT', 	1, 		'游戏分类ID'},
	{2,	1,	'm_accountId',  	'UINT', 	1, 		'玩家账户ID'},
}

---------------------------------------------牌局记录信息管理子模块-------------------------------------------

-- 麻将大厅向dbp加载牌局信息列表请求
SS_A2D_LoadRoundRecordInfoList_Req = 
{
	{1,1,'m_nAccountId','UINT',1,'玩家Id'},
}

-- 麻将大厅向dbp更新牌局信息列表请求
SS_A2D_UpdateRoundRecordInfoList_Req = 
{
	{1, 1,  'm_nOptType',       'UINT',     1,      '操作类型：0：插入数据 1：删除数据 2：更新数据'},
	{2,	1,	'm_gameAtomTypeId', 'UINT', 	1, 		'游戏类型id'},
	{3,	1,	'm_timeStamp', 		'UINT', 	1, 		'时间戳'},
	{4,	1,	'm_accountIds',  	'UINT', 	12, 	'玩家账户ID'},
	{5,	1,	'm_roundId',     	'STRING', 	1, 		'牌局id'},
	{6,	1,	'm_info',  			'STRING', 	1, 		'内容'},
}

-- 麻将大厅向dbp更新牌局信息列表返回
SS_D2A_UpdateRoundRecordInfoList_Ack = 
{
	{1, 1, "m_nRetCode", 		"INT", 		1, 		"0：成功 1：失败"},
	{2, 1,  'm_nOptType',       'UINT',     1,      '操作类型：0：插入数据 1：删除数据 2：更新数据'},
}

-- 麻将大厅向dbp加载牌局信息列表返回
SS_D2A_LoadRoundRecordInfoList_Ack = 
{
	{1, 1, "m_nRetCode", 			"INT",				1, 			"0：成功 1：失败"},
	{2,	1,	'm_accountId',  		'UINT', 			1, 			'玩家账户ID'},
	{3,	1,	'm_roundList',  		'PstMjRoundData', 	1024, 		'牌局列表'},
}


-- 游戏向大厅发送牌局索引
SS_G2A_RoundIdxInfo_Nty = 
{
	{1,	1,	'm_gameAtomTypeId', 'UINT', 	1, 		'游戏类型id'},
	{2,	1,	'm_accountIds',  	'UINT', 	12, 	'玩家账户ID'},
	{3,	1,	'm_roundId',     	'STRING', 	1, 		'牌局id'},
	{4,	1,	'm_info',  			'STRING', 	1, 		'内容'},
}


-- 客户端向大厅牌局列表请求
CS_C2A_RoundList_Req = 
{
	{1,	1,	'm_accountId',  'UINT', 1, '玩家账户ID'},
	
}

-- 客户端向大厅牌局列表返回
CS_A2C_RoundList_Ack = 
{
	{1,	1,	'm_accountId',  		'UINT', 			1, 			'玩家账户ID'},
	{2,	1,	'm_gameAtomTypeId',     'UINT', 			1, 			'游戏类型id'},
	{3,	1,	'm_roundList', 			'PstMjRoundData', 	1024, 		'牌局列表'},
}

---------------------------------------------房间战绩信息管理子模块-------------------------------------------
-- 客户端请求战绩信息
CS_C2A_RoomResultInfo_Req =
{
	
}

CS_A2C_RoomResultInfo_Ack =
{
	{1, 1, 'm_infoList', 'PstMjRoomResultInfo', 50, '房间战绩信息'},
}

-- 场景向大厅发送房间结算记录
SS_M2A_RoomLastBalanceInfo_Nty =
{
	{1, 1, 'm_roomId', 'UINT', 1, '房间号'},
	{2,	1,	'm_gameAtomTypeId',     'UINT', 			1, 			'游戏类型id'},
	{3,	1,	'm_belongAccountId',     'UINT', 			1, 			'记录所属玩家Id'},
	{4, 1, 'm_playerBalance', 'PstMjPlayerBalanceInfo', 10, '玩家信息'},
}

-- 麻将大厅向dbp加载房间战绩信息请求
SS_A2D_LoadRoomResultInfoList_Req = 
{
	{1, 1, 'm_accountId', 'UINT', 1, '账号id'},
	{2, 1, 'm_hallId', 'UINT', 1, '所在大厅'},
}

SS_D2A_LoadRoomResultInfoList_Ack = 
{
	{1, 1, 'm_nRetCode', 'UINT', 1, '结果'},
	{2, 1, 'm_accountId', 'UINT', 1, '账号id'},
	{3, 1, 'm_hallId', 'UINT', 1, '所在大厅'},
	{4, 1, 'm_VctInfoList', 'PstMjRoomResultDbInfo', 50, '房间战绩信息'},
}

-- 麻将大厅向dbp插入房间战绩信息请求
SS_A2D_InsertRoomResultInfoList_Req = 
{
	{1, 1, 'm_VctInfoList', 'PstMjRoomResultDbInfo', 1, '房间战绩信息'},
}

SS_D2A_InsertRoomResultInfoList_Ack = 
{
	{1, 1, 'm_nRetCode', 'UINT', 1, '结果'},
}


-- 超快赛房间玩家战绩查询请求
CS_C2A_GetFastRoomMyWarRecord_Req = 
{
	{1,	1,	'm_gameAtomTypeId',     'UINT', 			1, 			'游戏类型id'},
}

-- 超快赛房间玩家战绩查询返回
CS_A2C_GetFastRoomMyWarRecord_Ack = 
{
	{ 1,	1, 'm_bastWarRecord'					,'UINT'					,1		,'最佳战绩'},
	{ 2,	1, 'm_bastWarRecordTimeStamp'			,'UINT'					,1		,'最佳战绩时间戳'},
	{ 3,	1, 'm_rewardItem'						,'PstMjItem'			,1024	,'奖励物品'},
	{ 4,	1, 'm_getRankFirstTimes'				,'UINT'					,1		,'获得第一名次数'},
	{ 5,	1, 'm_enterLastRoundTimes'				,'UINT'					,1		,'进入决赛局次数'},
}

-- 上传排行榜数据到麻将大厅
SS_M2A_UpdateRankItem_Nty = 
{
	{1,	1,	'm_gameAtomTypeId'						,'UINT'					,1		,'gameAtomTypeId'},
	{2,	1,	'm_gameKindType'						,'UINT'					,1		,'gameKindType'},
	{3,	1,	'm_gameKindHead'						,'UINT'					,1		,'gameKindHead'},
	{4,	1,	'm_rankType'							,'UINT'					,1		,'rankType'},
	{5,	1,	'm_rankItem'							,'PstRankItem'			,1 		,'rankItem'},	
}