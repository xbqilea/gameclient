--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
local HNLayer= require("src.app.newHall.HNLayer") 
local RBWarRewardListLayer = import(".RBWarRewardListLayer") 
local RBWarRewardInfoLayer = class("RBWarRewardInfoLayer",function ()
     return HNLayer.new()
end)

function RBWarRewardInfoLayer:ctor()
  --  self:myInit()
    self:setupView()
end

function RBWarRewardInfoLayer:setupView() 
    
    local node = UIAdapter:createNode("RBWar/RBWarPageLotteryInfo.csb") 
     UIAdapter:adapter(node,handler(self, self.onTouchCallback)) 
      UIAdapter:praseNode(node,self)  
         local center = node:getChildByName("Panel") 
     local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
     self:addChild(node) 
     local pScene = g_GameController.gameScene;
    --  self.bmfont_poolNum:setString(pScene.m_nRewardPool g_GameController.m_rewardPool)
    self.bmfont_poolNum:setString(pScene.m_nRewardPool*0.01);
     g_GameController:gameRewardListReq()
end 

function  RBWarRewardInfoLayer:onTouchCallback( sender)
    local name = sender:getName()   
    if name == "button_record" then
        self.m_RewardListLayer =RBWarRewardListLayer.new()
        self:addChild(self.m_RewardListLayer)
    elseif name == "button_close" then
        self:close()
    end
end

function  RBWarRewardInfoLayer:Show( info)
    local pScene = g_GameController.gameScene;
    if #pScene.m_pHistoryList ==0 then
        return
    end
    self.text_people:setString(pScene.m_pHistoryList[1].m_totalPlayerNum)
    self.text_name:setString(pScene.m_pHistoryList[1].m_nickname)
    self.text_reward:setString(pScene.m_pHistoryList[1].m_rewardCoin*0.01)
    self.text_total:setString(pScene.m_pHistoryList[1].m_totalOpenReward*0.01)
end
return RBWarRewardInfoLayer