if not LUA_VERSION or LUA_VERSION ~= "5.3" then
	 module(..., package.seeall)
end 
require("src.app.game.BRHH.protoID")

-- 公共结构定义文件注册
netLoaderCfg_Templates_common = {
	"src/app/game/BRHH/pubStTemp",
}

-- 协议定义文件注册
netLoaderCfg_Templates	=	{	
	"src/app/game/BRHH/red",
}


-------------------------注册协议-----------------------------
-- 公共结构协议注册
netLoaderCfg_Regs_common = 
{
	PstRedHistory 				= 	PSTID_RED_HISTORY,
	PstRedTopPlayerInfo 		= 	PSTID_RED_TOPPLAYERINFO,
	PstRedOnlinePlayerInfo		=	PSTID_RED_ONLINEPLAYERINFO,
	PstRedBalanceInfo			=	PSTID_RED_BALANCEINFO,
	PstRedBalanceData			=	PSTID_RED_BALANCEDATA,
	PstRedLuDan					=	PSTID_RED_LUDAN,
	PstRedAreaBet				=	PSTID_RED_AREA_BET,
	PstRedRewardTimes			=	PSTID_RED_REWARD_TIMES,
	PstRedRewardPoolOrder 	    = 	PSTID_RED_REWARD_POOL_ORDER,
	PstRedRewardPoolPlayer      = 	PSTID_RED_REWARD_POOL_PLAYER,
	PstRedRewardPoolHistory		=	PSTID_RED_REWARD_POOL_HISTORY,
	PstRedContinueBet 			= 	PSTID_RED_CONTINUE_BET,
}

-- 协议注册
netLoaderCfg_Regs	=	
{	
	CS_G2C_Red_GameFree_Nty				=	CS_G2C_RED_GAMEFREE_NTY,		
	CS_G2C_Red_GameStart_Nty			=	CS_G2C_RED_GAMESTART_NTY,		
	CS_G2C_Red_GameOpenCard_Nty			=	CS_G2C_RED_GAMEOPENCARD_NTY,	
	CS_G2C_Red_GameEnd_Nty				=	CS_G2C_RED_GAMEEND_NTY,		
	CS_G2C_Red_History_Nty				=	CS_G2C_RED_HISTORY_NTY,		
	CS_C2G_Red_Bet_Req					=	CS_C2G_RED_BET_REQ,
	CS_G2C_Red_Bet_Ack					=	CS_G2C_RED_BET_ACK,	
	CS_G2C_Red_Bet_Nty					=	CS_G2C_RED_BET_NTY,				
	CS_C2G_Red_PlayerOnlineList_Req		=	CS_C2G_RED_PLAYERONLINELIST_REQ,
	CS_G2C_Red_PlayerOnlineList_Ack		=	CS_G2C_RED_PLAYERONLINELIST_ACK,
	CS_G2C_Red_TopPlayerList_Nty		=	CS_G2C_RED_TOP_PLAYER_LIST_NTY,
	CS_C2G_Red_Background_Req			=	CS_C2G_RED_BACKGROUND_REQ,	
	CS_G2C_Red_Background_Ack			=	CS_G2C_RED_BACKGROUND_ACK,
	CS_C2G_Red_CltInit_Nty				=	CS_C2G_RED_CLT_INIT_NYT,
	CS_G2C_Red_PlayerCnt_Nty			=	CS_G2C_RED_PLAYER_CNT_NTY,
	CS_G2C_Red_Init_Nty					=	CS_G2C_RED_INIT_NTY,
	CS_C2G_RedRewardPoolHistory_Req		=	CS_C2G_RED_REWARD_POOL_HISTORY_REQ,
	CS_G2C_RedRewardPoolHistory_Ack		=	CS_G2C_RED_REWARD_POOL_HISTORY_ACK,
	CS_C2G_Red_ContinueBet_Req			=	CS_C2G_RED_CONTINUE_BET_REQ,
	CS_G2C_Red_ContinueBet_Ack			=	CS_G2C_RED_CONTINUE_BET_ACK,
	
	SS_M2G_Red_GameCreate_Req			=	SS_M2G_RED_GAMECREATE_REQ,
	SS_G2M_Red_GameCreate_Ack			=	SS_G2M_RED_GAMECREATE_ACK,
	SS_G2M_Red_GameResult_Nty			=	SS_G2M_RED_GAMERESULT_NTY,
	CS_M2C_Red_Exit_Nty					=	CS_M2C_RED_EXIT_NTY,
}
--return netLoaderCfg_Regs

if LUA_VERSION  and LUA_VERSION == "5.3" then
	return netLoaderCfg_Regs
end

