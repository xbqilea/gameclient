--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
local HNLayer= require("src.app.newHall.HNLayer") 
local RBWarRewardLayer = class("RBWarRewardLayer",function ()
     return HNLayer.new()
end)

function RBWarRewardLayer:ctor()
  --  self:myInit()
    self:setupView()
end

function RBWarRewardLayer:setupView() 
    local node = UIAdapter:createNode("app/game/BRHH/res/DragonTiger/RBWarPageRewardStart.csb")
     local center = node:getChildByName("Panel") 
     local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
    local image_bg = node:getChildByName("image_bg")  
     UIAdapter:adapter(node,handler(self, self.onTouchCallback)) 
      UIAdapter:praseNode(node,self)
      self.image_normal:setVisible(false)
      self.image_select:setVisible(true)
      self.image_desNormal:setVisible(true)
      self.image_desSelect:setVisible(false)
	 self:addChild(node)
      image_bg:setScaleX(1/display.scaleX)
end 