module(..., package.seeall)

-- 公共结构协议定义

PstRedHistory = 
{
	{ 1, 	1, 'm_winType'			, 'UBYTE'				, 1		, '0:红方胜， 1:黑方胜 2:和' },
	{ 2, 	1, 'm_luckyStrike'		, 'UBYTE'				, 1 	, '是否幸运一击，1: 是， 0: 否' },
	{ 3, 	1, 'm_cardType'			, 'UBYTE'				, 1 	, '获胜方牌型' },
}

PstRedTopPlayerInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2, 	1, 'm_nickname'			, 'STRING'				, 1 	, '昵称' },
	{ 3, 	1, 'm_faceId'			, 'UINT'				, 1 	, '头像ID' },
	{ 4, 	1, 'm_score'			, 'UINT'				, 1 	, '金币' },
}

PstRedOnlinePlayerInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2, 	1, 'm_nickname'			, 'STRING'				, 1 	, '昵称' },
	{ 3, 	1, 'm_faceId'			, 'UINT'				, 1 	, '头像ID' },
	{ 4, 	1, 'm_score'			, 'UINT'				, 1 	, '金币' },
}

PstRedBalanceInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2,	1, 'm_profit'			, 'INT'					, 1		, '本轮净盈利'},
	{ 3,	1, 'm_curScore'			, 'UINT'				, 1		, '结算后金币'},
}

PstRedBalanceData =
{
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1    , '玩家ID'},
	{ 2		, 2		, 'm_curCoin'			, 'UINT'				, 1	   , '当前金币'},
	{ 3		, 2		, 'm_reason'			, 'SHORT'				, 1	   , '1-离线退出 2-强退 3-踢人'},
}

PstRedLuDan =
{
	{ 1		, 1		, 'm_num'			, 'UINT'				, 1    , '数量'},
	{ 2		, 2		, 'm_color'			, 'UBYTE'				, 1	   , '有无规则 1-表示有规则 0-表示无规则'},
}

PstRedAreaBet = 
{
	{ 1,	1, 'm_betId'			 , 'UINT'						, 1		, '下注区域ID  0:红,1:黑,2:幸运一击(豹子、同花顺、同花、顺子、对子(10-A))'},
	{ 2,	1, 'm_betValue'			 , 'UINT'						, 1		, '下注额'},
}

PstRedRewardTimes =
{
	{ 1,	1, 'm_id'			 , 'UINT'						, 1		, '倍数id  0:红,1:黑,2:豹子 3:同花顺 4:同花 5:顺子 6:对子(10-A))'},
	{ 2,	1, 'm_value'		 , 'UINT'						, 1		, '倍数'},
}

PstRedRewardPoolOrder = 
{
	{ 1,	1, 'm_type'			 	 , 'UINT'						, 1		, '排名类型: 1:第1名,2:第2名,3:第3名, 4：4~5名, 5:6~10名, 6:其他名次'},
	{ 2,	1, 'playerArr'		     , 'PstRedRewardPoolPlayer'	    , 1024	, '获奖玩家'},
}

PstRedRewardPoolPlayer = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2, 	1, 'm_nickname'			, 'STRING'				, 1 	, '昵称' },
	{ 3, 	1, 'm_faceId'			, 'UINT'				, 1 	, '头像ID' },
	{ 4, 	1, 'm_rewardCoin'		, 'UINT'				, 1 	, '奖金' },
	{ 5, 	1, 'm_curCoin'		    , 'UINT'				, 1 	, '当前金币' },
}

PstRedRewardPoolHistory =
{
	{ 1, 	1, 'm_upRankTime'		, 'UINT'				, 1		, '上榜时间' },
	{ 2, 	1, 'm_totalOpenReward'	, 'UINT'				, 1 	, '总开奖金额' },
	{ 3, 	1, 'm_totalPlayerNum'	, 'UINT'				, 1 	, '总获奖人数' },
	{ 4, 	1, 'm_nickname'			, 'STRING'				, 1 	, '一等奖得主昵称' },
	{ 5, 	1, 'm_rewardCoin'		, 'UINT'				, 1 	, '一等奖得主获得奖金' },
}

PstRedContinueBet =
{
	{ 1,	1, 'm_betAreaId'		 ,  'UINT'				, 1		, '下注区域id, 0:红,1:黑,2:幸运一击(豹子、同花顺、同花、顺子、对子(10-A))'},
	{ 2,	1, 'm_curBet'		 	 ,  'UINT'				, 1		, '此次下注'},
}

