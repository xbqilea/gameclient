--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
--endregion
----
---- RBWarMainLayer
---- Author: chenzhanming
---- Date: 2018-10-24 9:00:00
---- 龙虎斗主界面UI
----
local DlgAlert            = require("app.hall.base.ui.MessageBox")
local RBWarRecordListManager = import(".RBWarRecordListManager")
local RBWarUserListLayer = require("src.app.game.common.util.PlayerListLayer")
local RBWarRuleLayer = import(".RBWarRuleLayer")
local Scheduler            = require("framework.scheduler")
local GameSetLayer = require("src.app.newHall.childLayer.SetLayer")
local HNLayer = require("src.app.newHall.HNLayer")
local BigWinnerLayer = require("src.app.game.common.util.BigWinnerLayer")
local PlayerHeadLayer = require("src.app.game.common.util.PlayerHeadLayer")
local RBWarRewardResultLayer = import(".RBWarRewardResultLayer")
local RBWarRewardStartLayer = import(".RBWarRewardStartLayer")
local RBWarRewardInfoLayer = import(".RBWarRewardInfoLayer")
local GameRecordLayer = require("src.app.newHall.childLayer.GameRecordLayer")
local ChatLayer = require("src.app.newHall.chat.ChatLayer") 
local RBWarEvent = import(".src.RBWarEvent");
local RBWarRes = import(".src.RBWarRes");

local CChipCacheManager    = require("src.app.game.common.manager.CChipCacheManager")

--  常量定义
local CHIP_SCALE = 0.5
local BR_CHIP_SPEED = display.size.width    -- 金币飞行速度
local CHIP_FLY_STEPDELAY = 0.02             -- 连续飞行间隔
local CHIP_FLY_TIME = 0.2                   -- 筹码飞行时间
local CHIP_JOBSPACETIME = 0.3               -- 飞筹码任务队列间隔
local CHIP_FLY_SPLIT = 20


--endregion
local RBWarMainLayer = class("RBWarMainLayer", function()
    return HNLayer.new();
end)


-- -- 初始化牌的信息
-- function RBWarMainLayer:initPokersInfo()
--     self.m_pRecordLayer = RBWarRecordListManager.new();
--     self:addChild(self.m_pRecordLayer)
--     self.m_pRecordLayer:setVisible(false);
-- end

-- 下注阶段
function RBWarMainLayer:startGame()
    g_AudioPlayer:playEffect(RBWarRes.Audio.START);
    self.m_pStartAni:setVisible(true);
    self.m_pStartAni:setAnimation(0, "animation1", false);
end

-- 停止下注
function RBWarMainLayer:stopChipInAni()
    g_AudioPlayer:playEffect(RBWarRes.Audio.STOP_BET)
    self.m_pStartAni:setVisible(true);
    self.m_pStartAni:setAnimation(0, "animation2", false);
end


function RBWarMainLayer:ctor()
    self:myInit()
    self:setupViews();
    self:initMsgEvent();
    --  self.tableLogic:sendGameInfo()
    -- ToolKit:registDistructor(self, handler(self, self.onDestory));
end

function RBWarMainLayer:myInit()
    self._tableLayOut = nil;
    self.m_pBetButton = {};   --下注金额按钮
    self.m_pBetBtnLight = {}; --下注金额按钮边上的亮框
    self._xiaZhuTxt = {};   --下注金额文本
    self._gameStateBg = nil;                  --游戏状态背景
    self._img_state = nil;                    --游戏状态
    self._clockLoading = nil;
    self._clockProcess = nil;                 --时钟进度条
    self._clockTimeTxt = nil;                 --时间文本
    self._areaAllBetTxt = {}; --总下注多少的文本
    self._img_myBet = {};
    self._areaMyBetTxt = {}; --我下注多少的文本
    self._cardPile = nil;                     --牌堆
    self._xiaZhuArea = {};   --下注的区域
    self._tipBox = nil;                       --提示框 
    self._userListLayer = nil;              --玩家列表
    self.m_pRecordLayer = nil;              --历史胜负情况
    self._areaTotalValue = { 0, 0, 0 }	            --各区域下注量
    self._myareaTotalValue = { 0, 0, 0 }	            --我的各区域下注量
    self._ruleLayer = nil;                     --牌型规则
    self._recordNode = nil;                   --历史胜负
    self._Btn_RuleClose = nil;				--规则关闭按钮 
    self._winOrLoseArea = { 0, 0, 0 };--四个闲家的输赢情况
    self._userListLayerState = 1;       --记录玩家列表的伸缩状态
    self._zhuangListLayerState = 1;     --记录庄家列表的伸缩状态
    self.m_nSelectBetIndex = 0;               --下注类型
    self._allTime = 0;                      --总时间
    self._nowTime = 0;                      --剩余时间
    self._fnowTime = 0;--进度条剩余时间
    self._willXiaZhuMoney = 0;          --选中的金币数
    self.m_nsBetMoney = {}; --下注金额
    self._xiaZhuMaxMoney = nil; --房间当前最大下注金额
    self._areaNowMoney = nil; --当前房间还可以下注的金额
    --self._recordList;                   --本地保存之前的胜负情况
    self._ntInfo = nil;                       --庄家
    self._myInfo = nil;                       --本方
    self._otherInfo = {};				--其他玩家
    self._otherNode = {};
    self._handCard = {};    --手牌
    self._gameResultPanel = nil;			--手牌结算区域
    self._imgWlocalip = nil;					--哪家赢的标识
    self._textXianPolocal = nil;				--闲家的点数
    self._textZhuangPonit = nil;			--庄家的点数
    self._zhuangSeat = nil;                   --庄家的座位号
    self._foreground = nil;
    self._resultNode = nil;         --结算背景
    self._gameState = nil;                    --当前游戏的状态
    self._sendCardSpace = nil;                --发单张牌的间隔
    self._kaiCardSpace = nil;                 --开牌的间隔
    self._sendCardNum = 0;              --已经发了几张牌了
    self._pai = { {}, {} };  --牌数据  
    self._myHaveBet = false;            --是否已经下注  
    self._canBetMoney = 0;                  --还可以押多少金币
    self._userListNode = nil;--在线列表节点
    self._btn_upZhuang = nil;	--上庄按钮
    --self._btn_downZhuang=nil;--下庄按钮 
    self._btn_online = nil;
    self._winAreaImg = {};--赢的时候显示的框
    self._Particle_frame = nil;
    self._Particle_star = nil;
    self._Other_frame = {};
    self._Other_star = {};
    self._otherOther_Lost = {};
    self._myInfo_Lost = nil;
    self._other_Win = {};
    self._myInfo_Win = nil;
    --存放桌子上筹码的五个容器，庄0，闲1，和2，庄对3，闲对4 
    self.m_spMoneyTab = {};
    for i = 1, 3 do
        self.m_spMoneyTab[i] = {}
    end
    self._otherPos = {};--0-3其他玩家的位置,4在线玩家位置
    self._iGameCount = 0;--记录游戏当前局数
    self._iitemCount = 0;--记录当前显示的列数

    self._curMoney = 0;
    self._data = {};
    --存储规则界面复选框的容器
    self._ruleSelectCheckBoxs = {};
    --进度条
    self._progressTimer = nil;
    -- self._MyPos = nil;
    self._Scheduler1 = nil;
    self._Scheduler2 = nil
    self._SchedulerOnce1 = nil
    self._SchedulerOnce2 = nil
    self._backFlag = true
    self._cacheChouMa = {}

    self.m_flyJobVec = {}

    self.m_bNodeMenuMoving = false;
    self.m_bIsContinue = true;
end

function RBWarMainLayer:updateOnlineUserList(__info)
    self:_GetOnlinePlayerLayer():updateUserList(__info);
    self:_GetOnlinePlayerLayer():setVisible(true);
end

function RBWarMainLayer:setupViews()
    
    local start_clock = os.clock();
    local all_clock = start_clock;
    local end_clock = start_clock;
    -- g_AudioPlayer:playMusic(RBWarRes.Audio.BG, true)
    
    -- end_clock = os.clock();
    -- print("播放背景音----耗时", end_clock - start_clock);
    -- start_clock = end_clock;

    local gameTableNode = CacheManager:addCSBTo(self, RBWarRes.CSB.MAIN_LAYER);--UIAdapter:createNode(RBWarRes.CSB.MAIN_LAYER);

    end_clock = os.clock();
    print("加载csb----耗时", end_clock - start_clock);
    start_clock = end_clock;
    -- self:addChild(gameTableNode);
    -- gameTableNode:release();
    -- g_GameController.gameScene.m_pCSB_NODE_LIST[RBWarRes.CSB.MAIN_LAYER] = nil;

    local winSize = cc.Director:getInstance():getWinSize();
    local center = gameTableNode:getChildByName("Scene")
    center:setPosition(display.width / 2, display.height / 2);
    center:setAnchorPoint(cc.p(0.5, 0.5));
    
    -- local diffY = (display.size.height - 750) / 2
    -- gameTableNode:setPosition(cc.p(0, diffY))
    -- local diffX = 145 - (1624 - display.size.width) / 2
    -- center:setPositionX(diffX)
    --UIAdapter:adapter(gameTableNode, handler(self, self.onTouchCallback))

    -- self:initPokersInfo()
    end_clock = os.clock();
    -- print("加载csb耗时", end_clock - start_clock);
    start_clock = end_clock;
    UIAdapter:praseNode(gameTableNode, self)

    end_clock = os.clock();
    print("UIAdapter:praseNode----耗时", end_clock - start_clock);
    start_clock = end_clock;

    self._tableLayOut = gameTableNode:getChildByName("desk_node");
    local desk_node = self._tableLayOut:getChildByName("desk");
    self.m_pDeskNode = desk_node;
    local area_node = desk_node:getChildByName("area_node");
    local card_view = center:getChildByName("card_view");

    self.m_pCardView = require("src/app/game/BRHH/src/view/RBWarCardLayer").new(card_view);
    self:addChild(self.m_pCardView);

    --self.button_winner:loadTextures("hall/image/sanguosgj.png", "hall/image/sanguosgj.png", "hall/image/sanguosgj.png", 0)
    self.fileNode_betTip = self._tableLayOut:getChildByName("fileNode_betTip");
    self.fileNode_betTip:setVisible(false)
    -- self.panel_backMenu = gameTableNode:getChildByName("panel_backMenu");
    -- self.panel_backMenu:setSwallowTouches(false)
    -- self.panel_backMenu:setVisible(false)
    -- self.image_settingPanel = self.panel_backMenu:getChildByName("image_settingPanel");
    -- self.posX = self.image_settingPanel:getPositionX()
    -- self.backWidth = self.image_settingPanel:getContentSize().width
    local panel_seatedPlayers = self._tableLayOut:getChildByName("panel_seatedPlayers");

    --在线人数
    --text_allPlayerText:s
    --其他玩家区域  
    self.panel_itemModelPlayer = panel_seatedPlayers:getChildByName("panel_itemModelPlayer");
    self.panel_itemModelPlayer:setVisible(false);
    for i = 1, 6 do
        local nodeName = string.format("Node_%d", i);
        local node_other = panel_seatedPlayers:getChildByName(nodeName);
        self._otherNode[i] = node_other
        self._otherInfo[i] = PlayerHeadLayer.new(node_other, 2, CacheManager:addCSBTo(node_other, "common/PlayerHead.csb"));
        self:addChild(self._otherInfo[i]);
        -- self._otherPos[i] = cc.p(node_other:getPositionX() * display.scaleX, node_other:getPositionY());
        self._otherOther_Lost[i] = node_other:getChildByName("atlas_loseCoinNum");
        self._other_Win[i] = node_other:getChildByName("atlas_winCoinNum");
        self._other_Win[i]:setScale(1.5)
        self._otherOther_Lost[i]:setScale(1.5)
    end

    --自己的区域
    local image_opBar = desk_node:getChildByName("image_opBar");
    -- local button_bigWinBtn = image_opBar:getChildByName("button_winner");
    -- button_bigWinBtn:loadTextures("hall/image/sanguosgj.png", "hall/image/sanguosgj.png", "hall/image/sanguosgj.png", 0)
    -- button_bigWinBtn:setVisible(true)
    local node_my = image_opBar:getChildByName("Node_selfInfomation");
    self._btn_online = image_opBar:getChildByName("button_watchPlayers");
    -- self._otherPos[7] = cc.p(self._btn_online:getPositionX() * display.scaleX, self._btn_online:getPositionY());
    self.text_allPlayerText = self._btn_online:getChildByName("text_totalPlayerNum");
    self._otherNode[7] = self._btn_online;

    self._myInfo_Lost = gameTableNode:getChildByName("atlas_loseCoinNum");
    self._myInfo_Win = gameTableNode:getChildByName("atlas_winCoinNum");
    self._myInfo_Lost:setVisible(false)
    self._myInfo_Win:setVisible(false)
    self._myInfo_Lost:setPosition(node_my:getPositionX() + 80 + display.size.width / 2 - 667, node_my:getPositionY() + 120)
    self._myInfo_Win:setPosition(cc.p(self._myInfo_Lost:getPosition()));
    self._myInfo = PlayerHeadLayer.new(node_my, 1);
    self._myInfo:showPlayer(false)
    local image_selfAvatar = node_my:getChildByName("image_selfAvatar")
    image_selfAvatar:setScale(0.6)
    self:addChild(self._myInfo);
    -- self._MyPos = cc.p(node_my:getPositionX(), node_my:getPositionY());
    --下注按钮和对应的值
    --local Image_chips_bg_ = image_opBar:getChildByName("Image_chips_bg_");
    local listView_betListView = image_opBar:getChildByName("listView_betListView");
    listView_betListView:setInertiaScrollEnabled(false)
    listView_betListView:setBounceEnabled(false)
    -- local t = ccui.Widget:create();
    self.panel_itemModelBet = gameTableNode:getChildByName("panel_itemModelBet");
    for i = 1, RB_GAME_BET_NUMBER do
        local panel = self.panel_itemModelBet:clone()
        local button_btn = panel:getChildByName("button_btn")
        button_btn:loadTextures("common/game_common/chip/cm_" .. RB_ChipValue[i] .. ".png")
        listView_betListView:pushBackCustomItem(panel);
        self.m_pBetButton[i] = button_btn
        self.m_pBetButton[i]:addTouchEventListener(handler(self, self.onBetClick))
        self.m_pBetButton[i]:setTag(i)
        --self._xiaZhuTxt[i] = self._xiaZhuBtn[i]:getChildByName("Text_money");
        self.m_pBetBtnLight[i] = panel:getChildByName("image_sel");
        local gray = panel:getChildByName("button_gray");
        gray:setVisible(false)
        local rotate = cc.RotateBy:create(1, 360)
        self.m_pBetBtnLight[i]:runAction(cc.RepeatForever:create(rotate))
        self.m_pBetBtnLight[i]:setVisible(false)
    end

    local panel_panelBetLimit = area_node:getChildByName("panel_panelBetLimit");
    panel_panelBetLimit:setVisible(false)

    for index = 1, 3 do
        local item = area_node:getChildByName(string.format("area_node_%d", index));
        self._xiaZhuArea[index] = item:getChildByName("panel_betPos");
        local button_choosebet = item:getChildByName("button_choosebet");
        button_choosebet:addTouchEventListener(handler(self, self.onAreaClick));
        button_choosebet:setTag(index - 1);

        local button_children = button_choosebet:getChildren();
        for __, button in pairs(button_children) do
            button:addTouchEventListener(handler(self, self.onAreaClick));
            button:setTag(index - 1);
        end

        --local Image_4_0 = item:getChildByName("Image_4_0");
        self._areaAllBetTxt[index] = item:getChildByName("atlas_betNum");
        --local Image_selfBetNoooode_0 = item:getChildByName("Image_selfBetNoooode");
        self._areaMyBetTxt[index] = item:getChildByName("atlas_selfBetCnt");
        item:setLocalZOrder(100);
    end
    self.m_pFlyNode = area_node:getChildByName("fly_node");

    self.image_kaipaiImg = self._tableLayOut:getChildByName("image_kaipaiImg");
    self.image_kaipaiImg:setVisible(false)
    --玩家列表 
    -- self._userListLayer = RBWarUserListLayer.new();
    -- self._userListLayer:setVisible(false)
    -- self:addChild(self._userListLayer);
    -- self._userListLayer:setLocalZOrder(1000)

    -- self._userWinnerLayer = BigWinnerLayer.new();
    -- self._userWinnerLayer:setVisible(false)
    -- self:addChild(self._userWinnerLayer);
    -- self._userWinnerLayer:setLocalZOrder(1000)
    --规则牌型
    self:setKaiPaiImage(false)

    

    -- self.RBWarRewardResultLayer = RBWarRewardResultLayer:new()
    -- self:addChild(self.RBWarRewardResultLayer)
    -- self.RBWarRewardResultLayer:setVisible(false)

    -- self.RBWarRewardStartLayer = RBWarRewardStartLayer:new()
    -- self:addChild(self.RBWarRewardStartLayer)
    -- self.RBWarRewardStartLayer:setVisible(false)
    --ToolKit:removeLoadingDialog()
    --self.m_pFlyNode = self._xiaZhuArea[1];
    --历史记录
    self.m_pHistoryView = desk_node:getChildByName("record_back");

    --缓存管理添加委托构造筹码方法
    CChipCacheManager.getInstance():setCreateChipDelegate(function(nBetValue)
        local betType = 1;
        for i = 1, RB_GAME_BET_NUMBER do

            if (nBetValue * 0.01 == self.m_nsBetMoney[i]) then

                betType = i;
            end
        end

        local item = cc.Sprite:create("common/game_common/chip/cm_" .. RB_ChipValue[betType] .. ".png");
        self.m_pFlyNode:addChild(item);
        return item;
    end);

    end_clock = os.clock();
    print("初始化----耗时", end_clock - start_clock);
    start_clock = end_clock;

    --创建动画文件
    self.m_pStartAni = CacheManager:addSpineTo(self._tableLayOut, RBWarRes.Anima.KAI_JU);
    self.m_pStartAni:setVisible(false);

    self.m_pPK = CacheManager:addSpineTo(self._tableLayOut, RBWarRes.Anima.PK);
    self.m_pPK:setVisible(false);
    self.m_pPK:setSkin("huanghou_guowang");

    local desk_bg = desk_node:getChildByName("desk_bg");
    local desk_bg_size = desk_bg:getContentSize();
    self.m_pResultAni = CacheManager:addSpineTo(desk_bg, RBWarRes.Anima.RESULT_FROEM);
    self.m_pResultAni:setPosition(621, 277);
    self.m_pResultAni:setScale(1.02);
    self.m_pResultAni:setVisible(false);

    self.m_pDaoJiShiAni = CacheManager:addSpineTo(self._tableLayOut, RBWarRes.Anima.DAOJISHI);
    self.m_pDaoJiShiAni:setVisible(false);
 
    self.m_pDelayStartAni = CacheManager:addSpineTo(self._tableLayOut, RBWarRes.Anima.DELAY_START);
    self.m_pDelayStartAni:setVisible(false);

    end_clock = os.clock();
    print("加载 spine 动画----耗时", end_clock - start_clock);
    start_clock = end_clock;

    self.m_pWinAni = CacheManager:addDragonBonesTo(self.m_pDeskNode, RBWarRes.DragonBonesAni.WIN.FILE_NAME); --ccs.Armature:create(RBWarRes.DragonBonesAni.WIN.FILE_NAME);
    self.m_pWinAni:setPosition(0, 0);
    self.m_pWinAni:setVisible(false);

    end_clock = os.clock();
    print("加载 龙骨 动画----耗时", end_clock - start_clock);
    start_clock = end_clock;

    --初始化按钮监听事件
    local setting_view = center:getChildByName("setting_view");
    self.m_pSettingList = setting_view:getChildByName("setting_list"):getChildByName("setting_back");
    self.m_pSettingHideButton = setting_view:getChildByName("settion_hide_button");

    local exit_button = setting_view:getChildByName("button_exit");
    self.m_pHidebutton = setting_view:getChildByName("hide_button");
    self.m_pShowbutton = setting_view:getChildByName("show_button");

    local winner_button = self.m_pSettingList:getChildByName("button_winner");
    local setting_button = self.m_pSettingList:getChildByName("button_setting");
    local helpX_button = self.m_pSettingList:getChildByName("button_helpX");

    local button_shop = desk_node:getChildByName("button_shop");
    local record_button = desk_node:getChildByName("record_button");

    self.m_pContinueButton = image_opBar:getChildByName("continue_button");

    UIAdapter:registClickCallBack(exit_button, handler(self, self.onExitClick), true);
    UIAdapter:registClickCallBack(self.m_pHidebutton, handler(self, self.onPushClick), true);
    UIAdapter:registClickCallBack(self.m_pShowbutton, handler(self, self.onPopClick), true);
    UIAdapter:registClickCallBack(winner_button, handler(self, self.onWinnerClick), true);
    UIAdapter:registClickCallBack(setting_button, handler(self, self.onSettingClick), true);
    UIAdapter:registClickCallBack(helpX_button, handler(self, self.onHelpClick), true);
    UIAdapter:registClickCallBack(self.m_pSettingHideButton, handler(self, self.onPushClick), true);
    UIAdapter:registClickCallBack(self._btn_online, handler(self, self.onOnlinePlayerClick), true);
    UIAdapter:registClickCallBack(button_shop, handler(self, self.onShopClick), true);
    UIAdapter:registClickCallBack(record_button, handler(self, self.onRecordClick), true);
    UIAdapter:registClickCallBack(self.m_pContinueButton, handler(self, self.onContinueClick), true);

    self.m_pSettingList:setVisible(false);
    self.m_pSettingHideButton:setVisible(false);

    end_clock = os.clock();
    print("添加按钮事件----耗时", end_clock - start_clock);
    start_clock = end_clock;

    end_clock = os.clock();
    print("总耗时----  ", end_clock - all_clock);
    start_clock = end_clock;


    self.mTextRecord = UIAdapter:CreateRecord(nil, 24)
    setting_view:addChild(self.mTextRecord,1)
    self.mTextRecord:setAnchorPoint(cc.p(0, 0))
	local buttonsize = exit_button:getContentSize()
	local buttonpoint = cc.p(exit_button:getPosition())
	self.mTextRecord:setPosition(cc.p(buttonpoint.x - buttonsize.width / 2, buttonpoint.y + buttonsize.height / 2))

    return true;
end
function RBWarMainLayer:setKaiPaiImage(isvisble)
    self.image_kaipaiImg:setVisible(isvisble)
end

function RBWarMainLayer:setTotalAreaText(betId, value)
    self._areaAllBetTxt[betId]:setString(value / 100);
    self._areaTotalValue[betId] = value / 100
end

function RBWarMainLayer:setMyAreaText(betId, value)
    self._areaMyBetTxt[betId]:setVisible(true)
    self._areaMyBetTxt[betId]:setString(value / 100);
    self._myareaTotalValue[betId] = value / 100
end
function RBWarMainLayer:setMyMoneyChangForBet(money)

    self._myInfo:setUserMoney(money);
end

function RBWarMainLayer:setMyHaveBet(haveBet)

    self._myHaveBet = haveBet;
end

--设置开奖记录
function RBWarMainLayer:setHistory(history)
    -- { 1, 	1, 'm_winType'			, 'UBYTE'				, 1		, '0:红方胜， 1:黑方胜 2:和' },
    -- { 2, 	1, 'm_luckyStrike'		, 'UBYTE'				, 1 	, '是否幸运一击，1: 是， 0: 否' },
    -- { 3, 	1, 'm_cardType'			, 'UBYTE'				, 1 	, '获胜方牌型' },
    local pAreaTypeBack = self.m_pHistoryView:getChildByName("record_area_type_back");
    local pCardTypeBack = self.m_pHistoryView:getChildByName("record_card_type_back");
    local areaTypeSize = pAreaTypeBack:getContentSize();
    local cardTypeSize = pCardTypeBack:getContentSize();
    pAreaTypeBack:removeAllChildren();
    pCardTypeBack:removeAllChildren();
    local index = 0;
    for i = #history, 1, -1 do
        local item = history[i];

        --胜负类型
        -- local aw = index * 22.15;
        -- if aw < areaTypeSize.width then
        if 27 > index then
            local areaItem = nil;
            if 0 == item.m_winType then
                -- areaItem = ccui.ImageView:create("DragonTiger/ui/image_8.png");
                areaItem = ccui.ImageView:create("DragonTiger/recordUi/recordUiRes/record/corner_ball_red.png");
            elseif 1 == item.m_winType then
                -- areaItem = ccui.ImageView:create("DragonTiger/ui/image_7.png");
                areaItem = ccui.ImageView:create("DragonTiger/recordUi/recordUiRes/record/corner_ball_blue.png");
            else
                areaItem = ccui.ImageView:create("DragonTiger/recordUi/recordUiRes/record/corner_circle_green.png");
            end
            pAreaTypeBack:addChild(areaItem);
            areaItem:ignoreContentAdaptWithSize(true);
            areaItem:setPosition(areaTypeSize.width - index * 22.15 - 10, areaTypeSize.height / 2);
        end

        --牌型
        -- local cw = index * 55;
        -- if cw < cardTypeSize.width then
        if 11 > index then
            local image_name = string.format("DragonTiger/ui/rbwtype_table_%d.png", item.m_cardType);
            if item.m_cardType == 1 then
                image_name = string.format("DragonTiger/ui/rbwtype_table_%d_win.png", item.m_cardType);
            end
            local cardItem = ccui.ImageView:create(image_name);
            pCardTypeBack:addChild(cardItem);
            cardItem:ignoreContentAdaptWithSize(true);

            cardItem:setPosition(cardTypeSize.width - index * 54.5 - 26, cardTypeSize.height / 2);
        end

        index = index + 1;
    end
end


function RBWarMainLayer:setXiaZhuBtnTxt(money)

    for i = 1, RB_GAME_BET_NUMBER do

        local nowMoney = money[i];
        --	self._xiaZhuTxt[i]:setString(string.format("%d", nowMoney));
        self.m_nsBetMoney[i] = nowMoney;
    end
end


function RBWarMainLayer:dealGameResult(data)
    local pScene = g_GameController.gameScene;

    self:choumaAction();

    self:doSomethingLater(function()
        self:showPlayHeadflash(pScene.m_pAllResult);
    end, 1);

    self.m_pResultAni:setVisible(true);
    --0:红方胜， 1:黑方胜 2:和
    if pScene.m_bLuckyStrike then
        self.m_pResultAni:setAnimation(0, "animation3", true);
        self.m_pWinAni:setVisible(true);
        self.m_pWinAni:getAnimation():play(RBWarRes.DragonBonesAni.WIN.ANI.LUCKY_STRIKE, -1, -1);
    elseif 0 == pScene.m_nWinType then
        self.m_pResultAni:setAnimation(0, "animation2", true);
        self.m_pWinAni:setVisible(true);
        self.m_pWinAni:getAnimation():play(RBWarRes.DragonBonesAni.WIN.ANI.RED, -1, -1);
    elseif 1 == pScene.m_nWinType then
        self.m_pResultAni:setAnimation(0, "animation1", true);
        self.m_pWinAni:setVisible(true);
        self.m_pWinAni:getAnimation():play(RBWarRes.DragonBonesAni.WIN.ANI.BLACK, -1, -1);
    elseif 2 == pScene.m_nWinType then
        -- self.m_pResultAni:setAnimation(0, "animation3", true);
        self.m_pResultAni:setVisible(false);
        self.m_pResultAni:setAnimation(0, "animation4", true);
    end
    
    self:doSomethingLater(function()
        self.m_pWinAni:setVisible(false);
    end, 1.68);

    --self:setBetBtnEnable(false);
end

function RBWarMainLayer:setGameCount(data)


    --	if (self._iGameCount > table.nums(data)) then
    --		self._iitemCount = 0;
    --		self._recordLayer:allClear();
    --		self._recordLayer:clearData();
    --		self._recordLayer:setListviewBG(false);
    --	end
    self:_GetRecordLayer():setListviewBG(false);
    self._iGameCount = table.nums(data);
end
function RBWarMainLayer:dealYuceReslut(data1, data2, data3)

    --初始化走势图
    self:_GetRecordLayer():initCircle(data1);
    self:_GetRecordLayer():initBall(data2);
    self:_GetRecordLayer():initLine(data3);
end

function RBWarMainLayer:showLeftRecorf(data)
    self:_GetRecordLayer():allClear();
    self:_GetRecordLayer():clearData();
    local fgameCount = self._iGameCount / 6;
    local itemCount = math.ceil(fgameCount);
    self._iitemCount = itemCount;
    local index = 0;
    -- ID 1-和 2-龙 3-虎
    for i = 0, itemCount - 1 do
        local tab = { {}, {}, {}, {}, {}, {} }
        for j = 1, 6 do

            if data[i * 6 + j] then
                if data[i * 6 + j].m_winType == 2 then
                    tab[j][2] = 1
                elseif data[i * 6 + j].m_winType == 1 then
                    tab[j][1] = 1
                else
                    tab[j][0] = 1
                end

            end
        end
        self:_GetRecordLayer():showLeftRecord(tab, self._iGameCount);
    end
    if (self._iGameCount >= 48) then
        self:_GetRecordLayer():setListviewBG(true);
    end




    self:_GetRecordLayer():initConTrend(data, self._iGameCount);
    self:_GetRecordLayer():updateGameCountResult();
    self:_GetRecordLayer():falshTrendImg(self._iGameCount);
    self:_GetRecordLayer():showLuckNum(data);
    self:_GetRecordLayer():initType(data);
end

function RBWarMainLayer:showPlayHeadflash(data)
    for k, v in pairs(data) do
        if (v.m_accountId == Player:getAccountID()) then
            self._curMoney = v.m_curScore * 0.01
            if (0 <= v.m_profit) then


                self._myInfo_Win:setString("+" .. v.m_profit * 0.01);

                local fadeIn = cc.FadeIn:create(1);
                local fadeOut = cc.FadeOut:create(1);
                local seq = cc.Sequence:create(cca.delay(1.5), cc.CallFunc:create(function()
                    self._myInfo_Win:setVisible(true);
                    self._myInfo_Win:setOpacity(0);
                    --			 ;					
                end), fadeIn, fadeOut, nil);
                self._myInfo_Win:runAction(seq);
            end
            if (0 > v.m_profit) then

                self._myInfo_Lost:setString(v.m_profit * 0.01);

                local fadeIn = cc.FadeIn:create(1);
                local fadeOut = cc.FadeOut:create(1);
                local seq = cc.Sequence:create(cca.delay(1.5), cc.CallFunc:create(function()
                    self._myInfo_Lost:setVisible(true);
                    self._myInfo_Lost:setOpacity(0);
                end), fadeIn, fadeOut, nil);
                self._myInfo_Lost:runAction(seq);

            end
        end
        for i = 1, 6 do
            if (self._otherInfo[i]:getTag() == v.m_accountId and self._otherInfo[i]:isVisible()) then

                if (0 <= v.m_profit) then

                    self._other_Win[i]:setString("+" .. v.m_profit * 0.01);
                    local fadeIn = cc.FadeIn:create(1);
                    local fadeOut = cc.FadeOut:create(1);
                    local seq = cc.Sequence:create(cca.delay(1.5), cc.CallFunc:create(function()

                        self._other_Win[i]:setVisible(true);
                        self._other_Win[i]:setOpacity(0);

                    end), fadeIn, fadeOut, nil);
                    self._other_Win[i]:runAction(seq);

                else
                    self._otherOther_Lost[i]:setString(v.m_profit * 0.01);

                    local fadeIn = cc.FadeIn:create(1);
                    local fadeOut = cc.FadeOut:create(1);
                    local seq = cc.Sequence:create(cca.delay(1.5), cc.CallFunc:create(function()
                        self._otherOther_Lost[i]:setVisible(true);
                        self._otherOther_Lost[i]:setOpacity(0);
                    end), fadeIn, fadeOut, nil);
                    self._otherOther_Lost[i]:runAction(seq);

                end
            end
        end
    end
    --更新自己和庄家的金币
    local info = {}
    info.m_nickname = Player:getNickName()
    info.m_score = self._curMoney * 100
    info.m_vipLevel = Player:getVipLevel()
    info.m_faceId = Player:getFaceID()
    --info.m_frameId = Player:getFrameID()
    self._myInfo:setPlayerInfo(info);
end



function RBWarMainLayer:clearDesk()
    self.m_flyJobVec ={};
    self:clearAllChip();
    self:clearAreaBet();
    --self:setBetBtnEnable(false);
    self:setMyHaveBet(false);
    CChipCacheManager.getInstance():resetChipZOrder();
    -- if #self.m_pTmpLastBet > 0 then
    --     self.m_pLastBet = self.m_pTmpLastBet;
    -- end
    -- self.m_pTmpLastBet = {};
end

function RBWarMainLayer:clearAllChip()

    for i = 1, 3 do
        --self._xiaZhuArea[i]:removeAllChildren();
        local items = self.m_spMoneyTab[i];
        for index = 1, #items do
            local item = items[index];
            item:stopAllActions();
            item:setVisible(false);
            CChipCacheManager.getInstance():putChip(item, item.m_nBetValue);
        end
        self.m_spMoneyTab[i] = {};
    end
end

function RBWarMainLayer:updateUserList(__info)
    for i = 1, 6 do
        self._otherInfo[i]:showPlayer(false);
    end
    for k, v in pairs(__info) do
        self:showOtherInfo(v, k);
    end
end

function RBWarMainLayer:setWinOrLoseArea(winLose)

    for i = 1, 3 do
        self._winOrLoseArea[winLose + 1] = 1;
    end
end


function RBWarMainLayer:showMyInfo(score)
    local info = {}
    info.m_nickname = Player:getNickName()
    info.m_score = score
    info.m_vipLevel = Player:getVipLevel()
    info.m_faceId = Player:getFaceID()
    --info.m_frameId = Player:getFrameID()
    self._myInfo:setPlayerInfo(info);
    self._myInfo:showPlayer(true)
    self._curMoney = score * 0.01
end

function RBWarMainLayer:showAreaBetTxt(areaMoney)
    if #areaMoney == 0 then
        return
    end
    for k, v in pairs(areaMoney) do
        self._areaAllBetTxt[k]:setString(v.m_betValue / 100);
    end
end

function RBWarMainLayer:showMyBetTxt(areaMoney)
    if #areaMoney == 0 then
        return
    end

    for i = 1, 3 do
        if areaMoney[i].m_betValue <= 0 then
            self._areaMyBetTxt[i]:setVisible(false)
        end
    end

    for k, v in pairs(areaMoney) do
        if v.m_betValue > 0 then
            self._areaMyBetTxt[k]:setVisible(true)
            self._areaMyBetTxt[k]:setString(v.m_betValue / 100);
        end
    end
end

function RBWarMainLayer:clearAreaBet()
    for i = 1, 3 do
        self._areaAllBetTxt[i]:setString(string.format("%d", 0));
        self._areaMyBetTxt[i]:setVisible(false)
        self._areaMyBetTxt[i]:setString(string.format(""));
    end
    self._areaTotalValue = { 0, 0, 0 }
    self._myareaTotalValue = { 0, 0, 0 }
end

function RBWarMainLayer:leaveTableCallBack()

    GamePlatform:returnPlatform(DESKLIST);
end

function RBWarMainLayer:showOtherInfo(userInfo, index)

    if userInfo then
        self._otherInfo[index]:setPlayerInfo(userInfo);
        self._otherInfo[index]:setVisible(true);
        self._otherInfo[index]:showPlayer(true);
        self._otherInfo[index]:setTag(userInfo.m_accountId)
    end
end
function RBWarMainLayer:gameReward(info)
    if self.RBWarRewardStartLayer == nil then
        self.RBWarRewardStartLayer = RBWarRewardStartLayer:new()
        self:addChild(self.RBWarRewardStartLayer)
    end
    
    self.RBWarRewardStartLayer:setVisible(true);
    self.RBWarRewardStartLayer:show(info);
    self:doSomethingLater(function()
        self.RBWarRewardStartLayer:setVisible(false)
        if self.RBWarRewardResultLayer == nil then
            self.RBWarRewardResultLayer = RBWarRewardResultLayer:new()
            self:addChild(self.RBWarRewardResultLayer)
            --self.RBWarRewardResultLayer:setVisible(false)
        end
        self.RBWarRewardResultLayer:setVisible(true)
        self.RBWarRewardResultLayer:show(info)
        local pScene = g_GameController.gameScene;
        self.caici_num:setString(pScene.m_nRewardPool * 0.01);
        self:doSomethingLater(function()
            self.RBWarRewardResultLayer:setVisible(false);
        end, 3);
    end, 6);
end

function RBWarMainLayer:refBetBtnState()
    local pScene = g_GameController.gameScene;
    if GS_BET ~= pScene.m_nGameState then
        for k = 1, RB_GAME_BET_NUMBER do
            self.m_pBetButton[k]:setEnabled(false);
            self.m_pBetBtnLight[k]:setVisible(false);
            self.m_pBetButton[k]:setColor(cc.c3b(160, 160, 160));
        end
        self.m_pContinueButton:setEnabled(false);
        self.m_pContinueButton:setColor(cc.c3b(160, 160, 160));
        -- self.m_pContinueButton:setOpacity(180);
        self.m_bIsContinue = false;
    else
        if not self.m_bIsContinue or (not pScene:isLastBet()) then
            self.m_pContinueButton:setEnabled(false);
            self.m_pContinueButton:setColor(cc.c3b(160, 160, 160));
            -- self.m_pContinueButton:setOpacity(180);
        else
            self.m_pContinueButton:setEnabled(true);
            self.m_pContinueButton:setColor(cc.c3b(255, 255, 255));
            -- self.m_pContinueButton:setOpacity(255);
        end
        
        for k = 1, RB_GAME_BET_NUMBER do
            if self._curMoney >= RB_ChipValue[k] then
                self.m_pBetButton[k]:setEnabled(true);
                self.m_pBetButton[k]:setColor(cc.c3b(255, 255, 255));
                if k == self.m_nSelectBetIndex then
                    self.m_pBetBtnLight[k]:setVisible(true);
                else
                    self.m_pBetBtnLight[k]:setVisible(false);
                end
            else
                self.m_pBetButton[k]:setEnabled(false);
                self.m_pBetBtnLight[k]:setVisible(false);
                self.m_pBetButton[k]:setColor(cc.c3b(160, 160, 160));
                if k == self.m_nSelectBetIndex then
                    local btnSize = self.m_pBetButton[k]:getParent():getContentSize();
                    self.m_pBetButton[k]:setPosition(btnSize.width / 2, btnSize.height / 2);
                    self.m_nSelectBetIndex = 0;
                end
            end
        end
    end
end

function RBWarMainLayer:dealOnlineUser(data)

    self:_GetOnlinePlayerLayer():setUserMoneyData(data);
end

function RBWarMainLayer:clear()
    CChipCacheManager.releaseInstance(true);
    self:unMsgEvent();
    --self.m_pCardView:destory();
    if self._Scheduler1 then
        Scheduler.unscheduleGlobal(self._Scheduler1)
        self._Scheduler1 = nil
    end
    if self._SchedulerOnce1 then
        Scheduler.unscheduleGlobal(self._SchedulerOnce1)
        self._SchedulerOnce1 = nil
    end
    if self._SchedulerOnce2 then
        Scheduler.unscheduleGlobal(self._SchedulerOnce2)
        self._SchedulerOnce2 = nil
    end
end

function RBWarMainLayer:getPlayerPoint(accountId, betValue)
    local playerIndex = self:getPlayerIndex(accountId);
    if 0 == playerIndex then
        if betValue == nil then
            local coin_node = self._myInfo.node:getChildByName("text_selfMoneyNum");
            local worldPoint = coin_node:convertToWorldSpace(cc.p(0, coin_node:getContentSize().height/2));
            return self.m_pFlyNode:convertToNodeSpace(worldPoint);
        else
            local betType = 1;
            for i = 1, RB_GAME_BET_NUMBER do

                if (betValue * 0.01 == self.m_nsBetMoney[i]) then

                    betType = i;
                end
            end
            local buttonSize = self.m_pBetButton[betType]:getContentSize();
            local worldPoint = self.m_pBetButton[betType]:convertToWorldSpace(cc.p(buttonSize.width / 2, buttonSize.height / 2));
            return self.m_pFlyNode:convertToNodeSpace(worldPoint);
        end
    elseif 1 <= playerIndex and 6 >= playerIndex then
        local worldPoint = self._otherNode[playerIndex]:convertToWorldSpace(cc.p(0, 0));
        return self.m_pFlyNode:convertToNodeSpace(worldPoint);
    else
        local worldPoint = self._otherNode[7]:getParent():convertToWorldSpace(cc.p(self._otherNode[7]:getPosition()));
        return self.m_pFlyNode:convertToNodeSpace(worldPoint);
    end
end

function RBWarMainLayer:getPlayerIndex(accountId)
    if 0 == accountId or nil == accountId then
        return 7;
    elseif Player:getAccountID() == accountId then
        return 0;
    else
        for i = 1, 6 do
            if (self._otherInfo[i]:getTag() == accountId and self._otherInfo[i]:isVisible()) then
                return i;
            end
        end
        return 7;
    end
end

function RBWarMainLayer:getAreaPoint(areaId)
    --local size = self._xiaZhuArea[areaId]:getContentSize();
    local get_area_point = function(node)
        local size = node:getContentSize();
        local x = math.random(10, size.width - 10);
        local y = math.random(10, size.height - 10);
        local worldPoint = node:convertToWorldSpace(cc.p(x, y));
        return self.m_pFlyNode:convertToNodeSpace(worldPoint);
    end

    local node_pos = self._xiaZhuArea[areaId]:getChildByName("node_pos");
    if node_pos then
        if math.random(1, 2) == 2 then
            return get_area_point(node_pos);
        else
            return get_area_point(self._xiaZhuArea[areaId]);
        end
    else
        return get_area_point(self._xiaZhuArea[areaId]);
    end
end

function RBWarMainLayer:doSomethingLater(call, delay)
    local node = cc.Node:create();
    self:addChild(node);
    node:runAction(cc.Sequence:create(cc.DelayTime:create(delay), cc.CallFunc:create(call), cc.RemoveSelf:create(), nil));
end

function RBWarMainLayer:cloenChip(chip)
    local item = CChipCacheManager.getInstance():getChip(chip.m_nBetValue);
    item.m_nAccountId = chip.m_nAccountId;
    item.m_nAreaId = chip.m_nAreaId;
    item.m_nBetValue = chip.m_nBetValue;
    item:setScale(chip:getScale());
    return item;
end

function RBWarMainLayer:createStatiChip(accountId, areaId, betValue)
    local item = CChipCacheManager.getInstance():getChip(betValue);
    item.m_nAccountId = accountId;
    item.m_nAreaId = areaId;
    item.m_nBetValue = betValue;
    item:setScale(CHIP_SCALE - 0.05);
    local point = self:getAreaPoint(areaId);
    item:setPosition(point);
    item:setVisible(true);
    table.insert(self.m_spMoneyTab[areaId], item);
end

function RBWarMainLayer:flyChip(accountId, areaId, betValue, isPlayerAction)

    local item = CChipCacheManager.getInstance():getChip(betValue);
    item.m_nAccountId = accountId;
    item.m_nAreaId = areaId;
    item.m_nBetValue = betValue;
    item:setScale(CHIP_SCALE);

    local startPoint = self:getPlayerPoint(accountId, betValue);
    local endPoint = self:getAreaPoint(areaId);

    item:setPosition(startPoint);

    local actionAll = {}

    if Player:getAccountID() ~= accountId then
        item:setVisible(false);
        local delay = (math.random(0, 20) / 40);

        actionAll[#actionAll + 1] = cc.DelayTime:create(delay);
        if isPlayerAction then
            actionAll[#actionAll + 1] = cc.CallFunc:create(function()
                local index = self:getPlayerIndex(accountId);
                if index > 0 and index < 7 then
                    if index < 4 then
                        self._otherInfo[index]:showOtherRun(1, delay);
                    else
                        self._otherInfo[index]:showOtherRun(2, delay);
                    end
                end
            end);
        end
        actionAll[#actionAll + 1] = cc.Show:create();
    else
        item:setVisible(true);
    end
    actionAll[#actionAll + 1] = cc.CallFunc:create(function()
        if Player:getAccountID() == accountId then -- 自己的音效立刻播放
            g_AudioPlayer:playEffect(RBWarRes.Audio.THROW_CHIP);
        else
            if not self.m_isPlayBetSound then
                g_AudioPlayer:playEffect(RBWarRes.Audio.THROW_CHIP);
                -- 尽量避免重复播放
                self:doSomethingLater(function() self.m_isPlayBetSound = false end, math.random(2, 4) / 10);
                self.m_isPlayBetSound = true;
            end
        end
    end);

    local move_distance = cc.pGetDistance(startPoint, endPoint);
    local move_time = move_distance / display.size.width;
    local move_action = cc.MoveTo:create(move_time, endPoint);
    actionAll[#actionAll + 1] = cc.EaseOut:create(move_action, move_time + 0.3);
    actionAll[#actionAll + 1] = cc.ScaleTo:create(0.15, CHIP_SCALE - 0.05);

    item:runAction(cc.Sequence:create(actionAll));

    table.insert(self.m_spMoneyTab[areaId], item);
end

function RBWarMainLayer:choumaAction()
    local pScene = g_GameController.gameScene;
    local winAreaIdxs = {}
    local loseAreaIdxs = {}
    --0:红方胜， 1:黑方胜 2:和
    if pScene.m_bLuckyStrike then
        winAreaIdxs = { 3 };
        loseAreaIdxs = { 1, 2 };
    elseif 0 == pScene.m_nWinType then
        winAreaIdxs = { 1 };
        loseAreaIdxs = { 2, 3 }
    elseif 1 == pScene.m_nWinType then
        winAreaIdxs = { 2 };
        loseAreaIdxs = { 1, 3 };
    elseif 2 == pScene.m_nWinType then
        winAreaIdxs = { 1, 2 };
        loseAreaIdxs = { 3 };
    end

    local bankerwinvec = {}     -- 庄家获取的筹码
    local bankerlostvec = {}    -- 庄家赔付筹码
    local otherwinvec = {}      -- 玩家获取的筹码
    local othervec = {}         -- 玩家所有区域筹码

    for i = 1, #winAreaIdxs do
        local winType = winAreaIdxs[i];
        local chips = self.m_spMoneyTab[winType];
        for index = 1, #chips do
            local item = chips[index];
            local endPoint = self:getPlayerPoint(item.m_nAccountId);
            table.insert(othervec, { sp = item, endpos = endPoint });
        end
    end

    for i = 1, #loseAreaIdxs do
        local loseType = loseAreaIdxs[i];
        local chips = self.m_spMoneyTab[loseType];
        for index = 1, #chips do
            local item = chips[index];
            table.insert(bankerwinvec, { sp = item, endpos = cc.p(0, 0) });
        end
    end


    -- { 1,	1, 'm_id'			 , 'UINT' , 1		, '倍数id  0:红,1:黑,2:豹子 3:同花顺 4:同花 5:顺子 6:对子(10-A))'},
    -- { 2,	1, 'm_value'		 , 'UINT' , 1		, '倍数'},
    local tmp_multiple = 1;
    --红 黑
    if 2 ~= pScene.m_nWinType then
        local reward_item = pScene:getReward(pScene.m_nWinType);
        if reward_item then
            tmp_multiple = reward_item.m_value;
        end
    end
    local reward_item = nil;
    if 5 == pScene.m_nWinCardType then
        reward_item = pScene:getReward(2);
    elseif 4 == pScene.m_nWinCardType then
        reward_item = pScene:getReward(3);
    elseif 3 == pScene.m_nWinCardType then
        reward_item = pScene:getReward(4);
    elseif 2 == pScene.m_nWinCardType then
        reward_item = pScene:getReward(5);
    elseif 1 == pScene.m_nWinCardType then
        reward_item = pScene:getReward(6);
    end

    if reward_item then
        tmp_multiple = tmp_multiple + reward_item.m_value;
    end

    if pScene.m_bLuckyStrike then
        --幸运一击
    end

    local multiple = math.floor(tmp_multiple);
    if multiple < 2 then
        multiple = 2;
    end

    for k, v in pairs(othervec) do
        for i = 0, multiple - 1 do
            local item = self:cloenChip(v.sp);
            item:setPosition(cc.p(0, 0));
            table.insert(otherwinvec, { sp = item, endpos = v.endpos });

            local endPos = self:getAreaPoint(item.m_nAreaId);
            table.insert(bankerlostvec, { sp = item, endpos = endPos });
            table.insert(self.m_spMoneyTab[item.m_nAreaId], item)
        end
    end

    -- 飞到庄家
    local jobitem1 = {
        flytime    = CHIP_FLY_TIME, -- 飞行时间
        flytimedelay = true, -- 飞行时间延长随机时间(0.05~0.15)
        flysteptime = CHIP_FLY_STEPDELAY, -- 筹码队列飞行间隔
        nextjobtime = CHIP_JOBSPACETIME, -- 下个任务执行间隔时间
        chips        = bankerwinvec, -- 筹码队列集合 
        preCB        = function()                                  -- 任务开始时执行的回调，此回调根据preCBExec控制只执行一次

        end,
        preCBExec    = false,
        --overCB        = function() print("庄家获胜筹码飞行完毕") end, -- 动画任务完成后回调
        hideAfterOver = true                                          -- 动画完成后隐藏
    }

    -- 飞到赔付区域
    local jobitem2 = {
        flytime    = CHIP_FLY_TIME, -- 飞行时间
        flytimedelay = true, -- 飞行时间延长随机时间(0.05~0.15)
        flysteptime = CHIP_FLY_STEPDELAY, -- 筹码队列飞行间隔
        nextjobtime = CHIP_JOBSPACETIME, -- 下个任务执行间隔时间
        chips        = bankerlostvec, -- 筹码队列集合
        preCB        = function()                                    -- 任务开始时执行的回调，此回调根据preCBExec控制只执行一次

        end,
        preCBExec    = false,
        --overCB        = function() print("庄家获胜筹码飞行完毕") end, -- 动画任务完成后回调
        hideAfterOver = false                                          -- 动画完成后隐藏
    }


    -- 飞到玩家
    local jobitem3 = {
        flytime    = CHIP_FLY_TIME, -- 飞行时间
        flytimedelay = true, -- 飞行时间延长随机时间(0.05~0.15)
        flysteptime = CHIP_FLY_STEPDELAY, -- 筹码队列飞行间隔
        nextjobtime = CHIP_JOBSPACETIME, -- 下个任务执行间隔时间
        chips        = otherwinvec, -- 筹码队列集合
        preCB        = function()                                    -- 任务开始时执行的回调，此回调根据preCBExec控制只执行一次

        end,
        preCBExec    = false,
        --overCB        = function() print("庄家获胜筹码飞行完毕") end, -- 动画任务完成后回调
        hideAfterOver = true                                          -- 动画完成后隐藏
    }

    -- 飞到玩家
    local jobitem4 = {
        flytime    = CHIP_FLY_TIME, -- 飞行时间
        flytimedelay = true, -- 飞行时间延长随机时间(0.05~0.15)
        flysteptime = CHIP_FLY_STEPDELAY, -- 筹码队列飞行间隔
        nextjobtime = CHIP_JOBSPACETIME, -- 下个任务执行间隔时间
        chips        = othervec, -- 筹码队列集合
        preCB        = function()                                    -- 任务开始时执行的回调，此回调根据preCBExec控制只执行一次

        end,
        preCBExec    = false,
        --overCB        = function() print("庄家获胜筹码飞行完毕") end, -- 动画任务完成后回调
        hideAfterOver = true                                          -- 动画完成后隐藏
    }

    self.m_flyJobVec = {};
    self.m_flyJobVec.nIdx = 1 -- 任务处理索引
    self.m_flyJobVec.flyIdx = 1 -- 飞行队列索引
    self.m_flyJobVec.jobVec = {} -- 任务对象
    self.m_flyJobVec.overCB = function()
        print("所有飞行任务执行完毕")
        self.m_flyJobVec = {}
        --self:showSitWinNumber( g_GameController:getOpenAward() )
    end

    table.insert(self.m_flyJobVec.jobVec, { jobitem1 })
    table.insert(self.m_flyJobVec.jobVec, { jobitem2 })
    table.insert(self.m_flyJobVec.jobVec, { jobitem3, jobitem4 })

    self:doFlyJob()


end

-- 执行单个的飞行任务
function RBWarMainLayer:doFlyJob()

    -- 全部任务执行完成之前，被清理重置
    if nil == self.m_flyJobVec.nIdx or nil == self.m_flyJobVec.jobVec then return end

    -- 任务处理完了
    if self.m_flyJobVec.nIdx > #self.m_flyJobVec.jobVec then
        if self.m_flyJobVec.overCB then
            self.m_flyJobVec.overCB()
        end
        return
    end

    -- 取出一个当前需要处理的飞行任务
    local job = self.m_flyJobVec.jobVec[self.m_flyJobVec.nIdx]
    if not job then return end
    if 0 == #job then return end

    -- 按队列取出需要飞行的对象进行动画处理
    local flyvec = {}
    local mf = math.floor
    if self.m_flyJobVec.flyIdx <= CHIP_FLY_SPLIT then
        for i = 1, #job do
            if job[i] then
                --for j = 1, #job[i].chips do
                local segnum = mf(#job[i].chips / CHIP_FLY_SPLIT) -- 计算需要分成几段
                for m = 0, segnum do
                    local tgg = job[i].chips[m * CHIP_FLY_SPLIT + self.m_flyJobVec.flyIdx]
                    if tgg then
                        table.insert(flyvec, { sptg = tgg, idx = i })
                    end
                end
                --end
            end
        end
    end

    -- 当前队列都飞完了
    if 0 == #flyvec then
        -- 下个任务的执行
        self.m_flyJobVec.nIdx = self.m_flyJobVec.nIdx + 1
        self.m_flyJobVec.flyIdx = 1
        self:doSomethingLater(function()
            self:doFlyJob()
        end, job[1].nextjobtime) -- 多个任务时 取第一个任务的时间
        return
    end

    -- 开始飞筹码
    for i = 1, #flyvec do
        local tg = flyvec[i]
        if tg and tg.sptg then
            local ts = job[tg.idx].flytimedelay and job[tg.idx].flytime + math.random(5, 15) / 100 or job[tg.idx].flytime
            local mt = cc.MoveTo:create(ts, tg.sptg.endpos)
            if i == #flyvec then -- 最后一个筹码飞行完成后执行下一次的飞行回调
                self.m_flyJobVec.flyIdx = self.m_flyJobVec.flyIdx + 1
                self:doSomethingLater(function()
                    self:doFlyJob()
                end, job[tg.idx].flysteptime)
            end

            if job[tg.idx].hideAfterOver then
                tg.sptg.sp:runAction(cc.Sequence:create(cc.Show:create(), mt, cc.Hide:create()))
            else
                tg.sptg.sp:runAction(cc.Sequence:create(cc.Show:create(), mt))
            end
        end
    end

end

--神算子下注动画
function RBWarMainLayer:runShenSuanZi()
    -- self._otherInfo[4]
end

--大富豪下注动画
function RBWarMainLayer:runDaFuHao()
    --self._otherInfo[1]
end


function RBWarMainLayer:_GetOnlinePlayerLayer()
    if nil == self._userListLayer then
        self._userListLayer = RBWarUserListLayer.new();
        self._userListLayer:setVisible(false)
        self:addChild(self._userListLayer);
        self._userListLayer:setLocalZOrder(1000)
    end
    return self._userListLayer;
end

function RBWarMainLayer:_GetRecordLayer()
    if nil == self.m_pRecordLayer then
        self.m_pRecordLayer = RBWarRecordListManager.new();
        self:addChild(self.m_pRecordLayer)
        self.m_pRecordLayer:setVisible(false);
    end

    return self.m_pRecordLayer;
end

function RBWarMainLayer:setRecordId(id)
    if id and id ~= "" then
        self.mTextRecord:setString("牌局ID:"..id)
    end
end

-- 按钮事件
--退出
function RBWarMainLayer:onExitClick()
    local pScene = g_GameController.gameScene;
    if GS_FREE ~= pScene.m_nGameState and pScene:isCurrentBet() then
        TOAST("你已下住请等待本次开奖结束！");
        return;
    end
    self:clear();
    g_GameController:reqUserLeftGameServer();
end

function RBWarMainLayer:onPopClick()
    --显示
    if self.m_bNodeMenuMoving then return end
    self.m_bNodeMenuMoving = true

    self.m_pSettingHideButton:setVisible(true)
    self.m_pSettingList:setVisible(true)
    self.m_pSettingList:setPosition(cc.p(0, 30))

    local callback = cc.CallFunc:create(function()
        self.m_pShowbutton:setVisible(false)
        self.m_pHidebutton:setVisible(true)
    end)

    local callback2 = cc.CallFunc:create(function()
        self.m_bNodeMenuMoving = false
    end)

    self.m_pSettingList:stopAllActions()
    self.m_pSettingList:setOpacity(0)
    local aTime = 0.25
    local moveTo = cc.MoveTo:create(aTime, cc.p(0, 0))
    local show = cc.Show:create()
    local sp = cc.Spawn:create(cc.EaseBackOut:create(moveTo), cc.FadeIn:create(aTime))
    local seq = cc.Sequence:create(show, sp, callback, cc.DelayTime:create(0.1), callback2)
    self.m_pSettingList:runAction(seq)
end

function RBWarMainLayer:onPushClick()
    -- 隐藏
    if self.m_bNodeMenuMoving then return end
    self.m_bNodeMenuMoving = true

    local callback = cc.CallFunc:create(function()
        self.m_pShowbutton:setVisible(true)
        self.m_pHidebutton:setVisible(false)
    end)

    local callback2 = cc.CallFunc:create(function()
        self.m_bNodeMenuMoving = false
    end)

    self.m_pSettingHideButton:setVisible(false);
    self.m_pSettingList:stopAllActions();
    local aTime = 0.25;
    local moveTo = cc.MoveTo:create(aTime, cc.p(0, 30));
    local sp = cc.Spawn:create(cc.EaseBackIn:create(moveTo), cc.FadeOut:create(aTime));
    local hide = cc.Hide:create();
    local seq = cc.Sequence:create(sp, callback, hide, cc.DelayTime:create(0.1), callback2, nil);
    self.m_pSettingList:runAction(seq);
end

function RBWarMainLayer:onWinnerClick()
    local GameRecordLayer = GameRecordLayer.new(2, 191)
    self:addChild(GameRecordLayer)
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", { 191 });
end

--设置
function RBWarMainLayer:onSettingClick()
    local layer = GameSetLayer.new(191);
    self:addChild(layer);
    layer:setScale(1 / display.scaleX, 1 / display.scaleY);
    layer:setLocalZOrder(100);
end

--帮助
function RBWarMainLayer:onHelpClick()
    local layer = RBWarRuleLayer.new()
    self:addChild(layer);
    layer:setLocalZOrder(100);
end

--在线玩家列表
function RBWarMainLayer:onOnlinePlayerClick()
    local ChatLayer = ChatLayer.new()
    self:addChild(ChatLayer);
    --g_GameController:gamePlayerOnlineListReq();
end

--金奖池
function RBWarMainLayer:onShopClick()
    self.m_rewardInfoLayer = RBWarRewardInfoLayer.new();
    self:addChild(self.m_rewardInfoLayer);
end

--走势图
function RBWarMainLayer:onRecordClick()
    self:_GetRecordLayer():setVisible(true);
end

--续投
function RBWarMainLayer:onContinueClick()
    local pScene = g_GameController.gameScene;
    if GS_BET ~= pScene.m_nGameState then
        return;
    end

    if not self.m_bIsContinue then
        return;
    end
    self.m_pContinueButton:setEnabled(false);
    -- self.m_pContinueButton:setColor(cc.c3b(160, 160, 160));
    -- self.m_pContinueButton:setOpacity(180);
    self:doSomethingLater(function()
        if self.m_bIsContinue then 
            self.m_pContinueButton:setEnabled(true);
            -- self.m_pContinueButton:setColor(cc.c3b(255, 255, 255));
            -- self.m_pContinueButton:setOpacity(255);
        end
    end, 0.3);

    local betArr = self:_ChengeChip(pScene:getLastBets());
    if 0 == #betArr then
        return;
    end
    g_GameController:continueReq(betArr);
end

--选择筹码
function RBWarMainLayer:onBetClick(pSender)
    local btnName = pSender:getName();
    local nIndex = pSender:getTag();
    if self.m_nSelectBetIndex == nIndex then
        return;
    end
    if nIndex >= 1 and nIndex <= RB_GAME_BET_NUMBER then

        if self.m_nSelectBetIndex ~= 0 then
            self.m_pBetBtnLight[self.m_nSelectBetIndex]:setVisible(false);
            local btnSize = self.m_pBetButton[self.m_nSelectBetIndex]:getParent():getContentSize();
            self.m_pBetButton[self.m_nSelectBetIndex]:setPosition(btnSize.width / 2, btnSize.height / 2);
            self.m_pBetBtnLight[self.m_nSelectBetIndex]:setPosition(btnSize.width / 2, btnSize.height / 2);
        end
        local btnSize = self.m_pBetButton[nIndex]:getParent():getContentSize();
        self.m_pBetBtnLight[nIndex]:setVisible(true);
        self.m_pBetButton[nIndex]:setPosition(btnSize.width / 2, btnSize.height / 2 + 20);
        self.m_pBetBtnLight[nIndex]:setPosition(btnSize.width / 2, btnSize.height / 2 + 20);
        self.m_nSelectBetIndex = nIndex;
    end
end

--下注区域
function RBWarMainLayer:onAreaClick(pSender, event)
    if event == ccui.TouchEventType.ended then
        local btnName = pSender:getName();
        local pScene = g_GameController.gameScene;
        if (pScene.m_nGameState ~= GS_BET) then
            return;
        end
        --	    local money = 0 
        --        for k,v in pairs(self._myareaTotalValue) do
        --            money =v+money
        --        end
        local tag = pSender:getTag()
        --	    if self._willXiaZhuMoney +money > self._areaNowMoney then
        --            self:showNoticeMessage("超过单局玩家总下注限制！");
        --		    return;
        --        end
        if self.m_nSelectBetIndex < 1 or self.m_nSelectBetIndex > RB_GAME_BET_NUMBER then
            TOAST("请先选择下注金额！");
            return;
        end

        if self.m_nsBetMoney[self.m_nSelectBetIndex] > self._curMoney then
            TOAST("您的金币不足！");
            return;
        end

        g_GameController:gameBetReq(tag, self.m_nsBetMoney[self.m_nSelectBetIndex] * 100);
        --	end 
    end
end






function RBWarMainLayer:_ChengeChip(areaBetArr)
    local pScene = g_GameController.gameScene;
    local getMinCoin = function(coin)
        for i = RB_GAME_BET_NUMBER, 1, -1 do
            local value = RB_ChipValue[i];
            if (coin >= value * 100) then
                return (value * 100);
            end
        end
        return nil;
    end
    local retBet = {};
    for index = 1, GAME_CARD_NUM do
        local cur_coin = areaBetArr[index];
        if cur_coin > 0 then
            local coin = getMinCoin(cur_coin);
            while coin do
                cur_coin = cur_coin - coin;
                table.insert(retBet, { index - 1, coin });
                coin = getMinCoin(cur_coin);
            end
        end
    end
    return retBet;
end

--初始化
function RBWarMainLayer:_initDeskChip(myBetAll, ohterBetAll)

    local getMinCoin = function(coin)
        for i = RB_GAME_BET_NUMBER, 1, -1 do
            local value = RB_ChipValue[i];
            if (coin >= value * 100) then
                return (value * 100);
            end
        end
        return nil;
    end
    local start_clock = os.clock();
    --self:doSomethingLater(function()
        for _key, item in pairs(myBetAll) do
            local cur_coin = item.m_betValue;
            if cur_coin > 0 then
                local coin = getMinCoin(cur_coin);
                while coin do
                    cur_coin = cur_coin - coin;
                    self:createStatiChip(Player:getAccountID(), _key, coin);
                    coin = getMinCoin(cur_coin);
                end
            end
        end

        for _key, item in pairs(ohterBetAll) do
            local cur_coin = item.m_betValue;
            if cur_coin > 0 then
                local coin = getMinCoin(cur_coin);
                while coin do
                    cur_coin = cur_coin - coin;
                    self:createStatiChip(0, _key, coin);
                    coin = getMinCoin(cur_coin);
                end
            end
        end
    --end, 0.1);
    local end_clock = os.clock();
    print("创建桌面筹码---耗时", end_clock - start_clock);
    

end

function RBWarMainLayer:initMsgEvent()
    addMsgCallBack(self, RBWarEvent.MSG_INIT, handler(self, self.ON_MSG_INIT));
    addMsgCallBack(self, RBWarEvent.MSG_GAME_STATE, handler(self, self.ON_MSG_GAME_STATE));
    addMsgCallBack(self, RBWarEvent.MSG_HISTORY, handler(self, self.ON_MSG_HISTORY));
    addMsgCallBack(self, RBWarEvent.MSG_BET, handler(self, self.ON_MSG_BET));
    addMsgCallBack(self, RBWarEvent.MSG_PLAYERONLINELIST, handler(self, self.ON_MSG_PLAYERONLINELIST));
    addMsgCallBack(self, RBWarEvent.MSG_TOPPLAYERLIST, handler(self, self.ON_MSG_TOPPLAYERLIST));
    addMsgCallBack(self, RBWarEvent.MSG_REWARDPOOLHISTORY, handler(self, self.ON_MSG_REWARDPOOLHISTORY));
    addMsgCallBack(self, RBWarEvent.MSG_PLAYERCNT, handler(self, self.ON_MSG_PLAYERCNT));
    addMsgCallBack(self, RBWarEvent.MSG_CONTINUE_BET, handler(self, self.ON_MSG_CONTINUE_BET));
end

function RBWarMainLayer:unMsgEvent()
    removeMsgCallBack(self, RBWarEvent.MSG_INIT);
    removeMsgCallBack(self, RBWarEvent.MSG_GAME_STATE);
    removeMsgCallBack(self, RBWarEvent.MSG_HISTORY);
    removeMsgCallBack(self, RBWarEvent.MSG_BET);
    removeMsgCallBack(self, RBWarEvent.MSG_PLAYERONLINELIST);
    removeMsgCallBack(self, RBWarEvent.MSG_TOPPLAYERLIST);
    removeMsgCallBack(self, RBWarEvent.MSG_REWARDPOOLHISTORY);
    removeMsgCallBack(self, RBWarEvent.MSG_PLAYERCNT);
    removeMsgCallBack(self, RBWarEvent.MSG_CONTINUE_BET);
end


--消息事件
function RBWarMainLayer:ON_MSG_INIT(__msgId, __cmd)
    local pScene = g_GameController.gameScene;
    local nLeftTime = pScene:getLeftTime();

    self:setXiaZhuBtnTxt(RB_ChipValue);
    self._areaNowMoney = pScene.m_nTotalLimit;
    self:showMyInfo(pScene.m_nCurrerCoin);
    self.text_allPlayerText:setString("" .. pScene.m_nPlayerCount);
    self.caici_num:setString(pScene.m_nRewardPool * 0.01);

    self:showMyBetTxt(pScene.m_pMyAreaBet);
    self:showAreaBetTxt(pScene.m_pOtherAreaBet);
    self:refBetBtnState();

    self:setGameCount(pScene.m_pHistory);
    self:showLeftRecorf(pScene.m_pHistory);
    self:dealYuceReslut(pScene.m_pBigEyeRoad, pScene.m_pSmallRoad, pScene.m_pBugRoad);
    self:setHistory(pScene.m_pHistory);

    self.text_allPlayerText:setString(#pScene.m_pPlayerList);
    --self:updateOnlineUserList(pScene.m_pPlayerList);
    self:updateUserList(pScene.m_pTopPlayerList);
    self.text_allPlayerText:setString(pScene.m_nPlayerCount);
    self:setRecordId(pScene.m_sRecordId)
    if GS_BET == pScene.m_nGameState then
        --下注阶段
        self.m_pDelayStartAni:setVisible(false);
        self:_initDeskChip(pScene.m_pMyAreaBet, pScene.m_pOtherAreaBet);
        self.m_pCardView:setGameState(pScene.m_nGameState, nLeftTime);
        if nLeftTime > 3 then
            self:doSomethingLater(function()
                self.m_pDaoJiShiAni:setVisible(true);
                self.m_pDaoJiShiAni:setAnimation(0, "animation", false);
                -- g_AudioPlayer:playEffect(RBWarRes.Audio.COUNTDOWN);
            end, nLeftTime - 2.6)

            self:doSomethingLater(function()
                self.m_pDaoJiShiAni:setVisible(false);
            end, nLeftTime)
        end

    elseif GS_SEND_CARD == pScene.m_nGameState then
        --开牌阶段
        self.m_pDaoJiShiAni:setVisible(false);
        self:setWinOrLoseArea(pScene.m_nWinType, pScene.m_nLuckyStrike);
        self:_initDeskChip(pScene.m_pMyAreaBet, pScene.m_pOtherAreaBet);
        self.m_pCardView:setGameState(pScene.m_nGameState, nLeftTime);
    elseif GS_PLAY_GAME == pScene.m_nGameState then
        --结算阶段
        self.m_pDelayStartAni:setVisible(true);
        self.m_pDelayStartAni:setAnimation(0, "animation", true);
    else
        --等待
        if nLeftTime >= 1 then
            self.m_pDelayStartAni:setVisible(true);
            self.m_pDelayStartAni:setAnimation(0, "animation", true);
        end
        
    end

end

function RBWarMainLayer:ON_MSG_GAME_STATE(__msgId, __cmd)
    local pScene = g_GameController.gameScene;
    local nLeftTime = pScene:getLeftTime();
    self.m_pDelayStartAni:setVisible(false);
    self.m_pCardView:setGameState(pScene.m_nGameState, nLeftTime);
    self:setRecordId(pScene.m_sRecordId)
    if GS_BET == pScene.m_nGameState then
        --下注阶段
        self.m_bIsContinue = true;
        self.m_pResultAni:setVisible(false);
        self:startGame()
        self:doSomethingLater(function()
            self.m_pDaoJiShiAni:setVisible(true);
            self.m_pDaoJiShiAni:setAnimation(0, "animation", false);
            -- g_AudioPlayer:playEffect(RBWarRes.Audio.COUNTDOWN);
        end, nLeftTime - 2.6)

        self:doSomethingLater(function()
            self.m_pDaoJiShiAni:setVisible(false);
        end, nLeftTime)
    elseif GS_SEND_CARD == pScene.m_nGameState then
        --开牌阶段
        self.m_pDaoJiShiAni:setVisible(false);
        self:setWinOrLoseArea(pScene.m_nWinType, pScene.m_nLuckyStrike);
        self:stopChipInAni()
    elseif GS_PLAY_GAME == pScene.m_nGameState then
        --结算阶段
        self.m_pCardView:_HideTime();
        self:dealGameResult();
        self.caici_num:setString(pScene.m_nBeforeReward * 0.01);
        if pScene.m_bIsOpenReward then
            self:gameReward()
        end
    else
        --等待
        self.m_pResultAni:setVisible(false);
        self:clearDesk();
        self:doSomethingLater(function()
            self.m_pPK:setVisible(true);
            self.m_pPK:setAnimation(0, "animation", false);
            self.m_pPK:setOpacity(255);
        end, 0.5);

        self:doSomethingLater(function()
            self.m_pPK:runAction(cc.Sequence:create(cc.FadeOut:create(0.3), cc.Hide:create(), nil));
        end, 2.5);
    end
    self:refBetBtnState();
end

function RBWarMainLayer:ON_MSG_HISTORY(__msgId, __cmd)
    local pScene = g_GameController.gameScene;
    self:setGameCount(pScene.m_pHistory);
    self:showLeftRecorf(pScene.m_pHistory);
    self:dealYuceReslut(pScene.m_pBigEyeRoad, pScene.m_pSmallRoad, pScene.m_pBugRoad);
    self:setHistory(pScene.m_pHistory);
end

function RBWarMainLayer:ON_MSG_BET(__msgId, __cmd)
    local pScene = g_GameController.gameScene;
    self:flyChip(__cmd.m_nAccountId, __cmd.m_nAredId + 1, __cmd.m_nBetValue, true);
    self:showAreaBetTxt(pScene.m_pOtherAreaBet);
    if __cmd.m_nAccountId == Player:getAccountID() then
        self:showMyBetTxt(pScene.m_pMyAreaBet);
        self:showMyInfo(pScene.m_nCurrerCoin);
        self:refBetBtnState();
        self:setMyHaveBet(true);
        -- table.insert(self.m_pTmpLastBet, { m_nAredId = __cmd.m_nAredId + 1, m_nBetValue = __cmd.m_nBetValue });
    end
end

function RBWarMainLayer:ON_MSG_PLAYERONLINELIST(__msgId, __cmd)
    local pScene = g_GameController.gameScene;
    self.text_allPlayerText:setString(#pScene.m_pPlayerList);
    self:updateOnlineUserList(pScene.m_pPlayerList);
end

function RBWarMainLayer:ON_MSG_TOPPLAYERLIST(__msgId, __cmd)
    local pScene = g_GameController.gameScene;
    self:updateUserList(pScene.m_pTopPlayerList);
end

function RBWarMainLayer:ON_MSG_REWARDPOOLHISTORY(__msgId, __cmd)
    local pScene = g_GameController.gameScene;
    self.m_rewardInfoLayer:Show();
end

function RBWarMainLayer:ON_MSG_PLAYERCNT(__msgId, __cmd)
    local pScene = g_GameController.gameScene;
    self.text_allPlayerText:setString(pScene.m_nPlayerCount);
end

function RBWarMainLayer:ON_MSG_CONTINUE_BET(__msgId, __cmd)
    -- m_nAccountId = __cmd.m_betAccountId, m_pBetArr = tmpBet
    local pScene = g_GameController.gameScene;
    if __cmd.m_nAccountId == Player:getAccountID() then
        self.m_bIsContinue = false;
        self:refBetBtnState();
        self:showMyBetTxt(pScene.m_pMyAreaBet);
        self:showMyInfo(pScene.m_nCurrerCoin);
    end
    self:showAreaBetTxt(pScene.m_pOtherAreaBet);

    local betArr = self:_ChengeChip(__cmd.m_pBetArr);
    for index = 1, #betArr do
        local bet = betArr[index];
        self:flyChip(__cmd.m_nAccountId, bet[1] + 1, bet[2], false);
    end
end

return RBWarMainLayer