--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
--endregion
local Scheduler        = require("framework.scheduler")
local CCGameSceneBase    = require("src.app.game.common.main.CCGameSceneBase")
local RBWarGlobal        = import(".RBWarGlobal")
local RBWarGameTableLayer    = import(".RBWarMainLayer")
local DlgAlert = require("app.hall.base.ui.MessageBox")
local RBWarEvent = import(".src.RBWarEvent");
local RBWarRes = import(".src.RBWarRes");

local RBWarGameScene = class("RBWarGameScene", function()
    return CCGameSceneBase.new()
end)

function RBWarGameScene:ctor()
    self.m_RBWarMainLayer        = nil
    self.m_RBWarExitGameLayer    = nil
    self.m_RBWarRuleLayer        = nil
    self.m_RBWarMusicSetLayer    = nil
    self:myInit()
    self:initData();
end

-- 游戏场景初始化
function RBWarGameScene:myInit()
    -- self:loadResources()
    --RBWarRoomController:getInstance():setInGame( true )  
    -- 主ui
    --	cc.SpriteFrameCache:getInstance():addSpriteFrames("app/game/BRHH/res/DragonTiger/image/rbwarCCSImg1.plist", "app/game/BRHH/res/DragonTiger/image/rbwarCCSImg1.png")
    --self:initRBWarGameMainLayer()
    self:registBackClickHandler(handler(self, self.onBackButtonClicked)) -- Android & Windows注册返回按钮
    addMsgCallBack(self, MSG_ENTER_FOREGROUND, handler(self, self.onEnterForeground)) -- 转前台
    addMsgCallBack(self, MSG_ENTER_BACKGROUND, handler(self, self.onEnterBackground)) -- 转后台 
    --addMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT, handler(self, self.socketState))
    --addMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT, handler(self,self.onSocketEventMsgRecived))
    addMsgCallBack(self, UPDATE_GAME_RESOURCE, handler(self, self.on_UPDATE_GAME_RESOURCE));

    self.m_pLoadingLayer = require("src/app/game/BRHH/src/view/RBWarLoadingLayer").new();
    self:addChild(self.m_pLoadingLayer, 100);
    -- self:initMainLayer();
end

function RBWarGameScene:on_UPDATE_GAME_RESOURCE()
    self:initMainLayer();
    self.m_pLoadingLayer:closeView();
    --self:removeChild(self.m_pLoadingLayer);
    self.m_pLoadingLayer = nil;
end

---- 进入场景
function RBWarGameScene:onEnter()
    print("-----------RBWarGameScene:onEnter()-----------------")
    ToolKit:setGameFPS(1 / 60)
end

-- 初始化主ui
function RBWarGameScene:initMainLayer()
    self.m_RBWarMainLayer = RBWarGameTableLayer.new()
    self:addChild(self.m_RBWarMainLayer)
    if self.m_bIsMsgInit == true then
        self:_SendMsg(RBWarEvent.MSG_INIT);
    end
end

function RBWarGameScene:getMainLayer()
    return self.m_RBWarMainLayer
end

-- 显示游戏退出界面
-- @params msgName( string ) 消息名称
-- @params __info( table )   退出相关信息
-- 显示游戏退出界面
--[[function RBWarGameScene:showExitGameLayer()
    if self._Scheduler1 then
        Scheduler.unscheduleGlobal(self._Scheduler1)	
        self._Scheduler1 = nil
    end 
     
    UIAdapter:popScene()
    g_GameController.gameScene = nil
end
--]]
function RBWarGameScene:onEnter()
    print("------RBWarGameScene:onEnter begin--------")
    print("------RBWarGameScene:onEnter end--------")
end

-- 退出场景
function RBWarGameScene:onExit()
    print("------RBWarGameScene:onExit begin--------")
    local cache = cc.SpriteFrameCache:getInstance()
    cache:removeSpriteFramesFromFile("app/game/BRHH/res/DragonTiger/image/rbwarCCSImg1.plist")
    --cc.Director:getInstance():getScheduler():unscheduleScriptEntry(self.m_pSchedulerUpdate);
    

    if self.m_RBWarMainLayer then
        self.m_RBWarMainLayer:clear()
        self.m_RBWarMainLayer = nil
    end

    if self._Scheduler1 then
        Scheduler.unscheduleGlobal(self._Scheduler1)
        self._Scheduler1 = nil
    end

    removeMsgCallBack(self, MSG_ENTER_FOREGROUND);
    removeMsgCallBack(self, MSG_ENTER_BACKGROUND);
    removeMsgCallBack(self, UPDATE_GAME_RESOURCE);
    --removeMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT)
    --removeMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT)
    --    RBWarGlobal.m_isNeedReconectGameServer = false
    --    RBWarRoomController:getInstance():setInGame( false )
    --    RBWarGameController:getInstance():onDestory()
    --   self:RemoveResources()
    AudioManager:getInstance():stopAllSounds()
    AudioManager:getInstance():stopMusic()

    --释放未使用的csb实列
    CacheManager:removeAllExamples();

    -- 释放动画
    for _, strPathName in pairs(RBWarRes.vecReleaseAnim) do
        --local strJsonName = string.format("%s%s/%s.ExportJson", Lhdz_Res.strAnimPath, strPathName, strPathName)
        ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo(strPathName)
    end
    -- 释放整图
    for _, strPathName in pairs(RBWarRes.vecReleasePlist) do
        display.removeSpriteFrames(strPathName[1], strPathName[2])
    end
    -- 释放背景图
    for _, strFileName in pairs(RBWarRes.vecReleaseImg) do
        display.removeImage(strFileName)
    end
    -- 释放音频
    for _, strFileName in pairs(RBWarRes.vecReleaseSound) do
        AudioManager.getInstance():unloadEffect(strFileName)
    end
    print("------RBWarGameScene:onExit end--------")
end

-- 响应返回按钮事件
function RBWarGameScene:onBackButtonClicked()
    if self.m_RBWarMainLayer then
        self.m_RBWarMainLayer:clear()
    end
    g_GameController:reqUserLeftGameServer()
end

-- 从后台切换到前台
function RBWarGameScene:onEnterForeground()
    print("从后台切换到前台")
    g_GameController:gameBackgroundReq(2)
end

-- 从前台切换到后台
function RBWarGameScene:onEnterBackground()
    print("从前台切换到后台,游戏线程挂起!")
    g_GameController:gameBackgroundReq(1)
    g_GameController.m_BackGroudFlag = true
end

function RBWarGameScene:clearView()
    print("RBWarGameScene:clearView()")
end

-- 清理数据
function RBWarGameScene:clearData()

end

--退出游戏处理
--[[function RBWarGameScene:exitGame()   
    if self.m_RBWarMainLayer._Scheduler1 then
        Scheduler.unscheduleGlobal(self.m_RBWarMainLayer._Scheduler1)	
        self.m_RBWarMainLayer._Scheduler1 = nil
    end 
     ConnectManager:send2GameServer( g_GameController.m_gameAtomTypeId,"CS_C2G_UserLeft_Req", { })
    UIAdapter:popScene()
    self:closeGameSvrConnect()
    g_GameController.gameScene = nil
    g_GameController:onDestory()
end
--]]
function RBWarGameScene:initData()
    self.m_bIsMsgInit = false;

    self.m_nTotalLimit = 0;             --下注总限制
    self.m_nPlayerTotalLimit = 100;     --个人下注总限制
    self.m_nCurrerCoin = 0;             --自己的金币

    self.m_nGameState = 0;              --游戏阶段
    self.m_fLastClock = 0;
    self.m_nLeftTime = 0;               --本阶段剩余时间
    self.m_pMyAreaBet = {};             --自己下注金额
    self.m_pOtherAreaBet = {};          --他人下注金额
    self.m_pRewardTimesArr = {};        --PstRedRewardTimes 奖励倍数
    self.m_nPlayerCount = 0;            --玩家人数
    self.m_pPlayerList = {};            --在线玩家信息
    self.m_pTopPlayerList = {};         --前6名玩家信息
    self.m_pHistoryList = {};           --奖金奖池历史记录

    --开牌信息
    self.m_pRedCard = {};               --红方牌
    self.m_pBlackCard = {};             --黑方牌
    self.m_nRedCardType = -1;           --红方牌型
    self.m_nBlackCardType = -1;         --黑方配型
    self.m_nWinType = -1;               --0:红方胜， 1:黑方胜 2:和
    self.m_nWinCardType = -1;           --胜利方牌型
    self.m_bIsDisorderly = false;       --获胜方是否杂花235，1: 是， 0: 否
    self.m_nRewardPool = 0;             --奖金池金币
    self.m_nOpenReward = 0;             --奖金池本局随机派奖金额
    self.m_nBeforeReward = 0;           --开奖前的奖金池金币
    self.m_pAllResult = {};             --所有玩家结算结果
    self.m_bIsOpenReward = false;       --是否奖金池开奖, 1:是, 0:否
    self.m_bLuckyStrike = false;        --是否幸运一击
    self.m_pRewardArr = {};           --奖金池发奖情况

    --历史记录
    self.m_pHistory = {};                --PstRedHistory 历史记录
    self.m_pBigEyeRoad = {};             --PstRedLuDan 大眼仔路
    self.m_pSmallRoad = {};              --PstRedLuDan 小路
    self.m_pBugRoad = {};                --PstRedLuDan 小强路

    --上次投注记录
    self.m_pLastBet = { 0, 0, 0 };
    self.m_pTmpLastBet = { 0, 0, 0 };

    self.m_sRecordId = ""
    --self.m_pSchedulerUpdate = cc.Director:getInstance():getScheduler():scheduleScriptFunc(handler(self, self.onTimeUpdate), 1, true);
end


-- function RBWarGameScene:onTimeUpdate(dt)
--     self.m_nLeftTime = self.m_nLeftTime - 1;
--     if self.m_nLeftTime < 0 then
--         self.m_nLeftTime = 0;
--         return;
--     end
-- end

function RBWarGameScene:getReward(id)
    for index = 1, #self.m_pRewardTimesArr do
        local item = self.m_pRewardTimesArr[index];
        if id == item.m_id then
            return item;
        end
    end
    return nil;
end

function RBWarGameScene:getLeftTime()
    local curTime = os.clock();
    local t = (curTime - self.m_fLastClock);
    if t < 0.5 then
        return self.m_nLeftTime;
    end
    self.m_fLastClock = curTime;
    self.m_nLeftTime = self.m_nLeftTime - t;
    if self.m_nLeftTime < 0 then
        return 0;
    else
        return math.floor(self.m_nLeftTime + 0.2);
    end
end

function RBWarGameScene:getLastBets()
    return self.m_pLastBet;
end

function RBWarGameScene:isLastBet()
    for key,value in pairs(self.m_pLastBet) do
        if value > 0 then
            return true;
        end
    end
    return false;
end

function RBWarGameScene:isCurrentBet()
    for key,value in pairs(self.m_pTmpLastBet) do
        if value > 0 then
            return true;
        end
    end
    return false;
end


function RBWarGameScene:ON_INIT_NTY(__cmd)
    --初始化数据
    -- { 1,	1, 'm_state'		 	 ,  'UINT'						, 1		, '当前状态: 1-free阶段 2-下注阶段 3-开奖阶段 4-结算阶段'},
    -- { 2,	1, 'm_leftTime'		 	 ,  'UINT'						, 1		, '本状态剩余时间 单位s'},
    -- { 3,	1, 'm_curCoin'       	 ,  'UINT'       				, 1     , '玩家当前金币' },
    -- { 4,	1, 'm_totalBetLimit'   	 ,  'UINT'       				, 1     , '总下注限制' },
    -- { 5,	1, 'm_playerBetLimit'    ,  'UINT'       				, 1     , '个人下注限制' },
    -- { 6,	1, 'm_allAreaBetArr'     ,  'PstRedAreaBet'       		, 1024     , '各区域总下注' },
    -- { 7,	1, 'm_myAreaBetArr'      ,  'PstRedAreaBet'       		, 1024     , '我在各区域下注' },
    -- { 8,	1, 'm_redCard'        	 ,  'UINT'       				, 1024     , '红方牌' },
    -- { 9,	1, 'm_blackCard'         ,  'UINT'       				, 1024     , '黑方牌' },
    -- { 10,	1, 'm_redCardType'       ,  'UINT'       				, 1     , '红方牌型' },
    -- { 11,	1, 'm_blackCardType'     ,  'UINT'       				, 1     , '黑方牌型' },
    -- { 12, 	1, 'm_winType'			 , 'UINT'				        , 1		, '0:红方胜， 1:黑方胜 2:和' },
    -- { 13, 	1, 'm_luckyStrike'		 , 'UINT'				        , 1 	, '是否幸运一击，1: 是， 0: 否' },
    -- { 14, 	1, 'm_chipArr'		 	 , 'UINT'				        , 1024 	, '筹码' },
    -- { 15, 	1, 'm_rewardTimesArr'	 , 'PstRedRewardTimes'			, 1024 	, '奖励倍数' },
    -- { 16,	1, 'm_playerCnt'       	 ,  'UINT'       				, 1     , '玩家人数' },
    -- { 17,	1, 'm_winCardType' , 'UINT' , 1 , '获胜方牌型' },
    -- { 18, 1, 'm_isDisorderly'	, 'UINT'	, 1 , '获胜方是否杂花235，1: 是， 0: 否' },
    -- { 19,	1, 'm_rewardPool'        , 'UINT'       				, 1 , '奖金池金币' },
    self.m_nLeftTime = __cmd.m_leftTime;
    self.m_fLastClock = os.clock();
    self.m_nCurrerCoin = __cmd.m_curCoin;
    self.m_nTotalLimit = __cmd.m_totalBetLimit;
    self.m_nPlayerTotalLimit = __cmd.m_playerBetLimit;
    self.m_pMyAreaBet = __cmd.m_myAreaBetArr;
    self.m_pOtherAreaBet = __cmd.m_allAreaBetArr;
    self.m_pRedCard = __cmd.m_redCard;
    self.m_pBlackCard = __cmd.m_blackCard;
    self.m_nRedCardType = __cmd.m_redCardType;
    self.m_nBlackCardType = __cmd.m_blackCardType;
    self.m_nWinType = __cmd.m_winType;
    self.m_bLuckyStrike = (__cmd.m_luckyStrike == 1);
    self.m_pRewardTimesArr = __cmd.m_rewardTimesArr;
    self.m_nPlayerCount = __cmd.m_playerCnt;
    self.m_nWinCardType = __cmd.m_winCardType;
    self.m_bIsDisorderly = (__cmd.m_isDisorderly == 1);
    self.m_nRewardPool = __cmd.m_rewardPool;
    self.m_nBeforeReward = __cmd.m_rewardPool;
    self.m_sRecordId = __cmd.m_recordId or ""
    if 2 == __cmd.m_state then
        self.m_nGameState = GS_BET;
    elseif 3 == __cmd.m_state then
        self.m_nGameState = GS_SEND_CARD;
    elseif 4 == __cmd.m_state then
        self.m_nGameState = GS_PLAY_GAME;
    else
        self.m_nGameState = GS_FREE;
    end

    self.m_bIsMsgInit = true;
    self:_SendMsg(RBWarEvent.MSG_INIT);
end

function RBWarGameScene:ON_GAMEFREE_NTY(__cmd)
    --等待阶段
    -- { 1,	1, 'm_timeLeft'			 ,  'UINT'						, 1		, '该状态剩余时间'},
    self.m_nGameState = GS_FREE;
    self.m_nLeftTime = __cmd.m_timeLeft;
    self.m_fLastClock = os.clock();
    for index = 1, 3 do
        if self.m_pTmpLastBet[index] > 0 then
            self.m_pLastBet = self.m_pTmpLastBet;
            self.m_pTmpLastBet = { 0, 0, 0 };
            break;
        end
    end
    self:_SendMsg(RBWarEvent.MSG_GAME_STATE);
end

function RBWarGameScene:ON_GAMESTART_NTY(__cmd)
    --游戏开始
    -- { 1,	1, 'm_timeLeft'			 ,  'UINT'						, 1		, '该状态剩余时间'},
    self.m_nGameState = GS_BET;
    self.m_nLeftTime = __cmd.m_timeLeft;
    self.m_fLastClock = os.clock();
    self.m_sRecordId = __cmd.m_recordId or ""
    self:_SendMsg(RBWarEvent.MSG_GAME_STATE);
end

function RBWarGameScene:ON_GAMEOPENCARD_NTY(__cmd)
    --开牌
    -- { 1,	1, 'm_timeLeft'			 ,  'UINT'						, 1		, '该状态剩余时间'},
    -- { 2,	1, 'm_redCard'        	 ,  'UINT'       				, 1024     , '红方牌' },
    -- { 3,	1, 'm_blackCard'         ,  'UINT'       				, 1024     , '黑方牌' },
    -- { 4,	1, 'm_redCardType'       ,  'UINT'       				, 1     , '红方牌型' },
    -- { 5,	1, 'm_blackCardType'     ,  'UINT'       				, 1     , '黑方牌型' },
    -- { 6, 	1, 'm_winType'			 , 'UINT'				        , 1		, '0:红方胜， 1:黑方胜 2:和' },
    -- { 7, 	1, 'm_luckyStrike'		 , 'UINT'				        , 1 	, '是否幸运一击，1: 是， 0: 否' },
    -- { 8,	1, 'm_winCardType'       ,  'UINT'       				, 1     , '获胜方牌型' },
    -- { 9, 	1, 'm_isDisorderly'		 , 'UINT'				        , 1 	, '获胜方是否杂花235，1: 是， 0: 否' },
    self.m_nGameState = GS_SEND_CARD;
    self.m_nLeftTime = __cmd.m_timeLeft;
    self.m_fLastClock = os.clock();
    self.m_pRedCard = __cmd.m_redCard;
    self.m_pBlackCard = __cmd.m_blackCard;
    self.m_nRedCardType = __cmd.m_redCardType;
    self.m_nBlackCardType = __cmd.m_blackCardType;
    self.m_nWinType = __cmd.m_winType;
    self.m_bLuckyStrike = (1 == __cmd.m_luckyStrike);
    self.m_nWinCardType = __cmd.m_winCardType;
    self.m_bIsDisorderly = (1 == __cmd.m_isDisorderly);
    self:_SendMsg(RBWarEvent.MSG_GAME_STATE);
end

function RBWarGameScene:ON_GAMEEND_NTY(__cmd)
    --结算
    -- { 1,	1, 'm_timeLeft'			 ,  'UINT'						, 1		, '该状态剩余时间'},
    -- { 2,	1, 'm_allResult'      	 ,  'PstRedBalanceInfo'    		, 1024   , '所有玩家结算结果' },
    -- { 3,	1, 'm_isOpenReward'      , 'UINT'       				, 1     , '是否奖金池开奖, 1:是, 0:否' },
    -- { 4,	1, 'm_rewardPool'        , 'UINT'       				, 1     , '开奖后的奖金池金币' },
    -- { 5,	1, 'm_openReward'        , 'UINT'       				, 1     , '奖金池本局随机派奖金额' },
    -- { 6,	1, 'm_beforeReward'      , 'UINT'       				, 1     , '开奖前的奖金池金币' },
    -- { 7,	1, 'm_rewardArr'         , 'PstRedRewardPoolOrder'      , 1024  , '奖金池发奖情况' },
    self.m_nGameState = GS_PLAY_GAME;
    self.m_nLeftTime = __cmd.m_timeLeft;
    self.m_fLastClock = os.clock();
    self.m_pAllResult = __cmd.m_allResult;
    self.m_bIsOpenReward = (1 == __cmd.m_isOpenReward);
    self.m_nRewardPool = __cmd.m_rewardPool;
    self.m_nOpenReward = __cmd.m_openReward;
    self.m_nBeforeReward = __cmd.m_beforeReward;
    self.m_pRewardArr = __cmd.m_rewardArr;
    self:_SendMsg(RBWarEvent.MSG_GAME_STATE);
end

function RBWarGameScene:ON_HISTORY_NTY(__cmd)
    --历史记录
    -- { 1,	1, 'm_history'			 , 	'PstRedHistory'				, 1024	, '历史记录'},
    -- { 2, 	1, 'm_bigEyeRoad'		 , 	'PstRedLuDan'				, 1024	, '大眼仔路' },
    -- { 3, 	1, 'm_smallRoad'		 , 	'PstRedLuDan'				, 1024	, '小路' },
    -- { 4, 	1, 'm_bugRoad'			 , 	'PstRedLuDan'				, 1024	, '小强路' },
    self.m_pHistory = __cmd.m_history;
    self.m_pBigEyeRoad = __cmd.m_bigEyeRoad;
    self.m_pSmallRoad = __cmd.m_bigEyeRoad;
    self.m_pBugRoad = __cmd.m_bugRoad;
    self:_SendMsg(RBWarEvent.MSG_HISTORY);
end

function RBWarGameScene:ON_BET_ACK(__cmd)
    -- 自己下注
    -- { 1,	1, 'm_result'			 , 'INT'				, 1		, '结果 0:成功 -x:失败'},
    -- { 2,	1, 'm_betId'			 , 'UINT'				, 1		, '下注区域ID  0:红,1:黑,2:幸运一击(豹子、同花顺、同花、顺子、对子(10-A))'},
    -- { 3,	1, 'm_betValue'			 , 'UINT'				, 1		, '下注额'},
    -- { 4,	1, 'm_allAreaBetArr'     ,  'PstRedAreaBet'     , 1024  , '各区域总下注' },
    -- { 5,	1, 'm_myAreaBetArr'      ,  'PstRedAreaBet'     , 1024  , '我在各区域下注' },
    -- { 6,	1, 'm_curCoin'	     	 , 'UINT'				, 1		, '当前金币'},
    if 0 == __cmd.m_result then
        self.m_pOtherAreaBet = __cmd.m_allAreaBetArr;
        self.m_pMyAreaBet = __cmd.m_myAreaBetArr;
        self.m_nCurrerCoin = __cmd.m_curCoin;
        self.m_pTmpLastBet[__cmd.m_betId + 1] = __cmd.m_betValue + self.m_pTmpLastBet[__cmd.m_betId + 1];
        self:_SendMsg(RBWarEvent.MSG_BET, { m_nAccountId = Player:getAccountID(), m_nAredId = __cmd.m_betId, m_nBetValue = __cmd.m_betValue });
    else
        self:_ShowErrorMsg("BET_ACK", __cmd.m_result);
    end


end

function RBWarGameScene:ON_BET_NTY(__cmd)
    --其他玩家下注通知
    -- { 1,	1, 'm_betId'			 , 'UINT'				, 1		, '下注区域ID  0:红,1:黑,2:幸运一击(豹子、同花顺、同花、顺子、对子(10-A))'},
    -- { 2,	1, 'm_betValue'			 , 'UINT'				, 1		, '下注额'},
    -- { 3,	1, 'm_allAreaBetArr'     ,  'PstRedAreaBet'     , 1024  , '各区域总下注' },
    -- { 4,	1, 'm_accountId'	     , 'UINT'				, 1		, '下注玩家'},
    self.m_pOtherAreaBet = __cmd.m_allAreaBetArr;
    self:_SendMsg(RBWarEvent.MSG_BET, { m_nAccountId = __cmd.m_accountId, m_nAredId = __cmd.m_betId, m_nBetValue = __cmd.m_betValue });
end

function RBWarGameScene:ON_PLAYERONLINELIST_ACK(__cmd)
    --在线玩家信息
    --{ 1, 	1, 'm_playerList'		 , 'PstRedOnlinePlayerInfo'  	, 1024	, '在线玩家信息' },	
    self.m_pPlayerList = __cmd.m_playerList;
    self.m_nPlayerCount = #__cmd.m_playerList;
    self:_SendMsg(RBWarEvent.MSG_PLAYERONLINELIST);
end

function RBWarGameScene:ON_TOPPLAYERLIST_NTY(__cmd)
    --前六名玩家信息
    -- { 1, 	1, 'm_topPlayerList'		 , 'PstRedTopPlayerInfo'  			, 1024		, '前6名玩家(实际数量 <= 6)' },
    self.m_pTopPlayerList = __cmd.m_topPlayerList;
    self:_SendMsg(RBWarEvent.MSG_TOPPLAYERLIST);
end

function RBWarGameScene:ON_REWARDPOOLHISTORY_ACK(__cmd)
    --奖金池开奖历史记录
    -- { 1, 	1, 'm_historyList'		 , 'PstRedRewardPoolHistory'  	, 1024	, '奖金池开奖历史记录' },
    self.m_pHistoryList = __cmd.m_historyList;
    self:_SendMsg(RBWarEvent.MSG_REWARDPOOLHISTORY);
end

function RBWarGameScene:ON_PLAYERCNT_NTY(__cmd)
    --玩家数量
    self.m_nPlayerCount = __cmd.m_playerCount;
    self:_SendMsg(RBWarEvent.MSG_PLAYERCNT);
end

function RBWarGameScene:ON_CONTINUE_BET_ACK(__cmd)
    -- { 1, 	1, 'm_result'		 	 ,  'INT'					, 1		, '0:成功, -x:失败(m_result=0时，广播此ack)' },
    -- { 2, 	1, 'm_betAccountId'	 	 ,  'UINT'					, 1		, '下注玩家ID' },
    -- { 3,	1, 'm_curCoin'		 	 ,  'UINT'					, 1		, '下注成功后的玩家金币(仅在m_result=0,且m_betAccountId=自己时有效)'},
    -- { 4,	1, 'm_continueBetArr'	 ,  'PstRedContinueBet'	, 1024	, '玩家续押信息'},
    -- { 5,	1, 'm_allAreaBetArr'     ,  'PstRedAreaBet'     , 1024  , '各区域总下注' },
    -- { 6,	1, 'm_myAreaBetArr'      ,  'PstRedAreaBet'     , 1024  , '我在各区域下注(仅在m_result=0,且m_betAccountId=自己时有效)' },
    if 0 == __cmd.m_result then
        self.m_pOtherAreaBet = __cmd.m_allAreaBetArr;
        local tmpBet = { 0, 0, 0 };
        if __cmd.m_betAccountId == Player:getAccountID() then
            -- { 1,	1, 'm_betAreaId'		 ,  'UINT'				, 1		, '下注区域id, 0:红,1:黑,2:幸运一击(豹子、同花顺、同花、顺子、对子(10-A))'},
            -- { 2,	1, 'm_curBet'		 	 ,  'UINT'				, 1		, '此次下注'},
            self.m_nCurrerCoin = __cmd.m_curCoin;
            self.m_pMyAreaBet = __cmd.m_myAreaBetArr;
            for index = 1, #__cmd.m_continueBetArr do
                local item = __cmd.m_continueBetArr[index];
                local areaId = item.m_betAreaId + 1;
                self.m_pTmpLastBet[areaId] = item.m_curBet + self.m_pTmpLastBet[areaId];
                tmpBet[areaId] = item.m_curBet + tmpBet[areaId];
            end
        else
            for index = 1, #__cmd.m_continueBetArr do
                local item = __cmd.m_continueBetArr[index];
                local areaId = item.m_betAreaId + 1;
                tmpBet[areaId] = item.m_curBet + tmpBet[areaId];
            end
        end
        sendMsg(RBWarEvent.MSG_CONTINUE_BET, { m_nAccountId = __cmd.m_betAccountId, m_pBetArr = tmpBet });
    else
        self:_ShowErrorMsg("CONTINUE_BET_ACK", __cmd.m_result);
    end
end

function RBWarGameScene:_SendMsg(msgId, ...)
    if self.m_RBWarMainLayer then
        sendMsg(msgId, ...);
    end
end

function RBWarGameScene:_ShowErrorMsg(cmdId, errorCode)
    if self.m__pErrorCodeData == nil then
        self.m__pErrorCodeData = {};
        local bet_ack = {}
        bet_ack["-200101"] = "非下注阶段，不能下注！";
        bet_ack["-200102"] = "下注区域无效！";
        bet_ack["-200103"] = "金币不足，下注失败！";
        bet_ack["-200104"] = "您下注超过个人上限！";
        bet_ack["-200105"] = "已达下注总上限！";
        bet_ack["-200106"] = "下注失败, 携带金币低于30金币！";
        bet_ack["-200107"] = "下注筹码非法";
        bet_ack["-200199"] = "未知错误";
        self.m__pErrorCodeData["BET_ACK"] = bet_ack;

        local bet_ack = {}
        bet_ack["-200101"] = "非下注阶段，不能续投！";
        bet_ack["-200102"] = "下注区域无效！";
        bet_ack["-200103"] = "金币不足，续投失败！";
        bet_ack["-200104"] = "您下注超过个人上限！";
        bet_ack["-200105"] = "已达下注总上限！";
        bet_ack["-200106"] = "续投失败, 携带金币低于30金币！";
        bet_ack["-200107"] = "续投筹码非法";
        bet_ack["-200199"] = "未知错误";
        self.m__pErrorCodeData["CONTINUE_BET_ACK"] = bet_ack;


    end
    local code = "" .. errorCode;
    if self.m__pErrorCodeData[cmdId] and self.m__pErrorCodeData[cmdId][code] then
        TOAST(self.m__pErrorCodeData[cmdId][code]);
    end

end

return RBWarGameScene;