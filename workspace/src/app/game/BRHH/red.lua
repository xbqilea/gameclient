module(..., package.seeall)

CS_G2C_Red_Init_Nty = {
	{ 1,	1, 'm_state'		 	 ,  'UINT'						, 1		, '当前状态: 1-free阶段 2-下注阶段 3-开奖阶段 4-结算阶段'},
	{ 2,	1, 'm_leftTime'		 	 ,  'UINT'						, 1		, '本状态剩余时间 单位s'},
	{ 3,	1, 'm_curCoin'       	 ,  'UINT'       				, 1     , '玩家当前金币' },
	{ 4,	1, 'm_totalBetLimit'   	 ,  'UINT'       				, 1     , '总下注限制' },
	{ 5,	1, 'm_playerBetLimit'    ,  'UINT'       				, 1     , '个人下注限制' },
	{ 6,	1, 'm_allAreaBetArr'     ,  'PstRedAreaBet'       		, 1024     , '各区域总下注' },
	{ 7,	1, 'm_myAreaBetArr'      ,  'PstRedAreaBet'       		, 1024     , '我在各区域下注' },
	{ 8,	1, 'm_redCard'        	 ,  'UINT'       				, 1024     , '红方牌' },
	{ 9,	1, 'm_blackCard'         ,  'UINT'       				, 1024     , '黑方牌' },
	{ 10,	1, 'm_redCardType'       ,  'UINT'       				, 1     , '红方牌型' },
	{ 11,	1, 'm_blackCardType'     ,  'UINT'       				, 1     , '黑方牌型' },
	{ 12, 	1, 'm_winType'			 , 'UINT'				        , 1		, '0:红方胜， 1:黑方胜 2:和' },
	{ 13, 	1, 'm_luckyStrike'		 , 'UINT'				        , 1 	, '是否幸运一击，1: 是， 0: 否' },
	{ 14, 	1, 'm_chipArr'		 	 , 'UINT'				        , 1024 	, '筹码' },
	{ 15, 	1, 'm_rewardTimesArr'	 , 'PstRedRewardTimes'			, 1024 	, '奖励倍数' },
	{ 16,	1, 'm_playerCnt'       	 ,  'UINT'       				, 1     , '玩家人数' },
	{ 17,	1, 'm_winCardType' 		 , 'UINT' 						, 1 , '获胜方牌型' },
	{ 18, 	1, 'm_isDisorderly'		 , 'UINT'						, 1 , '获胜方是否杂花235，1: 是， 0: 否' },
	{ 19,	1, 'm_rewardPool'        , 'UINT'       				, 1 , '奖金池金币' },
	{ 20,	1, 'm_recordId'       	 , 'STRING'       				, 1 , '牌局编号' },
}

CS_G2C_Red_PlayerCnt_Nty = {
	{ 1,	1, 'm_playerCount'       ,  'UINT'       				, 1     , '玩家人数' },
}

CS_G2C_Red_GameFree_Nty = 
{
	{ 1,	1, 'm_timeLeft'			 ,  'UINT'						, 1		, '该状态剩余时间'},
}

CS_G2C_Red_GameStart_Nty = 
{
	{ 1,	1, 'm_timeLeft'			 ,  'UINT'						, 1		, '该状态剩余时间'},
	{ 2,	1, 'm_recordId'       	 , 'STRING'       				, 1 	, '牌局编号' },
}

CS_G2C_Red_GameOpenCard_Nty = 
{
	{ 1,	1, 'm_timeLeft'			 ,  'UINT'						, 1		, '该状态剩余时间'},
	{ 2,	1, 'm_redCard'        	 ,  'UINT'       				, 1024     , '红方牌' },
	{ 3,	1, 'm_blackCard'         ,  'UINT'       				, 1024     , '黑方牌' },
	{ 4,	1, 'm_redCardType'       ,  'UINT'       				, 1     , '红方牌型' },
	{ 5,	1, 'm_blackCardType'     ,  'UINT'       				, 1     , '黑方牌型' },
	{ 6, 	1, 'm_winType'			 , 'UINT'				        , 1		, '0:红方胜， 1:黑方胜 2:和' },
	{ 7, 	1, 'm_luckyStrike'		 , 'UINT'				        , 1 	, '是否幸运一击，1: 是， 0: 否' },
	{ 8,	1, 'm_winCardType' 		 , 'UINT' 						, 1 , '获胜方牌型' },
	{ 9, 	1, 'm_isDisorderly'		 , 'UINT'						, 1 , '获胜方是否杂花235，1: 是， 0: 否' },
}

CS_G2C_Red_GameEnd_Nty = 
{
	{ 1,	1, 'm_timeLeft'			 ,  'UINT'						, 1		, '该状态剩余时间'},
	{ 2,	1, 'm_allResult'      	 ,  'PstRedBalanceInfo'    		, 1024   , '所有玩家结算结果' },
	{ 3,	1, 'm_isOpenReward'      , 'UINT'       				, 1     , '是否奖金池开奖, 1:是, 0:否' },
	{ 4,	1, 'm_rewardPool'        , 'UINT'       				, 1     , '开奖后的奖金池金币' },
	{ 5,	1, 'm_openReward'        , 'UINT'       				, 1     , '奖金池本局随机派奖金额' },
	{ 6,	1, 'm_beforeReward'      , 'UINT'       				, 1     , '开奖前的奖金池金币' },
	{ 7,	1, 'm_rewardArr'         , 'PstRedRewardPoolOrder'      , 1024  , '奖金池发奖情况' },
}

CS_G2C_Red_History_Nty = 
{
	{ 1,	1, 'm_history'			 , 	'PstRedHistory'				, 1024	, '历史记录'},
	{ 2, 	1, 'm_bigEyeRoad'		 , 	'PstRedLuDan'				, 1024	, '大眼仔路' },
	{ 3, 	1, 'm_smallRoad'		 , 	'PstRedLuDan'				, 1024	, '小路' },
	{ 4, 	1, 'm_bugRoad'			 , 	'PstRedLuDan'				, 1024	, '小强路' },
}

CS_C2G_Red_Bet_Req = 
{
	{ 1,	1, 'm_betId'			 , 'UINT'						, 1		, '下注区域ID 0:红,1:黑,2:幸运一击(豹子、同花顺、同花、顺子、对子(10-A))'},
	{ 2,	1, 'm_betValue'			 , 'UINT'						, 1		, '下注额'},
}

CS_G2C_Red_Bet_Ack =
{
	{ 1,	1, 'm_result'			 , 'INT'				, 1		, '结果 0:成功 -x:失败'},
	{ 2,	1, 'm_betId'			 , 'UINT'				, 1		, '下注区域ID  0:红,1:黑,2:幸运一击(豹子、同花顺、同花、顺子、对子(10-A))'},
	{ 3,	1, 'm_betValue'			 , 'UINT'				, 1		, '下注额'},
	{ 4,	1, 'm_allAreaBetArr'     ,  'PstRedAreaBet'     , 1024  , '各区域总下注' },
	{ 5,	1, 'm_myAreaBetArr'      ,  'PstRedAreaBet'     , 1024  , '我在各区域下注' },
	{ 6,	1, 'm_curCoin'	     	 , 'UINT'				, 1		, '当前金币'},
}

CS_G2C_Red_Bet_Nty = 
{
	{ 1,	1, 'm_betId'			 , 'UINT'				, 1		, '下注区域ID  0:红,1:黑,2:幸运一击(豹子、同花顺、同花、顺子、对子(10-A))'},
	{ 2,	1, 'm_betValue'			 , 'UINT'				, 1		, '下注额'},
	{ 3,	1, 'm_allAreaBetArr'     ,  'PstRedAreaBet'     , 1024  , '各区域总下注' },
	{ 4,	1, 'm_accountId'	     , 'UINT'				, 1		, '下注玩家'},
}

CS_C2G_Red_PlayerOnlineList_Req =
{
}

CS_G2C_Red_PlayerOnlineList_Ack = 
{
	{ 1, 	1, 'm_playerList'		 , 'PstRedOnlinePlayerInfo'  	, 1024	, '在线玩家信息' },	
}

CS_G2C_Red_TopPlayerList_Nty = 
{
	{ 1, 	1, 'm_topPlayerList'		 , 'PstRedTopPlayerInfo'  			, 1024		, '前6名玩家(实际数量 <= 6)' },
}

CS_C2G_Red_Background_Req =
{
	{ 1,	1, 'm_type'			 , 'INT'						, 1		, '1-切到后台 2-切回游戏'},
}

CS_G2C_Red_Background_Ack =
{
	{ 1,	1, 'm_type'			 , 'INT'						, 1		, '1-切到后台 2-切回游戏'},
	{ 2,	1, 'm_ret'			 , 'INT'						, 1		, '结果 0-表示成功  <0 表示失败'},
}

CS_C2G_Red_CltInit_Nty =
{
}

CS_C2G_RedRewardPoolHistory_Req =
{
}

CS_G2C_RedRewardPoolHistory_Ack =
{
	{ 1, 	1, 'm_historyList'		 , 'PstRedRewardPoolHistory'  	, 1024	, '奖金池开奖历史记录' },	
}

CS_C2G_Red_ContinueBet_Req =
{
	{ 1,	1, 'm_continueBetArr'	 ,  'PstRedContinueBet'	, 1024	, '玩家续押信息'},
}

CS_G2C_Red_ContinueBet_Ack =
{
	{ 1, 	1, 'm_result'		 	 ,  'INT'					, 1		, '0:成功, -x:失败(m_result=0时，广播此ack)' },
	{ 2, 	1, 'm_betAccountId'	 	 ,  'UINT'					, 1		, '下注玩家ID' },
	{ 3,	1, 'm_curCoin'		 	 ,  'UINT'					, 1		, '下注成功后的玩家金币(仅在m_result=0,且m_betAccountId=自己时有效)'},
	{ 4,	1, 'm_continueBetArr'	 ,  'PstRedContinueBet'	, 1024	, '玩家续押信息'},
	{ 5,	1, 'm_allAreaBetArr'     ,  'PstRedAreaBet'     , 1024  , '各区域总下注' },
	{ 6,	1, 'm_myAreaBetArr'      ,  'PstRedAreaBet'     , 1024  , '我在各区域下注(仅在m_result=0,且m_betAccountId=自己时有效)' },
}

CS_M2C_Red_Exit_Nty =
{
	{ 1		, 1		, 'm_type'		,		'UBYTE'	, 1		, '退出， 0-正常结束 1-分配游戏服失败 2-同步游戏服失败 3-踢人'},
}

----------------------------------------服务器---------------------------------------
SS_M2G_Red_GameCreate_Req =
{
	{ 1		, 1		, 'm_vecAccounts'		,		'PstSyncGameDataEx'			, 1024		, '玩家数据'},
}

SS_G2M_Red_GameCreate_Ack =
{
	{ 1		, 1		, 'm_vecAccounts'		,		'PstOperateRes'				, 1024		, '玩家初始化结果数据'},
}

SS_G2M_Red_GameResult_Nty =
{
	{ 1		, 1		, 'm_vecAccounts'		,		'PstRedBalanceData'	, 1024		, '玩家数据'},
}
