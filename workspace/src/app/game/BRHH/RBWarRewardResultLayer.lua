--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
local HNLayer= require("src.app.newHall.HNLayer") 
local RBWarRes = import(".src.RBWarRes");
local RBWarRewardResultLayer = class("RBWarRewardResultLayer",function ()
     return HNLayer.new()
end)

function RBWarRewardResultLayer:ctor()
  --  self:myInit()
    self:setupView()
end

function RBWarRewardResultLayer:setupView() 
    
    local node = CacheManager:addCSBTo(self, RBWarRes.CSB.RBWarPageRewardResult) UIAdapter:createNode("app/game/BRHH/res/DragonTiger/RBWarPageRewardResult.csb") 
     --UIAdapter:adapter(node,handler(self, self.onTouchCallback)) 
       local center = node:getChildByName("Panel") 
     local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
      UIAdapter:praseNode(node,self)  
	--  self:addChild(node) 
end 

function RBWarRewardResultLayer:show(info) 
    local pScene = g_GameController.gameScene;
    self.tip:setVisible(false)
    self.tip_fail:setVisible(true)
    for k,v in pairs(pScene.m_pRewardArr) do
        if v.m_type<=3  then
            self["gold"..v.m_type]:setString(v.playerArr[1].m_rewardCoin*0.01)
            self["name"..v.m_type]:setString(v.playerArr[1].m_nickname)
            local head = ToolKit:getHead( v.playerArr[1].m_faceId);
            self["icon"..v.m_type]:loadTexture(head, 1);
            self["icon"..v.m_type]:setVisible(true);
        end
         if v.playerArr[1].m_accountId == Player:getAccountID() then
            self.tip:setVisible(true)
            self.tip:setString("恭喜您开奖获得金币："..v.playerArr[1].m_rewardCoin*0.01)
            self.tip_fail:setVisible(false)
            self:getParent()._myInfo:setUserMoney(v.playerArr[1].m_curCoin*0.01) 
        end

        for m,n in pairs(self:getParent()._otherInfo) do
            if n:getTag() == v.playerArr[1].m_accountId then
                n:setUserMoney(v.playerArr[1].m_curCoin*0.01)
            end
        end
    end
     
end

return RBWarRewardResultLayer