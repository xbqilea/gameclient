
local flag = "MSG_GAME_RBWar_Event_";
local RBWarEvent = {
    MSG_INIT                = flag.."MSG_RBWAR_INIT",
    MSG_GAME_STATE          = flag.."MSG_RBWAR_GAME_STATE",
    MSG_HISTORY             = flag.."MSG_RBWAR_HISTORY",
    MSG_BET                 = flag.."MSG_RBWAR_BET",
    MSG_PLAYERONLINELIST    = flag.."MSG_RBWAR_PLAYERONLINELIST",
    MSG_TOPPLAYERLIST       = flag.."MSG_RBWAR_TOPPLAYERLIST",
    MSG_REWARDPOOLHISTORY   = flag.."MSG_RBWAR_REWARDPOOLHISTORY",
    MSG_PLAYERCNT           = flag.."MSG_RBWAR_PLAYERCNT",
    MSG_CONTINUE_BET        = flag.."MSG_CONTINUE_BET";
};


return RBWarEvent;