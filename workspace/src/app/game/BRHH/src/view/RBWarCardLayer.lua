local RBWarRes = import("src/app/game/BRHH/src/RBWarRes");
local Scheduler = require("framework.scheduler");

local RBWarCardLayer = class("RBWarCardLayer", function()
    return display.newLayer();
end);

local SCHEDULER_OPEN_CARD = "SCHEDULER_OPEN_CARD";
local SCHEDULER_CLOCK = "SCHEDULER_CLOCK";
local SCHEDULER_SHOW_WIN_TYPE = "SCHEDULER_SHOW_WIN_TYPE";


function RBWarCardLayer:ctor(card_view)
    self:initData();
    self:initView(card_view);
    ToolKit:registDistructor(self, handler(self, self.onDestory));
end

function RBWarCardLayer:onDestory()
    self:_RemoveAllSomethingLater();
end

function RBWarCardLayer:initData()
    self.m_pHongCards = {};
    self.m_pHeiCards = {};
    self.m_pHongPX = nil;
    self.m_pHeiPX = nil;
    self.m_pRoot = nil;
    self.m_nClockTime = 0;
    self.m_pLeftTimeText = nil;
    self.m_pTimeTilte = nil
    self.m_pGuoWang = nil;
    self.m_pHuangHou = nil;
    self.m_tOpenCardTime = 0;
    self.m_pSomethingLaters = {};
    self.m_bIsHideCard = false;
    self.m_pHongTransform = {};
    self.m_pHeiTransform = {};
end

function RBWarCardLayer:initView(card_view)
    -- local csb_node = cc.CSLoader:createNode(RBWarRes.CSB.OPEN_CARD);
    -- self:addChild(csb_node);
    -- csb_node:setPosition(0, 0);
    -- csb_node:setAnchorPoint(cc.p(0, 0));
    -- local center = csb_node:getChildByName("Layer");
    -- center:setPosition(display.size.width / 2, display.height / 2);
    -- center:setAnchorPoint(cc.p(0.5, 0.5));
    -- self.m_pRoot = center;
    self.m_pRoot = card_view;
    self.m_pHongNode = self.m_pRoot:getChildByName("hong_card");
    self.m_pHeiNode = self.m_pRoot:getChildByName("hei_card");

    self.m_pVsImage = self.m_pRoot:getChildByName("image_vs");
    self.m_pHongPX = self.m_pHongNode:getChildByName("card_type_bg"):getChildByName("card_type");
    self.m_pHeiPX = self.m_pHeiNode:getChildByName("card_type_bg"):getChildByName("card_type");
    self.m_pHongWinImage = self.m_pHongNode:getChildByName("image_win");
    self.m_pHeiWinImage = self.m_pHeiNode:getChildByName("image_win");

    for index = 1, 3 do
        self.m_pHongCards[index] = self.m_pHongNode:getChildByName(string.format("card_%d", index));
        self.m_pHeiCards[index] = self.m_pHeiNode:getChildByName(string.format("card_%d", index));
    end

    local clock_time = self.m_pRoot:getChildByName("clock_node");
    self.m_pLeftTimeText = clock_time:getChildByName("clock_number");
    self.m_pTimeTilte = clock_time:getChildByName("image_state");
    
    --创建动画文件
    local guowang_node = self.m_pRoot:getChildByName("guowang_node");
    local huanghou_node = self.m_pRoot:getChildByName("huanghou_node");

    self.m_pGuoWang = sp.SkeletonAnimation:createWithJsonFile(RBWarRes.Anima.GUO_WANG .. ".json", RBWarRes.Anima.GUO_WANG .. ".atlas");
    self.m_pHuangHou = sp.SkeletonAnimation:createWithJsonFile(RBWarRes.Anima.HUANG_HOU .. ".json", RBWarRes.Anima.HUANG_HOU .. ".atlas");
    guowang_node:addChild(self.m_pGuoWang);
    huanghou_node:addChild(self.m_pHuangHou);
    self.m_pHuangHou:setPositionY(5);
    self.m_pGuoWang:setPositionY(5);
    self.m_pGuoWang:setAnimation(0, "animation1", true);
    self.m_pHuangHou:setAnimation(0, "animation1", true);
    self.m_pGuoWang:setScale(0.9);
    self.m_pHuangHou:setScale(0.9);

    self:_HideCards(false);
    self:_HideTime();
    for index = 1, 3 do
        self.m_pHongTransform[index] = self.m_pHongCards[index]:getNodeToParentTransform();
        self.m_pHeiTransform[index] = self.m_pHeiCards[index]:getNodeToParentTransform();
    end
end

function RBWarCardLayer:setGameState(gameState, time)
    self:_RemoveAllSomethingLater();
    
    self.m_tOpenCardTime = time;
    if GS_BET == gameState then
        self:_RunTime(time);
        self.m_pTimeTilte:loadTexture("DragonTiger/ui/image_22.png");
        self:_HideCards(false);
    elseif GS_SEND_CARD == gameState then
        self:_HideCards(false);
        self.m_pVsImage:setVisible(true);
        self.m_pVsImage:loadTexture("DragonTiger/image/hh_vs.png");
        self:_HideTime();
        local t = self.m_tOpenCardTime - 1.5;
        if 2.8 >= t then
            self:_OpenCard(self.m_tOpenCardTime >= 2.8);
        else
            self.m_tOpenCardTime = self.m_tOpenCardTime - t;
            self:_DoSomethingLater(SCHEDULER_OPEN_CARD, function()
                self:_OpenCard(true);
            end, 1.5, false);
        end
        return;
    elseif GS_PLAY_GAME == gameState then
        self:_HideTime();
    else
        self.m_pTimeTilte:loadTexture("DragonTiger/ui/image_21.png");
        --self:_HideTime();
        self:_RunTime(time);
        self:_HideCards(true);
        return;
    end
end

function RBWarCardLayer:_OpenCard(isAction)
    self.m_bIsHideCard = false;
    self.m_pHongNode:setVisible(true);
    self.m_pHeiNode:setVisible(true);
    -- self.m_pHeiPX:getParent():setVisible(false);
    -- self.m_pHongPX:getParent():setVisible(false);
    -- self.m_pHongWinImage:setVisible(false);
    -- self.m_pHeiWinImage:setVisible(false);
    self.m_pVsImage:setVisible(true);
    self.m_pVsImage:loadTexture("DragonTiger/image/hh_vs.png");
    if isAction then
        self:_ShowFlopCard(1);
    else
        local pScene = g_GameController.gameScene;

        local _setCardTexture = function (card, imagePath)
            local backSize = card:getContentSize();
            card:loadTexture(imagePath);
            local cardSize = card:getContentSize();
            card:setScale(backSize.height / cardSize.height * card:getScale());
        end

        for index = 1, 3 do 
            _setCardTexture(self.m_pHongCards[index], string.format("common/card/%d.png", pScene.m_pRedCard[index]));
            _setCardTexture(self.m_pHeiCards[index], string.format("common/card/%d.png", pScene.m_pBlackCard[index]));
            local pcard ;
        end
        self:_SetCardType(1);
        self:_SetCardType(2);
        self:_ShowWinType();
    end
    
end

function RBWarCardLayer:_ShowFlopCard(idx)
    if idx > 6 then
        self:_DoSomethingLater(SCHEDULER_SHOW_WIN_TYPE, handler(self, self._ShowWinType), 0.1, false);
        return;
    end
    local pScene = g_GameController.gameScene;
    local pCard = nil;
    local cardResPath = "";
    if 4 > idx then
        pCard = self.m_pHongCards[idx];
        cardResPath = string.format("common/card/%d.png", pScene.m_pRedCard[idx]);
    else
        pCard = self.m_pHeiCards[idx - 3];
        cardResPath = string.format("common/card/%d.png", pScene.m_pBlackCard[idx - 3]);
    end
    
    local orbit_1 = cc.OrbitCamera:create(0.2, 1, 0, 0, -90, 0, 0);
    local orbit_2 = cc.OrbitCamera:create(0.2, 1, 0, 90, -90, 0, 0);
    local callfunc = cc.CallFunc:create(function()
        local backSize = pCard:getContentSize();
        pCard:loadTexture(cardResPath);
        local cardSize = pCard:getContentSize();
        pCard:setScale(backSize.height / cardSize.height * pCard:getScale());
    end);

    local endCallFunc = cc.CallFunc:create(function()
        if idx == 3 then
            self:_SetCardType(1);
        elseif 6 == idx then
            self:_SetCardType(2);
        end
        self:_ShowFlopCard(idx + 1);
    end);

    if idx == 3 or 6 == idx then
        local spwan = cc.Spawn:create(cc.ScaleTo:create(0.1, 1.2), orbit_1);
        local scaleto = cc.ScaleTo:create(0.1, 127 / 214);
        pCard:runAction(cc.Sequence:create(spwan, callfunc, orbit_2, scaleto, endCallFunc, nil));
    else
        pCard:runAction(cc.Sequence:create(orbit_1, callfunc, orbit_2, endCallFunc, nil));
    end
end

function RBWarCardLayer:_DoSomethingLater(key, callBack, time, isLoop)
    self:_RemoveSomethingLater(key);
    if isLoop then
        self.m_pSomethingLaters[key] = Scheduler.scheduleGlobal(callBack, time);
    else
        self.m_pSomethingLaters[key] = Scheduler.performWithDelayGlobal(function()
            callBack();
            self.m_pSomethingLaters[key] = nil;
        end, time);
    end
end

function RBWarCardLayer:_RemoveSomethingLater(key)
    if self.m_pSomethingLaters[key] then
        Scheduler.unscheduleGlobal(self.m_pSomethingLaters[key]);
    end
    self.m_pSomethingLaters[key] = nil;
end

function RBWarCardLayer:_RemoveAllSomethingLater()
    for key, callBack in pairs(self.m_pSomethingLaters) do
        Scheduler.unscheduleGlobal(callBack);
        self.m_pSomethingLaters[key] = nil;
    end
end


function RBWarCardLayer:_SetCardType(winType)
    local pScene = g_GameController.gameScene;

    if 1 == winType then
        self.m_pHongPX:getParent():setVisible(true);
        self.m_pHongPX:loadTexture(string.format("DragonTiger/ui/rbwtype_%d.png", pScene.m_nRedCardType));
        -- self.m_pHongPX:setScale(0.8);
    else
        self.m_pHeiPX:getParent():setVisible(true);
        self.m_pHeiPX:loadTexture(string.format("DragonTiger/ui/rbwtype_%d.png", pScene.m_nBlackCardType));
        -- self.m_pHeiPX:setScale(0.8);
    end
end

function RBWarCardLayer:_ShowWinType()
    local pScene = g_GameController.gameScene;
    if pScene.m_nWinType == 0 then
        self.m_pHongWinImage:setVisible(true);
        self.m_pHongWinImage:setScale(2);
        self.m_pHongWinImage:setOpacity(0);
        self.m_pHongWinImage:runAction(cc.Sequence:create(cc.Spawn:create(cc.ScaleTo:create(0.05, 1), cc.FadeIn:create(0.05)), nil));
        self.m_pHongPX:getParent():setVisible(true);
        if pScene.m_bIsDisorderly then
            self.m_pHongPX:loadTexture("DragonTiger/image/rbwtype_6.png");
            -- self.m_pHongPX:setScale(0.8);
        else
            -- self.m_pHongPX:loadTexture(string.format("DragonTiger/image/rbwtype_%d.png", pScene.m_nRedCardType));
        end
        self.m_pHuangHou:setAnimation(0, "animation2", true);
        self.m_pGuoWang:setAnimation(0, "animation3", true);
    elseif pScene.m_nWinType == 1 then
        self.m_pHeiWinImage:setVisible(true);
        self.m_pHeiWinImage:setScale(2);
        self.m_pHeiWinImage:setOpacity(0);
        self.m_pHeiWinImage:runAction(cc.Sequence:create(cc.Spawn:create(cc.ScaleTo:create(0.05, 1), cc.FadeIn:create(0.05)), nil));
        self.m_pHeiPX:getParent():setVisible(true);
        if pScene.m_bIsDisorderly then
            self.m_pHeiPX:loadTexture("DragonTiger/image/rbwtype_6.png");
            -- self.m_pHeiPX:setScale(0.8);
        else
            -- self.m_pHeiPX:loadTexture(string.format("DragonTiger/image/rbwtype_%d.png", pScene.m_nBlackCardType));
        end
        self.m_pHuangHou:setAnimation(0, "animation3", true);
        self.m_pGuoWang:setAnimation(0, "animation2", true);
    else
        self.m_pVsImage:setVisible(true);
        self.m_pVsImage:loadTexture("DragonTiger/image/longfudou_winTxt1.png");
        self.m_pHuangHou:setAnimation(0, "animation2", true);
        self.m_pGuoWang:setAnimation(0, "animation2", true);
    end
    if pScene.m_bIsDisorderly == 1 then
        g_AudioPlayer:playEffect(RBWarRes.Audio["TYPE_6"]); --"DragonTiger/sound/type_6.mp3");
    else
        g_AudioPlayer:playEffect(RBWarRes.Audio["TYPE_"..pScene.m_nWinCardType]);--string.format("DragonTiger/sound/type_%d.mp3", pScene.m_nWinCardType));
    end
end

function RBWarCardLayer:_HideCards(isAction)

    self.m_pVsImage:setVisible(false);
    self.m_pHeiPX:getParent():setVisible(false);
    self.m_pHongPX:getParent():setVisible(false);
    self.m_pHongWinImage:setVisible(false);
    self.m_pHeiWinImage:setVisible(false);

    if self.m_bIsHideCard then
        return;
    end
    self.m_bIsHideCard = true;

    self.m_pGuoWang:setAnimation(0, "animation1", true);
    self.m_pHuangHou:setAnimation(0, "animation1", true);

    local hideCard = function(card)
        local orbit_1 = cc.OrbitCamera:create(0.2, 1, 0, 0, 90, 0, 0);
        local orbit_2 = cc.OrbitCamera:create(0.2, 1, 0, -90, 90, 0, 0);
        card:runAction(
            cc.Sequence:create(
                orbit_1,
                cc.CallFunc:create(function() 
                    card:loadTexture("common/card/cardBack.png");
                    card:setScale(1);
                end),
                orbit_2,
            nil)
        );
    end

    for index = 1, 3 do
        self.m_pHongCards[index]:stopAllActions();
        self.m_pHeiCards[index]:stopAllActions();
        if self.m_pHongTransform[index] then
            self.m_pHongCards[index]:setNodeToParentTransform(self.m_pHongTransform[index]);
        end
        if self.m_pHeiTransform[index] then
            self.m_pHeiCards[index]:setNodeToParentTransform(self.m_pHeiTransform[index]);
        end
    end

    if isAction == true then
        for index = 1, 3 do
            hideCard(self.m_pHongCards[index]);
            hideCard(self.m_pHeiCards[index]);
        end
    else
        for index = 1, 3 do
            self.m_pHongCards[index]:loadTexture("common/card/cardBack.png");
            self.m_pHeiCards[index]:loadTexture("common/card/cardBack.png");
            self.m_pHongCards[index]:setScale(1);
            self.m_pHeiCards[index]:setScale(1);
        end    
    end
    -- self.m_pHongNode:setVisible(false);
    -- self.m_pHeiNode:setVisible(false);
    
    
end

function RBWarCardLayer:_RunTime(leftTime)
    self.m_nClockTime = leftTime;
    self.m_pTimeTilte:setVisible(true);
    self.m_pLeftTimeText:setVisible(true);
    self.m_pLeftTimeText:setString(self.m_nClockTime);
    self:_RemoveSomethingLater(SCHEDULER_CLOCK);
    if leftTime <= 0 then
        self.m_pTimeTilte:setVisible(false);
        self.m_pLeftTimeText:setVisible(false);
        return;
    end
    self:_DoSomethingLater(SCHEDULER_CLOCK, handler(self, self._OnUpdateTime), 1, true);
end

function RBWarCardLayer:_OnUpdateTime(dt)
    self.m_nClockTime = self.m_nClockTime - 1;
    self.m_pLeftTimeText:setString("" .. self.m_nClockTime);
    if self.m_nClockTime <= 0 then
        self.m_nClockTime = 0;
        self:_RemoveSomethingLater(SCHEDULER_CLOCK);
        return;
    end
end

function RBWarCardLayer:_HideTime()
    -- self.m_pLeftTimeText:stopAllActions();
    self.m_pTimeTilte:setVisible(false);
    self.m_pLeftTimeText:setVisible(false);
    self:_RemoveSomethingLater(SCHEDULER_CLOCK);
end



return RBWarCardLayer;