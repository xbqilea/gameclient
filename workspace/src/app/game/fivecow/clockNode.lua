--
--  闹钟节点
--


local clockNode = class("clockNode",function( )
    return cc.Node:create()
end)

function clockNode:ctor(  )
    local path = "subgame/ddz/ddz_clock.csb"
    self.csbNode = cc.uiloader:load(path)
    self:addChild(self.csbNode)

    local ac = cc.CSLoader:createTimeline(path)
    self.csbNode.ac = ac
    self.csbNode:runAction(ac)
    ac:gotoFrameAndPause(0)

    UIAdapter:praseNode(self.csbNode,self)
    local tb = {"anima2_25", "anima2_25_0"}
    common_util.setBlendFunc(self.csbNode,tb, GL_ONE, GL_SRC_ALPHA)
    self.time = self["text_time"]
    self:setVisible(false)

    self.sp = self["Sprite_1"]
end

-- 抖动动画
function clockNode:playShakeAni( is_play )
    local ac = self.csbNode.ac 

    if is_play then
        self:setVisible(true)
        ac:gotoFrameAndPlay(0,60,true )
    else
        ac:gotoFrameAndPause(0)
        self:setVisible(false)
    end
end

function clockNode:setTime(time, is_playSound)
    if time <= 0 then self:setVisible(false) return end

    self:setVisible(true)

    -- if time > 5 then
    --     self.sp:setSpriteFrame("animation_nz_1.png")
    -- else
    --     self.sp:setSpriteFrame("animation_nz_2.png")
    -- end

    local color = cc.c3b(43,198,12)
    if time <= 2 then
        color = cc.c3b(255,29,0)
    elseif time <= 5 and time >= 3 then
        color = cc.c3b(255,166,0)
    end

    self.time:setColor(color)
    self.time:setString(time)

    if is_playSound then
--        if time > 1 and time <=5 then
--            AudioManager:playSound("Redblack>countdown")
--        elseif time == 1 then
--            AudioManager:playSound("Redblack>countdown2")
--        end
    end
end

return clockNode