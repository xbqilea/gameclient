--
--  显示牛几节点
--

local niu_des = {
    [0] ="niuniu_paimian_wuniu.png",
    [1] ="wuniu_01.png",
    [2] ="wuniu_02.png",
    [3] ="wuniu_03.png",
    [4] ="wuniu_04.png",
    [5] ="wuniu_05.png",
    [6] ="wuniu_06.png",
    [7] ="wuniu_07.png",
    [8] ="wuniu_08.png",
    [9] ="wuniu_09.png",
    [10] ="wuniu_niuniu.png",
    [11] ="wuniu_hua.png",
    [12] ="wuniu_zhadan.png",
    [13] ="wuniu_xiao.png",
}


local niu_node = class("niu_node",function( )
    return cc.Node:create()
end)
function niu_node:get_niuDes( niu)
    local str_des = niu_des[niu]
    local res = ""

--    if times > 1 then
--        res = "common_niuniu_di_2.png"
--    else
--        res = "common_niuniu_di_3.png"
--    end

    if niu >= 10 then
        res = "common_niuniu_di_1.png"
    elseif niu == 0 then
        res = "common_niuniu_di_4.png"
    else
        res = "common_niuniu_di_2.png"
    end

    return res, str_des
end
function niu_node:ctor(niu, show_rate)

  --  AudioManager:playSound("Bull>niu", nil, niu)

    show_rate = show_rate or false
     
	local res, niu_num = self:get_niuDes( niu)

    local path = "common/niu1_9.csb"
    self.csbNode = cc.uiloader:load(path)
    self:addChild(self.csbNode)

    local niu_rate = self.csbNode:getChildByName( "beishu")
    niu_rate:setVisible(false)
--    niu_rate:setString("x"..times.."倍")
    local ac = cc.CSLoader:createTimeline(path)
    self.csbNode.ac = ac
    self.csbNode:runAction(ac)

    local aniInfo = ac:getAnimationInfo( "animation0" )
    --ac:gotoFrameAndPlay(0,90, false)


    local image_di = self.csbNode:getChildByName( "img_nn_bg_1")
    image_di:setSpriteFrame(res)

    local text_niu = self.csbNode:getChildByName( "niu_num")
    local text_niu_br = self.csbNode:getChildByName( "niu_num_br")

    text_niu_br:setSpriteFrame(niu_num)
    text_niu:setSpriteFrame(niu_num)

    text_niu_br:setVisible(false)
    text_niu:setVisible(false)

    local posX, posY = text_niu_br:getPosition()
    local size = text_niu_br:getContentSize()

--    self.csbNode.win_node = self.csbNode:getChildByName( "Particle_02")
--    self.csbNode.win_node1 = self.csbNode:getChildByName( "Particle_1")
    self.csbNode.win_node2 = self.csbNode:getChildByName( "TX_ADD")
    self.csbNode.fangda_add = self.csbNode:getChildByName( "fangda_add")
    self.csbNode.niu_tx_ADD = self.csbNode:getChildByName( "5niu_tx_ADD")
    self.csbNode.zhadan_tx = self.csbNode:getChildByName( "zhadan_tx")
    self.csbNode.fangda_add:setSpriteFrame(niu_num)
--    self.csbNode.win_node:setVisible(false)
--    self.csbNode.win_node1:setVisible(false)
    self.csbNode.win_node2:setVisible(false)
    self.csbNode.fangda_add:setVisible(false)
    self.csbNode.niu_tx_ADD:setVisible(false)
    self.csbNode.zhadan_tx:setVisible(false)

    if niu > 0 then
        text_niu_br:setVisible(true)
      --  niu_rate:setVisible(show_rate)
        if show_rate == false then
            text_niu_br:setPositionX(0)
            self.csbNode.fangda_add:setPositionX(0)
        end
        if niu >= 7 then
--            self.csbNode.win_node:setVisible(true)
--            self.csbNode.win_node1:setVisible(true)
            self.csbNode.win_node2:setVisible(true)
        end
        
        if niu == 12 then
            self.csbNode.zhadan_tx:setVisible(true)
        elseif niu == 10 or niu == 11 or niu == 13 then
            self.csbNode.fangda_add:setVisible(true)
            if niu == 11 or niu == 13 then
                self.csbNode.niu_tx_ADD:setVisible(true)
            end
        end
    else
        
        text_niu:setVisible(true)
    end
end



return niu_node