module(..., package.seeall)

CS_M2C_Rob_Exit_Nty =
{
	{ 1		, 1		, 'm_type'		,		'UBYTE'	, 1		, '退出， 0-正常结束 1-分配游戏服失败 2-同步游戏服失败 3-踢人'},
}

CS_M2C_Rob_EnterScene_Nty =
{
	{ 1		, 1		, 'm_gameAtomTypeId'		, 'UINT'				, 1 	, '游戏最小类型ID'},
	{ 2		, 1		, 'm_type'					, 'UBYTE'				, 1		, '类型，0：匹配中，1:已匹配'},
	{ 3		, 1		, 'm_faceId'				, 'UINT'				, 1 	, '头像ID'},
	{ 4		, 1		, 'm_nickname'				, 'STRING'				, 1 	, '昵称'},
	{ 6		, 1		, 'm_goldCoin'				, 'UINT'				, 1 	, '金币'},
}

CS_G2C_Rob_Init_Nty = {
	{ 1,	1, 'm_state'		 	 ,  'UINT'						, 1		, '当前状态: 0:等待其他玩家连接游戏服 1-ready 2-发牌 3-抢庄 4-公布庄家 5-下注 6-拼牛 7-开奖 8-结算'},
	{ 2,	1, 'm_leftTime'		 	 ,  'UINT'						, 1		, '本状态剩余时间(单位秒)'},
	{ 3,	1, 'm_totalTime'		 ,  'UINT'						, 1		, '本状态总时间(单位秒)'},
	{ 4,	1, 'm_initPlayer'        ,  'PstRobInitPlayerInfo'      , 1024  , '本桌玩家数组' },
	{ 5, 	1, 'm_bankerId'			 ,  'UINT'						, 1 	, '庄家账户id(如果为0,表示还没有公布庄家)' },
	{ 6, 	1, 'm_bankerRobScore'	 ,  'UINT'						, 1 	, '庄家抢分 1:抢1 2:抢2 3:抢3 4:抢4(仅在m_bankerId != 0时有效)' },
	{ 7, 	1, 'm_robAnte'	 		 ,  'UINT'						, 1 	, '底注' },
	{ 8, 	1, 'm_roomName'	 		 ,  'STRING'					, 1 	, '房间名' },
	{ 9, 	1, 'm_maxMultiple'	 	 ,  'UINT'						, 1 	, '最大倍数(几倍场)' },
	{ 11, 	1, 'm_recordId'	 		 ,  'STRING'					, 1 	, '牌局编号' },
	{ 12,	1, 'm_myCardArr'		 ,  'PstRobEarlyCard'			, 5		, '我的牌数组(仅在m_state=2、3、4、5、6时有效)(最后一张牌如果为0，表示暗牌)'},
	{ 13,	1, 'm_robProcess'        ,  'PstRobBankerProcess'      	, 1024  , '抢庄过程(仅在m_state=3时有效)(部分玩家)' },
	{ 14,	1, 'm_betProcess'        ,  'PstRobBetProcess'      	, 1024  , '下注过程(仅在m_state=5时有效)(部分玩家)' },
	{ 15,	1, 'm_assembleProcess'   ,  'PstRobAssembleProcess'     , 1024  , '拼牛过程(仅在m_state=6时有效)(部分玩家)' },
	{ 16,	1, 'm_openCardResult'    ,  'PstRobOpenCardProcess'     , 1024  , '开牌结果(仅在m_state=7 或 m_state=8 时有效)' },
	{ 17,	1, 'm_bankerScoreOp'	 ,  'PstRobBankerScoreOp'		, 1024	, '玩家可操作的抢庄分数(仅在m_state=3,并且 玩家还未抢庄操作时有效)'},
	{ 18,	1, 'm_betMultipleOp'	 ,  'PstRobBetMultipleOp'		, 1024	, '玩家可操作的下注倍数(仅在m_state=5,并且 玩家还未下注倍数时有效)'},
}

CS_C2G_Rob_Background_Req =
{
	{ 1,	1, 'm_type'			 , 'INT'						, 1		, '1-切到后台 2-切回游戏'},
}

CS_G2C_Rob_Background_Ack =
{
	{ 1,	1, 'm_type'			 , 'INT'						, 1		, '1-切到后台 2-切回游戏'},
	{ 2,	1, 'm_ret'			 , 'INT'						, 1		, '结果 0-表示成功  <0 表示失败'},
}

CS_G2C_Rob_GameReady_Nty = 
{
	{ 1,	1, 'm_leftTime'			 ,  'UINT'						, 1		, '该状态剩余时间'},
}

CS_G2C_Rob_DispatchCard_Nty =
{
	{ 1,	1, 'm_leftTime'			 ,  'UINT'						, 1		, '该状态剩余时间'},
	{ 2,	1, 'm_myCardArr'		 ,  'PstRobEarlyCard'			, 5		, '我的牌数组(最后一张牌如果为0，表示暗牌)'},
}

CS_G2C_Rob_RobBanker_Nty = 
{
	{ 1,	1, 'm_leftTime'			 ,  'UINT'						, 1		, '该状态剩余时间'},
	{ 2,	1, 'm_bankerScoreOp'	 ,  'PstRobBankerScoreOp'		, 1024	, '玩家可操作的抢庄分数'},
}

CS_G2C_Rob_ShowBanker_Nty =
{
	{ 1,	1, 'm_leftTime'			 ,  'UINT'						, 1		, '该状态剩余时间'},
	{ 2, 	1, 'm_bankerId'			 ,  'UINT'						, 1 	, '庄家账户id' },
	{ 3, 	1, 'm_bankerRobScore'	 ,  'UINT'						, 1 	, '庄家抢分, 1:抢1 2:抢2 3:抢3 4:抢4' },
	{ 4, 	1, 'm_isRand'			 ,  'UINT'						, 1 	, '是否随机产生庄家, 0:否, 1:是' },
	{ 5, 	1, 'm_randPlayerArr'	 ,  'UINT'						, 1024 	, '随机产生庄家的玩家数组' },
}

CS_G2C_Rob_Bet_Nty =
{
	{ 1,	1, 'm_leftTime'			 ,  'UINT'						, 1		, '该状态剩余时间'},
	{ 2,	1, 'm_betMultipleOp'	 ,  'PstRobBetMultipleOp'		, 1024	, '玩家可操作的下注倍数'},
}

CS_G2C_Rob_Assemble_Nty =
{
	{ 1,	1, 'm_leftTime'			 ,  'UINT'						, 1		, '该状态剩余时间'},
	{ 2,	1, 'm_myCardArr'		 ,  'PstRobEarlyCard'			, 5		, '我的牌数组(最后一张牌如果为0，表示暗牌)'},
}

CS_G2C_Rob_OpenCard_Nty =
{
	{ 1,	1, 'm_leftTime'			 ,  'UINT'						, 1		, '该状态剩余时间'}, 
	{ 2,	1, 'm_openCardResult'    ,  'PstRobOpenCardProcess'     , 1024  , '开牌结果' },
}

CS_G2C_Rob_Balance_Nty = 
{
	{ 1,	1, 'm_balanceArr'		,  'PstRobBalanceClt'			, 1024		, '结算数组'},
}

CS_C2G_Rob_Banker_Req =
{
	{ 1, 	1, 'm_robScore'		, 'UINT'				, 1		, '抢庄分数, 0:不抢 1:抢1 2:抢2 3:抢3 4:抢4' },
}

CS_G2C_Rob_Banker_Ack  =
{
	{ 1, 	1, 'm_result'		, 'INT'				, 1		, '0:成功, -x:失败(m_result=0时，广播此ack)' },
	{ 2, 	1, 'm_robAccountId'	, 'UINT'			, 1		, '抢庄玩家ID' },
	{ 3, 	1, 'm_robScore'		, 'UINT'			, 1		, '抢庄分数, 0:不抢 1:抢1 2:抢2 3:抢3 4:抢4' },
}

CS_C2G_Rob_Bet_Req =
{
	{ 1, 	1, 'm_betMultiple'	, 'UINT'			, 1		, '下注倍数' },
}

CS_G2C_Rob_Bet_Ack =
{
	{ 1, 	1, 'm_result'		, 'INT'				, 1		, '0:成功, -x:失败(m_result=0时，广播此ack)' },
	{ 2, 	1, 'm_betAccountId'	, 'UINT'			, 1		, '下注玩家ID' },
	{ 3, 	1, 'm_betMultiple'	, 'UINT'			, 1		, '下注倍数' },
}

CS_C2G_Rob_Assemble_Req =
{
	{ 1, 	1, 'm_opType'			, 'UINT'			, 1		, '操作类型, 0:无牛 1:有牛' },
}

CS_G2C_Rob_Assemble_Ack =
{
	{ 1, 	1, 'm_result'			 ,  'INT'					, 1		, '0:成功, -x:失败' },
	{ 2, 	1, 'm_cardUnit'			 ,  'PstRobCardUnit'		, 1024	, '拼牛结果' },
}

CS_G2C_Rob_AssembleFinish_Nty =
{
	{ 2, 	1, 'm_accountId'	, 'UINT'			, 1		, '已完成拼牛玩家ID' },
}

CS_C2G_Rob_Exit_Req =
{
}

CS_G2C_Rob_Exit_Ack =
{
	{ 1, 	1, 'm_result'			 ,  'INT'					, 1		, '0:成功, -x:失败' },
	{ 2, 	1, 'm_keepCoin'			 ,  'UINT'					, 1		, '暂扣金币(m_result = 0,并且m_keepCoin != 0时提示)' },
}

CS_C2G_RobCheckExit_Req =
{
}

CS_G2C_RobCheckExit_Ack = {
	{ 1, 	1, 'm_result'			 ,  'INT'					, 1		, '0:成功, -x:失败' },
	{ 2, 	1, 'm_keepCoin'			 ,  'UINT'					, 1		, '暂扣金币(m_result = 0,并且m_keepCoin != 0时提示)' },
}

-- ----------------------------------------服务器---------------------------------------
SS_M2G_Rob_GameCreate_Req =
{
	{ 1		, 1		, 'm_gameObjId'			, 		'STRING'					, 1			, 'gameObjId' },
	{ 2		, 1		, 'm_vecAccounts'		,		'PstRobSceneSyncGameData'	, 1024		, '玩家数据'},
}

SS_G2M_Rob_GameCreate_Ack =
{
	{ 1		, 1		, 'm_gameObjId'			, 		'STRING'					, 1			, 'gameObjId' },
	{ 2		, 1		, 'm_result'			, 		'INT'						, 1			, '0:成功, -1:失败' },
	{ 3		, 1		, 'm_vecAccounts'		,		'PstOperateRes'				, 1024		, '玩家初始化结果数据'},
}

SS_G2M_Rob_GameResult_Nty =
{
	{ 1		, 1		, 'm_gameObjId'			, 		'STRING'			, 1			, 'gameObjId' },
	{ 2		, 1		, 'm_type'				, 		'UINT'				, 1			, '类型,0:全部正常结束, 1:单个玩家踢人' },
	{ 3		, 1		, 'm_vecAccounts'		,		'PstRobBalanceData'	, 1024		, '玩家数据'},
}
