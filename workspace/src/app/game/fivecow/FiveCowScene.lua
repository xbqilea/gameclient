--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--
-- FiveCowScene
-- Author: chenzhanming
-- Date: 2018-10-24 10:00:00
-- 百家乐主场景
--
local Scheduler           = require("framework.scheduler") 
local CCGameSceneBase     = require("src.app.game.common.main.CCGameSceneBase") 
local FiveCowMainLayer     = import(".FiveCowMainLayer")
local DlgAlert = require("app.hall.base.ui.MessageBox")
    
	
local FiveCowScene= class("FiveCowScene", function()
        return CCGameSceneBase.new()
    end)


function FiveCowScene:ctor()  
    self.m_FiveCowMainLayer         = nil 
    self.m_FiveCowExitGameLayer     = nil
    self.m_FiveCowRuleLayer         = nil
    self.m_FiveCowMusicSetLayer     = nil 
    self:myInit()
end

-- 游戏场景初始化
function FiveCowScene:myInit() 
 -- self:loadResources()
  --FiveCowRoomController:getInstance():setInGame( true )  
  -- 主ui
 
  self:initFiveCowGameMainLayer()


  self:registBackClickHandler(handler(self, self.onBackButtonClicked)) -- Android & Windows注册返回按钮
  addMsgCallBack(self, MSG_ENTER_FOREGROUND, handler(self, self.onEnterForeground)) -- 转前台
  addMsgCallBack(self, MSG_ENTER_BACKGROUND, handler(self, self.onEnterBackground)) -- 转后台 
  --addMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT, handler(self, self.socketState))
  --addMsgCallBack(self, POPSCENE_ACK,handler(self,self.showEndGameTip))
end

--[[
function FiveCowScene:showEndGameTip()
	local dlg = DlgAlert.showTipsAlert({title = "提示", tip = "游戏已结束", tip_size = 34})
    dlg:setSingleBtn("确定", function ()
		dlg:closeDialog()
		g_GameController:releaseInstance()
    end)
    dlg:setBackBtnEnable(false)
    dlg:enableTouch(false)
end
--]]

---- 进入场景
function FiveCowScene:onEnter()
   print("-----------FiveCowScene:onEnter()-----------------")
   ToolKit:setGameFPS(1/60.0) 

end


-- 初始化主ui
function FiveCowScene:initFiveCowGameMainLayer()
    self.m_FiveCowMainLayer = FiveCowMainLayer.new()
    self:addChild(self.m_FiveCowMainLayer)
end

function FiveCowScene:getMainLayer()
    return self.m_FiveCowMainLayer 
end

-- 显示游戏退出界面
-- @params msgName( string ) 消息名称
-- @params __info( table )   退出相关信息
-- 显示游戏退出界面
--[[
function FiveCowScene:showExitGameLayer()
    if self._Scheduler1 then
        Scheduler.unscheduleGlobal(self._Scheduler1)	
        self._Scheduler1 = nil
    end 
    UIAdapter:popScene()
    g_GameController.gameScene = nil
end
--]]

function FiveCowScene:onEnter()
	print("--------------FiveCowScene:onEnter begin-----------------------")
	print("--------------FiveCowScene:onEnter end-----------------------")
end
 
-- 退出场景
function FiveCowScene:onExit()
    print("--------------FiveCowScene:onExit begin-----------------------")
	
	if self.m_FiveCowMainLayer then
		self.m_FiveCowMainLayer:onExit()
		self.m_FiveCowMainLayer = nil
	end

    removeMsgCallBack(self, MSG_ENTER_FOREGROUND)
    removeMsgCallBack(self, MSG_ENTER_BACKGROUND)
    --removeMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT)
    --removeMsgCallBack(self, POPSCENE_ACK)
	
--    FiveCowGlobal.m_isNeedReconectGameServer = false
--    FiveCowRoomController:getInstance():setInGame( false )
--    FiveCowGameController:getInstance():onDestory()
 --   self:RemoveResources()
	print("--------------FiveCowScene:onExit end-----------------------")
	
end

-- 响应返回按钮事件
function FiveCowScene:onBackButtonClicked()
	if not g_GameController.m_canOut then
		TOAST("您处于游戏阶段，退出失败")
		return
    end
    g_GameController:rewFiveCowForceExit()  
end

-- 从后台切换到前台
function FiveCowScene:onEnterForeground()
	print("从后台切换到前台")  
	--g_GameController:gameBackgroundReq(2)
    if self:getMainLayer().m_endFlag then
        g_GameController:gameBackgroundReq(2)
        self:getMainLayer():setReadyVisible(true)
    else
        g_GameController:reqEnterScene()
        self:getMainLayer():fini()
    end
end

-- 从前台切换到后台
function FiveCowScene:onEnterBackground()
   print("从前台切换到后台,游戏线程挂起!")
   g_GameController:gameBackgroundReq(1)
   g_GameController.m_BackGroudFlag=true
end

function FiveCowScene:clearView()
   print("FiveCowScene:clearView()")
end
-- 清理数据
function FiveCowScene:clearData()
   
end

--退出游戏处理
--[[
function FiveCowScene:exitGame()   
    if self.m_FiveCowMainLayer._Scheduler1 then
        Scheduler.unscheduleGlobal(self.m_FiveCowMainLayer._Scheduler1)	
        self.m_FiveCowMainLayer._Scheduler1 = nil
    end 
      if self.m_FiveCowMainLayer._Scheduler2 then
            Scheduler.unscheduleGlobal(self.m_FiveCowMainLayer._Scheduler2)	
            self.m_FiveCowMainLayer._Scheduler2 = nil 
        end 
    UIAdapter:popScene()
    g_GameController.gameScene = nil
    g_GameController:onDestory()
end
--]]

return FiveCowScene
