--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

--扑克管理类 管理整个游戏的动作和牌型

local QznnPokerMgr = class("QznnPokerMgr")
local Poker = require("game.qznn.layer.QznnPoker")

local Player = cc.exports.PlayerInfo

local skewDtStart     = 0.2
local skewDtEnd       = 0.3
local pokerSelfScale  = 1
local pokerOtherScale = 0.8
local pokerDisMe      = 130
local pokerDisOther   = 35

QznnPokerMgr.g_instance = nil

function QznnPokerMgr.getInstance()
    if not QznnPokerMgr.g_instance then
        QznnPokerMgr.g_instance = QznnPokerMgr:new()
    end
    return QznnPokerMgr.g_instance
end
function QznnPokerMgr.releaseInstance()
    QznnPokerMgr.g_instance:clean()
    QznnPokerMgr.g_instance = nil
end

function QznnPokerMgr:clean()
    for i = 0 ,4 do
        for n = 0 ,4 do
            self.Pokers[i][n] = nil
        end
    end
    self.Pokers = {}
    self.targetPos = {}

    cc.Director:getInstance():setProjection(cc.DIRECTOR_PROJECTION3_D)
end

function QznnPokerMgr:ctor()
    --初始化动作
    self.m_wMeViewChiarIdx = 2
    self.startScale = 0.2

    self.disNum = 0.05
    self.flyDt = 0.35
    self.moveDt = 0.2

    self.allDt = self.disNum + self.flyDt + self.moveDt + skewDtStart + skewDtEnd

    local winSize = cc.Director:getInstance():getWinSize()
    self.startPos = cc.p(winSize.width*0.5,winSize.height*0.5)

    self.Pokers = {}
    self.targetPos = {}

    for i = 0 ,4 do
        self.Pokers[i] = {}
        for n = 0 ,4 do
            self.Pokers[i][n] = nil
        end
    end

    --self.Projection = cc.Director:getInstance():getProjection()
    cc.Director:getInstance():setProjection(cc.DIRECTOR_PROJECTION2_D)

end

--设置动画的各种参数  startPos targetPos [] 
function QznnPokerMgr:setAnimData(_pokerNode,_startPos,_targetPos,_pathPos)
    --初始化动作
    self.pokerNode = _pokerNode
    self.startPos = _startPos
    self.targetPos = table.copy(_targetPos)
    self.pathPos = table.copy(_pathPos)

    for i = 0 ,4 do
        for n = 0 ,4 do
            self.Pokers[i][n] = Poker:create()
            self.Pokers[i][n]:setVisible(false)
            self.pokerNode:addChild(self.Pokers[i][n])
        end
    end
end

function QznnPokerMgr:getPokers(index)
    return self.Pokers[index]
end

--提供单例方法 出现动画 展示发牌动画 设置回调函数

--发牌
--
function QznnPokerMgr:showFaPaiAni(idx,pokerDatas)
    --发牌延时
    --local faPaiDt = {[0] = 0.45, 0.6, 0.0, 0.15, 0.3}
    local faPaiDt = {[0] = 0.3, 0.4, 0.0, 0.1, 0.2}
    
    --创建5张牌 发牌
    for n = 0 ,4 do
        --没有创建
        self.Pokers[idx][n]:stopAllActions()
        --设置牌背面
        self.Pokers[idx][n]:setBack()

        --重置位置 重置大小
        self.Pokers[idx][n]:setPosition(self.startPos)
        self.Pokers[idx][n]:setScale(self.startScale)
        self.Pokers[idx][n]:setSkewX(0)
        self.Pokers[idx][n]:setRotation(0)

        --设置牌数据
        self.Pokers[idx][n]:setData(pokerDatas[n])
        self.Pokers[idx][n]:setVisible(true)

        --自己带翻牌
        if idx == self.m_wMeViewChiarIdx then
            --执行对应的缩放变化
            self.Pokers[idx][n]:runAction(cc.Sequence:create(
                cc.DelayTime:create(faPaiDt[idx]),
                cc.DelayTime:create(n*self.disNum),

                cc.ScaleTo:create(self.flyDt, pokerSelfScale),
                cc.DelayTime:create(self.disNum*4 - self.disNum*n),
                cc.DelayTime:create(self.moveDt)

            ))

            local bezier = {  
                self.pathPos[idx],
                self.pathPos[idx],
                self.targetPos[idx],  
            }  

            self.Pokers[idx][n]:runAction(cc.Sequence:create(
                cc.DelayTime:create(faPaiDt[idx]),
                cc.DelayTime:create(n*self.disNum),
                cc.BezierTo:create(self.flyDt, bezier),--array
                cc.DelayTime:create(self.disNum*4 - self.disNum*n),
                cc.MoveTo:create(self.moveDt,cc.p(self.targetPos[idx].x + n*pokerDisMe ,self.targetPos[idx].y)),
                cc.OrbitCamera:create(skewDtStart,1,0,0,90,0,0),--1,0,0,90,0,0
                cc.CallFunc:create(function()
                    self.Pokers[idx][n]:showCard()
                end),
                cc.OrbitCamera:create(skewDtEnd,1,0,270,90,0,0)-- 1,0,270,90,0,0
            ))
        else
            --其他玩家
            self.Pokers[idx][n]:setScale(self.startScale)
            --执行对应的缩放变化
            self.Pokers[idx][n]:runAction(cc.Sequence:create(
                cc.DelayTime:create(faPaiDt[idx]),
                cc.DelayTime:create(n*self.disNum),

                cc.ScaleTo:create(self.flyDt,pokerOtherScale),
                cc.DelayTime:create(self.disNum*4 - self.disNum*n),
                cc.ScaleTo:create(self.moveDt,pokerOtherScale)
            ))

            --旋转变化 
            self.Pokers[idx][n]:runAction(cc.Sequence:create(
                cc.DelayTime:create(faPaiDt[idx]),
                cc.DelayTime:create(n*self.disNum),

                cc.DelayTime:create(self.flyDt),
                cc.DelayTime:create(self.disNum*4 - self.disNum*n),
                cc.DelayTime:create(self.moveDt)
            ))

            local bezier = {  
                self.pathPos[idx],
                self.pathPos[idx],
                self.targetPos[idx],  
            }  
            self.Pokers[idx][n]:runAction(cc.Sequence:create(
                cc.DelayTime:create(faPaiDt[idx]),
                cc.DelayTime:create(n*self.disNum),

                cc.BezierTo:create(self.flyDt, bezier),
                cc.DelayTime:create(self.disNum*4 - self.disNum*n),
                cc.MoveTo:create(self.moveDt,cc.p(self.targetPos[idx].x + n*pokerDisOther ,self.targetPos[idx].y))
            ))
        end
    end
end

--设置自己的牌动画不可见
function QznnPokerMgr:disCardAnim(idx)
    for n = 0 ,4 do
        if self.Pokers[idx][n] ~= nil then
            self.Pokers[idx][n]:setVisible(false)
        end
    end
end

function QznnPokerMgr:disCardAnimAll()
    for i = 0 ,4 do
        for n = 0 ,4 do
            self.Pokers[i][n]:setVisible(false)
        end
    end
end


--设置所有动画不可见


return QznnPokerMgr
--endregion
