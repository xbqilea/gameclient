--QznnMainLayer
local QznnMainLayer = class("QznnMainLayer", cc.Layer)

local QznnRuleLayer  = require("game.qznn.layer.QznnRuleLayer")
local QznnDataMgr    = require("game.qznn.manager.QznnDataMgr")
local Qznn_Events    = require("game.qznn.scene.Qznn_Events")
local Qznn_Res       = require("game.qznn.scene.Qznn_Res")
local CMsgQznn       = require("game.qznn.proxy.CMsgQznn")
local QznnPokerMgr   = require("game.qznn.layer.QznnPokerMgr")
local CommonUserInfo = require("common.layer.CommonUserInfo")
local Effect         = require("common.manager.Effect")
local ParticalPool   = require("game.qznn.bean.ParticalPool")
local GoldPool       = require("game.qznn.bean.GoldPool")
local ChatLayer      = require("common.layer.ChatLayerNew")
local RollMsg        = require("common.layer.RollMsg")
local GameListConfig    = require("common.config.GameList")    --等级场配置

local Veil           = cc.exports.Veil
local Player         = cc.exports.PlayerInfo
local SLFacade       = cc.exports.SLFacade
local scheduler      = cc.exports.scheduler
local AudioManager   = cc.exports.AudioManager
local CUserManager   = cc.exports.CUserManager
local Public_Events  = cc.exports.Public_Events

local function MSG_PRINT(...) if false then printf(...) end end
---------------------------- constant -----------------------------------------
local IPHONEX_OFFSETX   = 100
local Z_MODEL     = 90
local Z_TOP       = 100
local WAIT_PLAYER = 0 --等待玩家
local WAIT_NEXT   = 1 --等待下一局
local WAIT_START  = 2 --等待开始

local TIP_CALL    = 0 --提示叫庄
local TIP_BET     = 1 --提示下注
local TIP_FINISH  = 2 --提示结束

local NO_OPERATION_COUNT = 3 --未操作轮数
local SELF_VIEWCHAIR = 2 --玩家默认显示位置索引
local GAME_PLAYER_QZNN = 5 --玩家
local CARD_COUNT_QZNN = 5 --牌数
local POS_OF_CENTER = cc.p(667, 375) --中心位置
local TipAniPos =      {[0] = cc.p(590, 515), cc.p(455, 277), cc.p(1025, 18), cc.p(877, 277),  cc.p(740, 515) } -- 提示动画最终显示位置
local UserInfoOffset = {[0] = cc.p(70, -70),  cc.p(70, -70),  cc.p(70, -70),  cc.p(-360, -70), cc.p(-360, -70)} -- 玩家信息弹出框相对位移
local CardTypeOffset = {[0] = cc.p(70, -38),  cc.p(70, -38),  cc.p(260, -37), cc.p(70, -38),   cc.p(70, -38)} -- 牌型信息相对发牌位置位移
local TipAniScale = 0.6
local CallStr = {[0] = "不抢", "抢x1", "抢x2", "抢x4"}
local BetStr = {[0] = "x5", "x10", "x15", "x20"}
---------------------------- constant -----------------------------------------

---------------------------- constructor --------------------------------------
QznnMainLayer.instance_ = nil
function QznnMainLayer.create()
    QznnMainLayer.instance_ = QznnMainLayer.new()
    return QznnMainLayer.instance_
end

function QznnMainLayer:ctor()
    self:enableNodeEvents()
end

function QznnMainLayer:onEnter()
    print(" - Qznn Layer onEnter - ")
    self:init()
end

function QznnMainLayer:onExit()
    self:clear()
    print(" - Qznn Layer onExit - ")
end

function QznnMainLayer:initAllVar_()
    --ccb
    self.m_rootUI = nil
    --node(6)
    self.m_pNodeTopMenu = nil   --顶部noode
    self.m_pNodeCard = nil      --牌面node
    self.m_pNodePlay = nil      --操作node
    self.m_pNodeType = nil      --结果node
    self.m_pNodeUser = nil      --玩家node
    self.m_pNodeAni = nil       --动画node
    --node bg
    self.m_pLbBaseScore = nil --底分label
    --node button
    self.m_pBtnRule = nil      --规则button
    self.m_pBtnVoice = nil     --声音button
    self.m_pBtnSound = nil     --音效button
    self.m_pBtnMusic = nil     --音乐button
    self.m_pBtnSubmieNiu = nil --摊牌button
    --node play(8)
    self.m_pNodeCallTips = nil --抢庄提示node
    self.m_pNodeBetTips = nil --下注提示node
    self.m_pNodeFinishTips = nil --完成提示node
    self.m_pNodeCall = nil --抢庄node
    self.m_pNodeBet = nil --下注node
    self.m_pNodeMatch = nil --配对node
    self.m_pNodeClock = nil --闹钟node
    self.m_pNodeResult = nil --结果node
    --LayerColor
    self.m_pGrayLayer = nil
    --node play call tips
    self.m_pNodeCallTip = {} --抢庄提示node
    --node play bet tips
    self.m_pNodeBetTip = {} --下注提示node
    self.m_pLbBetTip = {} --下注提示label
    --node play finish
    self.m_pSpFinish = {} --完成提示sprite
    --node play call
    self.m_pBtnCall = {} --抢庄倍数button
    --node play bet
    self.m_pBtnBet = {} --下注倍数button
    --node result
    self.m_pSpResult = {} --结果sprite
    --node user
    self.m_pNodeUsers = {} --玩家node
    self.m_pSpUserHead = {} --玩家头像sprite
    self.m_pSpHeadFrame = {} --玩家框sprite
    self.m_pSpHeadVip = {} --vip
    self.m_pLbUserNick = {} --玩家昵称label
    self.m_pLbUserGold = {} --玩家分数label
    self.m_pNodeUserBtn = {} --玩家按钮button
    self.m_pNodeUserBank = {} --玩家庄家node
    self.m_pNodeUserScore = {} --玩家得分node
    --self.m_pNodeUserInfo = {} --玩家弹窗node
    self.m_pSpBlink = {} --玩家闪烁sprite
    self.m_pNodeUserBankerNode = nil --庄家动画node
    self.m_pNodeUserScoreNode = nil --结算分数node
    --node ani
    self.m_pNodeFree = nil  --动画node
    self.m_pNodeEnd = nil   --结束node
    self.m_pNodeTop = nil   --顶层node
    --node left menu
    self.m_pNodeMenu = nil
    --node entrust
    self.m_pNodeEntrust = nil
    --node start tips
    self.m_pNodeStartTips = nil --游戏开始提示node
    self.m_pStartTips = nil --游戏开始提示
    --node clock status tips
    --self.m_pStatusTips = nil

    --ui
    self.m_pLbResultScore = {} --结算分数
    self.m_pLbStartCountDown = nil--开始倒计时
    self.m_pLbPoint = {}--配牌点数
    self.m_pClippingMenu = nil --系统按钮

    --ani
    self.m_pArmBanker = nil --叫庄动作
	self.m_pArmWaitStart = nil --等待开始动画
	self.m_pArmWaitNext = nil --下局开始动画
	self.m_pArmWaitPlayer = nil --等待玩家动画

    --save
    self.event_ = {}
    self.handle_ = {}

    --data
    self.m_posOfMenuX = 0
    self.m_posOfMenuY = 0
    self.m_posOfMenuU = 0

    self.m_bIsKill = false --被踢
    self.m_bAuto = false --自动
    self.m_bGameInitStart = false --开始
    self.m_bHaveReady = false --已准备
    self.m_bHaveOperate = false --已操作
    self.m_bShowEnd = false --动作结束
    self.m_bShowMenu = false --显示菜单
    self.m_bIsMenuMove = false
    self.m_bIsEntrust = false --玩家托管操作
    self.m_bIsDisPathChEnd = false --发牌完成
    
    self.m_nNoOperateCount = 0 --未操作计数
    self.m_wMeViewChair = 2 --自己
    self.m_nCountDown = 0 --计时
	self.m_nClockSoundID = 0 --音声id
    self.m_nWaitStatus = -1 
    self.m_nCardChooseIndex = {} --选中牌
    self.m_RandomBankers = {} --?
    self.m_nRandomBankerIndex = 0 --?
    self.m_bWaitRandonBanker = false
    self.m_nCardPoint = {} --点数
    self.m_PlayUsers = {} --用户数据
    self.m_pChooseBankers = {} --
    self.m_pNodeUserBankPos = {}
    self.m_cardBtns = {}

    self.m_pokerPos = {}
    self.m_pathPos = {}
    self.m_flyGoldPos = {}
    self.m_tipPos = {} --提示信息原始坐标
end
---------------------------- constructor --------------------------------------

---------------------------- init ---------------------------------------------
function QznnMainLayer:init() --onEnter->init
    local _do = {
        { func = self.initVar,      log = "初始化变量",  },
        { func = self.initCCB,      log = "加载界面",    },
        { func = self.initNode,     log = "初始化界面",  },
        { func = self.initEvent,    log = "注册监听",    },
        { func = self.initGameEvent,log = "注册监听",    },
        { func = self.initUpdate,   log = "注册循环",    },
        { func = self.initGame,     log = "开始游戏",    },
    }

    for i, init in pairs(_do) do
        MSG_PRINT(" - [%d] - [%s] - %s", i, init.func(self) or "ok",  init.log)
    end
end

function QznnMainLayer:clear() --onExit->clear

    local _do = {
        { func = self.cleanEvent,   log = "取消监听",   },
        { func = self.stopGameEvent,log = "取消监听",   },
        { func = self.stopUpdate,   log = "停止循环",   },
        { func = self.cleanGame,    log = "清理游戏",   },
    }
    
    for i, clean in pairs(_do) do
        MSG_PRINT(" - [%d] - [%s] - %s", i, clean.func(self) or "ok", clean.log)
    end
end

function QznnMainLayer:initVar()
    self:initAllVar_()
end
function QznnMainLayer:initCCB()

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    --csb
    self.m_pathUI = cc.CSLoader:createNode(Qznn_Res.CSB_MAIN_LAYER)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0, diffY))
    self.m_pathUI:addTo(self.m_rootUI)
    self.m_rootWidget = self.m_pathUI:getChildByName("QZNiuNiuMainView")
    
    --node(6)
    self.m_pNodeTopMenu   = self.m_rootWidget:getChildByName("node-topmenu")
    self.m_pNodeTopMenu:setLocalZOrder(100)
    self.m_pNodeCard      = self.m_rootWidget:getChildByName("node-card")
    self.m_pNodePlay      = self.m_rootWidget:getChildByName("node-play")
    self.m_pNodeType      = self.m_rootWidget:getChildByName("node-type")
    self.m_pNodeUser      = self.m_rootWidget:getChildByName("node-users")
    self.m_pNodeAni       = self.m_rootWidget:getChildByName("node-ani")
    self.m_pNodeVoice     = self.m_rootWidget:getChildByName("node-voice")
    self.m_pNodeVoice:setLocalZOrder(101)
    self.m_pNodeBg        = self.m_rootWidget:getChildByName("node-bg")
    self.m_pNodeStartTips = self.m_rootWidget:getChildByName("node-start-tips")
    self.m_pNodeMatch     = self.m_pNodePlay:getChildByName("node-match")
    --node button
    self.m_pBtnVoice     = self.m_pNodeTopMenu:getChildByName("btn_pop")
 
    --node menu
    self.m_pNodeMenu     = self.m_pNodeVoice:getChildByName("node-menu")
    self.m_pMenuBg       = self.m_pNodeMenu:getChildByName("Sprite_bg")
    self.m_pBtnRule      = self.m_pMenuBg:getChildByName("btn_rule")
    self.m_pBtnSound     = self.m_pMenuBg:getChildByName("btn_sound")
    self.m_pBtnMusic     = self.m_pMenuBg:getChildByName("btn_music")

    self.m_pBtnSubmieNiu = self.m_pNodeMatch:getChildByName("btn_takecard")

    local _nodeCardbtn = self.m_pNodeMatch:getChildByName("node-cardbutton")
    for i = 1, 5 do
        self.m_cardBtns[i] = _nodeCardbtn:getChildByName("btn_card" .. i)
    end

    --node label
    self.m_pLbBaseScore = self.m_pNodeBg:getChildByName("Image_desk"):getChildByName("Text_basescore")

    --node play
    self.m_pGrayLayer       = self.m_pNodePlay:getChildByName("graylayer")
    self.m_pNodeCallTips    = self.m_pNodePlay:getChildByName("node-call-tips")
    self.m_pNodeBetTips     = self.m_pNodePlay:getChildByName("node-bet-tips")
    self.m_pNodeFinishTips  = self.m_pNodePlay:getChildByName("node-finish-tips")
    self.m_pNodeCall        = self.m_pNodePlay:getChildByName("node-call")
    self.m_pNodeBet         = self.m_pNodePlay:getChildByName("node-bet")
    self.m_pNodeMatch       = self.m_pNodePlay:getChildByName("node-match")
    self.m_pNodeClock       = self.m_pNodePlay:getChildByName("node-clock")

    for i = 0, 4 do
        self.m_pNodeCallTip[i] = self.m_pNodeCallTips:getChildByName("Text_call" .. i)
        self.m_tipPos[i] = cc.p(self.m_pNodeCallTip[i]:getPosition())
        self.m_pLbBetTip[i]    = self.m_pNodeBetTips:getChildByName("Text_bet" .. i)
        self.m_pLbBetTip[i]:setPosition(self.m_tipPos[i])

        self.m_pSpFinish[i]    = self.m_pNodeFinishTips:getChildByName("Text_finish" .. i)
    end

    for i = 0, 3 do
        self.m_pBtnCall[i] = self.m_pNodeCall:getChildByName("btn_call" .. i)
        self.m_pBtnBet[i] = self.m_pNodeBet:getChildByName("btn_bet" .. i)
    end

    self.m_pStartTips = self.m_pNodeStartTips:getChildByName("Sprite_starttip") -- 开始动画

    --self.m_pStatusTips = tolua.cast(self["m_pStatusTips"], "cc.Sprite")

    --node type
    for i = 0, 4 do
        -- 牌型结果 node-cardtype0
        self.m_pSpResult[i] = self.m_pNodeType:getChildByName("node-cardtype" .. i)
    end
    --node player
    for i = 0, 4 do
        self.m_pNodeUsers[i]     = self.m_pNodeUser:getChildByName("user" .. i)
        self.m_pSpUserHead[i]    = self.m_pNodeUsers[i]:getChildByName("Image_head")
        self.m_pSpHeadVip[i]     = self.m_pNodeUsers[i]:getChildByName("Image_vip")
        self.m_pLbUserNick[i]    = self.m_pNodeUsers[i]:getChildByName("Text_nick")
        self.m_pLbUserGold[i]    = self.m_pNodeUsers[i]:getChildByName("Text_score")
        self.m_pNodeUserBtn[i]   = self.m_pNodeUsers[i]:getChildByName("btn_userinfo")
        self.m_pNodeUserBank[i]  = self.m_pNodeUsers[i]:getChildByName("node-bankericon")
        self.m_pNodeUserScore[i] = self.m_pNodeUsers[i]:getChildByName("Text_resultscore")
        --self.m_pNodeUserInfo[i] = tolua.cast(self["m_pNodeUserInfo"..i], "cc.Node")
        self.m_pSpBlink[i] = self.m_pNodeUsers[i]:getChildByName("Image_blink")
        self.m_pSpHeadFrame[i] = self.m_pNodeUsers[i]:getChildByName("Image_frame")
    end
    self.m_pNodeUserBankerNode = self.m_pNodeUser:getChildByName("node-banker")
    self.m_pNodeUserScoreNode  = self.m_pNodeUser:getChildByName("node-score")
    --node ani
    self.m_pNodeFree = self.m_pNodeAni:getChildByName("node-free")
    self.m_pNodeEnd  = self.m_pNodeAni:getChildByName("node-end")
    self.m_pNodeTop  = self.m_pNodeAni:getChildByName("node-top")
    --node entrust
    self.m_pNodeEntrust     = self.m_rootWidget:getChildByName("node-entrust")
    self.m_pBtnAutoExit     = self.m_pNodeEntrust:getChildByName("btn_exit")
    self.m_pBtnAutoContinue = self.m_pNodeEntrust:getChildByName("btn_continue")
    self.m_pLbClock         = self.m_pNodeEntrust:getChildByName("Text_clock")

    for i = 0, GAME_PLAYER_QZNN-1 do
        local pos_banker = cc.p(self.m_pNodeUserBank[i]:getPosition())
        local pos_offset = cc.p(self.m_pNodeUserBank[i]:getParent():getPosition())
        self.m_pNodeUserBankPos[i] = cc.pAdd(pos_banker, pos_offset)
    end

    self.m_pLbStartCountDown = self.m_pNodeClock:getChildByName("Image_countbg"):getChildByName("Text_counttip")

    
    --房间号
    local strName = ""
    local nBaseScore = PlayerInfo.getInstance():getBaseScore()
    local GameConfig = GameListConfig[PlayerInfo.getInstance():getKindID()]
    if GameConfig[nBaseScore] and GameConfig[nBaseScore].RoomName then
        strName = GameConfig[nBaseScore].RoomName
    end
    local strRoomNo = string.format(LuaUtils.getLocalString("STRING_187"),PlayerInfo.getInstance():getCurrentRoomNo())
    self.m_pLbRoomNo = cc.Label:createWithBMFont("public/font/11.fnt", strName.."  "..strRoomNo)
	self.m_pLbRoomNo:setAnchorPoint(cc.p(0.5, 0.5))
	self.m_pLbRoomNo:setPosition(cc.p(160, 720))
	self.m_pNodeBg:addChild(self.m_pLbRoomNo)

end
function QznnMainLayer:initNode()

    --结算分数
    for i = 0, 4 do
        self.m_pLbResultScore[i] = cc.Label:createWithBMFont(Qznn_Res.FONT_OF_WIN, 0)
        self.m_pLbResultScore[i]:setAnchorPoint(cc.p(0.5, 0.5))
                                :setScale(0.6)
        local pos_user = cc.p(self.m_pNodeUsers[i]:getPosition())
        local pos_offset = cc.p(self.m_pNodeUserScore[i]:getPosition())
        local pos_score = cc.pAdd(pos_user, pos_offset)
        self.m_pLbResultScore[i]:setPosition(pos_score)
        self.m_pLbResultScore[i]:addTo(self.m_pNodeUserScoreNode)
        self.m_flyGoldPos[i] = cc.pAdd(pos_user, cc.p(0, 32))
    end
    --配牌点数
    local _matchBG = self.m_pNodeMatch:getChildByName("Image_matchbg")
    for i = 0, 3 do
        self.m_pLbPoint[i] = _matchBG:getChildByName("Text_match_num" .. i)
    end
    --系统按钮
    self.m_pClippingMenu = self:createClipMenu(self.m_pNodeMenu)
    self.m_pClippingMenu:addTo(self.m_pNodeVoice)
    self.m_pNodeMenu:removeFromParent()
    self.m_pNodeMenu:addTo(self.m_pClippingMenu)

    --事件绑定
    self.m_pBtnReturn = self.m_pNodeTopMenu:getChildByName("btn_return")
    self.m_pBtnReturn:addClickEventListener(handler(self,self.onReturnClicked))

    self.m_pBtnMenu = self.m_pNodeTopMenu:getChildByName("btn_pop")
    self.m_pBtnMenu:addClickEventListener(handler(self,self.onMenuClicked))

    self.m_pBtnCloseMenu = self.m_pNodeMenu:getChildByName("btn_menuclose")
    self.m_pBtnCloseMenu:addClickEventListener(handler(self,self.onCloseMenuClicked))
    
    self.m_pBtnRule:addClickEventListener(handler(self,self.onRuleClicked))
    self.m_pBtnSound:addClickEventListener(handler(self,self.onSoundClicked))
    self.m_pBtnMusic:addClickEventListener(handler(self,self.onMusicClicked))

    self.m_pBtnSubmieNiu:addClickEventListener(handler(self,self.onSubmitMatchClicked))

    self.m_pBtnAutoExit:addClickEventListener(handler(self,self.onExitClicked))
    self.m_pBtnAutoContinue:addClickEventListener(handler(self,self.onGoonClicked))

    for i = 0, 4 do
        self.m_pNodeUserBtn[i]:setTag(i)
        self.m_pNodeUserBtn[i]:addClickEventListener(handler(self,self.onUserClicked))
    end

    for i = 0, 3 do
        self.m_pBtnCall[i]:addClickEventListener(handler(self,self.onCallClicked))
        self.m_pBtnBet[i]:addClickEventListener(handler(self,self.onBetClicked))
    end

    --发牌动画起点
    self.m_pAniStartPos = cc.p(self.m_pNodeCard:getChildByName("card_start_pos"):getPosition())

    for i = 0, 4 do
        self.m_pokerPos[i] = cc.p(self.m_pNodeCard:getChildByName(string.format("card_pos_%d",i)):getPosition())
        self.m_pathPos[i] = cc.p(self.m_pNodeCard:getChildByName(string.format("card_path_node_%d",i)):getPosition())
        self.m_pSpResult[i]:setPosition(cc.pAdd(self.m_pokerPos[i], CardTypeOffset[i])) -- 移动牌型的位置
    end

    --绑定组合牌型按钮和重置坐标
    local respos = self.m_pokerPos[SELF_VIEWCHAIR]
    for i = 1, 5 do
        self.m_cardBtns[i]:addClickEventListener(handler(self,self.onCardClicked))
        self.m_cardBtns[i]:setPosition(cc.p(respos.x + (i - 1) * 130, respos.y))
    end

    self:setNodeFixPostion()

    self.m_posOfMenuX = self.m_pNodeMenu:getPositionX()
    self.m_posOfMenuY = self.m_pNodeMenu:getPositionY()
    self.m_posOfMenuU = self.m_posOfMenuY + self.m_pNodeMenu:getContentSize().height + 10

    --聊天界面
    self.m_pChatLayer = ChatLayer:create()
    self.m_pChatLayer:initData({1,2})
    self.m_pChatLayer:setPlayerUpdateWay(true)
    self.m_pChatLayer:setBtnVisible(true,true,false,false)
    local move_iphonex_x = LuaUtils.isIphoneXDesignResolution() and -IPHONEX_OFFSETX or 0
    self.m_pChatLayer:setNodeAllOffset(self.m_pBtnReturn:getPositionX()-self.m_pChatLayer:getLeftBtnPosX(),0)
    self.m_pChatLayer:setLeftBtnOffsetX(self.m_pBtnReturn:getPositionX()-self.m_pChatLayer:getLeftBtnPosX())

    if LuaUtils.isIphoneXDesignResolution() then
        self.m_pChatLayer:setLeftBtnOffsetY(190)
    else
        self.m_pChatLayer:setLeftBtnMsgOffsetY(-190)
        self.m_pChatLayer:setLeftBtnPlayerOffsetY(190)
    end
    self.m_pNodeTopMenu:addChild(self.m_pChatLayer,101)

        --银商喇叭
    self.m_rollMsgObj = RollMsg.create()
    self.m_rollMsgObj:addTo(self.m_rootWidget)
    self.m_rollMsgObj:startRoll()
end

function QznnMainLayer:setNodeFixPostion()
    local diffX = 145-(1624-display.size.width)/2

    -- 根容器节点
    self.m_rootWidget:setPositionX(diffX)

    if LuaUtils.isIphoneXDesignResolution() then
        self.m_pBtnMenu:setPositionX(self.m_pBtnMenu:getPositionX() + IPHONEX_OFFSETX)
        self.m_pBtnReturn:setPositionX(self.m_pBtnReturn:getPositionX() - IPHONEX_OFFSETX)
        self.m_pNodeMenu:setPositionX(self.m_pNodeMenu:getPositionX() + IPHONEX_OFFSETX)
    end
end

function QznnMainLayer:flyIn()
    local deskAniTime  = 0.3
    local otherAniTime = 0.4
    local offset = 15

    --桌子下方进入
    local _desk = self.m_pNodeBg:getChildByName("Image_desk")
    local posx, posy = _desk:getPosition()
    _desk:setPositionY(posy - 400)
    local tableMove = cc.MoveTo:create(deskAniTime, cc.p(posx,posy))
    local tableScale = cc.ScaleTo:create(deskAniTime, 1)
    local tableSpawn = cc.Spawn:create(tableScale, tableMove)
    _desk:runAction(cc.Sequence:create(tableSpawn))

    --退出和其他玩家按钮
    posx, posy = self.m_pBtnMenu:getPosition()
    self.m_pBtnMenu:setPositionY(posy + 400)
    self.m_pBtnMenu:setOpacity(0)
    local topMove = cc.MoveTo:create(otherAniTime, cc.p(posx,posy - offset))
    local topMove2 = cc.MoveTo:create(0.2, cc.p(posx,posy))
    local spawn = cc.Spawn:create(cc.FadeIn:create(0.2), topMove)
    local seq = cc.Sequence:create(spawn, topMove2)
    self.m_pBtnMenu:runAction(seq)

    posx, posy = self.m_pBtnReturn:getPosition()
    self.m_pBtnReturn:setPositionY(posy + 400)
    self.m_pBtnReturn:setOpacity(0)
    topMove = cc.MoveTo:create(otherAniTime, cc.p(posx,posy - offset))
    topMove2 = cc.MoveTo:create(0.2, cc.p(posx,posy))
    spawn = cc.Spawn:create(cc.FadeIn:create(0.2), topMove)
    seq = cc.Sequence:create(spawn, topMove2)
    self.m_pBtnReturn:runAction(seq)

end

function QznnMainLayer:initEvent()  --初始化事件

    self.event_ = --自定义事件：1.自定义；2.放到游戏内部；3.使用前缀；4.接收CMsgXX数据
    {
        [Public_Events.MSG_USER_RELOGIN]         = { func = self.event_ReLogin,          log = "重新坐下",   debug = true, },
        [Public_Events.MSG_UPDATE_USER_SCORE]    = { func = self.event_UpdateUserScore,  log = "玩家分数",   debug = true, },

        [Public_Events.MSG_USER_FREE]            = { func = self.event_free,             log = "游戏空闲",   debug = true, },
        [Public_Events.MSG_USER_LEAVE]           = { func = self.event_free,             log = "游戏离开",   debug = true, },
        [Public_Events.MSG_USER_SIT]             = { func = self.event_sit,              log = "游戏坐下",   debug = true, },
        [Public_Events.MSG_SIT_SUCCESS]          = { func = self.event_sit,              log = "坐下成功",   debug = true, },
        [Public_Events.MSG_USER_READY]           = { func = self.event_ready,            log = "坐下成功",   debug = true, },
        [Public_Events.MSG_USER_COMEBACK]        = { func = self.event_comeback,         log = "游戏回来",   debug = true, },

        [Qznn_Events.MSG_QZNN_GAME_INIT]         = { func = self.event_GameInit,         log = "游戏清空",   debug = true, },
        [Qznn_Events.MSG_QZNN_GAME_START]        = { func = self.event_GameStart,        log = "游戏开始",   debug = true, },
        [Qznn_Events.MSG_QZNN_GAME_CALL_BANKER]  = { func = self.event_CallBanker,       log = "玩家叫庄",   debug = true, },
        [Qznn_Events.MSG_QZNN_GAME_DOWN_START]   = { func = self.event_DownStart,        log = "开始下注",   debug = true, },
        [Qznn_Events.MSG_QZNN_GAME_DOWN_JETTON]  = { func = self.event_DownJetton,       log = "玩家下注",   debug = true, },
        [Qznn_Events.MSG_QZNN_GAME_SEND_CARD]    = { func = self.event_SendCard,         log = "扑克数据",   debug = true, },
        [Qznn_Events.MSG_QZNN_GAME_USER_FINISH]  = { func = self.event_UserFinish,       log = "用户完成",   debug = true, },
        [Qznn_Events.MSG_QZNN_GAME_END]          = { func = self.event_GameEnd,          log = "游戏结束",   debug = true, },
    }

    local function onMsgUpdate_(event)  --接收自定义事件
        local name = event:getEventName()
        local msg = unpack(event._userdata)
        local processer = self.event_[name]
        MSG_PRINT("-- %s process: [%s] --", self.__cname, processer.log)
        processer.func(self, msg)
    end

    for key in pairs(self.event_) do   --监听事件
         SLFacade:addCustomEventListener(key, onMsgUpdate_, self.__cname)
    end
end
function QznnMainLayer:cleanEvent() --清理事件监听
    for key in pairs(self.event_) do
        SLFacade:removeCustomEventListener(key, self.__cname)
    end
    self.event_ = {}
end

function QznnMainLayer:initGameEvent() --断网/后台/前台/重登录成功
    self.event_game_ = {
        [Public_Events.MSG_GAME_NETWORK_FAILURE]  =  { func = self.onMsgEnterNetWorkFail, },
        [Public_Events.MSG_GAME_ENTER_BACKGROUND] =  { func = self.onMsgEnterBackGround,  },
        [Public_Events.MSG_GAME_ENTER_FOREGROUND] =  { func = self.onMsgEnterForeGround,  },
        [Public_Events.MSG_GAME_RELOGIN_SUCCESS]  =  { func = self.onMsgReloginSuccess,   },
        [Public_Events.MSG_GAME_EXIT]             =  { func = self.event_ExitGame,        },
        [Public_Events.MSG_NETWORK_FAILURE]       =  { func = self.event_NetFailure,      },
    }
    for key, event in pairs(self.event_game_) do   --监听事件
         SLFacade:addCustomEventListener(key, handler(self, event.func), self.__cname)
    end
end

function QznnMainLayer:stopGameEvent()
    for key in pairs(self.event_game_) do   --监听事件
         SLFacade:removeCustomEventListener(key, self.__cname)
    end
    self.event_game_ = {}
end

function QznnMainLayer:onMsgEnterNetWorkFail()
    --相当于进入后台
    self:onMsgEnterBackGround()
end

function QznnMainLayer:onMsgEnterBackGround()
    --self:stopUpdate()
    --self:cleanEvent()

    --FloatMessage.getInstance():pushMessage("进入后台，准备发送准备")
    print("进入后台", QznnDataMgr.getInstance():getGameStatus())
    self.m_bHaveReady = false
end

function QznnMainLayer:onMsgEnterForeGround()
    --第三步:返回时,不删资源
end

function QznnMainLayer:onMsgReloginSuccess()
    --第三步:返回时,登录成功,重置游戏
end

function QznnMainLayer:initUpdate() --开始循环
    
    self.handle_ = 
    {
        { [1] = handler(self, self.updateCountDown), [2] = 1.0, [3] = 0, },
    }
    for k, v in pairs(self.handle_) do
        self.handle_[k][3] = scheduler.scheduleGlobal(v[1], v[2])
    end
end
function QznnMainLayer:stopUpdate() --停止循环
    for k, v in pairs(self.handle_) do
        scheduler.unscheduleGlobal(self.handle_[k][3])
    end
    self.handle_ = {}
end
function QznnMainLayer:initGame() --开始游戏最后一步

    --初始化扑克牌管理类，传入贝塞尔发牌参数
    QznnPokerMgr.getInstance():setAnimData(self.m_pNodeCard, self.m_pAniStartPos, self.m_pokerPos, self.m_pathPos)

    --其他
    self.m_pLbBaseScore:setString("")
    self.m_pNodeMenu:setVisible(false)
    self.m_pNodeFree:setVisible(false)
    self.m_pNodePlay:setVisible(false)
    self.m_pNodeEnd:setVisible(false)

    table.walk(self.m_pNodeUsers, function(v, k)
        v:setVisible(false)
    end)
    table.walk(self.m_pSpBlink, function(v, k)
        v:setVisible(false)
    end)

    --清理
    self:cleanTable()

    --菜单按钮
    self:updateButtonOfMusic()
    self:updateButtonOfSound()

    --播放声音
    if AudioManager:getInstance():getStrMusicPath() ~= Qznn_Res.MUSIC_OF_BGM then
        AudioManager:getInstance():stopMusic()
        AudioManager:getInstance():playMusic(Qznn_Res.MUSIC_OF_BGM)
    end

    --if QznnDataMgr:getInstance():getGameStatusFree() 
    if  Player:getInstance():getTableID() == INVALID_TABLE
    and Player:getInstance():getChairID() == INVALID_CHAIR
    then
        self:doSendSitDown(INVALID_TABLE, INVALID_CHAIR) --发送坐下
    else
        QznnDataMgr:getInstance():setMeChairID(Player:getInstance():getChairID())
        self:doSendGameOption()
    end

    self:flyIn()
end
function QznnMainLayer:cleanGame() --清理游戏最后一步
    --停止动作
    self.m_rootUI:stopAllActions()

    ParticalPool.getInstance():clearPool()
    ParticalPool.getInstance():releaseInstance()
    GoldPool.getInstance():clearPool()
    GoldPool.getInstance():releaseInstance()

    --清理牌管理
    QznnPokerMgr.releaseInstance()

    --删除
    self:removeAllChildren()

    --处理声音
    AudioManager:getInstance():stopAllSounds()
    --删除单例
    QznnMainLayer.instance_ = nil

    --按home键进入后台时未起立
    if PlayerInfo.getInstance():getSitSuc() then
        self:doSendTableStandUp()
    end

    --清理数据
    PlayerInfo:getInstance():setSitSuc(false)

end
function QznnMainLayer:onMoveExitView() --点击返回/被踢出->结束
    local m_bIsKill = self.m_bIsKill
    --发送起立
    if not self:isMeJoin()
    or (QznnDataMgr:getInstance():getGameStatusFree() and not m_bIsKill)
    then
        self:doSendTableStandUp()
    end
    
    --离开
    cc.exports.PlayerInfo:getInstance():setSitSuc(false)
    cc.exports.PlayerInfo:getInstance():setIsQuickStart(false)
	cc.exports.PlayerInfo:getInstance():setIsReLoginInGame(false)
    cc.exports.PlayerInfo:getInstance():setIsGameBackToHall(true)
    cc.exports.PlayerInfo:getInstance():setIsGameBackToHallSuc(false)

    QznnDataMgr:getInstance():Clean()
    CUserManager:getInstance():clearUserInfo()

    --释放动画
    for i, name in pairs(Qznn_Res.vecReleaseAnim) do
        ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo(name)
    end
    --释放整图
    for i, name in pairs(Qznn_Res.vecReleasePlist) do
        display.removeSpriteFrames(name[1], name[2])
    end
    -- 释放背景图
    for i, name in pairs(Qznn_Res.vecReleaseImage) do
        display.removeImage(strFileName)
    end
    --释放音频
    for i, name in pairs(Qznn_Res.vecReleaseSound) do
        AudioManager.getInstance():unloadEffect(name)
    end

    --返回大厅
    SLFacade:dispatchCustomEvent(Public_Events.Load_Entry)

    if m_bIsKill then
        SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_MESSAGEBOX, "go-recharge")
    end

end

function QznnMainLayer:initGameInfo() --游戏循环开始初始化
    local myChairID = Player:getInstance():getChairID()
    if QznnDataMgr:getInstance():getGameStatusFree() then --空闲状态
        self:updateFreeLayer(true)
        self:updatePlayLayer(false)
        self:updateEndLayer(false)
        local tableID = Player:getInstance():getTableID()
        local otherUsers = CUserManager:getInstance():getUserInfoInTable(tableID)
        if table.nums(otherUsers) == 0 then --只有自己
            self:showWaitTip(WAIT_PLAYER)
        else
            self:showWaitTip(WAIT_START)
            self:startCountDown()
        end
        self:doSendReady() --发送准备
    elseif QznnDataMgr:getInstance():getGameStatusCall() then--抢庄状态
        self.m_bGameInitStart = true
        self:updateAllUserInfo()
        self:event_GameStart()
        self.m_bGameInitStart = false
        for i = 0, 4 do
            local times = QznnDataMgr:getInstance():getBankerTimes(i)
            if times >= 0 then
                self:showTip(TIP_CALL, i)
                if myChairID == i then
                    self:updateCallLayer(false)
                end
            end
        end
    elseif QznnDataMgr:getInstance():getGameStatusDown() then--下注状态
        self.m_bGameInitStart = true--这里中断返回需要设置庄家和庄家倍数
        self:updateAllUserInfo()
        self:event_GameStart()
        self:event_DownStart()
        self.m_bGameInitStart = false
        for i = 0, 4 do
            local times = QznnDataMgr:getInstance():getDownTimes(i)
            local banker = QznnDataMgr:getInstance():getBanker()
            if times >= 0 and banker ~= i then
                self:showTip(TIP_BET, i)
                if i == myChairID then
                    self:updateBetLayer(false)
                end
            end
        end
    elseif QznnDataMgr:getInstance():getGameStatusPlay() then--摊牌状态
        local userID = Player:getInstance():getUserID()
        local userInfo = CUserManager:getInstance():getUserInfoByUserID(userID)
        if userInfo.cbUserStatus == G_CONSTANTS.US_PLAYING then
            QznnDataMgr:getInstance():setCardType(myChairID)
        end
        self.m_bGameInitStart = true
        self:updateAllUserInfo()
        self:event_GameStart()
        self:event_DownStart()
        self:startCountDown()
        self:doSendCard()
        self:updateTypeLayer(true)
        local banker = QznnDataMgr:getInstance():getBanker()
        for i = 0, 4 do--需要显示抢庄倍数，和下注倍数
            local times = QznnDataMgr:getInstance():getDownTimes(i)
            if times >= 0 and banker ~= i then
                self:showStaticTip(TIP_BET, i)
                if i == myChairID then
                    self:updateBetLayer(false)
                end
            end
            if banker == i then--庄家显示抢庄倍数
                self:showStaticTip(TIP_CALL, i)
            end
        end

        for i = 0, CARD_COUNT_QZNN - 1 do
            local bFinishCard = QznnDataMgr:getInstance():getFinishCard(i)
            if myChairID == i then
                if bFinishCard then
                    self:doSomethingLater(function()
                        self:updateCardAndType(i)
                        self:updateMatchLayer(false)
                    end, 1)
                end
            else
                if bFinishCard then
                    self:doSomethingLater(function()
                        self:updateCardAndType(i)
                    end, 1)
                end
            end
        end
        self.m_bHaveOperate = true
    end

    if QznnDataMgr:getInstance():getGameStatusPlaying() then
        local userID = Player:getInstance():getUserID()
        local userInfo = CUserManager:getInstance():getUserInfoByUserID(userID)
        if userInfo.cbUserStatus ~= G_CONSTANTS.US_PLAYING then
            self:updateFreeLayer(true)
            self:showWaitTip(WAIT_NEXT)
        end
    end
    self:updateBaseScore() --设置底分
end
---------------------------- init ---------------------------------------------

--------------------------- on clicked event ----------------------------------
function QznnMainLayer:onMenuClicked()   --点击声音
    -- 动画过程中不响应
    if self.m_bIsMenuMove then 
        return
    end
    self.m_bIsMenuMove = true

    if  self.m_pNodeMenu:isVisible() then
        --菜单栏收起
        self.m_pBtnCloseMenu:setVisible(false)
        local callBack = cc.CallFunc:create(function()
            self.m_pBtnMenu:loadTextureNormal(Qznn_Res.PNG_OF_POP, ccui.TextureResType.plistType)
            self.m_pBtnMenu:loadTexturePressed(Qznn_Res.PNG_OF_POP, ccui.TextureResType.plistType)
        end)
        local callBack2 = cc.CallFunc:create(function()
            self.m_bIsMenuMove = false
        end)
        showMenuPush(self.m_pNodeMenu, callBack, callBack2, self.m_posOfMenuX, self.m_posOfMenuY + 400)
    else
        --菜单栏放下
        self.m_pBtnCloseMenu:setVisible(true)
        self.m_pNodeMenu:setPosition(cc.p(self.m_posOfMenuX, self.m_posOfMenuY + 400))
        local callBack = cc.CallFunc:create(function()
            self.m_pBtnMenu:loadTextureNormal(Qznn_Res.PNG_OF_PUSH, ccui.TextureResType.plistType)
            self.m_pBtnMenu:loadTexturePressed(Qznn_Res.PNG_OF_PUSH, ccui.TextureResType.plistType)
        end)
        local callBack2 = cc.CallFunc:create(function()
            self.m_bIsMenuMove = false
        end)
        showMenuPop(self.m_pNodeMenu, callBack, callBack2, self.m_posOfMenuX, self.m_posOfMenuY)
    end
end

function QznnMainLayer:onCloseMenuClicked()
    self:onMenuClicked()
end

function QznnMainLayer:onSoundClicked()   --点击音效
    if self.m_bIsMenuMove then
        return
    end
    --点击声音
    AudioManager.getInstance():playSound(Qznn_Res.SOUND_OF_BUTTON)
    
    --重设
    local bSound = not AudioManager:getInstance():getSoundOn()
    AudioManager:getInstance():setSoundOn(bSound)
    --设置音效
    if bSound then
        AudioManager.getInstance():playSound(Qznn_Res.SOUND_OF_BUTTON)
    else
        AudioManager.getInstance():stopAllSounds()
    end
    --设置按钮
    self:updateButtonOfSound(bSound)
end

function QznnMainLayer:onMusicClicked()   --点击音乐
    if self.m_bIsMenuMove then
        return
    end
    --点击声音
    AudioManager.getInstance():playSound(Qznn_Res.SOUND_OF_BUTTON)

    --重设
    local bMusic = not AudioManager:getInstance():getMusicOn()
    AudioManager:getInstance():setMusicOn(bMusic)
    --设置音乐
    if bMusic then
        local strPath = AudioManager:getInstance():getStrMusicPath()
        AudioManager:getInstance():playMusic(strPath)
    else
        AudioManager:getInstance():stopMusic()
    end
    --设置按钮
    self:updateButtonOfMusic(bMusic)
end

function QznnMainLayer:onReturnClicked()  --点击返回
    --点击声音
    AudioManager.getInstance():playSound(Qznn_Res.SOUND_OF_BUTTON)
    --被踢下
    if self.m_bIsKill then
        return
    elseif PlayerInfo.getInstance():IsInExperienceRoom() then --体验房退出提示
        self:showMessageBox("experience-room-exit")
    --游戏中, 提示
    elseif self:isMeJoin() and QznnDataMgr:getInstance():getGameStatusPlaying() then
        self:showMessageBox("game-exit-2")
    else --退出
        self:onMoveExitView() 
    end
end

function QznnMainLayer:onExitClicked() --点击继续
    --点击声音
    AudioManager.getInstance():playSound(Qznn_Res.SOUND_OF_BUTTON)
    self:stopCountDown()
    self:updateEntrustLayer(false)
    self:onMoveExitView() 
end

function QznnMainLayer:onGoonClicked() --点击继续
    --点击声音
    AudioManager.getInstance():playSound(Qznn_Res.SOUND_OF_BUTTON)
    self.m_bIsEntrust = false
    self:updateEntrustLayer(false)
end

function QznnMainLayer:onRuleClicked()--点击规则
    if self.m_bIsMenuMove then
        return
    end
    --点击声音
    AudioManager.getInstance():playSound(Qznn_Res.SOUND_OF_BUTTON)

    self:showRuleLayer()
end

--玩家信息展示
function QznnMainLayer:onUserClicked(pSender)
    --点击声音
    AudioManager.getInstance():playSound(Qznn_Res.SOUND_OF_BUTTON)

    --未消失
    local tag = pSender:getTag()
    if self.m_pNodeTop:getChildByTag(100+tag) then
        return
    end

    --弹窗
    local userID = self:getUserInSameTable(tag)
    local pos = cc.p(self.m_pNodeUsers[tag]:getPosition())
    pos = cc.pAdd(pos, UserInfoOffset[tag])
    self:showUserInfo(userID, pos, 100+tag, self.m_pNodeTop)
end

function QznnMainLayer:onCallClicked(pSender) --点击抢庄
    --播放音声
    AudioManager:getInstance():playSound(Qznn_Res.SOUND_OF_BUTTON)

    if not QznnDataMgr:getInstance():getGameStatusCall() then
        return
    end

    local index = pSender:getTag()
    --发送抢庄
    self:doSendCallBanker(index)

    --fix 抢庄增加动画先行
    local myChairID = Player:getInstance():getChairID()
    QznnDataMgr:getInstance():setBankerTimes(myChairID, index)
    QznnDataMgr:getInstance():setLocaBankerTimes(index)
    self:showTip(TIP_CALL, myChairID)
    self.m_bAuto = false
    if index > 0 then
        local sound = Player:getInstance():getGender() == 1 and Qznn_Res.SOUND_OF_CALL_WOMAN or Qznn_Res.SOUND_OF_CALL_MAN
        self:doSoundPlayLater(sound, 0.2)
    end

    --隐藏抢庄
    self:updateCallLayer(false)
    --设置抢完
    self.m_bHaveOperate = true
    --隐藏倒计时
    self.m_pNodeClock:setVisible(false)
end

function QznnMainLayer:onExitClicked() --点击继续
    --点击声音
    AudioManager.getInstance():playSound(Qznn_Res.SOUND_OF_BUTTON)
    self:stopCountDown()
    self:updateEntrustLayer(false)
    self:onMoveExitView() 
end

function QznnMainLayer:onGoonClicked() --点击继续
    --点击声音
    AudioManager.getInstance():playSound(Qznn_Res.SOUND_OF_BUTTON)
    self.m_bIsEntrust = false
    self:updateEntrustLayer(false)
end

function QznnMainLayer:onBetClicked(pSender) --点击下注
    --播放音声
    AudioManager:getInstance():playSound(Qznn_Res.SOUND_OF_BUTTON)

    if not QznnDataMgr:getInstance():getGameStatusDown() then
        return
    end

    --处理
    local index = pSender:getTag()
    --发送下注
    self:doSendDownJetton(index)

    --fix 下注动画先行
    local myChairID = Player:getInstance():getChairID()
    QznnDataMgr:getInstance():setDownTimes(myChairID, index)
    QznnDataMgr:getInstance():setLocalDownTimes(index)
    self:showTip(TIP_BET, myChairID)
    self.m_bAuto = false

    --隐藏下注
    self:updateBetLayer(false)
    --下注完成
    self.m_bHaveOperate = true
    --隐藏倒计时
    self.m_pNodeClock:setVisible(false)
end

function QznnMainLayer:onCardClicked(pSender) --点击牌
    --播放音声
    AudioManager:getInstance():playSound(Qznn_Res.SOUND_OF_CARD)
    if not self.m_pNodeMatch:isVisible() then
        return
    end
    --处理
    local index = pSender:getTag()
    local pointIndex = self.m_nCardChooseIndex[index]
    if pointIndex == -1 then
        for i = 0, 2 do
            if self.m_nCardPoint[i] == 0 then
                self.m_nCardChooseIndex[index] = i
                local chairID = Player:getInstance():getChairID()
                local cardData = QznnDataMgr:getInstance():getCardData(chairID, index)
                self.m_nCardPoint[i] = QznnDataMgr:GetCardValue_(cardData)
                break
            end
        end
    else
        self.m_nCardPoint[pointIndex] = 0
        self.m_nCardChooseIndex[index] = -1
    end
    self:updateMatch()
    self.m_bHaveOperate = true
end

function QznnMainLayer:onSubmitMatchClicked() --点击摊牌
    --播放音声
    AudioManager:getInstance():playSound(Qznn_Res.SOUND_OF_BUTTON)
    if not self.m_pNodeMatch:isVisible() then
        return
    end

   self:updateMatchLayer(false)
   self:onSubmitCard()
   self.m_bHaveOperate = true
end

----------------------- on clicked event --------------------------------------

----------------------------------- on msg event ------------------------------
function QznnMainLayer:event_ReLogin(msg) --重新登录 
    self:doSendReConnect()
end

function QznnMainLayer:event_ExitGame(_event)--退出游戏

    if _event and _event._userdata then
        local _userdata = unpack(_event._userdata)
        if _userdata and tonumber(_userdata) == 1 then
            -- 自己被踢
            self.m_bIsKill = true
            return
        end
    end

    if QznnDataMgr:getInstance():getGameStatusPlaying() then
        self:doSendTableStandUp()
    end
    
    self:onMoveExitView()
end  

function QznnMainLayer:event_NetFailure(msg)--游戏掉线
    
    if msg == "1" then
        FloatMessage.getInstance():pushMessage("STRING_023_1")
    else
        FloatMessage.getInstance():pushMessage("STRING_023")
    end
    self:onMoveExitView()
end

function QznnMainLayer:event_UpdateUserScore(msg)--更新玩家分数
    --nothing
end

function QznnMainLayer:event_free(msg)--空闲
    
    if msg == nil or msg == INVALID_CHAIR or self.m_bIsKill then
        return
    end

    local chairID = msg
    local myChairID = Player:getInstance():getChairID()
    if chairID == myChairID then
        self:onMoveExitView()
        return
    end

    local viewChair = self:switchViewChairID(chairID)
    self.m_pNodeUsers[viewChair]:setVisible(false)

    local tableID = Player:getInstance():getTableID()
    local otherUsers = CUserManager:getInstance():getUserInfoInTable(tableID)
    if table.nums(otherUsers) == 0 then

        if self.m_nWaitStatus == WAIT_START then
            if self.m_bIsEntrust then
                self:stopCountDown()
                self:updateEntrustLayer(false)
                FloatMessage:getInstance():pushMessage("STRING_222")
                self:onMoveExitView() 
                return
            else
                self:doSendReady()
            end
        end
        self:stopCountDown()
        self:showWaitTip(WAIT_PLAYER)
    end
end

function QznnMainLayer:event_sit(msg)--坐下

    if msg == INVALID_CHAIR then
        return
    end
    if msg == Player:getInstance():getChairID() then
        QznnDataMgr:getInstance():setMeChairID(msg)
    end
    self:updateAllUserInfo()
end

function QznnMainLayer:event_ready(msg)--准备
    local chairID = msg
    
    if chairID == INVALID_CHAIR then
        return
    end

    if chairID == Player:getInstance():getChairID() then
        self.m_bHaveReady = true
    end
end

function QznnMainLayer:event_comeback(msg)--返回
    if msg == nil or msg == INVALID_CHAIR then
        return
    end
    self:updateAllUserInfo()
end

--进入游戏场景的初始化
function QznnMainLayer:event_GameInit(msg)
    self:initGameInfo()
end

function QznnMainLayer:event_GameStart(msg)--游戏开始,开始抢庄
    
    self.m_bHaveReady = true
    self.m_bHaveOperate = false
    self.m_bIsEntrust = false
    self:updateFreeLayer(false)
    self:updateEndLayer(false)
    self:updatePlayLayer(true)
    self:cleanTable()

    for i = 0, 4 do --当局玩家
        local tableID = Player:getInstance():getTableID()
        local user = CUserManager:getInstance():getUserInfoByChairID(tableID, i)
        if user and user.dwUserID ~= 0 and (user.cbUserStatus == G_CONSTANTS.US_PLAYING or user.cbUserStatus == G_CONSTANTS.US_OFFLINE)then
            self.m_PlayUsers[table.nums(self.m_PlayUsers)] = table.deepcopy(user)
        end
    end

    local function goGame()
        if self:isMeJoin() then
            local val = QznnDataMgr:getInstance():getBankerTimes(Player:getInstance():getChairID())
            self:updateCallLayer(true)
            self:updateCallBtnStatus()
            --self.m_pNodeCards[self.m_wMeViewChair]:setVisible(true)
        end
        self:startCountDown()
    end

    if not self.m_bGameInitStart then
        self.m_pStartTips:setPosition(cc.p(-285, 400))
        local actionIn = cc.EaseBackOut:create( cc.MoveTo:create(0.5, cc.p(667,400)) )
        local delay = cc.DelayTime:create(0.4)
        local actionOut = cc.EaseBackIn:create( cc.MoveTo:create(0.4, cc.p(1780,400)) )
        local actionSeq = cc.Sequence:create(actionIn, delay, actionOut, cc.CallFunc:create(function()
            goGame()
        end))
        self.m_pStartTips:runAction(actionSeq)
        self:doSoundPlayLater(Qznn_Res.SOUND_OF_START, 0.0)
    else
        goGame()
    end
end

function QznnMainLayer:event_CallBanker(msg)--叫庄
    if msg == nil or msg == INVALID_CHAIR then
        return
    end
    local chairID = msg
    local myChairID = Player:getInstance():getChairID()
    local manager = QznnDataMgr:getInstance()
    --fix 抢庄动画先行,修正(如果自己的下注和服务器发下来不一样,修正)
    if myChairID == chairID then
        if manager:getLocaBankerTimes() ~= manager:getBankerTimes(myChairID) then
            print("====这里需要纠正==========")
            FloatMessage.getInstance():pushMessage("QZNN_2")
            self:showTip(TIP_CALL, chairID, true)
        end
        return
    end
    
    self:showTip(TIP_CALL, chairID)

    if chairID == myChairID then
        self.m_bAuto = false
        self:updateCallLayer(false)
    end
    for i, user in pairs(self.m_PlayUsers) do
        if user.wChairID == chairID and QznnDataMgr:getInstance():getBankerTimes(chairID) > 0 then
            local sound = user.cbGender == 1 and Qznn_Res.SOUND_OF_CALL_WOMAN or Qznn_Res.SOUND_OF_CALL_MAN
            self:doSoundPlayLater(sound, 0.2)
        end
    end
end

function QznnMainLayer:event_DownStart(msg)--开始下注
    self:updateCallLayer(false)
    self:updateBetBtnStatus()
    self:stopCountDown()

    self.m_RandomBankers = {}

    if self.m_bGameInitStart then
        self:onBet(0)
    else
        --是否显示选择庄家效果
        for i = 0, 4 do
            local times = QznnDataMgr:getInstance():getBankerTimes(i)
            local call = QznnDataMgr:getInstance():getFinalCallTimes()
            if self:isUserJoin(i) and times == call then
                self.m_RandomBankers[table.nums(self.m_RandomBankers)] = i
            end
        end
        self:doSomethingLater(handler(self, self.onBet), 0.5)
        self.m_bGameInitStart = false
    end
end

function QznnMainLayer:event_DownJetton(msg)--下注
    if msg == nil or msg == INVALID_CHAIR then 
        return
    end
    --如果在随机动画,就先不显示下注
    if self.m_bWaitRandonBanker then
        print("这里动画没表现完 chairID:"..msg.."  downTime:"..QznnDataMgr:getInstance():getDownTimes(msg))
        return
    end
    local myChairID = Player:getInstance():getChairID()
    local chairID = msg
    --fix 下注动画先行,修正
    if myChairID == chairID then
        --直接显示下注情况
        self:showTip(TIP_BET, chairID, true)
--        local manager =  QznnDataMgr:getInstance()
--        if manager:getDownTimes(myChairID) ~= manager:getLocalDownTimes() then
--            FloatMessage.getInstance():pushMessage("QZNN_1")
--            self:showTip(TIP_BET, chairID, true)
--        end
        return
    end
    self:showTip(TIP_BET, chairID)
    local myChairID = Player:getInstance():getChairID()
    if chairID == myChairID then
        self.m_bAuto = false
        self:updateBetLayer(false)
    end
end

function QznnMainLayer:event_SendCard(msg)--发牌
    self:stopCountDown()
    self:doSendCard()
    self:startCountDown()
    --if self.m_bWaitRandonBanker and QznnDataMgr:getInstance():getBanker() == Player:getInstance():getChairID() then
        self:doSomethingLater(function()
            self:moveTips(true)
        end, 1)
    --else
        --self:moveTips()
    --end
end

function QznnMainLayer:doSendCard()
    local function goSendCard()
        self:updateCallLayer(false)
        self:updateBetLayer(false)
        --self:stopCountDown()
        self:showCard()
    end

    if self.m_bWaitRandonBanker and QznnDataMgr:getInstance():getBanker() == Player:getInstance():getChairID() then
        self:doSomethingLater(function()
            goSendCard()
        end, 1)
    else
        goSendCard()
    end
end

function QznnMainLayer:event_UserFinish(msg)--完成
    self:updateTypeLayer(true)
    if msg == nil or msg == INVALID_CHAIR then
        return
    end

    local chairID = msg
    local myChairID = Player:getInstance():getChairID()
    --fix 摊牌动画先行(如果倍数和服务器发下来不一样修正)
    if myChairID == chairID then
        if QznnDataMgr:getInstance():getLocalCardType() ~= QznnDataMgr:getInstance():getCardType(myChairID) then
            self:updateCardAndType(myChairID)
        end
        return
    end
    --完成就显示牌
    self:updateCardAndType(chairID)
end

function QznnMainLayer:event_GameEnd(msg)--游戏结束
    
    --如果当局我没操作，算我托管
    if not self.m_bHaveOperate and self:isMeJoin() then
        self.m_bIsEntrust = true
    end

    self.m_bHaveReady = false
    --FloatMessage.getInstance():pushMessage("游戏结束，准备发送准备")
    self.m_bGameInitStart = false
    self.m_bShowEnd = true
    self.m_bIsDisPathChEnd = false
    self:updateMatchLayer(false)
    self:stopCountDown()
    self.m_nWaitStatus = -1 
    self:onEnd()
end

function QznnMainLayer:doSendReady()
    
    if self.m_bHaveReady then
        --FloatMessage.getInstance():pushMessage("没有发送准备")
        print("没有发送准备", QznnDataMgr.getInstance():getGameStatus())
        return
    else
        CMsgQznn:getInstance():sendUserReady()
        self.m_bHaveReady = true
    end
end

------------------add
function QznnMainLayer:doSendSitDown(table, chair)
    CMsgQznn:getInstance():sendSitDown(table, chair)
end
function QznnMainLayer:doSendGameOption()
    CMsgQznn:getInstance():sendGameOption()
end
function QznnMainLayer:doSendTableStandUp()
    CMsgQznn:getInstance():sendTableStandUp()
end
function QznnMainLayer:doSendCallBanker(index)
    CMsgQznn:getInstance():sendCallBanker(index)
end
function QznnMainLayer:doSendDownJetton(index)
    CMsgQznn:getInstance():sendDownJetton(index)
end
function QznnMainLayer:doSendTakeCard(cardData)
    CMsgQznn:getInstance():sendTakeCard(cardData)
end
function QznnMainLayer:doSendReConnect()
    CMsgQznn:getInstance():sendReconnect()
end
----------------------------------- on msg event ------------------------------
----------------------------------- update view -------------------------------

function QznnMainLayer:switchViewChairID(chairID)
    
    local viewChair = QznnDataMgr:getInstance():SwitchViewChairID(chairID)
    return viewChair
end

function QznnMainLayer:isMeJoin()
    local userID = Player:getInstance():getUserID()
    for i, user in pairs(self.m_PlayUsers) do
        if user.dwUserID == userID then
            return true
        end
    end
    return false
end

function QznnMainLayer:isUserJoin(chairID)
    for i, user in pairs(self.m_PlayUsers) do
        if user.wChairID == chairID then
            return true
        end
    end
    return false
end

function QznnMainLayer:updateAllUserInfo()
    if self.m_bShowEnd then
        return
    end
    for i = 0, 4 do
        self:updateUserInfo(i)
    end
end

function QznnMainLayer:updateUserInfo(chairID)
    
    if chairID == INVALID_CHAIR then
        return
    end 
    local viewChair = self:switchViewChairID(chairID)
    if viewChair == INVALID_CHAIR then
        return
    end   
    local myTableID = Player:getInstance():getTableID()
    local user = CUserManager:getInstance():getUserInfoByChairID(myTableID, chairID)
    if user == nil or user.wChairID == INVALID_CHAIR then
        self.m_pNodeUsers[viewChair]:setVisible(false)
    else
        self.m_pNodeUsers[viewChair]:setVisible(true)

        local strHeadIcon = string.format(Qznn_Res.PNG_OF_HEAD, user.wFaceID % 10 + 1)
        self.m_pSpUserHead[viewChair]:loadTexture(strHeadIcon,ccui.TextureResType.localType)

        --增加vip等级对应的头像框
        local headFrame = user.nVipLev > 6 and 6 or user.nVipLev
        if viewChair == self.m_wMeViewChair then
            headFrame = Player:getInstance():getFrameID()
        end

        local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", user.wFaceCircleID)
        self.m_pSpHeadFrame[viewChair]:loadTexture(framePath, ccui.TextureResType.plistType)

        --vip等级
        local vipLevel = math.min(user.nVipLev, VIP_LEVEL_MAX)
        local vipPath = string.format("hall/plist/vip/img-vip%d.png", vipLevel)
        self.m_pSpHeadVip[viewChair]:loadTexture(vipPath, ccui.TextureResType.plistType)

        local strNick = LuaUtils.getDisplayNickNameInGame(user.szNickName)
        --if viewChair == self.m_wMeViewChair then 
        --    strNick = LuaUtils.getDisplayNickNameInGame(user.szNickName, 10, 10)
        --else
        --    strNick = LuaUtils.getDisplayNickNameInGame(user.szNickName)
        --end
        self.m_pLbUserNick[viewChair]:setString(strNick)

        local strUserGold = LuaUtils.getFormatGoldAndNumberAndZi(user.lScore)
        self.m_pLbUserGold[viewChair]:setString(strUserGold)
    end
end

function QznnMainLayer:cleanTable()
    
    self:updateCallLayer(false)
    self:updateBetLayer(false)
    self:updateMatchLayer(false)
    self:updateTypeLayer(false)
    self:updateGrayLayer(false)
    self:hideTip(TIP_CALL)
    self:hideTip(TIP_BET)
    self:hideTip(TIP_FINISH)
    self:resetTipsPos()
    self:cleanAllCard()
    self:stopCountDown()
    self.m_PlayUsers = {}

    QznnPokerMgr.getInstance():disCardAnimAll()

    for i = 0, GAME_PLAYER_QZNN-1 do
        self.m_pSpResult[i]:setVisible(false)
        self.m_pSpBlink[i]:setOpacity(255)
        self.m_pSpBlink[i]:setVisible(false)
        self.m_pLbResultScore[i]:setString("")
    end

    for i = 0, CARD_COUNT_QZNN-1 do
        self.m_nCardChooseIndex[i] = -1
    end

    for i = 0, 3 do
        self.m_pLbPoint[i]:setString("")
        self.m_nCardPoint[i] = 0
    end

    if self.m_pArmBanker then
        self.m_pArmBanker:removeFromParent()
        self.m_pArmBanker = nil
    end
end

function QznnMainLayer:cleanAllCard()
    QznnPokerMgr.getInstance():disCardAnimAll()
end

function QznnMainLayer:setMyCardData()
    local myChairID = Player:getInstance():getChairID()
    local viewChair = self:switchViewChairID(myChairID)
    local cards = QznnPokerMgr.getInstance():getPokers(viewChair)
    for i = 0, CARD_COUNT_QZNN - 1 do
       local cardData = QznnDataMgr:getInstance():getCardData(myChairID, i)
       cards[i]:setDataAndShowCard(cardData)
    end
end

function QznnMainLayer:setUserCardData(chairID, haveData)
    local viewChair = self:switchViewChairID(chairID)
    local cards = QznnPokerMgr.getInstance():getPokers(viewChair)

    for i = 0, CARD_COUNT_QZNN-1 do
        cards[i]:setVisible(true)
        if haveData then
            local cardData = QznnDataMgr:getInstance():getCardData(chairID, i)
            cards[i]:setDataAndShowCard(cardData)
        else
            cards[i]:setBack()
        end
    end
end

--发牌
function QznnMainLayer:showCard()
    local myChairID = Player:getInstance():getChairID()
    local showSelfCard = false
    AudioManager.getInstance():playSound(Qznn_Res.SOUND_OF_FAPAI)

    for i, user in pairs(self.m_PlayUsers) do
        if user.wChairID == myChairID then showSelfCard = true end
        local viewChair = QznnDataMgr.getInstance():SwitchViewChairID(user.wChairID)
        if user.wChairID ~= INVALID_CHAIR then
            local cbCardDatas = QznnDataMgr.getInstance():getCardDatas(user.wChairID)
            QznnPokerMgr.getInstance():showFaPaiAni(viewChair,cbCardDatas)        
        end
    end

    if showSelfCard then
        self.m_rootUI:runAction(cc.Sequence:create( 
            cc.DelayTime:create(QznnPokerMgr.getInstance().allDt), 
            cc.CallFunc:create(function ()
                --倍率及抢庄 移动至牌的左下角
                self:updateMatchLayer(true)
            end))
        )
    end
end

--移动抢庄和下注倍率的提示
function QznnMainLayer:moveTips(useAni)
    if useAni then
        local moveFun = function(obj, pos)
            local move = cc.MoveTo:create(0.2, pos)
            local scale = cc.ScaleTo:create(0.2, TipAniScale)
            local spawn = cc.Spawn:create(move, scale)
            obj:runAction(spawn)
        end
        
        for i = 0, 4 do
            if self.m_pNodeCallTip[i]:isVisible() then
                moveFun(self.m_pNodeCallTip[i], TipAniPos[i])
            end
            if self.m_pLbBetTip[i]:isVisible() then
                moveFun(self.m_pLbBetTip[i], TipAniPos[i])
            end
        end
    else
        for i = 0, 4 do
            if self.m_pNodeCallTip[i]:isVisible() then
                self.m_pNodeCallTip[i]:setScale(TipAniScale)
                                      :setPosition(TipAniPos[i])
            end
            if self.m_pLbBetTip[i]:isVisible() then
                self.m_pLbBetTip[i]:setScale(TipAniScale)
                                      :setPosition(TipAniPos[i])
            end
        end
    end

end

--重置抢庄和下注倍率的提示位置
function QznnMainLayer:resetTipsPos()
    for i = 0, 4 do
        self.m_pNodeCallTip[i]:setPosition(self.m_tipPos[i])
        self.m_pNodeCallTip[i]:setScale(1)
        self.m_pLbBetTip[i]:setPosition(self.m_tipPos[i])
        self.m_pLbBetTip[i]:setScale(1)
    end 
end

function QznnMainLayer:onBet()
    
    AudioManager:getInstance():playSound(Qznn_Res.SOUND_OF_BET_START)
    self:hideTip(TIP_CALL)
    if self.m_bGameInitStart then
        self.m_bWaitRandonBanker = false
        self:setBanker()
        self:showNodeBet(0)
    else
        self.m_bWaitRandonBanker = true
        if table.nums(self.m_RandomBankers) >= 2 then
            self:updateGrayLayer(true)
            local num = table.nums(self.m_RandomBankers)
            self.m_nRandomBankerIndex = math.random(1, num) - 1
            self:doSomethingLater(function()
                self:showRandomBanker(0)
            end, 0.1)
        else
            self:showBankerAni()
            self:doSomethingLater(handler(self, self.showNodeBet), 1.5)
        end
        self.m_bGameInitStart = false
    end
end

function QznnMainLayer:showRandomBanker(index)
    
    AudioManager:getInstance():playSound(Qznn_Res.SOUND_OF_CALL_RANDOM)
    for i = 0, GAME_PLAYER_QZNN-1 do
        self.m_pSpBlink[i]:setVisible(false)
    end
    local round = table.nums(self.m_RandomBankers) > 3 and 4 or 5
    if index >= table.nums(self.m_RandomBankers) * round then
        local bankerID = QznnDataMgr:getInstance():getBanker()
        local viewChair = self:switchViewChairID(bankerID)
        self.m_pSpBlink[viewChair]:setVisible(true)
        self.m_pSpBlink[viewChair]:runAction(cc.FadeOut:create(2.0))
        self:showBankerAni()
        self:doSomethingLater(handler(self, self.showNodeBet), 1.5)
    else
        local chairID = self.m_RandomBankers[self.m_nRandomBankerIndex]
        local viewChair = self:switchViewChairID(chairID)
        self.m_pSpBlink[viewChair]:setVisible(true)
        self.m_nRandomBankerIndex = self.m_nRandomBankerIndex + 1
        self.m_nRandomBankerIndex = self.m_nRandomBankerIndex % table.nums(self.m_RandomBankers)

        index = index + 1
        local stime = ((0.3 - index * 0.03) < 0.02) and 0.02 or (0.3 - index * 0.03)
        self:doSomethingLater(function()
            self:showRandomBanker(index)
        end, stime)
    end
end

--设置庄家标识位置
function QznnMainLayer:setBanker()
    local bankerID = QznnDataMgr:getInstance():getBanker()
    local viewChair = self:switchViewChairID(bankerID)

    if self.m_bGameInitStart then --已经开始
        self:showBankerAni(true)
        self:showTip(TIP_CALL, bankerID, true)
        if self.m_pArmBanker then
            self.m_pArmBanker:setPosition(self.m_pNodeUserBankPos[viewChair])
        end
    else --
        local move = cc.MoveTo:create(0.2, self.m_pNodeUserBankPos[viewChair])
        local scale = cc.ScaleTo:create(0.2, 0.8)
        local spawn = cc.Spawn:create(move, scale)
        local call = cc.CallFunc:create(function()
            self:showTip(TIP_CALL, bankerID, true)
        end)
        local action = cc.Sequence:create(spawn, call)
        self.m_pArmBanker:runAction(action)
        self.m_bGameInitStart = false
    end
end

function QznnMainLayer:showNodeBet()
    
    --显示一下已经下注的
    if self.m_bWaitRandonBanker then
        for _, user in pairs(self.m_PlayUsers) do
            if user.dwUserID ~= Player:getInstance():getUserID() then
                if  QznnDataMgr:getInstance():getDownTimes(user.wChairID) >= 0 then
                    print("这里动画表现完显示已经下注  chairID:"..user.wChairID.."  downTime:"..QznnDataMgr:getInstance():getDownTimes(user.wChairID))
                    self:showTip(TIP_BET, user.wChairID)
                end
            end
        end
    end
    self.m_bWaitRandonBanker = false

    self:updateGrayLayer(false)
    local bankerID = QznnDataMgr:getInstance():getBanker()
    local myChairID = Player:getInstance():getChairID()
    local bStatusDown = QznnDataMgr:getInstance():getGameStatusDown()
    if self:isMeJoin() and bankerID ~= myChairID and bStatusDown then
        self:updateBetLayer(true)
    end
    self:startCountDown()
end

function QznnMainLayer:updateMatch()
    local cards = QznnPokerMgr.getInstance():getPokers(SELF_VIEWCHAIR)
    for i = 0, CARD_COUNT_QZNN-1 do
        local offset_y = self.m_nCardChooseIndex[i] ~= -1 and 30 or 0
        cards[i]:setPositionY(self.m_pokerPos[SELF_VIEWCHAIR].y + offset_y)
    end
    self.m_nCardPoint[3] = 0
    local point = {}
    for i = 0, 2 do
        point[i] = self.m_nCardPoint[i] > 10 and 10 or self.m_nCardPoint[i]
        self.m_nCardPoint[3] = self.m_nCardPoint[3] + point[i]
    end
    for i = 0, 3 do
        local strPoint = ""
        if i < 3 and self.m_nCardPoint[i] > 10 then
            strPoint = "10"
        elseif self.m_nCardPoint[i] > 0 then
            strPoint = string.format("%d", self.m_nCardPoint[i])
        end
        self.m_pLbPoint[i]:setString(strPoint)
    end
end

function QznnMainLayer:onSubmitCard(bAuto)
    
    if not self:isMeJoin() then
        return 
    end

    --停止倒计时
    self:stopCountDown()

    local cbCardData = { [0] = 0, 0, 0 ,0, 0, }
    local myChairID = Player:getInstance():getChairID()
    local cardType = QznnDataMgr:getInstance():getCardType(myChairID)
    local off_y = 30
    if cardType == G_CONSTANTS.CardValue_Nothing then --没牛
        local cards = QznnPokerMgr.getInstance():getPokers(SELF_VIEWCHAIR)
        for i = 0, CARD_COUNT_QZNN-1 do
            cards[i]:setPositionY(self.m_pokerPos[SELF_VIEWCHAIR].y)
            cbCardData[i] = QznnDataMgr:getInstance():getCardData(myChairID, i)
        end
    else
        --先判断玩家是否自己有配牌,有牛
        local selectCount = 0
        local selectValue = 0
        local manager = QznnDataMgr:getInstance()
        for i = 0, CARD_COUNT_QZNN-1 do
            if self.m_nCardChooseIndex[i] ~= -1 then
                selectCount = selectCount + 1
                selectValue = selectValue + manager:GetCardValue_(manager:getCardData(myChairID, i))
            end
        end
        if selectCount == 3 and selectValue%10 == 0 then
            local m, n = 0, 3
            for i = 0, CARD_COUNT_QZNN-1 do
                local cardData = QznnDataMgr:getInstance():getCardData(myChairID, i)
                if self.m_nCardChooseIndex[i] ~= -1 then
                    cbCardData[m] = cardData
                    m = m + 1
                else
                    cbCardData[n] = cardData
                    n = n + 1
                end
            end

            for i = 0, CARD_COUNT_QZNN-1 do
                QznnDataMgr:getInstance():setCardData(myChairID, i, cbCardData[i])
            end
        else
            manager:sortMyCardByNiu()
        end
    end

    self:updateMatchLayer(false)
    self:doSendTakeCard(cbCardData)
    self.m_pNodeClock:setVisible(false)

    --fix 摊牌动画先行
    self:updateCardAndType(myChairID)
    QznnDataMgr:getInstance():setLocalCardType(cardType)
end

function QznnMainLayer:onEnd()
    self:updateEndLayer(true)
    self:doSomethingLater(handler(self, self.showEndResult), 0.5)
end

function QznnMainLayer:showEndResult()
    
    local myChairID = - 1
    for _, user in pairs(self.m_PlayUsers) do
        if user.dwUserID == Player:getInstance():getUserID() then
            myChairID = user.wChairID
            break
        end
    end

    --通吃/通赔
    local bTongChi, bTongPei = true, true
    local bankerID = QznnDataMgr:getInstance():getBanker()
    for i, user in pairs(self.m_PlayUsers) do
        if user.wChairID ~= bankerID then
            local result = QznnDataMgr:getInstance():getResult(user.wChairID)
            if result > 0 then
                bTongChi = false
            elseif result < 0  then
                bTongPei = false
            end
        end
    end
    --sound
    if table.nums(self.m_PlayUsers) > 2 then
        if bTongChi then
            self:doSoundPlayLater(Qznn_Res.SOUND_OF_WIN_ALL,  0.0)
        elseif bTongPei then
            self:doSoundPlayLater(Qznn_Res.SOUND_OF_LOST_ALL, 0.0)
        end
    end

    if myChairID >= 0 then
        local score = QznnDataMgr:getInstance():getResult(myChairID)
        if score > 0 then
            self:doSomethingLater(handler(self, self.showWinAni),  0.0)
            self:doSoundPlayLater(Qznn_Res.SOUND_OF_WIN, 0.3)
        else
            self:doSomethingLater(handler(self, self.showLoseAni), 0.0)
            self:doSoundPlayLater(Qznn_Res.SOUND_OF_LOST,  0.0)
        end
    else
        --自己没参与
        self:doSomethingLater(handler(self, self.showResultScore), 1)
    end
end

function QznnMainLayer:showResultScore()

    --local pos = { [0] = {430, 725}, {83, 507}, {270, 167}, {1253, 507}, {963, 725}, }
    for i, user in pairs(self.m_PlayUsers) do
        local score = QznnDataMgr:getInstance():getResult(user.wChairID)
        local strFile = score > 0 and Qznn_Res.FONT_OF_WIN or Qznn_Res.FONT_OF_LOST
        local viewChair = self:switchViewChairID(user.wChairID)
        local strScore = LuaUtils.getFormatGoldAndNumberAndZi(score)
        local strResult = score > 0 and "+"..strScore or strScore
        local pos = cc.p(self.m_pLbResultScore[viewChair]:getPosition())
        local posStart = cc.pSub(pos, cc.p(0, 50))
        local action = cc.EaseElasticOut:create(cc.MoveTo:create(1.0, pos))
        
        self.m_pLbResultScore[viewChair]:setBMFontFilePath(strFile)
        self.m_pLbResultScore[viewChair]:setString(strResult)
        self.m_pLbResultScore[viewChair]:setPosition(posStart)
        self.m_pLbResultScore[viewChair]:runAction(action)
    end
    self:showFlyGold()
end

function QznnMainLayer:showFlyGold()
    --通吃/通赔
    local bTongChi, bTongPei = true, true
    local delayOut = 0
    local bankerID = QznnDataMgr:getInstance():getBanker()
    for i, user in pairs(self.m_PlayUsers) do
        if user.wChairID ~= bankerID then
            local result = QznnDataMgr:getInstance():getResult(user.wChairID)
            if result > 0 then
                bTongChi = false
            elseif result < 0  then
                bTongPei = false
            end
        end
    end

    local bankChairID = QznnDataMgr:getInstance():getBanker()
    local bankViewChair = self:switchViewChairID(bankChairID)
    if bTongChi then--全部飞给庄家
        delayOut = 1.5
        for i, user in pairs(self.m_PlayUsers) do
            if user.wChairID ~= bankChairID then
                local viewChair = self:switchViewChairID(user.wChairID)
                self:flyGoldEx(viewChair, bankViewChair)
            end 
        end
    elseif bTongPei then--全部从庄家那里飞
        delayOut = 1.5
        for i, user in pairs(self.m_PlayUsers) do
            if user.wChairID ~= bankChairID then
                local viewChair = self:switchViewChairID(user.wChairID)
                self:flyGoldEx(bankViewChair, viewChair)
            end 
        end
    else--有输有赢
        delayOut = 3
        --输掉飞给庄家，赢的庄家飞过来
        for i, user in pairs(self.m_PlayUsers) do
            if user.wChairID ~= bankChairID then
                local result = QznnDataMgr:getInstance():getResult(user.wChairID)
                local viewChair = self:switchViewChairID(user.wChairID)
                if result < 0 then --输给庄家
                    self:flyGoldEx(viewChair, bankViewChair)
                elseif result > 0 then--庄家输
                    self:flyGoldEx(bankViewChair, viewChair, 0.7)
                end
            end 
        end
    end

    if self.m_bIsKill then
        self:doSomethingLater(function()
            local score = Player:getInstance():getUserScore()
            local strScore = LuaUtils.getFormatGoldAndNumberAndZi(score)
            self.m_pLbUserGold[self.m_wMeViewChair]:setString(strScore)
            self:onMoveExitView()
        end, delayOut)
    else
        self:showResultEnd()
    end
end

function QznnMainLayer:showResultEnd()
    self.m_bShowEnd = false
    self:updateFreeLayer(true)
    self:stopCountDown()
    
    --如果当局我没操作，算我托管
    if self.m_bIsEntrust and self:isMeJoin() then
        self.m_pLbClock:setString(tostring(3))
        self:updateEntrustLayer(true)
    end

    local myTableID = Player:getInstance():getTableID()
    local otherUsers = CUserManager:getInstance():getUserInfoInTable(myTableID)
    if table.nums(otherUsers) == 0 then
        self:showWaitTip(WAIT_PLAYER)
        if not self.m_bIsEntrust then
            self:doSendReady()
        else
            self:startCountDown()
        end
    else
        self:startCountDown()
        self:showWaitTip(WAIT_START)
    end
    self:updateAllUserInfo()
end

function QznnMainLayer:updateCallBtnStatus()
    for i = 0, 3 do
        self.m_pBtnCall[i]:setEnabled(true)
    end
end

function QznnMainLayer:updateBetBtnStatus()
   for i = 0, 3 do
        self.m_pBtnBet[i]:setEnabled(true)
    end
end

function QznnMainLayer:showWaitTip(waitTipType)
    if self.m_pArmWaitPlayer then self.m_pArmWaitPlayer:setVisible(false) end
    if self.m_pArmWaitNext   then self.m_pArmWaitNext:setVisible(false)   end
    if self.m_pArmWaitStart  then self.m_pArmWaitStart:setVisible(false)  end
    self.m_nWaitStatus = waitTipType
    if     waitTipType == WAIT_PLAYER then 
        self:showWaitPlayerAni()
    elseif waitTipType == WAIT_NEXT   then 
        self:showWaitNextAni()
    elseif waitTipType == WAIT_START  then 
        self:showWaitStartAni()
    end
end

--显示抢庄、加倍提示信息
function QznnMainLayer:showTip(gameTipType, chairID, bFinal)
    bFinal = bFinal or false
    if chairID == INVALID_CHAIR then
        return
    end

    if not self:isUserJoin(chairID) then
        return
    end

    AudioManager:getInstance():playSound(Qznn_Res.SOUND_OF_TIPS)

    local viewChair = self:switchViewChairID(chairID)

    if gameTipType == TIP_CALL then -- 叫庄阶段
        local callIndex = QznnDataMgr:getInstance():getBankerTimes(chairID)
        if bFinal and callIndex == 0 then
            callIndex = 1
        end
        self.m_pNodeCallTip[viewChair]:setString(CallStr[callIndex])
        self.m_pNodeCallTip[viewChair]:setVisible(true)
    elseif gameTipType == TIP_BET then
        self:hideTip(TIP_CALL, viewChair)
        local betIndex = QznnDataMgr:getInstance():getDownTimes(chairID)
        local bei = QznnDataMgr:getInstance():getDownTimesChoose(betIndex)
        local strBei = string.format(Qznn_Res.PNG_OF_TIPBET, bei)
        self.m_pLbBetTip[viewChair]:setString(BetStr[betIndex])
        self.m_pLbBetTip[viewChair]:setVisible(true)
    elseif gameTipType == TIP_FINISH then
        self.m_pSpFinish[viewChair]:setVisible(true)
    end
end

--显示静态提示信息
function QznnMainLayer:showStaticTip(gameTipType, chairID)
    print("showStaticTip:" .. chairID)
    if chairID == INVALID_CHAIR then
        return
    end

    local viewChair = self:switchViewChairID(chairID)
    if gameTipType == TIP_CALL then -- 叫庄阶段
        local callIndex = QznnDataMgr:getInstance():getBankerTimes(chairID)
        self.m_pNodeCallTip[viewChair]:setString(CallStr[callIndex])
        self.m_pNodeCallTip[viewChair]:setVisible(true)
        self.m_pNodeCallTip[viewChair]:setScale(TipAniScale)
        self.m_pNodeCallTip[viewChair]:setPosition(TipAniPos[viewChair])

    elseif gameTipType == TIP_BET then
        self:hideTip(TIP_CALL, viewChair)
        local betIndex = QznnDataMgr:getInstance():getDownTimes(chairID)
        self.m_pLbBetTip[viewChair]:setString(BetStr[betIndex])
        self.m_pLbBetTip[viewChair]:setVisible(true)
        self.m_pLbBetTip[viewChair]:setScale(TipAniScale)
        self.m_pLbBetTip[viewChair]:setPosition(TipAniPos[viewChair])

    elseif gameTipType == TIP_FINISH then
        self.m_pSpFinish[viewChair]:setVisible(true)
        self.m_pSpFinish[viewChair]:setScale(1)
    end
end

function QznnMainLayer:hideTip(gameTipType, viewChair)
    
    viewChair = viewChair or -1

    if gameTipType == TIP_CALL then
        if viewChair == -1 then
            for i = 0, GAME_PLAYER_QZNN-1 do
                self.m_pNodeCallTip[i]:setVisible(false)
            end
        else
            self.m_pNodeCallTip[viewChair]:setVisible(false)
        end
    elseif gameTipType == TIP_BET then
        if viewChair == -1 then
            for i = 0, GAME_PLAYER_QZNN-1 do
                self.m_pLbBetTip[i]:setVisible(false)
            end
        else
            self.m_pLbBetTip[viewChair]:setVisible(false)
        end
    elseif gameTipType == TIP_FINISH then
        if viewChair == -1 then
            for i = 0, GAME_PLAYER_QZNN-1 do
                self.m_pSpFinish[i]:setVisible(false)
            end
        else
            self.m_pSpFinish[viewChair]:setVisible(false)
        end
    end
end

function QznnMainLayer:startCountDown()
    
    if not self:isMeJoin() and not QznnDataMgr:getInstance():getGameStatusFree() then
        return
    end
    
    local preStr = ""
    if QznnDataMgr:getInstance():getGameStatusFree() then
        preStr = ""
        self.m_nCountDown = QznnDataMgr:getInstance():getGameStartTime()
    elseif QznnDataMgr:getInstance():getGameStatusCall() then
        preStr = "请抢庄:"
        self.m_nCountDown = QznnDataMgr:getInstance():getBankerTime()
    elseif QznnDataMgr:getInstance():getGameStatusDown () then
        preStr = "请投注:"
        self.m_nCountDown = QznnDataMgr:getInstance():getDownJettonTime()
    elseif QznnDataMgr:getInstance():getGameStatusPlay () then
        preStr = "请摊牌:"
        self.m_nCountDown = QznnDataMgr:getInstance():getTakeCardTime()

    end
    self.m_pLbStartCountDown:setVisible(true)
    if QznnDataMgr:getInstance():getGameStatusPlaying() then
        self:updateStatusTips(true)
    end

    if "" ~= preStr then
        self.m_pNodeClock:setVisible(true)
        self:updateCountDownLabel(preStr .. self.m_nCountDown)
    else
        self.m_pNodeClock:setVisible(false)
    end

    self.m_bAuto = self:isMeJoin()
end

function QznnMainLayer:updateCountDown(dt)--循环计时
    
    if not self.m_pNodeClock:isVisible()
    and not self.m_pLbStartCountDown:isVisible()
    then
        return
    end

    local preStr = ""
    if QznnDataMgr:getInstance():getGameStatusFree() then
        preStr = ""
    elseif QznnDataMgr:getInstance():getGameStatusCall() then
        preStr = "请抢庄:"
    elseif QznnDataMgr:getInstance():getGameStatusDown() then
        preStr = "请投注:"
    elseif QznnDataMgr:getInstance():getGameStatusPlay() then
        preStr = "请摊牌:"
    end

    self.m_nCountDown = self.m_nCountDown - 1

    if self.m_nCountDown < 0 then
        return
    end

    if self.m_bIsEntrust and self.m_nCountDown > 0 then
        self.m_pLbClock:setString(tostring(self.m_nCountDown))
    end

    if QznnDataMgr:getInstance():getGameStatusFree() then
        self.m_pLabelStart:setString(self.m_nCountDown)
    end

    if self.m_nCountDown == 0 then
        self:handleAuto()
        self:stopCountDown()
    else
        if self.m_nCountDown == 1 and QznnDataMgr:getInstance():getGameStatusFree() then
            AudioManager:getInstance():playSound(Qznn_Res.SOUND_OF_READY)
        end
        if "" ~= preStr then
            self:updateCountDownLabel(preStr .. self.m_nCountDown)
        end
    end
end

function QznnMainLayer:stopCountDown()

    self.m_pNodeClock:setVisible(false)
    AudioManager:getInstance():stopSound(self.m_nClockSoundID)
    self:updateStatusTips(false)
    self:updateCountDownLabel()

    if self.m_pArmWaitStart then
        self.m_pArmWaitStart:setVisible(false)
    end
    self.m_nCountDown = -1
    self.m_bAuto = false
end

function QznnMainLayer:handleAuto()
    
    if not self.m_bAuto or not self:isMeJoin() then
        if not QznnDataMgr:getInstance():getGameStatusFree() then
            return
        end
    end

    local myChairID = Player:getInstance():getChairID()
    if QznnDataMgr:getInstance():getGameStatusFree() then  
        --如果当局托管，结束倒计时完,自动退出
        if self.m_bIsEntrust then
            --fix bug 没办法啊
            self:doSomethingLater(function()
                self:stopCountDown()
                self:updateEntrustLayer(false)
                FloatMessage:getInstance():pushMessage("STRING_222")
                self:onMoveExitView() 
            end, 0.003)
        else
            self:doSendReady()
        end
    elseif QznnDataMgr:getInstance():getGameStatusCall() then   
        self:doSendCallBanker(0)

        --fix 抢庄增加动画先行
        QznnDataMgr:getInstance():setBankerTimes(myChairID, 0)
        QznnDataMgr:getInstance():setLocaBankerTimes(0)
        self:showTip(TIP_CALL, myChairID)
        self.m_bAuto = false
       
        self:updateCallLayer(false)
    elseif QznnDataMgr:getInstance():getGameStatusDown() then   
        if myChairID == QznnDataMgr:getInstance():getBanker()
            or QznnDataMgr:getInstance():getDownTimes(myChairID) > 0 then
            return
        end

        self:doSendDownJetton(0)

         --fix 下注动画先行
        QznnDataMgr:getInstance():setDownTimes(myChairID, 0)
        QznnDataMgr:getInstance():setLocalDownTimes(0)
        self:showTip(TIP_BET, myChairID)
        self.m_bAuto = false

        self:updateBetLayer(false)
    elseif QznnDataMgr:getInstance():getGameStatusPlay() then   
        self:onSubmitCard(true)
        self:updateMatchLayer(false)
    end
end

--提示当前游戏状态，和倒计时联用
function QznnMainLayer:updateStatusTips(isVisible)
--    self.m_pStatusTips:setVisible(isVisible)
--    if QznnDataMgr:getInstance():getGameStatusFree() then  
--        self.m_pStatusTips:setVisible(false)
--    elseif QznnDataMgr:getInstance():getGameStatusCall() then   
--        self.m_pStatusTips:setSpriteFrame("gui-text-call.png")
--    elseif QznnDataMgr:getInstance():getGameStatusDown() then 
--        if Player:getInstance():getChairID() == QznnDataMgr:getInstance():getBanker() then
--            self.m_pStatusTips:setSpriteFrame("gui-text-bankwait.png")
--        else  
--            self.m_pStatusTips:setSpriteFrame("gui-text-bet.png")
--        end
--    elseif QznnDataMgr:getInstance():getGameStatusPlay() then  
--        self.m_pStatusTips:setSpriteFrame("gui-text-submit.png")
--    end
end


--add------------------------------------------------
function QznnMainLayer:updateCallLayer(enable)
    self.m_pNodeCall:setVisible(enable)
end
function QznnMainLayer:updateBetLayer(enable)
    self.m_pNodeBet:setVisible(enable)
end
function QznnMainLayer:updateMatchLayer(enable)
    self.m_pNodeMatch:setVisible(enable)
end
function QznnMainLayer:updateFreeLayer(enable)
    self.m_pNodeFree:setVisible(enable)
end
function QznnMainLayer:updatePlayLayer(enable)
    self.m_pNodePlay:setVisible(enable)
end
function QznnMainLayer:updateEndLayer(enable)
    self.m_pNodeEnd:setVisible(enable)
end
function QznnMainLayer:updateTypeLayer(enable)
    self.m_pNodeType:setVisible(enable)
end
function QznnMainLayer:updateEntrustLayer(enable)
    self.m_pNodeEntrust:setVisible(enable)
end
function QznnMainLayer:updateGrayLayer(enable)
    self.m_pGrayLayer:setVisible(enable)
end
function QznnMainLayer:updateBtnSubmit(enable)
    self.m_pBtnSubmieNiu:setEnabled(enable)
end
function QznnMainLayer:updateButtonOfMusic(enable)
    local bEnable = enable or AudioManager:getInstance():getMusicOn()
    local path = bEnable and Qznn_Res.PNG_OF_MUSIC_ON or Qznn_Res.PNG_OF_MUSIC_OFF
    self.m_pBtnMusic:loadTextureNormal(path,ccui.TextureResType.plistType)
end
function QznnMainLayer:updateButtonOfSound(enable)
    local bEnable = enable or AudioManager:getInstance():getSoundOn()
    local path = bEnable and Qznn_Res.PNG_OF_SOUND_ON or Qznn_Res.PNG_OF_SOUND_OFF
    self.m_pBtnSound:loadTextureNormal(path,ccui.TextureResType.plistType)
end
function QznnMainLayer:updateCountDownLabel(s)
    self.m_pLbStartCountDown:setString(s)
    self.m_pLbStartCountDown:setVisible(s~=nil)
end

function QznnMainLayer:updateBaseScore()
    local score = QznnDataMgr:getInstance():getBaseScore()
    local string175 = LuaUtils.getLocalString("STRING_175")
    local strFormat = LuaUtils.getFormatGoldAndNumber(score)
    local strScore = string.format("%s: %s", string175, strFormat)
    self.m_pLbBaseScore:setString(strScore)
end

--显示牌型
function QznnMainLayer:updateCardAndType(chairID)
    local myChairID = Player:getInstance():getChairID()
    local cardType = QznnDataMgr:getInstance():getCardType(chairID)
    --显示摊牌
    if chairID == myChairID then
        --动画现行已经显示
        self:setMyCardData()
        local cards = QznnPokerMgr.getInstance():getPokers(SELF_VIEWCHAIR)
        for i = 0, CARD_COUNT_QZNN-1 do
            cards[i]:setPositionY(self.m_pokerPos[SELF_VIEWCHAIR].y)
        end
    else
        self:setUserCardData(chairID, true)
    end
    if cardType > G_CONSTANTS.CardValue_Nothing then
        for i = 0, CARD_COUNT_QZNN-1 do
            local viewChair = self:switchViewChairID(chairID)
            local cards = QznnPokerMgr.getInstance():getPokers(viewChair)
            if i < 3 then
                local pos_y = cards[i]:getPositionY() + 15*2
                cards[i]:setPositionY(pos_y)
            end
        end
    end
    --显示牛几
    local viewChair = self:switchViewChairID(chairID)
    local cardPath = string.format(Qznn_Res.PNG_OF_NIU, cardType)
    self.m_pSpResult[viewChair]:setSpriteFrame(cardPath)
    self.m_pSpResult[viewChair]:setVisible(true)
    self.m_pSpResult[viewChair]:setScale(1)
    --动作
    local user 
    for _, _user in pairs(self.m_PlayUsers) do
        if _user.wChairID == chairID then
            user = _user
        end
    end
    if user then 
        --play sound
        local strGender = user.cbGender == 1 and Qznn_Res.SOUND_OF_WOMEN or Qznn_Res.SOUND_OF_MAN
        local strSound = strGender[cardType]
        AudioManager:getInstance():playSound(strSound)
    end
    --显示牛机动作,变小->变大
    local smallScale = viewChair == SELF_VIEWCHAIR and 0.8 or 0.5
    local bigScale = viewChair == SELF_VIEWCHAIR and 1 or 0.8
    local toSmall = cc.ScaleTo:create(0.2, smallScale)
    local toBig = cc.ScaleTo:create(0.2, bigScale)
    local action = cc.Sequence:create(toSmall, toBig)
    self.m_pSpResult[viewChair]:runAction(action)
end
----------------------------------- update view -------------------------------

----------------------------------- play animation ----------------------------
function QznnMainLayer:showWaitPlayerAni() --等待其他玩家动画
    if self.m_pArmWaitPlayer == nil then
        self.m_pArmWaitPlayer = Effect:getInstance():creatEffectWithDelegate(
            self.m_pNodeFree,
            Qznn_Res.ANI_OF_PLAYER,
            0,
            true,
            POS_OF_CENTER,
            Z_MODEL)
    end
    self.m_pArmWaitPlayer:setVisible(true)
end
function QznnMainLayer:showWaitNextAni() --等待下局开始动画
    if self.m_pArmWaitNext == nil then
        self.m_pArmWaitNext = Effect:getInstance():creatEffectWithDelegate(
            self.m_pNodeFree,
            Qznn_Res.ANI_OF_NEXT,
            0,
            true,
            POS_OF_CENTER,
            Z_MODEL)
    end
    self.m_pArmWaitNext:setVisible(true)
end
function QznnMainLayer:showWaitStartAni() --等待开始动画
    if self.m_pArmWaitStart == nil then
        self.m_pArmWaitStart = Effect:getInstance():creatEffectWithDelegate(
            self.m_pNodeFree,
            Qznn_Res.ANI_OF_WAIT,
            0,
            true,
            POS_OF_CENTER,
            Z_MODEL)
    end
    self.m_pArmWaitStart:setVisible(true)

    if self.m_pLabelStart == nil then --等待开始倒计时
        self.m_pLabelStart = cc.Label:createWithBMFont("game/qznn/font/cj_ps.fnt","")
        self.m_pLabelStart:setAnchorPoint(0.5, 0.5)
        self.m_pLabelStart:setPosition(190, 4)
        self.m_pLabelStart:addTo(self.m_pArmWaitStart)
    end
    if self.m_nCountDown >= 0 then
        self.m_pLabelStart:setString(self.m_nCountDown)
    else
        self.m_pLabelStart:setString("")
    end
end

-- 显示庄家表示落点动画
function QznnMainLayer:showBankerAni(bGoto)
    if self.m_pArmBanker then
        self.m_pArmBanker:removeFromParent()
        self.m_pArmBanker = nil
    end
    self.m_pArmBanker = Effect:getInstance():creatEffectWithDelegate(
        self.m_pNodeUserBankerNode,
        Qznn_Res.ANI_OF_BANKER,
        0,
        true,
        POS_OF_CENTER,
        Z_TOP)
    if bGoto then
        self.m_pArmBanker:setScale(0.8)
    else
        local animationEvent = function(armatureBack, movementType, movementID)
            if movementType == ccs.MovementEventType.complete then
                self:setBanker()
            end
        end
        self.m_pArmBanker:getAnimation():setMovementEventCallFunc(animationEvent)
        if not self.m_bGameInitStart then
            AudioManager:getInstance():playSound(Qznn_Res.SOUND_OF_BANKER)
        end
    end
end
function QznnMainLayer:showWinAni()
    local armuture = Effect:getInstance():creatEffectWithDelegate2(
        self.m_pNodeEnd,
        Qznn_Res.ANI_OF_WIN,
        "Animation1",
        true,
        POS_OF_CENTER,
        Z_MODEL)
    local animationEvent = function(armatureBack, movementType, movementID)
        if movementType == ccs.MovementEventType.complete 
        or movementType == ccs.MovementEventType.loopComplete
        then
            self:showResultScore()
            armatureBack:removeFromParent()
        end
    end
    armuture:getAnimation():setMovementEventCallFunc(animationEvent)
end
function QznnMainLayer:showLoseAni()
    local armuture = Effect:getInstance():creatEffectWithDelegate2(
        self.m_pNodeEnd,
        Qznn_Res.ANI_OF_LOSE,
        "Animation2",
        true,
        POS_OF_CENTER,
        Z_MODEL)
    local animationEvent = function(armatureBack, movementType, movementID)
        if movementType == ccs.MovementEventType.complete 
        or movementType == ccs.MovementEventType.loopComplete
        then
            self:showResultScore()
            armatureBack:removeFromParent()
        end
    end
    armuture:getAnimation():setMovementEventCallFunc(animationEvent)
end

----------------------------------- play animation ----------------------------

---------------------------------- resource -----------------------------------
function QznnMainLayer:doSoundPlayLater(sound, delay)
    
    if delay <= 0 then
        AudioManager:getInstance():playSound(sound)
    else
        self:doSomethingLater(function()
            AudioManager:getInstance():playSound(sound)
        end, delay)
    end
end
function QznnMainLayer:doSomethingLater(call, delay, ...)
    self.m_rootUI:runAction(cc.Sequence:create(cc.DelayTime:create(delay), cc.CallFunc:create(call, {...})))
end
function QznnMainLayer:getUserInSameTable(tag) --同一张桌子的玩家信息
    
    local userID = Player:getInstance():getUserID()
    if tag ~= self.m_wMeViewChair then
        local tableID = Player:getInstance():getTableID()
        local otherUsers = CUserManager:getInstance():getUserInfoInTable(tableID)
        for _, user in pairs(otherUsers) do
            local viewChair = QznnDataMgr:getInstance():SwitchViewChairID(user.wChairID)
            if tag == viewChair then
                userID = user.dwUserID
                break
            end
        end
    end
    return userID
end
---------------------------------- resource -----------------------------------

---------------------------------- open layer --------------------------------
function QznnMainLayer:showUserInfo(userID, pos, tag, node)
    CommonUserInfo.create(1)
        :setPosition(pos)
        :setTag(tag)
        :addTo(node)
        :updateUserInfo(userID)
end

function QznnMainLayer:showRuleLayer() --打开规则
    local pRule = QznnRuleLayer.create()
    pRule:addTo(self.m_rootUI, 101):setPositionX((display.width - 1624) / 2)
end

function QznnMainLayer:showMessageBox(msg) --打开确认框
    SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_MESSAGEBOX, msg)
end

function QznnMainLayer:showFloatmessage(msg) --提示信息
    FloatMessage:getInstance():pushMessage(msg)
end

--function QznnMainLayer:showRollMessage(msg)--滚动消息
--    RollMsg:getInstance():addMessage(msg)
--end
function QznnMainLayer:showIngameBank() --打开银行
    SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_INGAMEBANK)
end
---------------------------------- other layer --------------------------------

function QznnMainLayer:createClipMenu(menunode)
    local posx = menunode:getPositionX()
    local shap = cc.DrawNode:create()
    local pointArr = {cc.p(posx, 290), cc.p(posx + 400, 290), cc.p(posx + 400, 710), cc.p(posx, 710)}
    shap:drawPolygon(pointArr, 4, cc.c4f(255, 255, 255, 255), 2, cc.c4f(255, 255, 255, 255))
    local node = cc.ClippingNode:create(shap)
    return node
end

function QznnMainLayer:getPositionWithPopRect(size1, size2, dir, posAdd)--1-up;2-down;3-left;4-right
    
    local pos = { --up/down/left/right
        cc.p(0, size1.height/2 + size2.height/2),
        cc.p(0, 0 - size1.height/2 - size2.height/2),
        cc.p(0 - size1.width/2 - size2.width/2, 0),
        cc.p(size1.width/2 + size2.width/2, 0),
    }
    local offset_add = posAdd
    local offset_sub = cc.p(size2.width/2, size2.height/2)
    local ret = cc.pAdd(offset_add, cc.pSub(pos[dir], offset_sub))
    return ret
end

function QznnMainLayer:flyGold(_from, _to, _delaytime)
    if self.m_bIsKill and _from == self.m_wMeViewChair then
        local score = Player:getInstance():getUserScore()
        local strScore = LuaUtils.getFormatGoldAndNumberAndZi(score)
        self.m_pLbUserGold[self.m_wMeViewChair]:setString(strScore)
    end
    local function goldFly(from, to)
        AudioManager.getInstance():playSound(Qznn_Res.SOUND_OF_FLYGOLD)
        local arm  = ccs.Armature:create(Qznn_Res.ANI_OF_FLYGOLD)
        local armName = Qznn_Res.ANI_OF_FLY_GOLD[from.."-"..to][1]
        print("flyGold:"..armName)
        arm:getAnimation():play(armName, 0 , 0)
        arm:setPosition(cc.p(667, 375))
        arm:addTo(self)
        local animationEvent = function(armature, movementType, movementID)
            if movementType == ccs.MovementEventType.loopComplete or movementType == ccs.MovementEventType.complete then
                arm:removeFromParent()
            end
        end
        arm:getAnimation():setMovementEventCallFunc(animationEvent)
    end

    if _delaytime ~= nil and _delaytime > 0 then
        self:doSomethingLater(function()
            goldFly(_from, _to)
        end, _delaytime)
    else
        goldFly(_from, _to)
    end
end

function QznnMainLayer:flyGoldEx(_from, _to, _delaytime)
    if self.m_bIsKill and _from == self.m_wMeViewChair then
        local score = Player:getInstance():getUserScore()
        local strScore = LuaUtils.getFormatGoldAndNumberAndZi(score)
        self.m_pLbUserGold[self.m_wMeViewChair]:setString(strScore)
    end
    local function goldFly(from, to)
        local targetPos = cc.p(self.m_flyGoldPos[to])
        local startPos = cc.p(self.m_flyGoldPos[from])
        --计算距离
        local distance = ccpDistance(targetPos,startPos)
        local offset = 10
        local flyDt = 0.5

        local num = distance / 40

        if num  < 7 then
            num = 7
        --elseif num > 30 then
            --num = 30
        end

        local totalDt = 0.9
        local flyDtOffset = (totalDt - flyDt) / num

        for i = 1, num do
            --创建金币
            local coinTemp = GoldPool.getInstance():takeItem()
            if nil == coinTemp then
                return
            end
            coinTemp:setVisible(false)
            self.m_pNodeAni:addChild(coinTemp)

            local randomX = math.random()*3 - 1
            local randomY = math.random()*3 - 1

            --设置初始坐标
            coinTemp:setPosition(startPos)

            local bezier = {  
                ccp((startPos.x + targetPos.x)*0.5 + randomX*offset ,(startPos.y + targetPos.y)*0.5 + randomY*offset),
                ccp((startPos.x + targetPos.x)*0.5 + randomX*offset ,(startPos.y + targetPos.y)*0.5 + randomY*offset),
                cc.p(targetPos.x + math.random()*10, targetPos.y + math.random()*10),
            }

            --设置动画 移动完删除自己
            coinTemp:runAction(cc.Sequence:create(
                cc.Show:create(),
                cc.EaseExponentialInOut:create(cc.BezierTo:create(flyDt + i*flyDtOffset, bezier)),
                cc.CallFunc:create(function()
                    coinTemp:setVisible(false)
                    coinTemp:removeFromParent()
                    GoldPool.getInstance():putItem(coinTemp)
                    self:playPartical(to)
                end)
            ))
        end
        --金币配音
        self.m_rootUI:runAction(cc.Sequence:create(
            cc.DelayTime:create(flyDt), 
            cc.CallFunc:create(function()
                AudioManager.getInstance():playSound(Qznn_Res.SOUND_OF_FLYGOLD)
                
            end
        )))
    end

    if _delaytime ~= nil and _delaytime > 0 then
        self:doSomethingLater(function()
            goldFly(_from, _to)
        end, _delaytime)
    else
        goldFly(_from, _to)
    end
end

--在玩家头上播放效果
function QznnMainLayer:playPartical(viewChairId)
    --从池中取出粒子效果做延时放回池中
    local partical = ParticalPool.getInstance():takePartical()
    if nil == partical then return end

    self.m_pNodeAni:addChild(partical)
    partical:setPosition(self.m_flyGoldPos[viewChairId])
    partical:resetSystem()
    partical:runAction(cc.Sequence:create(
        cc.DelayTime:create(1),
        cc.CallFunc:create(function()
            partical:removeFromParent()
            ParticalPool.getInstance():putPartical(partical)
        end)
    ))
end

return QznnMainLayer