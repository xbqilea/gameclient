local CommonRoom = import("app.newHall.layer.CommonRoom")
local QznnRoomLayer = class("QznnRoomLayer", function()
    return CommonRoom.new()
end) --继承基类

local PREFIX        = "game/qznn/"
local PATH_ROOM_CSB = "game/qznn/csb/gui-qznn-roomChoose.csb"
local PATH_ROOM_UI  = "game/qznn/csb/gui-qznn-roomLayer.csb"

local CONFIG = {
	--[[[1] = {
        RoomScore   = 1,
		AnimName 	= PREFIX .. "effect/325_qiangzhuangniuniu_tiyanchang/325_qiangzhuangniuniu_tiyanchang",
	},]]--
	[1] = {
        RoomScore   = 1000,
		AnimName 	= PREFIX .. "effect/325_qiangzhuangniuniu_xinshouchang/325_qiangzhuangniuniu_xinshouchang",
	},
	[2] = {
        RoomScore   = 10000,
		AnimName 	= PREFIX .. "effect/325_qiangzhuangniuniu_chujichang/325_qiangzhuangniuniu_chujichang",
	},
	[3] = {
        RoomScore   = 100000,
		AnimName 	= PREFIX .. "effect/325_qiangzhuangniuniu_zhongjichang/325_qiangzhuangniuniu_zhongjichang",
	},
    --[[[3] = {
        RoomScore   = 200000,
		AnimName 	= PREFIX .. "effect/325_qiangzhuangniuniu_gaojichang/325_qiangzhuangniuniu_gaojichang",
	},]]--
}

local MUSIC = "game/qznn/sound/NiuBgm.mp3"

function QznnRoomLayer:ctor(roomList)
    self:init(roomList)
end

function QznnRoomLayer:init(roomList)
    self._roomList = roomList
    self:initCSB()
    self:initNode()
    self:initCommonRoom()
end

function QznnRoomLayer:onEnter()
    self.super:onEnter()
end

function QznnRoomLayer:onExit()
    self.super:onExit()
end

function QznnRoomLayer:initCSB()

    --node path
    self.m_pathUI = cc.CSLoader:createNode(PATH_ROOM_CSB)
    self.m_pathUI:addTo(self)
     local diffY = (display.size.height - 750) / 2
     self.m_pathUI:setPosition(cc.p(0,diffY))
     local center = self.m_pathUI:getChildByName("Layer_base");
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
    --node base
    self.m_pNodeUI = self.m_pathUI:getChildByName("Layer_base")

    --node child
    self.m_pNodeBg = self.m_pNodeUI:getChildByName("Node_bg")
    self.m_pNodeMenu = self.m_pNodeUI:getChildByName("Node_menu")
    self.m_pNodeRoom = self.m_pNodeUI:getChildByName("Node_room")
    self.m_pNodeUser = self.m_pNodeUI:getChildByName("Node_user")
    self.m_pNodeGold = self.m_pNodeUI:getChildByName("Node_gold")
    self.m_pNodeBank = self.m_pNodeUI:getChildByName("Node_bank")
    self.m_pNodeStart = self.m_pNodeUI:getChildByName("Node_start")
    self.m_pNodeTop   = self.m_pNodeUI:getChildByName("Node_top")

    self.m_pImageLogo = self.m_pNodeTop:getChildByName("Image_logo")--0.游戏名
    self.m_pBtnReturn = self.m_pNodeMenu:getChildByName("Button_back")--1.退出按钮
    self.m_pBtnRule = self.m_pNodeMenu:getChildByName("Button_help")--2.规则按钮
    self.m_pRoomView = self.m_pNodeRoom:getChildByName("ScrollView")--3.房间
    self.m_pLabelName = self.m_pNodeUser:getChildByName("Text_name")--4.名字
    self.m_pImageLevel = self.m_pNodeUser:getChildByName("Image_vip")--5.等级
    self.m_pImageHead = self.m_pNodeUser:getChildByName("Image_head")--6.头像
    self.m_pImageFrame = self.m_pNodeUser:getChildByName("Image_frame")--7.头像框
    self.m_pLabelGold = self.m_pNodeGold:getChildByName("Text_gold")--8.金币
    self.m_pImageBank = self.m_pNodeGold:getChildByName("Image_gold")--9.银行点击
    self.m_pBtnBank = self.m_pNodeGold:getChildByName("Button_bank")--9.银行点击
    self.m_pLabelBank = self.m_pNodeBank:getChildByName("Text_bank")--9.银行
    self.m_pBtnStart = self.m_pNodeStart:getChildByName("Button_quick")--10.开始
    self.m_pImageDown = self.m_pNodeBg:getChildByName("Image_down")--信息底框

    --房间layer
    self.m_pRoomView:removeAllChildren()
    self.m_pRoomUI = cc.CSLoader:createNode(PATH_ROOM_UI)
    self.m_pRoomUI:addTo(self.m_pRoomView)
    self.m_pRoomLayer = self.m_pRoomUI:getChildByName("Panel")

    --[[if ClientConfig.getInstance():getIsOtherChannel() then
        self.m_pImageLogo:loadTexture("game/qznn/roomChoose/gui-image-logo-2.png", ccui.TextureResType.plistType)
    end]]--
end

function QznnRoomLayer:initNode()

    --房间
    self.m_pBtnRoom = {}
    self.m_pImageState = {}
    self.m_pNodeRoomList = {}
    for i, v in pairs(CONFIG) do
        
        --房间节点
        local node = self.m_pRoomLayer:getChildByName("Panel_" .. v.RoomScore)
        local node_spine = node:getChildByName("Node_spine")
        local node_name  = node:getChildByName("Image_name")
        local node_score = node:getChildByName("Text_score")
        local node_base  = node:getChildByName("Text_base")
        local node_click = node:getChildByName("Image_click")
        local node_state = node:getChildByName("Image_state")

        self.m_pNodeRoomList[i] = node
        self.m_pNodeRoomList[i]:setTag(v.RoomScore)
        self.m_pImageState[i] = node_state
        self.m_pImageState[i]:setTag(v.RoomScore)

        --node
        local size = node:getContentSize()
        local posX, posY = node:getPosition()
        node:setPositionX(posX + size.width / 2)
        node:setPositionY(posY + size.height / 2)
        node:setAnchorPoint(0.5, 0.5)

        --动画节点
        local skeNode = sp.SkeletonAnimation:createWithBinaryFile(v.AnimName .. ".skel", v.AnimName .. ".atlas", 1)
        if skeNode then
            skeNode:setPosition(cc.p(0 ,0))
            skeNode:setAnimation(0, "animation", true)
            skeNode:addTo(node_spine)
        end

        --按钮节点
        local size_click = node_click:getContentSize()
        local pos_click = cc.p(node_click:getPosition())

        --使用ControlButton
        local pSprite = cc.Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
        local pSpriteNormal = ccui.Scale9Sprite:createWithSpriteFrame(pSprite:getSpriteFrame())
        local pSpriteSelect = ccui.Scale9Sprite:createWithSpriteFrame(pSprite:getSpriteFrame())
        local pButtonClick = cc.ControlButton:create(pSpriteNormal)
        pSpriteNormal:setContentSize(size_click)
        pSpriteSelect:setContentSize(size_click)
        --pButtonClick:setBackgroundSpriteForState(pSpriteSelect, cc.CONTROL_STATE_HIGH_LIGHTED)
        pButtonClick:setSwallowsTouches(false)
        pButtonClick:setContentSize(size_click)
        pButtonClick:setAnchorPoint(0.5, 0.5)
        pButtonClick:setPosition(pos_click)
        pButtonClick:setTag(i)
        pButtonClick:addTo(node, 999)
        table.insert(self.m_pBtnRoom, pButtonClick)

        --记录点击放大的节点
        pButtonClick.nodeClick = node
    end

    --背景特效
end

function QznnRoomLayer:initCommonRoom()

    --绑定4个button
    self:setButtonReturn(self.m_pBtnReturn)
    self:setButtonRule(self.m_pBtnRule)
    self:setButtonStart(self.m_pBtnStart)
    self:setButtonRoom2(self.m_pBtnRoom)
    self:setButtonBank(self.m_pImageBank)
    self:setButtonBank(self.m_pBtnBank)

    --绑定4个image
    self:setImageLevel(self.m_pImageLevel)
    self:setImageHead(self.m_pImageHead)
    self:setImageFrame(self.m_pImageFrame)
    --self:setImageState(self.m_pImageState)

    --绑定3个label
    self:setLabelName(self.m_pLabelName)
    self:setLabelGold(self.m_pLabelGold)
    self:setLabelBank(self.m_pLabelBank)

    --列表
    --self:setScrollView(self.m_pRoomView, self.m_pRoomLayer, self.m_pNodeRoomList)

    --箭头
    --self:createArrow(self.m_pRoomView, self.m_pRoomLayer, self.m_pNodeRoom)

    --位置
  --  self:adaptInfoTop({self.m_pBtnReturn, self.m_pBtnRule, }, {self.m_pImageLogo, })
   -- self:adaptInfoBar({self.m_pNodeUser, self.m_pNodeGold, self.m_pNodeStart, })
    --self:adaptInfoBar3({self.m_pNodeUser, self.m_pNodeGold, self.m_pNodeBank, self.m_pNodeStart, })

    --入场动画
    self:playActionTop({self.m_pBtnReturn, self.m_pBtnRule, self.m_pImageLogo, })
    self:playActionBar({self.m_pNodeUser, self.m_pNodeGold, self.m_pNodeBank, self.m_pNodeStart, self.m_pImageDown})
    self:playActionList(self.m_pNodeRoomList)

    --背景音乐
    --self:playBGMusic(MUSIC)
end

return QznnRoomLayer
-- endregion
