
--抢庄牛牛--
local text = {
    ["QZNN_1"] = "您的下注操作超时了,默认下最小注",
    ["QZNN_2"] = "您的抢庄操作超时了,默认不抢庄",
}
for k, v in pairs(text) do
    cc.exports.Localization_cn[k] = text[k]
end

local cmd = {

    ------ 抢庄牛牛 413-----------------------------------
    SUB_S_SysMessage_QZNN			= 220,          --系统消息
    SUB_S_GAME_START_QZNN			= 100,			--开始叫庄
    SUB_S_CALL_BANKER_NOTIFY_QZNN   = 101,	        --玩家抢庄通知
    SUB_S_DOWN_JETTON_QZNN			= 102,			--通知进入下注阶段
    SUB_S_DOWN_JETTON_NOTIFY_QZNN	= 103,	        --玩家下注通知
    SUB_S_SEND_CARD_QZNN			= 104,			--发牌
    SUB_S_USER_FINISH_QZNN			= 105,			--某个玩家完成配牌
    SUB_S_GAME_END_QZNN			    = 106,			--游戏结束
        
    SUB_C_CALL_BANKER_QZNN			= 1,			--用户叫庄
    SUB_C_DOWN_JETTON_QZNN			= 2,			--用户下注
    SUB_C_TAKE_CARD_QZNN			= 3,			--提交扑克
    ------ 抢庄牛牛 413-----------------------------------
}

for k, v in pairs(cmd) do
    cc.exports.G_C_CMD[k] = cmd[k]
end

--加载界面的提示语
local text = {
    "拼一拼，草房变洋房，摩托换奔驰。",
    "输了不投降，竞争意识强。",
    "输赢都不走，能做一把手。",
    "拼出今天的胜利，拼出明天的未来！",
    "抢庄家，大杀四方！",
}
cc.exports.Localization_cn["Loading_413"] = text