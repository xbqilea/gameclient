--region *.lua
--Date 2017-2-25
--Author xufei
--此文件由[BabeLua]插件自动生成

local Qznn_Events = { 

    MSG_QZNN_GAME_INIT          = "qznn_evt_game_init",         --初始化
    MSG_QZNN_GAME_START         = "qznn_evt_game_start",        --开始
    MSG_QZNN_GAME_CALL_BANKER   = "qznn_evt_game_call_banker",  --叫庄
    MSG_QZNN_GAME_DOWN_START    = "qznn_evt_game_down_start",   --开始下注
    MSG_QZNN_GAME_DOWN_JETTON   = "qznn_evt_game_down_jetton",  --下注
    MSG_QZNN_GAME_SEND_CARD     = "qznn_evt_game_send_card",    --发牌
    MSG_QZNN_GAME_USER_FINISH   = "qznn_evt_game_user_finish",  --完成发送
    MSG_QZNN_GAME_END           = "qznn_evt_game_end",          --结束
    MSG_QZNN_GAME_RESTART       = "qznn_evt_game_restart",      --重新开始
}

return Qznn_Events

--endregion
