
local QznnDataMgr = class("QznnDataMgr")

local Player = cc.exports.PlayerInfo

local GAME_STATUS_FREE = 0 -- 空闲状态
local GAME_STATUS_PLAY = 100 -- 游戏状态
local GAME_STATUS_WAIT = 200 -- 等待状态

QznnDataMgr.QZNN_T_FREE = GAME_STATUS_FREE
QznnDataMgr.QZNN_T_CALL = GAME_STATUS_PLAY
QznnDataMgr.QZNN_T_DOWN = GAME_STATUS_PLAY + 1
QznnDataMgr.QZNN_T_PLAY = GAME_STATUS_PLAY + 2

QznnDataMgr.instance_ = nil
function QznnDataMgr:getInstance()
    if QznnDataMgr.instance_ == nil then
        QznnDataMgr.instance_ = QznnDataMgr.new()
    end
    return QznnDataMgr.instance_
end
function QznnDataMgr:releaseInstance()
    QznnDataMgr.instance_ = nil
end
function QznnDataMgr:ctor()
    self:Clean()
end

function QznnDataMgr:Clean()

    self.m_cbGameStatus = GAME_STATUS_FREE
    self.m_cbGameStartTime = 10
    self.m_cbBankerTime = 10
    self.m_cbDownJettonTime = 10
    self.m_cbTakeCardTime = 10
    self.m_wBanker = INVALID_CHAIR
    self.m_wMeChairID = INVALID_CHAIR
    self.m_llBaseScore = 0
    self.m_cbFinalCallTimes = 0
    self.m_nMaxTypeMultiple = 5

    self.m_cbCardData = {}
    self.m_iBankerTimes = {}
    self.m_iDownTimes = {}
    self.m_llResult = {}
    self.m_bFinishCard = {}
    self.m_cbCardType = {}
    self.m_cbCardShowFlag = {}
    self.m_iBankerTimesChoose = {}
    self.m_iDownTimesChoose = {}
    for i = 0, 4 do
        self.m_cbCardData[i] = {}
        for j = 0, 4 do
            self.m_cbCardData[i][j] = -1
        end
        self.m_iBankerTimes[i] = 0
        self.m_iDownTimes[i] = -1
        self.m_llResult[i] = 0
        self.m_bFinishCard[i] = false
        self.m_cbCardType[i] = 0
        self.m_cbCardShowFlag[i] = 0

        self.m_iBankerTimesChoose[i] = i
        self.m_iDownTimesChoose[i] = i+1
    end

    --动画先行的变量
    self.m_iLocaBankerTimes = 0
    self.m_iLocalDownTimes = 0
    self.m_iLocalCardType = 0
end

function QznnDataMgr:resetData()

    self.m_wBanker = INVALID_CHAIR

    self.m_cbCardData = {}
    self.m_iBankerTimes = {}
    self.m_iDownTimes = {}
    self.m_llResult = {}
    self.m_bFinishCard = {}
    self.m_cbCardType = {}
    self.m_cbCardShowFlag = {}

    for i = 0, 4 do
        self.m_cbCardData[i] = {}
        for j = 0, 4 do
            self.m_cbCardData[i][j] = -1
        end
        self.m_iBankerTimes[i] = 0
        self.m_iDownTimes[i] = -1
        self.m_llResult[i] = 0
        self.m_bFinishCard[i] = false
        self.m_cbCardType[i] = 0
        self.m_cbCardShowFlag[i] = 0
    end

    self.m_iLocaBankerTimes = 0
    self.m_iLocalDownTimes = 0
    self.m_iLocalCardType = 0
end
function QznnDataMgr:setMeChairID(chairID)
    self.m_wMeChairID = chairID
end
function QznnDataMgr:getMeChairID()
    return self.m_wMeChairID
end
--public
function QznnDataMgr:GetMeViewChairID()
    return 2
end
function QznnDataMgr:SwitchViewChairID(wChairID)
    if wChairID == INVALID_CHAIR or wChairID >= 5 then
        return 0
    end
    local wChairCount = 5
    local meChairID = self:getMeChairID()
    local wViewChairID = (wChairID + wChairCount * 3 / 2 - meChairID) % wChairCount
    local ret = math.floor(wViewChairID)
    return ret
end
function QznnDataMgr:setCardType(wChairID)
    self.m_cbCardShowFlag[wChairID] = 0
    self.m_cbCardType[wChairID] = self:GetCardTypeByChairID_(self.m_cbCardData[wChairID], wChairID)
end
--private
function QznnDataMgr:GetCardTypeByChairID_(cbCardData, index)
    
    --特殊牛型
    if self:IsJinNiu_(cbCardData, index) then
        return G_CONSTANTS.CardValue_Jin
    elseif self:IsYinNiu_(cbCardData, index) then
        return G_CONSTANTS.CardValue_Yin
    end
    --普通牛型
    local kCount = 0
    for i = 0, 4 do
        if cbCardData[i] > 51 then
            kCount = kCount + 1
        end
    end
    if kCount > 0 then
        return self:GetKingType_(cbCardData, kCount, index)
    else
        return self:GetCardArrType_(cbCardData, index)
    end
    return G_CONSTANTS.CardValue_Nothing
end
function QznnDataMgr:IsJinNiu_(cbCardData, index)
    for i = 0, 4 do
        if cbCardData[i] > 51 then
            return false
        elseif cbCardData[i] % 13 < 10 then
            return false
        end
    end
    self.m_cbCardShowFlag[index] = math.pow(2, 0) + math.pow(2, 1) + math.pow(2, 2)
    return true
end
function QznnDataMgr:IsYinNiu_(cbCardData, index)
    local tagIndex = 0
    local nTenCount = 0
    local nJokerCount = 0
    for i = 0, 4 do 
        if cbCardData[i] > 51 then
            nJokerCount = nJokerCount + 1
        elseif cbCardData[i] % 13 == 9 then
            nTenCount = nTenCount + 1
            tagIndex = i
        elseif cbCardData[i] % 13 < 9 then
            return false
        end
    end
    if nJokerCount > 1 then
        return false
    elseif nTenCount == 1 and nJokerCount == 1 then
        return false
    elseif nTenCount > 1 then
        return false
    end

    nTenCount = 0
    for i = 0, 4 do
        if tagIndex ~= i and nTenCount < 4 then
            self.m_cbCardShowFlag[index] = self.m_cbCardShowFlag[index] +  math.pow(2, i)
            nTenCount = nTenCount + 1
        end
    end
    return true
end
function QznnDataMgr:GetBombType_(cbCardData)
    
    local cbCard = {}
    for i = 0, 12 do
        cbCard[i] = 0
    end
    local kingCount = 0
    for i = 0, 4 do
        if cbCardData[i] >  51 then
            kingCount = kingCount + 1
        else
            cbCard[cbCardData[i] % 13] = cbCard[cbCardData[i] % 13] + 1
        end
    end
    for i = 0, 12 do
        if cbCard[i] + kingCount >= 4 then
            return i
        end
    end
    return -1
end
function QznnDataMgr:GetKingType_(cbCardData, kCount, index)
    local temp, num, card = 0, 0, {[0] = 0, 0, 0, 0, 0}
    if kCount == 1 then
        for i = 0, 4 do
            local tCount = 1
            if cbCardData[i] <= 51 then
                for j = i + 1, 4 do
                    if cbCardData[j] <= 51 then
                        for k = j + 1, 4 do
                            --找三组合是否组合成牛
                            if cbCardData[k] <= 51 then
                                if (self:GetCardValue_(cbCardData[i]) +
                                    self:GetCardValue_(cbCardData[j]) +
                                    self:GetCardValue_(cbCardData[k])) % 10 == 0
                                then
                                    tCount, num = 0, 0
                                    for n = 0, 4 do
                                        if n ~= i and n ~= j and n ~= k then
                                            card[num] = cbCardData[n]
                                            num = num + 1
                                        end
                                    end
                                    card[num] = cbCardData[i]
                                    num = num + 1
                                    card[num] = cbCardData[k]
                                    num = num + 1
                                    card[num] = cbCardData[j]
                                    num = num + 1
                                end
                            end
                        end --end for k
                        if tCount == 0 then
                            temp = 0
                            break
                        end
                        --找两张最大组合
                        tCount = (self:GetCardValue_(cbCardData[i]) + self:GetCardValue_(cbCardData[j])) % 10
                        if temp < tCount or tCount == 0 then
                            temp = tCount
                            num = 0
                            card[num] = cbCardData[i]
                            num = num + 1
                            card[num] = cbCardData[j]
                            num = num + 1
                            for t = 0, 4 do
                                if t ~= i and t ~= j then
                                    card[num] = cbCardData[t]
                                    num = num + 1
                                end
                            end
                            if tCount == 0 then
                                break
                            end
                        end
                    end
                end --end for j
                if tCount == 0 then
                    break
                end
            end
        end --end for i
        for i = 2, 4 do
            local temp = card[i]
            for j = 0, 4 do
                if cbCardData[j] == temp then
                    self.m_cbCardShowFlag[index] = self.m_cbCardShowFlag[ndex] + math.pow(2, j)
                end
            end
        end
        return self:GetCardType_(temp)
    else--两张王绝对牛牛
        num = 0
        card[num] = 52
        num = num + 1
        for i = 0, 4 do
            if cbCardData[i] <= 51 then
                card[num] = cbCardData[i]
                num = num + 1
            end
        end
        card[num] = 53
        num = num + 1
        for i = 0, 2 do
            local temp = card[i]
            for j = 0, 4 do
                if cbCardData[j] == temp then
                    self.m_cbCardShowFlag[index] = self.m_cbCardShowFlag[index] + math.pow(2, j)
                end
            end
        end
        return G_CONSTANTS.CardValue_Ten
    end
    return G_CONSTANTS.CardValue_Nothing
end
function QznnDataMgr:GetCardArrType_(cbCardData, index)
    
    for i = 0, 4 do
        for j = i + 1, 4 do
            for k = j + 1, 4 do
                if (self:GetCardValue_(cbCardData[i]) +
                    self:GetCardValue_(cbCardData[j]) + 
                    self:GetCardValue_(cbCardData[k])) % 10 == 0
                then
                    local temp, num, card = 0, 0, {[0] = 0, 0, 0, 0, 0, }
                    for t = 0, 4 do
                        if t ~= i and t ~= j and t ~= k then
                            card[num] = cbCardData[t]
                            num = num + 1
                            temp = temp + self:GetCardValue_(cbCardData[t])
                        end
                    end
                    for t = 0, 4 do
                        if t == i or t == j or t == k then
                            card[num] = card[num] + cbCardData[t]
                            num = num + 1
                        end
                    end
                    self.m_cbCardShowFlag[index] = math.pow(2, i) + math.pow(2, j) + math.pow(2, k)
                    return self:GetCardType_(temp % 10)
                end
            end--end for k
        end--end for j
    end--end for i
    return G_CONSTANTS.CardValue_Nothing
end
function QznnDataMgr:GetCardType_(cbCount)
    if cbCount == 0 then
        return G_CONSTANTS.CardValue_Ten
    else
        return cbCount % 10
    end
end
function QznnDataMgr:GetCardValue_(cbCardData)
    if cbCardData % 13 >= 9 then
        return 10
    else
        return cbCardData % 13 + 1
    end
end
function QznnDataMgr:GetLogicValue_(cbCardData)
    if cbCardData == 52 then
        return 13
    elseif cbCardData == 53 then
        return 14
    else
        return cbCardData % 13
    end
end
--根据牛几设置客户端前三张牌为10的倍数
function QznnDataMgr:sortMyCardByNiu()
    local cbCardData = self.m_cbCardData[self.m_wMeChairID]
    local tempCardData = table.deepcopy(cbCardData)
    for i = 0, 4 do
        for j = i + 1, 4 do
            for k = j + 1, 4 do
                if (self:GetCardValue_(tempCardData[i]) +
                    self:GetCardValue_(tempCardData[j]) + 
                    self:GetCardValue_(tempCardData[k])) % 10 == 0
                then
                    --把i,j,k这三个索引的牌放到前三
                    self.m_cbCardData[self.m_wMeChairID] = {}
                    self.m_cbCardData[self.m_wMeChairID][0] = tempCardData[i]
                    self.m_cbCardData[self.m_wMeChairID][1] = tempCardData[j]
                    self.m_cbCardData[self.m_wMeChairID][2] = tempCardData[k]
                    for index = 0 , 4 do
                        if index ~= i and index ~= j and index ~= k then
                            table.insert(self.m_cbCardData[self.m_wMeChairID], tempCardData[index])
                        end
                    end

                    return
                end
            end--end for k
        end--end for j
    end--end for i
end
--set and get
function QznnDataMgr:setBaseScore(score) --底分
    self.m_llBaseScore = score
end
function QznnDataMgr:getBaseScore()
    return self.m_llBaseScore
end
function QznnDataMgr:setGameStatus(status) --状态
    self.m_cbGameStatus = status
end
function QznnDataMgr:getGameStatus()
    return self.m_cbGameStatus
end
function QznnDataMgr:setGameStartTime(time) --准备时间
    self.m_cbGameStartTime = time
end
function QznnDataMgr:getGameStartTime()
    return self.m_cbGameStartTime
end
function QznnDataMgr:setBankerTime(time) --抢庄时间
    self.m_cbBankerTime = time
end
function QznnDataMgr:getBankerTime()
    return self.m_cbBankerTime
end
function QznnDataMgr:setDownJettonTime(time) --下注时间
    self.m_cbDownJettonTime = time
end
function QznnDataMgr:getDownJettonTime()
    return self.m_cbDownJettonTime
end
function QznnDataMgr:setTakeCardTime(time) --配牌时间
    self.m_cbTakeCardTime = time
end
function QznnDataMgr:getTakeCardTime()
    return self.m_cbTakeCardTime
end
function QznnDataMgr:setBanker(banker) --庄家
    self.m_wBanker = banker
end
function QznnDataMgr:getBanker()
    return self.m_wBanker
end
function QznnDataMgr:setFinalCallTimes(times) --最终抢庄倍数
    self.m_cbFinalCallTimes = times
end
function QznnDataMgr:getFinalCallTimes()
    return self.m_cbFinalCallTimes
end
function QznnDataMgr:setMaxTypeMultiple(max) --最大赔率
    self.m_nMaxTypeMultiple = max
end
function QznnDataMgr:getMaxTypeMultiple()
    return self.m_nMaxTypeMultiple
end
--add
function QznnDataMgr:setCardData(id, index, value)
    self.m_cbCardData[id][index] = value
end
function QznnDataMgr:getCardData(i, j)
    return self.m_cbCardData[i][j]
end

function QznnDataMgr:getCardDatas(i)
    return self.m_cbCardData[i]
end

function QznnDataMgr:getCardType(id)
    return self.m_cbCardType[id]
end
function QznnDataMgr:setBankerTimes(id, times)
    self.m_iBankerTimes[id] = times
end
function QznnDataMgr:getBankerTimes(id)
    return self.m_iBankerTimes[id]
end
function QznnDataMgr:setBankerTimesChoose(id, choose)
    self.m_iBankerTimesChoose[id] = choose
end
function QznnDataMgr:getBankerTimesChoose(id)
    return self.m_iBankerTimesChoose[id]
end
function QznnDataMgr:setDownTimes(id, times)
    self.m_iDownTimes[id] = times
end
function QznnDataMgr:getDownTimes(id)
    return self.m_iDownTimes[id]
end
function QznnDataMgr:setDownTimesChoose(id, choose)
    self.m_iDownTimesChoose[id] = choose
end
function QznnDataMgr:getDownTimesChoose(id)
    return self.m_iDownTimesChoose[id]
end
function QznnDataMgr:setFinishCard(id, bFinish)
    self.m_bFinishCard[id] = bFinish
end
function QznnDataMgr:getFinishCard(id)
    return self.m_bFinishCard[id]
end
function QznnDataMgr:setResult(i, result)
    self.m_llResult[i] = result
end
function QznnDataMgr:getResult(i)
    if self.m_llResult[i] then
        return self.m_llResult[i]
    else
        return nil 
    end
end
function QznnDataMgr:getGameStatusFree()
    return self.m_cbGameStatus == QznnDataMgr.QZNN_T_FREE
end
function QznnDataMgr:getGameStatusCall()
    return self.m_cbGameStatus == QznnDataMgr.QZNN_T_CALL
end
function QznnDataMgr:getGameStatusDown()
    return self.m_cbGameStatus == QznnDataMgr.QZNN_T_DOWN
end
function QznnDataMgr:getGameStatusPlay()
    return self.m_cbGameStatus == QznnDataMgr.QZNN_T_PLAY
end
function QznnDataMgr:getGameStatusPlaying()
    return self.m_cbGameStatus > QznnDataMgr.QZNN_T_FREE
end
function QznnDataMgr:getCardShowFlag(chairID)
    return self.m_cbCardShowFlag[chairID]
end


--动画先行的变量
function QznnDataMgr:getLocaBankerTimes()
    return self.m_iLocaBankerTimes
end
function QznnDataMgr:setLocaBankerTimes(times)
    self.m_iLocaBankerTimes = times
end

function QznnDataMgr:getLocalDownTimes()
    return self.m_iLocalDownTimes
end
function QznnDataMgr:setLocalDownTimes(times)
    self.m_iLocalDownTimes = times
end

function QznnDataMgr:getLocalCardType()
    return self.m_iLocalCardType
end
function QznnDataMgr:setLocalCardType(types)
    self.m_iLocalCardType = types
end

return QznnDataMgr