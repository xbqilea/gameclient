module(..., package.seeall)

-- 进入游戏，等待开始， 发牌
CS_G2C_21Dot_GameState_Nty = 
{
	{ 1,	1, 'm_state'  		 	 ,  'UBYTE'						, 1		, '1-准备状态 2-下注状态 3-发牌状态 4-购买保险 5-要牌操作 6-结算'},
	{ 2,	1, 'm_minLimit'    	 	 ,  'UINT'       				, 1     , '下注下限' },
	{ 3,	1, 'm_maxLimit'    	 	 ,  'UINT'       				, 1     , '下注上限' },
	{ 4,	1, 'm_opLimitTime'  	 ,  'UINT'						, 1		, '操作时间, 如果是下注状态:超过时间(10s),自动下最小注, 如果是要牌状态:超过时间,自动停牌'},
	{ 5,	1, 'm_allPlayers'  		 ,  'Pst21DotPlayerInfo'		, 5		, '玩家信息'},
	--{ 6,	1, 'm_betInfo'			 ,  'Pst21DotBetInfo'			, 6		, '筹码信息'},
	{ 6,	1, 'm_bankerInfo'		 ,  'Pst21DotPart'				, 1		, '庄家牌信息'},
	{ 7,	1, 'm_leftTime'  		 ,  'UINT'						, 1		, '开始准备状态剩余时间'},
	{ 8,	1, 'm_lastBet'  		 ,  'UINT'						, 1		, '续压金额'},
	{ 9,	1, 'm_opIdx'        	 ,  'UINT'       				, 1     , '操作序列, 0表示庄家 座位号1-5 + 分牌, 对应数值为10,11,20,21... ' },
	{ 10,	1, 'm_canCut2two'        ,  'UBYTE'       				, 1     , '轮到自己操作时有效, 是否能够分牌 1-是 0-否' },
	{ 11,	1, 'm_canDouble'       	 ,  'UBYTE'       				, 1     , '轮到自己操作时有效, 是否双倍 1-是 0-否' },
	{ 12,	1, 'm_recordId'       	 ,  'STRING'       				, 1     , '牌局编号' },
}

--下注
CS_C2G_21Dot_Bet_Req = 
{
	{ 1,	1, 'm_chairId'			 , 'UBYTE'							, 1		, '操作序列, 0表示庄家 座位号1-5 + 分牌, 对应数值为10,11,20,21... '},
	{ 2,	1, 'm_betValue'			 , 'INT'							, 1		, '下注额'},
}

--续压
CS_C2G_21Dot_ContinueLastBet_Req = 
{
	{ 1,	1, 'm_chairId'			 , 'UBYTE'							, 1		, '操作序列, 0表示庄家 座位号1-5 + 分牌, 对应数值为10,11,20,21... '},
}

--下注结果通知, 如果下注失败(m_ret 为错误码), 只发给请求玩家, 成功则广播给所有玩家
CS_G2C_21Dot_Bet_Nty = 
{
	{ 1,	1, 'm_ret'			 	 , 'INT'							, 1		, '准备结果'},
	{ 2,	1, 'm_accountId'		 , 'UINT'							, 1		, '谁下的注'},
	{ 3,	1, 'm_chairId'			 , 'UBYTE'							, 1		, '操作序列, 0表示庄家 座位号1-5 + 分牌, 对应数值为10,11,20,21... '},
	{ 4,	1, 'm_betValue'			 , 'INT'							, 1		, '下注额'},
	{ 5,	1, 'm_curScore'			 , 'UINT'							, 1		, '当前金币'},
	{ 6,	1, 'm_isFinish'			 , 'UBYTE'							, 1		, '下注是否结束, 1-结束 0-未结束, 已经没有空位置下注'},
}

--结束下注
CS_C2G_21Dot_FinishBet_Req = 
{

}

--结束下注通知
CS_G2C_21Dot_FinishBet_Nty = 
{
	{ 1,	1, 'm_ret'			 	 , 'INT'							, 1		, '准备结果'},
	{ 2,	1, 'm_accountId'		 , 'UINT'							, 1		, '谁下的注'},
	{ 3,	1, 'm_chairId'			 , 'UBYTE'							, 1		, '操作序列, 0表示庄家 座位号1-5 + 分牌, 对应数值为10,11,20,21... '},
}


--购买保险
CS_C2G_21Dot_Insure_Req = 
{
{ 1,	1, 'm_chairId'	, 'UBYTE'	, 1	, '操作序列, 0表示庄家 座位号1-5 + 分牌, 对应数值为10,11,20,21... '},
{ 2,	1, 'm_opType'	, 'UBYTE'	, 1	, '操作类型 1-购买保险 2-不购买保险'},
}

--购买保险响应
CS_G2C_21Dot_Insure_Nty = 
{
	{ 1,	1, 'm_ret'			 	 , 'INT'							, 1		, '准备结果'},
	{ 2,	1, 'm_accountId'		 , 'UINT'							, 1		, '谁下的注'},
	{ 3,	1, 'm_chairId'			 , 'UBYTE'							, 1		, '操作序列, 0表示庄家 座位号1-5 + 分牌, 对应数值为10,11,20,21... '},
	{ 4,	1, 'm_opType'			 , 'UBYTE'							, 1		, '操作类型 1-购买保险 2-不购买保险'},
	{ 5,	1, 'm_betValue'			 , 'INT'							, 1		, '下注额'},
	{ 6,	1, 'm_curScore'			 , 'UINT'							, 1		, '当前金币'},
}

--分牌
CS_C2G_21Dot_Cut2Two_Req = 
{
	{ 1,	1, 'm_chairId'			 , 'UBYTE'							, 1		, '操作序列, 0表示庄家 座位号1-5 + 分牌, 对应数值为10,11,20,21... '},
}

--分牌响应
CS_G2C_21Dot_Cut2Two_Nty = 
{
	{ 1,	1, 'm_ret'			 	 , 'INT'							, 1		, '准备结果'},
	{ 2,	1, 'm_accountId'		 , 'UINT'							, 1		, '谁下的注'},
	{ 3,	1, 'm_chairId'			 , 'UBYTE'							, 1		, '操作序列, 0表示庄家 座位号1-5 + 分牌, 对应数值为10,11,20,21... '},
	{ 4,	1, 'm_parts'			 , 'Pst21DotPart'					, 2		, '分组'},
	{ 5,	1, 'm_curScore'			 , 'UINT'							, 1		, '当前金币'},
}

--停牌
CS_C2G_21Dot_StopSendCard_Req = 
{
	{ 1,	1, 'm_chairId'			 , 'UBYTE'							, 1		, '操作序列, 0表示庄家 座位号1-5 + 分牌, 对应数值为10,11,20,21... '},
}

--停牌
CS_G2C_21Dot_StopSendCard_Nty = 
{
	{ 1,	1, 'm_ret'			 	 , 'INT'							, 1		, '准备结果'},
	{ 2,	1, 'm_accountId'		 , 'UINT'							, 1		, '谁下的注'},
	{ 3,	1, 'm_chairId'			 , 'UBYTE'							, 1		, '操作序列, 0表示庄家 座位号1-5 + 分牌, 对应数值为10,11,20,21... '},
	{ 4,	1, 'm_points'		 	 , 'UBYTE'							, 2		, '点数, 如果停牌, 取最大点数'},
	{ 5,	1, 'm_dimType'		 	 , 'UBYTE'							, 1		, '牌型 0-操作未结束 1-爆牌 2-低于21点 3-21点 4-五小龙 5-黑杰克'},
}

--加倍
CS_C2G_21Dot_DoubleBet_Req = 
{
	{ 1,	1, 'm_chairId'			 , 'UBYTE'							, 1		, '操作序列, 0表示庄家 座位号1-5 + 分牌, 对应数值为10,11,20,21... '},
}

-- 加倍响应
CS_G2C_21Dot_DoubleBet_Nty = 
{
	{ 1,	1, 'm_ret'			 	 , 'INT'							, 1		, '准备结果'},
	{ 2,	1, 'm_accountId'		 , 'UINT'							, 1		, '谁下的注'},
	{ 3,	1, 'm_chairId'			 , 'UBYTE'							, 1		, '操作序列, 0表示庄家 座位号1-5 + 分牌, 对应数值为10,11,20,21... '},
	{ 4,	1, 'm_newCard'			 , 'INT'							, 1		, '新发牌,牌值'},
	{ 5,	1, 'm_betValue'			 , 'INT'							, 1		, '下注额'},
	{ 6,	1, 'm_points'			 , 'UBYTE'							, 2		, '加倍后自动停牌, 如有两个点数, 取最大点数, 这里只有一个点数'},
	{ 7,	1, 'm_dimType'		 	 , 'UBYTE'							, 1		, '牌型 0-操作未结束 1-爆牌 2-低于21点 3-21点 4-五小龙 5-黑杰克'},
	{ 8,	1, 'm_curScore'			 , 'UINT'							, 1		, '当前金币'},
}


--要牌
CS_C2G_21Dot_MoreCard_Req = 
{
	{ 1,	1, 'm_chairId'			 , 'UBYTE'							, 1		, '操作序列, 0表示庄家 座位号1-5 + 分牌, 对应数值为10,11,20,21... '},
}

--要牌响应
CS_G2C_21Dot_MoreCard_Nty = 
{
	{ 1,	1, 'm_ret'			 	 , 'INT'							, 1		, '准备结果'},
	{ 2,	1, 'm_accountId'		 , 'UINT'							, 1		, '谁操作的'},
	{ 3,	1, 'm_chairId'			 , 'UBYTE'							, 1		, '操作序列, 0表示庄家 座位号1-5 + 分牌, 对应数值为10,11,20,21... '},
	{ 4,	1, 'm_newCard'			 , 'INT'							, 1		, '新发牌,牌值'},
	{ 5,	1, 'm_points'			 , 'UBYTE'							, 2		, '点数'},
	{ 6,	1, 'm_dimType'		 	 , 'UBYTE'							, 1		, '牌型 0-操作未结束 1-爆牌 2-低于21点 3-21点 4-五小龙 5-黑杰克'},
}

--庄家翻牌, 要牌, 停牌操作
CS_G2C_21Dot_BankOp_Nty = 
{
	{ 1,	1, 'm_opType'			 , 'UBYTE'							, 1		, '操作类型 1-翻牌, 2-要牌 3-停牌'},
	{ 2,	1, 'm_newCard'			 , 'UBYTE'							, 1		, '新发牌,牌值, 若未停牌, 则为0'},
	{ 3,	1, 'm_points'			 , 'UBYTE'							, 2		, '点数'},
	{ 4,	1, 'm_dimType'		 	 , 'UBYTE'							, 1		, '牌型 0-操作未结束 1-爆牌 2-低于21点 3-21点 4-五小龙 5-黑杰克'},
}

--[[
CS_G2C_21Dot_Enter_Nty = 
{
	{ 1,	1, 'm_playerInfo'  		 ,  'PstGlodenFlowerPlayerInfo'	, 1		, '玩家信息'},
}

CS_G2C_21Dot_Leave_Nty = 
{
	{ 1,	1, 'm_accountId'  		 ,  'UINT'						, 1		, '玩家ID'},
	{ 2,	1, 'm_chairId'       	 ,  'INT'       				, 1     , '椅子号' },
}
--]]


--通知所有玩家游戏进入start状态,进入该状态, 即个玩家开始下注(10s)
CS_G2C_21Dot_Begin_Nty = 
{
	{ 1,	1, 'm_opTime'    	 	 ,  'INT'       		, 1     , '操作时间' },
	{ 2,	1, 'm_recordId'       	 ,  'STRING'       		, 1     , '牌局编号' },
}

--发牌阶段，不发实际牌，只告诉客户端在几个位置上发牌, 开始发两张牌
CS_G2C_21Dot_SendCard_Nty = 
{
	{ 1,	1, 'm_vecCards'    	 	 ,  'Pst21DotCards'       		, 6     , '牌面信息' },
}

--购买保险状态
CS_G2C_21Dot_BuyInsure_Nty = 
{
	{ 1,	1, 'm_opTime'    	 	 ,  'INT'       		, 1     , '操作时间' },
}

--通知玩家下注
CS_G2C_21Dot_Action_Nty = 
{
	{ 1,	1, 'm_accountId'  		 ,  'UINT'						, 1		, '玩家ID'},
	{ 2,	1, 'm_chairId'       	 ,  'BYTE'       				, 1     , '操作序列, 0表示庄家 座位号1-5 + 分牌, 对应数值为10,11,20,21... ' },
	-- { 3,	1, 'm_canBuyInsure'      ,  'UBYTE'       				, 1     , '是否能购买保险 1-是 0-否' },
	{ 3,	1, 'm_canCut2two'        ,  'UBYTE'       				, 1     , '是否能够分牌 1-是 0-否' },
	{ 4,	1, 'm_canDouble'       	 ,  'UBYTE'       				, 1     , '是否双倍 1-是 0-否' },
	{ 5,	1, 'm_opTime'    	 	 ,  'INT'       				, 1     , '操作时间' },
}

--通知所有玩家游戏进入结算处状态
CS_G2C_21Dot_GameEnd_Nty = 
{
	{ 1,	1		, 'm_allResult'      	,  		'Pst21DotUserTotalEndInfo'    	, 5   , '所有玩家结算结果，包括自己的' },
}

SS_M2G_21Dot_GameCreate_Req =
{
	{ 1		, 1		, 'm_strGameObjId'		,		'STRING'					, 1		, '游戏对象ID'},
	{ 2		, 1		, 'm_vecAccounts'		,		'Pst21DotSyncData'			, 5		, '玩家数据'},
}

SS_G2M_21Dot_GameCreate_Ack =
{
	{ 1		, 1		, 'm_strGameObjId'		,		'STRING'					, 1		, '游戏对象ID'},
	{ 2		, 1		, 'm_nResult'			,		'UINT'						, 1		, '结果 1-成功 0-失败'},
	{ 3		, 1		, 'm_vecAccounts'		,		'PstOperateRes'				, 5		, '玩家初始化结果数据'},
}

SS_M2G_21Dot_GameResult_Nty =
{
	{ 1		, 1		, 'm_strGameObjId'		,		'STRING'					, 1		, '游戏对象ID'},
	{ 2		, 1		, 'm_vecAccounts'		,		'Pst21DotBalanceData'		, 5		, '玩家数据'},
}

CS_M2C_21Dot_Exit_Nty =
{
	{ 1		, 1		, 'm_type'				,		'UBYTE'	, 1		, '退出， 0-正常结束 1-分配游戏服失败 2-同步游戏服失败'},
}

CS_G2C_21Dot_Kick_Nty =
{
	{ 1		, 1		, 'm_type'				,		'UBYTE'	, 1		, '踢人 1:金币低于每局入场限制被踢出 2:超时未准备踢出'},
}

CS_M2C_21Dot_StartMate_Nty =
{
	{ 1		, 1		, 'm_type'					, 'UBYTE'				, 1		, '类型，0：匹配中，1:已匹配'},
	{ 2		, 1		, 'm_faceId'				, 'UINT'				, 1 	, '头像ID'},
	{ 3		, 1		, 'm_nickname'				, 'STRING'				, 1 	, '昵称'},
	{ 4		, 1		, 'm_goldCoin'				, 'UINT'				, 1 	, '金币'},
}

CS_C2M_21Dot_StartMate_Req =
{
}

CS_M2C_21Dot_StartMate_Ack =
{
	{ 1		, 1		, 'm_result'				, 'INT'					, 1		, '开始匹配结果'},
}

CS_C2M_21Dot_Exit_Req =
{
}

CS_M2C_21Dot_Exit_Ack =
{
	{ 1		, 1		, 'm_result'				, 'INT'					, 1		, '退出结果'},
}
CS_C2G_21Dot_Background_Req =
{
	{ 1,	1, 'm_type'			 , 'INT'						, 1		, '1-切到后台 2-切回游戏'},
}

CS_G2C_21Dot_Background_Ack =
{
	{ 1,	1, 'm_type'			 , 'INT'						, 1		, '1-切到后台 2-切回游戏'},
	{ 2,	1, 'm_ret'			 , 'INT'						, 1		, '结果 0-表示成功  <0 表示失败'},
}