--计算相关
local blackjack_util = {}


local room_type_res = {
   [1001] = "common_ptc.png",
   [1002] = "common_xzc.png",
   [1003] = "common_lbc.png",
   [1004] = "common_fhc.png",
}

function blackjack_util:getRoomTypeRes()
    return room_type_res[PlayerData.roomId]
end

function blackjack_util:get_jetton_res( coin )
    local bet = {1,10,50,100,500}
    local jettons_res = {
        [1] = "point21_cm_1.png",
        [2] = "point21_cm_2.png",
        [3] = "point21_cm_3.png",
        [4] = "point21_cm_4.png",
        [5] = "point21_cm_5.png",
    }

    local res = jettons_res[1]
    for k,v in pairs(bet) do
        if coin == common_util.tansCoin(v) then
            res = jettons_res[k]
            return res
        end
    end
end


--传入当前区域总金币数,获取若干小面额金币数(筹码面额为1,10,50,100,500)
function blackjack_util:getJettons(coin)    
    local bet = {1,10,50,100,500}
    -- print("coin=",coin)
    return common_util.getJettonsCows(coin, bet)
end

--闹钟倒计时
-- function blackjack_util.pay_sound_clock( sec )
--     if sec == 2 or sec == 3 then
--         AudioManager:playSound("Redblack>countdown")
--     elseif sec == 1 then
--         AudioManager:playSound("Redblack>countdown2")
--     end
-- end

--
--获取筹码节点
local jettons_node = {}
local clone_index = 1
function blackjack_util.get_jettion(clone_node)
    local is_repeat = false
    local node = jettons_node[clone_index]
    if node then
        is_repeat = true
    else
        node = clone_node:clone()
        table.insert(jettons_node, node)
    end
    clone_index = clone_index + 1
    return node, is_repeat
end

function blackjack_util.getCardColor(idx)
    local tmp_color = math.floor(idx/16)
    local num = idx%16
    if tmp_color > 4 then
        num = num + (tmp_color-4)
    end
     print("getCardColor,idx=",idx,num,tmp_color)
    if num == 14 then
        num = 1
        -- print("14变为1了")
    end
    return {size = num, color = tmp_color+1}
end

function blackjack_util.getAllCardColor(data)
    local tmp = {}
    for i , v in ipairs(data) do
        table.insert(tmp,blackjack_util.getCardColor(v))
    end
    return tmp
end

function blackjack_util.throw_jetton(node, epos, time, callback)
    local time = time or common_util.random_time(500, 800)

    local ac = cc.MoveTo:create(time, cc.p(epos.x, epos.y))
    ac = cc.EaseExponentialOut:create(ac)

    -- local scale = cc.ScaleTo:create(time/2, scale)

    -- local angle = common_util.random_time(-180000, 180000)
    -- local rota = cc.RotateTo:create(1.2, angle)
    -- rota = cc.EaseExponentialOut:create(rota)

    -- ac = cc.Spawn:create(ac, scale, rota)

    local callfun = cc.CallFunc:create(function ( )
        if callback then
            callback(node)
        end
    end)


    ac = cc.Sequence:create(ac, callfun)
    node:runAction(ac)
end
return blackjack_util