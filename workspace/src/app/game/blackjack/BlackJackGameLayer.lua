local HNLayer= require("src.app.newHall.HNLayer")
local blackjack_util = require("app.game.blackjack.blackjack_util")
local GameRecordLayer= require("src.app.newHall.childLayer.GameRecordLayer")
local GameSetLayer= require("src.app.newHall.childLayer.SetLayer") 
local ChatLayer = require("src.app.newHall.chat.ChatLayer") 
local BlackJackGameLayer = class("BlackJackGameLayer", function()
    return HNLayer.new();
end)
local scheduler = require("framework.scheduler") 
local btn_node =  import(".btn_node") 

local MY_POS = 3
local ROOM_STATE_READY          = 1   --准备
local ROOM_STATE_BET            = 2   --押注
local ROOM_STATE_SENDCARD       = 3   --发牌操作
local ROOM_STATE_SAFE           = 4   --保险操作(不一定有)
local ROOM_STATE_PLAYER_LOOP    = 5   --玩家控牌操作：要牌，分牌，双倍，停牌
local ROOM_STATE_RESULT         = 6   --结算

local LOOP_NEED                  = 1   --要牌
local LOOP_DOUBLE               = 2   --双倍
local LOOP_SPLIT                = 3   --分牌
local LOOP_STOP                 = 4   --停牌
local LOOP_STAKE_STOP           = 5   --下注完成

local BET_MODE = 1              --下注操作
local CARD_MODE = 2             --牌的模式
local SAFE_MODE = 3            --保险模式
local WAIT_MODE = 4             --等待操作

function BlackJackGameLayer:ctor( ) 
    self:init()
    self:setupViews()
end

function BlackJackGameLayer:init()
    self.lasttime_bet_coin = 0
    self.m_MaxBet = 100
    self.select_pos = 30
    self.cur_safe_pos = 30
    self.all_player_panel1 = {}
    self.all_img_xiazhuqu = {}
    self.sch_all = {}
    self.chip_pos = {}                  --每个位置的筹码缓存
    self.split_list = {}                --key:pos 位置pos是否分牌
    self.chip_offset_y = {}
    self.select_panel_visible = false
    self.self_bet = false
    self.is_bet_over = false
    self.is_req_loop_bet_stop = false   --是否有请求下注完成
    self.req_loop_stop = {}             --是否有请求停牌  
   
    self.player_bet_list = {}   --记录保存下注的数据
    self.player_operate_list = {} --每个下注位是否操作完成
    self.no_bet_list_pos = {}   --空位还可下注的区域
    self.all_node_card1 = {}     --存牌
    self.zj_node_card = {}
    self.buy_safe_list = {}     --购买的保险列表
    self.all_card_value = {}
    self.play_21dian = {}       --播放了21点音效
end

function BlackJackGameLayer:addEvent()
    print("adddddddddddddddddddddddddddddddddddddddddddddddddddddd")
--    addMsgCallBack(self,BLACKJACK_ADD_PLAYER, handler(self, self.eventAddPlayer))  
--    addMsgCallBack(self,BLACKJACK_ROOM_BET, handler(self, self.eventRoomBet))
--    addMsgCallBack(self,BLACKJACK_ROOM_RESULT, handler(self, self.eventRoomResult))
--    addMsgCallBack(self,BLACKJACK_ROOM_STATE, handler(self, self.eventRoomState))  
--    addMsgCallBack(self,BLACKJACK_PLAYER_LOOP, handler(self, self.eventPlayerLoop))
--    addMsgCallBack(self,BLACKJACK_SEND_CARD, handler(self, self.eventSendCard))
--    addMsgCallBack(self,BLACKJACK_SAFE_RESULT, handler(self, self.eventSafeResult)) 
end 
function BlackJackGameLayer:onTouchButton(sender,eventType)
  
    if eventType == ccui.TouchEventType.ended then
            local GameRecordLayer = GameRecordLayer.new(2)
        self:addChild(GameRecordLayer)  
            ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", {205})
    end 
end
function BlackJackGameLayer:setupViews( )
    --AudioManager:playBgMusic("BlackJack>BGM")
    g_AudioPlayer:playMusic("subgame/point21/sound/BGM.mp3",true)
        local gameTableNode = UIAdapter:createNode("subgame/point21/point21.csb");
         local center = gameTableNode:getChildByName("Layer") 
     local diffY = (display.size.height - 750) / 2
    gameTableNode:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
	self:addChild(gameTableNode); 
     UIAdapter:praseNode(gameTableNode,self)

   --  UIAdapter:adapter(gameTableNode,handler(self, self.onTouchCallback))  


    print("onEnter=")
   -- self:noticeView()
    self:initMain()
    self:initMenu()
    self:initHeGuan()
    self:initPlayerPanel()
    self:initBtn() 
    self:initOther()
  --  self:showBtnMode(BET_MODE)
  --  self:updatenBtnMaxCoin()
    self:setParentBtnPanelVisible(false)
    self:addEvent()



--    self.room_state = g_GameController:getRoomState()
   -- self.is_recover = self.room_state.game_step > ROOM_STATE_READY
--    print("g_GameController.is_recover=",g_GameController.is_recover)
--    if g_GameController.is_recover == true then
--        --中途进来的
--        print("中途进来的")
--        if self.room_state and self.room_state.game_step == ROOM_STATE_RESULT then
--            self:showTips("本轮已结束，请等待下一轮开始")
--            return
--        end
--        if self.room_state and self.room_state.game_step == ROOM_STATE_SAFE then
--            --保险操作
--            local cards = g_GameController:getCardData(0)
--            if cards == nil then --服务器没有下发庄家牌
--                local info = {
--                    cards = {
--                        math.random(11,14),
--                    }
--                }
--                self:setCardData(info)
--            end
--        end
--        local all_player_data = g_GameController:getAllPlayerData()
--        for k , player in pairs(all_player_data) do
--            self:eventAddPlayer({pid=player.pid})
--        end
--        self:recoverGame()
--    end
--    g_GameController.is_recover = false
--    if self.room_state ~= nil then
--        print("还没有初始化完成，就收到服务器的房间状态，初始化完了手动调用")
--        if self.room_state.game_step == ROOM_STATE_READY then
--            self:showTips(TipConfig[220])
--        end
--        self:eventRoomState()
--    end
end

function BlackJackGameLayer:initHeGuan()
    --荷官
    self.node_heguan = self:seekNodeByName("node_heguan")
    local path = "common/heguang.csb"
    local ac = cc.CSLoader:createTimeline(path)
    self.node_heguan:runAction(ac)
    self.node_heguan.ac = ac
    local function heguang_callback()
        self.node_heguan.acting = false
        local ac_info = self.node_heguan.ac:getAnimationInfo("animation0")
        self.node_heguan.ac:gotoFrameAndPause(ac_info.startIndex)
        self.node_heguan.ac:gotoFrameAndPlay(ac_info.startIndex,ac_info.endIndex,true)
    end

    local ac_info = self.node_heguan.ac:getAnimationInfo("animation0")
    self.node_heguan.ac:gotoFrameAndPlay(0,300,true)

--    local btn_heguan = cc.uiloader:seekNodeByName(self.node_heguan, "panel")
--    local function btn_heguan_callback(sender, eventType)
--        if eventType == ccui.TouchEventType.ended then
--            if self.node_heguan.acting then
--                return 
--            end
--            self.node_heguan.acting = true
--            local ac_info = self.node_heguan.ac:getAnimationInfo("animation1")
--            self.node_heguan.ac:gotoFrameAndPause(ac_info.startIndex)
--            self.node_heguan.ac:gotoFrameAndPlay(ac_info.startIndex,ac_info.endIndex,false)
--            self.node_heguan.ac:clearLastFrameCallFunc()
--            self.node_heguan.ac:setLastFrameCallFunc(heguang_callback)
--        end
--    end
    -- btn_heguan:addTouchEventListener(btn_heguan_callback)
end

function BlackJackGameLayer:seekNodeByName(name)
    return self[name]
end
function BlackJackGameLayer:initMain()
    --发牌器
    self.img_fapaiqi = self:seekNodeByName("img_fapaiqi")
    self.pnl_desk = self:seekNodeByName("pnl_desk")
    self.pnl_desk:setLocalZOrder(4)
end

function BlackJackGameLayer:initMenu()
     -- 菜单按钮
     local game_menu = UIAdapter:createNode("game_room/game_menu.csb")
     game_menu:setLocalZOrder(19)
     game_menu:setVisible(false)
     self.game_menu = game_menu
     self.pnl_desk:addChild(game_menu)

     for k, v in pairs(self.game_menu:getChildren()) do
        print(k, v, v:getName())
     end

     local func_menu = function(event, event_type)
        if (event_type ~= 2) then return end
        local n_rotation = event:getRotation()
        if (n_rotation == 0) then
            event:setRotation(180)
            self.game_menu:setVisible(true)
        else
            event:setRotation(0)
            self.game_menu:setVisible(false)
        end
     end
     local func_chat = function(event, event_type)
         if (event_type ~= 2) then return end
         local ChatLayer = ChatLayer.new()
         self:addChild(ChatLayer);
     end
     local btn_menu =  self:seekNodeByName("btn_menu")
     btn_menu:setLocalZOrder(20)
     btn_menu:addTouchEventListener(func_menu)
      local btn_chat =  self:seekNodeByName("btn_chat")
     btn_chat:setLocalZOrder(20)
     btn_chat:addTouchEventListener(func_chat)
    
    -- 其他按钮
    local t_callfunc = {
        record = function(event, event_type)
            if (event_type ~= 2) then return end
            local GameRecordLayer = GameRecordLayer.new(2,205)
            self:addChild(GameRecordLayer)  
            ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", {205})
        end,
        setting = function(event, event_type)
            if (event_type ~= 2) then return end
            local layer = GameSetLayer.new(205);
		    self:addChild(layer);
            layer:setScale(1/display.scaleX, 1/display.scaleY);  
        end,
        help = function(event, event_type)
            if (event_type ~= 2) then return end
            self:showRule()
        end,
        exit = function(event, event_type)
            if (event_type ~= 2) then return end
            if self.btn_ready:isVisible() then
                g_GameController:reqQuitGame()
            else
                TOAST("您正在游戏中，不能退出！")
            end
        end,
        btn_panel_backMenu = function(event, event_type)
            if (event_type ~= 2) then return end
            btn_menu:setRotation(0)
            self.game_menu:setVisible(false)
        end,
    }
     local btn_exit =  self:seekNodeByName("btn_exit")
     btn_exit:setLocalZOrder(20)
     btn_exit:addTouchEventListener(t_callfunc.exit)
     local panel_backMenu = self.game_menu
     local btn_panel_backMenu = panel_backMenu:getChildByName("btn_panel_backMenu")
     btn_panel_backMenu:addTouchEventListener(t_callfunc.btn_panel_backMenu)
     local ImageView_2 = panel_backMenu:getChildByName("panel_backMenu_node"):getChildByName("ImageView_2")
     local auto_btn_more = ImageView_2:getChildByName("auto_btn_more")
     local button_setting = ImageView_2:getChildByName("button_setting")
     local button_helpX = ImageView_2:getChildByName("button_helpX")
     auto_btn_more:addTouchEventListener(t_callfunc.record)
     button_setting:addTouchEventListener(t_callfunc.setting)
     button_helpX:addTouchEventListener(t_callfunc.help)
end

function BlackJackGameLayer:initPlayerPanel()
    local path_fenshu = "subgame/point21/point21_fenshu.csb"
    for k = 1 , 5 do
        local panel = self:seekNodeByName("player_panel" .. k)
        panel.label_name = cc.uiloader:seekNodeByName(panel, "label_name")
        panel.label_coin = cc.uiloader:seekNodeByName(panel, "label_coin")
        panel.node_fenshu = cc.uiloader:seekNodeByName(panel, "node_fenshu")    --得分
        panel.img_head_icon = cc.uiloader:seekNodeByName(panel, "img_head_icon")
        panel.label_index = cc.uiloader:seekNodeByName(panel, "label_index")    --索引
         panel.imgShunxu = cc.uiloader:seekNodeByName(panel, "imgShunxu_"..k)    --索引
        panel.imgShunxu:setVisible(false)
        panel.player_panel = cc.uiloader:seekNodeByName(panel, "player_panel")
        local node = cc.uiloader:load(path_fenshu)
        panel.node_fenshu:addChild(node)
        local ac = cc.CSLoader:createTimeline(path_fenshu)
        node:runAction(ac)
        node.ac = ac

        node.img_lose = cc.uiloader:seekNodeByName(panel, "img_lose")
        node.img_lose.txt = cc.uiloader:seekNodeByName(node.img_lose, "txt")
        node.img_lose:setVisible(false)

        node.img_win = cc.uiloader:seekNodeByName(panel, "img_win")
        node.img_win.txt = cc.uiloader:seekNodeByName(node.img_win, "txt")
        node.img_win:setVisible(false)
        panel.node_fenshu.node = node

        local img_name = cc.uiloader:seekNodeByName(panel, "img_name")
        img_name:setLocalZOrder(1)


        --下注光圈
        local img_light = self:seekNodeByName("img_guang" .. k)
        img_light.txt = cc.uiloader:seekNodeByName(img_light, "txt")
        img_light.img_name = self:seekNodeByName("imgName" .. k)
        img_light.img_name.label_name = self:seekNodeByName("txtName" .. k)
        img_light:setVisible(false)
        img_light.img_name:setVisible(false)
        img_light.index = k
        img_light:addTouchEventListener(handler(self, self.onSelectPos))
        img_light:setVisible(false)

        panel:setVisible(false)
        self.all_player_panel1[k] = panel 
        --下注区域
        local img_xiazhuqu = self:seekNodeByName("img_xiazhuqu" .. k)

        --下注金额背景
        local img_bet_coin = self:seekNodeByName("img_bet_coin" .. k)
        local label_bet_coin = cc.uiloader:seekNodeByName(img_bet_coin, "label_bet_coin" .. k)
        img_bet_coin.label_bet_coin = label_bet_coin
        img_bet_coin:setVisible(false)

        --提示
        local img_tips = cc.uiloader:seekNodeByName(img_xiazhuqu, "img_tips")
        if img_tips then
            img_tips.txt = cc.uiloader:seekNodeByName(img_tips, "txt")
            img_tips:setVisible(false)
        end
        --牌节点
        local node_card = cc.uiloader:seekNodeByName(self.pnl_desk, "node_card" .. k)
        node_card.img_card_value = {}
        local panel_card_value = cc.uiloader:seekNodeByName(node_card, "panel_card_value")
        node_card.img_card_value[1] = cc.uiloader:seekNodeByName(panel_card_value, "img_card_value1")
        node_card.img_card_value[1].txt = cc.uiloader:seekNodeByName(panel_card_value, "txt")
        node_card.img_card_value[1]:setVisible(false)
        node_card.img_card_value[1].txt:setVisible(false)

        node_card.img_card_value[2] = cc.uiloader:seekNodeByName(node_card, "img_card_value2")
        node_card.img_card_value[2].txt = cc.uiloader:seekNodeByName(node_card.img_card_value[2], "txt")
        node_card.img_card_value[2]:setVisible(false)
        node_card.img_card_value[2].txt:setVisible(false)
        node_card:setVisible(true)

        --双倍
        node_card.img_double = {}
        node_card.img_double[1] = cc.uiloader:seekNodeByName(node_card, "img_double1")
        node_card.img_double[2] = cc.uiloader:seekNodeByName(node_card, "img_double2")
        node_card.img_double[1]:setVisible(false)
        node_card.img_double[2]:setVisible(false)
        local x,y = node_card.img_double[1]:getPosition()
        node_card.img_double[1].pos = cc.p(x,y)
        local x,y = node_card.img_double[2]:getPosition()
        node_card.img_double[2].pos = cc.p(x,y)


        --爆牌
        node_card.img_baozha = {}
        node_card.img_baozha[1] = cc.uiloader:seekNodeByName(node_card, "img_baozha1")
        node_card.img_baozha[2] = cc.uiloader:seekNodeByName(node_card, "img_baozha2")
        node_card.img_baozha[1]:setVisible(false)
        node_card.img_baozha[2]:setVisible(false)
        local x,y = node_card.img_baozha[1]:getPosition()
        node_card.img_baozha[1].pos = cc.p(x,y)
        local x,y = node_card.img_baozha[2]:getPosition()
        node_card.img_baozha[2].pos = cc.p(x,y)

        --筹码节点位置
        local node_coin_pos = self:seekNodeByName("node_coin_pos" .. k)

        img_xiazhuqu.img_light = img_light
        img_xiazhuqu.node_card = node_card
        img_xiazhuqu.img_tips = img_tips
        img_xiazhuqu.img_bet_coin = img_bet_coin
        img_xiazhuqu.node_coin_pos = node_coin_pos

        local coin_pos = self:seekNodeByName("coin_pos" .. k)
        img_xiazhuqu.coin_pos = coin_pos


        self.all_img_xiazhuqu[k] = img_xiazhuqu
    end
    self.node_coin_pos_zj = self:seekNodeByName("node_coin_pos_zj")
    self.node_card_zj = self:seekNodeByName("node_card_zj")
    self.node_card_zj.img_card_value = self:seekNodeByName("img_card_value_zj")
    self.node_card_zj.img_card_value.txt = cc.uiloader:seekNodeByName(self.node_card_zj.img_card_value, "txt")
    self.node_card_zj.img_card_value:setVisible(false)
    self.node_card_zj.img_baozha = self:seekNodeByName("img_baozha_zj")
    self.node_card_zj.img_baozha:setVisible(false)

    local all_node_card = self:seekNodeByName("all_node_card")
    all_node_card:setLocalZOrder(1)
end

function BlackJackGameLayer:initBtn()
    self.parent_btn_panel   = self:seekNodeByName("parent_btn_panel")     --按钮的父节点
    self.parent_btn_panel:setLocalZOrder(3)
    --分牌
    self.btn_split          = self:seekNodeByName("btn_split")       
    self.btn_split.img_mask = cc.uiloader:seekNodeByName(self.btn_split, "img_mask")       --分牌
    self.btn_split.img_mask:setVisible(false)

    --双倍
    self.btn_double         = self:seekNodeByName("btn_double")     
    self.btn_double.img_mask= cc.uiloader:seekNodeByName(self.btn_double, "img_mask")
    self.btn_double.img_mask:setVisible(false)

    --下注
    self.btn_bet            = self:seekNodeByName("btn_bet")           --下注
    self.btn_bet.txt        = cc.uiloader:seekNodeByName(self.btn_bet, "txt")
    self.btn_bet.img_mask   = cc.uiloader:seekNodeByName(self.btn_bet, "img_mask")
    self.btn_bet.img_mask:setVisible(false)

    --要牌
    self.btn_need_card      = self:seekNodeByName("btn_need_card") --要牌
    self.btn_need_card.txt  = cc.uiloader:seekNodeByName(self.btn_need_card, "txt")
    self.btn_need_card.img_mask = cc.uiloader:seekNodeByName(self.btn_need_card, "img_mask")
    self.btn_need_card.img_mask:setVisible(false)

    
    --最小注
    self.btn_min_bet        = self:seekNodeByName("btn_min_bet")   
    self.btn_min_bet.txt    = cc.uiloader:seekNodeByName(self.btn_min_bet, "txt")  --最小注(500)
    self.btn_min_bet.img_mask=cc.uiloader:seekNodeByName(self.btn_min_bet, "img_mask")
    self.btn_min_bet.img_mask:setVisible(false)
     
    

    --最大注
    self.btn_max_bet        = self:seekNodeByName("btn_max_bet")   
    self.btn_max_bet.txt    = cc.uiloader:seekNodeByName(self.btn_max_bet, "txt")  --最大注(500)
    self.btn_max_bet.img_mask= cc.uiloader:seekNodeByName(self.btn_max_bet, "img_mask")
    self.btn_max_bet.img_mask:setVisible(false)

    --续押
    self.btn_xuya           = self:seekNodeByName("btn_xuya")         
    self.btn_xuya.txt       = cc.uiloader:seekNodeByName(self.btn_xuya, "txt")
    self.btn_xuya.img_mask  = cc.uiloader:seekNodeByName(self.btn_xuya, "img_mask")
    self.btn_xuya.img_mask:setVisible(false)
    self.btn_xuya.is_xy = true

    --停牌
    self.btn_stop           = self:seekNodeByName("btn_stop")         
    self.btn_stop.img_mask  = cc.uiloader:seekNodeByName(self.btn_stop, "img_mask")
    self.btn_stop.img_mask:setVisible(false)
    self.btn_stop.cheStop = cc.uiloader:seekNodeByName(self.btn_stop,"cheStop")
    self.btn_stop.txt_stop = cc.uiloader:seekNodeByName(self.btn_stop,"txt_stop")
    self.btn_stop.txt_stop:setVisible(true)
    self.btn_stop.cheStop:setVisible(false)
    self.btn_stop.cheStop:setSelected(false)
    self.btn_stop.cheStop:setEnabled(false)


    --不买保险
    self.btn_unbuy          = self:seekNodeByName("btn_unbuy")
    self.btn_unbuy.img_mask = cc.uiloader:seekNodeByName(self.btn_unbuy, "img_mask")
    self.btn_unbuy.img_mask:setVisible(false)
    
    --买保险
    self.btn_buy          = self:seekNodeByName("btn_buy")
    self.btn_buy.img_mask = cc.uiloader:seekNodeByName(self.btn_buy, "img_mask")
    self.btn_buy.img_mask:setVisible(false)

    --准备/继续游戏
    self.btn_ready = self:seekNodeByName("btn_ready")
    -- self.btn_ready.img_mask = cc.uiloader:seekNodeByName(self.btn_ready, "img_mask")
    -- self.btn_ready.img_mask:setVisible(false)
    self.btn_ready:setVisible(false)
    self.btn_ready:setLocalZOrder(5)

    -- local node_ready = self:seekNodeByName("node_ready")
    -- local ac = cc.CSLoader:createTimeline("subgame/point21/point21_kspp_btn.csb")
    -- node_ready:runAction(ac)
    -- local ac_info = ac:getAnimationInfo("animation0")
    -- ac:gotoFrameAndPlay(ac_info.startIndex, ac_info.endIndex, true)

    self.img_select_panel = self:seekNodeByName("img_select_panel")
    self.slider = self:seekNodeByName("Slider")
    self.slider.lbl_select_coin = self:seekNodeByName("lbl_select_coin")
    self.slider:setPercent(0)
 
    self.slider:addEventListener(handler(self, self.onSliderEvent))
    self.slider.size = self.slider:getContentSize()
    self.imgXiazhu = self:seekNodeByName("imgXiazhu")
    self.imgXiazhu.y = self.imgXiazhu:getPositionY()
    -- self.node_all_in = self:seekNodeByName("node_all_in")
    -- self.node_all_in:setVisible(false)
    -- local path = "subgame/point21/point21_allin.csb"
    -- local ac = cc.CSLoader:createTimeline(path)
    -- self.node_all_in:runAction(ac)
    -- self.node_all_in.ac = ac
    -- local ac_info = ac:getAnimationInfo("animation0")
    -- self.node_all_in.ac:gotoFrameAndPlay(ac_info.startIndex, ac_info.endIndex, true)


    local btn_cancel = self:seekNodeByName("btn_cancel")
    btn_cancel:setVisible(false)
    self.btn_ready:addTouchEventListener(handler(self, self.onBtnReady))
    self.btn_unbuy:addTouchEventListener(handler(self, self.onBtnUnBuy))
    self.btn_buy:addTouchEventListener(handler(self, self.onBtnBuy))
    self.btn_double:addTouchEventListener(handler(self, self.onBtnDouble))
    self.btn_need_card:addTouchEventListener(handler(self, self.onBtnNeedCard))
    self.btn_min_bet:addTouchEventListener(handler(self, self.onBtnMinBet))
    self.btn_max_bet:addTouchEventListener(handler(self, self.onBtnMaxBet))
    self.btn_xuya:addTouchEventListener(handler(self, self.onBtnXuYa))
    self.btn_stop:addTouchEventListener(handler(self, self.onBtnStop))
    self.btn_split:addTouchEventListener(handler(self, self.onBtnSplit))
    self.btn_bet:addTouchEventListener(handler(self, self.onBtnBet)) 
    self:setImgSelectPanelVisible(self.select_panel_visible)
    self.btn_withdrawl:setVisible(false)
end

function BlackJackGameLayer:initOther()
    self.img_room_name = self:seekNodeByName("img_room_name")       --老板场/富豪场
    self.img_room_name:setVisible(false)
    self.label_min_coin = self:seekNodeByName("label_min_coin")     --最小押注
    self.label_min_coin:setVisible(false)
    self.lbl_bank_coin = self:seekNodeByName("lbl_bank_coin")       --金库：95.00万
    self.node_jetton = self:seekNodeByName("node_jetton")           --筹码节点
    self.sp_jetton = self:seekNodeByName("sp_jetton")  
    self.node_jetton:setLocalZOrder(2)

    --提示
    self.img_tips = self:seekNodeByName("img_tips")
    self.label_tips = self:seekNodeByName("label_tips")
    self.img_tips:setVisible(false)


    local function pnl_desk_callback(sender, eventType)
        if eventType == ccui.TouchEventType.ended then
            self:setImgSelectPanelVisible(false)
        end
    end  
    self.pnl_desk:addTouchEventListener(pnl_desk_callback)

    local namePoint = cc.p(self.img_room_name:getParent():getPosition())
    local nameSize = self.img_room_name:getParent():getContentSize()
    self.mTextRecord = UIAdapter:CreateRecord()
    self.pnl_desk:addChild(self.mTextRecord)
    self.mTextRecord:setPosition(cc.p(namePoint.x + nameSize.width / 2 + 10, namePoint.y))
end

--新增玩家
function BlackJackGameLayer:eventAddPlayer(pid) 
    local player = g_GameController:getPlayerData(pid)
    local show_pos = g_GameController:getShowPos(player.m_chairId)
    local room_state = g_GameController:getRoomState()

    local panel = self.all_player_panel1[show_pos]
    print("新增玩家panel=",show_pos,panel)
    if panel and panel.pid == nil then
       
        panel:setVisible(true)  
        panel.label_name:setString(player.m_nickname) 
        
        panel.label_coin:setString(player.m_score*0.01)
        local head = ToolKit:getHead( player.m_faceId)
        panel.img_head_icon:loadTexture(head,1) 
        panel.img_head_icon:setScale(1)
        panel.pid = pid
        print("只有这里才有赋值=",player.m_chairId)
        panel.pos = player.m_chairId
    end
end

--房间状态
function BlackJackGameLayer:eventRoomState()
    self.room_state = g_GameController:getRoomState()
    self.state = self.room_state.game_step
    self.btn_min_bet.txt:setString(string.format("最小注(%s)", self.room_state.m_minLimit*0.01))
    local coin = g_GameController:getMyData().m_score*0.01
    self:updatenBtnMaxCoin(coin)
   -- self.btn_max_bet.txt:setString(string.format("最大注(%s)",self.room_state.m_maxLimit*0.01))
    self.btn_ready:setVisible(false)
    dump(self.room_state,"房间状态=")
    local show_pos = g_GameController:getShowPos(self.room_state.loop_pos)
    for k , v in pairs(self.all_img_xiazhuqu) do
        self:setImgLight(k,false)
        if v.img_tips then
            v.img_tips:setVisible(false)
        end
        self:removeProgress(self.all_player_panel1[k])
    end
    if self.state == ROOM_STATE_READY then --准备
    elseif self.state == ROOM_STATE_BET then -- 押注阶段
        self:hideTips()
        self:stateBet()
    elseif self.state == ROOM_STATE_SENDCARD then --发牌
        for k , panel in pairs(self.all_player_panel1) do
            self:removeProgress(panel)  --先移除所有计时器
        end
        for k , v in pairs(self.all_img_xiazhuqu) do
            if v.img_light then
                self:setImgLight(k, false)
            end
            if v.img_tips then
                v.img_tips:setVisible(false)
            end
        end

    elseif self.state == ROOM_STATE_SAFE then --保险操作
        for k , panel in pairs(self.all_player_panel1) do
            self:removeProgress(panel)  --先移除所有计时器
        end
        self.cur_safe_pos = self.all_player_panel1[3].pos
        self:showBtnMode(SAFE_MODE)
        print("ROOM_STATE_SAFE====")
        self:showCountDown(true)
        if self:getBetPosCount() > 1 then
            self:setImgLight(g_GameController:getShowPos(self.all_player_panel1[3].pos), true)
        end
    elseif self.state == ROOM_STATE_PLAYER_LOOP then --玩家操作
        dump(self.player_bet_list,"轮到玩家操作")

        if self.player_bet_list[self.room_state.loop_pos] == g_GameController:getMyPid() then --是自己
            --玩家操作完
            table.insert(self.player_operate_list,self.room_state.loop_pos)

            self:showBtnMode(CARD_MODE)
            self:setParentBtnPanelVisible(true)

            dump(self.split_list)
            if self.split_list[show_pos] ~= nil then 
                --有分牌
                print("self.room_state.loop_pos=",self.room_state.loop_pos,self.room_state.loop_pos%10)
                if self.room_state.loop_pos%10 == 0 then --右边的 偶数右边的分牌
                    print("分牌的 右边操作")
                    for _ , node in pairs(self.all_node_card1[self.room_state.loop_pos+1]) do --左边的
                        node.img_Poker:setColor(cc.c3b(70,70,70))
                        node.is_black = true
                    end
                else
                    print("分牌的 左边操作")
                    --左边的
                    for _ , node in pairs(self.all_node_card1[self.room_state.loop_pos-1]) do --右边的
                        node.img_Poker:setColor(cc.c3b(70,70,70))
                        node.is_black = true
                    end
                    for _ , node in pairs(self.all_node_card1[self.room_state.loop_pos]) do --右边的
                        node.img_Poker:setColor(cc.c3b(255,255,255))
                        node.is_black = false
                    end
                end
            else
                if self:getBetPosCount() > 1 then
                    self:setImgLight(show_pos, true)
                end
            end
            print("self.btn_stop.cheStop:isSelected()",self.btn_stop.cheStop:isSelected())
            if self.btn_stop.cheStop:isSelected() then
                self:reqPlayerLoop(LOOP_STOP,self.room_state.loop_pos)
                self.btn_stop.cheStop:setSelected(false)
                self.btn_stop.cheStop:setVisible(false)
                self.btn_stop.txt_stop:setVisible(true)
                return
            end
        else
            for k , v in pairs(self.all_img_xiazhuqu) do
                self:setImgLight(k, false)
            end
            for _ , node_list in pairs(self.all_node_card1) do 
                for _, node in pairs(node_list) do
                    if node.is_black == true then
                        node.img_Poker:setColor(cc.c3b(255,255,255))
                        node.is_black = false
                    end
                end
            end
        end
        self:statePlayerLoop()
    elseif self.state == ROOM_STATE_RESULT then --结算
        
        for k , panel in pairs(self.all_player_panel1) do
            self:removeProgress(panel)  --先移除所有计时器
        end
         self:setParentBtnPanelVisible(false)
        for k , v in pairs(self.all_img_xiazhuqu) do
          --  v.img_light:setVisible(false)
            self:setImgLight(k, false)
        end 
         local function showReady(args)
           self.btn_ready:setVisible(true) 
        end
        local show =  scheduler.performWithDelayGlobal(showReady, 2)
        table.insert(self.sch_all, show)
       -- self.btn_ready:setVisible(true)
    else
    end
end

function BlackJackGameLayer:getBetPosCount()
    local num = 0
    for i = 10 , 50 , 10 do
        if self.player_bet_list[i] == g_GameController:getMyPid() then
            num = num + 1
        end
    end
    return num
end

function BlackJackGameLayer:statePlayerLoop() 
	if self.state ~= ROOM_STATE_PLAYER_LOOP then
		return 
	end
    --根据对应的操作显示按钮
    self:setSelectPos(self.room_state.loop_pos)
    local show_pos = g_GameController:getShowPos(self.room_state.loop_pos)
    self:showLightAction(show_pos)
    if self.player_bet_list[self.room_state.loop_pos] == g_GameController:getMyPid() then --如果是轮到自己操作
        print("是自己在操作=",self.room_state.loop_pos,show_pos)
        -- if self.all_player_panel1[show_pos].pid == nil then
            --是空位
        self:showBtnMode(CARD_MODE)
        -- setSplitEnable
     

        for k , v in pairs(self.all_img_xiazhuqu) do
            self:setImgLight(k, false)
        end
        --分牌也要提示吧?
        self:setImgLight(show_pos, true) 
        if g_GameController:getRoomState().m_canCut2two==1 then
                print("显示分牌按钮")
            self:setSplitEnable(true)

        else
            self:setSplitEnable(false)
            if self:getBetPosCount() > 1 then
                --有多个位置
                self.all_img_xiazhuqu[show_pos].img_light.txt:setVisible(false)
                print("亮光圈")
            end
        end

--        if self.all_card_value[self.room_state.loop_pos] then
--            if #self.all_node_card1[self.room_state.loop_pos] == 2 then
--                 if self.all_card_value[self.room_state.loop_pos][1] == 21 or self.all_card_value[self.room_state.loop_pos][2] == 21 then
--                    --自动停牌
--                    print("黑杰克21点自动停牌=")
--                    self:reqPlayerLoop(LOOP_STOP, self.room_state.loop_pos)
--                end
--            end
--        end

    else
        self:showBtnMode(WAIT_MODE)
--        --是别的玩家,判断是否的AI，是AI就前端做处理发牌
--        print("self.room_state.loop_pos=",self.room_state.loop_pos)
--        dump(self.player_bet_list)
--        if self.room_state.loop_pos == 0 then -- 庄家操作
--            -- local cards = g_GameController:getCardData(self.room_state.loop_pos)
--            -- local card = cards[#cards]
--            -- self:sendCard(self.room_state.loop_pos, blackjack_util.getCardColor(card))
--        else

--            local player_data = g_GameController:getPlayerData(self.player_bet_list[self.room_state.loop_pos])
--            if player_data and player_data.is_ai == true then --是AI 牌的数据已经发过来了 自动发牌
--                local cards = g_GameController:getCardData(self.room_state.loop_pos)
--                local t = 1
--                for k = 3 , #cards do
--                    local card = cards[k]
--                    local loop_pos = self.room_state.loop_pos
--                    local sch = scheduler.performWithDelayGlobal(function() 
--                   --     self:playSound("BlackJack>get_card")
--                        self:sendCard(loop_pos, blackjack_util.getCardColor(card))
--                    end, t)
--                    t = t + 1
--                    print("ttttttttttttttttttttttt=", t)
--                end
--            end
--        end
    end
    self:showCountDown()

end

function BlackJackGameLayer:playSound(path)
    print("path=",path)
    local num = SoundConfig[path].num
    -- local sex = math.random(1,2)
    local index = math.random(1,num)
    --AudioManager:playSound(path,false,index)
end

--设置选择押注金额面板
function BlackJackGameLayer:setImgSelectPanelVisible(visible)
    self.select_panel_visible = visible
    self.img_select_panel:setVisible(visible)
    if visible == false then
        self.btn_bet.txt:setString("下 注")
    end
end

function BlackJackGameLayer:setLabelSelectCoin(coin)

    
    common_util.setCoinStr(self.slider.lbl_select_coin, common_util.tansCoin(coin), 2)
end

--玩家离开
function BlackJackGameLayer:eventPlayerLeave(event)
    local pid = event.pid
    if self.over_game == true then
        --无视掉玩家离开房间
        return
    end
    local show_pos = g_GameController:getPlayerShowPos(pid)
    local panel = self.all_player_panel1[show_pos]
    if panel then 
        self:setPanelVisible(panel,false)
        panel.pid = nil
        panel.pos = nil
    end
end

function BlackJackGameLayer:setPanelVisible(panel,visible)
    if panel then
        if visible == true then
            panel:setVisible(visible)
        else
            panel:setVisible(false) 
            panel.pid = nil
        end
    end
end

--玩家押注
function BlackJackGameLayer:eventRoomBet(data) 
    data.pos = data.m_chairId
    data.pid = data.m_accountId
    local player_data = g_GameController:getPlayerData(data.m_accountId)
    local player_pos = g_GameController:getShowPos(player_data.m_chairId)
    local player_panel = self.all_player_panel1[player_pos]
    local bet_panel = nil
    local bet_pos = nil
    local is_empty_pos = false
    for k , p in pairs(self.all_player_panel1) do
        --有可能分牌
        if p.pos == data.m_chairId or p.pos == (data.m_chairId-1) then
            bet_panel = p
            bet_pos = k
            is_empty_pos = p.pid == nil
        end
    end
    if player_panel == nil or bet_panel == nil or bet_pos == nil then
        print("player_panel,bet_panel,bet_pos=",player_panel,bet_panel,bet_pos)
        print("怎么会是空的·~~~~~panel == nil~~~~~bet_panel == nil or bet_pos == nil~~~~~~~")
        return
    end
    
    self.player_bet_list[data.pos] = data.pid

    local bet_coin = data.m_betValue*0.01 
 --   player_data.coin = player_data.coin - bet_coin
    local p = self.all_img_xiazhuqu[bet_pos]
    p.img_bet_coin:setVisible(true)
    p.img_bet_coin.label_bet_coin:setString(bet_coin)
    player_panel.label_coin:setString(data.m_curScore*0.01)  
    local x,y = p.node_coin_pos:getPosition()
    self:showBetChips(data.pos, cc.p(x,y), bet_coin) 
    self.no_bet_list_pos[bet_pos] = nil
    local is_has = false
    for k ,v in pairs(self.no_bet_list_pos) do
        if v == true then
            is_has = true
        end
    end
    if is_has == false then --没有空位要下注的了，帮这个玩家倒计时关掉
        -- self:removeProgress(player_panel)
        if data.pid == g_GameController:getMyPid() then 
        --    self:reqPlayerLoop(LOOP_STAKE_STOP)
        end
    end

    if self.self_bet == true or data.pid == g_GameController:getMyPid() then
        self.self_bet = true
        --自己下注的，检测有没有别的空位可以下注
        if self.is_bet_over ~= true then
            if self:checkCanBetPos() == false then --没有了
                --默认帮玩家发送下注完成
                -- self.select_pos = player_data.pos
                self:setSelectPos(player_data.pos)
                print("默认帮玩家发送下注完成=",self.select_pos)
                -- self.player_bet_list[self.select_pos]
                if self.player_bet_list[self.select_pos] == nil then
               --     self:reqPlayerLoop(LOOP_STAKE_STOP)
                end
            end
        end
        if data.pid == g_GameController:getMyPid() then
            self:showLight()
            self:updatenBtnMaxCoin(data.m_curScore*0.01)
        end

        
    end
    p.img_light.img_name:setVisible(true)
    p.img_light.img_name.label_name:setString(player_data.m_nickname)
--    if true == is_empty_pos then
--        p.img_light.img_name:setVisible(true)
--        if player_data.pid == g_GameController:getMyPid() then
--            p.img_light.img_name.label_name:setString(player_data.m_nickName)
--        else
--            common_util.transName(p.img_light.img_name.label_name, player_data.m_nickName)
--        end
--    end

    local isDone = self:isMyBetDone()
    if self.self_bet == true and data.pid == Player:getAccountID() and not isDone then --自己已经下完注了
        self.btn_xuya.is_xy = false
        self.btn_xuya.txt:setString("下注完成")
        self.btn_xuya.img_mask:setVisible(false)
        self.btn_xuya:setTouchEnabled(true)
    end
end


function BlackJackGameLayer:checkCanBetPos()
    print("self.checkCanBetPos=",self.select_pos)
    local is_has = false
    for index , p in pairs(self.all_img_xiazhuqu) do
        local panel = self.all_player_panel1[index]
        local pos = panel.pos
        local visible = self.no_bet_list_pos[index] == true
        if visible == true then
            is_has = true
        end
        -- local sel_pos = math.floor(self.select_pos/10)
        -- print("sel_pos=",visible,sel_pos)
        if visible == true and self.select_pos ~= pos then  --已经选中的就不管
            if p.img_light then
                -- p.img_light:setVisible(true)
                self:setImgLight(index, true)
                if p.img_light.txt then
                    p.img_light.txt:setVisible(false)
                end
            end
            if p.img_tips then
                p.img_tips:setVisible(true)
            end
        else
            if visible == false then    --已经别人下注了
                if self.select_pos == pos then --如果是自己选中了
                    -- self.select_pos = self.all_player_panel1[3].pos  --恢复原来自身的位置
                    self:setSelectPos(self.all_player_panel1[3].pos)
                end
                if p.img_light then
                    -- p.img_light:setVisible(false)
                    self:setImgLight(index, false)
                    if p.img_light.txt then
                        p.img_light.txt:setVisible(false)
                    end
                end
                if p.img_tips then
                    p.img_tips:setVisible(false)
                end
            end
        end
    end
    return is_has
end

function BlackJackGameLayer:showBetChips(pos, epos, coin, is_safe, target_pos)
    target_pos = target_pos or pos
    local jettons = blackjack_util:getJettons(coin)
    local jetton_nodes = {}
    self.chip_pos[target_pos] = self.chip_pos[target_pos] or {} 
    for i,j in pairs(jettons) do
        if j ~= 0 then
            for m=1,j do
                local jetton = self:throw_jetton(pos, i, is_safe)
                table.insert(jetton_nodes, jetton)
                if is_safe ~= true then
                    table.insert(self.chip_pos[target_pos], jetton)
                end
            end
        end
    end
    self:throw_jettons(target_pos, jetton_nodes, epos, is_safe) 
end

--复原
function BlackJackGameLayer:eventRecover()
    local all_player_data = g_GameController:getAllPlayerData()
    for k , player in pairs(all_player_data) do

    end
end

--结算
local _result_num = 0
function BlackJackGameLayer:eventRoomResult(info)
--    if self.zj_node_card[2] == nil and _result_num < 3 then --牌还没有发完，就收到结算，有可能庄家是黑杰克，所以这么快收到结算
--        local sch = scheduler.performWithDelayGlobal(function() 
--            print("延迟结算")
--            self:eventRoomResult()
--        end,1.5)
--        _result_num = _result_num + 1
--        print("牌还没有发完，就收到结算，庄家是黑杰克")
--        table.insert(self.sch_all, sch)
--        return
--    end
--    --AudioManager:playSound("BlackJack>Win")
--    _result_num = 0
--    --判断庄家第二张牌还要不要翻牌
--    self.result_data = g_GameController:getResultData()
--    
--    local zj_card_num = 0
--    if self.result_data and self.result_data.result then
--        zj_card_num = #self.result_data.result[1].cards

--        if zj_card_num == 2 then
--            if #self.zj_node_card >= 2 and self.zj_node_card[2] and self.zj_node_card[2].is_look ~= true then
--                local aniInfo = self.zj_node_card[2].ac:getAnimationInfo("animation1")
--                self.zj_node_card[2].ac:gotoFrameAndPlay(aniInfo.startIndex, aniInfo.endIndex, false)
--                self:changeOneCard(self.result_data.result[1].cards[2], self.zj_node_card[2])
--                self:checkCardValue(self.result_data.result[1].cards[2].size, 0)
--                self.zj_node_card[2].is_look = true
--            else
--                print("#self.zj_node_card=",#self.zj_node_card)
--                dump(self.zj_node_card)
--            end
--        else
--            print("sssssssssssssssssssssssssssssss")
--        end
--    end

--    for k , v in pairs(self.result_data.result) do
--        
--    end
    
     self:setParentBtnPanelVisible(false)
    g_AudioPlayer:playEffect("subgame/point21/sound/Win.mp3")
    local coin_list = {}
    local all_coin = {}
    --先结算输的
    dump(info.m_allResult)
    for k , v in pairs(info.m_allResult) do
        v.pid = v.m_accountId
        v.pos = v.m_chairId
        v.coin = v.m_profit*0.01
        if v.m_accountId > 0 then
            local panel_pos = g_GameController:getShowPos(v.pos)
            local show_pos = g_GameController:getPlayerShowPos(v.pid)
            all_coin[show_pos] = all_coin[show_pos] or 0
            all_coin[show_pos] = all_coin[show_pos] + v.coin
            local p = self.all_img_xiazhuqu[panel_pos]
            local player_data = g_GameController:getPlayerData(v.pid)
            if v.coin <= 0 then 
                coin_list[panel_pos] = coin_list[panel_pos] or 0
                coin_list[panel_pos] = coin_list[panel_pos] + v.coin
                
                if v.coin == 0 then --平
                    self:collect_jettons(self.chip_pos[v.pos], self.all_img_xiazhuqu[show_pos].coin_pos)    
                    -- common_util.setCoinStr(p.img_bet_coin.label_bet_coin, common_util.tansCoin(v.coin), 2) 
                     
                else
                    --输
                    self:collect_jettons(self.chip_pos[v.pos], self.node_coin_pos_zj)
                    -- if coin_list[panel_pos] > 0 then
                    --     common_util.setCoinStr(p.img_bet_coin.label_bet_coin, common_util.tansCoin(coin_list[panel_pos]), 2, "+")
                    -- else
                    --     common_util.setCoinStr(p.img_bet_coin.label_bet_coin, common_util.tansCoin(coin_list[panel_pos]), 2)
                    -- end
                end

                
            end
             
            self.all_player_panel1[show_pos].label_coin:setString(v.m_curScore*0.01)
            
            
        end
    end

    --统一飘输赢值
    for show_pos , coin in pairs(all_coin) do
        if coin < 0 then
            if show_pos == 3 then 
                 g_AudioPlayer:playEffect("subgame/point21/sound/lose.mp3")
            end
            self.all_player_panel1[show_pos].node_fenshu.node.img_lose:setVisible(true)
            self.all_player_panel1[show_pos].node_fenshu.node.img_lose.txt:setString(common_util.tansCoin(coin))
            self.all_player_panel1[show_pos].node_fenshu.node.img_win:setVisible(false)
            local ac_info = self.all_player_panel1[show_pos].node_fenshu.node.ac:getAnimationInfo("animation0")
            self.all_player_panel1[show_pos].node_fenshu.node.ac:gotoFrameAndPlay(0,80, false)
        else
            if coin > 0 then
                if show_pos == 3 then
                    g_AudioPlayer:playEffect("subgame/point21/sound/Win.mp3")
                end
            end
            self.all_player_panel1[show_pos].node_fenshu.node.img_lose:setVisible(false)
            self.all_player_panel1[show_pos].node_fenshu.node.img_win:setVisible(true)
            self.all_player_panel1[show_pos].node_fenshu.node.img_win.txt:setString(common_util.tansCoin(coin))
            local ac_info = self.all_player_panel1[show_pos].node_fenshu.node.ac:getAnimationInfo("animation0")
            self.all_player_panel1[show_pos].node_fenshu.node.ac:gotoFrameAndPlay(0, 80, false)
        end
    end

    --结算赢的
    local function win_callback()
        for k , v in pairs(info.m_allResult) do
             v.pid = v.m_accountId
            v.pos = v.m_chairId
            v.coin = v.m_profit*0.01
            if v.pid > 0 then
                local panel_pos = g_GameController:getShowPos(v.pos)
                local show_pos = g_GameController:getPlayerShowPos(v.pid)
                local p = self.all_img_xiazhuqu[panel_pos]
                if v.coin > 0 then --赢
                    coin_list[panel_pos] = coin_list[panel_pos] or 0
                    coin_list[panel_pos] = coin_list[panel_pos] + v.coin
                    local x,y = p.node_coin_pos:getPosition()
                    self:showBetChips(0, cc.p(x,y), v.coin, nil, v.pos)

                    --0.5
                    local sch = scheduler.performWithDelayGlobal(function()
                        self:collect_jettons(self.chip_pos[v.pos], self.all_img_xiazhuqu[show_pos].coin_pos) 
                    self.all_player_panel1[show_pos].label_coin:setString(v.m_curScore*0.01)
                    end,1.5)
                end
            end
        end

    end

    local transChip = false
    for _,v in pairs(info.m_allResult) do
         v.pid = v.m_accountId
            v.pos = v.m_chairId
            v.coin = v.m_profit
        if v.pid > 0 and v.coin > 0 then
            transChip = true
        end
    end
    if transChip then
        local sch = scheduler.performWithDelayGlobal(win_callback, 2)
        table.insert(self.sch_all, sch)
    else
    --   self.all_player_panel1[3].label_coin:setstring
    end
   
end

--回收筹码
function BlackJackGameLayer:collect_jettons( nodes, aim_node,callback)
    local num = 0
    if nodes then
        for k,v in pairs(nodes) do
            local callfun = function ( node )
                node:setPosition(cc.p(-100,-100))
                num = num + 1
                if num == #nodes and callback then
                    callback()
                end
            end
            common_util.collect_jetton(v, aim_node, nil, nil, nil, 1, callfun)
        end
    else
        if callback then
            callback()
        end
    end
    

    local function callback()
        
        --AudioManager:playSound("BlackJack>Bet")
    end
    local sch = scheduler.performWithDelayGlobal(callback,1)
    table.insert(self.sch_all, sch)
end


--错误码
function BlackJackGameLayer:eventError(event)
    local data = event.data
    if data.code == 101003 then 
        --下注金额不对
        -- UIManager:get_com_tip("下注金额不对")
        print("===eventError101003===下注金额不对")
        self.btn_min_bet:setTouchEnabled(true)
        self.btn_min_bet.img_mask:setVisible(false)

        self.btn_max_bet:setTouchEnabled(true)
        self.btn_max_bet.img_mask:setVisible(false)

        self.btn_bet:setTouchEnabled(true)
        self.btn_bet.img_mask:setVisible(false)

        self.btn_xuya:setTouchEnabled(true)
        self.btn_xuya.img_mask:setVisible(false)
    else
    end
end 


--下注按钮
function BlackJackGameLayer:onBtnBet(sender, eventType)
    if eventType == ccui.TouchEventType.ended then 
        --AudioManager:playSound("all>click")
        if self.select_panel_visible == true then --确定按钮
            self.lasttime_bet_coin = self:getBetCoin()
            self:setBetEnable(false)
            self:setBtnXuyaEnable(false)
            self:reqBet(self.lasttime_bet_coin)
            self.btn_bet.txt:setString("下 注")
        else
            ---if self:getBtnBetIsBet() == true then --下注
            ---    self:setBtnBetIsBet(false)
            ---else        --确定
            --    self:setBtnBetIsBet(true)
            ---end
            self.btn_bet.txt:setString("确 定")
            self.slider:setPercent(1)
            local coin = self:getBetCoin()
            self:setLabelSelectCoin(coin)
            self.imgXiazhu:setPositionY(self.imgXiazhu.y + self.slider:getPercent()/100*self.slider.size.width)

        end
        self:setImgSelectPanelVisible(not self.select_panel_visible)
    end
end

--取款
function BlackJackGameLayer:onBtnWithDrawl(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        self:setImgSelectPanelVisible(false)
        --AudioManager:playSound("all>click")
        UIManager:openUI(UIModuleConst.withdrawalView)
    end
end

--分牌
function BlackJackGameLayer:onBtnSplit(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        --AudioManager:playSound("all>click")
         
--        local show_pos = g_GameController:getShowPos(self.room_state.loop_pos)
--        local pid = self.player_bet_list[self.room_state.loop_pos]
--        local panel_pos = g_GameController:getPlayerShowPos(pid)

--        local coin = self.all_player_panel1[show_pos].yazhu[self.select_pos%10+1]*self.min_coin
--        local player_data = g_GameController:getPlayerData(pid)
--        if coin > player_data.coin then
--            UIManager:get_com_tip(TipConfig[55])
--            return
--        end 
     --   self:setALLbtnEnable(false)
        self:setImgSelectPanelVisible(false)
       -- self:reqPlayerLoop(LOOP_SPLIT)
        g_GameController:FenPaiReq(self.room_state.loop_pos)
    end
end

--双倍
function BlackJackGameLayer:onBtnDouble(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        --AudioManager:playSound("all>click")

        local show_pos = g_GameController:getShowPos(self.room_state.loop_pos) 
      --  self:setALLbtnEnable(false)
        self:setImgSelectPanelVisible(false)
        g_GameController:DoubleBetReq(self.select_pos) 
    end
end

--不买保险
function BlackJackGameLayer:onBtnUnBuy(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        --AudioManager:playSound("all>click")
        self:setImgSelectPanelVisible(false)
        self.btn_buy:setVisible(false)
        self.btn_unbuy:setVisible(false)
        local data = {
            pos = self.cur_safe_pos,
            pid = g_GameController:getMyPid()
        }
        print("不买保险")
  --      self:checkBuySafe(data)
        g_GameController:InsureReq(self.cur_safe_pos,2)     
        --
    end
end

function BlackJackGameLayer:onBtnReady(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        --AudioManager:playSound("all>click")
        self.over_game = false
        self:cleanAll()
        self:stopAllSch()
    g_GameController:gameReadyReq()
         self.btn_ready:setVisible(false)

    end
end
 

function BlackJackGameLayer:showTips(str)
    if self.com_tip == nil then
        local com_tip = cc.uiloader:load("common/tishi.csb")
        com_tip:setPosition(cc.p(display.cx/display.scaleX, display.height*0.5))
        self:addChild(com_tip, 1000)

        local tip = cc.uiloader:seekNodeByName(com_tip, "text_tip")
        tip:setString(str)
        self.com_tip = com_tip
        self.com_tip.tip = tip
    else
        if self.com_tip.tip then
            self.com_tip.tip:setString(str)
        end
        self.com_tip:setVisible(true)
    end
end

function BlackJackGameLayer:hideTips()
    if self.com_tip then
        self.com_tip:setVisible(false)
    end
end
--买保险
function BlackJackGameLayer:onBtnBuy(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        --AudioManager:playSound("all>click")
        self:setImgSelectPanelVisible(false)

--        local show_pos = g_GameController:getShowPos(self.cur_safe_pos)
--        local coin = self.all_player_panel1[show_pos].yazhu[self.cur_safe_pos%10+1]
--        local player_data = g_GameController:getPlayerData(self.all_player_panel1[3].pid)
--        coin = coin / 2
--        if coin > player_data.coin then
--            local player = GameModel:getPlayerData()
--            print("买保险=================coin=",coin,player_data.coin,player.coin)
--            UIManager:get_com_tip(TipConfig[55])
--            return
--        end 

        print("请求购买保险=",self.cur_safe_pos)
        g_GameController:InsureReq(self.cur_safe_pos,1)     
--        self.btn_buy:setTouchEnabled(false)
--        self.btn_buy.img_mask:setVisible(true)
--        self.btn_unbuy:setTouchEnabled(false)
--        self.btn_unbuy.img_mask:setVisible(true)

    end
end

--设置下注按钮是否可以点击
function BlackJackGameLayer:setBetEnable(enable)
    self.btn_bet:setTouchEnabled(enable)
    self.btn_bet.img_mask:setVisible(not enable)

    self.btn_min_bet:setTouchEnabled(enable)
    self.btn_min_bet.img_mask:setVisible(not enable)

    self.btn_max_bet:setTouchEnabled(enable)
    self.btn_max_bet.img_mask:setVisible(not enable)

end

--续押
function BlackJackGameLayer:onBtnXuYa(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        --AudioManager:playSound("all>click")
        if sender.is_xy == true then --续押
            if self.lasttime_bet_coin > 0 then
                self:setBetEnable(false)
                self:setBtnXuyaEnable(false)
                self:reqBet(self.lasttime_bet_coin)
            end
        else
            local mydata = g_GameController:getMyData()
            -- self.select_pos = mydata.pos
            self:setSelectPos(mydata.pos)
            --下注完成
            g_GameController:FinishBetReq()
           -- self:reqPlayerLoop(LOOP_STAKE_STOP)

            for k , v in pairs(self.all_img_xiazhuqu) do
                -- if v.img_light then
                --     v.img_light:setVisible(false)
                -- end
                self:setImgLight(k, false)
                if v.img_tips then
                    v.img_tips:setVisible(false)
                end
            end
        end
        self:setImgSelectPanelVisible(false)
    end
end

--最小注
function BlackJackGameLayer:onBtnMinBet(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        --AudioManager:playSound("all>click")
        self.lasttime_bet_coin = g_GameController:getRoomState().m_minLimit*0.01
        self:setBetEnable(false)
        self:setBtnXuyaEnable(false)
        self:reqBet(self.lasttime_bet_coin)
        self:setImgSelectPanelVisible(false)
    end
end

--最大注
function BlackJackGameLayer:onBtnMaxBet(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        --AudioManager:playSound("all>click")
        self.lasttime_bet_coin = self.m_MaxBet
        self:setBetEnable(false)
        self:setBtnXuyaEnable(false)
        self:reqBet(self.lasttime_bet_coin)
        self:setImgSelectPanelVisible(false)
    end
end

--要牌
function BlackJackGameLayer:onBtnNeedCard(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        --AudioManager:playSound("all>click")
        self:setALLbtnEnable(false)
      --  self:reqPlayerLoop(LOOP_NEED)
        g_GameController:MoreCardReq(self.select_pos)
        self:setImgSelectPanelVisible(false)
    end
end

--停牌
function BlackJackGameLayer:onBtnStop(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        --AudioManager:playSound("all>click")
        if self.btn_stop.state == nil then
            self:setALLbtnEnable(false)
            self:setImgSelectPanelVisible(false)
            self:reqPlayerLoop(LOOP_STOP)
          --  self:playSound("BlackJack>stop")
        elseif self.btn_stop.state == "check" then
            self.btn_stop.cheStop:setSelected(not self.btn_stop.cheStop:isSelected())
        end
        
    end
end

function BlackJackGameLayer:setALLbtnEnable(enable) 
    self:setStopEnable(enable)
    self:setNeedCardEnable(enable)
    self:setDoubleEnable(enable)
    self:setSplitEnable(enable)
end

--设置停牌按钮
function BlackJackGameLayer:setStopEnable(enable)
    self.btn_stop.state = nil
    self.btn_stop:setTouchEnabled(enable)
    self.btn_stop.img_mask:setVisible(not enable)
end

--设置要牌按钮
function BlackJackGameLayer:setNeedCardEnable(enable)
    self.btn_need_card:setTouchEnabled(enable)
    self.btn_need_card.img_mask:setVisible(not enable)
    print("setNeedCardEnable=======")
end

--设置双倍按钮
function BlackJackGameLayer:setDoubleEnable(enable)
    self.btn_double:setTouchEnabled(enable)
    self.btn_double.img_mask:setVisible(not enable)
end

--设置分牌是否可以点击
function BlackJackGameLayer:setSplitEnable(enable)
    self.btn_split:setTouchEnabled(enable)
    self.btn_split.img_mask:setVisible(not enable)
end

--设置续押是否可以点击
function BlackJackGameLayer:setBtnXuyaEnable(enable)
    self.btn_xuya:setTouchEnabled(enable)
    self.btn_xuya.img_mask:setVisible(not enable)
end

--选择位置
function BlackJackGameLayer:onSelectPos(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        --AudioManager:playSound("all>click")
        self:setImgSelectPanelVisible(false)

        -- self.select_pos = self.all_player_panel1[sender.index].pos
        self:setSelectPos(self.all_player_panel1[sender.index].pos)
        print("onSelectPos self.select_pos=",self.select_pos)
        local p = self.all_img_xiazhuqu[sender.index]
        if p and p.img_tips then
            p.img_tips:setVisible(false)           --点击此区域下注
        end
        if p and p.img_light then
            p.img_light.txt:setVisible(true)           --请下注
        end
        self:setImgLight(sender.index, true)
        local last_p = self.all_img_xiazhuqu[self.last_select_pos]
        if last_p then
            last_p.img_light.txt:setVisible(false)
            last_p.img_tips:setVisible(true)        --这里要加多一个判断是否是空位
        end
        self.last_select_pos = self.select_pos
        self:setBetEnable(true)
        self:setBtnXuyaEnable(true)

        local coin = common_util.getShortString(common_util.tansCoin(self.lasttime_bet_coin), 2)
        self.btn_xuya.txt:setString(string.format("续押(%s)" ,coin))
        self.btn_xuya.is_xy = true
    end
end

--滑动条
function BlackJackGameLayer:onSliderEvent(sender, eventType)
    if eventType == ccui.SliderEventType.percentChanged then
        local coin = self:getBetCoin()
        self.imgXiazhu:setPositionY(self.imgXiazhu.y + self.slider:getPercent()/100*self.slider.size.width)
        self:setLabelSelectCoin(coin)      
        -- if self.slider:getPercent() >= 100 then
        --     self.node_all_in:setVisible(true)
        -- else
        --     self.node_all_in:setVisible(false)
        -- end
    end
end

--获取滑动条的金币
function BlackJackGameLayer:getBetCoin()
    local percent = self.slider:getPercent()
    
--    if val <= 0 then
--        val = 1
--    end
--    if val > 100 then
--        val = 100
--    end
    local min = g_GameController:getRoomState().m_minLimit*0.01
    local max = self.m_MaxBet
    local val = math.floor(max*percent/100)
    local coin = val
    if coin<min then
        coin =min
     end
    return coin
end

--请求押注
function BlackJackGameLayer:reqBet(coin)
    g_GameController:gameBetReq(self.select_pos, coin)
end

--玩家请求操作
function BlackJackGameLayer:reqPlayerLoop(loop,loop_pos)
    loop_pos = loop_pos or self.select_pos
    if LOOP_STAKE_STOP == loop then
        if self.is_req_loop_bet_stop == true then
            print("已经请求过下注完成了,拦截了")
            return
        end
        self.is_req_loop_bet_stop = true
    end
    if loop == LOOP_STOP then
        print("请求停牌=====================",loop_pos)
        if self.req_loop_stop[loop_pos] == true then
            print("已经请求过停牌了，拦截掉")
            return
        end
        print("停牌")
        self:showBtnMode(WAIT_MODE)
        self.req_loop_stop[loop_pos] = true
    end
    print("玩家请求操作,loop,loop_pos=",loop,loop_pos,os.time())
    g_GameController:StopSendCardReq(loop_pos) 
end

function BlackJackGameLayer:eventPlayerLoop(data)  
    data.pid = data.m_accountId
    local show_pos = g_GameController:getShowPos(data.m_chairId)
    -- local show_pos = g_GameController:getShowPos(data.card_pos)
    if data.loop_step == LOOP_STAKE_STOP then 
        if data.pid == g_GameController:getMyPid() then --是自己下注完成
            self.btn_xuya.is_xy = true
            local coin = common_util.getShortString(common_util.tansCoin(self.lasttime_bet_coin), 2)
            self.btn_xuya.txt:setString(string.format("续押(%s)" ,coin))
            self:showBtnMode(WAIT_MODE)
            self.is_bet_over = true
        end
        self:removeProgress(self.all_player_panel1[show_pos])
    elseif data.loop_step == LOOP_STOP then --停牌
        self:removeProgress(self.all_player_panel1[show_pos])
        g_AudioPlayer:playEffect("subgame/point21/sound/get_card1.mp3")
        if data.pid == g_GameController:getMyPid() then
            self:showBtnMode(WAIT_MODE)
        else
            if data.pid ~= 0 then
            --    self:playSound("BlackJack>stop")
            end
                
        end
        self.all_card_value[data.m_chairId] =data.m_points
        self:showCardValue(data.m_chairId)
        self:setImgLight(data.m_chairId, false)
         g_AudioPlayer:playEffect("subgame/point21/sound/stop_1.mp3")
    elseif data.loop_step == LOOP_NEED then         --要牌
        --AudioManager:playSound("BlackJack>get_card")
        g_AudioPlayer:playEffect("subgame/point21/sound/get_card1.mp3")
    elseif data.loop_step == LOOP_DOUBLE then       --双倍
        -- local index = 2

        local panel = self.all_player_panel1[show_pos]
        self:removeProgress(panel)
        if data.pid == g_GameController:getMyPid() then
            self:showBtnMode(WAIT_MODE) 
             g_AudioPlayer:playEffect("subgame/point21/sound/double.mp3") 
            -- local player = GameModel:getPlayerData()
            -- if coin > player.coin then
                -- UIManager:get_com_tip(TipConfig[55])
            -- else
                -- local x,y = self.all_img_xiazhuqu[show_pos].node_coin_pos:getPosition()
                -- self:showBetChips(data.pid, cc.p(x,y), coin)
--                local tmp_data = {
--                    data = {
--                        code = 0,
--                        pid = data.pid,
--                        pos = data.m_chairId,
--                        yazhu = need_yazhu
--                    }   
--                }
                self:eventRoomBet(data) 
        end
            
            -- if self.split_list[data.card_pos] == true then --是分牌的
        local node_card = self.all_img_xiazhuqu[show_pos].node_card
        node_card.img_double[data.m_chairId%10+1]:setVisible(true)
            -- end
        self.all_card_value[data.m_chairId] =data.m_points
        self:setImgLight(data.m_chairId, false)
       self:sendCard(data.m_chairId,data.m_newCard)
    elseif data.loop_step == LOOP_SPLIT then        --分牌
        --这里要特殊操作一下
        local node_card = self.all_node_card1[data.m_chairId]
        node_card[1]:runAction(cc.MoveBy:create(0.5, cc.p(-40,0)))
        node_card[2]:runAction(cc.MoveBy:create(0.5, cc.p(40,0)))
        self.all_img_xiazhuqu[show_pos].node_card.img_card_value[1]:setVisible(false)
        self.all_img_xiazhuqu[show_pos].node_card.img_card_value[2]:setVisible(false)
     --   local coin = self.all_player_panel1[show_pos].yazhu[data.m_chairId%10+1]
        -- self.all_player_panel1[show_pos].yazhu[2] = self.all_player_panel1[show_pos].yazhu
       -- self.all_player_panel1[show_pos].yazhu[2] = self.all_player_panel1[show_pos].yazhu[1]
            
       

        local x,y = self.all_img_xiazhuqu[show_pos].node_coin_pos:getPosition()
        if data.pid == g_GameController:getMyPid() then --是自己
            self.split_list[show_pos] = true
            -- self.split_list[show_pos+1] = true
        end
        self.player_bet_list[data.m_chairId+1] = data.pid
        self:showBetChips(data.m_chairId+1, cc.p(x,y), data.m_parts[1].m_betValue*0.01)    --丢筹码
         for k , jetton in pairs(self.chip_pos[data.m_chairId]) do
            jetton:runAction(cc.MoveBy:create(0.5,cc.p(40,0)))
        end
        local node_card = self.all_img_xiazhuqu[show_pos].node_card
        node_card.img_double[1]:runAction(cc.MoveTo:create(0.1, cc.p(node_card.img_double[1].pos.x+35, node_card.img_double[1].pos.y)))
        node_card.img_baozha[2]:runAction(cc.MoveTo:create(0.1, cc.p(node_card.img_baozha[2].pos.x+35, node_card.img_baozha[2].pos.y)))

        local p = self.all_img_xiazhuqu[show_pos]
        p.img_bet_coin:setVisible(true) 
        common_util.setCoinStr(p.img_bet_coin.label_bet_coin, common_util.tansCoin(data.m_parts[1].m_betValue*0.01), 2)  --显示下注金额
        self.all_player_panel1[show_pos].label_coin:setString(data.m_curScore*0.01)
--        local player_data = g_GameController:getPlayerData(data.pid)
--        player_data.coin = player_data.coin - coin
--        -- self.all_player_panel1[show_pos].yazhu = self.all_player_panel1[show_pos].yazhu*2
--        common_util.setCoinStr(self.all_player_panel1[show_pos].label_coin, common_util.tansCoin(player_data.coin), 2)
        self:splitCard(data)
    end
    if data.pid > 0 then
        print("玩家操作 show_pos=",show_pos)
        self:showLoopText(data.pid,show_pos,data.loop_step)
    end  
end

function BlackJackGameLayer:showLoopText(pid,show_pos,loop)
    local player_data = g_GameController:getPlayerData(pid)
    local panel = self.all_player_panel1[show_pos]
    local list = {
        [LOOP_STOP] = "停牌",
        [LOOP_NEED] = "要牌",
        [LOOP_DOUBLE] = "双倍",
        [LOOP_SPLIT] = "分牌",
        [LOOP_STAKE_STOP] = "下注完成",
    }
    if panel then
        if panel.sch then
            scheduler.unscheduleGlobal(panel.sch)
            panel.sch = nil
        end
        local name = player_data.m_nickName
        panel.sch = scheduler.performWithDelayGlobal(function () 
            panel.label_name:setString(player_data.m_nickname)
            
            if panel.sch then
                scheduler.unscheduleGlobal(panel.sch)
                panel.sch = nil
            end
        end,3)
        panel.label_name:setString(list[loop])
    end
end


--隐藏操作按钮
function BlackJackGameLayer:setParentBtnPanelVisible(visible)
    self.parent_btn_panel:setVisible(visible)
end

function BlackJackGameLayer:showBtnMode(mode_type)
    print("按钮模式显示为=========",mode_type,os.time())
    print("3333333333333333333333")
    if mode_type ~= SAFE_MODE and mode_type ~= WAIT_MODE then   --不是保险、不是等待操作
        local visible = mode_type == BET_MODE
        local mydata = g_GameController:getMyData()
        if mydata == nil then
            visible = false
        end
        if visible == true then
            print("下注时候")
            -- self.select_pos = mydata.pos
            self:setSelectPos(mydata.m_chairId)
            --显示下注模式，默认选中自己的位置
            print("showBtnMode self.select_pos=",self.select_pos,self.lasttime_bet_coin)
            print("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
            self.btn_xuya.is_xy = true
            if self.lasttime_bet_coin > 0 then
                local coin = common_util.getShortString(common_util.tansCoin(self.lasttime_bet_coin), 2)

                self.btn_xuya.txt:setString(string.format("续押(%s)" ,coin))
                if self.lasttime_bet_coin>self.m_MaxBet then
                     self.btn_xuya:setTouchEnabled(false)
                    self.btn_xuya.img_mask:setVisible(true)
                else
                
                    self.btn_xuya:setTouchEnabled(true)
                    self.btn_xuya.img_mask:setVisible(false)
                end
            else
                self.btn_xuya.txt:setString("续 押")
                self.btn_xuya.img_mask:setVisible(true)
                self.btn_xuya:setTouchEnabled(false)
            end


            self.btn_max_bet:setTouchEnabled(true)
            self.btn_max_bet.img_mask:setVisible(false)
            self.btn_min_bet:setTouchEnabled(true)
            self.btn_min_bet.img_mask:setVisible(false)
            self.btn_bet:setTouchEnabled(true)
            self.btn_bet.img_mask:setVisible(false)
        else

            self.btn_split:setTouchEnabled(true)
            self.btn_split.img_mask:setVisible(false)
 
            self.btn_double:setTouchEnabled(true)
            self.btn_double.img_mask:setVisible(false)
            --    if data.m_accountId == g_GameController:getMyPid() then
        local sch = scheduler.performWithDelayGlobal(function (  )
            self:setStopEnable(true)
            self:setNeedCardEnable(true)    --要判断是否够五张牌
        end,0.5)
        table.insert(self.sch_all,sch)
----    end 
--            self.btn_need_card:setTouchEnabled(true)
--            self.btn_need_card.img_mask:setVisible(false)
            print("==========img_mask=======")

            self:setStopEnable(true)
        end
        self.btn_bet:setVisible(visible)           --下注
        self.btn_min_bet:setVisible(visible)       --最小注
        self.btn_max_bet:setVisible(visible)       --最大注
        self.btn_xuya:setVisible(visible)          --续押
        print("btn_split                           =",not visible)
        self.btn_split:setVisible(not visible)        --分牌
        self.btn_double:setVisible(not visible)       --双倍
        self.btn_need_card:setVisible(not visible)    --要牌
        self.btn_stop:setVisible(not visible)         --停牌
        self.btn_stop.cheStop:setVisible(false)
        self.btn_stop.txt_stop:setVisible(true)

        self.btn_buy:setVisible(false)
        self.btn_unbuy:setVisible(false)
    else
        print("其它时候")
        self.btn_bet:setVisible(false)           --下注
        self.btn_min_bet:setVisible(false)       --最小注
        self.btn_max_bet:setVisible(false)       --最大注
        self.btn_xuya:setVisible(false)          --续押

        
        if mode_type == WAIT_MODE then --等待操作
            print("等待")
            self.btn_buy:setVisible(false)
            self.btn_unbuy:setVisible(false)

            -- print("isMyBetDone======",self:isMyBetDone())
            if self:isMyBetDone() or self.room_state.game_step == ROOM_STATE_RESULT then
                self.btn_split:setVisible(false)        --分牌
                self.btn_double:setVisible(false)       --双倍
                self.btn_need_card:setVisible(false)    --要牌
                self.btn_stop:setVisible(false)         --停牌
            else
                if self._send_all_card_finished and self.room_state.game_step == ROOM_STATE_PLAYER_LOOP then
                    self.btn_split:setVisible(true)        --分牌
                    self.btn_double:setVisible(true)       --双倍
                    self.btn_need_card:setVisible(true)    --要牌
                    self.btn_stop:setVisible(true)         --停牌
                end
                
            end

            self.btn_split:setTouchEnabled(false)
            self.btn_split.img_mask:setVisible(true)

            self.btn_double:setTouchEnabled(false)
            self.btn_double.img_mask:setVisible(true)

            self.btn_need_card:setTouchEnabled(false)
            self.btn_need_card.img_mask:setVisible(true)

            --等待别人操作停牌显示勾选按钮
            self.btn_stop:setTouchEnabled(true)
            self.btn_stop.state = "check"
            -- self.btn_stop.img_mask:setVisible(true)
            self.btn_stop.img_mask:setVisible(false)
            self.btn_stop.cheStop:setVisible(true)
            self.btn_stop.txt_stop:setVisible(false)
        else
            print("保险")
            self.btn_buy:setVisible(true)
            self.btn_unbuy:setVisible(true)
            self.btn_split:setVisible(false)        --分牌
            self.btn_double:setVisible(false)       --双倍
            self.btn_need_card:setVisible(false)    --要牌
            self.btn_stop:setVisible(false)         --停牌
        end
    end
end

--设置最大下注金额
function BlackJackGameLayer:updatenBtnMaxCoin(score)
    local player = g_GameController:getMyData()
    -- player = GameModel:getPlayerData()
    dump(player,"updatenBtnMaxCoin player=====") 
    local max = math.min(g_GameController:getRoomState().m_maxLimit*0.01,score)
    self.m_MaxBet = max
    self.btn_max_bet.txt:setString(string.format("最大注(%s)",max))
end

--设置光圈亮
function BlackJackGameLayer:setImgLight(show_pos, visible)
    if self.all_img_xiazhuqu[show_pos] then
        self.all_img_xiazhuqu[show_pos].img_light:setVisible(visible)
    end
end

--进行分牌
function BlackJackGameLayer:splitCard(data) 
    local pos1 = data.m_parts[1].m_chairId  --20
    local pos2 = data.m_parts[2].m_chairId  -- 21 
-----------
    self.all_node_card1[pos2] = { self.all_node_card1[pos1][1] }
    self.all_node_card1[pos1] = { self.all_node_card1[pos1][2] }

    self.all_card_value[pos1] =  data.m_parts[1].m_points
    self.all_card_value[pos2] =  data.m_parts[2].m_points 
    self:sendCard(pos1, data.m_parts[1].m_cards[2])
    
    local sch = scheduler.performWithDelayGlobal(function()
        self:sendCard(pos2, data.m_parts[2].m_cards[2])
    end,0.5)
    self.player_bet_list[pos2] = data.m_accountId
end

--给所有人发牌
function BlackJackGameLayer:allSendCard(data)
    --AudioManager:playSound("BlackJack>start")
    g_AudioPlayer:playEffect("subgame/point21/sound/Start.mp3")
    self:showBtnMode(WAIT_MODE) 
    for k , v in pairs(self.all_img_xiazhuqu) do
        self:setImgLight(k,false)
        if v.img_tips then
            v.img_tips:setVisible(false)
        end
        self:removeProgress(self.all_player_panel1[k])
    end
     local index = 1
    local send_card_list = {}
    for k , v in pairs(data.m_vecCards) do  
        self.all_card_value[v.m_chairId] =v.m_points 
    end
    for i = 1 , 2 do
        for k , v in pairs(data.m_vecCards) do 
         --   local tPos = g_GameController:getShowPos(v.m_chairId)
           -- print("v.pos=",v.pos,tmp_idnex)
            send_card_list[index] = {pos=v.m_chairId,card=v.m_cards[i]} 
            index = index+1
        end
    end 
    local t = 0
    for k , v in ipairs(send_card_list) do
        local sch = scheduler.performWithDelayGlobal(function() 
            print("发牌===========",k,v.pos)
            g_AudioPlayer:playEffect("subgame/point21/sound/washcard.mp3") 
            
            self:sendCard(v.pos,v.card) 
        end, t)   
        table.insert(self.sch_all,sch) 
        t = t + 0.15
    end

--    local sch = scheduler.performWithDelayGlobal(function ( )
--        self._send_all_card_finished = true
--     --   self:statePlayerLoop()
--        print("_send_all_card_finished======>",self._send_all_card_finished)
--    end,t+0.2)
--    table.insert(self.sch_all,sch)
end
function BlackJackGameLayer:SendZJCard(data)
    dump(self.zj_node_card)
    local t = 0
    self:setParentBtnPanelVisible(false)
    g_AudioPlayer:playEffect("subgame/point21/sound/Flipcard.mp3")
    self.all_card_value[0] =data.m_points
    if data.m_opType ==1 then
        local aniInfo = self.zj_node_card[2].ac:getAnimationInfo("animation1")
        self.zj_node_card[2].ac:gotoFrameAndPlay(0, 20, false)
        local card = blackjack_util.getCardColor(data.m_newCard)
        self:changeOneCard(card, self.zj_node_card[2])
        self:showCardValue(0) 
        print("庄家翻牌一次")
        self.zj_node_card[2].is_look = true 
        if data.m_dimType~= 0 then
            --停牌

        end
     else
        self:sendCard(0,data.m_newCard)
    end 
end
function BlackJackGameLayer:oneSendCard(data)
    --AudioManager:playSound("BlackJack>Flipcard")
     
        
    g_AudioPlayer:playEffect("subgame/point21/sound/get_card_1.mp3")
     --   self:playSound("BlackJack>get_card")
     self.all_card_value[data.m_chairId] =data.m_points
     self:sendCard(data.m_chairId,data.m_newCard)

--    if data.m_accountId == g_GameController:getMyPid() then
--        local sch = scheduler.performWithDelayGlobal(function (  )
--            self:setStopEnable(true)
--            self:setNeedCardEnable(true)    --要判断是否够五张牌
--        end,0.5)
--        table.insert(self.sch_all,sch)
--    end 

end

--发牌
function BlackJackGameLayer:eventSendCard(_,event)
    local data = event.data
    self.send_card_data = data
    local bet_list = g_GameController:getBetList()
    -- local all_player_data = g_GameController:getAllPlayerData()
    dump(bet_list)
    for k , v in pairs(data.cards) do
        if v.pid ~= 0 and bet_list[v.pid] == nil then
            local data = {
                pid = v.pid,
                yazhu = v.times,
                pos = v.pos,
                code = 0,
            }
            dump(event,"发牌的时候默认下注")
            g_GameController:setMyBetInfo(data)       --默认下注，服务器没有推送，只能前端做
        end
    end
    --有时候收到所有玩家的牌数据
    --有时候收到一个玩家的数据
    print("eventSendCard self.room_state.game_step=",self.room_state.game_step)
    if self.room_state.game_step > ROOM_STATE_SENDCARD then --不是所有人发牌的时候
        self:oneSendCard(data)
    else
        self:allSendCard(data)
    end
end

function BlackJackGameLayer:getNextLoopPos(loop_pos)
    loop_pos = loop_pos + 10
    if loop_pos > 50 then
        loop_pos = 10
    end
    return loop_pos
end

--保险结果
local is_can_play_safe = true
function BlackJackGameLayer:eventSafeResult(data) 
    self.btn_buy:setTouchEnabled(true)
    self.btn_buy.img_mask:setVisible(false)
    self.btn_unbuy:setTouchEnabled(true)
    self.btn_unbuy.img_mask:setVisible(false)
     data.pos = data.m_chairId
    data.pid = data.m_accountId
    if data.m_ret == 0  then --成功
        if data.m_opType == 1 then
           
            local show_pos = g_GameController:getShowPos(data.pos)
            local panel = self.all_player_panel1[show_pos]
            --丢钱出去 
            local x,y = self.node_coin_pos_zj:getPosition()     --庄家的位置
            self:showBetChips(data.pos, cc.p(x,y), data.m_betValue*0.01, true)
             local panel_pos = g_GameController:getPlayerShowPos(data.pid)
            self.all_player_panel1[panel_pos].label_coin:setString(data.m_curScore*0.01) 
            --判断是否还要不要购买保险
            if data.pid ==Player:getAccountID() then
                self:checkBuySafe(data)
            end
            local p = self.all_img_xiazhuqu[show_pos]
            p.img_bet_coin.label_bet_coin:setString(data.m_betValue*0.01) 
            if is_can_play_safe == true then
                --AudioManager:playSound("BlackJack>insure")
                g_AudioPlayer:playEffect("subgame/point21/sound/insure.mp3")
            end
            is_can_play_safe = false
            local sch = scheduler.performWithDelayGlobal(function() 
                is_can_play_safe = true
            end,2)
            table.insert(self.sch_all, sch)
        else
            if data.pid ==Player:getAccountID() then
                self:checkBuySafe(data)
            end
        end
    else
        TOAST("保险购买失败")
    end

end

function BlackJackGameLayer:checkBuySafe(data)
    print("检测是否还有别的位置要买保险")
    local is_all_buy_safe = true
    self.buy_safe_list[data.pos] = data.pid
    local pos = data.pos
    for k = 1 , 5 do
        pos = self:getNextLoopPos(pos)
        if self.buy_safe_list[pos] == nil then
            --没有买保险的
            local s_pos = g_GameController:getShowPos(pos)
            local p = self.all_player_panel1[s_pos]
            if p and p.pid == nil then --是不是空位
                if self.player_bet_list[pos] == g_GameController:getMyPid() then
                    --自己还有别的没有买保险
                    local img_light = self.all_img_xiazhuqu[s_pos].img_light
                    self:setImgLight(s_pos, true)
                    if img_light.txt then
                        img_light.txt:setVisible(false)
                    end
                    self.cur_safe_pos = pos
                    is_all_buy_safe = false
                    print("还有这个位置没有买保险===============s_pos, pos",s_pos, pos)
                    break
                else
                    print("panel bet not self=",pos,self.player_bet_list[pos])
                end
            else
                print("panel is player============================s_pos,pos=",s_pos,pos)
            end
        else
            print("pos is buy safe=============================pos=",pos)
        end
    end

    for k , v in pairs(self.all_img_xiazhuqu) do
        self:setImgLight(k, false)
    end
    if is_all_buy_safe == true then
        local pid = self.player_bet_list[data.pos]
        self:showBtnMode(WAIT_MODE)
        for k , panel in pairs(self.all_player_panel1) do
            if panel.pid ~= nil and panel.pid == data.pid then
                self:removeProgress(panel)    
            end
        end
    else
        for k , panel in pairs(self.all_player_panel1) do
            self:removeProgress(panel)  --先移除所有计时器
        end
        self:showBtnMode(SAFE_MODE)
        self:showCountDown(true)
        local tmp_pos = g_GameController:getShowPos(self.cur_safe_pos)
        self:setImgLight(tmp_pos, true)
    end
end

--提示下注闪光动画
function BlackJackGameLayer:showLightAction(show_pos)
    -- print("self.room_state.loop_pos=" .. self.room_state.loop_pos)
    -- local show_pos = g_GameController:getShowPos(self.room_state.loop_pos)
    local panel = self.all_player_panel1[show_pos]
    if panel then --是额外的位置
        self.all_img_xiazhuqu[show_pos].img_light.txt:setVisible(false)
        self:setImgLight(show_pos, true)
        local fade = cc.FadeIn:create(1)
        local fadeout = cc.FadeOut:create(1)
        local seq = cc.Sequence:create(fade, fadeout)
        local rep = cc.RepeatForever:create(seq)
        self.all_img_xiazhuqu[show_pos].img_light:runAction(rep)
    end
end

function BlackJackGameLayer:stateBet()
    --AudioManager:playSound("BlackJack>startbet")
    g_AudioPlayer:playEffect("subgame/point21/sound/startbet.mp3")
    self:showBtnMode(BET_MODE)
    self:setParentBtnPanelVisible(true)
    self:showCountDown(true)
--    --把所有玩家设置显示
--    local all_player_data = g_GameController:getAllPlayerData()
--    for k , player in pairs(all_player_data) do
--        local show_pos = g_GameController:getShowPos(player.pos)
--        local p = self.all_player_panel1[show_pos] 
--        if p then
--            if p.pid == nil then
--                self:eventAddPlayer({pid=player.pid})
--            end
--        end
--    end
    local loop_start = self.room_state.loop_start

    local pos_list = {}
    local index = 1
    -- 有玩家的位置记录下标
    -- 如 10:1 20:2
    for k = 1 , 5 do
        loop_start = self:getNextLoopPos(loop_start)
        local tmp_pos = g_GameController:getShowPos(loop_start)
        if self.all_player_panel1[tmp_pos].pid ~= nil then
            pos_list[loop_start] = index
            index = index + 1
        end
    end
    self:showLightAction(3)
    self.all_img_xiazhuqu[3].img_light.txt:setVisible(true)

    self.no_bet_list_pos = {} --未下注座位列表
    local last_pos = nil
    for k , p in ipairs(self.all_player_panel1) do
        if p.pid ~= nil then
            p:setVisible(true)
            p.label_index:setString(pos_list[p.pos])
            last_pos = p.pos
        else
            if last_pos ~= nil then
                p.pos = last_pos - 10
                if p.pos <= 0 then
                    p.pos = 50
                end
                last_pos = p.pos
                self.no_bet_list_pos[k] = true
            end
        end
    end

    last_pos = nil
    for k = #self.all_player_panel1 , 1 , -1 do
        local p = self.all_player_panel1[k]
        if p.pid ~= nil then
            last_pos = p.pos
        else
            if last_pos ~= nil then
                p.pos = last_pos + 10
                if p.pos > 50 then
                    p.pos = 10
                end
                last_pos = p.pos
                self.no_bet_list_pos[k] = true
            end
        end
    end

    for k , p in ipairs(self.all_player_panel1) do
        print("p.pos = ",p.pos)
    end
    dump(self.no_bet_list_pos)
end


--发牌
function BlackJackGameLayer:sendCard(pos,card)
    local x,y
    local node = nil
    local card_num = 0
    print("pos,card=",pos,card)
    local node_card
    local show_pos = g_GameController:getShowPos(pos)
    if pos == 0 then
        x,y = self.node_card_zj:getPosition()
        node_card = self.zj_node_card
    else
        self.all_node_card1[pos] = self.all_node_card1[pos] or {}
        node_card = self.all_node_card1[pos]
        x,y = self.all_img_xiazhuqu[show_pos].node_card:getPosition()

        if self.split_list[show_pos] ~= nil then
            --这个位置是分牌的
            if pos%10 == 1 then --左边的
                x = x - 50
            else
                --右边的
                x = x + 50
            end
        end
    end
    card_num = #node_card + 1
    if card_num > 1 then
        x = x + (card_num-1)*17 - (card_num-2)*10
        --17
        --21
        --25
        --29
        if self.split_list[show_pos] ~= nil then
            if pos%10 == 0 then
                x = x - 3
            end
        end
    end
    local function action_end_callback()
        if card then  --除了庄家的牌，都翻起来
            card = blackjack_util.getCardColor(card )
            if card.color > 0 and card.size > 0 then
                local aniInfo = node.ac:getAnimationInfo("animation1")  
                node.ac:gotoFrameAndPlay(0, 20, false)
                local function setLastFrameCallFunc()
                    node.ac:gotoFrameAndPause(20)
                end
                 local sch = scheduler.performWithDelayGlobal(setLastFrameCallFunc, 1/3)
                table.insert(self.sch_all, sch)
--                node.ac:setLastFrameCallFunc(function() 
--                    node.ac:gotoFrameAndPause(20)


--                end)
                 if pos%10 == 1 and self.room_state.loop_pos == (pos-1) then --分牌的，
                        node.img_Poker:setColor(cc.c3b(70,70,70))
                        node.is_black = true
                    end
                self:showCardValue(pos)
           --     self:checkCardValue(card.size, pos)
                self:changeOneCard(card, node)
                
            end
        end
        if card_num > 1 then
            for k , v in ipairs(node_card) do
                v:runAction(cc.MoveBy:create(0.2, cc.p(-7,0)))
            end
        end
        --AudioManager:playSound("DoubleBullUI>deal")
    end
    if card_num == 1 then
        node = self:createNodeCard(x+3,y,action_end_callback)
    else
        node = self:createNodeCard(x,y,action_end_callback)
    end
    node_card[card_num] = node

end
 

--显示牌值
function BlackJackGameLayer:showCardValue(pos)
    print("============showCardValue==============")
    local img_index = 2
    if pos%10 == 1 then
        img_index = 1
    end
    local show_pos = g_GameController:getShowPos(pos)

    if self.all_card_value[pos] then
        local val1 = self.all_card_value[pos][1]
        local val2 = self.all_card_value[pos][2]
        if val1 == nil then
            return
        end
        local str =nil
        if val2==nil then
            str =val1
        else
             str = val1 .. "/" .. val2 
        end
       
        local img = nil
        local node_card = nil

        if pos == 0 then
            node_card = self.node_card_zj
            img = node_card.img_card_value
            if val1 and val1 > 21 then
                node_card.img_baozha:setVisible(true)
            end
        else
            local p = self.all_img_xiazhuqu[show_pos]
            node_card = p.node_card
            img = node_card.img_card_value[img_index]
            if val1 > 21 then
                node_card.img_baozha[img_index]:setVisible(true)
            end
        end
        img:setVisible(true)
        img.txt:setVisible(true)
        img.txt:setString(str)
        if val1 > 21 then
            img:loadTexture("point21_qp_hong1.png", ccui.TextureResType.plistType)
            if is_result ~= true then
                local endpos = cc.p(0,0)
                if self.split_list[show_pos] == true then
                    --这个位置有分牌
                    if img_index == 1 then --左边
                        endpos.x = endpos.x - 50
                    else
                        endpos.x = endpos.x + 50
                    end
                end
                g_AudioPlayer:playEffect("subgame/point21/sound/Boom.mp3") 
                common_util:get_animation(node_card, "subgame/point21/point21_baozha.csb", 0,61, endpos, function(node) 
                    node:removeFromParent()
                    node = nil
                end)
                if self.player_bet_list[pos] == g_GameController:getMyPid() then
                    --是自己爆牌了
                    print("是自己爆牌了")
                    if self.room_state.game_step == ROOM_STATE_PLAYER_LOOP and self.room_state.loop_pos == pos then
                        --是自己操作
                  --      self:reqPlayerLoop(LOOP_STOP, pos)
                    end
                end
            end
        else
            if val1 == 21 or val2 == 21 then
                if pos == 0 then --庄家
                    print("zhuangjia 21dian============>",val1,val2)
                    if #self.zj_node_card > 2 and #self.zj_node_card < 5 then
                        if not self.play_21dian[pos] then
                            --AudioManager:playSound("BlackJack>21dian")
                             g_AudioPlayer:playEffect("subgame/point21/sound/21dian.mp3") 
                            self.play_21dian[pos] = true
                        end
                        
                    end
                else
                    print("xianjiajia 21dian============>",val1,val2)
                    if #self.all_node_card1[pos] > 2 and #self.all_node_card1[pos] < 5 then
                        if not self.play_21dian[pos] then
                             g_AudioPlayer:playEffect("subgame/point21/sound/21dian.mp3") 
                            --AudioManager:playSound("BlackJack>21dian")
                            self.play_21dian[pos] = true
                        end
                    end
                end
                if self.player_bet_list[pos] == g_GameController:getMyPid() then
                    print("自己已经21点了")
                    if self.room_state.game_step == ROOM_STATE_PLAYER_LOOP and self.room_state.loop_pos == pos then
                        --是自己操作
                     --   self:reqPlayerLoop(LOOP_STOP, pos)
                    end
                end
                img:loadTexture("point21_qp_lv1.png", ccui.TextureResType.plistType)
            else
                img:loadTexture("point21_qp_huang1.png", ccui.TextureResType.plistType)
            end
            local card = g_GameController:getCardData(pos)
            if pos == 0 then --庄家
                if #self.zj_node_card >= 5 then
                    img:setVisible(false)
                    img.txt:setVisible(false)
                    img.txt:setString(str)
                    if is_result ~= true then
                        local node = common_util:get_animation(node_card, "subgame/point21/point21_wuxiaolong.csb", 0,20, cc.p(5,-20), function () end)
                        node_card.node_wxl = node
                         g_AudioPlayer:playEffect("subgame/point21/sound/Dragon.mp3") 
                        --AudioManager:playSound("BlackJack>Dragon")
                    end
                elseif #self.zj_node_card == 2 then     
                    if val1 == 21 or val2 == 21 then       
                        img:setVisible(false)
                        img.txt:setVisible(false)
                        if node_card.node_blackjack == nil then
                            node_card.node_blackjack = true
                            local function play_blackjack_callback()
                                local node_blackjack
                                node_blackjack = common_util:get_animation(node_card, "subgame/point21/point21_blackjack.csb", 21,90, cc.p(0,-20), function() 
                                    node_card.node_blackjack = node_blackjack
                                end)
                            end
                            local node = display.newSprite("subgame/point21/point21/blackjack.png")
                            node_card:addChild(node)
                            node:setScale(0.4)
                            node:setPosition(0,-20)
                            node_card.node_blackjack = node
--                            node = common_util:get_animation(node_card, "subgame/point21/point21_blackjack.csb", 0,20, nil, function () 
--                                node:removeFromParent()
--                                play_blackjack_callback()
--                            end)
                            g_AudioPlayer:playEffect("subgame/point21/sound/BlackJack.mp3") 
                            --AudioManager:playSound("BlackJack>BlackJack")
                        end
                    end
                end
            else
                if #self.all_node_card1[pos] >= 5 then --五小龙
                    img:setVisible(false)
                    img.txt:setVisible(false)
                    img.txt:setString(str)
                    if is_result ~= true then
                        local endpos = cc.p(0,-20)
                        if self.split_list[show_pos] == true then
                            --这个位置有分牌
                            if img_index == 1 then --左边
                                endpos.x = endpos.x - 50
                            else
                                endpos.x = endpos.x + 50
                            end
                        end
                        if node_card.node_wxl == nil then
                            local node = common_util:get_animation(node_card, "subgame/point21/point21_wuxiaolong.csb", 0,20, endpos, function () end)
                            node_card.node_wxl = node
                            g_AudioPlayer:playEffect("subgame/point21/sound/Dragon.mp3") 
                            --AudioManager:playSound("BlackJack>Dragon")
                           
                        end
                        
                        if self.player_bet_list[pos] == g_GameController:getMyPid() then
                            --是自己爆牌了
                            print("是自己已经有五张牌了")
                      --      self:reqPlayerLoop(LOOP_STOP)
                        end
                    end
                elseif #self.all_node_card1[pos] == 2 then 
                    if val1 == 21 or val2 == 21 then
                        img:setVisible(false)
                        img.txt:setVisible(false)
--                        if node_card.node_blackjack == nil then
--                            node_card.node_blackjack = true
                            local x,y = node_card.img_double[pos%10+1]:getPosition()
                            local function play_blackjack_callback()
                                
                                local node_blackjack
                                node_blackjack = common_util:get_animation(node_card, "subgame/point21/point21_blackjack.csb", 21,90, cc.p(x,y+20), function() 
                                    node_card.node_blackjack = node_blackjack
                                end)
                            end
                            local node = display.newSprite("subgame/point21/point21/blackjack.png")
                            node_card:addChild(node)
                            node:setScale(0.4)
                             local endpos = cc.p(0,-20)
                            if self.split_list[show_pos] == true then
                                --这个位置有分牌
                                if img_index == 1 then --左边
                                    endpos.x = endpos.x - 50
                                else
                                    endpos.x = endpos.x + 50
                                end
                            end
                            node:setPosition(endpos)
                            if  node_card.node_blackjack == nil then
                                node_card.node_blackjack = node
                            else
                              node_card.node_blackjack1 = node
                            end
--                            node = common_util:get_animation(node_card, "subgame/point21/point21_blackjack.csb", 0,20, cc.p(x,y+20), function () 
--                                node:removeFromParent()
--                                play_blackjack_callback()
--                            end)
                            g_AudioPlayer:playEffect("subgame/point21/sound/BlackJack.mp3") 
                            --AudioManager:playSound("BlackJack>BlackJack")
                               if self.player_bet_list[pos] == g_GameController:getMyPid() then
                                    table.insert(self.player_operate_list,pos)
                                end
                       -- end
                    end
                end
            end
        end
    end
end

function BlackJackGameLayer:cleanAll()
    self.play_21dian = {}
    self.player_operate_list = {} 
    for k , v in pairs(self.all_img_xiazhuqu) do
        if v.node_card.node_wxl then
            v.node_card.node_wxl:removeFromParent()
            v.node_card.node_wxl = nil
        end
        if v.node_card.node_blackjack and v.node_card.node_blackjack ~= true then
            v.node_card.node_blackjack:removeFromParent()
        end
        v.node_card.node_blackjack = nil
        if v.node_card.node_blackjack1 and v.node_card.node_blackjack1 ~= true then
            v.node_card.node_blackjack1:removeFromParent()
        end
        v.node_card.node_blackjack1 = nil
        if v.node_card.img_card_value[1] then
            v.node_card.img_card_value[1]:setVisible(false)
            if v.node_card.img_card_value[1].txt then
                v.node_card.img_card_value[1].txt:setVisible(false)
            end
        end
        if v.node_card.img_card_value[2] then
            v.node_card.img_card_value[2]:setVisible(false)
            if v.node_card.img_card_value[2].txt then
                v.node_card.img_card_value[2].txt:setVisible(false)
            end
        end
        if v.node_card.img_double[1] then
            v.node_card.img_double[1]:setVisible(false)
            v.node_card.img_double[1]:setPosition(v.node_card.img_double[1].pos.x,v.node_card.img_double[1].pos.y)
        end
        if v.node_card.img_double[2] then
            v.node_card.img_double[2]:setVisible(false)
            v.node_card.img_double[2]:setPosition(v.node_card.img_double[2].pos.x,v.node_card.img_double[2].pos.y)
        end
        if v.node_card.img_baozha[1] then
            v.node_card.img_baozha[1]:setVisible(false)
            v.node_card.img_baozha[1]:setPosition(v.node_card.img_baozha[1].pos.x,v.node_card.img_baozha[1].pos.y)
        end
        if v.node_card.img_baozha[2] then
            v.node_card.img_baozha[2]:setVisible(false)
            v.node_card.img_baozha[2]:setPosition(v.node_card.img_baozha[2].pos.x,v.node_card.img_baozha[2].pos.y)
        end

        if v.img_bet_coin then
            v.img_bet_coin:setVisible(false)
        end

        if v.img_light then
            v.img_light:setVisible(false)
            v.img_light:stopAllActions()
            v.img_light:setOpacity(255)
            if v.img_light.img_name then
                v.img_light.img_name:setVisible(false)
            end
        end

    end

    for k , sch in pairs(self.sch_all) do
        if sch then
            scheduler.unscheduleGlobal(sch)
            sch = nil
        end
    end
    self.sch_all = {}
    if self.node_card_zj.img_card_value then
        self.node_card_zj.img_card_value:setVisible(false)
    end

    if self.node_card_zj.img_baozha then
        self.node_card_zj.img_baozha:setVisible(false)
    end
    self.is_req_loop_bet_stop = false
    if self.node_card_zj.node_wxl then
        self.node_card_zj.node_wxl:removeFromParent()
        self.node_card_zj.node_wxl = nil
    end
    if self.node_card_zj.node_blackjack and self.node_card_zj.node_blackjack ~= true then
        self.node_card_zj.node_blackjack:removeFromParent()
    end
    self.node_card_zj.node_blackjack = nil
    for k , list in pairs(self.all_node_card1) do
        for _, node in pairs(list) do
            if node then
                node:removeFromParent()
            end
        end
    end

    for k , node in pairs(self.zj_node_card) do
        if node then
            node:removeFromParent()
        end
    end

    for k , panel in pairs(self.all_player_panel1) do
        panel:setVisible(false)
        panel.pid = nil
        panel.pos = nil
        panel.yazhu = {}
        panel.label_index:setString("")
        panel.node_fenshu.node.ac:gotoFrameAndPause(0)
    end

    local all_jetton = g_GameController:getAllJetton()
    for k , jetton in pairs(all_jetton) do
        print("jetton======,",jetton)
        if jetton then
            jetton:removeFromParent()
        end
        
    end
    g_GameController:clearJetton() 
    self.is_bet_over = false
    self.split_list = {}        
    self.self_bet = false
    self.req_loop_stop = {}
    self.player_bet_list = {}   --记录保存位置对应的下注玩家id
    self.no_bet_list_pos = {}   --空位还可下注的区域
    self.all_node_card1 = {}     --存牌
    self.zj_node_card = {}
    self.buy_safe_list = {}     --购买的保险列表
    self.all_card_value = {}
    self.chip_offset_y = {}
    self.chip_pos = {}
    common_util.jettons_node = {}
    self.btn_stop.cheStop:setSelected(false)
    -- common_util.remove_jetton()
end

--玩家下注的是否都操作完了
function BlackJackGameLayer:isMyBetDone(  )
    dump(self.player_bet_list,"player_bet_list")
    dump(self.player_operate_list,"player_operate_list")
    local list = {}
    for loop_pos ,pid in pairs(self.player_bet_list) do
        if pid == Player:getAccountID() then
            for _,pos in pairs(self.player_operate_list) do
                if pos == loop_pos then
                    list[pos] = "Done"
                end
            end

        end
    end
    
    dump(list,"list")
    for loop_pos ,pid in pairs(self.player_bet_list) do
        if pid == Player:getAccountID() then
            if list[loop_pos] == nil then
                return false
            end
        end
    end
    return true
end

function BlackJackGameLayer:stopAllSch()

end
local cardcolor = {
    [1] = "poker_flower_a.png",
    [2] = "poker_flower_b.png",
    [3] = "poker_flower_c.png",
    [4] = "poker_flower_d.png",
    [11] = "poker_pic_r_j.png",
    [12] = "poker_pic_r_q.png",
    [13] = "poker_pic_r_k.png",
}

local cardsize = {
    [1] = {red = "poker_r_1.png", black = "poker_b_1.png"},
    [2] = {red = "poker_r_2.png", black = "poker_b_2.png"},
    [3] = {red = "poker_r_3.png", black = "poker_b_3.png"},
    [4] = {red = "poker_r_4.png", black = "poker_b_4.png"},
    [5] = {red = "poker_r_5.png", black = "poker_b_5.png"},
    [6] = {red = "poker_r_6.png", black = "poker_b_6.png"},
    [7] = {red = "poker_r_7.png", black = "poker_b_7.png"},
    [8] = {red = "poker_r_8.png", black = "poker_b_8.png"},
    [9] = {red = "poker_r_9.png", black = "poker_b_9.png"},
    [10] = {red = "poker_r_10.png", black = "poker_b_10.png"},
    [11] = {red = "poker_r_11.png", black = "poker_b_11.png"},
    [12] = {red = "poker_r_12.png", black = "poker_b_12.png"},
    [13] = {red = "poker_r_13.png", black = "poker_b_13.png"}, 
}
function BlackJackGameLayer:get_cardres(color, size)
    local color_b = cardcolor[color]
    local size_res = cardsize[size].red
    local color_s = cardcolor[color]
    if color == 2 or color == 4 then
        size_res = cardsize[size].black
    end

    if size > 10 then
        color_b = cardcolor[size]
    end

    local color = {}
    color.b = color_b
    color.s = color_s
    return color, size_res
end
--设置单张牌的数据
function BlackJackGameLayer:changeOneCard(card,node)
    local color, size = self:get_cardres(card.color,card.size)
    node.hb:loadTexture(color.b,ccui.TextureResType.plistType)
    node.hs:loadTexture(color.s,ccui.TextureResType.plistType)
    node.sz:loadTexture(size,ccui.TextureResType.plistType)
end

function BlackJackGameLayer:createNodeCard(to_x,to_y,callback)
    local node = cc.uiloader:load("common/poker_rotate.csb")
    node.img_Poker = cc.uiloader:seekNodeByName(node, "img_Poker")
    node.hb    = cc.uiloader:seekNodeByName(node, "ps_Huase")
    node.hs    = cc.uiloader:seekNodeByName(node, "ps_Huase_s")
    node.sz    = cc.uiloader:seekNodeByName(node, "ps_Shuzi")
    node.ac    = cc.CSLoader:createTimeline("common/poker_rotate.csb")
    node:setScale(0.3)
    node:setRotation(30)

    local size = self.img_fapaiqi:getContentSize()
    local x,y = self.img_fapaiqi:getPosition()
    node:setPosition(x - size.width/4,y - size.height/4)
    node:runAction(node.ac)
    node.ac:gotoFrameAndPause(0)
    --旋转动作
    --变大动作
    local t = 0.2
    local r = 180
    local s = 1
    local function action_end_callback()
        if callback then
            callback()
        end
    end
    local action_rotate1 = cc.RotateTo:create(t/2,r)
    local action_rotate2 = cc.RotateTo:create(t/2,r*2)
    local action_seq = cc.Sequence:create(action_rotate1,action_rotate2)
    local action_sacle = cc.ScaleTo:create(t,0.5)
    local action_move = cc.MoveTo:create(t,cc.p(to_x+25,to_y))
    local action_spawn = cc.Spawn:create(action_seq,action_sacle,action_move)
    local action_callback = cc.CallFunc:create(action_end_callback)
    local action = cc.Sequence:create(action_spawn,action_callback)
    node:runAction(action)

    self.pnl_desk:addChild(node)
    return node
end

--创建筹码
function BlackJackGameLayer:throw_jetton(pos,coin,is_safe)
    local x,y = 0,0
    if pos ~= 0 then
        local pid = self.player_bet_list[pos]
        local show_pos = g_GameController:getPlayerShowPos(pid)
        local panel = self.all_player_panel1[show_pos]
        -- print("throw_jetton show_pos =" , show_pos)
        local size = panel:getContentSize()
        x,y = panel:getPosition()
        x = x + size.width/2
        y = y + size.height/2
    else
        x,y = self.node_coin_pos_zj:getPosition()
    end
    local jetton =display.newSprite()
   -- local jetton, is_repeat = self.sp_jetton:clone()
    jetton:setPosition(x,y)
    local res = blackjack_util:get_jetton_res(coin)
    jetton:setSpriteFrame(res)

    self.node_jetton:addChild(jetton)
    if is_safe ~= true then
        g_GameController:addJetton(jetton)
    end
    return jetton
end

--显示当前要操作的区域
function BlackJackGameLayer:showLight()
    print("self.room_state.loop_pos=" .. self.room_state.loop_pos)
    local show_pos = g_GameController:getShowPos(self.room_state.loop_pos)
    local panel = self.all_player_panel1[show_pos]
    if panel and panel.pid == nil then --是额外的位置
        -- self.all_img_xiazhuqu[show_pos].img_light:setVisible(true)
        self:setImgLight(show_pos, true)
        self.all_img_xiazhuqu[show_pos].img_light.txt:setVisible(false)
    end
end


--显示倒计时转圈
function BlackJackGameLayer:showCountDown(is_all)
    if is_all == nil then
        is_all = false
    end
    local pid = self.player_bet_list[self.room_state.loop_pos]
    for k , panel in pairs(self.all_player_panel1) do 
        if is_all == true or panel.pid == pid then
            --该玩家操作
            local cur_value = 100
            self:removeProgress(panel)

            local Sprite_23 = cc.Sprite:createWithSpriteFrameName("point21_ps_jdt.png")
            local progress = cc.ProgressTimer:create(Sprite_23)
            -- progress:setColor(cc.c3b(0,0,0))
            progress:setType(display.PROGRESS_TIMER_RADIAL)
            local size = panel:getContentSize()
            progress:setPosition(size.width/2,size.height/2)

            panel.player_panel:addChild(progress)
            panel.progress = progress
            progress:setReverseDirection(true)

            panel.progress:setPercentage(100)
            panel.progress:setVisible(true)
            local cur_t = self.room_state.m_leftTime
            local all_t = 7
            local p = cur_t/all_t * 100
            print("cur_t,all_t,p=",cur_t,all_t,p)
            panel.progress:setPercentage(p)
            local action = cc.ProgressTo:create(cur_t,0)
            local t = 0
            local function progress_callback()
                if panel.progress then
                    local per = panel.progress:getPercentage()
                    local color = nil
                    if per >= 51 then
                        color = cc.c3b(81,255,81)
                    elseif per >= 31 then
                        color = cc.c3b(255,234,3)
                    else
                        color = cc.c3b(255,3,3)
                    end
                    if t == math.ceil(t) and panel.pid == g_GameController:getMyPid() then
                        if per <= 30 and per >= 10 then 
                            --AudioManager:playSound("BlackJack>Timecd")
                        elseif per < 10 then
                            --AudioManager:playSound("BlackJack>Timeout")
                        end
                    end
                    progress:setColor(color)
                    t = t + 0.5

                end
            end
            print("显示倒计时转圈=",panel.pos)
            print("1111111111111111111111111111111111111")
            panel.progress.sch = scheduler.scheduleGlobal(progress_callback,0.5)
            progress_callback()
            table.insert(self.sch_all,panel.progress.sch)
            local function callback()
                if self.room_state.game_step == ROOM_STATE_BET then --在押注阶段
                    if g_GameController:isBet() then
                        self.lasttime_bet_coin =g_GameController:getRoomState().m_lastBet
                        print("默认帮玩家下注")
                        self:reqBet(self.lasttime_bet_coin)
                    end
                    local mydata = g_GameController:getMyData()
                    -- self.select_pos = mydata.pos
                    self:setSelectPos(mydata.m_chairId)
                    print("快结束了,默认帮玩家发送下注完成")
                  --  self:reqPlayerLoop(LOOP_STAKE_STOP)
                end
                if panel.progress.sch then
                    common_util.stopSch( panel.progress.sch )
                    panel.progress.sch  = nil
                end
            end
            local seq = cc.Sequence:create(action,cc.CallFunc:create(callback))
            panel.progress:runAction(seq)
        else
            self:removeProgress(panel)
        end
    end

end


function BlackJackGameLayer:removeProgress(panel)
    if panel and panel.progress then
        if panel.progress.sch then
            scheduler.unscheduleGlobal(panel.progress.sch)
            panel.progress.sch = nil
        end
        panel.progress:removeFromParent()
        panel.progress = nil
    end
    if panel and type(panel.pos) ~= "function" then
        print("removeProgress panel.pos=",panel.pos)
        local show_pos = g_GameController:getShowPos(panel.pos)
        print("removeProgress show_pos=",show_pos)
        -- if self.all_img_xiazhuqu[show_pos].img_light then
        --     self.all_img_xiazhuqu[show_pos].img_light:setVisible(false)
        -- end
    else
        dump(panel)
        print("333333333333333333333333333333")
    end
end

--抛筹码动画
function BlackJackGameLayer:throw_jettons(pos, nodes, epos, is_safe)
    local delayTime = 0
    print("抛筹码动画=",pos)
    local show_pos = g_GameController:getShowPos(pos)
    if self.split_list[show_pos] == true then
        if pos%10 ~= 1 then
            epos.x = epos.x + 40
            print("右边的===================",epos.x)
        else
            epos.x = epos.x - 40
            print("左边的===================",epos.x)
        end
    end
    print("中间的=======================",epos.x)
    epos.y = epos.y - 10
    self.chip_offset_y[pos] = self.chip_offset_y[pos] or 0
    for k,v in pairs(nodes) do
        if g_GameController.is_recover == true then
            local zorder = g_GameController:getBetOrder()
            v:setLocalZOrder(zorder)
            self.chip_offset_y[pos] = self.chip_offset_y[pos] + 1
            local tmp = cc.p(epos.x,epos.y+self.chip_offset_y[pos])
            v:setPosition(tmp)
        else
            local sch = scheduler.performWithDelayGlobal(function (  )
                local zorder = g_GameController:getBetOrder()
                v:setLocalZOrder(zorder)
                local function remove_callback()
                    if is_safe == true then
                        v:removeFromParent()
                    end
                end
                self.chip_offset_y[pos] = self.chip_offset_y[pos] + 1
                local tmp = cc.p(epos.x,epos.y+self.chip_offset_y[pos]) --epos.y = epos.y + self.chip_offset_y[pos]
                --AudioManager:playSound("BlackJack>Bet")
                blackjack_util.throw_jetton(v, tmp, nil, remove_callback)
            end, delayTime)

            table.insert(self.sch_all, sch)
            delayTime = delayTime + 0.5/#nodes
        end
    end
end


function BlackJackGameLayer:setSelectPos(select_pos)
    self.select_pos = select_pos
end


function BlackJackGameLayer:onSynMyCoin()
--    local my_coin = GameModel:getPlayerData().coin
--    print("onSynMyCoin=========",my_coin)
--    print("=========>>>>>>>>>>>")
--    common_util.setCoinStr(self.all_player_panel1[3].label_coin, common_util.tansCoin(my_coin), 2)
--    self:eventUpdateBank()
end
 

function BlackJackGameLayer:onDelayNotice( event )
    local info = event.data
    table.insert(self.delayNoticeList,info)
end

function BlackJackGameLayer:onShowNotice()
    self.notice:setVisible(true)
end

function BlackJackGameLayer:onHideNotice()
    self.notice:setVisible(false)
end


function BlackJackGameLayer:showRule()
    if self.rule_view == nil then
        self.rule_view = cc.uiloader:load("subgame/point21/point21_ruler.csb")
        self:addChild(self.rule_view,1001)
        self.rule_view:setContentSize(cc.size(display.width,display.height))
           local center = self.rule_view:getChildByName("Layer") 
         local diffY = (display.size.height - 750) / 2
        self.rule_view:setPosition(cc.p(0,diffY))
     
        local diffX = 145-(1624-display.size.width)/2 
        center:setPositionX(diffX)
        --ccui.Helper:doLayout(self.rule_view)
--        if (self._winSize.width / self._winSize.height > 1.78) then 
--		    rule_view:setScale(display.scaleX, display.scaleY);
--           -- self.node:setPosition(self._winSize.width/display.scaleX / 2,self._winSize.height / 2); 
--	    end
        local panel_mask = cc.uiloader:seekNodeByName(self.rule_view, "Panel_1")
        local function panel_mask_callback(sender, eventType)
            if eventType == ccui.TouchEventType.ended then
                self:hideRule()
            end
        end
        panel_mask:addTouchEventListener(panel_mask_callback)
        local btnClose = cc.uiloader:seekNodeByName(self.rule_view, "btnClose")
        btnClose:addTouchEventListener(panel_mask_callback)
        local all_panel = {}
        local all_btn = {}
        for i = 1 , 3 do
            all_panel[i] = cc.uiloader:seekNodeByName(self.rule_view, "pnlRuler" .. i)
            all_btn[i] = cc.uiloader:seekNodeByName(self.rule_view, "game_rule" .. i)
            all_btn[i].lbl1 = cc.uiloader:seekNodeByName(all_btn[i],"lbl1")
            all_btn[i].lbl2 = cc.uiloader:seekNodeByName(all_btn[i],"lbl2")
             all_btn[i].bg = cc.uiloader:seekNodeByName(all_btn[i],"bg")
            if i ~= 1 then
                all_panel[i]:setVisible(false)
            else
               -- all_btn[i]:setEnabled(false)
             --   all_btn[i]:setBright(false)
                all_btn[i].lbl1:setVisible(true)
                all_btn[i].lbl2:setVisible(false)
                 all_btn[i].bg:setVisible(true)
            end
            local function btn_game_rule(sender, eventType)
                if eventType == ccui.TouchEventType.ended then
                     for k , panel in pairs(all_panel) do
                        all_btn[k].bg:setVisible(false)
                        all_btn[k].lbl1:setVisible(false)
                        all_btn[k].lbl2:setVisible(true)
                        panel:setVisible(false)
                    end
                    all_panel[i]:setVisible(true)
                    sender.lbl1:setVisible(true)
                    sender.lbl2:setVisible(false)
                    sender.bg:setVisible(true) 
                end
            end
            all_btn[i]:addTouchEventListener(btn_game_rule)
        end

    end
    self.rule_view:setVisible(true)
end

function BlackJackGameLayer:recoverGame()
    local all_player_data = g_GameController:getAllPlayerData()
    local room_state = g_GameController:getRoomState()
    self.room_state = room_state
    local loop_start = room_state.loop_start
    self.label_min_coin:setVisible(true)
    self.label_min_coin:setString(self.room_state.m_minLimit*0.01)
     local room_type_res = {
       [205001] = "common_ptc.png", 
       [205002] = "common_lbc.png",
       [205003] = "common_fhc.png",
    }
     self.img_room_name:setVisible(true)
    local png = room_type_res[g_GameController.m_gameAtomTypeId]
    self.img_room_name:loadTexture(png,ccui.TextureResType.plistType)
    dump(room_state,"房间状态")
    local max = 0
    for k , player in pairs(all_player_data) do
        for k , v in pairs(player.yazhu) do
            self.player_bet_list[v.pos] = v.pid
            max = max + 1
            --有分牌
            if v.pos%10 == 1 then
                local show_pos = g_GameController:getShowPos(v.pos)
                self.split_list[show_pos] = true
            end
        end
    end


    dump(self.split_list,"分牌的有哪些")
    local loop_list = {}        --10:1 20:2
    local index = 1
    if max < 5 then
        max = 5
    end
    for k = 1 , max do
        -- local tmp_pos = g_GameController:getShowPos(loop_start)
        if self.player_bet_list[loop_start] ~= nil then
            loop_list[loop_start] = index
            index = index + 1
        end
        local show_pos = g_GameController:getShowPos(loop_start)
        if self.split_list[show_pos] == true then --这个有分牌
            if loop_start%10 == 0 then
                loop_start = loop_start + 1
            else
                loop_start = self:getNextLoopPos(loop_start-1)
            end
        else
            loop_start = self:getNextLoopPos(loop_start)
        end
    end
    dump(loop_list,"操作顺序列表")
    for k , player in pairs(all_player_data) do
        for k , v in pairs(player.yazhu) do
            local jetton_nodes = {}
            local show_pos = g_GameController:getShowPos(v.pos)
            local coin = v.m_betValue*0.01
            local jettons = blackjack_util:getJettons(coin)

            --筹码
            print("复原筹码")
            local data = {
                pid = v.pid,
                yazhu = v.times,
                pos = v.pos,
                code = 0,
            }
            dump(event,"发牌的时候默认下注")
            g_GameController:setMyBetInfo(data)       --默认下注，服务器没有推送，只能前端做 有可能分牌的被覆盖?
            local p = self.all_img_xiazhuqu[show_pos]
            p.img_bet_coin:setVisible(true)
            p.img_bet_coin.label_bet_coin:setString(coin) 
            local x,y = p.node_coin_pos:getPosition()
            self:showBetChips(data.pos, cc.p(x,y), coin) 
             local node_card = p.node_card
             if  v.m_hasDouble ==1 then
                node_card.img_double[data.pos%10+1]:setVisible(true)
            end
            print("复原牌")
            self.all_node_card1[v.pos] = self.all_node_card1[v.pos] or {}
            for i , card in ipairs(v.cards) do
--                if i == 3 then
--                    local cur_index = loop_list[room_state.loop_pos]
--                    print("第三张牌了,当前操作的索引是=",cur_index, loop_list[v.pos])

--                    if cur_index < loop_list[v.pos] then
--                        print("还没有轮到这个操作=",v.pos,v.pid,player.m_nickName)
--                        break
--                    end
--                end
                local new_card = blackjack_util.getCardColor(card)

                --牌
                local node_card = self.all_node_card1[v.pos]
                local x,y = self.all_img_xiazhuqu[show_pos].node_card:getPosition()
                if self.split_list[show_pos] ~= nil then
                    if v.pos%10 == 1 then
                        x = x - 50
                    else
                        x = x + 50
                    end
                end
                local card_num = #node_card + 1
                if card_num > 1 then
                    x = x + (card_num-1)*17 - (card_num-2)*10
                    if self.split_list[show_pos] ~= nil then
                        if v.pos%10 == 0 then
                            x = x - 3
                        end
                    end
                end
                if card_num == 1 then
                    x = x + 3
                end
                local node = cc.uiloader:load("common/poker_rotate.csb")
                node.img_Poker = cc.uiloader:seekNodeByName(node, "img_Poker")
                node.hb    = cc.uiloader:seekNodeByName(node, "ps_Huase")
                node.hs    = cc.uiloader:seekNodeByName(node, "ps_Huase_s")
                node.sz    = cc.uiloader:seekNodeByName(node, "ps_Shuzi")
                node.ac    = cc.CSLoader:createTimeline("common/poker_rotate.csb")
                node:setScale(0.5)
                -- node:setRotation(30)
                node:setPosition(cc.p(x+28,y))
                node:runAction(node.ac)
                -- node.ac:gotoFrameAndPause(0)
                self.pnl_desk:addChild(node)
                node_card[card_num] = node
                dump(node_card)
                print("----------------")
                dump(self.all_node_card1[v.pos])
                local aniInfo = node.ac:getAnimationInfo("animation1")
                node.ac:gotoFrameAndPause(20)

                self:changeOneCard(new_card, node)
                self.all_card_value[v.pos] = v.m_points
                 self:showCardValue(v.pos)
              
                if card_num > 1 then
                    for k , v in ipairs(node_card) do
                        local tmp_x,tmp_y = v:getPosition()
                        v:runAction(cc.MoveBy:create(0.01, cc.p(-7,0)))
                    end
                end
            end
        end
    end

    --复原庄家的牌
    local cards = g_GameController:getCardData(0)
    if cards then
        for i , card in ipairs(cards) do
            local new_card = blackjack_util.getCardColor(card)

            --牌
            local node_card = self.zj_node_card
            local x,y = self.node_card_zj:getPosition()
            local card_num = #node_card + 1
            if card_num > 1 then
                x = x + (card_num-1)*17 - (card_num-2)*10
            end
            local node = cc.uiloader:load("common/poker_rotate.csb")
            node.img_Poker = cc.uiloader:seekNodeByName(node, "img_Poker")
            node.hb    = cc.uiloader:seekNodeByName(node, "ps_Huase")
            node.hs    = cc.uiloader:seekNodeByName(node, "ps_Huase_s")
            node.sz    = cc.uiloader:seekNodeByName(node, "ps_Shuzi")
            node.ac    = cc.CSLoader:createTimeline("common/poker_rotate.csb")
            node:setScale(0.5)
            -- node:setRotation(30)
            node:setPosition(cc.p(x+28,y))
            node:runAction(node.ac)
            -- node.ac:gotoFrameAndPause(0)
            self.pnl_desk:addChild(node)
            node_card[card_num] = node
            if new_card and new_card.color > 0 and new_card.size > 0 then
                local aniInfo = node.ac:getAnimationInfo("animation1")
                node.ac:gotoFrameAndPause(20)
                self:changeOneCard(new_card, node)
                self.all_card_value[0] = g_GameController.data.m_bankerInfo.m_points
                self:showCardValue(0)
            --    self:checkCardValue(new_card.size, 0)
            end
            if card_num > 1 then
                for k , v in ipairs(node_card) do
                    v:runAction(cc.MoveBy:create(0.01, cc.p(-7,0)))
                end
            end
        end
    end

    if self.player_bet_list[room_state.loop_pos] == g_GameController:getMyPid() then --自己操作
        --是自己操作
        print("还原中。。。是自己操作")
    end

end

function BlackJackGameLayer:hideRule()
    if self.rule_view then
        self.rule_view:setVisible(false)
    end
end

function BlackJackGameLayer:eventResetPipei(event)
    local data = event.data
    dump(event)
    self:hideTips()
    print("已经退出房间了")
    self:setParentBtnPanelVisible(false)
    self:stopAllSch()
    self:updatenBtnMaxCoin()
    self:onSynMyCoin()
    self.btn_ready:setVisible(true)
end

function BlackJackGameLayer:eventReenterGame()
    print("重连游戏")
    --model居然存了界面的节点,重连需要手动清除
    g_GameController:clearData()
    app:enterScene("subgame.point21.blackjackScene")
end



return BlackJackGameLayer


