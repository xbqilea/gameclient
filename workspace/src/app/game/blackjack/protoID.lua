
BEGIN_MODULEID("21点",201400) --[201400 -- 201430)
PSTID_21DOTPLAYERINFO       				=	GENPID(1, '历史记录')
PSTID_21DOTUSERENDINFO        				=	GENPID(2, '主界面玩家信息')
PSTID_21DOTBALANCEDATA        				=	GENPID(3, '其他玩家信息')
PSTID_21DOTSYNCDATA        					=	GENPID(4, '游戏同步信息')
--PSTID_21DOTBETINFO        					=	GENPID(5, '下注信息')
PSTID_21DOTCARDS							= 	GENPID(8, '牌')
PSTID_21DOTUSERTOTALENDINFO					= 	GENPID(9, '结算信息')
--PSTID_21DOTDIM								= 	GENPID(10, '牌面信息')
PSTID_21DOTPART								= 	GENPID(11, '分组信息')

BEGIN_MODULEID("21点",201430) --[201430 -- 201500)
CS_G2C_21DOT_GAMESTATE_NTY					=	GENPID(1, '玩家进入游戏下发信息')
CS_C2G_21DOT_BET_REQ						=	GENPID(2, '下注')
CS_C2G_21DOT_CONTINUELASTBET_REQ			=	GENPID(3, '续压')
CS_G2C_21DOT_BET_NTY						=	GENPID(4, '下注结果')
CS_C2G_21DOT_FINISHBET_REQ					=	GENPID(5, '下注结束')
CS_G2C_21DOT_FINISHBET_NTY					=	GENPID(6, '下注结束响应')
CS_C2G_21DOT_CUT2TWO_REQ					=	GENPID(7, '分牌')
CS_G2C_21DOT_CUT2TWO_NTY					=	GENPID(8, '分牌结果')
CS_C2G_21DOT_INSURE_REQ						=	GENPID(9, '购买保险')
CS_G2C_21DOT_INSURE_NTY						=	GENPID(10, '购买保险响应')
CS_C2G_21DOT_DOUBLEBET_REQ					=	GENPID(11, '加倍')
CS_G2C_21DOT_DOUBLEBET_NTY					=	GENPID(12, '加倍')
CS_C2G_21DOT_STOPSENDCARD_REQ				=	GENPID(13, '停牌')
CS_G2C_21DOT_STOPSENDCARD_NTY				=	GENPID(14, '停牌通知')
CS_C2G_21DOT_MORECARD_REQ					=	GENPID(15, '要牌')
CS_G2C_21DOT_MORECARD_NTY					=	GENPID(16, '要牌结果')
--CS_G2C_21DOT_ENTER_NTY						=	GENPID(16, '玩家进入')
--CS_G2C_21DOT_LEAVE_NTY						=	GENPID(17, '玩家离开')
CS_G2C_21DOT_BEGIN_NTY						=	GENPID(18, '游戏开始, 开始下注')
CS_G2C_21DOT_SENDCARD_NTY					=	GENPID(19, '发牌')
CS_G2C_21DOT_BUYINSURE_NTY					=	GENPID(20, '发牌')
CS_G2C_21DOT_ACTION_NTY						=	GENPID(21, '通知玩家开始操作')
CS_G2C_21DOT_GAMEEND_NTY					=	GENPID(22, '游戏结束')
SS_M2G_21DOT_GAMECREATE_REQ					=	GENPID(23, '创建游戏对象')
SS_G2M_21DOT_GAMECREATE_ACK					=	GENPID(24, '创建游戏对象结果')
SS_M2G_21DOT_GAMERESULT_NTY					=	GENPID(25, '游戏结果')
CS_M2C_21DOT_EXIT_NTY						=	GENPID(26, '通知玩家退出')
CS_G2C_21DOT_KICK_NTY						=	GENPID(27, '踢出通知')
CS_M2C_21DOT_STARTMATE_NTY					=	GENPID(28, '通知开始匹配')
CS_C2M_21DOT_STARTMATE_REQ					=	GENPID(29, '点击开始匹配按钮')
CS_M2C_21DOT_STARTMATE_ACK					=	GENPID(30, '点击开始匹配按钮响应')
CS_G2C_21DOT_BANKOP_NTY						=	GENPID(31, '庄家操作')
CS_C2M_21DOT_EXIT_REQ						=	GENPID(32, '退出')
CS_M2C_21DOT_EXIT_ACK						=	GENPID(33, '退出')
CS_C2G_21DOT_BACKGROUND_REQ					=	GENPID(34, '退出')
CS_G2C_21DOT_BACKGROUND_ACK					=	GENPID(35, '退出')