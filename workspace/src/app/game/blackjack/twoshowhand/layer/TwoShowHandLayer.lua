--region TwoShowHandLayer.lua
--Date 2017.06.07
--Auther JackyXu.
--Desc 二人梭哈 main layer.

local TwoShowHand_Events = require("game.twoshowhand.scene.TwoShowHandEvent")
local TwoShowHandClientMsg = require("game.twoshowhand.proxy.TwoShowHandClientMsg")
local TwoShowHandConst = require("game.twoshowhand.scene.TwoShowHandConst")

local scheduler             = cc.exports.scheduler
local Poker                 = require("game.twoshowhand.bean.Poker")
-- local Chip                  = require("game.twoshowhand.bean.TwoShowHandChip")
local CommonUserInfo        = require("common.layer.CommonUserInfo")
local TwoShowHandRuleLayer  = require("game.twoshowhand.layer.TwoShowHandRuleLayer")
local TwoShowHandDataMgr    = require("game.twoshowhand.manager.TwoShowHandDataMgr")
local ChatLayer             = require("common.layer.ChatLayerNew")
local TwoShowHandRes   = require("game.twoshowhand.scene.TwoShowHandRes")
local SpineManager      = require("common.manager.SpineManager")
local RollMsg              = require("common.layer.RollMsg")
local GameListConfig    = require("common.config.GameList")         --等级场配置


local TwoShowHandLayer = class("TwoShowHandLayer", cc.Layer)

local G_FlyJetton_Num_Max = 100 -- 最大单笔下注筹码个数
local G_Random_Fly_Time = {     -- 下注时筹码飞行动画
        0.1,
        0.2,
        0.3,
        0.35,
    }


local Chip_Max_Row = 8
local Chip_Max_Col = 12  

function TwoShowHandLayer:ctor()
    self:enableNodeEvents()
    
    math.randomseed(tostring(os.time()):reverse():sub(1, 7))

    TwoShowHandLayer.inst_ = self

    self:init()
end

function TwoShowHandLayer:init()
    print("TwoShowHandLayer:init")

    self:initVar()
    self:initCSB()
    self:initEvent()
    self:initSoundButton()
    self:initAfterEnter()
    self:tryFixIphoneX()

end

function TwoShowHandLayer:onEnter()

    TwoShowHandDataMgr.getInstance():setIsEnterGameView(true)

    self:initGameInfo()

    CMsgGame:sendGameOption()

    --快速进入桌子不自动准备
    PlayerInfo.getInstance():setIsTableQuickStart(false)
    -- if PlayerInfo.getInstance():getIsTableQuickStart() then
    --     if TwoShowHandDataMgr.getInstance():getGameStatus() == TwoShowHandConst.GAME_STATUS_FREE then
    --         CMsgGame:sendUserReady()
    --     end
    -- end
end

function TwoShowHandLayer:initVar()
    
    -- 定时器
    self.m_pDispatchCard1 = nil
    self.m_pDispatchCard2 = nil
    self.m_pShowWinner = nil
    self.m_pCountDown1 = nil
    self.m_pCountDown2 = nil
    self.m_pDispatchCardEnd = nil
    self.m_pShowConclude = nil
    -- 手牌
    self.m_pUserPoker = {{},{}}
    -- 音效ID
    self.m_nClockSoundID = 0
    -- 桌面筹码
    self.m_vecSpJetton = {}
    -- 发牌动画
    self.m_pArmDispatch = {{},{}}
    -- 倒计时秒数
    self.m_nCountdown = {0,0}
    -- 倒计时进度条
    self.m_pProgressTimer = {}
    -- 弃牌 5 张
    self.m_pSpGiveUpCard = {}
    -- 下注额度
    self.m_llTempStartScore = {0,0}
    -- 对手信息数据
    self.m_vecOtherUserInfo = nil
    --是否开牌阶段
    self.m_bIsOpenState = false
    self.m_bShowChaKanTip = true
    self.m_bIsKill = false
    -- 是否是对家弃牌
    self.m_bIsPlayerGiveUp = false
    -- 下拉菜单动画是否在动画中
    self.m_bIsMenuMove = false
    self.m_nLastTouchTime = 0
    --
    self.m_bDispatchCardFinsih = false
    --等待玩家动画
    self.m_pArmWaitUser = nil

    self.m_pTableChipList = {}
    self.m_pChipCacheList = {}
    --初始化行列筹码存放表
    for row=1,Chip_Max_Row do
        for col=1,Chip_Max_Col do
            self.m_pTableChipList[row] = self.m_pTableChipList[row] or {}
            self.m_pTableChipList[row][col] = self.m_pTableChipList[row][col] or {}
        end
    end
    self.m_pTableChipNumber = 0

    self.m_bSelfLooK = false --当前自己是否点击看牌
    self.m_bOtherLook = false --是否查看别人的牌
    self.m_pSelfAllChipNum = 0 -- 自己本轮下注总数
    self.m_pOtherAllChipNum = 0 -- 对方本轮下注总数
end

function TwoShowHandLayer:initCSB()
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    --csb
    self.m_pTwoMainView = cc.CSLoader:createNode(TwoShowHandRes.game_scene_res)
    self.m_pTwoMainView:addTo(self.m_rootUI)


    --界面适配
    self.m_pTwoMainView:setPositionX(145-(1624-display.size.width)/2)
    self.m_pTwoPanel = self.m_pTwoMainView:getChildByName("Panel_root")
    self.m_pTwoBg = self.m_pTwoMainView:getChildByName("game_bg")


    self.m_pBtnExit = self.m_pTwoPanel:getChildByName("btn_exit")
    self.m_pNodeLeft = self.m_pTwoPanel:getChildByName("m_pNodeLeft")
    self.m_pNodeLeft:setLocalZOrder(G_CONSTANTS.Z_ORDER_COMMON+1)
    self.m_pNodeMenu = self.m_pNodeLeft:getChildByName("m_pNodeMenu")
    self.m_pBtnMenuPush = self.m_pNodeLeft:getChildByName("m_pBtnMenuPush")
    self.m_pBtnMenuPop = self.m_pNodeLeft:getChildByName("m_pBtnMenuPop")
    self.m_pBtnSound = self.m_pNodeMenu:getChildByName("m_pBtnSound")
    self.m_pBtnMusic = self.m_pNodeMenu:getChildByName("m_pBtnMusic")
    self.m_pBtnRule = self.m_pNodeMenu:getChildByName("m_pBtnRule")
    self.m_pBtnBank = self.m_pNodeMenu:getChildByName("m_pBtnBank")
    self.m_pBtnChangeTable = self.m_pNodeMenu:getChildByName("m_pBtnChangeTable")

    self.m_pNodeFree = self.m_pTwoPanel:getChildByName("m_pNodeFree")
    self.m_pNodePlay = self.m_pTwoPanel:getChildByName("m_pNodePlay")
    self.m_pNodePlayMenu = self.m_pNodePlay:getChildByName("m_pNodePlayMenu")
    self.m_pNodeAddScore = self.m_pNodePlayMenu:getChildByName("m_pNodeAddScore")
    self.m_pNodeEnd = self.m_pTwoPanel:getChildByName("m_pNodeEnd")
    self.m_pOtherChipNum = self.m_pTwoPanel:getChildByName("other_chip_num")
    self.m_pOtherChipNum:hide()
    self.m_pSelfChipNum = self.m_pTwoPanel:getChildByName("self_chip_num")
    self.m_pSelfChipNum:hide()

    --银商喇叭
    self.m_rollMsgObj = RollMsg.create()
    self.m_rollMsgObj:addTo(self.m_pTwoMainView)
    self.m_rollMsgObj:startRoll()


    self.m_pNodeUser = {}
    self.m_pLbUsrNick = {}
    self.m_pImgUsrVip = {}
    self.m_pLbStrUsrGold = {}
    self.m_pSpHead = {}
    self.m_pSpFrame = {}
    self.m_pSpAlready = {}
    self.m_pNodeUserChip = {}
    self.m_pLbUserChipNum = {}
    self.m_pSpResult = {}
    self.m_pSpObscureCard = {}
    self.m_pBtnUserInfo = {}
    
    for i=1,2 do
        local str1 = string.format("m_pNodeUser%d",i-1)
        self.m_pNodeUser[i] = self.m_pTwoPanel:getChildByName(str1)
        
        local str2 = string.format("m_pLbUsrNick%d",i-1)
        self.m_pLbUsrNick[i] = self.m_pNodeUser[i]:getChildByName("Image_16"):getChildByName(str2)

        local str21 = string.format("m_pImgVip%d",i-1)
        self.m_pImgUsrVip[i] = self.m_pNodeUser[i]:getChildByName("Image_16"):getChildByName(str21)

        local str3 = string.format("m_pLbStrUsrGold%d",i-1)
        self.m_pLbStrUsrGold[i] = self.m_pNodeUser[i]:getChildByName("Image_16"):getChildByName(str3)
        
        local str4 = string.format("m_pSpHead%d",i-1)
        self.m_pSpHead[i] = self.m_pNodeUser[i]:getChildByName("m_pSpHeadBg"..(i-1)):getChildByName(str4)
        
        local str41 = string.format("m_pImgFrame%d",i-1)
        self.m_pSpFrame[i] = self.m_pNodeUser[i]:getChildByName("m_pSpHeadBg"..(i-1)):getChildByName(str41)

        local str5 = string.format("m_pSpAlready%d",i-1)
        self.m_pSpAlready[i] = self.m_pNodeUser[i]:getChildByName(str5)

        local str6 = string.format("m_pNodeUserChip%d",i-1)
        self.m_pNodeUserChip[i] = self.m_pNodeUser[i]:getChildByName(str6)
        
        local str7 = string.format("m_pLbUserChipNum%d",i-1)
        self.m_pLbUserChipNum[i] = self.m_pNodeUserChip[i]:getChildByName(str7)
        
        local str8 = string.format("m_pSpResult%d",i-1)
        self.m_pSpResult[i] = self.m_pNodeEnd:getChildByName(str8)
        
        local str9 = string.format("m_pSpObscureCard%d",i-1)
        self.m_pSpObscureCard[i] = self.m_pNodePlay:getChildByName(str9)

        self.m_pBtnUserInfo[i] = self.m_pNodeUser[i]:getChildByName("m_pbtnUserinfo")
        self.m_pBtnUserInfo[i]:setTag(i-1)

        -----设置一下name和coin的zorder
        self.m_pNodeUser[i]:getChildByName("Image_16"):setLocalZOrder(10)
    end
    
    self.m_pLbHistoryScore = self.m_pTwoPanel:getChildByName("node_history"):getChildByName("m_pLbHistoryScore")
    self.m_pLbLastScore = self.m_pTwoPanel:getChildByName("node_history"):getChildByName("m_pLbLastScore")
    self.m_pNodeFlyJetton = self.m_pTwoPanel:getChildByName("m_pNodeFlyJetton")

    self.m_pResultScore = {}
    for i=1,2 do
        self.m_pResultScore[i] = self.m_pNodeEnd:getChildByName(string.format("result_score_%d",i))
        self.m_pResultScore[i]:hide()
    end

    self.m_pNodeUserInfo = self.m_pTwoPanel:getChildByName("m_pNodeUserInfo")

    self.m_pBtnGiveUp = self.m_pNodePlayMenu:getChildByName("m_pBtnGiveUp")
    self.m_pBtnGiveUp:setTag(0)
    self.m_pBtnNotAdd = self.m_pNodePlayMenu:getChildByName("m_pBtnNotAdd")
    self.m_pBtnNotAdd:setTag(1)
    self.m_pBtnShowHand = self.m_pNodePlayMenu:getChildByName("m_pBtnShowHand")
    self.m_pBtnShowHand:setTag(2)
    self.m_pBtnFollow = self.m_pNodePlayMenu:getChildByName("m_pBtnFollow")
    self.m_pBtnFollow:setTag(3)
    self.m_pBtnAdd = self.m_pNodePlayMenu:getChildByName("m_pBtnAdd")
    self.m_pBtnAdd:setTag(4)


    self.m_pNodeBet = {}
    self.m_pLbBetNum = {}
    for i=1,4 do
        local str1 = string.format("m_pBtnBet%d",i-1)
        self.m_pNodeBet[i] = self.m_pNodePlayMenu:getChildByName("m_pNodeAddScore"):getChildByName(str1)
        self.m_pNodeBet[i]:setTag(i-1)
        self.m_pNodeBet[i]:setVisible(i~=4)
        
        local str2 = string.format("m_pLbBetNum%d",i-1)
        -- self.m_pLbBetNum[i] = tolua.cast(self[str2], "cc.Label")
        self.m_pLbBetNum[i] = self.m_pNodeBet[i]:getChildByName(str2)
    end

    self.m_pNodeKaiPaiMenu = self.m_pNodePlay:getChildByName("m_pNodeKaiPaiMenu")
    self.m_pBtnGiveUp2 = self.m_pNodeKaiPaiMenu:getChildByName("m_pBtnGiveUp")
    self.m_pBtnGiveUp2:setTag(5)
    self.m_pBtnKaiPai = self.m_pNodeKaiPaiMenu:getChildByName("m_pBtnKaiPai")
    self.m_pBtnKaiPai:setTag(6)

    self.m_pBtnObscureCard = self.m_pNodePlay:getChildByName("m_pBtnObscureCard")
    self.m_pBtnEnemyObscureCard = self.m_pNodePlay:getChildByName("m_pBtnObscureCard_0")
    
    self.m_pBtnStart = self.m_pNodeFree:getChildByName("m_pBtnStart")-- 进房间开始
    self.m_pBtnConcludeStart = self.m_pNodeFree:getChildByName("m_pBtnConcludeStart") -- 结算开始
    self.m_pBtnConcludeStart:hide()

    --聊天界面
    self.m_pChatLayer = ChatLayer:create()
    self.m_pChatLayer:initData({1,2})
    self.m_pChatLayer:setPlayerUpdateWay(true)
    self.m_pChatLayer:setBtnVisible(true,true,false,false)
    local diffX = 145- (1624-display.size.width)/2
    self.m_pChatLayer:setNodeAllOffset(0,0)
    self.m_pChatLayer:setLeftBtnOffsetX(0)
    self.m_pChatLayer:setLeftBtnOffsetY(60)
    self.m_pTwoMainView:addChild(self.m_pChatLayer,101)

    --房间号
    -- self.m_pSpBg = tolua.cast(self["m_pSpBg"], "cc.Sprite")
    -- self.m_pSpBg:setPositionX(-145)
    local strRoomFiled = LuaUtils.getLocalString(string.format("TWO_NIUNIU_ROOM_%d",PlayerInfo.getInstance():getBaseScore()))
    local strRoomNo = string.format(LuaUtils.getLocalString("STRING_187"), PlayerInfo.getInstance():getCurrentRoomNo())
    local m_pLbRoomFiled = cc.Label:createWithBMFont("public/font/11.fnt", strRoomFiled)
	m_pLbRoomFiled:setAnchorPoint(cc.p(0.5, 0.5))

    local move_iphonex_x = LuaUtils.isIphoneXDesignResolution() and -80 or 0

	m_pLbRoomFiled:setPosition(cc.p(50+move_iphonex_x, 610))
	self.m_pTwoMainView:addChild(m_pLbRoomFiled)
    local m_pLbRoomNo = cc.Label:createWithBMFont("public/font/11.fnt", strRoomNo)
	m_pLbRoomNo:setAnchorPoint(cc.p(0.5, 0.5))
	m_pLbRoomNo:setPosition(cc.p(50+move_iphonex_x, 585))
	self.m_pTwoMainView:addChild(m_pLbRoomNo)

    self:showBgAni()
end

function TwoShowHandLayer:initEvent()

    self.event_ = {
        [Public_Events.MSG_NETWORK_FAILURE]     = { func = self.onNetWorkErrExitGame,   log = "", },
        [Public_Events.MSG_UPDATE_USER_SCORE]   = { func = self.onMsgUpdateUsrScore,    log = "", },
        [Public_Events.MSG_GAME_EXIT]           = { func = self.onMsgExitGame,          log = "", },
        [Public_Events.MSG_GAME_ENTER_BACKGROUND]={ func = self.onMsgEnterBackGround,   log = "", },

        [Public_Events.MSG_USER_FREE]           = { func = self.onMsgFree,              log = "", },
        [Public_Events.MSG_USER_LEAVE]          = { func = self.onMsgFree,              log = "", },
        [Public_Events.MSG_USER_SIT]            = { func = self.onMsgSit,               log = "", },
        [Public_Events.MSG_USER_READY]          = { func = self.onMsgReady,             log = "", },
        [Public_Events.MSG_USER_COMEBACK]       = { func = self.onMsgUserComeBack,      log = "", },
        [Public_Events.MSG_MYSELF_FREE]         = { func = self.onMsgMyselfFree,        log = "", },
        [Public_Events.MSG_UPDATE_OTHER_USER_GOLD_GR]   = { func = self.event_UpdateOtherScore,       log = "更新玩家金币",   debug = true, },

        [TwoShowHand_Events.MSG_TWOSHOWHAND_INIT]               = { func = self.onMsgGameInit,          log = "", },
        [TwoShowHand_Events.MSG_TWOSHOWHAND_GAME_START]         = { func = self.onMsgGameStart,         log = "", },
        [TwoShowHand_Events.MSG_TWOSHOWHAND_ADD_SCORE]          = { func = self.onMsgGameAddScore,      log = "", },
        [TwoShowHand_Events.MSG_TWOSHOWHAND_GIVE_UP]            = { func = self.onMsgGameGiveUp,        log = "", },
        [TwoShowHand_Events.MSG_TWOSHOWHAND_SHOW_HAND]          = { func = self.onMsgGameShowHand,      log = "", },
        [TwoShowHand_Events.MSG_TWOSHOWHAND_SEND_CARD]          = { func = self.onMsgGameSendCard,      log = "", },
        [TwoShowHand_Events.MSG_TWOSHOWHAND_GAME_OPENCARD]      = { func = self.onMsgGameOpenCard,      log = "", },
        [TwoShowHand_Events.MSG_TWOSHOWHAND_GAME_OPENCARD_INFO] = { func = self.onMsgGameOpenCardInfo,  log = "", },
        [TwoShowHand_Events.MSG_TWOSHOWHAND_GAME_LOOKCARD]      = { func = self.onMsgGameLookCard,      log = "", },
        [TwoShowHand_Events.MSG_TWOSHOWHAND_GAME_LOOK_ENEMY_CARD] = { func = self.onMsgGameLookEnemyCard,      log = "", },
        [TwoShowHand_Events.MSG_TWOSHOWHAND_GAME_END]           = { func = self.onMsgGameEnd,           log = "", },
    }

    local function onMsgUpdate_(event)  --接收自定义事件
        local name = event:getEventName()
        local msg = unpack(event._userdata)
        local processer = self.event_[name]
        printf("-- %s process: [%s] --", self.__cname, processer.log)
        processer.func(self, msg)
    end

    for key in pairs(self.event_) do   --监听事件
         SLFacade:addCustomEventListener(key, onMsgUpdate_,self.__cname)
    end

    ----------添加按钮事件---------
    self.m_pBtnMenuPop:addClickEventListener(handler(self,self.onPopClicked))
    self.m_pBtnMenuPush:addClickEventListener(handler(self,self.onPushClicked))
    self.m_pNodeMenu:addClickEventListener(handler(self,self.onPushClicked))
    self.m_pBtnExit:addClickEventListener(handler(self,self.onReturnClicked))
    self.m_pBtnSound:addClickEventListener(handler(self,self.onSoundClicked))
    self.m_pBtnMusic:addClickEventListener(handler(self,self.onMusicClicked))
    self.m_pBtnRule:addClickEventListener(handler(self,self.onRuleClicked))
    self.m_pBtnBank:addClickEventListener(handler(self,self.onOpenInGameBankClicked))
    self.m_pBtnChangeTable:addClickEventListener(handler(self,self.onChangeTableClicked))
    for k,btn_user_info in pairs(self.m_pBtnUserInfo) do
        btn_user_info:addClickEventListener(handler(self,self.onUserInfoClicked))
    end

    self.m_pBtnStart:addClickEventListener(handler(self,self.onStartClicked))
    self.m_pBtnConcludeStart:addClickEventListener(handler(self,self.onStartClicked))

    self.m_pBtnGiveUp:addTouchEventListener(handler(self,self.onPlayMenuClicked))
    self.m_pBtnNotAdd:addTouchEventListener(handler(self,self.onPlayMenuClicked))
    self.m_pBtnShowHand:addTouchEventListener(handler(self,self.onPlayMenuClicked))
    self.m_pBtnFollow:addTouchEventListener(handler(self,self.onPlayMenuClicked))
    self.m_pBtnAdd:addTouchEventListener(handler(self,self.onPlayMenuClicked))
    self.m_pBtnGiveUp2:addTouchEventListener(handler(self,self.onPlayMenuClicked))
    self.m_pBtnKaiPai:addTouchEventListener(handler(self,self.onPlayMenuClicked))

    self.m_pNodeAddScore:getChildByName("Panel_2"):addClickEventListener(handler(self,self.onCancelAddMenuClicked))

    for k,node_bet in pairs(self.m_pNodeBet) do
        node_bet:addTouchEventListener(handler(self,self.onBetClicked))
    end

end

function TwoShowHandLayer:onExit()
    
    self:leaveGameClean()

    AudioManager:getInstance():stopAllSounds()
end

function TwoShowHandLayer:leaveGameClean()

    for key, value in pairs(self.event_) do
        SLFacade:removeCustomEventListener(key, self.__cname)
    end
    self.event_ = {}

    -- body
    -- 移除定时器
    if self.m_pDispatchCard1 then
       scheduler.unscheduleGlobal(self.m_pDispatchCard1)
       self.m_pDispatchCard1 = nil
    end
    if self.m_pShowWinner then
       scheduler.unscheduleGlobal(self.m_pShowWinner)
       self.m_pShowWinner = nil
    end
    if self.m_pCountDown1 then
        scheduler.unscheduleGlobal(self.m_pCountDown1)
        self.m_pCountDown1 = nil
    end
    if self.m_pCountDown2 then
        scheduler.unscheduleGlobal(self.m_pCountDown2)
        self.m_pCountDown2 = nil
    end
    if self.m_pDispatchCard2 then
        scheduler.unscheduleGlobal(self.m_pDispatchCard2)
        self.m_pDispatchCard2 = nil
    end
    if self.m_pDispatchCardEnd then
        scheduler.unscheduleGlobal(self.m_pDispatchCardEnd)
        self.m_pDispatchCardEnd = nil
    end
    if self.m_pShowConclude then
        scheduler.unscheduleGlobal(self.m_pShowConclude)
        self.m_pShowConclude = nil
    end

    -- 释放游戏数据
    if TwoShowHandDataMgr then
        TwoShowHandDataMgr.getInstance():Clean()
        TwoShowHandDataMgr.getInstance():releaseInstance()
    end
end

function TwoShowHandLayer:initAfterEnter()
    -- 播放背景音樂
    if AudioManager.getInstance():getStrMusicPath() ~= "game/twoshowhand/sound/twoshowhand-bg.mp3" then
        AudioManager.getInstance():stopMusic(false)
        AudioManager.getInstance():playMusic("game/twoshowhand/sound/twoshowhand-bg.mp3")
    end

    self.m_bShowChaKanTip =  cc.UserDefault:getInstance():getBoolForKey("showhandtip", true)
    
    self.m_pBtnMenuPush:setVisible(false)
    self.m_pNodeMenu:setVisible(false)
    
    local shap = cc.DrawNode:create()
    local pointArr = {cc.p(0,150), cc.p(300, 150), cc.p(300, 700), cc.p(0, 700)}
    shap:drawPolygon(pointArr, 4, cc.c4f(1, 0, 0, 1), 2, cc.c4f(255, 255, 255, 255))
    shap:setPosition(1034,20)
    self.m_pClippingMenu = cc.ClippingNode:create(shap)
    self.m_pClippingMenu:setPosition(cc.p(0,0))
    self.m_pNodeLeft:addChild(self.m_pClippingMenu)
    self.m_pNodeMenu:removeFromParent()
    self.m_pClippingMenu:addChild(self.m_pNodeMenu)

    
    if GameListManager.getInstance():getGameSwitch(G_CONSTANTS.GAMESWITCH.SPEAKER) and 
       GameListManager.getInstance():getGameSwitch(G_CONSTANTS.GAMESWITCH.USERLIST) then
        self.m_pNodeChat:setVisible(false)
    end

    -- self.m_pNodeNewMsg:setVisible(false)
    if self.m_pLbUsrNick[1] then
        local strTemp = PlayerInfo.getInstance():getNameNick()
        local strNick = LuaUtils.getDisplayNickName(strTemp, 8, true)
        self.m_pLbUsrNick[1]:setString(strNick)
    end

    if self.m_pImgUsrVip[1] then
        local viplv = PlayerInfo.getInstance():getVipLevel()
        local vipPath = string.format("hall/plist/vip/img-vip%d.png", viplv)
        self.m_pImgUsrVip[1]:loadTexture(vipPath, ccui.TextureResType.plistType)
    end

    local function obscureCardClickEvent()
        self.m_bSelfLooK = not self.m_bSelfLooK
        if self.m_bSelfLooK then
            self:onObscureCardTouchIn()
        else
            self:onObscureCardClicked2()
        end
    end

    self.m_pBtnObscureCard:addClickEventListener(obscureCardClickEvent)
    
    --看对方底牌按钮
    local obscureCardTouchEvent = function (sender,state)
        local gameStatus = TwoShowHandDataMgr.getInstance():getGameStatus()
        if gameStatus ~= TwoShowHandConst.GAME_STATUS_PLAY then
            local endType = TwoShowHandDataMgr.getInstance():getGameEndType()
            local sendCardCount = TwoShowHandDataMgr.getInstance():getSendCardCount()
            if endType == TwoShowHandConst.GAME_END_NORMAL and sendCardCount == 5 then
                return
            end
        end

        self.m_bOtherLook = not self.m_bOtherLook

        if self.m_bOtherLook then
            self.m_pUserPoker[2][1]:setVisible(true)
        else 
            self.m_pUserPoker[2][1]:setVisible(false)
        end
    end
    self.m_pBtnEnemyObscureCard:addClickEventListener(obscureCardTouchEvent)
    self.m_pBtnEnemyObscureCard:setEnabled(false)

    --[[--重新构建字符串
    local node = self.m_pLbUsrGold:getParent()
    local pos = cc.p(self.m_pLbUsrGold:getPosition())
    self.m_pLbUsrGold:removeFromParent()
    self.m_pLbUsrGold = nil
    self.m_pLbUsrGold = cc.Label:createWithCharMap("game/twoshowhand/gui/gui-tshowhand-number-1.png", 24, 34, 46)
    self.m_pLbUsrGold:setAnchorPoint(cc.p(0,0.5))
    self.m_pLbUsrGold:setPosition(pos)
    node:addChild(self.m_pLbUsrGold)]]
    
    --创建牌
    for i=1,2 do
        for n=1,5 do
            local posX, posY = self.m_pSpObscureCard[i]:getPosition()
            local size = self.m_pSpObscureCard[i]:getContentSize()
            local pos = cc.p(posX + (n-1) * 67, posY)
            -- local pos = (i == 1 and cc.p(492 + (n-1) * 48, 53) or cc.p(340 + (n-1) * 48, 475))
            self.m_pUserPoker[i][n] = Poker:create()
            self.m_pUserPoker[i][n]:setPosition(cc.p(pos.x, pos.y))
            --[[self.m_pUserPoker[i][n]:setScale(1.15)]]
            self.m_pNodePlay:addChild(self.m_pUserPoker[i][n], G_CONSTANTS.Z_ORDER_COMMON)
            self.m_pUserPoker[i][n]:setVisible(false)
        end
    end
    
    -- self.m_pNodeCardType:setVisible(false)

    self.m_pNodeFree:setLocalZOrder(G_CONSTANTS.Z_ORDER_COMMON)
    self.m_pBtnStart:setVisible(true)-- 进房间开始
    self.m_pBtnConcludeStart:setVisible(false)-- 结算开始
    self.m_pNodeFree:setVisible(false)
    self.m_pNodePlay:setVisible(false)
    self.m_pNodeEnd:setVisible(false)
    self.m_pNodeFlyJetton:setVisible(false)
    for i=1,2 do
        self.m_pSpAlready[i]:setVisible(false)
        self.m_pNodeUserChip[i]:setVisible(false)
        self.m_pLbUserChipNum[i]:setString("")
        self.m_pSpObscureCard[i]:setVisible(false)
    end
    if self.m_pNodeUser[2] then
        self.m_pNodeUser[2]:setVisible(false)
    end
    self.m_pBtnGiveUp:setEnabled(true)
    self.m_pNodeKaiPaiMenu:setVisible(false)
    self:setButtonVisible(self.m_pBtnNotAdd, true)
    self:setButtonEnable(self.m_pBtnShowHand, true)
    self:setButtonVisible(self.m_pBtnFollow, true)
    -- self:setButtonEnable(self.m_pBtnAdd, true)
    
end

function TwoShowHandLayer:initSoundButton()
    --*******************设置按钮类型,播放相关音效****************************
    --音乐 
    local music = AudioManager.getInstance():getMusicOn()
    local music_res = ""
    if music then
        AudioManager.getInstance():setMusicOn(true)
        music_res = "gui-tshowhand-button-yingyue-2.png"
    else
        AudioManager.getInstance():setMusicOn(false)
        AudioManager.getInstance():stopMusic()
        music_res = "gui-tshowhand-button-yingyue.png"
    end

    self.m_pBtnMusic:loadTextureNormal(music_res,ccui.TextureResType.plistType)
    self.m_pBtnMusic:loadTexturePressed(music_res,ccui.TextureResType.plistType)

    -- 音效
    local sound = AudioManager.getInstance():getSoundOn()
    local sound_res = ""
    if sound then
        AudioManager.getInstance():setSoundOn(true)
        sound_res = "gui-tshowhand-button-shengying-2.png"
    else
        AudioManager.getInstance():setSoundOn(false)
        sound_res = "gui-tshowhand-button-shengying.png"
    end

    self.m_pBtnSound:loadTextureNormal(sound_res,ccui.TextureResType.plistType)
    self.m_pBtnSound:loadTexturePressed(sound_res,ccui.TextureResType.plistType)

end

function TwoShowHandLayer:initGameInfo()
    -- if self.m_pLbUsrGold then
    --     local str = LuaUtils.getFormatGoldAndNumber(PlayerInfo.getInstance():getUserScore())
    --     self.m_pLbUsrGold:setString(str)
    -- end
    self.m_pLbStrUsrGold[1]:setString(LuaUtils.getGoldNumberZH(PlayerInfo.getInstance():getUserScore()))
    if self.m_pSpHead[1] then
        local nFaceID = PlayerInfo.getInstance():getFaceID()
        local strHeadIcon = string.format("hall/image/file/gui-icon-head-%02d.png", nFaceID % G_CONSTANTS.FACE_NUM + 1)
        self.m_pSpHead[1]:loadTexture(strHeadIcon, ccui.TextureResType.localType)

    end
    if self.m_pSpFrame[1] then
        local nFrameID = PlayerInfo.getInstance():getFrameID()
        local strFramePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", nFrameID)
        self.m_pSpFrame[1]:loadTexture(strFramePath, ccui.TextureResType.plistType)
    end

    self:updateHistoryScore()
    self:initOtherInfo()

    if not TwoShowHandDataMgr.getInstance():getIsLoadGameScene() then
        return
    end
    
    local gameStatus = TwoShowHandDataMgr.getInstance():getGameStatus()
    if gameStatus == TwoShowHandConst.GAME_STATUS_FREE then 
        self.m_pNodeFree:setVisible(true)
    elseif gameStatus == TwoShowHandConst.GAME_STATUS_PLAY then
        self:onGameStart(false)
        self:showCard()
        self:updateChipNum()
        for i=1,2 do
            local score = self:getPlayerScoreByIndex(i)
            self:flyJetton(i, score)
            self.m_pSpObscureCard[i]:setVisible(true)
        end
        
        local leftTime = TwoShowHandDataMgr.getInstance():getLeftTime()
        if TwoShowHandDataMgr.getInstance():getWaitOpenCard() then
            --开牌状态
            if not TwoShowHandDataMgr.getInstance().m_bOpenCard[1] then
                self:showProgrssTimer(1,leftTime)
                self.m_pNodeKaiPaiMenu:setVisible(true)
            end
            if not TwoShowHandDataMgr.getInstance().m_bOpenCard[2] then
                self:showProgrssTimer(2,leftTime)
            end
        else
            if TwoShowHandDataMgr.getInstance():CurrentUserIsMe() then
                if TwoShowHandDataMgr.getInstance().m_bShowHand[1] or TwoShowHandDataMgr.getInstance().m_bShowHand[2] then
                    self:setButtonEnable(self.m_pBtnShowHand, true)
                    self:setButtonVisible(self.m_pBtnNotAdd, false)
                    self:setButtonVisible(self.m_pBtnFollow, false)
                    -- self:setButtonEnable(self.m_pBtnAdd, false)
                else
                    local sendCount = TwoShowHandDataMgr.getInstance():getSendCardCount()
                    self:setButtonEnable(self.m_pBtnShowHand, sendCount >= 3)
                    local maxTimes = TwoShowHandDataMgr.getInstance():getTurnMaxTimes()
                    local bNotAdd = (maxTimes == 0)
                    local bFollow = (maxTimes > 0)
                    local bAdd = true
                    self:setButtonVisible(self.m_pBtnNotAdd, bNotAdd)
                    self:setButtonVisible(self.m_pBtnFollow, bFollow)
                    -- self:setButtonEnable(self.m_pBtnAdd,   bAdd)
                    
                    self:updateAddScoreStatus()
                end
                self.m_pNodePlayMenu:setVisible(true)
                -- self.m_pNodeAddScore:setVisible(false)
                self:showProgrssTimer(1,leftTime)
            else
                self:showProgrssTimer(2,leftTime)
            end
        end
    end
end
-------------------------------
-- msg
function TwoShowHandLayer:onNetWorkErrExitGame(msg)
    -- 网络连接失败不能发送起立 会算玩家主动弃牌
--    if TwoShowHandDataMgr.getInstance():getGameStatus() == TwoShowHandConst.GAME_STATUS_PLAY then
--        TwoShowHandClientMsg.getInstance():sendTableStandUp()
--    end
    if msg == "1" then
        FloatMessage.getInstance():pushMessage("STRING_023_1")
    else
        FloatMessage.getInstance():pushMessage("STRING_023")
    end
    self:onMoveExitView(true)
end

function TwoShowHandLayer:onMsgExitGame(_event)
    if _event == 1 then
        self.m_bIsKill = true
        return
    end
    local isMoveExit  = 1
    if _event ~= nil then 
        --主动退出情况 要收到起立成功才删除界面
        isMoveExit = tonumber(_event.packet)
    end

    if TwoShowHandDataMgr.getInstance():getGameStatus() == TwoShowHandConst.GAME_STATUS_PLAY then
        TwoShowHandClientMsg.getInstance():sendTableStandUp()
    end
    if isMoveExit == 1 then 
        self:onMoveExitView()
    end
end

function TwoShowHandLayer:onMsgEnterBackGround(msg)

    --self:onMoveExitView(false,false)
    PlayerInfo.getInstance():setChairID(G_CONSTANTS.INVALID_CHAIR)
    PlayerInfo.getInstance():setTableID(G_CONSTANTS.INVALID_TABLE)
    PlayerInfo.getInstance():setSitSuc(false)
    PlayerInfo.getInstance():setIsQuickStart(false)
    PlayerInfo:getInstance():setIsGameBackToHallSuc(false)
    --返回桌子界面
    SLFacade:dispatchCustomEvent(Public_Events.MSG_GAME_LOAD_SUCCESS)
end

function TwoShowHandLayer:onMsgUpdateUsrScore(_event)
    -- if (PlayerInfo.getInstance():getUserScore() - PlayerInfo.getInstance():getLastUserScore())> 0 then
    --     -- 玩家金币变化增加闪光动画
    --     self.m_pLbUsrGold:runAction(cc.Sequence:create(cc.ScaleTo:create(0.3, 1.3), cc.DelayTime:create(0.2), 
    --             cc.CallFunc:create(function()
    --                 self.m_pLbUsrGold:setString(LuaUtils.getFormatGoldAndNumber(PlayerInfo.getInstance():getUserScore()))
    --             end), cc.ScaleTo:create(0.3, 1)))
    --     self.m_pSpGoldGuang:runAction(cc.Sequence:create(cc.Show:create(),cc.DelayTime:create(1.0),cc.Hide:create()))
    -- else
    --     self.m_pLbUsrGold:setString(LuaUtils.getFormatGoldAndNumber(PlayerInfo.getInstance():getUserScore()))    
    -- end
    self.m_pLbStrUsrGold[1]:setString(LuaUtils.getGoldNumberZH(PlayerInfo.getInstance():getUserScore()))
end

function TwoShowHandLayer:onMsgFree(_event)
--    local _userdata = unpack(_event._userdata)
--    if not _userdata then
--        return
--    end
    local _userdata = _event

    local chairId = tonumber(_userdata)
    local myChairId =  PlayerInfo.getInstance():getChairID()
    print("===起立 chairID：".. chairId..", myChairID:"..myChairId)
    if chairId == G_CONSTANTS.INVALID_CHAIR or self.m_bIsKill then
        return
    end
    if chairId == PlayerInfo.getInstance():getChairID() then
        self:onMoveExitView()
        return
    end

    -- 对手离开
    if chairId ~= PlayerInfo.getInstance():getChairID() then
        self.m_pNodeUser[2]:setVisible(false)
        self.m_pSpAlready[2]:setVisible(false)
        self.m_pNodeUserChip[2]:setVisible(false)
        self.m_pLbUserChipNum[2]:setString("")
        self:hideProgressTimer(2)
        if self.m_pArmOffLine then
            self.m_pArmOffLine:setVisible(false)
        end
        
        -----------------------
--        -- 产品要求对家离开时隐藏手牌
--        for n=1,5 do
--            if self.m_pArmDispatch[2][n] then
--                self.m_pArmDispatch[2][n]:setVisible(false)
--            end
--            if self.m_pUserPoker[2][n] then
--                self.m_pUserPoker[2][n]:setVisible(false)
--            end
--            if self.m_bIsPlayerGiveUp and self.m_pSpGiveUpCard[n] then
--                self.m_pSpGiveUpCard[n]:removeFromParent()
--                self.m_pSpGiveUpCard[n] = nil
--            end
--        end
--        self.m_pSpResult[2]:setVisible(false)
--        self.m_pSpObscureCard[2]:setVisible(false)
        -----------------------

        if TwoShowHandDataMgr.getInstance().m_llResult[2] > TwoShowHandDataMgr.getInstance().m_llResult[1] or TwoShowHandDataMgr.getInstance().m_llResult[1] == 0 then
            if self.m_pArmWinnerFrame then
                self.m_pArmWinnerFrame:setVisible(false)
            end
            self:cleanJetton()
        end
    end
end

function TwoShowHandLayer:onMsgSit(_event)
--    local _userdata = unpack(_event._userdata)
--    if not _userdata then
--        return
--    end
    local _userdata = _event

    local chairId = tonumber(_userdata)
    if chairId == G_CONSTANTS.INVALID_CHAIR then
        return
    end
    self:initOtherInfo()
end

function TwoShowHandLayer:onMsgReady(_event)
--    local _userdata = unpack(_event._userdata)
--    if not _userdata then
--        return
--    end
    local _userdata = _event

    local chairId = tonumber(_userdata)
    if chairId == G_CONSTANTS.INVALID_CHAIR then
        return
    end

    self:onUserReady(chairId)
end

function TwoShowHandLayer:onMsgUserComeBack(_event)
    if self.m_pArmOffLine then
        self.m_pArmOffLine:setVisible(false)
    end
end

function TwoShowHandLayer:onMsgMyselfFree()
    if not self.m_bIsKill then
        self:onMoveExitView()
    end
end

function TwoShowHandLayer:onMsgGameInit(_event)
    self:initGameInfo()
end

function TwoShowHandLayer:onMsgGameStart(_event)
    -- print("onMsgGameStart")
    self:onGameStart(true)
end

-- 下注
function TwoShowHandLayer:onMsgGameAddScore(_event)
    AudioManager:getInstance():stopSound(self.m_nClockSoundID)
    for i=1,2 do
        self:hideProgressTimer(i)
    end
    if self.m_pArmOffLine then
        self.m_pArmOffLine:setVisible(false)
    end
    self.m_pNodePlayMenu:setVisible(false)
    
    local viewIndex = TwoShowHandDataMgr.getInstance():getViewIndex(TwoShowHandDataMgr.getInstance():getAddScoreUser())
    local addType = TwoShowHandDataMgr.getInstance():getAddType() -- 0不加注 1跟注 2加注
    local sendCardCount = TwoShowHandDataMgr.getInstance():getSendCardCount()
    self:setButtonEnable(self.m_pBtnShowHand, sendCardCount >= 3)
    
    local opType = tonumber(addType)
    local userInfo = CUserManager:getInstance():getUserInfoByChairID(PlayerInfo.getInstance():getTableID(), TwoShowHandDataMgr.getInstance():getAddScoreUser())
    local strGender = (userInfo.cbGender == 0 and "man" or "woman")
    local strSound = nil
    if addType == 0 then -- 不加注
        self:setButtonVisible(self.m_pBtnNotAdd, true)
        self:setButtonVisible(self.m_pBtnFollow, false)
        -- self:setButtonEnable(self.m_pBtnAdd, true)
        strSound = string.format("game/twoshowhand/sound/%spass.mp3",strGender)
    elseif addType == 1 then -- 跟注
        self:setButtonVisible(self.m_pBtnNotAdd, true)
        self:setButtonVisible(self.m_pBtnFollow, false)
        -- self:setButtonEnable(self.m_pBtnAdd, true)
        strSound = string.format("game/twoshowhand/sound/%scall.mp3",strGender)
    elseif addType == 2 then
        self:setButtonVisible(self.m_pBtnNotAdd, false)
        self:setButtonVisible(self.m_pBtnFollow, true)
        self:setButtonEnable(self.m_pBtnFollow, true)
        -- self:setButtonEnable(self.m_pBtnAdd, true)
        strSound = string.format("game/twoshowhand/sound/%srase.mp3",strGender)
        
        local currentDown = TwoShowHandDataMgr.getInstance():getAddDownTimes()
        if currentDown == 1 then
            opType = TwoShowHandConst.E_TYPE_MIN
        elseif currentDown == 4 then
            opType = TwoShowHandConst.E_TYPE_MAX
        end
    end
    AudioManager:getInstance():playSound(strSound)
    
    if addType == 1 or addType == 2 then
        local addScore = TwoShowHandDataMgr.getInstance():getCellScore()*TwoShowHandDataMgr.getInstance().m_iTurnCurrentDown[viewIndex]
        self:flyJetton(viewIndex, addScore)
    end
    self:updateChipNum()
    self:showCaoZuoTipAni(viewIndex, opType)
    
    if TwoShowHandDataMgr.getInstance():CurrentUserIsMe() then
        self:updateAddScoreStatus()

        --如果下一个操作的玩家是自己 特殊处理加注

        if addType == 0 then --不加注(对方先手不加注-该我)
            self:setAddScoreEnbale(true)
        elseif addType == 1 then --跟注
            self:setAddScoreEnbale(false)
        elseif addType == 2 then --加注
            if TwoShowHandDataMgr.getInstance():getSelfAdd() then
                self:setAddScoreEnbale(false)
            else
                self:setAddScoreEnbale(true)
            end
        end

        self.m_pNodePlayMenu:setVisible(true)
        self.m_pNodeAddScore:setVisible(true)
        self:showProgrssTimer(1)   
    else
        self.m_pNodeAddScore:setVisible(false)
        self:showProgrssTimer(2)
    end

    if viewIndex == 2 and (addType == 1 or addType == 2) and self.m_pOtherChipNum then
        local addScore = TwoShowHandDataMgr.getInstance():getCellScore()*TwoShowHandDataMgr.getInstance().m_iTurnCurrentDown[viewIndex]
        self.m_pOtherAllChipNum = self.m_pOtherAllChipNum + addScore
        self.m_pOtherChipNum:setString("+"..LuaUtils.getFormatGoldAndNumber(self.m_pOtherAllChipNum))
        self.m_pOtherChipNum:show()
    elseif viewIndex == 1 and (addType == 1 or addType == 2) and self.m_pSelfChipNum then
        local addScore = TwoShowHandDataMgr.getInstance():getCellScore()*TwoShowHandDataMgr.getInstance().m_iTurnCurrentDown[viewIndex]
        self.m_pSelfAllChipNum = self.m_pSelfAllChipNum + addScore
        self.m_pSelfChipNum:setString("+"..LuaUtils.getFormatGoldAndNumber(self.m_pSelfAllChipNum))
        self.m_pSelfChipNum:show()
    end

end

function TwoShowHandLayer:setAddScoreEnbale(is_enable)
    for k,v in pairs(self.m_pNodeBet) do
        v:getChildByName("Image_hui"):setVisible(not is_enable)
        v:setEnabled(is_enable)
    end
end

-- 弃牌
function TwoShowHandLayer:onMsgGameGiveUp(_event)
    -- print("onMsgGameGiveUp")
    AudioManager:getInstance():stopSound(self.m_nClockSoundID)
    local userInfo = CUserManager:getInstance():getUserInfoByChairID(PlayerInfo.getInstance():getTableID(), TwoShowHandDataMgr.getInstance():getGiveUpUser())
    local strGender = (userInfo.cbGender == 0 and "man" or "woman")
    AudioManager:getInstance():playSound(string.format("game/twoshowhand/sound/%sthrow.mp3",strGender))
    for i=1,2 do
        self:hideProgressTimer(i)
    end
    self.m_pNodePlayMenu:setVisible(false)
    if self.m_pArmOffLine then
        self.m_pArmOffLine:setVisible(false)
    end
    
    local viewIndex = TwoShowHandDataMgr.getInstance():getViewIndex(TwoShowHandDataMgr.getInstance():getGiveUpUser())
    self:showCaoZuoTipAni(viewIndex, TwoShowHandConst.E_TYPE_GIVEUP)
end

-- 梭哈
function TwoShowHandLayer:onMsgGameShowHand(_event)
    -- print("onMsgGameShowHand")
    AudioManager:getInstance():stopSound(self.m_nClockSoundID)
    for i=1,2 do
        self:hideProgressTimer(i)
    end
    if self.m_pArmOffLine then
        self.m_pArmOffLine:setVisible(false)
    end
    self.m_pNodePlayMenu:setVisible(false)
    -- self.m_pNodeAddScore:setVisible(false)
    self:setButtonVisible(self.m_pBtnNotAdd, false)
    self:setButtonVisible(self.m_pBtnFollow, true)
    self:setButtonEnable(self.m_pBtnFollow, true)
    -- self:setButtonEnable(self.m_pBtnAdd, false)
    self:setButtonEnable(self.m_pBtnShowHand, true)
    
    local viewIndex = TwoShowHandDataMgr.getInstance():getViewIndex(TwoShowHandDataMgr.getInstance():getShowHandUser())
    local userInfo = CUserManager:getInstance():getUserInfoByChairID(PlayerInfo.getInstance():getTableID(), TwoShowHandDataMgr.getInstance():getShowHandUser())
    local addScore = userInfo.lScore - TwoShowHandDataMgr.getInstance().m_llChipNum[viewIndex]
    self:flyJetton(viewIndex, addScore, true)
    
    TwoShowHandDataMgr.getInstance().m_llChipNum[viewIndex] = userInfo.lScore
    self:updateChipNum()
    self:showCaoZuoTipAni(viewIndex, TwoShowHandConst.E_TYPE_SHOWHAND)
    
    local strSound = (userInfo.cbGender == 0 and "game/twoshowhand/sound/manshowhand.mp3" or "game/twoshowhand/sound/womanshowhand.mp3")
    AudioManager:getInstance():playSound(strSound)
    self:playSoundDelay("game/twoshowhand/sound/showhand.mp3", 0.2)
    
    self:showShowHandAni(viewIndex)
    
    if TwoShowHandDataMgr.getInstance().m_bShowHand[1] and TwoShowHandDataMgr.getInstance().m_bShowHand[2] then
        return
    end
    if TwoShowHandDataMgr.getInstance():CurrentUserIsMe() then
        self.m_pNodePlayMenu:setVisible(true)
        self:setAddScoreEnbale(false)
        self.m_pNodeAddScore:show()
        self:showProgrssTimer(1)
    else
        self.m_pNodeAddScore:hide()
        self:showProgrssTimer(2)
    end
end

-- 发牌
function TwoShowHandLayer:onMsgGameSendCard(_event)
    -- print("onMsgGameSendCard")
    AudioManager:getInstance():stopSound(self.m_nClockSoundID)
    for i=1,2 do
        self:hideProgressTimer(i)
    end
    if self.m_pArmOffLine then
        self.m_pArmOffLine:setVisible(false)
    end
    -- 加注金额
    for i=1,4 do
        local beishu = i == 3 and 4 or i
        local score = TwoShowHandDataMgr.getInstance():getCellScore()*beishu
        self.m_pLbBetNum[i]:setString(LuaUtils.getFormatGoldAndNumber(score))
    end
    self.m_pNodePlayMenu:setVisible(false)
    self:updateChipNum()
    self:dispatchCard(0)
    if TwoShowHandDataMgr.getInstance().m_bShowHand[1] and TwoShowHandDataMgr.getInstance().m_bShowHand[2] then
        return
    end
    
    local sendCardCount = TwoShowHandDataMgr.getInstance():getSendCardCount()
    self:setButtonVisible(self.m_pBtnNotAdd, true)
    self:setButtonVisible(self.m_pBtnFollow, false)
    -- self:setButtonEnable(self.m_pBtnAdd, true)
    self:setButtonEnable(self.m_pBtnShowHand, sendCardCount >= 3)
    for i=1,3 do
        self.m_pNodeBet[i]:setVisible(true)
    end
end

function TwoShowHandLayer:onMsgGameOpenCard(_event)
    for i=1,2 do
        self:showProgrssTimer(i)
    end
    self.m_bIsOpenState = true
    self.m_pNodePlayMenu:setVisible(false)
    self.m_pNodeKaiPaiMenu:setVisible(true)
end

function TwoShowHandLayer:onMsgGameOpenCardInfo(_event)
--    local _userdata = unpack(_event._userdata)
--    if not _userdata then
--        return
--    end
    local _userdata = _event

    local chairID = tonumber(_userdata)
    if chairID == PlayerInfo.getInstance():getChairID() then
        self:hideProgressTimer(1)
        self.m_pNodeKaiPaiMenu:setVisible(false)
    else
        self:hideProgressTimer(2)
    end
end

function TwoShowHandLayer:onMsgGameLookCard(_event)
    self:showCaoZuoTipAni(2, TwoShowHandConst.E_TYPE_LOOKCARD)
end

function TwoShowHandLayer:onMsgGameLookEnemyCard(_event)
    --可以看对方底牌
    self.m_pBtnEnemyObscureCard:setEnabled(true)
    --设置对方底牌数据
    local enemyCardData = TwoShowHandDataMgr.getInstance().m_cbCardData[2][1]
    local color = CommonUtils.getInstance():getCardColor(enemyCardData)
    local value = CommonUtils.getInstance():getCardValue(enemyCardData)+1
    self.m_pUserPoker[2][1]:setCardData(color, value)
end

function TwoShowHandLayer:onMsgGameEnd(_event)
    -- print("TwoShowHandLayer:onMsgGameEnd")

    AudioManager:getInstance():stopSound(self.m_nClockSoundID)
    TwoShowHandDataMgr.getInstance():setGameStatus(TwoShowHandConst.GAME_STATUS_WAIT)
    self.m_pBtnObscureCard:setEnabled(false)
    self.m_pNodePlayMenu:setVisible(false)
    self.m_pNodeKaiPaiMenu:setVisible(false)
    self.m_pNodeEnd:setVisible(true)
    self.m_bIsOpenState = false

    self.m_bSelfLooK = false
    self.m_bOtherLook = false

    for i=1,2 do
        self:hideProgressTimer(i)
    end
    if self.m_pArmOffLine then
        self.m_pArmOffLine:setVisible(false)
    end
    if self.m_pArmChaKanTip then
        self.m_pArmChaKanTip:setVisible(false)
    end

    if self.m_pOtherChipNum then
        self.m_pOtherAllChipNum = 0
        self.m_pOtherChipNum:hide()
    end

    if self.m_pSelfChipNum then
        self.m_pSelfAllChipNum = 0
        self.m_pSelfChipNum:hide()
    end
    
    local endType = TwoShowHandDataMgr.getInstance():getGameEndType()
    local sendCardCount = TwoShowHandDataMgr.getInstance():getSendCardCount()
    if endType == TwoShowHandConst.GAME_END_NORMAL and sendCardCount == 5 then
        -- 正常结算
        -- 隐藏发牌动画
        for i=1,2 do
            for n=1,5 do
                if self.m_pArmDispatch[i][n] then
                    self.m_pArmDispatch[i][n]:setVisible(false)
                end
            end
        end
        self:showCard()
        -- 显示底牌
        for i=1,2 do
            self.m_pSpObscureCard[i]:setVisible(false)
            if self.m_pUserPoker[i][1] then
                self.m_pUserPoker[i][1]:setVisible(true)
            end
        end

        for i=1,2 do
            local cardType = TwoShowHandDataMgr.getInstance():GetCardSpecialType(TwoShowHandDataMgr.getInstance().m_cbCardData[i])
            self.m_pSpResult[i]:loadTexture(string.format("gui-tshowhand-cardtype-%d.png",cardType),ccui.TextureResType.plistType)
            self.m_pSpResult[i]:setScale(2)
            self.m_pSpResult[i]:setVisible(true)
            self.m_pSpResult[i]:runAction(cc.Sequence:create(cc.ScaleTo:create(0.2, 0.7), cc.ScaleTo:create(0.05, 1)))
        end
        AudioManager:getInstance():playSound("game/twoshowhand/sound/sound-tie.mp3")

        if self.m_pShowWinner then
           scheduler.unscheduleGlobal(self.m_pShowWinner)
           self.m_pShowWinner = nil
        end
        self.m_pShowWinner = scheduler.scheduleGlobal(handler(self, self.showWinner), 0.8)
    else
        -- 弃牌或者逃跑
        self:showCard()
        local index = (TwoShowHandDataMgr.getInstance().m_llResult[1] < TwoShowHandDataMgr.getInstance().m_llResult[2] and 1 or 2)
        for i=1,2 do
            self.m_pSpObscureCard[i]:setVisible(true)
            if self.m_pUserPoker[i][1] then
                self.m_pUserPoker[i][1]:setVisible(false)
            end
            for n=1,5 do
                if self.m_pArmDispatch[i][n] then
                    self.m_pArmDispatch[i][n]:setVisible(false)
                end
                if self.m_pUserPoker[i][n] and i == index then
                    self.m_pUserPoker[i][n]:setVisible(false)
                end
            end
        end
        self.m_bIsPlayerGiveUp = index == 2
        for i=1, TwoShowHandDataMgr.getInstance():getSendCardCount() do
            self.m_pSpGiveUpCard[i] = cc.Sprite:createWithSpriteFrameName("gui-tshowhand-card-bg.png")
            local posX, posY = self.m_pSpObscureCard[index]:getPosition()
            self.m_pSpGiveUpCard[i]:setPosition(cc.p(posX+(i-1)*48, posY))
            --[[self.m_pSpGiveUpCard[i]:setScale(0.87)]]
            self.m_pNodePlay:addChild(self.m_pSpGiveUpCard[i],G_CONSTANTS.Z_ORDER_COMMON)
        end
        self:showWinner(0)
    end
end

--------------------------------
-- update
-- 对手信息
function TwoShowHandLayer:initOtherInfo()
    local otherUsers = CUserManager:getInstance():getUserInfoInTable(PlayerInfo.getInstance():getTableID())
    if #otherUsers > 0 then
        self.m_pNodeUser[2]:setVisible(true)
        self.m_vecOtherUserInfo = otherUsers[1]
        if self.m_pSpHead[2] then
            local nFaceID = self.m_vecOtherUserInfo.wFaceID
            local strHeadIcon = string.format("hall/image/file/gui-icon-head-%02d.png", nFaceID % G_CONSTANTS.FACE_NUM + 1)
            self.m_pSpHead[2]:loadTexture(strHeadIcon, ccui.TextureResType.localType)
        end
        if self.m_pSpFrame[2] then
            local nFrameID = self.m_vecOtherUserInfo.wFaceCircleID
            local strFramePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", nFrameID)
            self.m_pSpFrame[2]:loadTexture(strFramePath, ccui.TextureResType.plistType)
        end
        local strNick = LuaUtils.getDisplayNickName(self.m_vecOtherUserInfo.szNickName, 8, true)
        self.m_pLbUsrNick[2]:setString(strNick)
        -- self.m_pLbStrUsrGold[2]:setString(LuaUtils.getGoldNumberZH(self.m_vecOtherUserInfo.lScore))
        self:setOtherScore(self.m_vecOtherUserInfo.lScore)

        local viplv = self.m_vecOtherUserInfo.nVipLev
        local vipPath = string.format("hall/plist/vip/img-vip%d.png", viplv)
        self.m_pImgUsrVip[2]:loadTexture(vipPath, ccui.TextureResType.plistType)

        if self.m_vecOtherUserInfo.cbUserStatus == G_CONSTANTS.US_READY then
            self:onUserReady(self.m_vecOtherUserInfo.wChairID)
        end
        local myInfo = CUserManager:getInstance():getUserInfoByUserID(PlayerInfo.getInstance():getUserID())
        if TwoShowHandDataMgr.getInstance():getGameStatus() == TwoShowHandConst.GAME_STATUS_FREE then
            if myInfo.cbUserStatus == G_CONSTANTS.US_READY then
                self:showProgrssTimer(2)
            else
                -- 开始准备倒计时
                self:showProgrssTimer(1)
            end
        end
    end
end

function TwoShowHandLayer:setOtherScore(score)
    if not score then return end
    self.m_pLbStrUsrGold[2]:setString(LuaUtils.getGoldNumberZH(score))
end

-- 更新历史成绩
function TwoShowHandLayer:updateHistoryScore()
    -- 更新历史成绩  上一局成绩
    local historyScore = TwoShowHandDataMgr.getInstance():getHistoryScore()
    local lastScore = TwoShowHandDataMgr.getInstance():getLastScore()
    local strHistory = (historyScore > 0 and string.format("+%s",LuaUtils.getFormatGoldAndNumberAndZi(historyScore)) or string.format("%s",LuaUtils.getFormatGoldAndNumberAndZi(historyScore)))
    local strlast = (lastScore > 0 and string.format("+%s",LuaUtils.getFormatGoldAndNumberAndZi(lastScore)) or string.format("%s",LuaUtils.getFormatGoldAndNumberAndZi(lastScore)))
    self.m_pLbHistoryScore:setString(strHistory)
    self.m_pLbLastScore:setString(strlast)
end

-- 显示倒计时条
function TwoShowHandLayer:showProgrssTimer(viewIndex, nLeftTime)
    -- body
    if viewIndex < 1 or viewIndex > 2 then
        return
    end
    local leftTime = tonumber(nLeftTime) or 0

    if self.m_pProgressTimer[viewIndex] == nil then
        local pTimerImg = cc.Sprite:createWithSpriteFrameName("gui-tshowhand-progress-timer.png")
        self.m_pProgressTimer[viewIndex] = cc.ProgressTimer:create(pTimerImg)
        self.m_pProgressTimer[viewIndex]:setType(cc.PROGRESS_TIMER_TYPE_RADIAL)
        self.m_pProgressTimer[viewIndex]:setPosition(cc.p(0,13))
        self.m_pProgressTimer[viewIndex]:setReverseDirection(true)
        self.m_pNodeUser[viewIndex]:addChild(self.m_pProgressTimer[viewIndex])
    end
    self.m_pProgressTimer[viewIndex]:setVisible(true)

    local countTime = 30 
    local gameStatus = TwoShowHandDataMgr.getInstance():getGameStatus()
    if gameStatus == TwoShowHandConst.GAME_STATUS_FREE then
        countTime = TwoShowHandDataMgr.getInstance():getGameStartTime()
    elseif gameStatus == TwoShowHandConst.GAME_STATUS_PLAY then
        countTime = (TwoShowHandDataMgr.getInstance():getWaitOpenCard() and TwoShowHandDataMgr.getInstance():getOpenCardTime() or TwoShowHandDataMgr.getInstance():getDownJettonTime())
    end
    if gameStatus == TwoShowHandConst.GAME_STATUS_PLAY or (gameStatus == TwoShowHandConst.GAME_STATUS_FREE  and self.m_nCountdown[viewIndex] <= 0) then
        self.m_nCountdown[viewIndex] = (leftTime > 0 and leftTime or countTime)
        local tempTime = leftTime
        local per = (leftTime > 0 and tempTime/countTime*100 or 100)
        local progress = cc.ProgressFromTo:create(self.m_nCountdown[viewIndex], per, 0)
        self.m_pProgressTimer[viewIndex]:runAction(progress)
    end

    if viewIndex == 1 then
        if self.m_pCountDown1 then
            scheduler.unscheduleGlobal(self.m_pCountDown1)
            self.m_pCountDown1 = nil
        end
        self.m_pCountDown1 = scheduler.scheduleGlobal(handler(self, self.updateCountDown), 1.0)
    else
        if self.m_pCountDown2 then
            scheduler.unscheduleGlobal(self.m_pCountDown2)
            self.m_pCountDown2 = nil
        end
        self.m_pCountDown2 = scheduler.scheduleGlobal(handler(self, self.updateCountDown2), 1.0)
    end
end

-- 删除倒计时条
function TwoShowHandLayer:hideProgressTimer(viewIndex)
    if viewIndex < 1 or viewIndex > 2 then
        return
    end
    if self.m_pProgressTimer[viewIndex] then
        self.m_pProgressTimer[viewIndex]:removeFromParent()
        self.m_pProgressTimer[viewIndex] = nil
    end
    if viewIndex == 1 then
        if self.m_pCountDown1 then
            scheduler.unscheduleGlobal(self.m_pCountDown1)
            self.m_pCountDown1 = nil
        end
    else
        if self.m_pCountDown2 then
            scheduler.unscheduleGlobal(self.m_pCountDown2)
            self.m_pCountDown2 = nil
        end
    end
    self.m_nCountdown[viewIndex] = 0
end

-- 更新倒计时
function TwoShowHandLayer:updateCountDown(dt)
    self.m_nCountdown[1] = self.m_nCountdown[1] - 1
    if self.m_nCountdown[1] <= 0 then
        AudioManager:getInstance():stopSound(self.m_nClockSoundID)
        self:handleAuto()
    elseif self.m_nCountdown[1] == 5 then
        self.m_nClockSoundID = AudioManager:getInstance():playSound("game/twoshowhand/sound/audio_reminded.wav",true)
    end
end
function TwoShowHandLayer:updateCountDown2(dt)
    self.m_nCountdown[2] = self.m_nCountdown[2] - 1
    if self.m_nCountdown[2] == -3 then
        self:showOffLineAni()
        if self.m_pCountDown2 then
            scheduler.unscheduleGlobal(self.m_pCountDown2)
            self.m_pCountDown2 = nil
        end
    end
end

-- 倒计时结束处理
function TwoShowHandLayer:handleAuto()
    local gameStatus = TwoShowHandDataMgr.getInstance():getGameStatus()
    print("TwoShowHandLayer:handleAuto gameStatus:", gameStatus)
    if gameStatus == TwoShowHandConst.GAME_STATUS_FREE then
        local myInfo = CUserManager:getInstance():getUserInfoByUserID(PlayerInfo.getInstance():getUserID())
        print("myInfo.cbUserStatus:", myInfo.cbUserStatus)
        if myInfo.cbUserStatus ~= G_CONSTANTS.US_READY then
            --self:onMoveExitView()
            TwoShowHandClientMsg.getInstance():sendTableStandUp()
        end
    elseif gameStatus == TwoShowHandConst.GAME_STATUS_PLAY then
        if self.m_bIsOpenState then
            -- 请求开牌
            TwoShowHandClientMsg.getInstance():sendOpenCard()

        elseif TwoShowHandDataMgr.getInstance():CurrentUserIsMe() then
            -- 请求弃牌
            TwoShowHandClientMsg.getInstance():sendGiveUp()

        end
    end
end

-- 飞筹码
-- function TwoShowHandLayer:flyJetton(viewIndex, score, bShowhand)
--     if viewIndex < 1 or viewIndex > 2 then
--         return
--     end
--     AudioManager:getInstance():playSound("game/twoshowhand/sound/sound-jetton.mp3")

--     math.randomseed(tostring(os.time()):reverse():sub(1, 7))
--     math.random()
    
--     local betScore = score
--     local tempScore = { 10000000, 5000000, 1000000, 100000, 10000, 1000, 100, 10, 1}

--     local vecTemp = {} -- 当前下注动画筹码 sp vec.
--     local nCount = 0
--     -- 计算筹码个数和面值
--     for i=1,#tempScore do
--         if nCount > G_FlyJetton_Num_Max then
--             break
--         end
--         local num = math.floor(betScore/tempScore[i])
--         if num > G_FlyJetton_Num_Max then
--             num = G_FlyJetton_Num_Max
--         end

--         local nSpJettonCount = #self.m_vecSpJetton
--         for n=1,num do
--             local strJetton = string.format("new-game-jetton-%d.png",tempScore[i])
--             local spJetton = cc.Sprite:createWithSpriteFrameName(strJetton)

--             self.m_pNodeFlyJetton:addChild(spJetton, G_CONSTANTS.Z_ORDER_TOP-i)
--             spJetton:setTag(viewIndex)
--             -- 加入总筹码 vec
--             nSpJettonCount = nSpJettonCount + 1
--             self.m_vecSpJetton[nSpJettonCount] = spJetton
--             -- 加入当前下注 vec
--             nCount = nCount + 1
--             vecTemp[nCount] = spJetton
--         end
--         betScore = betScore - tempScore[i]*num

--         if betScore <= 0 then
--             break
--         end
--     end
    
--     print("筹码飞的个数：%d", nCount)
--     if bShowhand and bShowhand == true then
--         for i=1, nCount do
--             local sp = vecTemp[i]
--             sp:setVisible(false)
--             sp:setScale(0.6)
--             local off2 = cc.p(math.random(-85, 85), math.random(-50, 70))
--             if viewIndex == 2 or (nCount < 3 and i == 2) then
--                 off2 = cc.p(math.random(-85, 85), math.random(-50, 70))
--             end
--             local desPos = cc.p(667 + off2.x, 410 + off2.y)
--             sp:setPosition(desPos)
--         end
--         local callBack = cc.CallFunc:create(function()
--                 for i=1,nCount do
--                     local sp = vecTemp[i]:setVisible(true)
--                 end
--             end)
--         self:runAction(cc.Sequence:create(cc.DelayTime:create(1.0), callBack))
--     else
--         for i=1, nCount do
--             local sp = vecTemp[i]
--             sp:setScale(0.6)
--             local off = cc.p(math.random(-20, 20), math.random(-20, 20))
--             local off2 = cc.p(math.random(-85, 85), math.random(-50, 70))
--             if viewIndex == 2 or (nCount < 3 and i == 2) then
--                 off2 = cc.p(math.random(-90, 90), math.random(-60, 80))
--             end
--             local tempPos = cc.p(self.m_pNodeUser[viewIndex]:getPosition())
--             local startPos = cc.p(tempPos.x + off.x, tempPos.y + off.y)
--             sp:setPosition(startPos)
--             local desPos = cc.p(667 + off2.x, 410 + off2.y)

--             -- local tmp=1000*0.1-1000*0.3
--             -- local flyTime = math.random(32767)%tmp/1000.0+0.1
--             -- local flyTime = randf(0.1, 0.3)

--             -- 产品要求将飞筹码动画速度改慢
--             local flyTime = math.floor( math.random() * 3 + 1)
--             sp:runAction(cc.MoveTo:create(G_Random_Fly_Time[flyTime], desPos))
--         end
--     end
-- end


function TwoShowHandLayer:flyJetton(viewIndex, score, bShowhand)
    if viewIndex < 1 or viewIndex > 2 then return end
    AudioManager:getInstance():playSound("game/twoshowhand/sound/sound-jetton.mp3")
    local betScore = score
    local tempScore = { 10000000, 5000000, 1000000, 100000, 10000, 1000, 100, 10, 1}

    --为了让丢出去的筹码更加真实
    local temp_score_list = {}

    local bet_max_num = Chip_Max_Row*Chip_Max_Col

    while betScore > 0 and bet_max_num > 0 do 
        local temp_score = self:getJettonScore(betScore)
        table.insert(temp_score_list,temp_score)
        betScore = betScore - temp_score
        bet_max_num = bet_max_num -1
    end

    local vecRet = {}
    local table_len = #temp_score_list
    for i=1,table_len do
        local rand_num = math.random(1,#temp_score_list)
        table.insert(vecRet,temp_score_list[rand_num])
        table.remove(temp_score_list,rand_num)
    end

    --随机取出不重复的筹码位置
    local jetton_pos_list = {}

    for i=1,#vecRet do
        local row,col = self:getJettonRowAndCol()
        local have_num = self:getJettonByRowAndCol(row,col)

        if bShowhand then
            --当不飞筹码时 创建的位置上有筹码了就不再创建
            if have_num < 2 then
                local desPos = self:getPositionByRowAndCol(row,col)
                local spJetton = self:getJetton(vecRet[i])
                spJetton:setScale(0.6)
                spJetton:hide()
                self:setJettonZOrder()
                spJetton:move(desPos)
                self.m_pNodeFlyJetton:addChild(spJetton,10)
                spJetton:release()
                local action1 = cc.DelayTime:create(1.0)
                local action2 = cc.CallFunc:create(function ()
                    spJetton:show()
                    self:recoverJetton(row,col)
                end)
                spJetton:runAction(cc.Sequence:create(action1,action2))
                self:addJettonByRowAndCol(row,col,spJetton)
            end
        else
            --飞筹码
            if have_num < 2 then
                local desPos = self:getPositionByRowAndCol(row,col)
                local spJetton = self:getJetton(vecRet[i])
                spJetton:setScale(0.6)
                self:setJettonZOrder()
                self.m_pNodeFlyJetton:addChild(spJetton,10)
                spJetton:release()

                local tempPos = cc.p(self.m_pNodeUser[viewIndex]:getPosition())
                local off = cc.p(math.random(-20, 20), math.random(-20, 20))
                spJetton:move(tempPos.x+off.x,tempPos.y+off.y)

                -- 产品要求将飞筹码动画速度改慢
                local flyTime = math.floor( math.random() * 3 + 1)
                local action1 = cc.MoveTo:create(G_Random_Fly_Time[flyTime], desPos)
                local action2 = cc.CallFunc:create(function ()
                    self:recoverJetton(row,col)
                end)
                spJetton:runAction(cc.Sequence:create(action1,action2))
                self:addJettonByRowAndCol(row,col,spJetton)
            end
        end

    end
end

function TwoShowHandLayer:setJettonZOrder()
    for i=1,#self.m_pTableChipList do
        for j=1,#self.m_pTableChipList[i] do
            for k,v in pairs(self.m_pTableChipList[i][j]) do
                v:setLocalZOrder(1)
            end
        end
    end
end

function TwoShowHandLayer:addJettonByRowAndCol(row,col,spJetton)
    if #self.m_pTableChipList[row][col] == 0 then
        self.m_pTableChipNumber = self.m_pTableChipNumber + 1
    end
    table.insert(self.m_pTableChipList[row][col],spJetton)
    local table_have_num = 0
    for i=1,Chip_Max_Row do
        for j=1,Chip_Max_Col do
            if #self.m_pTableChipList[i][j] >= 1 then
                table_have_num = table_have_num + 1
            end
        end
    end
end

function TwoShowHandLayer:recoverJetton(row,col)
    local cur_data = self.m_pTableChipList[row][col]
    if #cur_data > 1 then
        local cur_jetton = cur_data[1]
        table.insert(self.m_pChipCacheList,cur_jetton)
        cur_jetton:retain()
        cur_jetton:removeFromParent()
        table.remove(cur_data,1)
    end
end

function TwoShowHandLayer:getJettonRowAndCol()
    local row,col = 1,1
    if self.m_pTableChipNumber >= Chip_Max_Row*Chip_Max_Col/2 then
        local rand_num1 = math.random(2,Chip_Max_Row-1)
        local rand_num2 = math.random(2,Chip_Max_Row-1)
        if rand_num1 > rand_num2 then      
            row = math.random(rand_num2,rand_num1)
        else
            row = math.random(rand_num1,rand_num2)
        end

        rand_num1 = math.random(2,Chip_Max_Col-1)
        rand_num2 = math.random(2,Chip_Max_Col-1)
        if rand_num1 > rand_num2 then      
            col = math.random(rand_num2,rand_num1)
        else
            col = math.random(rand_num1,rand_num2)
        end
    else
        row = math.random(1,Chip_Max_Row)
        col = math.random(1,Chip_Max_Col)
    end

    return row,col
end

function TwoShowHandLayer:getJetton(score)
    local spJetton = nil
    if #self.m_pChipCacheList > 0 then
        spJetton = self.m_pChipCacheList[1]
        spJetton:setSpriteFrame(string.format("new-game-jetton-%d.png",score))
        table.remove(self.m_pChipCacheList,1)
    else
        spJetton = cc.Sprite:createWithSpriteFrameName(string.format("new-game-jetton-%d.png",score))
        spJetton:retain()
    end
    return spJetton
end

function TwoShowHandLayer:getJettonByRowAndCol(row,col)
    return #self.m_pTableChipList[row][col]
end

function TwoShowHandLayer:getJettonScore(betScore)
     local tempScore = { 10000000, 5000000, 1000000, 100000, 10000, 1000, 100, 10, 1}
     for i=1,#tempScore do
        if betScore >= tempScore[i] then
            return tempScore[i]
        end
     end
     return 0
end

function TwoShowHandLayer:getPositionByRowAndCol(row,col)
    local pos_x = 530 + col * 15 + math.random(-20,20)
    local pos_y = 300 + row * 20 + math.random(-20,20)
    return cc.p(pos_x,pos_y)
end

-- 清理桌面筹码
function TwoShowHandLayer:cleanJetton()
    -- for i=1, #self.m_vecSpJetton do
    --     local sp = self.m_vecSpJetton[i]
    --     sp:stopAllActions()
    --     sp:removeFromParent()
    -- end
    -- self.m_vecSpJetton = {}
    self.m_pTableChipNumber = 0
    for i=1,#self.m_pTableChipList do
        for j=1,#self.m_pTableChipList[i] do
            for k,cur_jetton in pairs(self.m_pTableChipList[i][j]) do
                if cur_jetton then
                    table.insert(self.m_pChipCacheList,cur_jetton)
                    cur_jetton:retain()
                    cur_jetton:removeFromParent()
               end
            end
            self.m_pTableChipList[i][j] = {}
        end
    end
end

-- 显示牌
function TwoShowHandLayer:showCard()
    local sendCount = TwoShowHandDataMgr.getInstance():getSendCardCount()
    print("------sendCount:%d", sendCount)
    for i=1,2 do
        for n=1,5 do
            if n <= sendCount then
                local cbCardData = TwoShowHandDataMgr.getInstance().m_cbCardData[i][n]
                print("------showcard i:%d  n:%d   data:%d", i, n, cbCardData)
                local color = CommonUtils.getInstance():getCardColor(cbCardData)
                local value = CommonUtils.getInstance():getCardValue(cbCardData)+1
                self.m_pUserPoker[i][n]:setCardData(color, value)
                if n~= 1 then   
                    self.m_pUserPoker[i][n]:setVisible(true)  
                end
            else
                self.m_pUserPoker[i][n]:setVisible(false)
            end
        end
        if TwoShowHandDataMgr.getInstance():getGameStatus() == TwoShowHandConst.GAME_STATUS_PLAY then
            self.m_bDispatchCardFinsih = true
        end
    end
end

-- 发牌
function TwoShowHandLayer:dispatchCard(dt)
    if self.m_pDispatchCard1 then
        scheduler.unscheduleGlobal(self.m_pDispatchCard1)
        self.m_pDispatchCard1 = nil
    end

    if TwoShowHandDataMgr.getInstance():getGameStatus() ~= TwoShowHandConst.GAME_STATUS_PLAY then
        return
    end
    local sendCount = TwoShowHandDataMgr.getInstance():getSendCardCount()
    if sendCount == 2 then
        local cbCardData = TwoShowHandDataMgr.getInstance().m_cbCardData[1][1]
        local color = CommonUtils.getInstance():getCardColor(cbCardData)
        local value = CommonUtils.getInstance():getCardValue(cbCardData)+1
        self.m_pUserPoker[1][1]:setCardData(color, value)
        
        self:showFaPaiAni(1)
        self:playSoundDelay("game/twoshowhand/sound/sound-card.mp3", 0.2)

        if self.m_pDispatchCard2 then
            scheduler.unscheduleGlobal(self.m_pDispatchCard2)
            self.m_pDispatchCard2 = nil
        end
        self.m_pDispatchCard2 = scheduler.scheduleGlobal(handler(self, self.dispatchCardTwo), 0.5)
    else
        self:showFaPaiAni(sendCount)
        self:playSoundDelay("game/twoshowhand/sound/sound-card.mp3", 0.7+0.3*(sendCount-3))
        local ftimes = { 0.3,0.3,0.3}

        if self.m_pDispatchCardEnd  then
            scheduler.unscheduleGlobal(self.m_pDispatchCardEnd)
            self.m_pDispatchCardEnd = nil
            --防止网络卡发牌消息一起来 动画显示错误
            for i=1,2 do
                for n=1,5 do
                    if self.m_pArmDispatch[i][n] then
                        self.m_pArmDispatch[i][n]:setVisible(false)
                    end
                end
            end
            self:showCard()
        end
        self.m_pDispatchCardEnd = scheduler.scheduleGlobal(handler(self, self.dispatchCardEnd), ftimes[sendCount-3 + 1])
    end
end

-- 游戏开始发第二张牌
function TwoShowHandLayer:dispatchCardTwo(dt)
    if self.m_pDispatchCard2 then
        scheduler.unscheduleGlobal(self.m_pDispatchCard2)
        self.m_pDispatchCard2 = nil
    end
    self.m_bDispatchCardFinsih = true
    self:playSoundDelay("game/twoshowhand/sound/sound-card.mp3", 0.6)
    self:showFaPaiAni(2)
    for i=1,2 do
        self.m_pSpObscureCard[i]:setVisible(true)
        if self.m_pArmDispatch[i][1] then
            self.m_pArmDispatch[i][1]:setVisible(false)
        end
    end

    if self.m_pDispatchCardEnd  then
        scheduler.unscheduleGlobal(self.m_pDispatchCardEnd)
        self.m_pDispatchCardEnd = nil
    end
    self.m_pDispatchCardEnd = scheduler.scheduleGlobal(handler(self, self.dispatchCardEnd), 0.3)
end

-- 发牌结束
function TwoShowHandLayer:dispatchCardEnd(dt)
    if self.m_pDispatchCardEnd then
        scheduler.unscheduleGlobal(self.m_pDispatchCardEnd)
        self.m_pDispatchCardEnd = nil
    end

    if TwoShowHandDataMgr.getInstance():getGameStatus() ~= TwoShowHandConst.GAME_STATUS_PLAY then
        return
    end
    local sendCount = TwoShowHandDataMgr.getInstance():getSendCardCount()
    -- 点击可产看底牌提示
    if self.m_bShowChaKanTip and sendCount then
        self:showChaKanTipAni()
    end
    -- 隐藏发牌动画
    for i=1,2 do
        for n=1,5 do
            if self.m_pArmDispatch[i][n] then
                self.m_pArmDispatch[i][n]:setVisible(false)
            end
        end
    end
    self:showCard()
    if TwoShowHandDataMgr.getInstance().m_bShowHand[1] and TwoShowHandDataMgr.getInstance().m_bShowHand[2] then
        return
    end
    if TwoShowHandDataMgr.getInstance():CurrentUserIsMe() then
        self.m_pNodePlayMenu:setVisible(true)
        self:setAddScoreEnbale(true)
        self.m_pNodeAddScore:setVisible(true)
        self:showProgrssTimer(1)
    else
        self.m_pNodeAddScore:setVisible(false)
        self:showProgrssTimer(2)
    end
end

-- 清理牌
function TwoShowHandLayer:cleanCard()
    for i=1,2 do
        self.m_pSpObscureCard[i]:setVisible(false)
        for n=1,5 do
            if self.m_pArmDispatch[i][n] then
                self.m_pArmDispatch[i][n]:setVisible(false)
            end
            if self.m_pUserPoker[i][n] then
                self.m_pUserPoker[i][n]:setVisible(false)
            end
        end
    end
end

-- 更新投注额
function TwoShowHandLayer:updateChipNum()
    for i=1,2 do
        local score = self:getPlayerScoreByIndex(i)
        self.m_pLbUserChipNum[i]:setString(LuaUtils.getFormatGoldAndNumber(score))
    end
end

function TwoShowHandLayer:getPlayerScoreByIndex(index)
    if not TwoShowHandDataMgr.getInstance().m_llChipNum[index] then return 0 end
    local score = TwoShowHandDataMgr.getInstance().m_llChipNum[index]
    --如果是梭哈显示全部金币
    if TwoShowHandDataMgr.getInstance().m_bShowHand[index] then 
       if index == 1 then 
            score = PlayerInfo.getInstance():getUserScore()
       else
            if self.m_vecOtherUserInfo then
                score = self.m_vecOtherUserInfo.lScore
            end
       end
    end
    return score

end

-- 更新加注按钮
function TwoShowHandLayer:updateAddScoreStatus()
-- --     local maxTimes = TwoShowHandDataMgr.getInstance():getTurnMaxTimes()
-- --     local myTimes = TwoShowHandDataMgr.getInstance():m_iTurnCurrentDown[0]
-- --     m_pBtnAdd:setEnabled(maxTimes<4)
-- --     int index = 0
-- --     for (int i = 0 i < 4 ++i)
-- --     {
-- --         if (i+1 <= 4-maxTimes)
-- --         {
-- --             m_pNodeBet[i]:setVisible(true)
-- --             m_pNodeBet[i]:setPosition(cc.p(1230, 130+index*75))
-- --             
-- --             local cellScore = TwoShowHandDataMgr.getInstance():getCellScore()
-- --             local score = (maxTimes-myTimes)*cellScore + (i+1)*cellScore
-- --             m_pLbBetNum[i]:setString(string.format("%lld",score))
-- --             ++index
-- --         }
-- --         else
-- --         {
-- --             m_pNodeBet[i]:setVisible(false)
-- --         }
-- --     }
    for i=1,3 do
        self.m_pNodeBet[i]:setVisible(true)
        local cellScore = TwoShowHandDataMgr.getInstance():getCellScore()
        local beishu = i == 3 and 4 or i
        self.m_pLbBetNum[i]:setString(LuaUtils.getFormatGoldAndNumber(cellScore*beishu))
    end
end

-- winner
function TwoShowHandLayer:showWinner(dt)
    if self.m_pShowWinner then
        scheduler.unscheduleGlobal(self.m_pShowWinner)
        self.m_pShowWinner = nil
    end

    AudioManager:getInstance():playSound("game/twoshowhand/sound/sound-star.mp3")
    local myScore = TwoShowHandDataMgr.getInstance().m_llResult[1]
    local otherScore = TwoShowHandDataMgr.getInstance().m_llResult[2]
    local index = (myScore > otherScore and 1 or 2)
    -----todo
    -- self:showWinnerAni(index)
    
    if self.m_pShowConclude then
        scheduler.unscheduleGlobal(self.m_pShowConclude)
        self.m_pShowConclude = nil
    end
    self.m_pShowConclude = scheduler.scheduleGlobal(handler(self, self.showConclude), 0.8)
end

-- 显示结算信息
function TwoShowHandLayer:showConclude(dt)
    if self.m_pShowConclude then
        scheduler.unscheduleGlobal(self.m_pShowConclude)
        self.m_pShowConclude = nil
    end

    TwoShowHandDataMgr.getInstance():setGameStatus(TwoShowHandConst.GAME_STATUS_FREE)
    -- self:showConcludeAni()
    
    local myScore = TwoShowHandDataMgr.getInstance().m_llResult[1]
    local otherScore = TwoShowHandDataMgr.getInstance().m_llResult[2]
    local myCardType = TwoShowHandDataMgr.getInstance():GetCardSpecialType(TwoShowHandDataMgr.getInstance().m_cbCardData[1])
    local otherCardType = TwoShowHandDataMgr.getInstance():GetCardSpecialType(TwoShowHandDataMgr.getInstance().m_cbCardData[2])

    self.m_pResultScore[1]:setFntFile(myScore >= 0 and TwoShowHandRes.game_result_win_res or TwoShowHandRes.game_result_lose_res)
    self.m_pResultScore[1]:setString(myScore >= 0 and "+"..LuaUtils.getFormatGoldAndNumber(myScore) or LuaUtils.getFormatGoldAndNumber(myScore))
    self.m_pResultScore[1]:show()
    local action1 = cc.EaseElasticOut:create(cc.MoveBy:create(0.6, cc.p(0, 170)))
    local action2 = cc.CallFunc:create(function ()
        self.m_pResultScore[1]:hide()
        self.m_pResultScore[1]:setPositionY(self.m_pResultScore[1]:getPositionY()-170)
    end)
    self.m_pResultScore[1]:runAction(cc.Sequence:create(action1,cc.DelayTime:create(1),action2))

    self.m_pResultScore[2]:setFntFile(otherScore >= 0 and TwoShowHandRes.game_result_win_res or TwoShowHandRes.game_result_lose_res)
    self.m_pResultScore[2]:setString(otherScore >= 0 and "+"..LuaUtils.getFormatGoldAndNumber(otherScore) or LuaUtils.getFormatGoldAndNumber(otherScore))
    self.m_pResultScore[2]:show()

    local action3 = cc.EaseElasticOut:create(cc.MoveBy:create(0.6, cc.p(0, 150)))
    local action4 = cc.CallFunc:create(function ()
        self.m_pResultScore[2]:hide()
        self.m_pResultScore[2]:setPositionY(self.m_pResultScore[2]:getPositionY()-150)
    end)
    self.m_pResultScore[2]:runAction(cc.Sequence:create(action3,cc.DelayTime:create(1),action4))

    performWithDelay(self.m_pNodeEnd,function ()
        self:doSettle()
    end,1)


    
    local strSound = (myScore > otherScore and "game/twoshowhand/sound/win.mp3" or "game/twoshowhand/sound/fail.mp3")
    AudioManager:getInstance():playSound(strSound)
    
    -- 金币足够的情况下
    -- if PlayerInfo.getInstance():getUserScore() >= PlayerInfo.getInstance():getMinEnterScore() and not self.m_bIsKill then
        self.m_pBtnStart:setVisible(false)-- 进房间开始
        self.m_pBtnConcludeStart:setVisible(true)-- 结算开始
        if not self.m_bIsKill then 
            self.m_pNodeFree:setVisible(true)
        end
        self:showProgrssTimer(1)
    -- end
end

-- 显示结算完成
function TwoShowHandLayer:showConcludEnd()
    if self.m_bIsKill then
        -- if PlayerInfo.getInstance():IsInExperienceRoom() then  --体验房
        --     SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_MESSAGEBOX, "go-recharge")
        -- else
        -- end
        self:onMoveExitView(true)
        return
    end

    self:updateHistoryScore()
    local myScore = TwoShowHandDataMgr.getInstance().m_llResult[1] or 0
    local otherScore = TwoShowHandDataMgr.getInstance().m_llResult[2] or 0
    if TwoShowHandDataMgr.getInstance():getGameStatus() ~= TwoShowHandConst.GAME_STATUS_PLAY then
            
        -- 金币足够的情况下
        -- if PlayerInfo.getInstance():getUserScore() >= PlayerInfo.getInstance():getMinEnterScore() then
            if not self.m_bIsKill and not self.m_pSpAlready[1]:isVisible() then
                self.m_pBtnStart:setVisible(false)-- 进房间开始
                self.m_pBtnConcludeStart:setVisible(true)-- 结算开始
                self.m_pNodeFree:setVisible(true)
            end
        -- end

        local backViewIndex = -1
        if TwoShowHandDataMgr.getInstance().m_bShowHand[1] and TwoShowHandDataMgr.getInstance().m_bShowHand[2] then
            -- 双方梭哈  钱多一方筹码飞回
            if myScore < 0 and self.m_llTempStartScore[1] > otherScore then
                backViewIndex = 1
            elseif otherScore < 0 and self.m_llTempStartScore[2] > myScore then
                backViewIndex = 2
            end
        end

        math.randomseed(tostring(os.time()):reverse():sub(1, 6))
    
        -- 飞筹码到winner
        -- for i=1, #self.m_vecSpJetton do
        --     local sp = self.m_vecSpJetton[i]
        --     if sp then
        --         local off = cc.p(math.random(-20, 20), math.random(-20, 20))

        --         local desPos = {}
        --         if myScore > otherScore then
        --             desPos = cc.p(self.m_pNodeUser[1]:getPosition())
        --         else
        --             desPos = cc.p(self.m_pNodeUser[2]:getPosition())
        --         end
        --         desPos = cc.p(desPos.x + off.x, desPos.y + off.y)
        --         --[[local desPos = (myScore > otherScore and cc.p(485 + off.x, 195 + off.y) or cc.p(850 + off.x, 625 + off.y))]]
                
        --         local tmp=1000*0.15-1000*0.35
        --         local flyTime = math.random(32767)%tmp/1000.0+0.15
        --         -- [[local flyTime = randf(0.15, 0.35)]]

        --         local index = #self.m_vecSpJetton/4*3
        --         if backViewIndex ~= -1 and (i-1 >= index) then
        --             --[[desPos = (backViewIndex == 1 and cc.p(485 + off.x, 195 + off.y) or cc.p(850 + off.x, 625 + off.y))]]
        --             sp:runAction(cc.Sequence:create(cc.DelayTime:create(0.4), cc.MoveTo:create(flyTime, desPos)))
        --         else
        --             sp:runAction(cc.MoveTo:create(flyTime, desPos))
        --         end
        --     end
        -- end

        for i=1,#self.m_pTableChipList do
            for j=1,#self.m_pTableChipList[i] do
                for k,cur_jetton in pairs(self.m_pTableChipList[i][j]) do
                    if cur_jetton then
                        local off = cc.p(math.random(-20, 20), math.random(-20, 20))

                        local desPos = {}
                        if myScore > otherScore then
                            desPos = cc.p(self.m_pNodeUser[1]:getPosition())
                        else
                            desPos = cc.p(self.m_pNodeUser[2]:getPosition())
                        end
                        desPos = cc.p(desPos.x + off.x, desPos.y + off.y)
                        --[[local desPos = (myScore > otherScore and cc.p(485 + off.x, 195 + off.y) or cc.p(850 + off.x, 625 + off.y))]]
                        
                        local tmp=1000*0.15-1000*0.35
                        local flyTime = math.random(32767)%tmp/1000.0+0.15
                        -- [[local flyTime = randf(0.15, 0.35)]]

                        local call_func = cc.CallFunc:create(function ()
                            table.insert(self.m_pChipCacheList,cur_jetton)
                            cur_jetton:retain()
                            cur_jetton:removeFromParent()
                        end)
                        cur_jetton:runAction(cc.Sequence:create(cc.DelayTime:create(0), cc.MoveTo:create(flyTime, desPos),call_func))
                    end
                end
                self.m_pTableChipList[i][j] = {}
            end
        end

    end
end

--------------------------
-- anim
--等待玩家动画
function TwoShowHandLayer:showWaitUserAni()
    -- ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("game/twoshowhand/effect/325_huanle5zhang_dengdaiwanjia/huanle5zhang_dengdaiwanjia.ExportJson")
    local strName = "huanle5zhang_dengdaiwanjia"
    if not self.m_pArmWaitUser then
        self.m_pArmWaitUser = ccs.Armature:create(strName)
        self.m_pArmWaitUser:move(1334/2,750/2-100 )
        self.m_pTwoPanel:addChild(self.m_pArmWaitUser)
    end
    self.m_pArmWaitUser:show()
    self.m_pArmWaitUser:getAnimation():play("Animation1")
end


-- 发牌动画
function TwoShowHandLayer:showFaPaiAni(index)
    print("showFaPaiAni index:", index)
    if index >= 2 then
        TwoShowHandDataMgr.getInstance():setSelfAdd(false)
    end

    if self.m_pOtherChipNum then
        self.m_pOtherAllChipNum = 0
        performWithDelay(self.m_pOtherChipNum,function ()
            self.m_pOtherChipNum:hide()
        end,0.7)
    end

    if self.m_pSelfChipNum then
        self.m_pSelfAllChipNum = 0
        performWithDelay(self.m_pSelfChipNum,function ()
            self.m_pSelfChipNum:hide()
        end,0.7)
    end

    -- ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("game/twoshowhand/effect/fapai_suoha/huanle5zhang_fapai.ExportJson")
    local strName  = "huanle5zhang_fapai"
    local strAnis1 = {
                {"Animation6", "Animation7", "Animation8", "Animation9", "Animation10"},
                {"Animation1", "Animation2", "Animation3", "Animation4", "Animation5"}
            }
    for i=1,2 do
        local strAniName = strAnis1[i][index]
        if self.m_pArmDispatch[i][index] == nil then
            self.m_pArmDispatch[i][index] = ccs.Armature:create(strName)
            if i == 2 then 
                self.m_pArmDispatch[i][index]:setPosition(cc.p(716,365))
            else
                self.m_pArmDispatch[i][index]:setPosition(cc.p(695,325))
            end
            self.m_pNodePlay:addChild(self.m_pArmDispatch[i][index], G_CONSTANTS.Z_ORDER_OVERRIDE)
        end
        self.m_pArmDispatch[i][index]:setVisible(true)
        self.m_pArmDispatch[i][index]:getAnimation():play(strAniName)
    end
end

-- 操作提示动画
function TwoShowHandLayer:showCaoZuoTipAni(viewIndex, type)
    if type == TwoShowHandConst.E_TYPE_MAX or type== TwoShowHandConst.E_TYPE_MIN then
        type = TwoShowHandConst.E_TYPE_ADD
    end
    -- ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("game/twoshowhand/effect/caozuotishi_suoha/huanle5zhang4_tishi.ExportJson")
    local strName  = "huanle5zhang4_tishi"
    local strAnis = { {"Animation1", "Animation2", "Animation3", "Animation4", "Animation5", "Animation11", "Animation12", "Animation13"},
        {"Animation6", "Animation7", "Animation8", "Animation9", "Animation10", "Animation14", "Animation15", "Animation16"}}
    local armature = ccs.Armature:create(strName)
    local pos = (viewIndex == 1 and cc.p(235, 288) or cc.p(1154, 635))
    armature:setPosition(pos)
    armature:getAnimation():play(strAnis[viewIndex][type+1])
    self.m_pNodePlay:addChild(armature,G_CONSTANTS.Z_ORDER_MODAL)

    local animationEvent = function (armatureBack,movementType,movementID)
        if movementType == ccs.MovementEventType.complete then
            self:effectEnd(armatureBack,movementType,strName)
        end
    end
    armature:getAnimation():setMovementEventCallFunc(animationEvent)
    
    -- if type == TwoShowHandConst.E_TYPE_MAX or type== TwoShowHandConst.E_TYPE_MIN then
    --     local cellScore = TwoShowHandDataMgr.getInstance():getCellScore()
    --     local strScore = (type == TwoShowHandConst.E_TYPE_MIN and tostring(cellScore) or tostring(cellScore*4))
    --     local lbAddScore = cc.Label:createWithSystemFont(strScore, "Helevetica", 25)
    --     local pos = (viewIndex == 1 and cc.p(15, -17) or cc.p(33, -10))
    --     lbAddScore:setPosition(pos)
    --     armature:addChild(lbAddScore, G_CONSTANTS.Z_ORDER_TOP)
    -- end
end

-- 梭哈动画
function TwoShowHandLayer:showShowHandAni(viewIndex)
    -- ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("game/twoshowhand/effect/suohua_suohua/huanle5zhang4_suohua.ExportJson")
    local strName  = "huanle5zhang4_suohua"
    local armature = ccs.Armature:create(strName)
    armature:setPosition(cc.p(667, 375))
    armature:getAnimation():play("animation1")
    armature:setTag(viewIndex)
    self.m_pNodePlay:addChild(armature, G_CONSTANTS.Z_ORDER_MODAL)
    if viewIndex ~= 1 then
        armature:setRotation(180)
        armature:setPosition(cc.p(667, 455))
    end
    local animationEvent = function (armatureBack,movementType,movementID)
        if movementType == ccs.MovementEventType.complete then
            self:effectEnd(armatureBack,movementType,strName)
        end
    end
    armature:getAnimation():setMovementEventCallFunc(animationEvent)
end

-- winner动画
function TwoShowHandLayer:showWinnerAni(viewIndex)
    -- ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("game/twoshowhand/effect/winner_2renniuniu/winner_2renniuniu.ExportJson")
    local strName  = "winner_2renniuniu"
    if self.m_pArmWinnerFrame == nil then
        self.m_pArmWinnerFrame = ccs.Armature:create(strName)
        self.m_pArmWinnerFrame:getAnimation():play("Animation2")
        self.m_pArmWinnerFrame:addTo(self.m_pNodeEnd, G_CONSTANTS.Z_ORDER_BACKGROUND)
    end
    local otherUsers = CUserManager:getInstance():getUserInfoInTable(PlayerInfo.getInstance():getTableID())
    if TwoShowHandDataMgr.getInstance().m_llResult[1] > TwoShowHandDataMgr.getInstance().m_llResult[2] then
        self.m_pArmWinnerFrame:setVisible(true)
    else
        self.m_pArmWinnerFrame:setVisible(#otherUsers > 0)
    end
    local pos = (viewIndex == 1 and cc.p(self.m_pNodeUser[1]:getPosition()) or cc.p(self.m_pNodeUser[2]:getPosition()))
    self.m_pArmWinnerFrame:setPosition(pos)
    
    -- winner扫光
    local strAniName = (viewIndex == 1 and "Animation1" or "Animation2desad")
    local armature = ccs.Armature:create(strName)
    local pos2 = (viewIndex == 1 and cc.p(700, 180) or cc.p(660, 620))
    armature:setPosition(pos2)
    armature:getAnimation():play(strAniName)
    self.m_pNodeEnd:addChild(armature, G_CONSTANTS.Z_ORDER_BACKGROUND)
    local animationEvent = function (armatureBack,movementType,movementID)
        if movementType == ccs.MovementEventType.complete then
            self:effectEnd(armatureBack,movementType,strName)
        end
    end
    armature:getAnimation():setMovementEventCallFunc(animationEvent)
end

-- 结算框动画
function TwoShowHandLayer:showConcludeAni()
    self.m_pNodeEnd:show()
    local strName  = (TwoShowHandDataMgr.getInstance().m_llResult[1] > 0 and "animation1_1" or "animation2_1")
    local strNameTwo = (TwoShowHandDataMgr.getInstance().m_llResult[1] > 0 and "animation1_2" or "animation2_2")

    if not self.m_pNodeSkeConclue then
        self.m_pNodeSkeConclue = SpineManager.getInstance():getSpine("game/twoshowhand/effect/325_huanle5zhang_jiesuan/huanle5zhang4_jiesuan")
    end
    if not self.m_pNodeSkeConclue then return end

    self.m_pNodeSkeConclue:move(667,375)
    self.m_pNodeSkeConclue:setAnimation(0,strName,false)
    self.m_pNodeSkeConclue:addTo(self.m_pNodeEnd,G_CONSTANTS.Z_ORDER_STATIC)

    local temp_sch = false

    self.m_pNodeSkeConclue:registerSpineEventHandler(function (event)
        if temp_sch then  return end
        temp_sch = true
        self.m_pNodeSkeConclue:setAnimation(0,strNameTwo,true)
        self.m_pNodeSkeConclue:setMix(strName,strNameTwo,0.2)    

    end,sp.EventType.ANIMATION_COMPLETE)

    --添加一个结算界面关闭定时器
    performWithDelay(self.m_pNodeEnd,function ()
        self:doSettle()
    end,4)

end

--结算定时器结束
function TwoShowHandLayer:doSettle()
    if self.m_pNodeSkeConclue then
        self.m_pNodeSkeConclue:unregisterSpineEventHandler(sp.EventType.ANIMATION_COMPLETE)
        self.m_pNodeSkeConclue:removeFromParent()
        self.m_pNodeSkeConclue = nil
    end
    self:showConcludEnd()
end

-- 查看底牌提示
function TwoShowHandLayer:showChaKanTipAni()
    -- ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("game/twoshowhand/effect/chakantishi_suoha/chakantishi_suoha.ExportJson")
    if self.m_pArmChaKanTip == nil then
        local strName  = "chakantishi_suoha"
        self.m_pArmChaKanTip = ccs.Armature:create(strName)
        self.m_pArmChaKanTip:setPosition(cc.p(560, 185))
        self.m_pArmChaKanTip:getAnimation():play("Animation1")
        self.m_pNodePlay:addChild(self.m_pArmChaKanTip, G_CONSTANTS.Z_ORDER_MODAL)
    end
    self.m_pArmChaKanTip:setVisible(true)
end

--背景动画
function TwoShowHandLayer:showBgAni()
    -- ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("game/twoshowhand/effect/game_bg/huanle5zhang4_beijing.ExportJson")
    local strName  = "huanle5zhang4_beijing"
    local m_pArmBg = ccs.Armature:create(strName)
    m_pArmBg:move(display.cx,display.cy)
    m_pArmBg:getAnimation():play("Animation1")
    self.m_pTwoBg:addChild(m_pArmBg)

end

-- offline
function TwoShowHandLayer:showOffLineAni()
    -- ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("game/twoshowhand/effect/diaoxian_doudizhu/diaoxian_doudizhu.ExportJson")
    if self.m_pArmOffLine == nil then
        local strName  = "diaoxian_doudizhu"
        self.m_pArmOffLine = ccs.Armature:create(strName)
        local pos = cc.p(self.m_pNodeUser[2]:getPosition())
        self.m_pArmOffLine:setPosition(cc.p(pos.x + 120, pos.y))
        self.m_pArmOffLine:getAnimation():play("Animation1")
        self.m_pNodePlay:addChild(self.m_pArmOffLine, G_CONSTANTS.Z_ORDER_MODAL)
    end
    self.m_pArmOffLine:setVisible(true)
end

-- 动画结束回调
function TwoShowHandLayer:effectEnd(armature, type, strArmName)
    if not armature then
        return
    end
    --[[local strArmName = armature:getName()]]
    if strArmName == "jiesuanyingle_suoha" or strArmName == "jiesuanshule_suoha" then
        self:showConcludEnd()
    end
    armature:removeFromParent()
    armature = nil
end

--------------------
-- help func
function TwoShowHandLayer:onGameStart(bIsNormalStart)
    -- body
    AudioManager:getInstance():stopSound(self.m_nClockSoundID)
    self:cleanCard()
    self:cleanJetton()
    self:updateHistoryScore()

    if self.m_pArmWaitUser then
        self.m_pArmWaitUser:hide()
    end
    self.m_bIsPlayerGiveUp = false
    self.m_bIsOpenState = false
    self.m_bDispatchCardFinsih = false
    self.m_pNodeFree:setVisible(false)
    self.m_pNodeEnd:setVisible(false)

    self.m_bSelfLooK = false
    self.m_bOtherLook = false

	--fix 有几率未清除掉
    if self.m_pNodeAniConclue ~= nil then
        self.m_pNodeAniConclue:removeFromParent()
        self.m_pNodeAniConclue = nil
    end
    self.m_pNodePlay:setVisible(true)
    self.m_pNodePlayMenu:setVisible(false)
    self.m_pNodeKaiPaiMenu:setVisible(false)
    self:setButtonVisible(self.m_pBtnNotAdd, false)
    self.m_pNodeFlyJetton:setVisible(true)
    for i=1,2 do
        self.m_pSpAlready[i]:setVisible(false)
        self.m_pSpResult[i]:setVisible(false)
        self.m_pSpObscureCard[i]:setVisible(false)
        self:hideProgressTimer(i)
    end
    if self.m_pArmOffLine then
        self.m_pArmOffLine:setVisible(false)
    end
    if self.m_pArmWinnerFrame then
        self.m_pArmWinnerFrame:setVisible(false)
    end
    for i=1,5 do
        if self.m_pSpGiveUpCard[i] then
            self.m_pSpGiveUpCard[i]:removeFromParent()
            self.m_pSpGiveUpCard[i] = nil
        end
    end
    self.m_pSpGiveUpCard = {}
    -- 加注金额
    for i=1,3 do
        self.m_pNodeBet[i]:setVisible(true)
        local beishu = i == 3 and 4 or i
        local score = TwoShowHandDataMgr.getInstance():getCellScore()*beishu
        self.m_pLbBetNum[i]:setString(LuaUtils.getFormatGoldAndNumber(score))
    end
    
    -- 飞筹码
    for i=1,2 do
        self.m_pNodeUserChip[i]:setVisible(true)
        self:updateChipNum()
        if bIsNormalStart then
            self:flyJetton(i, TwoShowHandDataMgr.getInstance():getCellScore())
        end
        
        self.m_llTempStartScore[1] = PlayerInfo.getInstance():getUserScore()
        local user = CUserManager:getInstance():getUserInfoInTable(PlayerInfo.getInstance():getTableID())
        if #user > 0 then
            self.m_llTempStartScore[2] = user[1].lScore
        end
    end
    
    self.m_pBtnObscureCard:setEnabled(true)
    self.m_pBtnGiveUp:setEnabled(true)
    self:setButtonVisible(self.m_pBtnNotAdd, true)
    self:setButtonEnable(self.m_pBtnShowHand, false)
    self:setButtonVisible(self.m_pBtnFollow, false)
    -- self:setButtonEnable(self.m_pBtnAdd, true)

    --底牌
    for i=1,2 do 
        self.m_pUserPoker[i][1]:setVisible(false)
    end

    if bIsNormalStart then
        -- 发牌
        if self.m_pDispatchCard1 then
           scheduler.unscheduleGlobal(self.m_pDispatchCard1)
           self.m_pDispatchCard1 = nil
        end
        self.m_pDispatchCard1 = scheduler.scheduleGlobal(handler(self, self.dispatchCard), 0.25)
    end
end

function TwoShowHandLayer:onUserReady(nChairId)
    -- body
    AudioManager:getInstance():stopSound(self.m_nClockSoundID)
    AudioManager:getInstance():playSound("game/twoshowhand/sound/sound-ready.mp3")
    
    local otherUsers = CUserManager:getInstance():getUserInfoInTable(PlayerInfo.getInstance():getTableID())
    if nChairId == PlayerInfo.getInstance():getChairID() then
        -- 自己准备
        self.m_pSpAlready[1]:setVisible(true)
        self.m_pNodeFree:setVisible(false)
        self.m_pNodePlay:setVisible(false)
        self.m_pNodeEnd:setVisible(false)
        self.m_pNodeFlyJetton:setVisible(false)
        self.m_pNodeUserChip[1]:setVisible(false)
        self.m_pLbUserChipNum[1]:setString("")
        self.m_pBtnObscureCard:setEnabled(false)
        self.m_pSpResult[1]:setVisible(false)
        
        self:cleanJetton()
        self:cleanCard()
        self:updateHistoryScore()
        self:hideProgressTimer(1)
        if #otherUsers > 0 then
            self:showProgrssTimer(2)
        end
        for i=1,5 do
            if self.m_pSpGiveUpCard[i] then
                self.m_pSpGiveUpCard[i]:removeFromParent()
                self.m_pSpGiveUpCard[i] = nil
            end
        end
        --添加等待玩家动画
        self:showWaitUserAni()
    else
        -- 对手准备
        self.m_pSpAlready[2]:setVisible(true)
        self.m_pNodeUserChip[2]:setVisible(false)
        self.m_pLbUserChipNum[2]:setString("")
        
        -----------------------
        -- just for test.
        -- 隐藏手牌
        for n=1,5 do
            if self.m_pArmDispatch[2][n] then
                self.m_pArmDispatch[2][n]:setVisible(false)
            end
            if self.m_pUserPoker[2][n] then
                self.m_pUserPoker[2][n]:setVisible(false)
            end
            if self.m_bIsPlayerGiveUp and self.m_pSpGiveUpCard[n] then
                self.m_pSpGiveUpCard[n]:removeFromParent()
                self.m_pSpGiveUpCard[n] = nil
            end
        end
        self.m_pSpResult[2]:setVisible(false)
        self.m_pSpObscureCard[2]:setVisible(false)
        -----------------------

        self:hideProgressTimer(2)
        self:showProgrssTimer(1)
    end
end

function TwoShowHandLayer:playSoundDelay(strSound, delayTime)
    if string.len(strSound) <= 0 then
        return
    end
    if delayTime <= 0 then
        AudioManager:getInstance():playSound(strSound)
        return
    end
    local callBack = cc.CallFunc:create(function()
            AudioManager:getInstance():playSound(strSound)
        end)
    self.m_pNodePlay:runAction(cc.Sequence:create(cc.DelayTime:create(delayTime), callBack))
end

function TwoShowHandLayer:setButtonEnable(btn, bEnable)
    if not btn then
        return
    end

    btn:setEnabled(bEnable)
    local node = btn:getParent()
    -- local sp = node:getChildByTag(10)
    -- local spDis = node:getChildByTag(11)
    -- if sp then
    --     sp:setVisible(bEnable)
    -- end
    -- if spDis then
    --     spDis:setVisible(not bEnable)
    -- end
end

function TwoShowHandLayer:setButtonVisible(btn,visible)
    if not btn then return end
    btn:setVisible(visible)
end

-- isToHall: true 返回大厅，否则返回选桌页面
-- isShowGameList: 返回大厅时是否显示游戏房间列表
function TwoShowHandLayer:onMoveExitView(isToHall)

    self:leaveGameClean()

    PlayerInfo.getInstance():setChairID(G_CONSTANTS.INVALID_CHAIR)
    PlayerInfo.getInstance():setTableID(G_CONSTANTS.INVALID_TABLE)
    PlayerInfo.getInstance():setSitSuc(false)
    PlayerInfo.getInstance():setIsQuickStart(false)
    PlayerInfo.getInstance():setIsReLoginInGame(false)

    --AudioManager.getInstance():playMusic("sound/table_choose_music.mp3")
    AudioManager:getInstance():stopAllSounds()

    TwoShowHandDataMgr.getInstance():Clean()
    if self.m_bIsKill or (isToHall and isToHall == true) then --被踢返回大厅
        --返回大厅
        PlayerInfo.getInstance():setIsGameBackToHall(true)
        SLFacade:dispatchCustomEvent(Public_Events.Load_Entry)
        SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_MESSAGEBOX, "go-recharge")
    else
        SLFacade:dispatchCustomEvent(Public_Events.MSG_GAME_LOAD_SUCCESS)
    end
end

----------------------------------------
-- ccbi 按纽响应
function TwoShowHandLayer:onPopClicked(pSender)
    -- 动画过程中不响应
    if self.m_bIsMenuMove then
        return
    end
    self.m_bIsMenuMove = true

    self.m_pNodeMenu:setPositionY(168)
    local callBack = cc.CallFunc:create(function()
        self.m_pBtnMenuPop:setVisible(false)
        self.m_pBtnMenuPush:setVisible(true)
    end)
    local callBack2 = cc.CallFunc:create(function()
        self.m_bIsMenuMove = false
    end)
    showMenuPop(self.m_pNodeMenu, callBack, callBack2, self.m_pNodeMenu:getPositionX(), 0)
end

function TwoShowHandLayer:onPushClicked(pSender)
    -- 动画过程中不响应
    if self.m_bIsMenuMove then
        return
    end
    self.m_bIsMenuMove = true

    local callBack = cc.CallFunc:create(function()
        self.m_pBtnMenuPop:setVisible(true)
        self.m_pBtnMenuPush:setVisible(false)
    end)
    local callBack2 = cc.CallFunc:create(function()
        self.m_bIsMenuMove = false
    end)
    showMenuPush(self.m_pNodeMenu, callBack, callBack2, self.m_pNodeMenu:getPositionX(), 168)
end

function TwoShowHandLayer.onMsgClicked(pSender)
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
    local self = TwoShowHandLayer.inst_

    -- TODO. open chat.
    -- self.m_pNodeNewMsg:setVisible(false)
    -- MsgCenter:getInstance():postMsg(MSG_SHOW_VIEW, __String:create("chat"))
end

function TwoShowHandLayer:onSoundClicked(pSender)
    if self.m_bIsMenuMove then 
        return
    end
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")

    local sound_res = ""
    -- 音效
    if (AudioManager.getInstance():getSoundOn())then -- 音效开着
        AudioManager.getInstance():setSoundOn(false)
        sound_res = "gui-tshowhand-button-shengying.png"
    else
        AudioManager.getInstance():setSoundOn(true)
        sound_res = "gui-tshowhand-button-shengying-2.png"
    end
    self.m_pBtnSound:loadTextureNormal(sound_res, ccui.TextureResType.plistType)
    self.m_pBtnSound:loadTexturePressed(sound_res, ccui.TextureResType.plistType)
end

function TwoShowHandLayer:onMusicClicked(pSender)
    if self.m_bIsMenuMove then 
        return
    end
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
    
    local music_res = ""
    -- 音乐
    if (AudioManager.getInstance():getMusicOn())then -- 音乐开着 
        AudioManager.getInstance():setMusicOn(false)
        music_res = "gui-tshowhand-button-yingyue.png"
    else
        AudioManager.getInstance():setMusicOn(true)
        music_res = "gui-tshowhand-button-yingyue-2.png"
        --播放音乐
        local strPath = AudioManager.getInstance():getStrMusicPath()
        AudioManager.getInstance():playMusic(strPath)
    end

    self.m_pBtnMusic:loadTextureNormal(music_res, ccui.TextureResType.plistType)
    self.m_pBtnMusic:loadTexturePressed(music_res, ccui.TextureResType.plistType)
end

function TwoShowHandLayer:onReturnClicked(pSender)

    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
    local self = TwoShowHandLayer.inst_

    if self.m_bIsKill then
        return
    end

    -- -- 收起菜单
    -- self.onPushClicked(nil)

    if TwoShowHandDataMgr.getInstance():getGameStatus() == TwoShowHandConst.GAME_STATUS_PLAY then
        -- 游戏中，需要提示
        SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_MESSAGEBOX, "game-exit-3")
    else
        TwoShowHandClientMsg.getInstance():sendTableStandUp()
    end
end

----todo
-- function TwoShowHandLayer:onCardTypeClicked(pSender)
--     AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
--     local self = TwoShowHandLayer.inst_

--     local tag = pSender:getTag()
--     self.m_pNodeCardType:setVisible(tag)
--     if tag == 1 then
--         self.m_pNodeCardType:setPosition(cc.p(-440, 10))
--         local move = cc.MoveTo:create(0.4, cc.p(0, 10))
--         self.m_pNodeCardType:runAction(cc.EaseBackInOut:create(move))
--     end
-- end

function TwoShowHandLayer:onRuleClicked(pSender)
    if self.m_bIsMenuMove then 
        return
    end
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")

    if self.m_rootUI:getChildByName("TwoShowHandRuleLayer") then
        self.m_rootUI:getChildByName("TwoShowHandRuleLayer"):setVisible(true)
        return
    end

    local pTwoShowHandRuleLayer = TwoShowHandRuleLayer:create()
    pTwoShowHandRuleLayer:setPositionX((display.width - 1624) / 2)
    self.m_rootUI:addChild(pTwoShowHandRuleLayer, G_CONSTANTS.Z_ORDER_COMMON)
end

function TwoShowHandLayer:onUserInfoClicked(pSender)
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
    local self = TwoShowHandLayer.inst_
    
    local tag = pSender:getTag()
    if self.m_pNodeUserInfo:getChildByTag(100+tag) then
        return
    end
    local pos = (tag == 0 and cc.p(20,150) or cc.p(810, 365))
    local userID = PlayerInfo.getInstance():getUserID()
    if tag == 1 then
        local otherUsers = CUserManager:getInstance():getUserInfoInTable(PlayerInfo.getInstance():getTableID(),false)
        if #otherUsers == 0 then
            return
        end
        local otherUser = otherUsers[1]
        userID = otherUser.dwUserID
    end
    local dialog = CommonUserInfo.create(1)
    dialog:setPosition(cc.p(self:getPositionX()+pos.x, self:getPositionY()+pos.y))
    self.m_pNodeUserInfo:addChild(dialog)
    dialog:setTag(100+tag)
    dialog:updateUserInfo(userID)
end

function TwoShowHandLayer:onStartClicked(pSender)
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")

    TwoShowHandClientMsg.getInstance():sendUserReady()
end

function TwoShowHandLayer:onPlayMenuClicked(pSender,eventType)

    if eventType == ccui.TouchEventType.began then
        AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
        pSender:setScale(pSender:getScale()+0.03)
    elseif eventType == ccui.TouchEventType.moved then
    elseif eventType == ccui.TouchEventType.ended then
        pSender:setScale(pSender:getScale()-0.03)

        local isHide = true
        local tag = pSender:getTag()
        if tag == 0 or tag == 5 then
            -- 弃牌
            TwoShowHandClientMsg.getInstance():sendGiveUp()
        elseif tag == 1 then 
            --不加
            TwoShowHandClientMsg.getInstance():sendNotAdd()
        elseif tag == 2 then
            --梭哈
            TwoShowHandClientMsg.getInstance():sendShowHand()
        elseif tag == 3 then
            --跟注
            TwoShowHandClientMsg.getInstance():sendFollow()
        elseif tag == 4 then
            --加注
            -- if self.m_pNodeAddScore:isVisible() then
            --     --再次点击加注 默认加最小注
            --     TwoShowHandClientMsg.getInstance():sendAddScore(1)
            -- end
            -- local bIsbtnShow = self.m_pNodeAddScore:isVisible()
            -- self.m_pNodeAddScore:setVisible(not bIsbtnShow)
            -- isHide = false
        elseif tag == 6 then
            --开牌
            TwoShowHandClientMsg.getInstance():sendOpenCard()
        end
        if isHide then 
            self.m_pNodePlayMenu:setVisible(false)
        end

    elseif eventType == ccui.TouchEventType.canceled then
        pSender:setScale(pSender:getScale()-0.03)
    end 

end

-- 取消加注(点击背景取消)
function TwoShowHandLayer:onCancelAddMenuClicked(pSender)
    -- local bIsbtnShow = self.m_pNodeAddScore:isVisible()
    -- if bIsbtnShow then
    --     self.m_pNodeAddScore:setVisible(false)
    -- end
end

function TwoShowHandLayer:onBetClicked(pSender,eventType)
    
    if eventType == ccui.TouchEventType.began then
        AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
        pSender:setScale(pSender:getScale()+0.03)
    elseif eventType == ccui.TouchEventType.moved then
    elseif eventType == ccui.TouchEventType.ended then
        pSender:setScale(pSender:getScale()-0.03)
        TwoShowHandDataMgr.getInstance():setSelfAdd(true)
        local tag = pSender:getTag()
        if tag == 2 then
            tag = 3
        end
        TwoShowHandClientMsg.getInstance():sendAddScore(tag+1)
    elseif eventType == ccui.TouchEventType.canceled then 
        pSender:setScale(pSender:getScale()-0.03)
    end
end

function TwoShowHandLayer.onObscureCardClicked(pSender)
    local self = TwoShowHandLayer.inst_
    self:onObscureCardClicked2()
end

----------------
-- 代码中重新设置 ccbi 按纽事件响应
function TwoShowHandLayer:onObscureCardClicked2()
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
    
    if TwoShowHandDataMgr.getInstance():getGameStatus() == TwoShowHandConst.GAME_STATUS_PLAY then
        if self.m_pUserPoker[1][1] then
            self.m_pUserPoker[1][1]:setVisible(false)
        end
    end
end
function TwoShowHandLayer:onObscureCardTouchIn(pSender)
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")

    if self.m_bShowChaKanTip then
        cc.UserDefault:getInstance():setBoolForKey("showhandtip", false)
        self.m_bShowChaKanTip = false
        if self.m_pArmChaKanTip then
            self.m_pArmChaKanTip:setVisible(false)
        end
    end
    print("--------发送消息给11111---------")
    if not self.m_bDispatchCardFinsih then 
        return
    end
    if self.m_pUserPoker[1][1] then
        self.m_pUserPoker[1][1]:setVisible(true)
    end
    print("--------发送消息给22222---------")
    --限制发看牌消息频率
    local nCurTime = os.time()
    if self.m_nLastTouchTime and nCurTime - self.m_nLastTouchTime <= 0 then
        return
    end
    self.m_nLastTouchTime = nCurTime
    print("--------发送消息给severs---------")
    TwoShowHandClientMsg.getInstance():sendLookCard()
end

function TwoShowHandLayer:tryFixIphoneX()
    if LuaUtils.isIphoneXDesignResolution() then
        self.m_pBtnMenuPop:setPositionX(self.m_pBtnMenuPop:getPositionX() + 80)
        self.m_pBtnMenuPush:setPositionX(self.m_pBtnMenuPush:getPositionX() + 80)
        self.m_pClippingMenu:setPositionX(self.m_pClippingMenu:getPositionX() + 80)
        self.m_pBtnExit:setPositionX(self.m_pBtnExit:getPositionX()-80)

        self.m_pChatLayer:setLeftBtnOffsetX(-80)
        self.m_pChatLayer:setNodeAllOffset(-80,0)
    end
end

--游戏中打开银行
function TwoShowHandLayer:onOpenInGameBankClicked()
   
    AudioManager.getInstance():playSound("res/public/sound/sound-button.mp3")

    --如果再游戏中不能取款
    if TwoShowHandDataMgr.getInstance():getGameStatus() == TwoShowHandConst.GAME_STATUS_PLAY then
        FloatMessage.getInstance():pushMessage("您正在游戏中，请稍后进行取款操作。")
        return
    end

    --体验场不能取钱
    if PlayerInfo.getInstance():IsInExperienceRoom() then 
        FloatMessage.getInstance():pushMessage("EXPERIENCE_BANK")
        return 
    end
    --没有设置密码
    if (PlayerInfo.getInstance():getIsInsurePass() == 0) then
        FloatMessage.getInstance():pushMessage("STRING_033")
        return
    end


    --弹出银行界面
    local GameConfig = GameListConfig[PlayerInfo.getInstance():getKindID()]
    local msg 
    if (GameConfig.SHOWTYPE == 1) then
        msg = 
        {
            strPath = GameConfig.BANKPATH or "hall/csb/BankInGame.csb", 
            show_type = GameConfig.SHOWTYPE,
        }
    else
        msg = GameConfig.BANKPATH or "hall/csb/BankInGame.csb"
    end

    SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_INGAMEBANK,msg)

end

function TwoShowHandLayer:onChangeTableClicked()
    AudioManager.getInstance():playSound("res/public/sound/sound-button.mp3")

    TwoShowHandClientMsg.getInstance():sendChangeTable()
end

function TwoShowHandLayer:event_UpdateOtherScore()
    local userID = CUserManager:getInstance():getUserIDScoreGR()

    if userID == PlayerInfo.getInstance():getUserID() then return end
    
    local is_on_table = CUserManager:getInstance():getInTableByTableIDAndUserID(PlayerInfo.getInstance():getTableID(),userID)
    if not is_on_table then return end

    local score = CUserManager:getInstance():getUserScoreByUserID(userID)
    self:setOtherScore(score)
end
------------------------------------

return TwoShowHandLayer
