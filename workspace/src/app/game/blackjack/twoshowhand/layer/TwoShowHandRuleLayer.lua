-- region TwoShowHandRuleLayer.lua
-- Date 2017.11.15
-- Auther JackXu.
-- Desc 游戏规则弹框 Layer.

local TwoShowHandRuleLayer = class("TwoShowHandRuleLayer", cc.exports.FixLayer)

function TwoShowHandRuleLayer:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()
    self:init()
end

function TwoShowHandRuleLayer:init()
    self:initCSB()
end

function TwoShowHandRuleLayer:onEnter()
    self.super:onEnter()

    self:setTargetShowHideStyle(self, self.SHOW_DLG_BIG, self.HIDE_DLG_BIG)
    self:showWithStyle()
end

function TwoShowHandRuleLayer:onExit()
    self.super:onExit()
end

function TwoShowHandRuleLayer:initCSB()

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    --csb
    self.m_pUiLayer = cc.CSLoader:createNode("game/twoshowhand/csb/gui-tsh-ruleLayer.csb")
    self.m_pUiLayer:addTo(self.m_rootUI, Z_ORDER_TOP)
    self.m_pLayerBase = self.m_pUiLayer:getChildByName("Layer_base")
    self.m_pScrollView = self.m_pLayerBase:getChildByName("ScrollView")
    self.m_pBtnClose = self.m_pLayerBase:getChildByName("Button_close")

    self.m_pBtnJieShao = self.m_pLayerBase:getChildByName("Button_1")
    self.m_pBtnPaiXing = self.m_pLayerBase:getChildByName("Button_2")

    self.m_pRuleJieShao = self.m_pLayerBase:getChildByName("ScrollView_1")
    self.m_pRulePaiXing = self.m_pLayerBase:getChildByName("ScrollView_2")

    self.m_pLayerTouch = self.m_pLayerBase:getChildByName("Panel_touch")

    --滚动条
    self.m_pRuleJieShao:setScrollBarEnabled(false)
    self.m_pRulePaiXing:setScrollBarEnabled(false)
    -- 关闭按纽
    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseClicked))
    --介绍
    self.m_pBtnJieShao:addClickEventListener(handler(self, self.onJieShaoClicked))
    --牌型
    self.m_pBtnPaiXing:addClickEventListener(handler(self, self.onPaiXingClicked))

    self.m_pLayerTouch:addClickEventListener(handler(self, self.onCloseClicked))

    self:onSwitchLayer(false)
end

function TwoShowHandRuleLayer:onCloseClicked()
    AudioManager.getInstance():playSound("public/sound/sound-close.mp3")
    self:onMoveExitView()
end

function TwoShowHandRuleLayer:onJieShaoClicked()
    AudioManager.getInstance():playSound("public/sound/sound-close.mp3")

    self:onSwitchLayer(true)
end

function TwoShowHandRuleLayer:onPaiXingClicked()
    AudioManager.getInstance():playSound("public/sound/sound-close.mp3")

    self:onSwitchLayer(false)
end

function TwoShowHandRuleLayer:onSwitchLayer(bTrue)
    
    if bTrue then
        self.m_pBtnJieShao:setEnabled(false)
        self.m_pBtnPaiXing:setEnabled(true)
        self.m_pBtnJieShao:setHighlighted(true)
        self.m_pBtnPaiXing:setHighlighted(false)
        self.m_pRuleJieShao:setVisible(true)
        self.m_pRulePaiXing:setVisible(false)
    else
        self.m_pBtnJieShao:setEnabled(true)
        self.m_pBtnPaiXing:setEnabled(false)
        self.m_pBtnJieShao:setHighlighted(false)
        self.m_pBtnPaiXing:setHighlighted(true)
        self.m_pRuleJieShao:setVisible(false)
        self.m_pRulePaiXing:setVisible(true)
    end
end

return TwoShowHandRuleLayer
