--region *.lua
--Date
--
--endregion

local TableChooseLayer = class("TableChooseLayer", cc.Layer)

local TableChooseItem = require("game.twoshowhand.layer.TableChooseItem")
local CommonUserInfo  = require("common.layer.CommonUserInfo")
local TwoShowHandRes  = require("game.twoshowhand.scene.TwoShowHandRes")
local RollMsg         = require("common.layer.RollMsg")

function TableChooseLayer:ctor()
    self:enableNodeEvents()
    self:init()
    self:initTableView()
end

function TableChooseLayer:init()
    local pLayerRoot = cc.CSLoader:createNode(TwoShowHandRes.game_choose_res)
    pLayerRoot:addTo(self)
    self.m_rootUI = pLayerRoot:getChildByName("Panel_root")
    self.m_rootUI:setPositionX((display.width - 1334) / 2)

    --table view
    self.m_table = self.m_rootUI:getChildByName("Panel_table")
    self.m_table:setContentSize(cc.size(display.width, 500))
    self.m_table:removeAllChildren()

    --table width
    if LuaUtils.isIphoneXDesignResolution() then
        self.m_width = display.width / 4
    else
        self.m_width = display.width / 3
    end

    self.top_root = self.m_rootUI:getChildByName("node_top")
    self.top_root:setLocalZOrder(10)
    self.btn_exit = self.top_root:getChildByName("Button_exit")
    self.btn_fast_start = self.top_root:getChildByName("Button_fast_start")
    self.game_icon = self.top_root:getChildByName("Image_game_level")
    self.m_pNodeDialog = self.m_rootUI:getChildByName("node_dialog")
    self.m_pNodeDialog:hide()
    self.m_pNodeDialog:setLocalZOrder(100)

    self.m_pBtnLeft   = self.top_root:getChildByName("Button_left")
    self.m_pBtnRight  = self.top_root:getChildByName("Button_right")

     --左箭头
    self.m_pBtnLeft:addClickEventListener(handler(self, self.onLeftClicked))
    --右箭头
    self.m_pBtnRight:addClickEventListener(handler(self, self.onRightClicked))

     --左箭头
    self.m_pBtnLeft:setPositionX((1334 - display.width) / 2 + 10)
    local seq = cc.Sequence:create(cc.MoveBy:create(0.4, cc.p(-10, 0)), cc.MoveBy:create(0.4, cc.p(10, 0)))
    self.m_pBtnLeft:runAction(cc.RepeatForever:create(seq))
    --右箭头
    self.m_pBtnRight:setPositionX(display.width - (display.width - 1334) / 2 - 10)
    local seq2 = cc.Sequence:create(cc.MoveBy:create(0.4, cc.p(10,0)), cc.MoveBy:create(0.4, cc.p(-10,0)))
    self.m_pBtnRight:runAction(cc.RepeatForever:create(seq2))

    --银商喇叭
    self.m_rollMsgObj = RollMsg.create()
    self.m_rollMsgObj:addTo(self.m_rootUI,99)
    self.m_rollMsgObj:startRoll()

    --xx场
    local kindID = PlayerInfo.getInstance():getKindID()
    local nBaseScore = PlayerInfo.getInstance():getBaseScore()
    local path_icon = string.format("game/twoshowhand/table/tablechoose_type-%d.png", nBaseScore)
    self.game_icon:loadTexture(path_icon, ccui.TextureResType.localType)

    ------------添加点击事件-------------------
    self.btn_exit:addClickEventListener(handler(self,self.onReturnClicked))
    self.btn_fast_start:addClickEventListener(handler(self,self.onQuickStartClicked))
    self.m_pNodeDialog:addClickEventListener(handler(self,self.onCloseDialogClicked))
    
    --位置
    self.btn_fast_start:setPositionX(1334 + (display.width - 1334) / 2)
    if LuaUtils.isIphoneXDesignResolution() then
        self.btn_exit:setPositionX(self.btn_exit:getPositionX()-120)
    end
end

function TableChooseLayer:onEnter()
    self.game_exit_listener = SLFacade:addCustomEventListener(Public_Events.MSG_GAME_EXIT, handler(self, self.onMsgExitGame))
    self.game_table_exit_listener = SLFacade:addCustomEventListener(Public_Events.MSG_GAME_TABLE_EXIT, handler(self, self.onMsgExitGame))
    self.no_network_listener = SLFacade:addCustomEventListener(Public_Events.MSG_NETWORK_FAILURE, handler(self, self.onMsgNetWorkFailre))                 
    self.go_sit_listener = SLFacade:addCustomEventListener(Public_Events.MSG_GO_SIT, handler(self, self.onMsgGoSit))
    self.sit_success_listener = SLFacade:addCustomEventListener(Public_Events.MSG_SIT_SUCCESS, handler(self, self.enterGame))

    --AudioManager.getInstance():stopMusic(true)
    --AudioManager.getInstance():playMusic("hall/sound/table_choose_music.mp3")
    
    if PlayerInfo.getInstance():getIsQuickStart() then
        PlayerInfo.getInstance():setIsTableQuickStart(true)
    end
    PlayerInfo.getInstance():setIsQuickStart(false)

    if PlayerInfo.getInstance():getIsTableQuickStart() then
        self:sitDown(G_CONSTANTS.INVALID_TABLE, G_CONSTANTS.INVALID_CHAIR)
    end
end

function TableChooseLayer:onExit()

    --取消注册
    SLFacade:removeEventListener(self.game_exit_listener)
    SLFacade:removeEventListener(self.game_table_exit_listener)
    SLFacade:removeEventListener(self.no_network_listener)
    SLFacade:removeEventListener(self.go_sit_listener)
    SLFacade:removeEventListener(self.sit_success_listener)

    --删除界面
    if self.userDialog then
        self.m_pNodeDialog:removeChild(self.userDialog)
        self.userDialog = nil
    end
    if self.m_pTableView then
        self.m_pTableView:removeAllChildren()
        self.m_pTableView = nil
    end
end

function TableChooseLayer:initTableView()
	if not self.m_pTableView then
        local tableSize = self.m_table:getContentSize()
        self.m_pTableView = cc.TableView:create(tableSize)
        self.m_pTableView:setIgnoreAnchorPointForPosition(false)
        self.m_pTableView:setAnchorPoint(cc.p(0,0))
        self.m_pTableView:setPosition(cc.p(0,0))
        self.m_pTableView:setDirection(cc.SCROLLVIEW_DIRECTION_HORIZONTAL)
        self.m_pTableView:setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN)
        self.m_pTableView:setTouchEnabled(true)
        self.m_pTableView:setDelegate()
        self.m_pTableView:addTo(self.m_table)

        self.m_pTableView:registerScriptHandler(handler(self,self.scrollViewDidScroll), cc.SCROLLVIEW_SCRIPT_SCROLL)
        self.m_pTableView:registerScriptHandler(handler(self,self.cellSizeForTable), cc.TABLECELL_SIZE_FOR_INDEX)
        self.m_pTableView:registerScriptHandler(handler(self,self.tableCellAtIndex), cc.TABLECELL_SIZE_AT_INDEX)
        self.m_pTableView:registerScriptHandler(handler(self,self.numberOfCellsInTableView), cc.NUMBER_OF_CELLS_IN_TABLEVIEW)
        self.m_pTableView:registerScriptHandler(handler(self,self.tableCellTouched), cc.TABLECELL_TOUCHED)
    end
    self.m_pTableView:reloadData()
    self:scrollTableView()
end

function TableChooseLayer:scrollViewDidScroll(sender,cell)
    local pos = sender:getContentOffset()
    local viewSize = self.m_pTableView:getContentSize()
    local layerSize = cc.size(display.width, 500)

    if pos.x < - 50 then
        self.m_pBtnLeft:setVisible(true)
    else
        self.m_pBtnLeft:setVisible(false)
    end

    if pos.x > layerSize.width - viewSize.width +50 then
        self.m_pBtnRight:setVisible(true)
    else
        self.m_pBtnRight:setVisible(false)
    end
end

function TableChooseLayer:tableCellTouched(sender,cell)
end

function TableChooseLayer:cellSizeForTable(table, idx)
    return self.m_width, 500
end

function TableChooseLayer:tableCellAtIndex(table, idx)
    local cell = table:dequeueCell() or cc.TableViewCell:new()
    self:initTableViewCell(cell, idx)
    return cell
end

function TableChooseLayer:numberOfCellsInTableView(table)
    local tableInfo = CUserManager.getInstance():getTableInfo()
    local count = math.ceil(tableInfo.wTableCount / 2)
    return count
end

function TableChooseLayer:initTableViewCell(cell, nIdx)

    cell:removeAllChildren()
    for i = 1, 2 do
        local tableID = nIdx * 2 + (i - 1)
        local tableItem = TableChooseItem.new()
        tableItem:init(tableID, i)
        tableItem:setHandler(self)
        tableItem:showUserDialog()
        tableItem:addTo(cell, G_CONSTANTS.Z_ORDER_STATIC)

        if i == 1 then
            tableItem:setPosition(self.m_width / 2, 350)
        elseif i == 2 then
            tableItem:setPosition(self.m_width / 2, 120)
        end
    end
end

function TableChooseLayer:onMsgGoSit(_event)

    local _userdata = unpack(_event._userdata)
    if not _userdata then
        return
    end
    local eventID = _userdata.name
    local msg = _userdata.packet

    if not msg then return end

    local strData = msg
    local vecStrings = {}
    vecStrings = string.split(strData, ",")
    if #vecStrings <= 0 then
        return
    end
    local tableId = tonumber(vecStrings[1])
    local chairId = tonumber(vecStrings[2])
    self:sitDown(tableId, chairId)
end

function TableChooseLayer:sitDown( tableID, chairID)
    CMsgGame:sendSitDown(tableID, chairID)
end

function TableChooseLayer:onMsgExitGame(_event)
    self:onMoveExitView()
end

function TableChooseLayer:onMsgNetWorkFailre(msg)
    if msg == "1" then
        FloatMessage.getInstance():pushMessage("STRING_023_1")
    else
        FloatMessage.getInstance():pushMessage("STRING_023")
    end  
    self:onMoveExitView()
end

function TableChooseLayer:enterGame()

    --记录offset
    local offset = self.m_pTableView:getContentOffset()
    PlayerInfo.getInstance():setChooseTableOffset(offset.y)

    --进入游戏
    SLFacade:dispatchCustomEvent(Public_Events.MSG_GAME_TABLE_SUCCESS)
end

function TableChooseLayer:onMoveExitView()
    
    if (PlayerInfo.getInstance():getServerType() == 2) then
        --体验房刷新分数
        local tempScore = PlayerInfo.getInstance():getTempUserScore();
        PlayerInfo.getInstance():setUserScore(tempScore)
    end

    --选桌的游戏，返回大厅时，才释放资源

    --释放资源
    local kindID = PlayerInfo.getInstance():getKindID()
    if kindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_TWONIUNIU then
        local TwoniuniuRes = require("game.twoniuniu.scene.TwoniuniuRes")
        --释放动画
        for i, name in pairs(TwoniuniuRes.vecReleaseAnim) do
            local strName = name .. ".ExportJson"
            ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo(strName)
        end
        --释放整图
        for i, name in pairs(TwoniuniuRes.vecReleasePlist) do
            display.removeSpriteFrames(name[1], name[2])
        end
        -- 释放背景图
        for _, strFileName in pairs(TwoniuniuRes.vecReleaseImg) do
            display.removeImage(strFileName)
        end
        --释放音频
        for i, name in pairs(TwoniuniuRes.vecReleaseSound) do
            AudioManager.getInstance():unloadEffect(name)
        end
    elseif kindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_TWOSHOWHAND then
        
        local TwoShowHandRes = require("game.twoshowhand.scene.TwoShowHandRes")
        --释放动画
        for i, name in pairs(TwoShowHandRes.vecReleaseAnim) do
            ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo(name)
        end
        --释放整图
        for i, name in pairs(TwoShowHandRes.vecReleasePlist) do
            display.removeSpriteFrames(name[1], name[2])
        end
        -- 释放背景图
        for _, strFileName in pairs(TwoShowHandRes.vecReleaseImg) do
            display.removeImage(strFileName)
        end
        --释放音频
        for i, name in pairs(TwoShowHandRes.vecReleaseSound) do
            AudioManager.getInstance():unloadEffect(name)
        end
    end

    --清理数据
    Effect.getInstance():Clear()
    CUserManager.getInstance():clear()

    --返回大厅
    PlayerInfo.getInstance():setIsGameBackToHall(true)
    PlayerInfo:getInstance():setIsGameBackToHallSuc(false)
	SLFacade:dispatchCustomEvent(Public_Events.Load_Entry)
end

------点击事件-----
function TableChooseLayer:onReturnClicked()
    AudioManager.getInstance():playSound("public/sound/sound-close.mp3")

    if PlayerInfo.getInstance():IsInExperienceRoom() then --体验房退出提示
        SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_MESSAGEBOX, "experience-room-exit")
        return
    end
    --清空offset
    PlayerInfo.getInstance():setChooseTableOffset(-1)

    self:onMoveExitView()
end

function TableChooseLayer:onQuickStartClicked()
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
    PlayerInfo.getInstance():setIsTableQuickStart(true)
    self:sitDown(G_CONSTANTS.INVALID_TABLE, G_CONSTANTS.INVALID_CHAIR)
end

function TableChooseLayer:onCloseDialogClicked()
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
    self:clearDialog()
end

function TableChooseLayer:clearDialog()
    CUserManager:getInstance():deleteUserDialogByTag(self.userDialog:getTag())
    self.m_pNodeDialog:setVisible(false)
    self.m_pNodeDialog:removeChild(self.userDialog)
    self.userDialog = nil
end
-------------------

function TableChooseLayer:showUserInfoDialog(pos , tag, info)
    if not pos or not tag or not info then return end
    print("TableChooseLayer:showUserInfoDialog")
    self.userDialog = CommonUserInfo.create(1)
    self.userDialog:setPosition(pos)
    self.userDialog:setTag(tag or 0)
    self.userDialog:updateUserInfoByInfo(info)
    self.userDialog:setHandler(self)
    self.m_pNodeDialog:setVisible(true)
    self.m_pNodeDialog:addChild(self.userDialog)
end

function TableChooseLayer:scrollTableView()
    local posY = PlayerInfo.getInstance():getChooseTableOffset()
    if posY == -1 then return end
    self.m_pTableView:setContentOffset(cc.p(0, posY))
end

function TableChooseLayer:onLeftClicked()
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")

    local offset = self.m_pTableView:getContentOffset()
    local sizeMove = display.width

    if offset.x + sizeMove < 0 then
        offset.x = offset.x + sizeMove
    else
        offset.x = 0
    end

    self.m_pTableView:setContentOffset(offset, true)
end

function TableChooseLayer:onRightClicked()
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")

    local offset = self.m_pTableView:getContentOffset()
    local size = self.m_pTableView:getContentSize()
    local sizeMove = display.width
    local sizeView = display.width
    if 0 - (offset.x - sizeMove - sizeView) < size.width then
        offset.x = offset.x - sizeMove
    else
        offset.x = sizeView - size.width
    end
    self.m_pTableView:setContentOffset(offset, true)
end

return TableChooseLayer
