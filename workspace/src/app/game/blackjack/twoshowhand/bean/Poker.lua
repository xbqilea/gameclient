--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
local TwoShowHandRes   = require("game.twoshowhand.scene.TwoShowHandRes")

local Poker = class("Poker", cc.Node)

function Poker:create()
    local pPoker = Poker.new()
    pPoker:init()
    return pPoker
end

function Poker:ctor()
    self:enableNodeEvents()
    self.m_pathUI = cc.CSLoader:createNode(TwoShowHandRes.game_poker_res):addTo(self)
end

function Poker:init()

    self.m_pSpBack = self.m_pathUI:getChildByName("Image_poker_back")
    self.m_pSpValue, self.m_pSpColor = {}, {}
    for i = 1, 2 do
        self.m_pSpValue[i] = self.m_pathUI:getChildByName("Image_poker"):getChildByName(string.format("Image_value_%d",i-1))
        self.m_pSpColor[i] = self.m_pathUI:getChildByName("Image_poker"):getChildByName(string.format("Image_color_%d",i-1))
    end

    -- self.m_pSpGary = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_gary")
    -- self.m_pSpGary:setVisible(false)
end

function Poker:resetPosition()
    
    local size = self.m_pSpBack:getContentSize()
    self.m_pathUI:setPosition(0 - size.width / 2, 0 - size.height / 2)
end

function Poker:onEnter()

end

function Poker:onExit()

end

function Poker:setCardData(color, value)
    if not value then
        local cbvalue = color
        local cbColor = color
        value = self:GetCardValue(cbvalue)
        color = bit.rshift(self:GetCardColor(cbColor), 4)
    end

    if color < 0 or color >= 4 or value < 1 or value > 15 then
        return
    end

    -- self.m_pSpBack:loadTexture("poker-bg.png", ccui.TextureResType.plistType)
    self.m_pSpBack:hide()

    -- if color == 4 then
    --     if value == 14 then
    --         value = 53
    --     else
    --         value = 54
    --     end
    --     local tempValue = value

    --     self.m_pSpValue[1]:loadTexture(string.format("value-%d.png", tempValue) , ccui.TextureResType.plistType)
    --     self.m_pSpValue[1]:setVisible(true)
    --     self.m_pSpValue[2]:setVisible(false)
    --     self.m_pSpColor[1]:setVisible(false)
    --     self.m_pSpColor[2]:setVisible(false)
    --     return
    -- end

    print("value", value, "color", color)
    for i = 1, 2 do
        self.m_pSpColor[i]:setVisible(true)
        self.m_pSpColor[i]:loadTexture(string.format("color-small-%d.png", color), ccui.TextureResType.plistType)
        self.m_pSpValue[i]:setVisible(true)
        self.m_pSpValue[i]:loadTexture(string.format("value-%d-%d.png", value, color%2), ccui.TextureResType.plistType)
    end

    -- local pSize = self.m_pSpBack:getContentSize()
    -- self.m_pSpValue[1]:setPositionY(pSize.height - 6)
    -- local x1,y1 = self.m_pSpValue[1]:getPosition()
    -- local size1 = self.m_pSpValue[1]:getContentSize()
    -- self.m_pSpColor[1]:setPosition(x1, y1 - size1.height - 1)

    -- self.m_pSpValue[2]:setPositionX(pSize.width - 6)
    -- local x2,y2 = self.m_pSpValue[2]:getPosition()
    -- local size2 = self.m_pSpValue[2]:getContentSize()
    -- self.m_pSpColor[2]:setPosition(x2, y2 + size2.height + 1)
end

-- function Poker:setGary(isGary)
--     self.m_pSpGary:setVisible(isGary)
-- end

--设置为地主
-- function Poker:setBanker(isBanker)
--     self.m_pSpBanker:setVisible(isBanker)
-- end

--设置为牌背面
function Poker:setBack()
    --poker-side1  poker-back
    -- self.m_pSpBack:loadTexture("poker-back.png", ccui.TextureResType.plistType)
    -- for i = 1, 2 do
    --     self.m_pSpValue[i]:setVisible(false)
    --     self.m_pSpColor[i]:setVisible(false)
    -- end
    self.m_pSpBack:show()
end

--获取数值
function Poker:GetCardValue(cbCardData)
    return bit.band(cbCardData, 0x0F)
end

--获取花色
function Poker:GetCardColor(cbCardData)
    return bit.band(cbCardData, 0xF0)
end

return Poker
--endregion
