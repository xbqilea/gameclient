--region TwoShowHandDataMgr.lua
--Date 2017.06.07
--Auther JackyXu.
--Desc 二人梭哈数据管理.

local TwoShowHandConst = require("game.twoshowhand.scene.TwoShowHandConst")

local TwoShowHandDataMgr = class("TwoShowHandDataMgr")

TwoShowHandDataMgr.g_instance = nil

function TwoShowHandDataMgr.getInstance()
    if not TwoShowHandDataMgr.g_instance then
        TwoShowHandDataMgr.g_instance = TwoShowHandDataMgr:new()
    end
    return TwoShowHandDataMgr.g_instance
end

function TwoShowHandDataMgr.releaseInstance()
    TwoShowHandDataMgr.g_instance = nil
end

function TwoShowHandDataMgr:ctor()
    self:Clean()
end

function TwoShowHandDataMgr:Clean()
    self.m_cbGameStatus = TwoShowHandConst.GAME_STATUS_FREE
    self.m_cbGameStartTime = 30
    self.m_cbDownJettonTime = 30
    self.m_cbOpenCardTime = 30
    self.m_llHistoryScore = 0
    self.m_llLastScore = 0
    self.m_wCurrentUser = 0
    self.m_llCellScore  = 0
    self.m_wGiveUpUser = 0
    self.m_wShowHandUser  = 0
    self.m_wAddScoreUser = 0
    self.m_cbAddType = 0
    self.m_iTurnMaxTimes = 0
    self.m_iAddDownTimes = 0
    self.m_cbSendCardCount = 0
    self.m_cbGameEndType = 0
    self.m_bWaitOpenCard = false
    self.m_iLeftTime = 0
    self.m_bIsEnterGameView = false
    self.m_bIsLoadGameScene = false
    
    self.m_cbCardData   = {         -- 玩家扑克  1自己 2对手
        [1] = {0,0,0,0,0},
        [2] = {0,0,0,0,0},
    }
    self.m_llResult     = {0,0}     -- 结算分数  1自己 2对手
    self.m_bShowHand    = {false, false}    -- 是否梭哈  1自己 2对手
    self.m_llChipNum    = {0,0}     -- 投注额   1自己 2对手
    self.m_iTurnCurrentDown    = {0,0}      -- 当前玩家下注倍数 1自己 2对手
    self.m_bOpenCard    = {false, false}    -- 是否已开牌 1自己 2对手
    self.m_bSelfAdd = false --本轮自己是否加注过
end

function TwoShowHandDataMgr:resetData()
    self.m_wCurrentUser = 0
    self.m_llCellScore  = 0
    self.m_wGiveUpUser = 0
    self.m_wShowHandUser  = 0
    self.m_wAddScoreUser = 0
    self.m_cbAddType = 0
    self.m_iTurnMaxTimes = 0
    self.m_iAddDownTimes = 0
    self.m_cbSendCardCount = 0
    self.m_cbGameEndType = 0
    self.m_bWaitOpenCard = false
    self.m_iLeftTime = 0
    
    self.m_cbCardData   = {         -- 玩家扑克  1自己 2对手
        [1] = {0,0,0,0,0},
        [2] = {0,0,0,0,0},
    }
    self.m_llResult     = {0,0}     -- 结算分数  1自己 2对手
    self.m_bShowHand    = {false, false}    -- 是否梭哈  1自己 2对手
    self.m_llChipNum    = {0,0}     -- 投注额   1自己 2对手
    self.m_iTurnCurrentDown    = {0,0}      -- 当前玩家下注倍数 1自己 2对手
    self.m_bOpenCard    = {false, false}    -- 是否已开牌 1自己 2对手
    self.m_bSelfAdd = false --本轮自己是否加注过
end

function TwoShowHandDataMgr:CurrentUserIsMe()
    return self.m_wCurrentUser == PlayerInfo.getInstance():getChairID()
end

function TwoShowHandDataMgr:getViewIndex(chairID)
    if chairID == PlayerInfo.getInstance():getChairID() then
        return 1
    end
    return 2
end

function TwoShowHandDataMgr:GetLogicValue(cbCardData)
    if cbCardData == 52 then
        return 14
    end
    if cbCardData == 53 then
        return 15
    end
    local cbValue = cbCardData % 13
    return cbValue == 0 and 13 or cbValue
end

function TwoShowHandDataMgr:GetCardType(cbCardData)
    local cbCardIndex = {}
    for i=1,16 do
        cbCardIndex[i] = 0
    end
    local bSameColour = true
    for i=1,5 do
        local nIndex = self:GetLogicValue(cbCardData[i])
        cbCardIndex[nIndex] = cbCardIndex[nIndex] + 1
        local color1 = math.floor(cbCardData[1] / 13)
        local color2 = math.floor(cbCardData[i] / 13)
        if (i > 1 and color1 ~= color2) then
            bSameColour = false
        end
    end
    
    local cbMinCount = 0
    local cbCardCount = {0,0,0,0,0}
    local cbMaxCardValue = 0
    local bLinkCard = false
    for i=1,16 do
        for j=1,5 do
            if cbCardIndex[i] == j-1 then
                cbCardCount[j] = cbCardCount[j] + 1
                if cbMinCount <= j then
                    cbMinCount = j
                    cbMaxCardValue = i
                end
            end
        end
        
        local bLink = true
        if i <= 10 and cbCardIndex[i] == 1 then
            for j=1,4 do
                if (cbCardIndex[i + j] ~= 1) then
                    bLink = false
                    break
                end
            end
            if bLink then
                bLinkCard = true
            end
        end
    end
    
    if (bSameColour and bLinkCard and cbMaxCardValue == 13) then
        return TwoShowHandConst.CT_KING_THS
    end
    if (bSameColour and bLinkCard) then
        return TwoShowHandConst.CT_TONG_HUA_SHUN
    end
    if (cbCardCount[5] == 1) then
        return TwoShowHandConst.CT_TIE_ZHI
    end
    if (cbCardCount[3] == 1 and cbCardCount[4] == 1) then
        return TwoShowHandConst.CT_HU_LU
    end
    if (bSameColour) then
        return TwoShowHandConst.CT_TONG_HUA
    end
    if (bLinkCard) then
        return TwoShowHandConst.CT_SHUN_ZI
    end
    if (cbCardCount[4] == 1) then
        return TwoShowHandConst.CT_THREE_TIAO
    end
    if (cbCardCount[3] == 2) then
        return TwoShowHandConst.CT_TWO_DOUBLE
    end
    if (cbCardCount[3] == 1) then
        return TwoShowHandConst.CT_ONE_DOUBLE
    end
    
    return TwoShowHandConst.CT_SINGLE
end

function TwoShowHandDataMgr:GetCardSpecialType(cbCardData)
    local cbTempCardData = table.copy(cbCardData)

    for i=1,5 do
        if cbTempCardData[i] % 13 == 0 then
            cbTempCardData[i] = cbTempCardData[i] + 6
            break
        end
    end
    
    local cbType1 = self:GetCardType(cbTempCardData)
    local cbType2 = self:GetCardType(cbCardData)
    
    if cbType1 > cbType2 then
        return cbType1
    end
    
    return cbType2
end

--游戏状态
function TwoShowHandDataMgr:setGameStatus(nGameStatus)
    self.m_cbGameStatus = nGameStatus
end
function TwoShowHandDataMgr:getGameStatus()
    return self.m_cbGameStatus
end

--历史成绩
function TwoShowHandDataMgr:setHistoryScore(nHistoryScore)
    self.m_llHistoryScore = nHistoryScore
end
function TwoShowHandDataMgr:getHistoryScore()
    return self.m_llHistoryScore
end

--上一局成绩
function TwoShowHandDataMgr:setLastScore(nLastScore)
    self.m_llLastScore = nLastScore
end
function TwoShowHandDataMgr:getLastScore()
    return self.m_llLastScore
end

--准备时间
function TwoShowHandDataMgr:setGameStartTime(nGameStartTime)
    self.m_cbGameStartTime = nGameStartTime
end
function TwoShowHandDataMgr:getGameStartTime()
    return self.m_cbGameStartTime
end

--下注时间
function TwoShowHandDataMgr:setDownJettonTime(nDownJettonTime)
    self.m_cbDownJettonTime = nDownJettonTime
end
function TwoShowHandDataMgr:getDownJettonTime()
    return self.m_cbDownJettonTime
end

--开牌时间
function TwoShowHandDataMgr:setOpenCardTime(nOpenCardTime)
    self.m_cbOpenCardTime = nOpenCardTime
end
function TwoShowHandDataMgr:getOpenCardTime()
    return self.m_cbOpenCardTime
end

--当前玩家
function TwoShowHandDataMgr:setCurrentUser(nCurrentUser)
    self.m_wCurrentUser = nCurrentUser
end
function TwoShowHandDataMgr:getCurrentUser()
    return self.m_wCurrentUser
end

--单元下注
function TwoShowHandDataMgr:setCellScore(nCellScore)
    self.m_llCellScore = nCellScore
end
function TwoShowHandDataMgr:getCellScore()
    return self.m_llCellScore
end

--弃牌玩家
function TwoShowHandDataMgr:setGiveUpUser(nGiveUpUser)
    self.m_wGiveUpUser = nGiveUpUser
end
function TwoShowHandDataMgr:getGiveUpUser()
    return self.m_wGiveUpUser
end

--梭哈玩家
function TwoShowHandDataMgr:setShowHandUser(nShowHandUser)
    self.m_wShowHandUser = nShowHandUser
end
function TwoShowHandDataMgr:getShowHandUser()
    return self.m_wShowHandUser
end

--加注玩家
function TwoShowHandDataMgr:setAddScoreUser(nAddScoreUser)
    self.m_wAddScoreUser = nAddScoreUser
end
function TwoShowHandDataMgr:getAddScoreUser()
    return self.m_wAddScoreUser
end

--下注类型： 0不加注 1跟注 2加注
function TwoShowHandDataMgr:setAddType(nAddType)
    self.m_cbAddType = nAddType
end
function TwoShowHandDataMgr:getAddType()
    return self.m_cbAddType
end

--本轮当前最大加注倍数
function TwoShowHandDataMgr:setTurnMaxTimes(nTurnMaxTimes)
    self.m_iTurnMaxTimes = nTurnMaxTimes
end
function TwoShowHandDataMgr:getTurnMaxTimes()
    return self.m_iTurnMaxTimes
end

--当前下注倍数
function TwoShowHandDataMgr:setAddDownTimes(nAddDownTimes)
    self.m_iAddDownTimes = nAddDownTimes
end
function TwoShowHandDataMgr:getAddDownTimes()
    return self.m_iAddDownTimes
end

--发牌数
function TwoShowHandDataMgr:setSendCardCount(nSendCardCount)
    self.m_cbSendCardCount = nSendCardCount
end
function TwoShowHandDataMgr:getSendCardCount()
    return self.m_cbSendCardCount
end

--结束类型
function TwoShowHandDataMgr:setGameEndType(nGameEndType)
    self.m_cbGameEndType = nGameEndType
end
function TwoShowHandDataMgr:getGameEndType()
    return self.m_cbGameEndType
end

--是否进入开牌状态
function TwoShowHandDataMgr:setWaitOpenCard(nWaitOpenCard)
    self.m_bWaitOpenCard = nWaitOpenCard
end
function TwoShowHandDataMgr:getWaitOpenCard()
    return self.m_bWaitOpenCard
end

--倒计时剩余时间
function TwoShowHandDataMgr:setLeftTime(nLeftTime)
    self.m_iLeftTime = nLeftTime
end
function TwoShowHandDataMgr:getLeftTime()
    return self.m_iLeftTime
end

--是否在游戏中
function TwoShowHandDataMgr:setIsEnterGameView(bIsEnterGameView)
    self.m_bIsEnterGameView = bIsEnterGameView and bIsEnterGameView == true or false
end
function TwoShowHandDataMgr:getIsEnterGameView()
    return self.m_bIsEnterGameView
end

--是否有收到场景消息
function TwoShowHandDataMgr:setIsLoadGameScene(bIsLoadGameScene)
    self.m_bIsLoadGameScene = bIsLoadGameScene
 end
function TwoShowHandDataMgr:getIsLoadGameScene()
    return self.m_bIsLoadGameScene
end

function TwoShowHandDataMgr:setSelfAdd(bool)
    self.m_bSelfAdd = bool
end

function TwoShowHandDataMgr:getSelfAdd()
    return self.m_bSelfAdd
end

return TwoShowHandDataMgr
