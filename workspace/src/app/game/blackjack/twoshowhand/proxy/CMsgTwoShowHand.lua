--region CMsgTwoShowHand.lua
--Date 2017.06.08
--Auther JackyXu.
--Desc 二人梭哈消息解析

local TwoShowHand_Events = require("game.twoshowhand.scene.TwoShowHandEvent")
local TwoShowHandConst = require("game.twoshowhand.scene.TwoShowHandConst")

local CMsgGame = require("common.app.CMsgGame")

local TwoShowHandDataMgr = require("game.twoshowhand.manager.TwoShowHandDataMgr")
local CMsgTwoShowHand = class("CMsgTwoShowHand", CMsgGame)

CMsgTwoShowHand.gclient = nil 

function CMsgTwoShowHand.getInstance()
    if not CMsgTwoShowHand.gclient then
        CMsgTwoShowHand.gclient = CMsgTwoShowHand:new()
    end
    return CMsgTwoShowHand.gclient
end

function CMsgTwoShowHand:ctor()

    self.func_game_ =
    {       
        [G_C_CMD.MDM_GF_FRAME] = {  --100--框架命令 
            [G_C_CMD.SUB_GF_GAME_STATUS]            = { func = self.process_100_100, log = "游戏状态", },
            [G_C_CMD.SUB_GF_GAME_SCENE]             = { func = self.process_100_101, log = "游戏场景", },
        },
            
        [G_C_CMD.MDM_GF_GAME] = { --200 --服务器命令结构
            [G_C_CMD.SUB_S_GAME_START_TWOSHOWHAND]    = {func = self.process_200_100, log="游戏开始"},
            [G_C_CMD.SUB_S_ADD_SCORE]                 = {func = self.process_200_101, log="加注结果"},
            [G_C_CMD.SUB_S_GIVE_UP]                   = {func = self.process_200_102, log="放弃跟注"},
            [G_C_CMD.SUB_S_SEND_CARD]                 = {func = self.process_200_103, log="发牌消息"},
            [G_C_CMD.SUB_S_SHOW_HAND]                 = {func = self.process_200_104, log="玩家梭哈"},
            [G_C_CMD.SUB_S_GAME_END_TWOSHOWHAND]      = {func = self.process_200_105, log="游戏结束"},
            [G_C_CMD.SUB_S_WAIT_OPEN_CARD]            = {func = self.process_200_106, log="等待开牌"},
            [G_C_CMD.SUB_S_OPEN_CARD_NOTIFY]          = {func = self.process_200_107, log="开牌通知"},
            [G_C_CMD.SUB_S_LOOK_CARD_NOTIFY]          = {func = self.process_200_108, log="看牌通知"},
            [G_C_CMD.SUB_S_LOOK_ENEMY_CARD]           = {func = self.process_200_110, log="看对方牌"},
            [G_C_CMD.SUB_S_SysMessage_TWOSHOWHAND]    = {func = self.process_200_220, log="系统消息"},
        },
    }
end

-------------------------以下为过滤出来需要在子类处理的消息
--游戏状态
function CMsgTwoShowHand:process_100_100(__data)
    
	local _buffer = __data:readData(__data:getReadableSize())

    local msg = {}
    msg.cbGameStatus     = _buffer:readChar()   --//游戏状态
    msg.cbAllowLookon     = _buffer:readChar()  --//旁观标志

    TwoShowHandDataMgr.getInstance():setGameStatus(msg.cbGameStatus)
end

--游戏场景
function CMsgTwoShowHand:process_100_101(__data)
 
    local len = __data:getReadableSize()
    local _buffer = __data:readData(len)

-- --场景消息
-- struct CMD_S_GameScene_TwoShowHand
-- {
--     uint8                           cbGameStartTimer                       --准备时间
--     uint8                           cbDownJettonTimer                      --下注时间
--     uint8                           cbOpenCardTimer                        --开牌时间
    
--     uint16                          wCurrentUser                           --当前玩家
--     uint8                           cbSendCount                            --发牌数
--     uint8                           cbCardData[2][5]                       --扑克数据
--     uint8                           cbObscureCard                          --底牌
--     bool                            bShowHand[2]                           --玩家梭哈
--     bool                            bOpenCard[2]                           --玩家开牌
--     bool                            bWaitOpenCard
    
--     long long                       llCellScore                            --单元积分
--     local                             iUserDownScore[2]                      --玩家下注
--     local                             iTurnCurrentDown[2]                    --本轮下注
--     local                             iTurnMaxDown                           --本轮最大
    
--     long long                       llLastResult[2]                        --上局成绩
--     long long                       llTotalResult[2]                       --总成绩
--     int                             iLeftTimer                             --剩余时间 
--      bool                            bAddJetton                              --玩家自己是否加倍
-- }
    
    local pGameScene = {}
    pGameScene.cbGameStartTimer = _buffer:readUChar()
    pGameScene.cbDownJettonTimer = _buffer:readUChar()
    pGameScene.cbOpenCardTimer = _buffer:readUChar()
    pGameScene.wCurrentUser = _buffer:readUShort()
    pGameScene.cbSendCount = _buffer:readUChar()
    pGameScene.cbCardData = {}
    for i=1,2 do
        pGameScene.cbCardData[i] = {}
        for j=1,5 do
            pGameScene.cbCardData[i][j] = _buffer:readUChar()
        end
    end
    pGameScene.cbObscureCard = _buffer:readUChar()
    pGameScene.bShowHand = {}
    pGameScene.bShowHand[1] = _buffer:readBoolean()
    pGameScene.bShowHand[2] = _buffer:readBoolean()
    pGameScene.bOpenCard = {}
    pGameScene.bOpenCard[1] = _buffer:readBoolean()
    pGameScene.bOpenCard[2] = _buffer:readBoolean()
    pGameScene.bWaitOpenCard = _buffer:readBoolean()
    pGameScene.llCellScore = _buffer:readLongLong()
    pGameScene.iUserDownScore = {}
    pGameScene.iUserDownScore[1] = _buffer:readInt()
    pGameScene.iUserDownScore[2] = _buffer:readInt()
    pGameScene.iTurnCurrentDown = {}
    pGameScene.iTurnCurrentDown[1] = _buffer:readInt()
    pGameScene.iTurnCurrentDown[2] = _buffer:readInt()
    pGameScene.iTurnMaxDown = _buffer:readInt()
    pGameScene.llLastResult = {}
    pGameScene.llLastResult[1] = _buffer:readLongLong()
    pGameScene.llLastResult[2] = _buffer:readLongLong()
    pGameScene.llTotalResult = {}
    pGameScene.llTotalResult[1] = _buffer:readLongLong()
    pGameScene.llTotalResult[2] = _buffer:readLongLong()
    pGameScene.iLeftTimer = _buffer:readInt()
    pGameScene.bSelfAdd = _buffer:readBoolean()

    print("=========梭哈场景消息======")
    TwoShowHandDataMgr.getInstance():setGameStartTime(pGameScene.cbGameStartTimer)
    TwoShowHandDataMgr.getInstance():setDownJettonTime(pGameScene.cbDownJettonTimer)
    TwoShowHandDataMgr.getInstance():setOpenCardTime(pGameScene.cbOpenCardTimer)
    
    TwoShowHandDataMgr.getInstance():setCurrentUser(pGameScene.wCurrentUser)
    TwoShowHandDataMgr.getInstance():setSendCardCount(pGameScene.cbSendCount)
    local myChair = PlayerInfo.getInstance():getChairID()
    local myIndex = (myChair == 0 and 1 or 2)
    local otherIndex = (myChair == 0 and 2 or 1)
    
    TwoShowHandDataMgr.getInstance().m_bShowHand[1] = pGameScene.bShowHand[myIndex]
    TwoShowHandDataMgr.getInstance().m_llChipNum[1] = pGameScene.iUserDownScore[myIndex]*pGameScene.llCellScore
    TwoShowHandDataMgr.getInstance().m_cbCardData[1] = table.copy(pGameScene.cbCardData[myIndex])
    TwoShowHandDataMgr.getInstance().m_bShowHand[2] = pGameScene.bShowHand[otherIndex]
    TwoShowHandDataMgr.getInstance().m_llChipNum[2] = pGameScene.iUserDownScore[otherIndex]*pGameScene.llCellScore
    TwoShowHandDataMgr.getInstance().m_cbCardData[2] = table.copy(pGameScene.cbCardData[otherIndex])
    TwoShowHandDataMgr.getInstance().m_cbCardData[1][1] = pGameScene.cbObscureCard
    print("自己底牌：",pGameScene.cbObscureCard)
    
    TwoShowHandDataMgr.getInstance():setCellScore(pGameScene.llCellScore)
    TwoShowHandDataMgr.getInstance().m_iTurnCurrentDown[1] = pGameScene.iTurnCurrentDown[myIndex]
    TwoShowHandDataMgr.getInstance().m_iTurnCurrentDown[2] = pGameScene.iTurnCurrentDown[otherIndex]
    TwoShowHandDataMgr.getInstance():setTurnMaxTimes(pGameScene.iTurnMaxDown)
    TwoShowHandDataMgr.getInstance():setHistoryScore(pGameScene.llTotalResult[myIndex])
    TwoShowHandDataMgr.getInstance():setLastScore(pGameScene.llLastResult[myIndex])
    
    printf("自己倍数：%d 对手倍数：%d",pGameScene.iTurnCurrentDown[myIndex], pGameScene.iTurnCurrentDown[otherIndex])
    printf("自己下注：%d 对手下注：%d",pGameScene.iUserDownScore[myIndex], pGameScene.iUserDownScore[otherIndex])
    print("本轮最大下注：",pGameScene.iTurnMaxDown)
    
    TwoShowHandDataMgr.getInstance():setWaitOpenCard(pGameScene.bWaitOpenCard)
    TwoShowHandDataMgr.getInstance().m_bOpenCard[1] = pGameScene.bOpenCard[myIndex]
    TwoShowHandDataMgr.getInstance().m_bOpenCard[2] = pGameScene.bOpenCard[otherIndex]
    TwoShowHandDataMgr.getInstance():setLeftTime(pGameScene.iLeftTimer)
    TwoShowHandDataMgr.getInstance():setIsLoadGameScene(true)
    TwoShowHandDataMgr.getInstance():setSelfAdd(pGameScene.bSelfAdd)

    SLFacade:dispatchCustomEvent(TwoShowHand_Events.MSG_TWOSHOWHAND_INIT)
end


-------------------------以下为 200 游戏消息
--游戏开始
function CMsgTwoShowHand:process_200_100(__data)
    local len = __data:getReadableSize()
    local _buffer = __data:readData(len)

    if not TwoShowHandDataMgr.getInstance():getIsEnterGameView() then
        print("CMsgTwoShowHand:process_200_100 return")
        return
    end

-- --游戏开始
-- struct CMD_S_GameStart_TwoShowHand
-- {
--     long long                           llCellScore                        --单元下注
--     uint16                              wCurrentUser                       --当前玩家
--     uint8                               cbObscureCard                      --底牌扑克
--     uint8                               cbCardData[2]                      --用户扑克
-- }

    local pGameData = {}
    pGameData.llCellScore = _buffer:readLongLong()
    pGameData.wCurrentUser = _buffer:readUShort()
    pGameData.cbObscureCard = _buffer:readUChar()
    pGameData.cbCardData = {}
    pGameData.cbCardData[1] = _buffer:readUChar()
    pGameData.cbCardData[2] = _buffer:readUChar()

    TwoShowHandDataMgr.getInstance():resetData()
    TwoShowHandDataMgr.getInstance():setCurrentUser(pGameData.wCurrentUser)
    TwoShowHandDataMgr.getInstance():setCellScore(pGameData.llCellScore)
    local myChair = PlayerInfo.getInstance():getChairID()
    local myIndex = (myChair == 0 and 1 or 2)
    local otherIndex = (myChair == 0 and 2 or 1)
    TwoShowHandDataMgr.getInstance().m_cbCardData[1][1] = pGameData.cbObscureCard --底牌
    TwoShowHandDataMgr.getInstance().m_cbCardData[1][2] = pGameData.cbCardData[myIndex]
    TwoShowHandDataMgr.getInstance().m_cbCardData[2][2] = pGameData.cbCardData[otherIndex]
    for i=1,2 do
        TwoShowHandDataMgr.getInstance().m_llChipNum[i] = pGameData.llCellScore
    end
    TwoShowHandDataMgr.getInstance():setGameStatus(TwoShowHandConst.GAME_STATUS_PLAY)
    TwoShowHandDataMgr.getInstance():setSendCardCount(2)

    SLFacade:dispatchCustomEvent(TwoShowHand_Events.MSG_TWOSHOWHAND_GAME_START)

    print("CMsgTwoShowHand:process_200_100 end")
end

--加注结果
function CMsgTwoShowHand:process_200_101(__data)
    local len = __data:getReadableSize()
    local _buffer = __data:readData(len)
    
-- -- 用户下注
-- struct CMD_S_AddScore
-- {
--     uint8                               cbAddType                          -- 下注类型： 0不加注 1跟注 2加注
--     uint16                              wCurrentUser                       -- 当前用户
--     uint16                              wAddScoreUser                      -- 加注用户
--     local                                 iAddTimes                          -- 加注倍数
--     local                                 iTurnMaxTimes                      -- 本轮当前最大加注倍数
-- }

    local pGameData = {}
    pGameData.cbAddType = _buffer:readUChar()
    pGameData.wCurrentUser = _buffer:readUShort()
    pGameData.wAddScoreUser = _buffer:readUShort()
    pGameData.iAddTimes = _buffer:readInt()
    pGameData.iTurnMaxTimes = _buffer:readInt()

    TwoShowHandDataMgr.getInstance():setCurrentUser(pGameData.wCurrentUser)
    TwoShowHandDataMgr.getInstance():setAddType(pGameData.cbAddType)
    local downTimes = pGameData.iTurnMaxTimes - TwoShowHandDataMgr.getInstance():getTurnMaxTimes()
    TwoShowHandDataMgr.getInstance():setTurnMaxTimes(pGameData.iTurnMaxTimes)
    TwoShowHandDataMgr.getInstance():setAddDownTimes(downTimes)
    TwoShowHandDataMgr.getInstance():setAddScoreUser(pGameData.wAddScoreUser)
    if pGameData.cbAddType == 1 or pGameData.cbAddType == 2 then
        local index = TwoShowHandDataMgr.getInstance():getViewIndex(pGameData.wAddScoreUser)
        local score = TwoShowHandDataMgr.getInstance():getCellScore()*pGameData.iAddTimes
        TwoShowHandDataMgr.getInstance().m_llChipNum[index] = TwoShowHandDataMgr.getInstance().m_llChipNum[index] + score
        TwoShowHandDataMgr.getInstance().m_iTurnCurrentDown[index] = pGameData.iAddTimes
    end

    SLFacade:dispatchCustomEvent(TwoShowHand_Events.MSG_TWOSHOWHAND_ADD_SCORE)

    print("CMsgTwoShowHand:process_200_101 end")
end

--放弃跟注
function CMsgTwoShowHand:process_200_102(__data)
    local len = __data:getReadableSize()
    local _buffer = __data:readData(len)

-- -- 用户放弃
-- struct CMD_S_GiveUp
-- {
--     uint16                              wGiveUpUser                        -- 放弃用户
-- }

    local pGameData = {}
    pGameData.wGiveUpUser = _buffer:readUShort()

    TwoShowHandDataMgr.getInstance():setGiveUpUser(pGameData.wGiveUpUser)
    SLFacade:dispatchCustomEvent(TwoShowHand_Events.MSG_TWOSHOWHAND_GIVE_UP)

    print("CMsgTwoShowHand:process_200_102 end")
end

--发牌消息
function CMsgTwoShowHand:process_200_103(__data)
    local len = __data:getReadableSize()
    local _buffer = __data:readData(len)

-- -- 发牌
-- struct CMD_S_SendCard_TwoShowHand
-- {
--     uint16                              wCurrentUser                       -- 当前用户
--     uint8                               cbSendCardCount                    -- 发牌数目
--     uint8                               cbCardData[2]                      -- 用户扑克
-- }

    local pGameData = {}
    pGameData.wCurrentUser = _buffer:readUShort()
    pGameData.cbSendCardCount = _buffer:readUChar()
    pGameData.cbCardData = {}
    pGameData.cbCardData[1] = _buffer:readUChar()
    pGameData.cbCardData[2] = _buffer:readUChar()

    TwoShowHandDataMgr.getInstance():setTurnMaxTimes(0)
    TwoShowHandDataMgr.getInstance():setAddDownTimes(0)
    TwoShowHandDataMgr.getInstance():setCurrentUser(pGameData.wCurrentUser)
    TwoShowHandDataMgr.getInstance():setSendCardCount(pGameData.cbSendCardCount)
    local myChair = PlayerInfo.getInstance():getChairID()
    local myIndex = (myChair == 0 and 1 or 2)
    local otherIndex = (myChair == 0 and 2 or 1)
    TwoShowHandDataMgr.getInstance().m_cbCardData[1][pGameData.cbSendCardCount] = pGameData.cbCardData[myIndex]
    TwoShowHandDataMgr.getInstance().m_cbCardData[2][pGameData.cbSendCardCount] = pGameData.cbCardData[otherIndex]
    TwoShowHandDataMgr.getInstance().m_iTurnCurrentDown[1] = 0
    TwoShowHandDataMgr.getInstance().m_iTurnCurrentDown[2] = 0

    SLFacade:dispatchCustomEvent(TwoShowHand_Events.MSG_TWOSHOWHAND_SEND_CARD)

    print("CMsgTwoShowHand:process_200_103 end")
end

--玩家梭哈
function CMsgTwoShowHand:process_200_104(__data)
    local len = __data:getReadableSize()
    local _buffer = __data:readData(len)

-- -- 用户梭哈
-- struct CMD_S_ShowHand
-- {
--     uint16                              wShowHandUser                      -- 梭哈用户
--     uint16                              wCurrentUser                       -- 当前用户
-- }

    local pGameData = {}
    pGameData.wShowHandUser = _buffer:readUShort()
    pGameData.wCurrentUser = _buffer:readUShort()

    TwoShowHandDataMgr.getInstance():setCurrentUser(pGameData.wCurrentUser)
    TwoShowHandDataMgr.getInstance():setShowHandUser(pGameData.wShowHandUser)
    local index = TwoShowHandDataMgr.getInstance():getViewIndex(pGameData.wShowHandUser)
    TwoShowHandDataMgr.getInstance().m_bShowHand[index] = true

    SLFacade:dispatchCustomEvent(TwoShowHand_Events.MSG_TWOSHOWHAND_SHOW_HAND)

    print("CMsgTwoShowHand:process_200_104 end")
end

--游戏结束
function CMsgTwoShowHand:process_200_105(__data)
    local len = __data:getReadableSize()
    local _buffer = __data:readData(len)
    
-- -- 游戏结束
-- struct CMD_S_GameEnd_TwoShowHand
-- {
--     uint8                               cbGameEndType                      -- 结束类型
--     uint8                               cbCardData[2][5]                   -- 用户扑克
--     long long                           llResult[2]                        -- 游戏得分
-- }

    local pGameData = {}
    pGameData.cbGameEndType = _buffer:readUChar()
    pGameData.cbCardData = {}
    for i=1,2 do
        pGameData.cbCardData[i] = {}
        for j=1,5 do
            pGameData.cbCardData[i][j] = _buffer:readUChar()
            -- print(string.format("i:%d,j:%d,card:%d", i, j, pGameData.cbCardData[i][j]))
        end
    end
    pGameData.llResult = {}
    pGameData.llResult[1] = _buffer:readLongLong()
    pGameData.llResult[2] = _buffer:readLongLong()

    TwoShowHandDataMgr.getInstance():setGameEndType(pGameData.cbGameEndType)
    local myChair = PlayerInfo.getInstance():getChairID()
    -- print("myChair:", myChair)
    local myIndex = (myChair == 0 and 1 or 2)
    local otherIndex = (myChair == 0 and 2 or 1)
    TwoShowHandDataMgr.getInstance().m_cbCardData[1] = table.copy(pGameData.cbCardData[myIndex])
    TwoShowHandDataMgr.getInstance().m_cbCardData[2] = table.copy(pGameData.cbCardData[otherIndex])
    TwoShowHandDataMgr.getInstance().m_llResult[1] = pGameData.llResult[myIndex]
    TwoShowHandDataMgr.getInstance().m_llResult[2] = pGameData.llResult[otherIndex]
    
    TwoShowHandDataMgr.getInstance():setLastScore(pGameData.llResult[myIndex])
    local tempScore = TwoShowHandDataMgr.getInstance():getHistoryScore() + pGameData.llResult[myIndex]
    TwoShowHandDataMgr.getInstance():setHistoryScore(tempScore)

    -- print("本局结束类型:%d", pGameData.cbGameEndType)
    SLFacade:dispatchCustomEvent(TwoShowHand_Events.MSG_TWOSHOWHAND_GAME_END)

    print("CMsgTwoShowHand:process_200_105 end")
end

--等待开牌
function CMsgTwoShowHand:process_200_106(__data)
    TwoShowHandDataMgr.getInstance():setWaitOpenCard(true)
    SLFacade:dispatchCustomEvent(TwoShowHand_Events.MSG_TWOSHOWHAND_GAME_OPENCARD)

    print("CMsgTwoShowHand:process_200_106 end")
end

--开牌通知
function CMsgTwoShowHand:process_200_107(__data)
    local len = __data:getReadableSize()
    local _buffer = __data:readData(len)

-- -- 开牌通知
-- struct CMD_S_OpneCardNotify
-- {
--     uint16                              wChairID
-- }

    local pGameData = {}
    pGameData.wChairID = _buffer:readUShort()

    local _event = {
        name = TwoShowHand_Events.MSG_TWOSHOWHAND_GAME_OPENCARD_INFO,
        packet = {wChairID = pGameData.wChairID},
    }
    SLFacade:dispatchCustomEvent(TwoShowHand_Events.MSG_TWOSHOWHAND_GAME_OPENCARD_INFO, _event)

    print("CMsgTwoShowHand:process_200_107 end")
end

--看牌通知
function CMsgTwoShowHand:process_200_108(__data)
    SLFacade:dispatchCustomEvent(TwoShowHand_Events.MSG_TWOSHOWHAND_GAME_LOOKCARD)

    print("CMsgTwoShowHand:process_200_108 end")
end

function CMsgTwoShowHand:process_200_110(__data)
--    //看对方明牌
--struct CMD_S_LookEnemyCard
--{
-- BYTE        cbCardData;
--};
    local len = __data:getReadableSize()
    local _buffer = __data:readData(len)
    local pGameData = {}
    pGameData.cbCardData = _buffer:readUChar()
    TwoShowHandDataMgr.getInstance().m_cbCardData[2][1] = pGameData.cbCardData

    SLFacade:dispatchCustomEvent(TwoShowHand_Events.MSG_TWOSHOWHAND_GAME_LOOK_ENEMY_CARD)
end

--系统消息
function CMsgTwoShowHand:process_200_220(__data)
    local len = __data:getReadableSize()
    local _buffer = __data:readData(len)

-- -- 系统消息
-- struct CMD_S_SysMessage
-- {
--     short GetBufferLen()
--     {
--         return (short)(sizeof(CMD_S_SysMessage)+sizeof(szSysMessage)/sizeof(char16_t)-256)
--     }
--     short              wSysType               -- 消息类型 （ 0-普通消息 ）
--     char16_t            szSysMessage[256]      -- 消息内容
-- }

    local pSysMessage = {}
    pSysMessage.wSysType = _buffer:readShort()
    pSysMessage.szSysMessage = _buffer:readString(256*2)

    local strMessage = string.trim(pSysMessage.szSysMessage)
    if pSysMessage.wSysType == G_CONSTANTS.UPRUN_MSG then
        FloatMessage.getInstance():pushMessageForString(strMessage)
    end

    print("CMsgTwoShowHand:process_200_220 end")
end

return CMsgTwoShowHand

--endregion
