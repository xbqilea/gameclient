module(..., package.seeall)

Pst21DotPlayerInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2, 	1, 'm_nickname'			, 'STRING'				, 1 	, '昵称' },
	{ 3, 	1, 'm_faceId'			, 'UINT'				, 1 	, '头像ID' },
	{ 4, 	1, 'm_score'			, 'UINT'				, 1 	, '金币' },
	{ 5, 	1, 'm_chairId'			, 'UBYTE'				, 1 	, '位子 0表示庄家 座位号1-5 + 分牌, 对应数值为10,11,20,21... ' },
	{ 6, 	1, 'm_bets'				, 'Pst21DotPart'		, 10 	, '分组信息' },
}

Pst21DotUserEndInfo = 
{
	{ 1, 	1, 'm_chairId'			, 'INT'					, 1		, '操作序列, 0表示庄家 座位号1-5 + 分牌, 对应数值为10,11,20,21... ' },
	{ 2,	1, 'm_profit'			, 'INT'					, 1		, '结算盈利, 扣税后, 不计下注'},
}

Pst21DotUserTotalEndInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	--{ 2, 	1, 'm_endInfo'			, 'Pst21DotUserEndInfo'	, 5		, '各区域下注结算' },
	{ 2,	1, 'm_profit'			, 'INT'					, 1		, '所有下注区域结算盈利, 扣税后, 不计下注(有多个下注区域情况下)'},
	{ 3,	1, 'm_curScore'			, 'UINT'				, 1		, '结算后总金币'},
	{ 4, 	1, 'm_chairId'			, 'UBYTE'				, 1		, '操作序列, 0表示庄家 座位号1-5 + 分牌, 对应数值为10,11,20,21... ' },
}

Pst21DotBalanceData =
{
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1    , '玩家ID'},
	{ 2		, 2		, 'm_curCoin'			, 'UINT'				, 1	   , '当前金币'},
	{ 3		, 2		, 'm_type'				, 'UBYTE'				, 1	   , '操作类型 0-正常结算 1-退出'},
	{ 4		, 2		, 'm_lastBet'			, 'INT'					, 1	   , '续压金额'},
}

Pst21DotSyncData =
{
	{ 1		, 1		, 'm_syncInfo'			, 'PstSyncGameDataEx'	, 1    , '玩家信息'},
	{ 2		, 2		, 'm_chairId'			, 'UBYTE'				, 1	   , '操作序列, 0表示庄家 座位号1-5 + 分牌, 对应数值为10,11,20,21... '},
	{ 3		, 2		, 'm_lastBet'			, 'INT'					, 1	   , '续压金额'},
}

--[[
Pst21DotDim =
{
	{ 1		, 1		, 'm_cards'		 	, 'UBYTE'	, 5	   , '牌面'},	
	{ 2		, 1		, 'm_betVal'		, 'UINT'	, 1    , '下注额'},
	{ 3		, 1		, 'm_hasDouble'		, 'UBYTE'	, 1    , '是否已进行双倍操作 1-是 0-否'},
	{ 4		, 1		, 'm_hasFinish'		, 'UBYTE'	, 1    , '是否要牌操作结束 1-已经结束 0-未结束'},
	{ 5		, 1		, 'm_points'		, 'UBYTE'	, 2    , '点数'},
	{ 6		, 1		, 'm_dim'			, 'UBYTE'	, 1    , '牌型, 0-操作未结束 1-爆牌 2-低于21点 3-21点 4-五小龙 5-黑杰克 '},
}

Pst21DotBetInfo = 
{
	{ 1		, 1		, 'm_accountId'			, 'INT'		, 1    , '玩家ID'},
	{ 2		, 1		, 'm_chairId'			, 'INT'		, 1    , '座位号 1, 2, 3, 4, 5 0-表示庄家'},
	{ 3		, 1		, 'm_hasBuyInsure'		, 'UBYTE'	, 1    , '是否已购买保险 1-是 0-否'},
	{ 4		, 1		, 'm_hasCut'			, 'UBYTE'	, 1    , '是否已经分牌 1-已经分牌 0-未分牌'},
	{ 5		, 1		, 'm_cutId'        	    , 'UBYTE'   , 1    , '非分牌为0 已经分牌轮到第一组操作为1, 第二组操作为2' },
	{ 6		, 1		, 'm_cards1'		 	, 'Pst21DotDim'	, 1	   , '第一组牌面数据'},	
	{ 7		, 1		, 'm_cards2'		 	, 'Pst21DotDim'	, 1	   , '第二组牌面数据'},	
}
--]]

Pst21DotPart = 
{
	{ 1		, 1		, 'm_chairId'			, 'INT'		, 1    , '操作序列, 0表示庄家 座位号1-5 + 分牌, 对应数值为10,11,20,21... '},
	{ 2		, 1		, 'm_hasBuyInsure'		, 'UBYTE'	, 1    , '是否已购买保险 1-是 0-否'},
	{ 3		, 1		, 'm_hasDouble'			, 'UBYTE'	, 1    , '是否已进行双倍操作 1-是 0-否'},
	{ 4		, 1		, 'm_hasFinish'			, 'UBYTE'	, 1    , '是否要牌操作结束 1-已经结束 0-未结束'},
	{ 5		, 1		, 'm_cards'		 		, 'UBYTE'	, 5	   , '牌面数据'},	
	{ 6		, 1		, 'm_betValue'		 	, 'INT'		, 1	   , '下注'},	
	{ 7		, 1		, 'm_points'		 	, 'UBYTE'	, 2	   , '点数, 牌型确定后取最大点数'},	
	{ 8		, 1		, 'm_dimType'		 	, 'UBYTE'	, 1	   , '牌型, 0-操作未结束 1-爆牌 2-低于21点 3-21点 4-五小龙 5-黑杰克 '},	
}


Pst21DotCards =
{
	{ 1,	1, 'm_chairId'		 	 , 'UBYTE'						, 1		, '操作序列, 0表示庄家 座位号1-5 + 分牌, 对应数值为10,11,20,21... '},
	{ 2,	1, 'm_cards'		 	 , 'UBYTE'						, 5		, '对方牌面'},
	{ 3,	1, 'm_points'		 	 , 'UBYTE'						, 2		, '牌面点数'},
}