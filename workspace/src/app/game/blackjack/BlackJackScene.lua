--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
local Scheduler           = require("framework.scheduler") 
local CCGameSceneBase     = require("src.app.game.common.main.CCGameSceneBase") 
local BlackJackGameLayer     = import(".BlackJackGameLayer")
local DlgAlert = require("app.hall.base.ui.MessageBox")

local BlackJackGameScene= class("BlackJackGameScene", function()
        return CCGameSceneBase.new()
end)

function BlackJackGameScene:ctor()  
    self.m_BlackJackMainLayer         = nil 
    self.m_BlackJackExitGameLayer     = nil
    self.m_BlackJackRuleLayer         = nil
    self.m_BlackJackMusicSetLayer     = nil 
    self:myInit()
end

-- 游戏场景初始化
function BlackJackGameScene:myInit() 
 -- self:loadResources()
  --BlackJackRoomController:getInstance():setInGame( true )  
  -- 主ui 
	self:initBlackJackGameMainLayer()

	self:registBackClickHandler(handler(self, self.onBackButtonClicked)) -- Android & Windows注册返回按钮
	addMsgCallBack(self, MSG_ENTER_FOREGROUND, handler(self, self.onEnterForeground)) -- 转前台
	addMsgCallBack(self, MSG_ENTER_BACKGROUND, handler(self, self.onEnterBackground)) -- 转后台 
	--addMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT, handler(self, self.socketState))
	--addMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT, handler(self,self.onSocketEventMsgRecived))
end

---- 进入场景
function BlackJackGameScene:onEnter()
   print("-----------BlackJackGameScene:onEnter()-----------------")
   ToolKit:setGameFPS(1/60.0) 
end

-- 初始化主ui
function BlackJackGameScene:initBlackJackGameMainLayer()
    self.m_BlackJackMainLayer = BlackJackGameLayer.new()
    self:addChild(self.m_BlackJackMainLayer)
end

function BlackJackGameScene:getMainLayer()
    return self.m_BlackJackMainLayer 
end

-- 显示游戏退出界面
-- @params msgName( string ) 消息名称
-- @params __info( table )   退出相关信息
-- 显示游戏退出界面
--[[
function BlackJackGameScene:showExitGameLayer()
    if self._Scheduler1 then
        Scheduler.unscheduleGlobal(self._Scheduler1)	
        self._Scheduler1 = nil
    end 
     
    UIAdapter:popScene()
    g_GameController.gameScene = nil
end
--]]

function BlackJackGameScene:onEnter()
	print("------BlackJackGameScene:onEnter begin--------")
	print("------BlackJackGameScene:onEnter end--------")
end
   
-- 退出场景
function BlackJackGameScene:onExit()
    print("------BlackJackGameScene:onExit begin--------") 
	
	if self.m_BlackJackMainLayer._Scheduler1 then
        Scheduler.unscheduleGlobal(self.m_BlackJackMainLayer._Scheduler1)	
        self.m_BlackJackMainLayer._Scheduler1 = nil
    end 
     self.m_BlackJackMainLayer:onCleanup()
    removeMsgCallBack(self, MSG_ENTER_FOREGROUND)
    removeMsgCallBack(self, MSG_ENTER_BACKGROUND)
    --removeMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT)
    --removeMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT)
--    BlackJackGlobal.m_isNeedReconectGameServer = false
--    BlackJackRoomController:getInstance():setInGame( false )
--    BlackJackGameController:getInstance():onDestory()
 --   self:RemoveResources()
	print("------BlackJackGameScene:onExit end--------")
end

-- 响应返回按钮事件
function BlackJackGameScene:onBackButtonClicked() 
	 g_GameController:exitReq()
        UIAdapter:popScene()   
end

-- 从后台切换到前台
function BlackJackGameScene:onEnterForeground()
	print("从后台切换到前台")
	g_GameController:gameBackgroundReq(2)
end

-- 从前台切换到后台
function BlackJackGameScene:onEnterBackground()
   print("从前台切换到后台,游戏线程挂起!")
   g_GameController:gameBackgroundReq(1)
   g_GameController.m_BackGroudFlag = true
end

function BlackJackGameScene:clearView()
   print("BlackJackGameScene:clearView()")
end

-- 清理数据
function BlackJackGameScene:clearData()
   
end 


return BlackJackGameScene
