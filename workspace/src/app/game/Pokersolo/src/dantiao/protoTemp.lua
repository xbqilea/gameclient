module(..., package.seeall)

-- 协议定义

--CLIENT<->SCENE

CS_C2M_DanTiao_ExitScene_Req = 
{
	
}

CS_M2C_DanTiao_ExitScene_Ack = 
{
	{ 1,	1, 'm_ret'               , 'INT'                 		, 1     , '结果返回' },
}

CS_C2M_DanTiao_EnterRoom_Req =                                	
{                                                       	
	{ 1,	1, 'm_roomId'            , 'UINT'                		, 1     , '房间号-6位数十进制' },
	{ 2,	1, 'm_roomKey'            , 'UINT'                		, 1     , '房间码' },
}

CS_M2C_DanTiao_EnterRoom_Ack =                                	
{
	{ 1,	1, 'm_roomId'            , 'UINT'                		, 1		, '房间号-6位数十进制' },
	{ 2,	1, 'm_ret'				 , 'INT'				 		, 1 	, '进入房间结果'},
	{ 3,	1, 'm_gameAtomTypeId'	 , 'UINT'				 		, 1		, '游戏类型id'},
    { 4, 	1, 'm_playerInfo'		 , 'PstDanTiaoPlayerInfo'  		, 1		, '房内玩家信息' },		
	--{ 5, 	1, 'm_nCasinoId'		     , 'UINT'  		                , 1		, '电玩城ID' },
} 

CS_M2C_DanTiao_EnterRoom_Nty =                                	
{
	{ 1,	1, 'm_roomId'            , 'UINT'                		, 1    	, '房间号-6位数十进制' },
	{ 2,    1, 'm_roomCreator'       , 'UINT'                		, 1    	, '房主的accountId' },
	{ 3, 	1, 'm_playerInfo'		 , 'PstDanTiaoOtherPlayerInfo'  	, 1		, '房内玩家信息' },	
}

CS_C2M_DanTiao_ExitRoom_Req =                                	
{                                                       	
}

CS_M2C_DanTiao_ExitRoom_Nty =                                	
{
	{ 1,	1, 'm_roomId'            , 'UINT'                		, 1     , '房间号-6位数十进制' },
	{ 2,	1, 'm_accountId'		 , 'UINT'						, 1 	, '玩家ID'},
}



--CLIENT<->SERVER

CS_C2G_DanTiao_Bet_Req = 
{
	{ 1,	1, 'm_betId'			 , 'UBYTE'						, 1		, '下注区域ID, 1:黑桃,2:红桃,3:梅花,4:方块,5:王'},
	{ 2,	1, 'm_betValue'			 , 'UINT'						, 1		, '下注额'},
	{ 3,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },	
}

CS_G2C_DanTiao_Bet_Ack = 
{
	{ 1,	1, 'm_betId'			 , 'UBYTE'						, 1		, '下注区域ID'},
	{ 2,	1, 'm_betValue'			 , 'UINT'						, 1		, '下注额'},		
	{ 3,	1, 'm_ret'			 	 , 'INT'						, 1		, '下注结果(-1:系统错误;-2:游戏不能下注;-3:金币不足;-4:新手时总投注额不能超过限制;-5:单区域投注总额不能超过限制;)'},
	{ 4,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },	
	{ 5,	1, 'm_curCoin'           , 'UINT'       				, 1     , '当前金币。 m_ret = 0时生效' },
	{ 6,	1, 'm_coinLimit'         , 'UINT'       				, 1     , '金币限制, 仅在m_ret为特定值时生效'},
}

--续压请求
CS_C2G_DanTiao_BetContinue_Req = 
{
	{ 1,	1, 'm_continueBetArr'	 ,  'PstDantiaoContinueBet'	, 1024	, '玩家续押信息'},
}

--续压响应
CS_G2C_DanTiao_BetContinue_Ack = 
{
	{ 1, 	1, 'm_result'		 ,  'INT'					, 1		, '0:成功, -x:失败(m_result=0时，广播此ack)' },
	{ 2, 	1, 'm_betAccountId'	 ,  'UINT'					, 1		, '下注玩家ID' },
	{ 3,	1, 'm_curCoin'		 ,  'UINT'					, 1		, '下注成功后的玩家金币(仅在m_result=0,且m_betAccountId=自己时有效)'},
	{ 4,	1, 'm_coinLimit'         , 'UINT'       		    , 1     , '金币限制, 仅在m_result为特定值时生效' },
	{ 5,	1, 'm_continueBetArr'	 ,  'PstDantiaoContinueBet'	, 1024	, '玩家续押信息'},
}

--发送玩家自己下注信息(进入游戏时发, 只发一次)
CS_G2C_DanTiao_Bet_Nty = 
{
	{ 1,	1, 'm_vecBetList'		 , 'PstDanTiaoBetInfo'			, 8		, '玩家下注信息'},
	{ 2,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },	
}

CS_G2C_DanTiao_Bet_Sum_Nty = 
{
	{ 1,	1, 'm_vecBetSumList'	 , 'PstDanTiaoBetSumInfo'			, 64	, '下注汇总信息'},
	{ 2,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },	
	{ 3,    1,'m_nTotalValue' , 'UINT'       				, 1     , '玩家下注总值'},
}

CS_G2C_DanTiao_Bet_Addition_Sum_Nty = 
{
	{ 1,	1, 'm_vecBetAddSumList'  , 'PstDanTiaoBetSumInfo'			, 64	, '下注增量汇总信息'},
	{ 2,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
}

CS_G2C_DanTiao_GameBalance_Nty = 
{
	{ 1,	1, 'm_profitTop3'		 , 'PstDantiaoProfitTop3'		, 64	, '盈利前三名'},
	{ 2,	1, 'm_vecBalance'		 , 'PstDanTiaoBalanceInfo'		, 64	, '结算信息'},
}

CS_G2C_DanTiao_GameDanTiaoNumber_Nty = 
{
	{ 1,	1, 'm_luckNumberInfo'	 , 'PstDanTiaoNumberInfo'		, 1		, '游戏中奖号码信息'},
	{ 2,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
}

CS_G2C_DanTiao_GameTimeLine_Nty = {
    { 1,    1, 'm_waitingMS'        , 'UINT'                        , 1    , '下注阶段剩余毫秒数' },
    { 2,    1, 'm_fateWhellMS'        , 'UINT'                        , 1    , '命运转轮剩余毫秒数' },
    { 3,    1, 'm_playingMS'        , 'UINT'                        , 1    , '开奖阶段剩余毫秒数' },
    { 4,    1, 'm_playingWaitMS'    , 'UINT'                        , 1    , '双响炮中间间隔时间' },
    { 5,    1, 'm_playingDoubleMS'    , 'UINT'                        , 1    , '双响炮开奖阶段剩余毫秒数' },
    { 6,    1, 'm_balanceMS'        , 'UINT'                        , 1    , '结算阶段剩余毫秒数' },
    { 7,    1, 'm_playerCount'    , 'UINT'                    , 1    , '玩家人数' },
    { 8,    1, 'm_accountId'        , 'UINT'                    , 1    , '玩家ID' },
    { 9,    1, 'm_fixedWaitingMS'    , 'UINT'                        , 1    , '下注阶段固定毫秒数' },
    { 10,    1, 'm_girlId'            , 'UINT'                        , 1    , '美女id' },
	{ 11,    1, 'm_roundInningInfo'    , 'PstDantiaoRoundInningInfo'    , 1    , '轮局数信息' },
	{ 12,	1, 'm_recordId'       	 ,  'STRING'       				, 1     , '牌局编号' },
	{ 13,	1, 'm_fourColorMin'         , 'UINT'       				, 1     , '四种花色最低下注' },
	{ 14,	1, 'm_fourColorMax'         , 'UINT'       				, 1     , '四种花色最高下注' },
	{ 15,	1, 'm_kingMax'         		, 'UINT'       				, 1     , '王最高下注' },
}

-- CS_C2G_DanTiao_BallHistory_Req = 
-- {
	-- { 1,	1, 'm_roundId'			 , 'UINT'						, 1		, '游戏轮次'},
	-- { 2,	1, 'm_roundNum'			 , 'UINT'						, 1		, '获取多少轮'},
	-- { 3,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
-- }

CS_G2C_DanTiao_BallHistory_Nty = 
{
	{ 1,	1, 'm_type'         	 , 'UINT'       				, 1     , '0:全量 1:增量' },
	{ 2,	1, 'm_vecBallHistory'	 , 'UINT'						, 1024	, '游戏历史中奖号码(0 - 100 个)'},
	{ 3,	1, 'm_historyAwardCnt'   , 'PstDantiaoHistoryAwardCnt'  , 1  , '历史开奖数量(黑桃、红桃、梅花、方块、王 各自开出的数量)' },
}

CS_G2C_DanTiao_RankPlayer_Nty = {
	{ 1,	1, 'm_vecRankPlayer'	 , 'PstDanTiaoRankPlayerInfo'		, 16	, '上榜玩家信息'},
	{ 2,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
}

CS_G2C_DanTiao_Notice_Bigbet_Nty = {
	{ 1,	1, 'm_betId'			 , 'UBYTE'						, 1		, '下注区域ID'},
	{ 2,	1, 'm_betValue'			 , 'UINT'						, 1		, '下注额'},
	{ 3, 	1, 'm_playerInfo'		 , 'PstDanTiaoOtherPlayerInfo'  	, 1		, '房内玩家信息' },	
	{ 4,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
}

CS_C2G_DanTiao_Rank_Req = 
{
	{ 1,	1, 'm_rankType'			 , 'UINT'						, 1		, '排行榜类型(1:日榜;2:周榜;'},
	{ 2,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
}

CS_G2C_DanTiao_Rank_Ack = 
{
	{ 1,	1, 'm_rankType'			 , 'UINT'						, 1		, '排行榜类型(1:日榜;2:周榜;'},
	{ 2, 	1, 'm_vecRankPlayerInfo' , 'PstDanTiaoDayWeekRankInfo'  	, 256	, '房内玩家信息' },	
	{ 3,	1, 'm_selfWinGold'		 , 'INT'						, 1		, '玩家自己盈利金币'},
	{ 4,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
}

CS_C2G_DanTiao_Bet_History_Req = 
{
	{ 1,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
}

CS_G2C_DanTiao_Bet_History_Ack = 
{
	{ 1, 	1, 'm_vecBetHistory' 	, 'PstDanTiaoBetHistoryInfo'  	, 32	, '下注历史信息' },	
	{ 2,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
}

CS_C2G_DanTiao_Bet_Cancel_Req = 
{
	{ 1,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
}

CS_G2C_DanTiao_Bet_Cancel_Ack = 
{
	{ 1,	1, 'm_ret'			 	 , 'INT'						, 1		, '撤销下注结果(-1:失败,0:成功'},	
	{ 2,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
	{ 3,    1, 'm_curCoin'           , 'UINT'                    	, 1    , '玩家当前金币(撤注成功后)' },
	-- { 4,	1, 'm_afterBetArr'		 , 'PstDanTiaoBetInfo'			, 1		, '撤注后的玩家下注信息'},
}

CS_C2G_DanTiao_Player_Online_List_Req	=
{
	{ 1,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
	{ 2,	1, 'm_startIndex'        , 'UINT'       				, 1     , '开始索引(从1开始)' },
	{ 3,	1, 'm_endIndex'          , 'UINT'       				, 1     , '结束索引' },
}

CS_G2C_DanTiao_Player_Online_List_Ack = 
{
	{ 1,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
	{ 2,	1, 'm_startIndex'        , 'UINT'       				, 1     , '开始索引(从1开始)' },
	{ 3,	1, 'm_endIndex'          , 'UINT'       				, 1     , '结束索引' },	
	{ 4,	1, 'm_maxPlayerCount'    , 'UINT'       				, 1     , '玩家总数' },
	{ 5, 	1, 'm_playerInfo'		 , 'PstDanTiaoOnlinePlayerInfo'  	, 64	, '在线玩家信息' },	
}

CS_G2C_DanTiao_Total_Round_Nty =
{ 
	{ 1,	1, 'nTotalRound'	 , 'UINT'						, 1 	, '总场数' },
	{ 2,	1, 'Spades_round'			 , 'UINT'						, 1		, '黑桃场数' },	
	{ 3,	1, 'Hearts_round'	 		 , 'UINT'				, 1 	, '红桃场数' },
	{ 4, 	1, 'Plumblossom_round', 'UINT'						, 1	    , '梅花场数'},
	{ 5, 	1, 'Square_round'	 , 'UINT'						, 1	    , '方块场数'},
	{ 6,	1, 'Joker_round'               , 'UINT'                 		, 1     , '王牌场数' },
    { 7,	1, 'Spades_round_Prob'	 , 'UINT'						, 1 	, '黑桃概率' },
	{ 8,	1, 'Hearts_round_Prob'			 , 'UINT'						, 1		, '红桃概率' },	
	{ 9,	1, 'Plumblossom_round_Prob'	 		 , 'UINT'				, 1 	, '梅花概率' },
	{ 10, 	1, 'Square_round_Prob', 'UINT'						, 1	    , '方块概率'},
	{ 11, 	1, 'Joker_round_Prob'	 , 'UINT'						, 1	    , '王牌概率'},
	{ 12, 	1, 'm_accountId'	 , 'UINT'						, 1	    , '玩家ID'},
}

CS_G2C_DanTiao_Win_GoldInfo_Nty =
{
	{1,	1, 'Win_Gold'	 , 'INT'						, 1 	, '赢得的金币数' },
	{ 2,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },   
}

CS_G2C_DanTiao_Watch_Message_Nty =
{
  { 1,	1, 'nWatchMessage'	, 'UINT'						, 1 	, '玩家阶段对应观战消息 1.(17场的时候) 2.(20场的时候)' },
  { 2,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
}


CS_G2C_DanTiao_Total_Gold_Nty=
{
{ 1,	1, 'nTotalGold'	, 'UINT'						, 1 	, '总金币' },
  { 2,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
  
}

CS_G2C_DanTiao_Total_Bet_Nty=
{
  { 1,	1, 'nTotalBet'	, 'UINT'						, 1 	, '总下注' },
  { 2,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
}

--[[
CS_C2G_DanTiao_Club_Player_Info_Req=
{
  { 1,	1, 'm_nMode'	, 'UINT'						, 1 	, '玩家模式' },
  { 2,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
  { 3,	1, 'm_casinoId'         , 'UINT'       				, 1     , '俱乐部ID' },
}

CS_G2C_DanTiao_Club_Player_Info_Ack = 
{
  { 1,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
  { 2, 1,   'm_casinoId'            ,'UINT'                       ,1       , '俱乐部ID'},
  { 3,	1, 'm_nTotalPlayer'	, 'UINT'						, 1 	, '总人数' },
}
--]]

CS_G2C_DanTiao_Online_Offline_Profit_Nty=
{
{ 1,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
  { 2, 1,   'm_nTotalBalance'            ,'INT'                       ,1       , '玩家从上线到下线净盈利通知'},
}

 CS_G2C_DanTiao_Bet_Max_Player_Nty          =
{
     { 1,	1, 'nAccountId'         , 'UINT'       				, 1     , '玩家ID' },
    { 2,	1, 'm_accountId'         , 'UINT'       				, 1     , '下注最多玩家ID' },
    { 3, 	1, 'm_nickname'			, 'STRING'				, 1 	, '昵称' },
}
	
CS_C2G_DanTiao_CuoPai_Complete_Player_Nty     =
{
  { 1,	1, 'nAccountId'         , 'UINT'       				, 1     , '玩家ID' },
  { 2,	1, 'm_nCuoPaiCompleteFlag'         , 'UINT'       				, 1     , '搓牌完成标志 0:未完成 1:完成' },
}

CS_G2C_DanTiao_CuoPai_Complete_Player_Nty     =
{
  { 1,	1, 'nAccountId'         , 'UINT'       				, 1     , '玩家ID' },
  { 2,	1, 'm_nCuoPaiCompleteFlag'         , 'UINT'       				, 1     , '搓牌完成标志 0:未完成 1:完成' },
}

CS_C2G_DanTiao_CuoPai_Start_Nty     =  
{
	{ 1,	1, 'nAccountId'         , 'UINT'       				, 1     , '玩家ID' },
	{ 2,	1, 'm_nCuoPaiAction'         , 'STRING'       				, 1     , '搓牌动作'},
}

CS_G2C_DanTiao_CuoPai_Start_Nty     =
{
	{ 1,	1, 'nAccountId'         	, 'UINT'       				, 1     , '玩家ID' },
	{ 2,	1, 'm_nCuoPaiAction'         , 'STRING'       			, 1     , '搓牌动作'},
	{ 3,	1, 'm_bRobotCuoPai'         , 'UBYTE'       			, 1     , '机器人开始搓牌，1-表示机器人搓牌，action为空，0表示真人搓牌'},
}

CS_G2C_DanTiao_Real_Online_Users_Nty = 
{
  { 1,	1, 'nAccountId'         , 'UINT'       				, 1     , '玩家ID' },
  { 2,	1, 'm_nRealOnlineCount'         , 'UINT'       				, 1     , '实时在线人数'},
}

CS_G2C_DanTiao_UserEachBet_Nty = 
{
	{1,	1,	'nAccountId'		,	'UINT'				,	1,		'玩家ID'},
	{2,	1,	'nBetId'			,	'UINT'				,	1,		'下注区域'},
	{3,	1,	'nBetCnt'			,	'UINT'				,	1,		'下注数量'},
}

CS_G2C_DanTiao_TopPlayer_Nty =
{
	{ 1,	1, 'topPlayerArr'	 , 'PstDanTiaoTopPlayerInfo'	 , 1024 	, '前6名玩家信息' },
}

CS_G2C_DanTiao_ChipAndAreaTimes_Nty =
{
	{1,	1,	'chipArr'		,	'UINT'							,	1024,		'筹码'},
	-- {2,	1,	'areaTimesArr'	,	'PstDanTiaoAreaTimes'			,	1024,		'下注区域倍数'},
}

--SERVER->SERVER

SS_M2G_DanTiao_CheckGamePlayer_Req =
{
	{ 1,	1, 'm_gameAtomTypeId'	 , 'UINT'						, 1 	, '游戏最小配置类型ID' },
	{ 2,	1, 'm_gameObjId'		 , 'STRING'						, 1 	, '游戏对象ID' },	
	{ 3,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
}

SS_G2M_DanTiao_CheckGamePlayer_Ack =
{
	{ 1,	1, 'm_gameAtomTypeId'	 , 'UINT'						, 1 	, '游戏最小配置类型ID' },
	{ 2,	1, 'm_gameObjId'		 , 'STRING'						, 1 	, '游戏对象ID' },
	{ 3,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
	{ 4,	1, 'm_ret'               , 'INT'                 		, 1     , '检查结果返回' },
}

SS_M2G_DanTiao_GameInit_Req = 
{
	{ 1,	1, 'm_gameAtomTypeId'	 , 'UINT'						, 1 	, '游戏最小配置类型ID' },
	{ 2,	1, 'm_gameObjId'		 , 'STRING'						, 1 	, '游戏对象ID' },
	{ 3,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },	
	{ 4,	1, 'm_gold'         	 , 'UINT'       				, 1     , '玩家金币' },
	{ 5,	1, 'm_keyGuld'         	 , 'STRING'       				, 1     , '游戏关联标识' },
	--{ 6,	1, 'm_casinoId'         	 , 'UINT'       				, 1     , '电玩城ID' },
	--{ 7,	1, 'm_careerId'         	 , 'UINT'       				, 1     , '创业模式ID' },
}

SS_G2M_DanTiao_GameInit_Ack =
{
	{ 1,	1, 'm_gameAtomTypeId'	 , 'UINT'						, 1 	, '游戏最小配置类型ID' },
	{ 2,	1, 'm_gameObjId'		 , 'STRING'						, 1 	, '游戏对象ID' },
	{ 3,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
	{ 4, 	1, 'm_playerCenterDomain', 'STRING'						, 1	    , '域名'},
	{ 5, 	1, 'm_playerCenterPort'	 , 'USHORT'						, 1	    , '端口'},	
	{ 6,	1, 'm_ret'               , 'INT'                 		, 1     , '初始化结果返回' },
}

SS_G2M_DanTiao_GoldBalance_Nty =
{
	{1, 1, 'm_balances'			 	 , 'PstDanTiaoBalanceEx'			, 1024		, '玩家结算信息'},
	{2,	1, 'm_roundInningInfo'	 	 ,  'PstDantiaoRoundInningInfo'		, 1			, '轮局数信息'},
}

SS_G2M_DanTiao_ExitRoom_Nty =
{
	{1, 1, 'm_strObjId'				 , 'STRING'						, 1		, '游戏对象ID'},
	{2,	1, 'm_accountId'        	 , 'UINT'       				, 1     , '玩家ID' },
	{3,	1, 'm_ret'              	 , 'INT'                 		, 1     , '退出结果返回' },
}

SS_G2M_DanTiao_RoundStatus_Nty =
{
	{1, 1, 'm_strObjId'				 , 'STRING'								, 1		, '游戏对象ID'},
	{2, 1, 'm_roundStatus'			 , 'UBYTE'								, 1		, '状态(0:开始;1:结束)' },
	{3,	1, 'm_girlId'	 	     	 ,  'UINT'								, 1		, '美女id'},
	{4,	1, 'm_roundInningInfo'	 	 ,  'PstDantiaoRoundInningInfo'			, 1		, '轮局数信息'},
	{5,	1, 'm_historyAwardCnt'   	 , 'PstDantiaoHistoryAwardCnt'  		, 1024  , '历史开奖数量' },
}

SS_G2M_DanTiao_Watch_Out_Nty =
{
    { 1,	1, 'm_nWatchCnt'	 , 'UINT'						, 1 	, '观战总次数' },
	{ 2,	1, 'm_accountId'         , 'UINT'       				, 1     , '玩家ID' },
	{ 3, 	1, 'm_strObjId'				 , 'STRING'						, 1		, '游戏对象ID'},
}

-------------------------------------------------------------------------------------------------------------
SS_M2G_DanTiao_GameCreate_Req =
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 		'UINT'			, 1	   , '游戏序号'},
	{ 2		, 1		, 'm_strGameObjId'		,		'STRING'					, 1		, '游戏ID'},
	{ 3		, 1		, 'm_nOpType'			,		'USHORT'						, 1		, '操作类型 1-创建桌子 2-新玩家加入'},
	{ 4		, 1		, 'm_vecAccounts'		,		'PstSyncGameDataEx'				, 1024		, '玩家数据'},
}

SS_G2M_DanTiao_GameCreate_Ack =
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 		'UINT'						, 1	   , '游戏序号'},
	{ 2		, 1		, 'm_strGameObjId'		,		'STRING'					, 1		, '游戏ID'},
	{ 3		, 1		, 'm_nOpType'			,		'USHORT'						, 1		, '操作类型 1-创建桌子 2-新玩家加入'},
	{ 4		, 1		, 'm_nResult'			,		'INT'						, 1		, '创建桌子结果[0:失败 1:成功]， 只针对m_nOpType == 有效'},
	{ 5		, 1		, 'm_vecAccounts'		,		'PstOperateRes'				, 1024		, '玩家初始化结果数据'},
}

SS_M2G_DanTiao_ForceLeave_Nty =
{
	{ 1		, 1		, 'm_nAccountID'		,		'UINT'		                    , 1	    , '账号'},
}

SS_M2G_DanTiao_GameMaintenance_Nty = 
{
	{ 1		, 1		, 'm_nGameTypeID'		, 'UINT'				, 1	   , '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_nOption'			, 'SHORT'				, 1	   , '1-维护 2-取消维护，如果场景重启会将此维护状态同步给场景（如果是维护状态）'},
	{ 3		, 1		, 'm_nStartTime'		, 'UINT'				, 1	   , '维护开始时间'},
	{ 4		, 1		, 'm_nEndTime'			, 'UINT'				, 1	   , '维护结束时间'},
}


CS_C2G_DanTiao_Background_Req =
{
	{ 1,	1, 'm_type'			 , 'INT'						, 1		, '1-切到后台 2-切回游戏'},
}

CS_G2C_DanTiao_Background_Ack =
{
	{ 1,	1, 'm_type'			 , 'INT'						, 1		, '1-切到后台 2-切回游戏'},
	{ 2,	1, 'm_ret'			 , 'INT'						, 1		, '退出结果 0-表示成功'},
}