--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

--endregion

local MyButton = class("MyButton", function()
    return cc.Node:create()
end)

MyButton.TOUCH_TYPE_CLICK = 1
MyButton.TOUCH_TYPE_CONTINUE = 2

local CONST_DELAY_CONTINUE_TIME = 1
local CONST_CONTINUE_TIME = 0.1

function MyButton:ctor( _normal, _selected, _disabled, _imageType )
    self:setContentSize(cc.size(0, 0))
    self._handler = nil
    self._normal = _normal
    self._selected = _selected
    self._disabled = _disabled
    self._imageType = _imageType
    self._isSwallowTouches = false
    self._isTouchEnabled = false
    self._isTouchEnabled = nil
    self._continueType = 0
    self._isContinue = false
    self._image = ccui.ImageView:create(_normal, _imageType)
    self:addChild(self._image)

    ToolKit:registDistructor(self, handler(self, self.onDestory))

end

function MyButton:onDestory()
    self:getEventDispatcher():removeEventListener(self._touchListener)
    self._touchListener = nil
end

function MyButton:addTouchEventListener(_handler)
    self._handler = _handler
end

function MyButton:getSize()
    return self._image:getContentSize()
end

function MyButton:setSwallowTouches(_isSwallowTouches)
    self._isSwallowTouches = _isSwallowTouches
    if self._touchListener then
        self._touchListener:setSwallowTouches(self._isSwallowTouches)
    end
end

function MyButton:setColor(color)
    self._image:setColor(color)
end

function MyButton:setTouchEnabled(_isTouchEnabled)
    if self._isTouchEnabled == _isTouchEnabled then
        return
    end
    self._isTouchEnabled = _isTouchEnabled
    if self._isTouchEnabled then
        self._isTouchBegan = false
        self._touchListener = cc.EventListenerTouchOneByOne:create()
        self._touchListener:setSwallowTouches(self._isSwallowTouches)
        self._touchListener:registerScriptHandler(handler(self,self.onTouchMoved),cc.Handler.EVENT_TOUCH_MOVED)
        self._touchListener:registerScriptHandler(handler(self,self.onTouchBegan),cc.Handler.EVENT_TOUCH_BEGAN)
        self._touchListener:registerScriptHandler(handler(self,self.onTouchEnded),cc.Handler.EVENT_TOUCH_ENDED)
        self._touchListener:registerScriptHandler(handler(self,self.onTouchCancelled),cc.Handler.EVENT_TOUCH_CANCELLED)
        self:getEventDispatcher():addEventListenerWithSceneGraphPriority(self._touchListener, self)
        self._image:loadTexture(self._normal, self._imageType)
        self:StopContinue()
    else
        self:getEventDispatcher():removeEventListener(self._touchListener)
        self._touchListener = nil
        self._isTouchBegan = false
        self:StopContinue()
        self._image:loadTexture(self._disabled, self._imageType)
    end
end

function MyButton:StopContinue()
    self._continueType = 0
    self:stopAllActions()
end

function MyButton:setIsContinue(isContinue)
    if isContinue == self._isContinue then
        return
    end

    self._isContinue = isContinue
    self:StopContinue()
end


function MyButton:loadTexture(_textureName, _textureResType)
    self._image:loadTexture(_textureName, _textureResType)
end



function MyButton:hitTest(_touch)
    local size = self._image:getContentSize()
    local point = self._image:convertToNodeSpace(_touch:getLocation())
    if point.x > 0 and point.y and point.x < size.width and point.y < size.height then
        return true
    end
    return false
end

function MyButton:interceptTouchEvent(_eventType)
    if self._handler then
        self._handler(self, _eventType)
    end
end

function MyButton:onTouchMoved(_touch, _eventType)
    if not self._isTouchBegan or not self:isVisible() or not self._isContinue then
        return
    end
    if self:hitTest(_touch) then
        self._image:loadTexture(self._selected, self._imageType)
        if self._continueType == 3 then
            self._continueType = 2
            self:doSomethingLater(function()
                self:interceptTouchEvent(MyButton.TOUCH_TYPE_CONTINUE)
            end, CONST_CONTINUE_TIME, true)
        elseif self._continueType == 0 then
            self._continueType = 1
            self:doSomethingLater(function()
                self._continueType = 2
                self:doSomethingLater(function()
                    self:interceptTouchEvent(MyButton.TOUCH_TYPE_CONTINUE)
                end, CONST_CONTINUE_TIME, true)
            end, CONST_DELAY_CONTINUE_TIME)
        end
    else
        self._image:loadTexture(self._normal, self._imageType)
        if self._continueType == 2 then
            self._continueType = 3
            self:stopAllActions()
        elseif self._continueType == 1 then
            self._continueType = 0
            self:stopAllActions()
        end
    end
    -- self:interceptTouchEvent()
end

function MyButton:onTouchBegan(_touch, _eventType)
    if self._isTouchBegan or not self:isVisible() then
        return false
    end

    if self:hitTest(_touch) then
        self._isTouchBegan = true
        self._image:loadTexture(self._selected, self._imageType)
        self:interceptTouchEvent(MyButton.TOUCH_TYPE_CLICK)
        if self._isContinue then
            self._continueType = 1
            self:doSomethingLater(function()
                self._continueType = 2
                self:doSomethingLater(function()
                    self:interceptTouchEvent(MyButton.TOUCH_TYPE_CONTINUE)
                end, CONST_CONTINUE_TIME, true)
            end, CONST_DELAY_CONTINUE_TIME)
        end
        return true
    end

    return false
end

function MyButton:onTouchEnded(_touch, _eventType)
    if not self._isTouchBegan or not self:isVisible() then
        return
    end
    self._isTouchBegan = false
    self:StopContinue()
    self._image:loadTexture(self._normal, self._imageType)
end

function MyButton:onTouchCancelled(_touch, _eventType)
    self._isTouchBegan = false
    self:StopContinue()
    self._image:loadTexture(self._normal, self._imageType)
end

function MyButton:doSomethingLater(_handler, _delay, _isLoop)
    self:stopAllActions()

    local delay = cca.delay(_delay)
    local callFunc = cca.callFunc(function()
        if _handler then
            _handler()
        end
    end)

    local seq = cca.seq({delay, callFunc})

    if _isLoop then
        self:runAction(cca.repeatForever(seq))
    else
        self:runAction(seq)
    end
end




return MyButton