--region require
local FiveStarsRes = require("FiveStars.config.FiveStarsRes")
local FiveStarsConfig = require("FiveStars.config.FiveStarsConfig")
local FiveStarsCardView = require("FiveStars.layer.FiveStarsCardView")
local GameSetLayer = require("src.app.newHall.childLayer.SetLayer")
local GameRecordLayer = require("src.app.newHall.childLayer.GameRecordLayer")
local FiveStarsData = require("FiveStars.config.FiveStarsData")
local scheduler = require("framework.scheduler")
local FiveStarsEvent = require("FiveStars.config.FiveStarsEvent")
local MyButton = require("FiveStars.layer.MyButton")
local ChatLayer = require("src.app.newHall.chat.ChatLayer")
local FivestarsHelpLayer = require("FiveStars.layer.FiveStarsHelpLayer")

-- local CChipCacheManager = require("src.app.game.common.manager.CChipCacheManager")
--endregion
local FiveStarsLayer = class("FiveStarsLayer", function()
    return display.newLayer()
end)

function FiveStarsLayer:ctor()
    self.m_nBetTypeCount = FiveStarsConfig.CONST_BET_COUNT
    self.m_nSelectBet = 1
    self.m_nClockTime = 0
    self.m_handlerUpdateClock = nil
    self.m_bIsContinue = false
    self.m_handlers = {}
    --黑红梅方
    self.m_AreaButtonImage = {
        {"ui_normal_10.png", "ui_push_10.png", "ui_s1.png"},
        {"ui_normal_12.png", "ui_push_12.png", "ui_s2.png"},
        {"ui_normal_14.png", "ui_push_14.png", "ui_s3.png"},
        {"ui_normal_16.png", "ui_push_16.png", "ui_s4.png"},
        {"ui_normal_18.png", "ui_push_18.png", "ui_wang2.png"}
    }
    self.m_bNodeMenuMoving = false
    self.m_aniList = {}
    self:initView()
    self:initMsgEvent()
    --ToolKit:registDistructor(self, handler(self, self.onDestory))
    UIAdapter:setIsTouches(true)
end

function FiveStarsLayer:onDestory()
    self:unMsgEvent()
    if (self.m_handlerUpdateClock) then
        scheduler.unscheduleGlobal(self.m_handlerUpdateClock)
    end
    self.m_handlerUpdateClock = nil

    -- for _k, _handler in pairs(self.m_handlers) do
    --     scheduler.unscheduleGlobal(_handler)
    -- end
    -- self.m_handlers = {}
    self.m_pCardView:onDestory()
    self.m_pCardView:removeFromParent()
    self.m_pCardView = nil
    UIAdapter:setIsTouches(false)
end

function FiveStarsLayer:initView()
    local newNode = CacheManager:addCSBTo(self, FiveStarsRes.CSB.MAIN_UI)
    self.m_pRoot = newNode:getChildByName("root")
    UIAdapter:adapter(self, nil, false)
    newNode:setPosition(cc.p(display.width / 2, display.height / 2))
    newNode:setAnchorPoint(cc.p(0.5, 0.5))

    local top_panel = self.m_pRoot:getChildByName("top_panel")
    local left_panel = self.m_pRoot:getChildByName("left_panel")
    local below_panel = self.m_pRoot:getChildByName("below_panel")
    local bet_type_node = below_panel:getChildByName("bet_type_node")
    local history_node = top_panel:getChildByName("history_node")
    local card = top_panel:getChildByName("card")

    self.mTextRecord = UIAdapter:CreateRecord(nil, 14)
    top_panel:addChild(self.mTextRecord)
    self.mTextRecord:setAnchorPoint(cc.p(0.5, 0))
    self.mTextRecord:setOpacity(50)
	local cardSize = card:getContentSize()
	local cardPoint = cc.p(card:getPosition())
    self.mTextRecord:setPosition(cc.p(cardPoint.x + cardSize.width / 2, cardPoint.y + cardSize.height + 1))
    
    self.m_pCardView = FiveStarsCardView.new(card)
    self:addChild(self.m_pCardView)

    self.m_pTextClockTime = self.m_pRoot:getChildByName("text_clock_time")
    self.m_pTextClockTime:setVisible(false)

    local winText = below_panel:getChildByName("Text_1")
    -- winText:setPositionX(17)
    winText:enableOutline(cc.c4b(110, 105, 73, 180), 2)

    self.m_pTextAllBetCoin = below_panel:getChildByName("text_other_bet_number")
    self.m_pTextAllBetCoin:setString("")
    -- self.m_pTextAllBetCoin:setPositionX(winText:getPositionX() + winText:getContentSize().width * winText:getScaleX() + 26)

    -- local _coin = self.m_pTextAllBetCoin:clone()
    -- below_panel:addChild(_coin)
    -- _coin:setPosition((self.m_pTextAllBetCoin:getPosition()))
    -- _coin:setString("888888")
    -- _coin:setColor(cc.c3b(64,64,64))
    -- _coin:setOpacity(100)


    self.m_pTextUserName = below_panel:getChildByName("text_user_name")
    self.m_pTextUserCoin = below_panel:getChildByName("text_user_coin")
    
    self.m_pTextUserCoin:setString(math.floor( Player:getGoldCoin() or 0 ))
    self.m_pTextUserName:enableOutline(cc.c4b(0xa1,0x8d,0x5c, 0xff), 2)
    self.m_pTextUserName:setString(Player:getNickName())
    --self.m_pTextUserName:setFontSize(21)
    -- self.m_pTextUserName:setPositionX(17)
    
    self.m_pTextSelfWinNumber = below_panel:getChildByName("self_win_number")
    self.m_pTextSelfLoseNumber = below_panel:getChildByName("self_lose_number")
    self.m_pTextSelfWinNumber:setFntFile("common/font/num_win.fnt")
    self.m_pTextSelfLoseNumber:setFntFile("common/font/num_lose.fnt")
    self.m_pTextSelfWinNumber:setVisible(false)
    self.m_pTextSelfLoseNumber:setVisible(false)
    

    self.m_pTextHistoryNumbers = {}
    for index = 1, FiveStarsConfig.CONST_AREA_COUNT do
        -- self.m_pTextHistoryNumbers[index] = history_node:getChildByName("text_history_nunber_" .. index)
        -- self.m_pTextHistoryNumbers[index]:setString("00")
        local _text = history_node:getChildByName("text_history_nunber_" .. index)
        local textLable = display.newTTFLabel({text = "00", font = "ttf/jcy.TTF", size = 24, color = cc.c3b(255,255,255)})
        history_node:addChild(textLable)
        textLable:setAnchorPoint(cc.p(_text:getAnchorPoint()))
        textLable:setPosition(cc.p(_text:getPosition()))
        self.m_pTextHistoryNumbers[index] = textLable
        _text:setVisible(false)
    end
    self.m_pTextHistoryTips = history_node:getChildByName("text_history_tips")
    self.m_pTextWheel = history_node:getChildByName("text_wheel")
    self.m_pTextRound = history_node:getChildByName("text_round")
    self.m_pTextWheel:setString("1场")
    self.m_pTextRound:setString("1局")
    self.m_pTextWheel:enableOutline(cc.c4b(96, 55, 33, 255), 1)
    self.m_pTextRound:enableOutline(cc.c4b(96, 55, 33, 255), 1)
    self.m_pTextHistoryTips:enableOutline(cc.c4b(96, 55, 33, 255), 1)
    local pTextGameName = history_node:getChildByName("game_name")
    pTextGameName:enableOutline(cc.c4b(96, 55, 33, 255), 1)

    self.m_pMHistoryListView = history_node:getChildByName("history_list_m")
    self.m_pLHistoryListView = history_node:getChildByName("history_list_L")
    self.m_pMHistoryListView:setTouchEnabled(false)

    local listSize = self.m_pMHistoryListView:getContentSize()
    local shap = cc.DrawNode:create()
    -- local pointArr = {cc.p(444,0), cc.p(665, 0), cc.p(665, 30), cc.p(444, 30)}
    local pointArr = { cc.p(442, 31), cc.p(listSize.width, 31), cc.p(listSize.width, listSize.height), cc.p(0, listSize.height), cc.p(0, 0), cc.p(442, 0) }

    shap:drawPolygon(pointArr, #pointArr, cc.c4f(255, 255, 255, 255), 2, cc.c4f(255, 255, 255, 255))

    self.m_pMClippingView = cc.ClippingNode:create(shap)
    history_node:addChild(self.m_pMClippingView)
    self.m_pMClippingView:setContentSize(self.m_pMHistoryListView:getContentSize())
    self.m_pMClippingView:setAnchorPoint(cc.p(0, 0))
    self.m_pMClippingView:setPosition(cc.p(self.m_pMHistoryListView:getPosition()))
    self.m_pMClippingView:setInverted(false)
    --self.m_pMClippingView:setAlphaThreshold(0.1)
    UIAdapter:replaceParent(self.m_pMHistoryListView, self.m_pMClippingView)
    self.m_pMHistoryListView:setPosition(cc.p(0, 0))
    self.m_pTextSelfAreaCoin = {}
    for index = 1, FiveStarsConfig.CONST_AREA_COUNT do
        self.m_pTextSelfAreaCoin[index] = below_panel:getChildByName("text_bet_number_" .. index)
        self.m_pTextSelfAreaCoin[index]:setString("0")
    end

    self.m_pImageBetSelectBack = bet_type_node:getChildByName("image_sel_bet_back")
    self.m_pTextBetValues = {}
    for index = 1, FiveStarsConfig.CONST_BET_COUNT do
        self.m_pTextBetValues[index] = bet_type_node:getChildByName("text_bet_type_" .. index)
        --self.m_pTextBetValues[index]:setVisible(false)
        self.m_pTextBetValues[index]:enableOutline(cc.c4b(96, 55, 33, 255), 1)
        self.m_pTextBetValues[index]:setTag(index)
        self:_RegistClickCallBack(self.m_pTextBetValues[index], handler(self, self.onSelectBetClick))
    end

    self.m_pAwardView = self.m_pRoot:getChildByName("AwardView")
    self.m_pAwardView:setVisible(false)

    local menuRoot = self.m_pRoot:getChildByName("menu")
    self.m_pMenuPanle = menuRoot:getChildByName("back")
    self.m_pMenuPanle:setVisible(false)

    --注册按钮事件

    self.m_pMenuBack = self.m_pRoot:getChildByName("top_button_back")
    self.m_pMenuBack:setVisible(false)
    
    self.m_pMenuPush = left_panel:getChildByName("top_button")
    self.m_pMenuPop = left_panel:getChildByName("dwn_button")
    local setting_button = self.m_pMenuPanle:getChildByName("setting_button")
    local record_button = self.m_pMenuPanle:getChildByName("zhanji_button")
    local help_button = self.m_pMenuPanle:getChildByName("guize_button")

    self.m_pMenuPop:setVisible(true)
    self.m_pMenuPush:setVisible(false)

    self:_RegistClickCallBack(setting_button, handler(self, self.onSettingClick), true)
    self:_RegistClickCallBack(record_button, handler(self, self.onRecordClick), true)
    self:_RegistClickCallBack(help_button, handler(self, self.onHelpClick), true)

    self:_RegistClickCallBack(self.m_pMenuBack, handler(self, self.onSettingPushClick), true)
    self:_RegistClickCallBack(self.m_pMenuPush, handler(self, self.onSettingPushClick), true)
    self:_RegistClickCallBack(self.m_pMenuPop, handler(self, self.onSettingPopClick), true)

    --下注区域按钮注册
    self.m_pAreaButtons = {}
    for index = 1, FiveStarsConfig.CONST_AREA_COUNT do
        local bet_button = below_panel:getChildByName("bet_button_" .. index)

        local area_button = MyButton.new(self.m_AreaButtonImage[index][1], self.m_AreaButtonImage[index][2], self.m_AreaButtonImage[index][1], ccui.TextureResType.plistType)
        below_panel:addChild(area_button)
        area_button:setTag(index)
        area_button:setPosition(cc.p(bet_button:getPosition()))
        area_button:setTouchEnabled(true)
        bet_button:setVisible(false)
        self.m_pAreaButtons[index] = area_button
        area_button:setIsContinue(true)
        area_button:addTouchEventListener(function(_sender, _eventType)
            if _eventType == MyButton.TOUCH_TYPE_CLICK then
                self:_PlayBetSound(true)
                self:onAreaClick(_sender)
            elseif _eventType == MyButton.TOUCH_TYPE_CONTINUE then
                if (self.m_nClockTime > 0.5 and FiveStarsData.getInstance().m_nGameState == FiveStarsConfig.STATE_BET) then
                    self:_PlayBetSound(false)
                    self:onAreaClick(_sender)
                end
            end
        end)
    end

    self.m_pBetCancelButton = below_panel:getChildByName("bet_cancel_button")
    self:_RegistClickCallBack(self.m_pBetCancelButton, handler(self, self.onBetCancelClick), false, FiveStarsRes.Audio.CANCEL_BET)
    self.m_pBetSwitchButton = below_panel:getChildByName("bet_type_button")
    self:_RegistClickCallBack(self.m_pBetSwitchButton, handler(self, self.onBetSwitchClick), false, FiveStarsRes.Audio.BET)

    local return_button = left_panel:getChildByName("return_button")
    
    self.m_pMHistoryButton = left_panel:getChildByName("min_button")
    self.m_pLHistoryButton = left_panel:getChildByName("l_button")

    self:_RegistClickCallBack(return_button, handler(self, self.onReturnClick), true)
    self:_RegistClickCallBack(self.m_pMHistoryButton, handler(self, self.onHistoryViewMClick), true)
    self:_RegistClickCallBack(self.m_pLHistoryButton, handler(self, self.onHistoryViewLClick), true)
    local btn_chat = left_panel:getChildByName("btn_chat")
    self:_RegistClickCallBack(btn_chat, handler(self, self.onChatClick), true)
    self:onHistoryViewLClick(self.m_pLHistoryButton)
    -- self:onHistoryViewMClick(self.m_pMHistoryButton)
    -- self:_SetHistory(nil)
    self:_RefreshButtonState()
    self:_SetSelectBet(2)

    --初始化动画
    -- self.m_aniCancel = UIAdapter:CreateRecord(nil, 52, nil, cc.c4b(0x6d,0x4b,0x08,0xff), 3)
    -- self.m_aniCancel:setString("最后三秒将不能取消下注")
    -- self.m_aniCancel:setAnchorPoint(cc.p(0.5,0.5))
    -- self.m_pRoot:addChild(self.m_aniCancel)
    -- self.m_aniCancel:setPosition(cc.p(667,335))

    self.m_aniStart = CacheManager:addSpineTo(self.m_pRoot, "FiveStars/effect/kaishixiazhu_kaijiang/kaishixiazhu_kaijiang", 0, ".json")
    self.m_aniStart:setVisible(false)
    self.m_aniStart:setPosition(cc.p(667, 410))

    self.m_pBetMaxText = display.newTTFLabel({ text = "限红:\n花色最低20分\n最高12000分\n王最高600分\n", font = UIAdapter.TTF_FZCYJ, size = 17, color = cc.c3b(0xFF,0xFF,0xFF)})
    self.m_pRoot:addChild(self.m_pBetMaxText)
    self.m_pBetMaxText:setAnchorPoint(cc.p(0,0))
    self.m_pBetMaxText:setPosition(cc.p(self.m_pLHistoryButton:getPositionX() - 48, 220))
end

function FiveStarsLayer:initMsgEvent()
    addMsgCallBack(self, FiveStarsEvent.MSG_GAME_INIT, handler(self, self.ON_GAME_INIT))
    addMsgCallBack(self, FiveStarsEvent.MSG_BET_ACK, handler(self, self.ON_BET_ACK))
    addMsgCallBack(self, FiveStarsEvent.MSG_SELF_BET_SUM_NTY, handler(self, self.ON_SELF_BET_SUM_NTY))
    addMsgCallBack(self, FiveStarsEvent.MSG_BET_SUM_NTY, handler(self, self.ON_BET_SUM_NTY))
    addMsgCallBack(self, FiveStarsEvent.MSG_GAME_BALANCE_NTY, handler(self, self.ON_GAME_BALANCE_NTY))
    addMsgCallBack(self, FiveStarsEvent.MSG_GAME_DT_NUMBER_NTY, handler(self, self.ON_GAME_DT_NUMBER_NTY))
    addMsgCallBack(self, FiveStarsEvent.MSG_GAME_STATE, handler(self, self.ON_GAME_STATE))
    addMsgCallBack(self, FiveStarsEvent.MSG_HISTORY_HTY, handler(self, self.ON_HISTORY_HTY))
    addMsgCallBack(self, FiveStarsEvent.MSG_CANCEL_ACK, handler(self, self.ON_CANCEL_ACK))
    addMsgCallBack(self, FiveStarsEvent.MSGONLINE_PLAYER_LIST_ACK, handler(self, self.ONONLINE_PLAYER_LIST_ACK))
    addMsgCallBack(self, FiveStarsEvent.MSG_SELF_WIN_GOLD, handler(self, self.ON_SELF_WIN_GOLD))
    addMsgCallBack(self, FiveStarsEvent.MSG_SELF_COIN, handler(self, self.ON_SELF_COIN))
    addMsgCallBack(self, FiveStarsEvent.MSG_PLAYER_CUOPAI, handler(self, self.ON_PLAYER_CUOPAI))
    addMsgCallBack(self, FiveStarsEvent.MSG_CUOPAI_START_NTY, handler(self, self.ON_CUOPAI_START_NTY))
    addMsgCallBack(self, FiveStarsEvent.MSGONLINE_PLAYER_COUNT, handler(self, self.ONONLINE_PLAYER_COUNT))
    addMsgCallBack(self, FiveStarsEvent.MSG_TOP_PLAYER_LIST, handler(self, self.ON_TOP_PLAYER_LIST))
    addMsgCallBack(self, FiveStarsEvent.MSG_CHIP_VALUES, handler(self, self.ON_CHIP_VALUES))
    addMsgCallBack(self, FiveStarsEvent.MSG_CONTINUE_BET, handler(self, self.ON_CONTINUE_BET))
    -- addMsgCallBack(self, FiveStarsEvent.MSG_DISTORY, handler(self, self.onDestory))

end

function FiveStarsLayer:unMsgEvent()
    removeMsgCallBack(self, FiveStarsEvent.MSG_GAME_INIT)
    removeMsgCallBack(self, FiveStarsEvent.MSG_BET_ACK)
    removeMsgCallBack(self, FiveStarsEvent.MSG_SELF_BET_SUM_NTY)
    removeMsgCallBack(self, FiveStarsEvent.MSG_BET_SUM_NTY)
    removeMsgCallBack(self, FiveStarsEvent.MSG_GAME_BALANCE_NTY)
    removeMsgCallBack(self, FiveStarsEvent.MSG_GAME_DT_NUMBER_NTY)
    removeMsgCallBack(self, FiveStarsEvent.MSG_GAME_STATE)
    removeMsgCallBack(self, FiveStarsEvent.MSG_HISTORY_HTY)
    removeMsgCallBack(self, FiveStarsEvent.MSG_CANCEL_ACK)
    removeMsgCallBack(self, FiveStarsEvent.MSGONLINE_PLAYER_LIST_ACK)
    removeMsgCallBack(self, FiveStarsEvent.MSG_SELF_WIN_GOLD)
    removeMsgCallBack(self, FiveStarsEvent.MSG_SELF_COIN)
    removeMsgCallBack(self, FiveStarsEvent.MSG_PLAYER_CUOPAI)
    removeMsgCallBack(self, FiveStarsEvent.MSG_CUOPAI_START_NTY)
    removeMsgCallBack(self, FiveStarsEvent.MSGONLINE_PLAYER_COUNT)
    removeMsgCallBack(self, FiveStarsEvent.MSG_TOP_PLAYER_LIST)
    removeMsgCallBack(self, FiveStarsEvent.MSG_CHIP_VALUES)
    removeMsgCallBack(self, FiveStarsEvent.MSG_CONTINUE_BET)
    -- removeMsgCallBack(self, FiveStarsEvent.MSG_DISTORY)
end

--region 按钮事件
---下注按钮
function FiveStarsLayer:onAreaClick(target)
    local pData = FiveStarsData.getInstance()
    local nArea = target:getTag()
    if (self.m_nSelectBet <= 0 or self.m_nSelectBet > #pData.m_pBetValues) then
        TOAST("请先选择下注金额！")
        target:StopContinue()
        return
    end

    if (pData.m_nCurrentCoin < pData.m_pBetValues[self.m_nSelectBet]) then
        TOAST("您的金币不足！")
        target:StopContinue()
        return
    end

    if (pData.m_nGameState ~= FiveStarsConfig.STATE_BET) then
        TOAST("请在下注阶段下注！")
        target:StopContinue()
        return
    end
    print("\n--------send--- CS_C2G_DanTiao_Bet_Req:{"..nArea..", "..pData.m_pBetValues[self.m_nSelectBet]..","..Player:getAccountID().."}------------\n")
    ConnectManager:send2GameServer(
        g_GameController:getGameAtomTypeId(),
        "CS_C2G_DanTiao_Bet_Req",
        {
            nArea,
            pData.m_pBetValues[self.m_nSelectBet],
            Player:getAccountID()
        }
    )
end

---取消下注
function FiveStarsLayer:onBetCancelClick(target)
    local pData = FiveStarsData.getInstance()
    if (not pData:isCurrentBet()) then
        TOAST("你没有下注！")
        return
    end
    ConnectManager:send2GameServer(g_GameController.m_gameAtomTypeId, "CS_C2G_DanTiao_Bet_Cancel_Req", { Player:getAccountID() })

end

---切换下注筹码
function FiveStarsLayer:onBetSwitchClick(target)
    if self.m_nBetTypeCount > 1 then
        self:_SetSelectBet(self.m_nSelectBet % self.m_nBetTypeCount + 1)
    else
        self:_SetSelectBet(1)
    end
end

function FiveStarsLayer:onSelectBetClick(target)
    self:_SetSelectBet(target:getTag())
end 
function FiveStarsLayer:onChatClick(target)
    local ChatLayer = ChatLayer.new()
        self:addChild(ChatLayer);
end
---返回游戏大厅
function FiveStarsLayer:onReturnClick(target)
    local pData = FiveStarsData.getInstance()
    if (pData:isCurrentBet() and pData.m_nGameState ~= FiveStarsConfig.STATE_AWARD) then
        TOAST("游戏中无法退出！")
        return
    end
    self:getParent():StrongbackGame()
end

---设置
function FiveStarsLayer:onSettingClick(target)
    local layer = GameSetLayer.new(141);
    self:addChild(layer);
    layer:setScale(1 / display.scaleX, 1 / display.scaleY);
end

---显示小路单
function FiveStarsLayer:onHistoryViewMClick(target)
    self.m_pMClippingView:setVisible(true)
    self.m_pLHistoryListView:setVisible(false)
    self.m_pMHistoryButton:setVisible(false);
    self.m_pLHistoryButton:setVisible(true);
    self.m_pTextHistoryTips:setVisible(false);
end

---显示大路单
function FiveStarsLayer:onHistoryViewLClick(target)
    self.m_pMClippingView:setVisible(false)
    self.m_pLHistoryListView:setVisible(true)
    self.m_pMHistoryButton:setVisible(true);
    self.m_pLHistoryButton:setVisible(false);
    self.m_pTextHistoryTips:setVisible(true);
end

---续投
function FiveStarsLayer:onContinueBet(target)
    if (self.m_bIsContinue) then
        TOAST("你已经续投过了！")
        return
    end
    local pData = FiveStarsData.getInstance()
    if (not pData:isLastBet()) then
        TOAST("你没有下注过！")
        return
    end

    self.m_pContinueButton:setEnabled(false)
    self:_DoSomethingLater(function()
        if (not self.m_bIsContinue) then
            self.m_pContinueButton:setEnabled(true)
        end
    end, 0.1)

    local chips = self:_ChangeChips(pData.m_pBetValues)
    ConnectManager:send2GameServer(g_GameController.m_gameAtomTypeId, "CS_C2G_DanTiao_BetContinue_Req", { chips })
end

---规则
function FiveStarsLayer:onHelpClick(target)
    if self:getChildByName("FivestarsHelpLayer") then
        return
    end
    local helpView = FivestarsHelpLayer.new()
    helpView:setName("FivestarsHelpLayer")
    self:addChild(helpView, 1)
end

---战绩
function FiveStarsLayer:onRecordClick(target)
    local layer = GameRecordLayer.new(2, 141)
    self:addChild(layer, 1)
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", { 141 })
end

function FiveStarsLayer:onSettingPushClick(target)
    if self.m_bNodeMenuMoving then return end
    self.m_bNodeMenuMoving = true

    local callback = cc.CallFunc:create(function()
        self.m_pMenuPop:setVisible(true)
        self.m_pMenuPush:setVisible(false)
    end)

    local callback2 = cc.CallFunc:create(function()
        self.m_bNodeMenuMoving = false
    end)
    self.m_pMenuBack:setVisible(false)
    self.m_pMenuPanle:stopAllActions()
    local aTime = 0.25
    local moveTo = cc.MoveTo:create(aTime, cc.p(0, 30))
    local sp = cc.Spawn:create(cc.EaseBackIn:create(moveTo), cc.FadeOut:create(aTime))
    local hide = cc.Hide:create()
    local seq = cc.Sequence:create(sp, callback, hide, cc.DelayTime:create(0.1), callback2, nil)
    self.m_pMenuPanle:runAction(seq)
end

function FiveStarsLayer:onSettingPopClick(target)
    if self.m_bNodeMenuMoving then return end
    self.m_bNodeMenuMoving = true

    self.m_pMenuBack:setVisible(true)

    self.m_pMenuPanle:setVisible(true)
    self.m_pMenuPanle:setPosition(cc.p(0, 30))

    local callback = cc.CallFunc:create(function()
        self.m_pMenuPop:setVisible(false)
        self.m_pMenuPush:setVisible(true)
    end)

    local callback2 = cc.CallFunc:create(function()
        self.m_bNodeMenuMoving = false
    end)

    self.m_pMenuPanle:stopAllActions()
    self.m_pMenuPanle:setOpacity(0)
    local aTime = 0.25
    local moveTo = cc.MoveTo:create(aTime, cc.p(0, 0))
    local show = cc.Show:create()
    local sp = cc.Spawn:create(cc.EaseBackOut:create(moveTo), cc.FadeIn:create(aTime))
    local seq = cc.Sequence:create(show, sp, callback, cc.DelayTime:create(0.1), callback2)
    self.m_pMenuPanle:runAction(seq)
end

--endregion
--region 私有函数

function FiveStarsLayer:_RegistClickCallBack(_btn, _handler, _isNeedScale, _soundFile)
    if _btn == nil or _handler == nil then
        print("get nil btn or handler when registClickCallBack")
        return
    end
    local soundFile = _soundFile or ToolKit:getCurrentScene():getSoundPath()
    local isNeedScale = _isNeedScale or false
    local actionScale = 1.1
    if _btn.addTouchEventListener == nil then
        -- print("btn is not click type ui !")
        return
    end
    local sendButtonHandler = function (sender)
        if sender:getDescription() == "Button" then
            g_AudioPlayer:playEffect(soundFile) -- play effect sound
        end
        _handler(sender)
    end

    local setButtonScale = function (sender, scale)
        if (isNeedScale) then
            sender:setScale(scale)
        end
    end


    _btn:addTouchEventListener(function(sender, eventType)
        if sender then
            if eventType == ccui.TouchEventType.began then
                setButtonScale(sender, actionScale)
            elseif eventType == ccui.TouchEventType.moved then
                if (sender:isHighlighted()) then
                    setButtonScale(sender, actionScale)
                else
                    setButtonScale(sender, 1)
                end
            elseif eventType == ccui.TouchEventType.canceled then
                setButtonScale(sender, 1)
            elseif eventType == ccui.TouchEventType.ended then
                setButtonScale(sender, 1)
                sendButtonHandler(sender)
            end
        end
    end)
end

function FiveStarsLayer:_PlayBetSound(isPlay)
    if not self.m_pBetSoundTime or isPlay then
        self.m_pBetSoundTime = os.clock()
        g_AudioPlayer:playEffect(FiveStarsRes.Audio.BET)
    else
        local clock = os.clock()
        if (clock - self.m_pBetSoundTime > 0.1) then
            self.m_pBetSoundTime = clock
            g_AudioPlayer:playEffect(FiveStarsRes.Audio.BET)
        end
    end
end

function FiveStarsLayer:_SetSelectBet(index)
    self.m_nSelectBet = index
    local text = self.m_pTextBetValues[self.m_nSelectBet]
    local textSize = text:getContentSize()
    self.m_pImageBetSelectBack:setPosition(cc.p(text:getPosition()))
    self.m_pImageBetSelectBack:setContentSize({width = textSize.width + 20, height = textSize.height})
end

function FiveStarsLayer:_ChangeChips(_bets)
    local pData = FiveStarsData.getInstance()
    local getMinValue = function(coin)
        for index = #pData.m_pBetValues, 1, -1 do
            local value = pData.m_pBetValues[index]
            if (coin >= value) then
                return value
            end
        end
        return nil
    end

    local chips = {}

    for index = 1, #_bets do
        local maxCoin = _bets[index]
        if (0 < maxCoin) then
            local coin = getMinValue(maxCoin)
            while (coin) do
                table.insert(chips, { index, coin })
                maxCoin = maxCoin - coin
                coin = getMinValue(maxCoin)
            end
        end
    end

    return chips
end

---设置历史记录
function FiveStarsLayer:_SetHistory(info)
    if (info == nil) then
        return
    end

    local history = { 0, 0, 0, 0, 0 }
    history[1] = info.m_historyAwardCnt.m_spadeCnt
    history[2] = info.m_historyAwardCnt.m_heartCnt
    history[3] = info.m_historyAwardCnt.m_clubCnt
    history[4] = info.m_historyAwardCnt.m_diamondCnt
    history[5] = info.m_historyAwardCnt.m_kingCnt
    for index = 1, 5 do
        self.m_pTextHistoryNumbers[index]:setString(string.format( "%02d", history[index] ))
    end


    local container_m = self.m_pMHistoryListView:getInnerContainer()
    container_m:removeAllChildren()
    local container_l = self.m_pLHistoryListView:getInnerContainer()
    container_l:removeAllChildren()
    container_m:setAnchorPoint(cc.p(0, 0))
    container_l:setAnchorPoint(cc.p(0, 0))
    local size_m = container_m:getContentSize()
    local size_l = container_l:getContentSize()

    local listSize_m = self.m_pMHistoryListView:getContentSize()
    local listSize_l = self.m_pLHistoryListView:getContentSize()

    local h = math.floor(#info.m_vecBallHistory / 6)
    local w = 6

    if (#info.m_vecBallHistory % 6 > 0) then
        h = h + 1
    end

    size_m.height = math.max( h * 31, listSize_m.height)
    size_l.height = math.max( h * 66, listSize_l.height)
    
    container_m:setContentSize(size_m)
    container_l:setContentSize(size_l)
    
    -- container_m:setPosition(cc.p(0, listSize_m.height - size_m.height))
    -- container_l:setPosition(cc.p(0, listSize_l.height - size_l.height))

    container_m:setPosition(cc.p(0, 0))
    container_l:setPosition(cc.p(0, 0))

    local index = 0
    -- for i = #info.m_vecBallHistory, 1, -1 do
    for i = 1, #info.m_vecBallHistory do
        local pokerId = info.m_vecBallHistory[i]
        local icon_m = ccui.ImageView:create(string.format("history_0_0x%02X.png", pokerId), ccui.TextureResType.plistType)
        local icon_l = ccui.ImageView:create(string.format("history_1_0x%02X.png", pokerId), ccui.TextureResType.plistType)
        container_m:addChild(icon_m)
        container_l:addChild(icon_l)
        icon_m:setAnchorPoint(cc.p(0, 0))
        icon_l:setAnchorPoint(cc.p(0, 0))

        local x = index % 6 * 111
        local y_m = math.floor(index / 6) * 31 + 29
        local y_l = math.floor(index / 6) * 66 + 63

        icon_m:setPosition(cc.p(x, size_m.height - y_m))
        icon_l:setPosition(cc.p(x, size_l.height - y_l))
        index = index + 1
    end

end

---更新按钮状态
function FiveStarsLayer:_RefreshButtonState()
    local pData = FiveStarsData.getInstance()
    local betTypeCount = 0
    
    if (pData.m_nGameState == FiveStarsConfig.STATE_BET) then
        self.m_pBetSwitchButton:setEnabled(true)
        self.m_pBetSwitchButton:setColor(cc.c3b(255, 255, 255))
        for index = #self.m_pTextBetValues, 1, -1 do
            if ((index == 1) or (pData.m_pBetValues[index] and pData.m_nCurrentCoin >= pData.m_pBetValues[index])) then
                self.m_pTextBetValues[index]:setEnabled(true)
                self.m_pTextBetValues[index]:setColor(cc.c3b(255, 255, 255))
                if (0 == self.m_nSelectBet) then
                    self:_SetSelectBet(index)
                end
                if (0 == betTypeCount) then
                    betTypeCount = index
                end
            else
                self.m_pTextBetValues[index]:setEnabled(false)
                self.m_pTextBetValues[index]:setColor(cc.c3b(160, 160, 160))
                if (self.m_nSelectBet == index) then
                    self.m_nSelectBet = 0
                end
            end
        end
        self.m_nBetTypeCount = math.max( betTypeCount, 1 )
        for index = 1, FiveStarsConfig.CONST_AREA_COUNT do
            self.m_pAreaButtons[index]:setTouchEnabled(true)
            -- self.m_pAreaButtons[index]:setSwallowTouches(false)
            self.m_pAreaButtons[index]:setColor(cc.c3b(255, 255, 255))
        end

        if (self.m_nClockTime > 3 and pData:isCurrentBet()) then
            self.m_pBetCancelButton:setEnabled(true)
            self.m_pBetCancelButton:setColor(cc.c3b(255, 255, 255))
        else
            self.m_pBetCancelButton:setEnabled(false)
            self.m_pBetCancelButton:setColor(cc.c3b(160, 160, 160))
        end

        -- if (not self.m_bIsContinue) then
        --     self.m_pContinueButton:setEnabled(true)
        --     self.m_pContinueButton:setColor(cc.c3b(255, 255, 255))
        -- else
        --     self.m_pContinueButton:setEnabled(false)
        --     self.m_pContinueButton:setColor(cc.c3b(160, 160, 160))
        -- end
    else
        -- self.m_pContinueButton:setEnabled(false)
        -- self.m_pContinueButton:setColor(cc.c3b(160, 160, 160))
        self.m_pBetSwitchButton:setEnabled(false)
        self.m_pBetSwitchButton:setColor(cc.c3b(160, 160, 160))

        self.m_pBetCancelButton:setEnabled(false)
        self.m_pBetCancelButton:setColor(cc.c3b(160, 160, 160))

        for index = #self.m_pTextBetValues, 1, -1 do
            self.m_pTextBetValues[index]:setEnabled(false)
            self.m_pTextBetValues[index]:setColor(cc.c3b(160, 160, 160))
        end
        for index = 1, FiveStarsConfig.CONST_AREA_COUNT do
            self.m_pAreaButtons[index]:setTouchEnabled(false)
            -- self.m_pAreaButtons[index]:loadTexture(self.m_AreaButtonImage[index][1], ccui.TextureResType.plistType)
            self.m_pAreaButtons[index]:setColor(cc.c3b(160, 160, 160))
            -- self:_UnButtonScheduleHandler(self.m_pAreaButtons[index])
        end
    end
end

function FiveStarsLayer:_ShowStateTime(time, isVisible)
    if (time > 0) then
        self.m_nClockTime = math.floor(time)
        self.m_pTextClockTime:setVisible(isVisible)
        self.m_pTextClockTime:setString(string.format("%02d", self.m_nClockTime))
        -- g_AudioPlayer:playEffect(FiveStarsRes.Audio.CLOCK_TIME, false)
        if (self.m_handlerUpdateClock) then
            scheduler.unscheduleGlobal(self.m_handlerUpdateClock)
        end
        self.m_handlerUpdateClock = scheduler.scheduleGlobal(handler(self, self._UpdateStateTime), 1)
    else
        self.m_pTextClockTime:setVisible(false)
        if (self.m_handlerUpdateClock) then
            scheduler.unscheduleGlobal(self.m_handlerUpdateClock)
        end
        self.m_handlerUpdateClock = nil
    end

end

function FiveStarsLayer:_UpdateStateTime(dt)
    self.m_nClockTime = math.max(0, self.m_nClockTime - 1)
    self.m_pTextClockTime:setString(string.format("%02d", self.m_nClockTime))
    if (0 >= self.m_nClockTime) then
        -- self.m_pTextClockTime:setVisible(false)
        scheduler.unscheduleGlobal(self.m_handlerUpdateClock)
        self.m_handlerUpdateClock = nil
    else
        local pData = FiveStarsData.getInstance()
        if (pData and pData.m_nGameState == FiveStarsConfig.STATE_BET) then
            if (self.m_nClockTime == 3) then
                g_AudioPlayer:playEffect(FiveStarsRes.Audio.CLOCK_TIME, false)
                self:_RefreshButtonState()
            elseif (self.m_nClockTime == 10) then
                g_AudioPlayer:playEffect(FiveStarsRes.Audio.LAST10, false)
            end
        end
    end
end

function FiveStarsLayer:_DoSomethingLater(call, delay)
    self:runAction(cca.seq({ cca.delay(delay), cca.callFunc(call) }))
end

function FiveStarsLayer:_SetBetValues(values)
    for index = 1, #self.m_pTextBetValues do
        if (index <= #values) then
            self.m_pTextBetValues[index]:setVisible(true)
            self.m_pTextBetValues[index]:setString(values[index] * 0.01)
        else
            self.m_pTextBetValues[index]:setVisible(false)
        end
    end
end

function FiveStarsLayer:_ClearDesk()
    for index = 1, FiveStarsConfig.CONST_AREA_COUNT do
        self.m_pTextSelfAreaCoin[index]:stopAllActions()
        self.m_pTextSelfAreaCoin[index]:setOpacity(255)
        self.m_pTextSelfAreaCoin[index]:setString("0")
    end
    self.m_pTextAllBetCoin:stopAllActions()
    self.m_pTextAllBetCoin:setOpacity(255)
    self.m_pTextAllBetCoin:setString("")
end

function FiveStarsLayer:_RefreshAllBetCoin()
    -- local gold = FiveStarsData.getInstance():getSelfToalGold()
    -- self.m_pTextAllBetCoin:setString(gold * 0.01)
    local currentBets = FiveStarsData.getInstance():getCurrentBets()
    for index = 1, FiveStarsConfig.CONST_AREA_COUNT do
        local str = string.format( "%d", currentBets[index] / 100 )
        self.m_pTextSelfAreaCoin[index]:setString( str )
    end
end

function FiveStarsLayer:_RefreshCurrentCoin()
    local pData = FiveStarsData.getInstance()
    self.m_pTextUserCoin:setString( string.format( "%d", pData.m_nCurrentCoin/100) )
end

function FiveStarsLayer:_ShowAwardView(info, top3)
    self.m_pAwardView:setVisible(true)
    self.m_pAwardView:setPosition(cc.p(1500, 375))
    
    if self.m_pAwardView.m_pTileAnima == nil then
        self.m_pAwardView.m_pTileAnima = CacheManager:addSpineTo(self.m_pAwardView, "effect/yaoyiyaojiesuan_donghua_new/yaoyiyaojiesuan_donghua_new", 0, ".skel")
        self.m_pAwardView.m_pTileAnima:setAnchorPoint(cc.p(0,0))
    end

    local pBg = self.m_pAwardView:getChildByName("award_bg")
    local pMyNode = self.m_pAwardView:getChildByName("Node_0")
    local pNoBet = self.m_pAwardView:getChildByName("no_bet")
    -- local pCloseButton = self.m_pAwardView:getChildByName("award_view_button")

    local setTop3Info = function(rank, topInfo)
        -- { 1,	1, 'm_order'		 	 ,  'UINT'				, 1		, '名次'},
        -- { 2,	1, 'm_nickname'		 	 ,  'STRING'			, 1		, '昵称'},
        -- { 3,	1, 'm_profit'		 	 ,  'INT'				, 1		, '盈利'},
        local pRankNode = self.m_pAwardView:getChildByName(string.format( "Node_%d",rank ))
        if topInfo == nil then
            pRankNode:setVisible(false)
            return
        end
        pRankNode:setVisible(true)
        local rank_icon = pRankNode:getChildByName("rank_icon")
        local name_text = pRankNode:getChildByName("name_text")
        local coin_text = pRankNode:getChildByName("coin_text")

        rank_icon:loadTexture(string.format( "hall/image/rank/tjylc_phb_hg%d.png",rank ))
        name_text:setString(topInfo.m_nickname)
        coin_text:setString(topInfo.m_profit * 0.01)

    end

    for i = 1, 3 do
        if top3 == nil then
            setTop3Info(i, nil)
        else
            local item = top3[i]
            setTop3Info(item.m_order, item)
        end
    end

    if info ~= nil then
        pMyNode:setVisible(true)
        pNoBet:setVisible(false)
        local text_self_name = pMyNode:getChildByName("text_self_name");
        local pWinNumber = pMyNode:getChildByName("win_number");
        local pLoseNumber = pMyNode:getChildByName("lose_number");

        pWinNumber:setFntFile("common/font/num_win.fnt")
        pLoseNumber:setFntFile("common/font/num_lose.fnt")

        pWinNumber:setPositionX(8)
        pLoseNumber:setPositionX(8)

        text_self_name:setString(Player:getNickName());
        -- pWinNumber:setFntFile("common/font/num_win.fnt");
        -- pLoseNumber:setFntFile("common/font/num_lose.fnt");
        if 0 < info.m_winGold then
            g_AudioPlayer:playEffect(FiveStarsRes.Audio.SCORE)
            pBg:loadTexture("ui_award_win_back.png", ccui.TextureResType.plistType)
            self.m_pAwardView.m_pTileAnima:setAnimation(0, "animation1", true)
            pWinNumber:setVisible(true)
            pLoseNumber:setVisible(false)
            
            pWinNumber:setString("+"..info.m_winGold * 0.01)
        else
            pBg:loadTexture("ui_award_lose_back.png", ccui.TextureResType.plistType)
            self.m_pAwardView.m_pTileAnima:setAnimation(0, "animation2", true)
            pWinNumber:setVisible(false)
            pLoseNumber:setVisible(true)
            
            pLoseNumber:setString(""..info.m_winGold * 0.01)
        end
    else
        pMyNode:setVisible(false)
        pNoBet:setVisible(true)
        pBg:loadTexture("ui_award_win_back.png", ccui.TextureResType.plistType)
        -- self.m_pAwardView.m_pTileAnima:setAnimation(0, "animation1", true);
    end
    local size = self.m_pAwardView:getParent():getContentSize()
    self.m_pAwardView:setOpacity(0)
    -- pCloseButton:setEnabled(true)

    self.m_pAwardView:runAction(
        cc.Sequence:create(
            cc.Show:create(),
            cc.EaseSineOut:create(
                cc.Spawn:create(
                    cc.FadeIn:create(0.8),
                    cc.Sequence:create(
                        cc.MoveTo:create(0.5, cc.p(size.width/2 - 100, size.height/2)),
                        cc.MoveTo:create(0.2, cc.p(size.width/2, size.height/2)),
                    nil)
                )
            ),
            cc.DelayTime:create(2),
            cc.CallFunc:create(function()
                self:_CloseAwrdView()
            end),
        nil)
    )

end

function FiveStarsLayer:_CloseAwrdView()
    -- local pCloseButton = self.m_pAwardView:getChildByName("award_view_button")
    -- pCloseButton:setEnabled(false)
    local size = self.m_pAwardView:getParent():getContentSize()
    self.m_pAwardView:stopAllActions()
    self.m_pAwardView:runAction(
        cc.Sequence:create(
            cc.Spawn:create(
                cc.FadeOut:create(0.5),
                cc.Sequence:create(
                    cc.MoveTo:create(0.3, cc.p(size.width/2 + 100, size.height/2)),
                    cc.MoveTo:create(0.2, cc.p(-100, size.height/2))
                )
            ),
            cc.Hide:create(),
        nil)
    )
end

function FiveStarsLayer:_PlayAni(_key, _name, _point, _loop, _delayRemove)
    self.m_aniList = self.m_aniList or {}
    local ani = self.m_aniList[_key]
    if (not ani) then
        local point = _point or cc.p(667, 375)
        self.m_aniList[_key] = CacheManager:addDragonBonesTo(self.m_pRoot, _key)
        ani = self.m_aniList[_key]
        ani:setPosition(point)
        ani:setOpacity(255)
    end

    ani:getAnimation():play(_name)
    if not _loop then
        local animationEvent = function (armatureBack, movementType, movementID)
            if movementType ~= ccs.MovementEventType.complete then
                return
            end
            if _delayRemove and _delayRemove > 0 then
                ani:runAction(cca.seq({cca.delay(_delayRemove), cca.fadeOut(0.2), cca.callFunc(function()
                    self.m_aniList[_key] = nil
                    CacheManager:putDragonBonesNode(_key, ani)
                end)}))
            else
                ani:runAction(cca.seq({cca.fadeOut(0.2), cca.callFunc(function()
                    self.m_aniList[_key] = nil
                    CacheManager:putDragonBonesNode(_key, ani)
                end)}))
            end
        end

        ani:getAnimation():setMovementEventCallFunc(animationEvent)
    end
end

function FiveStarsLayer:_StopAni(_key)
    self.m_aniList = self.m_aniList or {}
    local ani = self.m_aniList[_key]
    if ani then
        ani:removeFromParent()
        self.m_aniList[_key] = nil
    end
end

function FiveStarsLayer:_SetRecordID(id)
    if id and id ~= "" then
        self.mTextRecord:setString("牌局ID:"..id)
    end
end

function FiveStarsLayer:_NodeBlink(_node, count)
    _node:stopAllActions()
    local seq = cca.seq({ cca.fadeIn(0.1), cca.delay(0.3), cca.fadeOut(0.1)})
    _node:runAction(cca.repeatForever(seq))
end


--endregion
--region 消息接受函数
function FiveStarsLayer:ON_GAME_INIT(_msgId, cmd)
    local pData = FiveStarsData.getInstance()
    self:_SetBetValues(pData.m_pBetValues)
    self:_RefreshButtonState()
    self:_ShowStateTime(pData:getLeftTime(), true)
    self:ON_HISTORY_HTY(nil)
    self:_RefreshCurrentCoin()
    self.m_pTextWheel:setString(pData.m_nWheel .. "场")
    self.m_pTextRound:setString(pData.m_nRound .. "局")
    self.m_pCardView:hideCard(pData.m_nGirlId)
    self:_SetRecordID(pData.m_sRecordId)
    self.m_pBetMaxText:setString("限红:\n花色最低"..(pData.m_nFourColorMin*0.01).."分\n最高"..(pData.m_nFourColorMax*0.01).."分\n王最高"..(pData.m_nKingMax*0.01).."分\n")

    if (pData.m_nGameState == FiveStarsConfig.STATE_BET) then
        self:_ClearDesk()
        -- self.m_pTextAllBetCoin:setString(pData:getSelfToalGold())
    else
        self:_PlayAni("bairenniuniu_dengdaikaiju", "Animation1", cc.p(667, 400), true)
    end
end

function FiveStarsLayer:ON_GAME_STATE(_msgId, cmd)
    local pData = FiveStarsData.getInstance()
    self:_RefreshButtonState()
    self.m_pTextWheel:setString(pData.m_nWheel .. "场")
    self.m_pTextRound:setString(pData.m_nRound .. "局")
    self:_SetRecordID(pData.m_sRecordId)
    self.m_pBetMaxText:setString("限红:\n花色最低"..(pData.m_nFourColorMin*0.01).."分\n最高"..(pData.m_nFourColorMax*0.01).."分\n王最高"..(pData.m_nKingMax*0.01).."分\n")
    if (pData.m_nGameState == FiveStarsConfig.STATE_BET) then
        for index = 1, FiveStarsConfig.CONST_AREA_COUNT do
            self.m_pAreaButtons[index]:stopAllActions()
        end
        self:_StopAni("bairenniuniu_dengdaikaiju")
        self.m_aniStart:setVisible(true)
        self.m_aniStart:setOpacity(255)
        self.m_aniStart:setAnimation(0, "Animation1", false);
        self.m_aniStart:runAction(cca.seq({cca.delay(2.2), cca.fadeOut(0.2), cca.hide()}))
        -- self:_DoSomethingLater(function ()
        --     self.m_aniStart:runAction(cca.seq({cca.delay(1), cca.fadeOut(0.2), cca.hide()}))
        -- end, 1.2)

        --self:_PlayAni("kaishixiazhu_kaijiang", "Animation1", cc.p(667, 400), false, 1)
        g_AudioPlayer:playEffect(FiveStarsRes.Audio.START_BET, false)
        self:_ClearDesk()
        self.m_pCardView:hideCard(pData.m_nGirlId)
        self:_ShowStateTime(pData:getLeftTime(), true)
    -- elseif (pData.m_nGameState == FiveStarsConfig.STATE_OPEN) then
    --     self:_ShowStateTime(pData:getLeftTime(), true)
    --     g_AudioPlayer:playEffect(FiveStarsRes.Audio.STOP_BET, false)
    --     self:_DoSomethingLater(function()
    --         g_AudioPlayer:playEffect(FiveStarsRes.Audio.OPEN_CARD, false)
    --         self.m_pCardView:openCard(pData.m_pLuckNumberInfo.m_dantiaoNum, pData:getLeftTime() - 2)
    --     end, 2)
    elseif (pData.m_nGameState == FiveStarsConfig.STATE_AWARD) then
        if not self.m_aniList["bairenniuniu_dengdaikaiju"] then
            g_AudioPlayer:playEffect(FiveStarsRes.Audio.STOP_BET, false)
            -- self:_PlayAni("kaishixiazhu_kaijiang", "Animation2", cc.p(667, 400))
            self.m_aniStart:setVisible(true)
            self.m_aniStart:setOpacity(255)
            self.m_aniStart:setAnimation(0, "Animation2", false);
            self.m_aniStart:runAction(cca.seq({cca.delay(2.2), cca.fadeOut(0.2), cca.hide()}))
            -- self:_DoSomethingLater(function ()
                
            -- end, 1.2)
            self:_ShowStateTime(pData:getLeftTime(), true)
            -- self:_RefreshCurrentCoin()
            -- if (pData.m_nWinGold > 0) then
            --     g_AudioPlayer:playEffect(FiveStarsRes.Audio.SCORE)
            -- elseif (pData.m_nWinGold < 0) then
            -- end
        end
    end

end

function FiveStarsLayer:ON_BET_ACK(_msgId, cmd)
    if (0 == cmd.nReslut) then
        -- local pData = FiveStarsData.getInstance()
        if (Player:getAccountID() == cmd.nAccountId) then
            self:_RefreshCurrentCoin()
            self:_RefreshAllBetCoin()
            self:_RefreshButtonState()
        end
    else
        -- self:_UnButtonScheduleHandler(self.m_pAreaButtons[cmd.nAreaId])
        self.m_pAreaButtons[cmd.nAreaId]:StopContinue()
    end
end

function FiveStarsLayer:ON_SELF_BET_SUM_NTY(_msgId, cmd)
    self:_RefreshAllBetCoin()
end

function FiveStarsLayer:ON_BET_SUM_NTY(_msgId, cmd)
    self:_RefreshAllBetCoin()
end

function FiveStarsLayer:ON_GAME_BALANCE_NTY(_msgId, cmd)
    --结算
    if self.m_aniList["bairenniuniu_dengdaikaiju"] then
        return
    end
    local pData = FiveStarsData.getInstance()
    self:_RefreshCurrentCoin()
    self:_RefreshAllBetCoin()
    local myBet = nil
    for k, v in pairs(cmd.m_vecBalance) do
        if (v.m_accountId == Player:getAccountID()) then
            myBet = v
            break
        end
    end


    local buttonAction = function(button, _s1, _s2)
        local s1Func = cca.callFunc(function ()
            button:loadTexture(_s1, ccui.TextureResType.plistType)
            -- button:setColor(cc.c3b(160, 160, 160))
        end)
        local s2Func = cca.callFunc(function ()
            button:loadTexture(_s2, ccui.TextureResType.plistType)
            -- button:setColor(cc.c3b(255, 255, 255))
        end)
        local seq = cca.seq({s2Func, cca.delay(0.4), s1Func, cca.delay(0.1)})
        button:stopAllActions()
        button:setColor(cc.c3b(255, 255, 255))
        
        button:runAction(cca.repeatForever(seq))
    end

    self:_DoSomethingLater(function ()
        self.m_pCardView:openCard(pData.m_pLuckNumberInfo.m_dantiaoNum, 1.5)
        
        self:_DoSomethingLater(function ()
            for index = 1, FiveStarsConfig.CONST_AREA_COUNT do
                self.m_pAreaButtons[index]:setColor(cc.c3b(255, 255, 255))
            end
            buttonAction(self.m_pAreaButtons[pData.m_pLuckNumberInfo.m_awardType], self.m_AreaButtonImage[pData.m_pLuckNumberInfo.m_awardType][1], self.m_AreaButtonImage[pData.m_pLuckNumberInfo.m_awardType][3])
            self:_NodeBlink(self.m_pTextSelfAreaCoin[pData.m_pLuckNumberInfo.m_awardType])
            self:_SetHistory(pData.m_pHistoryInfo)
            self:_NodeBlink(self.m_pTextAllBetCoin)
            self.m_pTextAllBetCoin:setString("0")
            if (myBet) then
               if (myBet.m_winGold > 0) then
                    self.m_pTextAllBetCoin:setString(""..myBet.m_winGold*0.01)
                    g_AudioPlayer:playEffect(FiveStarsRes.Audio.WIN)
                    -- self.m_pTextSelfLoseNumber:setVisible(false)
                    -- self.m_pTextSelfWinNumber:setVisible(true)
                    -- self.m_pTextSelfWinNumber:setOpacity(255)
                    -- self.m_pTextSelfWinNumber:setString("+"..myBet.m_winGold*0.01)
                    -- self.m_pTextSelfWinNumber:runAction(cca.seq({
                    --     cca.show(), cca.delay(1), cca.fadeOut(0.5)
                    -- }))
               elseif (myBet.m_winGold < 0) then
                    g_AudioPlayer:playEffect(FiveStarsRes.Audio.LOSE)
                    self.m_pTextSelfWinNumber:setVisible(false)
                    self.m_pTextSelfLoseNumber:setVisible(true)
                    self.m_pTextSelfLoseNumber:setOpacity(255)
                    self.m_pTextSelfLoseNumber:setString(""..myBet.m_winGold*0.01)
                    self.m_pTextSelfLoseNumber:runAction(cca.seq({
                        cca.show(), cca.delay(3), cca.fadeOut(0.5)
                    }))
               end
            else

            end
            -- self:_ShowAwardView(myBet, cmd.m_profitTop3)
        end, 2)
    end, 1.5)

    -- self.m_pAwardView:runAction(
    --     cc.Sequence:create(
    --         cc.DelayTime:create(4),
    --         cc.CallFunc:create(function()
    --             -- self:showPlayHeadflash(cmd.m_vecBalance);
                
    --         end),
    --     nil)
    -- )
    
end

function FiveStarsLayer:ON_GAME_DT_NUMBER_NTY(_msgId, cmd)

end

function FiveStarsLayer:ON_HISTORY_HTY(_msgId, cmd)

    -- { 1,	1, 'm_spadeCnt'         , 'UINT'       				, 1     , '黑桃数量' },
    -- { 2,	1, 'm_heartCnt'         , 'UINT'       				, 1     , '红桃数量' },
    -- { 3,	1, 'm_clubCnt'          , 'UINT'       				, 1     , '梅花数量' },
    -- { 4,	1, 'm_diamondCnt'       , 'UINT'       				, 1     , '方块数量' },
    -- { 5,	1, 'm_kingCnt'          , 'UINT'       				, 1     , '王数量' },
    local pData = FiveStarsData.getInstance()
    if not self.m_IsInitHistoryInfo then
        self.m_IsInitHistoryInfo = true
        self:_SetHistory(pData.m_pHistoryInfo)
    end
end

function FiveStarsLayer:ON_CANCEL_ACK(_msgId, cmd)
    self:_RefreshCurrentCoin()
    self:_RefreshAllBetCoin()
end

function FiveStarsLayer:ONONLINE_PLAYER_LIST_ACK(_msgId, cmd)

end

function FiveStarsLayer:ON_SELF_WIN_GOLD(_msgId, cmd)

end

function FiveStarsLayer:ON_SELF_COIN(_msgId, cmd)
    self:_RefreshCurrentCoin()
end

function FiveStarsLayer:ON_PLAYER_CUOPAI(_msgId, cmd)
    -- local pData = FiveStarsData.getInstance()
    -- if pData.m_nCuoPaiCompleteFlag == 1 then
    --     --搓牌完成
    --     g_AudioPlayer:playEffect(FiveStarsRes.Audio.OPEN_CARD, false)
    --     self.m_pCardView:openCard(pData.m_pLuckNumberInfo.m_dantiaoNum, pData:getLeftTime() - 2)
    -- else
    --     --搓牌未完成
    -- end
end

function FiveStarsLayer:ON_CUOPAI_START_NTY(_msgId, cmd)

end

function FiveStarsLayer:ONONLINE_PLAYER_COUNT(_msgId, cmd)

end

function FiveStarsLayer:ON_TOP_PLAYER_LIST(_msgId, cmd)

end

function FiveStarsLayer:ON_CHIP_VALUES(_msgId, cmd)
    local pData = FiveStarsData.getInstance()
    self:_SetBetValues(pData.m_pBetValues)
end

function FiveStarsLayer:ON_CONTINUE_BET(_msgId, cmd)
    if (cmd.nReslut == 0) then
        if (Player:getAccountID() == cmd.nAccountId) then
            self.m_bIsContinue = true
            self.m_pContinueButton:setEnabled(false)
            self.m_pContinueButton:setColor(cc.c3b(160, 160, 160))
            self:_RefreshCurrentCoin()
            self:_RefreshButtonState()
        else
    
        end
        self:_RefreshAllBetCoin()
    end
end


--endregion
return FiveStarsLayer