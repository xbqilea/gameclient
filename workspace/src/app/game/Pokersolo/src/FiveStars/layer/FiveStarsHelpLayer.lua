-- region *.lua
-- Date
-- 此文件由[BabeLua]插件自动生成	
local HNLayer= require("src.app.newHall.HNLayer") 
local FiveStarsRes = require("FiveStars.config.FiveStarsRes")
local FiveStarsHelpLayer = class("FiveStarsHelpLayer", function()
    return HNLayer.new()
end)

function FiveStarsHelpLayer:ctor()
    self:initCSB()
end

function FiveStarsHelpLayer:initCSB()
    local layer = cc.CSLoader:createNode(FiveStarsRes.CSB.HELP)
    layer:addTo(self)
    self.m_pathUI = layer:getChildByName("Layer")
    self.m_pathUI:setAnchorPoint(cc.p(0.5, 0.5))
    self.m_pathUI:setPosition(cc.p(display.width / 2, display.height / 2))
    
    self.m_pBtnClose = self.m_pathUI:getChildByName("image_bg"):getChildByName("button_close")
    self.m_pBtnClose:addTouchEventListener(handler(self, self.onReturnClicked))

    local back = self.m_pathUI:getChildByName("Layout_2")
    back:addTouchEventListener(handler(self, self.onReturnClicked))

end

function FiveStarsHelpLayer:onReturnClicked(sender,eventType)
    if eventType ~=ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound(ToolKit:getCurrentScene():getSoundPath())
    self:close()
end

return FiveStarsHelpLayer