local FiveStarsRes = {}

FiveStarsRes.CSB = {}
FiveStarsRes.CSB.MAIN_UI = "FiveStars/csb/main_ui.csb"
FiveStarsRes.CSB.HELP = "FiveStars/csb/DragonHelp.csb"


FiveStarsRes.Audio = {}
FiveStarsRes.Audio.START_BET = "FiveStars/audio/pls_bet.mp3"
FiveStarsRes.Audio.STOP_BET = "FiveStars/audio/goodluck.mp3"
FiveStarsRes.Audio.BET = "FiveStars/audio/bet.mp3"
FiveStarsRes.Audio.CLOCK_TIME = "FiveStars/audio/clock_time.mp3"
FiveStarsRes.Audio.SCORE = "FiveStars/audio/score.mp3"
FiveStarsRes.Audio.OPEN_CARD = "FiveStars/audio/open_card.mp3"
FiveStarsRes.Audio.LAST10 = "FiveStars/audio/last10.mp3"
FiveStarsRes.Audio.CANCEL_BET = "FiveStars/audio/cancel_bet.mp3"
FiveStarsRes.Audio.LOSE = "FiveStars/audio/lose.mp3"
FiveStarsRes.Audio.WIN = "FiveStars/audio/win.mp3"

FiveStarsRes.Ani = {}

FiveStarsRes.Image = {}
FiveStarsRes.Image.POKER_BACK = "FiveStars/card/card_back/card_back_girl_%d.png"


--region 加载资源列表
FiveStarsRes.vecPlist = {
-- {"game/car/plist/car.plist", "game/car/plist/car.png", },
    {"FiveStars/ui/history_icon.plist", "FiveStars/ui/history_icon.png",},
    {"FiveStars/ui/main_ui.plist", "FiveStars/ui/main_ui.png",},
    {"FiveStars/ui/award.plist", "FiveStars/ui/award.png"},
    
    -- {"dantiao_plist/g_mainscene.plist", "dantiao_plist/g_mainscene.png",},
	-- {"dantiao_plist/g_rule.plist", "dantiao_plist/g_rule.png",},
    -- {"dantiao_plist/g_set.plist", "dantiao_plist/g_set.png",},
    
}

FiveStarsRes.vecImage = {
    "FiveStars/card/card_back/card_back_girl_1.png",
    "FiveStars/card/card_back/card_back_girl_2.png",
    "FiveStars/card/card_back/card_back_girl_3.png",
    "FiveStars/card/card_back/card_back_girl_4.png",
    "FiveStars/card/card_back/card_back_girl_5.png",
    "FiveStars/card/card_back/card_back_girl_6.png",
    "FiveStars/card/card_back/card_back_girl_7.png",
    "FiveStars/card/card_back/card_back_girl_8.png",
    "FiveStars/card/card_back/card_back_girl_9.png",
    "FiveStars/card/card_back/card_back_girl_10.png",
    "FiveStars/card/card_back/card_back_girl_11.png",
    "FiveStars/card/card_back/card_back_girl_12.png",
    "FiveStars/card/card_back/card_back_girl_13.png",
    "FiveStars/card/card_back/card_back_girl_14.png",
    "FiveStars/card/card_back/card_back_girl_15.png",
    "FiveStars/card/card_back/card_back_girl_16.png",
    "FiveStars/card/card_back/card_back_girl_17.png",
    "FiveStars/card/card_back/card_back_girl_18.png",
    "FiveStars/card/card_back/card_back_girl_19.png",
    "FiveStars/card/card_back/card_back_girl_20.png",
    "FiveStars/card/card_back/card_back_girl_21.png",
    "FiveStars/card/card_back/card_back_girl_22.png",
    "FiveStars/card/card_back/card_back_girl_23.png",
    "FiveStars/card/card_back/card_back_girl_24.png",
    "FiveStars/card/card_back/card_back_girl_25.png",
    "FiveStars/card/card_back/card_back_girl_26.png",
    "FiveStars/card/card_back/card_back_goldkuang.png",
    "FiveStars/card/card_back/card_back_poker.png",

    -- "FiveStars/ui/ui_background.png",
    "FiveStars/font/daojishi_number.png",
}

FiveStarsRes.vecAnim = {
--    "game/car/animation/bairenniuniu_dengdaikaiju/bairenniuniu_dengdaikaiju.ExportJson",
    -- "FiveStars/effect/bairenniuniu_dengdaikaiju/bairenniuniu_dengdaikaiju.ExportJson",
 	-- "FiveStars/effect/dant_mvpjiesuan/dant_mvpjiesuan.ExportJson",
 	-- "FiveStars/effect/dant_youwin/dant_youwin.ExportJson",
 	-- "FiveStars/effect/daojishi_1/daojishi_1.ExportJson",
    --  "FiveStars/effect/kaishixiazhu_kaijiang/kaishixiazhu_kaijiang.ExportJson",
     "FiveStars/effect/bairenniuniu_dengdaikaiju/bairenniuniu_dengdaikaiju.ExportJson",
}

FiveStarsRes.vecSound = {

}

FiveStarsRes.vecMusic = {

}

--endregion

return FiveStarsRes