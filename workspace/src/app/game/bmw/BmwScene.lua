--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
--endregion
local Scheduler        = require("framework.scheduler")
local CCGameSceneBase    = require("src.app.game.common.main.CCGameSceneBase")
local BmwGameTableLayer    = import(".BmwGameLayer")
local DlgAlert = require("app.hall.base.ui.MessageBox")
local BMWData = require("app.game.bmw.src.BMWData");
local BMWEvent = require("app.game.bmw.src.BMWEvent");

local BMWLodingLayer = require("app.game.bmw.src.BMWLodingLayer")

local BmwGameScene = class("BmwGameScene", function()
    return CCGameSceneBase.new()
end)

function BmwGameScene:ctor()
    self.m_BmwMainLayer        = nil
    self.m_BmwExitGameLayer    = nil
    self.m_BmwRuleLayer        = nil
    self.m_BmwMusicSetLayer    = nil
    self.m_bIsNetInit = false;
    self:myInit()
end

-- 游戏场景初始化
function BmwGameScene:myInit()
    -- self:loadResources()
    --BmwRoomController:getInstance():setInGame( true )  
    -- 主ui 
    --self:initBmwGameMainLayer()
    self:initBmwLodingLayer()

    self:registBackClickHandler(handler(self, self.onBackButtonClicked)) -- Android & Windows注册返回按钮
    addMsgCallBack(self, MSG_ENTER_FOREGROUND, handler(self, self.onEnterForeground)) -- 转前台
    addMsgCallBack(self, MSG_ENTER_BACKGROUND, handler(self, self.onEnterBackground)) -- 转后台 
    --addMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT, handler(self, self.socketState))
    --addMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT, handler(self,self.onSocketEventMsgRecived))
    addMsgCallBack(self, UPDATE_GAME_RESOURCE, handler(self, self.updateCallback))
end

function BmwGameScene:updateCallback()
    self:initBmwGameMainLayer()
    self:removeChild(self.m_pBmwLodingLayer);
    self.m_pBmwLodingLayer = nil;
end

---- 进入场景
function BmwGameScene:onEnter()
    print("-----------BmwGameScene:onEnter()-----------------")
    ToolKit:setGameFPS(1 / 60)
end

function BmwGameScene:initBmwLodingLayer()
    self.m_pBmwLodingLayer = BMWLodingLayer.new()
    self:addChild(self.m_pBmwLodingLayer)
end


-- 初始化主ui
function BmwGameScene:initBmwGameMainLayer()
    self.m_BmwMainLayer = BmwGameTableLayer.new()
    self:addChild(self.m_BmwMainLayer)
    if self.m_bIsNetInit then
        self:_SendMsg(BMWEvent.MSG_INIT);
    end
end

function BmwGameScene:getMainLayer()
    return self.m_BmwMainLayer
end

-- 显示游戏退出界面
-- @params msgName( string ) 消息名称
-- @params __info( table )   退出相关信息
-- 显示游戏退出界面
--[[function BmwGameScene:showExitGameLayer()
    if self._Scheduler1 then
        Scheduler.unscheduleGlobal(self._Scheduler1)	
        self._Scheduler1 = nil
    end 
     
    UIAdapter:popScene()
    g_GameController.gameScene = nil
end
--]]
function BmwGameScene:onEnter()
    print("------BmwGameScene:onEnter begin--------")
    print("------BmwGameScene:onEnter end--------")
end

-- 退出场景
function BmwGameScene:onExit()
    print("------BmwGameScene:onExit begin--------")

    if self.m_BmwMainLayer._Scheduler1 then
        Scheduler.unscheduleGlobal(self.m_BmwMainLayer._Scheduler1)
        self.m_BmwMainLayer._Scheduler1 = nil
    end
    self.m_BmwMainLayer:onCleanup()
    removeMsgCallBack(self, MSG_ENTER_FOREGROUND)
    removeMsgCallBack(self, MSG_ENTER_BACKGROUND)
    removeMsgCallBack(self, UPDATE_GAME_RESOURCE)
    --removeMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT)
    --removeMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT)
    --    BmwGlobal.m_isNeedReconectGameServer = false
    --    BmwRoomController:getInstance():setInGame( false )
    --    BmwGameController:getInstance():onDestory()
    --   self:RemoveResources()
    print("------BmwGameScene:onExit end--------")
end

-- 响应返回按钮事件
function BmwGameScene:onBackButtonClicked()
    g_GameController:exitReq()
    UIAdapter:popScene()
end

-- 从后台切换到前台
function BmwGameScene:onEnterForeground()
    print("从后台切换到前台")
    g_GameController:gameBackgroundReq(2)
end

-- 从前台切换到后台
function BmwGameScene:onEnterBackground()
    print("从前台切换到后台,游戏线程挂起!")
    g_GameController:gameBackgroundReq(1)
    g_GameController.m_BackGroudFlag = true
end

function BmwGameScene:clearView()
    print("BmwGameScene:clearView()")
end

-- 清理数据
function BmwGameScene:clearData()

end

--退出游戏处理
--[[function BmwGameScene:exitGame()   
    if self.m_BmwMainLayer._Scheduler1 then
        Scheduler.unscheduleGlobal(self.m_BmwMainLayer._Scheduler1)	
        self.m_BmwMainLayer._Scheduler1 = nil
    end 
     ConnectManager:send2GameServer( g_GameController.m_gameAtomTypeId,"CS_C2G_UserLeft_Req", { })
    UIAdapter:popScene()
    self:closeGameSvrConnect()
    g_GameController.gameScene = nil
    g_GameController:onDestory()
end
--]]
--消息处理
function BmwGameScene:ON_Bmw_Init_Nty(__cmd)
    --初始化
    -- { 1,	1, 'm_state'		 	 	 ,  'UINT'						, 1		, '当前状态: 1-ready 2-下注 3-开奖'},
    -- { 2,	1, 'm_leftTime'		 	 	 ,  'UINT'						, 1		, '本状态剩余时间(单位秒)'},
    -- { 3,	1, 'm_totalTime'		 	 ,  'UINT'						, 1		, '本状态总时间(单位秒)'},
    -- { 4,	1, 'm_playerInfo'			 ,  'PstBmwInitPlayerInfo'		, 1		, '玩家基本信息'},
    -- { 5, 	1, 'm_minGameCoin'			 ,  'UINT'						, 1 	, '下注必须满足的最低拥有游戏金币' },
    -- { 6, 	1, 'm_playerLimit'			 ,  'UINT'						, 1 	, '个人下注上限' },
    -- { 7, 	1, 'm_allPlayerTotalLimit'	 ,  'UINT'						, 1 	, '所有玩家总下注上限' },
    -- { 8,	1, 'm_isLook'			 	 ,  'UINT'						, 1		, '是否旁观者, 0:否, 1:是'},
    -- { 9,	1, 'm_lastAwardType'		 ,  'UINT'						, 1		, '上一次开奖类型(同CS_G2C_Bmw_OpenAward_Nty协议的m_awardType)'},
    -- { 10,	1, 'm_lastGridId'			 ,  'UINT'						, 1		, '上一次跑灯停留格子id'},
    -- { 11,	1, 'm_allTotalBet'   	 	 ,  'UINT'						, 1		, '所有区域、所有玩家总下注'},
    -- { 12, 	1, 'm_betArea'				 ,  'PstBmwBetAreaMultiple'		, 1024 	, '初始化下注区域' },
    -- { 13, 	1, 'm_chipArr'			 	 ,  'UINT'						, 1024 	, '初始化筹码' },
    -- { 14, 	1, 'm_history'			 	 ,  'UINT'						, 1024 	, '历史开奖记录(元素类型同CS_G2C_Bmw_OpenAward_Nty协议的m_awardType)' },
    -- { 15, 	1, 'm_areaTotalBet'			 ,  'PstBmwAreaTotalBet'		, 1024 	, '各区域总下注' },
    -- { 16, 	1, 'm_myAreaBet'		 	 ,  'PstBmwAreaMyBet'			, 1024 	, '我在各区域的下注' },
    BMWData.getInstance():Set_Init_Nty(__cmd);
    self.m_bIsNetInit = true;
    self:_SendMsg(BMWEvent.MSG_INIT);
end

function BmwGameScene:ON_Bmw_GameReady_Nty(__cmd)
    --等待状态
    -- { 1,	1, 'm_leftTime'			 ,  'UINT'						, 1		, '该状态剩余时间'},
    -- { 2, 	1, 'm_isLook'			 , 'UINT'						, 1 	, '是否旁观者, 0:否, 1:是' },
    BMWData.getInstance():Set_GameReady_Nty(__cmd);
    self:_SendMsg(BMWEvent.MSG_GAME_STATE);
end

function BmwGameScene:ON_Bmw_OpenAward_Nty(__cmd)
    --开奖
    -- { 1,	1, 'm_result'			 ,  'INT'						, 1		, '0:成功, -X:失败'},
    -- { 2,	1, 'm_leftTime'			 ,  'UINT'						, 1		, '该状态剩余时间'},
    -- { 3,	1, 'm_awardType'		 ,  'UINT'						, 1		, '开奖类型, 1:大保时捷  2：大宝马  3:大奔驰  4:大大众 5:小保时捷 6:小宝马 7:小奔驰 8:小大众 '},
    -- { 4,	1, 'm_openGridId'		 ,  'UINT'						, 1		, '本次开奖后跑灯停留格子id'},
    -- { 5,	1, 'm_allTotalBet'   	 ,  'UINT'						, 1		, '所有区域、所有玩家总下注'},
    -- { 6,	1, 'm_awardBetAreaId'	 ,  'UINT'						, 1024	, '本次中奖的下注区域id'},
    -- { 7, 	1, 'm_history'			 ,  'UINT'						, 1024 	, '历史开奖记录(元素类型同m_awardType)' },
    -- { 8,	1, 'm_balanceInfo'  	 ,  'PstBmwBalanceClt'			, 1		, '自己结算数据'},
    -- { 9,	1, 'm_topPlayerBalance'  ,  'PstBmwBalanceClt'			, 1024	, '左右两侧前6名的结算(数量<=6)'},
    -- PstBmwBalanceClt
    -- { 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
    -- { 2,	1, 'm_profit'			, 'INT'					, 1		, '本轮净盈利'},
    -- { 3,	1, 'm_curCoin'			, 'UINT'				, 1		, '结算后金币'},
    BMWData.getInstance():Set_OpenAward_Nty(__cmd);
    self:_SendMsg(BMWEvent.MSG_GAME_STATE, {
        m_nAwardType = __cmd.m_awardType,
        m_nOpenGridId = __cmd.m_openGridId,
        m_pBalanceInfo = __cmd.m_balanceInfo,
        m_pTopPlayerBalance = __cmd.m_topPlayerBalance
    });
end

function BmwGameScene:ON_Bmw_Bet_Ack(__cmd)
    --玩家下注
    -- { 1, 	1, 'm_result'		 ,  'INT'					, 1		, '0:成功, -x:失败(m_result=0时，广播此ack)' },
    -- { 2, 	1, 'm_betAccountId'	 ,  'UINT'					, 1		, '下注玩家ID' },
    -- { 3,	1, 'm_curCoin'		 ,  'UINT'					, 1		, '下注成功后的玩家金币(仅在m_result=0,且m_betAccountId=自己时有效)'},
    -- { 4,	1, 'm_betAreaId'	 ,  'UINT'					, 1		, '下注区域id'},
    -- { 5,	1, 'm_betValue'		 ,  'UINT'					, 1		, '本次下注额'},
    -- { 6,	1, 'm_allTotalBet'   ,  'UINT'					, 1		, '所有区域、所有玩家总下注'},
    -- { 7, 	1, 'm_areaTotalBet'	 ,  'PstBmwAreaTotalBet'	, 1024 	, '各区域总下注' },
    -- { 8, 	1, 'm_myAreaBet'	 ,  'PstBmwAreaMyBet'	    , 1024 	, '我在各区域的下注(仅在m_result=0,且m_betAccountId=自己时有效)' },
    if 0 == __cmd.m_result then
        BMWData.getInstance():Set_Bet_Ack(__cmd);
        self:_SendMsg(BMWEvent.MSG_BET, {
            m_nAccountId = __cmd.m_betAccountId,
            m_nAreaId = __cmd.m_betAreaId,
            m_nBetValue = __cmd.m_betValue
        });
    else
        self:_ShowErrorMsg("BET_ACK", __cmd.m_result);
    end
end

function BmwGameScene:ON_Bmw_Bet_Nty(__cmd)
    --下注状态
    -- { 1,	1, 'm_leftTime'			 ,  'UINT'						, 1		, '该状态剩余时间'},
    BMWData.getInstance():Set_Bet_Nty(__cmd);
    self:_SendMsg(BMWEvent.MSG_GAME_STATE);
end

function BmwGameScene:ON_Bmw_OnlinePlayerList_Ack(__cmd)
    --在线玩家信息
    -- { 1,	1, 'm_playerList'			 , 'PstBmwInitPlayerInfo'		, 1024		, '玩家基本信息'},
    BMWData.getInstance():Set_OnlinePlayerList_Ack(__cmd);
    self:_SendMsg(BMWEvent.MSG_ONLINE_PLAYER_LIST);
end

function BmwGameScene:ON_Bmw_Background_Ack(__cmd)
    --切换后台
end

function BmwGameScene:ON_Bmw_Exit_Ack(__cmd)
    --退出游戏
end

function BmwGameScene:ON_Bmw_TopPlayerList_Nty(__cmd)
    --前6名玩家信息
    -- { 1, 	1, 'm_topPlayerList'		 , 'PstBmwTopPlayerInfo'  			, 1024		, '前6名玩家(实际数量 <= 6)' },
    BMWData.getInstance():Set_TopPlayerList_Nty(__cmd);
    self:_SendMsg(BMWEvent.MSG_TOP_PLAYER_LIST);
end

function BmwGameScene:ON_Bmw_ContinueBet_Ack(__cmd)
    --续投
    -- { 1, 	1, 'm_result'		 ,  'INT'					, 1		, '0:成功, -x:失败(m_result=0时，广播此ack)' },
    -- { 2, 	1, 'm_betAccountId'	 ,  'UINT'					, 1		, '下注玩家ID' },
    -- { 3,	1, 'm_curCoin'		 ,  'UINT'					, 1		, '下注成功后的玩家金币(仅在m_result=0,且m_betAccountId=自己时有效)'},
    -- { 4,	1, 'm_continueBetArr'	 ,  'PstBmwContinueBet'	, 1024	, '玩家续押信息'},
    -- { 5,	1, 'm_allTotalBet'   ,  'UINT'					, 1		, '所有区域、所有玩家总下注'},
    -- { 6, 	1, 'm_areaTotalBet'	 ,  'PstBmwAreaTotalBet'	, 1024 	, '各区域总下注' },
    -- { 7, 	1, 'm_myAreaBet'	 ,  'PstBmwAreaMyBet'	    , 1024 	, '我在各区域的下注(仅在m_result=0,且m_betAccountId=自己时有效)' },
    if 0 == __cmd.m_result then
        BMWData.getInstance():Set_ContinueBet_Ack(__cmd);
        -- { 1,	1, 'm_betAreaId'		 ,  'UINT'				, 1		, '下注区域id, 1:大保时捷  2：大宝马  3:大奔驰  4:大大众 5:小保时捷 6:小宝马 7:小奔驰 8:小大众'},
        -- { 2,	1, 'm_curBet'		 	 ,  'UINT'				, 1		, '此次下注'},
        local betArr = { 0, 0, 0, 0, 0, 0, 0, 0 };
        for index = 1, #__cmd.m_continueBetArr do
            local item = __cmd.m_continueBetArr[index];
            betArr[item.m_betAreaId] = item.m_curBet + betArr[item.m_betAreaId];
        end
        self:_SendMsg(BMWEvent.MSG_CONTINUE_BET, {
            m_nAccountId = __cmd.m_betAccountId,
            m_pBetArr = betArr
        });
    else 
        self:_ShowErrorMsg("CONTINUE_BET_ACK", __cmd.m_result);
    end
end

function BmwGameScene:_SendMsg(msgId, cmd)
    if self:getMainLayer() then
        sendMsg(msgId, cmd);
    end
end

function BmwGameScene:_ShowErrorMsg(msgId, errorCode)
    if nil == self.m_pErrorData then
        self.m_pErrorData = {};
        local betCode = {}
        betCode["-204201"] = "非下注阶段，不能下注！";
        betCode["-204202"] = "下注区域无效！";
        betCode["-204203"] = "金币不足，下注失败！";
        betCode["-204204"] = "您下注超过个人上限！";
        betCode["-204205"] = "已达下注总上限！";
        betCode["-204206"] = "下注失败, 携带金币低于30金币！";
        betCode["-204207"] = "下注筹码非法";
        betCode["-204208"] = "开奖出错,请联系客服";
        betCode["-204209"] = "配置出错";
        betCode["-204210"] = "玩家不存在";
        betCode["-204299"] = "未知错误";
        self.m_pErrorData["BET_ACK"] = betCode;

        local ContinueBet_Ack = {}
        ContinueBet_Ack["-204201"] = "非下注阶段，不能续投！";
        ContinueBet_Ack["-204202"] = "下注区域无效！";
        ContinueBet_Ack["-204203"] = "金币不足，续投失败！";
        ContinueBet_Ack["-204204"] = "您下注超过个人上限！";
        ContinueBet_Ack["-204205"] = "已达下注总上限！";
        ContinueBet_Ack["-204206"] = "续投失败, 携带金币低于30金币！";
        ContinueBet_Ack["-204207"] = "续投筹码非法";
        ContinueBet_Ack["-204208"] = "开奖出错,请联系客服";
        ContinueBet_Ack["-204209"] = "配置出错";
        ContinueBet_Ack["-204210"] = "玩家不存在";
        ContinueBet_Ack["-204299"] = "未知错误";
        self.m_pErrorData["CONTINUE_BET_ACK"] = ContinueBet_Ack;

    end

    if nil ~= self.m_pErrorData[msgId] and nil ~= self.m_pErrorData[msgId][""..errorCode] then
        TOAST(self.m_pErrorData[msgId][""..errorCode]);
    end
    
end



return BmwGameScene