if not LUA_VERSION or LUA_VERSION ~= "5.3" then
	 module(..., package.seeall)
end 
require("src.app.game.bmw.protoID")

-- 公共结构定义文件注册
netLoaderCfg_Templates_common = {
	"src/app/game/bmw/pubStTemp",
}

-- 协议定义文件注册
netLoaderCfg_Templates	=	{	
	"src/app/game/bmw/bmw",
}

-------------------------注册协议-----------------------------
-- 公共结构协议注册
netLoaderCfg_Regs_common = 
{
	PstBmwBalanceSvr			=	PSTID_BMW_BALANCESVR,
	PstBmwInitPlayerInfo 		= 	PSTID_BMW_INITPLAYERINFO,
	PstBmwBalanceClt			=	PSTID_BMW_BALANCECLT,
	PstBmwSceneSyncGameData		=	PSTID_BMW_SCENE_SYNC_GAMEDATA,
	PstBmwBetAreaMultiple		=	PSTID_BMW_BET_AREA_MULTIPLE,
	PstBmwAreaTotalBet			=	PSTID_BMW_AREA_TOTAL_BET,
	PstBmwTopPlayerInfo			=	PSTID_BMW_TOPPLAYERINFO,
	PstBmwAreaMyBet				=	PSTID_BMW_AREA_MY_BET,
    PstBmwContinueBet 			= 	PSTID_BMW_CONTINUE_BET,
}

-- 协议注册
netLoaderCfg_Regs	=	
{	
	CS_M2C_Bmw_Exit_Nty					=	CS_M2C_BMW_EXIT_NTY,
	
	CS_G2C_Bmw_Init_Nty						=	CS_G2C_BMW_INIT_NTY,
	CS_C2G_Bmw_Background_Req				=	CS_C2G_BMW_BACKGROUND_REQ,	
	CS_G2C_Bmw_Background_Ack				=	CS_G2C_BMW_BACKGROUND_ACK,
	CS_C2G_Bmw_Exit_Req						=	CS_C2G_BMW_EXIT_REQ,
	CS_G2C_Bmw_Exit_Ack						=	CS_G2C_BMW_EXIT_ACK,
	CS_C2G_Bmw_OnlinePlayerList_Req			=	CS_C2G_BMW_ONLINE_PLAYER_LIST_REQ,
	CS_G2C_Bmw_OnlinePlayerList_Ack			=	CS_G2C_BMW_ONLINE_PLAYER_LIST_ACK,
	CS_G2C_Bmw_GameReady_Nty 				= 	CS_G2C_BMW_GAMEREADY_NTY,
	CS_G2C_Bmw_Bet_Nty 						= 	CS_G2C_BMW_BET_NTY,
	CS_G2C_Bmw_OpenAward_Nty 				= 	CS_G2C_BMW_OPEN_AWARD_NTY,
	CS_C2G_Bmw_Bet_Req						=	CS_C2G_BMW_BET_REQ,
	CS_G2C_Bmw_Bet_Ack 						=   CS_G2C_BMW_BET_ACK,
	CS_G2C_Bmw_TopPlayerList_Nty			=	CS_G2C_BMW_TOP_PLAYER_LIST_NTY,
	CS_C2G_Bmw_ContinueBet_Req				=	CS_C2G_BMW_CONTINUE_BET_REQ,
	CS_G2C_Bmw_ContinueBet_Ack				=	CS_G2C_BMW_CONTINUE_BET_ACK,

	SS_M2G_Bmw_GameCreate_Req				=	SS_M2G_BMW_GAMECREATE_REQ,
	SS_G2M_Bmw_GameCreate_Ack				=	SS_G2M_BMW_GAMECREATE_ACK,
	SS_G2M_Bmw_GameResult_Nty				=	SS_G2M_BMW_GAMERESULT_NTY,
}
--return netLoaderCfg_Regs

if LUA_VERSION  and LUA_VERSION == "5.3" then
	return netLoaderCfg_Regs
end

