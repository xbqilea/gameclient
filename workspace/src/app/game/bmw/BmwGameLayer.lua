--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
--endregion
--
-- Author: lhj
-- Date: 2018-10-19 
-- 设置层
-- BmwGameLayer 
local Scheduler            = require("framework.scheduler")
local HNLayer = require("src.app.newHall.HNLayer")
local PlayerListLayer = require("src.app.game.common.util.PlayerListLayer")
local PlayerHeadLayer = require("src.app.game.common.util.PlayerHeadLayer")
local GameSetLayer = require("src.app.newHall.childLayer.SetLayer")
local BmwRuleLayer = import(".BmwRuleLayer")
local GameRecordLayer = require("src.app.newHall.childLayer.GameRecordLayer")
local CChipCacheManager    = require("src.app.game.common.manager.CChipCacheManager")
local ChatLayer = require("src.app.newHall.chat.ChatLayer") 
local BMWEvent = require("app.game.bmw.src.BMWEvent")
local BMWRes = require("app.game.bmw.src.BMWRes")
local BMWData = require("app.game.bmw.src.BMWData")
local BMWConfig = require("app.game.bmw.src.BMWConfig")

--endregion
local game_res_id = 80000042
local plist_res = game_res_id .. "/game_100019/"

local audioPath = game_res_id .. "/audio/"

local aniPath = game_res_id .. "/game_ani/"

local img_path = game_res_id .. "/game_img/"
local game_audio_res = game_res_id .. "/100019/audio/"
local game_fnt_res = game_res_id .. "/font/"

local BmwGameLayer = class("BmwGameLayer", function()
    return HNLayer.new();
end)

function BmwGameLayer:ctor()
    self:myInit()
    self:setupViews();
    --  self.tableLogic:sendGameInfo()
    -- self:msgGameInit()
    -- self:msgGameState()
    -- self:msgGameTopPlayerList()
    -- self:msgGamePlayerOnlineList()
    self:_InitMsgEvent();
end

function BmwGameLayer:myInit()
    self.reward_record = {}

    self.bet_record = {}
    self._backFlag = true

    self.current_round_bet_record = {}

    self.continue_bet_array = {}

    self.isCustomize = true

    self.max_bet_gold = 1000

    self.min_bet_gold = 1

    self.is_in_background = 0

    self.is_in_foreground = 0

    self.run_reward_timer = 0

    self.run_reward_idx = 0

    self.dest_reward_idx = 0

    self.win_bet_pos = {}

    self.cur_bet_value = 0

    self.cur_bet_pos = {}

    self.players_data = {}
    self.players_item = {}

    self.currentScore = {}

    self.is_auto_bet = false

    self.auto_bet_round = 0

    self.total_score_data = {}

    self.fly_coin = {}

    self.current_count_down = -1

    self.is_add_coin = false

    self.add_coin_per_frame = 0

    self.add_coin_times = 100

    self.game_end_info = nil

    self.next_track = 0

    self.last_light = -1

    self.stop_fly_coin = false

    self.cur_bet_index = 1

    self.max_bet_num = 6
    self.table_coin_node_array = {}
    self.bet_frame_coins_1 = {}
    self.bet_frame_coins_2 = {}
    self.bet_frame_coins_3 = {}
    self.bet_frame_coins_4 = {}
    self.bet_frame_coins_5 = {}
    self.bet_frame_coins_6 = {}
    self.bet_frame_coins_7 = {}
    self.bet_frame_coins_8 = {}

    self.totalBet = { 0, 0, 0, 0, 0, 0, 0, 0 }



    self.m_bContinue = false    --是否续投过

    self.m_isPlayBetSound = false
    self.m_chip_scale = 0.75
    self.m_bNodeMenuMoving = false
    self.m_flyJobVec = {}
    self.m_betChips = {}
    self.m_areaPos = {}
    self.m_playerPos = {}

    self.m_area_multiple = {}


end

function BmwGameLayer:onTouchCallback(sender)
    local name = sender:getName()
    local subStr = string.sub(name, 1, 8)
    local subStr1 = string.sub(name, 1, 9)
    if name == "button_setting" then  -- 设置  
        local layer = GameSetLayer.new(204);
        self:addChild(layer);
        layer:setScale(1 / display.scaleX, 1 / display.scaleY);
    elseif name == "button_helpX" then -- 规则 
        local layer = BmwRuleLayer.new()
        self:addChild(layer);
        layer:setLocalZOrder(100)
    elseif name == "button_exit" then -- 退出  
        --        g_GameController:exitReq()
    elseif name == "auto_btn_other" then
        local ChatLayer = ChatLayer.new()
        self:addChild(ChatLayer);
      --  g_GameController:gamePlayerOnlineListReq()
    elseif name == "button_back" then
        if BMWData.getInstance().m_nGameState ~= BMWConfig.STATE_FREE and BMWData.getInstance():isBet() then
            TOAST("游戏中无法退出！");
            return;
        end
        g_GameController:exitReq()
    elseif name == "auto_btn_more" then
        local GameRecordLayer = GameRecordLayer.new(2, 204)
        self:addChild(GameRecordLayer)
        ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", { 204 })
    elseif subStr == "btn_bet_" then

        local name_prefix = string.sub(name, 9, #name)

        self.cur_bet_index = tonumber(name_prefix)

        self:update_select_btn()
    elseif subStr1 == "bet_grid_" then

        --下注
        local name_prefix = string.sub(name, 10, #name)

        local pos = tonumber(name_prefix)

        self:request_xiazhu(pos)

    elseif name == "btn_continue" then
        --续投
        self:onContinueClick()

    elseif name == "btn_menu_push" or name == "btn_panel_backMenu" then
        --隐藏
        if self.m_bNodeMenuMoving then return end
        self.m_bNodeMenuMoving = true

        local callback = cc.CallFunc:create(function()
            self.btn_menu_pop:setVisible(true)
            self.btn_menu_push:setVisible(false)
        end)

        local callback2 = cc.CallFunc:create(function()
            self.m_bNodeMenuMoving = false
        end)
        self.btn_panel_backMenu:setVisible(false)
        self.ImageView_2:stopAllActions()
        local aTime = 0.25
        local moveTo = cc.MoveTo:create(aTime, cc.p(0, 30))
        local sp = cc.Spawn:create(cc.EaseBackIn:create(moveTo), cc.FadeOut:create(aTime))
        local hide = cc.Hide:create()
        local seq = cc.Sequence:create(sp, callback, hide, cc.DelayTime:create(0.1), callback2, nil)
        self.ImageView_2:runAction(seq)
    elseif name == "btn_menu_pop" then
        --显示
        if self.m_bNodeMenuMoving then return end
        self.m_bNodeMenuMoving = true

        self.btn_panel_backMenu:setVisible(true)
        self.panel_backMenu:setVisible(true)
        self.panel_backMenu:setLocalZOrder(100)
        self.ImageView_2:setVisible(true)
        self.ImageView_2:setPosition(cc.p(0, 30))

        local callback = cc.CallFunc:create(function()
            self.btn_menu_pop:setVisible(false)
            self.btn_menu_push:setVisible(true)
        end)

        local callback2 = cc.CallFunc:create(function()
            self.m_bNodeMenuMoving = false
        end)

        self.ImageView_2:stopAllActions()
        self.ImageView_2:setOpacity(0)
        local aTime = 0.25
        local moveTo = cc.MoveTo:create(aTime, cc.p(0, 0))
        local show = cc.Show:create()
        local sp = cc.Spawn:create(cc.EaseBackOut:create(moveTo), cc.FadeIn:create(aTime))
        local seq = cc.Sequence:create(show, sp, callback, cc.DelayTime:create(0.1), callback2)
        self.ImageView_2:runAction(seq)
    end
end
function BmwGameLayer:setupViews()

    --  Audio:getInstance():playBgm();
    g_AudioPlayer:playMusic(BMWRes.Audio.BACKGROUND, true);
    local winSize = cc.Director:getInstance():getWinSize();
    self.root_node = UIAdapter:createNode(BMWRes.CSB.MAIN_LAYER);
    self:addChild(self.root_node);
    UIAdapter:adapter(self.root_node, handler(self, self.onTouchCallback))
    UIAdapter:praseNode(self.root_node, self)
    local center = self.root_node:getChildByName("Scene")
    local diffY = (display.size.height - 750) / 2
    self.root_node:setPosition(cc.p(0, diffY))

    local diffX = 145 - (1624 - display.size.width) / 2
    center:setPositionX(diffX)
    self:loading_res()
    self:update_state_text(1)
    self:update_continue_btn(false)
    self.posX = self.ImageView_2:getPositionX()
    self.backWidth = self.ImageView_2:getContentSize().width
    self.panel_backMenu:setSwallowTouches(false)
    self.panel_backMenu:setVisible(false)

    for i = 1, 6 do

        local btn_bet_ = self.auto_fb_panel:getChildByName("btn_bet_" .. i):getChildByName("image_circle")
        btn_bet_:setVisible(false)
        btn_bet_:runAction(cc.RepeatForever:create(cc.RotateBy:create(1, 360)))

    end
    self:update_select_btn()
    self._userListLayer = PlayerListLayer.new();
    self._userListLayer:setVisible(false)
    self:addChild(self._userListLayer);
    self._userListLayer:setLocalZOrder(1000)
    self:showAnimationWithName(self.Node_3, "benchibaoma_changjingtexiao", "Animation1", nil, true)

    self.btn_continue:setColor(cc.c3b(160, 160, 160))
    self.btn_continue:setEnabled(false)


    self.mTextRecord = UIAdapter:CreateRecord()
    center:addChild(self.mTextRecord)
    local button_back = center:getChildByName("button_back")
	local buttonsize = button_back:getContentSize()
	local buttonpoint = cc.p(button_back:getPosition())
	self.mTextRecord:setPosition(cc.p(buttonpoint.x - buttonsize.width / 2, buttonpoint.y + buttonsize.height / 2 + 20))


    --缓存管理添加委托构造筹码方法
    CChipCacheManager.getInstance():setCreateChipDelegate(function(betValue)
        for index = 1, #self.game_bet_list do
            if self.game_bet_list[index] == betValue then
                local item = cc.Sprite:create(BMWRes.Image["CHIP_" .. index]);
                self.m_pNodeFlyIcon:addChild(item);
                return item;
            end
        end
        local item = cc.Sprite:create(BMWRes.Image.CHIP_6);
        self.m_pNodeFlyIcon:addChild(item);
        return item;
    end);

    --获取庄家位置
    --local worldPos = self.m_pAnimalUI:convertToWorldSpace(cc.p(0,0))
    self.m_bankerPos = cc.p(0, 0) --self.m_pNodeFlyIcon:convertToNodeSpace(worldPos)

    -- 随机筹码位置
    for i = 1, BMWConfig.AREA_COUNT do
        local bet_grid = self.auto_mid_table:getChildByName("bet_grid_" .. i)
        local node = bet_grid:getChildByName("area_box_" .. i)

        local size = node:getContentSize();

        local worldPos = bet_grid:convertToWorldSpace(cc.p(node:getPosition()))
        local layerPos = self.m_pNodeFlyIcon:convertToNodeSpace(worldPos)

        local maxX = size.width
        local minX = 0
        local maxY = size.height
        local minY = 0

        self.m_areaPos[i] = {}

        for n = 1, 60 do
            local randomX = math.random(minX, maxX)
            local randomY = math.random(minY, maxY)

            local random_pos = cc.p(randomX + layerPos.x, randomY + layerPos.y)

            table.insert(self.m_areaPos[i], random_pos)
        end

    end

    for index = 1, 8 do
        if index == 8 then

            local world_pos = center:convertToWorldSpace(cc.p(self.auto_btn_other:getPosition()))

            self.m_playerPos[index] = self.m_pNodeFlyIcon:convertToNodeSpace(world_pos)

        else

            local user_panel = self.auto_mid_panel_user:getChildByName("user_node_pos_" .. index)

            local world_pos = self.auto_mid_panel_user:convertToWorldSpace(cc.p(user_panel:getPosition()))

            self.m_playerPos[index] = self.m_pNodeFlyIcon:convertToNodeSpace(world_pos)

        end
    end

    return true;
end
function BmwGameLayer:showAnimationWithName(node, name, animationName, callback, notCleanFlag)
    local pos = cc.p(node:getPosition())
    local armature = ccs.Armature:create(name)
    --armature:setPosition(pos) 
    node:addChild(armature)
    armature:getAnimation():play(animationName, -1, -1)
    return armature
end
function BmwGameLayer:updateOnlineUserList(__info)
    self._userListLayer:updateUserList(__info);
    self._userListLayer:setVisible(true);
end
function BmwGameLayer:init_light()

    for i = 0, 27 do

        local light = self["Image_light_" .. i]:getChildByName("Image_light")

        light:setLocalZOrder(-1)

    end

end
function BmwGameLayer:loading_res()

--[[  local t_res = {

        game_res_id .."/game_img/game_img_plist",
        aniPath .. "game_star/game_star0",

        aniPath .. "game_end/game_end0",
        aniPath .. "feiqinzoushou_jiesuandongwu/feiqinzoushou_jiesuandongwu0",
        aniPath .. "benchibaoma_changjingtexiao/benchibaoma_changjingtexiao0",
    }

    for _ , res in ipairs(t_res) do

        cc.SpriteFrameCache:getInstance():addSpriteFrames(res .. ".plist",res .. ".png") 

    end   
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(aniPath .. "game_star/game_star.ExportJson")
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(aniPath .. "feiqinzoushou_jiesuandongwu/feiqinzoushou_jiesuandongwu.ExportJson")
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(aniPath .. "benchibaoma_changjingtexiao/benchibaoma_changjingtexiao.ExportJson")
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(aniPath .. "game_end/game_end.ExportJson")
    ]]
end

function BmwGameLayer:onCleanup()
    -- self:clearEvent()
    --清除池缓存
    CChipCacheManager.getInstance():Clear(true)
    CChipCacheManager.getInstance():resetChipZOrder()

    AudioManager:getInstance():stopAllSounds()
    AudioManager:getInstance():stopMusic()


    -- 释放动画
    for _, strPathName in pairs(BMWRes.vecReleaseAnim) do
        --local strJsonName = string.format("%s%s/%s.ExportJson", Lhdz_Res.strAnimPath, strPathName, strPathName)
        ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo(strPathName)
    end
    -- 释放整图
    for _, strPathName in pairs(BMWRes.vecReleasePlist) do
        display.removeSpriteFrames(strPathName[1], strPathName[2])
    end
    -- 释放背景图
    for _, strFileName in pairs(BMWRes.vecReleaseImg) do
        display.removeImage(strFileName)
    end
    -- 释放音频
    for _, strFileName in pairs(BMWRes.vecReleaseSound) do
        AudioManager.getInstance():unloadEffect(strFileName)
    end
    
end
function BmwGameLayer:update_continue_btn(notEnabled)

    if #self.continue_bet_array > 0 then

        self.btn_bet_continue:setEnabled(notEnabled)

    else

        self.btn_bet_continue:setEnabled(false)

    end

end

--  初始化游戏场景
function BmwGameLayer:init_BmwGameLayer(scene_type, response)

    rawset(_G, "GAME_STATE", response["m_state"])

    self.scene_type = scene_type

end


function BmwGameLayer:init_game_ui()

end
---
function BmwGameLayer:close_scene()

    self:clear_scene()

end
function BmwGameLayer:showRunTime(time)

    self._allTime = time;
    self._nowTime = time;
    self._fnowTime = time;
    if self._nowTime <= 0 then
        self._nowTime = 0
        -- self.daojishi_time:setVisible(false);
        --      self._gameStateBg:setVisible(false);	
        self.time_f_1:setString("0");
        self.time_f_2:setString("0");
    end
    local t1 = 0
    local t2 = 0
    if self._nowTime >= 10 then
        t1 = 1
        t2 = self._nowTime - 10
    else
        t1 = 0
        t2 = self._nowTime
    end
    self.time_f_1:setString(string.format("%d", t2));
    self.time_f_2:setString(string.format("%d", t1));
    if self._Scheduler1 then
        Scheduler.unscheduleGlobal(self._Scheduler1)
        self._Scheduler1 = nil
    end

    if (time <= 0) then
        --	self._gameStateBg:setVisible(false); 
    else
        --self.daojishi_time:setVisible(true);
        --	self._gameStateBg:setVisible(true);
        self._Scheduler1 = Scheduler.scheduleGlobal(handler(self, self.updateGameTime), 1)
    end
end
function BmwGameLayer:updateGameTime(dt)


    self._nowTime = self._nowTime - 1;

    if self._nowTime <= 0 then
        self._nowTime = 0
    end
    local t1 = 0
    local t2 = 0
    if self._nowTime >= 10 then
        t1 = 1
        t2 = self._nowTime - 10
    else
        t1 = 0
        t2 = self._nowTime
    end
    self.time_f_1:setString(string.format("%d", t2));
    self.time_f_2:setString(string.format("%d", t1));
    if (self._nowTime <= 0) then
        self._nowTime = 0;

        if self._Scheduler1 then
            Scheduler.unscheduleGlobal(self._Scheduler1)
            self._Scheduler1 = nil
        end

    end
end
function BmwGameLayer:showLeftTime(time)
    self.daojishi_img:setVisible(true);
    self:showRunTime(time)

    print("倒计时===============", self.table_state, self.is_already_run, time)
    if self.table_state == 3 and self.is_already_run == nil then

        self.is_already_run = 1

        self:add_light_speed(time)

    end

end

function BmwGameLayer:openAward(response)

    if response == nil then return end

    self.winBetPos = response.m_awardBetAreaId

    self.dest_track = response.m_openGridId

    g_AudioPlayer:playEffect(BMWRes.Audio.END_BET);

    self:_PlayAni("game_end")

    self:showGameEndView(response)
    -- self:game_end_coin_change( response )         
end

function BmwGameLayer:add_light_speed(leftTime)

    if leftTime and leftTime < 9 then

        return

    end

    --    if not self.last_winPos or self.last_winPos < 0 then
    --        self.last_winPos = math.random( 0 , 27 )
    --    end
    local quanImg = { "quancs.png", "quanhs.png", "quanls.png", "quanzs.png" }
    local nowWinPos = 1

    local toWinPos = self.dest_track or 6

    self.last_winPos = 1

    local count = 0

    --    if nowWinPos > toWinPos then
    --        count = 28 - nowWinPos + toWinPos
    --    else
    --        count = toWinPos - nowWinPos
    --    end
    if self.auto_mid_table:getChildByTag(100) then

        self.auto_mid_table:getChildByTag(100):removeFromParent()

    end
    local sum = 32 * 3 + self.dest_track

    --2s加速  
    local action = {}

    local timer = 0.4

    self.fadeTime = 0.4

    local colorIndex = 1

    local fadeIndex = 1

    local dex_sum = 0

    if leftTime > 5 and leftTime < 5 + (sum - 13) * 0.03 then

        dex_sum = sum - 6

        sum = 6

        timer = 0.2

        self.fadeTime = 0.25

    elseif leftTime >= 5 + (sum - 13) * 0.03 and leftTime <= 5 + (sum - 13) * 0.03 + 1.5 then

        dex_sum = 7

        sum = sum - 7

        timer = 0.02

        self.fadeTime = 0.25

    end

    for j = 1, sum do

        local indexPos = nowWinPos + j - 1

        if indexPos > 31 then

            indexPos = indexPos % 32

        end

        local item = self["auto_mid_table"]:getChildByName("item_" .. indexPos)

        action[#action + 1] = cc.CallFunc:create(function()

            local light = cc.Sprite:create(img_path .. quanImg[colorIndex])



            self.auto_mid_table:addChild(light, 100, 100)

            light:setPosition(cc.p(item:getPositionX(), item:getPositionY() + 1))

            light:setScale(1.1)

            if fadeIndex < sum then

                if fadeIndex % 3 == 0 or fadeIndex >= sum - 6 or fadeIndex <= 4 then

                    g_AudioPlayer:playEffect(BMWRes.Audio.CAR_TURN, 0);

                end

                light:runAction(cc.Sequence:create({ cc.FadeOut:create(self.fadeTime), cc.RemoveSelf:create() }))

            else

                g_AudioPlayer:playEffect(BMWRes.Audio.CAR_TURN_END, 0);
                --闪光动画
                if g_GameController:getOpenAward() then
                    --self:showSitWinNumber( g_GameController:getOpenAward() ) 
                    self:update_trand(g_GameController:getOpenAward().m_history)
                    self:play_pos_animation()
                    --self:flychipex()
                    self:_DoSomethingLater(function() self:_FlyChipEX() end, 3)
                end


            end

            colorIndex = colorIndex + 1
            if colorIndex > 4 then
                colorIndex = 1
            end

            fadeIndex = fadeIndex + 1

            if fadeIndex > sum - 6 then

                self.fadeTime = self.fadeTime + 0.15

            else
                self.fadeTime = self.fadeTime - 0.05

                if self.fadeTime <= 0.35 then

                    self.fadeTime = 0.35

                end

                if fadeIndex == sum - 6 then

                    self.fadeTime = 0.35

                end
            end

        end)

        action[#action + 1] = cc.DelayTime:create(timer)



        if j > sum - 6 then

            timer = timer + 0.15

        else

            timer = timer - 0.1

            if timer <= 0.03 then

                timer = 0.03

            end

            if j == sum - 6 then

                timer = 0.3

            end
        end

    end

    self.root_node:runAction(cc.Sequence:create(action))

end
function BmwGameLayer:play_ani_spine(node, spine_data, animation_name, func, offset_pos, delayTime)

    local spineAnim = sp.SkeletonAnimation:create(spine_data.json, spine_data.atlas)

    local size = node:getContentSize()

    if offset_pos then
        spineAnim:setPosition(cc.p(size.width / 2 * display.scaleX + offset_pos.x, size.height / 2 + offset_pos.y))
    else
        spineAnim:setPosition(cc.p(size.width / 2 * display.scaleX, size.height / 2))
    end

    if not animation_name then
        animation_name = "animation"
    end

    spineAnim:setAnimation(0, animation_name, false);

    if not delayTime then

        delayTime = 1

    end

    node:addChild(spineAnim, 3, 9000)

    node:runAction(cc.Sequence:create(
    cc.DelayTime:create(delayTime),
    cc.CallFunc:create(function()
        spineAnim:removeFromParent()

    end)
    ))
end
function BmwGameLayer:play_end_animation(item)

    local chebiao_win = sp.SkeletonAnimation:create(aniPath .. "daojishi_car/chebiao_tx.json", aniPath .. "daojishi_car/chebiao_tx.atlas")

    local pos = cc.p(item:getPosition())

    chebiao_win:setPosition(cc.p(pos.x, pos.y))

    chebiao_win:setLocalZOrder(0)

    self.auto_mid_table:addChild(chebiao_win, 150, 150)

    chebiao_win:setAnimation(0, "animation", true)
    chebiao_win:setScale(1.2)

    self.auto_mid_table:runAction(cc.Sequence:create(
    cc.DelayTime:create(1.2),
    cc.CallFunc:create(function()

        chebiao_win:removeFromParent()

    end)
    ))

    --下注地方动画
    local area_idx = dest_track

    local bet_img_path = {["1"] = "bet_img_1", ["2"] = "bet_img_3", ["3"] = "bet_img_5", ["4"] = "bet_img_7", ["5"] = "bet_img_2", ["6"] = "bet_img_4", ["7"] = "bet_img_6", ["8"] = "bet_img_8" }

    local oofX = self["bet_grid_" .. area_idx]:getContentSize().width * 0.5

    local oofY = self["bet_grid_" .. area_idx]:getContentSize().height * 0.5

    local sizebet_grid = cc.size()
    local srcPos = cc.p(self["bet_grid_" .. area_idx]:getPositionX() - oofX + self["bet_grid_" .. area_idx]:getChildByName("logo"):getPositionX(),
    self["bet_grid_" .. area_idx]:getPositionY() - oofY + self["bet_grid_" .. area_idx]:getChildByName("logo"):getPositionY())

    -- local toPos = self["auto_mid_panel_coin"]:convertToNodeSpace(srcPos)
    local imageS = ""

    local bet_icon_img = ""

    if tonumber(area_idx) == 1 or tonumber(area_idx) == 5 then

        imageS = ccui.ImageView:create(img_path .. "dun_light.png")

    else

        imageS = ccui.ImageView:create(img_path .. "yuan_light.png")

    end

    if tonumber(area_idx) >= 5 then

        imageS:setScale(0.8)

    end

    print("dzf...bet_img_path[area_idx]:", bet_img_path[area_idx])
    bet_icon_img = ccui.ImageView:create("80000042/game_img/" .. bet_img_path[area_idx] .. ".png", 1)

    -- imageS:setPosition(cc.p(bet_icon_img:getContentSize().width*0.5,bet_icon_img:getContentSize().height*0.5))
    imageS:setAnchorPoint(cc.p(0.5, 0.5))

    imageS:setPosition(srcPos)



    -- bet_icon_img:addChild(imageS)
    bet_icon_img:setAnchorPoint(cc.p(0.5, 0.5))

    bet_icon_img:setPosition(srcPos)

    self["auto_mid_panel_coin"]:addChild(bet_icon_img, 10000, 10000)
    self["auto_mid_panel_coin"]:addChild(imageS, 10001, 10001)

    imageS:runAction(cc.RepeatForever:create(cc.Sequence:create({ cc.FadeTo:create(0.5, 125), cc.FadeTo:create(0.5, 225) })))

    self.auto_mid_table:getChildByName("table_info_msg"):getChildByName("new_tip"):setVisible(false)

    -- self.root_node:runAction( cc.Sequence:create( cc.DelayTime:create(4) , cc.CallFunc:create( function() 
    --     imageS:stopAllActions()
    --     bet_icon_img:removeFromParent()
    --     imageS:removeFromParent()
    -- end)))
end

function BmwGameLayer:get_path_from_idx(index)

    local dex = self:get_trand_index(index)

    return game_light_res .. "light_" .. dex .. ".png"

end

function BmwGameLayer:game_show_bet_state(data)

    get_room_info().loop = data.loop

    for i = 1, #data.loop do

        local path = self:get_path_from_idx(data.loop[i])

        self["Image_light_" .. (i - 1)]:loadTexture(path)

    end

end

function BmwGameLayer:get_new_array_by_array(array1, array2)

    local array3 = {}

    for i = 1, #array1 do

        local new_betValue = array2[i] - array1[i]

        array3[i] = new_betValue

    end

    return array3

end

function BmwGameLayer:get_coins_array_by_coins(coins, index, coins_array)

    local bet_value = self.game_bet_list[index]

    if not bet_value then

        return coins_array

    end

    if coins >= bet_value then

        insert_table(coins_array, bet_value)

        coins = coins - bet_value

    else

        index = index - 1

    end

    self:get_coins_array_by_coins(coins, index, coins_array)

end

function BmwGameLayer:updateMyCoin(coin)

    local myScore = self["auto_self_panel"]:getChildByName("my_coin")

    self.my_coin_num = coin

    api_label_setString_num(myScore, coin)

end

--游戏公告推送
function BmwGameLayer:show_nitify_info(data)

    print("游戏公告=================")

    if self.richText then

        self.richText:stopAllActions()

        self.richText:removeFromParent()

    end

    self.richText = api_create_richText(data)

    self.richText:setPosition(1300, 17)

    self.auto_Panel_notify:addChild(self.richText)

    self.richText:runAction(cc.Sequence:create({

        cc.CallFunc:create(function()

            self.auto_panal_notify:setVisible(true)

        end),
        cc.MoveBy:create(20, cc.p(-1800, 0)),
        cc.CallFunc:create(function()

            self.auto_panal_notify:setVisible(false)

            self.richText:removeFromParent()

            self.richText = nil

        end)
    }))

end

function BmwGameLayer:slider_changed(_sender)

    self.super.slider_changed(self, _sender)

    local name = _sender:getName()

    if name == "bet_slider" then

        local pos = cc.p(self.bet_slider:getSlidBallRenderer():getPosition())

        self.bet_value_bg:setPositionX(pos.x + 130)

        local percent = _sender:getPercent()

        local diff = self.max_bet_gold - self.min_bet_gold

        local num = self.min_bet_gold

        if percent > 0 then

            num = math.floor((self.min_bet_gold + percent * diff / 100) / 10) * 10

        end

        self.bet_value_slider:setString("" .. num)

    end

end

function BmwGameLayer:touch_began(_sender, _type)

    self.super.touch_began(self, _sender, _type)

    local name = _sender:getName()

    local subStr = string.sub(name, 1, 11)

    if subStr == "bet_grid_" then

        local Image_light = _sender:getChildByName("Image_light")

        Image_light:setOpacity(255)

        Image_light:setVisible(true)

    end

end

function BmwGameLayer:touch_move(_sender, _type)

    self.super.touch_move(self, _sender, _type)

end

function BmwGameLayer:touch_ended(_sender, _type)

    g_audio:play_effects()

    if self.super.touch_ended(self, _sender, _type) == true then

        return
        --- 基类拦截消息
    end

    local name = _sender:getName()

    local subStr = string.sub(name, 1, 11)

    if name == "auto_bt_start" then
        ----游戏开始
        g_game_proto:request_user_ready()

    elseif name == "btn_bet_continue" then

        for i = 1, #self.continue_bet_array do

            local bet_array = self.continue_bet_array[i]

            g_net:ws_request(GAMEROOM_COMMAND.MSG_GAME_USER_BET_REQ, bet_array)

        end

        self:update_continue_btn(false)

    elseif name == "auto_bt_quit" then
        ----退出房间
        local confirm = function()

            g_game_proto:request_user_quit()

        end

        g_notifyer:notify_ui_event("ui_notify_open_msg_box", {["type"] = msgbox_type.confirm, ["text"] = "是否要退出游戏?", ["fairy"] = fairy_type.cry, ["confirm"] = confirm })

    elseif name == "auto_btn_other" then

        g_net:ws_request(GAMEROOM_COMMAND.MSG_GAME_GET_OFFSET_PLAYER_ACK, {["show_ani"] = 0, ["m_accountId"] = get_my_id() })

    elseif name == "auto_bt_help" then

        game_rule.new(self.root_node, game_id)

        self.auto_panel_set:setVisible(false)

    elseif name == "auto_bt_set" then

        local setView = _zp_setting.new(self.root_node)

        setView:setLocalZOrder(10015)

        self.auto_panel_set:setVisible(false)

    elseif name == "auto_btn_more" then

        -- if self.auto_panel_set:isVisible() == false then
        --     self.auto_panel_set:setVisible( true )
        -- else
        --     self.auto_panel_set:setVisible( false )
        -- end
        game_more_info_view.new(self.root_node, game_id, 1)

    elseif name == "auto_panel_set" then

        self.auto_panel_set:setVisible(false)

    elseif name == "auto_btn_back" then

        local confirm = function()

            g_net:ws_request(FRAME_COMMAND.MSG_USER_LEAVE_ROOM_REQUEST, {})

        end

        g_notifyer:notify_ui_event("ui_notify_open_msg_box", {["type"] = 2, ["text"] = "是否要退出游戏?", ["confirm"] = confirm })

    elseif subStr == "bet_grid_" then

        --下注
        local Image_light = _sender:getChildByName("Image_light")

        Image_light:setVisible(false)

        local name_prefix = string.sub(name, 12, #name)

        local pos = tonumber(name_prefix)

        self:request_xiazhu(pos)

    elseif name == "btn_xiazhu_6" then

        self:setCustomizeNum(_sender)

    elseif subStr == "btn_xiazhu_" then

        local name_prefix = string.sub(name, 12, #name)

        self.cur_bet_index = tonumber(name_prefix)

        self:update_select_btn()

    end

end

--设置自定义筹码
function BmwGameLayer:setCustomizeNum(sender)

    local bet_value = sender:getChildByName("txt_num")

    if self.isCustomize then

        self.isCustomize = false

        --self.bet_slider_bg:setRotation( 0 )
        self.bet_slider_bg:setVisible(true)

        bet_value:setVisible(false)

        self.Image_zidingyi:setVisible(true)

    else

        self.isCustomize = true

        self.bet_slider_bg:setVisible(false)

        local num = tonumber(self.bet_value_slider:getString())

        bet_value:setVisible(true)

        bet_value:setString("" .. num)

        self.Image_zidingyi:setVisible(false)

        self.cur_bet_index = 6

        self:update_select_btn()

    end

end


function BmwGameLayer:request_xiazhu(pos)

    if self:check_xiazhu() then

        local value = self.game_bet_list[self.cur_bet_index]

        if self.cur_bet_index == 6 then

            value = tonumber(self.bet_value_slider:getString()) or 1

        end
        ConnectManager:send2GameServer(g_GameController.m_gameAtomTypeId, "CS_C2G_Bmw_Bet_Req", { pos, value })
        --  g_game_proto:request_user_bet( pos - 1 , value )
    end

end

function BmwGameLayer:check_xiazhu()

    if self.table_state ~= 2 then
        TOAST("无法下注，请在下注时间进行下注！")


        return false

    end

    return true

end

function BmwGameLayer:touch_cancel(_sender, _type)

    self.super.touch_cancel(self, _sender, _type)

    local name = _sender:getName()

    local subStr = string.sub(name, 1, 11)

    if subStr == "bet_grid_" then

        local Image_light = _sender:getChildByName("Image_light")

        Image_light:setVisible(false)

    end

end

function BmwGameLayer:init_ui()

end

function BmwGameLayer:get_user_index_position(index)

    local offx = game_adapter:getAdapterOffX()

    if index == 7 then

        local auto_self_panel = ccui.Helper:seekWidgetByName(self.auto_panel, "auto_self_panel")

        auto_self_panel.self_head = ccui.Helper:seekWidgetByName(auto_self_panel, "self_head")

        local x, y = auto_self_panel.self_head:getPosition()

        return x + offx, y

    elseif index == 8 then

        local auto_btn_other = ccui.Helper:seekWidgetByName(self.auto_panel, "auto_btn_other")

        local x, y = auto_btn_other:getPosition()

        return x + offx, y

    else

        local user_panel = ccui.Helper:seekWidgetByName(self.auto_panel, "auto_user_portrait_" .. index)

        --return user_panel:getPosition()
        local x, y = user_panel:getPosition()

        return x + offx, y

    end

end

--清理自己输赢值
function BmwGameLayer:clear_round_win()

    for i = 1, 4 do

        self["auto_self_win_des_" .. i]:setVisible(false)

    end

end

--游戏结算
function BmwGameLayer:game_end_coin_change(response)

    self.game_end_data = response

    local win_coin_arr = {}

    local fail_coin_arr = {}

    for key, coin in pairs(self.table_coin_node_array) do

        local isWin = false

        for k, v in pairs(response.m_awardBetAreaId) do

            if coin and coin.tag == v + 1 then

                isWin = true

            end

        end

        if isWin then

            win_coin_arr[#win_coin_arr + 1] = coin

        else

            fail_coin_arr[#fail_coin_arr + 1] = coin

        end

    end

    self.func2 = function()

        if #win_coin_arr > 0 then

            g_AudioPlayer:playEffect("gold_jiesuan")

            for i = 1, #win_coin_arr do

                local to_pos = cc.p(self:get_user_index_position(win_coin_arr[i].index))

                local rand = math.random(3, 6) / 10

                local array = {}

                local uAction = cc.JumpTo:create(rand, to_pos, math.random(20, 50), 1)

                local ease = cc.EaseSineInOut:create(uAction)

                array[#array + 1] = ease

                array[#array + 1] = cc.CallFunc:create(function()

                    win_coin_arr[i]:setVisible(false)

                end)

                if i == #win_coin_arr then

                    array[#array + 1] = cc.DelayTime:create(0.1)


                    array[#array + 1] = cc.CallFunc:create(function()

                        self:showGameEndView()

                    end)

                end

                win_coin_arr[i]:runAction(cc.Sequence:create(array))

            end

        else

            self:showGameEndView()

        end

    end

    self.func1 = function()

        for k, v in pairs(fail_coin_arr) do

            if v then

                v:setVisible(false)

            end

        end

        self.func2()

    end

    self.func1()

end

function BmwGameLayer:showGameEndView(response)

    --    self:play_open_award( self.winBetPos ) 
    --self:showSitWinNumber( response )
    --self:clear_scene()
end

--清理场景
function BmwGameLayer:clear_scene()

    self.totalBet = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }

    self:clear_bet_text()

    self:clear_Coin()

    --  self:clear_light()
    --  self:clear_btn_light()
    self.is_already_run = nil

    self:update_bet_total_score(0)

end

function BmwGameLayer:clear_bet_text()
    for k = 1, 8 do
        local total_score = self["bet_grid_" .. k]:getChildByName("total_bet_value_" .. k)
        total_score:setString("0")
        local my_score = self["bet_grid_" .. k]:getChildByName("my_bet_value_" .. k)
        my_score:setString("0")
    end



end

--清理金币
function BmwGameLayer:clear_Coin()

    if #self.table_coin_node_array > 0 then

        for i = 1, #self.table_coin_node_array do

            if self.table_coin_node_array[i] then

                --self.table_coin_node_array[i]:removeFromParent()
                local item = self.table_coin_node_array[i];
                item:setVisible(false);
                CChipCacheManager.getInstance():putChip(item, item.m_nBetValue)
            end

        end

        self.table_coin_node_array = {}

    end

    if #self.m_betChips > 0 then
        for i = 1, #self.m_betChips do
            local item = self.m_betChips[i]
            item:setVisible(false)
            CChipCacheManager.getInstance():putChip(item, item.m_nBetValue)
        end
    end
    self.m_betChips = {}

    CChipCacheManager.getInstance():resetChipZOrder()
end


--清理灯
function BmwGameLayer:clear_light()

    for i = 0, 27 do

        local light = self["Image_light_" .. i]:getChildByName("Image_light")

        light:stopAllActions()

        light:setVisible(false)

    end

end

function BmwGameLayer:clear_btn_light()

    for i = 1, 11 do

        local Image_light = self["bet_grid_" .. i]:getChildByName("Image_light")

        Image_light:stopAllActions()

        Image_light:setOpacity(255)

        Image_light:setVisible(false)

    end

end

--开奖动画
function BmwGameLayer:play_open_award(info)

    if not info then return end

    for i = 1, 11 do

        local is_win = false

        for k, v in pairs(info) do

            if i == v + 1 then

                is_win = true

                break

            end

        end

        if is_win then

            local Image_light = self["bet_grid_" .. i]:getChildByName("Image_light")

            local array = {}

            array[#array + 1] = cc.CallFunc:create(function() Image_light:setVisible(true) end)

            array[#array + 1] = cc.Blink:create(5, 5)

            array[#array + 1] = cc.CallFunc:create(function() Image_light:setVisible(false) end)

            Image_light:runAction(cc.Sequence:create(array))

        end

    end

end
function BmwGameLayer:play_pos_animation()

    if not self.dest_track then

        return

    end
    if g_GameController:getOpenAward() == nil then
        return
    end
    local index = g_GameController:getOpenAward().m_awardType
    -- 1:大保时捷  2：大宝马  3:大奔驰  4:大大众 5:小保时捷 6:小宝马 7:小奔驰 8:小大众'
    if index == 2 then --熊猫动画

        g_AudioPlayer:playEffect(BMWRes.Audio.REWARDINGEFFECT2)
        self:_PlayAni("feiqinzoushou_jiesuandongwu", nil, 6)

    elseif index == 5 then --小保时捷动画

        g_AudioPlayer:playEffect(BMWRes.Audio.REWARDINGEFFECT0)
        self:_PlayAni("feiqinzoushou_jiesuandongwu", nil, 3)

    elseif index == 4 then --狮子动画

        g_AudioPlayer:playEffect(BMWRes.Audio.REWARDINGEFFECT6)
        self:_PlayAni("feiqinzoushou_jiesuandongwu", nil, 4)

    elseif index == 3 then --兔子动画

        g_AudioPlayer:playEffect(BMWRes.Audio.REWARDINGEFFECT4)
        self:_PlayAni("feiqinzoushou_jiesuandongwu", nil, 5)

    elseif index == 7 then --猴子动画
        g_AudioPlayer:playEffect(BMWRes.Audio.REWARDINGEFFECT4)
        self:_PlayAni("feiqinzoushou_jiesuandongwu", nil, 1)

    elseif index == 1 then --大保时捷

        g_AudioPlayer:playEffect(BMWRes.Audio.REWARDINGEFFECT0)
        self:_PlayAni("feiqinzoushou_jiesuandongwu", nil, 7)

    elseif index == 6 then --孔雀动画

        g_AudioPlayer:playEffect(BMWRes.Audio.REWARDINGEFFECT2)
        self:_PlayAni("feiqinzoushou_jiesuandongwu", nil, 2)

    elseif index == 8 then --鸽子动画

        g_AudioPlayer:playEffect(BMWRes.Audio.REWARDINGEFFECT6)
        self:_PlayAni("feiqinzoushou_jiesuandongwu", nil, 0)

    end


end
--获取筹码结束坐标
function BmwGameLayer:get_pos_by_index(nIndex)

    local index = nIndex

    local node_pos = nil

    local pos = nil

    local node_pos = self.Panel_bet_table:getChildByName("bet_grid_" .. index)

    local x, y = node_pos:getPosition()

    pos = self.Panel_bet_table:convertToWorldSpace(cc.p(x * display.scaleX, y * display.scaleY))

    if index == 1 or index == 7 then --飞禽 or 走兽

        pos.x = pos.x + math.floor(math.random(0, 60)) + 40

        pos.y = pos.y + math.floor(math.random(0, 50)) - 25

    elseif index == 6 then --鲨鱼

        pos.x = pos.x + math.floor(math.random(0, 150)) - 75

        pos.y = pos.y + math.floor(math.random(0, 60)) - 30

    else  --其他

        pos.x = pos.x + math.floor(math.random(0, 80)) - 40

        pos.y = pos.y + math.floor(math.random(0, 50)) - 25

    end

    return pos

end

--初始化下注按钮数值
function BmwGameLayer:init_bet_btn_value()
    for i = 1, 5 do

        self["btn_bet_" .. i]:getChildByName("bet_value"):setString(self.game_bet_list[i] * 0.01)

    end


end

--初始化下注区赔率
function BmwGameLayer:init_bet_count(info)

    if not info then return end

    for k, v in pairs(info) do

        if self["bet_grid_" .. v.m_betAreaId] then

            local count_num = self["bet_grid_" .. v.m_betAreaId]:getChildByName("muilt" .. (k - 1))
            count_num:setString("" .. v.m_multiple)

        end
        self.m_area_multiple[v.m_betAreaId] = v.m_multiple
        --        if k == 12 then
        --            self.jinsha_bet_num:setString( "" .. v )
        --        end
    end

end
function BmwGameLayer:get_logo_from_idx(idx)
    local path = ""
    -- local table = {['logo_4'] = {0,7,15,21} , ['logo_5'] =  {1,6,12,16} , ['logo_2'] = {2,14} , ['logo_7'] = {3,10,13,19} , ['logo_6'] = {4,9,18,22} , ['logo_0'] = {5,17} , ['logo_3'] = {8,20} , ['logo_1'] = {11,23} }
    local table = {['dz_1'] = { 0, 8, 16, 24,}, ['dz_2'] = { 1, 9, 17, 25 }, ['bsj_1'] = { 2, 10, 18, 26 }, ['bsj_2'] = { 3, 11, 19, 27 },
    ['bm_1'] = { 4, 12, 20, 28 }, ['bm_2'] = { 5, 13, 21, 29 }, ['bc_1'] = { 6, 14, 22, 30 }, ['bc_2'] = { 7, 15, 23, 31 } }

    for k, v in pairs(table) do
        for i = 1, #v do
            if v[i] == idx then
                return "80000042/game_img/" .. k .. ".png"
            end
        end
    end
end
--更新走势
function BmwGameLayer:update_trand(info)

    -- self.reward_record[ #self.reward_record + 1 ] = idx
    for i = 1, 7 do

        self["recod_img_" .. i]:setVisible(false)

    end

    self.auto_mid_table:getChildByName("table_info_msg"):getChildByName("new_tip"):setVisible(false)
    if #info == 7 then
        self.auto_mid_table:getChildByName("table_info_msg"):getChildByName("new_tip"):setVisible(true)
    end
    local index = 7
    local table = { "bsj_1", "bm_1", "bc_1", "dz_1", "bsj_2", "bm_2", "bc_2", "dz_2" }
    for k, v in pairs(info) do
        local path = "80000042/game_img/" .. table[v] .. ".png"
        self["recod_img_" .. k]:loadTexture(path, 1)
        self["recod_img_" .. k]:setVisible(true)
        self["recod_img_" .. k]:setScale(0.7)
    end

end

--获取输赢类型
function BmwGameLayer:get_trand_index(index)

    if index == 0 or index == 1 or index == 2 then

        return 11

    elseif index == 3 or index == 4 or index == 5 then

        return 8

    elseif index == 6 then

        return 12

    elseif index == 7 or index == 8 or index == 9 then

        return 6

    elseif index == 10 or index == 11 or index == 12 then

        return 7

    elseif index == 13 then

        return 2

    elseif index == 14 or index == 15 or index == 16 then

        return 3

    elseif index == 17 or index == 18 or index == 19 then

        return 5

    elseif index == 20 then

        return 9

    elseif index == 21 or index == 22 or index == 23 then

        return 4

    elseif index == 24 or index == 25 or index == 26 then

        return 10

    elseif index == 27 then

        return 1

    end

    return 8

end

--更新游戏状态文字
function BmwGameLayer:update_state_text(m_state)

    self.table_state = m_state
    self["table_state_img"]:setVisible(true);
    if m_state == 2 then --下注状态 0


        self["table_state_img"]:loadTexture("80000042/game_img/table_state_1.png", 1)

    elseif m_state == 3 then --结算状态 1
        self["table_state_img"]:loadTexture("80000042/game_img/table_state_2.png", 1)

    elseif m_state == 1 then --等待状态 2 
        self["table_state_img"]:loadTexture("80000042/game_img/dd3.png")
    end
    self:_RefBetButton()
end

--更新总下注分数
function BmwGameLayer:update_bet_total_score(score)

    local str = string.format("%.2f", score)

    self.sum_bet_score:setString(str)

end

--个人信息 info={name = "zhangsan",coin = 10001,m_accountId = "10001",headImg = ""}
function BmwGameLayer:update_myself_info(info)

    if info == nil then

        return

    end
    local img_head = self["user_node_1"]:getChildByName("image_head")

    local txt_name = self["user_node_1"]:getChildByName("txt_name")

    local txt_coin = self["user_node_1"]:getChildByName("txt_coin")

    --  local n_name = api_set_StringWithEllipsis( info.nickname , 5*myPanel.my_name:getFontSize() , myPanel.my_name:getFontSize() )
    txt_name:setString(info.m_nickname)

    self.my_coin_num = info.m_curCoin * 0.01
    txt_coin:setString(self.my_coin_num)
    local head = ToolKit:getHead(info.m_faceId)
    img_head:loadTexture(head, 1)
    img_head:setScale(0.7)



end

--更新自己分数
function BmwGameLayer:update_user_score(score)

    local txt_coin = self["user_node_1"]:getChildByName("txt_coin")

    self.my_coin_num = score

    txt_coin:setString(score)

end

--更新下注区总分数
function BmwGameLayer:update_total_xiazhu_score(info)

    if not info then return end

    self.totalBet = info

    local total = 0

    -- for k, v in pairs(info) do
    --     local total_score = self["total_bet_value_" .. k]
    --     total_score:setString("" .. (v.m_totalBet or 0) * 0.01)
    --     total = total + (v.m_totalBet or 0) * 0.01
    -- end
    for index = 1, #info do
        local total_score = self["total_bet_value_" .. index]
        total_score:setString("" .. (info[index] or 0) * 0.01)
        total = total + (info[index] or 0) * 0.01
    end


    self:update_bet_total_score(total)

end

--更新下注区自己分数
function BmwGameLayer:update_self_xiazhu_score(info)

    if not info then return end

    -- for k, v in pairs(info) do
    --     local my_score = self["my_bet_value_" .. v.m_betAreaId]
    --     my_score:setString(v.m_myBet * 0.01)
    -- end
    for index = 1, #info do
        local my_score = self["my_bet_value_" .. index];
        my_score:setString(info[index] * 0.01);
    end
end

-- 更新用户状态
function BmwGameLayer:update_users_info(seatInfos)

    if seatInfos == nil then

        return

    end

    for i = 1, 6 do

        local auto_user_portrait_ = self["user_node_" .. (i + 1)]

        local seatInfo = seatInfos[i]

        self:update_user_info(auto_user_portrait_, seatInfo)

    end

end

function BmwGameLayer:update_user_info(auto_user_portrait_, seatInfo)

    if seatInfo then

        auto_user_portrait_.m_accountId = seatInfo.m_accountId


        local head_img = auto_user_portrait_:getChildByName("image_head")

        local name_label = auto_user_portrait_:getChildByName("txt_name")

        local l_score_label = auto_user_portrait_:getChildByName("txt_coin")

        --   local n_name = api_set_StringWithEllipsis( seatInfo.nickname , 5*name_label:getFontSize() , name_label:getFontSize() )
        name_label:setString(seatInfo.m_nickname)
        l_score_label:setString((seatInfo.m_score or seatInfo.m_curCoin) * 0.01)
        --   api_label_setString_num( l_score_label , seatInfo.coin )
        if seatInfo.m_faceId then
            local head = ToolKit:getHead(seatInfo.m_faceId)
            head_img:loadTexture(head, 1)
            head_img:setScale(0.7)
        end

        auto_user_portrait_:setVisible(true)
    else

        auto_user_portrait_.m_accountId = nil

        auto_user_portrait_:setVisible(false)

    end

end

function BmwGameLayer:update_select_btn()

    for i = 1, 6 do

        local cur_btn = self["btn_bet_" .. i]

        local Image_bg = cur_btn:getChildByName("image_circle")

        Image_bg:setLocalZOrder(-1)

        if self.cur_bet_index == i then

            Image_bg:setVisible(true)

        else

            Image_bg:setVisible(false)

        end

    end

end

function BmwGameLayer:check_coins(coins)

    if self.my_coin_num >= coins + 1 then

        return true

    end

    return false

end

function BmwGameLayer:showSitWinNumber(data)
    if data == nil then
        return
    end
    for k, v in pairs(data.m_topPlayerBalance) do

        for i = 2, 7 do

            local auto_panel = self["user_node_" .. i]

            if auto_panel.m_accountId == v.m_accountId then

                self:show_sit_win_number(auto_panel, v.m_profit * 0.01, cc.p(51, 95))

            end

        end

    end
    self:show_sit_win_number(self["user_node_1"], data.m_balanceInfo.m_profit * 0.01, cc.p(68, 52))

    self:update_user_score(data.m_balanceInfo.m_curCoin * 0.01)

    --    if data.player then
    --        self:show_sit_win_number(self.auto_self_panel , data.player.currentScore , cc.p( 68 , 52 ) )
    --        self:update_user_score( data.player.coin )
    --    end
end

--显示下注区赢的特效
function BmwGameLayer:Show_bet_win_Effect(index)

    local img_win = self["niuniu_table_btn_" .. index]:getChildByName("img_win")

    if index == 2 then

        -- print( img_win )
    end

    ani_cro:bet_Win_Effect(img_win)

end

--显示用户输赢多少
--y用户自己 pos = cc.p( 68 , 52 ) , 坐在玩家 pos = cc.p( 51 , 95 ) , 庄家 pos = cc.p( 50 , -70 )
--self:show_sit_win_number( self.auto_self_panel , -450000 , cc.p( 68 , 52 ) )
function BmwGameLayer:show_sit_win_number(node_view, num, pos)

    if num == 0 then

        return

    end

    local img = nil

    local path = game_fnt_res .. "num_win.fnt"

    if num >= 0 then

        img = ccui.ImageView:create(game_fnt_res .. "douniu_table_win_zi_bg.png")

        local TextAtlas = ccui.TextBMFont:create(str, path)

        if num >= 10000 then

            num = num / 10000

            local strCoin = string.format("%.2f", num) .. "万"

            TextAtlas:setString("+" .. strCoin)

        else

            local strCoin = string.format("%.2f", num)

            TextAtlas:setString("+" .. strCoin)

        end

        img:addChild(TextAtlas)

        TextAtlas:setPosition(img:getContentSize().width / 2, img:getContentSize().height / 2)

    else

        img = ccui.ImageView:create(game_fnt_res .. "douniu_table_lose_zi_bg.png")

        path = game_fnt_res .. "num_lose.fnt"

        local TextAtlas = ccui.TextBMFont:create("", path)

        if math.abs(num) >= 10000 then

            num = math.abs(num) / 10000

            local strCoin = string.format("%.2f", num) .. "万"

            TextAtlas:setString("-" .. strCoin)

        else

            local strCoin = string.format("%.2f", math.abs(num))

            TextAtlas:setString("-" .. strCoin)

        end

        img:addChild(TextAtlas)

        TextAtlas:setPosition(img:getContentSize().width / 2, img:getContentSize().height / 2)

    end

    img:setPosition(pos)

    node_view:addChild(img)

    local array = {}

    array[#array + 1] = cc.MoveBy:create(0.3, cc.p(0, 50))

    array[#array + 1] = cc.DelayTime:create(1)

    array[#array + 1] = cc.FadeOut:create(0.5)

    array[#array + 1] = cc.CallFunc:create(function()

        img:removeFromParent()

    end)

    img:runAction(cc.Sequence:create(array))

end

--显示游戏错误提示
function BmwGameLayer:show_game_tips(str)

    self.auto_Image_tips:stopAllActions()

    self.auto_Image_tips:setVisible(true)

    self.auto_Image_tips:setOpacity(255)

    local Text_des = self.auto_Image_tips:getChildByName("Text_tips")

    Text_des:setString(str)

    local array = {}

    self.auto_Image_tips:setPositionY(400)

    array[#array + 1] = cc.MoveBy:create(0.4, cc.p(0, 60))

    array[#array + 1] = cc.FadeOut:create(0.5)

    array[#array + 1] = cc.CallFunc:create(function()

        self.auto_Image_tips:setVisible(false)

        self.auto_Image_tips:setPositionY(400)

    end)

    self.auto_Image_tips:stopAllActions()

    self.auto_Image_tips:runAction(cc.Sequence:create(array))

end

function BmwGameLayer:user_quit_room(response)

    if response.m_accountId == get_my_id() then

        g_notifyer:notify_frame_event("quit_game_state")

    end

end



function BmwGameLayer:_PlayAni(animation_name, complete, index, isLoop)
    index = index or 0;
    self.m_pAniAll = self.m_pAniAll or {};
    if not self.m_pAniAll[animation_name] then
        local animation = ccs.Armature:create(animation_name);
        self.m_pAniAll[animation_name] =  animation;
        self.auto_mid_panel_ani:addChild(animation, 1);
        local pos = cc.p(self.auto_mid_panel_ani:getChildByName("node_ani_pos"):getPosition());
        animation:setPosition(pos);
    end
    
    local call_back = function(armature, movementType, movement_name)
        if movementType == ccs.MovementEventType.loopComplete then
        elseif movementType == ccs.MovementEventType.complete then
            self.m_pAniAll[animation_name]:setVisible(false);
            if complete then
                complete();
            end
        end
    end
    self.m_pAniAll[animation_name]:setVisible(true);
    self.m_pAniAll[animation_name]:getAnimation():setMovementEventCallFunc(call_back);
    if isLoop and isLoop == true then
        self.m_pAniAll[animation_name]:getAnimation():playWithIndex(index, 0, 1);
    else
        self.m_pAniAll[animation_name]:getAnimation():playWithIndex(index, 0, 0);
    end
end

function BmwGameLayer:_HideAni(ani_name)
    self.m_pAniAll = self.m_pAniAll or {};
    if self.m_pAniAll[ani_name] then
        self.m_pAniAll[ani_name]:setVisible(false);
    end
end

function BmwGameLayer:_IsAniVisible(ani_name)
    self.m_pAniAll = self.m_pAniAll or {};
    if self.m_pAniAll[ani_name] then
        return self.m_pAniAll[ani_name]:isVisible();
    end
    return false;
end

function BmwGameLayer:_DoSomethingLater(call, delay)
    self.m_pAnimalUI:runAction(cc.Sequence:create(cc.DelayTime:create(delay), cc.CallFunc:create(call), nil))
end

function BmwGameLayer:_GetChipsByAreas(areas)
    local getMinCoin = function(coin)
        for index = BMWConfig.BET_TYPE_COUNT, 1, -1 do
            local v = self.game_bet_list[index];
            if coin >= v then
                return v;
            end
        end
        return nil;
    end
    local ret = {};
    for index = 1, #areas do
        local cur_coin = areas[index];
        if cur_coin > 0 then
            local coin = getMinCoin(cur_coin);
            while (coin) do
                cur_coin = cur_coin - coin;
                table.insert(ret, { index, coin });
                coin = getMinCoin(cur_coin);
            end
        end
    end

    return ret;
end

-- 更新下注按钮
function BmwGameLayer:_RefBetButton()
    if self.game_bet_list == nil or self.my_coin_num == nil then
        return
    end

    for i = 1, #self.game_bet_list do

        local btn = self["btn_bet_" .. i]

        local Image_bg = btn:getChildByName("image_circle")
        Image_bg:setVisible(false)
        if self.my_coin_num < self.game_bet_list[i] * 0.01 then
            btn:setEnabled(false)
            btn:setColor(cc.c3b(160, 160, 160))
        else
            btn:setEnabled(true)
            btn:setColor(cc.c3b(250, 250, 250))
            if self.cur_bet_index == i then
                Image_bg:setVisible(true)
            end
        end

    end
    if self.table_state ~= 2 then
        self.btn_continue:setEnabled(false);
        self.btn_continue:setColor(cc.c3b(160, 160, 160));
        for i = 1, #self.game_bet_list do

            local btn = self["btn_bet_" .. i]
            btn:setEnabled(false)
            btn:setColor(cc.c3b(160, 160, 160))

            local Image_bg = btn:getChildByName("image_circle")
            Image_bg:setVisible(false)
        end
    else
        if not self.m_bContinue and BMWData.getInstance():isLastBet() then
            self.btn_continue:setEnabled(true);
            self.btn_continue:setColor(cc.c3b(255, 255, 255));
        else
            self.btn_continue:setEnabled(false);
            self.btn_continue:setColor(cc.c3b(160, 160, 160));
        end
    end

end

function BmwGameLayer:_InitDeskChip()
    local data = BMWData.getInstance();
    local oterBetArr = self:_GetChipsByAreas(data.m_pAreaTotalBet);
    for index = 1, #oterBetArr do
        local item = oterBetArr[index];
        self:_CreateStaticChip(0, item[1], item[2]);
    end

    local myBetArr = self:_GetChipsByAreas(data.m_pMyAreaBet);
    for index = 1, #myBetArr do
        local item = myBetArr[index];
        self:_CreateStaticChip(Player:getAccountID(), item[1], item[2])
    end
end

-- 获取玩家下标
function BmwGameLayer:_GetPlayerIndex(nAccountId)
    if Player:getAccountID() == nAccountId then
        return 1;
    else
        for i = 2, 7 do
            local panal = self["user_node_" .. i]
            if panal.m_accountId and panal.m_accountId == nAccountId then
                return i
            end
        end
    end
    return 8;

end

-- 获取玩家坐标
function BmwGameLayer:_GetPlayerPoint(nAccountId, betValue)
    if Player:getAccountID() == nAccountId then
        return self.m_playerPos[1];
    else
        local index = self:_GetPlayerIndex(nAccountId);
        return self.m_playerPos[index];
    end
end

-- 获取一个随机坐标
function BmwGameLayer:_RandomAreaPoint(areaId)
    local n = #self.m_areaPos[areaId];
    local index = math.random(1, n);
    local array = self.m_areaPos[areaId];
    return array[index];
end

-- 克隆一个赔付的筹码对象(注意克隆的对象里面不包含任何数据信息以及纹理路径地址)
function BmwGameLayer:_ClonePayoutChip(sp)
    local item = CChipCacheManager.getInstance():getChip(sp.m_nBetValue)
    item:setAnchorPoint(sp:getAnchorPoint())
    item:setScale(sp:getScale())
    item:setPosition(sp:getPosition())
    item:setVisible(false)
    item.m_nAccountId = sp.m_nAccountId;
    item.m_nAreaId = sp.m_nAreaId;
    item.m_nBetValue = sp.m_nBetValue;
    return item
end

-- 创建一个静态放置筹码
function BmwGameLayer:_CreateStaticChip(nAccountId, areaId, betValue)
    local item = CChipCacheManager.getInstance():getChip(betValue);
    item.m_nAccountId = nAccountId;
    item.m_nAreaId = areaId;
    item.m_nBetValue = betValue;
    item:stopAllActions();

    local endPoint = self:_RandomAreaPoint(areaId);
    item:setVisible(true);
    item:setScale(self.m_chip_scale);
    item:setPosition(endPoint);
    self.table_coin_node_array[#self.table_coin_node_array + 1] = item;
end

-- 创建一个飞行筹码 (betType:筹码类型 index:下注位置 bSelf:是否自己)
function BmwGameLayer:_FlyChip(nAccountId, areaId, betValue)

    local item = CChipCacheManager.getInstance():getChip(betValue);
    item.m_nAccountId = nAccountId;
    item.m_nAreaId = areaId;
    item.m_nBetValue = betValue;
    self.table_coin_node_array[#self.table_coin_node_array + 1] = item;
    item:stopAllActions()

    local begin = self:_GetPlayerPoint(nAccountId, betValue);
    local endpos = self:_RandomAreaPoint(areaId);

    item:setScale(self.m_chip_scale)
    item:setPosition(begin)
    item:setVisible(false)

    local move_distance = cc.pGetDistance(begin, endpos)
    local move_time = move_distance / BMWConfig.BR_CHIP_SPEED
    local move_action = cc.MoveTo:create(move_time, endpos)
    local ease_action = cc.EaseOut:create(move_action, move_time + 0.3)
    local scale_action = cc.ScaleTo:create(0.15, self.m_chip_scale - 0.05)
    local time = 0;
    if Player:getAccountID() == nAccountId then
        time = 0;
    else
        time = (math.random(0, 20) / 40);
    end

    local delay = cc.DelayTime:create(time)
    local call = cc.CallFunc:create(function()
        item:setVisible(true);
        if Player:getAccountID() == nAccountId then -- 自己的音效立刻播放
            g_AudioPlayer:playEffect(BMWRes.Audio.BET);
        else
            if not self.m_isPlayBetSound then
                g_AudioPlayer:playEffect(BMWRes.Audio.BET);
                -- 尽量避免重复播放
                self:_DoSomethingLater(function() self.m_isPlayBetSound = false end, math.random(2, 4) / 10);
                self.m_isPlayBetSound = true;
            end
        end
    end)

    item:runAction(cc.Sequence:create(delay, call, ease_action, scale_action, nil));

    local playerIndex = self:_GetPlayerIndex(nAccountId);
    if 1 < playerIndex and 8 > playerIndex then
        local panal = self["user_node_" .. playerIndex]
        local array = {}

        local dir = 20

        if 4 < playerIndex then
            dir = -20
        end
        array[#array + 1] = cc.DelayTime:create(time)
        local action1 = cc.MoveBy:create(0.05, cc.p(dir, 0))

        array[#array + 1] = action1

        local action2 = action1:reverse()

        array[#array + 1] = action2

        panal:runAction(cc.Sequence:create(array))
    end
    return item
end

-- 以队列方式进行飞筹码
function BmwGameLayer:_FlyChipEX()
    local openAward = g_GameController:getOpenAward()

    local bankerwinvec = {}     -- 庄家获取的筹码
    local bankerlostvec = {}    -- 庄家赔付筹码
    local otherwinvec = {}      -- 玩家获取的筹码
    local othervec = {}         -- 玩家所有区域筹码

    for i = 1, #self.table_coin_node_array do
        local item = self.table_coin_node_array[i]
        local isWind = false
        for k = 1, #openAward.m_awardBetAreaId do
            local areaId = openAward.m_awardBetAreaId[k]
            if areaId == item.m_nAreaId then
                isWind = true
            end
        end

        if isWind == true then
            local endPos = self:_GetPlayerPoint(item.m_nAccountId)
            table.insert(othervec, { sp = item, endpos = endPos })
        else
            table.insert(bankerwinvec, { sp = item, endpos = self.m_bankerPos })
        end
    end

    for k, v in pairs(othervec) do
        local multiple = self.m_area_multiple[v.sp.m_nAreaId]
        if multiple > 1 then
            for i = 0, multiple - 1 do
                local item = self:_ClonePayoutChip(v.sp)
                item:setPosition(self.m_bankerPos)
                table.insert(otherwinvec, { sp = item, endpos = v.endpos })

                local endPos = self:_RandomAreaPoint(item.m_nAreaId)
                table.insert(bankerlostvec, { sp = item, endpos = endPos })
                table.insert(self.m_betChips, item)
            end
        end
    end

    -- 飞到庄家
    local jobitem1 = {
        flytime    = BMWConfig.CHIP_FLY_TIME, -- 飞行时间
        flytimedelay = true, -- 飞行时间延长随机时间(0.05~0.15)
        flysteptime = BMWConfig.CHIP_FLY_STEPDELAY, -- 筹码队列飞行间隔
        nextjobtime = BMWConfig.CHIP_JOBSPACETIME, -- 下个任务执行间隔时间
        chips        = bankerwinvec, -- 筹码队列集合 
        preCB        = function()                                  -- 任务开始时执行的回调，此回调根据preCBExec控制只执行一次

        end,
        preCBExec    = false,
        --overCB        = function() print("庄家获胜筹码飞行完毕") end, -- 动画任务完成后回调
        hideAfterOver = true                                          -- 动画完成后隐藏
    }

    -- 飞到赔付区域
    local jobitem2 = {
        flytime    = BMWConfig.CHIP_FLY_TIME, -- 飞行时间
        flytimedelay = true, -- 飞行时间延长随机时间(0.05~0.15)
        flysteptime = BMWConfig.CHIP_FLY_STEPDELAY, -- 筹码队列飞行间隔
        nextjobtime = BMWConfig.CHIP_JOBSPACETIME, -- 下个任务执行间隔时间
        chips        = bankerlostvec, -- 筹码队列集合
        preCB        = function()                                    -- 任务开始时执行的回调，此回调根据preCBExec控制只执行一次

        end,
        preCBExec    = false,
        --overCB        = function() print("庄家获胜筹码飞行完毕") end, -- 动画任务完成后回调
        hideAfterOver = false                                          -- 动画完成后隐藏
    }


    -- 飞到玩家
    local jobitem3 = {
        flytime    = BMWConfig.CHIP_FLY_TIME, -- 飞行时间
        flytimedelay = true, -- 飞行时间延长随机时间(0.05~0.15)
        flysteptime = BMWConfig.CHIP_FLY_STEPDELAY, -- 筹码队列飞行间隔
        nextjobtime = BMWConfig.CHIP_JOBSPACETIME, -- 下个任务执行间隔时间
        chips        = otherwinvec, -- 筹码队列集合
        preCB        = function()                                    -- 任务开始时执行的回调，此回调根据preCBExec控制只执行一次

        end,
        preCBExec    = false,
        --overCB        = function() print("庄家获胜筹码飞行完毕") end, -- 动画任务完成后回调
        hideAfterOver = true                                          -- 动画完成后隐藏
    }

    -- 飞到玩家
    local jobitem4 = {
        flytime    = BMWConfig.CHIP_FLY_TIME, -- 飞行时间
        flytimedelay = true, -- 飞行时间延长随机时间(0.05~0.15)
        flysteptime = BMWConfig.CHIP_FLY_STEPDELAY, -- 筹码队列飞行间隔
        nextjobtime = BMWConfig.CHIP_JOBSPACETIME, -- 下个任务执行间隔时间
        chips        = othervec, -- 筹码队列集合
        preCB        = function()                                    -- 任务开始时执行的回调，此回调根据preCBExec控制只执行一次

        end,
        preCBExec    = false,
        --overCB        = function() print("庄家获胜筹码飞行完毕") end, -- 动画任务完成后回调
        hideAfterOver = true                                          -- 动画完成后隐藏
    }

    self.m_flyJobVec.nIdx = 1 -- 任务处理索引
    self.m_flyJobVec.flyIdx = 1 -- 飞行队列索引
    self.m_flyJobVec.jobVec = {} -- 任务对象
    self.m_flyJobVec.overCB = function()
        print("所有飞行任务执行完毕")
        self.m_flyJobVec = {}
        self:showSitWinNumber(g_GameController:getOpenAward())
    end

    table.insert(self.m_flyJobVec.jobVec, { jobitem1 })
    table.insert(self.m_flyJobVec.jobVec, { jobitem2 })
    table.insert(self.m_flyJobVec.jobVec, { jobitem3, jobitem4 })

    self:_DoFlyJob()
end

-- 执行单个的飞行任务
function BmwGameLayer:_DoFlyJob()
    -- 全部任务执行完成之前，被清理重置
    if nil == self.m_flyJobVec.nIdx or nil == self.m_flyJobVec.jobVec then return end

    -- 任务处理完了
    if self.m_flyJobVec.nIdx > #self.m_flyJobVec.jobVec then
        if self.m_flyJobVec.overCB then
            self.m_flyJobVec.overCB()
        end
        return
    end

    -- 取出一个当前需要处理的飞行任务
    local job = self.m_flyJobVec.jobVec[self.m_flyJobVec.nIdx]
    if not job then return end
    if 0 == #job then return end

    -- 按队列取出需要飞行的对象进行动画处理
    local flyvec = {}
    local mf = math.floor
    if self.m_flyJobVec.flyIdx <= BMWConfig.CHIP_FLY_SPLIT then
        for i = 1, #job do
            if job[i] then
                --for j = 1, #job[i].chips do
                local segnum = mf(#job[i].chips / BMWConfig.CHIP_FLY_SPLIT) -- 计算需要分成几段
                for m = 0, segnum do
                    local tgg = job[i].chips[m * BMWConfig.CHIP_FLY_SPLIT + self.m_flyJobVec.flyIdx]
                    if tgg then
                        table.insert(flyvec, { sptg = tgg, idx = i })
                    end        
                end
                --end
            end
        end
    end

    -- 当前队列都飞完了
    if 0 == #flyvec then
        -- 下个任务的执行
        self.m_flyJobVec.nIdx = self.m_flyJobVec.nIdx + 1
        self.m_flyJobVec.flyIdx = 1
        self:_DoSomethingLater(function()
            self:_DoFlyJob()
        end, job[1].nextjobtime) -- 多个任务时 取第一个任务的时间
        return
    end

    -- 开始飞筹码
    for i = 1, #flyvec do
        local tg = flyvec[i]
        if tg and tg.sptg then
            local ts = job[tg.idx].flytimedelay and job[tg.idx].flytime + math.random(5, 15) / 100 or job[tg.idx].flytime
            local mt = cc.MoveTo:create(ts, tg.sptg.endpos)
            if i == #flyvec then -- 最后一个筹码飞行完成后执行下一次的飞行回调
                self.m_flyJobVec.flyIdx = self.m_flyJobVec.flyIdx + 1
                self:_DoSomethingLater(function()
                    self:_DoFlyJob()
                end, job[tg.idx].flysteptime)
            end

            if job[tg.idx].hideAfterOver then
                tg.sptg.sp:runAction(cc.Sequence:create(cc.Show:create(), mt, cc.Hide:create()))
            else
                tg.sptg.sp:runAction(cc.Sequence:create(cc.Show:create(), mt))
            end
        end
    end

end

function BmwGameLayer:_SetRecord(_recordId)
    if _recordId ~= "" then
        self.mTextRecord:setString("牌局ID:".._recordId)
    end
end


--****************************按钮回调****************************
--续投
function BmwGameLayer:onContinueClick()
    if self.m_bContinue then
        TOAST("你已经续投过了！")
        return
    end --已经续投过了
    if self.table_state ~= 2 then return end

    self.btn_continue:setEnabled(false)
    self:_DoSomethingLater(function()
        if not self.m_bContinue then
            self.btn_continue:setEnabled(true)
        end
    end, 0.3);
    local chips = self:_GetChipsByAreas(BMWData.getInstance().m_pMyLastBet);

    ConnectManager:send2GameServer(g_GameController.m_gameAtomTypeId, "CS_C2G_Bmw_ContinueBet_Req", { chips })

end

--msg消息回调
function BmwGameLayer:_ON_MSG_INIT(msgId, cmd)
    local nLeftTime = BMWData.getInstance():getLeftTime();
    local nGameState = BMWData.getInstance().m_nGameState;
    self["table_state_img"]:setVisible(false);
    self.daojishi_img:setVisible(false);

    self.table_state = nGameState
    self.game_bet_list = BMWData.getInstance().m_pAllChipValue;
    self.max_bet_gold = self.game_bet_list[#self.game_bet_list];
    self.min_bet_gold = self.game_bet_list[1];
    self.bet_value_slider:setString("" .. self.min_bet_gold)
    self:update_trand(BMWData.getInstance().m_nHistory);
    self:update_myself_info(BMWData.getInstance().m_pPlayerInfo)
    self:init_bet_btn_value()
    self:init_bet_count(BMWData.getInstance().m_pBetArea)
    self.dest_track = BMWData.getInstance().m_nLastGridId;
    self:clear_scene();
    self:_SetRecord(BMWData.getInstance().m_sRecordId)
    if BMWConfig.STATE_BET == nGameState then
        self:update_state_text(2);
        self:showLeftTime(nLeftTime);
        self:_PlayAni("game_star", nil);
        self:_InitDeskChip();
        self:update_self_xiazhu_score(BMWData.getInstance().m_pMyAreaBet);
        self:update_total_xiazhu_score(BMWData.getInstance().m_pAreaTotalBet);
    else
        self:_PlayAni("bairenniuniu_dengdaikaiju", nil, 0, true);
    end

end

function BmwGameLayer:_ON_MSG_GAME_STATE(msgId, cmd)
    local nLeftTime = BMWData.getInstance():getLeftTime();
    local nGameState = BMWData.getInstance().m_nGameState;
    self:_SetRecord(BMWData.getInstance().m_sRecordId)
    if BMWConfig.STATE_BET == nGameState then
        self.m_bContinue = false;
        self:clear_scene();
        self:_HideAni("bairenniuniu_dengdaikaiju");
        self:update_state_text(2);
        g_AudioPlayer:playEffect(BMWRes.Audio.START_BET);

        self:showLeftTime(nLeftTime);

        self:_PlayAni("game_star", nil);
    elseif BMWConfig.STATE_OPEN == nGameState then
        self:update_state_text(3);
        self:openAward(g_GameController:getOpenAward());
        self:showLeftTime(nLeftTime);
    else
        if not self:_IsAniVisible("bairenniuniu_dengdaikaiju") then
            self:update_state_text(1);
            self:showLeftTime(nLeftTime);
        end
    end
end



function BmwGameLayer:_ON_MSG_BET(msgId, cmd)
    local data = BMWData.getInstance();
    self:_FlyChip(cmd.m_nAccountId, cmd.m_nAreaId, cmd.m_nBetValue);
    self:update_total_xiazhu_score(data.m_pAreaTotalBet);
    if cmd.m_nAccountId == Player:getAccountID() then --是否是自己
        self:update_self_xiazhu_score(data.m_pMyAreaBet);
        self:update_user_score(data.m_nCurCoin * 0.01);
        self:_RefBetButton();
    end
end

function BmwGameLayer:_ON_MSG_ONLINE_PLAYER_LIST(msgId, cmd)
    self:updateOnlineUserList(BMWData.getInstance().m_pPlayerInfo);
end

function BmwGameLayer:_ON_MSG_BACKGROUND(msgId, cmd)
end

function BmwGameLayer:_ON_MSG_EXIT(msgId, cmd)
end

function BmwGameLayer:_ON_MSG_TOP_PLAYER_LIST(msgId, cmd)
    self:update_users_info(BMWData.getInstance().m_pTopPlayerList);
end

function BmwGameLayer:_ON_MSG_CONTINUE_BET(msgId, cmd)
    local data = BMWData.getInstance();

    local chips = self:_GetChipsByAreas(cmd.m_pBetArr);
    for index = 1, #chips do
        local item = chips[index];
        self:_FlyChip(cmd.m_nAccountId, item[1], item[2]);
    end

    self:update_total_xiazhu_score(data.m_pAreaTotalBet);
    if cmd.m_nAccountId == Player:getAccountID() then --是否是自己
        self:update_self_xiazhu_score(data.m_pMyAreaBet);
        self:update_user_score(data.m_nCurCoin * 0.01);
        self.m_bContinue = true;
        self:_RefBetButton();
    end
end




--**********************初始化基础属性********************
function BmwGameLayer:_InitMsgEvent()
    -- MSG_INIT                = flag.."MSG_BMW_INIT_NTY", --初始化
    -- MSG_GAME_STATE          = flag.."MSG_BMW_GAME_STATE_NTY", --游戏状态
    -- MSG_BET                 = flag.."MSG_BMW_BET_ACK", --玩家下注
    -- MSG_ONLINE_PLAYER_LIST  = flag.."MSG_BMW_ONLINE_PLAYER_LIST_ACK", --在线玩家列表
    -- MSG_BACKGROUND          = flag.."MSG_BMW_BACKGROUND_ACK", --切换后台
    -- MSG_EXIT                = flag.."MSG_BMW_EXIT_ACK", --退出游戏
    -- MSG_TOP_PLAYER_LIST     = flag.."MSG_BMW_TOP_PLAYER_LIST_NTY", --前六名玩家
    -- MSG_CONTINUE_BET        = flag.."MSG_BMW_CONTINUE_BET_ACK", --续投
    addMsgCallBack(self, BMWEvent.MSG_INIT, handler(self, self._ON_MSG_INIT));
    addMsgCallBack(self, BMWEvent.MSG_GAME_STATE, handler(self, self._ON_MSG_GAME_STATE));
    addMsgCallBack(self, BMWEvent.MSG_BET, handler(self, self._ON_MSG_BET));
    addMsgCallBack(self, BMWEvent.MSG_ONLINE_PLAYER_LIST, handler(self, self._ON_MSG_ONLINE_PLAYER_LIST));
    addMsgCallBack(self, BMWEvent.MSG_BACKGROUND, handler(self, self._ON_MSG_BACKGROUND));
    addMsgCallBack(self, BMWEvent.MSG_EXIT, handler(self, self._ON_MSG_EXIT));
    addMsgCallBack(self, BMWEvent.MSG_TOP_PLAYER_LIST, handler(self, self._ON_MSG_TOP_PLAYER_LIST));
    addMsgCallBack(self, BMWEvent.MSG_CONTINUE_BET, handler(self, self._ON_MSG_CONTINUE_BET));
    ToolKit:registDistructor(self, handler(self, self._ON_DESTORY));
end

function BmwGameLayer:_ClearMsgEvent()
    removeMsgCallBack(self, BMWEvent.MSG_INIT);
    removeMsgCallBack(self, BMWEvent.MSG_GAME_STATE);
    removeMsgCallBack(self, BMWEvent.MSG_BET);
    removeMsgCallBack(self, BMWEvent.MSG_ONLINE_PLAYER_LIST);
    removeMsgCallBack(self, BMWEvent.MSG_BACKGROUND);
    removeMsgCallBack(self, BMWEvent.MSG_EXIT);
    removeMsgCallBack(self, BMWEvent.MSG_TOP_PLAYER_LIST);
    removeMsgCallBack(self, BMWEvent.MSG_CONTINUE_BET);
end


function BmwGameLayer:_ON_DESTORY()
    self:_ClearMsgEvent();
end


return BmwGameLayer