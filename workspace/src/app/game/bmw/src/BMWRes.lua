local BMWRes = {};

BMWRes.CSB = {};
BMWRes.CSB.MAIN_LAYER = "80000042/game_100019.csb";
BMWRes.CSB.HELP = "80000042/bmwHelp.csb";

BMWRes.Image = {};
BMWRes.Image.CHIP_1 = "80000042/game_img/niuniu_img_1.png";
BMWRes.Image.CHIP_2 = "80000042/game_img/niuniu_img_2.png";
BMWRes.Image.CHIP_3 = "80000042/game_img/niuniu_img_3.png";
BMWRes.Image.CHIP_4 = "80000042/game_img/niuniu_img_4.png";
BMWRes.Image.CHIP_5 = "80000042/game_img/niuniu_img_5.png";
BMWRes.Image.CHIP_6 = "80000042/game_img/niuniu_img_6.png";

BMWRes.Audio = {};
BMWRes.Audio.BACKGROUND = "80000042/audio/background.mp3";
BMWRes.Audio.END_BET = "80000042/audio/100019/lh_end_bet.mp3";
BMWRes.Audio.START_BET = "80000042/audio/100019/lh_start_bet.mp3";
BMWRes.Audio.CAR_TURN = "80000042/audio/sound_car_turn.mp3";
BMWRes.Audio.CAR_TURN_END = "80000042/audio/sound_car_turn_end.mp3";
BMWRes.Audio.BET = "80000042/audio/gold_xiazhu.mp3";
BMWRes.Audio.REWARDINGEFFECT0 = "80000042/audio/Rewardingeffect0.mp3";
BMWRes.Audio.REWARDINGEFFECT1 = "80000042/audio/Rewardingeffect1.mp3";
BMWRes.Audio.REWARDINGEFFECT2 = "80000042/audio/Rewardingeffect2.mp3";
BMWRes.Audio.REWARDINGEFFECT3 = "80000042/audio/Rewardingeffect3.mp3";
BMWRes.Audio.REWARDINGEFFECT4 = "80000042/audio/Rewardingeffect4.mp3";
BMWRes.Audio.REWARDINGEFFECT5 = "80000042/audio/Rewardingeffect5.mp3";
BMWRes.Audio.REWARDINGEFFECT6 = "80000042/audio/Rewardingeffect6.mp3";
BMWRes.Audio.REWARDINGEFFECT7 = "80000042/audio/Rewardingeffect7.mp3";


BMWRes.vecReleaseAnim = {  -- 退出时需要释放的动画资源
    "80000042/game_ani/game_star/game_star.ExportJson",
    "80000042/game_ani/feiqinzoushou_jiesuandongwu/feiqinzoushou_jiesuandongwu.ExportJson",
    "80000042/game_ani/benchibaoma_changjingtexiao/benchibaoma_changjingtexiao.ExportJson",
    "80000042/game_ani/game_end/game_end.ExportJson",
    "80000042/game_ani/game_star/game_star.ExportJson",
    "80000042/game_ani/game_end/game_end.ExportJson",
    "80000042/game_ani/bairenniuniu_dengdaikaiju/bairenniuniu_dengdaikaiju.ExportJson",
}

BMWRes.vecReleasePlist = {
    {"80000042/game_img/game_img_plist.plist","80000042/game_img/game_img_plist.png",},
    {"80000042/game_ani/game_star/game_star0.plist","80000042/game_ani/game_star/game_star0.png",},
    {"80000042/game_ani/game_end/game_end0.plist","80000042/game_ani/game_end/game_end0.png",},
    {"80000042/game_ani/feiqinzoushou_jiesuandongwu/feiqinzoushou_jiesuandongwu0.plist","80000042/game_ani/feiqinzoushou_jiesuandongwu/feiqinzoushou_jiesuandongwu0.png",},
    {"80000042/game_ani/benchibaoma_changjingtexiao/benchibaoma_changjingtexiao0.plist","80000042/game_ani/benchibaoma_changjingtexiao/benchibaoma_changjingtexiao0.png",},
    {"80000042/game_ani/bairenniuniu_dengdaikaiju/bairenniuniu_dengdaikaiju0.plist","80000042/game_ani/bairenniuniu_dengdaikaiju/bairenniuniu_dengdaikaiju0.png"},
    {"80000042/game_img/game_img_plist.plist", "80000042/game_img/game_img_plist.png",},
}

BMWRes.vecReleaseImg = {
    "80000042/font/douniu_table_win_zi_bg.png",
    "80000042/font/douniu_table_lose_zi_bg.png",
    "80000042/game_100019/table.png",
    "80000042/game_battay/battery_lightnig.png",
    "80000042/game_battay/game_battay_1.png",
    "80000042/game_battay/game_battay_2.png",
    "80000042/game_battay/game_battay_3.png",
    "80000042/game_battay/game_battay_4.png",
    "80000042/game_battay/game_battay_5.png",
    "80000042/game_battay/game_wifi_1.png",
    "80000042/game_battay/game_wifi_2.png",
    "80000042/game_battay/game_wifi_3.png",
    "80000042/game_battay/game_wifi_4.png",
    "80000042/game_img/button/btn_anniu.png",
    "80000042/game_img/button/btn_bank_close.png",
    "80000042/game_img/button/btn_record.png",
    "80000042/game_img/button/btn_return.png",
    "80000042/game_img/button/btn_seting.png",
    "80000042/game_img/button/btn_shang.png",
    "80000042/game_img/button/btn_xia.png",
    "80000042/game_img/backe_menu_bg.png",
    "80000042/game_img/bank_bg.png",
    "80000042/game_img/btn_xutou.png",
    "80000042/game_img/dd3.png",
    "80000042/game_img/douniu_table_lose_zi_bg.png",
    "80000042/game_img/douniu_table_win_zi_bg.png",
    "80000042/game_img/dun_light.png",
    "80000042/game_img/game-bank005.png",
    "80000042/game_img/game-bank006.png",
    "80000042/game_img/guizhe_table.png",
    "80000042/game_img/hh_table_bottom.png",
    "80000042/game_img/niuniu_btn_1.png",
    "80000042/game_img/niuniu_btn_2.png",
    "80000042/game_img/niuniu_btn_3.png",
    "80000042/game_img/niuniu_btn_4.png",
    "80000042/game_img/niuniu_btn_5.png",
    "80000042/game_img/niuniu_btn_6.png",
    "80000042/game_img/niuniu_btn_other.png",
    "80000042/game_img/niuniu_img_1.png",
    "80000042/game_img/niuniu_img_2.png",
    "80000042/game_img/niuniu_img_3.png",
    "80000042/game_img/niuniu_img_4.png",
    "80000042/game_img/niuniu_img_5.png",
    "80000042/game_img/niuniu_img_6.png",
    "80000042/game_img/quancs.png",
    "80000042/game_img/quanhs.png",
    "80000042/game_img/quanls.png",
    "80000042/game_img/quanzs.png",
    "80000042/game_img/touch_down_1.png",
    "80000042/game_img/touch_down_2.png",
    "80000042/game_img/touch_down_3.png",
    "80000042/game_img/touch_down_4.png",
    "80000042/game_img/touch_down_5.png",
    "80000042/game_img/touch_down_6.png",
    "80000042/game_img/touch_down_7.png",
    "80000042/game_img/touch_down_8.png",
    "80000042/game_img/yuan_light.png",
    "80000042/gui/record/1.png",
    "80000042/gui/record/2.png",
    "80000042/gui/record/bg.png",
    "80000042/gui/record/title.png",
    "80000042/gui/setting/1.png",
    "80000042/gui/setting/2.png",
    "80000042/gui/setting/3.png",
    "80000042/gui/setting/bg.png",
    "80000042/gui/setting/close.png",
    "80000042/gui/setting/music.png",
    "80000042/gui/setting/sound.png",
    "80000042/gui/setting/title.png",
}

BMWRes.vecReleaseSound = {
    "80000042/audio/100019/lh_start_bet.mp3",
    "80000042/audio/100019/lh_end_bet.mp3",
    "80000042/audio/sound_car_turn.mp3",
    "80000042/audio/sound_car_turn_end.mp3",
    "80000042/audio/Rewardingeffect0.mp3",
    "80000042/audio/Rewardingeffect1.mp3",
    "80000042/audio/Rewardingeffect2.mp3",
    "80000042/audio/Rewardingeffect3.mp3",
    "80000042/audio/Rewardingeffect4.mp3",
    "80000042/audio/Rewardingeffect5.mp3",
    "80000042/audio/Rewardingeffect6.mp3",
    "80000042/audio/Rewardingeffect7.mp3",
    "80000042/audio/gold_xiazhu.mp3",
    "80000042/audio/background.mp3",
}




BMWRes.vecLoadingPlist = {
    {"80000042/game_img/game_img_plist.plist","80000042/game_img/game_img_plist.png",},
    {"80000042/game_ani/game_star/game_star0.plist","80000042/game_ani/game_star/game_star0.png",},
    {"80000042/game_ani/game_end/game_end0.plist","80000042/game_ani/game_end/game_end0.png",},
    {"80000042/game_ani/feiqinzoushou_jiesuandongwu/feiqinzoushou_jiesuandongwu0.plist","80000042/game_ani/feiqinzoushou_jiesuandongwu/feiqinzoushou_jiesuandongwu0.png",},
    {"80000042/game_ani/benchibaoma_changjingtexiao/benchibaoma_changjingtexiao0.plist","80000042/game_ani/benchibaoma_changjingtexiao/benchibaoma_changjingtexiao0.png",},
    {"80000042/game_ani/bairenniuniu_dengdaikaiju/bairenniuniu_dengdaikaiju0.plist","80000042/game_ani/bairenniuniu_dengdaikaiju/bairenniuniu_dengdaikaiju0.png"},
    {"80000042/game_img/game_img_plist.plist", "80000042/game_img/game_img_plist.png",},
}

BMWRes.vecLoadingImage = {
    "80000042/font/douniu_table_win_zi_bg.png",
    "80000042/font/douniu_table_lose_zi_bg.png",
    "80000042/game_100019/table.png",
    "80000042/game_battay/battery_lightnig.png",
    "80000042/game_battay/game_battay_1.png",
    "80000042/game_battay/game_battay_2.png",
    "80000042/game_battay/game_battay_3.png",
    "80000042/game_battay/game_battay_4.png",
    "80000042/game_battay/game_battay_5.png",
    "80000042/game_battay/game_wifi_1.png",
    "80000042/game_battay/game_wifi_2.png",
    "80000042/game_battay/game_wifi_3.png",
    "80000042/game_battay/game_wifi_4.png",
    "80000042/game_img/button/btn_anniu.png",
    "80000042/game_img/button/btn_bank_close.png",
    "80000042/game_img/button/btn_record.png",
    "80000042/game_img/button/btn_return.png",
    "80000042/game_img/button/btn_seting.png",
    "80000042/game_img/button/btn_shang.png",
    "80000042/game_img/button/btn_xia.png",
    "80000042/game_img/backe_menu_bg.png",
    "80000042/game_img/bank_bg.png",
    "80000042/game_img/btn_xutou.png",
    "80000042/game_img/dd3.png",
    "80000042/game_img/douniu_table_lose_zi_bg.png",
    "80000042/game_img/douniu_table_win_zi_bg.png",
    "80000042/game_img/dun_light.png",
    "80000042/game_img/game-bank005.png",
    "80000042/game_img/game-bank006.png",
    "80000042/game_img/guizhe_table.png",
    "80000042/game_img/hh_table_bottom.png",
    "80000042/game_img/niuniu_btn_1.png",
    "80000042/game_img/niuniu_btn_2.png",
    "80000042/game_img/niuniu_btn_3.png",
    "80000042/game_img/niuniu_btn_4.png",
    "80000042/game_img/niuniu_btn_5.png",
    "80000042/game_img/niuniu_btn_6.png",
    "80000042/game_img/niuniu_btn_other.png",
    "80000042/game_img/niuniu_img_1.png",
    "80000042/game_img/niuniu_img_2.png",
    "80000042/game_img/niuniu_img_3.png",
    "80000042/game_img/niuniu_img_4.png",
    "80000042/game_img/niuniu_img_5.png",
    "80000042/game_img/niuniu_img_6.png",
    "80000042/game_img/quancs.png",
    "80000042/game_img/quanhs.png",
    "80000042/game_img/quanls.png",
    "80000042/game_img/quanzs.png",
    "80000042/game_img/touch_down_1.png",
    "80000042/game_img/touch_down_2.png",
    "80000042/game_img/touch_down_3.png",
    "80000042/game_img/touch_down_4.png",
    "80000042/game_img/touch_down_5.png",
    "80000042/game_img/touch_down_6.png",
    "80000042/game_img/touch_down_7.png",
    "80000042/game_img/touch_down_8.png",
    "80000042/game_img/yuan_light.png",
    "80000042/gui/record/1.png",
    "80000042/gui/record/2.png",
    "80000042/gui/record/bg.png",
    "80000042/gui/record/title.png",
    "80000042/gui/setting/1.png",
    "80000042/gui/setting/2.png",
    "80000042/gui/setting/3.png",
    "80000042/gui/setting/bg.png",
    "80000042/gui/setting/close.png",
    "80000042/gui/setting/music.png",
    "80000042/gui/setting/sound.png",
    "80000042/gui/setting/title.png",
}

BMWRes.vecLoadingAnim = {
    "80000042/game_ani/game_star/game_star.ExportJson",
    "80000042/game_ani/feiqinzoushou_jiesuandongwu/feiqinzoushou_jiesuandongwu.ExportJson",
    "80000042/game_ani/benchibaoma_changjingtexiao/benchibaoma_changjingtexiao.ExportJson",
    "80000042/game_ani/game_end/game_end.ExportJson",
    "80000042/game_ani/game_star/game_star.ExportJson",
    "80000042/game_ani/game_end/game_end.ExportJson",
    "80000042/game_ani/bairenniuniu_dengdaikaiju/bairenniuniu_dengdaikaiju.ExportJson",
}
BMWRes.vecLoadingSound = {
    "80000042/audio/100019/lh_start_bet.mp3",
    "80000042/audio/100019/lh_end_bet.mp3",
    "80000042/audio/sound_car_turn.mp3",
    "80000042/audio/sound_car_turn_end.mp3",
    "80000042/audio/Rewardingeffect0.mp3",
    "80000042/audio/Rewardingeffect1.mp3",
    "80000042/audio/Rewardingeffect2.mp3",
    "80000042/audio/Rewardingeffect3.mp3",
    "80000042/audio/Rewardingeffect4.mp3",
    "80000042/audio/Rewardingeffect5.mp3",
    "80000042/audio/Rewardingeffect6.mp3",
    "80000042/audio/Rewardingeffect7.mp3",
    "80000042/audio/gold_xiazhu.mp3",
    "80000042/audio/background.mp3",
}
BMWRes.vecLoadingMusic = {
    "80000042/audio/background.mp3",
}

return BMWRes