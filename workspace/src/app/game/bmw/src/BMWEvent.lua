local flag = "BMW_Event_"
local BMWEvent = {
    MSG_BMW_GAME_INIT               = flag.."MSG_BMW_GAME_INIT",
    MSG_BMW_GAME_GAME_STATE         = flag.."MSG_BMW_GAME_GAME_STATE",
    MSG_BMW_GAME_TOP_PLAYER_LIST    = flag.."MSG_BMW_GAME_TOP_PLAYER_LIST",
    MSG_BMW_PLAYR_ONLINE_LIST       = flag.."MSG_BMW_PLAYR_ONLINE_LIST",

    MSG_INIT                = flag.."MSG_BMW_INIT_NTY", --初始化
    MSG_GAME_STATE          = flag.."MSG_BMW_GAME_STATE_NTY", --游戏状态
    MSG_BET                 = flag.."MSG_BMW_BET_ACK", --玩家下注
    MSG_ONLINE_PLAYER_LIST  = flag.."MSG_BMW_ONLINE_PLAYER_LIST_ACK", --在线玩家列表
    MSG_BACKGROUND          = flag.."MSG_BMW_BACKGROUND_ACK", --切换后台
    MSG_EXIT                = flag.."MSG_BMW_EXIT_ACK", --退出游戏
    MSG_TOP_PLAYER_LIST     = flag.."MSG_BMW_TOP_PLAYER_LIST_NTY", --前六名玩家
    MSG_CONTINUE_BET        = flag.."MSG_BMW_CONTINUE_BET_ACK", --续投




}

return BMWEvent