local BMWConfig = {};

BMWConfig.STATE_FREE = 1;   --等待
BMWConfig.STATE_BET = 2;    --下注
BMWConfig.STATE_OPEN = 3;   --开奖

BMWConfig.AREA_COUNT = 8; --下注区域数
BMWConfig.BET_TYPE_COUNT = 5; --筹码类型数

BMWConfig.BR_CHIP_SPEED = display.width; -- 金币飞行速度
BMWConfig.CHIP_FLY_STEPDELAY = 0.02; -- 连续飞行间隔
BMWConfig.CHIP_FLY_TIME = 0.2; -- 筹码飞行时间
BMWConfig.CHIP_JOBSPACETIME = 0.3; -- 飞筹码任务队列间隔
BMWConfig.CHIP_FLY_SPLIT = 20; --飞行间隔数

return BMWConfig;