module(..., package.seeall)

CS_M2C_Bmw_Exit_Nty =
{
	{ 1		, 1		, 'm_type'		,		'UBYTE'	, 1		, '退出， 0-正常结束 1-分配游戏服失败 2-同步游戏服失败 3-踢人'},
}

CS_G2C_Bmw_Init_Nty = {
	{ 1,	1, 'm_state'		 	 	 ,  'UINT'						, 1		, '当前状态: 1-ready 2-下注 3-开奖'},
	{ 2,	1, 'm_leftTime'		 	 	 ,  'UINT'						, 1		, '本状态剩余时间(单位秒)'},
	{ 3,	1, 'm_totalTime'		 	 ,  'UINT'						, 1		, '本状态总时间(单位秒)'},
	{ 4,	1, 'm_playerInfo'			 ,  'PstBmwInitPlayerInfo'		, 1		, '玩家基本信息'},
	{ 5, 	1, 'm_minGameCoin'			 ,  'UINT'						, 1 	, '下注必须满足的最低拥有游戏金币' },
	{ 6, 	1, 'm_playerLimit'			 ,  'UINT'						, 1 	, '个人下注上限' },
	{ 7, 	1, 'm_allPlayerTotalLimit'	 ,  'UINT'						, 1 	, '所有玩家总下注上限' },
	{ 8,	1, 'm_isLook'			 	 ,  'UINT'						, 1		, '是否旁观者, 0:否, 1:是'},
	{ 9,	1, 'm_lastAwardType'		 ,  'UINT'						, 1		, '上一次开奖类型(同CS_G2C_Bmw_OpenAward_Nty协议的m_awardType)'},
	{ 10,	1, 'm_lastGridId'			 ,  'UINT'						, 1		, '上一次跑灯停留格子id'},
	{ 11,	1, 'm_allTotalBet'   	 	 ,  'UINT'						, 1		, '所有区域、所有玩家总下注'},
	{ 12, 	1, 'm_betArea'				 ,  'PstBmwBetAreaMultiple'		, 1024 	, '初始化下注区域' },
	{ 13, 	1, 'm_chipArr'			 	 ,  'UINT'						, 1024 	, '初始化筹码' },
	{ 14, 	1, 'm_history'			 	 ,  'UINT'						, 1024 	, '历史开奖记录(元素类型同CS_G2C_Bmw_OpenAward_Nty协议的m_awardType)' },
	{ 15, 	1, 'm_areaTotalBet'			 ,  'PstBmwAreaTotalBet'		, 1024 	, '各区域总下注' },
	{ 16, 	1, 'm_myAreaBet'		 	 ,  'PstBmwAreaMyBet'			, 1024 	, '我在各区域的下注' },
	{ 17,	1, 'm_recordId'       		 ,  'STRING'       				, 1     , '牌局编号' },
}

CS_C2G_Bmw_Background_Req =
{
	{ 1,	1, 'm_type'			 , 'INT'						, 1		, '1-切到后台 2-切回游戏'},
}

CS_G2C_Bmw_Background_Ack =
{
	{ 1,	1, 'm_type'			 , 'INT'						, 1		, '1-切到后台 2-切回游戏'},
	{ 2,	1, 'm_ret'			 , 'INT'						, 1		, '结果 0-表示成功  <0 表示失败'},
}

CS_C2G_Bmw_Exit_Req =
{
}

CS_G2C_Bmw_Exit_Ack =
{
	{ 1, 	1, 'm_result'			 ,  'INT'					, 1		, '0:成功, -x:失败' },
	{ 2, 	1, 'm_keepCoin'			 ,  'UINT'					, 1		, '暂扣金币(m_result = 0,并且m_keepCoin != 0时提示)' },
}

CS_C2G_Bmw_OnlinePlayerList_Req =
{
}

CS_G2C_Bmw_OnlinePlayerList_Ack =
{
	{ 1,	1, 'm_playerList'			 , 'PstBmwInitPlayerInfo'		, 1024		, '玩家基本信息'},
}

CS_G2C_Bmw_GameReady_Nty = 
{
	{ 1,	1, 'm_leftTime'			 ,  'UINT'						, 1		, '该状态剩余时间'},
	{ 2, 	1, 'm_isLook'			 , 'UINT'						, 1 	, '是否旁观者, 0:否, 1:是' },
}

CS_G2C_Bmw_Bet_Nty =
{
	{ 1,	1, 'm_leftTime'			 ,  'UINT'						, 1		, '该状态剩余时间'},
	{ 2,	1, 'm_recordId'       		 ,  'STRING'       				, 1     , '牌局编号' },
}

CS_G2C_Bmw_OpenAward_Nty =
{
	{ 1,	1, 'm_result'			 ,  'INT'						, 1		, '0:成功, -X:失败'},
	{ 2,	1, 'm_leftTime'			 ,  'UINT'						, 1		, '该状态剩余时间'},
	{ 3,	1, 'm_awardType'		 ,  'UINT'						, 1		, '开奖类型, 1:大保时捷  2：大宝马  3:大奔驰  4:大大众 5:小保时捷 6:小宝马 7:小奔驰 8:小大众 '},
	{ 4,	1, 'm_openGridId'		 ,  'UINT'						, 1		, '本次开奖后跑灯停留格子id'},
	{ 5,	1, 'm_allTotalBet'   	 ,  'UINT'						, 1		, '所有区域、所有玩家总下注'},
	{ 6,	1, 'm_awardBetAreaId'	 ,  'UINT'						, 1024	, '本次中奖的下注区域id'},
	{ 7, 	1, 'm_history'			 ,  'UINT'						, 1024 	, '历史开奖记录(元素类型同m_awardType)' },
	{ 8,	1, 'm_balanceInfo'  	 ,  'PstBmwBalanceClt'			, 1		, '自己结算数据'},
	{ 9,	1, 'm_topPlayerBalance'  ,  'PstBmwBalanceClt'			, 1024	, '左右两侧前6名的结算(数量<=6)'},
} 

CS_C2G_Bmw_Bet_Req =
{
	{ 1,	1, 'm_betAreaId'		 ,  'UINT'						, 1		, '下注区域id, 1:大保时捷  2：大宝马  3:大奔驰  4:大大众 5:小保时捷 6:小宝马 7:小奔驰 8:小大众'},
	{ 2,	1, 'm_betValue'		 	 ,  'UINT'						, 1		, '下注额'},
}

CS_G2C_Bmw_Bet_Ack =
{
	{ 1, 	1, 'm_result'		 ,  'INT'					, 1		, '0:成功, -x:失败(m_result=0时，广播此ack)' },
	{ 2, 	1, 'm_betAccountId'	 ,  'UINT'					, 1		, '下注玩家ID' },
	{ 3,	1, 'm_curCoin'		 ,  'UINT'					, 1		, '下注成功后的玩家金币(仅在m_result=0,且m_betAccountId=自己时有效)'},
	{ 4,	1, 'm_betAreaId'	 ,  'UINT'					, 1		, '下注区域id'},
	{ 5,	1, 'm_betValue'		 ,  'UINT'					, 1		, '本次下注额'},
	{ 6,	1, 'm_allTotalBet'   ,  'UINT'					, 1		, '所有区域、所有玩家总下注'},
	{ 7, 	1, 'm_areaTotalBet'	 ,  'PstBmwAreaTotalBet'	, 1024 	, '各区域总下注' },
	{ 8, 	1, 'm_myAreaBet'	 ,  'PstBmwAreaMyBet'	    , 1024 	, '我在各区域的下注(仅在m_result=0,且m_betAccountId=自己时有效)' },
}

CS_G2C_Bmw_TopPlayerList_Nty =
{
	{ 1, 	1, 'm_topPlayerList'		 , 'PstBmwTopPlayerInfo'  			, 1024		, '前6名玩家(实际数量 <= 6)' },
}

CS_C2G_Bmw_ContinueBet_Req =
{
	{ 1,	1, 'm_continueBetArr'	 ,  'PstBmwContinueBet'	, 1024	, '玩家续押信息'},
}

CS_G2C_Bmw_ContinueBet_Ack =
{
	{ 1, 	1, 'm_result'		 ,  'INT'					, 1		, '0:成功, -x:失败(m_result=0时，广播此ack)' },
	{ 2, 	1, 'm_betAccountId'	 ,  'UINT'					, 1		, '下注玩家ID' },
	{ 3,	1, 'm_curCoin'		 ,  'UINT'					, 1		, '下注成功后的玩家金币(仅在m_result=0,且m_betAccountId=自己时有效)'},
	{ 4,	1, 'm_continueBetArr'	 ,  'PstBmwContinueBet'	, 1024	, '玩家续押信息'},
	{ 5,	1, 'm_allTotalBet'   ,  'UINT'					, 1		, '所有区域、所有玩家总下注'},
	{ 6, 	1, 'm_areaTotalBet'	 ,  'PstBmwAreaTotalBet'	, 1024 	, '各区域总下注' },
	{ 7, 	1, 'm_myAreaBet'	 ,  'PstBmwAreaMyBet'	    , 1024 	, '我在各区域的下注(仅在m_result=0,且m_betAccountId=自己时有效)' },
}

-- ----------------------------------------服务器---------------------------------------
SS_M2G_Bmw_GameCreate_Req =
{
	{ 1		, 1		, 'm_vecAccounts'		,		'PstBmwSceneSyncGameData'	, 1024		, '玩家数据'},
}

SS_G2M_Bmw_GameCreate_Ack =
{
	{ 1		, 1		, 'm_result'			, 		'INT'						, 1			, '0:成功, -1:失败' },
	{ 2		, 1		, 'm_vecAccounts'		,		'PstOperateRes'				, 1024		, '玩家初始化结果数据'},
}

SS_G2M_Bmw_GameResult_Nty =
{
	{ 1		, 1		, 'm_vecAccounts'		,		'PstBmwBalanceSvr'		, 1024		, '玩家数据'},
}