--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
local CardListBoard = class("CardListBoard",function()
    return display.newLayer()
end)
function CardListBoard:ctor()
    self:myInit()
    self:setupViews()

end   

	function CardListBoard:setupViews()
        local _eventDispatcher = cc.Director:getInstance():getEventDispatcher()
	     local listenner = cc.EventListenerTouchOneByOne:create();
        local  function onTouchBegan( touch, event )
		    local pos = touch:getLocation();
		    pos = self:convertToNodeSpace(pos);
		    local rect = cc.rect(0, 0, self:getContentSize().width, self:getContentSize().height);
		    if(not rect.containsPoint(pos)) then
		    
			    local winSize = cc.Director:getInstance():getWinSize();
			    local size = self:getContentSize();
			    local destPos = cc.p(winSize.width + size.width, 0);
			    self:stopAllActions();
			    local moveTo = cc.MoveTo:create(0.2,destPos);
			    local seq = cc.Sequence:create(moveTo, cc.RemoveSelf:create(), nil);	
			    self:runAction(seq);
		    end
            return true
	    end 
	    listenner:registerScriptHandler(onTouchBegan , cc.Handler.EVENT_TOUCH_BEGAN )
 
	    listenner:setSwallowTouches(true);
	    _eventDispatcher:addEventListenerWithSceneGraphPriority(listenner, self);
		
		 
		local bg = Sprite:create("Games/goldenflower/table/display_card_type.png");
		 
		local size = bg:getContentSize();

		self:setContentSize(size);
		bg:setPosition(size.width/2,size.height/2);
        bg:setAnchorPoint(cc.p(0.5,0.5));
		bg:ignoreAnchorPointForPosition(false);
		self:addChild(bg,1);
		return true;
	end
     return CardListBoard