--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion

local GAME_OUT = "Games/goldenflower/player/flag_out.png"; 
local Global = import(".GoldenFlowerGlobal") 
local PokerCard =import(".GoldenFlowerPokerCard")
local CardListBoard = class("CardListBoard",function()
    return display.newLayer()
end)
function CardListBoard:ctor(seatNo,iCardBg)
    self:myInit()
    self:setupViews(seatNo,iCardBg)

end 
   
function CardListBoard:setupViews( seatNo, iCardBg) 

	self:ignoreAnchorPointForPosition(false);
	self:setAnchorPoint(cc.p(0.5, 0.5));
	self:setContentSize(cc.p(0, 0));

    for i = 0, Global.MAX_CARD_COUNT-1 do 
        self._handCard[i] = PokerCard.new(0x00);
        self._handCard[i]:setCardValue(0);
        self._handCard[i]:setScale(0.86);
        self._handCard[i]:setAnchorPoint(cc.p(0.5, 0)); 
        iCardBg:addChild(self._handCard[i], i);
        self._handCard[i]:setVisible(false);
    end

    self:setHandCardPosition();
		 
end
function CardListBoard:getHandCardVisible()
    return self._handCard[0]:isVisible()
end
function CardListBoard:myInit()
	
    self._handCard = {}
    self._isQuit = false;
	
end
	 

function CardListBoard:setHandCardPosition()
         
    local parentSize = self._handCard[0]:getParent():getContentSize();
    for cardIndex = 0,Global.MAX_CARD_COUNT-1 do 
        self._handCard[cardIndex]:setPosition(cc.p(parentSize.width / 2 + 70*(cardIndex - 1), 50)); 
    end
end

function CardListBoard:setHandCard( index,  value)
    
    if (index == 0 or index == 1 or index == 2) then
        
        self._handCard[index]:setCardValue(value);
        self._handCard[index]:setVisible(true);

        if (value ~= 0x00) then
            
            self:reorderChild(self._handCard[index], 4);
            
        else
            
            self:reorderChild(self._handCard[index], 0);
        end
    end
end

function CardListBoard:showHandCard( cards)
    
    for  cardIndex = 0,#cards-1 do
        
        self._handCard[cardIndex]:setCardValue(cards[cardIndex+1]);
        self._handCard[cardIndex]:setVisible(true);
    end
end

function CardListBoard:lose()
    
    self._isQuit = true;
    for  i = 0, Global.MAX_CARD_COUNT-1 do
        
        if (self._handCard[i]:getCardValue() == 0) then
            
            self._handCard[i]:setGray(true);
        end          
    end
end
function CardListBoard:isShowCard()
    return self._handCard[0]:getCardValue() ~= 0
end
function CardListBoard:resetGray()
     for  i = 0, Global.MAX_CARD_COUNT-1 do 
         self._handCard[i]:setGray(false); 
    end
end
function CardListBoard:isQuit()
    
    return self._isQuit;
end

function CardListBoard:setHandCardBroken()
    
    for  i = 0, Global.MAX_CARD_COUNT-1 do
        
        self._handCard[i]:setCardValue(0x00);
		--self._handCard[i]:setGray(false);
    end
end

function CardListBoard:hideHandCard( visible)
    
    for  i = 0, Global.MAX_CARD_COUNT-1 do
        
        if (self._handCard[i]) then
            
            self._handCard[i]:setVisible(visible);
		end
    end
end	
             
function CardListBoard:getHandCard( index)
    
    if (index == 0 or index == 1 or index == 2) then
        
        return  self._handCard[index];
    end
    return nil;
end

function CardListBoard:changeHandCardValue( pNode, index, value)
    
    if (index == 0 or index == 1 or index == 2) then
        self._handCard[index]:setCardValue(value);
        self._handCard[index]:setVisible(true);

        if (value ~= 0x00) then
            
            self:reorderChild(self._handCard[index], 4);
            
        else
            
            self:reorderChild(self._handCard[index], 0);
        end
    end
end


return CardListBoard