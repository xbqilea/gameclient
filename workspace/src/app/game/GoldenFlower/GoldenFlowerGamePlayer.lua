--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
local USER_HEAD_MASK = "Games/goldenflower/player/touxiangdi.png";
local Player_Normal_M = "Games/goldenflower/player/icon_1.png";
local Player_Normal_W = "Games/goldenflower/player/icon_2.png";
local u_msgbox = "Games/goldenflower/player/moreinfo.csb";
local 	MESSAGE_ZORDER = 10;
local Global = import(".GoldenFlowerGlobal") 
local GameUserHead = require("src.app.newHall.rootLayer.UserHeadLayer")
local PlayerUI = class("PlayerUI",function()
    return display.newLayer()
end)
function PlayerUI:ctor(user)
    self:myInit()
    self:setupViews(user)

end   
 function PlayerUI:myInit()
    self._isBoy = true;
    self._userId = Global.INVALID_USER_ID;
    self._textName = nil;
    self._textMoney = nil; 
    self._ready = nil;          --准备
    self._readyDown = nil;      --准备在玩家头像下方（用于视图座位号3或4的玩家）
    self._nextGame = nil;       --提示下一局
    self._giveup = nil;         --弃牌
    self._watch = nil;          --看牌
    self._flatout = nil;		   --淘汰
    self._iOffLine = nil;
    self._ilocal = nil;
    self._iOwner = nil;
    self._iBanker = nil;
    self._iClock = nil;
    self._userHead = nil;
    self._headFrame = nil;	--头像框
    self._text_vipNum = nil;
    self._node_player = nil;
    self._userinfo = nil;
    self._curMoney = 0;
    self._bBoy = false;
 end   

function PlayerUI:setupViews(user) 
    self._node_player = user; 
    local img_Head =    user:getChildByName("Image_avatar");
	local img_Frame =   user:getChildByName("Image_frame");
	local img_VipFrame =user:getChildByName("Image_vip");
	self._text_vipNum = img_VipFrame:getChildByName("AtlasLabel_1");
	 
	img_VipFrame:setLocalZOrder(10000);
	self._text_vipNum:setLocalZOrder(10001); 
	img_Frame:setVisible(false);
	img_Head:setVisible(false);
	local headUrl = ToolKit:getHead(1)
	self._userHead = GameUserHead.new(img_Head,img_Frame);
    self._userHead:show(USER_HEAD_MASK);
	self._userHead:loadTexture(headUrl);
	self._userHead:setLocalZOrder(2000);
	self._userHead:setVisible(true);
 	 
    local image_bg = user:getChildByName("Image_bg");
    self._textName = image_bg:getChildByName("Text_nickname");
    self._textMoney = image_bg:getChildByName("AtlasLabel_money");
     self.atlas_winCoinNum = image_bg:getChildByName("atlas_winCoinNum"); 
     
      self.atlas_loseCoinNum = image_bg:getChildByName("atlas_loseCoinNum"); 
	local Ysize = self._textMoney:getContentSize().width;
    local Image_moneybg = image_bg:getChildByName("Image_moneybg");
    Image_moneybg:setVisible(false) 
    self._textMoney:setScale(1.05);
    self._iOffLine = user:getChildByName("Image_offline");
    self._iOffLine:setLocalZOrder(3);
--    self._ilocal = user:getChildByName("Image_local");
--    self._ilocal:setLocalZOrder(3);
    self._iOwner = user:getChildByName("Image_host");
    self._iOwner:setLocalZOrder(4);
    self._iBanker = user:getChildByName("Image_dealer");
    self._iBanker:setLocalZOrder(4);
    self._ready = user:getChildByName("Image_ready");
    self._ready:setLocalZOrder(3);
    self._readyDown = user:getChildByName("Image_ready_down");
    self._readyDown:setLocalZOrder(3);
    self._nextGame = user:getChildByName("Image_nextGame");
    self._nextGame:setLocalZOrder(3);
    self._giveup = user:getChildByName("Image_giveup");
    self._giveup:setLocalZOrder(4);
	self._flatout = user:getChildByName("Image_flatout");
	self._flatout:setLocalZOrder(4);
    self._watch = user:getChildByName("Image_watch");
    self._watch:setLocalZOrder(4);
    return true;
end

function PlayerUI:loadUser( info)
    
    self._userinfo = info;
    self:setUserId(info.m_accountId);
    self:setHead(info.m_faceId);
	self:setHeadFrame(info.m_frameId);
	self:setVip(info.m_vipLevel);  
    self:setUserName(info.m_nickname);
	self:setUserMoney(info.m_score);
	self._curMoney = info.m_score*0.01;
   -- self:setlocalHead(false);
    self:setOwner(false);
    self:setBanker(false);
    self:setReady(false, Global.INVALID_DESKSTATION);
    self:setWatch(false,Global.INVALID_DESKSTATION);
    self:setGiveup(false, Global.INVALID_DESKSTATION);
	self:setFlatOut(false, Global.INVALID_DESKSTATION);
  --  self:setOffline(info.bUserState == USER_CUT_GAME);
    self:setVisible(true);
end
function PlayerUI:setAfterBetMoney(money)
    local fMoney = money*0.01;
    self._curMoney = self._curMoney - fMoney
	self._textMoney:setString( self._curMoney);
end
function PlayerUI:setVisible( visible)
    
    if (self._node_player) then
        
        self._node_player:setVisible(visible);
    end
end

function PlayerUI:setUserSex( isBoy)
    
    self._isBoy = isBoy;
end

function PlayerUI:setUserId( userId)
    
    self._userId = userId;
end

function PlayerUI:setUserName( name)
     
    if (nil == self._textName) then
        return;
    end
	self._textName:setFontName("platform/common/RTWSYueRoudGoG0v1-Regular.ttf");
	self._textName:setTextAreaSize(cc.size(103,23));
	self._textName:setAnchorPoint(cc.p(0.5,0.5));
	self._textName:setTextHorizontalAlignment(0);
	self._textName:setTextVerticalAlignment(0);
    self._textName:setString(name);
	self._textName:setPositionX(100);
end
function PlayerUI:getNodeOfPlayer()
    return self._node_player
end
function PlayerUI:setUserMoney( money)
    
    if (nil == self._textMoney) then
        return;
    end
	local fMoney = money*0.01;
    self._curMoney = fMoney
	self._textMoney:setString( fMoney);
end	
function PlayerUI:playHeadMoney( money)
    
    
	local fMoney = money*0.01;
     if (0 < fMoney) then 
					 
		self.atlas_winCoinNum:setString("+"..fMoney); 
		local fadeIn = cc.FadeIn:create(1.0);
		local fadeOut = cc.FadeOut:create(1.0);
		local seq = cc.Sequence:create(cca.delay(1.5), cc.CallFunc:create(function()

			self.atlas_winCoinNum:setVisible(true);
			self.atlas_winCoinNum:setOpacity(0);

		end), fadeIn, fadeOut, nil);
		self.atlas_winCoinNum:runAction(seq);

	end
	if (0 > fMoney) then 
		self.atlas_loseCoinNum:setString(fMoney);

		local fadeIn = cc.FadeIn:create(1.0);
		local fadeOut = cc.FadeOut:create(1.0);
		local seq = cc.Sequence:create(cca.delay(1.5),  cc.CallFunc:create(function()
			self.atlas_loseCoinNum:setVisible(true);
			self.atlas_loseCoinNum:setOpacity(0);
		end), fadeIn, fadeOut, nil);
		self.atlas_loseCoinNum:runAction(seq);

	end  
end			
    

function PlayerUI:setlocalHead( islocal)
  
    if (nil == self._ilocal) then
        return;
    end
    self._ilocal:setVisible(islocal);
end

function PlayerUI:setHead(ID)
    
    if (nil == self._userHead) then
        return;
    end 
	local headUrl = ToolKit:getHead(ID)
	self._userHead:loadTexture(headUrl,1);

end
function PlayerUI:setVip( vipnum)
	
	local str = string.format("%d", vipnum);
	self._text_vipNum:setString(str);
end

function PlayerUI:setHeadFrame( id)
	if (nil == self._headFrame) then
        return;
    end
	local headFrameUrl = string.format("platform/userData/res/frame/frame_%d.png", id);
	self._headFrame:setTexture(headFrameUrl);
    self._headFrame:setVisible(false)
end

function PlayerUI:setBanker( isBanker)
    if (nil == self._iBanker) then
        return;
    end
    self._iBanker:setVisible(false);
end

function PlayerUI:setOwner( isOwner)
    if (nil == self._iOwner) then
        return;
    end
    self._iOwner:setVisible(isOwner);
end

function PlayerUI:setOffline( isOffline)
    
    if (nil == self._iOffLine) then
        return;
    end
    self._iOffLine:setVisible(isOffline);
end

function PlayerUI:getHeadPositionX()
    
    if (self._node_player) then
        
        return self._node_player:getPositionX();
    end
    return 0;
end
function PlayerUI:getPlayerHead()
    return self._userHead
end
function PlayerUI:getHeadPositionY()
    
    if (self._node_player) then
        
        return self._node_player:getPositionY();
    end
    return 0;
end
function PlayerUI:getHeadSize()
    
    local img_Frame = self._node_player:getChildByName("Image_frame");

    if (img_Frame) then
        
        return img_Frame:getContentSize();
  
    end
    return cc.Size(0,0);
end

function PlayerUI:getChatParent()
    
    local img_Frame = self._node_player:getChildByName("Image_frame");

    return img_Frame;
end

function PlayerUI:setReady( bshow,  viewSeat)
    if (nil == self._ready) then
        return;
    end
    if (bshow) then 
            
            self._ready:setVisible(bshow); 
        
    else
        
        self._ready:setVisible(bshow);
      --  self._readyDown:setVisible(bshow);
    end
end

function PlayerUI:setNextGame( bshow) 
    if (nil == self._nextGame) then
        return;
    end
    self._nextGame:setVisible(bshow);
end

	
function PlayerUI:setGiveup( bshow,  viewSeat)
    if (nil == self._giveup) then
        return;
    end

    self._giveup:setVisible(bshow);
    --显示的时候需要调整位置
    if (viewSeat ~= Global.INVALID_DESKSTATION) then
        
        local image_card = self._node_player:getParent():getChildByName(string.format("Image_card_%d", viewSeat));
        local worldPosition = image_card:getParent():convertToWorldSpace(cc.p(image_card:getPositionX(),image_card:getPositionY()));
        local nodePosition = self._node_player:convertToNodeSpace(worldPosition);
        self._giveup:setPosition(nodePosition.x, 40+nodePosition.y);

        if (3 == viewSeat or 4 == viewSeat) then
            
            self._giveup:setPosition(nodePosition.x, 30+nodePosition.y);
        end
    end
end

function PlayerUI:setWatch( bshow, viewSeat) 
    if (nil == self._watch) then
        return;
    end
    self._watch:setVisible(bshow);
    --显示的时候需要调整位置
    if (viewSeat ~= Global.INVALID_DESKSTATION) then
        
        local image_card = self._node_player:getParent():getChildByName(string.format("Image_card_%d", viewSeat));
        local worldPosition = image_card:getParent():convertToWorldSpace(cc.p(image_card:getPositionX(),image_card:getPositionY()));
        local nodePosition = self._node_player:convertToNodeSpace(worldPosition);
        self._watch:setPosition(nodePosition.x, 40+nodePosition.y);

        if (3 == viewSeat or 4 == viewSeat) then
            
            self._watch:setPosition(nodePosition.x, 30+nodePosition.y);
        end
    end
end


function PlayerUI:setFlatOut( bshow,  viewSeat)
	
	if (nil == self._flatout) then
        return;
    end
	self._flatout:setVisible(bshow);

	if (viewSeat ~= Global.INVALID_DESKSTATION) then
		
		local image_card = self._node_player:getParent():getChildByName(string.format("Image_card_%d", viewSeat));
		local worldPosition = image_card:getParent():convertToWorldSpace(cc.p(image_card:getPositionX(),image_card:getPositionY()));
		local nodePosition = self._node_player:convertToNodeSpace(worldPosition);
		self._flatout:setPosition(nodePosition.x, 40+nodePosition.y);

		if (3 == viewSeat or 4 == viewSeat) then
			
			self._flatout:setPosition(nodePosition.x, 30+nodePosition.y);
		end
	end
end


function PlayerUI:playWaitTime()
    
--		/*local image_bg = self._node_player:getChildByName("Image_bg"));
--		local TouXiang = SkeletonAnimation:createWithFile("goldenflower/skeleton/touXiang/default.json", "goldenflower/skeleton/touXiang/default.atlas");
--		TouXiang:setPosition(image_bg:getContentSize() / 2);
--		TouXiang:setName("touXiangAnimation");
--		image_bg:addChild(TouXiang, 3);
--		local ani = TouXiang:setAnimation(0, "animation", true);
--		TouXiang:setTrackEndListener(ani, [=](local trackIndex) {
--			TouXiang:runAction(Sequence:create(DelayTime:create(0.02f), RemoveSelf:create(true), nil));
--		});*/
end

function PlayerUI:stopWaitTime()
    
    local image_bg = self._node_player:getChildByName("Image_bg");
    local TouXiang = image_bg:getChildByName("touXiangAnimation");
    if (TouXiang)then
        
        TouXiang:stopAllActions();
        TouXiang:removeFromParent();
    end   
end

function PlayerUI:playAction( action)
    
    local audio=""
    local filename="" 
    if OperateAction.eLookCard == action then
        audio=GOLD_LOOK_LADY
        if self._bBoy then
            audio =  GOLD_LOOK_MAN 
        end
        filename ="Games/goldenflower/table/desc_see.png"; 
        
    elseif OperateAction.eGiverUp == action then
        audio=GOLD_GIVEUP_LADY
        if self._bBoy then
            audio =  GOLD_GIVEUP_MAN 
        end  
        
    elseif OperateAction.eNote == action then
		audio=GOLD_ADD_LADY
        if self._bBoy then
            audio =  GOLD_ADD_MAN 
        end  
        filename="Games/goldenflower/table/desc_add.png";
        
    elseif OperateAction.eFollow == action then 
        audio=GOLD_FOLLOW_LADY
        if self._bBoy then
            audio =  GOLD_FOLLOW_MAN 
        end  
        filename="Games/goldenflower/table/desc_follow.png";
        
	elseif OperateAction.eCompareCard == action then  
        audio=GOLD_PK_LADY
        if self._bBoy then
            audio =  GOLD_PK_MAN 
        end  
        filename="Games/goldenflower/table/desc_cmp.png";
		
	end

        g_AudioPlayer:playEffect(audio); 
  

    if ( filename~="") then
        
        local sp = cc.Sprite:create(filename);
        sp:setPosition(self._userHead:getContentSize().width / 2, self._userHead:getContentSize().height / 2);
        self._userHead:addChild(sp, 10);
        sp:setGlobalZOrder(10);
        sp:runAction(cc.Sequence:create(cc.MoveBy:create(0.2, cc.p(0, 90)), cc.DelayTime:create(0.8), cc.RemoveSelf:create(), nil));
    end
end

function PlayerUI:getCurMoney()
	
	return self._curMoney;
end

function PlayerUI:getUserSex()
	
    return self._bBoy;
end
     

	
return PlayerUI
