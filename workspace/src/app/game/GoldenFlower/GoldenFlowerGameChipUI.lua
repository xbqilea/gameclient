--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion

local GameChipUI = class("GameChipUI",function()
    return display.newSprite()
end)
function GameChipUI:ctor(filename)
    self:myInit()
    self:setupViews(filename)

end 
 

function GameChipUI:setValue( value)
	
		
	local textAtlas = ccui.TextAtlas:create("0", "Games/goldenflower/dashboard/number_chips9.png", 18, 21, ".");
	if(value ~= self._value) then 
		self._value = value;
--			local tmp = {};
--			sprintf(tmp,"%lld",self._value);

		textAtlas:setString(self._value);
	end 
	textAtlas:setVisible(self._value > 0);
		 
	textAtlas:setPosition(self:getContentSize().width / 2 , self:getContentSize().height/ 2 +5);
    textAtlas:setScaleX(0.8)
	self:addChild(textAtlas);
end

function GameChipUI:getValue() 
	
	return self._value;
end

function GameChipUI:setupViews(filename)
	
	 self:setTexture(filename)
	local size = self:getContentSize();
	self._text = ccui.Text:create("0","Arial",25);
	self._text:setColor(cc.c3b(0,0,0));
	self._text:setAnchorPoint(cc.p(0.5, 0.5));
    self._text:setPosition(size.width / 2, size.height*0.6);
    self._text:setScaleX(0.7);
	self._text:setVisible(false);
	self:addChild(self._text); 
end

function GameChipUI:myInit()
    self._value = 0
    self._text =nil
end

return GameChipUI