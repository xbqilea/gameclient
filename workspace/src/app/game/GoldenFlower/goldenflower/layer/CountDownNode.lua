--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local CountDownNode = class("CountDownNode", function()
return display.newNode()
end)

local scheduler = require("framework.scheduler")

local PROGRESS_TAG = 100

local START_RED = 68
local START_GREEN = 185
local START_BLUE = 22

local END_RED = 207
local END_GREEN = 49
local END_BLUE = 49

function CountDownNode:ctor()
   -- self:enableNodeEvents()

    self.red = 0
    self.green = 0
    self.blue = 0
    self.entryId = nil
    self.uid = 0
    self.allTime = 0
    self.leftTime = 0
    self.nowLength = 0
    self.allLength =  0
    self.allColorLength = 0
    self.width = 0
    self.height = 0
    self.cdpos = cc.p(0,0)
    self.isPlayAudio = false
    self.isShowing = false
    self.draw = cc.DrawNode:create()
    self:addChild(self.draw, 10)

    --诈金花现在很卡，帧率低导致进度条很慢，改用真实时间
    self.m_fBeginTime = 0
end

function CountDownNode:release()
    --unschedule(schedule_selector(countDownManage::startCountDownVoice));
    --self:stopCountDownVoice()
end

function CountDownNode:startCountDownVoice(s)

end

function CountDownNode:stopCountDownVoice()

end

function CountDownNode:DrawNode(s1)
    if self.entryId ~= nil then
        local durTime = os.clock() - self.m_fBeginTime
        self.leftTime = self.allTime - durTime
        if self.leftTime <= 0 and self.isPlayAudio then
            self.isPlayAudio = false
            AudioManager.getInstance():playSound("zjh/zjh/sound/lottery_xz_start.mp3")
        end
    
        local rate = self.leftTime / self.allTime
        if rate <= 0 then
            rate = 0.01
        end
    
        self.draw:clear()
        --local s = CCDirector.::getInstance()->getWinSize();
        self.nowLength= self.allLength * rate
    
        local lenght = self.allColorLength * ( 1 - rate )
    
        local red1 =18
        local green1=255
        local blue1 = 0
    
        local drawRadius = 2.5   --//4; --划线的宽度
        if (self.red+lenght) > 255 then
            red1 = 255
            green1 = 255 -(self.red+lenght-255)
        else
            red1 = self.red+lenght
        end
        local color = cc.c4b(red1/255, green1/255, blue1/255,1)
        local radius = 13
        local angle = 0
        if self.nowLength > (self.width * 1.5 + self.height *2 + radius) then        
            local hasPass = self.width*0.5 - (self.nowLength - self.width*1.5 - self.height*2)
            self.draw:drawSegment(cc.p(self.cdpos.x + hasPass,self.cdpos.y + self.height*0.5), cc.p(self.cdpos.x + self.width*0.5 - radius,self.cdpos.y + self.height*0.5), drawRadius, color)        
            --for (float angle = 0.5 * M_PI; angle >= 0; angle -= 0.01)
            for angle = 0.5 * M_PI, 0, -0.01 do
                self.draw:drawDot(cc.p(radius * math.cos(angle) + self.cdpos.x + self.width*0.5 - radius, radius * math.sin(angle) + self.cdpos.y + self.height*0.5 - radius),drawRadius, color)            
            end
            self.draw:drawSegment(cc.p(self.cdpos.x+self.width*0.5,self.cdpos.y+self.height*0.5-radius), cc.p(self.cdpos.x+self.width*0.5,self.cdpos.y-self.height*0.5+radius), drawRadius, color)
            --for (float angle = 2 * M_PI; angle >= 1.5* M_PI; angle -= 0.01)
            for angle = 2 * M_PI, 1.5* M_PI, -0.01 do
                self.draw:drawDot(cc.p(radius * math.cos(angle)+self.cdpos.x+self.width*0.5-radius, radius * math.sin(angle)+self.cdpos.y-self.height*0.5+radius), drawRadius, color)
            end
            self.draw:drawSegment(cc.p(self.cdpos.x+self.width*0.5-radius,self.cdpos.y-self.height*0.5), cc.p(self.cdpos.x-self.width*0.5+radius,self.cdpos.y-self.height*0.5),drawRadius, color)
            --for (float angle = 1.5 * M_PI; angle >= 1.0* M_PI; angle -= 0.01)
            for angle = 1.5 * M_PI, 1.0* M_PI, -0.01 do
                self.draw:drawDot(cc.p(radius * math.cos(angle)+self.cdpos.x-self.width*0.5+radius, radius * math.sin(angle)+self.cdpos.y-self.height*0.5+radius), drawRadius, color)
            end
            self.draw:drawSegment(cc.p(self.cdpos.x-self.width*0.5,self.cdpos.y-self.height*0.5+radius), cc.p(self.cdpos.x-self.width*0.5,self.cdpos.y+self.height*0.5-radius),drawRadius, color)     
            --for (float angle = 1.0 * M_PI; angle >= 0.5* M_PI; angle -= 0.01)
            for angle = 1.0 * M_PI, 0.5* M_PI, -0.01 do
                self.draw:drawDot(cc.p(radius * math.cos(angle)+self.cdpos.x-self.width*0.5+radius, radius * math.sin(angle)+self.cdpos.y+self.height*0.5-radius), drawRadius, color)
            end
            self.draw:drawSegment(cc.p(self.cdpos.x-self.width*0.5+radius,self.cdpos.y+self.height*0.5), cc.p(self.cdpos.x,self.cdpos.y+self.height*0.5),drawRadius, color)
        elseif self.nowLength > (self.width*1.5+self.height*2-radius) then
            local left = self.nowLength-self.width*1.5-self.height*2+radius
            --for (float angle = 0.5 * M_PI*((left)/(2*radius)); angle >= 0; angle -= 0.01)
            for angle = 0.5 * M_PI*((left)/(2*radius)), 0, -0.01 do
                self.draw:drawDot(cc.p(radius * math.cos(angle)+self.cdpos.x+self.width*0.5-radius, radius * math.sin(angle)+self.cdpos.y+self.height*0.5-radius),drawRadius, color)
            end
            self.draw:drawSegment(cc.p(self.cdpos.x+self.width*0.5,self.cdpos.y+self.height*0.5-radius), cc.p(self.cdpos.x+self.width*0.5,self.cdpos.y-self.height*0.5+radius), drawRadius, color)
            --for (float angle = 2 * M_PI; angle >= 1.5* M_PI; angle -= 0.01)
            for angle = 2 * M_PI, 1.5* M_PI, -0.01 do
                self.draw:drawDot(cc.p(radius * math.cos(angle)+self.cdpos.x+self.width*0.5-radius, radius * math.sin(angle)+self.cdpos.y-self.height*0.5+radius), drawRadius, color)
            end
            self.draw:drawSegment(cc.p(self.cdpos.x+self.width*0.5-radius,self.cdpos.y-self.height*0.5), cc.p(self.cdpos.x-self.width*0.5+radius,self.cdpos.y-self.height*0.5),drawRadius, color)
            --for (float angle = 1.5 * M_PI; angle >= 1.0* M_PI; angle -= 0.01)
            for angle = 1.5 * M_PI, 1.0* M_PI, -0.01 do
                self.draw:drawDot(cc.p(radius * math.cos(angle)+self.cdpos.x-self.width*0.5+radius, radius * math.sin(angle)+self.cdpos.y-self.height*0.5+radius), drawRadius, color)
            end
            self.draw:drawSegment(cc.p(self.cdpos.x-self.width*0.5,self.cdpos.y-self.height*0.5+radius), cc.p(self.cdpos.x-self.width*0.5,self.cdpos.y+self.height*0.5-radius),drawRadius, color)        
            --for (float angle = 1.0 * M_PI; angle >= 0.5* M_PI; angle -= 0.01)
            for angle = 1.0 * M_PI, 0.5* M_PI, -0.01 do
                self.draw:drawDot(cc.p(radius * math.cos(angle)+self.cdpos.x-self.width*0.5+radius, radius * math.sin(angle)+self.cdpos.y+self.height*0.5-radius), drawRadius, color)
            end
            self.draw:drawSegment(cc.p(self.cdpos.x-self.width*0.5+radius,self.cdpos.y+self.height*0.5), cc.p(self.cdpos.x,self.cdpos.y+self.height*0.5),drawRadius, color)
        elseif self.nowLength > (self.width*1.5+self.height+radius) then
            local hasPass = self.height-radius*2-(self.nowLength-self.width*1.5-self.height-radius)
            self.draw:drawSegment(cc.p(self.cdpos.x+self.width*0.5,self.cdpos.y+self.height*0.5-radius-hasPass), cc.p(self.cdpos.x+self.width*0.5,self.cdpos.y-self.height*0.5+radius), drawRadius, color)
            --for (float angle = 2 * M_PI; angle >= 1.5* M_PI; angle -= 0.01)
            for angle = 2 * M_PI, 1.5* M_PI, -0.01 do
                self.draw:drawDot(cc.p(radius * math.cos(angle)+self.cdpos.x+self.width*0.5-radius, radius * math.sin(angle)+self.cdpos.y-self.height*0.5+radius), drawRadius, color)
            end
            self.draw:drawSegment(cc.p(self.cdpos.x+self.width*0.5-radius,self.cdpos.y-self.height*0.5), cc.p(self.cdpos.x-self.width*0.5+radius,self.cdpos.y-self.height*0.5), drawRadius, color)
            --for (float angle = 1.5 * M_PI; angle >= 1.0* M_PI; angle -= 0.01)
            for angle = 1.5 * M_PI, 1.0* M_PI, -0.01 do
                self.draw:drawDot(cc.p(radius * math.cos(angle)+self.cdpos.x-self.width*0.5+radius, radius * math.sin(angle)+self.cdpos.y-self.height*0.5+radius), drawRadius, color)
            end
            self.draw:drawSegment(cc.p(self.cdpos.x-self.width*0.5,self.cdpos.y-self.height*0.5+radius), cc.p(self.cdpos.x-self.width*0.5,self.cdpos.y+self.height*0.5-radius),drawRadius, color)
            --for (float angle = 1.0 * M_PI; angle >= 0.5* M_PI; angle -= 0.01)
            for angle = 1.0 * M_PI, 0.5* M_PI, -0.01 do
                self.draw:drawDot(cc.p(radius * math.cos(angle)+self.cdpos.x-self.width*0.5+radius, radius * math.sin(angle)+self.cdpos.y+self.height*0.5-radius), drawRadius, color)
            end
            self.draw:drawSegment(cc.p(self.cdpos.x-self.width*0.5+radius,self.cdpos.y+self.height*0.5), cc.p(self.cdpos.x,self.cdpos.y+self.height*0.5),drawRadius, color)
        elseif self.nowLength > (self.width*1.5+self.height-radius) then
            local haspass  = 2*radius - (self.nowLength-self.width*1.5-self.height+radius)        
            --for (float angle = 2 * M_PI-0.5*M_PI*(haspass/(2*radius)); angle >= 1.5* M_PI; angle -= 0.01)
            for angle = 2 * M_PI-0.5*M_PI*(haspass/(2*radius)), 1.5* M_PI, -0.01 do
                self.draw:drawDot(cc.p(radius * math.cos(angle)+self.cdpos.x+self.width*0.5-radius, radius * math.sin(angle)+self.cdpos.y-self.height*0.5+radius), drawRadius, color)
            end
            self.draw:drawSegment(cc.p(self.cdpos.x+self.width*0.5-radius,self.cdpos.y-self.height*0.5), cc.p(self.cdpos.x-self.width*0.5+radius,self.cdpos.y-self.height*0.5), drawRadius, color)
            --for (float angle = 1.5 * M_PI; angle >= 1.0* M_PI; angle -= 0.01)
            for angle = 1.5 * M_PI, 1.0* M_PI, -0.01 do
                self.draw:drawDot(cc.p(radius * math.cos(angle)+self.cdpos.x-self.width*0.5+radius, radius * math.sin(angle)+self.cdpos.y-self.height*0.5+radius), drawRadius, color)
            end
            self.draw:drawSegment(cc.p(self.cdpos.x-self.width*0.5,self.cdpos.y-self.height*0.5+radius), cc.p(self.cdpos.x-self.width*0.5,self.cdpos.y+self.height*0.5-radius),drawRadius, color)
            --for (float angle = 1.0 * M_PI; angle >= 0.5* M_PI; angle -= 0.01)
            for angle = 1.0 * M_PI, 0.5* M_PI, -0.01 do
                self.draw:drawDot(cc.p(radius * math.cos(angle)+self.cdpos.x-self.width*0.5+radius, radius * math.sin(angle)+self.cdpos.y+self.height*0.5-radius), drawRadius, color)
            end
            self.draw:drawSegment(cc.p(self.cdpos.x-self.width*0.5+radius,self.cdpos.y+self.height*0.5), cc.p(self.cdpos.x,self.cdpos.y+self.height*0.5),drawRadius, color)
        elseif self.nowLength > (self.width*0.5+self.height+radius) then
            local hasPass = self.width-2*radius-(self.nowLength-self.width*0.5-self.height-radius)
            self.draw:drawSegment(cc.p(self.cdpos.x+self.width*0.5-radius-hasPass,self.cdpos.y-self.height*0.5), cc.p(self.cdpos.x-self.width*0.5+radius,self.cdpos.y-self.height*0.5), drawRadius, color)
            --for (float angle = 1.5 * M_PI; angle >= 1.0* M_PI; angle -= 0.01)
            for angle = 1.5 * M_PI, 1.0* M_PI, -0.01 do
                self.draw:drawDot(cc.p(radius * math.cos(angle)+self.cdpos.x-self.width*0.5+radius, radius * math.sin(angle)+self.cdpos.y-self.height*0.5+radius), drawRadius, color)
            end
            self.draw:drawSegment(cc.p(self.cdpos.x-self.width*0.5,self.cdpos.y-self.height*0.5+radius), cc.p(self.cdpos.x-self.width*0.5,self.cdpos.y+self.height*0.5-radius),drawRadius, color)
            --for (float angle = 1.0 * M_PI; angle >= 0.5* M_PI; angle -= 0.01)
            for angle = 1.0 * M_PI, 0.5* M_PI, -0.01 do
                self.draw:drawDot(cc.p(radius * math.cos(angle)+self.cdpos.x-self.width*0.5+radius, radius * math.sin(angle)+self.cdpos.y+self.height*0.5-radius), drawRadius, color)
            end
            self.draw:drawSegment(cc.p(self.cdpos.x-self.width*0.5+radius,self.cdpos.y+self.height*0.5), cc.p(self.cdpos.x,self.cdpos.y+self.height*0.5),drawRadius, color)
        elseif self.nowLength > (self.width*0.5+self.height-radius) then
            local haspass  = 2*radius-( self.nowLength-self.width*0.5-self.height+radius)
            --for (float angle = 1.5 * M_PI-0.5*M_PI*(haspass/(2*radius)); angle >= 1.0* M_PI; angle -= 0.01)
            for angle = 1.5 * M_PI-0.5*M_PI*(haspass/(2*radius)), 1.0* M_PI, -0.01 do
                self.draw:drawDot(cc.p(radius * math.cos(angle)+self.cdpos.x-self.width*0.5+radius, radius * math.sin(angle)+self.cdpos.y-self.height*0.5+radius), drawRadius, color)
            end
            self.draw:drawSegment(cc.p(self.cdpos.x-self.width*0.5,self.cdpos.y-self.height*0.5+radius), cc.p(self.cdpos.x-self.width*0.5,self.cdpos.y+self.height*0.5-radius),drawRadius, color)
            --for (float angle = 1.0 * M_PI; angle >= 0.5* M_PI; angle -= 0.01)
            for angle = 1.0 * M_PI, 0.5* M_PI, -0.01 do
                self.draw:drawDot(cc.p(radius * math.cos(angle)+self.cdpos.x-self.width*0.5+radius, radius * math.sin(angle)+self.cdpos.y+self.height*0.5-radius), drawRadius, color)
            end
            self.draw:drawSegment(cc.p(self.cdpos.x-self.width*0.5+radius,self.cdpos.y+self.height*0.5), cc.p(self.cdpos.x,self.cdpos.y+self.height*0.5),drawRadius, color)
        elseif self.nowLength > (self.width*0.5+radius) then
            local hasPass = self.height-2*radius-(self.nowLength-self.width*0.5-radius)
            self.draw:drawSegment(cc.p(self.cdpos.x-self.width*0.5,self.cdpos.y-self.height*0.5+radius+hasPass), cc.p(self.cdpos.x-self.width*0.5,self.cdpos.y+self.height*0.5-radius),drawRadius, color)
            --for (float angle = 1.0 * M_PI; angle >= 0.5* M_PI; angle -= 0.01)
            for angle = 1.0 * M_PI, 0.5* M_PI, -0.01 do
                self.draw:drawDot(cc.p(radius * math.cos(angle)+self.cdpos.x-self.width*0.5+radius, radius * math.sin(angle)+self.cdpos.y+self.height*0.5-radius), drawRadius, color)
            end
            self.draw:drawSegment(cc.p(self.cdpos.x-self.width*0.5+radius,self.cdpos.y+self.height*0.5), cc.p(self.cdpos.x,self.cdpos.y+self.height*0.5),drawRadius, color)
        elseif self.nowLength > (self.width*0.5-radius) then
            local hasPass  = 2*radius-(self.nowLength-self.width*0.5+radius)
            --for (float angle = 1.0 * M_PI-0.5*M_PI*(hasPass/(2*radius)); angle >= 0.5* M_PI; angle -= 0.01)
            for angle = 1.0 * M_PI-0.5*M_PI*(hasPass/(2*radius)), 0.5* M_PI, -0.01 do
                self.draw:drawDot(cc.p(radius * math.cos(angle)+self.cdpos.x-self.width*0.5+radius, radius * math.sin(angle)+self.cdpos.y+self.height*0.5-radius), drawRadius, color)
            end
            self.draw:drawSegment(cc.p(self.cdpos.x-self.width*0.5+radius,self.cdpos.y+self.height*0.5), cc.p(self.cdpos.x,self.cdpos.y+self.height*0.5),drawRadius, color)
        elseif self.nowLength > 0 then
            local hasPass = self.width*0.5-radius-self.nowLength
            self.draw:drawSegment(cc.p(self.cdpos.x-self.width*0.5+radius+hasPass,self.cdpos.y+self.height*0.5), cc.p(self.cdpos.x,self.cdpos.y+self.height*0.5),drawRadius, color)
        else
            if self.entryId then
	            scheduler.unscheduleGlobal(self.entryId)
                self.entryId = nil
            end
        end
    end
end

function CountDownNode:startCountDown(uid,leftSecond,allSecond,pos,pWidth,pHeight)
    self.isShowing = true
    self.isPlayAudio = true
    self.uid = uid
    
    local scale = 1       --//GameSystem::getInstance()->scale;
    --[[local targetPlatform = cc.Application:getInstance():getTargetPlatform()
    if cc.PLATFORM_OS_ANDROID == targetPlatform then
        local  visibleRect = cc.Director:getInstance():getOpenGLView():getVisibleRect()         --注意
        local wh = visibleRect.size.width / visibleRect.size.height
        if wh > 1.7 then
            scale = 1.0
        else
            scale = scale-0.07
        end
    else--]]
        scale = (scale == 1 and scale) or (scale-0.05)
    --end
    
    self.width= pWidth*scale + 5
    self.height= pHeight*scale + 5
    self.allLength = self.width*2 + self.height*2
    
    --unschedule(schedule_selector(countDownManage::startCountDownVoice));
    --self:stopCountDownVoice()
    self.draw:clear()
    if leftSecond> allSecond then
        leftSecond = allSecond
    end
    local leftRate = (leftSecond*1.0) / allSecond
    self.nowLength = self.allLength*leftRate
    self.allColorLength = (255+255-18)*leftRate
    print("self.allColorLength:",self.allColorLength)
    self.leftTime = leftSecond
    self.allTime = allSecond
    self.cdpos = pos
    self.red=18
    self.green=255
    self.blue= 0
    --self.DrawNode()
    self.m_fBeginTime =  os.clock()
    self.entryId = scheduler.scheduleGlobal(handler(self, self.DrawNode), 0.02)
end

function CountDownNode:hideCountDown()
    self.isShowing = false
    --unschedule(schedule_selector(countDownManage::startCountDownVoice));
    --self:stopCountDownVoice()
    if self.entryId then
	    scheduler.unscheduleGlobal(self.entryId)
        self.entryId = nil
    end
    self.draw:clear()
    self.m_fBeginTime = 0
end

return CountDownNode
--endregion
