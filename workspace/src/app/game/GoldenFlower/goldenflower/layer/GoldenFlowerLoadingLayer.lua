--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local GoldenFlowerRes = import("..scene.GoldenFlowerRes") 
local CommonLoading = require("src.app.newHall.layer.CommonLoading")
local GoldenFlowerLoadingLayer = class("GoldenFlowerLoadingLayer",  function ()
    return CommonLoading.new()
end)

local PATH_CSB = "game/goldenflower/csb/gui-gf-loadLayer.csb"

function GoldenFlowerLoadingLayer.loading()
    return GoldenFlowerLoadingLayer.new(true)
end

function GoldenFlowerLoadingLayer.reload()
    return GoldenFlowerLoadingLayer.new(false)
end

function GoldenFlowerLoadingLayer:ctor(bBool)
     self:setNodeEventEnabled(true)
    self.bLoad = bBool
    self:init()
end

function GoldenFlowerLoadingLayer:init()
 --   self.super:init(self)
    self:initCSB()
    self:initCommonLoad()
      self:startLoading()
end

function GoldenFlowerLoadingLayer:onEnter()
   -- self.super:onEnter()
end

function GoldenFlowerLoadingLayer:onExit()
    --self.super:onExit()
end


function GoldenFlowerLoadingLayer:initCSB()

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    --ccb
    self.m_pathUI = cc.CSLoader:createNode(PATH_CSB)
    self.m_pathUI:setPositionX((display.width - 1624) / 2)
    self.m_pathUI:addTo(self.m_rootUI)

    --node
    self.m_pNodeBase = self.m_pathUI:getChildByName("Layer_base")
    self.m_pNodeBg   = self.m_pNodeBase:getChildByName("Node_bg")
    self.m_pNodeLoad = self.m_pNodeBase:getChildByName("Node_load")
    self.m_pNodeText = self.m_pNodeBase:getChildByName("Node_text")

    --bar
    self.m_pLoadingBar = self.m_pNodeLoad:getChildByName("LoadingBar")

    --text
    self.m_pLabelPercent = self.m_pNodeText:getChildByName("Text_percent")
    self.m_pLabelWord    = self.m_pNodeText:getChildByName("Text_word")

    self.m_pImageLogo    = self.m_pNodeBg:getChildByName("Image_logo")
    ----------------------------------------------
    --百度推广需求
--    if CommonUtils:getInstance():getIsBaiduCheck() then
--        self.m_pImageLogo:setVisible(false)
--    end
    --10549-10552百度推广包
    --local channel = LuaNativeBridge.getInstance():getAppChannel()
    --if 10549 <= channel and channel <= 10552 then
    --    self.m_pImageLogo:setVisible(false)
    --end
--    if ClientConfig.getInstance():getIsOtherChannel() then
--        self.m_pImageLogo:loadTexture("game/goldenflower/roomChoose/gui-image-logo-load-2.png", ccui.TextureResType.plistType)
--    end
    -----------------------------------------------
end

function GoldenFlowerLoadingLayer:initCommonLoad()
    
    -------------------------------------------------------
    --设置界面ui
    self:setLabelPercent(self.m_pLabelPercent) --百分比文字
  --  self:setLabelWord(self.m_pLabelWord)       --提示文字
    self:setBarPercent(self.m_pLoadingBar)     --进度条
    -------------------------------------------------------
    --音效/音乐/骨骼/动画/动画/碎图/大图/其他
    self:addLoadingList(GoldenFlowerRes.vecReleaseAnim,  self.TYPE_EFFECT)
    -------------------------------------------------------
end

return GoldenFlowerLoadingLayer
--endregion
