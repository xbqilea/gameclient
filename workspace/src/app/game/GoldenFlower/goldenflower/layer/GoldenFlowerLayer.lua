--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

--local GoldenFlowerSceneProxy = ProxyMgr:retrieveProxy(ProxyMgr:getProxyRegistry().GoldenFlowerScene_Proxy)

local TimerEngine = import("..manager.TimerEngine")
local ClockMgr =  import("..manager.ClockMgr")
import("..manager.GameLogic")
local ZhaJinHuaDataMgr = import("..manager.ZhaJinHuaDataMgr")
import("..scene.GoldenFlowerSceneEvent")
--local CMsgGoldenFlower = require("game.goldenflower.proxy.CMsgGoldenFlower")
--local ChatLayer = require("common.layer.ChatLayerNew")
--local GameListConfig    = require("common.config.GameList")    --等级场配置
--local RollMsg              = require("common.layer.RollMsg")

local GoldenFlowerScene_Events = import("..scene.GoldenFlowerSceneEvent")
local GoldenFlowerRes = import("..scene.GoldenFlowerRes")
local Effect = require("src.app.newHall.manager.Effect")
--cc.exports.LoadGoldenFlowerUI = function( name, ... )
--    return require("game.goldenflower.layer."..name):create(...)
--end

local CardSprite            = import(".CardSprite")
local GameSettled           = import(".GameSettled")
local CommonPlayerLayer     = import(".CommonPlayerLayer")
local CardLayer             = import(".CardLayer")
local RuleLayer             = import(".RuleLayer")
local SettingLayer          = import(".SettingLayer")
local TipLayer              = import(".TipLayer")

local scheduler = require("framework.scheduler")
local HNLayer= require("src.app.newHall.HNLayer") 
local GoldenFlowerLayer = class("GoldenFlowerLayer", function()
    return HNLayer.new()
end)

----------------------------------
local GF_MAX_CARD_COUNT         = 59
local MAX_CHAIR                 = 100	
local IDI_START_GAME            = 200				    --开始定时器
local IDI_USER_ADD_SCORE        = 201					--加注定时器
local IDI_USER_COMPARE_CARD     = 202				    --选比牌用户定时器
local IDI_USER_SHOW_CARD        = 203
local IDI_DISABLE               = 204					--过滤定时器
local TIME_USER_COMPARE_CARD    = 30					--比牌定时器
local TIME_START_GAME           = 30					--开始定时器
local TIME_USER_ADD_SCORE       = 30                    --加注定时器	
----------------------------------

GoldenFlowerLayer.instance_ = nil
function GoldenFlowerLayer:getInstance()
    return GoldenFlowerLayer.instance_
end

function GoldenFlowerLayer.create()
    if GoldenFlowerLayer.instance_ ~= nil then
        GoldenFlowerLayer.instance_ = nil
    end
    GoldenFlowerLayer.instance_ = GoldenFlowerLayer.new():init()
    return GoldenFlowerLayer.instance_
end
function GoldenFlowerLayer:ctor()
    self:enableNodeEvents()
    ZhaJinHuaDataMgr:getInstance()
    ClockMgr.getInstance():clear()
    self.bit = 0
    self.isZD = 0
    self.isCloseSelf = 1 
    self.mBGameStart = false
    self.m_compareEffect = nil
    self.settledLayer = nil
    self.m_bCompareCard = false
    self.isTouchCheckBox = false
    self.showExit = false
    self.m_wWaitUserChoice = G_CONSTANTS.INVALID_CHAIR
    self.m_CardReachCount = 0
    self.playersNode = {}
    self.m_Button_callzh ={}
    self.m_Text_callzh = {}
    self.m_PlayerNode = {}
    self.m_uiJettons = {}

    self.m_wCompareChairID = { G_CONSTANTS.INVALID_CHAIR, G_CONSTANTS.INVALID_CHAIR}
    self.m_SendingCard = false
    self.m_wLoserUser = G_CONSTANTS.INVALID_CHAIR

    CardSprite.setAutoScaleSize(1)
    CardSprite.setCardArrayWithRow("game/goldenflower/zjh/CARD1.png",CardSprite.g_cbCardData, GF_MAX_CARD_COUNT, 5, 13)
    for i = 1,  GF_MAX_CARD_COUNT do
        self:addChild(CardSprite.g_pCardArray[i]) 
        CardSprite.g_pCardArray[i]:setVisible(false)
    end

    for i = 1, GF_GAME_PLAYER do 
        if i <= 2 then
            self.m_PlayerNode[i] = CommonPlayerLayer.create("game/goldenflower/player_ui.json")
        elseif i == 3 then
            self.m_PlayerNode[i] = CommonPlayerLayer.create("game/goldenflower/player_ui_me.json")          
        else
            self.m_PlayerNode[i] = CommonPlayerLayer.create("game/goldenflower/player_ui_0.json")
        end
        --self:addChild(self.m_PlayerNode[i])
        self.m_PlayerNode[i]:retain()
        self.m_PlayerNode[i]:SetOwner(self)
    end
    self.bomb = nil
    self.particleBomb = nil
    self.m_bIsOperate = false
    --选择比牌
    self.m_bIsCompSelect = false
    --当前剩余操作时间
    self.m_nCurTime = 0
    --客户端动画先行弃牌
    self.m_bClientAbandon = false
    
    self.m_pactionEffect = nil

    self.m_bShowCard = false
    --0818
end

function GoldenFlowerLayer:init()
    self.m_pathUI = ccs.GUIReader:getInstance():widgetFromJsonFile("game/goldenflower/main_ui.json")
    
    if not self.m_pathUI then
        return false
    end
    self:addChild(self.m_pathUI)

    self.node_player = ccui.Helper:seekWidgetByName(self.m_pathUI, "node_player")

    --self.m_pathUI:setPosition(cc.p(0, (display.height-750)/2))                        --适配注意
    self.m_Panel_Root = ccui.Helper:seekWidgetByName(self.m_pathUI, "Panel_root")
    self.Panel_287 = ccui.Helper:seekWidgetByName(self.m_pathUI,"Panel_287")

    self.Panel_bg = self.Panel_287:getChildByName("Image_1")
--    if ClientConfig.getInstance():getIsOtherChannel() then
--        self.Panel_bg:loadTexture("game/goldenflower/newpic/bg2.jpg", ccui.TextureResType.localType)
--    end

    self.m_player_node_2 = ccui.Helper:seekWidgetByName(self.m_pathUI, "player_node_2")
    self.m_Panel_pai = ccui.Helper:seekWidgetByName(self.m_pathUI, "Panel_pai")
    self.m_Panel_pai:setLocalZOrder(20)
    self.m_Panel_show_pai = ccui.Helper:seekWidgetByName(self.m_pathUI, "Panel_pai_0")
    self.m_Panel_show_pai:setLocalZOrder(20)

    local m_ImageBg = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_1")
    
    self.chipRect = ccui.Helper:seekWidgetByName(self.m_pathUI, "chipPos")

    self.m_pLabel_tax = ccui.Helper:seekWidgetByName(self.m_pathUI, "Label_Tax_value")
    self:updateCurRound(0,0)

    --弃牌
    self.m_Button_qp = ccui.Helper:seekWidgetByName(self.m_pathUI, "Button_qp")
    self.m_Button_qp:addTouchEventListener(handler(self, self.OnButton_qp))
    --比牌
    self.m_Button_bp = ccui.Helper:seekWidgetByName(self.m_pathUI, "Button_bp")    
    self.m_Button_bp:addTouchEventListener(handler(self, self.OnButton_bp))
    --看牌
    self.m_Button_kp = ccui.Helper:seekWidgetByName(self.m_pathUI, "Button_kp")
    self.m_Button_kp:addTouchEventListener(handler(self, self.OnButton_kp))
    --跟注
    self.m_Button_gz = ccui.Helper:seekWidgetByName(self.m_pathUI, "Button_gz")
    self.m_Button_gz:addTouchEventListener(handler(self,self.OnButton_gz))
    --加注
    self.m_Button_jz = ccui.Helper:seekWidgetByName(self.m_pathUI, "Button_jz")
    self.m_Button_jz:addTouchEventListener(handler(self, self.OnButton_jz))   
    self.m_img_gz = ccui.Helper:seekWidgetByName(self.m_Button_gz, "Image_gz")
    self.m_img_xz = ccui.Helper:seekWidgetByName(self.m_Button_gz,"Image_xz")
    --跟到底
    self.imgeA = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_a")
    self.imgeL = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_l")
    self.m_CheckBox_gdd = ccui.Helper:seekWidgetByName(self.m_pathUI, "CheckBox_gdd")
    self.m_CheckBox_gdd:addTouchEventListener(function(sender, touchType) 
        if ccui.TouchEventType.began == touchType then
            AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
            if self.bit == 0 then
--                print("----------self.bit == 0--")
                self.imgeA:setVisible(false)
                self.imgeL:setVisible(true)
                self.bit = 1
            else
--                print("----------self.bit == 1---")
                self.imgeA:setVisible(true)
                self.imgeL:setVisible(false)
                self.bit = 0
            end
            if self.m_pPanel_setting:isVisible() then
                self:OnButton_return()
            end
            self:OnCheckButton_gdd(sender, touchType)           
        end        
    end)
    self.m_Button_allin = ccui.Helper:seekWidgetByName(self.m_pathUI, "Button_qx")
    self.m_Button_allin:addTouchEventListener(handler(self, self.OnButton_allin))
    --亮牌
    self.m_Button_lp = ccui.Helper:seekWidgetByName(self.m_pathUI, "Button_lp")
    self.m_Button_lp:addTouchEventListener(handler(self,self.OnButton_lp))

    self:setQPEnable(false)
    self:setBPEnable(false)
    self:setKPEnable(false)
    self:setJZEnable(false)
    self:setGZEnable(false)
    self:setGDDEnable(false)
    self.m_Button_allin:setVisible(false)
    self:showShowPokerButton(false)
    
    local m_ImageFapai = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_fapai")
    
    self.m_pLabel_uplimit_bet = ccui.Helper:seekWidgetByName(self.m_pathUI, "Label_uplimit_bet")
    self.m_pLabel_unit_bet = ccui.Helper:seekWidgetByName(self.m_pathUI, "Label_unit_bet")
    self.m_pLabel_current_total_bet = ccui.Helper:seekWidgetByName(self.m_pathUI, "Label_current_total_bet")
    ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_zzbg"):setLocalZOrder(100)
    
    self.tsbgImg = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_tsbg")
    self.tsbgImg:setLocalZOrder(26)
    self.tsbgImg:setVisible(false)
    self.m_Image_notice_compare = ccui.Helper:seekWidgetByName(self.tsbgImg, "Image_compare_notice")
    self.m_Image_notice_compare:setVisible(false)
    self.m_Image_notice_compare_wait = ccui.Helper:seekWidgetByName(self.tsbgImg, "Image_wait_comp_notice")
    self.m_Image_notice_compare_wait:setVisible(false)

    
   for i = 1, GF_GAME_PLAYER do        
        local buf_name = string.format("player_node_%d", i-1)        
        local node = ccui.Helper:seekWidgetByName(self.m_pathUI, buf_name)
        self.m_PlayerNode[i]:AttachTo(node)
        self.m_PlayerNode[i].m_pathUI:setVisible(false)
        table.insert(self.playersNode, node)
    end
    self:EnableAllButton(false)
    
    self.m_pCompareBg = ccui.Helper:seekWidgetByName(self.m_pathUI, "compareBg")
    self.m_pCompareBg:setVisible(false)
    self.m_CompareBg = self.m_pCompareBg:clone()
    self:addChild(self.m_CompareBg)

    self.m_pPanelEntrust = ccui.Helper:seekWidgetByName(self.m_pathUI, "Panel_Entrust")
    self.m_pPanelEntrust:setVisible(false)
    self.m_pButtonExit = ccui.Helper:seekWidgetByName(self.m_pathUI, "Button_Exit")
    self.m_pButtonGoon = ccui.Helper:seekWidgetByName(self.m_pathUI, "Button_Goon")
    local dif = (display.width-1334) / 2
    self.m_pButtonExit:setPositionX(self.m_pButtonExit:getPositionX()+dif)
    self.m_pButtonGoon:setPositionX(self.m_pButtonGoon:getPositionX()+dif)
    self.m_pButtonExit:addTouchEventListener(handler(self, self.OnButton_Exit))
    self.m_pButtonGoon:addTouchEventListener(handler(self, self.OnButton_Goon))

    self.choumaHolder = ccui.Helper:seekWidgetByName(self.m_pathUI, "choumaHolder")
    self.choumaHolder:setLocalZOrder(22)
    self.animHodler = ccui.Helper:seekWidgetByName(self.m_pathUI, "animHodler")
    self.animHodler:setLocalZOrder(23)
    self.tipsHolder = ccui.Helper:seekWidgetByName(self.m_pathUI, "tipHolder")
    self.tipsHolder:setLocalZOrder(25)

    self.m_LayoutJz = ccui.Helper:seekWidgetByName(self.m_pathUI, "Panel_chips")
    self.m_LayoutJz:setVisible(false)
    self.m_image_chipbg = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_chipbg")
    self.x,self.y = self.m_image_chipbg:getPosition()
    for i=1, 5 do
        local buff = nil
        buff = string.format("Button_chip%d", i-1)
        self.m_Button_callzh[i] = ccui.Helper:seekWidgetByName(self.m_pathUI, buff)
        self.m_Button_callzh[i]:addTouchEventListener(function(sender, touchType)
            if ccui.TouchEventType.ended == touchType then
                self:hideCM()
                self:OnButton_jzSelect(sender)
            end
        end)
        buff = string.format("Label_chip%d", i-1)
        self.m_Text_callzh[i] = ccui.Helper:seekWidgetByName(self.m_pathUI, buff)
        self.m_Button_callzh[i]:setVisible(false)
        self.m_Text_callzh[i]:setVisible(false)
    end
      
    self.m_LayoutTouch = ccui.Helper:seekWidgetByName(self.m_pathUI, "Panel_touch")
    self.m_LayoutTouch:setEnabled(false)
    self.m_LayoutTouch:setVisible(false)
    self.m_LayoutTouch:addTouchEventListener(function(sender, touchType)
        if ccui.TouchEventType.ended == touchType then
            self:hideCM()            
            self:OnTouchLayer()
        end
    end)

    local panel_hg = ccui.Helper:seekWidgetByName(self.m_pathUI,"Panel_hg")
    local contentSize = panel_hg:getContentSize()
    Effect:getInstance():creatEffectWithDelegate2(panel_hg, "heguan_1", "animation1",true,cc.p(contentSize.width/2,contentSize.height/2))
    --即将开始游戏
    self.panel_ksyx = ccui.Helper:seekWidgetByName(self.m_pathUI,"Panel_ksyx")
    self.panel_ksyx:setVisible(false)
    
    self.m_pkyx = Effect:getInstance():creatEffectWithDelegate2(self.panel_ksyx, "dengdaikaishi_1", "animation1",true,cc.p(-20,0))
    if self.m_pkyx then
        self.m_pkyx:setAnchorPoint(cc.p(0.5,0.5))
        local size = self.m_pkyx:getContentSize()
        self.textAtlas = ccui.TextAtlas:create("9","game/goldenflower/zjh/num/num_green.png", 32, 43, '0')
        self.panel_ksyx:addChild(self.textAtlas)
        self.textAtlas:setPosition(cc.p(size.width/2,0))
    end
    
     self.m_pButton_Return = ccui.Helper:seekWidgetByName(self.m_pathUI, "Button_return")
    self.m_pButton_Return:addTouchEventListener(handler(self, self.onReturnClicked))

    self.m_pButton_pop = ccui.Helper:seekWidgetByName(self.m_pathUI, "Button_pop")
    self.m_pButton_pop:addTouchEventListener(handler(self, self.onMenuClicked))
    self.m_pButton_push = ccui.Helper:seekWidgetByName(self.m_pathUI, "Button_push")
    self.m_pButton_push:addTouchEventListener(handler(self, self.onMenuClicked))
    self.m_pButton_push:hide()

    self.m_pMenu_bg = ccui.Helper:seekWidgetByName(self.m_pathUI, "Panel_13")
    self.m_pMenu_bg:addTouchEventListener(handler(self, self.onMenuClicked))
    self.m_pMenu_bg:setVisible(false)
    self.m_pPanel_setting = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_bg")
    local shap = cc.DrawNode:create()
    local point = {cc.p(1100,410), cc.p(1410, 410), cc.p(1410, 710), cc.p(1100, 710)}
    shap:drawPolygon(point, 4, cc.c4b(255, 255, 255, 255), 2, cc.c4b(255, 255, 255, 255))
    self.m_pClippingMenu = cc.ClippingNode:create(shap)
    self.Panel_287:addChild(self.m_pClippingMenu,250)
    self.m_pClippingMenu:setPosition(cc.p(0, 0))
    self.m_pPanel_setting:removeFromParent()
    self.m_pClippingMenu:addChild(self.m_pPanel_setting)
    self.m_pPanel_setting:setVisible(false)
    self.m_pPanel_setting:setPosition(1100+self.m_pPanel_setting:getContentSize().width/2,750)

    self.m_pButton_help = ccui.Helper:seekWidgetByName(self.m_pPanel_setting, "btn_rule")
    self.m_pButton_help:addTouchEventListener(handler(self, self.OnButton_help))

    self.m_pButton_setMusic = ccui.Helper:seekWidgetByName(self.m_pPanel_setting, "btn_music")
    self.m_pButton_setMusic:addTouchEventListener(handler(self, self.onMusicClicked))
--    if not AudioManager.getInstance():getMusicOn() then
--        self.m_pButton_setMusic:loadTextureNormal("game/goldenflower/newpic/btnmusicun.png", ccui.TextureResType.localType)
--    end
    self.m_pButton_setSound = ccui.Helper:seekWidgetByName(self.m_pPanel_setting, "btn_sound")
    self.m_pButton_setSound:addTouchEventListener(handler(self, self.onSoundClicked))
--    if not AudioManager.getInstance():getSoundOn() then
--        self.m_pButton_setSound:loadTextureNormal("game/goldenflower/newpic/btnyingxiaoun.png", ccui.TextureResType.localType)
--    end

    self.pPanel_setClose = ccui.Helper:seekWidgetByName(self.m_pathUI, "Panel_setClose")
     self.pPanel_setClose:addTouchEventListener(function(sender, touchType)
        if ccui.TouchEventType.ended == touchType then
            if self.m_pPanel_setting:isVisible() then
                self:OnButton_return()
            end
        end
    end)

    
    self:tryFixIphoneX()

    return self
end

function GoldenFlowerLayer:initEvent()
  
      
       
       addMsgCallBack(self,GoldenFlowerScene_Events.MSG_USER_ENTER,handler(self,self.OnEventUserEnter))
       addMsgCallBack(self,GoldenFlowerScene_Events.MSG_FISG_DEL_PLAYER, handler(self,self.OnEventUserLeave))
       addMsgCallBack(self,GoldenFlowerScene_Events.MSG_ZHAJINHUA_INIT,handler(self,self.initTableInfo))
       addMsgCallBack(self,GoldenFlowerScene_Events.MSG_ZHAJINHUA_START,handler(self,self.startGame))
       addMsgCallBack(self,GoldenFlowerScene_Events.MSG_ZHAJINHUA_JETTON,handler(self,self.onMsgUserJetton))
       addMsgCallBack(self,GoldenFlowerScene_Events.MSG_ZHAJINHUA_LOOK,handler(self,self.onMsgUserLook))
       addMsgCallBack(self,GoldenFlowerScene_Events.MSG_ZHAJINHUA_COMPARE,handler(self,self.onMsgUserCompare))
       addMsgCallBack(self,GoldenFlowerScene_Events.MSG_ZHAJINHUA_OPEN,handler(self,self.onMsgUserOpen))
       addMsgCallBack(self,GoldenFlowerScene_Events.MSG_ZHAJINHUA_ABANDON,handler(self,self.onMsgUserAbandon))
       addMsgCallBack(self,GoldenFlowerScene_Events.MSG_ZHAJINHUA_GAMEEND,handler(self,self.onMsgGameEnd))
    
     
end

function GoldenFlowerLayer:cleanEvent() --清理事件监听
    removeMsgCallBack(self,GoldenFlowerScene_Events.MSG_USER_ENTER)
    removeMsgCallBack(self,GoldenFlowerScene_Events.MSG_FISG_DEL_PLAYER)
    removeMsgCallBack(self,GoldenFlowerScene_Events.MSG_ZHAJINHUA_INIT)
    removeMsgCallBack(self,GoldenFlowerScene_Events.MSG_ZHAJINHUA_START)
    removeMsgCallBack(self,GoldenFlowerScene_Events.MSG_ZHAJINHUA_JETTON)
    removeMsgCallBack(self,GoldenFlowerScene_Events.MSG_ZHAJINHUA_LOOK)
    removeMsgCallBack(self,GoldenFlowerScene_Events.MSG_ZHAJINHUA_COMPARE)
    removeMsgCallBack(self,GoldenFlowerScene_Events.MSG_ZHAJINHUA_OPEN)
    removeMsgCallBack(self,GoldenFlowerScene_Events.MSG_ZHAJINHUA_ABANDON)
    removeMsgCallBack(self,GoldenFlowerScene_Events.MSG_ZHAJINHUA_GAMEEND)
end

function GoldenFlowerLayer:hideCM()
    for i=1, 5 do
        self.m_Button_callzh[i]:stopAllActions()
        self.m_Button_callzh[i]:setVisible(false)
        self.m_Text_callzh[i]:setVisible(false)
    end
    
end

function GoldenFlowerLayer:SetUserTableScore(wChairID, lTableScore, lCurrentScore)
--    print("--SetUserTableScore-121212-")
    local buf = LuaUtils.getFormatGoldAndNumber(lTableScore)
    self:GetPlayerUI(wChairID).m_AtlasLabel_bet:setString(buf)
    self:GetPlayerUI(wChairID):SetScore(lCurrentScore)
end

function GoldenFlowerLayer:SetGameEndInfo(wWinner)
    AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/jetton-showhand.mp3")
    local len = table.nums(self.m_uiJettons)
    for key, var in pairs(self.m_uiJettons) do
        local pf = self:GetPlayerUI(wWinner)

        local sz = pf.m_Image_Select:getContentSize()
        local mypos = cc.p(15, 20)
        mypos = pf.m_Image_Select:convertToWorldSpace(mypos)
        mypos = self.choumaHolder:convertToNodeSpace(mypos)
        if var then 
            local function func(node,value)
                node:removeFromParent()
                if value.key == len then
                    self.m_uiJettons = {}
                end
            end
            local callback = cc.CallFunc:create(func,{key = key})
            local seq = cc.Sequence:create(cc.MoveTo:create(1, mypos), callback)
            var:runAction(seq)
        end
    end
    
    self.animHodler:removeAllChildren()
    for i = 1, GF_GAME_PLAYER do
        self:GetPlayerUI(i-1).panel_qx:setVisible(false)    
    end    
end

function GoldenFlowerLayer:OnJettonMoveEnd(f)
    f:removeFromParent()
end

function GoldenFlowerLayer:SetWaitUserChoice(wChoice)
    self.m_wWaitUserChoice = wChoice
    self.tsbgImg:setVisible(false)
    self.m_Image_notice_compare_wait:setVisible(false)
    self.m_Image_notice_compare:setVisible(false)
     
    
    self.tsbgImg:setVisible(wChoice ~= G_CONSTANTS.INVALID_CHAIR)
    if wChoice == ZhaJinHuaDataMgr:getInstance():getMyChairId() then
        self.m_Image_notice_compare:setVisible(wChoice ~= G_CONSTANTS.INVALID_CHAIR)
    else
        self.m_Image_notice_compare_wait:setVisible(wChoice == G_CONSTANTS.INVALID_CHAIR)
    end
end

function GoldenFlowerLayer:SetBankerUser(wBankerUse)
    for i = 1, GF_GAME_PLAYER do
        if wBankerUse == (i-1) then
            self:GetPlayerUI(wBankerUse).m_Image_Banker:setVisible(true)
        else
            local obj = self:GetPlayerUI(i-1)
            obj.m_Image_Banker:setVisible(false)
        end
    end
end

function GoldenFlowerLayer:SetScoreInfo(lMaxCellScore, lCellScore)
    --单注最多为底注的10倍
    if lCellScore >= ZhaJinHuaDataMgr:getInstance().m_lCellScore*10 then
        lCellScore = ZhaJinHuaDataMgr:getInstance().m_lCellScore*10
    end
    local buf = LuaUtils.getFormatGoldAndNumber(lMaxCellScore)
    self.m_pLabel_uplimit_bet:setString(buf)
    buf = LuaUtils.getFormatGoldAndNumber(lCellScore)
    self.m_pLabel_unit_bet:setString(buf)
end

function GoldenFlowerLayer:SetCompareCardLabel(bCompareCard, bCompareUser)    
    self.m_bCompareCard = bCompareCard
    if bCompareCard and bCompareUser ~= nil then
        for i = 1, GF_GAME_PLAYER do
            self:GetPlayerUI(i-1):SetCompareCard(bCompareUser[i])
        end  
    end
    if not bCompareUser then
        for i = 1, GF_GAME_PLAYER do
            self:GetPlayerUI(i-1):SetCompareCard(false)
        end
    end
end

function GoldenFlowerLayer:StopUpdataScore(bStop)

end

function GoldenFlowerLayer:spriteCard_pos_less_second(m1, m2)
    return m1:getPositionX() < m2:getPositionX()
end

function GoldenFlowerLayer:animationEvent(armature, movementType, movementID)

end

function GoldenFlowerLayer:PerformCompareCard(wCompareUser, wLoserUser)
    --yung
    self.m_CompareBg:setVisible(true)
    
    --牌的基础数据
    local iChairID1 = wCompareUser[1]
    local iChairID2 = wCompareUser[2]
    
    self.m_wCompareChairID[1] = iChairID1
    self.m_wCompareChairID[2] = iChairID2
    self.m_wLoserUser = wLoserUser
    
    local enemyCards ={}
    local myCards = {}
    for i = 1,GF_CARD_COUNT do
        --local xxx1 = self:GetPlayerUI(iChairID1).m_CardControl.m_Card[i]
        local xxx1 = self:GetPlayerUI(iChairID1).m_Card[i]
        if (xxx1 and xxx1:getNumberOfRunningActions() == 0) then
            xxx1:setLocalZOrder(xxx1:getLocalZOrder() + 100)
            enemyCards[i] = xxx1
        end
        local xxx2 = self:GetPlayerUI(iChairID2).m_Card[i]
        if (xxx2 and xxx2:getNumberOfRunningActions() == 0) then
            xxx2:setLocalZOrder(xxx2:getLocalZOrder() + 100)
            myCards[i] = xxx2
        end
    end
    local len = table.nums(enemyCards)
    local lenT = table.nums(myCards)
    if len == 0 or lenT == 0 then
        self:hideComPareCardLayer()
        for i = 1, GF_CARD_COUNT do  
            local card1 = enemyCards[i]
            local card2 = myCards[i]
            if card1 ~= nil then
                self:BPCallBack(card1, wLoserUser == iChairID1,wLoserUser)
            end
            if card2 ~= nil then
                self:BPCallBack(card2, wLoserUser == iChairID2,wLoserUser)
            end
        end
        print("--------有个玩家滚蛋了-----------")
        return
    end

    table.sort(enemyCards, handler(self, self.spriteCard_pos_less_second))
    table.sort(myCards, handler(self, self.spriteCard_pos_less_second))

    local m_compareEffect = Effect:getInstance():creatEffectWithDelegate2(self.m_CompareBg, "pk_bipai", "Animation1",true,cc.p(1624/2,self.m_CompareBg:getContentSize().height/2))
    m_compareEffect:setName("compareEffect")
    local func = function (armature,movementType,movementID)
            if movementType == ccs.MovementEventType.complete or movementType == ccs.MovementEventType.loopComplete then
                if armature then 
                    armature:removeFromParent()
                end
            end
        end
    m_compareEffect:getAnimation():setMovementEventCallFunc(func)
    local kernel0 = ZhaJinHuaDataMgr:getInstance():getUserByChairId(self:GetPlayerUI(wCompareUser[1]).m_ChairID)
    local kernel1 = ZhaJinHuaDataMgr:getInstance():getUserByChairId(self:GetPlayerUI(wCompareUser[2]).m_ChairID)
    local nFaceID = kernel0.m_faceId
    if nFaceID < 0 then
        nFaceID = 0
    end
    
    local nFaceID1 = kernel1.m_faceId
    if nFaceID1 < 0 then
        nFaceID1 = 0 
    end
    local strHeadIcon =ToolKit:getHead(nFaceID)
    local strHeadIcon1 =ToolKit:getHead(nFaceID1)
--    local strHeadIcon = string.format("hall/image/file/gui-icon-head-%02d.png", nFaceID % G_CONSTANTS.FACE_NUM + 1)
--    local strHeadIcon1 = string.format("hall/image/file/gui-icon-head-%02d.png", nFaceID1 % G_CONSTANTS.FACE_NUM + 1)
    
    --初始名字和头像
    local buff = nil
    
    local label0 = ccui.Text:create(LuaUtils.getDisplayNickName(kernel0.m_nickname, 8, true) , "Arial", 18)
    label0:setTextColor(cc.c3b(255,255,255))
    
    local label1 = ccui.Text:create(LuaUtils.getDisplayNickName(kernel1.m_nickname, 8, true) , "Arial", 18)
    label1:setTextColor(cc.c3b(255,255,255))
    
    local namebg0 = ccui.ImageView:create("game/goldenflower/zjh/bipai_name.png", ccui.TextureResType.localType)
    local namebg1 = ccui.ImageView:create("game/goldenflower/zjh/bipai_name.png", ccui.TextureResType.localType)

    local head0 = ccui.ImageView:create(strHeadIcon, 1)
    local head1 = ccui.ImageView:create(strHeadIcon1, 1)

    local scaleHead = 110/head0:getContentSize().width
    
    head0:setScale(110/head0:getContentSize().width)
    head1:setScale(110/head1:getContentSize().width)

    self.m_CompareBg:addChild(head0,100)
    self.m_CompareBg:addChild(head1,100)

    head0:addChild(namebg0,100)
    head1:addChild(namebg1,100)
    namebg0:setPosition(cc.p(head0:getContentSize().width/2, -20))
    namebg1:setPosition(cc.p(head1:getContentSize().width/2, -20))
    
    namebg0:addChild(label0)
    namebg1:addChild(label1)
    label0:setPosition(cc.p(namebg0:getContentSize().width/2, namebg0:getContentSize().height/2))
    label1:setPosition(cc.p(namebg1:getContentSize().width/2, namebg0:getContentSize().height/2))

    --坐标和动作列表
    head0:setAnchorPoint(cc.p(0.5,0.5))
    head1:setAnchorPoint(cc.p(0.5,0.5))
    
    local headPy = self.m_CompareBg:getContentSize().height/2+ head1:getContentSize().height/2 -35
    
    head0:setPosition(cc.p(-230, headPy))
    head1:setPosition(cc.p(self.m_CompareBg:getContentSize().width+230, headPy))
    
    local difx = (display.width - 1334)/2
    difx = difx < 145 and 145 or difx
    --头像入场终点坐标
    local leftEnd = cc.p(452+difx,headPy + 35)
    local rightEnd = cc.p(926+difx,headPy+5)

    --头像回去的坐标
    local backLeftPos = cc.p(70, 98)
    backLeftPos = self:GetPlayerUI(wCompareUser[1]).m_Image_Select:convertToWorldSpace(backLeftPos)
    backLeftPos = self.m_CompareBg:convertToNodeSpace(backLeftPos)
    
    local backrightPos = cc.p(70, 98)    --fakePlayerPos[i]
    backrightPos = self:GetPlayerUI(wCompareUser[2]).m_Image_Select:convertToWorldSpace(backrightPos)
    backrightPos = self.m_CompareBg:convertToNodeSpace(backrightPos)
    
    --头像入场动作
    local moveleft = cc.MoveTo:create(0.3, leftEnd)
    local moveRight = cc.MoveTo:create(0.3, rightEnd)
    
    --比牌缩放
    local flag = (wCompareUser[1] == wLoserUser)
    local function func()
        if flag then
            head0:setColor(cc.c3b(150,150,150))
        else
            head1:setColor(cc.c3b(150,150,150))
        end
        self:onBombFinished(0)
    end
    
    local scaleSmall =  cc.Spawn:create(cc.ScaleTo:create(0.5,scaleHead-0.03),cc.CallFunc:create(func))
    local scaleBig = cc.Spawn:create(cc.ScaleTo:create(0.5,scaleHead+0.03))
    
    local function funcT()
        namebg0:setVisible(false)
    end

    local function funcTh()
        namebg1:setVisible(false)
    end
    
    --比牌结束后返回 并隐藏名字
    local moveBack_left = cc.Spawn:create(cc.MoveTo:create(0.5,backLeftPos), cc.CallFunc:create(funcT))
    local moveBack_right = cc.Spawn:create(cc.MoveTo:create(0.5,backrightPos), cc.CallFunc:create(funcTh))
    
    --最终回调  比牌结束 和 比牌 后 牌的状态改变    
    local function funcF(args)
        print("============funcF=============")
        for i = 1, GF_CARD_COUNT do  
            local card1 = enemyCards[i]
            local card2 = myCards[i]
            printf("i = %d  wLoserUser = %d iChairID1 = %d iChairID2 = %d",i, wLoserUser, iChairID1, iChairID2)
            print(tostring(card1 ~= nil), tostring(card2 ~= nil))
            if card1 ~= nil then
                self:BPCallBack(card1, wLoserUser == iChairID1,wLoserUser)
            end
            if card2 ~= nil then
                self:BPCallBack(card2, wLoserUser == iChairID2,wLoserUser)
            end
        end
        --比牌第二个动画
        local chairId =  PlayerInfo:getInstance():getChairID()
        local posImg = cc.p(self:GetPlayerUI(chairId).m_WinFlag:getPosition())
        local posNode = cc.p(self:GetPlayerUI(chairId).m_AttachNode:getPosition())
        local pos = cc.pAdd(posNode, posImg)

        if chairId == iChairID1 or chairId == iChairID2 then 
            local animation1 = nil
            if chairId == wLoserUser then
                animation1 = Effect:getInstance():creatEffectWithDelegate2(self.animHodler, "jiesuan_jiemian", "Animation2",true,pos) -- obj
            else 
                animation1 = Effect:getInstance():creatEffectWithDelegate2(self.animHodler, "jiesuan_jiemian", "Animation1",true,pos) -- obj cc.p(0,0)
            end
            local func = function (armature,movementType,movementID)
                if movementType == ccs.MovementEventType.complete then
                    if armature then 
                        armature:removeFromParent()
                    end
                end
            end
            self:StopCompareCard(iChairID1, iChairID2)
            self:playCompareSound(iChairID1, iChairID2)
            animation1:setLocalZOrder(20)
            animation1:getAnimation():setMovementEventCallFunc(func)
            self:showFailTag(wLoserUser)       
        else
            self:StopCompareCard(iChairID1, iChairID2)
            self:showFailTag(wLoserUser)       
        end 
    end
    
    self.actionList_left = cc.Sequence:create(moveleft, cc.DelayTime:create(0.5),(wCompareUser[1] == wLoserUser and scaleSmall or scaleBig), cc.DelayTime:create(1.0),moveBack_left)
    self.actionList_right = cc.Sequence:create(moveRight, cc.DelayTime:create(0.5),(wCompareUser[2] == wLoserUser and scaleSmall or scaleBig), cc.DelayTime:create(1.0),moveBack_right,cc.CallFunc:create(funcF))
    
    head0:runAction(self.actionList_left)
    head1:runAction(self.actionList_right)
    
    local function funcFi()
        AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/gamesolo.mp3")
    end
    self:runAction(cc.Sequence:create(cc.DelayTime:create(0.2),cc.CallFunc:create(funcFi)))
end

function GoldenFlowerLayer:CardMoveBack(obj, i)
    if i== (GF_CARD_COUNT-1) then
        self:StopCompareCard()
    end
end

function GoldenFlowerLayer:ReachCount(obj, i)

end

function GoldenFlowerLayer:CompreAnimationEnd() 
    if ZhaJinHuaDataMgr:getInstance().m_wLostUser > GF_GAME_PLAYER then
        --self.m_AnimCompNode:removeFromParent()
        return
    end
    
    --AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/g_cmp_fire.mp3")
    self.bomb = ccui.ImageView:create("game/goldenflower/zjh/zjh-fj-bpzd.png",ccui.TextureResType.localType)
    self:addChild(bomb,101)
    
    local fadeOut = cc.FadeOut:create(0.4)
    self.bomb:runAction(fadeOut)
    self.bomb:setScale(0.7)

    --输牌用户
    local wLostUser = ZhaJinHuaDataMgr:getInstance().m_wLostUser
    if wLostUser == self.m_wCompareChairID[1] then
        self.bomb:setPosition(cc.p(320,320))    --280
    else
        self.bomb:setPosition(cc.p(800,320))   --800
    end

    self:doSomethingLater(handler(self, self.onBombFinished), 1.2)
end

function GoldenFlowerLayer:onBombFinished()
   
    --if (!kernel.isEmpty()) 
        local wMeChairID = ZhaJinHuaDataMgr:getInstance():getMyChairId() --kernel.GetMeChairID()
        if wMeChairID == self.m_wCompareChairID[1] or wMeChairID == self.m_wCompareChairID[2] then
            if wMeChairID == self.m_wLoserUser then
                --AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/g_cmp_fail.mp3")
                --AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/pm_lose.mp3")
            else
               --AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/g_cmp_suc.mp3")
            end
        end 
end

function GoldenFlowerLayer:BPCallBack(sender, lost, id)
    local card = sender --dynamic_cast<SpriteCard_GF *>(sender)
    if card then
        if lost then
            --自己看牌了比牌熟了不用置灰
            if ZhaJinHuaDataMgr:getInstance():getMyChairId() == id and ZhaJinHuaDataMgr:getInstance().m_bMingZhu[id+1] then
                return
            end
            local card_bak = CardSprite.getCardWithData(CardSprite.g_cbCardBackGray)
            card:setSpriteFrame(card_bak:getSpriteFrame())        
        end
    end
end

function GoldenFlowerLayer:DispatchUserCard(wChairID, cbCardData)

end

function GoldenFlowerLayer:bFalshCard(wFalshUser)
    return true
end

function GoldenFlowerLayer:SendCard()
    return true
end


function GoldenFlowerLayer:StopFlashCard()

end

function GoldenFlowerLayer:hideComPareCardLayer()
    if self.m_CompareBg:isVisible() then
        self.m_CompareBg:setVisible(false) --yung
        self.m_CompareBg:stopAllActions()
        self.m_CompareBg:removeAllChildren()
    end
end

function GoldenFlowerLayer:StopCompareCard(iChairID1, iChairID2)
    self.m_CompareBg:setVisible(false) --yung
    self.m_CompareBg:stopAllActions()
    self.m_CompareBg:removeAllChildren()
    
    for i = 1, GF_CARD_COUNT do
        local xxx1 = self:GetPlayerUI(self.m_wCompareChairID[1]).m_Card[i]
        if xxx1 then
            xxx1:setLocalZOrder(xxx1:getLocalZOrder() - 100)
        end
        local xxx2 = self:GetPlayerUI(self.m_wCompareChairID[2]).m_Card[i]
        if xxx2 then
            xxx2:setLocalZOrder(xxx2:getLocalZOrder() - 100)
        end
    end
    
    

    if ZhaJinHuaDataMgr:getInstance().m_wLostUser > GF_GAME_PLAYER then
        return
    end
    --输牌用户
    local wLostUser = ZhaJinHuaDataMgr:getInstance().m_wLostUser
--    print("wLostUser " .. tostring(wLostUser))

    self:gray(wLostUser,false)

    local wMeChairID = ZhaJinHuaDataMgr:getInstance():getMyChairId()  --kernel.GetMeChairID()

    --设置扑克
    if ZhaJinHuaDataMgr:getInstance().m_cbHandCardData[wMeChairID] ~= 0 then
        self:GetPlayerUI(wMeChairID):SetCardData(ZhaJinHuaDataMgr:getInstance().m_cbHandCardData[wMeChairID+1], GF_CARD_COUNT,wMeChairID)
    end

    --玩家人数
    local bCount = 0
    for i = 1, GF_GAME_PLAYER do
        if ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i] then
            bCount = bCount + 1
        end
    end
    
    --判断结束
    if bCount > 1 then
        --控件信息
        if (kernel.cbUserStatus ~= G_CONSTANTS.US_LOOKON) and  (wMeChairID == ZhaJinHuaDataMgr:getInstance().m_wCurrentUser) then
            self:UpdataControl()
        end
        --设置时间        
        self:SetGameClock(ZhaJinHuaDataMgr:getInstance().m_wCurrentUser, IDI_USER_ADD_SCORE, ZhaJinHuaDataMgr:getInstance().nOperatorTime)
        --self:EnableQPButton()
    else
        local i = 1
        for i = 1, GF_GAME_PLAYER do
            if ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i] then            
                break
            end
        end
        if (i-1) == wMeChairID or wMeChairID == ZhaJinHuaDataMgr:getInstance().m_wLostUser then
           g_GameController:doSendAnimationEnd()
        end
    end
end

function GoldenFlowerLayer:playCompareSound(iChairID1, iChairID2)
    
    

    if ZhaJinHuaDataMgr:getInstance().m_wLostUser > GF_GAME_PLAYER then
        return
    end
    --输牌用户
    local wLostUser = ZhaJinHuaDataMgr:getInstance().m_wLostUser
    local wMeChairID = ZhaJinHuaDataMgr:getInstance():getMyChairId()

    if kernel.cbUserStatus ~= G_CONSTANTS.US_LOOKON then--and wLostUser == wMeChairID then
        if iChairID2 == wMeChairID or iChairID1 == wMeChairID then
            if wLostUser == wMeChairID then
                AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/game_lose.mp3")
            else
                AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/game_win.mp3")
            end
        end
    end
end

function GoldenFlowerLayer:RevokCompareSelect()  
    --fix 增加触摸事件
    self.m_LayoutTouch:setVisible(false)
    self.m_LayoutTouch:setEnabled(false)
    self.pPanel_setClose:setLocalZOrder(19)
    self.m_bIsCompSelect = false
  -- g_GameController:doSendRevokeCompare()
end

function GoldenFlowerLayer:StopSendCard()    

     
    --旁观界面
    local bCount = 0;
    for  i = 1, GF_GAME_PLAYER do
        if ZhaJinHuaDataMgr.getInstance().m_cbPlayStatus[i] then
            bCount = bCount + 1
        end
    end

    if ZhaJinHuaDataMgr.getInstance():getPangGuang() then

    else
        if bCount > 1 then
            self:UpdataControl()
        end
    end
    
    self:FinishDispatchCard()
end

function GoldenFlowerLayer:FinishDispatchCard()
    self.m_SendingCard = false
    self:showCheckButton(true)
    self.m_Panel_pai:setLocalZOrder(20)
    self.m_Panel_show_pai:setLocalZOrder(20)
    self:SetGameClock(ZhaJinHuaDataMgr:getInstance().m_wCurrentUser, IDI_USER_ADD_SCORE, ZhaJinHuaDataMgr:getInstance().nOperatorTime)
    if self.isCloseSelf == 1 then
       g_GameController:doSendCardAnimationEnd()
    end
    self.mBGameStart = true
end

function GoldenFlowerLayer:jz()
    self.m_Button_gz:loadTextureNormal("game/goldenflower/newpic/btngenzhu.png", ccui.TextureResType.localType)
    self.m_img_gz:setVisible(false)
    self.m_img_xz:setVisible(false)
end

function GoldenFlowerLayer:xz()
    self.m_Button_gz:loadTextureNormal("game/goldenflower/newpic/btnxiazhu.png", ccui.TextureResType.localType)
    self.m_img_gz:setVisible(false)
    self.m_img_xz:setVisible(false)
end

function GoldenFlowerLayer:RandInt(x, y)
    assert(((y >= x) and "<RandInt>") or "y is less than x")
    math.randomseed(tostring(os.time()):reverse():sub(1,7))
    local random = math.random()
    return random % (y - x + 1) + x
end

function GoldenFlowerLayer:OnButtonClosePopView(obj)
--    print("-------------GoldenFlowerLayer:OnButtonClosePopView---------------")
    --[[self.m_PopViewBg:stopAllActions()
    local pos = cc.p(self.m_PopViewBg:getPosition())
    print("m_PopViewBg end x:%f y:%f",pos.x,pos.y)
    local moveTo = cc.MoveTo:create(0.15, cc.p(143, 735))
    local function func()
        self:dismiss()
    end
    local actionCallBack = cc.CallFunc:create(func)
    local actionall = cc.Sequence:create(moveTo,actionCallBack)
    self.m_PopViewBg:runAction(actionall)--]]
end

function GoldenFlowerLayer:dismiss()
    self.m_PopZheZaoView:setVisible(false)
end

function GoldenFlowerLayer:OnButtonPopView( obj)
    print("-------------GoldenFlowerLayer:OnButtonPopView---------------")
    --[[self.m_PopZheZaoView:setVisible(true)    
    local pos = cc.p(self.m_PopViewBg:getPosition())
    print("m_PopViewBg beigin x:%f y:%f",pos.x,pos.y)
    local moveTo = cc.MoveTo:create(0.15, cc.p(143, 545))
    self.m_PopViewBg:runAction(moveTo)--]]
end
 
function GoldenFlowerLayer:OnButton_return(_event)
    if self.m_bMenuMoving then return end

    self.m_bMenuMoving = true

    if self.m_pPanel_setting:isVisible() then
        --菜单栏收起
        self.m_pMenu_bg:setVisible(false) --全屏关闭
        self.pPanel_setClose:setLocalZOrder(17)
        local call = cc.CallFunc:create(function() 
            self.m_pButton_pop:setVisible(true)
            self.m_pButton_push:setVisible(false)
        end)
        local call2 = cc.CallFunc:create(function() 
            self.m_bMenuMoving = false 
        end) 
        showMenuPush(self.m_pPanel_setting, call, call2, 1100+self.m_pPanel_setting:getContentSize().width/2, 750)
    else
        --菜单栏放下
        self.m_pMenu_bg:setVisible(true)
        self.pPanel_setClose:setLocalZOrder(19)
        self.m_pPanel_setting:setPosition(cc.p(1100+self.m_pPanel_setting:getContentSize().width/2, 750))
        local call = cc.CallFunc:create(function() 
            self.m_pButton_pop:setVisible(false)
            self.m_pButton_push:setVisible(true)
        end)
        local call2 = cc.CallFunc:create(function() 
            self.m_bMenuMoving = false 
        end) 
        showMenuPop(self.m_pPanel_setting, call, call2, 1100+self.m_pPanel_setting:getContentSize().width/2, 410+self.m_pPanel_setting:getContentSize().height/2)
    end   
end

function GoldenFlowerLayer:onMusicClicked()
    if self.m_bMenuMoving then return end

    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    if not AudioManager.getInstance():getMusicOn() then
        AudioManager.getInstance():setMusicOn(true)
        AudioManager.getInstance():playMusic("game/goldenflower/zjh/sound/bgm.mp3")
        self.m_pButton_setMusic:loadTextureNormal("game/goldenflower/newpic/btnmusic.png", ccui.TextureResType.localType)
    else
        AudioManager.getInstance():setMusicOn(false)
        AudioManager:getInstance():stopMusic()
        self.m_pButton_setMusic:loadTextureNormal("game/goldenflower/newpic/btnmusicun.png", ccui.TextureResType.localType)
    end
end

function GoldenFlowerLayer:onSoundClicked()
    if self.m_bMenuMoving then return end

    
    if not AudioManager.getInstance():getSoundOn() then
        AudioManager.getInstance():setSoundOn(true)
        AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
        self.m_pButton_setSound:loadTextureNormal("game/goldenflower/newpic/btnyingxiao.png", ccui.TextureResType.localType)
    else
        AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
        AudioManager.getInstance():setSoundOn(false)
        AudioManager:getInstance():stopAllSounds()
        self.m_pButton_setSound:loadTextureNormal("game/goldenflower/newpic/btnyingxiaoun.png", ccui.TextureResType.localType)
    end
    
end

function GoldenFlowerLayer:closeSelf()
    local action = nil
    local minEnterScore = PlayerInfo.getInstance():getMinEnterScore()
    self.isCloseSelf = 0
    self:ExitToHall()
end

function GoldenFlowerLayer:ExitToHall()
    PlayerInfo:getInstance():setIsQuickStart(false)
    PlayerInfo.getInstance():setIsGameBackToHall(true)
    --起立返回大厅
    self:callStandUp()
    PlayerInfo.getInstance():setIsGameBackToHall(true)
    PlayerInfo:getInstance():setIsGameBackToHallSuc(false)
    SLFacade:dispatchCustomEvent(Public_Events.Load_Entry)
end

function GoldenFlowerLayer:onBackToRoom(pNode)

end

function GoldenFlowerLayer:onReturnClicked(sender,eventType)
    if eventType ~= ccui.TouchEventType.ended then
        return
    end
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    -- 体验房退出提示
   self:getParent():onBackButtonClicked()
end

function GoldenFlowerLayer:OnButton_help(sender,eventType)
     if eventType ~= ccui.TouchEventType.ended then
        return
    end
    if self.m_bMenuMoving then return end

    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    local ruleLayer = RuleLayer.create()
    ruleLayer:setPositionX((display.width - 1624) / 2)
    --fix 那个比牌背景层级是201，规则要高于比牌
    self:addChild(ruleLayer,G_CONSTANTS.Z_ORDER_TOP + 102)
end

function GoldenFlowerLayer:onMenuClicked(sender,eventType)
     if eventType ~= ccui.TouchEventType.ended then
        return
    end
    self:OnButton_return()
end

function GoldenFlowerLayer:OnButton_Exit(sender,eventType)
     if eventType ~= ccui.TouchEventType.ended then
        return
    end
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    local index = PlayerInfo:getInstance():getChairID()
    self:GetPlayerUI(index).cdn:hideCountDown()
    self:ExitToHall()
end

function GoldenFlowerLayer:OnButton_Goon(sender,eventType)
     if eventType ~= ccui.TouchEventType.ended then
        return
    end
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    self.m_bIsOperate = true
    self:updateEntrustLayer(false)
end

function GoldenFlowerLayer:checkCanDoMsg()
    if (not ZhaJinHuaDataMgr:getInstance().m_BReturnMsg) then
        return false
    end
    return true
end

function GoldenFlowerLayer:OnButton_qp(sender,eventType)
    if eventType ~= ccui.TouchEventType.ended then
        return
    end
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    if not self:checkCanDoMsg() then
        return 
    end
    if self.m_pPanel_setting:isVisible() then
        self:OnButton_return()
    end
    self:OnTouchLayer()
    self:showAllInButton(false)
    self:EnableAllButton(false)        --sjf 新加
    self.m_bIsOperate = true
    if ZhaJinHuaDataMgr:getInstance().m_wCurrentUser == PlayerInfo:getInstance():getChairID() then
        self:KillGameClock(IDI_USER_ADD_SCORE)
    end
   g_GameController:sendGiveUp()

    --fix 动画先行
    self:clientAbandon()
end

function GoldenFlowerLayer:OnButton_bp(sender,eventType)
     if eventType ~= ccui.TouchEventType.ended then
        return
    end
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    if self.m_pPanel_setting:isVisible() then
        self:OnButton_return()
    end
    self:OnTouchLayer()
    self:doButtonBP()
    self.m_bIsOperate = true
end

function GoldenFlowerLayer:OnButton_kp(sender,eventType)
 if eventType ~= ccui.TouchEventType.ended then
        return
    end
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
--    if self:checkCanDoMsg() == false or self:IsDispatchCard() == true then
--        return
--    end

--    self.m_bIsOperate = true
    
    
    

    local wMeChairID = ZhaJinHuaDataMgr:getInstance():getMyChairId() 
    self:setKPEnable(false)
    -- 防止没返回,当轮到自己跟注操作时,依然可以被设置可用:m_ViewLayer->m_Button_kp->setEnabled((m_bMingZhu[wMeChairID]) ? FALSE : TRUE)
    ZhaJinHuaDataMgr:getInstance().m_bMingZhu[wMeChairID+1] = true
    if self.m_pPanel_setting:isVisible() then
        self:OnButton_return()
    end
    self:OnTouchLayer()
    self:UpdataControl()
   g_GameController:sendLook()
end

--开牌
function GoldenFlowerLayer:callOpenCard()
    self:EnableAllButton(false)
    --删除时间
    self:KillGameClock(IDI_USER_ADD_SCORE)
    
  g_GameController:doSendOpenCard()
end

function GoldenFlowerLayer:changeCard(sender)
    local card = sender
    if card then
        local card_bak = CardSprite.getCardWithData(card:getCardData())
        card:setSpriteFrame(card_bak:getSpriteFrame())
    end
end

function GoldenFlowerLayer:ShowCardValue(chairID)
    local player = self:GetPlayerUI(chairID)    
    for j = 1, GF_CARD_COUNT do
        local card = player.m_Card[j]
        if card then
            local card_bak = CardSprite.getCardWithData(card:getCardData())    
            if card_bak then
                card:setSpriteFrame(card_bak:getSpriteFrame())
            end
        end
    end
end

function GoldenFlowerLayer:ShowCardValueWithOutCheckStatu(chairID)
    local player = self:GetPlayerUI(chairID)
    if player then
        for j = 1, GF_CARD_COUNT do
            local card = player.m_Card[j]
            if card then
                local card_bak = CardSprite.getCardWithData(card:getCardData())
                if card_bak then
                    card:setSpriteFrame(card_bak:getSpriteFrame())
                end
            end
        end
    end    
end

function GoldenFlowerLayer:OnButton_gz(obj)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    if not self:checkCanDoMsg() then
--        print("----OnButton_gz")
        return
    end
    if self.m_pPanel_setting:isVisible() then
        self:OnButton_return()
    end
    self:OnTouchLayer()
    self:showAllInButton(false)
    self:EnableAllButton(false)
    self:OnActionFollow(1)
    self.m_bIsOperate = true
end

function GoldenFlowerLayer:OnButton_lp(obj)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    self:OnTouchLayer()
   g_GameController:doSendShowCard()    
    --隐藏亮牌按钮
    self:showShowPokerButton(false)
end

function GoldenFlowerLayer:OnActionFollow(wTemp)
    
    

    local client = ZhaJinHuaDataMgr:getInstance()
    local lCurrentScore = client.m_lCellScore * client.m_lCurrentTimes
    local wMeChairID = ZhaJinHuaDataMgr:getInstance():getMyChairId()    --kernel.GetMeChairID()
    local currentUser = client.m_wCurrentUser
    if wMeChairID ~= currentUser then
        return
    end
   
    --删除时间
    self:KillGameClock(IDI_USER_ADD_SCORE)

    if ZhaJinHuaDataMgr:getInstance().isAllin then
        self:doFollowAllIn()
        return
    end
    
    --获取筹码
    if wTemp == 0 then
        lCurrentScore = client.m_lCellScore * (client.m_lCurrentTimes + 1)
    else
        lCurrentScore = client.m_lCellScore * client.m_lCurrentTimes
    end
    --明注加倍
    if client.m_bMingZhu[wMeChairID+1] then
        lCurrentScore = lCurrentScore * 2
    end

    local wb = WWBuffer:create()
    wb:writeLongLong(lCurrentScore)
    wb:writeUShort(0)    
    wb:writeUShort(G_CONSTANTS.INVALID_CHAIR)
    wb:writeBoolean(false)
    
   g_GameController:doSendMsg(G_C_CMD.GF_SUB_C_ADD_SCORE, wb)

    --fix 下注/跟注 动画先行
    --TODO
end

function GoldenFlowerLayer:FollowBet()
    self:UpdataControl()
    if self.m_Button_gz:isEnabled() == true then
        local function func()
            self:OnActionFollow_gz()
        end
        local callback = cc.CallFunc:create(func)
        local seq = cc.Sequence:create(cc.DelayTime:create(1), callback)
        self:runAction(seq)
        self:showAllInButton(false)
        self:EnableAllButton(false)
    else

    end
end

function GoldenFlowerLayer:OnButton_jz(obj)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")  
    if self.m_LayoutJz:isVisible() == false then
        if not self:checkCanDoMsg() then
            return
        end
        self.pPanel_setClose:setLocalZOrder(17)
        if self.m_pPanel_setting:isVisible() then
            self:OnButton_return()
        end
        self.m_bIsOperate = true
        self:hideCM()
        self:showJzLayer(true)
    end
end

function GoldenFlowerLayer:OnCheckButton_gdd(obj, t)
    self.m_bIsOperate = true
    if self.m_LayoutJz:isVisible() == true then
        self:OnTouchLayer()
        self.imgeA:setVisible(true)
        self.imgeL:setVisible(false)
        self.bit = 0
        return
    end
    if self.bit == 1 then
        if not self:checkCanDoMsg() then     
            return
        end
        self:FollowBet()
    end
end

function GoldenFlowerLayer:OnButton_jzSelect(obj)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    local button = obj    
    for i = 1, 5 do
        if button == self.m_Button_callzh[i] then            
            local lCurrentScore = ZhaJinHuaDataMgr:getInstance().m_lCellScore *2*i
            if ZhaJinHuaDataMgr:getInstance().m_bMingZhu[PlayerInfo:getInstance():getChairID() + 1] then
                lCurrentScore = lCurrentScore * 2
            end
            
            
            
            local lastMoney = kernel.lScore - ZhaJinHuaDataMgr:getInstance().m_lTableScore[PlayerInfo:getInstance():getChairID()+1]
            if lastMoney >= lCurrentScore then
                self:showJzLayer(false)
                self:EnableAllButton(false)  
                self:showAllInButton(false)       
                --删除时间
                self:KillGameClock(IDI_USER_ADD_SCORE)
                --发送消息
                local wb = WWBuffer:create()
                wb:writeLongLong(lCurrentScore)
                wb:writeUShort(0)                
                wb:writeUShort(G_CONSTANTS.INVALID_CHAIR)
                wb:writeBoolean(false)
    
               g_GameController:doSendMsg(G_C_CMD.GF_SUB_C_ADD_SCORE, wb)
            else
                FloatMessage.getInstance():pushMessage("STRING_178")
            end
        end
    end
end

function GoldenFlowerLayer:showJzLayer(flag)
    self.m_LayoutJz:setVisible(flag)
    self.m_LayoutTouch:setVisible(flag)
    self.m_LayoutTouch:setEnabled(flag)

    if flag then
        self:showCM(flag)
    end
end

function GoldenFlowerLayer:showCM(flag)
    if flag then
        local times = {2,4,6,8,10}
        for i = 1, 5 do
            local minSocre = 0 
            local lCurrentScore = 0
            local isMingPai = 0            
            minSocre = ZhaJinHuaDataMgr:getInstance().m_lCellScore * 2 * (ZhaJinHuaDataMgr:getInstance().m_lCurrentTimes/2)
            lCurrentScore = ZhaJinHuaDataMgr:getInstance().m_lCellScore *2*i
            if ZhaJinHuaDataMgr:getInstance().m_bMingZhu[PlayerInfo:getInstance():getChairID() + 1] then                
                isMingPai = 1
                minSocre = minSocre * 2
                lCurrentScore = lCurrentScore * 2
            end

            --校验数据
            if (minSocre + ZhaJinHuaDataMgr:getInstance().m_lTableScore[PlayerInfo:getInstance():getChairID() +1]) > ZhaJinHuaDataMgr:getInstance().m_lUserMaxScore then
                self.m_Button_callzh[i]:setEnabled(false)                
                self.m_Button_callzh[i]:setColor(cc.c3b(150,150,150))
                self.m_Text_callzh[i]:setColor(cc.c3b(150,150,150))
                return
            end

            local times_num = 1
            if isMingPai == 1 then
                --self.m_Text_callzh[i]:loadTexture(string.format("game/goldenflower/zjh/chip/room-%d/num-double-%d.png", ZhaJinHuaDataMgr:getInstance().m_lCellScore,times[i]*ZhaJinHuaDataMgr:getInstance().m_lCellScore),ccui.TextureResType.localType)
                times_num = 2
            -- else
            --     self.m_Text_callzh[i]:loadTexture(string.format("game/goldenflower/zjh/chip/room-%d/num-%d.png", ZhaJinHuaDataMgr:getInstance().m_lCellScore,times[i]*ZhaJinHuaDataMgr:getInstance().m_lCellScore),ccui.TextureResType.localType)
            end

            self.m_Text_callzh[i]:setString(self:dealChipNumber(times[i]*ZhaJinHuaDataMgr:getInstance().m_lCellScore*times_num))

           
            local addScoreCount = ZhaJinHuaDataMgr:getInstance().m_lCurrentScoreCount
            if lCurrentScore <= minSocre then
                self.m_Button_callzh[i]:setEnabled(false)  
                self.m_Button_callzh[i]:setColor(cc.c3b(150,150,150))
                self.m_Text_callzh[i]:setColor(cc.c3b(150,150,150))
            else
                self.m_Button_callzh[i]:setEnabled(true)      
                self.m_Button_callzh[i]:setColor(cc.c3b(255,255,255))
                self.m_Text_callzh[i]:setColor(cc.c3b(255,255,255))
            end
        end
        self.index = 1
        self:CMFZ()
    end
end

function GoldenFlowerLayer:CMFZ()
    local callback = cc.CallFunc:create(function()
        self.m_Text_callzh[self.index]:setVisible(true)
        self.index = self.index + 1
        if self.index <= 5 then
            self:CMFZ()
        end
    end)
    local callbackT = cc.CallFunc:create(function()
        self.m_Button_callzh[self.index]:setVisible(true)
    end)
    self.m_Button_callzh[self.index]:runAction(cc.Sequence:create(callbackT, callback))
end

function GoldenFlowerLayer:updateEntrustLayer(isVisible)
    self.m_pPanelEntrust:setVisible(isVisible)
    self.m_pCompareBg:setVisible(isVisible)
end

function GoldenFlowerLayer:GetPlayerUI(chairID)
    local id = self:GetPlayerControlID(chairID)
    return self.m_PlayerNode[id + 1]
end

function GoldenFlowerLayer:getPlayerNodeById(i)
    return self.m_PlayerNode[i]
end

function GoldenFlowerLayer:GetMyControlID()
    return 2
end

function GoldenFlowerLayer:GetPlayerControlID(chairID)
    local id = self:GetMyControlID()
    local mychairID = PlayerInfo:getInstance():getChairID()
    local d = mychairID - chairID
    return ((id + d + GF_GAME_PLAYER) % GF_GAME_PLAYER)
end

function GoldenFlowerLayer:GetPlayerChairID(seatID)
    local mySeatID = self:GetMyControlID()
    local mychairID = PlayerInfo:getInstance():getChairID()
    local d = mySeatID - seatID
    return ((mychairID + d + GF_GAME_PLAYER) % GF_GAME_PLAYER)
end

function GoldenFlowerLayer:setWaitPanelContent(str)

end

function GoldenFlowerLayer:sendCardStart()
    self.m_SendingCard = true      --布尔值执行 加运算符    
    self:sendCardToPlayerTable(true)
end

function GoldenFlowerLayer:continue(cardStartPos,cardSize,userCount,func,j,i,tempPos)
    if not ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i] then
        return
    end 
    --print("---i------== "..i.."-----j--== "..j)
    local xxx = self:GetPlayerUI(i-1).m_Card[j]
    xxx:setVisible(false)
    tempPos = cardStartPos
    tempPos.x = tempPos.x + self.iSendcard * cardSize.width / 3.0
    xxx:setPosition(tempPos)
    xxx:setAnchorPoint(cc.p(0.5,0.5))
    xxx:setLocalZOrder(self.iSendcard)
    self.k = self.k + 1
    if self.k == userCount then
        local action = cc.Sequence:create(cc.DelayTime:create(0.05 * self.iSendcard),
                                        cc.Show:create(),
                                        cc.DelayTime:create(1),
                                        cc.CallFunc:create(func))
        xxx:runAction(action)   
    else
        local action = cc.Sequence:create(cc.DelayTime:create(0.05 * self.iSendcard), cc.Show:create())
        xxx:runAction(action)
    end
    self.iSendcard = self.iSendcard + 1
end

function GoldenFlowerLayer:continueT(ani,cardSize,userCount,func,funcT,j,i,roatationAngle)
    if not ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i] then
        return
    end
    local fScale = 1
    local pf = self:GetPlayerUI(i-1)
    local xxx = pf.m_Card[j]
    if xxx then        
        local ctlID = self:GetPlayerControlID(i-1)
        local sz = pf.m_Image_card:getContentSize()
        local mypos = cc.p(sz.width / 2, sz.height / 2.0)        
        if ctlID == 0 then
            mypos.x = mypos.x - 15
            mypos.y = mypos.y - 108
        elseif ctlID == 4 then
            mypos.x = mypos.x + 25
            mypos.y = mypos.y - 108
        end
        mypos = pf.m_Image_card:convertToWorldSpace(mypos)
        mypos = self.Panel_287:convertToNodeSpace(mypos)
        local targetPos = mypos
        if ctlID == 2 then
            fScale = 1
        elseif ctlID == 0 or ctlID == 4 then
            fScale = 0.46
        elseif ctlID == 1 or ctlID == 3 then
            fScale = 0.6
        end

        targetPos.x = targetPos.x + (j-1) * cardSize.width * fScale / 3.0
        if j == 2 then
            print("----i---== "..i.."----targetPos.x--== "..targetPos.x.."----targetPos.y--== "..targetPos.y)
            self:GetPlayerUI(i-1).m_Card[j].tagPos.x = targetPos.x
            self:GetPlayerUI(i-1).m_Card[j].tagPos.y = targetPos.y            
        end
        if ani then
            local actMove = cc.MoveTo:create(0.1, targetPos)        
            local actRot = nil
            if ctlID < 2 then
                actRot = cc.RotateTo:create(0.1, roatationAngle)
            elseif ctlID > 2 then
                actRot = cc.RotateTo:create(0.1, -roatationAngle)
            end
            local actionSpawn = nil
            if actRot == nil then
                actionSpawn = cc.Spawn:create(actMove)
            else
                local function funcTh()
                    if ctlID == 0 or ctlID == 4 then
                        xxx:setScaleX(0.46)
                        xxx:setScaleY(0.36)
                    elseif ctlID == 1 or ctlID == 3 then
                        xxx:setScaleX(0.6)
                        xxx:setScaleY(0.48)
                    end
                    if ctlID < 2 then
                        xxx:setRotationSkewX(28)
                    elseif ctlID > 2 then  
                        xxx:setRotationSkewX(-28)
                    end
                end
                local funCall = cc.CallFunc:create(funcTh)
                actionSpawn = cc.Spawn:create(actMove,funCall)
            end
            
            self.kT = self.kT + 1
            if  self.kT == userCount then
                local callback = cc.CallFunc:create(func)
                local action = cc.Sequence:create(cc.DelayTime:create(0.15 * self.iSendcardT), actionSpawn, callback)
                xxx:runAction(action)
            else
                local callback = cc.CallFunc:create(funcT)
                local action = cc.Sequence:create(cc.DelayTime:create(0.15 * self.iSendcardT), callback, actionSpawn)
                xxx:runAction(action)
            end
        else
            xxx:setVisible(true)
            xxx:setAnchorPoint(cc.p(0.5, 0.5))
            xxx:setPosition(targetPos)
            if ctlID == 0 or ctlID == 4 then
                xxx:setScaleX(0.46)
                xxx:setScaleY(0.36)
            elseif ctlID == 1 or ctlID == 3 then
                xxx:setScaleX(0.6)
                xxx:setScaleY(0.48)
            elseif ctlID == 2 then
                --xxx:setScale(0.8)
            end
            -- if ctlID < 2 then
            --     xxx:setRotationSkewX(28)
            -- elseif ctlID > 2 then  
            --     xxx:setRotationSkewX(-28)
            -- end
        end
                
        self.iSendcardT = self.iSendcardT + 1
    end
end

function GoldenFlowerLayer:getPokerScale(obj,chairId)
    local ctlID = self:GetPlayerControlID(chairId)
    local scaleX = 1
    local scaleY = 2

    if ctlID == 0 or ctlID == 4 then
        scaleX = 0.46
        scaleY = 0.36
    elseif ctlID == 1 or ctlID == 3 then
        scaleX = 0.6
        scaleY = 0.48
    elseif ctlID == 2 then
        scaleX = 1
        scaleY = 1
    end
    if ctlID < 2 then
        obj:setRotationSkewX(28)
    elseif ctlID > 2 then  
        obj:setRotationSkewX(-28)
    end
    return scaleX, scaleY
end

function GoldenFlowerLayer:sendCardToPlayerTable(ani)   
     --[[
        0630新代码
    --]]
    self.iSendcard = 0
    local userCount = 0
    local fScaleX = 1
    local fScaleY = 1
    
    self.m_Panel_pai:setLocalZOrder(ani and 24 or 20)
    self.m_Panel_show_pai:setLocalZOrder(ani and 24 or 20)

    for j = 1, GF_CARD_COUNT do
        for i = 1, GF_GAME_PLAYER do
            if ZhaJinHuaDataMgr:getInstance().m_lTableScore[i] > 0 then
                userCount = userCount + 1
            end
        end
    end

    local bankerChairID = ZhaJinHuaDataMgr.getInstance().m_wBankerUser
    local startSeatID = (self:GetPlayerControlID(bankerChairID) + 1) % GF_GAME_PLAYER 
    
    local sendInterval = 0.1
    local k = 1
    for i = 1, GF_CARD_COUNT do
        for j = startSeatID, (startSeatID + GF_GAME_PLAYER - 1) do
            local seatID = j % GF_GAME_PLAYER
            local curChairID = self:GetPlayerChairID(seatID)
            if (ZhaJinHuaDataMgr.getInstance().m_lTableScore[curChairID + 1] ~= 0) then
                local curPoker = self:getPlayerNodeById(seatID + 1).m_Card[i]
                if curPoker then
                    if ani then
                        --起始位置（飞牌起始点）
                        local nodeStartPos = cc.p(650,520)--curPoker:getParent():convertToNodeSpace(cc.p(650,550))
                        --本来位置（飞牌结束点）
                        local defaultPos = cc.p(curPoker:getPosition())
                        curPoker:setPosition(nodeStartPos)
                        curPoker:setVisible(false)
                        curPoker:setScale(0.3)
                        local actMove = cc.EaseSineOut:create(cc.MoveTo:create(0.3, defaultPos))
                        fScaleX, fScaleY = self:getPokerScale(curPoker,curChairID)
                        local actScale = cc.ScaleTo:create(0.3, fScaleX, fScaleY)
                        local actionSpawn = cc.Spawn:create(actMove, actScale)
                        local callbackSound = cc.CallFunc:create(handler(self,self.PlaySendCardSound))
                        k = k + 1
                        if k == userCount then
                            local callback = cc.CallFunc:create(function()
--                                self.m_player_node_2:setLocalZOrder(16)
                                self:StopSendCard()
                            end)
                            local action = cc.Sequence:create(cc.DelayTime:create(sendInterval * self.iSendcard), cc.Show:create(), callbackSound, actionSpawn,cc.DelayTime:create(0.1), callback)
                            curPoker:runAction(action)
                        else
                            local action = cc.Sequence:create(cc.DelayTime:create(sendInterval * self.iSendcard), cc.Show:create(), callbackSound, actionSpawn)
                            curPoker:runAction(action)
                        end
                    else
     
                        if ZhaJinHuaDataMgr:getInstance().m_lTableScore[curChairID + 1] > 0 then
                            fScaleX, fScaleY = self:getPokerScale(curPoker,curChairID)
                            curPoker:setScaleX(fScaleX)
                            curPoker:setScaleY(fScaleY)
                            curPoker:setVisible(true)
                        end
                    end
                    self.iSendcard = self.iSendcard + 1
                end
            end
        end
    end
 end

function GoldenFlowerLayer:PlaySendCardSound()
    AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/fapai.mp3")
end

function GoldenFlowerLayer:SetAllTotalBet(v)
    local buf = LuaUtils.getFormatGoldAndNumber(v)
    self.m_pLabel_current_total_bet:setString(buf)
end

function GoldenFlowerLayer:updateUserScore(chairid)
    local kernel = ZhaJinHuaDataMgr:getInstance():getUserByChairId(chairid)
    
    local node = nil
    if kernel and CUserManager:getInstance():isInTable(kernel) then        
        local chair_id = ZhaJinHuaDataMgr:getInstance():getMyChairId()
        
        node = self:GetPlayerUI(chair_id)
        node:setNickName(kernelszNickName)
        node:setUserVip(pIClientUserItem.nVipLev)
        if node then
            node:SetScore(kernel.lScore)--lUserScore
        end
    end    
end

function GoldenFlowerLayer:ShowTimeOut(chairID, clockID, escape)   
    local buf = string.format("%d",escape)
    --yung
    self:GetPlayerUI(chairID).m_nCountDownTime = escape
    if escape <= 0 then
--        print("---ShowTimeOut-escape--== "..escape)
        self:GetPlayerUI(chairID).cdn:hideCountDown()
    end
end

function GoldenFlowerLayer:onCleanTableAndShowStart(ty)
    self:onCleanTable(0)    
    local mUserId = PlayerInfo:getInstance():getUserID()
    
    if kernel.cbUserStatus ~= G_CONSTANTS.US_LOOKON then
        if self.m_bIsOperate then
           g_GameController:doSendReady()
            self.m_bIsOperate = false
        else
            self:updateEntrustLayer(false)
            FloatMessage.getInstance():pushMessage("STRING_181")
            self:ExitToHall()
            return
        end
    end
--    print("----onCleanTableAndShowStart----")
    self:KillGameClock(IDI_USER_SHOW_CARD) 
    self:showShowPokerButton(false)
end

function GoldenFlowerLayer:down(time)
    self.textAtlas:setString(time)
end

function GoldenFlowerLayer:cleanState(chairID)
    local seatID = self:GetPlayerUI(chairID)
end

function GoldenFlowerLayer:onCleanTable(dt)
    self.panel_ksyx:setVisible(false)
    self:removeAllTag()
    self:showGameSettled(false)   
    self:EnableAllButton(false)
    self:showJzLayer(false)
    self:showAllInButton(false)
    ZhaJinHuaDataMgr:getInstance().isAllin = false
    
    self:showCheckButton(false)
    
    self.choumaHolder:removeAllChildren()
    
    local mUserId = PlayerInfo:getInstance():getUserID()
    
    
    self:KillGameClock(IDI_START_GAME)
    
    self:SetBankerUser(G_CONSTANTS.INVALID_CHAIR)

    for i = 1, GF_GAME_PLAYER do
        ZhaJinHuaDataMgr:getInstance():setKanPai(i, false)
        local obj = self:GetPlayerUI(i-1)
        obj:SetCardData(nil, 0,i-1)
        obj:SetDisplayHead(false)
        obj:SetCardColor(G_CONSTANTS.INVALID_CHAIR)
        obj:SetCompareCard(false)
        obj:setBet(0.00)
        self:GetPlayerUI(i-1).m_panle_waitXJ:setVisible(false)
        self:removeGray(i-1,true)
    end
    self:xz()
end

function GoldenFlowerLayer:gray(chairId,v)
    ShaderUtils:addShader(self:GetPlayerUI(chairId).m_Image_Banker:getVirtualRenderer():getSprite(), "gray")
    ShaderUtils:addShader(self:GetPlayerUI(chairId).m_pHead:getVirtualRenderer():getSprite(), "gray")
    ShaderUtils:addShader(self:GetPlayerUI(chairId).m_Image_21:getVirtualRenderer():getSprite(), "gray")
    ShaderUtils:addShader(self:GetPlayerUI(chairId).m_Image_32:getVirtualRenderer():getSprite(), "gray")
    ShaderUtils:addShader(self:GetPlayerUI(chairId).m_Image_guang:getVirtualRenderer():getSprite(), "gray")
    ShaderUtils:addShader(self:GetPlayerUI(chairId).m_Label_score:getVirtualRenderer(), "gray")
    ShaderUtils:addShader(self:GetPlayerUI(chairId).m_AtlasLabel_bet:getVirtualRenderer(), "gray")
end

function GoldenFlowerLayer:removeGray(chairId,v)
    ShaderUtils:removeShader(self:GetPlayerUI(chairId).m_Image_Banker)
    ShaderUtils:removeShader(self:GetPlayerUI(chairId).m_pHead)
    ShaderUtils:removeShader(self:GetPlayerUI(chairId).m_Image_21)
    ShaderUtils:removeShader(self:GetPlayerUI(chairId).m_Image_32)
    ShaderUtils:removeShader(self:GetPlayerUI(chairId).m_Image_guang)
    ShaderUtils:removeShader(self:GetPlayerUI(chairId).m_Label_score:getVirtualRenderer())
    ShaderUtils:removeShader(self:GetPlayerUI(chairId).m_AtlasLabel_bet:getVirtualRenderer())    
end

function GoldenFlowerLayer:OnAutoGameStart(dt)
    self:doSomethingLater(handler(self, self.onCleanTable), dt)
end

function GoldenFlowerLayer:SendChouMa(chairID, iCurrentTimes,  isGameStart) --dzTag)  --value,
    local screenSize = self:getContentSize()
    local oldSize = screenSize
    screenSize.width = screenSize.width * 0.4
    screenSize.height = screenSize.height * 0.2

    local cellScore = ZhaJinHuaDataMgr:getInstance().m_lCellScore
    local flyChoumaTab = {}
    local len = ZhaJinHuaDataMgr:getInstance().m_bMingZhu[chairID+1] and 2 or 1
    --创建筹码
    for i = 1, len do
        local sChouMa = ccui.ImageView:create(string.format("game/goldenflower/zjh/chip/bg-chip-%02d.png",iCurrentTimes),ccui.TextureResType.localType)
        -- local imgtxt = ccui.ImageView:create(string.format("game/goldenflower/zjh/chip/room-%d/num-%d.png",cellScore,iCurrentTimes*cellScore),ccui.TextureResType.localType)
        local imgtxt = cc.Label:createWithBMFont(GoldenFlowerRes.Chip_Font_Res[iCurrentTimes], self:dealChipNumber(iCurrentTimes*cellScore))
        sChouMa:retain()
        table.insert(self.m_uiJettons, sChouMa)
        self.choumaHolder:addChild(sChouMa)
        sChouMa:setScale(0.9)
        sChouMa:addChild(imgtxt,20)
        imgtxt:setPosition(sChouMa:getContentSize().width / 2, sChouMa:getContentSize().height / 2)
        sChouMa:setVisible(false)
        table.insert(flyChoumaTab,sChouMa)    
    end
    local ctrId = self:GetPlayerControlID(chairID)  
    local pos = cc.p(self.m_PlayerNode[ctrId+1].m_AttachNode:getPosition())
    
    if ZhaJinHuaDataMgr:getInstance().isAllin == false then  
        if not isGameStart then --游戏开局去掉手的动画
            local diffPos = cc.p(self.m_PlayerNode[ctrId+1].panel_xz:getPosition())
            pos = cc.pAdd(pos, diffPos)
            if ctrId == 0 then
                pos.x = 70--pos.x - 178 - 145
                pos.y = pos.y - 53
            elseif ctrId == 1 then
                pos.x = 70--pos.x - 178 - 100
            elseif ctrId == 3 then
                pos.x = 1265--pos.x + 178 + 100
            elseif ctrId == 4 then
                pos.x = 1265--pos.x + 178 + 145
                pos.y = pos.y - 53
            end
            local obj = nil
            local name = "anim" .. ctrId
            if ctrId == 0 then  
                obj = Effect:getInstance():creatEffectWithDelegate2(self.animHodler, "xiazhu_danshou", "Animation1",true,pos)
            elseif ctrId == 1 then
                obj = Effect:getInstance():creatEffectWithDelegate2(self.animHodler, "xiazhu_danshou", "Animation1",true,pos)
            elseif ctrId == 2 then               
                obj = Effect:getInstance():creatEffectWithDelegate2(self.animHodler, "xiazhu_qian", "animation1",true,pos)
            elseif ctrId == 3 then
                obj = Effect:getInstance():creatEffectWithDelegate2(self.animHodler, "xiazhu_danshou", "Animation2",true,pos)
            elseif ctrId == 4 then
                obj = Effect:getInstance():creatEffectWithDelegate2(self.animHodler, "xiazhu_danshou", "Animation2",true,pos)                
            end
            obj:setName(name)
            local func = function (armature,movementType,movementID)
                if movementType == ccs.MovementEventType.complete then
                    self.animHodler:removeChildByName(name)
                end
            end
            obj:getAnimation():setMovementEventCallFunc(func)
        end
        
        --播放筹码动画
        --ModifyBy: Frank
        local handDelaytTime = 0.2
        if isGameStart then handDelaytTime = 0 end
        local x,y = self.playersNode[self:GetPlayerControlID(chairID)+1]:getPosition()
        local len = #flyChoumaTab
        for i = 1, len do
            local sChouMa = flyChoumaTab[i]
            if ctrId == 2 then
                sChouMa:setPosition(cc.p(x + 70,y + 99))
            elseif ctrId == 1 then
                sChouMa:setPosition(cc.p(x+165 + 70,y + 99))
            elseif ctrId == 3 then
                sChouMa:setPosition(cc.p(x-165 + 70,y + 99))
            elseif ctrId == 4 then
                sChouMa:setPosition(cc.p(x + 70,y-86 + 99))
            elseif ctrId == 0 then
                sChouMa:setPosition(cc.p(x + 70,y-86 + 99))
            end
            local point = cc.p(0,0)
            local minX = self.chipRect:getPositionX() - self.chipRect:getContentSize().width/2
            local maxX = self.chipRect:getPositionX() + self.chipRect:getContentSize().width/2
            local minY = self.chipRect:getPositionY() - self.chipRect:getContentSize().height/2
            local maxY = self.chipRect:getPositionY() + self.chipRect:getContentSize().height/2

            local rangeX = maxX - minX
            local random = math.random(1,tonumber(rangeX))
            point.x = random + minX
        
            local rangeY = maxY - minY
            random = math.random(1,tonumber(rangeY))
            point.y = random + minY
                                
            if point.y >= (maxY - 26) then
                point.y = (maxY - 26)
            end

--            local actMove = cc.MoveTo:create(0.2, point)
            local time = 0.4
            local actionMove = cc.EaseSineOut:create(cc.MoveTo:create(time, point))
            local actionScale = cc.EaseSineOut:create(cc.ScaleTo:create(time, 0.6))
            local actMove = cc.Spawn:create(actionMove, actionScale)
            local function funcT()
                sChouMa:setVisible(true)
            end
            sChouMa:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(funcT), actMove, cc.DelayTime:create(0.1)))
        end
     
    elseif ZhaJinHuaDataMgr:getInstance().isAllin == true then
        local diffPos = cc.p(self.m_PlayerNode[ctrId+1].panel_qx:getPosition())
        pos = cc.pAdd(pos, diffPos)
        if ctrId == 0 then
            pos.x = 0--pos.x - 180 - 70
            pos.y = pos.y+25
        elseif ctrId == 1 then
            pos.x = -30--pos.x - 180 - 50
            pos.y = pos.y + 50
        elseif ctrId == 2 then
             pos.y = pos.y - 15
        elseif ctrId == 3 then
            pos.x = 1310--pos.x - 180 + 50
            pos.y = pos.y + 50
        elseif ctrId == 4 then
            pos.x = 1310--pos.x + 180 + 70
            pos.y = pos.y+25
        end
        local obj = nil
        if ctrId == 0 then
            obj = Effect:getInstance():creatEffectWithDelegate2(self.animHodler, "allin_you2", "small_zuo",true,pos)
            obj:setRotation(15)
        elseif ctrId == 1 then
            obj = Effect:getInstance():creatEffectWithDelegate2(self.animHodler, "allin_you2", "small_zuo",true,pos)
        elseif ctrId == 2 then
            obj = Effect:getInstance():creatEffectWithDelegate2(self.animHodler, "allin_qian", "animation1",true,pos)
        elseif ctrId == 3 then
            obj = Effect:getInstance():creatEffectWithDelegate2(self.animHodler, "allin_you2", "small_you",true,pos)
        elseif ctrId == 4 then
            obj = Effect:getInstance():creatEffectWithDelegate2(self.animHodler, "allin_you2", "small_you",true,pos)
            obj:setRotation(-15)
        end
        obj:setScale(1.3)
        -- local func = function (armature,movementType,movementID)
        --     if movementType == ccs.MovementEventType.complete or movementType == ccs.MovementEventType.loopComplete then
        --         if armature then 
        --             armature:removeFromParent()
        --         end
        --     end
        -- end
        -- obj:getAnimation():setMovementEventCallFunc(func)
    end  
end

function GoldenFlowerLayer:setMovementEventCallFunc(obj,parent)
    
end

function GoldenFlowerLayer:setFrameEventCallFunc(obj,parent,sChouMa,actMove)
    
end

function GoldenFlowerLayer:randomN(minVal,maxVal,chairID)
    local v1 = math.min(minVal, maxVal)
    local v2 = math.max(minVal, maxVal)
    return chairID * 15 + minVal
end

function GoldenFlowerLayer:EnableAllButton(v)
    self:setQPEnable(v)
    self:setBPEnable(v)
    self:setKPEnable(v)
    self:setJZEnable(v)
    self:setGZEnable(v)
end

function GoldenFlowerLayer:UpdataControl()
    --放弃按钮
    local mUserId = PlayerInfo:getInstance():getUserID()
    
    
    local c = ZhaJinHuaDataMgr:getInstance()
    local wChairID = ZhaJinHuaDataMgr:getInstance():getMyChairId()
    
    --fix 自己还在游戏才执行
    if (kernel.cbUserStatus  ~= G_CONSTANTS.US_LOOKON) and (wChairID == c.m_wCurrentUser) and ZhaJinHuaDataMgr:getInstance().canPlay then
        
        --看牌按钮
        local bEnable = c.m_bMingZhu[wChairID+1]  and 0 or 1
        if bEnable == 0 then
            bEnable = false
        else
            bEnable = true
        end
        if not ZhaJinHuaDataMgr:getInstance().m_bAlowLookCardFirstTurn and ZhaJinHuaDataMgr:getInstance().curRoundIndex < 2 then
            --不允许首轮看牌
            bEnable = false
        end

        self:setKPEnable(bEnable)
        
        --比牌按钮
        self:setBPEnable( ZhaJinHuaDataMgr:getInstance().curRoundIndex > 1 )

        --判断allin显示
        self:showAllIn()
        
        --跟注按钮 全下情况 跟注一直显示
        if (ZhaJinHuaDataMgr:getInstance().isAllin) then
            self:setGZEnable(false)
            self:setGDDEnable(false)
        else
            self:setGZEnable(true)
        end
        
        --明注加倍       
        local currenScore = ZhaJinHuaDataMgr:getInstance().m_lCellScore*(ZhaJinHuaDataMgr:getInstance().m_lCurrentTimes)
        local needScore = ((c.m_bMingZhu[wChairID+1] == true) and currenScore*2) or currenScore
        local myScore = self:GetPlayerUI(wChairID):GetScore()   
        if(myScore >= needScore) then
            --加注按钮
            if (ZhaJinHuaDataMgr:getInstance().isAllin or ZhaJinHuaDataMgr:getInstance().m_lCurrentTimes >= 10) then
                self:setJZEnable(false)
            else
                self:setJZEnable(true)
            end
 
            if (c.isAllin) then
                self:showCheckButton(false)
            else
                self:showCheckButton(true)
            end
        else
            self:setJZEnable(false)
            self:setGZEnable(false)
            self:showAllInButton(false)
            self:setBPEnable(true)
            self:showCheckButton(false)
        end
    elseif (kernel.cbUserStatus ~= G_CONSTANTS.US_LOOKON) and (kernel.cbUserStatus == G_CONSTANTS.US_PLAYING) and ZhaJinHuaDataMgr:getInstance().canPlay then
        --看牌按钮
        local bEnable = c.m_bMingZhu[wChairID+1]  and 0 or 1
        if bEnable == 0 then
            bEnable = false
        else
            bEnable = true
        end
        if not ZhaJinHuaDataMgr:getInstance().m_bAlowLookCardFirstTurn and ZhaJinHuaDataMgr:getInstance().curRoundIndex < 2 then
            --不允许首轮看牌
            bEnable = false
        end
        self:setKPEnable(bEnable)
    end
    self:EnableQPButton()
end

function GoldenFlowerLayer:LookCard(chairID, v,count)
    local obj = self:GetPlayerUI(chairID)
    local diffX,diffY = obj.m_Image_kanpai_action:getPosition()
    local ctrId = self:GetPlayerControlID(chairID)
    local buf_name = string.format("player_node_%d", ctrId)        
    local node = ccui.Helper:seekWidgetByName(self.m_pathUI, buf_name)
    local x,y = node:getPosition()

    if ctrId == 0 or ctrId == 4 then
        diffY = diffY - 120
    end
    local pos = cc.p(x + diffX, y + diffY)
    if v == 0 then  --看牌
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.tipsHolder, "paopaokuang_zhajinhua", "Animation5",true,pos, 10)
        self:SetLookCard(false,chairID)
        obj:playerLookCardSound()
    elseif v == 1 then  --跟注
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.tipsHolder, "paopaokuang_zhajinhua", "Animation1",true,pos, 10)        
        if not ZhaJinHuaDataMgr:getInstance().isAllin then
            obj:playerFollowSound()  
        else
            obj:playerAllInSound()
        end
    elseif v == 2 then  --加注   
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.tipsHolder, "paopaokuang_zhajinhua", "Animation4",true,pos, 10)  
        if not ZhaJinHuaDataMgr:getInstance().isAllin then     
            obj:playerAddSound()
        else
            obj:playerAllInSound()
        end
    elseif v == 3 then  --弃牌
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.tipsHolder, "paopaokuang_zhajinhua", "Animation7",true,pos, 10)
        obj:playerGiveUpSound()
        self:OnGiveUp(chairID)
    elseif v == 4 then  --下注
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.tipsHolder, "paopaokuang_zhajinhua", "Animation9", true, pos, 10)
        obj:playBetSound()
    elseif v == 5 then -- 亮牌
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.tipsHolder, "paopaokuang_zhajinhua", "Animation6", true, pos, 10)
    elseif v == 6 then -- 全下
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.tipsHolder, "paopaokuang_zhajinhua", "Animation8", true, pos, 10)
        obj:playerAllInSound()
    elseif v == 7 then -- 跟到底
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.tipsHolder, "paopaokuang_zhajinhua", "Animation3", true, pos, 10)
    elseif v == 8 then
        
    end
end


function GoldenFlowerLayer:OnGiveUp(chairID)
    if chairID == PlayerInfo:getInstance():getChairID() then
        local data = ZhaJinHuaDataMgr:getInstance().m_cbHandCardData[chairID+1]
        self:GetPlayerUI(chairID):SetCardData(data, GF_CARD_COUNT,chairID)
        
        local LeftPos = cc.p(10000, 0)
        local MyCards    --std::vector<PlayerNode_GF*> MyCards
        
        for i = 1, GF_CARD_COUNT do
            local xxx = self:GetPlayerUI(chairID).m_Card[i]
            local x, y = xxx:getPosition()
            if LeftPos.x > x then
                LeftPos = cc.p(xxx:getPosition())
            end
        end
        if ZhaJinHuaDataMgr:getInstance().m_bMingZhu[chairID+1] then
            
        else
            for i = 1, GF_CARD_COUNT do
                local card = self:GetPlayerUI(chairID).m_Card[i]
                local card_bak = CardSprite.getCardWithData(CardSprite.g_cbCardBackGray)
                card:setSpriteFrame(card_bak:getSpriteFrame())
            end
        end 
    else
        local RightPos = cc.p(0, 0)
        local vecCards = {}
        
        for j = 1, GF_CARD_COUNT do
            local xxx = self:GetPlayerUI(chairID).m_Card[j]
            if xxx and (xxx:getChairID() == chairID) then
                table.insert(vecCards, xxx)
                local x, y = xxx:getPosition()
                if RightPos.x < x then
                    RightPos = cc.p(xxx:getPosition())
                end
            end
        end
        local len = table.nums(vecCards)
        if len == 0 then
            return
        end
        table.sort(vecCards, handler(self, self.spriteCard_pos_less_second))
        local roatationAngle = -15
        
        for i = 1, len do
            local card = vecCards[i]
            local card_bak = CardSprite.getCardWithData(CardSprite.g_cbCardBackGray)
            card:setSpriteFrame(card_bak:getSpriteFrame())
            -- local actRot = cc.RotateTo:create(0.5, roatationAngle)
            -- card:runAction(actRot)
            -- roatationAngle = roatationAngle + 15
        end
    end
    self:gray(chairID,false)
    self:showGiveTag(chairID)
end

function GoldenFlowerLayer:IsDispatchCard()
    return self.m_SendingCard
end

function GoldenFlowerLayer:SetGameTax(v, chairID)

end

function GoldenFlowerLayer:OnCompareSelected(pf)
    if ZhaJinHuaDataMgr:getInstance().m_usNoCompare[pf.m_ChairID] ~= 0 and ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[pf.m_ChairID] then
        FloatMessage.getInstance():pushMessage("STRING_193")
        return false
    end
    
    --清理界面
    self:SetCompareCardLabel(false, nil)

    --清理提示标志
     self:SetWaitUserChoice(G_CONSTANTS.INVALID_CHAIR)
    
    --删除定时器
    self:KillGameClock(IDI_USER_COMPARE_CARD)
    
    local mUserId = PlayerInfo:getInstance():getUserID()
    
    local wMeChairID = ZhaJinHuaDataMgr:getInstance():getMyChairId()    --kernel.GetMeChairID()

    local lCurrentScore = (ZhaJinHuaDataMgr:getInstance().m_bMingZhu[wMeChairID+1] and (ZhaJinHuaDataMgr:getInstance().m_lCurrentTimes * ZhaJinHuaDataMgr:getInstance().m_lCellScore * 2)) or (ZhaJinHuaDataMgr:getInstance().m_lCurrentTimes * ZhaJinHuaDataMgr:getInstance().m_lCellScore)
  
--    local wb = WWBuffer:create()
--    wb:writeLongLong(lCurrentScore)
--    wb:writeUShort(1)
--    wb:writeUShort(pf.m_ChairID)
--    wb:writeBoolean(false)
--    --加注消息
--   g_GameController:doSendMsg(G_C_CMD.GF_SUB_C_ADD_SCORE, wb)

--    local wb = WWBuffer:create()
--    wb:writeUShort(pf.m_ChairID)
    --比牌消息
   g_GameController:sendCompare(G_C_CMD.GF_SUB_C_COMPARE_CARD, pf.m_ChairID)
    return true
end

function GoldenFlowerLayer:GetCompareInfo()
    return self.m_bCompareCard
end

function GoldenFlowerLayer:OnWinScoreMoveEnd(node,startPos)
    node:setVisible(false)
    node:setPosition(startPos)
    node:stopAllActions()
end

function GoldenFlowerLayer:OnGameEnd()  --CMD_S_GameEnd data
    local chairId = PlayerInfo:getInstance():getChairID()
    for i = 1,GF_GAME_PLAYER do
        local value = ZhaJinHuaDataMgr:getInstance().m_lWinScore[i]
        if value and value ~= 0 then
            
            local ctrId = self:GetPlayerControlID(i-1)
            local start = self:GetPlayerUI(chairId).m_PosWinScoreStart
            if ctrId == 0 or ctrId == 4 then
                start = cc.p(start.x,start.y-140)
                self:GetPlayerUI(i-1).panelY:setPositionY(start.y)
                self:GetPlayerUI(i-1).panelS:setPositionY(start.y)
            elseif ctrId == 1 or ctrId == 3 then
                start = cc.p(start.x,start.y-55)
                self:GetPlayerUI(i-1).panelY:setPositionY(start.y)
                self:GetPlayerUI(i-1).panelS:setPositionY(start.y)
            end

            local over = cc.p(start.x,start.y+15)       
            local move = cc.MoveTo:create(1.5, over)
            local delay = cc.DelayTime:create(2.5)
            local function func()
                if ZhaJinHuaDataMgr:getInstance().m_lWinScore[i] > 0 then
                    self:OnWinScoreMoveEnd(self:GetPlayerUI(i-1).panelY,self:GetPlayerUI(chairId).m_PosWinScoreStart)
                elseif ZhaJinHuaDataMgr:getInstance().m_lWinScore[i] < 0 then
                    self:OnWinScoreMoveEnd(self:GetPlayerUI(i-1).panelS,self:GetPlayerUI(chairId).m_PosWinScoreStart)
                end
            end
            local function funcT()
                self.choumaHolder:removeAllChildren()
                if self:GetPlayerUI(chairId).m_panle_waitXJ:isVisible() == false then
                    if ZhaJinHuaDataMgr:getInstance().m_lWinScore[chairId+1] > 0 then
                        self:GetPlayerUI(chairId).m_Label_score:runAction(cc.Sequence:create(cc.ScaleTo:create(0.3, 1.2), 
                        cc.CallFunc:create(function()
                            self:GetPlayerUI(chairId):SetScore(PlayerInfo.getInstance():getUserScore())
                        end), cc.DelayTime:create(0.2), cc.ScaleTo:create(0.3, 0.85)))
                        self:GetPlayerUI(chairId).m_Image_guang:runAction(cc.Sequence:create(cc.Show:create(),cc.DelayTime:create(1.0),cc.Hide:create()))
                    end
                end
            end

            local seq = cc.Sequence:create(cc.Sequence:create(move,cc.CallFunc:create(funcT), delay,cc.CallFunc:create(func)))
            if ZhaJinHuaDataMgr:getInstance().m_lWinScore[i] > 0 then
                self:GetPlayerUI(i-1).panelY:setVisible(true)
                self:GetPlayerUI(i-1).m_TxtWinScoreY:setString("+"..LuaUtils.getFormatGoldAndNumber(ZhaJinHuaDataMgr:getInstance().m_lWinScore[i]))
                self:GetPlayerUI(i-1).panelY:runAction(seq)
            elseif ZhaJinHuaDataMgr:getInstance().m_lWinScore[i] < 0 then
                self:GetPlayerUI(i-1).panelS:setVisible(true)
                self:GetPlayerUI(i-1).m_TxtWinScoreS:setString("-".. LuaUtils.getFormatGoldAndNumber( math.abs(ZhaJinHuaDataMgr:getInstance().m_lWinScore[i])))
                self:GetPlayerUI(i-1).panelS:runAction(seq)
            end
        end
    end

    if self:GetPlayerUI(chairId).m_panle_waitXJ:isVisible() == false then
        self.panel_ksyx:setVisible(true)
        self:down(ZhaJinHuaDataMgr:getInstance().nShowCardTime)   
        self:SetGameClock(PlayerInfo:getInstance():getChairID(), IDI_USER_SHOW_CARD, ZhaJinHuaDataMgr:getInstance().nShowCardTime)
    end   
    self:onCheckAutoGiveUp()
end

function GoldenFlowerLayer:OnBackScore(chairID, Score)
    local  sChouMa = ccui.ImageView:create(string.format("game/goldenflower/zjh/chip/bg-cmd-%02d.png",ZhaJinHuaDataMgr:getInstance().m_lCurrentTimes),ccui.TextureResType.localType)  --ccui.ImageView:create(string.format("game/goldenflower/zjh/chip/chip/zjh_chip%d.png",Score%9), ccui.TextureResType.localType)    --注意
    sChouMa:setScale(0.6)
    self.choumaHolder:addChild(sChouMa)
    local label = ccui.Text:create("", "Arial", 25)
  
    label:setString(LuaUtils.getFormatGoldAndNumber(Score))
    label:setTextColor(cc.c3b(255,255,255))    
    sChouMa:addChild(label,20)
    label:setPosition(sChouMa:getContentSize().width / 2, sChouMa:getContentSize().height / 2+5)
    label:setAnchorPoint(cc.p(0.5, 0.5))

    local startPos = cc.p(0,0)
    local minX = self.chipRect:getPositionX() - self.chipRect:getContentSize().width/2
    local maxX = self.chipRect:getPositionX() + self.chipRect:getContentSize().width/2
    local rangeX = maxX - minX
    math.randomseed(tostring(os.time()):reverse():sub(1,7))
    local random = math.random(1,tonumber(rangeX))
    startPos.x = random + minX
    
    local minY = self.chipRect:getPositionY() - self.chipRect:getContentSize().height/2
    local maxY = self.chipRect:getPositionY() + self.chipRect:getContentSize().height/2
    local rangeY = maxY - minY
    random = math.random(1,tonumber(rangeY))
    startPos.y = random + minY
    
    sChouMa:setPosition(startPos)
    local DesPos = cc.p(self.playersNode[self:GetPlayerControlID(chairID)+1]:getPosition())
    DesPos = cc.pAdd(DesPos, cc.p(70,98))
    local actMove = cc.MoveTo:create(0.5, DesPos)   
    local function func()
        sChouMa:removeFromParent()
    end
    local callback = cc.CallFunc:create(func)
    sChouMa:runAction(cc.Sequence:create(cc.DelayTime:create(3.0), actMove, callback))
end

function GoldenFlowerLayer:SetCardColor(v)
end

function GoldenFlowerLayer:SetLookCard(v,chairId)
    if chairId and chairId == ZhaJinHuaDataMgr:getInstance():getMyChairId() then return end
    if self:GetPlayerUI(chairId).m_Card[GF_CARD_COUNT] then
        local card_bak = CardSprite.getCardWithData(0x47)
        self:GetPlayerUI(chairId).m_Card[GF_CARD_COUNT]:setSpriteFrame(card_bak:getSpriteFrame())
    end
end

function GoldenFlowerLayer:SetDisplayHead(v)
end

function GoldenFlowerLayer:SetCardData(data, count, chairId)
    for i = 1, count do
        if not self:GetPlayerUI(chairId).m_Card[i] then      --if (!m_Card[i])
            local xxx = CardSprite.getCardWithBack()
            self:GetPlayerUI(chairId).m_Card[i] = xxx
        end  
        self:GetPlayerUI(chairId).m_Card[i]:setCardData(data[i])
    end
end

function GoldenFlowerLayer:SetCompareBack(b,chairId)
    for i = 1, GF_CARD_COUNT do
        if self:GetPlayerUI(chairId).m_Card[i] then
            if b then
                self:GetPlayerUI(chairId).m_Card[i]:setColor(cc.c3b(255, 0, 0))
            else
                self:GetPlayerUI(chairId).m_Card[i]:setColor(cc.c3b(255, 255, 255))
            end
        end
    end
end

function GoldenFlowerLayer:SetVisible(v,chairId)
    for i = 1, GF_CARD_COUNT do
        if self:GetPlayerUI(chairId).m_Card[i] then
            self:GetPlayerUI(chairId).m_Card[i]:setVisible(v)
        end
    end
end

function GoldenFlowerLayer:ShowClock(chairID)
    local pf = self:GetPlayerUI(chairID)
    --yung
    local nTime = pf.m_nCountDownTime
--    print("--111--ShowClock---nTime-== "..nTime)
    if pf.cdn.isShowing or (nTime <= 0) then
--        print("-222---ShowClock---nTime-== "..nTime)
        return
    end
    pf.cdn:startCountDown(chairID, nTime, nTime, cc.p(69,98), 102, 102)
end

function GoldenFlowerLayer:HideClock(chairID)
    local pf = self:GetPlayerUI(chairID)
    if (pf ~= nil) and (not pf.cdn.isShowing) then
        return  
    end
    pf.m_nCountDownTime = 0
    --yung
    pf.cdn:hideCountDown()
end

function GoldenFlowerLayer:RefreshCards()
    local cardSize = CardSprite.getCardWithBack():getContentSize()
    self.m_Panel_show_pai:hide()
    for i = 1, GF_GAME_PLAYER do
        for j = 1, GF_CARD_COUNT do

            if self:GetPlayerUI(i-1).m_Card[j] then
                self:GetPlayerUI(i-1).m_Card[j]:removeFromParent()
                self:GetPlayerUI(i-1).m_Card[j] = nil
            end

            if self:GetPlayerUI(i-1).m_ShowCard[j] then
                self:GetPlayerUI(i-1).m_ShowCard[j]:removeFromParent()
                self:GetPlayerUI(i-1).m_ShowCard[j] = nil
            end

            self:GetPlayerUI(i-1).m_Card[j] = CardSprite.getCardWithBack()
            local xxx = self:GetPlayerUI(i-1).m_Card[j]
            self.m_Panel_pai:addChild(self:GetPlayerUI(i-1).m_Card[j])
            xxx:setAnchorPoint(cc.p(0.5,0.5))
            xxx:setChairID(i-1)
            -- xxx:addShowCardButton()

            self:GetPlayerUI(i-1).m_ShowCard[j] = CardSprite.getCardWithBack()
            local new_xxx = self:GetPlayerUI(i-1).m_ShowCard[j]
            self.m_Panel_show_pai:addChild(self:GetPlayerUI(i-1).m_ShowCard[j])
            new_xxx:setAnchorPoint(cc.p(0.5,0.5))
            new_xxx:setChairID(i-1)
            new_xxx:setScale(0.4)

            local ctlID = self:GetPlayerControlID(i-1)
            local sz = self:GetPlayerUI(i-1).m_Image_card:getContentSize()
            local mypos = cc.p(sz.width / 2, sz.height / 2.0)        
            if ctlID == 0 then
                mypos.x = mypos.x - 15
                mypos.y = mypos.y - 108
            elseif ctlID == 4 then
                mypos.x = mypos.x + 25
                mypos.y = mypos.y - 108
            end
            mypos = self:GetPlayerUI(i-1).m_Image_card:convertToWorldSpace(mypos)
            mypos = self.Panel_287:convertToNodeSpace(mypos)
            local new_targetPos = cc.p(mypos.x,mypos.y)
            local targetPos = mypos
            local fScale = 0
            if ctlID == 2 then
                fScale = 1
            elseif ctlID == 0 or ctlID == 4 then
                fScale = 0.46
            elseif ctlID == 1 or ctlID == 3 then
                fScale = 0.6
            end

            local diff = math.floor(cardSize.width * fScale / 3.0)
            targetPos.x = targetPos.x + math.floor((j-1) * diff)
            xxx:setPosition(targetPos)
            xxx:setVisible(false)
            self:GetPlayerUI(i-1).m_Card[j].tagPos = targetPos

            
            new_targetPos.x = new_targetPos.x + math.floor((j-1) * 25)
            if ctlID == 0 then
                new_targetPos.y = new_targetPos.y + 70
            elseif ctlID == 1 then
                new_targetPos.y = new_targetPos.y -70
            elseif ctlID == 2 then
                new_targetPos.x = new_targetPos.x+190
            elseif ctlID == 3 then
                new_targetPos.y = new_targetPos.y -70
            elseif ctlID == 4 then
                new_targetPos.y = new_targetPos.y + 70
            end

            new_xxx:move(new_targetPos)

        end
    end

    
end    

function GoldenFlowerLayer:OnActionFollow_gz()
    self:OnActionFollow(1)
end

function GoldenFlowerLayer:OnActionFollow_jz()
    self:OnActionFollow(0)
end

function GoldenFlowerLayer:onEnter()
    self:initEvent()

    ClockMgr.getInstance().mTimerEngine:SetTimerEngineSink(self)
    ClockMgr.getInstance().mTimerEngine:StartService()
    self.m_CloseSelf = SLFacade:addCustomEventListener(GoldenFlowerScene_Events.Close_Self, handler(self,self.closeSelf))

    local useID = PlayerInfo.getInstance():getUserID()
    local userInfo = CUserManager.getInstance():getUserInfoByUserID(useID)

    if  userInfo.wTableID == G_CONSTANTS.INVALID_TABLE
    and userInfo.wChairID == G_CONSTANTS.INVALID_CHAIR
    then
       g_GameController:sendSitDown(G_CONSTANTS.INVALID_TABLE, G_CONSTANTS.INVALID_CHAIR) --发送坐下
    else
       g_GameController:sendGameOption()
    end
    --设置底分
    --325 
--    local lCellScore =  PlayerInfo:getInstance():getBaseScore()
--    self.m_pLabel_uplimit_bet:setString( LuaUtils.getFormatGoldAndNumber(lCellScore))

    if AudioManager.getInstance():getStrMusicPath() ~= "game/goldenflower/zjh/sound/bgm.mp3" then
        AudioManager.getInstance():playMusic("game/goldenflower/zjh/sound/bgm.mp3")
    end
end

function GoldenFlowerLayer:onExit()
    print("GoldenFlowerLayer:onExit")
    self:cleanEvent()

    if self.m_pBtnShowCard then
        self.m_pBtnShowCard:removeFromParent()
        self.m_pBtnShowCard = nil
    end

    --socket已经断开
    --if ZhaJinHuaDataMgr.getInstance():getGameStatus() ~= GAME_STATUS_PLAY
    --or PlayerInfo.getInstance():getUserStatus() ~= G_CONSTANTS.US_PLAYING
    --then
    --    self:callStandUp()
    --end

    for i = 1, GF_GAME_PLAYER do
        self.m_PlayerNode[i]:release()
    end
    ZhaJinHuaDataMgr:getInstance():clear()
    PlayerInfo.getInstance():setSitSuc(false)
    SLFacade:removeEventListener(self.m_CloseSelf)

    --释放单例
    GoldenFlowerLayer.instance_ = nil

    self:removeAllChildren()

    -- 释放动画
    for _, strPathName in pairs(GoldenFlowerRes.vecReleaseAnim) do
        local strJsonName = strPathName ..".ExportJson"
        ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo(strJsonName)
    end
    -- 释放整图
    for _, strPathName in pairs(GoldenFlowerRes.vecReleasePlist) do
        local strPlistName = strPathName .. ".plist"
        local strPngName = strPathName .. ".png"
        display.removeSpriteFrames(strPlistName, strPngName)
    end
    -- 释放背景图
    for _, strFileName in pairs(GoldenFlowerRes.vecReleaseImg) do
        display.removeImage(strFileName)
    end
    -- 释放音频
    for _, strFileName in pairs(GoldenFlowerRes.vecReleaseSound) do
        AudioManager.getInstance():unloadEffect(strFileName)
    end
end

function GoldenFlowerLayer:continueTh(tabldid,i)
    --获取用户
    local pClientUserItem = ZhaJinHuaDataMgr:getInstance():getUserByChairId(i-1)
    self:HideClock(i-1)
    if pClientUserItem == nil or (not CUserManager:getInstance():isInTable(pClientUserItem)) or  (pClientUserItem.wTableID ~= PlayerInfo:getInstance():getTableID()) then                
        return
    end
    local _event = {
        name = Public_Events.MSG_USER_ENTER,
        packet = pClientUserItem.dwUserID
    }
    SLFacade:dispatchCustomEvent(Public_Events.MSG_USER_ENTER,_event)

    if ZhaJinHuaDataMgr:getInstance().m_lTableScore[i] > 0 then
        self:SetUserTableScore(i-1, ZhaJinHuaDataMgr:getInstance().m_lTableScore[i], pClientUserItem.lScore -  ZhaJinHuaDataMgr:getInstance().m_lTableScore[i])
        if(ZhaJinHuaDataMgr.getInstance().m_cbPlayStatus[i]) then
            ZhaJinHuaDataMgr.getInstance().m_JoinUsers[i] = pClientUserItem
            self.playerCount = self.playerCount + 1
            ZhaJinHuaDataMgr.getInstance():setJoinPlayerCount(playerCount)
        end
    end
    self:showUserInfo(pClientUserItem, pClientUserItem.cbUserStatus)
end

--更新用户信息显示状态
function GoldenFlowerLayer:updatePlayerPanel()
    local tableId = PlayerInfo:getInstance():getTableID()
    for i = 1, GF_GAME_PLAYER do 
        local userInfo = CUserManager.getInstance():getUserInfoByChairID( tableId,(i-1))
        if not userInfo then
            self:GetPlayerUI(i-1).m_pathUI:setVisible(false)
            self:GetPlayerUI(i-1):clearUser()
        end
    end
end

function GoldenFlowerLayer:initTableInfo(msg)
    if not ZhaJinHuaDataMgr:getInstance().m_bDataInit then 
        --return
    end
    ZhaJinHuaDataMgr:getInstance().m_BReturnMsg = true
--    print("-----1111------ZhaJinHuaDataMgr:getInstance().canPlay = false------------")
    ZhaJinHuaDataMgr:getInstance().canPlay = false
    self.m_bIsOperate = false
    self.imgeA:setVisible(true)
    self.imgeL:setVisible(false)
    self.bi = 0
    --self:showGameSettled(false)
    self:showJzLayer(false)
    self:hideComPareCardLayer()
    self:updatePlayerPanel()

    local wMeChairID = PlayerInfo:getInstance():getChairID()
    local tabldid = PlayerInfo:getInstance():getTableID()
    local mUserId = PlayerInfo:getInstance():getUserID()
    
    
    print("ZhaJinHuaDataMgr:getInstance().m_cbGameStatus:",ZhaJinHuaDataMgr:getInstance().m_cbGameStatus )
    if (ZhaJinHuaDataMgr:getInstance().m_cbGameStatus <= GAME_STATUS_FREE) or (ZhaJinHuaDataMgr:getInstance().m_cbGameStatus == GAME_STATUS_WAIT) then
        --设置控件
        self:SetAllTotalBet(0) --ZhaJinHuaDataMgr:getInstance().m_lCellScore

        if (kernel.cbUserStatus ~= G_CONSTANTS.US_LOOKON) and (kernel.cbUserStatus ~= G_CONSTANTS.US_READY) then
           g_GameController:doSendReady()
            --显示其他用户
            local allUser = CUserManager:getInstance():getUserInfoInTable(PlayerInfo:getInstance():getTableID())
            local len = table.nums(allUser)
            for i = 1, len do
                local pIClientUserItem = allUser[i]
                self:showUserInfo(pIClientUserItem, pIClientUserItem.cbUserStatus)
            end
        end
        
        self:showCheckButton(false)

        self:GetPlayerUI(ZhaJinHuaDataMgr:getInstance():getMyChairId()).m_panle_waitXJ:setVisible(false)        
    elseif ZhaJinHuaDataMgr:getInstance().m_cbGameStatus == GAME_STATUS_PLAY then
        self:SetAllTotalBet(ZhaJinHuaDataMgr:getInstance().m_lCurrentScore)
        self:updateCurRound(ZhaJinHuaDataMgr:getInstance().curRoundIndex, ZhaJinHuaDataMgr:getInstance().maxRounds)
        
        --如果中断回来是游戏状态，都算自己操作过，这一局不后悔算托管操作
        self.m_bIsOperate = true

        --设置变量
        self.playerCount = 0
        for i = 1, GF_GAME_PLAYER do
           self:continueTh(tabldid,i)
        end
        
        --庄家标志
        self:SetBankerUser(ZhaJinHuaDataMgr:getInstance().m_wBankerUser)
        
        --左上信息
        self:SetScoreInfo(ZhaJinHuaDataMgr:getInstance().m_lCellScore, ZhaJinHuaDataMgr:getInstance().m_lCellScore * ZhaJinHuaDataMgr:getInstance().m_lCurrentTimes)
        self:RefreshCards()

        --显示牌
        self:sendCardToPlayerTable(false)

        --判断是否弃牌了
        for i = 1, GF_GAME_PLAYER do
            local curChairID = i-1
            local userInfo = ZhaJinHuaDataMgr:getInstance():getUserByChairId(curChairID)
            if userInfo.cbUserStatus == G_CONSTANTS.US_PLAYING then--游戏状态
                if not ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i] and wMeChairID ~= curChairID then--弃牌了
                     self:OnGiveUp(curChairID)
                end  
            end
        end

        --设置是否看牌
        for i = 1, GF_GAME_PLAYER do
            --设置扑克
            if ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i] then           
                self:GetPlayerUI(i-1):SetCardData(ZhaJinHuaDataMgr:getInstance().m_cbHandCardData[i], GF_CARD_COUNT,i-1)
                if (((i-1) ~= wMeChairID)  or (kernel.cbUserStatus == G_CONSTANTS.US_LOOKON) ) and ZhaJinHuaDataMgr:getInstance().m_bMingZhu[i] then
                    self:GetPlayerUI(i-1):SetLookCard(true,i-1)
                end
            end
        end
        if ZhaJinHuaDataMgr:getInstance().m_bMingZhu[wMeChairID+1] and (kernel.cbUserStatus ~= G_CONSTANTS.US_LOOKON) then
            self:ShowCardValue(wMeChairID)
        end
        
        local currentUser = ZhaJinHuaDataMgr:getInstance().m_wCurrentUser
        --判断控件
        if currentUser < GF_GAME_PLAYER then
            if (not ZhaJinHuaDataMgr:getInstance().bCompareState) then
                --控件信息
                if (kernel.cbUserStatus == G_CONSTANTS.US_PLAYING)  then
                    if ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[wMeChairID + 1] then
                        ZhaJinHuaDataMgr:getInstance().canPlay = true
                    end
                    self:UpdataControl()
                    self:showCheckButton(true)
                    if ZhaJinHuaDataMgr:getInstance().isAllin and wMeChairID == ZhaJinHuaDataMgr:getInstance().m_wCurrentUser then
                        self:reSetAllInLayer()
                    end
                end

                if ZhaJinHuaDataMgr:getInstance().nLastSeconds > ZhaJinHuaDataMgr:getInstance().nOperatorTime then
                    ZhaJinHuaDataMgr:getInstance().nLastSeconds = ZhaJinHuaDataMgr:getInstance().nOperatorTime
                end
               
                if ZhaJinHuaDataMgr:getInstance().nLastSeconds <= 0 then
                    ZhaJinHuaDataMgr:getInstance().nLastSeconds = 3
                end
                
                --设置时间
                self:SetGameClock(ZhaJinHuaDataMgr:getInstance().m_wCurrentUser, IDI_USER_ADD_SCORE, ZhaJinHuaDataMgr:getInstance().nLastSeconds)
                self:GetPlayerUI(ZhaJinHuaDataMgr:getInstance().m_wCurrentUser).cdn:startCountDown(ZhaJinHuaDataMgr:getInstance().m_wCurrentUser, ZhaJinHuaDataMgr:getInstance().nLastSeconds, ZhaJinHuaDataMgr:getInstance().nOperatorTime, cc.p(69,98), 102, 102)
                
                self:GetPlayerUI(ZhaJinHuaDataMgr:getInstance().m_wCurrentUser).cdn.leftTime = ZhaJinHuaDataMgr:getInstance().nLastSeconds * 1.00
                self:GetPlayerUI(ZhaJinHuaDataMgr:getInstance().m_wCurrentUser).cdn.allTime = ZhaJinHuaDataMgr:getInstance().nOperatorTime * 1.00 
--                print("-----奇怪的----ZhaJinHuaDataMgr:getInstance().m_wCurrentUser---== "..ZhaJinHuaDataMgr:getInstance().m_wCurrentUser)
                self:GetPlayerUI(ZhaJinHuaDataMgr:getInstance().m_wCurrentUser).m_nCountDownTime = ZhaJinHuaDataMgr:getInstance().nLastSeconds
                ClockMgr.getInstance().m_wUserClock[ZhaJinHuaDataMgr:getInstance().m_wCurrentUser+1] = ZhaJinHuaDataMgr:getInstance().nOperatorTime 
                ClockMgr.getInstance().m_nElapseCount =  ZhaJinHuaDataMgr:getInstance().nLastSeconds
            else
                if (kernel.cbUserStatus ~= G_CONSTANTS.US_LOOKON) and (wMeChairID == ZhaJinHuaDataMgr:getInstance().m_wCurrentUser) then
--                    print("-----2222------ZhaJinHuaDataMgr:getInstance().canPlay = true------------")
                    if ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[wMeChairID + 1] then
                        ZhaJinHuaDataMgr:getInstance().canPlay = true
                    end
                    --选择玩家比牌
                    local bCompareUser = {}
                    for i = 1, GF_GAME_PLAYER do
                        if ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i] and (wMeChairID ~= (i-1)) and (ZhaJinHuaDataMgr:getInstance().m_usNoCompare[i] == 0 ) then
                            bCompareUser[i] = true
                        else 
                            bCompareUser[i] = false
                        end
                    end
                    
                    --设置箭头
                    self:SetCompareCardLabel(true, bCompareUser)
                    
                    --设置时间
                    self:SetGameClock(wMeChairID, IDI_USER_COMPARE_CARD, ZhaJinHuaDataMgr:getInstance().nCompareAnimationTime)
                    
                    --提示标志
                    self:SetWaitUserChoice(wMeChairID)
                    
                    --设置按钮
                    self:showCheckButton(false)
                    self:EnableAllButton(false)
                    self:showAllInButton(false)
                    
                    --等待比牌
                   g_GameController:doSendWaitCompare()

                    --fix 增加触摸事件
                    self.m_LayoutTouch:setVisible(true)
                    self.m_LayoutTouch:setEnabled(true)
                    self.pPanel_setClose:setLocalZOrder(17)
                    self.m_bIsCompSelect = true
                else
                    --比牌背景
                    self:GetPlayerUI(ZhaJinHuaDataMgr:getInstance().m_wCurrentUser):SetCompareBack(true,ZhaJinHuaDataMgr:getInstance().m_wCurrentUser)
                    self:SetGameClock(ZhaJinHuaDataMgr:getInstance().m_wCurrentUser, IDI_DISABLE, ZhaJinHuaDataMgr:getInstance().nCompareAnimationTime)
                    
                    --提示标志
                    self:SetWaitUserChoice(ZhaJinHuaDataMgr:getInstance().m_wCurrentUser)
                end
            end
        end
        --旁观 提示等待下一局
        if kernel.cbUserStatus ~= G_CONSTANTS.US_PLAYING or not ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[wMeChairID + 1] then
            self.m_bIsOperate = true
            self:GetPlayerUI(ZhaJinHuaDataMgr:getInstance():getMyChairId()).m_panle_waitXJ:setVisible(true)
            self:EnableAllButton(false)
            self:showAllInButton(false)
            self:showCheckButton(false)
            --隐藏扑克
            for j = 1, GF_CARD_COUNT do
                local xxx = self:GetPlayerUI(wMeChairID).m_Card[j]
                if xxx and xxx:getParent() then
                    xxx:removeFromParent()
                end
                self:GetPlayerUI(wMeChairID).m_Card[j] = nil
            end
            --重置押注分数
            self:SetUserTableScore(wMeChairID, 0, PlayerInfo.getInstance():getUserScore())

            --旁观状态收不到游戏消息了，倒计时可以消除
            self:KillGameClock(IDI_USER_ADD_SCORE)
        else

        end
    end
end

--起立退出
function GoldenFlowerLayer:callStandUp()
   g_GameController:doSendExit()
end

-- 时钟接口
--获取用户时间
function GoldenFlowerLayer:GetUserClock(wChairID)
   --效验参数
    assert(wChairID < MAX_CHAIR)
    if wChairID >= MAX_CHAIR then
        return 0
    end
    --获取时间
    return ClockMgr.getInstance().m_wUserClock[wChairID+1]
end

--设置用户时间
function GoldenFlowerLayer:SetUserClock(wChairID, wUserClock)
    --设置变量
    if wChairID == G_CONSTANTS.INVALID_CHAIR then
        for i = 1, MAX_CHAIR do
            ClockMgr.getInstance().m_wUserClock[i] = wUserClock
        end
    else
        ClockMgr.getInstance().m_wUserClock[wChairID+1] = wUserClock
    end
    
    --更新界面    
    self:OnEventUserClock(wChairID, wUserClock)
    return
end

--时钟标识
function GoldenFlowerLayer:GetClockID()
    return ClockMgr.getInstance().m_wClockID
end

--时钟位置
function GoldenFlowerLayer:GetClockChairID()
    return ClockMgr.getInstance().m_wClockChairID
end

--删除时钟
function GoldenFlowerLayer:KillGameClock(wClockID) 
--    print("----KillGameClock----")
    --逻辑处理
    if (ClockMgr.getInstance().m_wClockID ~= 0) and ((ClockMgr.getInstance().m_wClockID == wClockID) or (wClockID == 0)) then
        --设置界面
        ClockMgr.getInstance().mTimerEngine:StopTimer(ClockMgr.getInstance().m_wClockID)
        
        --事件通知
        if ClockMgr.getInstance().m_wClockChairID ~= G_CONSTANTS.INVALID_CHAIR then
            assert(ClockMgr.getInstance().m_wClockChairID < GF_GAME_PLAYER)
            --删除用户时间
            if ClockMgr.getInstance().m_wClockChairID ~= G_CONSTANTS.INVALID_CHAIR then
                self:SetUserClock(ClockMgr.getInstance().m_wClockChairID, 0)
            else
                self:SetUserClock(G_CONSTANTS.G_CONSTANTS.INVALID_CHAIR, 0)
            end
            
            self:HideClock(ClockMgr.getInstance().m_wClockChairID)
        end
        
        --设置变量
        ClockMgr.getInstance().m_wClockID = 0
        ClockMgr.getInstance().m_nElapseCount = 0
        ClockMgr.getInstance().m_wClockChairID = G_CONSTANTS.INVALID_CHAIR
    end
        
    return
end

--设置时钟
function GoldenFlowerLayer:SetGameClock(wChairID, wClockID, nElapse)
    
    --删除时间
    if ClockMgr.getInstance().m_wClockID ~= 0 then
        self:KillGameClock(ClockMgr.getInstance().m_wClockID)
    end
--    print("---111111---SetGameClock-SetGameClock----")
    --设置时间
    if (wChairID < GF_GAME_PLAYER) and (nElapse > 0) then
--        print("---222222-----SetGameClock-SetGameClock----")
        --设置变量
        ClockMgr.getInstance().m_wClockID = wClockID
        ClockMgr.getInstance().m_nElapseCount = nElapse
        ClockMgr.getInstance().m_wClockChairID = wChairID
        
        --设置用户时间
        --self:SetUserClock(ClockMgr.getInstance().m_wClockChairID, ClockMgr.getInstance().m_nElapseCount)
        --事件通知     
        --self:OnEventGameClockInfo(ClockMgr.getInstance().m_wClockChairID, ClockMgr.getInstance().m_nElapseCount, ClockMgr.getInstance().m_wClockID)

        --设置时间
        ClockMgr.getInstance().mTimerEngine:StartTimer(ClockMgr.getInstance().m_wClockID)
     end
    
    return
end

function GoldenFlowerLayer:OnTimerEngineTick(id)
    --时间处理
    if (ClockMgr.getInstance().m_wClockID == id) and (ClockMgr.getInstance().m_wClockChairID ~= G_CONSTANTS.INVALID_CHAIR) then
        --事件处理
        if ClockMgr.getInstance().m_nElapseCount > 0 then
            ClockMgr.getInstance().m_nElapseCount = ClockMgr.getInstance().m_nElapseCount - 1
        end     
        local bSuccess = self:OnEventGameClockInfo(ClockMgr.getInstance().m_wClockChairID, ClockMgr.getInstance().m_nElapseCount, ClockMgr.getInstance().m_wClockID)        
        
        --删除处理
        if (ClockMgr.getInstance().m_nElapseCount == 0) or (bSuccess == false) then
            self:KillGameClock(ClockMgr.getInstance().m_wClockID)
        end
        
        return
    end
end

function GoldenFlowerLayer:OnEventUserClock(wChairID, wUserClock)
    self:ShowClock(wChairID)
end


function GoldenFlowerLayer:OnEventGameClockKill(wChairID)
    self:HideClock(wChairID)
    return true
end

function GoldenFlowerLayer:OnEventGameClockInfo(wChairID, nElapse, wClockID)
    local mUserId = PlayerInfo:getInstance():getUserID()
    
    local buf = string.format("%d", nElapse)
    --//m_ViewLayer->m_PlayerNode[wClockID].m_Label_clock->setString(buf)
    if wClockID == IDI_START_GAME then
----        print("-OnEventGameClockInfo(---IDI_START_GAME-------")
--        self:ShowTimeOut(wChairID, wClockID, nElapse)
--        --设置用户时间
--        self:SetUserClock(ClockMgr.getInstance().m_wClockChairID, ClockMgr.getInstance().m_nElapseCount)        
--        if nElapse == 0 then
--            self:KillGameClock(IDI_START_GAME)
----            print("-----3333----OnEventGameClockInfo----------")
--            self:callStandUp()
--            --AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/f_slot_point.mp3")
--            FloatMessage.getInstance():pushMessage("STRING_181")
--        end
    elseif wClockID == IDI_USER_ADD_SCORE then
        --记录当前剩余时间
        self.m_nCurTime = nElapse

        self:ShowTimeOut(wChairID, wClockID, nElapse)
        --设置用户时间
        self:SetUserClock(ClockMgr.getInstance().m_wClockChairID, ClockMgr.getInstance().m_nElapseCount)
        local wMeChairID = ZhaJinHuaDataMgr:getInstance():getMyChairId()
            
        --中止判断
        if nElapse == 0 then
            --最少下注
            if (kernel.cbUserStatus ~= G_CONSTANTS.US_LOOKON) and (wMeChairID == ZhaJinHuaDataMgr:getInstance().m_wCurrentUser) then
                --自动放弃
                self:GiveUp()
            end
            return false
        end
            
        --警告通知
        if (nElapse <= 5) and (wMeChairID == ZhaJinHuaDataMgr:getInstance().m_wCurrentUser) and (kernel.cbUserStatus ~= G_CONSTANTS.US_LOOKON) then
            --AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/GAME_WARN.MP3")
            return true
        end
    elseif wClockID == IDI_USER_COMPARE_CARD then
        --记录比分用时,用于取消比牌重新开始计算
        self.m_nCurTime =  self.m_nCurTime - (ZhaJinHuaDataMgr:getInstance().nCompareAnimationTime - nElapse)

        self:ShowTimeOut(wChairID, wClockID, nElapse)
        --设置用户时间
        self:SetUserClock(ClockMgr.getInstance().m_wClockChairID, ClockMgr.getInstance().m_nElapseCount)
        if nElapse == 0 then
            --删除定时器
            self:KillGameClock(IDI_USER_COMPARE_CARD)
            --清理界面
            self:SetCompareCardLabel(false, nil)

            self.m_bIsCompSelect = false
            --自动放弃
            self:GiveUp()
            return false
        end
    elseif wClockID == IDI_USER_SHOW_CARD then        
        self:ShowTimeOut(wChairID, wClockID, nElapse)
        --设置用户时间
        self:SetUserClock(ClockMgr.getInstance().m_wClockChairID, ClockMgr.getInstance().m_nElapseCount)
        self:down(nElapse)
        if (nElapse == 0) then
--            print("-elseif wClockID == IDI_USER_SHOW_CARD then-nElapse--== "..nElapse)
            self:KillGameClock(IDI_START_GAME)
            self.panel_ksyx:setVisible(false)
        elseif (nElapse == 1) then
            AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/heguan-ready.mp3")
        end
    elseif wClockID == IDI_DISABLE then
    else
        return false
    end    
    return true
end

function GoldenFlowerLayer:GiveUp()
    self:EnableAllButton(false)
    self:showJzLayer(false)
    --删除时间
    self:KillGameClock(IDI_USER_ADD_SCORE)
    --发送消息
   g_GameController:doSendGiveUp()

    --fix 动画先行
    self:clientAbandon()
end

function GoldenFlowerLayer:OnButton_allin(sender,eventType)
    if eventType ~= ccui.TouchEventType.ended then
        return
    end
    --fix:隐藏弃牌
    --self.m_Button_qp:setEnabled(false)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
--    if( not self:checkCanDoMsg()) then
--        return
--    end
    if self.m_pPanel_setting:isVisible() then
        self:OnButton_return()
    end
    self:OnTouchLayer()
    --删除时间
    self:KillGameClock(IDI_USER_ADD_SCORE)
   
    local curPlayer = 0
    --对方用户剩余的钱
    local userLastMoney = 0
    local chairID = -1
    for i = 1, GF_GAME_PLAYER do
        if ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i] then
            local kernel = ZhaJinHuaDataMgr:getInstance():getUserByChairId(i-1)
            local userscore = self:GetPlayerUI(i-1):GetScore()
            if kernel  and  CUserManager:getInstance():isInTable(kernel) and (ZhaJinHuaDataMgr:getInstance():getMyChairId() ~= PlayerInfo:getInstance():getChairID()) then
                curPlayer = curPlayer + 1
                userLastMoney = userscore
                chairID = i - 1
            end
        end
    end
    
    if curPlayer == 1 then
        self:EnableAllButton(false)
               g_GameController:sendAllIn()
        
        self:EnableAllButton(false)      
--        self:showAllInButton(false)
--        --最大下注
--        local maxScore = (ZhaJinHuaDataMgr:getInstance().maxRounds - ZhaJinHuaDataMgr:getInstance().curRoundIndex) * ZhaJinHuaDataMgr:getInstance().m_lCellScore * 10

--        --我剩余的钱
--        local userscore = self:GetPlayerUI(PlayerInfo:getInstance():getChairID()):GetScore()

--        --获取一个基数  用来和对方比较
--        local  baseScore = ((maxScore > userscore) and userscore) or maxScore

--        --应该的下注额
--        local userBetscore = 0

--        local meMingzhu = ZhaJinHuaDataMgr:getInstance().m_bMingZhu[PlayerInfo:getInstance():getChairID()+1]
--        userBetscore = ((baseScore > userLastMoney) and userLastMoney) or baseScore

--        if (not meMingzhu) then
--            userBetscore = userBetscore / 2
--        end

--        local addScore = 0  --真正下注额度
--        local lastS = 0   --尾数
--        lastS = userBetscore % ZhaJinHuaDataMgr:getInstance().m_lCellScore

--        addScore = userBetscore - lastS
--        local wb = WWBuffer:create()        
--        wb:writeLongLong(addScore)
--        wb:writeUShort(0)
--        wb:writeUShort(G_CONSTANTS.INVALID_CHAIR)
--        wb:writeBoolean(true)
        --加注消息

    end
end

function GoldenFlowerLayer:showAllIn()    
    if self.imgeL:isVisible() then
        self:showAllInButton(false)
        return
    end
        
    if(not self.m_Button_bp:isEnabled()) then
        return
    end
        
    local meKernel = CUserManager:getInstance():getUserInfoByUserID(PlayerInfo:getInstance():getUserID())
    if (ZhaJinHuaDataMgr:getInstance().m_wCurrentUser ~= meZhaJinHuaDataMgr:getInstance():getMyChairId()) then
        return
    end
    
    if (ZhaJinHuaDataMgr:getInstance().isAllin) then
        return
    end

    local curPlayer = 0
    for i = 1, GF_GAME_PLAYER do
        if ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i] then
            local kernel = ZhaJinHuaDataMgr:getInstance():getUserByChairId(i -1)
            
            if kernel and CUserManager:getInstance():isInTable(kernel) and (ZhaJinHuaDataMgr:getInstance():getMyChairId() ~= PlayerInfo:getInstance():getChairID()) then
                curPlayer = curPlayer + 1
            end
        end
    end
    --test
    self:showAllInButton(curPlayer == 1)
end

function GoldenFlowerLayer:showGameSettled(flag,msg)
    if flag then

    else
        self:RefreshCards()
    end

end

function GoldenFlowerLayer:OnTouchLayer()
    if self.m_LayoutJz:isVisible() then 
        self:showJzLayer(false)
    end

    --fix 如果在比牌选择界面,点击了其他,比牌选择
    if ZhaJinHuaDataMgr:getInstance().bCompareState and self.m_bIsCompSelect then
        self:RevokCompareSelect()
    end
end

function GoldenFlowerLayer:updateCurRound(curRound, maxRound)
    local buff = string.format("%d/%d",curRound,maxRound)
    self.m_pLabel_tax:setString(buff)
end

function GoldenFlowerLayer:showAllInButton(v)
    self.m_Button_allin:setVisible(v)
    self.m_Button_allin:setEnabled(v) 
end

function GoldenFlowerLayer:onCheckAutoGiveUp()
    if not self.m_bIsOperate then
        self:updateEntrustLayer(true)    
    end
end

function GoldenFlowerLayer:updateTableStatus(state)

end

function GoldenFlowerLayer:showCheckButton(v)
    self:setGDDEnable(v)
    if (not v) then
        self.imgeA:setVisible(true)
        self.imgeL:setVisible(false)
        self.bit = 0
    else

    end
end

function GoldenFlowerLayer:OnButton_chat(obj)
    
end

function GoldenFlowerLayer:reSetAllInLayer()
    self:showAllInButton(false)

    self:setJZEnable(false)
    self:setGZEnable(false)
    self:setGDDEnable(false)
    self:setBPEnable(true)
    self:setQPEnable(true)

    if ZhaJinHuaDataMgr:getInstance().m_bMingZhu[PlayerInfo:getInstance():getChairID()+1] then
        self:setKPEnable(false)
    else
        self:setKPEnable(true)
    end
end

function GoldenFlowerLayer:EnableQPButton()
    local chairID = PlayerInfo:getInstance():getChairID()
    local v = true
    if ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[chairID+1] then
        v = true
    else
        v = false
    end    
    
    self:setQPEnable(v)
end

function GoldenFlowerLayer:EnalbleKPButton()
    local mUserId = PlayerInfo:getInstance():getUserID()
    
    local wChairID = ZhaJinHuaDataMgr:getInstance():getMyChairId()
    if(kernel.cbUserStatus ~= G_CONSTANTS.US_LOOKON) and (kernel.cbUserStatus == G_CONSTANTS.US_PLAYING) and (ZhaJinHuaDataMgr:getInstance().canPlay) then
        --首轮过后 看牌按钮一直显示
        if (ZhaJinHuaDataMgr:getInstance().curRoundIndex > 1) or ZhaJinHuaDataMgr:getInstance().m_bAlowLookCardFirstTurn then
            local bvisble
            if ZhaJinHuaDataMgr:getInstance().m_bMingZhu[wChairID+1] then
                bvisble = false
            else
                bvisble = true
            end

            self:setKPEnable(bvisble)
        end        
    end
end

function GoldenFlowerLayer:setNodeEnable(node, isEnable)
    if isEnable then
        node:setLocalZOrder(20)
        node:setOpacity(255)
    else
        node:setLocalZOrder(18)
        node:setOpacity(0.45*255)
    end
end

function GoldenFlowerLayer:setQPEnable(isEnable)
    self.m_Button_qp:setEnabled(isEnable)
    self:setNodeEnable(self.m_Button_qp, isEnable)
end

function GoldenFlowerLayer:setBPEnable(isEnable)
    self.m_Button_bp:setEnabled(isEnable)
    self:setNodeEnable(self.m_Button_bp, isEnable)
end

function GoldenFlowerLayer:setKPEnable(isEnable)
    self.m_Button_kp:setEnabled(isEnable)
    self:setNodeEnable(self.m_Button_kp, isEnable)
end

function GoldenFlowerLayer:setJZEnable(isEnable)
    self.m_Button_jz:setEnabled(isEnable)
    self:setNodeEnable(self.m_Button_jz, isEnable)
end

function GoldenFlowerLayer:setGZEnable(isEnable)
    self.m_Button_gz:setEnabled(isEnable)
    self:setNodeEnable(self.m_Button_gz, isEnable)
end

function GoldenFlowerLayer:setGDDEnable(isEnable)
    self.m_CheckBox_gdd:setEnabled(isEnable)
    self:setNodeEnable(self.m_CheckBox_gdd, isEnable)
end

-----------------------------------------明天继续---------------------------------------------
function GoldenFlowerLayer:doFollowAllIn()
    local mUserId = PlayerInfo:getInstance():getUserID()
    
    
    local client = ZhaJinHuaDataMgr:getInstance()
    local wMeChairID = ZhaJinHuaDataMgr:getInstance():getMyChairId()       --.GetMeChairID()
    local currentUser = client.m_wCurrentUser
    if wMeChairID ~= currentUser then
        return
    end

    local allInlCurrentScore = 0
    local meMingzhu = ZhaJinHuaDataMgr:getInstance().m_bMingZhu[PlayerInfo:getInstance():getChairID()+1]
    if meMingzhu then
        allInlCurrentScore = (client.m_lCellScore) * (client.m_lCurrentTimes) * 2
    else
        allInlCurrentScore = (client.m_lCellScore) * (client.m_lCurrentTimes)
    end    
    --构造变量
    local wCompareUser
    --查找上家 
    --[[for (int64_t i = (int64_t)wMeChairID - 1 i--)
    {
        if (i == -1)i = gf::GF_GAME_PLAYER - 1
        if (ZhaJinHuaDataMgr::getInstance()->m_cbPlayStatus[i] == TRUE)
        {
            _CompareCard.wCompareUser = (uint16_t)i
            break
        }
    }--]]
    --[[for i = (((wMeChairID == 0) and 5) or wMeChairID),1, -1 do     --递减
        if ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i] == true then
            wCompareUser = i - 1
            break
        end
    end--]]
    local i = (((wMeChairID == 0) and 5) or wMeChairID)
    while true do  
        if i == 0 then
            i = GF_GAME_PLAYER
        end  
        if ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i] then
            wCompareUser = i -1
            break
        end
        i = i - 1
    end
--    print("wCompareUser " .. tostring(wCompareUser))
    --加注消息
    local wb = WWBuffer:create()
    wb:writeLongLong(allInlCurrentScore)
    wb:writeUShort(1)
    wb:writeUShort(wCompareUser)
    wb:writeBoolean(true)    
    
   g_GameController:doSendMsg(G_C_CMD.GF_SUB_C_ADD_SCORE, wb)

    --比牌
    local wb = WWBuffer:create()
    wb:writeUShort(wCompareUser)
   g_GameController:doSendMsg(G_C_CMD.GF_SUB_C_COMPARE_CARD, wb)
end

function GoldenFlowerLayer:doGameRoundOver()
    local curPlayer = 0
    for i = 1, GF_GAME_PLAYER do
        if ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i] then
            local kernel = ZhaJinHuaDataMgr:getInstance():getUserByChairId(i-1)
            if kernel and CUserManager:getInstance():isInTable(kernel) and (ZhaJinHuaDataMgr:getInstance():getMyChairId() ~= PlayerInfo:getInstance():getChairID()) then
                curPlayer = curPlayer + 1
            end
        end
    end
    
    --只剩两个人 比牌
    if curPlayer == 1 then
        self:doButtonBP()
    else
        self:callOpenCard()
    end
end

--比牌
function GoldenFlowerLayer:doButtonBP()
    if( not self:checkCanDoMsg()) then
        return
    end

    self:showCheckButton(false)
    self:EnableAllButton(false)
    self:showAllInButton(false)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    
    ---allin 直接走特殊逻辑
--    if ZhaJinHuaDataMgr:getInstance().isAllin then
--        self:doFollowAllIn()
--        return
--    end
    
    local mUserId = PlayerInfo:getInstance():getUserID()
    
    --删除定时器    
  --  self:KillGameClock(IDI_USER_ADD_SCORE)
    
    local wMeChairID = ZhaJinHuaDataMgr:getInstance():getMyChairId()  --.GetMeChairID()
    local llTimesScore = ZhaJinHuaDataMgr:getInstance().m_lCurrentTimes * ZhaJinHuaDataMgr:getInstance().m_lCellScore
    local lCurrentScore = ((ZhaJinHuaDataMgr:getInstance().m_bMingZhu[wMeChairID+1]) and (llTimesScore*2)) or llTimesScore

    --当前人数
    local UserCount = 0
    for i = 1, GF_GAME_PLAYER do
        if ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i] then
            UserCount = UserCount + 1
        end
    end
    
    --庄家在第一轮没下注只能跟上家比牌 或 只剩下两人
    --//    if ((ZhaJinHuaDataMgr::getInstance()->m_wBankerUser == wMeChairID && (ZhaJinHuaDataMgr::getInstance()->m_lTableScore[wMeChairID] - lCurrentScore) == ZhaJinHuaDataMgr::getInstance()->m_lCellScore) || UserCount == 2)
    if UserCount == 2 then
--        print("------11111111111-------选择玩家比牌--选择玩家比牌")
        ZhaJinHuaDataMgr:getInstance().bCompareState = true
        
        --构造变量
        local wCompareUser
        --[[for i = (((wMeChairID == 0) and 5) or wMeChairID),1, -1 do     --递减
            if ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i] == true then
                wCompareUser = i -1
                break
            end
        end--]]
        local i = (((wMeChairID == 0) and 5) or wMeChairID)
        while true do    
            if i == 0 then
                i = GF_GAME_PLAYER
            end        
            if ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i] then
                wCompareUser = i -1
                break
            end
            i = i - 1
        end

--        --加注消息
--        local wb = WWBuffer:create()
--        wb:writeLongLong(lCurrentScore)
--        wb:writeUShort(1)
--        wb:writeUShort(wCompareUser)
--        wb:writeBoolean(false)        

       g_GameController:sendCompare(wCompareUser)
        --比牌
--        local wb = WWBuffer:create()
--        wb:writeUShort(wCompareUser)

--       g_GameController:doSendMsg(G_C_CMD.GF_SUB_C_COMPARE_CARD, wb)
    else	--选择玩家比牌
--        print("-------2222222222------选择玩家比牌--选择玩家比牌")
        local bCompareUser = {}
        for i = 1, GF_GAME_PLAYER do
            if ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i]  and (wMeChairID ~= (i-1)) and  ( ZhaJinHuaDataMgr:getInstance().m_usNoCompare[i] == 0 ) then
                bCompareUser[i] = true
            else 
                bCompareUser[i] = false 
            end
        end
        
        --设置箭头
        self:SetCompareCardLabel(true, bCompareUser)
       
        ZhaJinHuaDataMgr:getInstance().bCompareState = true
        
        --fix 增加触摸事件
        self.m_LayoutTouch:setVisible(true)
        self.m_LayoutTouch:setEnabled(true)
        self.pPanel_setClose:setLocalZOrder(17)
        self.m_bIsCompSelect = true

        --发送等待"比牌"信息
   --    g_GameController:doSendWaitCompare()
        
        --设置时间
       -- self:SetGameClock(wMeChairID, IDI_USER_COMPARE_CARD, ZhaJinHuaDataMgr:getInstance().nCompareAnimationTime)
        
        --提示标志
        self:SetWaitUserChoice(wMeChairID)
    end
    
    ZhaJinHuaDataMgr:getInstance().m_lCompareCount = lCurrentScore
end


function GoldenFlowerLayer:continueF(i,wMeChairID)
    if (i-1) == wMeChairID then
        return true
    end
    if ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i] and (ZhaJinHuaDataMgr:getInstance().m_usNoCompare[i] == 0 ) then
        return false
    end
end
function GoldenFlowerLayer:OnUserShowCard(chairID, cbCardData)
    local mChairId = PlayerInfo:getInstance():getChairID()
    if chairID ~= mChairId then
        --test 
        self:removeTag(chairID)
        local data = ZhaJinHuaDataMgr:getInstance().m_cbHandCardData[chairID+1]
        self:GetPlayerUI(chairID):SetCardData(data, GF_CARD_COUNT,chairID)
        self:ShowCardValue(chairID)
        --showUserPokerShow(chairID)
        --游戏结算的时候如果亮牌->隐藏弃牌，已看牌
        --m_pSpUserLooked[seatID]->setVisible(false)
        --m_pSpUserAbandon[seatID]->setVisible(false)
    end
end

function GoldenFlowerLayer:showShowPokerButton(v)
    self.m_Button_lp:setVisible(v)
    self.m_Button_lp:setEnabled(v)    
end

function GoldenFlowerLayer:showFailTag(chairId)
    self:removeTag(chairId)
    local img = ccui.ImageView:create("game/goldenflower/zjh/ts/lose_b.png",ccui.TextureResType.localType)
    self.Panel_287:addChild(img,22)
    img:setAnchorPoint(cc.p(0.5,0.5))

    --fixbug------------------------
    if chairId == INVALID_CHAIR then
        return
    end
    if self:GetPlayerUI(chairId) == nil then
        return
    end
    if self:GetPlayerUI(chairId).m_Card == nil then
        return
    end
    if self:GetPlayerUI(chairId).m_Card[2] == nil then
        return
    end
    if self:GetPlayerUI(chairId).m_Card[2].tagPos == nil then
        return
    end
    --fixbug------------------------

    local pos = self:GetPlayerUI(chairId).m_Card[2].tagPos
    local size = self.Panel_287:getContentSize()
    if PlayerInfo:getInstance():getChairID() ~= chairId then
        local ctrId = self:GetPlayerControlID(chairId)
        if ctrId == 0 or ctrId == 4 then
            img:setScale(0.85)
        end
        img:getLayoutParameter():setMargin({ left = pos.x-53, right = 0, top = size.height - pos.y-30, bottom = 0})
    else
--        print("--比牌失败等于自己")
        img:getLayoutParameter():setMargin({ left = pos.x-53, right = 0, top = size.height - pos.y+10, bottom = 0})
    end
--    img:setName(chairId)
    local name = "img" .. chairId
    img:setName(name)
end

function GoldenFlowerLayer:showGiveTag(chairId)
    self:removeTag(chairId)
    local obj = nil
    local pos = self:GetPlayerUI(chairId).m_Card[2].tagPos
    local size = self.Panel_287:getContentSize()
    if PlayerInfo:getInstance():getChairID() ~= chairId then                
        obj = ccui.ImageView:create("game/goldenflower/zjh/ts/paimian_qipai.png",ccui.TextureResType.localType)    
        self.Panel_287:addChild(obj,22)
        local ctrId = self:GetPlayerControlID(chairId)
        if ctrId == 0 or ctrId == 4 then
            obj:setScale(0.85)
        end
        obj:getLayoutParameter():setMargin({ left = pos.x-53, right = 0, top = size.height - pos.y-30, bottom = 0})
    else
--        print("--111--rLayer:showGiveTag----")
        obj = Effect:getInstance():creatEffectWithDelegate2(self.Panel_287, "qipai", "animation1",true,cc.p(pos.x,pos.y-40),22)
--        print("--222--rLayer:showGiveTag----")
        --obj:setPosition(cc.p(pos.x,pos.y-30))
        --obj:getAnimation():play("animation1")
    end
    local name = "img" .. chairId
    obj:setName(name)
end

function GoldenFlowerLayer:removeAllTag()
    for i = 1, GF_GAME_PLAYER do
        local name = "img" .. (i-1)
        local obj = self.Panel_287:getChildByName(name)
        if obj then
            obj:removeFromParent()
        end
    end
end

function GoldenFlowerLayer:removeTag(chairId)
    local name = "img" .. chairId
    local obj = self.Panel_287:getChildByName(name)
    if obj then
        obj:removeFromParent()
    end
end

function GoldenFlowerLayer:hideWaitAnim()
    local node = self:GetPlayerUI(ZhaJinHuaDataMgr:getInstance():getMyChairId())
    node:setIsPrepare(false) 
end

function GoldenFlowerLayer:onMsgUserJetton(msg)
    local mUserId = PlayerInfo:getInstance():getUserID()
    
    local manager = ZhaJinHuaDataMgr:getInstance()

    --变量定义
    local wMeChairID = ZhaJinHuaDataMgr:getInstance():getMyChairId()
    local wAddScoreUser = manager.wAddScoreUser

    --删除定时器
    if (kernel.cbUserStatus == G_CONSTANTS.US_LOOKON) or (wAddScoreUser ~= wMeChairID) then
        self:KillGameClock(IDI_USER_ADD_SCORE)
    end

    self:SetScoreInfo(manager.m_lCellScore, manager.m_lCellScore*manager.m_lCurrentTimes)
    
    manager.m_lTableScore[wAddScoreUser+1] = manager.m_lTableScore[wAddScoreUser+1] + manager.lAddScoreCount
    print("----manager.m_wCurrentUser---== "..manager.m_wCurrentUser.."----wAddScoreUser---== "..wAddScoreUser.."----manager.lAddScoreCount---== "..manager.lAddScoreCount.."----m_lCurrentScoreCount---== "..manager.m_lCurrentScoreCount)
    if((manager.m_wCurrentUser ~= wAddScoreUser) and (manager.lAddScoreCount ~= 0)) or (manager.isAllin and (manager.lAddScoreCount ~= 0)) then --相等了为比牌花的钱，不提示下注
        if manager.m_lCurrentScoreCount == 0 then
            --第一次喊牌
            print("----第一次喊牌-------------11111111111111----------== "..manager.m_lCurrentTimes)
            manager.m_nJiaZhuCount = manager.m_nJiaZhuCount + 1
            local v = ((manager.m_lCurrentTimes > 1) and 2) or 4
            self:LookCard(wAddScoreUser, v, manager.m_nJiaZhuCount)           
        else
            if (manager.m_bCurrentMingZhu and manager.m_bMingZhu[wAddScoreUser+1])
            or (( not manager.m_bCurrentMingZhu) and (not manager.m_bMingZhu[wAddScoreUser+1]))
            then
                --前一个和我都明牌或者暗牌
                if manager.lAddScoreCount > manager.m_lCurrentScoreCount then
                    manager.m_nJiaZhuCount = manager.m_nJiaZhuCount + 1
                    local v = manager.isAllin and 6 or 2
                    self:LookCard(wAddScoreUser, v, manager.m_nJiaZhuCount)
                else
                    manager.m_nGenZhuCount = manager.m_nGenZhuCount + 1
                    self:LookCard(wAddScoreUser, 1, manager.m_nGenZhuCount)
                end
            elseif manager.m_bCurrentMingZhu
            and (not manager.m_bMingZhu[wAddScoreUser+1])
            then
                --前一个明牌我暗牌0
                if(manager.lAddScoreCount *2) > manager.m_lCurrentScoreCount then
                    manager.m_nJiaZhuCount = manager.m_nJiaZhuCount + 1
                    local v = manager.isAllin and 6 or 2
                    self:LookCard(wAddScoreUser, v, manager.m_nJiaZhuCount)
                else
                    manager.m_nGenZhuCount = manager.m_nGenZhuCount + 1
                    self:LookCard(wAddScoreUser, 1, manager.m_nGenZhuCount)
                end
            elseif( not manager.m_bCurrentMingZhu) and manager.m_bMingZhu[wAddScoreUser+1] then
                --前一个暗牌我明牌
                if manager.lAddScoreCount > (manager.m_lCurrentScoreCount * 2) then
                    manager.m_nJiaZhuCount = manager.m_nJiaZhuCount + 1
                    local v = manager.isAllin and 6 or 2
                    self:LookCard(wAddScoreUser, v, manager.m_nJiaZhuCount)
                else
                    manager.m_nGenZhuCount = manager.m_nGenZhuCount + 1
                    self:LookCard(wAddScoreUser, 1, manager.m_nGenZhuCount)
                end
            end
        end
        
        manager.m_bCurrentMingZhu = manager.m_bMingZhu[wAddScoreUser+1]
        manager.m_lCurrentScoreCount = manager.lAddScoreCount
    end
    
    local userscore = self:GetPlayerUI(wAddScoreUser):GetScore()
    userscore = userscore - manager.lAddScoreCount

    manager.m_lCurrentScore = manager.m_lCurrentScore + manager.lAddScoreCount
    self:SetAllTotalBet(manager.m_lCurrentScore)
    self:SetUserTableScore(wAddScoreUser, manager.m_lTableScore[wAddScoreUser+1], userscore)

    --控件信息
    if wMeChairID == manager.m_wCurrentUser then
        manager.m_lCompareCount = 0
        if manager.isAllin then         --如果是allin
            if self:getInstance().imgeL:isVisible() then
                self:EnableAllButton(false)
                self:showAllInButton(false)
                if not manager.bCompareState then
                    self:OnActionFollow(1)
                end
            else
                self:reSetAllInLayer()
            end
        else
            if manager.bIsCmpCard then
                self:doButtonBP()
            elseif (manager.curRoundIndex > manager.maxRounds) and (not manager.bCompareState) then
                if manager.canPlay and (manager.m_wCurrentUser == PlayerInfo:getInstance():getChairID()) then
                    self:callOpenCard()
                end
            else
                if not manager.bCompareState then
                    local currenScore = manager.m_lCellScore*(manager.m_lCurrentTimes)
                    local needScore = ((manager.m_bMingZhu[wMeChairID+1] == true) and currenScore*2) or currenScore
                    local myScore = self:GetPlayerUI(wMeChairID):GetScore()    --getUserScoreByChairID(wChairID)
                    --如果其他玩家都禁比 自己的筹码也无法再跟注
                    if  myScore <= needScore then  --[[ 0814修改 (myScore < needScore)]]
                        self:UpdataControl()
                        self:EnableQPButton()
                        
                        self.imgeA:setVisible(true)
                        self:showCheckButton(false)
                        self:setGZEnable(false)
                        self:showAllInButton(false)
                        self:setBPEnable(true)
                    elseif self.imgeL:isVisible() then
                        self:FollowBet()
                        if self.imgeL:isVisible() then
                            self:EnableAllButton(false)
                            self:showAllInButton(false)
                            self:EnableQPButton()
                        end
                    else
                        self:UpdataControl()
                        self:EnableQPButton()
                    end
                else
                    self:setBPEnable(false)
                    self:setQPEnable(false)
                end
            end
        end
    else
        self:EnableAllButton(false)
        self:EnableQPButton()
        self:EnalbleKPButton()
    end

    --设置时间
    if not manager.bCompareState then
        self:SetGameClock(manager.m_wCurrentUser, IDI_USER_ADD_SCORE, ZhaJinHuaDataMgr:getInstance().nOperatorTime)
    end
    if manager.lAddScoreCount > 0 then
        math.randomseed(tostring(os.time()):reverse():sub(1,7))
--        if ZhaJinHuaDataMgr:getInstance().m_bMingZhu[wAddScoreUser+1] == true then
--            self:SendChouMa(wAddScoreUser, ZhaJinHuaDataMgr:getInstance().m_lCurrentTimes, msg.lAddScoreCount)
--        end
        self:SendChouMa(wAddScoreUser, manager.m_lCurrentTimes) --, manager.lAddScoreCount)
        AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/jetton-single.mp3")
    end
    if manager.curRoundIndex <= manager.maxRounds then
        self:updateCurRound(manager.curRoundIndex, manager.maxRounds)
    end 

    --如果allin了
    if manager.isAllin and wMeChairID == wAddScoreUser then
        self:EnableAllButton(false)
        self:setGDDEnable(false)
    end

    if manager.m_lCurrentScoreCount ~= 0 and manager.lAddScoreCount ~= 0 then
        self:jz()
    elseif manager.m_lCurrentScoreCount == 0 and manager.lAddScoreCount == 0 then
        self:xz()
    end
end

function GoldenFlowerLayer:startGame(msg)

    local function continueT(i)
        ZhaJinHuaDataMgr:getInstance().m_wLostUserID[i] = G_CONSTANTS.INVALID_CHAIR
        ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i] = false
        ZhaJinHuaDataMgr:getInstance().m_lTableScore[i] = 0
        ZhaJinHuaDataMgr:getInstance().m_bMingZhu[i] = false
        ZhaJinHuaDataMgr:getInstance().m_bGiveUp[i] = false
        
        --获取用户
        local userinfo = ZhaJinHuaDataMgr:getInstance():getUserByChairId( i-1)
        
        if (userinfo == nil) then
            return
        end
        
        --游戏信息
        print("ZhaJinHuaDataMgr:getInstance().m_lCellScore " .. tostring(ZhaJinHuaDataMgr:getInstance().m_lCellScore))
        ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i] = true
        ZhaJinHuaDataMgr:getInstance().m_lTableScore[i] = ZhaJinHuaDataMgr:getInstance().m_lCellScore
        ZhaJinHuaDataMgr:getInstance().m_bMingZhu[i] = false
        GoldenFlowerLayer:getInstance():SetUserTableScore(i-1, ZhaJinHuaDataMgr:getInstance().m_lTableScore[i], userinfo.m_score*0.01 - ZhaJinHuaDataMgr:getInstance().m_lCellScore)
        --用户信息
        local buff = string.format("%s",userinfo.m_nickname)
        ZhaJinHuaDataMgr:getInstance().m_szAccounts[i][1] = buff
        ZhaJinHuaDataMgr:getInstance().m_lCurrentScore = ZhaJinHuaDataMgr:getInstance().m_lCurrentScore + ZhaJinHuaDataMgr:getInstance().m_lTableScore[i]
        if  (i-1) == ZhaJinHuaDataMgr:getInstance().m_wBankerUser then
        
            local user = ZhaJinHuaDataMgr:getInstance().m_wBankerUser
            local userUI = self:GetPlayerUI(user)
            userUI.m_Image_Banker:setVisible(true)
        else
            --todo
            local user = ZhaJinHuaDataMgr:getInstance().m_wBankerUser
            local userUI = self:GetPlayerUI(user)
            userUI.m_Image_Banker:setVisible(false)
        end

        --记录参加本局的玩家
        ZhaJinHuaDataMgr.getInstance().m_JoinUsers[i] = userinfo
        local playerCount = ZhaJinHuaDataMgr.getInstance():getJoinPlayerCount() + 1
        ZhaJinHuaDataMgr.getInstance():setJoinPlayerCount(playerCount)
    end

    --设置变量
    for i = 1, GF_GAME_PLAYER do
        continueT(i)
    end
     --界面设置
    self:updateCurRound(1, ZhaJinHuaDataMgr:getInstance().maxRounds)
    self:SetBankerUser(ZhaJinHuaDataMgr:getInstance().m_wBankerUser)
    self:SetScoreInfo(ZhaJinHuaDataMgr:getInstance().m_lCellScore, ZhaJinHuaDataMgr:getInstance().m_lCellScore * ZhaJinHuaDataMgr:getInstance().m_lCurrentTimes)
    self:SetAllTotalBet(ZhaJinHuaDataMgr:getInstance().m_lCurrentScore)
    self:RefreshCards()
    
    --清空上局遗留的tag显示
    self:removeAllTag()
    
    self:EnableAllButton(false)
    self:showAllInButton(false)

    --fix 游戏初始化变量
    self.m_bClientAbandon = false

    --隐藏'等待其他玩家动画'动画
    self:hideWaitAnim()

    self.m_SendingCard = true
    for i = 1, GF_GAME_PLAYER do
        if ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i] then
            self:SendChouMa(i-1, 1, 2,true)
            AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/jetton-single.mp3")
        end
    end

    self:doSomethingLater(function()
        AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/heguan-fapai.mp3")
        self:sendCardStart()
    end, 0.6)
end

function GoldenFlowerLayer:onMsgUserLook(msg)
    local wId = tonumber(msg)
    local mUserId = PlayerInfo:getInstance():getUserID()
    

     ZhaJinHuaDataMgr:getInstance().m_bMingZhu[wId+1] = true
    if wId == ZhaJinHuaDataMgr:getInstance():getMyChairId() then
        local data = ZhaJinHuaDataMgr:getInstance().m_cbHandCardData[wId+1]
        self:GetPlayerUI(wId):SetCardData(data, GF_CARD_COUNT,wId)

        local LeftPos = cc.p(10000, 0)
        
        for i = 1, GF_CARD_COUNT do
            local xxx = self:GetPlayerUI(wId).m_Card[i]
            if LeftPos.x > xxx:getPositionX() then
                LeftPos = cc.p(xxx:getPosition())
            end
        end
        
        for i = 1, GF_CARD_COUNT do
            local card = self:GetPlayerUI(wId).m_Card[i]            
            local OldPos = cc.p(card:getPosition())
            local actMove1 = cc.MoveTo:create(0.2, LeftPos)
            local actMove2 = cc.MoveTo:create(0.2, OldPos)
            local function func()
                self:changeCard(card)
            end
            local callback = cc.CallFunc:create(func)
            local act = cc.Sequence:create(actMove1,callback,actMove2)
            card:runAction(act)
        end
    end
    self:LookCard(wId, 0)
end

function GoldenFlowerLayer:onMsgUserCompare(msg)
    local vecStrings = {}
    vecStrings = string.split(msg, ",")
    if #vecStrings == 0 or table.nums(vecStrings) == 0 then
        return
    end
    local wCurrentUser = tonumber(vecStrings[1])
    local wCompareUser = {}
    wCompareUser[1] = tonumber(vecStrings[2])
    wCompareUser[2] = tonumber(vecStrings[3])
    local wLostUser = tonumber(vecStrings[4])

    local lastUser = ZhaJinHuaDataMgr.getInstance().m_wCurrentUser 
    local mUserId = PlayerInfo:getInstance():getUserID()
    
    local wMeChairID = ZhaJinHuaDataMgr:getInstance():getMyChairId()

    self.m_bIsCompSelect = false
    --删除时间
    self:KillGameClock(IDI_DISABLE)

    --取消等待
    self:SetWaitUserChoice(G_CONSTANTS.INVALID_CHAIR)

    --输牌用户
    ZhaJinHuaDataMgr:getInstance().m_wLostUser = wLostUser

    for i = 1, GF_GAME_PLAYER do
        if ZhaJinHuaDataMgr:getInstance().m_wLostUserID[i] == G_CONSTANTS.INVALID_CHAIR then
            ZhaJinHuaDataMgr:getInstance().m_wLostUserID[i] = wLostUser
            break
        end
    end

    --设置变量
    ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[wLostUser+1] = false

    --状态设置
    if wLostUser == wMeChairID then
        ZhaJinHuaDataMgr:getInstance():setGameStatus(GAME_STATUS_FREE)
        ZhaJinHuaDataMgr:getInstance().canPlay = false
    end

    if wLostUser == PlayerInfo:getInstance():getChairID() then
        --self:showCheckButton(false)
        self:EnableAllButton(false)
        self:showAllInButton(false)

        self:SetCompareCardLabel(false, nil)
    end

    self:doSomethingLater(function()
        self:GetPlayerUI(lastUser):playerPKSound()   
        self:PerformCompareCard(wCompareUser, wLostUser)
    end, 0.8)
end

function GoldenFlowerLayer:onMsgUserOpen(msg)
    local wWinner = tonumber(msg)
    --取消等待
    self:SetWaitUserChoice(G_CONSTANTS.INVALID_CHAIR)
    --胜利用户
    ZhaJinHuaDataMgr:getInstance().m_wWinnerUser = wWinner
    --游戏结束
    self:StopUpdataScore(true)

    FloatMessage.getInstance():pushMessage("STRING_183")
end

function GoldenFlowerLayer:onMsgUserAbandon(msg)
    local wGiveUpUser = tonumber(msg)
    local mUserId = PlayerInfo:getInstance():getUserID()
    
    local wMeChairID = ZhaJinHuaDataMgr:getInstance():getMyChairId()

    if wGiveUpUser ~= wMeChairID then
        self:LookCard(wGiveUpUser, 3)
    else
        --fix 在别人下注的时候断网弃牌,导致服务器没收到我发送弃牌，再连上会收到别人不下注消息会更新我的按钮状态,需要重置为弃牌状态
        self:EnableAllButton(false)
        self:showAllInButton(false)
        self:showCheckButton(false)
        
        --服务器帮我弃牌了
        if not self.m_bClientAbandon then
            --重新设置被选比牌用户
            local wMeChairID = PlayerInfo:getInstance():getChairID()
            self:LookCard(wMeChairID, 3)
            if ZhaJinHuaDataMgr:getInstance().bCompareState and (ZhaJinHuaDataMgr:getInstance().m_wCurrentUser == wMeChairID) then
                local bCompareUser = {}
                local bHaveComparePlayer = false
                for i = 1,GF_GAME_PLAYER do
                    if ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i]  and wMeChairID ~= (i-1) and (ZhaJinHuaDataMgr:getInstance().m_usNoCompare[i] == 0 ) then
                        bCompareUser[i] = true
                        bHaveComparePlayer = true
                    else
                        bCompareUser[i] = false
                    end
                end
                if bHaveComparePlayer then
                    --设置箭头
                    self:SetCompareCardLabel(true, bCompareUser)
                else
                    --如果没有可比牌的玩家
                    ZhaJinHuaDataMgr:getInstance().bCompareState = false

                    self:KillGameClock(IDI_USER_COMPARE_CARD)
                    self:SetCompareCardLabel(false, nil)
                    self:SetWaitUserChoice(G_CONSTANTS.INVALID_CHAIR)

                    self:EnableAllButton(true)
                    self:showAllIn()
                    self:UpdataControl()
                end
            end

            --状态设置
            ZhaJinHuaDataMgr:getInstance():setGameStatus(GAME_STATUS_FREE)
            ZhaJinHuaDataMgr:getInstance().canPlay = false

            self:EnableAllButton(false)
            self:showAllInButton(false)
            self:showCheckButton(false)

            --删除时间
            if ZhaJinHuaDataMgr:getInstance().m_wCurrentUser == wMeChairID then
                self:KillGameClock(IDI_USER_ADD_SCORE)
                self:showJzLayer(false)
            end

            --比牌提示
            self:SetWaitUserChoice(G_CONSTANTS.INVALID_CHAIR)
            --设置箭头
            self:SetCompareCardLabel(false, nil)
            self.m_bClientAbandon = false
        end
    end
end

function GoldenFlowerLayer:onMsgGameEnd(msg)
    local mUserId = PlayerInfo:getInstance():getUserID()
    
    local wMeChiar = ZhaJinHuaDataMgr:getInstance():getMyChairId()
     --删除定时器
    self:KillGameClock(IDI_USER_ADD_SCORE)
    --处理控件
    self:showAllInButton(false)
    self:showJzLayer(false)
    self.mBGameStart = false
    self.m_CompareBg:setVisible(false)

    --如果自己的牌没看到花就显示一下吧
    if not ZhaJinHuaDataMgr:getInstance().m_bMingZhu[wMeChiar+1] then
        local data = ZhaJinHuaDataMgr:getInstance().m_cbHandCardData[wMeChiar+1]
        self:GetPlayerUI(wMeChiar):SetCardData(data, GF_CARD_COUNT, wMeChiar)
        self:ShowCardValue(wMeChiar)
    end

    --检查自己参加本局才弹出结算框
    for i = 1, GF_GAME_PLAYER do
        local userInfo = ZhaJinHuaDataMgr:getInstance().m_JoinUsers[i]
        if userInfo and userInfo.dwUserID == PlayerInfo:getInstance():getUserID() then
            --比牌输了，结束展示比牌对手的牌
            for i= 1, 4 do
                local loseChairID = ZhaJinHuaDataMgr:getInstance().m_CompareWithMe[i]
                if (loseChairID  ~= G_CONSTANTS.INVALID_CHAIR) then
                    --等于是对手亮牌给自己看
                    if loseChairID ~= wMeChiar then
                        self:removeTag(loseChairID)
                    end
                    local data = ZhaJinHuaDataMgr:getInstance().m_cbHandCardData[loseChairID+1]
                    self:GetPlayerUI(loseChairID):SetCardData(data, GF_CARD_COUNT,loseChairID)
                    self:ShowCardValue(loseChairID)
                end
            end
            self:showShowPokerButton(true)
            if(not ZhaJinHuaDataMgr:getInstance().m_bMingZhu[wMeChiar+1]) then
                local data = ZhaJinHuaDataMgr:getInstance().m_cbHandCardData[wMeChiar+1]
                self:GetPlayerUI(wMeChiar):SetCardData(data, GF_CARD_COUNT,wMeChiar)
                self:ShowCardValue(wMeChiar)
            end
            break
        end
    end
    self:OnGameEnd()
    self.m_bCurrentMingZhu = false
    
    --fixBug:游戏结束，将分清零
    --self.m_lCurrentScoreCount = 0
    ZhaJinHuaDataMgr:getInstance().m_lCurrentScoreCount = 0

    self.bCompareState = false
    self.m_wCurrentUser = G_CONSTANTS.INVALID_CHAIR
    self.m_wBankerUser = G_CONSTANTS.INVALID_CHAIR

    --回收筹码
    if ZhaJinHuaDataMgr:getInstance().m_gameWinner < GF_GAME_PLAYER then
        self:SetGameEndInfo(ZhaJinHuaDataMgr:getInstance().m_gameWinner)
        --飞筹码有一秒,延迟一秒显示赢家闪烁动画
        self:doSomethingLater(function()
            self:GetPlayerUI(ZhaJinHuaDataMgr:getInstance().m_gameWinner):showWinerBlink()
        end, 1)
    end

    self:EnableAllButton(false)
    self:showAllInButton(false)
    self:showCheckButton(false)
    --状态设置
    self:SetWaitUserChoice(G_CONSTANTS.INVALID_CHAIR)
    ZhaJinHuaDataMgr:getInstance():setGameStatus(GAME_STATUS_FREE)
    ZhaJinHuaDataMgr:getInstance().canPlay = false    

    --体验房金币不足处理
    -- if PlayerInfo.getInstance():IsInExperienceRoom() then 
    --     local score = PlayerInfo:getInstance():getUserScore() 
    --     local minTableScore =PlayerInfo.getInstance():getMinTableScore()
    --     if score < minTableScore or score <= 0 then 
    --         SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_MESSAGEBOX, "go-recharge")
    --     end
    -- end
end

function GoldenFlowerLayer:onMsgGoReady(msg)
    self:onCleanTableAndShowStart(1)
end

function GoldenFlowerLayer:onMsgShowCard(msg)
    local wChairID = tonumber(msg)
    self:LookCard(wChairID, 5)
    self:OnUserShowCard(wChairID, ZhaJinHuaDataMgr:getInstance().m_cbHandCardData[wChairID+1])
end

function GoldenFlowerLayer:onMsgCurrentUser(msg)
    local mUserId = PlayerInfo:getInstance():getUserID()
    
    local wMeChairID = ZhaJinHuaDataMgr:getInstance():getMyChairId()

    --取消等待
    self:SetWaitUserChoice(G_CONSTANTS.INVALID_CHAIR)
    
    --去掉选择比牌状态
    self:SetCompareCardLabel(false, nil)
    
    --控件信息
    if (kernel.cbUserStatus ~= US_LOOKON) and ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[wMeChairID+1] then
        if (wMeChairID == ZhaJinHuaDataMgr:getInstance().m_wCurrentUser) then
            local currenScore = ZhaJinHuaDataMgr:getInstance().m_lCellScore * (ZhaJinHuaDataMgr:getInstance().m_lCurrentTimes)
            local needScore = ((ZhaJinHuaDataMgr:getInstance().m_bMingZhu[wMeChairID+1] == true) and (currenScore*2)) or currenScore
            local myScore = self:GetPlayerUI(wMeChairID):GetScore()
            --如果其他玩家都禁比 自己的筹码也无法再跟注
            if myScore <= needScore then
                self:UpdataControl()
                self:EnableQPButton()
                
                self.imgeA:setVisible(true)
                self:showCheckButton(false)
                self:setGZEnable(false)
                self:showAllInButton(false)
                self:setBPEnable(true)
            elseif self.imgeL:isVisible() then
                self:FollowBet()
                if self.imgeL:isVisible() then
                    self:EnableAllButton(false)
                    self:showAllInButton(false)
                    self:EnableQPButton()
                end
            else
                self:UpdataControl()
                self:EnableQPButton()
            end
        else
            self:EnableAllButton(false)
            self:EnableQPButton()
            self:EnalbleKPButton()
            self:showCheckButton(true)
        end
    else
        self:EnableAllButton(false)
        self:showCheckButton(false)
        self:showAllInButton(false)
    end

    --玩家人数
    local bCount = 0
    for i=1, GF_GAME_PLAYER do
        if ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i] then
            bCount = bCount + 1
        end
    end
--    print("-----------OnSubCurrentChairID----bCount--== "..bCount)
    if bCount > 1 then
        --设置时间
        self:SetGameClock(ZhaJinHuaDataMgr:getInstance().m_wCurrentUser, IDI_USER_ADD_SCORE, ZhaJinHuaDataMgr:getInstance().nOperatorTime)
        self:EnableQPButton()
    else
        self:EnableAllButton(false)
        self:showCheckButton(false)
        self:showAllInButton(false)
    end
end

function GoldenFlowerLayer:onMsgRevokeCompare()

    if ZhaJinHuaDataMgr:getInstance().m_wCurrentUser == PlayerInfo:getInstance():getChairID() then
        --取消等待
        self:SetWaitUserChoice(G_CONSTANTS.INVALID_CHAIR)
        --去掉选择比牌状态
        self:SetCompareCardLabel(false, nil)
        --设置按钮状态
        self:UpdataControl()
        --重新开始倒计时
        if self.m_nCurTime <= 0 then
            self.m_nCurTime = 3
        end
        self:SetGameClock(PlayerInfo:getInstance():getChairID(), IDI_USER_ADD_SCORE, self.m_nCurTime)
    end
end

function GoldenFlowerLayer:onShowOneCardBtn()
    local diffx = 145 - (1624 - display.width)/2    
    if not self.m_pBtnShowCard then
        self.m_pBtnShowCard = LuaUtils.getSuperBtn(self.Panel_287,cc.p(display.cx-diffx,display.height-50),50,handler(self,self.onShowOneCardClicked))
    end

    if self.m_bShowCard then
        performWithDelay(self.m_Panel_show_pai,function ()
            self:onShowOneCardClicked(self.m_bShowCard)
        end,1.2)
    end
end

function GoldenFlowerLayer:onShowOneCardClicked(show_bool)
    self.m_bShowCard = show_bool
    local is_visible = self.m_Panel_show_pai:isVisible()
    if is_visible then
        self.m_Panel_show_pai:hide()
    else
        for i=1,GF_GAME_PLAYER do
            local is_have_data = ZhaJinHuaDataMgr:getInstance().m_lTableScore[i] --该座位是否有玩家(用玩家下注分数来判断)
            local cur_data = ZhaJinHuaDataMgr:getInstance():getCardsDataByCharID(i-1)
            if cur_data and is_have_data > 0 then
                for j=1,GF_CARD_COUNT do
                    self:GetPlayerUI(i-1).m_ShowCard[j]:setCardData(cur_data[j])
                    local card_bak = CardSprite.getCardWithData(cur_data[j])
                    self:GetPlayerUI(i-1).m_ShowCard[j]:setSpriteFrame(card_bak:getSpriteFrame())
                    self:GetPlayerUI(i-1).m_ShowCard[j]:show()
                end
            else
                for j=1,GF_CARD_COUNT do
                    if self:GetPlayerUI(i-1).m_ShowCard[j] then
                        self:GetPlayerUI(i-1).m_ShowCard[j]:hide()
                    end
                end
            end
        end
        self.m_Panel_show_pai:show()
    end


end

-----------------0817 炸金花scene内方法移动--------------
function GoldenFlowerLayer:showUserInfo(pIClientUserItem, cbUserStatus)
    if(pIClientUserItem == nil) or (not CUserManager:getInstance():isInTable(pIClientUserItem)) then
        return
    end
    local tableID = PlayerInfo:getInstance():getTableID()
    
    if tableID ~= pIClientUserItem.wTableID then
        return
    end

    local cid = pIClientUserItem.wChairID
    local node = self:GetPlayerUI(cid)
    
    node.m_pathUI:setVisible(true)
    node:setNickName(pIClientUserItem.m_nickname)
    node:setUserVip(pIClientUserItem.nVipLev)
    node:SetGunder(pIClientUserItem.cbGender or 0)

    local nFaceID = pIClientUserItem.m_faceId
    if nFaceID < 0 then
        nFaceID = 0
    end
    local realid = nFaceID % G_CONSTANTS.FACE_NUM + 1
    
    local strHeadIcon = string.format("hall/image/file/gui-icon-head-%02d.png", realid)
    node:setHead(strHeadIcon)

    local nFrameID = pIClientUserItem.wFaceCircleID
    local strFramePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", nFrameID)
    node:setFrame(strFramePath)

    if ZhaJinHuaDataMgr:getInstance().m_lTableScore[pIClientUserItem.wChairID+1] > 0 then
        local curAddScore = ZhaJinHuaDataMgr:getInstance().m_lTableScore[pIClientUserItem.wChairID+1]
        node:SetScore(pIClientUserItem.lScore - curAddScore)
    else
        node:SetScore(pIClientUserItem.lScore)
    end
--    local len = #ZhaJinHuaDataMgr:getInstance().m_zbUser
--    print("---m_zbUser-------- == "..len)
--    if pIClientUserItem.cbUserStatus == G_CONSTANTS.US_READY and len == 1 then        
--        node:setIsPrepare(true)
--        self:showSettlementFrame(pIClientUserItem.wChairID, false)
--    else
--        node:setIsPrepare(false)
--    end
    node:setIsZhuang(false)
    node:setHeadKuang(false)
    node:setShowGoldKuang(false)
    node:setShowHead(true)
    node.m_ChairID = pIClientUserItem.wChairID
end

function GoldenFlowerLayer:showUserInfoByNew(pIClientUserItem,  cbUserStatus)
    if (pIClientUserItem == nil) or (not CUserManager:getInstance():isInTable(pIClientUserItem)) then
        return
    end
    local tableID = PlayerInfo:getInstance():getTableID()
    
    if tableID ~= pIClientUserItem.wTableID then
        return
    end

    local cid = pIClientUserItem.wChairID
    local node = self:GetPlayerUI(cid)
    node:SetScore(pIClientUserItem.lScore)
end

function GoldenFlowerLayer:net_user_state_update(chair_id, user_state)
    if self ~= nil then
        local node = self:GetPlayerUI(chair_id)    
        node.m_pathUI:setVisible(true)
    end
    if user_state == G_CONSTANTS.US_READY then
        ZhaJinHuaDataMgr:getInstance().m_bIsReady[chair_id] = true    
    end
end

function GoldenFlowerLayer:net_user_leave(chair_id)
    local node = self:GetPlayerUI(chair_id)
    node.m_pathUI:setVisible(false)
    node:SetVisible(false,chair_id)      --注意  node->m_CardControl.SetVisible(false);
    node:clearUser(true)
    self:removeTag(chair_id)
    for j = 1, GF_CARD_COUNT do
        local xxx = self:GetPlayerUI(chair_id).m_Card[j]
        if xxx and xxx:getParent() then
            xxx:removeFromParent()
        end
        self:GetPlayerUI(chair_id).m_Card[j] = nil
    end
end

function GoldenFlowerLayer:showSettlementFrame(chair_id, isShow)
    local node = self:GetPlayerUI(chair_id)
    node:setShowGoldKuang(isShow)
end

function GoldenFlowerLayer:onMsgExitGame(msg)
    self:callStandUp()

    PlayerInfo.getInstance():setIsGameBackToHall(true)
    PlayerInfo:getInstance():setIsGameBackToHallSuc(false)
    SLFacade:dispatchCustomEvent(Public_Events.Load_Entry)

    if msg and tonumber(msg) == 1 then
        SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_MESSAGEBOX, "go-recharge")
    end
end

function GoldenFlowerLayer:onMsgNetWorkFailre(msg)
    --self:callStandUp()

    if msg == "1" then
        FloatMessage.getInstance():pushMessage("STRING_023_1")
    else
        FloatMessage.getInstance():pushMessage("STRING_023")
    end
    
    PlayerInfo.getInstance():setChairID(INVALID_CHAIR)
    PlayerInfo.getInstance():setTableID(INVALID_TABLE)
    PlayerInfo.getInstance():setSitSuc(false)
    PlayerInfo:getInstance():setIsGameBackToHallSuc(false)
    SLFacade:dispatchCustomEvent(Public_Events.Load_Entry)
end

function GoldenFlowerLayer:onMsgUpdateChatInfo(_event)
end

function GoldenFlowerLayer:onMsgHiteChatNoticeEffect(_event)
end

function GoldenFlowerLayer:onMsgUpdateUsrScore(_event)
end

--站起  退出游戏
function GoldenFlowerLayer:onMsgFree(msg)
    local chairID = tonumber(msg) 
    if chairID == PlayerInfo:getInstance():getChairID() then        
        self:closeSelf(true)
    else
        self:OnEventUserLeave(msg)
    end
end

function GoldenFlowerLayer:onMsgSit(msg)
    local chairId = tonumber(msg)

    local tableId = PlayerInfo:getInstance():getTableID()    
    local pIClientUserItem = ZhaJinHuaDataMgr:getInstance():getUserByChairId(tableId, chairId)
    if (pIClientUserItem ~= nil) or (not CUserManager:getInstance():isInTable(pIClientUserItem)) then
        return
    end
    self:showUserInfo(pIClientUserItem, pIClientUserItem.cbUserStatus)
end

function GoldenFlowerLayer:onMsgReady(msg)
    local chairId = tonumber(msg)

    local allUser = CUserManager.getInstance():getUserInfoInTable(PlayerInfo.getInstance():getTableID())
    local chiarid = ZhaJinHuaDataMgr:getInstance():getMyChairId()

    if #allUser >= 1 then
        --隐藏自己等待动画
        
        if chiarid ~= 65535 then
            local node = self:GetPlayerUI(chiarid)
            node:setIsPrepare(false)
        end
    else
        if chiarid ~= 65535 then
            local node = self:GetPlayerUI(chiarid)
            node:setIsPrepare(true)
            self:showSettlementFrame(chiarid, false)
        end
    end
end

function GoldenFlowerLayer:OnEventUserEnter(msg)
    local uid = tonumber(msg)

    local pIClientUserItem = CUserManager:getInstance():getUserInfoByUserID(uid)
    if(pIClientUserItem == nil) or (not CUserManager:getInstance():isInTable(pIClientUserItem)) then
        return
    end

    local tableID = PlayerInfo:getInstance():getTableID()
    if tableID ~= pIClientUserItem.wTableID then
        return
    end

    self:GetPlayerUI(pIClientUserItem.wChairID).m_pathUI:setVisible(true)
    if pIClientUserItem.cbUserStatus ~= G_CONSTANTS.US_LOOKON then    --(!pIClientUserItem.IsLookonMode() )
        self:showUserInfo(pIClientUserItem, pIClientUserItem.cbUserStatus)
    end
end

--自己做下
function GoldenFlowerLayer:OnEventSeifSitDown(msg)
    local chairid = tonumber(msg)

    local pIClientUserItem = ZhaJinHuaDataMgr:getInstance():getUserByChairId( chairid)
    local obj = self:GetPlayerUI(pIClientUserItem.wChairID)
    obj.m_pathUI:setVisible(true)
    if pIClientUserItem.cbUserStatus ~= G_CONSTANTS.US_LOOKON then
        self:showUserInfo(pIClientUserItem, pIClientUserItem.cbUserStatus)
    end
end


function GoldenFlowerLayer:OnEventUserLeave(msg)
    local chairID = tonumber(msg)

    local tableID = PlayerInfo:getInstance():getTableID()    

    local pIClientUserItem = ZhaJinHuaDataMgr:getInstance():getUserByChairId(tableID, chairID)
    self:GetPlayerUI(chairID).m_pathUI:setVisible(false)
    if not pIClientUserItem or pIClientUserItem.cbUserStatus ~= G_CONSTANTS.US_LOOKON then
        -- 是不是观看用户
        local wChairID = chairID
        local meChairID = PlayerInfo:getInstance():getChairID()
        if (meChairID ~= G_CONSTANTS.INVALID_CHAIR) and (meChairID~= wChairID) then
            --///< 其他用户离开
            self:net_user_leave(wChairID)
        else
            self:closeSelf()
        end
    end
end

function GoldenFlowerLayer:OnEventUserStatus(msg)
    local uid = tonumber(msg)

    local tableID = PlayerInfo:getInstance():getTableID()
    
    local pIClientUserItem = CUserManager:getInstance():getUserInfoByUserID(uid)
    if ( pIClientUserItem == nil ) or (not CUserManager:getInstance():isInTable(pIClientUserItem)) or (tableID ~= pIClientUserItem.wTableID) then
        return
    end
    --G_CONSTANTS.US_READY
    self:net_user_state_update(pIClientUserItem.wChairID, pIClientUserItem.cbUserStatus)
    self:showUserInfo(pIClientUserItem, pIClientUserItem.cbUserStatus)
    
    if ZhaJinHuaDataMgr:getInstance().m_cbGameStatus == GAME_STATUS_FREE then 
        self:showUserInfoByNew(pIClientUserItem, pIClientUserItem.cbUserStatus)
    end

end

function GoldenFlowerLayer:OnMsgUpdateTableStatus(_event)
    local _userdata = unpack(_event._userdata)
    if not _userdata then
        return
    end

    local tableid = _userdata

    if tableid == PlayerInfo:getInstance():getTableID() then
--        self:updateTableStatus(GameSceneDataMgr:getInstance():getTableStatus())
    end
end

function GoldenFlowerLayer:doSomethingLater(call, delay)
    self:runAction(cc.Sequence:create(cc.DelayTime:create(delay), cc.CallFunc:create(call)))
end


------------------动画先行
function GoldenFlowerLayer:clientAbandon()--客户端动画先行弃牌
    --重新设置被选比牌用户
    local wMeChairID = PlayerInfo:getInstance():getChairID()
    self:LookCard(wMeChairID, 3)
    if ZhaJinHuaDataMgr:getInstance().bCompareState and (ZhaJinHuaDataMgr:getInstance().m_wCurrentUser == wMeChairID) then
        local bCompareUser = {}
        local bHaveComparePlayer = false
        for i = 1,GF_GAME_PLAYER do
            if ZhaJinHuaDataMgr:getInstance().m_cbPlayStatus[i]  and wMeChairID ~= (i-1) and (ZhaJinHuaDataMgr:getInstance().m_usNoCompare[i] == 0 ) then
                bCompareUser[i] = true
                bHaveComparePlayer = true
            else
                bCompareUser[i] = false
            end
        end
        if bHaveComparePlayer then
            --设置箭头
            self:SetCompareCardLabel(true, bCompareUser)
        else
            --如果没有可比牌的玩家
            ZhaJinHuaDataMgr:getInstance().bCompareState = false

            self:KillGameClock(IDI_USER_COMPARE_CARD)
            self:SetCompareCardLabel(false, nil)
            self:SetWaitUserChoice(G_CONSTANTS.INVALID_CHAIR)

            self:EnableAllButton(true)
            self:showAllIn()
            self:UpdataControl()
        end
    end

    --状态设置
    ZhaJinHuaDataMgr:getInstance():setGameStatus(GAME_STATUS_FREE)
    ZhaJinHuaDataMgr:getInstance().canPlay = false

    self:EnableAllButton(false)
    self:showAllInButton(false)
    self:showCheckButton(false)

    --删除时间
    if ZhaJinHuaDataMgr:getInstance().m_wCurrentUser == wMeChairID then
        self:KillGameClock(IDI_USER_ADD_SCORE)
        self:showJzLayer(false)
    end

    --比牌提示
    self:SetWaitUserChoice(G_CONSTANTS.INVALID_CHAIR)
    --设置箭头
    self:SetCompareCardLabel(false, nil)

    self.m_bClientAbandon = true
end

function GoldenFlowerLayer:dealChipNumber(chip_value)
    local num_str = ""
    if chip_value >= 10000 then
        num_str = chip_value/10000 .. "万"
    elseif chip_value >= 1000 then
        num_str = chip_value/1000 .. "千"
    else
        num_str = chip_value
    end
    return num_str
end

function GoldenFlowerLayer:tryFixIphoneX()
    if LuaUtils.isIphoneXDesignResolution() then
        self.m_pClippingMenu:setPositionX(self.m_pClippingMenu:getPositionX() + 80)
        self.m_pButton_pop:getLayoutParameter():setMargin({left = self.m_pButton_pop:getPositionX() +45,top = 10})
        self.m_pButton_push:getLayoutParameter():setMargin({left = self.m_pButton_push:getPositionX() +45,top = 10})
        self.m_pButton_Return:getLayoutParameter():setMargin({left = self.m_pButton_Return:getPositionX() -120,top = 10})


        self.m_newChatLayer:setLeftBtnOffsetX(-80)
        self.m_newChatLayer:setNodeAllOffset(-80,0)
        self.m_newChatLayer:setLeftBtnMsgOffsetY(100)
        self.m_newChatLayer:setLeftBtnPlayerOffsetY(-100)
    end
end

function GameTableLayer:showBaseNote( note)

--    stringstream buff;
--    buff << "底注:" << note; 
     self.m_pLabel_unit_bet:setString(note)
     
end

function GameTableLayer:showLimitPerNote( note)

--    stringstream buff;
--    buff << "顶注:" << note; 
    self.m_pLabel_uplimit_bet:setString(note)
end
 


function GameTableLayer:showTotalNote( note)
 
	self.m_pLabel_current_total_bet:setString(Money) 
end
return GoldenFlowerLayer

--endregion
