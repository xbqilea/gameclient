--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local TipLayer = class("TipLayer", FixLayer)

local ZhaJinHuaDataMgr = require("game.goldenflower.manager.ZhaJinHuaDataMgr")

function TipLayer.create()
    return TipLayer.new():init()
end

function TipLayer:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()
end

function TipLayer:init()
    self.m_pathUI = ccs.GUIReader:getInstance():widgetFromJsonFile("game/goldenflower/tip.json")
    self:addChild(self.m_pathUI)
    local gbBtn = ccui.Helper:seekWidgetByName(self.m_pathUI, "Button_gb")
    gbBtn:addClickEventListener(handler(self,self.onGBClicked))

    local qxBtn = ccui.Helper:seekWidgetByName(self.m_pathUI, "Button_qx")
    qxBtn:addClickEventListener(handler(self,self.onGBClicked))

    local qdBtn = ccui.Helper:seekWidgetByName(self.m_pathUI, "Button_qd")
    qdBtn:addClickEventListener(function()
        ZhaJinHuaDataMgr:getInstance().canPlay = false            
        --self.goldenFlowerLayer:onReturnClicked()
        SLFacade:dispatchCustomEvent(Public_Events.MSG_GAME_EXIT) --立即退出
    end)

    return self
end

function TipLayer:setCallBack(obj)
    self.goldenFlowerLayer = obj
end

function TipLayer:onGBClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    self:removeFromParent()
end

return TipLayer

--endregion
