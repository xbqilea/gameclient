--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
--local UserInfoLayer = require("game.goldenflower.layer.UserInfoLayer")
local HNLayer= require("src.app.newHall.HNLayer") 
local CardSprite = import(".CardSprite")
local CountDownNode = import(".CountDownNode")
local CommonPlayerLayer = class("CommonPlayerLayer", function()
    return HNLayer.new()
end)

local P_WIDTH = 1334
local P_WIDTH_2 = P_WIDTH/2

local P_HEIGHT = 750
local P_HEIGHT_2 = P_HEIGHT/2

function CommonPlayerLayer.create(uiFile)
   return  CommonPlayerLayer.new():init(uiFile)
end

function CommonPlayerLayer:ctor()
--    self.super:ctor(self)
--    self:enableNodeEvents()

    self.m_ChairID = INVALID_CHAIR
	self.m_UserScore = 0
    self.m_nCountDownTime = 0
	self.m_pOwner = nil
    self.isReady = false
    self.m_pSpCardIcon = {}
    self.m_AttachNode = nil
    self.m_bCompareUser = false
    self.m_pactionEffect = nil
    self.curGender = 0                  --0女1男
    self.m_Card = {}
    self.m_ShowCard = {}
end

function CommonPlayerLayer:init(uiFile)
--    print("uiFile ".. uiFile)
	self.m_pathUI = ccs.GUIReader:getInstance():widgetFromJsonFile(uiFile)
        
    local size = self.m_pathUI:getContentSize()    
	self.m_pathUI:setTag(0)
	self.m_pathUI:setAnchorPoint(cc.p(0.5, 0.5))

    self.m_Image_21 = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_21")
    self.m_Image_32 = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_32")
    local panelInfo = ccui.Helper:seekWidgetByName(self.m_pathUI, "Panel_info")    
	local m_Label_clock = ccui.Helper:seekWidgetByName(self.m_pathUI, "AtlasLabel_clock")
	self.m_Button_head = ccui.Helper:seekWidgetByName(self.m_pathUI, "Button_head")
    self.m_pHead = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_26")
--    self.m_Button_head:addTouchEventListener(function(sender, touchType)
--        if ccui.TouchEventType.began == touchType then
--            if panelInfo:getChildByTag(self.m_ChairID) == nil then    

--                AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

--                if self.m_pOwner.tipsHolder:getChildByTag((111111 + self.m_ChairID)) then
--                    return
--                end     

--                local ctrId = self.m_pOwner:GetPlayerControlID(self.m_ChairID)
--                local pos = cc.p(panelInfo:getPosition())
--                if ctrId == 1 or ctrId == 3 then
--                    pos.y = 30
--                end
--                pos = cc.pAdd(cc.p(self.m_AttachNode:getPosition()), pos)

--                local userId = CUserManager.getInstance():getUserIDByChairID2(PlayerInfo.getInstance():getTableID(),self.m_ChairID)
--                local userInfoLayer = UserInfoLayer.create(userId)
--                if not userInfoLayer then return end
--                userInfoLayer:setTag((111111 + self.m_ChairID))
--                userInfoLayer:setPosition(pos)
--                self.m_pOwner.tipsHolder:addChild(userInfoLayer)
--            end
--        end
--    end)
	self.m_AtlasLabel_bet = ccui.Helper:seekWidgetByName(self.m_pathUI, "BitmapLabel_21")
    self.m_AtlasLabel_bet:setString("0")
        
	self.m_Image_kanpai_action = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_kanpai_action")
    self.m_Image_kanpai_action:setVisible(false)
	self.m_Label_name = ccui.Helper:seekWidgetByName(self.m_pathUI, "Label_name")

    self.m_Label_name:setAnchorPoint(0, 0.5)
    self.m_Label_name:setPosition(0, 178)

--    self.m_Image_vip = ccui.ImageView:create("hall/plist/vip/img-vip0.png", ccui.TextureResType.plistType)
--    self.m_Image_vip:setAnchorPoint(1, 0.5)
--    self.m_Image_vip:setPosition(55, 178)
--    self.m_Image_vip:addTo(self.m_Label_name:getParent(), self.m_Label_name:getLocalZOrder())

	self.m_Label_score = ccui.Helper:seekWidgetByName(self.m_pathUI, "Label_score")
    self.m_Image_guang = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_30")
    self.m_Image_guang:setVisible(false)

	self.m_Image_card = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_card")
    
	self.m_Image_ready = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_ready")
    self.ddwjAnimation = Effect:getInstance():creatEffectWithDelegate2(self.m_Image_ready, "wait-player", "Animation1",true,cc.p(0, 0))
    self.ddwjAnimation:setName("wait-player")
    self.m_panle_waitXJ = ccui.Helper:seekWidgetByName(self.m_pathUI, "Panel_waitxj")
    self.ddxjAnimation =Effect:getInstance():creatEffectWithDelegate2(self.m_panle_waitXJ, "dengdai_1", "animation1",true,cc.p(0, 0))
    self.ddxjAnimation:setName("dengdai_1")
    self.panelY = ccui.Helper:seekWidgetByName(self.m_pathUI, "Panel_y")
	self.m_TxtWinScoreY = ccui.Helper:seekWidgetByName(self.panelY, "BitmapLabel_WinScore")
    self.panelS = ccui.Helper:seekWidgetByName(self.m_pathUI, "Panel_s")
	self.m_TxtWinScoreS = ccui.Helper:seekWidgetByName(self.panelS, "BitmapLabel_WinScore")
	self.m_PosWinScoreStart = cc.p(self.panelY:getPosition())

    self.m_WinFlag = ccui.Helper:seekWidgetByName(self.m_pathUI, "Panel_slsb")

	self.m_Image_Select = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_select")
	self.m_Image_Select:setVisible(false)
    self.size_select = self.m_Image_Select:getContentSize()
    self.m_Image_Select:addTouchEventListener(function(sender, touchType)
        if ccui.TouchEventType.began == touchType then
            self:OnCompareCardSelected()
        end
    end)

	self.m_Image_Banker = ccui.Helper:seekWidgetByName(self.m_pathUI, "Image_Banker")
	self.m_Image_Banker:setVisible(false)

    
    --yung add countdown
    self.cdn = CountDownNode.new()
    self.m_pathUI:addChild(self.cdn,10)
    self.cdn:setName("countDownNode")
    self.cdn:hideCountDown()
    
    self.panel_xz = ccui.Helper:seekWidgetByName(self.m_pathUI, "Panel_xz")
    self.panel_xz:setVisible(false)
    --self.panel_xz  = panel_xz:clone()
    --self:addChild()
    self.panel_qx = ccui.Helper:seekWidgetByName(self.m_pathUI, "Panel_qx")
    self.panel_qx:setVisible(false)

    --增加赢家动画
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("game/goldenflower/zjh/effect/playerglow/playerglow.ExportJson")
    self.m_pArmatureBlink = ccs.Armature:create("playerglow")
    self.m_pArmatureBlink:setPosition(cc.p(70, 100))
    self.m_pArmatureBlink:setAnchorPoint(cc.p(0.5, 0.5))
    self.m_pArmatureBlink:addTo(self.m_pathUI, 99)
    self.m_pArmatureBlink:setVisible(false)
    local animationEvent = function(armatureBack, movementType, movementID)
        if movementType == ccs.MovementEventType.complete 
        or movementType == ccs.MovementEventType.loopComplete
        then
            self.m_pArmatureBlink:setVisible(false)
        end
    end
    self.m_pArmatureBlink:getAnimation():setMovementEventCallFunc(animationEvent)

    return self
end

function CommonPlayerLayer:onEnter()
--    print("CommonPlayerLayer onEnter")
    self.super:onEnter()
end

function CommonPlayerLayer:onExit()
--    print("CommonPlayerLayer onExit")
    self.super:onExit()
end

function CommonPlayerLayer:AttachTo(node)
	self.m_AttachNode = node
	self.m_AttachNode:addChild(self.m_pathUI)
	self.m_AttachNode:setContentSize(self.m_pathUI:getContentSize())
    local sz = self.m_AttachNode:getContentSize()
	self.m_pathUI:setPosition(cc.p(sz.width / 2, sz.height / 2))
end

function CommonPlayerLayer:setIsPrepare(v)
    self.isReady = true
	self.m_Image_ready:setVisible(v)
end

function CommonPlayerLayer:setShowGoldKuang(v)

end

function CommonPlayerLayer:setBuyMoney(v)

end

function CommonPlayerLayer:setIsZhuang(v)

end

function CommonPlayerLayer:setNickName(name)
    local strName = tostring(name)
    local strTemp = LuaUtils.getDisplayNickName(strName, 6, true)
	self.m_Label_name:setString(strTemp)
end

function CommonPlayerLayer:setUserVip(vipLv)

    local vipPath = string.format("hall/plist/vip/img-vip%d.png", vipLv)
    self.m_Image_vip:loadTexture(vipPath, ccui.TextureResType.plistType)
    self.m_Image_vip:setVisible(true)
end

function CommonPlayerLayer:SetScore(v)    
	self.m_UserScore = v    
--    local strScore = LuaUtils.getGoldNumber(v)
    local strScore = LuaUtils.getGoldNumberZH(v)
	self.m_Label_score:setString(strScore)
end

function CommonPlayerLayer:GetScore()
	return self.m_UserScore
end

function CommonPlayerLayer:setBet(v)
--    print("--1212---CommonPlayerLayer:setBet(v)")
	self.m_AtlasLabel_bet:setString(v)
end

function CommonPlayerLayer:setHeadKuang(v)

end

function CommonPlayerLayer:setHead(ID)
--    print("----CommonPlayerLayer:setHead(--== "..fileName)
	--self.m_Button_head:loadTextures(fileName, fileName, "", ccui.TextureResType.localType)
    local fileName = ToolKit:getHead(ID)
    self.m_pHead:loadTexture(fileName,1)
    self.m_pHead:setScale(0.7)
end

function CommonPlayerLayer:setFrame(fileName)
--    local frameNode = self.m_pHead:getChildByName("frame")
--    if frameNode == nil then
--        frameNode = ccui.ImageView:create(fileName, ccui.TextureResType.plistType)
--        frameNode:setName("frame")
--        frameNode:setAnchorPoint(0.5, 0.5)
--        frameNode:setPosition(self.m_pHead:getContentSize().width / 2, self.m_pHead:getContentSize().height / 2)
--        frameNode:setScale(0.90)
--        frameNode:addTo(self.m_pHead)
--    else
--        frameNode:loadTexture(fileName, ccui.TextureResType.plistType)
--    end
end

function CommonPlayerLayer:setShowHead(v)
    if v == true then
--        print("----CommonPlayerLayer:setShowHead(-----v == true")
    else
--        print("----CommonPlayerLayer:setShowHead(----v == false")
    end
    self.m_Button_head:setVisible(v)
--    print(" CommonPlayerLayer:setShowHead ")
end

function CommonPlayerLayer:clearUser(v)
	self.m_Label_name:setString("")
    self.m_Image_vip:setVisible(false)
	self.m_Label_score:setString("0")
	--self.m_Label_state:setString("offline")
--    print("----CommonPlayerLayer:clearUser(")
	self.m_Button_head:setVisible(false)
    self.m_AtlasLabel_bet:setString("0") 
    --self.m_Kanpai_action_Type:setVisible(false)
    --self.sprite_tips:setVisible(false)
    self.isReady = false
end

function CommonPlayerLayer:SetCompareCard(bCompare)
	self.m_bCompareUser = bCompare
    self.m_Image_Select:setVisible(self.m_bCompareUser)
    if bCompare then
        self.kbpAnimation = Effect:getInstance():creatEffectWithDelegate2(self.m_Image_Select, "kebipai_1", "animation1",true,cc.p(self.size_select.width/2,self.size_select.height/2))
        self.jtAnimation = Effect:getInstance():creatEffectWithDelegate2(self.m_Image_Select, "kebipai_1", "animation1_copy1",true,cc.p(self.size_select.width/2,self.size_select.height/2))
        if not self.jtAnimation then return end
        local ctrId = self.m_ChairID
        if ctrId == 1 or ctrId == 2 then
            self.jtAnimation:setRotation(180)
            self.jtAnimation:setPosition(cc.p(self.size_select.width/2 +124,self.size_select.height/2+57))
        elseif ctrId == 3 or ctrId == 4 then          
            self.jtAnimation:setPosition(cc.p(self.size_select.width/2 -124,self.size_select.height/2+57))
        end
    else
        self.m_Image_Select:stopAllActions()
        self.m_Image_Select:removeAllChildren()
    end
end

function CommonPlayerLayer:OnCompareCardSelected()
    if self.m_pOwner then
        self.m_pOwner:OnCompareSelected(self)
        self.m_bCompareUser = false
        self.cdn:hideCountDown()
        --cc.Director:getInstance():getScheduler():pauseTarget(self)
        self.m_Image_Select:setVisible(self.m_bCompareUser)
        if (not self.m_bCompareUser) then
            for i = 1, 5 do
                local obj = self.m_pOwner:GetPlayerUI(i-1)
                obj.m_Image_Select:stopAllActions()
                obj.m_Image_Select:removeAllChildren()
            end
        end
    end
end

function CommonPlayerLayer:SetOwner(owner)
	self.m_pOwner = owner
end

function CommonPlayerLayer:LookCardMessage(v, count,chairId)
    self.m_pOwner.cardHolder:setLocalZOrder(0)
    local ctrId = self.m_pOwner:GetPlayerControlID(chairId)
    local y = self.m_Image_kanpai_action:getPositionY()
    if ctrId == 0 or ctrId == 4 then
        y = y - 110
    end
    local pos = cc.p(self.m_Image_kanpai_action:getPositionX(), y)
    if v == 0 then  --看牌
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.m_pathUI, "paopaokuang_zhajinhua", "Animation5",true,pos, 10)
        self.m_pOwner:SetLookCard(false,chairId)   --GoldenFlowerLayer:getInstance():SetLookCard(false)
        self:playerLookCardSound()
    elseif v == 1 then  --跟注
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.m_pathUI, "paopaokuang_zhajinhua", "Animation1",true,pos, 10)        
        if not ZhaJinHuaDataMgr:getInstance().isAllin then
            self:playerFollowSound()  
        else
            self:playerAllInSound()
        end
    elseif v == 2 then  --加注   
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.m_pathUI, "paopaokuang_zhajinhua", "Animation4",true,pos, 10)  
        if not ZhaJinHuaDataMgr:getInstance().isAllin then     
            self:playerAddSound()
        else
            self:playerAllInSound()
        end
    elseif v == 3 then  --弃牌
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.m_pathUI, "paopaokuang_zhajinhua", "Animation7",true,pos, 10)
        self:playerGiveUpSound()
    elseif v == 4 then  --下注
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.m_pathUI, "paopaokuang_zhajinhua", "Animation9", true, pos, 10)
        self:playBetSound()
    elseif v == 5 then -- 亮牌
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.m_pathUI, "paopaokuang_zhajinhua", "Animation6", true, pos, 10)
    elseif v == 6 then -- 全下
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.m_pathUI, "paopaokuang_zhajinhua", "Animation8", true, pos, 10)
        self:playerAllInSound()
    elseif v == 7 then -- 跟到底
        self.m_pactionEffect = Effect:getInstance():creatEffectWithDelegate2(self.m_pathUI, "paopaokuang_zhajinhua", "Animation3", true, pos, 10)
    elseif v == 8 then
        
    end
end
 
function CommonPlayerLayer:playBetSound()
    if self.curGender == 1 then
        AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/sound_woman/Call_02.mp3")
    else
        AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/sound_man/Call_02.mp3")
    end
end

function CommonPlayerLayer:playerFollowSound()
    local random = math.random(1,3)
    if self.curGender == 1 then
        AudioManager.getInstance():playSound(string.format("game/goldenflower/zjh/sound/sound_woman/Call_0%d.mp3",random))
    else
        AudioManager.getInstance():playSound(string.format("game/goldenflower/zjh/sound/sound_man/Call_0%d.mp3",random))
    end
end

function CommonPlayerLayer:playerAddSound()
    local random = math.random(1,4)
    if self.curGender == 1 then
        AudioManager.getInstance():playSound(string.format("game/goldenflower/zjh/sound/sound_woman/Add_0%d.mp3",random))
    else
        AudioManager.getInstance():playSound(string.format("game/goldenflower/zjh/sound/sound_man/Add_0%d.mp3",random))
    end
end

function CommonPlayerLayer:playerAllInSound()
    if self.curGender == 1 then
        AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/sound_woman/showhand.mp3")
    else
        AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/sound_man/showhand.mp3")
    end
end

function CommonPlayerLayer:playerPKSound()
    if self.curGender == 1 then
        AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/sound_woman/PK_01.mp3")
    else
        AudioManager.getInstance():playSound("game/goldenflower/zjh/sound/sound_man/PK_01.mp3")
    end
end

function CommonPlayerLayer:playerGiveUpSound()
    local random = math.random(1,5)
    if self.curGender == 1 then
        AudioManager.getInstance():playSound(string.format("game/goldenflower/zjh/sound/sound_woman/Quit_0%d.mp3",random))
    else
        AudioManager.getInstance():playSound(string.format("game/goldenflower/zjh/sound/sound_man/Quit_0%d.mp3",random))
    end
end

function CommonPlayerLayer:playerLookCardSound()
    local random = math.random(1,3)
    if self.curGender == 1 then
        AudioManager.getInstance():playSound(string.format("game/goldenflower/zjh/sound/sound_woman/Look_0%d.mp3",random))
    else
        AudioManager.getInstance():playSound(string.format("game/goldenflower/zjh/sound/sound_man/Look_0%d.mp3",random))
    end
end

function CommonPlayerLayer:SetGunder(v)
    self.curGender = v
end

function CommonPlayerLayer:SetCardColor(v)
    self.m_pOwner:SetCardColor(v)
end

function CommonPlayerLayer:SetLookCard(v,chairId)
    self.m_pOwner:SetLookCard(v,chairId)
end

function CommonPlayerLayer:SetDisplayHead(v)
    self.m_pOwner:SetDisplayHead(v)
end

function CommonPlayerLayer:SetCardData(data, count,chairId)
--    --print("-------------CommonPlayerLayer:SetCardData(data, count)---------------")
    self.m_pOwner:SetCardData(data, count,chairId)
end

function CommonPlayerLayer:SetCompareBack(v,chairId)
    self.m_pOwner:SetCompareBack(v,chairId)
end

function CommonPlayerLayer:SetVisible(v,chairId)
    self.m_pOwner:SetVisible(v,chairId)
end

function CommonPlayerLayer:showWinerBlink()
    self.m_pArmatureBlink:setVisible(true)
    self.m_pArmatureBlink:getAnimation():playWithIndex(0, -1, 0)
end

function CommonPlayerLayer:loadUser(info)
 --   self:setUserId(info.m_accountId);
    self:setHead(info.m_faceId);
	--self:setHeadFrame(info.m_frameId);
--	self:setVip(info.m_vipLevel);  
    self.m_Label_name:setString(info.m_nickname);
	self:setUserMoney(info.m_score);
	self._curMoney = info.m_score*0.01;
end
function CommonPlayerLayer:setUserMoney( money) 
	local fMoney = money*0.01;
    self._curMoney = fMoney
	self.m_Label_score:setString( fMoney);
end	
return CommonPlayerLayer
--endregion
