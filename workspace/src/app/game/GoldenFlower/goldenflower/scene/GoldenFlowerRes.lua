
local GoldenFlowerRes = --所有资源
{
    vecReleaseAnim = {  -- 退出时需要释放的动画资源
	    "game/goldenflower/zjh/effect/kapianshiyong_zhajinhua/kapianshiyong_zhajinhua.ExportJson",
        "game/goldenflower/zjh/effect/jiesuan_jiemian/jiesuan_jiemian.ExportJson",
	    "game/goldenflower/zjh/effect/pk_bipai/pk_bipai.ExportJson",
	    "game/goldenflower/zjh/effect/paopaokuang_zhajinhua/paopaokuang_zhajinhua.ExportJson",
        "game/goldenflower/zjh/effect/dengdai_1/dengdai_1.ExportJson",
        "game/goldenflower/zjh/effect/dengdaikaishi_1/dengdaikaishi_1.ExportJson",
        "game/goldenflower/zjh/effect/wait-player/wait-player.ExportJson",
        "game/goldenflower/zjh/effect/allin_qian/allin_qian.ExportJson",
        -- "game/goldenflower/zjh/effect/allin_you/allin_you.ExportJson",
        -- "game/goldenflower/zjh/effect/allin_zuo/allin_zuo.ExportJson",
        "game/goldenflower/zjh/effect/allin_you2/allin_you2.ExportJson",
        "game/goldenflower/zjh/effect/xiazhu_danshou/xiazhu_danshou.ExportJson",
        "game/goldenflower/zjh/effect/xiazhu_qian/xiazhu_qian.ExportJson",
        "game/goldenflower/zjh/effect/qipai/qipai.ExportJson",
        "game/goldenflower/zjh/effect/kebipai_1/kebipai_1.ExportJson",
        "game/goldenflower/zjh/effect/heguan_1/heguan_1.ExportJson",
        "game/goldenflower/zjh/effect/playerglow/playerglow.ExportJson",
    },

    vecReleasePlist = {
        
    },

    vecReleaseImg = {
       "game/goldenflower/zjh/CARD1.png",
    },

    vecReleaseSound = {
        "game/goldenflower/zjh/fapai.mp3",
        "game/goldenflower/zjh/gamesolo.mp3",
        "game/goldenflower/zjh/game_lose.mp3",
        "game/goldenflower/zjh/game_win.mp3",
        "game/goldenflower/zjh/heguan-fapai.mp3",
        "game/goldenflower/zjh/heguan-ready.mp3",
        "game/goldenflower/zjh/jetton-showhand.mp3",
        "game/goldenflower/zjh/jetton-single.mp3",
        "game/goldenflower/zjh/jetton_recyle.mp3",
        "game/goldenflower/zjh/lottery_xz_start.mp3",

        "game/goldenflower/zjh/sound_man/Add_01.mp3",
        "game/goldenflower/zjh/sound_man/Add_02.mp3",
        "game/goldenflower/zjh/sound_man/Add_03.mp3",
        "game/goldenflower/zjh/sound_man/Add_04.mp3",
        "game/goldenflower/zjh/sound_man/Call_01.mp3",
        "game/goldenflower/zjh/sound_man/Call_02.mp3",
        "game/goldenflower/zjh/sound_man/Call_03.mp3",
        "game/goldenflower/zjh/sound_man/Look_01.mp3",
        "game/goldenflower/zjh/sound_man/Look_02.mp3",
        "game/goldenflower/zjh/sound_man/Look_03.mp3",
        "game/goldenflower/zjh/sound_man/PK_01.mp3",
        "game/goldenflower/zjh/sound_man/Quit_01.mp3",
        "game/goldenflower/zjh/sound_man/Quit_02.mp3",
        "game/goldenflower/zjh/sound_man/Quit_03.mp3",
        "game/goldenflower/zjh/sound_man/Quit_04.mp3",
        "game/goldenflower/zjh/sound_man/Quit_05.mp3",
        "game/goldenflower/zjh/sound_man/showhand.mp3",

        "game/goldenflower/zjh/sound_woman/Add_01.mp3",
        "game/goldenflower/zjh/sound_woman/Add_02.mp3",
        "game/goldenflower/zjh/sound_woman/Add_03.mp3",
        "game/goldenflower/zjh/sound_woman/Add_04.mp3",
        "game/goldenflower/zjh/sound_woman/Call_01.mp3",
        "game/goldenflower/zjh/sound_woman/Call_02.mp3",
        "game/goldenflower/zjh/sound_woman/Call_03.mp3",
        "game/goldenflower/zjh/sound_woman/Look_01.mp3",
        "game/goldenflower/zjh/sound_woman/Look_02.mp3",
        "game/goldenflower/zjh/sound_woman/Look_03.mp3",
        "game/goldenflower/zjh/sound_woman/PK_01.mp3",
        "game/goldenflower/zjh/sound_woman/Quit_01.mp3",
        "game/goldenflower/zjh/sound_woman/Quit_02.mp3",
        "game/goldenflower/zjh/sound_woman/Quit_03.mp3",
        "game/goldenflower/zjh/sound_woman/Quit_04.mp3",
        "game/goldenflower/zjh/sound_woman/Quit_05.mp3",
        "game/goldenflower/zjh/sound_woman/showhand.mp3",
    },

    Chip_Font_Res = {
        [1] = "game/goldenflower/room-font/cm_y.fnt",
        [2] = "game/goldenflower/room-font/cm_lv.fnt",
        [5] = "game/goldenflower/room-font/cm_chips4.fnt",
--        [6] = "game/goldenflower/room-font/cm_lan.fnt",
--        [8] = "game/goldenflower/room-font/cm_hong.fnt",
        [10] = "game/goldenflower/room-font/cm_zi.fnt",
    },
}

return GoldenFlowerRes
--endregion
