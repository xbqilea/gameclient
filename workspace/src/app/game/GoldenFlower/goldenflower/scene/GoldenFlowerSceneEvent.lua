--region *.lua
--Date 2017-4-4
--Author zhangenzhi
--此文件由[BabeLua]插件自动生成

local prefixFlag = "GoldenFlowerScene_Event_"

local GoldenFlowerScene_Events =
{
    Main_Entry          = prefixFlag .. "mainEntry",    --进入斗地主游戏场景
    Exit_Entry          = prefixFlag .. "exitEnter",    --离开

    Main_Layer          = prefixFlag .. "mainLayer",    --进入主界面

    Socket_Disconnect  = prefixFlag .. "SocketDisconnect", --游戏掉线
    Lord_Socket_ReconSuc    = prefixFlag .. "SocketReconSuc",   --重连成功
    Lord_Socket_NoNetWork   = prefixFlag .. "SocketNoNetWork",  --重连失败
    Close_Self              = prefixFlag .. "Close_Self",

    MSG_ZHAJINHUA_INIT                  = prefixFlag .. "initData",         --载入场景数据
    MSG_ZHAJINHUA_START                 = prefixFlag .. "game_start",       --游戏开始
    MSG_ZHAJINHUA_JETTON                = prefixFlag .. "game_jetton",      --用户下注
    MSG_ZHAJINHUA_LOOK                  = prefixFlag .. "game_look",        --看牌操作
    MSG_ZHAJINHUA_COMPARE               = prefixFlag .. "game_compare",     --比牌操作
    MSG_ZHAJINHUA_REVOKECOM             = prefixFlag .. "game_revokecom",   --比牌操作
    MSG_ZHAJINHUA_OPEN                  = prefixFlag .. "game_open",        --开牌操作
    MSG_ZHAJINHUA_ABANDON               = prefixFlag .. "game_abandon",     --用户放弃
    MSG_ZHAJINHUA_GAMEEND               = prefixFlag .. "game_end",         --游戏结束
    MSG_ZHAJINHUA_GOREADY               = prefixFlag .. "game_goready",     --游戏去准备
    MSG_ZHAJINHUA_SHOWCARD              = prefixFlag .. "game_showcard",    --玩家亮牌
    MSG_ZHAJINHUA_CURRENT               = prefixFlag .. "game_currentuser", --当前说话玩家
    SHOW_OTHER_CARD                     = prefixFlag .. "show_other_card", --看某个玩家的牌型
    MSG_USER_ENTER = "MSG_USER_ENTER",
    MSG_FISG_DEL_PLAYER = "MSG_FISG_DEL_PLAYER"
}

return GoldenFlowerScene_Events

--endregion
