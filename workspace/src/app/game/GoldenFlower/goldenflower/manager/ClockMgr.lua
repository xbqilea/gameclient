--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local TimerEngine = import(".TimerEngine")

local ClockMgr = class("ClockMgr")
local instance = nil

function ClockMgr.getInstance()
	if not instance then
		instance  = ClockMgr.new()
	end
	return instance 
end

function ClockMgr:ctor()
    self.m_wClockID	= nil					--时钟标识
    self.m_nElapseCount = nil				--时钟计数
    self.m_wClockChairID = nil				--时钟位置
    self.m_wUserClock = {}		            --用户时钟
    self.mTimerEngine = TimerEngine.getInstance()   --时钟引擎
end

function ClockMgr:clear()
	self.m_wClockID = 0					
    self.m_nElapseCount = 0				
    self.m_wClockChairID = 0		
    self.m_wUserClock = {}
    self.mTimerEngine:StopService()
end

return ClockMgr
--endregion
