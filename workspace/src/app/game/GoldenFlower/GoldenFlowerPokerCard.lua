--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion 
local PokerCard = class("HandCard",function()
    return display.newSprite()
end)
function PokerCard:ctor(cardValue)
    self:myInit()
    self:setupViews(cardValue)

end

function PokerCard:myInit()
	self._Value = nil
    self._ignoreAnchorPointForPosition = false;
end

function PokerCard:setupViews(cardValue) 
		-- out of range
	if (not(
		(cardValue >= 0x00 and cardValue <= 0x0D) or
		(cardValue >= 0x11 and cardValue <= 0x1D) or
		(cardValue >= 0x21 and cardValue <= 0x2D) or
		(cardValue >= 0x31 and cardValue <= 0x3D))) then
		
		return false;
	end
         
	self._Value = cardValue;
	local fileName = self:getCardTextureFileName(self._Value);
	if(self:setSpriteFrame(display.newSpriteFrame(fileName))) then
		return true;
	end
	return false;
end	
 

function PokerCard:setCardValue(cardValue) 
	if (cardValue<0x00 or cardValue>0x3E) then
		
		return;
	end
	local fileName = self:getCardTextureFileName(cardValue);	
	self:setSpriteFrame(display.newSpriteFrame(fileName));
	self._Value = cardValue;
    print("cardValue    "..cardValue)
end

function PokerCard:getCardValue()
	
	return self._Value;
end

function PokerCard:getCardTextureFileName( cardValue) 
	local filename=string.format( "0x%02X.png", cardValue);

	return filename;
end
function PokerCard:setGray(flag)  
    -- 默认vert
--    local sp = cc.Sprite:createWithTexture(self:getTexture());
--    local vertShaderByteArray = "\n"..  
--        "attribute vec4 a_position; \n" ..  
--        "attribute vec2 a_texCoord; \n" ..  
--        "attribute vec4 a_color; \n"..  
--        "#ifdef GL_ES  \n"..  
--        "varying lowp vec4 v_fragmentColor;\n"..  
--        "varying mediump vec2 v_texCoord;\n"..  
--        "#else                      \n" ..  
--        "varying vec4 v_fragmentColor; \n" ..  
--        "varying vec2 v_texCoord;  \n"..  
--        "#endif    \n"..  
--        "void main() \n"..  
--        "{\n" ..  
--        "gl_Position = CC_PMatrix * a_position; \n"..  
--        "v_fragmentColor = a_color;\n"..  
--        "v_texCoord = a_texCoord;\n"..  
--        "}"  

--    -- 置灰frag
--    local flagShaderByteArray = "#ifdef GL_ES \n" ..  
--        "precision mediump float; \n" ..  
--        "#endif \n" ..  
--        "varying vec4 v_fragmentColor; \n" ..  
--        "varying vec2 v_texCoord; \n" ..  
--        "void main(void) \n" ..  
--        "{ \n" ..  
--        "vec4 c = texture2D(CC_Texture0, v_texCoord); \n" ..  
--        "gl_FragColor.xyz = vec3(0.4*c.r + 0.4*c.g +0.4*c.b); \n"..  
--        "gl_FragColor.w = c.w; \n"..  
--        "}"  

--    -- 移除置灰frag  
--    local pszRemoveGrayShader = "#ifdef GL_ES \n" ..  
--        "precision mediump float; \n" ..  
--        "#endif \n" ..  
--        "varying vec4 v_fragmentColor; \n" ..  
--        "varying vec2 v_texCoord; \n" ..  
--        "void main(void) \n" ..  
--        "{ \n" ..  
--        "gl_FragColor = texture2D(CC_Texture0, v_texCoord); \n" ..  
--        "}" 
--    if flag then 
--        local glProgram = cc.GLProgram:createWithByteArrays(vertShaderByteArray,flagShaderByteArray)  
--        glProgram:bindAttribLocation(cc.ATTRIBUTE_NAME_POSITION,cc.VERTEX_ATTRIB_POSITION)  
--        glProgram:bindAttribLocation(cc.ATTRIBUTE_NAME_COLOR,cc.VERTEX_ATTRIB_COLOR)  
--        glProgram:bindAttribLocation(cc.ATTRIBUTE_NAME_TEX_COORD,cc.VERTEX_ATTRIB_FLAG_TEX_COORDS)  
--        glProgram:link()  
--        glProgram:updateUniforms()  
--        sp:setGLProgram(glProgram)  
--    else 
--        local glProgram = cc.GLProgram:createWithByteArrays(vertShaderByteArray,pszRemoveGrayShader)  
--        glProgram:bindAttribLocation(cc.ATTRIBUTE_NAME_POSITION,cc.VERTEX_ATTRIB_POSITION)  
--        glProgram:bindAttribLocation(cc.ATTRIBUTE_NAME_COLOR,cc.VERTEX_ATTRIB_COLOR)  
--        glProgram:bindAttribLocation(cc.ATTRIBUTE_NAME_TEX_COORD,cc.VERTEX_ATTRIB_FLAG_TEX_COORDS)  
--        glProgram:link()  
--        glProgram:updateUniforms()  
--        sp:setGLProgram(glProgram) 
--    end 
    if flag then
        self:addGray()
    else
        self:removeGray()
    end
end 
--function PokerCard:setGray(isGray) 
--	if (isGray == true) then

--		local sprite_chess = cc.Sprite:createWithTexture(self:getTexture());
--		sprite_chess:setPosition(sprite_chess:getContentSize().width / 2, sprite_chess:getContentSize().height / 2);

--		local render = cc.RenderTexture:create(sprite_chess:getContentSize().width, sprite_chess:getContentSize().height, cc.TEXTURE2_D_PIXEL_FORMAT_RGB_A8888);
--		render:beginWithClear(0.0, 0.0, 0.0, 0.0);
--		sprite_chess:visit();
--		render:endToLua();
--		cc.Director:getInstance():getRenderer():render();

--		local finalImage = render:newImage();

--		local pData = finalImage:getData();

--		local iIndex = 0;

--		for  i = 0, finalImage:getHeight()-1 do
--			for  j = 0, finalImage:getWidth()-1 do 
--				local iBPos = iIndex;

--				iB = pData[iIndex];
--				iIndex=iIndex+1;

--				local iG = pData[iIndex];
--				iIndex=iIndex+1;

--				local iR = pData[iIndex];
--				iIndex=iIndex+1;
--				iIndex=iIndex+1;

--				local iGray = 0.3 * iR + 0.6 * iG + 0.1 * iB;

--				pData[iBPos] = pData[iBPos + 1] 
--                    pData[iBPos + 1] = pData[iBPos + 2]
--                    pData[iBPos + 2] = iGray;
--			end
--		end

--		local texture = cc.Texture2D;
--		texture:initWithImage(finalImage);
--		self:setTexture(texture);

--		finalImage = nil;
--		texture:release();

--	else 
--		self:setSpriteFrame(self:getCardTextureFileName(self._Value));
--	end
--end



return PokerCard