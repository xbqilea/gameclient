--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
local CHIP_FILENAME1 = "Games/goldenflower/dashboard/chip_1.png";
local CHIP_FILENAME2 = "Games/goldenflower/dashboard/chip_3.png";
local CHIP_FILENAME3 = "Games/goldenflower/dashboard/chip_4.png";
local CHIP_FILENAME4 = "Games/goldenflower/dashboard/chip_4.png";
local  TOTAL_CARDS = 52; -- total cards
local eZCommunityCard =10 --公共牌
	 
	 
local eDealerNode =0
local eDealerStart=1
local eDealerFlop = 10
local eDealerTurn= 11
local eDealerRiver= 12
local eDealerEnd= 13
local  Scheduler              =  require("framework.scheduler")
require("framework.functions")
local PokerCard = import(".GoldenFlowerPokerCard")
local DrawEllipse = import(".GoldenFlowerDrawEllipse")
local GameChipUI = import(".GoldenFlowerGameChipUI")
local Global = import(".GoldenFlowerGlobal") 
local GameDelearUI =class("GameDelearUI",function()
    return display.newLayer()
end)

function GameDelearUI:ctor(table)
    self:myInit()
    self:setupViews(table)

end

function GameDelearUI:myInit()
	self._tableChip ={};                    --每个玩家桌上的筹码
	self._seatPolocals={};                 --每个玩家座位上的点
	self._BetFlyToPolocals={};             --最后筹码飞向赢家的点
	self._dealerPolocal=nil;                             --荷官的位置位置
	self._winPool={};               --玩家赢的结果
	self._handCard={};                       --手上有牌的玩家座位号
	self._curCount=nil;
    self._vBetNum={};  
	self._buttonStation=nil;                                    --庄家位置号 
	self.bover = false;
	self._cardSuite={};                              -- card suite
	self._ellipse=nil;                                  -- a ellipse area used for chips pool
	self._chipsPool=nil;                             -- chips pool for chip recycle
    self._table=nil;
    self._Scheduler1 = nil
    self._Scheduler2 = nil
end
function GameDelearUI:onExit()
    if self._Scheduler1 then
            Scheduler.unscheduleGlobal(self._Scheduler1)
            self._Scheduler1 = nil    
    end
    if self._Scheduler2 then
            Scheduler.unscheduleGlobal(self._Scheduler2)
            self._Scheduler2 = nil    
    end
end
function GameDelearUI:setupViews(tableUI)  
    self._table = tableUI;
	--self._tableChip.resize(Global.PLAY_COUNT);
 

	-- init card suite with back image, total 52 cards in a suite
	for i = 0,TOTAL_CARDS-1 do 
		self._cardSuite[i] = PokerCard.new(0x00);
		self:addChild(self._cardSuite[i], i);
		self._cardSuite[i]:setVisible(false);
	end	
		

	-- init the ellipse area chip pool
	local visibleSize =cc.Director:getInstance():getVisibleSize();
	local origin = cc.Director:getInstance():getVisibleOrigin();
	local e_origin = cc.p(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2);
	self._ellipse = DrawEllipse.new(e_origin, 150, 50);

	-- init the chip pool 
	self._chipsPool = display.newLayer()
	self._chipsPool:setPosition(origin);
	self:addChild(self._chipsPool);

end
function GameDelearUI:showABetMoney( betCount)
	
    --四种类型的筹码
    for betType = 1, Global.E_CHOUMA_COUNT-1 do
        
    --绘制没有加倍的筹码
        for betCount1 = 0, betCount[betType].m_closeNum-1 do  
			local Money = self._vBetNum[betType].m_value*0.01;
			self:writeBet(Money, false); 
        end
    --绘制要加倍的筹码
        for betCount2 = 0, betCount[betType].m_openNum-1 do  
			local Money = self._vBetNum[betType].m_value*0.01;
			self:writeBet(Money, true);
        end
    end
end

function GameDelearUI:betMoney( seatNo, money)
	
    if (not self:seatNoOk(seatNo) or #self._vBetNum==0) then
		
		return;
	end
	local DMoney = money*0.01;
    --四种类型的筹码
	local chip = nil;
    if (money>0 and money <= self._vBetNum[1].m_value) then
        
        chip = GameChipUI.new(CHIP_FILENAME1);
         
    elseif (money > self._vBetNum[1].m_value and money <=self._vBetNum[2].m_value) then
        
        chip = GameChipUI.new(CHIP_FILENAME2);
         
    elseif (money > self._vBetNum[2].m_value) then
        
        chip = GameChipUI.new(CHIP_FILENAME4);
    end
	chip:setValue(DMoney);
	chip:setTag(seatNo);
	chip:setPosition(self:getBetFlyToPolocal(seatNo));

	self._chipsPool:addChild(chip);
	local chipPolocal;
	local flag =true; 
	chipPolocal = self._ellipse:getEllipseOrigin(); 
    local num = -1
    if math.random(1,30000) % 2 ==0 then
        num =1
    end
    local x = self._ellipse:getEllipseX()
    local y = self._ellipse:getEllipseY()
    local randx = math.random(1,x)
        local randy = math.random(1,y)
	local xOffset = randx*num
	local yOffset = randy*num
    local pos ={}
	pos.x =chipPolocal.x+ xOffset;
	pos.y =chipPolocal.y+ yOffset;  
	chip:runAction(cc.MoveTo:create(0.1, pos));
--	 g_AudioPlayer:playEffect(GAME_XIAZHU);
end

--给玩家发手牌
function GameDelearUI:dealHandCard(handCard)
	
	self._table._SystemOpencard:setVisible(true);
	self._handCard = handCard;
	self._curCount = 0;
		
	self:dispatchHandCard();
end

function GameDelearUI:dispatchHandCard()
	
	--------------------------------------------------------------/
	-- radius
	local radius = 250;

	-- Total angle
	local alpha = 40;
	local y_adjustment = 230;

	local beta = (180 - alpha) / 2;

	-- angle value TO radians value
	local r_alpha = alpha * Global.M_PI / 180;									--所有牌的弧度
	local r_beta = beta * Global.M_PI / 180;									--第一张牌与水平轴的弧度
	local r_alphaSplit = r_alpha / (TOTAL_CARDS - 1);                    --每一张牌的弧度

	local delaycounter = 0;
	local dealerPosition = cc.p(self._dealerPolocal.x,self._dealerPolocal.y-80);
    
    --显示所有将要发的牌
	for  i = 0, TOTAL_CARDS -1 do
		
        delaycounter = delaycounter+1
		local x_offset = radius * math.cos(r_alphaSplit*i + r_beta);
		local y_offset = (-1)*radius * math.sin(r_alphaSplit*i + r_beta) + y_adjustment;
		local rotateAngle = (r_alphaSplit*i + r_beta) * 180 / math.pi - 90;

		self._cardSuite[i]:setPosition(cc.p(dealerPosition.x + x_offset, dealerPosition.y + y_offset));
		self._cardSuite[i]:runAction(cc.Sequence:create(
			cc.RotateBy:create(0.01, rotateAngle),
			cc.CallFunc:create(function()
			self._cardSuite[i]:setVisible(true);
			self._cardSuite[i]:setScale(0.86);
		end), nil));
	end
    if self._Scheduler1 == nil then
        self._Scheduler1 = Scheduler.scheduleGlobal(handler(self, self.showUserHandCard),0.3)	
    end
	----------------------------------------------------------------
	--schedule(schedule_selector(GameDelearUI:showUserHandCard), 0.3f, 3 * _handCard.size() - 1, 0);
end

function GameDelearUI:showUserHandCard( delta)
	
	local playercount = #self._handCard;

	self._curCount =self._curCount+1;
	local cardIndex = math.floor((self._curCount - 1) / playercount);
	local playerIndex = (self._curCount - 1) % playercount;

	local theTopIndexOfCardSuite = TOTAL_CARDS - self._curCount;
    local CardListBoard = self._table:getPlayerCardListBoard(self._handCard[playerIndex+1].bySeatNo);

	local move = cc.MoveTo:create(0.2, self:getSeatPolocal(self._handCard[playerIndex+1].bySeatNo));
	local ease = cc.EaseSineOut:create(move);
	local rotate = cc.RotateTo:create(0.2, 180);
	local spwn = cc.Spawn:create(ease, rotate);

	self._cardSuite[theTopIndexOfCardSuite]:runAction(cc.Sequence:create(
		spwn,
		cc.CallFunc:create(function()
			g_AudioPlayer:playEffect(GAME_FAPAI);
		    self._cardSuite[theTopIndexOfCardSuite]:setVisible(false);
 		    --显示手牌
            if (CardListBoard ~= nil) then
                CardListBoard:setHandCard(cardIndex,0);
 		    end 
	end),nil));

	if (self._curCount >= #self._handCard * 3) then
        if self._Scheduler2 == nil then
		  self._Scheduler2 =Scheduler.scheduleGlobal(handler(self, self.recycleCardSuite), 0.1)	
        end
         if self._Scheduler1 then
            Scheduler.unscheduleGlobal(self._Scheduler1)
            self._Scheduler1 = nil    
        end
		--回收发的牌
		--schedule(schedule_selector(GameDelearUI:recycleCardSuite), 0.1f, 0, 0.4f);
	end 

end

-- recycle card suite
function GameDelearUI:recycleCardSuite( delta)
	
	local delaycounter = 0;
	for i = TOTAL_CARDS - 1 - self._curCount,0,-1 do
		
		if (self._cardSuite[i]:isVisible() == true) then 
			self._cardSuite[i]:runAction(cc.Sequence:create(

				cc.CallFunc:create(function()
				self._cardSuite[i]:setVisible(false);
				self._cardSuite[i]:setRotation(0);
				self._table._SystemOpencard:setVisible(false);
			end), nil));
			
		else 
			self._cardSuite[i]:setRotation(0);
		end

	end
    if self._Scheduler2 then
        Scheduler.unscheduleGlobal(self._Scheduler2)
        self._Scheduler2 = nil    
    end
end

function GameDelearUI:recycleChips( winnerSeatNo)
	
	local winnerpos = self:getBetFlyToPolocal(winnerSeatNo);
	for   k,chip in pairs( self._chipsPool:getChildren()) do 
		local scaleBig = cc.ScaleTo:create(0.8, 1.5);
		local scaleSmall = cc.ScaleTo:create(0.4, 0);
		local move = cc.MoveTo:create(2.0, winnerpos);
		local spaw = cc.Spawn:create(scaleBig, move);
		chip:runAction(cc.Sequence:create(
			spaw,
			scaleSmall,
			cc.DelayTime:create(0.4),
			cc.CallFunc:create(function()
			chip:removeFromParent();
		end), nil));
		local orbit = cc.OrbitCamera:create(0.4, 1, 0, 0, 360, 0, 0);
		chip:runAction(cc.Repeat:create(orbit,2)); 
	end 
end

--给玩家派奖（先收筹码，再派奖）
function GameDelearUI:dealWinPool(winPool)
	
	self._winPool = winPool;
	g_AudioPlayer:playEffect(GAME_SHOUCHOUMA);

	self:runAction(cc.Sequence:create(
		cc.CallFunc:create(handler(self,self.collectChip)),
		cc.DelayTime:create(0.6),
		nil));
end

   

	  
function GameDelearUI:getSeatPolocal( seatNo)
	    
    local pos = self._seatPolocals[seatNo]
    if not self:seatNoOk(seatNo) then
        pos = cc.p(0,0)
    end
	return pos
end

function GameDelearUI:getBetFlyToPolocal( seatNo)
	local pos = self._BetFlyToPolocals[seatNo]
    if not self:seatNoOk(seatNo) then 
        pos = cc.p(0,0)
    end
	return pos
	 
end

function GameDelearUI:collectChip()
	
	for i = 0, Global.PLAY_COUNT-1 do 
		if (self._tableChip[i] ~= nil) then
			
			self._tableChip[i]:runAction(cc.Sequence:create(
				cc.EaseOut:create(cc.MoveTo:create(0.4, self._dealerPolocal), 2.5),
				cc.RemoveSelf:create(),
				nil));
			self._tableChip[i] = nil;
		end
	end
end

function GameDelearUI:setDealerPolocal( polocal)
	
	self._dealerPolocal = polocal;
	 
end

function GameDelearUI:setSeatPolocal(polocals)
	
	self._seatPolocals = polocals;
end

function GameDelearUI:setBetFlyToPolocal(polocals)
	
	self._BetFlyToPolocals = polocals;
end

function GameDelearUI:setButtonStation( buttonStation)
	
	self._buttonStation = buttonStation;
end

function GameDelearUI:seatNoOk( seatNo)
	
	return (seatNo >= 0 and seatNo <= Global.PLAY_COUNT);
end

function GameDelearUI:clear()  
end

function GameDelearUI:clearChouMa()
    
    self._chipsPool:removeAllChildren();
end

function GameDelearUI:saveFourBetNum(vBetNum)
     
    self._vBetNum = vBetNum;
end

function GameDelearUI:writeBet( money, isDouble)
        
    local betNum = money;
    if isDouble then
        betNum = money*2
    end
    --四种类型的筹码
    local chip = nil;
    if (betNum>0 and betNum <=self._vBetNum[1].m_value) then
        
        chip = GameChipUI.new(CHIP_FILENAME1);
        
    elseif (betNum>self._vBetNum[1].m_value and betNum<=self._vBetNum[2].m_value) then
        
        chip = GameChipUI.new(CHIP_FILENAME2);
   
    elseif (betNum>self._vBetNum[2].m_value) then
        
        chip = GameChipUI.new(CHIP_FILENAME4);
    end

    chip:setValue(betNum);
    self._chipsPool:addChild(chip);
    local chipPolocal;
    local flag =true;
       
   	chipPolocal = self._ellipse:getEllipseOrigin(); 
    local num = -1
    if math.random(1,30000) % 2 ==0 then
        num =1
    end
    local x = self._ellipse:getEllipseX()
    local y = self._ellipse:getEllipseY()
    local randx = math.random(1,x)
        local randy = math.random(1,y)
	local xOffset = randx*num
	local yOffset = randy*num
    local pos ={}
	pos.x =chipPolocal.x+ xOffset;
	pos.y =chipPolocal.y+ yOffset;  
	chip:runAction(cc.MoveTo:create(0.1, pos));
end

return GameDelearUI