--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--
-- GoldenFlowerGameScene
-- Author: chenzhanming
-- Date: 2018-10-24 10:00:00
-- 百家乐主场景
--
local scheduler           = require("framework.scheduler") 
local CCGameSceneBase     = require("src.app.game.common.main.CCGameSceneBase") 
local GoldenFlowerLayer        = import(".goldenflower.layer.GoldenFlowerGameTableLayer")  
local GoldenFlowerLoadingLayer = require("src.app.game.GoldenFlower.goldenflower.layer.GoldenFlowerLoadingLayer")  
local DlgAlert = require("app.hall.base.ui.MessageBox")

local GoldenFlowerGameScene= class("GoldenFlowerGameScene", function()
        return CCGameSceneBase.new()
    end)


function GoldenFlowerGameScene:ctor()  
    self.m_GoldenFlowerMainLayer         = nil 
    self.m_GoldenFlowerExitGameLayer     = nil
    self.m_GoldenFlowerRuleLayer         = nil
    self.m_GoldenFlowerMusicSetLayer     = nil 
    self.m_bIsInit =false
    self:myInit()
end

-- 游戏场景初始化
function GoldenFlowerGameScene:myInit() 
 -- self:loadResources()
  --GoldenFlowerRoomController:getInstance():setInGame( true )  
  -- 主ui
   self.m_GoldenFlowerLoadingLayer = GoldenFlowerLoadingLayer.new()
   self:addChild(self.m_GoldenFlowerLoadingLayer)
--self:initGoldenFlowerGameMainLayer()
  self:registBackClickHandler(handler(self, self.onBackButtonClicked)) -- Android & Windows注册返回按钮
  addMsgCallBack(self, MSG_ENTER_FOREGROUND, handler(self, self.onEnterForeground)) -- 转前台
  addMsgCallBack(self, MSG_ENTER_BACKGROUND, handler(self, self.onEnterBackground)) -- 转后台  
  --addMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT, handler(self, self.socketState))
  --addMsgCallBack(self, POPSCENE_ACK,handler(self,self.showEndGameTip))
  --addMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT, handler(self,self.onSocketEventMsgRecived))
  addMsgCallBack(self,UPDATE_GAME_RESOURCE, handler(self,self.updateCallback))
end
 function GoldenFlowerGameScene:updateCallback()
    
    self:initGoldenFlowerGameMainLayer()
 end
 function GoldenFlowerGameScene:initGoldenFloweGameMainLayer()
    
 end
---- 进入场景
function GoldenFlowerGameScene:onEnter()
   print("------------GoldenFlowerGameScene:onEnter begin------------")
   ToolKit:setGameFPS(1/60.0) 
   print("------------GoldenFlowerGameScene:onEnter end------------")
   
end

-- 退出场景
function GoldenFlowerGameScene:onExit()
	print("------------GoldenFlowerGameScene:onExit begin------------")
   
	if self.m_GoldenFlowerMainLayer then
		self.m_GoldenFlowerMainLayer:onExit()
		self.m_GoldenFlowerMainLayer = nil
	end
	
    --removeMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT)
    removeMsgCallBack(self, MSG_ENTER_FOREGROUND)
    removeMsgCallBack(self, MSG_ENTER_BACKGROUND)
    removeMsgCallBack(self,UPDATE_GAME_RESOURCE)
    --removeMsgCallBack(self, POPSCENE_ACK)
	--removeMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT)
	
	ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo("platform/biyingqipai/biyingqipai.ExportJson");
--    GoldenFlowerGlobal.m_isNeedReconectGameServer = false
--    GoldenFlowerRoomController:getInstance():setInGame( false )
--    GoldenFlowerGameController:getInstance():onDestory()
 --   self:RemoveResources()
 	print("------------GoldenFlowerGameScene:onExit end------------")
end

function GoldenFlowerGameScene:reqExitGame()
    g_GameController:reqUserLeftGameServer()
end

function GoldenFlowerGameScene:onBackButtonClicked()
    if (g_GameController:isGameing()) then
		TOAST("游戏中无法退出房间！")	
	else
		g_GameController:reqUserLeftGameServer()
	end
end

-- 初始化主ui
function GoldenFlowerGameScene:initGoldenFlowerGameMainLayer()
    self.m_GoldenFlowerMainLayer = GoldenFlowerLayer.new()
    self:addChild(self.m_GoldenFlowerMainLayer)
     self.m_bIsInit = true
     sendMsg("MSG_GAME_INIT")
end

function GoldenFlowerGameScene:getMainLayer()
    return self.m_GoldenFlowerMainLayer 
end

-- 显示游戏退出界面
-- @params msgName( string ) 消息名称
-- @params __info( table )   退出相关信息
-- 显示游戏退出界面
function GoldenFlowerGameScene:showExitGameLayer()
   if tolua.isnull( self.m_GoldenFlowerExitGameLayer )  then
      self.m_GoldenFlowerExitGameLayer = GoldenFlowerExitLayer.new( self )
      self:addChild( self.m_GoldenFlowerExitGameLayer ,GoldenFlowerGlobal.UILevel )
   else
      self.m_GoldenFlowerExitGameLayer:setVisible( true )
   end
   local params = { sureCallBackHander = handler( self, self.reqExitGame )  }
   self.m_GoldenFlowerExitGameLayer:rerfeshNormalView( params )
end
 

-- 从后台切换到前台
function GoldenFlowerGameScene:onEnterForeground()
	print("从后台切换到前台")
end

-- 从前台切换到后台
function GoldenFlowerGameScene:onEnterBackground()
	print("从前台切换到后台,游戏线程挂起!")
	g_GameController.m_BackGroudFlag = true
end

function GoldenFlowerGameScene:clearView()
   print("GoldenFlowerGameScene:clearView()")
end

-- 清理数据
function GoldenFlowerGameScene:clearData()
   
end

return GoldenFlowerGameScene
