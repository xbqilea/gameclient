local BirdConfig = require("src.app.game.bird.src.BirdConfig");

local BirdData = {}

function BirdData.getInstance()
    if g_GameController.__InstanceBirData == nil then
        g_GameController.__InstanceBirData = BirdData.new();
    end
    return g_GameController.__InstanceBirData;
end

function BirdData.releaseInstance()
    g_GameController.__InstanceBirData = nil;
end

function BirdData.new()
    local o = {}
    setmetatable(o, BirdData);
    BirdData.__index = BirdData;
    o:_InitAttr();
    return o;
end

function BirdData:onDestory()
    
end


function BirdData:getLeftTime()
    local fClock = os.clock();
    local t = fClock - self.m_fClockTime;
    if t < 0.5 then
        return self.m_nLeftTime;
    end
    return math.floor(self.m_nLeftTime - t + 0.3);
end

--是否有下注
function BirdData:isLastBet()
    for index = 1, #self.m_pMyLastBet do
        if self.m_pMyLastBet[index] > 0 then
            return true;
        end
    end
    return false;
end

--本局是否下了注
function BirdData:isBet()
    for index = 1, #self.m_pTmpLastBet do
        if self.m_pTmpLastBet[index] > 0 then
            return true;
        end
    end
    return false;
end




function BirdData:_InitAttr()
    self.m_nGameState = BirdConfig.STATE_FREE;  --游戏状态
    self.m_nLeftTime = 0; --本状态剩余时间
    self.m_nTotalTime = 0; --本状态总时间
    self.m_nCurCoin = 0; --当前金币
    -- { 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	-- { 2, 	1, 'm_nickname'			, 'STRING'				, 1 	, '昵称' },
	-- { 3, 	1, 'm_faceId'			, 'UINT'				, 1 	, '头像ID' },
    -- { 4, 	1, 'm_curCoin'			, 'UINT'				, 1 	, '金币' },
    self.m_pPlayerInfo = nil; --玩家信息
    self.m_nMinGameCoin = 0; --下注必须满足的最低拥有游戏金币
    self.m_nPlayerLimit = 0; --个人下注上限
    self.m_pAllPlayerTotalLimit = {}; --所有玩家总下注上限
    self.m_bIsLook = false; --是否旁观者
    self.m_nLastAwardType = 0; --上一次开奖类型
    self.m_nLastGridId = 0; --上一次跑灯停留格子id
    self.m_nAllTotalBet = 0; --所有区域、所有玩家总下注
    -- { 1,	1, 'm_betAreaId'		 ,  'UINT'				, 1		, '下注区域id, 1:大保时捷  2：大宝马  3:大奔驰  4:大大众 5:小保时捷 6:小宝马 7:小奔驰 8:小大众'},
	-- { 2,	1, 'm_multiple'		 	 ,  'UINT'				, 1		, '倍数'},
    self.m_pBetArea = nil; --初始化下注区域
    self.m_pAllChipValue = {1, 10, 50, 100, 500, 1000}; --初始化筹码
    self.m_nHistory = 0; --历史开奖记录
    self.m_pAreaTotalBet = {0,0,0,0,0,0,0,0,0,0,0}; --各区域总下注
    self.m_pMyAreaBet = {0,0,0,0,0,0,0,0,0,0,0}; --我在各区域的下注

    -- self.m_nAwardType = 0; --本次开奖类型
    -- self.m_nOpenGridId = 0; --本次开奖后跑灯停留格子id
    -- self.m_nAwardBetAreaId = 0; --本次中奖的下注区域id

    self.m_fClockTime = 0;
    self.m_pMyLastBet = {0,0,0,0,0,0,0,0,0,0,0};
    self.m_pTmpLastBet = {0,0,0,0,0,0,0,0,0,0,0};

    self.m_nGoldShark = 1;
    self.m_nSliverShark = 1;

    self.m_pPlayerList = {};
    self.m_pTopPlayerList = {};

    self.m_sRecordId = ""

end


function BirdData:SET_INIT_NTY(cmd)
    --初始化
    -- { 1,	1, 'm_state'		 	 	 ,  'UINT'						, 1		, '当前状态: 1-ready 2-下注 3-开奖'},
	-- { 2,	1, 'm_leftTime'		 	 	 ,  'UINT'						, 1		, '本状态剩余时间(单位秒)'},
	-- { 3,	1, 'm_totalTime'		 	 ,  'UINT'						, 1		, '本状态总时间(单位秒)'},
	-- { 4,	1, 'm_playerInfo'			 ,  'PstBmwInitPlayerInfo'		, 1		, '玩家基本信息'},
	-- { 5, 	1, 'm_minGameCoin'			 ,  'UINT'						, 1 	, '下注必须满足的最低拥有游戏金币' },
	-- { 6, 	1, 'm_playerLimit'			 ,  'UINT'						, 1 	, '个人下注上限' },
	-- { 7, 	1, 'm_allPlayerTotalLimit'	 ,  'UINT'						, 1 	, '所有玩家总下注上限' },
    -- { 8,	1, 'm_isLook'			 	 ,  'UINT'						, 1		, '是否旁观者, 0:否, 1:是'},
    -- { 9, 	1, 'm_goldShark'			 ,  'UINT'						, 1 	, '金鲨倍数' },
	-- { 10, 	1, 'm_sliverShark'			 ,  'UINT'						, 1 	, '银鲨倍数' },
	-- { 9,	1, 'm_lastAwardType'		 ,  'UINT'						, 1		, '上一次开奖类型(同CS_G2C_Bmw_OpenAward_Nty协议的m_awardType)'},
	-- { 10,	1, 'm_lastGridId'			 ,  'UINT'						, 1		, '上一次跑灯停留格子id'},
	-- { 11,	1, 'm_allTotalBet'   	 	 ,  'UINT'						, 1		, '所有区域、所有玩家总下注'},
	-- { 12, 	1, 'm_betArea'				 ,  'PstBmwBetAreaMultiple'		, 1024 	, '初始化下注区域' },
	-- { 13, 	1, 'm_chipArr'			 	 ,  'UINT'						, 1024 	, '初始化筹码' },
	-- { 14, 	1, 'm_history'			 	 ,  'UINT'						, 1024 	, '历史开奖记录(元素类型同CS_G2C_Bmw_OpenAward_Nty协议的m_awardType)' },
	-- { 15, 	1, 'm_areaTotalBet'			 ,  'PstBmwAreaTotalBet'		, 1024 	, '各区域总下注' },
    -- { 16, 	1, 'm_myAreaBet'		 	 ,  'PstBmwAreaMyBet'			, 1024 	, '我在各区域的下注' },

    self.m_nGameState = cmd.m_state;
    self.m_fClockTime = os.clock();
    self.m_nLeftTime = cmd.m_leftTime;
    self.m_nTotalTime = cmd.m_totalTime;
    self.m_pPlayerInfo = cmd.m_playerInfo;
    self.m_nCurCoin = cmd.m_playerInfo.m_curCoin;
    self.m_nMinGameCoin = cmd.m_miGameCoin;
    self.m_nPlayerLimit = cmd.m_playerLimit;
    self.m_pAllPlayerTotalLimit = cmd.m_allPlayerTotalLimit;
    self.m_bIsLook = cmd.m_isLook == 1;
    self.m_nLastAwardType = cmd.m_lastAwardType;
    self.m_nLastGridId = cmd.m_lastGridId;
    self.m_nAllTotalBet = cmd.m_allTotalBet;
    self.m_pBetArea = cmd.m_betArea;
    self.m_pAllChipValue = cmd.m_chipArr;
    self.m_nHistory = cmd.m_history;

    self.m_nGoldShark = cmd.m_goldShark;
    self.m_nSliverShark = cmd.m_sliverShark;
    self.m_sRecordId = cmd.m_recordId

    -- { 1,	1, 'm_betAreaId'		 ,  'UINT'				, 1		, '下注区域id, 1:大保时捷  2：大宝马  3:大奔驰  4:大大众 5:小保时捷 6:小宝马 7:小奔驰 8:小大众'},
	-- { 2,	1, 'm_totalBet'		 	 ,  'UINT'				, 1		, '此区域总下注'},
    for index = 1, #cmd.m_areaTotalBet do 
        local item = cmd.m_areaTotalBet[index];
        self.m_pAreaTotalBet[item.m_betAreaId] = item.m_totalBet;
    end

    -- { 1,	1, 'm_betAreaId'		 ,  'UINT'				, 1		, '下注区域id, 1:大保时捷  2：大宝马  3:大奔驰  4:大大众 5:小保时捷 6:小宝马 7:小奔驰 8:小大众'},
    -- { 2,	1, 'm_myBet'		 	 ,  'UINT'				, 1		, '我在此区域的下注'},
    for index = 1, #cmd.m_myAreaBet do
        local item = cmd.m_myAreaBet[index];
        self.m_pMyAreaBet[item.m_betAreaId] = item.m_myBet;
    end
end

function BirdData:SET_GAME_READY_NTY(cmd)
    --等待阶段
    -- { 1,	1, 'm_leftTime'			 ,  'UINT'						, 1		, '该状态剩余时间'},
    -- { 2, 	1, 'm_isLook'			 , 'UINT'						, 1 	, '是否旁观者, 0:否, 1:是' },
    self.m_bIsLook = cmd.m_isLook == 1;
    self.m_fClockTime = os.clock();
    self.m_nLeftTime = cmd.m_leftTime;
    self.m_nGameState = BirdConfig.STATE_FREE;
    if self:isBet() then
        self.m_pMyLastBet = self.m_pTmpLastBet;
    end
    self.m_pTmpLastBet = {0,0,0,0,0,0,0,0,0,0,0};
end

function BirdData:SET_OPEN_AWARD_NTY(cmd)
    --开牌阶段
    -- { 1,	1, 'm_result'			 ,  'INT'						, 1		, '0:成功, -X:失败'},
	-- { 2,	1, 'm_leftTime'			 ,  'UINT'						, 1		, '该状态剩余时间'},
	-- { 3,	1, 'm_awardType'		 ,  'UINT'						, 1		, '开奖类型, 1:大保时捷  2：大宝马  3:大奔驰  4:大大众 5:小保时捷 6:小宝马 7:小奔驰 8:小大众 '},
	-- { 4,	1, 'm_openGridId'		 ,  'UINT'						, 1		, '本次开奖后跑灯停留格子id'},
	-- { 5,	1, 'm_allTotalBet'   	 ,  'UINT'						, 1		, '所有区域、所有玩家总下注'},
	-- { 6,	1, 'm_awardBetAreaId'	 ,  'UINT'						, 1024	, '本次中奖的下注区域id'},
	-- { 7, 	1, 'm_history'			 ,  'UINT'						, 1024 	, '历史开奖记录(元素类型同m_awardType)' },
	-- { 8,	1, 'm_balanceInfo'  	 ,  'PstBmwBalanceClt'			, 1		, '自己结算数据'},
    -- { 9,	1, 'm_topPlayerBalance'  ,  'PstBmwBalanceClt'			, 1024	, '左右两侧前6名的结算(数量<=6)'},
    
    -- PstBmwBalanceClt
    -- { 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
    -- { 2,	1, 'm_profit'			, 'INT'					, 1		, '本轮净盈利'},
    -- { 3,	1, 'm_curCoin'			, 'UINT'				, 1		, '结算后金币'},

    self.m_nGameState = BirdConfig.STATE_OPEN;
    self.m_fClockTime = os.clock();
    self.m_nLeftTime = cmd.m_leftTime;
    self.m_nLastAwardType = cmd.m_areaType;
    self.m_nLastGridId = cmd.m_openGridId;
    self.m_nHistory = cmd.m_history;
    self.m_nCurCoin = cmd.m_balanceInfo.m_curCoin;
    self.m_pAreaTotalBet = {0,0,0,0,0,0,0,0,0,0,0}; --各区域总下注
    self.m_pMyAreaBet = {0,0,0,0,0,0,0,0,0,0,0}; --我在各区域的下注

    -- { 1,	1, 'm_betAreaId'		 ,  'UINT'				, 1		, '下注区域id, 1:大保时捷  2：大宝马  3:大奔驰  4:大大众 5:小保时捷 6:小宝马 7:小奔驰 8:小大众'},
	-- { 2,	1, 'm_totalBet'		 	 ,  'UINT'				, 1		, '此区域总下注'},
    -- for index = 1, #cmd.m_allTotalBet do 
    --     local item = cmd.m_allTotalBet[index];
    --     self.m_pAreaTotalBet[item.m_betAreaId] = item.m_totalBet;
    -- end
end

function BirdData:SET_BET_ACK(cmd)
    --玩家下注
    -- { 1, 	1, 'm_result'		 ,  'INT'					, 1		, '0:成功, -x:失败(m_result=0时，广播此ack)' },
	-- { 2, 	1, 'm_betAccountId'	 ,  'UINT'					, 1		, '下注玩家ID' },
	-- { 3,	1, 'm_curCoin'		 ,  'UINT'					, 1		, '下注成功后的玩家金币(仅在m_result=0,且m_betAccountId=自己时有效)'},
	-- { 4,	1, 'm_betAreaId'	 ,  'UINT'					, 1		, '下注区域id'},
	-- { 5,	1, 'm_betValue'		 ,  'UINT'					, 1		, '本次下注额'},
	-- { 6,	1, 'm_allTotalBet'   ,  'UINT'					, 1		, '所有区域、所有玩家总下注'},
	-- { 7, 	1, 'm_areaTotalBet'	 ,  'PstBmwAreaTotalBet'	, 1024 	, '各区域总下注' },
    -- { 8, 	1, 'm_myAreaBet'	 ,  'PstBmwAreaMyBet'	    , 1024 	, '我在各区域的下注(仅在m_result=0,且m_betAccountId=自己时有效)' },

    if cmd.m_betAccountId == Player:getAccountID() then
        self.m_nCurCoin = cmd.m_curCoin;
        for index = 1, #cmd.m_myAreaBet do
            local item = cmd.m_myAreaBet[index];
            self.m_pMyAreaBet[item.m_betAreaId] = item.m_myBet;
        end
        self.m_pTmpLastBet[cmd.m_betAreaId] = cmd.m_betValue + self.m_pTmpLastBet[cmd.m_betAreaId];
    end
    
    self.m_nAllTotalBet = cmd.m_allTotalBet;
    for index = 1, #cmd.m_areaTotalBet do 
        local item = cmd.m_areaTotalBet[index];
        self.m_pAreaTotalBet[item.m_betAreaId] = item.m_totalBet;
    end
end

function BirdData:SET_BET_NTY(cmd)
    --下注阶段
    -- { 1,	1, 'm_leftTime'			 ,  'UINT'						, 1		, '该状态剩余时间'},
    self.m_nGameState = BirdConfig.STATE_BET;
    self.m_fClockTime = os.clock();
    self.m_nLeftTime = cmd.m_leftTime;
    self.m_sRecordId = cmd.m_recordId
end

function BirdData:SET_ONLINE_PLAYER_LIST_ACK(cmd)
    --在线玩家
    -- { 1,	1, 'm_playerList'			 , 'PstBmwInitPlayerInfo'		, 1024		, '玩家基本信息'},
    self.m_pPlayerList = cmd.m_playerList;
end

function BirdData:SET_BACKGROUND_ACK(cmd)
    --切换后台
end

function BirdData:SET_EXIT_ACK(cmd)
    --退出
end

function BirdData:SET_TOP_PLAYER_LIST_NTY(cmd)
    --前六名玩家
    -- { 1, 	1, 'm_topPlayerList'		 , 'PstBmwTopPlayerInfo'  			, 1024		, '前6名玩家(实际数量 <= 6)' },
    self.m_pTopPlayerList = cmd.m_topPlayerList;
end

function BirdData:SET_CONTINUE_BET_ACK(cmd)
    --续投
    -- { 1, 	1, 'm_result'		 ,  'INT'					, 1		, '0:成功, -x:失败(m_result=0时，广播此ack)' },
	-- { 2, 	1, 'm_betAccountId'	 ,  'UINT'					, 1		, '下注玩家ID' },
	-- { 3,	1, 'm_curCoin'		 ,  'UINT'					, 1		, '下注成功后的玩家金币(仅在m_result=0,且m_betAccountId=自己时有效)'},
	-- { 4,	1, 'm_continueBetArr'	 ,  'PstBmwContinueBet'	, 1024	, '玩家续押信息'},
	-- { 5,	1, 'm_allTotalBet'   ,  'UINT'					, 1		, '所有区域、所有玩家总下注'},
	-- { 6, 	1, 'm_areaTotalBet'	 ,  'PstBmwAreaTotalBet'	, 1024 	, '各区域总下注' },
    -- { 7, 	1, 'm_myAreaBet'	 ,  'PstBmwAreaMyBet'	    , 1024 	, '我在各区域的下注(仅在m_result=0,且m_betAccountId=自己时有效)' },
    
    if cmd.m_betAccountId == Player:getAccountID() then
        self.m_nCurCoin = cmd.m_curCoin;
        for index = 1, #cmd.m_myAreaBet do
            local item = cmd.m_myAreaBet[index];
            self.m_pMyAreaBet[item.m_betAreaId] = item.m_myBet;
        end
        -- { 1,	1, 'm_betAreaId'		 ,  'UINT'				, 1		, '下注区域id, 1:大保时捷  2：大宝马  3:大奔驰  4:大大众 5:小保时捷 6:小宝马 7:小奔驰 8:小大众'},
        -- { 2,	1, 'm_curBet'		 	 ,  'UINT'				, 1		, '此次下注'},
        for index = 1, #cmd.m_continueBetArr do
            local item = cmd.m_continueBetArr[index];
            self.m_pTmpLastBet[item.m_betAreaId] = item.m_curBet + self.m_pTmpLastBet[item.m_betAreaId];
        end
    end

    for index = 1, #cmd.m_areaTotalBet do 
        local item = cmd.m_areaTotalBet[index];
        self.m_pAreaTotalBet[item.m_betAreaId] = item.m_totalBet;
    end
end








return BirdData;