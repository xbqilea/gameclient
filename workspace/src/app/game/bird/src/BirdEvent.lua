local flag = "Bird_Event_"
local BirdEvent = {
    MSG_INIT_NTY                = flag.."MSG_INIT_NTY",
    MSG_GAME_STATE              = flag.."MSG_GAME_STATE",
    MSG_BET                     = flag.."MSG_BET",
    MSG_ONLINE_PLAYER_LIST_ACK  = flag.."MSG_ONLINE_PLAYER_LIST_ACK",
    MSG_TOP_PLAYER_LIST_NTY     = flag.."MSG_TOP_PLAYER_LIST_NTY",
    MSG_CONTINUE_BET_ACK        = flag.."MSG_CONTINUE_BET_ACK",

    MSG_BACKGROUND_ACK          = flag.."MSG_BACKGROUND_ACK",
    MSG_EXIT_ACK                = flag.."MSG_EXIT_ACK",
}

return BirdEvent