local BirdConfig = {};

BirdConfig.STATE_FREE = 1;   --等待
BirdConfig.STATE_BET = 2;    --下注
BirdConfig.STATE_OPEN = 3;   --开奖

BirdConfig.AREA_COUNT = 11; --下注区域数
BirdConfig.BET_TYPE_COUNT = 5; --筹码类型数

BirdConfig.BR_CHIP_SPEED = display.width; -- 金币飞行速度
BirdConfig.CHIP_FLY_STEPDELAY = 0.02; -- 连续飞行间隔
BirdConfig.CHIP_FLY_TIME = 0.2; -- 筹码飞行时间
BirdConfig.CHIP_JOBSPACETIME = 0.3; -- 飞筹码任务队列间隔
BirdConfig.CHIP_FLY_SPLIT = 20; --飞行间隔数
BirdConfig.CHIP_SCALE = 0.75

return BirdConfig;