--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
--endregion
--
-- Author: lhj
-- Date: 2018-10-19 
-- 设置层
-- BirdGameLayer 
local Scheduler            = require("framework.scheduler")
local HNLayer = require("src.app.newHall.HNLayer")
local PlayerListLayer = require("src.app.game.common.util.PlayerListLayer")
local PlayerHeadLayer = require("src.app.game.common.util.PlayerHeadLayer")
local GameSetLayer = require("src.app.newHall.childLayer.SetLayer")
local BirdRuleLayer = import(".BirdRuleLayer")
local GameRecordLayer = require("src.app.newHall.childLayer.GameRecordLayer")
--local AnimationBetLayer = import(".AnimationBetLayer")
local CChipCacheManager = require("src.app.game.common.manager.CChipCacheManager")
local BirdEvent = require("app.game.bird.src.BirdEvent")
local BirdRes = require("app.game.bird.src.BirdRes")
local BirdData = require("app.game.bird.src.BirdData")
local BirdConfig = require("app.game.bird.src.BirdConfig")
local ChatLayer = require("src.app.newHall.chat.ChatLayer") 
--endregion
local game_id_red = 80000044
local game_room_res = game_id_red .. "/game_room/"

local game_fnt_res = game_id_red .. "/font/"

local game_bet_btn_res = game_id_red .. "/bet/"

local game_ani_res = game_id_red .. "/ani/"

local game_win_res = game_id_red .. "/Winning/"

local game_light_res = game_id_red .. "/light/"
local game_audio_res = game_id_red .. "/audio/"

local BirdGameLayer = class("BirdGameLayer", function()
    return HNLayer.new();
end)

function BirdGameLayer:ctor()
    -- self:initEvent()
    self:myInit()
    self:setupViews();
    --  self.tableLogic:sendGameInfo()
    -- self:msgGameInit()
    -- self:msgGameState()
    -- self:msgGameTopPlayerList()
    self:_InitMsgEvent();
    ToolKit:registDistructor(self, handler(self, self._OnDestory));
end

function BirdGameLayer:_OnDestory()
    self:_UnMsgEvent();
end

function BirdGameLayer:myInit()
    self.click_face_anim_time = 0

    self.jianzihu = false
    self._backFlag = true
    self.table_state = 0

    self.max_bet_num = 6

    self.my_coin_num = 0

    self.isCustomize = true

    self.max_bet_gold = 1000

    self.min_bet_gold = 1

    self.cur_bet_index = 1

    self.notice_moveing = false

    self.game_end_data = {}

    self.wait_show_xiazhu_coin = {}

    self.myselfXiazhuNum = 0

    self.continue_bet_array = {}

    self.table_coin_node_array = {}

    self.game_jetton_path_list = {

        game_room_res .. "niuniu_img_1.png",

        game_room_res .. "niuniu_img_2.png",

        game_room_res .. "niuniu_img_3.png",

        game_room_res .. "niuniu_img_4.png",

        game_room_res .. "niuniu_img_5.png",

        game_room_res .. "niuniu_img_6.png",
    }

    self.totalBet = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }

    self.onback_data = 0

    self.select_coin_button_max_index = 0

    self.game_bet_list = { 1, 10, 50, 100, 500, 1000 }

    self.sit_user_list = {}

    --    self.area_multiple = {}
    --    local offx = game_adapter:getAdapterOffX()
    --    self.offx = offx
    --    local pt_scale = game_adapter:getScreenScale()
    --    self.pt_scale = pt_scale.x
    --    if offx == 0 then
    --        self.pt_scale = 1
    --    end
    --    self.win_position = {cc.p(199*self.pt_scale + offx, 150 ),cc.p((199+247)*self.pt_scale + offx,150),cc.p((197+247*2)*self.pt_scale + offx,150),cc.p((197+247*3)*self.pt_scale + offx,200),cc.p(590*self.pt_scale + offx,670)}
    --    local json_ui = "game_res/" .. game_res_id .. "/" .. game_id ..  "/BirdGameLayer.csb"
    --    self:init(json_ui)
    self.m_isPlayBetSound = false
    self.m_chip_scale = 0.75

    self.m_flyJobVec = {}
    self.m_betChips = {}

    self.m_bNodeMenuMoving = false

    self.m_bContinue = false    --是否已续投过
    self.m_pShipLast = {
        m_bIsLast = false,

    }       --上一把下注记录
    self.m_pShipLastTmp = {
        m_bIsLast = false,

    }
end

function BirdGameLayer:onTouchCallback(sender)
    local name = sender:getName()
    local subStr = string.sub(name, 1, 11)
    if name == "button_setting" then  -- 设置  
        local layer = GameSetLayer.new(203);
        self:addChild(layer);
        --layer:setScale(1/display.scaleX, 1/display.scaleY);  
    elseif name == "button_helpX" then -- 规则 
        local layer = BirdRuleLayer.new()
        self:addChild(layer);
        layer:setLocalZOrder(100)
    elseif name == "button_exit" then -- 退出  
        --g_GameController:exitReq()
    elseif name == "auto_btn_other" then    
   --     g_GameController:gamePlayerOnlineListReq()
   --     local t = ChatMgr:getInstance():getRoomMsgList(10010001)
        local ChatLayer = ChatLayer.new()
        self:addChild(ChatLayer);
    elseif name == "button_back" then
        if  BirdData.getInstance().m_nGameState ~= BirdConfig.STATE_FREE and BirdData.getInstance():isBet() then
            TOAST("游戏中无法退出！")
            return
        end
        g_GameController:exitReq()
    elseif name == "btn_menu_pop" then
        --显示
        if self.m_bNodeMenuMoving then return end
        self.m_bNodeMenuMoving = true

        self.btn_panel_backMenu:setVisible(true)
        self.panel_backMenu:setVisible(true)
        self.panel_backMenu:setLocalZOrder(100)
        self.ImageView_2:setVisible(true)
        self.ImageView_2:setPosition(cc.p(0, 30))

        local callback = cc.CallFunc:create(function()
            self.btn_menu_pop:setVisible(false)
            self.btn_menu_push:setVisible(true)
        end)

        local callback2 = cc.CallFunc:create(function()
            self.m_bNodeMenuMoving = false
        end)

        self.ImageView_2:stopAllActions()
        self.ImageView_2:setOpacity(0)
        local aTime = 0.25
        local moveTo = cc.MoveTo:create(aTime, cc.p(0, 0))
        local show = cc.Show:create()
        local sp = cc.Spawn:create(cc.EaseBackOut:create(moveTo), cc.FadeIn:create(aTime))
        local seq = cc.Sequence:create(show, sp, callback, cc.DelayTime:create(0.1), callback2)
        self.ImageView_2:runAction(seq)

    elseif name == "btn_menu_push" or name == "btn_panel_backMenu" then
        -- 隐藏
        if self.m_bNodeMenuMoving then return end
        self.m_bNodeMenuMoving = true

        local callback = cc.CallFunc:create(function()
            self.btn_menu_pop:setVisible(true)
            self.btn_menu_push:setVisible(false)
        end)

        local callback2 = cc.CallFunc:create(function()
            self.m_bNodeMenuMoving = false    
        end)

        self.btn_panel_backMenu:setVisible(false)
        self.ImageView_2:stopAllActions()
        local aTime = 0.25
        local moveTo = cc.MoveTo:create(aTime, cc.p(0, 30))
        local sp = cc.Spawn:create(cc.EaseBackIn:create(moveTo), cc.FadeOut:create(aTime))
        local hide = cc.Hide:create()
        local seq = cc.Sequence:create(sp, callback, hide, cc.DelayTime:create(0.1), callback2, nil)
        self.ImageView_2:runAction(seq)

    elseif name == "auto_btn_more" then
        local GameRecordLayer = GameRecordLayer.new(2, 203)
        self:addChild(GameRecordLayer)
        ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", { 203 })
    elseif subStr == "btn_xiazhu_" then

        local name_prefix = string.sub(name, 12, #name)

        self.cur_bet_index = tonumber(name_prefix)

        self:update_select_btn()
    elseif subStr == "Button_bet_" then

        --下注
        local Image_light = sender:getChildByName("Image_light")

        Image_light:setVisible(false)

        local name_prefix = string.sub(name, 12, #name)

        local pos = tonumber(name_prefix)

        self:request_xiazhu(pos)

    elseif name == "btn_continue" then
        -- 续投
        self:onContinueClick()
    end
end
function BirdGameLayer:setupViews() 
    --  Audio:getInstance():playBgm();
    local start_clock = os.clock();

    local winSize = cc.Director:getInstance():getWinSize();
    self.root_node = CacheManager:addCSBTo(self, BirdRes.CSB.MAIN);
    -- self:addChild(self.root_node);
    local center = self.root_node:getChildByName("center")
    local diffY = (display.size.height - 750) / 2
    self.root_node:setPosition(cc.p(0, diffY))

    local diffX = 145 - (1624 - display.size.width) / 2
    center:setPositionX(diffX)
    UIAdapter:adapter(self.root_node, handler(self, self.onTouchCallback))
    UIAdapter:praseNode(self.root_node, self)
    self:update_state_text(1)
    self:update_continue_btn(false)
    self.posX = self.ImageView_2:getPositionX()
    self.backWidth = self.ImageView_2:getContentSize().width
    self.panel_backMenu:setSwallowTouches(false)
    self.panel_backMenu:setVisible(false)
    self.auto_Panel_tips:setLocalZOrder(10)

    self.auto_panel:setLocalZOrder(5)

    self.auto_mid_panel_animation:setLocalZOrder(10)



    self.auto_panal_notify:setVisible(false)

    for i = 1, 6 do

        local Image_bg = self.auto_xiazhu_btn_panel:getChildByName("btn_xiazhu_" .. i):getChildByName("Image_bg")

        Image_bg:runAction(cc.RepeatForever:create(cc.RotateBy:create(1, 360)))

    end
    self["baisha_bet_num"]:setString("24")
    self:init_light()
    self:update_select_btn()
    self._userListLayer = PlayerListLayer.new();
    self._userListLayer:setVisible(false)
    self:addChild(self._userListLayer);
    self._userListLayer:setLocalZOrder(1000)
    self:showAnimationWithName(self.Node_1, "feiqinzoushou_changjingtexiao", "Animation1", nil, true)
    self:showAnimationWithName(self.Node_1, "feiqinzoushou_changjingtexiao", "Animation2", nil, true)
    self:showAnimationWithName(self.Node_1, "feiqinzoushou_changjingtexiao", "Animation3", nil, true)

    --缓存管理添加委托构造筹码方法
    CChipCacheManager.getInstance():setCreateChipDelegate( function(betValue)
        --local sp = cc.Sprite:createWithSpriteFrameName(self:getFileOfChip(nIndex))
        for index = 1, #self.game_bet_list do
            if self.game_bet_list[index] == betValue then
                local item = cc.Sprite:create(BirdRes.Image["BET_"..index]);
                self.m_pNodeFlyIcon:addChild(item);
                return item;
            end
        end
        local item = cc.Sprite:create(BirdRes.Image["BET_6"]);
        self.m_pNodeFlyIcon:addChild(item);
        return item;
    end)

    -- 获取筹码动画层
    --[[local auto_mid = center:getChildByName("auto_mid")
    self.m_pNodeFlyIcon = auto_mid:getChildByName("m_pNodeFlyIcon")

    -- 获取庄家位置
    local panel_bet_table = auto_mid:getChildByName("Panel_bet_table")
    self.m_pAnimalUI = panel_bet_table:getChildByName("m_pAnimalUI")

    local animalUIPos = cc.p(self.m_pAnimalUI:getPosition())
    ]]
    --local worldPos = self.m_pAnimalUI:convertToWorldSpace(cc.p(0,0))
    self.m_bankerPos = cc.p(0, 0) --self.m_pAnimalUI:convertToNodeSpace(worldPos)

    -- 获取玩家位置
    self.m_playerPos = {}

    for i = 1, 8 do
        if i == 7 then

            local auto_panel = ccui.Helper:seekWidgetByName(self.auto_panel, "auto_self_panel")

            auto_panel.self_head = ccui.Helper:seekWidgetByName(auto_panel, "self_head")

            local world_pos = auto_panel.self_head:convertToWorldSpace(cc.p(0, 0))

            local layer_pos = self.m_pNodeFlyIcon:convertToNodeSpace(world_pos)

            self.m_playerPos[i] = layer_pos

        elseif i == 8 then

            local auto_btn_other = ccui.Helper:seekWidgetByName(self.auto_panel, "auto_btn_other")

            local world_pos = self.auto_panel:convertToWorldSpace(cc.p(auto_btn_other:getPosition()))

            local layer_pos = self.m_pNodeFlyIcon:convertToNodeSpace(world_pos);

            self.m_playerPos[i] = layer_pos

        else

            local user_panel = ccui.Helper:seekWidgetByName(self.auto_panel, "auto_user_portrait_" .. i)

            local world_pos = self.auto_panel:convertToWorldSpace(cc.p(user_panel:getPosition()));

            local layer_pos = self.m_pNodeFlyIcon:convertToNodeSpace(world_pos);

            self.m_playerPos[i] = layer_pos

        end
    end

    self.m_areaPos = {}
    for i = 1, BirdConfig.AREA_COUNT do
        local node = self.AreaBoxNode:getChildByName("AreaBoxNode_" .. i)

        local size = node:getContentSize();

        local nodeWorldPos = self.AreaBoxNode:convertToWorldSpace(cc.p(node:getPosition()))
        local layerPos = self.m_pNodeFlyIcon:convertToNodeSpace(nodeWorldPos);

        local maxX = size.width / 2
        local minX = -maxX
        local maxY = size.height / 2
        local minY = -maxY

        self.m_areaPos[i] = {}

        for n = 1, 50 do
            local randomX = math.random(minX, maxX)
            local randomY = math.random(minY, maxY)

            local random_pos = cc.p(randomX + layerPos.x, randomY + layerPos.y)

            table.insert(self.m_areaPos[i], random_pos)
        end

    end

    self.btn_continue:setColor(cc.c3b(160, 160, 160))
    self.btn_continue:setEnabled(false)

    self.mTextRecord = UIAdapter:CreateRecord()
    self.auto_panel:addChild(self.mTextRecord)
    local button_back = self.auto_panel:getChildByName("button_back")
	local buttonsize = button_back:getContentSize()
	local buttonpoint = cc.p(button_back:getPosition())
	self.mTextRecord:setPosition(cc.p(buttonpoint.x - buttonsize.width / 2, buttonpoint.y + buttonsize.height / 2 + 20))

    print("初始化场景耗时---：", os.clock() - start_clock);
    return true;
end
function BirdGameLayer:updateOnlineUserList(__info)
    self._userListLayer:updateUserList(__info);
    self._userListLayer:setVisible(true);
end

function BirdGameLayer:init_light()

    for i = 0, 27 do

        local light = self["Image_light_" .. i]:getChildByName("Image_light")

        light:setLocalZOrder(-1)

    end

end

function BirdGameLayer:onCleanup()
    -- self:clearEvent()
    --清除池缓存
    CChipCacheManager.getInstance():Clear(true)
    CChipCacheManager.getInstance():resetChipZOrder()

end
function BirdGameLayer:update_continue_btn(notEnabled)

    if #self.continue_bet_array > 0 then

        self.btn_bet_continue:setEnabled(notEnabled)

    else

        self.btn_bet_continue:setEnabled(false)

    end

end
function BirdGameLayer:showAnimationWithName(node, name, animationName, callback, notCleanFlag)
    local pos = cc.p(node:getPosition())
    local armature = CacheManager:addDragonBonesTo(node, name);-- ccs.Armature:create(name)
    --armature:setPosition(pos) 
    -- node:addChild(armature)
    armature:getAnimation():play(animationName, -1, -1)
    return armature
end
function BirdGameLayer:play_state_animation(animation_name, complete, index)

    if not index then

        index = 0

    end

    local pos = cc.p(self.auto_mid_panel_animation:getChildByName("pos_state_animtion"):getPosition())

    local animation = CacheManager:addDragonBonesTo(self.auto_mid_panel_animation, animation_name, 1); --ccs.Armature:create(animation_name)

    animation:getAnimation():playWithIndex(index, 0, 0)

    -- self.auto_mid_panel_animation:addChild(animation, 1)

    local call_back = function(armature, movementType, movement_name)

        if movementType == ccs.MovementEventType.loopComplete then


        elseif movementType == ccs.MovementEventType.complete then
            CacheManager:putDragonBonesNode(animation_name, animation);
            -- animation:removeFromParent()

            if complete then

                complete()

            end

        end

    end

    animation:getAnimation():setMovementEventCallFunc(call_back)

    animation:setPosition(pos)

end

function BirdGameLayer:open_scene()

    local cur_state = get_room_info()["m_state"]

    local room_users = get_room_users_info()

    self:init_ui()

    ----通知框架更新房间ui
    self:init_BirdGameLayer(1, get_room_info())

    self:render_game_start_scene(get_room_info())

end

-- 初始化游戏场景
function BirdGameLayer:init_BirdGameLayer(scene_type, response)

    rawset(_G, "GAME_STATE", response["m_state"])

    self.scene_type = scene_type

end


function BirdGameLayer:init_game_ui()

end

--断线重连
function BirdGameLayer:user_reconnect_room(response)

    local gameInfo = get_room_info()

    self:render_game_start_scene(gameInfo)

end

function BirdGameLayer:close_scene()

    self:clear_scene()

end
function BirdGameLayer:showRunTime(time)

    self._allTime = time;
    self._nowTime = time;
    self._fnowTime = time;
    if self._nowTime <= 0 then
        self._nowTime = 0
        self.daojishi_time:setVisible(false);
        --      self._gameStateBg:setVisible(false);	
    end
    self.daojishi_time:setString(string.format("%d", self._nowTime));

    if self._Scheduler1 then
        Scheduler.unscheduleGlobal(self._Scheduler1)
        self._Scheduler1 = nil
    end

    if (time <= 0) then
        --	self._gameStateBg:setVisible(false); 
    else
        self.daojishi_time:setVisible(true);
        --	self._gameStateBg:setVisible(true);
        self._Scheduler1 = Scheduler.scheduleGlobal(handler(self, self.updateGameTime), 1)
    end
end
function BirdGameLayer:updateGameTime(dt)


    self._nowTime = self._nowTime - 1;

    if self._nowTime <= 0 then
        self._nowTime = 0
    end
    self.daojishi_time:setString(string.format("%d", self._nowTime));
    if (self._nowTime <= 0) then    
        self._nowTime = 0;

        if self._Scheduler1 then
            Scheduler.unscheduleGlobal(self._Scheduler1)
            self._Scheduler1 = nil
        end

    end
end
function BirdGameLayer:showLeftTime(time)
    self:showRunTime(time)




    print("倒计时===============", self.table_state, self.is_already_run, time)
    if self.table_state == 2 then
        
    end
    if self.table_state == 3 and self.is_already_run == nil then

        self.is_already_run = 1

        self:add_light_speed(time)

    end

end
function BirdGameLayer:ui_notify_listener(event)

    local envent_name = event:getEventName()

    local listen_events = {

        ["ui_notify_open_msg_box"] = function()

            local typ = event.data["type"]

            local text = event.data["text"]

            local confirm = event.data["confirm"]

            local cancel = event.data["cancel"]

            local fairy = event.data["fairy"]

            if self.root_node:getChildByTag(game_scene_tag["msg_box"]) then

                self.root_node:removeChildByTag(game_scene_tag["msg_box"])

            end

            if self.root_node:getChildByTag(game_scene_tag["msg_box"]) == nil then

                local boxView = _box.new(self.root_node, typ, text, fairy, confirm, cancel)

                boxView:setLocalZOrder(10120)

            end

        end,

        ["game_info_No_gold"] = function()

            local text = event.data["text"]

            local confirm = function()

                _chongzhi_view.new(self.root_node)

            end

            if self.root_node:getChildByTag(game_scene_tag["msg_box"]) then

                self.root_node:removeChildByTag(game_scene_tag["msg_box"])

            end

            if self.root_node:getChildByTag(game_scene_tag["msg_box"]) == nil then

                local path = "hall/" .. hall_res_id .. "/hall_new/chongzhi/chongzhi_img_16.png"

                _box.new(self.root_node, 2, text, fairy, confirm, cancel, path)

            end

        end,

        ["ui_notify_notify_show"] = function()

            local data = event.data

            self:show_nitify_info(data)

        end,

        ["game_update_user_coins"] = function()

            local data = event.data.coin or 0

            get_room_info()["user"].coin = tonumber(data) or 0

            self:update_user_score(data)

        end,

        ["ui_notify_close_msg_box"] = function()

            local box = self.root_node:getChildByTag(game_scene_tag["msg_box"])

            if box then

                box:remove()

            end

        end,

        ["ui_notify_lock_screen"] = function()

            local timeout = event.data["timeout"] or 5

            local show_ani = event.data["show_ani"] or 1

            local text = event.data["text"] or "请等待......"

            local loading = self.root_node:getChildByTag(game_scene_tag["loading"])

            if not loading then

                _loading.new(self.root_node, timeout, text, show_ani)

            else

                loading:update(text, timeout, show_ani)

            end

        end,

        ["ui_notify_unlock_screen"] = function()

            local loading = self.root_node:getChildByTag(game_scene_tag["loading"])

            if loading then

                loading:leave()

            end

        end,

        ["notify_frame_init_ui_state"] = function()
            ----初始化ui
            local scene_type = event.data["scene_type"]

            self:init_BirdGameLayer(scene_type, response)

        end,

        ["ui_notify_lock_card"] = function()

            local lock = event.data["lock"]

            self.mahjong_layout:lock(lock)

        end,

        ["ui_notify_reconnect_scene"] = function()
            --- 闪断闪连的情况
            self:close_scene()

            self:open_scene()

            --local gameInfo = get_room_info()
            --self:render_game_start_scene(gameInfo)
        end,

        ["ui_notify_enter_game"] = function()
            ----进入游戏
            local response = event.data["response"]

            self:render_game_start_scene(response)

        end,

        ["ui_notify_user_reconnect_room"] = function()
            ----断线重连
            local response = event.data["response"]

            self:clearCoin()

            self.root_node:stopAllActions()

            self:clear_round_win()

            set_room_info_on_reconnect(response)

            self:user_reconnect_room(response)

        end,

        ["ui_notify_game_start"] = function()
            ----开始游戏
            local response = event.data["game_info"]

        end,

        ["ui_notify_game_bet"] = function()

            local response = event.data["game_info"]

            self:clear_scene()

            self:update_continue_btn(true)

            self:update_users_info(response.users)

            self:update_state_text(response.m_state)

            --   self:game_show_bet_state( response )
            local func = function()

            end

            g_AudioPlayer:playEffect(game_audio_res .. "hh_start.mp3")

            self:play_state_animation("game_star", func)


        end,

        ["ui_notify_scene_user_add_bet"] = function()

            local response = event.data["response"]

            self:game_user_add_bet(response)

            --self:update_users_info( response.users )
        end,

        ["ui_notify_scene_offsetPlayer_add_bet"] = function()

            local response = event.data["response"]

            self:user_offset_player_add_bet_ack(response)

        end,

        ["ui_notify_game_open_reward"] = function()

            local response = event.data["game_info"]

            self:update_state_text(response.m_state)

            self:update_continue_btn(false)

            self.winBetPos = response.winBetPos

            for k, v in pairs(get_room_info().loop) do

                if v == response.winPos then

                    self.dest_track = k

                end

            end

            g_AudioPlayer:playEffect("hh_stop")

            self:play_state_animation("game_end", func)

        end,

        ["ui_notify_game_round_end"] = function()
            -- 单局结算
            local response = event.data["game_info"]

            -- print( "ui_notify_game_round_end===================" )
            -- dump(   response )
            local player = response.player

            local bet_values = player.betCoin

            self.continue_bet_array = {}

            for i = 1, #bet_values do

                local nIndex = i - 1

                if bet_values[i] > 0 then

                    local bet_array = {["betValue"] = bet_values[i], ["betPos"] = nIndex, ["show_ani"] = 0 }

                    insert_table(self.continue_bet_array, bet_array)

                end

            end

            self:update_state_text(response.m_state)

            self:game_end_coin_change(response)

        end,

        ["ui_notify_clear_scene"] = function()

            self:close_scene()

        end,

        ["ui_notify_net_delay"] = function()

            self.delay_time = (os.clock() - self.net_delay) * 1000

        end,

        ["ui_notify_update_count_down"] = function()
            --倒计时
            local response = event.data["response"]

            self:update_state_time(response.time)

            if self.table_state == 2 and response.time == 5 then

                local str_json = game_ani_res .. "daojishi_54321/daojishi_54321.json"

                local str_atlas = game_ani_res .. "daojishi_54321/daojishi_54321.atlas"

                if not self.daojishi_armature then

                    self.daojishi_armature = sp.SkeletonAnimation:createWithJsonFile(str_json, str_atlas, 1)  --特效

                    self.daojishi_armature:setAnimation(0, "animation", false)

                    local pos = cc.p(self.auto_mid_panel_animation:getChildByName("pos_state_animtion"):getPosition())

                    self.daojishi_armature:setPosition(pos.x, 0)

                    self.auto_mid_panel_animation:addChild(self.daojishi_armature, 1)

                else

                    self.daojishi_armature:setVisible(true)

                    self.daojishi_armature:setAnimation(0, "animation", false)

                end

                self.daojishi_armature:runAction(cc.Sequence:create({
                    cc.DelayTime:create(5),
                    cc.CallFunc:create(function()
                        self.daojishi_armature:setVisible(false)
                    end)
                }))

            end

            if self.table_state == 2 and response.time <= 5 then

                g_AudioPlayer:playEffect("lh_time_out_notice")

            end

            print("倒计时===============", self.table_state, self.is_already_run, response.time)

            if self.table_state == 1 and self.is_already_run == nil then

                self.is_already_run = 1

                self:add_light_speed(response.time)

            end

        end,

        ["ui_notify_quit_room"] = function()

            local response = event.data["response"]

            self:user_quit_room(response)

        end,

        ["ui_notify_event_user_enterforeground"] = function()

            local request = {};
            request.onBack = 1                --前台
            request.show_ani = 2
            g_net:ws_request(FRAME_COMMAND.MSG_APP_CHANGE_STATE_REQUEST, request)

        end,

        ["ui_notify_event_user_enterbackground"] = function()

            local request = {}
            request.onBack = 0                 --后台
            request.show_ani = 2
            g_net:ws_request(FRAME_COMMAND.MSG_APP_CHANGE_STATE_REQUEST, request)

        end,

        ["game_info_tips"] = function()

            local text = event.data["text"]
            self:show_game_tips(text)

        end,

        ["ui_notify_user_quit_game"] = function()

            local response = event.data["response"]

            self:user_quit_room(response)

        end,

        ["ui_notify_offset_player"] = function()

            local response = event.data["response"]

            game_play_list.new(self.root_node, response.users)

        end,

        ["ui_notify_pay_success"] = function()

            local data = event.data["response"]

            if data.type == 1 or data.type == 2 then

                self:updateMyCoin(data.coin)

                set_my_coin_game(data.coin)

                g_notifyer:notify_ui_event("ui_notify_open_msg_box", {["type"] = msgbox_type.know, ["text"] = "充值成功！" })

            end

        end,


    }

    local listen_event = listen_events[envent_name]

    if listen_event then

        listen_event()

    end

end

function BirdGameLayer:openAward(response)

    if response == nil then return end


    self.winBetPos = response.m_awardBetAreaId

    self.dest_track = response.m_openGridId

    g_AudioPlayer:playEffect(BirdRes.Audio.HH_STOP)

    self:play_state_animation("game_end")


    -- self:game_end_coin_change( response )         
end

function BirdGameLayer:add_light_speed(leftTime)

    if leftTime and leftTime < 9 then

        return

    end

    --    if not self.last_winPos or self.last_winPos < 0 then
    --        self.last_winPos = math.random( 0 , 27 )
    --    end
    local nowWinPos = 1

    local toWinPos = self.dest_track or 6

    self.last_winPos = 1

    local count = 0

    --    if nowWinPos > toWinPos then
    --        count = 28 - nowWinPos + toWinPos
    --    else
    --        count = toWinPos - nowWinPos
    --    end
    local sum = 28 * 3 + self.dest_track

    --2s加速  
    local action = {}

    local timer = 0.5

    self.fadeTime = 0.5

    local colorIndex = 1

    local fadeIndex = 1

    local dex_sum = 0

    if leftTime > 5 and leftTime < 5 + (sum - 13) * 0.03 then

        dex_sum = sum - 6

        sum = 6

        timer = 0.3

        self.fadeTime = 0.35

    elseif leftTime >= 5 + (sum - 13) * 0.03 and leftTime <= 5 + (sum - 13) * 0.03 + 1.5 then

        dex_sum = 7

        sum = sum - 7

        timer = 0.03

        self.fadeTime = 0.35

    end

    for j = 1, sum do

        local indexPos = nowWinPos + j - 1 + dex_sum

        if indexPos > 27 then

            indexPos = indexPos % 28

        end

        local item = self["auto_mid"]:getChildByName("Image_light_" .. indexPos)

        action[#action + 1] = cc.CallFunc:create(function()

            local light = item:getChildByName("Image_light")

            if fadeIndex < sum then

                if fadeIndex % 3 == 0 or fadeIndex >= sum - 6 or fadeIndex <= 4 then

                    g_AudioPlayer:playEffect(game_audio_res .. "sound_car_turn.mp3")

                end

                light:runAction(cc.Sequence:create({
                    cc.CallFunc:create(function()
                        light:setVisible(true)
                        light:setOpacity(255)
                    end),
                    cc.FadeOut:create(self.fadeTime),
                    cc.CallFunc:create(function()
                        light:setVisible(false)
                    end)
                }))

            else

                g_AudioPlayer:playEffect(game_audio_res .. "sound_car_turn_end.mp3")

                light:setVisible(true)

                light:setOpacity(255)
                if g_GameController:getOpenAward() == nil then
                    return
                end
                if g_GameController:getOpenAward() then
                    -- self:showSitWinNumber( g_GameController:getOpenAward() )
                    self:showGameEndView(g_GameController:getOpenAward().m_awardBetAreaId)
                    self:play_pos_animation()
                    self:update_trand(g_GameController:getOpenAward().m_history)
                    --self._AnimationBetLayer:reverse_anima(g_GameController:getOpenAward().g_GameController:getOpenAward());
                    self:_DoSomethingLater(function() self:_Flychipex() end, 3)
                end

            end

            colorIndex = colorIndex + 1

            if colorIndex > 4 then

                colorIndex = 1

            end

            fadeIndex = fadeIndex + 1

            if fadeIndex > sum - 6 then

                self.fadeTime = self.fadeTime + 0.15

            else
                self.fadeTime = self.fadeTime - 0.05

                if self.fadeTime <= 0.35 then

                    self.fadeTime = 0.35

                end

                if fadeIndex == sum - 6 then

                    self.fadeTime = 0.35

                end
            end

        end)

        action[#action + 1] = cc.DelayTime:create(timer)

        if j > sum - 6 then

            timer = timer + 0.15

        else

            timer = timer - 0.1

            if timer <= 0.03 then

                timer = 0.03

            end

            if j == sum - 6 then

                timer = 0.3

            end

        end

    end

    self.root_node:runAction(cc.Sequence:create(action))

end

function BirdGameLayer:play_pos_animation()

    if not self.dest_track then

        return

    end
    if g_GameController:getOpenAward() == nil then
        return
    end
    local index = g_GameController:getOpenAward().m_awardType

    if index == 13 then --通赔动画

        g_AudioPlayer:playEffect(game_audio_res .. "sound_tp.mp3")

        self:play_state_animation("feiqinzoushou_tongpei")

    elseif index == 12 then --通杀动画

        g_AudioPlayer:playEffect(game_audio_res .. "sound_tc.mp3")

        self:play_state_animation("feiqinzoushou_tongchi")

    elseif index == 15 then --鲨鱼动画

        g_AudioPlayer:playEffect(game_audio_res .. "sound_sy.mp3")

        self:play_state_animation("feiqinzoushou_shayu25")

    elseif index == 14 then --金鲨鱼动画

        g_AudioPlayer:playEffect(game_audio_res .. "sound_sy.mp3")

        self:play_state_animation("feiqinzoushou_jinshax100")

    elseif index == 5 then --熊猫动画

        g_AudioPlayer:playEffect(game_audio_res .. "sound_xm.mp3")

        self:play_state_animation("feiqinzoushou_jiesuandongwu", nil, 6)

    elseif index == 8 then --老鹰动画

        g_AudioPlayer:playEffect(game_audio_res .. "sound_ly.mp3")

        self:play_state_animation("feiqinzoushou_jiesuandongwu", nil, 3)

    elseif index == 11 then --狮子动画

        g_AudioPlayer:playEffect(game_audio_res .. "sound_sz.mp3")

        self:play_state_animation("feiqinzoushou_jiesuandongwu", nil, 4)

    elseif index == 10 then --兔子动画

        g_AudioPlayer:playEffect(game_audio_res .. "sound_tz.mp3")

        self:play_state_animation("feiqinzoushou_jiesuandongwu", nil, 5)

    elseif index == 6 then --猴子动画

        g_AudioPlayer:playEffect(game_audio_res .. "sound_hz.mp3")

        self:play_state_animation("feiqinzoushou_jiesuandongwu", nil, 1)

    elseif index == 7 then --燕子动画

        g_AudioPlayer:playEffect(game_audio_res .. "sound_yz.mp3")

        self:play_state_animation("feiqinzoushou_jiesuandongwu", nil, 7)

    elseif index == 1 then --孔雀动画

        g_AudioPlayer:playEffect(game_audio_res .. "sound_kq.mp3")

        self:play_state_animation("feiqinzoushou_jiesuandongwu", nil, 2)

    elseif index == 2 then --鸽子动画

        g_AudioPlayer:playEffect(game_audio_res .. "sound_gz.mp3")

        self:play_state_animation("feiqinzoushou_jiesuandongwu", nil, 0)

    end


end

function BirdGameLayer:get_path_from_idx(index)

    local dex = self:get_trand_index(index)

    return game_light_res .. "light_" .. dex .. ".png"

end

function BirdGameLayer:game_show_bet_state(data)

    get_room_info().loop = data.loop

    for i = 1, #data.loop do

        local path = self:get_path_from_idx(data.loop[i])

        self["Image_light_" .. (i - 1)]:loadTexture(path)

    end

end

--玩家下注
function BirdGameLayer:game_user_add_bet(user)

    local player_index = 8

    if user.m_betAccountId == Player:getAccountID() then --是否是自己

        player_index = 7

        self:xiazhu_animation(7, user.m_betAreaId, user.m_betValue * 0.01)

        --self._AnimationBetLayer:play_anima(7, user.m_betAccountId, user.m_betAreaId, user.m_betValue*0.01);
        self:update_self_xiazhu_score(user.m_myAreaBet)

        self:update_user_score(user.m_curCoin * 0.01)

        --      self:update_bet_end()
        self:_RefBetButton()

        --[[ if #self.m_pShipLast > 0 then
            self.m_pShipLast = {}
            self.m_bContinue = false
            self:updateContinueBtnState()
        end
        ]]
        self:addLastChip(user.m_betValue, user.m_betAreaId)
        --table.insert(self.m_pShipLastTmp, {user.m_betAreaId, user.m_betValue})
        --table.insert(self.m_pShipLast, user.m_betAreaId)
        --table.insert(self.m_pShipLast, user.m_betValue)
        return
    end

    for i = 1, 6 do

        local panal = self["auto_user_portrait_" .. i]

        if panal.m_accountId and panal.m_accountId == user.m_betAccountId then

            player_index = i

            self:update_user_info(panal, user)

            self:xiazhu_animation(i, user.m_betAreaId, user.m_betValue * 0.01)
            --self._AnimationBetLayer:play_anima(i, user.m_betAccountId, user.m_betAreaId, user.m_betValue*0.01);
        --[[            local array = {}

            local dir = 20

            if i > 3 then

                dir = -20

            end

            local action1 = cc.MoveBy:create(0.05,cc.p(dir,0))

            array[#array + 1] = action1

            local action2 = action1:reverse()

            array[#array + 1] = action2

            panal:runAction( cc.Sequence:create(array) )
            ]]
        end

    end

    if player_index == 8 then
        self:xiazhu_animation(8, user.m_betAreaId, user.m_betValue * 0.01)
        --self._AnimationBetLayer:play_anima(8, user.m_betAccountId, user.m_betAreaId, user.m_betValue*0.01);
    end


    --  self.totalBet[ user.m_betAreaId + 1 ] = self.totalBet[ user.m_betAreaId + 1 ] + user.m_betValue
    self:update_total_xiazhu_score(user)

end

function BirdGameLayer:get_new_array_by_array(array1, array2)

    local array3 = {}

    for i = 1, #array1 do

        local new_betValue = array2[i] - array1[i]

        array3[i] = new_betValue

    end

    return array3

end

--其他玩家下注
function BirdGameLayer:user_offset_player_add_bet_ack(data)

    local totalBet = data.totalBet

    local new_total_bet_array = self:get_new_array_by_array(self.totalBet, totalBet)

    for i = 1, #new_total_bet_array do

        if new_total_bet_array[i] > 0 then

            self:show_bet_fly_coin(8, i, new_total_bet_array[i])

        end

    end

    self:update_total_xiazhu_score(data.totalBet)

end

function BirdGameLayer:show_bet_fly_coin(index, pos, num)

    local bet_arrays = {}

    self:get_coins_array_by_coins(num, 5, bet_arrays)

    for i = 1, #bet_arrays do

        self:xiazhu_animation(index, pos - 1, bet_arrays[i])

    end

end

function BirdGameLayer:get_coins_array_by_coins(coins, index, coins_array)

    local bet_value = self.game_bet_list[index]

    if not bet_value then

        return coins_array

    end

    if coins >= bet_value then

        insert_table(coins_array, bet_value)

        coins = coins - bet_value

    else

        index = index - 1

    end

    self:get_coins_array_by_coins(coins, index, coins_array)

end

function BirdGameLayer:updateMyCoin(coin)

    local myScore = self["auto_self_panel"]:getChildByName("my_coin")

    self.my_coin_num = coin

    api_label_setString_num(myScore, coin)

end

--游戏公告推送
function BirdGameLayer:show_nitify_info(data)

    print("游戏公告=================")

    if self.richText then

        self.richText:stopAllActions()

        self.richText:removeFromParent()

    end

    self.richText = api_create_richText(data)

    self.richText:setPosition(1300, 17)

    self.auto_Panel_notify:addChild(self.richText)

    self.richText:runAction(cc.Sequence:create({

        cc.CallFunc:create(function()

            self.auto_panal_notify:setVisible(true)

        end),
        cc.MoveBy:create(20, cc.p(-1800, 0)),
        cc.CallFunc:create(function()

            self.auto_panal_notify:setVisible(false)

            self.richText:removeFromParent()

            self.richText = nil

        end)
    }))

end

function BirdGameLayer:slider_changed(_sender)

    self.super.slider_changed(self, _sender)

    local name = _sender:getName()

    if name == "bet_slider" then

        local pos = cc.p(self.bet_slider:getSlidBallRenderer():getPosition())

        self.bet_value_bg:setPositionX(pos.x + 130)

        local percent = _sender:getPercent()

        local diff = self.max_bet_gold - self.min_bet_gold

        local num = self.min_bet_gold

        if percent > 0 then

            num = math.floor((self.min_bet_gold + percent * diff / 100) / 10) * 10

        end

        self.bet_value_slider:setString("" .. num)

    end

end

function BirdGameLayer:touch_began(_sender, _type)

    self.super.touch_began(self, _sender, _type)

    local name = _sender:getName()

    local subStr = string.sub(name, 1, 11)

    if subStr == "Button_bet_" then

        local Image_light = _sender:getChildByName("Image_light")

        Image_light:setOpacity(255)

        Image_light:setVisible(true)

    end

end

function BirdGameLayer:touch_move(_sender, _type)

    self.super.touch_move(self, _sender, _type)

end

function BirdGameLayer:touch_ended(_sender, _type)

    g_audio:play_effects()

    if self.super.touch_ended(self, _sender, _type) == true then

        return
        --- 基类拦截消息
    end

    local name = _sender:getName()

    local subStr = string.sub(name, 1, 11)

    if name == "auto_bt_start" then
        ----游戏开始
        g_game_proto:request_user_ready()

    elseif name == "btn_bet_continue" then

        for i = 1, #self.continue_bet_array do

            local bet_array = self.continue_bet_array[i]

            g_net:ws_request(GAMEROOM_COMMAND.MSG_GAME_USER_BET_REQ, bet_array)

        end

        self:update_continue_btn(false)

    elseif name == "auto_bt_quit" then
        ----退出房间
        local confirm = function()

            g_game_proto:request_user_quit()

        end

        g_notifyer:notify_ui_event("ui_notify_open_msg_box", {["type"] = msgbox_type.confirm, ["text"] = "是否要退出游戏?", ["fairy"] = fairy_type.cry, ["confirm"] = confirm })

    elseif name == "auto_btn_other" then

        g_net:ws_request(GAMEROOM_COMMAND.MSG_GAME_GET_OFFSET_PLAYER_ACK, {["show_ani"] = 0, ["m_accountId"] = get_my_id() })

    elseif name == "auto_bt_help" then

        game_rule.new(self.root_node, game_id)

        self.auto_panel_set:setVisible(false)

    elseif name == "auto_bt_set" then

        local setView = _zp_setting.new(self.root_node)

        setView:setLocalZOrder(10015)

        self.auto_panel_set:setVisible(false)

    elseif name == "auto_btn_more" then

        -- if self.auto_panel_set:isVisible() == false then
        --     self.auto_panel_set:setVisible( true )
        -- else
        --     self.auto_panel_set:setVisible( false )
        -- end
        game_more_info_view.new(self.root_node, game_id, 1)

    elseif name == "auto_panel_set" then

        self.auto_panel_set:setVisible(false)

    elseif name == "auto_btn_back" then

        local confirm = function()

            g_net:ws_request(FRAME_COMMAND.MSG_USER_LEAVE_ROOM_REQUEST, {})

        end

        g_notifyer:notify_ui_event("ui_notify_open_msg_box", {["type"] = 2, ["text"] = "是否要退出游戏?", ["confirm"] = confirm })

    elseif subStr == "Button_bet_" then

        --下注
        local Image_light = _sender:getChildByName("Image_light")

        Image_light:setVisible(false)

        local name_prefix = string.sub(name, 12, #name)

        local pos = tonumber(name_prefix)

        self:request_xiazhu(pos)

    elseif name == "btn_xiazhu_6" then

        self:setCustomizeNum(_sender)

    elseif subStr == "btn_xiazhu_" then

        local name_prefix = string.sub(name, 12, #name)

        self.cur_bet_index = tonumber(name_prefix)

        self:update_select_btn()

    end

end

--设置自定义筹码
function BirdGameLayer:setCustomizeNum(sender)

    local bet_value = sender:getChildByName("txt_num")

    if self.isCustomize then

        self.isCustomize = false

        --self.bet_slider_bg:setRotation( 0 )
        self.bet_slider_bg:setVisible(true)

        bet_value:setVisible(false)

        self.Image_zidingyi:setVisible(true)

    else

        self.isCustomize = true

        self.bet_slider_bg:setVisible(false)

        local num = tonumber(self.bet_value_slider:getString())

        bet_value:setVisible(true)

        bet_value:setString("" .. num)

        self.Image_zidingyi:setVisible(false)

        self.cur_bet_index = 6

        self:update_select_btn()

    end

end


function BirdGameLayer:request_xiazhu(pos)

    if self:check_xiazhu() then

        local value = self.game_bet_list[self.cur_bet_index]

        if self.cur_bet_index == 6 then

            value = tonumber(self.bet_value_slider:getString()) or 1

        end
        ConnectManager:send2GameServer(g_GameController.m_gameAtomTypeId, "CS_C2G_Bird_Bet_Req", { pos, value })
        --  g_game_proto:request_user_bet( pos - 1 , value )
    end

end

function BirdGameLayer:check_xiazhu()

    if self.table_state ~= 2 then
        TOAST("无法下注，请在下注时间进行下注！")


        return false

    end

    return true

end

function BirdGameLayer:touch_cancel(_sender, _type)

    self.super.touch_cancel(self, _sender, _type)

    local name = _sender:getName()

    local subStr = string.sub(name, 1, 11)

    if subStr == "Button_bet_" then

        local Image_light = _sender:getChildByName("Image_light")

        Image_light:setVisible(false)

    end

end

function BirdGameLayer:init_ui()

end

function BirdGameLayer:get_user_index_position(index)

    local offx = game_adapter:getAdapterOffX()

    if index == 7 then

        local auto_self_panel = ccui.Helper:seekWidgetByName(self.auto_panel, "auto_self_panel")

        auto_self_panel.self_head = ccui.Helper:seekWidgetByName(auto_self_panel, "self_head")

        local x, y = auto_self_panel.self_head:getPosition()

        return x + offx, y

    elseif index == 8 then

        local auto_btn_other = ccui.Helper:seekWidgetByName(self.auto_panel, "auto_btn_other")

        local x, y = auto_btn_other:getPosition()

        return x + offx, y

    else

        local user_panel = ccui.Helper:seekWidgetByName(self.auto_panel, "auto_user_portrait_" .. index)

        --return user_panel:getPosition()
        local x, y = user_panel:getPosition()

        return x + offx, y

    end

end

--清理自己输赢值
function BirdGameLayer:clear_round_win()

    for i = 1, 4 do

        self["auto_self_win_des_" .. i]:setVisible(false)

    end

end

--游戏结算
function BirdGameLayer:game_end_coin_change(response)

    self.game_end_data = response

    local win_coin_arr = {}

    local fail_coin_arr = {}

    for key, coin in pairs(self.table_coin_node_array) do

        local isWin = false

        for k, v in pairs(response.m_awardBetAreaId) do

            if coin and coin.tag == v + 1 then

                isWin = true

            end

        end

        if isWin then

            win_coin_arr[#win_coin_arr + 1] = coin

        else

            fail_coin_arr[#fail_coin_arr + 1] = coin

        end

    end

    self.func2 = function()

        if #win_coin_arr > 0 then

            g_AudioPlayer:playEffect("gold_jiesuan")

            for i = 1, #win_coin_arr do

                local to_pos = cc.p(self:get_user_index_position(win_coin_arr[i].index))

                local rand = math.random(3, 6) / 10

                local array = {}

                local uAction = cc.JumpTo:create(rand, to_pos, math.random(20, 50), 1)

                local ease = cc.EaseSineInOut:create(uAction)

                array[#array + 1] = ease

                array[#array + 1] = cc.CallFunc:create(function()

                    win_coin_arr[i]:setVisible(false)

                end)

                if i == #win_coin_arr then

                    array[#array + 1] = cc.DelayTime:create(0.1)


                    array[#array + 1] = cc.CallFunc:create(function()

                        self:showGameEndView()

                    end)

                end

                win_coin_arr[i]:runAction(cc.Sequence:create(array))

            end

        else

            self:showGameEndView()

        end

    end

    self.func1 = function()

        for k, v in pairs(fail_coin_arr) do

            if v then

                v:setVisible(false)

            end

        end

        self.func2()

    end

    self.func1()

end

function BirdGameLayer:showGameEndView(response)

    self:play_open_award(self.winBetPos)

    --self:showSitWinNumber( response )
    --self:clear_scene()
end

--清理场景
function BirdGameLayer:clear_scene()

    self.totalBet = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }

    self:clear_bet_text()

    self:clear_Coin()

    self:clear_light()

    self:clear_btn_light()

    self.is_already_run = nil

    self:update_bet_total_score(0)

end

function BirdGameLayer:clear_bet_text()
    for k = 1, 11 do
        local total_score = self["Button_bet_" .. k]:getChildByName("total_score")
        total_score:setString("0")
        local my_score = self["Button_bet_" .. k]:getChildByName("my_score")
        my_score:setString("0")
    end



end

--清理金币
function BirdGameLayer:clear_Coin()

    if #self.table_coin_node_array > 0 then

        for i = 1, #self.table_coin_node_array do

            if self.table_coin_node_array[i] then

                local item = self.table_coin_node_array[i];
                item:setVisible(false);
                CChipCacheManager.getInstance():putChip(item, item.m_nBetValue)
                --self.table_coin_node_array[i]:removeFromParent()
            end

        end

        self.table_coin_node_array = {}

    end

    if #self.m_betChips > 0 then
        for i = 1, #self.m_betChips do
            local item = self.m_betChips[i]
            item:setVisible(false)
            CChipCacheManager.getInstance():putChip(item, item.m_nBetValue)
        end
    end
    self.m_betChips = {}

    CChipCacheManager.getInstance():resetChipZOrder()
end


--清理灯
function BirdGameLayer:clear_light()

    for i = 0, 27 do

        local light = self["Image_light_" .. i]:getChildByName("Image_light")

        light:stopAllActions()

        light:setVisible(false)

    end

end

function BirdGameLayer:clear_btn_light()

    for i = 1, 11 do

        local Image_light = self["Button_bet_" .. i]:getChildByName("Image_light")

        Image_light:stopAllActions()

        Image_light:setOpacity(255)

        Image_light:setVisible(false)

    end

end

--开奖动画
function BirdGameLayer:play_open_award(info)

    if not info then return end

    for i = 1, 11 do

        local is_win = false

        for k, v in pairs(info) do

            if i == v then

                is_win = true

                break

            end

        end

        if is_win then

            local Image_light = self["Button_bet_" .. i]:getChildByName("Image_light")

            local array = {}

            array[#array + 1] = cc.CallFunc:create(function() Image_light:setVisible(true) end)

            array[#array + 1] = cc.Blink:create(5, 5)

            array[#array + 1] = cc.CallFunc:create(function() Image_light:setVisible(false) end)

            Image_light:runAction(cc.Sequence:create(array))

        end

    end

end

--获取筹码结束坐标
function BirdGameLayer:get_pos_by_index(nIndex)

    local index = nIndex

    local node_pos = nil

    local pos = nil

    local node_pos = self.Panel_bet_table:getChildByName("Button_bet_" .. index)

    local x, y = node_pos:getPosition()

    pos = self.Panel_bet_table:convertToWorldSpace(cc.p(x * display.scaleX, y * display.scaleY))

    if index == 1 or index == 7 then --飞禽 or 走兽

        pos.x = pos.x + math.floor(math.random(0, 60)) + 40

        pos.y = pos.y + math.floor(math.random(0, 50)) - 25

    elseif index == 6 then --鲨鱼

        pos.x = pos.x + math.floor(math.random(0, 150)) - 75

        pos.y = pos.y + math.floor(math.random(0, 60)) - 30

    else  --其他

        pos.x = pos.x + math.floor(math.random(0, 80)) - 40

        pos.y = pos.y + math.floor(math.random(0, 50)) - 25

    end

    return pos

end

--获取筹码路径
function BirdGameLayer:get_jetton_path(value)

    for k, v in pairs(self.game_bet_list) do

        if v * 0.01 == value then

            return self.game_jetton_path_list[k]

        end

    end

    return self.game_jetton_path_list[6]

end

--下注动画
function BirdGameLayer:xiazhu_animation(index, pos, value)

    local spr = self:createSpriteOfChip(value, index, pos, (index == 7))
    self.table_coin_node_array[#self.table_coin_node_array + 1] = spr

--[[    local path = self:get_jetton_path( value )

    local coin_sprite = cc.Sprite:create( path )

    coin_sprite:setScale( 0.7 )
     

    local begin_pos = cc.p( 0 , 0 )
    local node_pos = self.Panel_bet_table:getChildByName( "Button_bet_" .. pos)
    local btnArea = node_pos:getContentSize();
	local to_pos;
    local randomNumX = math.random(-30, 30);
	local randomNumY =  math.random(-30, 30);
    
     node_pos:addChild( coin_sprite )
	to_pos = cc.p((btnArea.width / 2) + randomNumX , (btnArea.height / 2) + randomNumY ); 
  --  local to_pos = self:get_pos_by_index( pos )

    if index == 7 then

        local auto_self_panel = ccui.Helper:seekWidgetByName(self.auto_panel,"auto_self_panel")

        auto_self_panel.self_head = ccui.Helper:seekWidgetByName(auto_self_panel,"self_head")

        begin_pos.x , begin_pos.y = auto_self_panel.self_head:getPosition()

        coin_sprite.index = index

    elseif index == 8 then

        local auto_btn_other = ccui.Helper:seekWidgetByName( self.auto_panel,"auto_btn_other")

        begin_pos.x , begin_pos.y = auto_btn_other:getPosition()

        begin_pos.x = begin_pos.x - 145

        coin_sprite.index = index

    else

        local user_panel = ccui.Helper:seekWidgetByName(self.auto_panel,"auto_user_portrait_" .. index)

        begin_pos.x , begin_pos.y = user_panel:getPosition() 
        coin_sprite.index = index

    end
    begin_pos.x = begin_pos.x+145
    begin_pos = node_pos:convertToNodeSpace(begin_pos);
  --  local offx = game_adapter:getAdapterOffX()

    coin_sprite:setPosition( begin_pos )

    coin_sprite:setVisible(false)

    self.table_coin_node_array[#self.table_coin_node_array + 1] = coin_sprite

    local coinArray ={}

    local mathTime = (math.random(1,9))/10

    if index ~= 8 then

        mathTime = 0

    end

    coinArray[#coinArray + 1] = cc.DelayTime:create( mathTime )

    coinArray[#coinArray + 1] = cc.CallFunc:create( function()  

        g_AudioPlayer:playEffect( game_audio_res.."gold_xiazhu.mp3" )
            
        coin_sprite:setVisible(true)

    end )

    local uAction = cc.JumpTo:create( 0.3 ,to_pos,math.floor(math.random(20,50)),1)

    local ease = cc.EaseSineInOut:create( uAction )

    local rotation = cc.RotateBy:create( 0.3 , math.random(-30,30) )

    coinArray[#coinArray + 1] = cc.Spawn:create( ease,rotation)
        
    local scale1Action = cc.ScaleBy:create(0.1,0.95*0.7)

    local scale2Action = scale1Action:reverse()

    coinArray[#coinArray + 1] = scale1Action

    coinArray[#coinArray + 1] = scale2Action

    coin_sprite:runAction( cc.Sequence:create(coinArray) )
    ]]
end

--初始化下注按钮数值
function BirdGameLayer:init_bet_btn_value()

    for i = 1, 5 do

        local txt_num = self["btn_xiazhu_" .. i]:getChildByName("txt_num")

        txt_num:setString("" .. self.game_bet_list[i] * 0.01)

    end

end

--初始化下注区赔率
function BirdGameLayer:init_bet_count(info)

    if not info then return end

    for k, v in pairs(info) do

        if self["Button_bet_" .. v.m_betAreaId] then

            local count_num = self["Button_bet_" .. v.m_betAreaId]:getChildByName("count_num")

            count_num:setString("" .. v.m_multiple)
            if v.m_betAreaId == 9 then
                count_num:setString("24")
            end
        end

        --        if k == 12 then
        --            self.jinsha_bet_num:setString( "" .. v )
        --        end
    end

end

--更新走势
function BirdGameLayer:update_trand(info)

    for i = 1, 8 do

        self["Image_trand_" .. i]:setVisible(false)

    end

    self.Image_trand_new:setVisible(false)

    if not info or #info == 0 then return end

    local count = #info

    local dx = 0

    if count > 8 then

        count = 8

        dx = #info - count

    end

    self.Image_trand_new:setVisible(true)

    local x, y = self["Image_trand_" .. count]:getPosition()

    local size = self["Image_trand_" .. count]:getContentSize()

    self.Image_trand_new:setPosition(x + size.width / 4, y + size.height / 4)

    for k, v in pairs(info) do

        self["Image_trand_" .. k]:setVisible(true)


        self["Image_trand_" .. k]:loadTexture(game_win_res .. "winning_" .. v .. ".png")

    end

end

--获取输赢类型
function BirdGameLayer:get_trand_index(index)

    if index == 0 or index == 1 or index == 2 then

        return 11

    elseif index == 3 or index == 4 or index == 5 then

        return 8

    elseif index == 6 then

        return 12

    elseif index == 7 or index == 8 or index == 9 then

        return 6

    elseif index == 10 or index == 11 or index == 12 then

        return 7

    elseif index == 13 then

        return 2

    elseif index == 14 or index == 15 or index == 16 then

        return 3

    elseif index == 17 or index == 18 or index == 19 then

        return 5

    elseif index == 20 then

        return 9

    elseif index == 21 or index == 22 or index == 23 then

        return 4

    elseif index == 24 or index == 25 or index == 26 then

        return 10

    elseif index == 27 then

        return 1

    end

    return 8

end

--倒计时
function BirdGameLayer:update_state_time(time)

    if time then

        self.daojishi_time:setString("" .. time)

    end

end

--更新游戏状态文字
function BirdGameLayer:update_state_text(m_state)

    self.table_state = m_state

    if m_state == 2 then --下注状态 0

        self.Image_game_state:loadTexture(game_room_res .. "bet_img_24.png")

    elseif m_state == 3 then --结算状态 1

        self.Image_game_state:loadTexture(game_room_res .. "bet_img_1.png")

    elseif m_state == 1 then --等待状态 2

        self.Image_game_state:loadTexture(game_room_res .. "bet_img_30.png")

    end
    self:_RefBetButton()
end

function BirdGameLayer:_RefBetButton()
    local pData = BirdData.getInstance();
    for i = 1, #self.game_bet_list do
        local btn = self["btn_xiazhu_" .. i]
        local image = btn:getChildByName("Image_bg")
        image:setVisible(i == self.cur_bet_index)

        if self.my_coin_num < self.game_bet_list[i] * 0.01 then
            image:setVisible(false)
            btn:setEnabled(false)
            btn:setColor(cc.c3b(160, 160, 160))
        else
            btn:setEnabled(true)
            btn:setColor(cc.c3b(255, 255, 255))
        end
    end

    if BirdConfig.STATE_BET ~= pData.m_nGameState then
        for i = 1, #self.game_bet_list do
            local btn = self["btn_xiazhu_" .. i]
            local image = btn:getChildByName("Image_bg")
            image:setVisible(false)
            btn:setEnabled(false)
            btn:setColor(cc.c3b(160, 160, 160))
        end
        self.btn_continue:setEnabled(false);
        self.btn_continue:setColor(cc.c3b(160,160,160));
    else
        if ((not self.m_bContinue) and BirdData.getInstance():isLastBet()) then
            self.btn_continue:setEnabled(true);
            self.btn_continue:setColor(cc.c3b(255,255,255));
        else
            self.btn_continue:setEnabled(false);
            self.btn_continue:setColor(cc.c3b(160,160,160));
        end
    end
end



--更新总下注分数
function BirdGameLayer:update_bet_total_score(score)

    local str = string.format("%.2f", score)

    self.lable_total_score:setString(str)

end

--个人信息 info={name = "zhangsan",coin = 10001,m_accountId = "10001",headImg = ""}
function BirdGameLayer:update_myself_info(info)

    if info == nil then

        return

    end

    local myPanel = ccui.Helper:seekWidgetByName(self.auto_panel, "auto_self_panel")

    myPanel.self_head = ccui.Helper:seekWidgetByName(myPanel, "self_head")

    myPanel.my_name = ccui.Helper:seekWidgetByName(myPanel, "my_name")

    myPanel.my_coin = ccui.Helper:seekWidgetByName(myPanel, "my_coin")

    --  local n_name = api_set_StringWithEllipsis( info.nickname , 5*myPanel.my_name:getFontSize() , myPanel.my_name:getFontSize() )
    myPanel.my_name:setString(info.m_nickname)

    self.my_coin_num = info.m_curCoin * 0.01
    myPanel.my_coin:setString(self.my_coin_num)
    local head = ToolKit:getHead(info.m_faceId)
    myPanel.self_head:loadTexture(head, 1)
    myPanel.self_head:setScale(0.7)



end

--更新自己分数
function BirdGameLayer:update_user_score(score)

    local my_coin = self.auto_self_panel:getChildByName("my_coin")

    self.my_coin_num = score

    my_coin:setString(score)

end

--更新下注区总分数
function BirdGameLayer:update_total_xiazhu_score(info)
    local pData = BirdData.getInstance()
    
    for index = 1, BirdConfig.AREA_COUNT do

        local total_score = self["Button_bet_" .. index]:getChildByName("total_score")
        total_score:setString("" .. (pData.m_pAreaTotalBet[index] or 0) * 0.01)

    end

    self:update_bet_total_score(pData.m_nAllTotalBet * 0.01)

end

--更新下注区自己分数
function BirdGameLayer:update_self_xiazhu_score(info)

    if not info then return end

    -- for k, v in pairs(info) do
    --     local my_score = self["Button_bet_" .. v.m_betAreaId]:getChildByName("my_score")
    --     my_score:setString(v.m_myBet * 0.01)
    -- end
    for index = 1, BirdConfig.AREA_COUNT do
        local my_score = self["Button_bet_" .. index]:getChildByName("my_score")
        my_score:setString(info[index] * 0.01)
    end
end

-- 更新用户状态
function BirdGameLayer:update_users_info(seatInfos)

    if seatInfos == nil then

        return

    end

    for i = 1, 6 do

        local auto_user_portrait_ = ccui.Helper:seekWidgetByName(self.auto_panel, "auto_user_portrait_" .. i)

        auto_user_portrait_.user_head_panel = ccui.Helper:seekWidgetByName(auto_user_portrait_, "user_head_panel_" .. i)

        local seatInfo = seatInfos[i]

        self:update_user_info(auto_user_portrait_, seatInfo)

    end

end

function BirdGameLayer:update_user_info(auto_user_portrait_, seatInfo)

    if seatInfo then

        auto_user_portrait_.m_accountId = seatInfo.m_accountId

        auto_user_portrait_.user_head_panel:setVisible(true)

        local head_img = ccui.Helper:seekWidgetByName(auto_user_portrait_.user_head_panel, "head")

        local name_label = ccui.Helper:seekWidgetByName(auto_user_portrait_.user_head_panel, "name")

        local l_score_label = ccui.Helper:seekWidgetByName(auto_user_portrait_.user_head_panel, "l_score")

        --   local n_name = api_set_StringWithEllipsis( seatInfo.nickname , 5*name_label:getFontSize() , name_label:getFontSize() )
        name_label:setString(seatInfo.m_nickname)
        l_score_label:setString((seatInfo.m_score or seatInfo.m_curCoin) * 0.01)
        --   api_label_setString_num( l_score_label , seatInfo.coin )
        if seatInfo.m_faceId then
            local head = ToolKit:getHead(seatInfo.m_faceId)
            head_img:loadTexture(head, 1)
            head_img:setScale(0.7)
        end


    else

        auto_user_portrait_.m_accountId = nil

        auto_user_portrait_.user_head_panel:setVisible(false)

    end

end

function BirdGameLayer:update_select_btn()

    for i = 1, 6 do

        local cur_btn = self["btn_xiazhu_" .. i]

        local Image_bg = cur_btn:getChildByName("Image_bg")

        Image_bg:setLocalZOrder(-1)

        if self.cur_bet_index == i then

            Image_bg:setVisible(true)

        else

            Image_bg:setVisible(false)

        end

    end

end
--[[function BirdGameLayer:check_coins( coins )

    if  self.my_coin_num >= coins + 1  then
    
        return true

    end

    return false

end

--更新下注按钮
function BirdGameLayer:update_bet_end()
    
    if self.cur_bet_index == 6 then  --自定义筹码不进行检查

        return

    end

    local num = 1

    for i = 1 , #self.game_bet_list  do

        local btn = self.auto_xiazhu_btn_panel:getChildByName("btn_xiazhu_" .. i)

        local txt_num = btn:getChildByName("txt_num")

        if self:check_coins( self.game_bet_list[i] ) == false then

            btn:setTouchEnabled( false )

            btn:setOpacity(125)

            btn:getChildByName("shadow_panel"):setVisible(true)

            txt_num:setFntFile( game_fnt_res .. "chip_disable.fnt" )

        else

            btn:setTouchEnabled( true )

            btn:setOpacity(255)

            btn:getChildByName("shadow_panel"):setVisible(false)

            txt_num:setFntFile(  game_fnt_res .. "chip_" .. i.. ".fnt" )

            num = i
        
        end

    end

    if self.cur_bet_index > num then

        self.cur_bet_index = num

        self:update_select_btn()

    end  
        
end
]]
function BirdGameLayer:render_game_start_scene(response)

    self.table_state = response.m_state
    --1-ready 2-下注 3-开奖
    if response.m_state == 3 then  --开奖状态
        self.m_openAward = response
    elseif response.m_state == 1 then --等待状态

    elseif response.m_state == 2 then  --下注状态

    end

    self.game_bet_list = response.m_chipArr or { 1, 10, 50, 100, 500, 1000 }

    self.max_bet_gold = response.m_chipArr[#response.m_chipArr] or 1000

    self.min_bet_gold = response.m_chipArr[1] or 1

    self.bet_value_slider:setString("" .. self.min_bet_gold)

    -- self.winBetPos = response.winBetPos
    self:init_bet_btn_value()

    self:update_state_text(response.m_state)

    self:init_bet_count(response.m_betArea)
    self.jinsha_bet_num:setString(response.m_goldShark)
    self:update_trand(response.m_history)

    self:update_myself_info(response.m_playerInfo)


    self:update_self_xiazhu_score(response.m_myAreaBet)


    --    if response.m_state == 2 or response.m_state == 3 then
    --        self:user_offset_player_add_bet_ack( response , true )
    --    end
    --  self:game_show_bet_state( response )
    self.dest_track = response.m_lastGridId


    self:update_total_xiazhu_score(response)

    --  self:update_users_info( response.users ) 
    --  self:update_bet_end()
    self:showLeftTime(response.m_leftTime)
    self:_RefBetButton()

    local getMinBetValue = function(coin)
        for i = #self.game_bet_list, 1, -1 do
            local value = self.game_bet_list[i];
            if value <= coin then
                return value;
            end
        end
        return nil;
    end
    --7自己的筹码
    for _key, _coin in pairs(g_GameController.m_pMyBetCoin) do
        local coin = getMinBetValue(_coin)
        while coin do
            self:_CreateStaticChip(coin * 0.01, 7, _key, true);
            _coin = _coin - coin;
            if _coin < 0 then
                _coin = 0;
            end
            coin = getMinBetValue(_coin);
        end
    end
    --8他人的筹码
    for _key, _coin in pairs(g_GameController.m_pOtenrBetCoin) do
        local coin = getMinBetValue(_coin)
        while coin do
            self:_CreateStaticChip(coin * 0.01, 8, _key, false);
            _coin = _coin - coin;
            if _coin < 0 then
                _coin = 0;
            end
            coin = getMinBetValue(_coin);
        end
    end

end

function BirdGameLayer:showSitWinNumber(data)
    if data == nil then
        return
    end
    for k, v in pairs(data.m_topPlayerBalance) do

        for i = 1, 6 do

            local auto_panel = ccui.Helper:seekWidgetByName(self.auto_panel, "auto_user_portrait_" .. i)

            if auto_panel.m_accountId == v.m_accountId then

                self:show_sit_win_number(auto_panel, v.m_profit * 0.01, cc.p(51, 95))

            end

        end

    end
    self:show_sit_win_number(self.auto_self_panel, data.m_balanceInfo.m_profit * 0.01, cc.p(68, 52))

    self:update_user_score(data.m_balanceInfo.m_curCoin * 0.01)

    --    if data.player then
    --        self:show_sit_win_number(self.auto_self_panel , data.player.currentScore , cc.p( 68 , 52 ) )
    --        self:update_user_score( data.player.coin )
    --    end
end

--显示下注区赢的特效
function BirdGameLayer:Show_bet_win_Effect(index)

    local img_win = self["niuniu_table_btn_" .. index]:getChildByName("img_win")

    if index == 2 then

        -- print( img_win )
    end

    ani_cro:bet_Win_Effect(img_win)

end

--显示用户输赢多少
--y用户自己 pos = cc.p( 68 , 52 ) , 坐在玩家 pos = cc.p( 51 , 95 ) , 庄家 pos = cc.p( 50 , -70 )
--self:show_sit_win_number( self.auto_self_panel , -450000 , cc.p( 68 , 52 ) )
function BirdGameLayer:show_sit_win_number(node_view, num, pos)

    if num == 0 then

        return

    end

    local img = nil

    local path = game_room_res .. "num_win.fnt"

    if num >= 0 then

        img = ccui.ImageView:create(game_room_res .. "douniu_table_win_zi_bg.png")

        local TextAtlas = ccui.TextBMFont:create(str, path)

        if num >= 10000 then

            num = num / 10000

            local strCoin = string.format("%.2f", num) .. "万"

            TextAtlas:setString("+" .. strCoin)

        else

            local strCoin = string.format("%.2f", num)

            TextAtlas:setString("+" .. strCoin)

        end

        img:addChild(TextAtlas)

        TextAtlas:setPosition(img:getContentSize().width / 2, img:getContentSize().height / 2)

    else

        img = ccui.ImageView:create(game_room_res .. "douniu_table_lose_zi_bg.png")

        path = game_room_res .. "num_lose.fnt"

        local TextAtlas = ccui.TextBMFont:create("", path)

        if math.abs(num) >= 10000 then

            num = math.abs(num) / 10000

            local strCoin = string.format("%.2f", num) .. "万"

            TextAtlas:setString("-" .. strCoin)

        else

            local strCoin = string.format("%.2f", math.abs(num))

            TextAtlas:setString("-" .. strCoin)

        end

        img:addChild(TextAtlas)

        TextAtlas:setPosition(img:getContentSize().width / 2, img:getContentSize().height / 2)

    end

    img:setPosition(pos)

    node_view:addChild(img)

    local array = {}

    array[#array + 1] = cc.MoveBy:create(0.3, cc.p(0, 50))

    array[#array + 1] = cc.DelayTime:create(1)

    array[#array + 1] = cc.FadeOut:create(0.5)

    array[#array + 1] = cc.CallFunc:create(function()

        img:removeFromParent()

    end)

    img:runAction(cc.Sequence:create(array))

end

--显示游戏错误提示
function BirdGameLayer:show_game_tips(str)

    self.auto_Image_tips:stopAllActions()

    self.auto_Image_tips:setVisible(true)

    self.auto_Image_tips:setOpacity(255)

    local Text_des = self.auto_Image_tips:getChildByName("Text_tips")

    Text_des:setString(str)

    local array = {}

    self.auto_Image_tips:setPositionY(400)

    array[#array + 1] = cc.MoveBy:create(0.4, cc.p(0, 60))

    array[#array + 1] = cc.FadeOut:create(0.5)

    array[#array + 1] = cc.CallFunc:create(function()

        self.auto_Image_tips:setVisible(false)

        self.auto_Image_tips:setPositionY(400)

    end)

    self.auto_Image_tips:stopAllActions()

    self.auto_Image_tips:runAction(cc.Sequence:create(array))

end

function BirdGameLayer:user_quit_room(response)

    if response.m_accountId == get_my_id() then

        g_notifyer:notify_frame_event("quit_game_state")

    end

end

-- region 飞金币动画相关

--创建一个飞行筹码 (betType:筹码类型 index:下注位置 bSelf:是否自己)
function BirdGameLayer:_FlyChip(nAccountId, nAreaId, nBetValue)

    local item = CChipCacheManager.getInstance():getChip(nBetValue)
    item.m_nAccountId = nAccountId;
    item.m_nAreaId = nAreaId;
    item.m_nBetValue = nBetValue;
    self.table_coin_node_array[#self.table_coin_node_array + 1] = item;
    item:stopAllActions();

    local time = 0
    if Player:getAccountID()  == nAccountId then
        time = 0.003
    else
        time = (math.random(0, 20) / 40)
    end

    local endpos = self:_RandomAreaPoint(nAreaId);
    local begin = self:_GetPlayerPoint(nAccountId);

    item:setScale(self.m_chip_scale)
    item:setPosition(begin)
    item:setVisible(false)

    local move_distance = cc.pGetDistance(begin, endpos)
    local move_time = move_distance / BirdConfig.BR_CHIP_SPEED
    local move_action = cc.MoveTo:create(move_time, endpos)
    local ease_action = cc.EaseOut:create(move_action, move_time + 0.3)
    local scale_action = cc.ScaleTo:create(0.15, self.m_chip_scale - 0.05)
    

    local delay = cc.DelayTime:create(time)
    local call = cc.CallFunc:create(function()
        item:setVisible(true)
        if Player:getAccountID()  == nAccountId then -- 自己的音效立刻播放
            g_AudioPlayer:playEffect(game_audio_res .. "gold_xiazhu.mp3")
        else
            if not self.m_isPlayBetSound then
                g_AudioPlayer:playEffect(game_audio_res .. "gold_xiazhu.mp3")
                -- 尽量避免重复播放
                self:_DoSomethingLater(function() self.m_isPlayBetSound = false end, math.random(2, 4) / 10)
                self.m_isPlayBetSound = true
            end
        end

    end)

    item:runAction(cc.Sequence:create(delay, call, ease_action, scale_action, nil))

    local playerIndex = self:_GetPlayerIndex(nAccountId);

    if playerIndex > 0 and playerIndex < 7 then
        local panal = self["auto_user_portrait_" .. playerIndex]
        local array = {}

        local dir = 20

        if playerIndex > 3 then

            dir = -20

        end

        array[#array + 1] = cc.DelayTime:create(time)

        local action1 = cc.MoveBy:create(0.05, cc.p(dir, 0))

        array[#array + 1] = action1

        local action2 = action1:reverse()

        array[#array + 1] = action2

        panal:runAction(cc.Sequence:create(array))
    end

    return item
end

-- 获取玩家位置
function BirdGameLayer:_GetPlayerPoint(nAccountId, nBetValue)
    local index = self:_GetPlayerIndex(nAccountId);
    return self.m_playerPos[index];
end

-- 随机下注区域的一个位置
function BirdGameLayer:_RandomAreaPoint(betAreaId)

    local n = #self.m_areaPos[betAreaId]

    local index = math.random(1, n)

    local array = self.m_areaPos[betAreaId]


    return array[index]
end

-- 克隆一个赔付的筹码对象(注意克隆的对象里面不包含任何数据信息以及纹理路径地址)
function BirdGameLayer:_CloneChip(sp)
    local item = CChipCacheManager.getInstance():getChip(sp.m_nBetValue);
    item:setAnchorPoint(sp:getAnchorPoint());
    item:setScale(sp:getScale());
    item:setPosition(sp:getPosition());
    item:setVisible(false);

    item.m_nAccountId = sp.m_nAccountId;
    item.m_nAreaId = sp.m_nAreaId;
    item.m_nBetValue = sp.m_nBetValue;

    return item;
end

function BirdGameLayer:_DoSomethingLater(call, delay)
    self.m_pAnimalUI:runAction(cc.Sequence:create(cc.DelayTime:create(delay), cc.CallFunc:create(call), nil))
end

-- 获取玩家位置
function BirdGameLayer:_GetPlayerIndex(accountId)
    if accountId == nil then
        return 8
    end

    if accountId == Player:getAccountID() then
        return 7
    end

    for i = 1, 6 do

        local auto_panel = ccui.Helper:seekWidgetByName(self.auto_panel, "auto_user_portrait_" .. i)

        if auto_panel.m_accountId == accountId then

            return i
        end

    end

    
    return 8
end

--创建一个静态放置筹码(betType:筹码类型 index:下注位置 bSelf:是否自己)
function BirdGameLayer:_CreateStaticChip(accountId, areaId, betValue)
    local item = CChipCacheManager.getInstance():getChip(betValue);
    item.m_nAccountId = accountId;
    item.m_nAreaId = areaId;
    item.m_nBetValue = betValue;
    item:stopAllActions();
    item:setVisible(true);
    item:setScale(BirdConfig.CHIP_SCALE - 0.05);
    local endPoint = self:_RandomAreaPoint(areaId);
    item:setPosition(endPoint);
    self.table_coin_node_array[#self.table_coin_node_array + 1] = item;
    return item;
end

function BirdGameLayer:_ChengeAreaBets(areaCoin)
    local getMinCoin = function (coin)
        for index = BirdConfig.BET_TYPE_COUNT, 1, -1 do
            if coin >= self.game_bet_list[index] then
                return self.game_bet_list[index];
            end
        end
        return nil;
    end
    local ret = {};
    for index = 1, BirdConfig.AREA_COUNT do 
        local cur_coin = areaCoin[index];
        if cur_coin > 0 then
            local coin = getMinCoin(cur_coin);
            while(coin) do
                cur_coin = cur_coin - coin;
                table.insert(ret, {index, coin});
                coin = getMinCoin(cur_coin);
            end
        end
    end

    return ret;
end

function BirdGameLayer:_InitDeskChips()
    local start_clock = os.clock();
    local pData = BirdData.getInstance();
    local otherBets = self:_ChengeAreaBets(pData.m_pAreaTotalBet);
    local myBets = self:_ChengeAreaBets(pData.m_pMyAreaBet);
    for index = 1, #otherBets do
        local value = otherBets[index];
        self:_CreateStaticChip(0, value[1], value[2]);
    end

    for index = 1, #myBets do
        local value = myBets[index];
        self:_CreateStaticChip(0, value[1], value[2]);
    end

    print("创建桌面筹码耗时---", os.clock() - start_clock);

end

-- 以队列方式进行飞筹码
function BirdGameLayer:_Flychipex()

    self.m_betChips = {}

    local openAward = g_GameController:getOpenAward()

    local bankerwinvec = {}     -- 庄家获取的筹码
    local bankerlostvec = {}    -- 庄家赔付筹码
    local otherwinvec = {}      -- 玩家获取的筹码
    local othervec = {}         -- 玩家所有区域筹码

    local areaMultiple = {}
    for i = 1, BirdConfig.AREA_COUNT do
        local tableNumber = self["Button_bet_" .. i]:getChildByName("count_num")
        local numberString = tableNumber:getString()
        table.insert(areaMultiple, tonumber(numberString))
    end

    for i = 1, #self.table_coin_node_array do
        local item = self.table_coin_node_array[i]
        local isWind = false
        for k = 1, #openAward.m_awardBetAreaId do
            local areaId = openAward.m_awardBetAreaId[k]
            if areaId == item.m_nAreaId then
                isWind = true
            end
        end

        if isWind == true then
            local endPos = self:_GetPlayerPoint(item.index)
            table.insert(othervec, { sp = item, endpos = endPos })
        else
            table.insert(bankerwinvec, { sp = item, endpos = self.m_bankerPos })
        end
    end

    for k, v in pairs(othervec) do
        local multiple = areaMultiple[v.sp.m_nAreaId]
        if multiple > 1 then
            for i = 0, multiple - 1 do
                local item = self:_CloneChip(v.sp)
                item:setPosition(self.m_bankerPos)
                table.insert(otherwinvec, { sp = item, endpos = v.endpos })

                local endPos = self:_RandomAreaPoint(item.m_nAreaId)
                table.insert(bankerlostvec, { sp = item, endpos = endPos })
                table.insert(self.m_betChips, item);
            end
        end
    end

    -- 飞到庄家
    local jobitem1 = {
        flytime    = BirdConfig.CHIP_FLY_TIME, -- 飞行时间
        flytimedelay = true, -- 飞行时间延长随机时间(0.05~0.15)
        flysteptime = BirdConfig.CHIP_FLY_STEPDELAY, -- 筹码队列飞行间隔
        nextjobtime = BirdConfig.CHIP_JOBSPACETIME, -- 下个任务执行间隔时间
        chips        = bankerwinvec, -- 筹码队列集合 
        preCB        = function()                                  -- 任务开始时执行的回调，此回调根据preCBExec控制只执行一次

        end,
        preCBExec    = false,
        --overCB        = function() print("庄家获胜筹码飞行完毕") end, -- 动画任务完成后回调
        hideAfterOver = true                                          -- 动画完成后隐藏
    }

    -- 飞到赔付区域
    local jobitem2 = {
        flytime    = BirdConfig.CHIP_FLY_TIME, -- 飞行时间
        flytimedelay = true, -- 飞行时间延长随机时间(0.05~0.15)
        flysteptime = BirdConfig.CHIP_FLY_STEPDELAY, -- 筹码队列飞行间隔
        nextjobtime = BirdConfig.CHIP_JOBSPACETIME, -- 下个任务执行间隔时间
        chips        = bankerlostvec, -- 筹码队列集合
        preCB        = function()                                    -- 任务开始时执行的回调，此回调根据preCBExec控制只执行一次

        end,
        preCBExec    = false,
        --overCB        = function() print("庄家获胜筹码飞行完毕") end, -- 动画任务完成后回调
        hideAfterOver = false                                          -- 动画完成后隐藏
    }


    -- 飞到玩家
    local jobitem3 = {
        flytime    = BirdConfig.CHIP_FLY_TIME, -- 飞行时间
        flytimedelay = true, -- 飞行时间延长随机时间(0.05~0.15)
        flysteptime = BirdConfig.CHIP_FLY_STEPDELAY, -- 筹码队列飞行间隔
        nextjobtime = BirdConfig.CHIP_JOBSPACETIME, -- 下个任务执行间隔时间
        chips        = otherwinvec, -- 筹码队列集合
        preCB        = function()                                    -- 任务开始时执行的回调，此回调根据preCBExec控制只执行一次

        end,
        preCBExec    = false,
        --overCB        = function() print("庄家获胜筹码飞行完毕") end, -- 动画任务完成后回调
        hideAfterOver = true                                          -- 动画完成后隐藏
    }

    -- 飞到玩家
    local jobitem4 = {
        flytime    = BirdConfig.CHIP_FLY_TIME, -- 飞行时间
        flytimedelay = true, -- 飞行时间延长随机时间(0.05~0.15)
        flysteptime = BirdConfig.CHIP_FLY_STEPDELAY, -- 筹码队列飞行间隔
        nextjobtime = BirdConfig.CHIP_JOBSPACETIME, -- 下个任务执行间隔时间
        chips        = othervec, -- 筹码队列集合
        preCB        = function()                                    -- 任务开始时执行的回调，此回调根据preCBExec控制只执行一次

        end,
        preCBExec    = false,
        --overCB        = function() print("庄家获胜筹码飞行完毕") end, -- 动画任务完成后回调
        hideAfterOver = true                                          -- 动画完成后隐藏
    }

    self.m_flyJobVec.nIdx = 1 -- 任务处理索引
    self.m_flyJobVec.flyIdx = 1 -- 飞行队列索引
    self.m_flyJobVec.jobVec = {} -- 任务对象
    self.m_flyJobVec.overCB = function()
        print("所有飞行任务执行完毕")
        self.m_flyJobVec = {}
        self:showSitWinNumber(g_GameController:getOpenAward())
    end

    table.insert(self.m_flyJobVec.jobVec, { jobitem1 })
    table.insert(self.m_flyJobVec.jobVec, { jobitem2 })
    table.insert(self.m_flyJobVec.jobVec, { jobitem3, jobitem4 })

    self:_DoFlyJob()
end

-- 执行单个的飞行任务
function BirdGameLayer:_DoFlyJob()

    -- 全部任务执行完成之前，被清理重置
    if nil == self.m_flyJobVec.nIdx or nil == self.m_flyJobVec.jobVec then return end

    -- 任务处理完了
    if self.m_flyJobVec.nIdx > #self.m_flyJobVec.jobVec then
        if self.m_flyJobVec.overCB then
            self.m_flyJobVec.overCB()
        end
        return
    end

    -- 取出一个当前需要处理的飞行任务
    local job = self.m_flyJobVec.jobVec[self.m_flyJobVec.nIdx]
    if not job then return end
    if 0 == #job then return end

    -- 按队列取出需要飞行的对象进行动画处理
    local flyvec = {}
    local mf = math.floor
    if self.m_flyJobVec.flyIdx <= BirdConfig.CHIP_FLY_SPLIT then
        for i = 1, #job do
            if job[i] then
                --for j = 1, #job[i].chips do
                local segnum = mf(#job[i].chips / BirdConfig.CHIP_FLY_SPLIT) -- 计算需要分成几段
                for m = 0, segnum do
                    local tgg = job[i].chips[m * BirdConfig.CHIP_FLY_SPLIT + self.m_flyJobVec.flyIdx]
                    if tgg then
                        table.insert(flyvec, { sptg = tgg, idx = i })
                    end                
                end
                --end
            end
        end
    end

    -- 当前队列都飞完了
    if 0 == #flyvec then
        -- 下个任务的执行
        self.m_flyJobVec.nIdx = self.m_flyJobVec.nIdx + 1
        self.m_flyJobVec.flyIdx = 1
        self:_DoSomethingLater(function()
            self:_DoFlyJob()
        end, job[1].nextjobtime) -- 多个任务时 取第一个任务的时间
        return
    end

    -- 开始飞筹码
    for i = 1, #flyvec do
        local tg = flyvec[i]
        if tg and tg.sptg then
            local ts = job[tg.idx].flytimedelay and job[tg.idx].flytime + math.random(5, 15) / 100 or job[tg.idx].flytime
            local mt = cc.MoveTo:create(ts, tg.sptg.endpos)
            if i == #flyvec then -- 最后一个筹码飞行完成后执行下一次的飞行回调
                self.m_flyJobVec.flyIdx = self.m_flyJobVec.flyIdx + 1
                self:_DoSomethingLater(function()
                    self:_DoFlyJob()
                end, job[tg.idx].flysteptime)
            end

            if job[tg.idx].hideAfterOver then
                tg.sptg.sp:runAction(cc.Sequence:create(cc.Show:create(), mt, cc.Hide:create()))
            else
                tg.sptg.sp:runAction(cc.Sequence:create(cc.Show:create(), mt))
            end
        end
    end

end


--按钮回调
--续投
function BirdGameLayer:onContinueClick()
    if self.m_bContinue then
        TOAST("你已经续投过了！")
        return
    end --已经续投过了
    local pData = BirdData.getInstance();
    if BirdConfig.STATE_BET ~= pData.m_nGameState then
        return;
    end
    if not pData:isLastBet() then
        return;
    end
    self.btn_continue:setEnabled(false)
    self:_DoSomethingLater(function() 
       if not self.m_bContinue then
           self.btn_continue:setEnabled(true) 
       end
    end, 0.2)
    local chips = self:_ChengeAreaBets(pData.m_pMyLastBet); --self:chengeLastChip()
    --发送续投消息
    ConnectManager:send2GameServer(g_GameController.m_gameAtomTypeId, "CS_C2G_Bird_ContinueBet_Req", { chips })

end


function BirdGameLayer:_SetRecordID(_recordId)
    if _recordId ~= "" then
        self.mTextRecord:setString("牌局ID:".._recordId)
    end
end

--处理消息函数
function BirdGameLayer:ON_INIT_NTY(msgId, cmd)
    local pData = BirdData.getInstance();
    self:clear_scene();
    self.game_bet_list = pData.m_pAllChipValue or { 1, 10, 50, 100, 500, 1000 }
    self.max_bet_gold =  pData.m_pAllChipValue[#pData.m_pAllChipValue] or 1000
    self.min_bet_gold = pData.m_pAllChipValue[1] or 1
    self.bet_value_slider:setString("" .. self.min_bet_gold)
    self:init_bet_btn_value()
    self:init_bet_count(pData.m_pBetArea)
    self:update_myself_info(pData.m_pPlayerInfo)
    self.dest_track = pData.m_nLastGridId
    self:update_users_info(pData.m_pTopPlayerList);
    self.jinsha_bet_num:setString(pData.m_nGoldShark)
    self:update_trand(pData.m_nHistory)
    self:_SetRecordID(pData.m_sRecordId)
    
    if BirdConfig.STATE_BET == pData.m_nGameState then
        self:update_state_text(pData.m_nGameState)
        self:update_self_xiazhu_score(pData.m_pMyAreaBet)
        self:update_total_xiazhu_score()
        self:showLeftTime(pData:getLeftTime());
        self:_RefBetButton()
        self:_InitDeskChips();
    else
        --等待下局开始
        local pos = cc.p(self.auto_mid_panel_animation:getChildByName("pos_state_animtion"):getPosition())
        self.m_pDelayStartAni = CacheManager:addSpineTo(self.auto_mid_panel_animation, "80000044/ani/qddxyjks_zi/qddxyjks_zi", 1);
        -- self.m_pDelayStartAni = sp.SkeletonAnimation:createWithJsonFile("80000044/ani/qddxyjks_zi/qddxyjks_zi.json", "80000044/ani/qddxyjks_zi/qddxyjks_zi.atlas", 1)  --特效
        -- self.auto_mid_panel_animation:addChild(self.m_pDelayStartAni, 1);
        self.m_pDelayStartAni:setAnimation(0, "animation", true);
        self.m_pDelayStartAni:setPosition(pos);
    end
end

function BirdGameLayer:ON_GAME_STATE(msgId, cmd)
    local pData = BirdData.getInstance();
    self:_SetRecordID(pData.m_sRecordId)
    if BirdConfig.STATE_BET == pData.m_nGameState then
        self.m_bContinue = false
        if self.m_pDelayStartAni then
            self.m_pDelayStartAni:removeFromParent();
            self.m_pDelayStartAni = nil;
        end
        self:clear_scene()
        self:update_state_text(2)
        self:showLeftTime(pData:getLeftTime())
        g_AudioPlayer:playEffect(BirdRes.Audio.HH_START)
        self:play_state_animation("game_star");
    elseif BirdConfig.STATE_OPEN == pData.m_nGameState then
        self:update_state_text(3)
        self:openAward(g_GameController:getOpenAward());
        self:showLeftTime(pData:getLeftTime())
    else
        if not self.m_pDelayStartAni then
            self:update_state_text(1)
            self:showLeftTime(pData:getLeftTime())
        end
    end
end

function BirdGameLayer:ON_BET(msgId, cmd)
    local pData = BirdData.getInstance();
    self:_FlyChip(cmd.m_nAccountId, cmd.m_nAreaId, cmd.m_nBetValue);
    self:update_total_xiazhu_score();
    if Player:getAccountID() == cmd.m_nAccountId then
        self:update_self_xiazhu_score(pData.m_pMyAreaBet);
        self:update_user_score(pData.m_nCurCoin * 0.01);
        self:_RefBetButton();
    end
end

function BirdGameLayer:ON_ONLINE_PLAYER_LIST_ACK(msgId, cmd)
    local pData = BirdData.getInstance();
    self:updateOnlineUserList(pData.m_pPlayerList);
end

function BirdGameLayer:ON_TOP_PLAYER_LIST_NTY(msgId, cmd)
    local pData = BirdData.getInstance();
    self:update_users_info(pData.m_pTopPlayerList);
end

function BirdGameLayer:ON_CONTINUE_BET_ACK(msgId, cmd)
    local pData = BirdData.getInstance();
    local bets = self:_ChengeAreaBets(cmd.m_pBets);
    for index = 1, #bets do
        local value = bets[index];
        self:_FlyChip(cmd.m_nAccountId, value[1], value[2]);
    end

    self:update_total_xiazhu_score();
    if Player:getAccountID() == cmd.m_nAccountId then
        self.m_bContinue = true;
        self:update_self_xiazhu_score(pData.m_pMyAreaBet);
        self:update_user_score(pData.m_nCurCoin * 0.01);
        self:_RefBetButton();
    end
end


function BirdGameLayer:_InitMsgEvent()
    addMsgCallBack(self, BirdEvent.MSG_INIT_NTY, handler(self, self.ON_INIT_NTY));
    addMsgCallBack(self, BirdEvent.MSG_GAME_STATE, handler(self, self.ON_GAME_STATE));
    addMsgCallBack(self, BirdEvent.MSG_BET, handler(self, self.ON_BET));
    addMsgCallBack(self, BirdEvent.MSG_ONLINE_PLAYER_LIST_ACK, handler(self, self.ON_ONLINE_PLAYER_LIST_ACK));
    addMsgCallBack(self, BirdEvent.MSG_TOP_PLAYER_LIST_NTY, handler(self, self.ON_TOP_PLAYER_LIST_NTY));
    addMsgCallBack(self, BirdEvent.MSG_CONTINUE_BET_ACK, handler(self, self.ON_CONTINUE_BET_ACK));
end

function BirdGameLayer:_UnMsgEvent()
    removeMsgCallBack(self, BirdEvent.MSG_INIT_NTY);
    removeMsgCallBack(self, BirdEvent.MSG_GAME_STATE);
    removeMsgCallBack(self, BirdEvent.MSG_BET);
    removeMsgCallBack(self, BirdEvent.MSG_ONLINE_PLAYER_LIST_ACK);
    removeMsgCallBack(self, BirdEvent.MSG_TOP_PLAYER_LIST_NTY);
    removeMsgCallBack(self, BirdEvent.MSG_CONTINUE_BET_ACK);
end




return BirdGameLayer