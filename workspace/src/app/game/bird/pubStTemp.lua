module(..., package.seeall)

-- 公共结构协议定义

PstBirdBalanceSvr =
{
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1    , '玩家ID'},
	{ 2		, 2		, 'm_curCoin'			, 'UINT'				, 1	   , '当前金币'},
	{ 3		, 2		, 'm_reason'			, 'SHORT'				, 1	   , '0:正常结束 1-离线退出 2-强退 3-踢人'},
}

PstBirdInitPlayerInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2, 	1, 'm_nickname'			, 'STRING'				, 1 	, '昵称' },
	{ 3, 	1, 'm_faceId'			, 'UINT'				, 1 	, '头像ID' },
	{ 4, 	1, 'm_curCoin'			, 'UINT'				, 1 	, '金币' },
}

PstBirdBalanceClt = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2,	1, 'm_profit'			, 'INT'					, 1		, '本轮净盈利'},
	{ 3,	1, 'm_curCoin'			, 'UINT'				, 1		, '结算后金币'},
}

PstBirdSceneSyncGameData =
{
	{ 1, 	1, 'm_publicGameData'		, 'PstSyncGameDataEx'	, 1		, '玩家公共游戏数据' },
}

PstBirdBetAreaMultiple =
{
	{ 1,	1, 'm_betAreaId'		 ,  'UINT'				, 1		, '下注区域id, 1:孔雀 2:鸽子 3:飞禽 4：走兽 5:熊猫 6:猴子 7:燕子 8:老鹰 9:鲨鱼 10:兔子 11:狮子'},
	{ 2,	1, 'm_multiple'		 	 ,  'UINT'				, 1		, '倍数'},
}

PstBirdAreaTotalBet =
{
	{ 1,	1, 'm_betAreaId'		 ,  'UINT'				, 1		, '下注区域id, 1:孔雀 2:鸽子 3:飞禽 4：走兽 5:熊猫 6:猴子 7:燕子 8:老鹰 9:鲨鱼 10:兔子 11:狮子'},
	{ 2,	1, 'm_totalBet'		 	 ,  'UINT'				, 1		, '此区域总下注'},
}

PstBirdTopPlayerInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2, 	1, 'm_nickname'			, 'STRING'				, 1 	, '昵称' },
	{ 3, 	1, 'm_faceId'			, 'UINT'				, 1 	, '头像ID' },
	{ 4, 	1, 'm_score'			, 'UINT'				, 1 	, '金币' },
}

PstBirdAreaMyBet =
{
	{ 1,	1, 'm_betAreaId'		 ,  'UINT'				, 1		, '下注区域id, 1:孔雀 2:鸽子 3:飞禽 4：走兽 5:熊猫 6:猴子 7:燕子 8:老鹰 9:鲨鱼 10:兔子 11:狮子'},
	{ 2,	1, 'm_myBet'		 	 ,  'UINT'				, 1		, '我在此区域的下注'},
}

PstBirdContinueBet =
{
	{ 1,	1, 'm_betAreaId'		 ,  'UINT'				, 1		, '下注区域id, 1:孔雀 2:鸽子 3:飞禽 4：走兽 5:熊猫 6:猴子 7:燕子 8:老鹰 9:鲨鱼 10:兔子 11:狮子'},
	{ 2,	1, 'm_curBet'		 	 ,  'UINT'				, 1		, '此次下注'},
}