--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成
--endregion
local Scheduler        = require("framework.scheduler")
local CCGameSceneBase    = require("src.app.game.common.main.CCGameSceneBase")
local BirdGameTableLayer    = import(".BirdGameLayer")
local DlgAlert = require("app.hall.base.ui.MessageBox")

local BirdLodingLayer = require("app.game.bird.src.BirdLodingLayer")
local BirdEvent = require("src.app.game.bird.src.BirdEvent");
local BirdData = require("src.app.game.bird.src.BirdData");
local BirdRes = require("app.game.bird.src.BirdRes");


local BirdGameScene = class("BirdGameScene", function()
    return CCGameSceneBase.new()
end)

function BirdGameScene:ctor()
    self.m_BirdMainLayer        = nil
    self.m_BirdExitGameLayer    = nil
    self.m_BirdRuleLayer        = nil
    self.m_BirdMusicSetLayer    = nil
    self.m_bIsMsgInit = false;
    self:myInit()
end

-- 游戏场景初始化
function BirdGameScene:myInit()
    -- self:loadResources()
    --BirdRoomController:getInstance():setInGame( true )  
    -- 主ui 
    --self:initBirdGameMainLayer()
    self:initBirdLodingLayer()

    self:registBackClickHandler(handler(self, self.onBackButtonClicked)) -- Android & Windows注册返回按钮
    addMsgCallBack(self, MSG_ENTER_FOREGROUND, handler(self, self.onEnterForeground)) -- 转前台
    addMsgCallBack(self, MSG_ENTER_BACKGROUND, handler(self, self.onEnterBackground)) -- 转后台 
    --addMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT, handler(self, self.socketState))
    --addMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT, handler(self,self.onSocketEventMsgRecived))
    addMsgCallBack(self, UPDATE_GAME_RESOURCE, handler(self, self.updateCallback))

    ToolKit:registDistructor(self, handler(self, self.onDestory));

end
function BirdGameScene:updateCallback()
    self:initBirdGameMainLayer()
    self.m_pBirdLodingLayer:closeView();
    
end
---- 进入场景
function BirdGameScene:onEnter()
    print("-----------BirdGameScene:onEnter()-----------------")
    ToolKit:setGameFPS(1 / 60)
end

function BirdGameScene:initBirdLodingLayer()
    self.m_pBirdLodingLayer = BirdLodingLayer.new()
    self:addChild(self.m_pBirdLodingLayer, 100);
end

-- 初始化主ui
function BirdGameScene:initBirdGameMainLayer()
    self.m_BirdMainLayer = BirdGameTableLayer.new()
    self:addChild(self.m_BirdMainLayer)
    if self.m_bIsMsgInit then
        self:__SendMsg(BirdEvent.MSG_INIT_NTY);
    end
end

function BirdGameScene:getMainLayer()
    return self.m_BirdMainLayer
end

-- 显示游戏退出界面
-- @params msgName( string ) 消息名称
-- @params __info( table )   退出相关信息
-- 显示游戏退出界面
--[[function BirdGameScene:showExitGameLayer()
    if self._Scheduler1 then
        Scheduler.unscheduleGlobal(self._Scheduler1)	
        self._Scheduler1 = nil
    end 
     
    UIAdapter:popScene()
    g_GameController.gameScene = nil
end
--]]
function BirdGameScene:onEnter()
    print("------BirdGameScene:onEnter begin--------")
    print("------BirdGameScene:onEnter end--------")
end

-- 退出场景
function BirdGameScene:onExit()
    print("------BirdGameScene:onExit begin--------")
    -- if self.m_BirdMainLayer._Scheduler1 then
    --     Scheduler.unscheduleGlobal(self.m_BirdMainLayer._Scheduler1)
    --     self.m_BirdMainLayer._Scheduler1 = nil
    -- end
    
    -- self.m_BirdMainLayer:onCleanup()
    -- removeMsgCallBack(self, MSG_ENTER_FOREGROUND)
    -- removeMsgCallBack(self, MSG_ENTER_BACKGROUND)
    -- removeMsgCallBack(self, UPDATE_GAME_RESOURCE)
    -- --removeMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT)
    -- --removeMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT)
    -- --    BirdGlobal.m_isNeedReconectGameServer = false
    -- --    BirdRoomController:getInstance():setInGame( false )
    -- --    BirdGameController:getInstance():onDestory()
    -- --   self:RemoveResources()
    print("------BirdGameScene:onExit end--------")
end

function BirdGameScene:onDestory()
    print("------BirdGameScene:onDestory begin--------")
    if self.m_BirdMainLayer._Scheduler1 then
        Scheduler.unscheduleGlobal(self.m_BirdMainLayer._Scheduler1)
        self.m_BirdMainLayer._Scheduler1 = nil
    end
    
    self.m_BirdMainLayer:onCleanup()
    removeMsgCallBack(self, MSG_ENTER_FOREGROUND)
    removeMsgCallBack(self, MSG_ENTER_BACKGROUND)
    removeMsgCallBack(self, UPDATE_GAME_RESOURCE)
    --removeMsgCallBack(self, PublicGameMsg.MS_PUBLIC_GAME_SERVER_SOCKET_CONNECT)
    --removeMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT)
    --    BirdGlobal.m_isNeedReconectGameServer = false
    --    BirdRoomController:getInstance():setInGame( false )
    --    BirdGameController:getInstance():onDestory()
    --   self:RemoveResources()

    AudioManager:getInstance():stopAllSounds()
    AudioManager:getInstance():stopMusic()

    CacheManager:removeAllExamples();

    -- 释放动画
    for _, strPathName in pairs(BirdRes.vecReleaseAnim) do
        --local strJsonName = string.format("%s%s/%s.ExportJson", Lhdz_Res.strAnimPath, strPathName, strPathName)
        ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo(strPathName)
    end
    -- 释放整图
    for _, strPathName in pairs(BirdRes.vecReleasePlist) do
        display.removeSpriteFrames(strPathName[1], strPathName[2])
    end
    -- 释放背景图
    for _, strFileName in pairs(BirdRes.vecReleaseImg) do
        display.removeImage(strFileName)
    end
    -- 释放音频
    for _, strFileName in pairs(BirdRes.vecReleaseSound) do
        AudioManager.getInstance():unloadEffect(strFileName)
    end


    print("------BirdGameScene:onDestory end--------")
end

-- 响应返回按钮事件
function BirdGameScene:onBackButtonClicked()
    g_GameController:exitReq()
    UIAdapter:popScene()
end

-- 从后台切换到前台
function BirdGameScene:onEnterForeground()
    print("从后台切换到前台")
    g_GameController:gameBackgroundReq(2)
end

-- 从前台切换到后台
function BirdGameScene:onEnterBackground()
    print("从前台切换到后台,游戏线程挂起!")
    g_GameController:gameBackgroundReq(1)
    g_GameController.m_BackGroudFlag = true
end

function BirdGameScene:clearView()
    print("BirdGameScene:clearView()")
end

-- 清理数据
function BirdGameScene:clearData()

end

--退出游戏处理
--[[function BirdGameScene:exitGame()   
    if self.m_BirdMainLayer._Scheduler1 then
        Scheduler.unscheduleGlobal(self.m_BirdMainLayer._Scheduler1)	
        self.m_BirdMainLayer._Scheduler1 = nil
    end 
     ConnectManager:send2GameServer( g_GameController.m_gameAtomTypeId,"CS_C2G_UserLeft_Req", { })
    UIAdapter:popScene()
    self:closeGameSvrConnect()
    g_GameController.gameScene = nil
    g_GameController:onDestory()
end
--]]
function BirdGameScene:ON_INIT_NTY(cmd)
    BirdData.getInstance():SET_INIT_NTY(cmd);
    self.m_bIsMsgInit = true;
    self:__SendMsg(BirdEvent.MSG_INIT_NTY);
end

function BirdGameScene:ON_GAME_READY_NTY(cmd)
    BirdData.getInstance():SET_GAME_READY_NTY(cmd);
    self:__SendMsg(BirdEvent.MSG_GAME_STATE);
end

function BirdGameScene:ON_OPEN_AWARD_NTY(cmd)
    BirdData.getInstance():SET_OPEN_AWARD_NTY(cmd);
    self:__SendMsg(BirdEvent.MSG_GAME_STATE);
end

function BirdGameScene:ON_BET_ACK(cmd)
    if 0 == cmd.m_result then
        BirdData.getInstance():SET_BET_ACK(cmd);

        self:__SendMsg(BirdEvent.MSG_BET, { m_nAccountId = cmd.m_betAccountId, m_nAreaId = cmd.m_betAreaId, m_nBetValue = cmd.m_betValue });
    else
        self:_ShowErrorMsg("BET_ACK", cmd.m_result);
    end
end

function BirdGameScene:ON_BET_NTY(cmd)
    BirdData.getInstance():SET_BET_NTY(cmd);
    self:__SendMsg(BirdEvent.MSG_GAME_STATE);
end

function BirdGameScene:ON_ONLINE_PLAYER_LIST_ACK(cmd)
    BirdData.getInstance():SET_ONLINE_PLAYER_LIST_ACK(cmd);
    self:__SendMsg(BirdEvent.MSG_ONLINE_PLAYER_LIST_ACK);
end

function BirdGameScene:ON_BACKGROUND_ACK(cmd)
    --BirdData.getInstance():SET_BACKGROUND_ACK(cmd);
end

function BirdGameScene:ON_EXIT_ACK(cmd)
    --BirdData.getInstance():SET_EXIT_ACK(cmd);
end

function BirdGameScene:ON_TOP_PLAYER_LIST_NTY(cmd)
    BirdData.getInstance():SET_TOP_PLAYER_LIST_NTY(cmd);
    self:__SendMsg(BirdEvent.MSG_TOP_PLAYER_LIST_NTY);
end

function BirdGameScene:ON_CONTINUE_BET_ACK(cmd)
    if 0 == cmd.m_result then
        BirdData.getInstance():SET_CONTINUE_BET_ACK(cmd);
        local bets = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        for index = 1, #cmd.m_continueBetArr do
            local item = cmd.m_continueBetArr[index];
            bets[item.m_betAreaId] = item.m_curBet + bets[item.m_betAreaId];
        end

        self:__SendMsg(BirdEvent.MSG_CONTINUE_BET_ACK, { m_nAccountId = cmd.m_betAccountId, m_pBets = bets });
    else
        self:_ShowErrorMsg("CONTINUE_BET_ACK", cmd.m_result);
    end

end

function BirdGameScene:__SendMsg(msgId, sender)
    if self:getMainLayer() then
        sendMsg(msgId, sender);
    end
end

function BirdGameScene:_ShowErrorMsg(key, code)
    if not self.m__pMsgCodeData then
        self.m__pMsgCodeData = {};
        local betCode = {};
        betCode["-201201"] = "非下注阶段，不能下注！";
        betCode["-201202"] = "下注区域无效！";
        betCode["-201203"] = "金币不足，下注失败！";
        betCode["-201204"] = "您下注超过个人上限！";
        betCode["-201205"] = "已达下注总上限！";
        betCode["-201206"] = "下注失败, 携带金币低于30金币！";
        betCode["-201207"] = "下注筹码非法";
        betCode["-201208"] = "开奖出错,请联系客服";
        betCode["-201209"] = "配置出错";
        betCode["-201210"] = "玩家不存在";
        betCode["-201299"] = "未知错误";
        self.m__pMsgCodeData["BET_ACK"] = betCode;

        local CONTINUE_BET_ACKCode = {};
        CONTINUE_BET_ACKCode["-201201"] = "非下注阶段，不能续投！";
        CONTINUE_BET_ACKCode["-201202"] = "下注区域无效！";
        CONTINUE_BET_ACKCode["-201203"] = "金币不足，续投失败！";
        CONTINUE_BET_ACKCode["-201204"] = "您下注超过个人上限！";
        CONTINUE_BET_ACKCode["-201205"] = "已达下注总上限！";
        CONTINUE_BET_ACKCode["-201206"] = "续投失败, 携带金币低于30金币！";
        CONTINUE_BET_ACKCode["-201207"] = "续投筹码非法";
        CONTINUE_BET_ACKCode["-201208"] = "开奖出错,请联系客服";
        CONTINUE_BET_ACKCode["-201209"] = "配置出错";
        CONTINUE_BET_ACKCode["-201210"] = "玩家不存在";
        CONTINUE_BET_ACKCode["-201299"] = "未知错误";
        self.m__pMsgCodeData["CONTINUE_BET_ACK"] = CONTINUE_BET_ACKCode;
    end

    if self.m__pMsgCodeData[key] and self.m__pMsgCodeData[key][""..code] then
        TOAST(self.m__pMsgCodeData[key][""..code]);
    end
end



return BirdGameScene