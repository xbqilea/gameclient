if not LUA_VERSION or LUA_VERSION ~= "5.3" then
	 module(..., package.seeall)
end 
require("src.app.game.bird.protoID")

-- 公共结构定义文件注册
netLoaderCfg_Templates_common = {
	"src/app/game/bird/pubStTemp",
}

-- 协议定义文件注册
netLoaderCfg_Templates	=	{	
	"src/app/game/bird/bird",
}
-------------------------注册协议-----------------------------
-- 公共结构协议注册
netLoaderCfg_Regs_common = 
{
	PstBirdBalanceSvr			=	PSTID_BIRD_BALANCESVR,
	PstBirdInitPlayerInfo 		= 	PSTID_BIRD_INITPLAYERINFO,
	PstBirdBalanceClt			=	PSTID_BIRD_BALANCECLT,
	PstBirdSceneSyncGameData	=	PSTID_BIRD_SCENE_SYNC_GAMEDATA,
	PstBirdBetAreaMultiple		=	PSTID_BIRD_BET_AREA_MULTIPLE,
	PstBirdAreaTotalBet			=	PSTID_BIRD_AREA_TOTAL_BET,
	PstBirdTopPlayerInfo		=	PSTID_BIRD_TOPPLAYERINFO,
	PstBirdAreaMyBet			=	PSTID_BIRD_AREA_MY_BET,
	PstBirdContinueBet 			= 	PSTID_BIRD_CONTINUE_BET,
}

-- 协议注册
netLoaderCfg_Regs	=	
{	
	CS_M2C_Bird_Exit_Nty					=	CS_M2C_BIRD_EXIT_NTY,
	
	CS_G2C_Bird_Init_Nty					=	CS_G2C_BIRD_INIT_NTY,
	CS_C2G_Bird_Background_Req				=	CS_C2G_BIRD_BACKGROUND_REQ,	
	CS_G2C_Bird_Background_Ack				=	CS_G2C_BIRD_BACKGROUND_ACK,
	CS_C2G_Bird_Exit_Req					=	CS_C2G_BIRD_EXIT_REQ,
	CS_G2C_Bird_Exit_Ack					=	CS_G2C_BIRD_EXIT_ACK,
	CS_C2G_Bird_OnlinePlayerList_Req		=	CS_C2G_BIRD_ONLINE_PLAYER_LIST_REQ,
	CS_G2C_Bird_OnlinePlayerList_Ack		=	CS_G2C_BIRD_ONLINE_PLAYER_LIST_ACK,
	CS_G2C_Bird_GameReady_Nty 				= 	CS_G2C_BIRD_GAMEREADY_NTY,
	CS_G2C_Bird_Bet_Nty 					= 	CS_G2C_BIRD_BET_NTY,
	CS_G2C_Bird_OpenAward_Nty 				= 	CS_G2C_BIRD_OPEN_AWARD_NTY,
	CS_C2G_Bird_Bet_Req						=	CS_C2G_BIRD_BET_REQ,
	CS_G2C_Bird_Bet_Ack 					=   CS_G2C_BIRD_BET_ACK,
	CS_G2C_Bird_TopPlayerList_Nty			=	CS_G2C_BIRD_TOP_PLAYER_LIST_NTY,
	CS_C2G_Bird_ContinueBet_Req				=	CS_C2G_BIRD_CONTINUE_BET_REQ,
	CS_G2C_Bird_ContinueBet_Ack				=	CS_G2C_BIRD_CONTINUE_BET_ACK,
	SS_M2G_Bird_GameCreate_Req				=	SS_M2G_BIRD_GAMECREATE_REQ,
	SS_G2M_Bird_GameCreate_Ack				=	SS_G2M_BIRD_GAMECREATE_ACK,
	SS_G2M_Bird_GameResult_Nty				=	SS_G2M_BIRD_GAMERESULT_NTY,
}    
--return netLoaderCfg_Regs

if LUA_VERSION  and LUA_VERSION == "5.3" then
	return netLoaderCfg_Regs
end

     