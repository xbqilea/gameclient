--region *.lua
--Date
--
--endregion

local HoxDataMgr                    = import("..manager.HoxDataMgr")
local HNLayer= require("src.app.newHall.HNLayer") 
local HandredcattleResultLayer = class("HandredcattleResultLayer", function() 
    return HNLayer.new()
end)

local scheduler = require("framework.scheduler")

function HandredcattleResultLayer.create()
    return HandredcattleResultLayer:new():init()
end

function HandredcattleResultLayer:ctor() 
end

function HandredcattleResultLayer:init()

    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    self.m_pathUI = cc.CSLoader:createNode("game/handredcattle/HandredcattleResultLayer.csb")
    self.m_pathUI:addTo(self.m_rootUI)
    local dif = (display.width-1334) / 2
    self.m_pathUI:setPositionX(dif)

--    self.m_pColorLayer = self.m_pathUI:getChildByName("color_layer")
--    self.m_pColorLayer:addClickEventListener(handler(self, self.onCloseClicked))

    self.m_pInfoNode = self.m_pathUI:getChildByName("Image_bg")
    
--    self.m_pBtnClose = self.m_pInfoNode:getChildByName("Button_close")
--    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseClicked))


    self.m_pNiuValue = {}
    self.m_pGoldValue = {}
    local nodeResult = self.m_pInfoNode:getChildByName("Node_result")
    for i = 1, 4 do
        local infop = nodeResult:getChildByName("tiandixuanhuang"..i)
        self.m_pNiuValue[i] = infop:getChildByName("sp_niutype")
        self.m_pGoldValue[i] = infop:getChildByName("result_score")
    end

    local nodeJiesuan = self.m_pInfoNode:getChildByName("Node_jiesuan")


    self.m_pGoldTotalValue = nodeJiesuan:getChildByName("selfresult_score")
    self.m_pSpWeiTouzhu = nodeJiesuan:getChildByName("sp_weitouzhu")
    self.m_pSpWeiTouzhu:setVisible(false)

    local zhuangP = self.m_pInfoNode:getChildByName("toptagbg")
    self.m_pDealerNiuValue = zhuangP:getChildByName("sp_zhuang_niutype")
    self.m_pDealerGoldValue = zhuangP:getChildByName("zhuang_score")
    self.m_pDealerName = zhuangP:getChildByName("zhuang_name")

    local pathskel = "game/handredcattle/effect/bairenniuniujiesuan_donghua/bairenniuniujiesuan_donghua.skel"
    local pathAtlas = "game/handredcattle/effect/bairenniuniujiesuan_donghua/bairenniuniujiesuan_donghua.atlas"
    self.jiesuanAnimo = sp.SkeletonAnimation:createWithBinaryFile(pathskel, pathAtlas)
    self.jiesuanAnimo:setPosition(-290, -5)
    self.m_pInfoNode:addChild(self.jiesuanAnimo)

    self:initView()
    self.indexImg = 1
    self.m_pInfoNode:setAnchorPoint(cc.p(0.5,0.5))
    self:resultLayerShowAni(self.m_pInfoNode)
   return self
end 

function HandredcattleResultLayer:onEnter()
    
end

function HandredcattleResultLayer:onExit()
    
end

function HandredcattleResultLayer:initView()
    for i = 1, 4 do
        self.m_pNiuValue[i]:setVisible(false)
        self.m_pGoldValue[i]:setVisible(false)
    end
    self.m_pSpWeiTouzhu:setVisible(false)
    self.m_pDealerNiuValue:setVisible(false)
    self.m_pGoldTotalValue:setOpacity(0)

    --庄家的钱
    local tem = HoxDataMgr.getInstance():getOpenData()
    local nMoneyNum = tem.llBankerResult or 0
    local strPath = nMoneyNum < 0 and "game/handredcattle/font/brnn_shu_01.fnt" or "game/handredcattle/font/brnn_ying_01.fnt"
    self.m_pDealerGoldValue:setFntFile(strPath)
    local str = nMoneyNum <0 and self:getFormatGoldAndNumber(nMoneyNum) or "+"..self:getFormatGoldAndNumber(nMoneyNum)
    self.m_pDealerGoldValue:setString(str)
    
    --zhuang_name
    if HoxDataMgr.getInstance():getBankerChairId() == 65535 then
        self.m_pDealerName:setString("庄家逃跑")
    else
        local banker_name = HoxDataMgr.getInstance():getBankerName()
        local name_str = LuaUtils.getDisplayNickName(banker_name, 9, true)
        if HoxDataMgr.getInstance():isBanker() then
            self.m_pDealerName:setString(name_str)
        else
            self.m_pDealerName:setString(LuaUtils.replaceWXNickName(name_str))
        end

    end
    --排行
    self:showRanksValue()
    --去播放动画
    self:doSomethingLater(handler(self, self.playResultEffect), 0.2)

    --关闭界面(这里关闭是因为结算界面会挡住庄家轮换提示)
    self:doSomethingLater(handler(self, self.closeResultView), 6.0)

    local tem = HoxDataMgr.getInstance():getOpenData()
    local nMoneyNum = tem.llFinaResult
--    for i = 1,4 do
--        nMoneyNum = nMoneyNum + tem.llAreaMyResult[i]
--    end
    local isBanker = HoxDataMgr.getInstance():isBanker()
    --nMoneyNum = isBanker and -nMoneyNum or nMoneyNum
    if nMoneyNum >= 0 then 
        self.m_pInfoNode:loadTexture("game/handredcattle/image/result/win.png",ccui.TextureResType.localType)
        self.jiesuanAnimo:setAnimation(0, "animation1", true)
    else
        self.m_pInfoNode:loadTexture("game/handredcattle/image/result/fail.png",ccui.TextureResType.localType)
        self.jiesuanAnimo:setAnimation(0, "animation2", true)
    end

    --播放声音
    if HoxDataMgr.getInstance():getUsrChipCount() == 0 and not isBanker then
        AudioManager:getInstance():playSound("game/handredcattle/sound/sound-result-nobet.mp3")
        return
    end
    local nResultMoney = tem.llFinaResult
--    for i = 1, 4 do-- DOWN_COUNT_HOX
--        nResultMoney = nResultMoney + tem.llAreaMyResult[i]
--    end

    --nResultMoney = isBanker and -nResultMoney or nResultMoney
    if nResultMoney <= 0 then
        AudioManager:getInstance():playSound("game/handredcattle/sound/sound-result-lose.mp3")
    else
        AudioManager:getInstance():playSound("game/handredcattle/sound/sound-result-win.mp3")
    end
end

function HandredcattleResultLayer:closeResultView()
    print("HandredcattleResultLayer:closeResultView")
    self:setVisible(false)
    --self:close()
end

--function HandredcattleResultLayer:onCloseClicked()
--    AudioManager:getInstance():playSound("game/handredcattle/sound/sound-close.mp3")
--    --self:closeResultView()
--    self:resultLayerhideAni(self.m_pInfoNode)
--end

function HandredcattleResultLayer:showRanksValue()
    local tem = HoxDataMgr.getInstance():getRankData()
    local count = #tem
    local nodeRank = self.m_pInfoNode:getChildByName("Node_rank")
    for i = 1, 3 do
        local pRankIndex = nodeRank:getChildByName("userinfolist"..i)
        pRankIndex:setVisible(i <= count)

        local pRankName = pRankIndex:getChildByName("Text_username")
        if i <= count then
            local name = tem[i].m_nick
            pRankName:setString(name)
        end
        pRankName:setVisible(i <= count)

        local pRankGold = pRankIndex:getChildByName("Text_userscore")
        if i <= count then
            pRankGold:setString(tem[i].m_score*0.01)
        end
        pRankGold:setVisible(i <= count)
    end
end

function HandredcattleResultLayer:playResultEffect()
    --牛几的动画
    self.m_pDealerNiuValue:setVisible(true)
    local nDealerValue = HoxDataMgr.getInstance().m_cbCardType[1]
    self.m_pDealerNiuValue:loadTexture(string.format("game/handredcattle/image/plist/handredcattle-result/gui-niuniu-niu-%d.png",nDealerValue), ccui.TextureResType.plistType)
    
    self.m_pDealerNiuValue:setScale(1.65)
    local pAction1 = cc.ScaleTo:create(0.125, 0.65)
    local pAction2 = cc.ScaleTo:create(0.083, 1)
    local delay = cc.DelayTime:create(0.2)
    local callBack = cc.CallFunc:create(function()
        self:playImageEffect()
    end)
    self.m_pDealerNiuValue:runAction(cc.Sequence:create(pAction1,pAction2,delay,callBack))
    AudioManager:getInstance():playSound("game/handredcattle/sound/sound-tie.mp3")
end

function HandredcattleResultLayer:playImageEffect(target)
    local pTarget = target
    if not pTarget then
        --表示第一个
        self.m_pNiuValue[1]:setVisible(true)
        local nDealerValue = HoxDataMgr.getInstance().m_cbCardType[2]
        self.m_pNiuValue[1]:loadTexture(string.format("game/handredcattle/image/plist/handredcattle-result/gui-niuniu-niu-%d.png",nDealerValue), ccui.TextureResType.plistType)

        self.m_pNiuValue[1]:setTag(2)
        
        self.m_pNiuValue[1]:setScale(1.65 * 0.7)
        local pAction1 = cc.ScaleTo:create(0.09, 0.65 * 0.7)
        local pAction2 = cc.ScaleTo:create(0.055, 1)
        local delay = cc.DelayTime:create(0.18)
        local callBack = cc.CallFunc:create( function()
            self:playImageEffect(self.m_pNiuValue[1])
        end)
        self.m_pNiuValue[1]:runAction(cc.Sequence:create(pAction1,pAction2,delay,callBack))
        AudioManager:getInstance():playSound("game/handredcattle/sound/sound-tie.mp3")
    else
        local tag = pTarget:getTag()
        if(tag >= 5) then
            --图片动画播放结束,播放钱的动画
            --self:playLabelOneEffect()
            self:doSomethingLater(handler(self, self.playTotalEffect), 0.15)
            return
        end

        self.m_pNiuValue[tag]:setVisible(true)
        local nNiuValue = HoxDataMgr.getInstance().m_cbCardType[tag + 1]
        self.m_pNiuValue[tag]:loadTexture(string.format("game/handredcattle/image/plist/handredcattle-result/gui-niuniu-niu-%d.png",nNiuValue), ccui.TextureResType.plistType)
        self.m_pNiuValue[tag]:setTag(tag + 1)
        
        self.m_pNiuValue[tag]:setScale(1.65 * 0.7)
        local pAction1 = cc.ScaleTo:create(0.09, 0.65 * 0.7)
        local pAction2 = cc.ScaleTo:create(0.055, 1)
        local delay = cc.DelayTime:create(0.18)
        local callBack = cc.CallFunc:create( function()
            self:playImageEffect(self.m_pNiuValue[tag])
        end)
        self.m_pNiuValue[tag]:runAction(cc.Sequence:create(pAction1,pAction2,delay,callBack))
        AudioManager:getInstance():playSound("game/handredcattle/sound/sound-tie.mp3")
    end
end

function HandredcattleResultLayer:playLabelOneEffect()
    self.m_pGoldValue[1]:setVisible(true)
    local tem = HoxDataMgr.getInstance():getOpenData()
    local isBanker = HoxDataMgr.getInstance():isBanker()
    local nMoneyNum = tem.llAreaMyResult[1]
    nMoneyNum = isBanker and -nMoneyNum or nMoneyNum
    local strPath = nMoneyNum < 0 and "game/handredcattle/font/brnn_shu_01.fnt" or "game/handredcattle/font/brnn_ying_01.fnt"
    self.m_pGoldValue[1]:setFntFile(strPath)
    local fuhao = nMoneyNum < 0 and "" or "+"
    self.m_pGoldValue[1]:setString(fuhao..self:getFormatGoldAndNumber(nMoneyNum))
    self:doSomethingLater(handler(self, self.playLabelTwoEffect), 0.15)
end

function HandredcattleResultLayer:playLabelTwoEffect()
    self.m_pGoldValue[2]:setVisible(true)
    local tem = HoxDataMgr.getInstance():getOpenData()
    local isBanker = HoxDataMgr.getInstance():isBanker()
    local nMoneyNum = tem.llAreaMyResult[2]
    nMoneyNum = isBanker and -nMoneyNum or nMoneyNum
    local strPath = nMoneyNum < 0 and "game/handredcattle/font/brnn_shu_01.fnt" or "game/handredcattle/font/brnn_ying_01.fnt"
    self.m_pGoldValue[2]:setFntFile(strPath)
    local fuhao = nMoneyNum < 0 and "" or "+"
    self.m_pGoldValue[2]:setString(fuhao..self:getFormatGoldAndNumber(nMoneyNum))
    self:doSomethingLater(handler(self, self.playLabelThreeEffect), 0.15)
end

function HandredcattleResultLayer:playLabelThreeEffect()
    self.m_pGoldValue[3]:setVisible(true)
    local tem = HoxDataMgr.getInstance():getOpenData()
    local isBanker = HoxDataMgr.getInstance():isBanker()
    local nMoneyNum = tem.llAreaMyResult[3]
    nMoneyNum = isBanker and -nMoneyNum or nMoneyNum
    local strPath = nMoneyNum < 0 and "game/handredcattle/font/brnn_shu_01.fnt" or "game/handredcattle/font/brnn_ying_01.fnt"
    self.m_pGoldValue[3]:setFntFile(strPath)
    local fuhao = nMoneyNum < 0 and "" or "+"
    self.m_pGoldValue[3]:setString(fuhao..self:getFormatGoldAndNumber(nMoneyNum))
    
    self:doSomethingLater(handler(self, self.playLabelFourEffect), 0.15)
end

function HandredcattleResultLayer:playLabelFourEffect()
    self.m_pGoldValue[4]:setVisible(true)
    local tem = HoxDataMgr.getInstance():getOpenData()
    local isBanker = HoxDataMgr.getInstance():isBanker()
    local nMoneyNum = tem.llAreaMyResult[4]
    nMoneyNum = isBanker and -nMoneyNum or nMoneyNum
    local strPath = nMoneyNum < 0 and "game/handredcattle/font/brnn_shu_01.fnt" or "game/handredcattle/font/brnn_ying_01.fnt"
    self.m_pGoldValue[4]:setFntFile(strPath)
    local fuhao = nMoneyNum < 0 and "" or "+"
    self.m_pGoldValue[4]:setString(fuhao..self:getFormatGoldAndNumber(nMoneyNum))
    
    self:doSomethingLater(handler(self, self.playTotalEffect), 0.15)

end

function HandredcattleResultLayer:playTotalEffect()
    local pAction1 = cc.FadeIn:create(0.25)
    self.m_pGoldTotalValue:runAction(pAction1) 
    local isBanker = HoxDataMgr.getInstance():isBanker()
    if HoxDataMgr.getInstance():getUsrChipCount() == 0 and not isBanker then
        --未下注 
        self.m_pGoldTotalValue:setString("")
        self.m_pSpWeiTouzhu:setVisible(true)
        return
    end

    local tem = HoxDataMgr.getInstance():getOpenData()
    local nMoneyNum = tem.llFinaResult
--    for i = 1,4 do
--        nMoneyNum = nMoneyNum + tem.llAreaMyResult[i]
--    end

--    if (not isBanker) and (nMoneyNum < 0) then
--        --未中奖
--        self.m_pGoldTotalValue:setString("")
--        --self.m_pSpWeiTouzhong:setVisible(true)
--        return
--    end

--    local isBanker = HoxDataMgr.getInstance():isBanker()
--    nMoneyNum = isBanker and -nMoneyNum or nMoneyNum
    local strPath = nMoneyNum < 0 and "game/handredcattle/font/bjl_shu_01.fnt" or "game/handredcattle/font/bjl_ying_01.fnt"
    self.m_pGoldTotalValue:setFntFile(strPath)

    self.m_pGoldTotalValue:setOpacity(255)
    local str = nMoneyNum < 0 and self:getFormatGoldAndNumber(nMoneyNum) or "+"..self:getFormatGoldAndNumber(nMoneyNum)
    self.m_pGoldTotalValue:setString( "结算 " .. str )
end

function HandredcattleResultLayer:effectEnd(armature, movementEventType, name)
    if armature == nil then return end
    armature:removeFromParent()
end

function HandredcattleResultLayer:doSomethingLater(call, delay)
    self:runAction(cc.Sequence:create(cc.DelayTime:create(delay), cc.CallFunc:create(call)))
end

function HandredcattleResultLayer:getFormatGoldAndNumber(gold)
    return LuaUtils.getFormatGoldAndNumberAndZi(gold)
--    local yuangold = gold
--    local goldabs = math.abs(gold)
--    if goldabs<10000 then
--        return yuangold<0 and "-"..goldabs or "+"..goldabs
--    end

--    if goldabs > 99999994 and goldabs <100000000 then goldabs = 99999990 end
--    local goldNumStr = goldabs>=100000000 and string.format("%.4f",goldabs/100000000) or string.format("%.1f",goldabs/10000)
--    local danwei = goldabs>=100000000 and "y" or "w"
--    local num = goldabs>=100000000 and 5 or 2
--    local xiaoshu = string.sub(goldNumStr,-num)
--    local zhenshu = math.floor(goldNumStr)
--    --local FormatGold = LuaUtils.FormatNumber(zhenshu)
--    local FormatGold = zhenshu
--    local goldStr = FormatGold..xiaoshu..danwei
--    goldStr = yuangold<0 and "-"..goldStr or "+"..goldStr
--    return goldStr
end
--结算界面关闭
function HandredcattleResultLayer:resultLayerhideAni(pNode)
    if self.isHide then return end
    self.isHide = true
    pNode:stopAllActions()
    pNode:setScale(1.0)
    local scaleTo = cc.ScaleTo:create(0.4, 0.0)
    local ease = cc.EaseBackIn:create(scaleTo)
    local callback = cc.CallFunc:create(function ()
        self:setVisible(false)
    end)
    local seq = cc.Sequence:create(ease, callback)
    pNode:runAction(seq)
end


--结算界面出现动画
function HandredcattleResultLayer:resultLayerShowAni(pNode)
     if (pNode ~= nil)then
        pNode:stopAllActions()
        pNode:setScale(0.0)
        local show = cc.Show:create()
        local scaleTo = cc.ScaleTo:create(0.4, 1.0)
        local ease = cc.EaseBackOut:create(scaleTo)
        local seq = cc.Sequence:create(show,ease)
        pNode:runAction(seq)
    end
end
return HandredcattleResultLayer