--region *.lua
--Date
--
--endregion
local HNLayer= require("src.app.newHall.HNLayer") 
local HandredcattleRuleLayer = class("HandredcattleRuleLayer", function()
    return HNLayer.new()
end)
local HandredcattleRes              = import("..scene.HandredcattleRes") 

function HandredcattleRuleLayer.create()
    HandredcattleRuleLayer.instance_ = HandredcattleRuleLayer.new()
    return HandredcattleRuleLayer.instance_
end

function HandredcattleRuleLayer:ctor()
    
   
    self:init()
end

function HandredcattleRuleLayer:init()
    --self:initVar()
    self:initCSB()
    --self:initView()
end 
 
--function HandredcattleRuleLayer:initVar()
--    --self.m_nSelectedType = 1
--end

function HandredcattleRuleLayer:initCSB()
  --  self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_NORMAL, FixLayer.HIDE_DLG_NORMAL)
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    self.m_pathUI = cc.CSLoader:createNode(HandredcattleRes.CSB_GAME_RULE)
    self.m_pathUI:addTo(self.m_rootUI)
    local diffX = 145 - (1624 - display.size.width) / 2
    self.m_pathUI:setPositionX(diffX)

    self.m_pathUI:getChildByName("Panel_2"):hide()
   
    self.m_rootNode       = self.m_pathUI:getChildByName("Panel_1")
    self.m_pBtnClose      = self.m_rootNode:getChildByName("BTN_close")
--    self.m_pBtnJieShao    = self.m_rootNode:getChildByName("BTN_jieshao")
--    self.m_pBtnGuiZe      = self.m_rootNode:getChildByName("BTN_wanfa")
--    self.m_spJishaoNormal = self.m_rootNode:getChildByName("Sprite_1")
--    self.m_spJishaoSelect = self.m_rootNode:getChildByName("Sprite_1_1")
--    self.m_spWanfaNormal  = self.m_rootNode:getChildByName("Sprite_2")
--    self.m_spWanfaselect  = self.m_rootNode:getChildByName("Sprite_2_1")
--    self.m_svJieshao      = self.m_rootNode:getChildByName("SV_jieshao")
--    self.m_svRule         = self.m_rootNode:getChildByName("SV_wanfa")

    -- 设置税率提示
--    local tip = self.m_rootNode:getChildByName("Text_1")
--    local str = LuaUtils.getLocalString("STRING_236")
--    tip:setString( string.format(str, CBetManager.getInstance():getGameTax() * 100) )

    self.m_pBtnClose:addTouchEventListener(handler(self, self.onReturnClicked))
--    self.m_pBtnJieShao:addTouchEventListener(function()
--        self:onSwitchClicked(1)
--    end)
--    self.m_pBtnGuiZe:addTouchEventListener(function()
--        self:onSwitchClicked(2)
--    end)
--    self.m_svJieshao:setScrollBarEnabled(false)
--    self.m_svRule:setScrollBarEnabled(false)

    self.m_Panel_4 = self.m_pathUI:getChildByName("Panel_4")
    self.m_Panel_4:addTouchEventListener(handler(self, self.onReturnClicked))

--    if ClientConfig.getInstance():getIsOtherChannel() then
--        local sprite = self.m_rootNode:getChildByName("SV_wanfa"):getChildByName("Sprite_11")
--        sprite:setTexture("game/handredcattle/image/gui-niuniu-gz-1.png")
--    end
end

function HandredcattleRuleLayer:onReturnClicked(sender,eventType)
     if eventType ~=ccui.TouchEventType.ended then
                return
            end
    AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_CLOSE)
    self:close()
end

--function HandredcattleRuleLayer:initView()
--    self:updateView(self.m_nSelectedType)
--end 

--function HandredcattleRuleLayer:onSwitchClicked(nTag)
--    AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_BUTTON)
--    if not nTag then return end
--    self:updateView(nTag)
--end

--function HandredcattleRuleLayer:updateView( nType)
--    if not nType then return end
--    local isVisible = (nType == 1)
--    self.m_spJishaoSelect:setVisible(isVisible)
--    self.m_spWanfaselect:setVisible(not isVisible)
--    self.m_svJieshao:setVisible(isVisible)
--    self.m_svRule:setVisible(not isVisible)
--    self.m_nSelectedType = nType

--end

return HandredcattleRuleLayer