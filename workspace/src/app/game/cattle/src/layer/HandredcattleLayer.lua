--region *.lua
--Date
--
--endregion

local HoxDataMgr                    = import("..manager.HoxDataMgr")
local NiuNiuCBetManager             = import("..manager.NiuNiuCBetManager")
local HandredcattlePokerMgr         = import("..manager.HandredcattlePokerMgr") 
local Handredcattle_Events          = import("..scene.HandredcattleEvent")
local HandredcattleRes              = import("..scene.HandredcattleRes")
local HandredcattleTrendLayer       = import(".HandredcattleTrendLayer")
local HandredcattleRuleLayer        = import(".HandredcattleRuleLayer")
local HandredcattleOtherInfoLayer   = import(".HandredcattleOtherInfoLayer")
local HandredcattleBankerLayer   = import(".HandredcattleBankerLayer")
local Handredcattle_Const           = import("..scene.Handredcattle_Const")
local CBetManager                   = require("src.app.game.common.util.CBetManager")  
local HandredcattleResultlayer      = import(".HandredcattleResultlayer")  
local HandredcattleApplyLayer      = import(".HandredcattleApplyLayer")  
local GameRecordLayer= require("src.app.newHall.childLayer.GameRecordLayer")
local GameSetLayer= require("src.app.newHall.childLayer.SetLayer") 
local ChatLayer = require("src.app.newHall.chat.ChatLayer") 
local scheduler           = require("framework.scheduler") 
local CHIP_FLY_STEPDELAY            = 0.02 --筹码连续飞行间隔
local CHIP_FLY_TIME                 = 0.40 --飞筹码动画时间
local CHIP_JOBSPACETIME             = 0.60 --飞筹码任务队列间隔
local CHIP_FLY_SPLIT                = 20 -- 飞行筹码分组 分XX次飞完
local CHIP_ITEM_COUNT               = 4 --下注项数
local HNLayer= require("src.app.newHall.HNLayer")
local HandredcattleLayer = class("HandredcattleLayer", function()
    return HNLayer.new()
end)

--region 初始化 

function HandredcattleLayer:ctor()
    self:setNodeEventEnabled(true)

    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    self.m_pathUI = cc.CSLoader:createNode(HandredcattleRes.CSB_GAME_MAIN)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0, diffY))
    self.m_pathUI:addTo(self.m_rootUI)

    self.isNewView = true
    self:init()
end

function HandredcattleLayer:init()
    math.randomseed(tostring(os.time()):reverse():sub(1,7))
    self:initVar()
    self:initCCB()
    self:initNode()
    return self
end

function HandredcattleLayer:onEnter()
    self:initGame()
    self:playwBgEffect()
  --  self:startGameEvent()
    self:initEvent()
    self:initUpdate()
end

function HandredcattleLayer:onExit()
    self:stopAllActions()
    self:stopUpdate()
    self:unRegistNetCallback()
  --  self:stopGameEvent()
    self:removeAllChildren()
    AudioManager:getInstance():stopAllSounds() --bug5282
end

function HandredcattleLayer:initVar()
    self.isOpenMenu                 = false
    self.isPlayingZuoZhuang         = false
    self._bIsShowSelfHeadAtTable    = false
    self.m_nLastZhuangId            = INVALID_CHAIR
    self.nIndexChoose               = 0
    self.m_pStatu                   = nil
    self._bIsPlayClock              = false
    self._bIsPlayDaojishi           = false
    self.pChatNoticeEffect          = nil
    self.m_vecSpriteOther           = {}
    self._bIsPlaySound              = false
    self.m_isPlayBetSound           = false -- 其他人下注筹码音效播放状态
    self.movementNames              = {}
    self.movementNames1             = {}
    self.m_pSendCardsArmatures      = {}
    self.m_nCardsIndex              = 0
    self.m_nPlayersIndex            = 0
    self.m_vecSpriteSelf            = {}
    self.m_pLbMyBettingGold         = {}
    self.m_pLbBettingGold           = {}
    self.m_pControlButtonJetton     = {}
    self.m_pControlButtonAnimal     = {}
    self.m_pWinEffect               = {}
    self.cardSp                     = {}
    for i = 1, 5 do
        self.cardSp[i]              = {}
    end
    self.m_pAnimWin                 = {}
    self.m_nodeNVPos                = {}

    self.cardPos                    = {}
    self.pokerPos                   = {}
    self.pokerPathPos               = {}
    self.m_othersPos                = nil
    self.m_selfPos                  = nil
    self.m_bankPos                  = cc.p(420,676)
    self.m_sysbankPos               = cc.p(445,693)
    self.m_flyJobVec                = {}
end

--ui对象引用初始化
function HandredcattleLayer:initCCB()
    ------------node---------------
--    self.m_pTalkNode            = ccui.Helper:seekWidgetByName(self.m_pathUI, "Node_talk")
    self.m_rootNode             = self.m_pathUI:getChildByName("Panel_node_base")
        --筹码层
    self.m_pNodeChipSpr         = self.m_rootNode:getChildByName("Panel_node_chip")
        --需要播入场动画
    self.m_pNodeLeft            = self.m_pathUI:getChildByName("Panel_node_left")
    self.m_pNodeMiddle          = self.m_rootNode:getChildByName("Panel_node_middle")
    self.m_pNodeTop             = self.m_rootNode:getChildByName("Panel_node_top_info")
    self.m_pNodeBottomButton    = self.m_rootNode:getChildByName("Panel_node_bottom_button")
    self.m_pNodeTopButton       = self.m_pathUI:getChildByName("Panel_node_top")
    self.m_pNodeTopClock        = self.m_pathUI:getChildByName("Panel_node_clock")
    
    self.m_pNodeTopRight        = self.m_pathUI:getChildByName("Panel_node_top_right")
    self.m_pNodeBottomInfo      = self.m_pathUI:getChildByName("Panel_node_bottom")
    self.m_pSpRoomTable         = self.m_rootNode:getChildByName("Sprite_room_table")
        ----
--    self.m_pSenderCardsNode     = self.m_pathUI:getChildByName("Panel_node_sendCard")

    self.m_pNodeBanker          = self.m_pNodeTop:getChildByName("Node_banker")
    self.m_pRootNodeMenu        = self.m_pathUI:getChildByName("Panel_node_menu")
    self.m_pNodeClock           = self.m_pNodeTopClock:getChildByName("IMG_clock")
    self.m_pNodeTip             = self.m_pathUI:getChildByName("Panel_node_tip")
    self.m_pNodeClipMenu        = self.m_pathUI:getChildByName("Panel_node_clip")
    self.m_pNodeDlg             = self.m_pathUI:getChildByName("Panel_dialog") 
    self.m_pNodeTrend             = self.m_pathUI:getChildByName("Panel_node_trend") 
    ------------text---------------
    self.m_pLbUsrGold           = self.m_pNodeBottomInfo:getChildByName("Node_usergold"):getChildByName("TXT_player_gold_num")
    self.m_pLbZhuangGold        = self.m_pNodeBanker:getChildByName("TXT_zhuang_gold")
    self.m_pLbAskNum            = self.m_pNodeTop:getChildByName("TXT_ask_num")
    --self.m_pLbResultGold        = self.m_pNodeTip:getChildByName("TXT_result_gold")
    self.m_pLbOtherNum          = self.m_pNodeLeft:getChildByName("TXT_other_num")
    self.m_BtnZhuang        = self.m_pNodeBanker:getChildByName("BTN_zhuang")
    self.m_pLbResultSelf        = self.m_pNodeTip:getChildByName("TXT_result_goldself")
    self.m_pLbResultOther       = self.m_pNodeTip:getChildByName("TXT_result_goldother")
    self.m_pLbResultBanker      = self.m_pNodeTip:getChildByName("TXT_result_goldbanker")

    --调整位置（未调整ui）
    self.m_pLbResultBanker:setPositionY(self.m_pLbResultBanker:getPositionY() + 100)

    for i = 1, Handredcattle_Const.DOWN_COUNT_HOX  do -- 初始化4个玩家的区域
        self.m_pLbMyBettingGold[i]  = self.m_pNodeMiddle:getChildByName( string.format("node_bet_%d",i)):getChildByName("TXT_my_bet_num")
        self.m_pLbBettingGold[i]    = self.m_pNodeMiddle:getChildByName( string.format("node_bet_%d",i)):getChildByName("TXT_bet_num")
    end
    self.m_pLbCountTime         = self.m_pNodeClock:getChildByName("TXT_count_time")
    self.m_pSpStatus            = self.m_pNodeClock:getChildByName("Image_status")
    self.m_pLbTotalBetting      = self.m_pNodeMiddle:getChildByName("node_info"):getChildByName("TXT_total_betting")
    self.m_pLbRemaingBetting    = self.m_pNodeMiddle:getChildByName("node_info"):getChildByName("TXT_remain_betting")
  --  self.m_pNodeMiddle:getChildByName("node_info"):getChildByName("Image_3"):setVisible(false)
    ------------Sprite-------------
    self.m_pNodeMenu        = self.m_pRootNodeMenu:getChildByName("IMG_menu_bg")
     self.m_pNodeMenu:setVisible(false)
    self.m_pLianzhuangImage = self.m_pNodeTip:getChildByName("Sprite_lianzhuang")
    ------------Button-------------
    for i = 1, Handredcattle_Const.JETTON_ITEM_COUNT do
        self.m_pControlButtonJetton[i] = self.m_pNodeBottomButton:getChildByName( string.format("BTN_jetton_%d",i))
        --self.m_pControlButtonJetton[i]:setContentSize(cc.size(109, 116))
    end
    for i = 1, Handredcattle_Const.DOWN_COUNT_HOX  do
        self.m_pControlButtonAnimal[i] = self.m_pNodeMiddle:getChildByName( string.format("node_bet_%d",i)):getChildByName("BTN_bet")
    end
    self.m_pBtnClearBet     = self.m_pNodeBottomButton:getChildByName("BTN_clear_bet")
    self.m_pBtnClearBet:setVisible(false)
    self.m_pBtnContinue     = self.m_pNodeBottomButton:getChildByName("BTN_continue")
    self.m_pDealerBtn       = self.m_pNodeTop:getChildByName("BTN_dealer")
    --self.m_pRecordBtn       = self.m_pNodeTopRight:getChildByName("BTN_record")
    self.m_pRuleBtn         = self.m_pNodeMenu:getChildByName("BTN_rule")
    self.m_pBankmenuBtn     = self.m_pNodeMenu:getChildByName("BTN_menubank")
    self.m_pBankBtn         = self.m_pNodeBottomInfo:getChildByName("BTN_bank")
--    self.m_pChatBtn     = ccui.Helper:seekWidgetByName(self.m_pathUI, "BTN_chat")
    self.m_pBtnMenuPop      = self.m_pNodeTopRight:getChildByName("BTN_menu_pop")
    self.m_pBtnMenuPush     = self.m_pNodeClipMenu:getChildByName("BTN_bg")
    self.m_pBtnReturn       = self.m_pNodeTopButton:getChildByName("BTN_return")
    self.m_pBtnSound        = self.m_pNodeMenu:getChildByName("BTN_sound")
    self.m_pVoiceBtn        = self.m_pNodeMenu:getChildByName("BTN_music")
    self.m_pBtnOtherInfo    = self.m_pNodeLeft:getChildByName("BTN_other_player")
    ------------AnimNode-----------
    self.m_pAnimNV          = self.m_rootNode:getChildByName("Arm_node_nv")
    self.m_pNodeShowResult  = self.m_pathUI:getChildByName("Panel_node_anim")

    self.m_pNodeCard        = self.m_rootNode:getChildByName("Node_cardani")

    self.mTextRecord = UIAdapter:CreateRecord(nil, 24)
    self.m_pBtnReturn:getParent():addChild(self.mTextRecord)
    self.mTextRecord:setAnchorPoint(cc.p(0, 1))
	local buttonsize = self.m_pBtnReturn:getContentSize()
	local buttonpoint = cc.p(self.m_pBtnReturn:getPosition())
	self.mTextRecord:setPosition(cc.p(buttonpoint.x - buttonsize.width / 2, buttonpoint.y - buttonsize.height / 2 - 10))
    
--     --聊天界面
--    self.m_newChatLayer = ChatLayer:create()
--    self.m_newChatLayer:initData({1,2})
--    self.m_newChatLayer:setPlayerUpdateWay(false)
--    self.m_newChatLayer:setBtnVisible(true,true,false,false)

--    local diffX = 145 - (1624-display.size.width)/2
--    local diffX1 = LuaUtils.isIphoneXDesignResolution() and -75 or 0
--    self.m_newChatLayer:setNodeAllOffset(diffX+self.m_pNodeLeft:getPositionX()-self.m_newChatLayer:getLeftBtnPosX()+diffX1+self.m_pNodeLeft:getContentSize().width/2,0)
--    self.m_newChatLayer:setLeftBtnOffsetX(diffX+self.m_pNodeLeft:getPositionX()-self.m_newChatLayer:getLeftBtnPosX()+diffX1+self.m_pNodeLeft:getContentSize().width/2)
--    self.m_newChatLayer:setLeftBtnOffsetY(self.m_pNodeLeft:getPositionY()-self.m_newChatLayer:getLeftBtnPosY()+self.m_pNodeLeft:getContentSize().height/2)
--    self.m_newChatLayer:addTo(self,101)
  --  self.m_pNodeLeft:hide()

--    --银商喇叭
--    self.m_rollMsgObj = RollMsg.create()
--    self.m_rollMsgObj:addTo(self.m_pNodeMiddle)
--    self.m_rollMsgObj:startRoll()

    self:initBtnClickEvent()
    self:setNodeFixPostion()

end

function HandredcattleLayer:updateBankBtn()
    if  not HoxDataMgr.getInstance():isInBankList() then
        self.m_pDealerBtn:loadTextures("game/handredcattle/image/apply/apply.png","game/handredcattle/image/apply/apply.png","game/handredcattle/image/apply/apply.png",0)
    else
        self.m_pDealerBtn:loadTextures("game/handredcattle/image/apply/unapply.png","game/handredcattle/image/apply/unapply.png","game/handredcattle/image/apply/unapply.png",0)
    end 
end
-- 分辨率适配
function HandredcattleLayer:setNodeFixPostion()
    local diffX = 145-(1624-display.size.width)/2
    -- 根容器节点
    self.m_rootNode:setPositionX(diffX)
--    -- 下注容器节点
--    self.m_pNodeChipSpr:setPositionX(diffX)
--    -- 提示信息容器节点
    self.m_pNodeTip:setPositionX(diffX)
    self.m_pNodeShowResult:setPositionX(diffX)
    self.m_pNodeDlg:setPositionX(diffX)
    self.m_pNodeTrend:setPositionX( self.m_pNodeTrend:getPositionX() + diffX)
    --FloatMessage.getInstance():setPositionX(diffX)
    self.m_pNodeTopClock:setPositionX(diffX)

    self.m_othersPos = cc.p(self.m_pNodeLeft:getPositionX() + 52, self.m_pNodeLeft:getPositionY() + 53)
    self.m_selfPos = cc.p(self.m_pNodeBottomInfo:getPositionX() + 50, 33.5)

    --if display.size.width == 1624 then
    if LuaUtils.isIphoneXDesignResolution() then
        self.m_pNodeTopButton:setPosition(cc.p(70,0)) 
        self.m_pNodeBottomInfo:setPosition(cc.p(70,0)) 
        -- self.m_newChatLayer:setNodeAllOffset(70, 0)
        self.m_pNodeLeft:setPositionX(70) 
        self.m_pNodeTopRight:setPositionX(display.size.width - 125) 
        self.m_pNodeTrend:setPositionX(self.m_pNodeTrend:getPositionX() + 70)
        --self.m_pNodeTopRight:setPositionX(self.m_pNodeTopRight:getPositionX() + 70) 
--        local x,y = self.m_pLbResultGold:getPosition()
--        self.m_pLbResultGold:setPosition(cc.p(x - diffX + 70,y))

        self.m_pLbResultSelf:setPositionX(self.m_pLbResultSelf:getPositionX() - diffX + 70)
        self.m_pLbResultOther:setPositionX(self.m_pLbResultOther:getPositionX() - diffX + 70)
        --self.m_pLbResultBanker:setPositionX(self.m_pLbResultBanker:getPositionX() - diffX + 70)

        self.m_othersPos.x = self.m_othersPos.x + 70 - diffX
        self.m_selfPos.x = self.m_selfPos.x + 70 - diffX
    end

end

function HandredcattleLayer:initNode()
    self:initClipNode()
    self:initClockNode()
    self:initMenuView()
    self:playInGameEffect()
    --[[不显示上庄需...
    local minBankerScore = HoxDataMgr.getInstance():getMinBankerScore()
--    local forMinBankerGold = string.format(LuaUtils.getLocalString("STRING_160"), LuaUtils.getFormatGoldAndNumber(minBankerScore))
    local m_pLbBankerGoldNeed = ccui.Helper:seekWidgetByName(self.m_pathUI, "TXT_banker_gold_need") 
    m_pLbBankerGoldNeed:setString(LuaUtils.getFormatGoldAndNumber(minBankerScore)) --forMinBankerGold)
    --]]
    self.m_pAniStartPos = cc.p(self.m_pNodeCard:getChildByName("card_start_pos"):getPosition())
    for i = 1, 5 do
        self.cardPos[i] = self.m_pNodeCard:getChildByName(string.format("card_pos_%d",i))
        self.pokerPos[i] = cc.p(self.cardPos[i]:getPosition())

        --移动曲线节点
        local tempNode = self.m_pNodeCard:getChildByName(string.format("card_path_node_%d",i))
        self.pokerPathPos[i] = cc.p(tempNode:getPosition())
    end
    local endcb = function()
        self:playWinEffect()
        self:checkMyJetton()

        self:doSomethingLater(function()
            self:flychipex()
        end, 2.0)
    end
    HandredcattlePokerMgr.getInstance():releaseInstance()
    HandredcattlePokerMgr.getInstance():setAnimData(self.m_pNodeCard, self.m_pAniStartPos, self.pokerPos, self.pokerPathPos, endcb)

   

    --历史记录
    self.m_trendLayer = HandredcattleTrendLayer.new()
    self.m_pNodeTrend:addChild(self.m_trendLayer)

    --庄家动画
    self.m_pAnibanker = HandredcattleRes.CreateSpineAni(HandredcattleRes.SPINE_OF_SYSBANKER.FILEPATH)
   -- if ClientConfig.getInstance():getIsMainChannel() then
        self.m_pAnibanker:setAnimation(0, "animation1", true)
--    else
--        self.m_pAnibanker:setAnimation(0, "animation2", true)
--    end
    self.m_pAnibanker:setToSetupPose()
    self.m_pAnibanker:update(0)
    self.m_pAnibanker:setPosition(cc.p(-262, 8))
    self.m_pAnibanker:setVisible(false)
    self.m_pNodeTop:addChild(self.m_pAnibanker)

    --银行按钮
    self:updateBtStateAtAward()
end

-- 初始化弹出菜单剪切区域
function HandredcattleLayer:initClipNode()
    local shap = cc.DrawNode:create()
    local pointArr = {cc.p(0,300), cc.p(400, 300), cc.p(400, 700), cc.p(0, 700)}
    shap:drawPolygon(pointArr, 4, cc.c4f(255, 255, 255, 255), 2, cc.c4f(255, 255, 255, 255))
    self.m_pClippingMenu = cc.ClippingNode:create(shap)
    local diffX = LuaUtils.isIphoneXDesignResolution() and display.size.width - 300 or display.size.width - 239
    self.m_pClippingMenu:setPosition(cc.p(diffX,0))
    self.m_pNodeClipMenu:addChild(self.m_pClippingMenu)
    self.m_pNodeMenu:removeFromParent()
    self.m_pClippingMenu:addChild(self.m_pNodeMenu)
end

function HandredcattleLayer:initClockNode()
    self.m_pLbCountTime:setString(NiuNiuCBetManager.getInstance():getTimeCount() or "20")
end

function HandredcattleLayer:initMenuView()
    --音乐 
    local music = AudioManager.getInstance():getMusicOn()
    if music then
        AudioManager.getInstance():setMusicOn(true)
        self.m_pVoiceBtn:loadTextureNormal(HandredcattleRes.IMG_MUSIC_ON, ccui.TextureResType.plistType)
    else
        AudioManager.getInstance():setMusicOn(false)
        AudioManager.getInstance():stopMusic()
        self.m_pVoiceBtn:loadTextureNormal(HandredcattleRes.IMG_MUSIC_OFF, ccui.TextureResType.plistType)
    end

    --音效
    local sound = AudioManager.getInstance():getSoundOn()
    if sound then
        AudioManager.getInstance():setSoundOn(true)
        self.m_pBtnSound:loadTextureNormal(HandredcattleRes.IMG_SOUND_ON, ccui.TextureResType.plistType)
    else
        AudioManager.getInstance():setSoundOn(false)
        self.m_pBtnSound:loadTextureNormal(HandredcattleRes.IMG_SOUND_OFF, ccui.TextureResType.plistType)
    end

    -- 播放背景音樂
    AudioManager.getInstance():stopMusic(false)
    AudioManager.getInstance():playMusic(HandredcattleRes.MUSIC_OF_BGM)
end

function HandredcattleLayer:initGame()
    self:initSelfInfo()
    self:doSomethingLater(function()
        local nStatus = HoxDataMgr.getInstance():getGameStatus()
        if nStatus ~= HoxDataMgr.eGAME_STATUS_CHIP then
            self:initWaitingAnim()
        end
    end, 0.5)
    self:updateBetMoney()
    self:onMsgGameScene(nil)
    self:onMsgUpdateBnakerInfo(nil)
    self:UpdateCotinueAndClearBtState(false)
    NiuNiuCBetManager.getInstance():setContinue(false)
    self:initAlreadyJettion()
    self:updateOtherNum()

    for i = 1,6 do
        local node = self.m_pNodeTip:getChildByName("Node_nv_" .. i)
        self.m_nodeNVPos[i] = cc.p(node:getPosition())
    end
end

function HandredcattleLayer:initAlreadyJettion()
    --self
    local num = HoxDataMgr.getInstance():getUsrChipCount()
    for i = 1, num do
        local selfChip = HoxDataMgr.getInstance():getUsrChipByIdx(i)
        local endPos = self:getEndPositon(selfChip.wChipIndex)
        self:createChipSprite(selfChip.wJettonIndex, selfChip.wChipIndex, endPos, true)
    end
    --other
    for i = 1, Handredcattle_Const.DOWN_COUNT_HOX  do
        local totalVal = HoxDataMgr.getInstance():getOtherChip(i)

        local totalTable = CBetManager.getInstance():getSplitGoldNew(totalVal)
        for k,v in pairs(totalTable) do
           local endPos = self:getEndPositon(i)
           local index = HoxDataMgr.getInstance():GetJettonMaxIdx(v)
           self:createChipSprite(index,i,endPos,false)
        end
    end
end

function HandredcattleLayer:initSelfInfo()
    --需要格式化名字长度
    local m_pLbUsrNickName = self.m_pNodeBottomInfo:getChildByName("TXT_player_name")
    local name = Player:getNickName()
    --if string.utf8len(name) > 6 then
    --    name = LuaUtils.getDisplayNickName(name, 10,true)
    --end
    local strNick = LuaUtils.getDisplayNickName2(name, 170, "Helvetica", 22, "..")
    m_pLbUsrNickName:setString(strNick)

    --需要格式化金币
    local nScore = HoxDataMgr.getInstance():getGameSelfScore()
    local strScore = LuaUtils.getFormatGoldAndNumberAndZi(nScore)
    self.m_pLbUsrGold:setString(strScore)

--    --vip等级
--    local vipLevel = PlayerInfo.getInstance():getVipLevel()
--    local vipPath = string.format("hall/plist/vip/img-vip%d.png", vipLevel)
--    local m_pImgUserVip = self.m_pNodeBottomInfo:getChildByName("Image_vip")
--    m_pImgUserVip:loadTexture(vipPath, ccui.TextureResType.plistType)
end

--endregion

--region 事件绑定

--按钮事件绑定
function HandredcattleLayer:initBtnClickEvent()
    for i = 1, Handredcattle_Const.JETTON_ITEM_COUNT do
        self.m_pControlButtonJetton[i]:addTouchEventListener(handler(self,function()
            --主动点击筹码才播放音效
            AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_JETTON)
            self:onBetNumClicked(i)
        end))
    end
    for i = 1, Handredcattle_Const.DOWN_COUNT_HOX  do
        self.m_pControlButtonAnimal[i]:addTouchEventListener(function(sender,eventType)
            if eventType ~=ccui.TouchEventType.ended then
                return
            end
            self:onAnimalClicked(i)
        end)
    end
    --self.m_pBtnClearBet:addTouchEventListener(handler(self,self.onBetClearClicked))
    self.m_pBtnContinue:addTouchEventListener(handler(self,self.onBetContinueClicked))
    self.m_pDealerBtn:addTouchEventListener(handler(self,self.onDealerClicked)) 
    --self.m_pRecordBtn:addTouchEventListener(handler(self,self.onRecordClicked))
    self.m_pRuleBtn:addTouchEventListener(handler(self,self.onRuleClicked))
    self.m_pBankBtn:addTouchEventListener(handler(self,self.onBankClicked))
    self.m_pBankmenuBtn:addTouchEventListener(handler(self,self.onBankClicked))
    
--    self.m_pChatBtn:addTouchEventListener(handler(self,self.onMsgClicked))
    self.m_pBtnMenuPop:addTouchEventListener(handler(self,self.onMenuClicked))
    self.m_pBtnMenuPush:addTouchEventListener(handler(self,self.onMenuClicked))
    self.m_pBtnReturn:addTouchEventListener(handler(self,self.onReturnClicked))
    self.m_pBtnSound:addTouchEventListener(handler(self,self.onSoundClicked))
    self.m_pVoiceBtn:addTouchEventListener(handler(self,self.onVoiceClicked))
    self.m_pBtnOtherInfo:addTouchEventListener(handler(self,self.onOtherInfoClicked))
     self.m_BtnZhuang:addTouchEventListener(handler(self,self.onZhuangMessageClicked))
end

--系统事件绑定及处理
function HandredcattleLayer:initEvent()
   
     --    addMsgCallBack(self,Handredcattle_Events.MSG_HOX_UPDATE_BANKER_INFO,handler(self,self.onMsgUpdateBnakerInfo))--,  log = "庄家信息"},
         addMsgCallBack(self,Handredcattle_Events.MSG_HOX_SCENE_UPDATE,handler(self,self.onMsgGameScene))--,         log = "场景信息"},
         addMsgCallBack(self,Handredcattle_Events.MSG_HOX_UDT_GAME_STATUE,handler(self,self.onMsgUpdateGameStatue))--,  log = "游戏状态"},
         addMsgCallBack(self,Handredcattle_Events.MSG_HOX_GAME_END,handler(self,self.onMsgGameEnd))--,           log = "游戏结束"},
         addMsgCallBack(self,Handredcattle_Events.MSG_GAME_SHIP,handler(self,self.onMsgGameChip))--,          log = "下注信息"},
         addMsgCallBack(self,Handredcattle_Events.MSG_GAME_CONTINUE_SUCCESS,handler(self,self.onMsgGameContinueSuc))--,   log = "续投信息"},
         addMsgCallBack(self,Handredcattle_Events.MSG_HOX_REQUEST_BANKER_INFO,handler(self,self.onMsgRequestBnakerInfo))--, log = "请求上庄"}, 
         addMsgCallBack(self,Handredcattle_Events.MSG_UPDATE_ROLL_MSG,handler(self,self.addRollMessageInfo))--,     log = "滚动信息"},     
         addMsgCallBack(self,Handredcattle_Events.MSG_UPDATE_APPLYARRAY_NTY,handler(self,self.onMsgUpdateBnakerInfo))
         addMsgCallBack(self,Handredcattle_Events.ONLINE_LIST,handler(self,self.onlineListInfo))
        
end

function HandredcattleLayer:unRegistNetCallback()
  removeMsgCallBack(self,Handredcattle_Events.MSG_HOX_UPDATE_BANKER_INFO) 
  removeMsgCallBack(self,Handredcattle_Events.MSG_HOX_SCENE_UPDATE) 
  removeMsgCallBack(self,Handredcattle_Events.MSG_HOX_UDT_GAME_STATUE) 
  removeMsgCallBack(self,Handredcattle_Events.MSG_HOX_GAME_END) 
  removeMsgCallBack(self,Handredcattle_Events.MSG_GAME_SHIP) 
  removeMsgCallBack(self,Handredcattle_Events.MSG_GAME_CONTINUE_SUCCESS) 
  removeMsgCallBack(self,Handredcattle_Events.MSG_HOX_REQUEST_BANKER_INFO)  
  removeMsgCallBack(self,Handredcattle_Events.MSG_UPDATE_APPLYARRAY_NTY) 
end

function HandredcattleLayer:startGameEvent()
    self.event_game_ = {  
        [Public_Events.MSG_GAME_NETWORK_FAILURE]  =  { func = self.onMsgEnterNetWorkFail, log = "网络中断"  ,   debug = true,},
        [Public_Events.MSG_GAME_ENTER_BACKGROUND] =  { func = self.onMsgEnterBackGround,  log = "切换至后台",   debug = true, },
        [Public_Events.MSG_GAME_ENTER_FOREGROUND] =  { func = self.onMsgEnterForeGround,  log = "切换至前台",   debug = true, },
        [Public_Events.MSG_GAME_RELOGIN_SUCCESS]  =  { func = self.onMsgReloginSuccess,   log = "重登录成功",   debug = true, },
        [Public_Events.MSG_GAME_EXIT]             =  { func = self.onMsgExitGame,         log = "游戏退出"},
        [Public_Events.MSG_NETWORK_FAILURE]       =  { func = self.onMsgNetWorkFailre,    log = "网络错误"},

    }
    for key, event in pairs(self.event_game_) do   --监听事件
         SLFacade:addCustomEventListener(key, handler(self, event.func), self.__cname)
    end
end

function HandredcattleLayer:stopGameEvent()
    for key in pairs(self.event_game_) do   --监听事件
         SLFacade:removeCustomEventListener(key, self.__cname)
    end
    self.event_game_ = {}
end

function HandredcattleLayer:onMsgEnterNetWorkFail()
    self:onMsgEnterBackGround()
end

--进入后台
function HandredcattleLayer:onMsgEnterBackGround()
    self:unRegistNetCallback()
    AudioManager:getInstance():stopAllSounds()
    AudioManager.getInstance():stopMusic(true)
 --   PlayerInfo.getInstance():setSitSuc(false)
    CBetManager.getInstance():clearUserChip()
    CBetManager.getInstance():clear()
    --CMsgAnimal.getInstance():destroyCountDownShedule()

    --进入后台返回时,标识为不删资源
    self.m_bEnterBackground = true
end

--进入前台
function HandredcattleLayer:onMsgEnterForeGround()

end

--重登录成功
function HandredcattleLayer:onMsgReloginSuccess()

end

--倒计时
function HandredcattleLayer:initUpdate()
    self.handle = scheduler.scheduleGlobal(handler(self,self.onMsgCountDown), 1) 
end

function HandredcattleLayer:onMsgCountDown(value)
    --每秒刷新其他玩家数量
    self:updateOtherNum()

    local temValue = NiuNiuCBetManager.getInstance():getTimeCount()

    if temValue <= 0 then
        return
    end

    NiuNiuCBetManager.getInstance():setTimeCount(temValue-1)
    local count = NiuNiuCBetManager.getInstance():getTimeCount()

    self.m_pLbCountTime:setString( string.format("%d%d", math.floor(count/10), count%10))
    if count <= 3 and HoxDataMgr.getInstance():getGameStatus() == HoxDataMgr.eGAME_STATUS_CHIP then
        if not self._bIsPlayDaojishi then
            self._bIsPlayDaojishi = true
            --显示321
            self:showDaojishi(count)
        end
    end

end

--更新庄家信息
function HandredcattleLayer:onMsgUpdateBnakerInfo(label)
    --m_pNodeBanker
  --  self:doSomethingLater(function() 
    local m_pLbZhuangName = self.m_pNodeBanker:getChildByName("TXT_zhuang_name") 
    local num = HoxDataMgr.getInstance():getBankerArray()
    m_pLbZhuangName:setString("当前上庄人数:"..#num) 
    self.m_pLbZhuangGold:setString(HoxDataMgr.getInstance():getBankerGold())
    local num = HoxDataMgr.getInstance():getApplyNum() 
    self.m_pLbAskNum:setString("申请人数:"..num)
    self:updateBankBtn()
    --    end,0.2) 
end

--更新上庄金币
function HandredcattleLayer:onMsgGameScene(msg)
    self:refreshJettonState()
    self:onBetNumClicked(1);
    self:onMsgUpdateGameStatue(nil)
    --场景初始化时必须设置初始值
    local score = NiuNiuCBetManager.getInstance():getJettonScore(1)
    NiuNiuCBetManager.getInstance():setLotteryBaseScore(score)
end

--游戏状态切换
function HandredcattleLayer:onMsgUpdateGameStatue(_,msg)
    local status = HoxDataMgr.getInstance():getGameStatus()
    self.m_trendLayer:updateView()
    self:updateBankBtn()
    self:setRecordId(HoxDataMgr.getInstance():getRecordId())
    if status == HoxDataMgr.eGAME_STATUS_IDLE then
        self.m_pSpStatus:loadTexture("niuniu_status_kaijiang.png", ccui.TextureResType.plistType)
        --空闲
        print("－－－－－－－－－ 牛牛空闲状态 －－－－－－－－－－－")
        HandredcattlePokerMgr.getInstance():disCardAnimAll()
        self._bIsPlayDaojishi= false
        self:cleanupWinArmatures()
        self:resetNaozhongEffect()
    elseif status == HoxDataMgr.eGAME_STATUS_CHIP then
        if self.resultLayer and self.resultLayer:isVisible() then
            self.resultLayer:setVisible(false)
        end
        self.m_pSpStatus:loadTexture("niuniu_status_xiazhu.png", ccui.TextureResType.plistType)
        --投注
        self:showNaoZhongImage()
        self:updateBtStateAtAward()
        self:exitResultView()
        if msg then
            self:showTimeTips(1)
        end
         

    elseif status == HoxDataMgr.eGAME_STATUS_END then
        self.m_pSpStatus:loadTexture("niuniu_status_kaijiang.png", ccui.TextureResType.plistType)
        self.m_pLbCountTime:setString( string.format("%d", NiuNiuCBetManager.getInstance():getTimeCount()))
        --self.m_pNodeClock:setVisible(false)
        self:UpdateCotinueAndClearBtState(false)
        self:resetNaozhongEffect()
        --开奖
        self:updateBetMoney() 
    end
end

function HandredcattleLayer:onMsgGameEnd(msg)
    self:updateBtStateAtAward()
    self:showTimeTips(2)
    
end

--玩家投注
function HandredcattleLayer:onMsgGameChip(_,msg)
    self:updateBetMoney()
   -- self:refreshJettonState()
    
    self:updateChipSuc(msg)
    self.m_pLbUsrGold:setString(HoxDataMgr.getInstance():getGameSelfScore())
--    else
--        local str = label
--        if str == "ChipCanle" then
--            self:updateChipCancel()
--           -- TOAST("STRING_011")
--        elseif str == "OtherChipCanle" then
--            self:updateOtherUserDelChip()
--        end
--    end
end

--续投
function HandredcattleLayer:onMsgGameContinueSuc(label)
    self:updateBetMoney()
    if not label then 
        self:ContinueChipSucOfOther()
        self:refreshJettonState()
        return
    end 
    if label ~= "MyContinueSuc" then return end
    
    local lContinueCount = HoxDataMgr.getInstance():getUsrContinueChipCount()
    for i = 1, lContinueCount do
        local chip = HoxDataMgr.getInstance():getUsrContinueChipByIdx(i)
        local beginPos = self:getBeginPosition(chip.wJettonIndex)
        local endPos = self:getEndPositon(chip.wChipIndex)
        self:createFlyChipSprite(chip.wJettonIndex,chip.wChipIndex,beginPos,endPos,true,false,true)
    end

    self:UpdateCotinueAndClearBtState(false)
    
    self:refreshJettonState()
end

--请求上庄
function HandredcattleLayer:onMsgRequestBnakerInfo(label)
    if not label then return end
    local nRank = HoxDataMgr.getInstance():isInBnakerList()
    if nRank == -1 then
        local nSize = HoxDataMgr.getInstance():getBankerListSize()
        local strFormat = LuaUtils.getLocalString("STRING_242")
        local strSize = string.format(strFormat, nSize)
        self.m_pLbAskNum:setString(strSize)
    else
        local strFormat = LuaUtils.getLocalString("STRING_241")
        local strRank = string.format(strFormat, nRank)
        self.m_pLbAskNum:setString(strRank)
    end

    local pStrChairID = tonumber(label)
    local nMeChairID = PlayerInfo.getInstance():getChairID()
    if nMeChairID == pStrChairID then
        local isBanker = HoxDataMgr.getInstance():isBanker() or (nRank ~= -1)
        self:updateBankerButtonStatus(isBanker)
    end
end

--滚动消息
function HandredcattleLayer:addRollMessageInfo(msg)
    if not msg or next(msg) == nil then return end
    local pSysMessage = msg 
    if pSysMessage.wSysType == 1 then-- UPRUN_MSG
        local strMessage = string.trim(pSysMessage.szSysMessage) or ""
        TOAST(strMessage)
    end
end

--用户积分变更
function HandredcattleLayer:onMsgGoldChange(msg)
    self:refreshJettonState()
    if not self.m_pLbUsrGold then return end
    if HoxDataMgr.getInstance():getGameStatus() ~= HoxDataMgr.eGAME_STATUS_END then
        local curScore = HoxDataMgr.getInstance():getGameSelfScore()
        local strUsrBanlance = LuaUtils.getFormatGoldAndNumberAndZi(curScore)
        self.m_pLbUsrGold:setString(tostring(strUsrBanlance))
    end
    print("onMsgGoldChange", HoxDataMgr.getInstance():getGameSelfScore())
end

function HandredcattleLayer:onMsgExitGame(msg)
    self.m_bEnterBackground = false
    self:onMoveExitView()
end

function HandredcattleLayer:onMsgNetWorkFailre(msg)
    TOAST("STRING_023")
    self:onMoveExitView()
end

function HandredcattleLayer:onDealerClicked(sender,eventType)
    if eventType ~=ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_BUTTON) 
    if HoxDataMgr.getInstance():isBanker() or HoxDataMgr.getInstance():isInBankList() then
         sendMsg(Handredcattle_Events.MSG_COW_XIAZHUANG_REQ,{})
         return
    end
     if HoxDataMgr.getInstance():getGameSelfScore() < HoxDataMgr.getInstance():getMinBankerScore() then
       TOAST("上庄金币不足")
        return
    end
    local HandredcattleApplyLayer = HandredcattleApplyLayer.new()
    self:addChild(HandredcattleApplyLayer)
--    if bState and HoxDataMgr.getInstance():getGameSelfScore() < HoxDataMgr.getInstance():getMinBankerScore() then
--        local nScore = HoxDataMgr.getInstance():getMinBankerScore()
--        local strScore = LuaUtils.getFormatGoldAndNumberAndZi(nScore)
--        local strFormat = LuaUtils.getLocalString("STRING_239")
--        local str = string.format(strFormat, strScore)
--        TOAST(str)
--        self:showRecharge()
--        return
--    end
--    CMsgHandredcattle.getInstance():sendMsgOfReqHost(bState)
end

function HandredcattleLayer:onRuleClicked(sender,eventType)
     if eventType ~=ccui.TouchEventType.ended then
        return
    end
    if self.m_bMenuMoving then
        return
    end
    AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_BUTTON)
    --弹出规则弹窗
    local ruleLayer = HandredcattleRuleLayer:create()
    -- ruleLayer:setPositionX((display.width - 1334) / 2)
    self:addChild(ruleLayer,6)

--    local CommonRule = require("common.layer.CommonRule")
--    local nKind = PlayerInfo.getInstance():getKindID()
--    local pRule = CommonRule.new(nKind)
--    pRule:addTo(self.m_rootUI, 101)
end

function HandredcattleLayer:onAnimalClicked(nIndex)
    if not nIndex then return end

    --庄家不能下注
    if HoxDataMgr.getInstance():isBanker() then 
        TOAST("庄家不能下注")
        return
    end
     
    --请稍后，还没有到下注时间
    if HoxDataMgr.getInstance():getGameStatus() ~= HoxDataMgr.eGAME_STATUS_CHIP then
        TOAST("请稍后，还没有到下注时间")
        return
    end
    
    --不够下注，弹充值
    if HoxDataMgr.getInstance():getGameSelfScore() < NiuNiuCBetManager.getInstance():getLotteryBaseScore() then
       TOAST("金币不足")
        return
    end

    --已下注的筹码
    local llTotalValue = 0
    for i = 1, 4 do --自己下注
        local llUserValue = HoxDataMgr.getInstance():getUserAllChip(i)
        llTotalValue = llTotalValue + llUserValue
    end

--    --玩家的钱不够赔时
--    if (HoxDataMgr.getInstance():getGameSelfScore() + llTotalValue) < llTotalValue * HoxDataMgr.getInstance():getModeType()
--    or (HoxDataMgr.getInstance():getGameSelfScore() + llTotalValue) < (llTotalValue + NiuNiuCBetManager.getInstance():getLotteryBaseScore()) * HoxDataMgr.getInstance():getModeType()
--    or (HoxDataMgr.getInstance():getGameSelfScore()) < (NiuNiuCBetManager.getInstance():getLotteryBaseScore() * HoxDataMgr.getInstance():getModeType())
--    then
--        TOAST("NIUNIU_6")
--        self:showRecharge()
--        return
--    end

--    self.m_pBtnContinue:setEnabled(false)
--    self.m_pBtnContinue:setColor(cc.c3b(160,160,160))

    local _data = {}
    _data.wChipIndex = nIndex - 1
    _data.wJettonIndex = NiuNiuCBetManager.getInstance():getLotteryBaseScoreIndex()  
    local tab = {1,10,50,100,500}
     sendMsg(Handredcattle_Events.MSG_COW_BET_XIAZHU,{m_addArea = nIndex - 1, m_addScore = tab[NiuNiuCBetManager.getInstance():getLotteryBaseScoreIndex()+1]})
   -- CMsgHandredcattle.getInstance():sendMsgOfChipStart(_data)
end

function HandredcattleLayer:onBetNumClicked(nIndex)
    if not nIndex then return end
    if self.nIndexChoose == nIndex then
        return
    end
    local score = NiuNiuCBetManager.getInstance():getJettonScore(nIndex)
    if score and HoxDataMgr.getInstance():getGameSelfScore() < score then
        return
    end

    --让先前选中的变成原始位置
    local fNormalY = 24
    local fSelectY = 38
    
    --原来选择的需要飞回来
    if self.nIndexChoose > 0 then
        local posNowX, posNowY = self.m_pControlButtonJetton[self.nIndexChoose]:getPosition()
        self.m_pControlButtonJetton[self.nIndexChoose]:setPosition(cc.p(posNowX,fNormalY))
    end
    
    --新选择的需要飞出去
    local posNewX, posNewY = self.m_pControlButtonJetton[nIndex]:getPosition()
    self.m_pControlButtonJetton[nIndex]:setPosition(cc.p(posNewX, fSelectY))
    
    self.nIndexChoose = nIndex
    NiuNiuCBetManager.getInstance():setLotteryBaseScore(score)
end

function HandredcattleLayer:onBankClicked(sender,eventType)
    if eventType ~=ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_BUTTON)

    if self.m_bMenuMoving then
        return
    end

    local GameRecordLayer = GameRecordLayer.new(2,105)
    self:addChild(GameRecordLayer,100)    
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", {105})
end

function HandredcattleLayer:onBetContinueClicked(sender,eventType)
    if eventType ~=ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_BUTTON)
    
    --还没有到下注时间
    if HoxDataMgr.getInstance():getGameStatus() ~= HoxDataMgr.eGAME_STATUS_CHIP then
        TOAST("还没有到下注时间")
        return
    end
    --没有下注记录
--    local count = HoxDataMgr.getInstance():getUsrContinueChipCount()
--    if count <= 0 then
--        TOAST("没有下注记录")
--        return
--    end
    --已经续投过
--    if NiuNiuCBetManager.getInstance():getContinue() then
--        TOAST("STRING_004")
--        return
--    end
    --庄家不能下注
    if NiuNiuCBetManager.getInstance():getBankerUserId() == Player:getAccountID() then
        TOAST("庄家不能下注")
        return
    end 
    
--    local continueChip = {}
--    continueChip.llDownTotal = {}
--    for i = 1, Handredcattle_Const.DOWN_COUNT_HOX  do
--        continueChip.llDownTotal[i] = 0
--    end
--    local llSumScore = 0
--    for i = 1, count do
--        local chip = HoxDataMgr.getInstance():getUsrContinueChipByIdx(i)
--        if chip then
--            local baseVale = NiuNiuCBetManager.getInstance():getLotteryBaseScoreByIndex(chip.wJettonIndex)
--            local score = continueChip.llDownTotal[chip.wChipIndex]
--            continueChip.llDownTotal[chip.wChipIndex] = baseVale + score
--            llSumScore = baseVale + llSumScore
--        end
--    end
--    --显示充值
--    if HoxDataMgr.getInstance():getGameSelfScore() < llSumScore then
--        TOAST("金币不足")
--     --   self:showRecharge()
--        return
--    end
    --不足赔付
--    if HoxDataMgr.getInstance():getGameSelfScore() < (llSumScore * HoxDataMgr.getInstance():getModeType()) then
--        TOAST("金币不足")
--        self:showRecharge()
--        return
--    end

    --禁用续投
    self.m_pBtnContinue:setEnabled(false)
    self.m_pBtnContinue:setColor(cc.c3b(160,160,160))
    sendMsg(Handredcattle_Events.MSG_COW_CONTINUE_XIAZHU) -- 续压 
end

function HandredcattleLayer:onMsgClicked(sender,eventType)
    if eventType ~=ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_BUTTON)
--    self.m_pNodeNewMsg:setVisible(false)
    --弹出聊天弹窗
--    MsgCenter::getInstance()->postMsg(MSG_SHOW_VIEW, __String::create("chat"))
end

--左上按钮点击事件
function HandredcattleLayer:onMenuClicked(sender,eventType)
    if eventType ~=ccui.TouchEventType.ended then
        return
    end
    if self.m_bMenuMoving then
        return
    end
    self.m_bMenuMoving = true

    if self.m_pNodeMenu:isVisible() then
        --菜单栏收起 
        self.m_pBtnMenuPush:setVisible(false)
        local callBack = cc.CallFunc:create(function()
            self.m_pBtnMenuPop:loadTextureNormal(HandredcattleRes.IMG_PUSH, ccui.TextureResType.plistType)
        end)
        local callBack2 = cc.CallFunc:create(function()
            self.m_bMenuMoving = false
        end)
        showMenuPush(self.m_pNodeMenu, callBack, callBack2, 2, 780 )
    else 
        --菜单栏放下
        self.m_pBtnMenuPush:setVisible(true)
        self.m_pNodeMenu:setPosition(cc.p(2, 780))
        local callBack = cc.CallFunc:create(function()
            self.m_pBtnMenuPop:loadTextureNormal(HandredcattleRes.IMG_PUSH1, ccui.TextureResType.plistType)
        end)
        local callBack2 = cc.CallFunc:create(function()
            self.m_bMenuMoving = false
        end)
        showMenuPop(self.m_pNodeMenu, callBack, callBack2, 2, 655 )
    end
    
end

function HandredcattleLayer:onReturnClicked(sender,eventType)
    if eventType ~=ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_CLOSE)
    self:getParent():showAskExitGameTip()
--    if HoxDataMgr.getInstance():isBanker() then --当庄
--        if HoxDataMgr.getInstance():getAllChip() > 0 then
--            --请下庄后才能退出房间
--            TOAST("请下庄后才能退出房间")
--        else --没有投注纪录,直接退回游戏
--            self:onMoveExitView(nil,nil,true)
--        end
--    else --未当庄
--        if HoxDataMgr.getInstance():getUsrChipCount() > 0 then
--            --有投注纪录，则需要提示
--            sendMsg(self,Public_Events.MSG_SHOW_MESSAGEBOX, "game-exit-1")
--        else --没有投注纪录,直接退回游戏
--            self:onMoveExitView(nil,nil,true)
--        end
--    end
end

function HandredcattleLayer:onSoundClicked(sender,eventType)
    if eventType ~=ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_BUTTON)
    
    if self.m_bMenuMoving then
        return
    end

     local layer = GameSetLayer.new(105);
	self:addChild(layer); 
    layer:setLocalZOrder(100)
end

function HandredcattleLayer:onVoiceClicked(sender,eventType)
    if eventType ~=ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_BUTTON)

    if self.m_bMenuMoving then
        return
    end
  local layer = GameSetLayer.new();
	self:addChild(layer); 
    layer:setLocalZOrder(100)
end

function HandredcattleLayer:onZhuangMessageClicked(sender,eventType)
    if eventType ~=ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_BUTTON)
    --玩家列表
    local infoLayer = HandredcattleBankerLayer.new()
    self:addChild(infoLayer,4) 
end
function HandredcattleLayer:onOtherInfoClicked(sender,eventType)
    if eventType ~=ccui.TouchEventType.ended then
        return
    end
    AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_BUTTON)
     local ChatLayer = ChatLayer.new()
        self:addChild(ChatLayer);
    --玩家列表
--    local infoLayer = HandredcattleOtherInfoLayer.new()
--    self.m_pNodeDlg:addChild(infoLayer,4)
   -- sendMsg(Handredcattle_Events.MSG_COW_ONLINE_REQ)

--    self.m_newChatLayer:onOutLeftPlayerClicked()
end
function HandredcattleLayer:onlineListInfo()
    local infoLayer = HandredcattleOtherInfoLayer.new()
    self.m_pNodeDlg:addChild(infoLayer,4)
end
--endregion

--region 辅助方法

--用户数据清理
function HandredcattleLayer:cleanPlayerData()
    --删除layer时，清理数据
--    PlayerInfo.getInstance():setChairID(INVALID_CHAIR)
--    PlayerInfo.getInstance():setTableID(INVALID_TABLE)
--    PlayerInfo.getInstance():setSitSuc(false)
--    PlayerInfo.getInstance():setIsQuickStart(false)
--    PlayerInfo.getInstance():setIsGameBackToHall(true)
end

function HandredcattleLayer:stopUpdate()
    if self.handle then
        scheduler.unscheduleGlobal(self.handle)
    end
end

function HandredcattleLayer:onMoveExitView()
   
    -----------------------------
    NiuNiuCBetManager.getInstance():clear()
    HoxDataMgr.getInstance():Clear()
    AudioManager:getInstance():stopAllSounds()
    AudioManager.getInstance():stopMusic(true)
    self:cleanPlayerData()
    self:stopUpdate()

    --释放动画
    for i, name in pairs(HandredcattleRes.vecReleaseAnim) do
        ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo(name)
    end
    --释放整图
    for i, name in pairs(HandredcattleRes.vecReleasePlist) do
        display.removeSpriteFrames(name[1], name[2])
    end
    -- 释放背景图
    for i, name in pairs(HandredcattleRes.vecReleaseImg) do
        display.removeImage(name)
    end
    --释放音频
    for i, name in pairs(HandredcattleRes.vecReleaseSound) do
        AudioManager.getInstance():unloadEffect(name)
    end

    --sendMsg(self,Public_Events.Load_Entry)
end

function HandredcattleLayer:doSomethingLater(call, time)
    self:runAction(cc.Sequence:create(cc.DelayTime:create(time), cc.CallFunc:create(call)))
end

function HandredcattleLayer:getJettonEnableState(index)
    if HoxDataMgr.getInstance():getGameStatus() ~= 3 then
        --非投注状态
        return false
    end
    
    if HoxDataMgr.getInstance():isBanker() then
        return false
    end

--    local llTotalValue = 0
--    for i = 1, 4 do
--        --自己下注
--        local llUserValue = HoxDataMgr.getInstance():getUserAllChip(i)
--        llTotalValue = llUserValue + llTotalValue
--    end

--    local nextCost = (llTotalValue + NiuNiuCBetManager.getInstance():getJettonScore(index)) * HoxDataMgr.getInstance():getModeType()
--    if (HoxDataMgr.getInstance():getGameSelfScore()+llTotalValue) < llTotalValue * HoxDataMgr.getInstance():getModeType()
--        or (HoxDataMgr.getInstance():getGameSelfScore()+llTotalValue) < nextCost then
--        --玩家的钱不够赔时
--        return false
--    end

--    if HoxDataMgr.getInstance():getGameSelfScore() < (NiuNiuCBetManager.getInstance():getJettonScore(index) * HoxDataMgr.getInstance():getModeType()) then
--        --玩家钱币不足
--        return false
--    end

--    if HoxDataMgr.getInstance():getRemaingChip() < NiuNiuCBetManager.getInstance():getJettonScore(index) then
--        --剩下投注金币过小
--        return false
--    end
    
    return true
end

function HandredcattleLayer:getJettonSelAdjust()
    local nModeType = HoxDataMgr.getInstance():getModeType()
    local nMeScore = HoxDataMgr.getInstance():getGameSelfScore()
    local nRemain = HoxDataMgr.getInstance():getRemaingChip()
    local nBaseScore = NiuNiuCBetManager.getInstance():getLotteryBaseScore()
    local nJetton = NiuNiuCBetManager.getInstance().m_llJettonValue

    local llTotalValue = 0
    for i = 1, CHIP_ITEM_COUNT do --自己下注
        llTotalValue = llTotalValue + HoxDataMgr.getInstance():getUserAllChip(i)
    end
    for i = Handredcattle_Const.JETTON_ITEM_COUNT, 1, -1 do
        if (nMeScore + llTotalValue) >= (llTotalValue + nJetton[i]) * nModeType --自己的积分够赔付
            and nJetton[i] <= nRemain then --庄家积分够赔付
            if i < self.nIndexChoose then --新的筹码索引比现有选中的小
                return i
            else
                return self.nIndexChoose
            end
        end
    end

    return 1 --默认选中第一个
end

--系统提示
function HandredcattleLayer:showTimeTips(_nType)
    --[[ 
         1 -- 开始投注   
         2 -- 开始开奖  
         3 -- 玩家坐庄
         4 -- 系统坐庄 
    --]]
    if _nType == 1 then
        if self.m_pNodeTip:getChildByTag(Handredcattle_Const.waitArmTag) then 
            self.m_pNodeTip:removeChildByTag(Handredcattle_Const.waitArmTag) 
        end
        AudioManager.getInstance():playSound(HandredcattleRes.SOUND_START_WAGER)
        self:initJieduanAnim(_nType)
    elseif _nType == 2 then
        AudioManager.getInstance():playSound(HandredcattleRes.SOUND_END_WAGER)
        self:initJieduanAnim(_nType)
    end
end

function HandredcattleLayer:resetNaozhongEffect()
    local anim = self.m_pathUI:getChildByTag(2016)
    if anim then
        anim:setVisible(false)
    end
    if self.m_pNodeTip:getChildByTag(Handredcattle_Const.daojishiArmTag) then
        self.m_pNodeTip:removeChildByTag(Handredcattle_Const.daojishiArmTag)
        self._bIsPlayDaojishi = false
    end
end

function HandredcattleLayer:checkMyJetton()
    local i = HoxDataMgr.getInstance():getUsrChipCount()
    if i <= 0 and not HoxDataMgr.getInstance():isBanker() then
        TOAST("本局您没有下注")
    end
end

function HandredcattleLayer:ContinueChipSucOfOther()
    local len = HoxDataMgr.getInstance():getQueueOtherContinueChipCount()
    for i = 1, len do
        local chipInfo = HoxDataMgr.getInstance()._queueOtherUserContinueChip[i]
        if chipInfo then
            local wChipIndex = chipInfo.wChipIndex
            local wTotalChipVale= chipInfo.llTotalChipVale
            if wTotalChipVale > 0 then
                local wJettonIndexVec = HoxDataMgr.getInstance():splitTotalValToVec(wTotalChipVale)
                for i = 1, #wJettonIndexVec do
                    local wJettonIndex = wJettonIndexVec[i]
                    local beginPos = self:getBeginPositionForAllOther()
                    local endPos =  self:getEndPositon(wChipIndex)
                    self:createFlyChipSprite(wJettonIndex,wChipIndex,beginPos,endPos,false,false,true)
                end
            end
        end
    end
    HoxDataMgr.getInstance():cleanQueueOtherContinueChipCount()
end

function HandredcattleLayer:showNaoZhongImage()
    self._bIsPlaySound = false
    self._bIsPlayClock = false
    self.m_pLbCountTime:setString(NiuNiuCBetManager.getInstance():getTimeCount() or "20")
end

function HandredcattleLayer:exitResultView()
    self:updateBetMoney()
--    --清空下注筹码精灵
--    self:clearVecSpriteSelf()
--    --清空其他玩家下注筹码精灵
--    self:clearVecSpriteOther()
    self.m_flyJobVec = {}
    self:clearChipSprite()
    self.m_pNodeShowResult:removeAllChildren()
    self:checkIsShowContinuBt()
    for i = 1, 5 do
        self.cardSp[i] = {}
    end

--    local llGold = HoxDataMgr.getInstance():getBankerChairId() == INVALID_CHAIR
--                    and 999999999
--                    or HoxDataMgr.getInstance():getBankerGold()

--    self.m_pLbZhuangGold:setString(LuaUtils.getFormatGoldAndNumberAndZi(llGold))
end

function HandredcattleLayer:clearChipSprite()
    self.m_pNodeChipSpr:removeAllChildren()
    self.m_vecSpriteOther = {}
    self.m_vecSpriteSelf = {}
end

function HandredcattleLayer:checkIsShowContinuBt()
    local isShow = NiuNiuCBetManager.getInstance():getContinue()
    self:UpdateCotinueAndClearBtState(isShow)
end

function HandredcattleLayer:cleanupWinArmatures()
    for i = 1, #self.m_pWinEffect do
        if self.m_pWinEffect[i] then
            self.m_pWinEffect[i]:removeFromParent()
        end
    end
    self.m_pWinEffect = {}
end

function HandredcattleLayer:setBankerNeedAnim()
    self.delayAnim = false
    if  HoxDataMgr.getInstance():isBanker() == true
    and HoxDataMgr.getInstance():getBankerCurrResult() > 0
    then 
        self.delayAnim = true
    end
end

function HandredcattleLayer:getBeginPosition(index)
    return Handredcattle_Const.jettonStartPos[index]
end

-- 获取投注区筹码落点
function HandredcattleLayer:getEndPositon(index)
    local pos = {}
    if index == 1 then
        pos = cc.p(294,353)--cc.p(295 , 396)
    elseif index == 2 then
        pos = cc.p(540,353)--cc.p(516, 396)
    elseif index == 3 then
        pos = cc.p(785,353)--cc.p(745, 396)
    elseif index == 4 then
        pos = cc.p(1029,353)--cc.p(970, 396)
    else
        pos = cc.p(294,353)--cc.p(295, 396)
    end
    local factor1 = (math.random() > 0.5) and  (-1) or 1
    local factor2 = (math.random() > 0.5) and  (-1) or 1
    pos.x = pos.x + math.random(0,76) * factor1
    pos.y = pos.y + math.random(0,35) * factor2
    return pos
end

-- 获取其他玩家投注筹码动画起始点
function HandredcattleLayer:getBeginPositionForAllOther(iOtherIndex)
    return self.m_othersPos
end

function HandredcattleLayer:getSelfGoldPosition()
    return self.m_selfPos
end

function HandredcattleLayer:getJettonIconFile(nIndex)
    local str = "game-jetton-1000.png"
    if not nIndex then 
        return str
    end

    local nScore = NiuNiuCBetManager.getInstance():getJettonScore(nIndex)
    str = string.format("game/handredcattle/image/"..HandredcattleRes.IMG_JETTON, nScore)
    return str
end

--endregion

--region UI组件状态刷新

function HandredcattleLayer:refreshJettonState()
    for i = 1, Handredcattle_Const.JETTON_ITEM_COUNT do
        local bStatue = self:getJettonEnableState(i)
        local color = bStatue and cc.c3b(255,255,255) or cc.c3b(160,160,160)
        local opacity = bStatue and 255 or 180
        self.m_pControlButtonJetton[i]:setOpacity(opacity)
        self.m_pControlButtonJetton[i]:setColor(color)
        self.m_pControlButtonJetton[i]:setEnabled(bStatue)
    end

   -- self:updateJettonSelAdjust()
end

function HandredcattleLayer:updateJettonSelAdjust()
    local newSelIndex = self:getJettonSelAdjust()
    if newSelIndex >= 1 and  newSelIndex < self.nIndexChoose then
        self:onBetNumClicked(newSelIndex)
    end
end

--更新上庄按钮状态
function HandredcattleLayer:updateBankerButtonStatus(isBanker)
    local strPath = isBanker and HandredcattleRes.IMG_QUXIAO or HandredcattleRes.IMG_SHENQING 
    self.m_pDealerBtn:loadTextureNormal(strPath,ccui.TextureResType.plistType)
end

--更新下注金币
function HandredcattleLayer:updateBetMoney()
    local llTotalValue = 0
    for i = 1, 4 do
        --自己下注
        local llUserValue = HoxDataMgr.getInstance():getUserAllChip(i)
        self.m_pLbMyBettingGold[i]:setString(LuaUtils.getFormatGoldAndNumberAndZi(llUserValue))

        --单区域总下注
        local llValue = HoxDataMgr.getInstance():getOtherChip(i) + llUserValue
        self.m_pLbBettingGold[i]:setString(LuaUtils.getFormatGoldAndNumberAndZi(llValue))

        llTotalValue = llTotalValue + llValue
    end
    --所有下注
    self.m_pLbTotalBetting:setString(LuaUtils.getFormatGoldAndNumberAndZi(llTotalValue))

--    local strValue = "9999999.99"
--    if HoxDataMgr.getInstance():getBankerChairId() ~= G_CONSTANTS.INVALID_CHAIR then
--        strValue = LuaUtils.getFormatGoldAndNumberAndZi(HoxDataMgr.getInstance():getRemaingChip())
--    end
--    self.m_pLbRemaingBetting:setString(strValue)
end

--更新投注按钮状态
function HandredcattleLayer:UpdateCotinueAndClearBtState(curState,isHide)
    if HoxDataMgr.getInstance():isBanker() then
        curState = false
    end
    if isHide then
        self.m_pBtnContinue:setVisible(true)
        self.m_pBtnContinue:setEnabled(false)
    else
        self.m_pBtnContinue:setVisible(true)
        self.m_pBtnContinue:setEnabled(curState)
    end
    local color = curState and cc.c3b(255,255,255) or cc.c3b(160,160,160)
    self.m_pBtnContinue:setColor(color)
end

--玩家投注
function HandredcattleLayer:updateChipSuc(chipInfo)
  
    local isMyFly = chipInfo.bIsSelf
    local wUserChairId = chipInfo.wChairID
    local wChipIndex = chipInfo.wChipIndex
    local wJettonIndex= chipInfo.wJettonIndex 
    local beginPos = {}
    if isMyFly then
        beginPos = self:getBeginPosition(wJettonIndex)
    else
        beginPos = self.m_othersPos
    end
    local endPos = self:getEndPositon(wChipIndex)
        
    if isMyFly then
        self:createFlyChipSprite(wJettonIndex,wChipIndex,beginPos,endPos,isMyFly,false)
--        self:UpdateCotinueAndClearBtState(false)
        self:updateJettonSelAdjust()
    else
        self:doSomethingLater(function()
                self:createFlyChipSprite(wJettonIndex,wChipIndex,beginPos,endPos,isMyFly,false)
            end,
            math.random(0,8)/10)
    end 
end

--取消投注
function HandredcattleLayer:updateChipCancel()
    local i = HoxDataMgr.getInstance():getUsrChipCount()
    if i <= 0 then
        --self:UpdateCotinueAndClearBtState(true)
        self:clearVecSpriteSelf()
        self:refreshJettonState()
    else
        self:UpdateCotinueAndClearBtState(false)
    end
end

--取消投注
function HandredcattleLayer:updateOtherUserDelChip()
    local len = HoxDataMgr.getInstance():getQueueOtherDelChipCount()
    local index = 1
    while len > 0 do    
        local tempTab = {}
        for i = 1, #self.m_vecSpriteOther do
            tempTab[i] = false
        end

        local chipInfo = HoxDataMgr.getInstance():getQueueOtherDelChip(index)
        index = index + 1
        
        local wChipIndex = chipInfo.wChipIndex
        local wTotalChipVale= chipInfo.llTotalChipVale
        if wTotalChipVale > 0 then
            wJettonIndexVec = HoxDataMgr.getInstance():splitTotalValToVec(wTotalChipVale)
            for i = 1, #wJettonIndexVec do
                for k,v in pairs(self.m_vecSpriteOther) do
                    if v and v.pChipSpr and (v.wChipIndex == wChipIndex) and (v.wJettonIndex == wJettonIndexVec[i]) then
                        if not tempTab[k] then
                            v.pChipSpr:removeFromParent()
                            tempTab[k] = true
                            break
                        end
                    end
                end
            end
        end
        local tempSpr = {}
        for k,v in pairs(tempTab) do
            if not v then
                table.insert(tempSpr, self.m_vecSpriteOther[k])
            end
        end
        self.m_vecSpriteOther = tempSpr
        len = len - 1
    end
    HoxDataMgr.getInstance():clearOtherDelChip()
end

function HandredcattleLayer:updateBtStateAtAward()
   
    
    local bShow = HoxDataMgr.getInstance():getGameStatus() == HoxDataMgr.eGAME_STATUS_CHIP
     self:refreshJettonState()
    -- if self.m_pBtnContinue then
    --     self.m_pBtnContinue:setEnabled(bShow)
    --     local color = bShow and cc.c3b(255,255,255) or cc.c3b(160,160,160)
    --     self.m_pBtnContinue:setColor(color)
    -- end
    

    local iShow = HoxDataMgr.getInstance():getGameStatus() == HoxDataMgr.eGAME_STATUS_END 

    local i = HoxDataMgr.getInstance():getUsrChipCount()
end

function HandredcattleLayer:updateOtherNum()
   -- local userTab = CUserManager.getInstance():getUserInfoInTable(PlayerInfo.getInstance():getTableID(),true)
    self.m_pLbOtherNum:setString(HoxDataMgr.getInstance():getOtherNum())
 --   self.m_newChatLayer:updateOutPlayerData(userTab) 
end
--endregion

--region 动画相关

--背景动画
function HandredcattleLayer:playwBgEffect()
    --背景动画
    self.m_pAnimNV:stopAllActions()
    --第一段固定4秒
    self.m_pAnimNV:setPosition(self.m_nodeNVPos[1])
    local move1 = cc.MoveTo:create(4.0,self.m_nodeNVPos[2])
    local move2 = cc.MoveTo:create(2.1,self.m_nodeNVPos[3])
    local move3 = cc.MoveTo:create(2.6,self.m_nodeNVPos[4])
    local move4 = cc.MoveTo:create(3.0,self.m_nodeNVPos[5])
    local move5 = cc.MoveTo:create(4.0,self.m_nodeNVPos[6])

    local randomTime = (math.random()) * 3
    local delayTime = cc.DelayTime:create(randomTime)
    local callFunc = cc.CallFunc:create(function()
        self:playwBgEffect()
    end)
    self.m_pAnimNV:runAction(cc.Sequence:create(move1, move2, move3, move4, move5, delayTime, callFunc))
end

--主界面入场动画
function HandredcattleLayer:playInGameEffect()
    --进游戏交互动画
    self.m_pSpRoomTable:setPosition(cc.p(666,-109))
    self.m_pSpRoomTable:setScale(1.4)
    local tableMove = cc.MoveTo:create(0.4,cc.p(666,216))
    local tableScale = cc.ScaleTo:create(0.4,1)
    local tableSpawn = cc.Spawn:create(tableScale, tableMove)
    self.m_pSpRoomTable:runAction(cc.Sequence:create(tableSpawn))

    self.m_pNodeTop:setPosition(cc.p(680,780))
    self.m_pNodeTop:setOpacity(0)
    local topNodeDelay = cc.DelayTime:create(0.3)
    local topNodeMove = cc.MoveTo:create(0.5,cc.p(680,650))
    local topNodeEaseMove = cc.EaseBackInOut:create(topNodeMove) --EaseQuarticActionIn
    local topNodeFadeto = cc.FadeTo:create(0.5,255)
    local topNodeSpawn = cc.Spawn:create(topNodeFadeto, topNodeEaseMove)
    self.m_pNodeTop:runAction(cc.Sequence:create(topNodeDelay, topNodeSpawn))

    local posy = self.m_pNodeTopClock:getPositionY()
    self.m_pNodeTopClock:setPositionY( posy + 100)
    self.m_pNodeTopClock:setOpacity(0)
    local topNodeDelay2 = cc.DelayTime:create(0.3)
    local topNodeMove2 = cc.MoveTo:create(0.5,cc.p(self.m_pNodeTopClock:getPositionX(),posy))
    local topNodeEaseMove2 = cc.EaseBackInOut:create(topNodeMove2)
    local topNodeFadeto2 = cc.FadeTo:create(0.5,255)
    local topNodeSpawn2 = cc.Spawn:create(topNodeFadeto2, topNodeEaseMove2)
    self.m_pNodeTopClock:runAction(cc.Sequence:create(topNodeDelay2, topNodeSpawn2))

    local x = self.m_pNodeTopButton:getPositionX()
    self.m_pNodeTopButton:setPosition(cc.p(x,840))
    self.m_pNodeTopButton:setOpacity(0)
    local topButtonNodeDelay = cc.DelayTime:create(0.3)
    local topButtonNodeMove = cc.MoveTo:create(0.5,cc.p(x,750))
    local topButtonNodeEaseMove = cc.EaseBackInOut:create(topButtonNodeMove)
    local topButtonNodeFadeto = cc.FadeTo:create(0.5,255)
    local topButtonNodeSpawn = cc.Spawn:create(topButtonNodeFadeto, topButtonNodeEaseMove)
    self.m_pNodeTopButton:runAction(cc.Sequence:create(topButtonNodeDelay, topButtonNodeSpawn))

    local x = self.m_pNodeTopRight:getPositionX()
    self.m_pNodeTopRight:setPosition(cc.p(x,840))
    self.m_pNodeTopRight:setOpacity(0)
    local topButtonNodeDelay1 = cc.DelayTime:create(0.3)
    local topButtonNodeMove1 = cc.MoveTo:create(0.5,cc.p(x,750))
    local topButtonNodeEaseMove1 = cc.EaseBackInOut:create(topButtonNodeMove1)
    local topButtonNodeFadeto1 = cc.FadeTo:create(0.5,255)
    local topButtonNodeSpawn1 = cc.Spawn:create(topButtonNodeFadeto1, topButtonNodeEaseMove1)
    self.m_pNodeTopRight:runAction(cc.Sequence:create(topButtonNodeDelay1, topButtonNodeSpawn1))

    self.m_pNodeBottomButton:setPosition(cc.p(772,-109))
    self.m_pNodeBottomButton:setOpacity(0)
    local bottomButtonNodeDelay = cc.DelayTime:create(0.3)
    local bottomButtonNodeMove = cc.MoveTo:create(0.5,cc.p(772,29))
    local bottomButtonNodeEaseMove = cc.EaseBackInOut:create(bottomButtonNodeMove)
    local bottomButtonNodeFadeto = cc.FadeTo:create(0.5,255)
    local bottomButtonNodeSpawn = cc.Spawn:create(bottomButtonNodeFadeto, bottomButtonNodeEaseMove)
    self.m_pNodeBottomButton:runAction(cc.Sequence:create(bottomButtonNodeDelay, bottomButtonNodeSpawn))

    local x = self.m_pNodeBottomInfo:getPositionX()
    self.m_pNodeBottomInfo:setPosition(cc.p(x,-100))
    self.m_pNodeBottomInfo:setOpacity(0)
    local bottomNodeDelay = cc.DelayTime:create(0.3)
    local bottomNodeMove = cc.MoveTo:create(0.5,cc.p(x,0))
    local bottomNodeEaseMove = cc.EaseBackInOut:create(bottomNodeMove)
    local bottomNodeFadeto = cc.FadeTo:create(0.5,255)
    local bottomNodeSpawn = cc.Spawn:create(bottomNodeFadeto, bottomNodeEaseMove)
    self.m_pNodeBottomInfo:runAction(cc.Sequence:create(bottomNodeDelay, bottomNodeSpawn))

    self.m_pNodeMiddle:setScale(0.01)
    self.m_pNodeMiddle:setOpacity(0)
    local midScale = cc.ScaleTo:create(0.5,1)
    local midDelay = cc.DelayTime:create(0.5)
    local midFadeto = cc.FadeTo:create(0.25,255)
    local midEaseScale = cc.EaseBackOut:create(midScale)
    local midSpawn = cc.Spawn:create(midFadeto,midEaseScale)
    self.m_pNodeMiddle:runAction(cc.Sequence:create(midDelay, midSpawn))

    self.m_pNodeTrend:setScale(0.01)
    self.m_pNodeTrend:setOpacity(0)
    local midScale = cc.ScaleTo:create(0.5,1)
    local midDelay = cc.DelayTime:create(0.5)
    local midFadeto = cc.FadeTo:create(0.25,255)
    local midEaseScale = cc.EaseBackOut:create(midScale)
    local midSpawn = cc.Spawn:create(midFadeto,midEaseScale)
    self.m_pNodeTrend:runAction(cc.Sequence:create(midDelay, midSpawn))


    self.m_pNodeChipSpr:setOpacity(0)
    local chipDelay = cc.DelayTime:create(0.8)
    local chipFadeto = cc.FadeTo:create(0.1,255)
    self.m_pNodeChipSpr:runAction(cc.Sequence:create(chipDelay, chipFadeto))

    self.m_pNodeLeft:setScale(0.01)
    local leftDelay = cc.DelayTime:create(0.3)
    local leftScale = cc.ScaleTo:create(0.5,1)
    local spwan = cc.Spawn:create(cc.FadeIn:create(0.3), leftScale)
    self.m_pNodeLeft:runAction(cc.Sequence:create(leftDelay, spwan))

--    --这里做动画直接拿对应的对象
--    self.m_newChatLayer.m_pBtnLeftMsg:setScale(0.01)
--    local chatDelay = cc.DelayTime:create(0.3)
--    local chatScale = cc.ScaleTo:create(0.5,1)
--    local spwan1 = cc.Spawn:create(cc.FadeIn:create(0.3), chatScale)
--    self.m_newChatLayer.m_pBtnLeftMsg:runAction(cc.Sequence:create(chatDelay, spwan1))

--    self.m_newChatLayer.m_pBtnLeftPlayer:setScale(0.01)
--    local chatDelay1 = cc.DelayTime:create(0.3)
--    local chatScale1 = cc.ScaleTo:create(0.5,1)
--    local spwan2 = cc.Spawn:create(cc.FadeIn:create(0.3), chatScale1)
--    self.m_newChatLayer.m_pBtnLeftPlayer:runAction(cc.Sequence:create(chatDelay1, spwan2))
end

--静态放置筹码
function HandredcattleLayer:createChipSprite(jettonIndex, wChipIndex, endPos, isMyFly)
    local pSpr = cc.Sprite:create(self:getJettonIconFile(jettonIndex))
    if not pSpr then return end
    
    pSpr:setScale(0.35)
    pSpr:setPosition(endPos)
    self.m_pNodeChipSpr:addChild(pSpr,1)

    if isMyFly then
        local chipInfo = {}
        chipInfo.wChipIndex = wChipIndex
        chipInfo.wJettonIndex = jettonIndex
        chipInfo.pChipSpr = pSpr
        table.insert(self.m_vecSpriteSelf,chipInfo)
    else
        local chipInfo = {}
        chipInfo.wChipIndex = wChipIndex
        chipInfo.wJettonIndex = jettonIndex
        chipInfo.pChipSpr = pSpr
        table.insert(self.m_vecSpriteOther,chipInfo)
    end
end

--飞筹码动画
function HandredcattleLayer:createFlyChipSprite(jettonIndex,wChipIndex,startPos,endPos,isMyFly,isNeedDelay)
    local pSpr = cc.Sprite:create(self:getJettonIconFile(jettonIndex))
    if pSpr == nil then return end
    pSpr:setScale(0.4)
    pSpr:setPosition(startPos)
    self.m_pNodeChipSpr:addChild(pSpr,1)

    local move_distance = cc.pGetDistance(startPos,endPos)
    local move_time = move_distance/BR_CHIP_SPEED
    local forward = cc.MoveTo:create(move_time,endPos)
    local eMove = cc.EaseOut:create(forward,move_time+0.3)
    local scale_action = cc.ScaleTo:create(0.15,0.35)
    local Value = isNeedDelay and ( math.random(0,10) % 100)/100.0 or 0 
    local call = cc.CallFunc:create(function ()
        if isMyFly then -- 自己的音效立刻播放
             if jettonIndex > 3 then -- 分投注级别播放音效 金额大于100块的播放高价值音效
                 AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_BETHIGH)
             else
                 AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_BETLOW)
             end            
        else
            if not self.m_isPlayBetSound then
                if jettonIndex > 3 then -- 分投注级别播放音效 金额大于100块的播放高价值音效
                    AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_BETHIGH)
                else
                    AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_BETLOW)
                end
                --AudioManager.getInstance():playSound("game/animals/soundpublic/sound-jetton.mp3")
                -- 尽量避免重复播放
                self:doSomethingLater(function() self.m_isPlayBetSound  = false end, math.random(2,4)/10)
                self.m_isPlayBetSound = true
            end
        end
    end)
    
    local seq = cc.Sequence:create(cc.Hide:create(), cc.DelayTime:create( Value ),cc.Show:create(), call, eMove,scale_action)
    pSpr:runAction(seq)
    
    if isMyFly then
        local chipInfo = {}
        chipInfo.wChipIndex = wChipIndex
        chipInfo.wJettonIndex = jettonIndex
        chipInfo.pChipSpr = pSpr
        table.insert(self.m_vecSpriteSelf,chipInfo)
    else
        local chipInfo = {}
        chipInfo.wChipIndex = wChipIndex
        chipInfo.wJettonIndex = jettonIndex
        chipInfo.pChipSpr = pSpr
        table.insert(self.m_vecSpriteOther, #self.m_vecSpriteOther + 1, chipInfo)
    end
end

--替换开牌牌面资源
function HandredcattleLayer:playCardsEffect()
    local nMulti = 0
    local nBase = 5
    self.m_nCardsIndex = 1
    self.m_nPlayersIndex = 1
    local tem = HoxDataMgr.getInstance():getOpenData()
    local nCardNum = 0
    --开牌动画
    local boneName = "%d-%d_Copy1"
    -- 1是庄家 2-5下方从左至右，天地玄黄
    -- 发牌顺序:1->5
    AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_FAPAI)
    for i = 1, Handredcattle_Const.PLAYERS_COUNT_HOX do
        HandredcattlePokerMgr.getInstance():showFaPaiAni(i,tem.cbSendCardData[i])
    end
end

function HandredcattleLayer:effectEnd(armature,movementEventType,name)
    if not armature then return end
    local strEffecName = armature:getName()
    if strEffecName == HandredcattleRes.ANI_OF_NAOZHONG then

        if self.m_pNodeTip:getChildByTag(Handredcattle_Const.daojishiArmTag) then
            self.m_pNodeTip:removeChildByTag(Handredcattle_Const.daojishiArmTag)
            self._bIsPlayDaojishi = false
        end

    elseif strEffecName == HandredcattleRes.ANI_OF_WIN then
        armature:getAnimation():play("Animation2")
        return

    end  
    armature:removeFromParent()
end

--得分动画
function HandredcattleLayer:playUserScoreEffect()
    local selfcurscore = 0 -- 自己最终得分
    local bankercurscore = 0 -- 庄家最终得分
    local othercurscore = 0 -- 其他玩家最终得分

    --加钱
    local tem = HoxDataMgr.getInstance():getOpenData()
    local llAwardValue = 0
    local llAwareBet = 0
    if HoxDataMgr.getInstance():isBanker() then
        llAwardValue = tem.llBankerResult
    else
        for k, v in pairs(tem.llAreaMyResult) do --计算收益
            llAwardValue = llAwardValue + v
        end
        for k, v in pairs(tem.bPlayerWin) do --计算成本
            llAwareBet = llAwareBet + HoxDataMgr.getInstance():getUserAllChip(k)
        end
        --llAwardValue = tem.llFinaResult --0
        print("onMsgGoldChange", "2", llAwardValue, llAwareBet, HoxDataMgr.getInstance():getGameSelfScore() + llAwardValue + llAwareBet)
    end

    local llUserScore = HoxDataMgr.getInstance():getGameSelfScore()

    local str = ""
    local curpath = ""
    local tax = 1 - CBetManager.getInstance():getGameTax()

    -- 显示庄家分数动画
    bankercurscore = tem.llBankerResult
    str = LuaUtils.getFormatGoldAndNumberAndZi(bankercurscore)
    if bankercurscore > 0 then
        str = "+" .. str
    end
    curpath = bankercurscore < 0 and HandredcattleRes.FNT_RESULT_LOSE or HandredcattleRes.FNT_RESULT_WIN
    --self:flyNumOfGoldChange(self.m_pLbResultBanker, curpath, str)

    -- 显示其他玩家分数动画
    if HoxDataMgr.getInstance():isBanker() then
        othercurscore = bankercurscore > 0 and -bankercurscore/tax or -bankercurscore
    else
        for i = 1, 4 do
            othercurscore = othercurscore + tem.llAreaTotalResult[i] - tem.llAreaMyResult[i]
        end
    end

    if othercurscore > 0 then -- 其他玩家返回的胜利分数是没有扣税的
        othercurscore = othercurscore * (1 - CBetManager.getInstance():getGameTax())
    end

    str = LuaUtils.getFormatGoldAndNumberAndZi(othercurscore)
    if othercurscore > 0 then
        str = "+" .. str
    end
    curpath = othercurscore < 0 and HandredcattleRes.FNT_RESULT_LOSE or HandredcattleRes.FNT_RESULT_WIN
    --self:flyNumOfGoldChange(self.m_pLbResultOther, curpath, str)

    -- 显示自己分数动画
    if HoxDataMgr.getInstance():getUsrChipCount() > 0 or HoxDataMgr.getInstance():isBanker() then
        if HoxDataMgr.getInstance():isBanker() then
            selfcurscore = bankercurscore
        else
            selfcurscore = tem.llFinaResult
        end
        str = LuaUtils.getFormatGoldAndNumberAndZi(selfcurscore)
        if selfcurscore > 0 then
            str = "+" .. str
        end

        curpath = selfcurscore < 0 and HandredcattleRes.FNT_RESULT_LOSE or HandredcattleRes.FNT_RESULT_WIN
        --self:flyNumOfGoldChange(self.m_pLbResultSelf, curpath, str)
    end
    
    --自己金币变化动画
--    if llAwardValue > 0 then
--        --Effect:getInstance():showScoreChangeEffect(self.m_pLbUsrGold, llUserScore - llAwardValue, llAwardValue, HoxDataMgr.getInstance():getGameSelfScore(), 1, 10)
--        --AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_WIN)
--        self.m_pLbUsrGold:setString(LuaUtils.getFormatGoldAndNumberAndZi(HoxDataMgr.getInstance():getGameSelfScore() + tem.llFinaResult + llAwareBet))
--    else
--        --local soundStr = (llAwardValue == 0) and  HandredcattleRes.SOUND_OF_NOBET or HandredcattleRes.SOUND_OF_LOSE
--        --AudioManager.getInstance():playSound(soundStr)
--        self.m_pLbUsrGold:setString(LuaUtils.getFormatGoldAndNumberAndZi(HoxDataMgr.getInstance():getGameSelfScore() + tem.llFinaResult + llAwareBet))
--    end

--    local llGold = 0
--    if HoxDataMgr.getInstance():isBanker() then
--        HoxDataMgr.getInstance():setBankerGold(HoxDataMgr.getInstance():getGameSelfScore())
--    end
--    local llGold = HoxDataMgr.getInstance():getBankerChairId() == INVALID_CHAIR
--                    and 999999999
--                    or HoxDataMgr.getInstance():getBankerGold()
--    self.m_pLbZhuangGold:setString(LuaUtils.getFormatGoldAndNumberAndZi(llGold))
end

--弹金币动画
function HandredcattleLayer:flyNumOfGoldChange(lb, filepath, str)
    lb:setFntFile(filepath)
    lb:setString(str)
    lb:setOpacity(255)
    lb:setScale(0.6)
    lb:setVisible(true)
    local pos = cc.p(lb:getPosition())
    local callBack = cc.CallFunc:create(function ()
        lb:setVisible(false)
        lb:setPosition(pos)
        lb:stopAllActions()
    end)

    local pSeq = cc.Sequence:create(
        cc.EaseBounceOut:create(cc.MoveBy:create(0.8, cc.p(0,-50))),
        cc.DelayTime:create(2.0),
        cc.FadeOut:create(0.4),
        callBack
        )

    lb:runAction(pSeq)   
end

--等待动画
function HandredcattleLayer:initWaitingAnim()
    if self.m_pNodeTip:getChildByTag(Handredcattle_Const.waitArmTag) then return end
    local armature = ccs.Armature:create(HandredcattleRes.ANI_OF_DENGDAI)
    armature:setPosition(Handredcattle_Const.waitArmPos)
    armature:getAnimation():play("Animation1")
    armature:setName(HandredcattleRes.ANI_OF_DENGDAI)
    armature:setTag(Handredcattle_Const.waitArmTag)
    self.m_pNodeTip:addChild(armature)
end

--转场动画
function HandredcattleLayer:initJieduanAnim(index)
    local armature = ccs.Armature:create(HandredcattleRes.ANI_OF_JIEDUAN)
    armature:setPosition(Handredcattle_Const.jieduanArmPos)
    local animationEvent = function(armature, movementType, movementID)
        if (movementType == ccs.MovementEventType.complete or movementType == ccs.MovementEventType.loopComplete) then
            self.m_pLianzhuangImage:setVisible(false)
            armature:removeFromParent()

            if 2 == index then
                self:playCardsEffect()
            end 
        end
    end
    armature:getAnimation():play(string.format("Animation%d",index) )
    armature:getAnimation():setMovementEventCallFunc(animationEvent)
    armature:setName(HandredcattleRes.ANI_OF_JIEDUAN)
    armature:setTag(Handredcattle_Const.jieduanArmTag)
    self.m_pNodeTip:addChild(armature)
end

--赢牌动画
function HandredcattleLayer:playWinEffect()
    local tem = HoxDataMgr.getInstance():getOpenData()
    local showWinEffect = false
    for i = 1, 4 do
        if HoxDataMgr.getInstance():isBanker() then
            showWinEffect = -tem.bPlayerWin[i]
        else
            showWinEffect = tem.bPlayerWin[i]
        end
        if showWinEffect>0 then
           local ani = ccs.Armature:create(HandredcattleRes.ANI_OF_WIN)
            ani:setPosition(Handredcattle_Const.winArmPos[i])
            ani:getAnimation():play("Animation1")
            ani:setName(HandredcattleRes.ANI_OF_WIN)
            ani:setTag(Handredcattle_Const.winArmTag+i)
            local func = function(armature,movementType,strName)
                if movementType == ccs.MovementEventType.complete then
                    self:effectEnd(ani,ccs.MovementEventType.complete,i)
                     self.m_pLbUsrGold:setString(HoxDataMgr.getInstance():getGameSelfScore())
                end
            end
            ani:getAnimation():setMovementEventCallFunc(func)
            self.m_pNodeTip:addChild(ani)
            table.insert( self.m_pWinEffect, ani)
        end
    end

    --同步历史记录
    HoxDataMgr.getInstance():addBoardCount()
    --通知结算界面更新历史数据
   
    self.m_trendLayer:updateView()
  --  sendMsg(self,Handredcattle_Events.MSG_HOX_UPDATE_HISTORY_INFO, _event)
end

-- 显示最后3秒倒计时和播放音效
function HandredcattleLayer:showDaojishi(count)
    local armature = self.m_pNodeTip:getChildByTag(Handredcattle_Const.daojishiArmTag)
    --if self.m_pNodeTip:getChildByTag(Handredcattle_Const.daojishiArmTag) then return end
    if nil == armature then
        armature = ccs.Armature:create(HandredcattleRes.ANI_OF_DAOJISHI)
        armature:setPosition(Handredcattle_Const.daojishiArmPos)
        armature:setName(HandredcattleRes.ANI_OF_DAOJISHI)
        armature:setTag(Handredcattle_Const.daojishiArmTag)
        self.m_pNodeTip:addChild(armature)
    end
    
    armature:getAnimation():playWithIndex(0, -1, -1)
    --armature:getAnimation():play("Animation1")

    local frameIndex = math.ceil((3 -count) * 60 + 1)
    if frameIndex > 170 then frameIndex = 170 end 
    armature:getAnimation():gotoAndPlay(frameIndex)

    --帧回调无效？使用延迟播放
    for i = 1, count do
        self:doSomethingLater(function ()
            if self.m_nSoundClock then
                AudioManager.getInstance():stopSound(self.m_nSoundClock)
                self.m_nSoundClock = nil
            end
            if self._bIsPlayDaojishi then
                self.m_nSoundClock = AudioManager.getInstance():playSound("game/handredcattle/sound/sound-countdown.mp3")
            end
        end , i - 1)
    end
end

-- 克隆一个赔付的筹码对象
function HandredcattleLayer:clonePayoutChip(jettonIndex)
    local pos = cc.p(411,678)
    local pSpr = cc.Sprite:create(self:getJettonIconFile(jettonIndex))
    if not pSpr then return end
    pSpr:setScale(0.35)
    pSpr:setPosition(pos)
    pSpr:setLocalZOrder(0)
    self.m_pNodeChipSpr:addChild(pSpr)
    return pSpr
end

-- 以队列方式进行飞筹码
function HandredcattleLayer:flychipex()
    -- 处理现有的筹码对象列表
    -- 筹码精灵对象  tag() 代表投注在哪个区域
    local tem = HoxDataMgr.getInstance():getOpenData()

    local bankerwinvec = {} -- 庄家获取的筹码
    local bankerlostvec = {} -- 庄家赔付筹码
    local selfwinvec = {} -- 我获取的筹码
    local otherwinvec = {} -- 其他人获取的筹码
    local selfvec = {} -- 我的所有区域筹码
    local othervec = {} -- 其他人的所有区域筹码
    local bankerwin = false
    local bankerlose = false
    local otherwin = false
    local selfwin = false

    for i = 1, CHIP_ITEM_COUNT do
        bankerwinvec[i] = {}
        bankerlostvec[i] = {}
        selfwinvec[i] = {}
        otherwinvec[i] = {}
        selfvec[i] = {bankwin =  tem.bPlayerWin[i]<0, vec = {}}
        othervec[i] = {bankwin =  tem.bPlayerWin[i]<0, vec = {}}
    end

    for k, v in pairs(self.m_vecSpriteSelf) do
         table.insert(selfvec[v.wChipIndex].vec, v)
    end

    for k, v in pairs(self.m_vecSpriteOther) do
        table.insert(othervec[v.wChipIndex].vec, v)
    end

    -- 遍历获胜的区域 必须用pairs方式 保证所有筹码遍历到
    local selfPos = self.m_selfPos
    local bankerPos = (HoxDataMgr.getInstance():getBankerChairId() == INVALID_CHAIR) and self.m_sysbankPos or self.m_bankPos
    local otherPos = self.m_othersPos
    for i = 1, CHIP_ITEM_COUNT do
        for k, v in pairs(selfvec[i].vec) do
            if v then
                v.pChipSpr:setTag(v.wJettonIndex)
                if selfvec[i].bankwin then
                    table.insert(bankerwinvec[i], {sp = v.pChipSpr, endpos = bankerPos})
                    bankerwin = true
                else
                    table.insert(selfwinvec[i], {sp = v.pChipSpr, endpos = selfPos})
                    selfwin = true
                end
            end
        end

        for k, v in pairs(othervec[i].vec) do
            if v then
                v.pChipSpr:setTag(v.wJettonIndex)
                if selfvec[i].bankwin then
                    table.insert(bankerwinvec[i], {sp = v.pChipSpr, endpos = bankerPos})
                    bankerwin = true
                else
                    table.insert(otherwinvec[i], {sp = v.pChipSpr, endpos = otherPos})
                    otherwin = true
                end
            end
        end
    end

    -- 处理庄家需要赔付的筹码 庄家赔付的筹码是多个区域的 需要分区域处理
    -- 同时新建的赔付的筹码引用需要加在玩家获胜筹码集合中 赔付筹码大余8个 则取半
    -- 按倍率赔付筹码数量 大于一定数量的筹码 则不再增加
    -- 赔付筹码是否需要随机分配结束pos飞行？
    local countself = 0
    local countother = 0
    local rewardCount = 0
    local rewardCountMax = 30 -- 最多一个区域赔付30个筹码
    local offset = 7
    for i = 1, CHIP_ITEM_COUNT do
        countself = #selfwinvec[i] -- 后面会对vec进行插入 所以保存一个初始的索引
        for j = 1, countself do
            -- 取出原始的筹码对象复制
            local oldsp = selfwinvec[i][j].sp
            if oldsp then
                local newsp = self:clonePayoutChip(oldsp:getTag())
                -- 初始点在庄家处
                newsp:setPosition(bankerPos)
                -- 加入赔付队列 落点坐标直接从预定义的随机落点取 赔付的筹码使用随机区域落点互换
                --table.insert( bankerlostvec[i], { sp = newsp, endpos = self.m_randmChipOthers[i][m % #self.m_randmChipOthers[i]]})
                table.insert( bankerlostvec[i], { sp = newsp, endpos = cc.p(oldsp:getPosition())})
                bankerlose = true
                -- 加入自己的获胜队列
                table.insert( selfwinvec[i], { sp = newsp, endpos = selfPos })
            end
        end
        countother = #otherwinvec[i]
        countother = countother > 8 and countother/2 or countother
        for j = 1, countother do
            local oldsp = otherwinvec[i][j].sp
            if oldsp then
                local newsp = self:clonePayoutChip(oldsp:getTag())
                newsp:setPosition(bankerPos)
                local randx = (math.random(-offset*100, offset*100)) / 100
                local randy = (math.random(-offset*100, offset*100)) / 100
                --table.insert( bankerlostvec[i], { sp = newsp, endpos = self.m_randmChipSelf[i][j % #self.m_randmChipSelf[i]] })
                table.insert( bankerlostvec[i], { sp = newsp, endpos = cc.p(oldsp:getPosition()) })
                bankerlose = true
                --table.insert( bankerlostvec[i], { sp = newsp, endpos = cc.p(oldsp:getPosition()) })
                table.insert( otherwinvec[i], { sp = newsp, endpos = otherPos })
            end
        end
    end

    -- 庄家获取筹码
    local jobitem1 = {
        flytime       = CHIP_FLY_TIME,                                -- 飞行时间
        flytimedelay  = false,                                        -- 飞行时间延长随机时间(0.05~0.15)
        flysteptime   = CHIP_FLY_STEPDELAY,                           -- 筹码队列飞行间隔
        nextjobtime   = CHIP_JOBSPACETIME,                            -- 下个任务执行间隔时间
        chips         = bankerwinvec,                                 -- 筹码队列集合 二维数组[下注区域索引][筹码对象引用]
        preCB         = function()                                    -- 任务开始时执行的回调，此回调根据preCBExec控制只执行一次
                            if bankerwin then
                                AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_GETGOLD)
                            end
                        end,
        preCBExec     = false,
        --overCB        = function() print("庄家获胜筹码飞行完毕") end, -- 动画任务完成后回调
        hideAfterOver = true                                          -- 动画完成后隐藏
    }

    -- 庄家赔付筹码
    local jobitem2 = {
        flytime       = CHIP_FLY_TIME,
        flysteptime   = CHIP_FLY_STEPDELAY,
        nextjobtime   = CHIP_JOBSPACETIME,
        chips         = bankerlostvec,
        preCB         = function()
                            if bankerlose then
                                AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_GETGOLD)
                            end
                        end,
        --overCB        = function() print("庄家赔付筹码飞行完毕") end,
        hideAfterOver = false
    }

    -- 其他人赢得的筹码
    local jobitem3 = {
        flytime       = CHIP_FLY_TIME + 0.1,
        flysteptime   = CHIP_FLY_STEPDELAY,
        nextjobtime   = CHIP_JOBSPACETIME,
        chips         = otherwinvec,
        preCB         = function()
                            if otherwin then
                                AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_GETGOLD)
                            end
                        end,
        preCBExec     = false,
        --overCB        = function() print("其他人获胜筹码飞行完毕") end,
        hideAfterOver = true
    }

    -- 自己赢得的筹码
    local jobitem4 = {
        flytime       = CHIP_FLY_TIME + 0.1,
        flytimedelay  = true,                                        -- 飞行时间延长随机时间(0.05~0.15)
        flysteptime   = 0.01,
        nextjobtime   = CHIP_JOBSPACETIME,
        chips         = selfwinvec,
        preCB         = function()
                            if selfwin then
                                AudioManager.getInstance():playSound(HandredcattleRes.SOUND_OF_GETGOLD)
                            end
                        end,
        preCBExec     = false,
        --overCB        = function() print("自己获胜筹码飞行完毕") end,
        hideAfterOver = true
    }

    self.m_flyJobVec.nIdx = 1 -- 任务处理索引
    self.m_flyJobVec.flyIdx = 1 -- 飞行队列索引
    self.m_flyJobVec.jobVec = {} -- 任务对象
    self.m_flyJobVec.overCB = function()
        print("所有飞行任务执行完毕")
        self.m_flyJobVec = {}
        --self:showGoldChangeAnimation()
    --    self:playUserScoreEffect()

        self:doSomethingLater(function()
            if self.resultLayer == nil then
                self.resultLayer = HandredcattleResultlayer.create()
                self.resultLayer:setName("HandredcattleResultlayer")
                self.resultLayer:addTo(self.m_pathUI, 5)
            else
                self.resultLayer:setVisible(true)
                self.resultLayer:initView()
            end
        end, 1.0)
    end

    table.insert(self.m_flyJobVec.jobVec, { jobitem1 }) -- 1 飞行庄家获取筹码
    table.insert(self.m_flyJobVec.jobVec, { jobitem2 }) -- 2 飞行庄家赔付筹码
    table.insert(self.m_flyJobVec.jobVec, { jobitem3, jobitem4 }) -- 3 其他人筹码和自己筹码一起飞行

    self:doFlyJob()
end



-- 执行单个的飞行任务
function HandredcattleLayer:doFlyJob()
    -- 全部任务执行完成之前，被清理重置
    if nil == self.m_flyJobVec.nIdx or nil == self.m_flyJobVec.jobVec then return end

    -- 任务处理完了
    if self.m_flyJobVec.nIdx > #self.m_flyJobVec.jobVec then
        if self.m_flyJobVec.overCB then
            self.m_flyJobVec.overCB()
        end
        return
    end

    -- 取出一个当前需要处理的飞行任务
    local job = self.m_flyJobVec.jobVec[self.m_flyJobVec.nIdx]
    if not job then return end
    if 0 == #job then return end

    -- 按队列取出需要飞行的对象进行动画处理
    local flyvec = {}
    local mf = math.floor
    if self.m_flyJobVec.flyIdx <= CHIP_FLY_SPLIT then
        for i = 1, #job do
            if job[i] then
                if job[i].preCB and (not job[i].preCBExec) then
                    job[i].preCB()
                    job[i].preCBExec = true
                end
                for j = 1, #job[i].chips do
                    local segnum = mf(#job[i].chips[j] / CHIP_FLY_SPLIT) -- 计算需要分成几段
                    for m = 0, segnum do
                        local tgg = job[i].chips[j][m*CHIP_FLY_SPLIT + self.m_flyJobVec.flyIdx]
                        if tgg then
                            table.insert(flyvec, { sptg = tgg, idx = i })
                        end                    
                    end
                end
            end
        end
    end

    -- 当前队列都飞完了
    if 0 == #flyvec then
        -- 下个任务的执行
        self.m_flyJobVec.nIdx = self.m_flyJobVec.nIdx + 1
        self.m_flyJobVec.flyIdx = 1
        self:doSomethingLater(function ()
            for i = 1, #job do
                if job[i].overCB then job[i].overCB() end
            end
            self:doFlyJob()
        end , job[1].nextjobtime) -- 多个任务时 取第一个任务的时间
        return
    end

    -- 开始飞筹码
    for i = 1, #flyvec do
        local tg = flyvec[i]
        if tg and tg.sptg then
            local ts = job[tg.idx].flytimedelay and job[tg.idx].flytime + math.random(5,15) / 100 or job[tg.idx].flytime
            local mt = cc.MoveTo:create(ts, tg.sptg.endpos)
            if i == #flyvec then -- 最后一个筹码飞行完成后执行下一次的飞行回调
                self.m_flyJobVec.flyIdx = self.m_flyJobVec.flyIdx + 1
                self:doSomethingLater(function ()
                    self:doFlyJob()
                end , job[tg.idx].flysteptime)
            end

            if job[tg.idx].hideAfterOver then
                tg.sptg.sp:runAction(cc.Sequence:create(cc.Show:create(), mt, cc.Hide:create()))
            else
                tg.sptg.sp:runAction(cc.Sequence:create(cc.Show:create(), mt))
            end
        end
    end
end

function HandredcattleLayer:showRecharge()
    sendMsg(self,Public_Events.MSG_SHOW_MESSAGEBOX, "go-recharge")
end

function HandredcattleLayer:setRecordId(id)
    if id and id ~= "" then
        self.mTextRecord:setString("牌局ID:"..id)
    end
end

--endregion

return HandredcattleLayer
