--region *.lua
--Date
--
--endregion

local Handredcattle_Events = require("game.handredcattle.scene.HandredcattleEvent")
local HandredcattleNotice = class("HandredcattleNotice", cc.Node)

function HandredcattleNotice.create(_index)
    return HandredcattleNotice:new():init(_index)
end

function HandredcattleNotice:ctor()
    self:enableNodeEvents()
    self.m_nNoticeType = 0
end

function HandredcattleNotice:init(_index)

    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    self.m_pathUI = ccs.GUIReader:getInstance():widgetFromJsonFile("game/handredcattle/HandredcattleNoticeView.json")
    self.m_pathUI:addTo(self.m_rootUI)


    self.m_pInfoLabel = ccui.Helper:seekWidgetByName(self.m_pathUI, "LB_info")
    self.m_pInfoLabel2 = ccui.Helper:seekWidgetByName(self.m_pathUI, "LB_info_2")

    self.m_pBtnOk = ccui.Helper:seekWidgetByName(self.m_pathUI, "BTN_ok")
    self.m_pBtnOk:addClickEventListener(handler(self,self.onOKClicked))
    self.m_pBtnNo = ccui.Helper:seekWidgetByName(self.m_pathUI, "BTN_no")
    self.m_pBtnNo:addClickEventListener(handler(self,self.onReturnClicked))

    self.m_nNoticeType = _index or 0
    self:setNoticeType()
   return self
end 

function HandredcattleNotice:onEnter()
    
end

function HandredcattleNotice:onExit()

end

function HandredcattleNotice:onOKClicked()
    AudioManager.getInstance():playSound("game/handredcattle/sound/sound-button.mp3")
    local _event = {
        name = Handredcattle_Events.MSG_GAME_EXIT,
        packet = nil,
    }
    SLFacade:dispatchCustomEvent(Handredcattle_Events.MSG_GAME_EXIT, _event)
    self:setVisible(false)
    self:removeFromParent()
end

function HandredcattleNotice:onReturnClicked()
    AudioManager.getInstance():playSound("game/handredcattle/sound/sound-button.mp3")
    self:removeFromParent()
end

function HandredcattleNotice:setNoticeType()
    if self.m_nNoticeType == 1 then 
        self.m_pInfoLabel:setString(LuaUtils.getLocalString("MESSAGE_10"))
        self.m_pInfoLabel2:setString("")
    elseif self.m_nNoticeType == 3 then
        self.m_pInfoLabel:setString(LuaUtils.getLocalString("MESSAGE_12"))
        self.m_pInfoLabel2:setString("")
    end
end

return HandredcattleNotice