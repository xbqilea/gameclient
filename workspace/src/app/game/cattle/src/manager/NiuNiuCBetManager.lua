--region *.lua
--Date
--
--endregion

local NiuNiuCBetManager = class("NiuNiuCBetManager")

NiuNiuCBetManager.g_instance = nil
local CHIP_ITEM_COUNT =5
NiuNiuCBetManager.ELOTTERYBASEMONEY = {
    1,     --10,       --0.1
    10,    --100,      --1
    50,    --500,      --5
    100,   --1000,     --10
    500,   --5000,     --50 
}

----结果类型
--HIT_DST_TYPE = {
--    eHitNor     = 1,				--普通类型
--    eHitBoss    = 2,				--超级BOSS ( 100倍的大鲨鱼 )
--    eHitKillAll = 3,				--通杀类型
--    eHitSysLost = 4,				--通赔类型
--}

local JETTON_ITEM_COUNT = 5

function NiuNiuCBetManager.getInstance()
    if not NiuNiuCBetManager.g_instance then
        NiuNiuCBetManager.g_instance = NiuNiuCBetManager.new()
    end
    return NiuNiuCBetManager.g_instance
end

function NiuNiuCBetManager.releaseInstance()
    NiuNiuCBetManager.g_instance = nil
end


function NiuNiuCBetManager:ctor()
    self.m_eStatus = false
    self.m_eLotteryBaseScore = 10
    self.m_llWinScore = 0
    self.m_llChipScore = 0
    self.m_sGameResult = 0
    self.m_sHitDstType = 0
    self.m_sDstIndex = -1
    self.m_sDstPos = 0
    self.m_sLastPos = 0
    self.m_bContinue = true
    self.m_sTimeCount = 0
    -------
    self.m_vecUserShip = {}
    self.m_vecUserShipLast = {}
    self.m_vecUserShipLastSecond = {}
    self.arrAllServerStatic = {}
    self.arrAllServerAllStatic = {}
    self.arrAllServerEveryAmount = {}
    self.m_vecGameOpenLog = 0
    self.m_vecReqUserID = {}            --庄家userid列表
    self.m_arrRemaingBettingGold = {}   --每个选项剩余多少可投注
    self.m_arrMatchRatioInfo = {}

    for i = 1, CHIP_ITEM_COUNT do
        self.arrAllServerStatic[i] = 0
        self.arrAllServerAllStatic[i] = 0
    end

    self._nBankerUserId = 0
    self._nLastBankerUserId = 0
    self._nSelfSlot = 0
    self._strBankerName = ""
    self._iBankerTimes = 0
    self._llBankerScore = 0
    self._llLastBankerScore = 0

    self.m_arrMatchRatioInfo = {
        6,
        8,
        8,
        12,
        6,
        8,
        8,
        12,
        24,
        2,
        2,
    }
    self:initRemaingBettingValue()
    -------

    --飞禽和赛马
    self._strBankerUserName = ""
    self.m_strWinResult = ""
    self.m_nDealerId = 0
    self.m_bIsBetting = false
    self.m_nPlayerResultGold = 0
    self.m_nBankerResultGold  = 0
    self.m_nBankerScoreNeed = 0
    self.m_vecPlayerWin = {}

    self.m_llJettonValue = {1,10,50,100,500}
--    for i = 1, JETTON_ITEM_COUNT do
--        self.m_llJettonValue[i] = 10
--    end
end

function NiuNiuCBetManager:clear()
    for i = 1, CHIP_ITEM_COUNT do
        self.arrAllServerStatic[i] = 0
        self.arrAllServerAllStatic[i] = 0
    end
    self:setShipScore(0)
    self:setWinScore(0)
    self.m_vecUserShip = {}
    self.m_vecUserShipLast = {}
    self.m_vecUserShipLastSecond = {}
    self.m_sDstIndex = 1
    self.m_strWinResult = ""
    self._iBankerTimes = 0
    self._llBankerScore = 0
    self._llLastBankerScore = 0
    self._strBankerName = {}
    self._nBankerUserId = 0
    self._nLastBankerUserId = 0
    self._nSelfSlot = 0
    self.m_vecReqUserID = {}
    return true
end

function NiuNiuCBetManager:setStatus(m_eStatus)
    self.m_eStatus = m_eStatus
end

function NiuNiuCBetManager:getStatus()
    return self.m_eStatus
end

function NiuNiuCBetManager:setLotteryBaseScore(m_eLotteryBaseScore)
    self.m_eLotteryBaseScore = m_eLotteryBaseScore
end

function NiuNiuCBetManager:getLotteryBaseScore()
    return self.m_eLotteryBaseScore
end

--玩家的结算数据
function NiuNiuCBetManager:setWinScore(m_llWinScore)
    self.m_llWinScore = m_llWinScore
end

function NiuNiuCBetManager:getWinScore()
    return self.m_llWinScore
end

function NiuNiuCBetManager:setShipScore(m_llChipScore)
    self.m_llChipScore = m_llChipScore
end

function NiuNiuCBetManager:getShipScore()
    return self.m_llChipScore
end

function NiuNiuCBetManager:setGameResult(m_sGameResult)
    self.m_sGameResult = m_sGameResult
end

function NiuNiuCBetManager:getGameResult()
    return self.m_sGameResult
end

--本轮的开奖数据
function NiuNiuCBetManager:setHitDstType(m_sHitDstType)
    self.m_sHitDstType = m_sHitDstType
end

function NiuNiuCBetManager:getHitDstType()
    return self.m_sHitDstType
end

function NiuNiuCBetManager:setDstIndex(m_sDstIndex)
    self.m_sDstIndex = m_sDstIndex
end

function NiuNiuCBetManager:getDstIndex()
    return self.m_sDstIndex
end

function NiuNiuCBetManager:setLastPos(m_sLastPos)
    self.m_sLastPos = m_sLastPos
end

function NiuNiuCBetManager:getLastPos()
    return self.m_sLastPos
end

function NiuNiuCBetManager:setDstPos(m_sDstPos)
    self.m_sDstPos = m_sDstPos
end

function NiuNiuCBetManager:getDstPos()
    return self.m_sDstPos
end

function NiuNiuCBetManager:setContinue(m_bContinue)
    self.m_bContinue = m_bContinue
end

function NiuNiuCBetManager:getContinue()
    return self.m_bContinue
end

function NiuNiuCBetManager:setTimeCount(m_sTimeCount)
    self.m_sTimeCount = m_sTimeCount
end

function NiuNiuCBetManager:getTimeCount()
    return self.m_sTimeCount
end
--
--庄家的ID
function NiuNiuCBetManager:setBankerUserId(_nBankerUserId)
    self._nBankerUserId = _nBankerUserId
end

function NiuNiuCBetManager:getBankerUserId()
    return self._nBankerUserId
end

--上局庄家的Id
function NiuNiuCBetManager:setLastBankerUserId(_nLastBankerUserId)
    self._nLastBankerUserId = _nLastBankerUserId
end

function NiuNiuCBetManager:getLastBankerUserId()
    return self._nLastBankerUserId
end

--本人排位
function NiuNiuCBetManager:setSelfSlot(_nSelfSlot)
    self._nSelfSlot = _nSelfSlot
end

function NiuNiuCBetManager:getSelfSlot()
    return self._nSelfSlot
end

--庄家的名字
function NiuNiuCBetManager:setBankerName(_strBankerName)
    self._strBankerName = _strBankerName
end

function NiuNiuCBetManager:getBankerName()
    return self._strBankerName
end

--庄家坐庄次数
function NiuNiuCBetManager:setBankerTimes(_iBankerTimes)
    self._iBankerTimes = _iBankerTimes
end

function NiuNiuCBetManager:getBankerTimes()
    return self._iBankerTimes
end

--庄家的分数
function NiuNiuCBetManager:setBankerScore(_llBankerScore)
    self._llBankerScore = _llBankerScore
end

function NiuNiuCBetManager:getBankerScore()
    return self._llBankerScore
end

--庄家的分数
function NiuNiuCBetManager:setLastBankerScore(_llLastBankerScore)
    self._llLastBankerScore = _llLastBankerScore
end

function NiuNiuCBetManager:getLastBankerScore()
    return self._llLastBankerScore
end

--[[
    飞禽和赛马的结算数据
--]]
--庄家的名字
function NiuNiuCBetManager:setBankerUserName(_strBankerUserName)
    self._strBankerUserName = _strBankerUserName
end

function NiuNiuCBetManager:getBankerUserName()
    return self._strBankerUserName
end

--显示结果的字符串
function NiuNiuCBetManager:setWinResult(m_strWinResult)
    self.m_strWinResult = m_strWinResult
end

function NiuNiuCBetManager:getWinResult()
    return self.m_strWinResult
end

--庄家ID
function NiuNiuCBetManager:setDealerId(m_nDealerId)
    self.m_nDealerId = m_nDealerId
end

function NiuNiuCBetManager:getDealerId()
    return self.m_nDealerId
end

--是否下注了
function NiuNiuCBetManager:setIsBetting(m_bIsBetting)
    self.m_bIsBetting = m_bIsBetting
end

function NiuNiuCBetManager:getIsBetting()
    return self.m_bIsBetting
end

--玩家结果金币
function NiuNiuCBetManager:setPlayerResultGold(m_nPlayerResultGold)
    self.m_nPlayerResultGold = m_nPlayerResultGold
end

function NiuNiuCBetManager:getPlayerResultGold()
    return self.m_nPlayerResultGold
end

--庄家结果金币
function NiuNiuCBetManager:setBankerResultGold(m_nBankerResultGold)
    self.m_nBankerResultGold = m_nBankerResultGold
end

function NiuNiuCBetManager:getBankerResultGold()
    return self.m_nBankerResultGold
end

--庄家结果金币
function NiuNiuCBetManager:setBankerScoreNeed(m_nBankerScoreNeed)
    self.m_nBankerScoreNeed = m_nBankerScoreNeed
end

function NiuNiuCBetManager:getBankerScoreNeed()
    return self.m_nBankerScoreNeed
end

------------
function NiuNiuCBetManager:getScoreShip(index)
    local value = 0
    for k,v in pairs(self.m_vecUserShip) do
        if v.wChipIndex == index then
            value = value + v.llChipValue
        end
    end
    return value
end

function NiuNiuCBetManager:setAllServerStatic( index, llValue)
    if index >= 0 and index < CHIP_ITEM_COUNT then
        self.arrAllServerStatic[index] = llValue
    end
end

function NiuNiuCBetManager:getAllServerStatic( index)
    local temp = 0
    if index >= 0 and index < CHIP_ITEM_COUNT then
        temp = self.arrAllServerStatic[index]
    end
    return temp
end

function NiuNiuCBetManager:clearAllServerAllStatic()
    for i = 1, CHIP_ITEM_COUNT do
        self.arrAllServerStatic[i] = 0
        self.arrAllServerAllStatic[i] = 0
    end
end

function NiuNiuCBetManager:getGameOpenLog( index)
    if index >=0 and index < #self.m_vecGameOpenLog then
        return self.m_vecGameOpenLog[index]
    end
    return 0
end

function NiuNiuCBetManager:refreshLast()
    if #self.m_vecUserShip > 0 then
        self.m_vecUserShipLast = self.m_vecUserShip
        self.m_vecUserShip = {}
    end
end

function  NiuNiuCBetManager:addUserShip( ship)
    if ship then
        table.insert(self.m_vecUserShip,ship)
        table.insert(self.m_vecUserShipLastSecond,ship)
    end
end

function NiuNiuCBetManager:getUserShip( index)
    local chip = {}
    if index < #self.m_vecUserShip then
        chip = self.m_vecUserShip[index]
    end
    return chip
end

function NiuNiuCBetManager:clearUserChipLastSecond()
    self.m_vecUserShipLastSecond = {}
end

function NiuNiuCBetManager:clearUserChip() 
    self.m_vecUserShip = {}
end

function NiuNiuCBetManager:getUserChipLastSecond( i)
    local ret = 0
    for k, v in pairs(self.m_vecUserShipLastSecond) do
        if v and v.wChipIndex == i then
            ret = ret + v.llChipValue
        end
    end
    return ret
end

function NiuNiuCBetManager:getUserShipLast( index)
    local chip = {}
    if index >=0 and index < #self.m_vecUserShipLas then
        chip = self.m_vecUserShipLast[index]
    end
    return chip
end

--获取筹码按钮的响应状态
function NiuNiuCBetManager:getJettonEnableState( index)
--    local array = {NiuNiuCBetManager.ELOTTERYBASEMONEY.eMONEY_ONE, NiuNiuCBetManager.ELOTTERYBASEMONEY.eMONEY_TWO, NiuNiuCBetManager.ELOTTERYBASEMONEY.eMONEY_THREE, NiuNiuCBetManager.ELOTTERYBASEMONEY.eMONEY_FOUR, NiuNiuCBetManager.ELOTTERYBASEMONEY.eMONEY_FIVE, NiuNiuCBetManager.ELOTTERYBASEMONEY.eMONEY_SIX}
    if self:getStatus() ~= EGAMESTATUE.eGAMELOTTERY then
        --非投注状态
        return false
    end
    if PlayerInfo.getInstance():getTempUserScore() < self.m_llJettonValue[index] then --NiuNiuCBetManager.ELOTTERYBASEMONEY[index]
        --玩家钱币不足
        return false
    end
    return true
end

--获取筹码选择标志的响应状态
function NiuNiuCBetManager:getJettonSelAdjust()
    if PlayerInfo.getInstance():getTempUserScore() > self:getLotteryBaseScore() then
        return -1
    end
--    local array = self.m_llJettonValue --NiuNiuCBetManager.ELOTTERYBASEMONEY
    for i = JETTON_ITEM_COUNT , 1, -1 do
        if self:getJettonScore(i) <= PlayerInfo.getInstance():getTempUserScore() then
            return  i
        end
    end
    return 0
end

function NiuNiuCBetManager:getJettonScore( index)
    if not index or index < 1 or index > JETTON_ITEM_COUNT then
        return self.m_llJettonValue[1]
    end
    return self.m_llJettonValue[index]

--    if not index then return 10 end
--    return NiuNiuCBetManager.ELOTTERYBASEMONEY[index]
--    local array = {NiuNiuCBetManager.ELOTTERYBASEMONEY.eMONEY_ONE, NiuNiuCBetManager.ELOTTERYBASEMONEY.eMONEY_TWO, NiuNiuCBetManager.ELOTTERYBASEMONEY.eMONEY_THREE, 
--                    NiuNiuCBetManager.ELOTTERYBASEMONEY.eMONEY_FOUR, NiuNiuCBetManager.ELOTTERYBASEMONEY.eMONEY_FIVE, NiuNiuCBetManager.ELOTTERYBASEMONEY.eMONEY_SIX}
--    return array[index]
end

function  NiuNiuCBetManager:setBankerUserIdByID( nUserID)
    if self._nLastBankerUserId == 0 then
        self._nLastBankerUserId = nUserID
    else
        self._nLastBankerUserId = self._nBankerUserId
    end
    self._nBankerUserId = nUserID
end

function NiuNiuCBetManager:getBankerNickName() 

end

function NiuNiuCBetManager:getBankerGold()
    return self._llBankerScore
end

function NiuNiuCBetManager:setBankerGold( llBankerScore)
    self._llLastBankerScore = self._llBankerScore
    self._llBankerScore = llBankerScore
end

function NiuNiuCBetManager:getBankerListSize()
    return #self.m_vecReqUserID
end

function NiuNiuCBetManager:addBankerListByUserId( nUserID)
    if nUserID then
        table.insert(self.m_vecReqUserID, nUserID)
    end
end

function NiuNiuCBetManager:getBankerListByIndex( nIdx)
    if nIdx >= #self.m_vecReqUserID or nIdx < 1 then
        return 0
    end
    return self.m_vecReqUserID[index]
end

function NiuNiuCBetManager:cleanBankerList()
    self.m_vecReqUserID = {}
end

--去计算每个选项还剩余多少钱可投注
function NiuNiuCBetManager:initRemaingBettingValue()
    for i = 1, CHIP_ITEM_COUNT do
        self.m_arrRemaingBettingGold[i] = 0
    end
end

function NiuNiuCBetManager:resetRemaingBettingValue()
    local m_nAllBettingGold = 0
    if self.nBankerUserId == -1 then
        --系统庄家
        for i = 1, CHIP_ITEM_COUNT do
            self.m_arrRemaingBettingGold[i] = -1
        end
    else
        --玩家庄家
        for i = 1, CHIP_ITEM_COUNT do
            m_nAllBettingGold = m_nAllBettingGold + self.arrAllServerAllStatic[i]
        end
        for j = 1, CHIP_ITEM_COUNT do
            local Remaing = (self._llBankerScore + m_nAllBettingGold - self.arrAllServerAllStatic[j] * self.m_arrMatchRatioInfo[j]) / self.m_arrMatchRatioInfo[j]
            self.m_arrRemaingBettingGold[j] = (Remaing < 1000) and 0 or (Remaing / 1000 * 1000)
        end
    end
end

function NiuNiuCBetManager:getRemaingBettingGold( nIndex)
    if nIndex >= 0 and nIndex < DOWN_COUNT then
        return self.m_arrRemaingBettingGold[nIndex]
    end
    return 0
end

--[[
    飞禽和赛马
--]]
function NiuNiuCBetManager:IsBanker()
    if PlayerInfo.getInstance():getChairID() == self.m_nDealerId or PlayerInfo.getInstance():getUserID() == self.m_nDealerId then
        return true
    end
    return false
end

function NiuNiuCBetManager:clearPlayerWin()
    self.m_vecPlayerWin = {}
end

function NiuNiuCBetManager:addPlayerWin( nRankIndex, strName, nWinGold)
    for k,v in pairs(self.m_vecPlayerWin) do
        if v.nRankIndex == nRankIndex then
            return
        end
    end
    local tmp = {}
    tmp.nRankIndex = nRankIndex
    tmp.strUserNickName = strName
    tmp.nWinGold = nWinGold
    table.insert(self.m_vecPlayerWin, tmp)
end

function NiuNiuCBetManager:getPlayerWinInfo( nIndex)
    local info = {}
    if nIndex >= 0 and nIndex < #self.m_vecPlayerWin then
        info = self.m_vecPlayerWin[nIndex]
    end
    return info
end

function NiuNiuCBetManager:getLotteryBaseScoreIndex()
    local i = 1
    for i = 1, JETTON_ITEM_COUNT do
        if (self.m_llJettonValue[i] == self.m_eLotteryBaseScore) then
            return (i-1)
        end
    end
    return (i - 1)

--    local index = 1
--    if self.m_eLotteryBaseScore == NiuNiuCBetManager.ELOTTERYBASEMONEY[1] then
--        index = 1
--    elseif self.m_eLotteryBaseScore == NiuNiuCBetManager.ELOTTERYBASEMONEY[2] then
--        index = 2
--    elseif self.m_eLotteryBaseScore == NiuNiuCBetManager.ELOTTERYBASEMONEY[3] then
--        index = 3
--    elseif self.m_eLotteryBaseScore == NiuNiuCBetManager.ELOTTERYBASEMONEY[4] then
--        index = 4
--    elseif self.m_eLotteryBaseScore == NiuNiuCBetManager.ELOTTERYBASEMONEY[5] then
--        index = 5
--    elseif self.m_eLotteryBaseScore == NiuNiuCBetManager.ELOTTERYBASEMONEY[6] then
--        index = 6
--    end
--    return (index-1)
end

function  NiuNiuCBetManager:getLotteryBaseScoreByIndex( index)
    if(not index or index < 1 or index > JETTON_ITEM_COUNT) then
        return self.m_llJettonValue[1]
    end
    return self.m_llJettonValue[index]

--    local baseVale = NiuNiuCBetManager.ELOTTERYBASEMONEY[iIndex] or 0
--    if iIndex == 1 then
--        baseVale = NiuNiuCBetManager.ELOTTERYBASEMONEY.eMONEY_ONE
--    elseif iIndex == 2 then
--        baseVale = NiuNiuCBetManager.ELOTTERYBASEMONEY.eMONEY_TWO
--    elseif iIndex == 3 then
--        baseVale = NiuNiuCBetManager.ELOTTERYBASEMONEY.eMONEY_THREE
--    elseif iIndex == 4 then
--        baseVale = NiuNiuCBetManager.ELOTTERYBASEMONEY.eMONEY_FOUR
--    elseif iIndex == 5 then
--        baseVale = NiuNiuCBetManager.ELOTTERYBASEMONEY.eMONEY_FIVE
--    elseif iIndex == 6 then
--        baseVale = NiuNiuCBetManager.ELOTTERYBASEMONEY.eMONEY_SIX
--    end
--    return baseVale
end

function NiuNiuCBetManager:setAllServerEveryAmount( index, llValue)
    if index >= 0 and index < CHIP_ITEM_COUNT then
        self.arrAllServerEveryAmount[index] = llValue
    end
end

function NiuNiuCBetManager:getAllServerEveryAmount( index)
    if index >= 0 and index < CHIP_ITEM_COUNT then
        return self.arrAllServerEveryAmount[index]
    end
    return 0
end

function NiuNiuCBetManager:clearAllServerEveryAmount()
    for i = 1, CHIP_ITEM_COUNT do
        self.arrAllServerEveryAmount[i] = 0
    end
end

function NiuNiuCBetManager:setJettonScore(index, score)
--    if not data or next(data) == nil then return end
--        print("===================== updateBaseMoney")
--    for k,v in pairs(data) do
--        print(k.. " : " .. tostring(v))
--    end
--    NiuNiuCBetManager.ELOTTERYBASEMONEY = clone(data)
    if(not index or index > JETTON_ITEM_COUNT) then
        return
    end
    self.m_llJettonValue[index] = score or 1000
end

return NiuNiuCBetManager
