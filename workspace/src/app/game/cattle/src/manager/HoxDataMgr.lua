--region *.lua
--Date
--
--endregion

local NiuNiuCBetManager =import(".NiuNiuCBetManager")


local HoxDataMgr = class("HoxDataMgr")
local CardValue_Nothing					= 0                  --牛破
local CardValue_One						= 1                  --牛一
local CardValue_Two						= 2                  --牛二
local CardValue_Three					= 3                  --牛三
local CardValue_Four					= 4                  --牛四
local CardValue_Five					= 5                  --牛五
local CardValue_Six						= 6                  --牛六
local CardValue_Seven					= 7                  --牛七
local CardValue_Eight					= 8                  --牛八
local CardValue_Nine					= 9                  --牛九
local CardValue_Ten						= 10                 --牛牛
local CardValue_Yin						= 11                 --银牛（4花牛）
local CardValue_Jin						= 12                 --金牛（5花牛）
local CardValue_Bomb					= 16                 --炸弹（是由四张相同的牌）
HoxDataMgr.instance = nil   
--HoxDataMgr.E_GAME_STATUE = 
--{
--    eGAME_STATUS_WAIT = 1,            --等待阶段
--	eGAME_STATUS_IDLE = 2,            --进入空闲状态
--	eGAME_STATUS_CHIP = 3,            --进入投注阶段
--	eGAME_STATUS_END  = 4,            --进入开牌阶段
--} 
    HoxDataMgr.eGAME_STATUS_WAIT = 1            --等待阶段
	HoxDataMgr.eGAME_STATUS_IDLE = 2            --进入空闲状态
	HoxDataMgr.eGAME_STATUS_CHIP = 3            --进入投注阶段
	HoxDataMgr.eGAME_STATUS_END  = 4            --进入开牌阶段 

local DOWN_COUNT_HOX = 4
local MAX_HITORY_COUNT					= 40 
local JETTON_ITEM_COUNT = 5

HoxDataMgr.ErrorID = 
{
	Err_AddScore_By_Area = -120000	,-- 下注区错误
	Err_AddScore_By_Banker = -120001,-- 庄家不可下注
	Err_AddScore_By_Times = -120002,-- 下注倍数错误
	Err_AddScore_By_Banker_Limit = -120003,--下注已达庄家上限
	Err_AddScore_By_Player_Limit = -120004,--下注已达上限
	Err_ApplyBanker_By_user_null = -120020,--申请玩家信息为空
	Err_ApplyBanker_By_Lack_Score = -120021,--申请玩家金币不足
	Err_ApplyBanker_By_Array_Full = -120022,--申请队列已满
	Err_ApplyBanker_By_Exist_Banker = -120023,--玩家已在庄家队列中
	Err_ApplyBanker_By_Exist_Apply = -120024,--玩家已在申请队列中
	Success_ApplyBanker_By_Update_Banker = -120025,--刷新庄家队列
	Success_ApplyBanker_By_Update_Apply = -120026,--刷新申请队列
	Success_CancelBanker_By_Exit_Apply = -120040,--退出申请队列
	Success_CancelBanker_By_Exit_Banker = -120041,--退出庄家队列
	Success_CancelBanker_By_Cur_Game = -120042,--正在游戏中
	Err_CancelBanker_By_No_Exist_Array = -120043,--不存在队列中
	Err_CancelBanker_By_Repeat_Cancel = -120044,--重复申请退庄
	Err_EnterTalbe_By_Over_ReadyCount = -120060,--准备进桌人数已超上限
	Err_EnterTalbe_By_OverCount = -120061,--桌子人数已超上限
	Err_EnterTable_By_Exist = -120062,--玩家已在此桌
	Err_EnterTable_By_ExistOther = -120063,--玩家已在其他桌
	Err_EnterTable_By_AllocChair = -120064,--分配椅子报错
	Err_EnterTable_By_User_Null = -120065,--玩家为空
	Err_EnterTable_By_ExistReady = -120066,--已在准备队列中
	Err_ExitTable_By_EnterGame = -120080,--进入游戏报错
	Err_ExitTable_By_InitUser_InfoNull = -120081,--信息为空
	Err_ExitTable_By_InitUser_TableExist = -120082,--已在桌子中
	Err_ExitTable_By_InitUser_ChairExist = -120083,--已在椅子中

	Err_ExitTable_By_ERROR_GAME = 120084, -- 游戏服异常
	Err_ExitRoom_By_EnterGame = -120100,--进入游戏报错
	Err_ExitRoom_By_InitUser_InfoNull = -120101,--信息为空
	Err_ExitRoom_By_InitUser_TableExist = -120102,--已在桌子中
	Err_ExitRoom_By_InitUser_ChairExist = -120103,--已在椅子中
	Err_ExitRoom_By_Error_GAM = -120104,--游戏服异常
	Err_G2M_Init_User_Req_By_Info_Null = -120120,--信息为空
	Err_G2M_Init_User_Req_By_Table_Exist = -120121,--已在桌子中
	Err_G2M_Init_User_Req_By_Chair_Exist = -120122,--已在椅子中
	Type_LeftScene_By_HallOffLine = -120140,--场景掉线
	Type_LeftScene_By_C2M = -120141,--客户端请求
	Rst_LfetGame_By_GameOffLine_By_TableNull = -120160,--桌子为空  退出消息
	Rst_LfetGame_By_GameOffLine_By_Playing = -120161,--正在游戏中
	Rst_LfetGame_By_GameOffLine_By_Normal = -120162,--正常退出
	Rst_LfetGame_By_C2G_By_UserNull = -120163,--玩家为空
	Rst_LfetGame_By_C2G_By_TableNull = -120164,--桌子为空
	Rst_LfetGame_By_C2G_By_Playing = -120165,--正在游戏中
	Rst_LfetGame_By_C2G_By_Normal = -120166,--正常退出
	Rst_LfetGame_By_Scene_Offline_By_UserNull = -120167,--玩家为空
	Rst_LfetGame_By_Scene_Offline_By_TableNull = -120168,--桌子为空
	Rst_LfetGame_By_Scene_Offline_By_Playing = -120169,--正在游戏中
	Rst_LfetGame_By_Scene_Offline_By_Normal = -120170,--正常退出
	Rst_LfetGame_By_C2M_By_UserNull = -120171,--玩家为空
	Rst_LfetGame_By_C2M_By_TableNull = -120172,--桌子为空
	Rst_LfetGame_By_C2M_By_Playing = -120173,--正在游戏中
	Rst_LfetGame_By_C2M_By_Normal = -120174,--正常退出
	Rst_LfetGame_By_OverTime_Logon = -120175,--超时登陆
	Rst_LfetGame_By_OverLookTimes = -120176,--超过旁观局数
	Rst_LfetGame_By_LackScore = -120177,--积分不足
	Rst_LfetGame_By_KickOut = -120178,--踢人
	Rst_LfetGame_By_Error = -120179,--服务器异常

	Type_Left_Table_By_GameOffLine = -120180,--游戏掉线(离开桌子)
	Type_Left_Table_By_C2G = -120181,--客户端请求游戏(离开桌子)
	Type_Left_Table_By_SceneOffLine = -120182,--场景掉线(离开桌子)
	Type_Left_Table_By_C2M = -120183,--客户端请求场景(离开桌子)
	Type_Left_Table_By_OverLookTimes = -120184,--超过旁观局数(离开桌子)
	Type_Left_Table_By_LackScore = -120185,--积分不足(离开桌子)
	Type_Left_Table_By_Change_Table = -120186,--换桌(离开桌子)
	Rst_G2M_ReEnter_Req_By_TableNull = -120200, -- 桌子为空
	Err_C2G_InitGame_Req_By_EnterStatus = -120220, -- 状态错误
	Err_C2G_InitGame_Req_By_TableNull = -120221, -- 桌子为空
	Err_G2C_SUPPLY = -120240, -- 补充失败
	-- 结束退出申请队列结果值
	Rst_GameEnd_ExitApply_Data_Null = -120260 ,-- 玩家信息为空
	Rst_GameEnd_ExitApply_LACK_SCORE = -120261 ,-- 金币不足上庄
	-- 结束退出庄家队列结果值
	Rst_GameEnd_ExitBanker_Data_Null = -120280 ,-- 玩家信息为空
	Rst_GameEnd_ExitBanker_Lack_Score = -120281 ,-- 金币不足上庄
	Rst_GameEnd_ExitBanker_Over_Times = -120282, -- 超过连庄最大局数
}
HoxDataMgr.ErrorID.tips = 
{
	[HoxDataMgr.ErrorID.Rst_LfetGame_By_GameOffLine_By_TableNull] = "与服务器断开连接" ,
	[HoxDataMgr.ErrorID.Rst_LfetGame_By_GameOffLine_By_Playing] = "与服务器断开连接",
	[HoxDataMgr.ErrorID.Rst_LfetGame_By_GameOffLine_By_Normal] = "与服务器断开连接",
	[HoxDataMgr.ErrorID.Rst_LfetGame_By_C2G_By_UserNull] = "与服务器断开连接",
	[HoxDataMgr.ErrorID.Rst_LfetGame_By_C2G_By_TableNull] = "与服务器断开连接",
	-- [HoxDataMgr.ErrorID.Rst_LfetGame_By_C2G_By_Playing] = "与房间断开连接",
	-- [Rst_LfetGame_By_C2G_By_Normal] = -120166,--正常退出
	[HoxDataMgr.ErrorID.Rst_LfetGame_By_Scene_Offline_By_UserNull] = "与服务器断开连接",
	[HoxDataMgr.ErrorID.Rst_LfetGame_By_Scene_Offline_By_TableNull] = "与服务器断开连接",
	[HoxDataMgr.ErrorID.Rst_LfetGame_By_Scene_Offline_By_Playing] = "与服务器断开连接",
	[HoxDataMgr.ErrorID.Rst_LfetGame_By_Scene_Offline_By_Normal] = "与服务器断开连接",
	[HoxDataMgr.ErrorID.Rst_LfetGame_By_C2M_By_UserNull] = "与服务器断开连接",
	[HoxDataMgr.ErrorID.Rst_LfetGame_By_C2M_By_TableNull] = "与服务器断开连接",
	[HoxDataMgr.ErrorID.Rst_LfetGame_By_C2M_By_Playing] = "与服务器断开连接",
	-- [Rst_LfetGame_By_C2M_By_Normal] = -120174,--正常退出
	[HoxDataMgr.ErrorID.Rst_LfetGame_By_OverTime_Logon] = "与服务器断开连接",--超时登陆
	[HoxDataMgr.ErrorID.Rst_LfetGame_By_OverLookTimes] = "由于您长时间未下注，自动退出游戏",--超过旁观局数
	[HoxDataMgr.ErrorID.Rst_LfetGame_By_LackScore] = "金币不足，您已离开房间",--积分不足
	[HoxDataMgr.ErrorID.Type_Left_Table_By_GameOffLine] = "与服务器断开连接",--游戏掉线(离开桌子)
	[HoxDataMgr.ErrorID.Type_Left_Table_By_C2G] = "与服务器断开连接",--客户端请求游戏(离开桌子)
	[HoxDataMgr.ErrorID.Type_Left_Table_By_SceneOffLine] = "与服务器断开连接",--场景掉线(离开桌子)
	[HoxDataMgr.ErrorID.Type_Left_Table_By_C2M] = "与服务器断开连接",--客户端请求场景(离开桌子)
	[HoxDataMgr.ErrorID.Type_Left_Table_By_OverLookTimes] = "与服务器断开连接",--超过旁观局数(离开桌子)
	-- 退庄提示
	[HoxDataMgr.ErrorID.Rst_GameEnd_ExitApply_Data_Null] = "玩家信息为空",
	[HoxDataMgr.ErrorID.Rst_GameEnd_ExitApply_LACK_SCORE] = "金币不足，您已离开上庄队列",

	[HoxDataMgr.ErrorID.Rst_LfetGame_By_KickOut] = "与服务器断开连接",
	[HoxDataMgr.ErrorID.Rst_LfetGame_By_Error] = "服务器异常",
	-- 结束退出庄家队列结果值
	[HoxDataMgr.ErrorID.Rst_GameEnd_ExitBanker_Data_Null] = "玩家信息为空",
	[HoxDataMgr.ErrorID.Rst_GameEnd_ExitBanker_Lack_Score] = "金币不足系统自动换庄",
	[HoxDataMgr.ErrorID.Rst_GameEnd_ExitBanker_Over_Times] = "连庄局数已达上限，系统自动换庄",
}
function HoxDataMgr.getInstance() 
    if HoxDataMgr.instance == nil then
        HoxDataMgr.instance = HoxDataMgr.new()  
    end
    return HoxDataMgr.instance
end

function HoxDataMgr.releaseInstance() 
    HoxDataMgr.instance = nil
end

function HoxDataMgr:ctor()
    self.m_nModeType = 4 
    self:Clear()
end

function HoxDataMgr:Clear() 
    self.currbankerReuslt = 0
    self._bIsGameScene = false
	self._eGameStatus = 1
	self._ucSelectedJettonIdx = 0
	self._llBankerGold = 0
	self._llBankerLastGold = 0
	self._nCurBankerTimes = 0
	self._strBankerName = ""
	self._nBankerChairId = 65535
	self._nBankerUsrId = 0
	self._nLastBankerChairId = 0
    self.m_lCurLastChipSumNum = 0
    self._nIsMyFly = true
    self._nFlyChipUserChairId = 0
    self._nFlyChipIndex = 0
    self._nJettonIndex = 0
    self._nIsAndroid = false
    self.m_nCurTableRealUserNum = 0
    self.m_llMinBankerScore = 0
    self.m_llMaxBankerScore = 0
    self.m_nTotalBoard = 0
    self.m_selfInfo = 0
	self._vecUsrChip = {}                           --保存当局玩家投注 
	self._vecOpenData = {}                          --开奖数据
	self._vecHistoryList = {}                       --历史记录
	self._vecBankerList = {}                        --庄家列表
    self._queueOtherUserChip = {}                   --其他玩家下注队列(显示用) 
    self._queueOtherUserDelChip = {}                --其他玩家清空下注显示队列(显示用)
    self.m_cbCardShowFlag = {}
    self.m_cbCardType = {}                          --牌型
    self.m_cbMaxCard = {}
    self.m_bPlayerWin = {}                          --闲家输赢
    self.m_arrTableChairID = {}                     --桌子的玩家
    self.m_arrOtherChip = {}                        --其他玩家的下注总额
    self.m_arrBoardCount = {}                       --区域赢局数
    self._vecleaderList = {}
--    self:setMinBankerScore(0)
    self.m_bankList = {}
    self.m_applyList= {}
	for i = 1, 5 do
        self.m_cbCardShowFlag[i] = 0                --牌型显示标记索引(使用一个字节的8个位的低5位标识0:不显示,1:显示)
		self.m_cbCardType[i] = 0
		self.m_cbMaxCard[i] = 0
		if 5 ~= i then
			self.m_bPlayerWin[i] = 0
        end
	end
    self.m_otherNum = 0
    self:clearOtherChip()
    self:clearTableChairID()
    self:clearBoardCount()
     self.m_applyNum =0

    self.m_sRecordId = ""
end
function HoxDataMgr:setBankerArray(info)
    self.m_bankList = info 
end
function HoxDataMgr:getBankerArray()
    return self.m_bankList
end
function HoxDataMgr:setApplyArray(info)
    self.m_applyList = info
end
function HoxDataMgr:getApplyArray()
    return self.m_applyList
end
function HoxDataMgr:setApplyNum(num)
    self.m_applyNum = num
end
function HoxDataMgr:getApplyNum()
    return self.m_applyNum
end
function HoxDataMgr:setIsGameScene(_bIsGameScene)
    self._bIsGameScene = _bIsGameScene
end
function HoxDataMgr:isInBankList()
    for k,v in pairs(self.m_bankList) do
        if v.m_accountId == Player:getAccountID() then
            return true
        end
    end
    for k,v in pairs(self.m_applyList) do
        if v == Player:getAccountID() then
            return true
        end
    end
    return false
end
function HoxDataMgr:getMyBankScore()
    for k,v in pairs(self.m_bankList) do
        if v.m_accountId == Player:getAccountID() then
            return v.m_bankerCoin*0.01
        end
    end
end
function HoxDataMgr:setOnlinePlayers(info)
    self.m_onlineList = info
end
function HoxDataMgr:getOnlinePlayers()
    return self.m_onlineList
end
function HoxDataMgr:getIsGameScene()
    return self._bIsGameScene
end

function HoxDataMgr:setGameStatus(_eGameStatus)
    self._eGameStatus = _eGameStatus
end

function HoxDataMgr:getGameStatus()
    return self._eGameStatus
end

function HoxDataMgr:setSelectedJettonIdx(_ucSelectedJettonIdx)
    self._ucSelectedJettonIdx = _ucSelectedJettonIdx
end

function HoxDataMgr:getSelectedJettonIdx()
    return self._ucSelectedJettonIdx
end

function HoxDataMgr:setBankerLastGold(_llBankerLastGold)
    self._llBankerLastGold = _llBankerLastGold
end

function HoxDataMgr:getBankerLastGold()
    return self._llBankerLastGold
end

function HoxDataMgr:setBankerName(_strBankerName)
    self._strBankerName = _strBankerName
end

function HoxDataMgr:getBankerName()
    return self._strBankerName
end

function HoxDataMgr:setBankerUsrId(_nBankerUsrId)
    self._nBankerUsrId = _nBankerUsrId
end

function HoxDataMgr:getBankerUsrId()
    return self._nBankerUsrId
end

function HoxDataMgr:setBankerChairId(_nBankerChairId)
    self._nBankerChairId = _nBankerChairId
end

function HoxDataMgr:getBankerChairId()
    return self._nBankerChairId or 65535
end

function HoxDataMgr:setLastBankerChairId(_nLastBankerChairId)
    self._nLastBankerChairId = _nLastBankerChairId
end

function HoxDataMgr:getLastBankerChairId()
    return self._nLastBankerChairId
end

function HoxDataMgr:setCurBankerTimes(_nCurBankerTimes)
    self._nCurBankerTimes = _nCurBankerTimes
end

function HoxDataMgr:getCurBankerTimes()
    return self._nCurBankerTimes
end

--保存当前局玩家投注总值,用于清除返还用
function HoxDataMgr:setCurLastChipSumNum(m_lCurLastChipSumNum)
    self.m_lCurLastChipSumNum = m_lCurLastChipSumNum
end

function HoxDataMgr:getCurLastChipSumNum()
    return self.m_lCurLastChipSumNum
end

--是否是自己下注的
function HoxDataMgr:setIsMyFly(_nIsMyFly)
    self._nIsMyFly = _nIsMyFly
end

function HoxDataMgr:getIsMyFly()
    return self._nIsMyFly
end

--要飞的玩家椅子ID
function HoxDataMgr:setFlyChipUserChairId(_nFlyChipUserChairId)
    self._nFlyChipUserChairId = _nFlyChipUserChairId
end

function HoxDataMgr:getFlyChipUserChairId()
    return self._nFlyChipUserChairId
end

--要飞的下注区域
function HoxDataMgr:setFlyChipIndex(_nFlyChipIndex)
    self._nFlyChipIndex = _nFlyChipIndex
end

function HoxDataMgr:getFlyChipIndex()
    return self._nFlyChipIndex
end

--要飞的下注筹码
function HoxDataMgr:setJettonIndex(_nJettonIndex)
    self._nJettonIndex = _nJettonIndex
end

function HoxDataMgr:getJettonIndex()
    return self._nJettonIndex
end
---
--是否是机器人下注的
function HoxDataMgr:setIsAndroid(_nIsAndroid)
    self._nIsAndroid = _nIsAndroid
end

function HoxDataMgr:getIsAndroid()
    return self._nIsAndroid
end

--当前桌子用户数量
function HoxDataMgr:setCurTableRealUserNum(m_nCurTableRealUserNum)
    self.m_nCurTableRealUserNum = m_nCurTableRealUserNum
end

function HoxDataMgr:getCurTableRealUserNum()
    return self.m_nCurTableRealUserNum
end

--今日总局数
function HoxDataMgr:setTotalBoard(m_nTotalBoard)
    self.m_nTotalBoard = m_nTotalBoard
end

function HoxDataMgr:getTotalBoard()
    return self.m_nTotalBoard
end

--上庄限制
function HoxDataMgr:setMinBankerScore(m_llMinBankerScore)
    self.m_llMinBankerScore = m_llMinBankerScore
end

function HoxDataMgr:getMinBankerScore()
    return self.m_llMinBankerScore
end
function HoxDataMgr:setMaxBankerScore(m_llMaxBankerScore)
    self.m_llMaxBankerScore = m_llMaxBankerScore
end
function HoxDataMgr:getMaxBankerScore()
    return self.m_llMaxBankerScore
end
function HoxDataMgr:setGameSelfScore(info)
    self.m_selfInfo = info
end
function HoxDataMgr:getGameSelfScore()
    return self.m_selfInfo
end
function HoxDataMgr:setOtherNum(num)
    self.m_otherNum = num
end

function HoxDataMgr:getOtherNum()
    return self.m_otherNum
end
function HoxDataMgr:clearOtherChip()
    for i = 1, DOWN_COUNT_HOX do
        self.m_arrOtherChip[i] = 0
    end
end

function HoxDataMgr:setBankerGold(llBankerGold)
	self._llBankerLastGold = self._llBankerGold
	self._llBankerGold = llBankerGold
end

function HoxDataMgr:getBankerGold()
	return self._llBankerGold
end

--将投注加入当前记录
function HoxDataMgr:addUsrChip(chip)
	table.insert(self._vecUsrChip, chip)
end
function HoxDataMgr:getAllUsrChip()
    local total =0
    local tab ={1,10,50,100,500}
	for k,v in pairs(self._vecUsrChip) do 
        total = total+tab[v.wJettonIndex]
    end
    return total
end

--通过索引获取投注信息
function HoxDataMgr:getUsrChipByIdx(index)
    if not index or index > #self._vecUsrChip then return end
--	assert(index < #self._vecUsrChip)
	return self._vecUsrChip[index]
end

--获取投注数量
function HoxDataMgr:getUsrChipCount()
	return #self._vecUsrChip
end

--清除投注内容--清空按钮
function HoxDataMgr:clearUsrChip()
	self._vecUsrChip = {}
end

--将续投加入当前记录
function HoxDataMgr:recordBetContinue()
    for k,v in pairs(self._vecUsrChipContinue) do
		self:addUsrChip(v) 
    end
end

--更新续投内容
function HoxDataMgr:updateBetContinueRec()
	if #self._vecUsrChip > 0 then
		self._vecUsrChipContinue = self._vecUsrChip
		self._vecUsrChip = {}
	end
    self:clearOtherChip()
end

function HoxDataMgr:getUsrContinueChipByIdx(index)
    if not index or index > #self._vecUsrChipContinue then return end
	return self._vecUsrChipContinue[index]
end

function HoxDataMgr:getUsrContinueChipCount()
	return #self._vecUsrChipContinue
end

--[[
 *  获取自己在某一个区域下注的总额
 *  @param index 区域index
 *  @return 返回的钱
 --]]
function HoxDataMgr:getUserAllChip(index)
    local nAllChip = 0
    for k,v in pairs(self._vecUsrChip) do
        if v.wChipIndex == index then
            local value = NiuNiuCBetManager.getInstance():getJettonScore(v.wJettonIndex)
            nAllChip = nAllChip + value
        end
    end
    return nAllChip
end

--其他玩家的数据
function HoxDataMgr:addOtherChip( index, llValue)
    if index <= DOWN_COUNT_HOX then
        self.m_arrOtherChip[index] = self.m_arrOtherChip[index] or 0
        self.m_arrOtherChip[index] = self.m_arrOtherChip[index] + llValue
    end
end

function HoxDataMgr:delOtherChip( index, llValue)
    if index <= DOWN_COUNT_HOX then
        self.m_arrOtherChip[index] = self.m_arrOtherChip[index] - llValue
        self.m_arrOtherChip[index] = (self.m_arrOtherChip[index] < 0) and 0 or self.m_arrOtherChip[index]
    end
end

function HoxDataMgr:getOtherChip( index)
    if index > DOWN_COUNT_HOX then
        return 0
    end
    return self.m_arrOtherChip[index]
end

function HoxDataMgr:getAllChip()
    local llAllChip = 0
    for k, v in pairs(self.m_arrOtherChip) do
        llAllChip = llAllChip + v
    end
    return llAllChip
end

function HoxDataMgr:getOtherAllChip()
    local llValue = 0
    for i = 1, DOWN_COUNT_HOX do
        llValue = llValue + self:getOtherChip(i)
    end
    return llValue
end

--获取可下注金额
function HoxDataMgr:getRemaingChip()
    local llValue = 0
    for i = 1, DOWN_COUNT_HOX do
        llValue = llValue + self:getUserAllChip(i) + self:getOtherChip(i)
    end
    local llRemainingValue = (self._llBankerGold / self:getModeType()) - llValue
    return (llRemainingValue > 0) and llRemainingValue or 0
end

function HoxDataMgr:clearOtherChip()
    for i = 1, DOWN_COUNT_HOX do
        self.m_arrOtherChip[i] = 0
    end
end

function HoxDataMgr:sortShowCard()
    --排序
    self._vecOpenData[1].bMoveMarkIndex = {}
    local result = self._vecOpenData[1]
    for i = 1, 5 do 
        local sortData = result.cbSendCardData[i]  

        local cardData = {}
        local markIndex = 0
        for j = 1, 5 do
            local nCardNum = sortData[j]
            if not nCardNum or nCardNum < 0 or nCardNum > 79 then break end
            local bitNum = math.pow(2,j-1)
            local result = bit.band(bitNum,self.m_cbCardShowFlag[i])
            if (result ~= 0) then
                markIndex = markIndex + 1
                table.insert(cardData, 1, nCardNum)
            else
                table.insert(cardData, nCardNum)
            end
        end
        self._vecOpenData[1].bMoveMarkIndex[i] = markIndex
        self._vecOpenData[1].cbSendCardData[i] = cardData
    end
end

--开奖数据
function HoxDataMgr:addOpenData(tmp)
	self._vecOpenData = {}
    self._vecleaderList = tmp.cbRankList
    table.insert(self._vecOpenData, tmp) 
end

function HoxDataMgr:getRankData() 
    return self._vecleaderList
end

function HoxDataMgr:getOpenData() 
    --todo
	if #self._vecOpenData ~= 0 then
		return self._vecOpenData[1]
	end
    local tab = {}
	return tab
end

--历史记录
function HoxDataMgr:addHistoryToList( stgHis)
	if #self._vecHistoryList > MAX_HITORY_COUNT then
--        for i = 1, MAX_HITORY_COUNT do
--            if self._vecHistoryList[i+1] then
--                self._vecHistoryList[i] = self._vecHistoryList[i+1] 
--            end
--        end
        table.remove(self._vecHistoryList)
    end
    table.insert(self._vecHistoryList, 1, stgHis)
end

function HoxDataMgr:getHistoryByIdx( nIdx)
	return self._vecHistoryList[#self._vecHistoryList + 1 - nIdx]
end

function HoxDataMgr:getHistoryListSize()
	return #self._vecHistoryList
end

function HoxDataMgr:clearHistory()
    self._vecHistoryList = {}
	return self._vecHistoryList
end

function HoxDataMgr:clearBoardCount() 
    for i = 1, DOWN_COUNT_HOX do
        self.m_arrBoardCount[i] = 0
    end
end

--区域赢局数
function HoxDataMgr:getWinBoardCount( index)
    if index <= DOWN_COUNT_HOX then
        return self.m_arrBoardCount[index]
    end
    return 0
end

function HoxDataMgr:setWinBoardCount( index, count)
    if index <= DOWN_COUNT_HOX then
        self.m_arrBoardCount[index] = count
    end
end

--区域输局数
function HoxDataMgr:getLoseBoardCount( index)
    if index <= DOWN_COUNT_HOX then
        local nLoseCount = self.m_nTotalBoard - self.m_arrBoardCount[index]
        return (nLoseCount < 0) and 0 or nLoseCount
    end
    return 0
end 
--自加一次局数
function HoxDataMgr:addBoardCount()
    self.m_nTotalBoard = self.m_nTotalBoard + 1
    local stgHis = {}
    stgHis.bWin = {}
    for i = 1, DOWN_COUNT_HOX do
        if self._vecOpenData[1].bPlayerWin[i]>0 then
            self.m_arrBoardCount[i] = self.m_arrBoardCount[i] + 1
        end
        stgHis.bWin[i] = self._vecOpenData[1].bPlayerWin[i]>0
    end
    self:addHistoryToList(stgHis)
end

--庄家列表
function HoxDataMgr:addBankerListByChairId( wChairId, wUsrId, strNickName)
	for k,v in pairs(self._vecBankerList) do
		if v.wChairID == wChairId then
			return
        end
    end
	local tmp = {}
	tmp.dwUserID = wUsrId
	tmp.wChairID = wChairId
	tmp.strUserNickName = strNickName
    table.insert(self._vecBankerList, tmp)
end

function HoxDataMgr:getBankerInfoByIndex( nIdx)
	return self._vecBankerList[nIdx]
end

function HoxDataMgr:delBankerByUsrId( nUsrId)
    --todo
	for i =1, #self._vecBankerList.size() do
		if self._vecBankerList[i] and self._vecBankerList[i].dwUserID == nUsrId then
            table.remove(self._vecBankerList,i)
		end
	end
end

function HoxDataMgr:delBankerByChairId( wChairId)
    --todo
	for i = 1, #self._vecBankerList do
		if self._vecBankerList[i] and self._vecBankerList[i].wChairID == wChairId then
            table.remove(self._vecBankerList, i)
		end
	end
end

function HoxDataMgr:getBankerListSize()
	return #self._vecBankerList
end

function HoxDataMgr:clearBankerList()
	self._vecBankerList = {}
end

--在返回index，不在返回－1
function HoxDataMgr:isInBnakerList()
    local nIndex = 0
    for k,v in pairs(self._vecBankerList) do
        if v.wChairID == Player:getAccountID() then
            return nIndex
        else
            nIndex = nIndex + 1
        end
    end
    return -1
end

function HoxDataMgr:isBanker()
    for k,v in pairs(self.m_bankList) do
        if v.m_accountId == Player:getAccountID() then
            return true
        end
    end
    return false
end

--比牌
function HoxDataMgr:ComparePlayerCard()
	local openData = self._vecOpenData[1]
	for i = 1, 5 do
        self.m_cbCardShowFlag[i] = 0
		self.m_cbCardType[i] = self:GetCardTypeByIndex(openData.cbSendCardData[i],i)
		self.m_cbMaxCard[i] = self:GetMaxCard(openData.cbSendCardData[i])
	end
    
    self.m_bPlayerWin[1] = openData.bPlayerWin[1]
	self.m_bPlayerWin[2] = openData.bPlayerWin[2]
	self.m_bPlayerWin[3] = openData.bPlayerWin[3]
	self.m_bPlayerWin[4] = openData.bPlayerWin[4]
end
function HoxDataMgr:setCardType(cardType)
    self.m_cbCardType = cardType 
end
--获取牛牛类型
function HoxDataMgr:GetCardTypeByIndex(cbCardData, index)
	--特殊牛型
    --todo
    if self:IsMinNiu(cbCardData) then
        return CardValue_Small
	elseif self:IsBomb(cbCardData) then
		return CardValue_Bomb
	elseif self:IsJinNiu(cbCardData,index) then
		return CardValue_Jin
	elseif self:IsYinNiu(cbCardData,index) then
		return CardValue_Yin
    end
	--普通牛型
	local kCount = 0
	for i = 1, 5 do
		if cbCardData[i] > 64 then
			kCount = kCount + 1
        end
    end
	if kCount > 0 then
		return self:GetKingType(cbCardData, kCount,index)
	else
		return self:GetCardArrType(cbCardData,index)
    end
	return CardValue_Nothing
end

--是否金牛
function HoxDataMgr:IsJinNiu( cbCardData, index)
	for i = 1, 5 do
		if cbCardData[i] <= 64 then
            if (cbCardData[i] % 16) <= 10 then
			    return false
            end
        end
	end
    self.m_cbCardShowFlag[index] = math.pow(2, 0) + math.pow(2, 1) + math.pow(2, 2)
	return true
end

--是否银牛
function HoxDataMgr:IsYinNiu( cbCardData, index)
    local tagIndex = 0
    local nTenCount = 0
	for i = 1, 5 do
		if cbCardData[i] <= 64 then
		    if (cbCardData[i] % 16) < 10 then
			    return false
            elseif (cbCardData[i] % 16) == 9 then
                nTenCount = nTenCount + 1
                tagIndex = i
            end
        end
	end

    if nTenCount > 1 then return false end
    nTenCount = 0
    for i = 1, 5 do
        if (tagIndex ~= i) and (nTenCount < 4) then
            self.m_cbCardShowFlag[index] = self.m_cbCardShowFlag[index] + math.pow(2, i-1)
            nTenCount = nTenCount + 1
        end
    end
	return true
end

--是否炸弹
function HoxDataMgr:IsBomb( cbCardData)
	local cbCard = {}
    local len = 13
    for i = 1,len do
        cbCard[i] = 0
    end
	local kingCount = 0
	for i = 1, 5 do
		if (cbCardData[i] > 64) then
			kingCount = kingCount + 1
		else
            local index = cbCardData[i] % 16 + 1
            local tmp = cbCard[index]
			cbCard[index] = tmp + 1
		end
	end
	for i = 1, 13 do
		if (cbCard[i] + kingCount) >= 4 then
			return true
        end
	end
	return false
end

--是否五小牛
function HoxDataMgr:IsMinNiu( cbCardData)
    local nTotalPoint = 0
    for i = 1, 5 do
        if (cbCardData[i] > 64) then
			nTotalPoint = nTotalPoint + 1
		else
			if (self:GetLogicValue(cbCardData[i]) >= 5) then
                return false
            end
            nTotalPoint = nTotalPoint + self:GetLogicValue(cbCardData[i])+ 1
		end
    end
    if nTotalPoint <= 10 then
        return true
    end
    return false
end

--获取炸弹基数
function HoxDataMgr:getBombValue( cbCardData)
	local cbCard = {}
    local len = 13
    for i = 1,len do
        cbCard[i] = 0
    end
    local kingCount = 0
    for i = 1, 5 do
        if cbCardData[i] > 64 then
            kingCount = kingCount + 1
        else
            local tmp = cbCard[cbCardData[i] % 16] or 0
            cbCard[cbCardData[i] % 16] = tmp + 1
        end
    end
    for i = 1, 13 do
        if(cbCard[i] + kingCount) >= 4 then
            return i
        end
    end
    return -1
end

--获取百搭类型
function HoxDataMgr:GetKingType( cbCardData, kCount, index)
	local temp = 0
	local num = 1
	local card = {} --5
	if kCount == 1 then
		for i = 1, 5 do
			local tCount = 1
			if cbCardData[i] <= 64 then
			    for j = i + 1, 5 do
				    if cbCardData[j] <= 64 then
				        for k = j + 1, 5 do
				        --找三组合能否组合成牛
					        if cbCardData[k] <= 64 then
					            if (self:GetCardValue(cbCardData[i]) +
                                    self:GetCardValue(cbCardData[j]) +
                                    self:GetCardValue(cbCardData[k])) % 10 == 0 then

						            tCount = 0
						            num = 1
						            for n = 1, 5 do
							            if n ~= i and n ~= j and n ~= k then
                                            card[num] = cbCardData[n]
                                            num = num + 1
                                        end
						            end
						            card[num] = cbCardData[i]
                                    num = num + 1
						            card[num] = cbCardData[k]
                                    num = num + 1
                                    card[num] = cbCardData[j]
                                    num = num + 1
                                end
					        end
                        end
                        if tCount == 0 then
					        temp = 0
					        break
				        end
                        --找两张最大组合
				        tCount = (self:GetCardValue(cbCardData[i]) + self:GetCardValue(cbCardData[j])) % 10
				        if temp < tCount or tCount == 0 then
					        temp = tCount
					        num = 1
					        card[num] = cbCardData[i]
                            num = num + 1
					        card[num] = cbCardData[j]
                            num = num + 1
					        for t = 1, 5 do
						        if t ~= i and t ~= j then
						            card[num] = cbCardData[t]
                                    num = num + 1
                                end
					        end
					        if tCount == 0 then
                                break
                            end
				        end
				    end
                end
                if tCount == 0 then 
                    break
                end
            end
        end
        for i = 3, 5 do
            local temp = card[i]
            for j = 1, 5 do
                if cbCardData[j] == temp then
                    local tmp = self.m_cbCardShowFlag[index]
                    self.m_cbCardShowFlag[index] = tmp + math.pow(2, j-1)
                end
            end
        end
	    return self:GetCardType(temp)
	else
    --两张王绝对牛牛
		num = 1
		card[num] = 52
        num = num + 1
        for i = 1, 5 do
            if cbCardData[i] <= 64 then
                card[num] = cbCardData[i]
                num = num + 1
            end
        end
        card[num] = 53
        num = num + 1
        for i = 1, 3 do
            local temp = card[i]
            for j = 1, 5 do
                if cbCardData[j] == temp then
                    local tmp = self.m_cbCardShowFlag[index]
                    self.m_cbCardShowFlag[index] = tmp + math.pow(2, j-1)
                end
            end
        end
		return CardValue_Ten
	end
	return CardValue_Nothing
end

--获取普通类型
function HoxDataMgr:GetCardArrType( cbCardData, index)
	for i =1, 5 do
		for j = i + 1, 5 do
			for k = j + 1, 5 do
				if ((self:GetCardValue(cbCardData[i]) + self:GetCardValue(cbCardData[j]) + self:GetCardValue(cbCardData[k])) % 10 == 0) then
					local temp = 0
					local num = 1
					local card = {}
					for t = 1, 5 do
						if t ~= i and t ~= j and t ~= k then
						    card[num] = cbCardData[t]
					        num = num + 1
						    temp = temp + self:GetCardValue(cbCardData[t])
                        end
					end
					for t = 1, 5 do
						if t ~= i and t ~= j and t ~= k then
							card[num] = cbCardData[t]
                            num = num + 1
                        end
					end
                    self.m_cbCardShowFlag[index] = math.pow(2, i-1) + math.pow(2, j-1) + math.pow(2, k-1)
					return self:GetCardType(temp % 10)
				end
			end
		end
	end
	return CardValue_Nothing
end

--获取牌点数
function HoxDataMgr:GetCardValue( cbCardData)
	return ((cbCardData % 16) >= 9) and 10 or (cbCardData % 16 + 1)
end

function HoxDataMgr:GetLogicValue( cbCardData)		
--获取逻辑点数
    local cardData = 0
    if cbCardData == 52 then
        cardData = 16
    elseif cbCardData == 53 then
        cardData = 14
    else
        cardData = cbCardData % 16
    end
    return cardData
end

function HoxDataMgr:GetMaxColorKing( cbCardData, cbColor)
	local cbTempMax = 0
	local cbTempColor = 0
	for i = 1, 5 do
		if (cbCardData[i] % 16) > cbTempMax then
			cbTempMax = cbCardData[i] % 16
			cbTempColor = cbCardData[i] / 16
		end
		if cbCardData[i] > 64 then
			return cbCardData[i]
        end
	end
	cbColor = cbTempColor
	return cbTempMax
end

function HoxDataMgr:GetCardType( cbCount)
	if 0 == cbCount then
        return CardValue_Ten
    end
	return cbCount % 10
end

function HoxDataMgr:GetMaxCard( cbCardData)
	local cbMaxCard = cbCardData[1]
	for i = 1, 4 do
		if ((cbMaxCard % 16) < (cbCardData[i + 1] % 16)) then
			cbMaxCard = cbCardData[i + 1]
		elseif ((cbMaxCard % 16) == (cbCardData[i + 1] % 16)) then
			if ((cbMaxCard / 16) < (cbCardData[i + 1] / 16)) then
				cbMaxCard = cbCardData[i + 1]
			end
		end
	end
	return cbMaxCard
end

--function HoxDataMgr:GetJettonValueByIdx(ucIdx)
--    local value = 0
--	if (1 == ucIdx) then 
--        value = 10
--    elseif (2 == ucIdx) then 
--        value = 100
--    elseif (3 == ucIdx) then 
--        value = 500
--    elseif (4 == ucIdx) then 
--        value = 1000
--    elseif (5 == ucIdx) then 
--        value = 5000
--    elseif (6 == ucIdx) then 
--        value = 10000
--    end
--	return value
--end

--function HoxDataMgr:GetJettonIndexByValue( llValue)
--    local value = 0
--    if 10 == llValue then
--        value = 1
--    elseif 100 == llValue then
--        value = 2
--    elseif 500 == llValue then
--        value = 3
--    elseif 1000 == llValue then
--        value = 4
--    elseif 5000 == llValue then
--        value = 5
--    elseif 10000 == llValue then
--        value = 6
--    end
--end

--function HoxDataMgr:GetJettonDimIndexByValue( llValue)
--    local index = 1
--    if 10000 <= llValue then
--        index = 6
--    elseif 5000 <= llValue then
--        index = 5        
--    elseif 1000 <= llValue then
--        index = 4
--    elseif 500 <= llValue then
--        index = 3
--    elseif 100 <= llValue then
--        index = 2
--    elseif 10 <= llValue then
--        index = 1
--    end
--	return index
--end

function HoxDataMgr:GetJettonMaxIdx( llValue)
    if 0 == llValue then
        llValue = PlayerInfo.getInstance():getUserScore()
    end    
    for i = JETTON_ITEM_COUNT, 1, -1 do
        if llValue >= NiuNiuCBetManager.getInstance():getJettonScore(i) then
            return i
        end
    end
    return 1
end

--获取当前局投注总数
function HoxDataMgr:GetCurUserChipSumNum()
    local sumNum = 0
    for k,v in pairs(self._vecUsrChip) do
        if v then
            sumNum = sumNum + NiuNiuCBetManager.getInstance():getJettonScore(v.wJettonIndex)
        end
    end
    self.m_lCurLastChipSumNum = sumNum
end

--获取数组对应的其他玩家索引
function HoxDataMgr:GetArrTableIndexByChairId( iChairId)
    local index = 1
    for i = 1, 8 do--TABLE_USER_COUNT do
        if self.m_arrTableChairID[i] == iChairId then
            index = i
            break
        end
    end
    return index
end

function HoxDataMgr:cleanQueueOtherContinueChipCount()
    self._queueOtherUserContinueChip = {}
end


function HoxDataMgr:getQueueOtherContinueChipCount()
    return #self._queueOtherUserContinueChip
end

function HoxDataMgr:getQueueOtherDelChipCount()
    local chipCount = #self._queueOtherUserDelChip
    return chipCount
end

function HoxDataMgr:getQueueOtherDelChip(_index)
    if not _index then return end
    return self._queueOtherUserDelChip[_index]
end

--通过筹码总值计算筹码索引
function HoxDataMgr:splitTotalValToVec( iTotalVal)
    local vec = {}
    
    while iTotalVal > 0 do
        local index = HoxDataMgr:getInstance():GetJettonMaxIdx(iTotalVal)
        if index >= 1 then
            iTotalVal = iTotalVal - NiuNiuCBetManager.getInstance():getJettonScore(index)
        end
        table.insert(vec,index)
    end
    return vec
end

function HoxDataMgr:getTableChairID()
    return self.m_arrTableChairID
end

function HoxDataMgr:setTableChairIDByIndex(_index,_chairid)
    if not _index or not _chairid then return end
    self.m_arrTableChairID[_index] = _chairid
end

function HoxDataMgr:clearTableChairID()
    for i = 1, 8 do--TABLE_USER_COUNT
        self.m_arrTableChairID[i] = INVALID_CHAIR
    end
end

function HoxDataMgr:updateSelfData(_data)
    for i = 1,8 do
        if PlayerInfo.getInstance():getUserID() == _data.dwUserID and PlayerInfo.getInstance():getUserID() == self.m_arrTableChairID[i] then
            self.m_arrTableChairID[i] = _data.UserStatus.wChairID
        end
    end
end

function HoxDataMgr:pushOtherUserChip(_data)
    if not _data then return end
    table.insert(self._queueOtherUserChip,_data) 
end
function HoxDataMgr:getOtherUserChipNum()
   return #self._queueOtherUserChip
end
function HoxDataMgr:getOtherUserChip(_index)
    if not _index then return end
    return self._queueOtherUserChip[_index]
end

function HoxDataMgr:clearOtherUserChip()
    self._queueOtherUserChip = {}
end

function HoxDataMgr:clearOtherDelChip()
    self._queueOtherUserDelChip = {}
end

function HoxDataMgr:setModeType(num)
    self.m_nModeType = num or 4
end

function HoxDataMgr:getModeType()
    return self.m_nModeType
end

function HoxDataMgr:setBankerCurrResult(bankerReuslt)
    self.currbankerReuslt = bankerReuslt or 0
end

function HoxDataMgr:getBankerCurrResult()
    return self.currbankerReuslt
end

function HoxDataMgr:setRecordId(id)
    self.m_sRecordId = id or ""
end

function HoxDataMgr:getRecordId()
    return self.m_sRecordId
end

return HoxDataMgr