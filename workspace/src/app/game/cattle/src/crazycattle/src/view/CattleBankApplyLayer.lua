--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--
-- 兑换
--  
local Handredcattle_Events          = import("..scene.HandredcattleEvent")
local HNLayer= require("src.app.newHall.HNLayer")
local editTxt = nil
local CattleBankApplyLayer = class("CattleBankApplyLayer", function ()
    return HNLayer.new()
end)
 

function CattleBankApplyLayer:ctor()
    print("CattleBankApplyLayer:ctor")
  --  dump( __params )
   -- self.params = __params 
        self:setNodeEventEnabled(true)
    addMsgCallBack(self, Handredcattle_Events.MSG_UPDATE_APPLYARRAY_NTY, handler(self, self.onUpdateBank ))
    addMsgCallBack(self,  Handredcattle_Events.MSG_CLOSE, handler(self, self.close )) 
    self:myInit()
    self:setupViews()
end
function CattleBankApplyLayer:onUpdateBank()
     self.playerCount:setString(HoxDataMgr.getInstance():getBankerCount())
     local cur = tonumber(self.text_perentLabel:getString())
     self.fenhong:setString(math.floor(cur/((HoxDataMgr.getInstance():getBankerGold()/100)+cur)*100).."%")
end
function CattleBankApplyLayer:onEnter()
    print("*******CattleBankApplyLayer:onEnter()********")
end

function CattleBankApplyLayer:myInit()
    self.root = nil
    self._imageBG = nil
    self.goldNumLabel= nil
    self.tfGoldNum = nil
    self.goldSlider_1 = nil 
end 
-- 初始化界面
function CattleBankApplyLayer:setupViews()
    self.root = UIAdapter:createNode("cow_game_csb/BankApplyLayer.csb")
    
    self:addChild(self.root) 
   
    UIAdapter:adapter(self.root, handler(self, self.onTouchCallback))
 
     local center =self.root:getChildByName("center"); 
    self._imageBG =center:getChildByName("image_Bg"); 
     
     local diffY = (display.size.height - 750) / 2
    self.root:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
      local panel_contentPanel = self._imageBG:getChildByName("panel_contentPanel")
  self.image_openInput = panel_contentPanel:getChildByName("image_openInput")
    -- 金币数量
    self.goldNumLabel = self._imageBG:getChildByName("bmfont_myMoneyLabel")
    self.goldNumLabel:setString( string.format("%0.2f",HoxDataMgr.getInstance():getGameSelfScore().m_score-HoxDataMgr.getInstance():getMinBankerScore())  )
    -- 上庄人数
    self.playerCount = self._imageBG:getChildByName("bmfont_playerCount")
    self.playerCount:setString(HoxDataMgr.getInstance():getBankerCount())
    --分红比例
    self.fenhong = self._imageBG:getChildByName("bmfont_fenhong")
    self.fenhong:setString(math.floor(HoxDataMgr.getInstance():getMinBankerScore()/(HoxDataMgr.getInstance():getBankerGold()+HoxDataMgr.getInstance():getMinBankerScore())*100).."%")
    local panel_contentPanel = self._imageBG:getChildByName("panel_contentPanel")
    
    local TextField_name = self._imageBG:getChildByName("TextField_name")
    TextField_name:setString(Player:getNickName())
    self.tfGoldNum =   self.image_openInput:getChildByName("TextField_number") 
    self.tfGoldNum:setVisible(false) 
    -- 滑动条
    self.goldSlider_1 = panel_contentPanel:getChildByName("slider_moneySlider")
     self.goldSlider_1:setPercent(0)
    self.goldSlider_1:addEventListener(handler(self,self.onTouchSliderCallBack))
    self.text_perentLabel =panel_contentPanel:getChildByName("text_perentLabel")
    self.text_perentLabel:setString(HoxDataMgr.getInstance():getMinBankerScore())
  
    -- 注册析构函数
    ToolKit:registDistructor( self, handler(self, self.onDestory) )
 
end 
 
-- 点击事件回调
function CattleBankApplyLayer:onTouchCallback( sender )
    local name = sender:getName()
    print("name=--",name)
     g_GameMusicUtil:playSound(GAME_SOUND_BUTTON); 
    if name == "button_maxBtn" then       -- 最大 
     
        local score = HoxDataMgr.getInstance():getMinBankerScore() 
        local split =  math.floor(HoxDataMgr.getInstance():getGameSelfScore().m_score/score)
        local goldNum=split*score
      
      self.goldSlider_1:setPercent( 100 ) 
      if goldNum>=HoxDataMgr.getInstance():getMaxBankerScore() then
            goldNum=HoxDataMgr.getInstance():getMaxBankerScore()
        end 
      self.text_perentLabel:setString(goldNum)
       self.goldNumLabel:setString(string.format("%0.2f",HoxDataMgr.getInstance():getGameSelfScore().m_score-goldNum) )
       
       self.fenhong:setString(math.floor(goldNum/((HoxDataMgr.getInstance():getBankerGold()/100)+goldNum)*100).."%")
    elseif name == "button_closeBtn" then     -- 关闭
      self:close()  
    elseif name == "button_changeBtn" then 
       self:sendReqZhuang(tonumber(self.text_perentLabel:getString())*100)
    end
end

function CattleBankApplyLayer:sendMSGshuangzhuang(score)  
    sendMsg(Handredcattle_Events.MSG_COW_SHANGZHUANG_REQ,{score}) --请求上庄
end
function CattleBankApplyLayer:sendReqZhuang(score)
    if HoxDataMgr.getInstance():canApplyBank() then
        self:sendMSGshuangzhuang(score)
        self.m_bAlwaysApply = true
    else
        self.m_bAlwaysApply = false
        local CowGameTipsNode = require("src.app.game.cattle.src.common.cattleDlgTip.CattleGameTipsNode")
        local score = HoxDataMgr.getInstance():getMinBankerScore()
        CowGameTipsNode.new( "本房间上庄需要" .. score.. "金币，您金币不足" )
    end 
end
function CattleBankApplyLayer:onTouchSliderCallBack( sender ,eventType )
    print("CattleBankApplyLayer:onTouchSliderCallBac")
    print("eventType=", eventType)
    local score = HoxDataMgr.getInstance():getMinBankerScore()
    if eventType == 0 then
        local money =HoxDataMgr.getInstance():getGameSelfScore().m_score
        if money>=HoxDataMgr.getInstance():getMaxBankerScore() then
            money=HoxDataMgr.getInstance():getMaxBankerScore()
        end
        local split =  math.floor(money/score)
        local per = (sender:getPercent()/100)
        for i =1 ,split do 
            if ((i-1)/split)<=per and per<=(i/split) then 
                local cur = i*score
                print(cur)
                 print(per)
                self.text_perentLabel:setString(cur) 
                self.goldNumLabel:setString(string.format("%0.2f",HoxDataMgr.getInstance():getGameSelfScore().m_score-cur))

                local bank = HoxDataMgr.getInstance():getBankerGold()
                self.fenhong:setString(math.floor(cur/((HoxDataMgr.getInstance():getBankerGold()/100)+cur)*100).."%")
                return
            end
        end 
    end
end
  
  
function CattleBankApplyLayer:onDestory()
    print("*******CattleBankApplyLayer:onDestory()*******")
    removeMsgCallBack(self,  Handredcattle_Events.MSG_UPDATE_APPLYARRAY_NTY ) 
    removeMsgCallBack(self,  Handredcattle_Events.MSG_CLOSE ) 
end

 
return CattleBankApplyLayer

