--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 设置层
-- CowSetLayer

local CowSound = require("src.app.game.cattle.src.common.CowSound.CowSound")
local CowDialogBase = require("src.app.game.cattle.src.common.cattleDlgTip.CattleDialogBase")

local CowSetLayer = class("CowSetLayer", function()
    return CowDialogBase.new()
end)

function CowSetLayer:ctor()
	self:myInit()
	self:setupViews()    
end

function CowSetLayer:myInit()
	self.m_root = nil
	self.m_layout_bg = nil
	self.m_txt_title = nil
	self.m_checkbox_music = nil
	self.m_checkbox_sound = nil
	self.dlgEnableTouch = false
end

function CowSetLayer:setupViews()
    self.m_root = UIAdapter:createNode("cow_game_csb/cow_game_set.csb")
    self:addChild(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    self.m_layout_bg = self.m_root:getChildByName("layout_bg") -- @5

    self.music_type = CowSound:getInstance():getGameMusicStatus()
    self.sound_type = CowSound:getInstance():getGameSoundStatus()

    

    if self.music_type == false then
        self.music_type = ccui.CheckBoxEventType.unselected
    else
        self.music_type = ccui.CheckBoxEventType.selected
    end

    if self.sound_type == false then
        self.sound_type = ccui.CheckBoxEventType.unselected
    else
        self.sound_type = ccui.CheckBoxEventType.selected
    end

    self:updateViews()
end

function CowSetLayer:updateViews()
	self:updateBtnViews("btn_music", "img_music", self.music_type)
    self:updateBtnViews("btn_sound", "img_sound", self.sound_type)
end

function CowSetLayer:updateBtnViews(btn, bg, type)
    
	local _btn = self.m_root:getChildByName(btn)
    local _bg = self.m_root:getChildByName(bg)
    local children = _btn:getChildren()
    local _open = children[1];
    local _close = children[2];

    --------------------------

    local bg_name = "settings_di_1.png";
    _open:setVisible(type == ccui.CheckBoxEventType.selected);
    _close:setVisible(type == ccui.CheckBoxEventType.unselected);
    local _x = 40
    if type == ccui.CheckBoxEventType.selected then
        _x = -40
        bg_name = "settings_di_2.png";
    end
    _btn:loadTextures(bg_name, bg_name, bg_name, 1)
    _bg:setPositionX(_btn:getPositionX() + _x);

end

----------- 获取变化后开关的状态 -------------
function CowSetLayer:getExBtnType(_type)
    if _type == ccui.CheckBoxEventType.selected then
        return ccui.CheckBoxEventType.unselected
    end
    return ccui.CheckBoxEventType.selected
end

-- 点击事件回调
function CowSetLayer:onTouchCallback( sender )
    local name = sender:getName()
    print("name = ",name)
    if name == "btn_music" then
    	CowSound:getInstance():playEffect("OxButton")
    	self.music_type = self:getExBtnType(self.music_type)
    	CowSound:getInstance():setGameMusicStatus(self.music_type)
    elseif name == "btn_sound" then
    	CowSound:getInstance():playEffect("OxButton")
    	self.sound_type = self:getExBtnType(self.sound_type)
    	CowSound:getInstance():setGameSoundStatus(self.sound_type)
	elseif name == "btn_close" then
		CowSound:getInstance():playEffect("OxButton")
		self:getParent():showSetLayer(false)
    end

    self:updateViews()
end

function CowSetLayer:onExit()
   print("CowSetLayer:onExit")
end

return CowSetLayer