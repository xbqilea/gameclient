--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- CowPokerLayer
-- 玩家扑克牌界面，包括闲家+庄家的牌
require("src.app.game.cattle.src.crazycattle.src.def.CattleDef")

local CowXianJiaPokerNode = require("src.app.game.cattle.src.crazycattle.src.view.CattleXianJiaPokerNode")
local CowPlayerInfo_Tip_Dlg = require("app.game.cattle.src.roompublic.src.common.CattlePlayerInfo_Tip_Dlg")
local CowClipperRoundHead = require("src.app.game.cattle.src.roompublic.src.common.CattleClipperRoundHead")
local CattleBankApplyLayer = import(".CattleBankApplyLayer")
local CowPokerLayer = class("CowPokerLayer", function()
    return display.newLayer()
end)

function CowPokerLayer:ctor(_controll, topLayer, bottomLayer ) -- _info ,_controll
	self:myInit(_controll, topLayer, bottomLayer)
	self:setupViews()

end

function CowPokerLayer:myInit( _controll, topLayer, bottomLayer ) --_info,_controll
	self.m_root = nil
	self.m_layout_size = nil
	self.m_layout_contain = nil
    -- self.m_info = _info
    self.m_controll = _controll
	self.m_xiajiapoker = {}
    self.m_TopLayer = topLayer    --顶部layer
    self.m_bottomLayer = bottomLayer --底部layer

    self.m_reqXiazhuang = false
    self.m_bAlwaysApply = false
    self.m_count = 0
    self.m_totalScore= 0
    self.m_addBetTab={}
    addMsgCallBack(self, Handredcattle_Events.MSG_UPDATE_APPLYARRAY_NTY, handler(self, self.updateBetInfo ))
end

function CowPokerLayer:setupViews()
    self.m_root = UIAdapter:createNode("cow_game_csb/cow_poker_layer.csb")
    self:addChild(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    
    self.m_layout_contain = self.m_root:getChildByName("layout_contain")
    self.m_layout_size = self.m_root:getChildByName("layout_size")
    for i=1,CowDef.m_BetEraeCount do
    	local areaXiazhu = self.m_layout_size:getChildByName("layout_xianjiapoker_" .. i )
    	
    	-- self.m_xiajiapoker[#self.m_xiajiapoker+1] = CowXianJiaPokerNode.new({m_area = i-1,m_info = self.m_info},self.m_controll)
        self.m_xiajiapoker[#self.m_xiajiapoker+1] = CowXianJiaPokerNode.new({m_area = i-1},self.m_controll, self.m_bottomLayer, self.m_TopLayer, self)
    	areaXiazhu:addChild(self.m_xiajiapoker[#self.m_xiajiapoker])	
    end

    self.m_btn_zhuang = self.m_root:getChildByName("btn_zhuang")

    self.m_player_num = self.m_root:getChildByName("player_num")

    self:setBetsInfo()

    self:initZhuangBtnInfo()

    --self:initZhuangHeadInfo()

    for i=1, CowDef.m_MaxBankCount do
        local bankerBtn = self.m_root:getChildByName(string.format("z_head_%d", i))
        bankerBtn:addTouchEventListener(handler(self, self.onBankerTouchCallback))
    end
end

function CowPokerLayer:setBetsInfo()

    self.can_bet_txt = self.m_root:getChildByName("can_bet_txt")
    self.al_bet_txt = self.m_root:getChildByName("al_bet_txt")
    self:updateBetInfo()
    self:initAlBetInfo()
end


function CowPokerLayer:initZhuangHeadInfo()
   local bankerArray = self.m_controll:getBankerArray()

   print("-------------------initZhuangHeadInfo----------------------------")

   for i=1, CowDef.m_MaxBankCount do
        local bankerNode = self.m_root:getChildByName(string.format("z_head_%d", i))

        local maskbankerNode = self.m_root:getChildByName(string.format("s_head_%d", i))


        if bankerArray[i] then
            ----------------
            bankerNode:setVisible(true)

            maskbankerNode:setVisible(true)

            local bankerInfo = self:getCellBankerInfo(bankerArray[i].m_chairId)

            if bankerNode.m_userId  then
                
                if bankerNode.m_userId ~= bankerInfo.m_userId then
                    if bankerInfo ~= nil then
                        if bankerNode:getChildByName("head_img") then
                            bankerNode:getChildByName("head_img"):updateTexture(bankerInfo.m_avatarId, bankerInfo.m_userId)
                            bankerNode.m_userId = bankerInfo.m_userId
                        end
                    end
                end

            else

                local head_img = CowClipperRoundHead.new(bankerNode:getContentSize(),Player:getFaceID(),Player:getAccountID(),true, false, "ps_daojishi_bar.png")
                head_img:setName("head_img")
                bankerNode:addChild(head_img)
                head_img:setScale(0.88)
                if bankerInfo ~= nil then
                    head_img:updateTexture(bankerInfo.m_avatarId, bankerInfo.m_userId)
                    bankerNode.m_userId = bankerInfo.m_userId
                end
                head_img:setPosition(bankerNode:getContentSize().width*0.5, bankerNode:getContentSize().height*0.5)
            end            

        else
            --------------
            bankerNode:setVisible(false)
            maskbankerNode:setVisible(false)
        end
   end
end

function CowPokerLayer:getCellBankerInfo(_chairId)
    local bankerInfo = nil

    local allUserInfo = self.m_controll:getArrUserData()
    for key, value in ipairs(allUserInfo) do
        if _chairId == value.m_chairId then
            bankerInfo = value
            break
        end
    end

    return bankerInfo
end

function CowPokerLayer:updateZhuangHeadInfo()
    self:initZhuangHeadInfo()
end

function CowPokerLayer:initZhuangBtnInfo()
    if self.m_controll and (self.m_controll:isInApplyArray() or self.m_controll:isInBankerArray() ) then
        self.m_btn_zhuang:loadTextures("ps_button_xiazhuang.png", "ps_button_xiazhuang.png", "ps_button_xiazhuang.png", 1)
    else
        self.m_btn_zhuang:loadTextures("ps_button_shangzhuang.png", "ps_button_shangzhuang.png", "ps_button_shangzhuang.png", 1)
    end
end

function CowPokerLayer:updateShangXiaZhuangBtn()

    if self.m_controll and self.m_controll:getCurCancelBanker() == CowDef.m_curCancelBanker then  -- 断线退出庄家
        self.m_btn_zhuang:loadTextures("ps_button_shangzhuang.png", "ps_button_shangzhuang.png", "ps_button_shangzhuang.png", 1)
    else
        if self.m_controll:isInApplyArray() or self.m_controll:isInBankerArray() then
            self.m_btn_zhuang:loadTextures("ps_button_xiazhuang.png", "ps_button_xiazhuang.png", "ps_button_xiazhuang.png", 1)
        else
            self.m_btn_zhuang:loadTextures("ps_button_shangzhuang.png", "ps_button_shangzhuang.png", "ps_button_shangzhuang.png", 1)
        end
    end
end

-- 创建空界面-------------
function CowPokerLayer:updateCowPokerLayer( _info,_controll )
    self.m_info = _info
    self.m_controll = _controll
    print("updateCowPokerLayer = ",#self.m_xiajiapoker)
    for i=1,#self.m_xiajiapoker do
        self.m_xiajiapoker[i]:updateCowPokerLayer({m_area = i-1,m_info = self.m_info},self.m_controll)
    end
    self:updateBetInfo()

    self:initZhuangBtnInfo()

    self:initZhuangHeadInfo()
end

function CowPokerLayer:updateBetInfo()
    local allscore = self.m_controll:getTotalBank()

    self.can_bet_txt:setString(allscore*0.001)
end

function CowPokerLayer:initAlBetInfo()
    self.al_bet_txt:setString("0")
end

function CowPokerLayer:initCowPokerLayer()
    -- 闲家牌
    self:resetPoker()
    self:updateBetInfo()
    self:initAlBetInfo()

    self:updateShangXiaZhuangBtn()
    self:updateZhuangHeadInfo()
end

function CowPokerLayer:updateAlXiaZhu()
    if self.m_controll then
        self.al_bet_txt:setString("" ..self.m_controll:getAddAllScore())
    end
end

function CowPokerLayer:callFreeState( _state )
    for i=1,#self.m_xiajiapoker do
        self.m_xiajiapoker[i]:setFreeState( _state or CowDef.m_gameState.EndFree )
    end
end

function CowPokerLayer:updateAllUserCounts(_counts)
    self.m_player_num:setString(_counts)
end

function CowPokerLayer:updateUserSortInfo()
    --[[local xianjiaList = self.m_controll:getAllXianJiaInfo()
    for i=1, 6 do

        local node = self.m_root:getChildByName(string.format("node_user_%d", i))
        local layout_head =  node:getChildByName("layout_head")
        local empty_spr = node:getChildByName("empty_spr")
        local img_head = node:getChildByName("img_head")
        local name_txt = node:getChildByName("name_txt")
        if xianjiaList[i] == nil then
            empty_spr:setVisible(true)
            img_head:removeAllChildren()
            name_txt:setString("虚位以待")
            layout_head.userId = nil
            layout_head.m_chairId = nil
        else
            empty_spr:setVisible(false)
            img_head:removeAllChildren()
            name_txt:setString(xianjiaList[i].m_nickName)

            layout_head.userId = xianjiaList[i].m_userId
            layout_head.m_chairId = xianjiaList[i].m_chairId

            local head_img = CowClipperRoundHead.new(img_head:getContentSize(), Player:getFaceID(),Player:getAccountID(), false)
            img_head:addChild(head_img)
            head_img:setScale(0.80)
            head_img:updateTexture(xianjiaList[i].m_avatarId, xianjiaList[i].m_userId)
            head_img:setPosition(img_head:getContentSize().width*0.5, img_head:getContentSize().height*0.5)
        end
    end--]]
end

------------------------------ 牌的逻辑 -----------------------------
function CowPokerLayer:updateBankerArrayNty( isinbankArray )
    for i=1,#self.m_xiajiapoker do
        self.m_xiajiapoker[i]:setCanXiazhu( not isinbankArray  ) -- or false
    end

    self:updateBetInfo()
    self:updateShangXiaZhuangBtn()
    self:updateZhuangHeadInfo()
end

function CowPokerLayer:updateApplyArrayNty()
    self:updateShangXiaZhuangBtn()
end

-- 切换筹码消息
function CowPokerLayer:sendChooseChip()
        for i=1,#self.m_xiajiapoker do
        self.m_xiajiapoker[i]:setChooseChip( true )
    end
end
-- 显示闲家的牌
function CowPokerLayer:showXianJiaPai(_index)
    if _index and _index <= CowDef.m_BetEraeCount then
        self.m_xiajiapoker[_index]:openPoker()
    end
end

    -- 重置闲家牌
function CowPokerLayer:resetPoker()
	for i=1,CowDef.m_BetEraeCount do
        self.m_xiajiapoker[i]:reSetPokerXianJiaPokerNode()
    end
end

function CowPokerLayer:startAddScoreNty( _info )
    if self.m_controll then
        for i=1,#self.m_xiajiapoker do
            self.m_xiajiapoker[i]:startAddScoreNty( true,self.m_controll:isInBankerArray() )
        end
    end
end

function CowPokerLayer:showStartAddScoreState()
    for i=1,#self.m_xiajiapoker do
        self.m_xiajiapoker[i]:showStartAddScoreState()
    end
end

function CowPokerLayer:addScoreNty( _info, _isSelf, _isFirstEnter)  -- _isSelf 自己下注, _isFirstEnter 刚进入游戏场景, _layer  底部layer
 --   for i=1,#self.m_xiajiapoker do
--    if _isSelf == true then
--         self.m_xiajiapoker[_info.m_addArea+1]:addScoreNty( _info,_isSelf, _isFirstEnter)
--         self.m_totalScore =_info.m_addScore+ self.m_totalScore
--          self.al_bet_txt:setString(self.m_totalScore*0.01) 
--    else
--        self.m_count =  self.m_count +1
--        _info._isSelf= _isSelf
--        table.insert( self.m_addBetTab,_info )
--        if self.m_count == 10 then

--            for k,_info in pairs(self.m_addBetTab) do
--                self.m_xiajiapoker[_info.m_addArea+1]:addScoreNty( _info,_info._isSelf, _isFirstEnter) 
--                self.m_totalScore =_info.m_addScore+ self.m_totalScore

--            end
--             self.al_bet_txt:setString(self.m_totalScore*0.01) 
--            self.m_count = 0
--            self.m_addBetTab = {} 
--        end
--    end
    self.m_xiajiapoker[_info.m_addArea+1]:addScoreNty( _info,_isSelf, _isFirstEnter)
  --  self.m_totalScore =_info.m_addScore+ self.m_totalScore
    if _info.m_bet then
        self.al_bet_txt:setString(_info.m_bet*0.01) 
    else
        self.al_bet_txt:setString(g_GameController:getAllBet()*0.01) 
    end
  --  end    
end
function CowPokerLayer:recoverData(info)
    self.m_totalScore = info.m_totalBet
     self.al_bet_txt:setString(self.m_totalScore*0.01) 
     self.can_bet_txt:setString(info.m_totalBankerCoin*0.001) 
     for k,v in pairs(info.m_areaBet) do 
         self.m_xiajiapoker[v.m_areaId+1]:recoverData(v)
    end 
end
function CowPokerLayer:pokerNode()
    for i=1,#self.m_xiajiapoker do
        self.m_xiajiapoker[i]:showlayoutpoker( _hide )
    end 
end
-- 闲家小计飞
function CowPokerLayer:xiaojiTip()
    for i=1,#self.m_xiajiapoker do
        self.m_xiajiapoker[i]:xiaojiTip()
    end
end
-- 显示胜负结果
function CowPokerLayer:showSuccessOrFailure( _index )
    self.m_xiajiapoker[_index]:shengFuBiaoJiFangdasuoxiao()
end

function CowPokerLayer:gameEndNty( _info )
    for i=1,#self.m_xiajiapoker do
        self.m_xiajiapoker[i]:gameEndNty(_info, _info.m_cardArray[i+1])
    end
     self.m_totalScore= 0
end

function CowPokerLayer:setCanntXiazhu()
    for i=1,#self.m_xiajiapoker do
        self.m_xiajiapoker[i]:setCanXiazhu( false ) -- 设置不能下注了
    end
end

function CowPokerLayer:showPokerAndxiaoji()
    for i=1,#self.m_xiajiapoker do
        self.m_xiajiapoker[i]:showPokerAndxiaoji() 
    end
end

function CowPokerLayer:showChoumaFlyResult()
    for i=1, #self.m_xiajiapoker do
        self.m_xiajiapoker[i]:showChoumaFlyResult()
    end
end

------------------------------------------------------------------------------------------------

function CowPokerLayer:cancelBankerAck(_info)

    if _info.m_result == 0 then
        
    elseif _info.m_result == CowDef.ErrorID.Success_CancelBanker_By_Cur_Game then --成功退庄(还在游戏中)
       self.m_reqXiazhuang = true
       local CowGameTipsNode = require("src.app.game.cattle.src.common.cattleDlgTip.CattleGameTipsNode")
       CowGameTipsNode.new("等本局结束后下庄") 
      
    elseif _info.m_result == CowDef.ErrorID.Success_CancelBanker_By_Exit_Apply then -- 成功退庄(退出申请队列)
       local CowGameTipsNode = require("src.app.game.cattle.src.common.cattleDlgTip.CattleGameTipsNode")
       CowGameTipsNode.new("等本局结束后下庄") 
    elseif _info.m_result == CowDef.ErrorID.Success_CancelBanker_By_Exit_Banker then -- 成功退庄(退出庄家队列)
       
    else
        local CowGameTipsNode = require("src.app.game.cattle.src.common.cattleDlgTip.CattleGameTipsNode")
        CowGameTipsNode.new(_info.m_result)
    end
end

function CowPokerLayer:applyBankerAck(_info)

    if _info.m_result == 0 then  -- 成功
    elseif  _info.m_result == CowDef.ErrorID.Success_ApplyBanker_By_Update_Apply then -- 申请成功，刷新排队列表
        local CowGameTipsNode = require("src.app.game.cattle.src.common.cattleDlgTip.CattleGameTipsNode")
        CowGameTipsNode.new("等本局结束后上庄")
         sendMsg(Handredcattle_Events.MSG_CLOSE)
    elseif _info.m_result == CowDef.ErrorID.Success_ApplyBanker_By_Update_Banker then -- 申请成功，刷新庄家列表

    elseif _info.m_result == CowDef.ErrorID.Err_ApplyBanker_By_Lack_Score then -- 申请失败，金币不足
        local CowGameTipsNode = require("src.app.game.cattle.src.common.cattleDlgTip.CattleGameTipsNode")
        CowGameTipsNode.new(_info.m_result)
    else
        local CowGameTipsNode = require("src.app.game.cattle.src.common.cattleDlgTip.CattleGameTipsNode")
        CowGameTipsNode.new(_info.m_result)
    end
end

function CowPokerLayer:lianXuShangZhuang()
    if self.m_bAlwaysApply then
        --self:sendReqZhuang()
    end
end

function CowPokerLayer:setAlwaysApply( _alwaysApply )
    self.m_bAlwaysApply = _alwaysApply
end

function CowPokerLayer:onBankerTouchCallback(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
      --[[ if sender.m_userId then
            CowPlayerInfo_Tip_Dlg.new(sender.m_userId, self.m_controll):showDialog()
       end--]]
       sendMsg(Handredcattle_Events.MSG_SHOW_BANKINFO, true)
    end
end

function CowPokerLayer:setReqXiazhuang(bool)
    self.m_reqXiazhuang = _bool
end


function CowPokerLayer:sendReqZhuang(score)
    if self.m_controll:canApplyBank() then
        self:sendMSGshuangzhuang(score)
        self.m_bAlwaysApply = true
    else
        self.m_bAlwaysApply = false
        local CowGameTipsNode = require("src.app.game.cattle.src.common.cattleDlgTip.CattleGameTipsNode")
        local score = self.m_controll:getApplyScore()*0.01
        CowGameTipsNode.new( "本房间上庄需要" .. score.. "金币，您金币不足" )
    end 
end

function CowPokerLayer:sendMSGxiazhuang()     
    sendMsg(Handredcattle_Events.MSG_COW_XIAZHUANG_REQ,{}) --请求下庄
end

function CowPokerLayer:sendMSGshuangzhuang(score)  
    sendMsg(Handredcattle_Events.MSG_COW_SHANGZHUANG_REQ,{score}) --请求上庄
end

-- 点击事件回调
function CowPokerLayer:onTouchCallback( sender )
    local name = sender:getName()
    print("点击事件回调CowPokerLayer------->name = ",name)
    if name == "layout_head" then
        if self.m_controll then
            print("userId:::::::::::", sender.userId)
            if sender.userId then
                CowPlayerInfo_Tip_Dlg.new(sender.userId,self.m_controll):showDialog()
            end
        end

    elseif name == "btn_zhuang" then
        --显示庄家信息-------
        if self.m_reqXiazhuang == true then
            local CowGameTipsNode = require("src.app.game.cattle.src.common.cattleDlgTip.CattleGameTipsNode")
            CowGameTipsNode.new( "等本局结束后下庄" )
            return
        end

        if self.m_controll:isInBankerArray() then
            --申请下庄
            self.m_reqXiazhuang = true
            self:sendMSGxiazhuang()
        elseif self.m_controll:isInApplyArray() then
            --申请下庄
            self:sendMSGxiazhuang()
        else
            --申请上庄
            if g_GameController:getGameSelfInfo().m_score<g_GameController:getApplyScore() then
                TOAST("上庄金币不足")
            else
                self.m_controll.gameScene:showBankApplyLayer() 
            end
            --self:sendReqZhuang()
        end

        --sendMsg(Handredcattle_Events.MSG_SHOW_BANKINFO, true)
    elseif name == "btn_palyerlist" then
        --self:getParent():showPlayerInfoListLayer()
    end
end

function CowPokerLayer:onExit()
	print("CowPokerLayer:onExit")
    for i=1,#self.m_xiajiapoker do
        self.m_xiajiapoker[i]:onExit()
    end
    self:removeAllChildren()
    removeMsgCallBack(self, Handredcattle_Events.MSG_UPDATE_APPLYARRAY_NTY)
end

return CowPokerLayer