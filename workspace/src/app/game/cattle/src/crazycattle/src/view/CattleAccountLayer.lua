--
-- Author: lhj
-- Date: 2018-08-27 
-- 结算界面
-- CowAccountLayer

local CowClipperRoundHead = require("src.app.game.cattle.src.roompublic.src.common.CattleClipperRoundHead")
local CowAccountLayer = class("CowAccountLayer", function()
    return display.newLayer()
end)

function CowAccountLayer:ctor(_control)
	self.m_control = _control
	self:myInit()    
    self:setupViews()
end

function CowAccountLayer:myInit()
	
end

function CowAccountLayer:setupViews()
	self.m_root = UIAdapter:createNode("cow_game_csb/cow_game_account_layer.csb")
    self:addChild(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))

    for i=1, 5 do

    	local account_node = self.m_root:getChildByName(string.format("max_account_node_%d", i))
    	local head_img = account_node:getChildByName("head_img")
        head_img:setVisible(false)
    	local num_node = account_node:getChildByName("num_node")
    	local bg = account_node:getChildByName("bg")
    	bg:setVisible(false)

    	local headNode = CowClipperRoundHead.new(head_img:getContentSize(),Player:getFaceID(),Player:getAccountID(),true)
    	account_node:addChild(headNode)
    	headNode:setName("headNode")
    	headNode:setScale(0.8)
    	headNode:setPosition(head_img:getPosition())

    	local fail_atla = ccui.TextAtlas:create("0", "numbers/fail_account.png", 38, 38, ".")
    	fail_atla:setAnchorPoint(cc.p(0, 0.5))
    	fail_atla:addTo(num_node)

    	fail_atla:setName("fail_atla")

    	fail_atla:setVisible(false)

    	local win_atla = ccui.TextAtlas:create("0", "numbers/win_account.png", 38, 38, ".")
    	win_atla:setAnchorPoint(cc.p(0, 0.5))
    	win_atla:addTo(num_node)

    	win_atla:setName("win_atla")

    	win_atla:setVisible(false)

    	account_node:setVisible(false)
    end
end

function CowAccountLayer:showViews()

	self:setVisible(true)
	local userAccountList = self.m_control:getAllGameScore()

	if #userAccountList == 0 then
		for i=1, 5 do
			local account_node = self.m_root:getChildByName(string.format("max_account_node_%d", i))
			account_node:setVisible(false)
		end

		return
	end

	if #userAccountList > 1 then
		table.sort(userAccountList, function(a, b)
			return a.m_score > b.m_score
		end)
	end

	local isRankMyState = false

	for i=1, 5 do
		if userAccountList[i] then
			local playerInfo = self:getMaxWinName(userAccountList[i].m_chairId)
			if Player:getAccountID() == playerInfo.m_userId then
				isRankMyState = true
			end
		end
	end

	for i=1, 5 do
		local account_node = self.m_root:getChildByName(string.format("max_account_node_%d", i))
    	local head_img = account_node:getChildByName("head_img")
    	local num_node = account_node:getChildByName("num_node")
    	local win_name = account_node:getChildByName("win_name")
    	local bg = account_node:getChildByName("bg")

    	local rank_img = account_node:getChildByName("rank_img")

    	local userAccount = nil

		if isRankMyState == false and i == 5 then
			local selfUserAccount = self:getSelfAccountInfo(self.m_control:getSelfChairId())
			if selfUserAccount then
				userAccount = selfUserAccount
			else
				userAccount = userAccountList[i]
			end
		else
			userAccount = userAccountList[i]
		end

		if userAccount then

			account_node:setVisible(true)

			local playerInfo = self:getMaxWinName(userAccount.m_chairId)

			local nickName  = ToolKit:shorterString(playerInfo.m_nickName, 6)
			win_name:setString(nickName)

			account_node:getChildByName("headNode"):updateTexture(playerInfo.m_avatarId, playerInfo.m_userId)

			if userAccount.m_score > 0 then

				num_node:getChildByName("win_atla"):setVisible(true)
				num_node:getChildByName("fail_atla"):setVisible(false)

				num_node:getChildByName("win_atla"):setString("/" .. math.abs(userAccount.m_score)*0.01)
				num_node:setPositionX(win_name:getPositionX() + win_name:getContentSize().width + 30)

			else

				num_node:getChildByName("win_atla"):setVisible(false)
				num_node:getChildByName("fail_atla"):setVisible(true)

				num_node:getChildByName("fail_atla"):setString("/" .. math.abs(userAccount.m_score)*0.01)
				num_node:setPositionX(win_name:getPositionX() + win_name:getContentSize().width + 30)

			end


			if Player:getAccountID() == playerInfo.m_userId then
				bg:setVisible(true)
				rank_img:setSpriteFrame(display.newSpriteFrame("pinshi_zi_jzj.png"))
			else
				bg:setVisible(false)
				rank_img:setSpriteFrame(display.newSpriteFrame(string.format("rank_hg%d.png", i)))
			end

		else
			account_node:setVisible(false)
		end
	end
end

function CowAccountLayer:getMaxWinName(_chairId)

	local palyerInfo = nil

    local allUserInfo = self.m_control:getArrUserData()
    for key, value in ipairs(allUserInfo) do
    	if _chairId == value.m_chairId then
    		palyerInfo = value
    		break
    	end
    end

    return palyerInfo
end

function CowAccountLayer:getSelfAccountInfo(_chairId)
	local userAccountList = self.m_control:getAllGameScore()
	for i=1, #userAccountList do
		if _chairId == userAccountList[i].m_chairId then
			return userAccountList[i]
		end
	end
	return nil
end

function CowAccountLayer:hideViews()
	self:setVisible(false)
end

function CowAccountLayer:onTouchCallback(sender)
	-- body
end

return CowAccountLayer