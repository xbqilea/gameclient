--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 比牌下注动画
-- CowSpecialEffectsBgLayer


require("src.app.game.cattle.src.crazycattle.src.def.CattleDef")
local CowAnimation = require("src.app.game.cattle.src.common.animation.CattleAnimation")
local CowArmatureResource = require("src.app.game.cattle.src.common.animation.CattleArmatureResource")

local CowSound = require("src.app.game.cattle.src.common.CowSound.CowSound")

local CowSpecialEffectsBgLayer = class("CowSpecialEffectsBgLayer", function()
    return display.newLayer()
end)


function CowSpecialEffectsBgLayer:ctor( _info )
	self:myInit(_info)    
    self:setupViews()
end

function CowSpecialEffectsBgLayer:myInit( _info )
    self.m_root = nil
    self.m_layout_bg = nil
    self.m_info = _info 
    self.m_img_bg = nil

    self.txt_heji = nil
    self.m_img_result_lost_bg = nil
    self.m_img_result_win_bg = nil
    self.m_txt_win_Atlas = nil
    self.m_txt_lost_Atlas = nil
    self.m_startX = 0

end

function CowSpecialEffectsBgLayer:setupViews()
    self.m_root = UIAdapter:createNode("cow_game_csb/cow_specialeffects_bglayer.csb")
    self:addChild(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    
    self.m_layout_bg = self.m_root:getChildByName("layout_bg") -- @5
    self.m_img_bg = self.m_root:getChildByName("img_bg")  -- @sfull@5
    self.txt_heji = self.m_layout_bg:getChildByName("txt_heji")
    self.m_img_result_lost_bg = self.m_layout_bg:getChildByName("img_result_lost_bg")
    self.m_img_result_win_bg = self.m_layout_bg:getChildByName("img_result_win_bg")

    self.m_txt_win_Atlas =  ccui.TextAtlas:create("0", "numbers/pinshi_shuzi_huangseh.png", 80, 80, ".")

    self.m_txt_win_Atlas:setPosition(self.txt_heji:getPositionX(),self.txt_heji:getPositionY())
    self.m_txt_win_Atlas:setAnchorPoint(self.txt_heji:getAnchorPoint())
    self.m_layout_bg:addChild(self.m_txt_win_Atlas)

    self.m_txt_lost_Atlas = ccui.TextAtlas:create("0", "numbers/pinshi_shuzi_langsel.png", 80, 80, ".")

    self.m_txt_lost_Atlas:setPosition(self.txt_heji:getPositionX(),self.txt_heji:getPositionY())
    self.m_txt_lost_Atlas:setAnchorPoint(self.txt_heji:getAnchorPoint())
    self.m_layout_bg:addChild(self.m_txt_lost_Atlas)

    self.m_txt_lost_Atlas:setVisible(false)
    self.m_txt_win_Atlas:setVisible(false)
     self.m_img_result_lost_bg:setVisible(false)
    self.m_img_result_win_bg:setVisible(false)
    self.m_startX = self.txt_heji:getPositionX() + 300
    self.txt_heji:removeFromParent()
    self.m_img_bg:setVisible(false)

    -- self.m_layout_bg:setVisible(false)
    
    self:showResultView(self.m_info)
end

function CowSpecialEffectsBgLayer:showResultView(_info)
    print("showResultView")
    self:stopAllAnimation()

    if _info == nil then
        return
    end

    if _info.m_score >=  0 then
        CowSound:getInstance():playEffect("OxSelfWin")
    else
        CowSound:getInstance():playEffect("OxSelfLose")
    end

    if _info.m_score < 0 then
        self.m_img_result_lost_bg:setVisible(true)

        --self.m_img_result_lost_bg:setPositionY(self.m_img_result_lost_bg_pos_result_Y)
        --self:playAnimation( CowArmatureResource.ANI_GAMELOST_ID,function ()
            self.m_txt_win_Atlas:setVisible(false)
            self.m_txt_lost_Atlas:setVisible(true)
            self.m_txt_lost_Atlas:setString("/".. _info.m_score*0.01 )
            local moveAction = cc.MoveTo:create(0.3, cc.p(452, self.m_txt_lost_Atlas:getPositionY() ))
            local fadeIn = cc.FadeIn:create(0.1)
            local delay = cc.DelayTime:create(1)
            local swan = cc.Sequence:create(fadeIn,moveAction, delay, cc.CallFunc:create(function()
                self:stopAllAnimation()
                local curScene = cc.Director:getInstance():getRunningScene()
                if curScene.m_cowAccountLayer then
                    curScene.m_cowAccountLayer:showViews()
                end
            end))
            self.m_txt_lost_Atlas:runAction(swan)
        --end )
    else    
        self.m_img_result_win_bg:setVisible(true)
        --self.m_img_result_win_bg:setPositionY(self.m_img_result_win_bg_pos_result_Y)
        --self:playAnimation( CowArmatureResource.ANI_GAMEWIN_ID,function ()
            self.m_txt_lost_Atlas:setVisible(false)
            self.m_txt_win_Atlas:setVisible(true)
            self.m_txt_win_Atlas:setString("/".._info.m_score*0.01)
            local moveAction = cc.MoveTo:create(0.3, cc.p(452, self.m_txt_win_Atlas:getPositionY() ))
            local fadeIn = cc.FadeIn:create(0.1)
            local delay = cc.DelayTime:create(1)
            local swan = cc.Sequence:create(fadeIn,moveAction, delay, cc.CallFunc:create(function()
                self:stopAllAnimation()
                local curScene = cc.Director:getInstance():getRunningScene()
                if curScene.m_cowAccountLayer then
                    curScene.m_cowAccountLayer:showViews()
                end
            end))
            self.m_txt_win_Atlas:runAction(swan)
        --end )
    end

end

--开始下注动画
function CowSpecialEffectsBgLayer:playXiaZuAnimation( _hander )
	self:stopAllAnimation()
    self:playAnimation(CowArmatureResource.ANI_GAMEXIAZHU_ID,_hander)
    local anim = CowAnimation:getInstance():getArmatureById(CowArmatureResource.ANI_GAMEXIAZHU_ID)
    if anim then
        anim:hangeDisplay( "bipai","fknn_txt_xiazhu.png" )
    end
end

--开始比牌动画
function CowSpecialEffectsBgLayer:playBiPaiAnimation( _hander )
	self:stopAllAnimation()
    self:playAnimation(CowArmatureResource.ANI_GAMEBIPAI_ID,_hander)
 
end

--开始动画
function CowSpecialEffectsBgLayer:playerStartAnim( _hander )
    self:stopAllAnimation()
    -- self.m_img_bg:setVisible(false)
    --self.m_img_result_win_bg:setVisible(true)

    self:playAnimation(CowArmatureResource.ANI_GMAESTART_ID,_hander)

end

--结束动画
function CowSpecialEffectsBgLayer:playerEndAnim( _hander )
    self:stopAllAnimation() 
--[[    self.m_img_result_lost_bg:setVisible(true)--]]
    self:playAnimation(CowArmatureResource.ANI_GMAEEDN_ID,_hander)
end

--等待上庄动画
function CowSpecialEffectsBgLayer:playWaitShangZhuang( _hander )
    self:stopAllAnimation()
    self:playAnimation(CowArmatureResource.ANI_PLAYERSHANGZHUANG_ID,_hander)
end

function CowSpecialEffectsBgLayer:playAnimation( _id ,_hander) 
    
    local _startAnim = CowAnimation:getInstance():getArmatureById(_id)
    if _startAnim then
        _startAnim:playAnimation( function ()
            if _hander then
                _hander()
            end

        end )
    end
end

function CowSpecialEffectsBgLayer:addAllArmature( _anim )
    if _anim then
        self:addChild(_anim,1)
        if _anim:getresourceById() == CowArmatureResource.ANI_GAMELOST_ID  then
            
            _anim:setPosition(self:getContentSize().width*0.5,self:getContentSize().height*0.5) 
        elseif _anim:getresourceById() == CowArmatureResource.ANI_GAMEWIN_ID then 
            _anim:setPosition(self:getContentSize().width*0.5,self:getContentSize().height*0.5)
        else
            _anim:setPosition(self:getContentSize().width*0.5, self:getContentSize().height*0.5)  
        end
        
    end
end

function CowSpecialEffectsBgLayer:playerBiPaiAnimLastFrame( _hander ,_time)
    self:playBiPaiAnimation()
    self:playerAnimLastFrame(CowArmatureResource.ANI_GAMEBIPAI_ID,_hander,_time,159)
end

function CowSpecialEffectsBgLayer:playerXiaZuAnimLastFrame( _hander ,_time)
    self:playXiaZuAnimation()
    self:playerAnimLastFrame(CowArmatureResource.ANI_GAMEXIAZHU_ID,_hander,_time,159)
end

function CowSpecialEffectsBgLayer:playerAnimLastFrame(_id, _hander,_time,_index )
    print("_time = ",_time)
    local _startAnim = CowAnimation:getInstance():getArmatureById(_id)
    if _startAnim then
        _startAnim:playgotoFrame( _hander,_time,_index)
    end
end

--------------------- 结束相关动画 -----------------------
function CowSpecialEffectsBgLayer:stopAnimation( _id )
    for i=1,#CowSpecialEffectsBgLayer.ArmatureID do
        if CowSpecialEffectsBgLayer.ArmatureID[i] == _id then
            local _startAnim = CowAnimation:getInstance():getArmatureById(_id)
            if _startAnim then
                _startAnim:stopAnimation()
            end
            break
        end
    end
end

function CowSpecialEffectsBgLayer:stopAllAnimation()
    CowAnimation:getInstance():stopAllShengBiPaiArmature()
    self.m_txt_win_Atlas:stopAllActions()
    self.m_txt_lost_Atlas:stopAllActions()
    self.m_txt_win_Atlas:setPositionX(self.m_startX)
    self.m_txt_lost_Atlas:setPositionX(self.m_startX)
    self.m_txt_win_Atlas:setOpacity(0)
    self.m_txt_lost_Atlas:setOpacity(0)
    self.m_img_result_lost_bg:setVisible(false)
    self.m_img_result_win_bg:setVisible(false)
    self.m_img_bg:setVisible(false)

    self.m_animTb = {}
end


function CowSpecialEffectsBgLayer:onTouchCallback( sender )
    
end
return CowSpecialEffectsBgLayer