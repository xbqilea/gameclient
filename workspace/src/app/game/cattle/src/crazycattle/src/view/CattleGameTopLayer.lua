--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- CowGameTopLayer
-- 牛牛顶上的信息
local scheduler = require("framework.scheduler")
local CowClipperRoundHead = require("src.app.game.cattle.src.roompublic.src.common.CattleClipperRoundHead")
local CowAnimation = require("src.app.game.cattle.src.common.animation.CattleAnimation")
local CowSound = require("src.app.game.cattle.src.common.CowSound.CowSound")

local CowPlayerInfo_Tip_Dlg = require("app.game.cattle.src.roompublic.src.common.CattlePlayerInfo_Tip_Dlg")
local CowGameTopLayer = class("CowGameTopLayer", function()
    return display.newLayer()
end)
local offSetWidth = 20
function CowGameTopLayer:ctor( _controll ) --_info,_controll
	self:myInit( _controll ) -- _info ,_controll
	self:setupViews()
end

function CowGameTopLayer:myInit( _controll ) --_info, _controll
	self.m_root = nil
    -- layout_top@2
    self.m_layout_top = nil
    ------ layput_zhuangjiapai@2
    self.m_layout_zhuangjiapai = nil
    self.m_img_zhuangpokerbg = nil
    --layout_poker
    self.m_layout_poker = nil
    self.m_img_poker = {}

    -- layout_tihuan
    self.m_layout_tihuan = nil
    self.m_img_tihuan = nil
    self.m_img_tihuan_poker = nil
    self.m_img_tihuan_pos = nil
    self.m_img_tihuan_poker_pos = nil
    
       -- result
    self.m_layout_result = nil
    self.m_img_zhadan = nil
    self.m_cow_type_img = nil



    -- 下注时间
    self.m_layout_xiazhutime = nil
    self.m_img_countdown = nil
    self.m_img_lasttime = nil -- 下注倒计时

    ------ layout_zhuangjianame
    self.m_layout_zhuangjianame = nil
    self.m_txt_allscore = nil -- 总金额
    self.m_img_cowhead = {} -- 8个庄家
    self.m_img_cowheadbg = {}
    -----layout_left@1
    self.m_layout_left = nil

    self.m_btn_back = nil

    ------layout_right@3
    self.m_layout_right = nil

    self.m_layout_shangzhuang = nil


    -- self.m_btn_shangzhuang = nil      --上庄

    self.m_txt_shangzhuangtip = nil

    -- self.m_btn_xiazhuang = nil        --下庄

    self.m_txt_shangzhuangbanktip = nil
    self.m_txt_time = nil
    --------------------------- 非界面 -----------------------
    self.m_lastTime = 0

    self.m_btn_zhuang = nil
        -- 庄家牌
    self.m_cardInfo = nil

    -- self.m_info = _info 

    self._controll = _controll
    self.m_totleCount = 0          --总局数
    
    --设置
    self.m_IsShowSet = false

end

function CowGameTopLayer:setupViews()
	self.m_root = UIAdapter:createNode("cow_game_csb/cow_gameTop_layer.csb")
    self:addChild(self.m_root)
   
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))


    self.m_layout_bg = self.m_root:getChildByName("layout_bg")
    self.m_layout_bg:setSwallowTouches(false)

    -- layout_top@2
    self.m_layout_top = self.m_root:getChildByName("layout_top") -- @2
    ------ layput_zhuangjiapai@2
    self.m_layout_zhuangjiapai = self.m_layout_top:getChildByName("layout_zhuangjiapai") -- @2
    self.m_img_zhuangpokerbg = self.m_layout_zhuangjiapai:getChildByName("img_zhuangpokerbg")
    
    self.m_layout_poker = self.m_layout_zhuangjiapai:getChildByName("layout_poker")
    -- self.m_layout_tihuan
    self.m_layout_tihuan = self.m_layout_zhuangjiapai:getChildByName("layout_tihuan")
    self.m_img_tihuan = self.m_layout_tihuan:getChildByName("img_tihuan")
    self.m_img_tihuan_pos = cc.p(self.m_img_tihuan:getPositionX(),self.m_img_tihuan:getPositionY())
    self.m_img_tihuan_poker = self.m_layout_tihuan:getChildByName("img_tihuan_poker")
    self.m_img_tihuan_poker_pos = cc.p(self.m_img_tihuan_poker:getPositionX(),self.m_img_tihuan_poker:getPositionY())
    
    for i=1,CowDef.m_MaxCardCount do
        self.m_img_poker[#self.m_img_poker+1] = self.m_layout_poker:getChildByName("img_poker_" .. i)
    end
    self.m_layout_result = self.m_layout_zhuangjiapai:getChildByName("layout_result")
    


    self.m_img_zhadan = self.m_layout_result:getChildByName("img_zhadan")
    self.m_cow_type_img = self.m_layout_result:getChildByName("cow_type_img")
    ----------------
    -- dump(cc.FileUtils:getInstance():isFileExist("numbers/fknn_guess_red_num.png"))
    self.m_layout_xiazhutime = self.m_layout_zhuangjiapai:getChildByName("layout_xiazhutime")
    self.m_img_lasttime = self.m_layout_xiazhutime:getChildByName("img_lasttime")
    local pos = self.m_img_lasttime:getPosition()

    self.m_timeLabelAtlas = cc.LabelAtlas:_create("9", "numbers/ps_daojishi_sz_jin.png",48,60,48)
    if self.m_timeLabelAtlas then
        self.m_layout_xiazhutime:addChild(self.m_timeLabelAtlas,1)
        self.m_timeLabelAtlas:setPosition(self.m_img_lasttime:getPosition())
        self.m_timeLabelAtlas:setAnchorPoint(self.m_img_lasttime:getAnchorPoint())
        self.m_img_lasttime:removeSelf()
    end
    ------ layout_zhuangjianame
    self.m_layout_zhuangjianame = self.m_root:getChildByName("layout_zhuangjianame")
    self.m_txt_allscore = self.m_layout_zhuangjianame:getChildByName("txt_allscore") -- 总金额
    local  _avatarId = Player:getFaceID()
    local  _userId = Player:getAccountID()

    --[[for i=1,CowDef.m_MaxBankCount do
        local headimg= self.m_layout_zhuangjianame:getChildByName("img_cowhead_" .. i ) -- 8个庄家
        self.m_img_cowheadbg[#self.m_img_cowheadbg+1] = headimg
        -- 默认没有庄家
        local size = headimg:getContentSize()
        self.m_img_cowhead[#self.m_img_cowhead+1] = CowClipperRoundHead.new(cc.size(size.width-4,size.height-4),_avatarId ,_userId,true)
        headimg:addChild(self.m_img_cowhead[#self.m_img_cowhead],-1)
        self.m_img_cowhead[#self.m_img_cowhead]:setPosition(cc.p(headimg:getContentSize().width*0.5,headimg:getContentSize().height*0.5))
        self.m_img_cowhead[#self.m_img_cowhead]:setVisible(false)
    end
--]]
    -----layout_left@1
    self.m_layout_left = self.m_root:getChildByName("layout_left") -- @1
    self.m_btn_back = self.m_root:getChildByName("btn_back")

    ------layout_right@3  -- 
    self.m_layout_right = self.m_root:getChildByName("layout_right") -- @3
    self.m_layout_shangzhuang = self.m_root:getChildByName("layout_shangzhuang")
    
    -- 上一行 --
    self.m_txt_shangzhuangbanktip = self.m_root:getChildByName("txt_shangzhuangbanktip")
    self.m_txt_shzhbankinfo = self.m_root:getChildByName("txt_shzhbankinfo")
    self.m_txt_shangzhuangbanktip:setString(" ")
    self.m_txt_shzhbankinfo:setString(" ")
    -- 下一行 --
    self.m_txt_shzhinfo = self.m_root:getChildByName("txt_shzhinfo")
    self.m_txt_shzhinfo:setString(" ")

    self.m_txt_shangzhuangtip = self.m_root:getChildByName("txt_shangzhuangtip")
    self.m_txt_shangzhuangtip:setString(" ") 


    --设置--
    self.m_set_layout = self.m_root:getChildByName("set_layout")


    --
    self.m_txt_bg = self.m_root:getChildByName("txt_bg")

    self.m_zhuang_node = self.m_root:getChildByName("zhuang_node")
    self.m_zhuang_node:setVisible(false)

    self.m_txt_bg:setContentSize(cc.size(200, 76))

    self.m_t_player_num = self.m_root:getChildByName("t_player_num")

    --庄家人数显示

    self:updateBankerArrayNty()
    self:initTiHuanPoker()
    self:initCowGameTopLayer1()


end

function CowGameTopLayer:updateCowGameTopLayer( _info, _controll )
    self.m_info = _info 
    self._controll = _controll
    print("updateCowGameTopLayer")
    self:initCowGameTopLayer()
    self:showShangZhuangScore()

end
--function CowGameTopLayer:onClickRecordBtn( sender, eventType )
--	if eventType == ccui.TouchEventType.ended then
--		local GameRecordLayer = GameRecordLayer.new(2)
--        self:addChild(GameRecordLayer)   
--        GameRecordLayer:setScaleX(display.scaleX)
--         ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", {105})
--	end
--end
function CowGameTopLayer:showShangZhuangScore()
    -- 上庄金币
--[[    self.m_txt_shangzhuangbanktip:setVisible(true)
    if self._controll:isInBankerArray() then
        self.m_txt_shangzhuangbanktip:setString( " "  ) -- 自动
        self.m_txt_shzhbankinfo:setString( "连续上庄"  )
    else
        self.m_txt_shzhbankinfo:setString( "上庄金币:"  )
        self.m_txt_shangzhuangbanktip:setString( tostring(CowRoomDef:scoreNumberCell(self._controll:getApplyScore()) )   )
        local width = self.m_txt_shzhbankinfo:getContentSize().width + offSetWidth
        self.m_txt_shangzhuangbanktip:setPositionX(width )
    end--]]

    self.m_txt_shzhbankinfo:setString( "上庄金币:"  )
    self.m_txt_shangzhuangbanktip:setString(self._controll:getApplyScore()*0.01)
    local width = self.m_txt_shzhbankinfo:getContentSize().width + offSetWidth
    self.m_txt_shangzhuangbanktip:setPositionX(width )
    

end

function CowGameTopLayer:initCowGameTopLayer1()
    self.m_layout_tihuan:setVisible(false) -- 隐藏翻牌
    self:showlayoutcowPoint( false ) -- 隐藏牛牛点数
    self:showlayoutpoker( false )
    self.m_layout_xiazhutime:setVisible(false)


    self.m_reqXiazhuang = false

    -- 初始化庄家默认背景牌
    self:showBackPoker()
    -- 停止播放倒计时
    self:endLastTimer()
    self.m_layout_poker:stopAllActions()
    self.m_layout_poker:setScale(1)
        --归位
    self.m_img_tihuan:stopAllActions()
    self.m_img_tihuan_poker:stopAllActions()
    self.m_img_tihuan:setPosition(self.m_img_tihuan_pos)
    self.m_img_tihuan_poker:setPosition(self.m_img_tihuan_poker_pos)

end
-- _gamescene == 0 场景消息刷新,1 结束
function CowGameTopLayer:initCowGameTopLayer( _gamestate )
    self:initCowGameTopLayer1()
    -- 显示庄家信息（8个庄家，+最大下注，金额）
    if self._controll then
        self:showShangZhuangScore()
        self:updateAllBankScore()
        self:updateBankerArrayNty( self._controll:getBankerArray(),_gamestate)
    end
    
end

function CowGameTopLayer:showlayoutcowPoint( _hide )
    self.m_layout_result:setVisible(_hide)
end

function CowGameTopLayer:showlayoutpoker( _hide )
    self.m_layout_poker:setVisible(_hide)
end

--刷新
function CowGameTopLayer:updateApplyArrayNty( _info )
    if _info.m_hasInsArray == true then -- 显示队列
        self.m_txt_shangzhuangtip:setVisible(true)
        self.m_txt_shangzhuangtip:setString(_info.m_num .. "/" .. _info.m_count)
        self.m_txt_shzhinfo:setString("我的队列:")
        local width = self.m_txt_shzhinfo:getContentSize().width + offSetWidth
        self.m_txt_shangzhuangtip:setPositionX(width )
    elseif self._controll:isInBankerArray() then --自己是庄家，显示剩余上庄次数

    else --隐藏
        self.m_txt_shangzhuangtip:setVisible(true)
        self.m_txt_shzhinfo:setString("排队人数:")
        self.m_txt_shangzhuangtip:setString(_info.m_count)
        self.m_totleCount = _info.m_count
        local width = self.m_txt_shzhinfo:getContentSize().width + offSetWidth
        self.m_txt_shangzhuangtip:setPositionX(width )
    end

    if self._controll then
        self:updateShangXiaZhuangBtn( self._controll:isInBankerArray(), _info.m_hasInsArray )
    end
end

function CowGameTopLayer:setshangzhuangtip()
    -- self.m_txt_shangzhuangtip:setVisible(true)
    self.m_txt_shzhinfo:setString("排队人数:")
    self.m_txt_shangzhuangtip:setString(tostring(self.m_totleCount))
    local width = self.m_txt_shzhinfo:getContentSize().width + offSetWidth
    self.m_txt_shangzhuangtip:setPositionX(width )
end

function CowGameTopLayer:updateBankerArrayNty( _bankerArray,_gamestate )

    self:showShangZhuangScore()
    self:updateAllBankScore()
    -----------------------------------
    if self._controll then
        self:updateShangXiaZhuangBtn(self._controll:isInBankerArray(),self._controll:isInApplyArray(),_gamestate)
    end
end

-- 刷新下注上限进度条
function CowGameTopLayer:updateXiaZhuMaxBar()
    ------------
end

function CowGameTopLayer:updateAllBankScore()
   -- 总金额
    if self._controll then
       local allBankScore = self._controll:getAllBankAllScore()
       self.m_txt_allscore:setString("总金额:" .. allBankScore)
       local allscore = self._controll:getAllBankScore()
    end
end


--显示倒计时
function CowGameTopLayer:showLastTimer( _lastTime )
    self:showlayoutpoker ( true )
    self.m_timeLabelAtlas:setString("" .. _lastTime)
    self.m_layout_xiazhutime:setVisible(true)
    self.m_lastTime = _lastTime or 0
    if  self.m_lastTime <= 0 then
        self.m_lastTime = 0
    end
    function updateLastTimer()
        CowSound:getInstance():playEffect("OxCountTime")
        self.m_lastTime = self.m_lastTime - 1
        if self.m_lastTime <= 0 then
            self:endLastTimer()
        end
        -- print("self.m_lastTime = ",self.m_lastTime)
        self.m_timeLabelAtlas:setString("" .. self.m_lastTime)
    end
    
    self.m_lastTimer = scheduler.scheduleGlobal(updateLastTimer,1)
end
-- 结束倒计时动画，隐藏倒数计时界面
function CowGameTopLayer:endLastTimer()
    if self.m_lastTimer then
        scheduler.unscheduleGlobal(self.m_lastTimer)
        self.m_lastTimer = nil
    end
    self.m_layout_xiazhutime:setVisible(false)
    self.m_lastTime = 0
    if self:getParent() ~= nil then
        self:getParent():setCanntXiazhu()
    end
end

function CowGameTopLayer:updateShangXiaZhuangBtn( _inbankArray,_inApplyArray,_gamestate )

    print("_inbankArray, _inApplyArray", _inbankArray, _inApplyArray)


    if self._controll and self._controll:isInBankerArray() then
        self.m_zhuang_node:setVisible(true)
        self.m_txt_bg:setContentSize(cc.size(200, 120))
    else
        self.m_zhuang_node:setVisible(false)
        self.m_txt_bg:setContentSize(cc.size(200, 76))
    end

    if _gamestate == 1 then
        self._controll:setCurCancelBanker(0)
    end
end

function CowGameTopLayer:bankerTimesAck( _info )
    if self._controll:isInBankerArray() then
        self.m_t_player_num:setString(tostring(_info.m_times))
        self:setshangzhuangtip()
    end
end

-- 设置牌
function CowGameTopLayer:gameEndNty( _info )
    self.m_cardInfo = _info
    -- dump(self.m_cardInfo, "desciption")
    -- 关闭倒计时
    self:endLastTimer()
end

-- 显示庄家牌1,先显示随机的牌，4张，在翻牌，最后显示排好顺序的牌
function CowGameTopLayer:showRandPoker()
    -- 现在先显示4张，然后再显示5张
    local temp = {}
    for i=1,#self.m_cardInfo.m_bankerRandCard.m_card do
        temp[i] = self.m_cardInfo.m_bankerRandCard.m_card[i]
    end
    temp[5] = 0x50
    if #temp < 4 then -- 随机数组为空
        temp = {0x50,0x50,0x50,0x50,0x50}
    end
    self:reSetPoker( temp )
    --放大缩小
    local sequence = CowAnimation:creatAreaPokerScaleBigToSmall( handler(self, self.sendShowXianJiaPai) )
    self.m_layout_poker:runAction(sequence)
end

function CowGameTopLayer:sendShowXianJiaPai()
    CowSound:getInstance():playEffect("Ox_Card_Fly")
    performWithDelay(self, function()
        if self._controll then
            self._controll.gameScene:showXianJiaPai1()
        end
    end, 0.5)
end

function CowGameTopLayer:initTiHuanPoker()
    --初始化裁剪区域
    local rect = self.m_layout_tihuan:getBoundingBox()
    local x = self.m_layout_tihuan:getPositionX()
    local y = self.m_layout_tihuan:getPositionY()
    local size = self.m_layout_tihuan:getContentSize()
    self.m_ClipRect=display.newClippingRectangleNode(cc.rect(x,y,size.width,size.height))
    -- self.m_ClipRect:addTo(self.m_layout_zhuangjiapai)
    self.m_layout_zhuangjiapai:addChild(self.m_ClipRect)
    self.m_layout_tihuan:removeFromParent()
    self.m_ClipRect:addChild(self.m_layout_tihuan)
end

function CowGameTopLayer:bankfanPai()
    -- 被翻背面
    local fileName = CowCardConfig:getCardFileName( 0x50 )
    self.m_img_tihuan_poker:loadTexture(fileName,1)
    self.m_layout_tihuan:setVisible(true)
    self.m_img_poker[CowDef.m_MaxCardCount]:setVisible(false)

    -- 被翻正面
    -- dump(self.m_cardInfo.m_bankerRandCard.m_card)
    local beifanfileName = CowCardConfig:getBianCardFileName( self.m_cardInfo.m_bankerRandCard.m_card[5] )
    self.m_img_tihuan:loadTexture(beifanfileName, 1)
    -- 动作
    local BackFirstAct=cc.MoveBy:create(0.5,cc.p(0, -16))
    local FanPaiFirstAct=cc.MoveBy:create(0.5,cc.p(3, 16))
    local FirstDelayAct=cc.DelayTime:create(1)
    local BackSecondAct=cc.MoveBy:create(1,cc.p(0, -33))
    local FanPaiSecondAct=cc.MoveBy:create(1,cc.p(11, 33))
    local SecondDelayAct=cc.DelayTime:create(1)
        --完成翻牌
    local function FanPaiFinish()
        --隐藏裁剪区扑克
        self.m_layout_tihuan:setVisible(false)
        self.m_img_poker[CowDef.m_MaxCardCount]:setVisible(true)
        --显示排好顺序的牌
        if self.m_cardInfo then
            self:reSetPoker( self.m_cardInfo.m_bankerRandCard.m_card )
        end
        --归位
        self.m_img_tihuan:setPosition(self.m_img_tihuan_pos)
        self.m_img_tihuan_poker:setPosition(self.m_img_tihuan_poker_pos)
        -- 变牌
        self:juageBianPai()
        -- 显示牛几
        self:showCowCount()

    end
    --生成翻牌结束响应
    local ActFunFanPaiFinish=cc.CallFunc:create(FanPaiFinish)
    --生成背面扑克连续动作
    local ActBackSeq=cc.Sequence:create(BackFirstAct,FirstDelayAct,
        BackSecondAct,SecondDelayAct)
    --生成翻牌扑克连续动作
    local ActFanPaiSeq=cc.Sequence:create(FanPaiFirstAct,FirstDelayAct,
        FanPaiSecondAct,SecondDelayAct,ActFunFanPaiFinish)
    --背面扑克运行动作
    self.m_img_tihuan_poker:runAction(ActBackSeq)
    --翻牌扑克运行动作
    self.m_img_tihuan:runAction(ActFanPaiSeq)    
end 

--显示牛几点数
function CowGameTopLayer:showCowCount()
    local result = self.m_cardInfo.m_cardType[1]
    self:showlayoutcowPoint( true )
    if nil == result then
        -- CowDebug.printDlg( "判断牛牛点数异常" )
    elseif CowDef.CowCount.Bomb == result then --炸弹
        self.m_img_zhadan:setVisible(true)
        self.m_cow_type_img:setVisible(false)
        CowSound:getInstance():playEffect( "OxBomb" )
    elseif CowDef.CowCount.WUNIU == result then --无牛
        self.m_img_zhadan:setVisible(false)
        self.m_cow_type_img:setVisible(true)
        self.m_cow_type_img:setSpriteFrame(display.newSpriteFrame("ps_msz_hui_niu0.png"))
        CowSound:getInstance():playEffect( "OxWuNiu" )
    elseif CowDef.CowCount.JIN_NIU == result then --金牛
        self.m_img_zhadan:setVisible(false)
        self.m_cow_type_img:setVisible(true)
        self.m_cow_type_img:setSpriteFrame(display.newSpriteFrame("ps_msz_jin_jinniu.png"))
        CowSound:getInstance():playEffect( "OxJinNiu" )
    elseif CowDef.CowCount.YIN_NIU == result then --银牛
        self.m_img_zhadan:setVisible(false)
        self.m_cow_type_img:setVisible(true)
        self.m_cow_type_img:setSpriteFrame(display.newSpriteFrame("ps_msz_jin_yinniu.png"))
        CowSound:getInstance():playEffect( "OxYinNiu" )
    elseif CowDef.CowCount.NIU_NIU == result then --牛牛
        self.m_img_zhadan:setVisible(false)
        self.m_cow_type_img:setVisible(true)
        self.m_cow_type_img:setSpriteFrame(display.newSpriteFrame("ps_msz_jin_niuniu.png"))
        CowSound:getInstance():playEffect( "OxNiuNiu" )
    else  -- 正常点数
        self.m_img_zhadan:setVisible(false)
        self.m_cow_type_img:setVisible(true)
        self.m_cow_type_img:setSpriteFrame(display.newSpriteFrame(string.format("ps_msz_jin_niu%d.png", result)))
        CowSound:getInstance():playEffect( "OxNiu" .. result)
    end

    sendMsg(Handredcattle_Events.MSG_SHOW_XIANJIA_XIAOJI)
end

------------------------ 牌显示背面 ----------------
function CowGameTopLayer:reSetPoker( _data )
    if _data == nil then  --还原
        self:showBackPoker()
        return
    end
    for i=1,#_data do
        local fileName = CowCardConfig:getCardFileName( _data[i] )
        -- print("reSetPoker fileName = ",fileName)
        if fileName then
            self.m_img_poker[i]:loadTexture(fileName,1)
            self.m_img_poker[i]:setVisible(true)
        end
    end
end

function CowGameTopLayer:showBackPoker()
    self:reSetPoker( {0x50,0x50,0x50,0x50,0x50} )
    for i=1,#self.m_img_poker do
        self.m_img_poker[i]:getChildByName("img_bianpai_" .. i):setVisible(false)
    end
end

-- 显示牛几的时候，判断变牌，有就变
function CowGameTopLayer:juageBianPai()
    if self:isJackPoker() then
        for i=1,#self.m_cardInfo.m_jokerCardData do
            -- print("扑克变牌数据_info.m_jokerCardData == %02x ",self.m_cardInfo.m_jokerCardData[i].m_card)
            -- print("变牌点数_info.m_jokerCardData = ",self.m_cardInfo.m_jokerCardData[i].m_changeValue)
            for k=1,#self.m_cardInfo.m_bankerRandCard.m_card do
                if self.m_cardInfo.m_jokerCardData[i].m_card == self.m_cardInfo.m_bankerRandCard.m_card[k] then
                    self.m_img_poker[k]:loadTexture(CowCardConfig:getBianPaiJockFileName( self.m_cardInfo.m_jokerCardData[i].m_card ),1)
                    self.m_img_poker[k]:getChildByName("img_bianpai_" .. k):setVisible(true)
                    self.m_img_poker[k]:getChildByName("img_bianpai_" .. k):loadTexture(CowCardConfig:getBianPaisamllJockFileName(self.m_cardInfo.m_jokerCardData[i].m_changeValue,self.m_cardInfo.m_jokerCardData[i].m_card),1)
                end
            end
        end
    end
end

-- 手里是否有王
function CowGameTopLayer:isJackPoker()
    for k=1,#self.m_cardInfo.m_bankerRandCard.m_card do
        if 0x4E == self.m_cardInfo.m_bankerRandCard.m_card[k] or 
            0x4F == self.m_cardInfo.m_bankerRandCard.m_card[k] then
            return true
        end
    end
    return false
end

function CowGameTopLayer:showSetInfo()
    if self.m_IsShowSet then
        self.m_set_layout:runAction(cc.MoveTo:create(0.2, cc.p(300, -4)))
    else
        self.m_set_layout:runAction(cc.MoveTo:create(0.2, cc.p(492, -4)))
    end
end

-- 点击事件回调
function CowGameTopLayer:onTouchCallback( sender )
    local name = sender:getName()
    print("CowGameTopLayer--->name = ",name)
    if  name == "btn_zhuang" then
        --显示庄家信息-------
        sendMsg(Handredcattle_Events.MSG_SHOW_BANKINFO, true)
    elseif name == "btn_back" then
        self:getParent():showAskExitGameTip()
    elseif name == "layout_zhuangjianame" then  --弹出庄家信息
    elseif name == "img_banktitle" then
        --sendMsg(Handredcattle_Events.MSG_SHOW_BANKINFO,true)
    elseif name == "set_btn" then
        self.m_IsShowSet = true
        self:showSetInfo()
    elseif name == "layout_bg" then
        self.m_IsShowSet = false
        self:showSetInfo()
    elseif name == "btn_sound" then
        self:getParent():showSetLayer(true)
    elseif name == "btn_exit" then
        self:getParent():showAskExitGameTip()
    elseif name == "btn_rule" then
        --规则--
        self:getParent():showGuizeLayer(true)
     elseif name == "btn_record" then 
      
    else
        local substring = string.sub(name,1,15)
        local num = tonumber(string.sub(name,16,16)) 
        if  substring == "layout_cowhead_" then
            if self.m_img_cowhead[num] and self.m_img_cowhead[num]:isVisible()  then
                -- local info = self._controll:getUserWinLostTimesInfo(self.m_img_cowhead[num].m_userid)
                if self.m_img_cowhead[num].m_userid then
                    print("onItemModifyHeadClick info")
                    CowPlayerInfo_Tip_Dlg.new(self.m_img_cowhead[num].m_userid,self._controll):showDialog()
                end  
            end
        end

    end
end

function CowGameTopLayer:removeMsg()

end

function CowGameTopLayer:onExit()
    self:removeMsg()
    self:endLastTimer()
   print("CowGameTopLayer:onExit")
end
return CowGameTopLayer
