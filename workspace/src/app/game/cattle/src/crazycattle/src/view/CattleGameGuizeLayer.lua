--
-- Author: 
-- Date: 2018-08-27 
-- 规则
-- CowGameGuizeLayer

local CowSound = require("src.app.game.cattle.src.common.CowSound.CowSound")
local CowDialogBase = require("src.app.game.cattle.src.common.cattleDlgTip.CattleDialogBase")

local CowGameGuizeLayer = class("CowGameGuizeLayer", function()
    return CowDialogBase.new()
end)

function CowGameGuizeLayer:ctor()
	self:myInit()
	self:setupViews()    
end

function CowGameGuizeLayer:myInit()

end

function CowGameGuizeLayer:setupViews()
    self.m_root = UIAdapter:createNode("cow_game_csb/cow_game_guize_layer.csb")
    self:addChild(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    UIAdapter:praseNode(self.m_root,self)
    local winSize = cc.Director:getInstance():getWinSize();
    self.m_root:setPositionX((winSize.width-winSize.width/display.scaleX)/2)
end

function CowGameGuizeLayer:onTouchCallback(sender)
    local name = sender:getName()
    if name == "btn_close" then
        CowSound:getInstance():playEffect("OxButton")
        self:getParent():showGuizeLayer(false)
    end
end

function CowGameGuizeLayer:onExit()
   print("CowGameGuizeLayer:onExit")
end

return CowGameGuizeLayer