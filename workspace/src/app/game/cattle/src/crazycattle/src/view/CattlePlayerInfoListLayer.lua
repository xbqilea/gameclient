--
-- Author: lhj
-- Date: 2018-08-07 10：16
-- 玩家列表
-- CowPlayerInfoListLayer
local CowPalyerInfoNode = require("src.app.game.cattle.src.crazycattle.src.view.CattlePalyerInfoNode")
require("src.app.game.cattle.src.crazycattle.src.def.CattleDef")
local ClipperRound = require("src.app.hall.base.ui.HeadIconClipperArea") 
local MaxUpdateCount = 2
local CowSound = require("src.app.game.cattle.src.common.CowSound.CowSound")
local CowDialogBase = require("src.app.game.cattle.src.common.cattleDlgTip.CattleDialogBase")
local CowPlayerInfoListLayer = class("CowPlayerInfoListLayer", function()
    return CowDialogBase.new()
end)

function CowPlayerInfoListLayer:ctor( _controll )
	self:myInit(_controll)
	self:setupViews() 
end

function CowPlayerInfoListLayer:myInit( _controll )
	self.m_root = nil
	self.m_lauout_bankbg = nil
	self.m_controll = _controll
	self.m_CurPage = 1
end

function CowPlayerInfoListLayer:setupViews()
    self.m_root = UIAdapter:createNode("cow_game_csb/cow_player_info_list_layer.csb")
    self:addChild(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
	
    self.m_lauout_bankbg = self.m_root:getChildByName("layout_bg")
  	self.m_layout_listview = self.m_lauout_bankbg:getChildByName("layout_listview")
end

function CowPlayerInfoListLayer:refreshPlayerInfoListLayer(_controll)
	self:setVisible(true)
	if self.m_PlayerScrollView then
		self.m_PlayerScrollView:removeFromParent()
		self.m_PlayerScrollView = nil
	end
    self.m_CurPage = 1

	self:updateCowPlayerInfoListLayer(_controll)
end

function CowPlayerInfoListLayer:updateCowPlayerInfoListLayer( _controll )
	self.m_controll = _controll
	self:updatePlayerInfoList(self.m_controll:getArrUserData() )
end
function CowPlayerInfoListLayer:updatePlayerInfoList( _arrUserArray )
	if _arrUserArray == nil then
		return
	end

	local xianjia = self.m_controll:getAllXianJiaInfo()
	if #xianjia == 0 then
		return
	end

	local modelData = self:getTableViewData(xianjia, self.m_CurPage)

    self.m_PlayerScrollView = ToolKit:createTableView(cc.size(630,380),cc.size(630, 108), modelData, handler(self, self.createItem))

    self.m_PlayerScrollView:addTo(self.m_root:getChildByName("player_tableview"))
end

function CowPlayerInfoListLayer:createItem(index)
	local node = nil
    node = CowPalyerInfoNode.new()
    node:setMainLayer(self)
    return node
end

function CowPlayerInfoListLayer:getTableViewData(modelData, page)
    local data = {}

    local num = 0

    if page*4*2 >= #modelData then
    	num = #modelData % 2 == 0 and #modelData /2 or  math.floor(#modelData / 2) + 1 
    else
    	num = page * 4
    end

    for i=1, num do
    	local cellData = {}
    	cellData.data = {}
    	cellData.size = cc.size(630, 108)
    	table.insert(data, cellData)
    end

    table.insert(data, {})

    return data
end

function CowPlayerInfoListLayer:refreshScrollView()
  	if self.m_PlayerScrollView then
	    local mmtData = self:getTableViewData(self.m_controll:getAllXianJiaInfo(), self.m_CurPage)
	    self.m_PlayerScrollView:setViewData(mmtData)
	    self.m_PlayerScrollView:reloadData()
	    self.m_PlayerScrollView:setItemMiddle((self.m_CurPage - 1)* 4  + 1)
   end
end

-- 点击事件回调
function CowPlayerInfoListLayer:onTouchCallback( sender )
    local name = sender:getName()
    print("CowPlayerInfoListLayer--->name = ",name)
    if name == "btn_close" then
    	self:setVisible(false)
    elseif name == "layout_bg" then
    	self:setVisible(false)
    end
end

return CowPlayerInfoListLayer