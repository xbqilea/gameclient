--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 高位0 方块，1梅花，2红桃，3黑桃 poker_spade8.png
-- poker_hearts4.png
CowCardConfig=
{
    [0x01]="nn_poker_fk_1.png",[0x02]="nn_poker_fk_2.png",[0x03]="nn_poker_fk_3.png",
    [0x04]="nn_poker_fk_4.png",[0x05]="nn_poker_fk_5.png",[0x06]="nn_poker_fk_6.png",
    [0x07]="nn_poker_fk_7.png",[0x08]="nn_poker_fk_8.png",[0x09]="nn_poker_fk_9.png",
    [0x0A]="nn_poker_fk_10.png",[0x0B]="nn_poker_fk_j.png",[0x0C]="nn_poker_fk_q.png",
    [0x0D]="nn_poker_fk_k.png",[0x11]="nn_poker_mh_1.png",[0x12]="nn_poker_mh_2.png",
    [0x13]="nn_poker_mh_3.png",[0x14]="nn_poker_mh_4.png",[0x15]="nn_poker_mh_5.png",
    [0x16]="nn_poker_mh_6.png",[0x17]="nn_poker_mh_7.png",[0x18]="nn_poker_mh_8.png",
    [0x19]="nn_poker_mh_9.png",[0x1A]="nn_poker_mh_10.png",[0x1B]="nn_poker_mh_j.png",
    [0x1C]="nn_poker_mh_q.png",[0x1D]="nn_poker_mh_k.png",[0x21]="nn_poker_hx_1.png",
    [0x22]="nn_poker_hx_2.png",[0x23]="nn_poker_hx_3.png",[0x24]="nn_poker_hx_4.png",
    [0x25]="nn_poker_hx_5.png",[0x26]="nn_poker_hx_6.png",[0x27]="nn_poker_hx_7.png",
    [0x28]="nn_poker_hx_8.png",[0x29]="nn_poker_hx_9.png",[0x2A]="nn_poker_hx_10.png",
    [0x2B]="nn_poker_hx_j.png",[0x2C]="nn_poker_hx_q.png",[0x2D]="nn_poker_hx_k.png",
    [0x31]="nn_poker_ht_1.png",[0x32]="nn_poker_ht_2.png",[0x33]="nn_poker_ht_3.png",
    [0x34]="nn_poker_ht_4.png",[0x35]="nn_poker_ht_5.png",[0x36]="nn_poker_ht_6.png",
    [0x37]="nn_poker_ht_7.png",[0x38]="nn_poker_ht_8.png",[0x39]="nn_poker_ht_9.png",
    [0x3A]="nn_poker_ht_10.png",[0x3B]="nn_poker_ht_j.png",[0x3C]="nn_poker_ht_q.png",
    [0x3D]="nn_poker_ht_k.png",[0x4E]="nn_poker_smallking.png",[0x4F]="nn_poker_bigkiing.png",
    [0x50]="nn_poker_back.png"
}
-- 变牌
CowCardConfig.m_bianPai=
{
    [0x01]="nn_poker_turn_fk_1.png",[0x02]="nn_poker_turn_fk_2.png",[0x03]="nn_poker_turn_fk_3.png",
    [0x04]="nn_poker_turn_fk_4.png",[0x05]="nn_poker_turn_fk_5.png",[0x06]="nn_poker_turn_fk_6.png",
    [0x07]="nn_poker_turn_fk_7.png",[0x08]="nn_poker_turn_fk_8.png",[0x09]="nn_poker_turn_fk_9.png",
    [0x0A]="nn_poker_turn_fk_10.png",[0x0B]="nn_poker_turn_fk_j.png",[0x0C]="nn_poker_turn_fk_q.png",
    [0x0D]="nn_poker_turn_fk_k.png",[0x11]="nn_poker_turn_mh_1.png",[0x12]="nn_poker_turn_mh_2.png",
    [0x13]="nn_poker_turn_mh_3.png",[0x14]="nn_poker_turn_mh_4.png",[0x15]="nn_poker_turn_mh_5.png",
    [0x16]="nn_poker_turn_mh_6.png",[0x17]="nn_poker_turn_mh_7.png",[0x18]="nn_poker_turn_mh_8.png",
    [0x19]="nn_poker_turn_mh_9.png",[0x1A]="nn_poker_turn_mh_10.png",[0x1B]="nn_poker_turn_mh_j.png",
    [0x1C]="nn_poker_turn_mh_q.png",[0x1D]="nn_poker_turn_mh_k.png",[0x21]="nn_poker_turn_hx_1.png",
    [0x22]="nn_poker_turn_hx_2.png",[0x23]="nn_poker_turn_hx_3.png",[0x24]="nn_poker_turn_hx_4.png",
    [0x25]="nn_poker_turn_hx_5.png",[0x26]="nn_poker_turn_hx_6.png",[0x27]="nn_poker_turn_hx_7.png",
    [0x28]="nn_poker_turn_hx_8.png",[0x29]="nn_poker_turn_hx_9.png",[0x2A]="nn_poker_turn_hx_10.png",
    [0x2B]="nn_poker_turn_hx_j.png",[0x2C]="nn_poker_turn_hx_q.png",[0x2D]="nn_poker_turn_hx_k.png",
    [0x31]="nn_poker_turn_ht_1.png",[0x32]="nn_poker_turn_ht_2.png",[0x33]="nn_poker_turn_ht_3.png",
    [0x34]="nn_poker_turn_ht_4.png",[0x35]="nn_poker_turn_ht_5.png",[0x36]="nn_poker_turn_ht_6.png",
    [0x37]="nn_poker_turn_ht_7.png",[0x38]="nn_poker_turn_ht_8.png",[0x39]="nn_poker_turn_ht_9.png",
    [0x3A]="nn_poker_turn_ht_10.png",[0x3B]="nn_poker_turn_ht_j.png",[0x3C]="nn_poker_turn_ht_q.png",
    [0x3D]="nn_poker_turn_ht_k.png",[0x4E]="nn_poker_turn_smallking.png",[0x4F]="nn_poker_turn_bigkiing.png",
}

-- 变牌
CowCardConfig.m_smallbianPai=
{
	[0x01]="nn_poker_num_black_1.png",[0x02]="nn_poker_num_black_2.png",[0x03]="nn_poker_num_black_3.png",
	[0x04]="nn_poker_num_black_4.png",[0x05]="nn_poker_num_black_5.png",[0x06]="nn_poker_num_black_6.png",
	[0x07]="nn_poker_num_black_7.png",[0x08]="nn_poker_num_black_8.png",[0x09]="nn_poker_num_black_9.png",
	[0x0A]="nn_poker_num_black_10.png",[0x0B]="nn_poker_num_black_j.png",[0x0C]="nn_poker_num_black_q.png",
	[0x0D]="nn_poker_num_black_k.png",[0x11]="nn_poker_num_black_1.png",[0x12]="nn_poker_num_black_2.png",
	[0x13]="nn_poker_num_black_3.png",[0x14]="nn_poker_num_black_4.png",[0x15]="nn_poker_num_black_5.png",
	[0x16]="nn_poker_num_black_6.png",[0x17]="nn_poker_num_black_7.png",[0x18]="nn_poker_num_black_8.png",
	[0x19]="nn_poker_num_black_9.png",[0x1A]="nn_poker_num_black_10.png",[0x1B]="nn_poker_num_black_j.png",
	[0x1C]="nn_poker_num_black_q.png",[0x1D]="nn_poker_num_black_k.png",[0x21]="nn_poker_num_black_1.png",
	[0x22]="nn_poker_num_black_2.png",[0x23]="nn_poker_num_black_3.png",[0x24]="nn_poker_num_black_4.png",
	[0x25]="nn_poker_num_black_5.png",[0x26]="nn_poker_num_black_6.png",[0x27]="nn_poker_num_black_7.png",
	[0x28]="nn_poker_num_black_8.png",[0x29]="nn_poker_num_black_9.png",[0x2A]="nn_poker_num_black_10.png",
	[0x2B]="nn_poker_num_black_j.png",[0x2C]="nn_poker_num_black_q.png",[0x2D]="nn_poker_num_black_k.png",
	[0x31]="nn_poker_num_black_1.png",[0x32]="nn_poker_num_black_2.png",[0x33]="nn_poker_num_black_3.png",
	[0x34]="nn_poker_num_black_4.png",[0x35]="nn_poker_num_black_5.png",[0x36]="nn_poker_num_black_6.png",
	[0x37]="nn_poker_num_black_7.png",[0x38]="nn_poker_num_black_8.png",[0x39]="nn_poker_num_black_9.png",
	[0x3A]="nn_poker_num_black_10.png",[0x3B]="nn_poker_num_black_j.png",[0x3C]="nn_poker_num_black_q.png",
	[0x3D]="nn_poker_num_black_k.png",
}

CowCardConfig.m_bigbianPai=
{
    [0x01]="nn_poker_num_red_1.png",[0x02]="nn_poker_num_red_2.png",[0x03]="nn_poker_num_red_3.png",
    [0x04]="nn_poker_num_red_4.png",[0x05]="nn_poker_num_red_5.png",[0x06]="nn_poker_num_red_6.png",
    [0x07]="nn_poker_num_red_7.png",[0x08]="nn_poker_num_red_8.png",[0x09]="nn_poker_num_red_9.png",
    [0x0A]="nn_poker_num_red_10.png",[0x0B]="nn_poker_num_red_j.png",[0x0C]="nn_poker_num_red_q.png",
    [0x0D]="nn_poker_num_red_k.png",[0x11]="nn_poker_num_red_1.png",[0x12]="nn_poker_num_red_2.png",
    [0x13]="nn_poker_num_red_3.png",[0x14]="nn_poker_num_red_4.png",[0x15]="nn_poker_num_red_5.png",
    [0x16]="nn_poker_num_red_6.png",[0x17]="nn_poker_num_red_7.png",[0x18]="nn_poker_num_red_8.png",
    [0x19]="nn_poker_num_red_9.png",[0x1A]="nn_poker_num_red_10.png",[0x1B]="nn_poker_num_red_j.png",
    [0x1C]="nn_poker_num_red_q.png",[0x1D]="nn_poker_num_red_k.png",[0x21]="nn_poker_num_red_1.png",
    [0x22]="nn_poker_num_red_2.png",[0x23]="nn_poker_num_red_3.png",[0x24]="nn_poker_num_red_4.png",
    [0x25]="nn_poker_num_red_5.png",[0x26]="nn_poker_num_red_6.png",[0x27]="nn_poker_num_red_7.png",
    [0x28]="nn_poker_num_red_8.png",[0x29]="nn_poker_num_red_9.png",[0x2A]="nn_poker_num_red_10.png",
    [0x2B]="nn_poker_num_red_j.png",[0x2C]="nn_poker_num_red_q.png",[0x2D]="nn_poker_num_red_k.png",
    [0x31]="nn_poker_num_red_1.png",[0x32]="nn_poker_num_red_2.png",[0x33]="nn_poker_num_red_3.png",
    [0x34]="nn_poker_num_red_4.png",[0x35]="nn_poker_num_red_5.png",[0x36]="nn_poker_num_red_6.png",
    [0x37]="nn_poker_num_red_7.png",[0x38]="nn_poker_num_red_8.png",[0x39]="nn_poker_num_red_9.png",
    [0x3A]="nn_poker_num_red_10.png",[0x3B]="nn_poker_num_red_j.png",[0x3C]="nn_poker_num_red_q.png",
    [0x3D]="nn_poker_num_red_k.png",
}
-- 变牌,大小王
CowCardConfig.m_BianPaiJock=
{
	[0x4E]="nn_poker_lai_smallking.png",[0x4F]="nn_poker_lai_bigkiing.png",
}

-- 根据牌的数据，获取牌的图片名字
function CowCardConfig:getCardFileName( CardItem )
	-- 10 转 16
	if CowCardConfig[CardItem] then
		-- print("CowCardConfig[CardItem] = ",CowCardConfig[CardItem])
		return CowCardConfig[CardItem]
	end
	-- CowDebug.printDlg("ERROR:CowCardConfig getCardFileName CardItem is nil " .. ( CardItem or "nil") )
	return nil
end


-- 根据牌的数据，获取变牌的图片名字
function CowCardConfig:getBianCardFileName( CardItem )
	-- 10 转 16
	if CowCardConfig.m_bianPai[CardItem] then
		-- print("CowCardConfig[CardItem] = ",CowCardConfig.m_bianPai[CardItem])
		return CowCardConfig.m_bianPai[CardItem]
	end
	-- CowDebug.printDlg("ERROR:CowCardConfig getBianCardFileName CardItem is nil " .. ( CardItem or "nil") )
	return nil
end

function CowCardConfig:getBianPaiJockFileName( CardItem )
	-- 10 转 16
	if CowCardConfig.m_BianPaiJock[CardItem] then
		-- print("CowCardConfig[CardItem] = ",CowCardConfig.m_BianPaiJock[CardItem])
		return CowCardConfig.m_BianPaiJock[CardItem]
	end
	-- CowDebug.printDlg("ERROR:CowCardConfig getBianPaiJockFileName CardItem is nil " .. ( CardItem or "nil") )
	return nil
end

function CowCardConfig:getBianPaisamllJockFileName( CardItem,_king )
	if _king == 0x4E then
		if CowCardConfig.m_smallbianPai[CardItem] then
			-- print("CowCardConfig[CardItem] = ",CowCardConfig.m_smallbianPai[CardItem])
			return CowCardConfig.m_smallbianPai[CardItem]
		end
	else
		if CowCardConfig.m_bigbianPai[CardItem] then
			print("CowCardConfig[CardItem] = ",CowCardConfig.m_bigbianPai[CardItem])
			return CowCardConfig.m_bigbianPai[CardItem]
		end
	end
	-- CowDebug.printDlg("ERROR:CowCardConfig getBianPaisamllJockFileName CardItem is nil " .. ( CardItem or "nil") )
	return nil
end

return CowCardConfig