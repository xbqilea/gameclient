--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 疯狂牛牛主场景
local HoxDataMgr                    = import("src.app.game.cattle.src.manager.HoxDataMgr")
local CCGameSceneBase = require("src.app.game.common.main.CCGameSceneBase") 
local scheduler = require("framework.scheduler")  
--local CattleBankApplyLayer = require("src.app.game.cattle.src.crazycattle.src.view.CattleBankApplyLayer")
local DlgAlert = require("app.hall.base.ui.MessageBox")
local HandredcattleLayer        = require("src.app.game.cattle.src.layer.HandredcattleLayer")  
local HandredcattleLoadingLayer	    = require("src.app.game.cattle.src.layer.HandredcattleLoadingLayer")  
local CowGameScene = class("CowGameScene", function ()
	return CCGameSceneBase.new()
end)

function CowGameScene:ctor() -- __baseData
    -- ToolKit:setGameFPS(1/60.0)
	print("----------------- CowGameScene:ctor ------------")
    

   -- addMsgCallBack(self, Handredcattle_Events.MSG_SHOW_BANKINFO, handler(self, self.showBankInfoLayer) )-- 显示庄家列表
      self.m_HandredcattleLoadingLayer = HandredcattleLoadingLayer.new()
    self:addChild(self.m_HandredcattleLoadingLayer) 
    addMsgCallBack(self, MSG_ENTER_FOREGROUND, handler(self, self.onEnterForeground)) -- 转前台
    addMsgCallBack(self, MSG_ENTER_BACKGROUND, handler(self, self.onEnterBackground)) -- 转后台
    self:registBackClickHandler(handler(self, self.onBackButtonClicked)) -- Android & Windows注册返回按钮
    ToolKit:registDistructor(self, handler(self, self.onDestory)) -- 注册析构函数 
    --addMsgCallBack(self, MSG_RECONNECT_LOBBY_SERVER, handler(self, self.onLobbyReconnect))
    addMsgCallBack(self,UPDATE_GAME_RESOURCE, handler(self,self.updateCallback))
end
 
  function CowGameScene:updateCallback()
   self.m_HandredcattleLayer = HandredcattleLayer.new()
    self:addChild(self.m_HandredcattleLayer)
 end
 
--------------------- 弹出窗 --------------------

function CowGameScene:showPlayerInfoListLayer()
    ---显示牛牛闲家列表
    if self.m_cowPlayerInfoListLayer == nil then
        self.m_cowPlayerInfoListLayer = CowPlayerInfoListLayer.new(g_GameController) --g_GameController
        self.m_cowDialogBase[#self.m_cowDialogBase+1] = self.m_cowPlayerInfoListLayer
        self:addChild(self.m_cowPlayerInfoListLayer,3)
        self.m_cowPlayerInfoListLayer:updateCowPlayerInfoListLayer( g_GameController )
    else
        self.m_cowPlayerInfoListLayer:refreshPlayerInfoListLayer(g_GameController)
    end
end

function CowGameScene:showCowDlgTipLayer( _visible,_message,_handler,_params ) 
    local params = { 
        message = _message -- todo: 换行 
    }
    local dlg = require("app.hall.base.ui.MessageBox").new()
    local _leftCallback = function ()       
        self:askExitGame()
    end

    local _rightcallback = function () 
                                  
    end
     
    dlg:TowSubmitAlert(params, _leftCallback, _rightcallback)
    dlg:showDialog()
end

function CowGameScene:showSignleDlg( _message )
	local dlg = DlgAlert.showTipsAlert({title = "提示", tip = _message})
    dlg:setSingleBtn("确定", function ()
		self:StrongbackGame()
    end)
    dlg:setBackBtnEnable(false)
    dlg:enableTouch(false)
end
 
 
 

function CowGameScene:userExitGameReq()
    g_GameController:userExitGameReq()
end

-- 弹出框
function CowGameScene:showAskExitGameTip()
    local message = ""
    local maxScore = 0
    local relust =HoxDataMgr.getInstance():getGameStatus()~=  HoxDataMgr.eGAME_STATUS_WAIT --and g_GameController:getGameState() ~= CowDef.m_gameState.END and g_GameController:getGameState() ~= CowDef.m_gameState.EndFree
    -- print("g_GameController:getGameState() = ",g_GameController:getGameState())
    if HoxDataMgr.getInstance():getUsrChipCount() > 0 and relust then -- 自己有下注
     --   maxScore = CowRoomDef:scoreNumberCell( self.m_selfAddScore*0.1 )
        message = "强退将暂时扣除" .. math.floor(HoxDataMgr.getInstance():getAllUsrChip()*10) .."金币，用于本局结算，结算后自动返还剩余金币，是否退出？"
        self:showCowDlgTipLayer( true,message,handler(self, self.askExitGame) ) -- askExitGame
    elseif HoxDataMgr.getInstance():isBanker() and relust then-- 自己是庄家，有玩家下注
      --  maxScore = CowRoomDef:scoreNumberCell(g_GameController:getMyBankerCoin()*0.01)
        message = "强退将暂时扣除" .. math.floor(HoxDataMgr.getInstance():getMyBankScore()) .."金币，用于本局结算，结算后自动返还剩余金币，是否退出？"
        self:showCowDlgTipLayer( true,message,handler(self, self.askExitGame) ) -- askExitGame
    else
        self:askExitGame()
    end
    
end

function CowGameScene:showAskGotoChongZhiTip()
    local allScore = g_GameController:getApplyScore()
    local str = "申请庄家需要" .. allScore .. "金币，" .."当前金币不足，是否前往购买？"
    self:showCowDlgTipLayer( true,str,handler(self, self.gotoChongZhi) )
end

function CowGameScene:showServiceErrorNtyTip()
    self:showCowDlgTipLayer( true,"服务器内部错误",handler(self, self.StrongbackGame),{m_signle = true,m_yes="确定",m_close = true,m_touch = true} )
end

function CowGameScene:sendExitGameReq()
    print("CowGameScene:sendExitGameReq")
    g_GameController:sendExitGameReq()
end
 

-- 强退出游戏
function CowGameScene:StrongbackGame()
    -- g_GameController:onDestory()
    print("CowGameScene:StrongbackGame")
    --self:exitGame()
    self.m_HandredcattleLayer:onMoveExitView()
	g_GameController:releaseInstance()
end

function CowGameScene:getSceneName()
    return "CowGameScene"
end

function CowGameScene:askExitGame()-- 是否退出游戏
    if g_GameController then
        if ConnectManager:isConnectGameSvr( g_GameController:getGameAtomTypeId() ) then
            g_GameController:userExitGameReq()
        else
            self:StrongbackGame()
        end
    else
        self:StrongbackGame()
    end
end
 

function CowGameScene:updateBankInfoListState()
    if self.m_cowBankInfoListLayer then
        self.m_cowBankInfoListLayer:setReqXiazhuang(false)
    end

    if self.m_cowPokerLayer then
        self.m_cowPokerLayer:setReqXiazhuang(false)
    end
end
 

---------------- 注册事件 -----------------------------
function CowGameScene:onBackButtonClicked()
    -- CowSound:getInstance():playEffect("OxButton")
        --监听手机返回键
    local result = self:hideCowDialogBase()
    if result == false then --弹出退出游戏提示框
        print("退出游戏")
        self:showAskExitGameTip()
    end
end

function CowGameScene:hideCowDialogBase()
    for i=1,#self.m_cowDialogBase do
        if self.m_cowDialogBase[i]:isVisible() then
            if self.m_cowDialogBase[i].m_mustChoose == true then
                return true
            else
                self.m_cowDialogBase[i]:hideCowDialogBase(false)
                return true
            end
            
        end
    end
    return false
end

function CowGameScene:onDestory()
	AudioManager:getInstance():stopAllSounds()
    AudioManager:getInstance():stopMusic()
end 

---------------------------- 游戏转后台 -------------------------------
function CowGameScene:onEnterForeground()
    print("CowGameScene onEnterForeground") 
end

function CowGameScene:onEnterBackground()
    --self.m_onEnterForeground = false
    g_GameController.m_BackGroudFlag = true
    print("CowGameScene onEnterBackground")
end

--[[
function CowGameScene:getOnEnterForeground()
    return self.m_onEnterForeground
end
--]]
---------------------------- 退出游戏 -------------------------------
--[[
function CowGameScene:exitGame()
    self:onExitGame()  -- posScene
end
--]]

function CowGameScene:onCleanup()
    print("CowGameScene:onCleanup")
end

function CowGameScene:onEnter() 
	print("------------CowGameScene:onEnter end------------")
end

function CowGameScene:onExit()
    print("------------CowGameScene:onExit begin------------") 
    removeMsgCallBack(self, MSG_ENTER_FOREGROUND)
    removeMsgCallBack(self, MSG_ENTER_BACKGROUND) 
    --removeMsgCallBack(self, MSG_RECONNECT_LOBBY_SERVER)
    removeMsgCallBack(self,UPDATE_GAME_RESOURCE)
end
 
 

return CowGameScene