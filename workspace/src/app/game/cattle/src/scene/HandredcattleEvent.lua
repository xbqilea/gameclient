--region *.lua
--Date 2017-2-25
--Author xufei
--此文件由[BabeLua]插件自动生成

local prefixFlag = "Handredcattle_Event_"

local Handredcattle_Events =
{
    MSG_HOX_UPDATE_BANKER_INFO      = prefixFlag .. "1", --更新庄家数据
    MSG_HOX_SCENE_UPDATE            = prefixFlag .. "2", --更新庄家数据
    MSG_HOX_UDT_GAME_STATUE         = prefixFlag .. "3", --更新游戏状态
    MSG_HOX_GAME_END                = prefixFlag .. "4", --开始比牌
    MSG_HOX_UPDATE_TABLE_INFO       = prefixFlag .. "5", --更新桌子信息
    MSG_GAME_SHIP                   = prefixFlag .. "6", --投注成功
    MSG_GAME_CONTINUE_SUCCESS       = prefixFlag .. "7", --续投成功
    MSG_HOX_REQUEST_BANKER_INFO     = prefixFlag .. "8", --上庄申请、取消
    MSG_UPDATE_ROLL_MSG             = prefixFlag .. "9", --系统消息
    MSG_HOX_UPDATE_HISTORY_INFO     = prefixFlag .. "13",--更新历史数据
    MSG_COW_BET_XIAZHU = "Handredcattle_Events.MSG_COW_BET_XIAZHU", --下注消息
    MSG_COW_CHAGE_CHIP = "Handredcattle_Events.MSG_COW_CHAGE_CHIP", --切换筹码消息
    MSG_COW_XIAOJI_TIP_OVER  = "Handredcattle_Events.MSG_COW_XIAOJI_TIP_OVER", --小计往上飘动画完成
    MSG_COW_OPENCARD_OVER  = "Handredcattle_Events.MSG_COW_OPENCARD_OVER", --开牌动画完成（一桌的）
    MSG_COW_EXITGAME_REQ  = "Handredcattle_Events.MSG_COW_EXITGAME_REQ", --请求离开桌子
    MSG_COW_SHANGZHUANG_REQ  = "Handredcattle_Events.MSG_COW_SHANGZHUANG_REQ", --请求上庄
    MSG_COW_XIAZHUANG_REQ  = "Handredcattle_Events.MSG_COW_XIAZHUANG_REQ", --请求下庄
    MSG_UPDATE_APPLYARRAY_NTY  = "Handredcattle_Events.MSG_UPDATE_APPLYARRAY_NTY", --刷新申请庄家队列通知
    MSG_CLOSE  = "Handredcattle_Events.MSG_CLOSE", --关闭
    MSG_SHOW_BANKINFO  = "Handredcattle_Events.MSG_SHOW_BANKINFO", --显示庄家列表
    MSG_COW_CONTINUE_XIAZHU = "Handredcattle_Events.MSG_COW_CONTINUE_XIAZHU", --显示庄家列表
    MSG_SHOW_XIANJIA_XIAOJI = "Handredcattle_Events.MSG_SHOW_XIANJIA_XIAOJI", -- 庄家翻完牌，提示闲家显示小计 
    ONLINE_LIST = "ONLINE_LIST",--在线玩家列表
    MSG_COW_ONLINE_REQ = "MSG_COW_ONLINE_REQ"--在线玩家列表请求
}   

return Handredcattle_Events

--endregion
