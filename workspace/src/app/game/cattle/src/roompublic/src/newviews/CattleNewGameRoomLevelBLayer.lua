--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 二级游戏房间(疯狂牛牛，彩金牛牛，抢庄牛牛，牌友房)

local CowShareInfoLayer = require("app.game.cattle.src.roompublic.src.common.CattleShareInfoLayer")
local CowItemGameSmallLevelB = require("app.game.cattle.src.roompublic.src.newviews.CattleItemGameSmallLevelB")
local CowNewItemGameLevelB = require("app.game.cattle.src.roompublic.src.newviews.CattleNewItemGameLevelB")
local CowGuideLayer_1 = require("app.game.cattle.src.roompublic.src.function.cattleguide.CattleGuideLayer_1")
local CowFriendCreate = require("app.game.cattle.src.friendcattle.src.view.CowFriendCreate")
local scheduler = require("framework.scheduler")
local FriendCowInputRoomIDLayer = require("app.game.cattle.src.friendcattle.src.view.FriendCowInputRoomIDLayer")

local CowNewGameRoomLevelBLayer = class("CowNewGameRoomLevelBLayer", function ()
    return CowShareInfoLayer.new()
end)

function CowNewGameRoomLevelBLayer:ctor(__params)
    self:myInit( __params )
    self:setupViews(__params)

    -- 新手任务
    TotalController:registerNetMsgCallback(self, Protocol.SceneServer, "CS_H2C_HandleMsg_Ack", handler(self, self.sceneNetMsgHandler))
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    self:addNodeEventListener(cc.NODE_EVENT, function(event)
        if event.name == "enter" then
            self:onEnter()
        elseif event.name == "exit" then
            -- self:onExit()
        elseif event.name == "enterTransitionFinish" then
            -- self:onEnterTransitionFinish()
        elseif event.name == "exitTransitionStart" then
            -- self:onExitTransitionStart()
        elseif event.name == "cleanup" then
            -- self:onDestory()
        end
    end)
    self:setLayerName("CowNewGameRoomLevelBLayer")
end

-- 初始化成员变量
function CowNewGameRoomLevelBLayer:myInit( __params )
    self.m_portalId = nil -- 游戏类型id(来自配表, 只用于显示)
    self.m_enterItemb = nil
    self.m_root = nil
    self.m_layout_bg = nil
    self.m_offsetX = 0 -- 一个B级按钮的间隔
    self.m_width = 0 -- 一个B级按钮的宽
    self.m_layoutPosX = {}
    self.m_layout = {}
    self.m_friendGame = {}
    self.m_netMsgHandlerSwitch = {} -- 消息集合
    self.m_roomCfg = {} -- 房间配置
    self.m_tableId = 0
    self.m_friendGameId = nil -- 牌友房ID
    -- self:initData(__params)
    self:initMsgHandler()
end

--结束定时器
function CowNewGameRoomLevelBLayer:endLastTimer()
    if self.m_lastTimer then
        scheduler.unscheduleGlobal(self.m_lastTimer)
        self.m_lastTimer = nil
    end
end

function CowNewGameRoomLevelBLayer:onEnter()
    print("CowNewGameRoomLevelBLayer:onEnter")
    self:sendCS_C2S_OxSelScene_Last_EnterRoom_Req()
    --self:sendGetRoomCfg()
    self:sendCS_C2M_OxFriend_UserInRoom_Req()
end

function CowNewGameRoomLevelBLayer:onEndAnimation()
    function delayerTime()
        self:endLastTimer()
        ConnectManager:send2SceneServer( CowRoomDef.GUIDE,"CS_C2S_OxSelScene_XinShou_Req")
    end
    if self.m_lastTimer == nil then
        self.m_lastTimer = scheduler.scheduleGlobal(delayerTime,0.1)
    end
end

function CowNewGameRoomLevelBLayer:onPushAnimation( __finishCallback )
    CowUIHelp:getInstance():showRoomPushAction(self.m_layout_bg, __finishCallback )
end

function CowNewGameRoomLevelBLayer:onPopAnimation( __finishCallback  )
    CowUIHelp:getInstance():showRoomPopAction(self.m_layout_bg, __finishCallback )
end

function CowNewGameRoomLevelBLayer:layoutAni()
    self.m_layout_bg:setVisible(true)
    local action = cc.ScaleTo:create(0.15,1)
    self.m_layout_bg:setScale(0.5)
    self.m_layout_bg:runAction(action)
end

function CowNewGameRoomLevelBLayer:showCowGuide()
    if g_hasEndGuide == false then
        g_hasEndGuide = true
        CowGuideLayer_1.new():showGuide()
    end
end
-- 初始化界面
function CowNewGameRoomLevelBLayer:setupViews(__params)
    self.m_root = UIAdapter:createNode( "cow_publicroom_csb/cow_new_room_level_b_layer.csb")
    self:setLayoutContent(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    self.m_layout_bg = self.m_root:getChildByName("layout_main")
    self:setHideNode( self.m_layout_bg )
    -- 疯狂牛牛，彩金牛牛，抢庄牛牛
    self:updategameDataNty(__params)
end

-- 刷新界面
function CowNewGameRoomLevelBLayer:initData( __params )
    self.m_games = {}
    self.m_layout = {}
    self.m_friendGame = {}
    self.m_itemSamllLevelB = {}

    local data = fromFunction(__params._info.m_portalId)

    self:setProtalId(__params._info.m_portalId )
    self:setPreviousProtalId(data.father )
    for k, v in pairs(__params._info.m_portalList) do
        local data = RoomData:getGameDataById(v.m_portalId)
        if data then 
            if CowToolKit:isFriendCowGameType(data.gameType) then
                -- 获取牌友房房间ID
                local roomsetingdata = RoomData:getChildrenByGameId(data.id)
                self.m_friendGame[#self.m_friendGame+1] = roomsetingdata[1].funcId
            else
                self.m_games[#self.m_games+1] = data
            end
        end
    end
end

function CowNewGameRoomLevelBLayer:updategameDataNty( __params )

    --dump(__params, "牛牛配置信息:", 10)

    for i=3,5 do
        if self.m_layout[i] then
            self.m_layout[i]:removeAllChildren()
        end 
    end
    self:initData(__params)

    if not self.m_friendGame[1] then  -- 没有开牌友房
        for i=1,2 do
            if self.m_layout[i] then
                self.m_layout[i]:removeAllChildren()
            end 
        end
        self.m_enterItemb = nil
    end
    
    table.sort(self.m_games, function(a, b)
        return a.id > b.id
    end)
    
    -- 其他三个游戏
    for i=3,5 do
        self.m_layout[i] = self.m_layout_bg:getChildByName("layout_"..i)
        if self.m_games[i-2] then
            local child = self.m_layout[i]:getChildByTag(123456)
            if  child then
                child:reSetViews(self.m_games[i-2])
            else
                self.m_itemSamllLevelB[i-2] = CowItemGameSmallLevelB.new(self.m_games[i-2])
                self.m_itemSamllLevelB[i-2]:setTag(123456)
                self.m_layout[i]:addChild(self.m_itemSamllLevelB[i-2])
            end
        end
    end

    -- 牌友房
    if self.m_friendGame[1] then

        if not self.m_enterItemb then
            local create = CowRoomDef.FRIENF_INFO_Type.CREATE
            for i=1,2 do
                self.m_layout[i] = self.m_layout_bg:getChildByName("layout_"..i)
                -- 创建 and 加入
                local chuangjian = CowNewItemGameLevelB.new(self.m_friendGame[1],create,self)
                self.m_layout[i]:addChild(chuangjian)
                chuangjian:setTag(create)
                chuangjian:setPositionX(self.m_layout[i]:getContentSize().width*0.5)
                chuangjian:setPositionY(self.m_layout[i]:getContentSize().height*0.5)
                if create ==  CowRoomDef.FRIENF_INFO_Type.ENTER then
                    self.m_enterItemb = chuangjian
                end
                create =  CowRoomDef.FRIENF_INFO_Type.ENTER

            end
        end
    end
end

function CowNewGameRoomLevelBLayer:updatePos()

end

function CowNewGameRoomLevelBLayer:showCreateRoomLayer()
        ------------------------------------------------
        -- self:checkWx(
    --     function ()
    --         -- 弹出创建房间界面
        if FriendCowPlayerData:getInstance():getRoomCfgData() ~= nil then
            --弹出创建房间界面
            local froom = CowFriendCreate.new(self)
            froom:showDialog()
            froom:setConfig(FriendCowPlayerData:getInstance():getRoomCfgData())
        else
            TOAST("木有收到创房配置")
        end
    --     end)
end
----------------- get set -----------------
function CowNewGameRoomLevelBLayer:getRoomCfg()
    return self.m_roomCfg
end
------------------ 消息处理 ---------------------
function CowNewGameRoomLevelBLayer:initMsgHandler()
    self.m_netMsgHandlerSwitch[ "CS_S2C_OxSelScene_XinShou_Ack"]   =  handler(self,self.newPlayerGuide)
    self.m_netMsgHandlerSwitch[ "CS_S2C_OxSelScene_Last_EnterRoom_Ack"]   =  handler(self,self.last_enterroom_ack)
    self.m_netMsgHandlerSwitch[ "CS_M2C_OxFriend_GetRoomCfg_Ack"]   =  handler(self,self.cs_m2c_oxfriend_getroomcfg_ack)
    self.m_netMsgHandlerSwitch[ "CS_M2C_OxFriend_CreateRoom_Ack"]   =  handler(self,self.cs_m2c_oxfriend_createroom_ack)
    self.m_netMsgHandlerSwitch[ "CS_M2C_OxFriend_EnterRoom_Ack"]   =  handler(self,self.cs_m2c_oxfriend_enterroom_ack)
    self.m_netMsgHandlerSwitch[ "CS_M2C_OxFriend_UserInRoom_Ack"]   =  handler(self,self.cs_m2c_oxfriend_userinroom_ack)

end
-- 请求上一次玩过的游戏ID
function CowNewGameRoomLevelBLayer:sendCS_C2S_OxSelScene_Last_EnterRoom_Req()
    ConnectManager:send2SceneServer( CowRoomDef.GUIDE,"CS_C2S_OxSelScene_Last_EnterRoom_Req",nil)
end
-- 请求玩家是否在牌友房
function CowNewGameRoomLevelBLayer:sendCS_C2M_OxFriend_UserInRoom_Req()
    ConnectManager:send2SceneServer( CowRoomDef.FriendCow,"CS_C2M_OxFriend_UserInRoom_Req")
end
-- 进入游戏
function CowNewGameRoomLevelBLayer:sendEnterGame()
    if self.m_friendGame[1]  then
        RoomTotalController:getInstance():reqEnterScene( self.m_friendGame[1]) --self.m_tableId
    end
end
-- 进入房间
function CowNewGameRoomLevelBLayer:sendEnterRoom( _roomID )
    if _roomID then
        ToolKit:addLoadingDialog(10, "正在进入房间...")
        ConnectManager:send2SceneServer( CowRoomDef.FriendCow,"CS_C2M_OxFriend_EnterRoom_Req",{tostring(_roomID)})
    end
end

-- 获取牌友房配置，进界面就获取
function CowNewGameRoomLevelBLayer:sendGetRoomCfg()
    ConnectManager:send2SceneServer( CowRoomDef.FriendCow,"CS_C2M_OxFriend_GetRoomCfg_Req") 
end
-- 创建房间
function CowNewGameRoomLevelBLayer:sendCreateRoom( _data )
---------------------------------
    local m_gameType = _data.m_gameType
    local m_gameRounds = _data.m_gameRounds
    local m_maxApplyTimes = _data.m_maxApplyTimes
    local m_maxAddTimes = _data.m_maxAddTimes
    local m_multiCheck = _data.m_multiCheck
    ConnectManager:send2SceneServer( CowRoomDef.FriendCow,"CS_C2M_OxFriend_CreateRoom_Req",
        {m_gameType,m_gameRounds,m_maxApplyTimes,m_maxAddTimes,m_multiCheck})
end
-- 加入房间
function CowNewGameRoomLevelBLayer:enterFriendRoom()
    -- self:checkWx(
        -- function ()
            self.m_InputRoomview = FriendCowInputRoomIDLayer.new(self)
            self.m_InputRoomview:showDialog()
        -- end
    -- )
end

-- 返回房间
function CowNewGameRoomLevelBLayer:backFriendRoom()
    self:sendEnterGame()
end

function CowNewGameRoomLevelBLayer:sceneNetMsgHandler( __idStr, __info )
    if __idStr == "CS_H2C_HandleMsg_Ack" then
        --ToolKit:removeLoadingDialog()
        if __info.m_result == 0 then
                local gameAtomTypeId = __info.m_gameAtomTypeId
                if __info.m_message  and __info.m_message[1] then
                    local cmdId = __info.m_message[1].id
                    local info = __info.m_message[1].msgs
                    if cmdId and info then  -- 0未引导，1完成
                        if self.m_netMsgHandlerSwitch[cmdId] then
                            (self.m_netMsgHandlerSwitch[cmdId])( info )
                        end
                    end
                end
        else
            -- ToolKit:showErrorTip(__info.m_result)
        end
    end
end

function CowNewGameRoomLevelBLayer:newPlayerGuide( _info )
    if _info.m_result == 0 then
        g_hasEndGuide = false
    else
        g_hasEndGuide = true
    end
    if  g_hasEndGuide == false then
        g_hasEndGuide = true
        CowGuideLayer_1.new():showGuide()
    end
end

function CowNewGameRoomLevelBLayer:updateEnterItemBName(_back )
    if self.m_enterItemb then
        self.m_enterItemb:updateSp_name(_back)
    end
end
function CowNewGameRoomLevelBLayer:cs_m2c_oxfriend_userinroom_ack( _info )
    dump(_info,"cs_m2c_oxfriend_userinroom_ack")
    self:updateEnterItemBName( _info.m_result == 1 )
end
function CowNewGameRoomLevelBLayer:last_enterroom_ack( _info )
    g_cowLastEnterRoomID = _info.m_roomId
end
-- 房间配置返回
function CowNewGameRoomLevelBLayer:cs_m2c_oxfriend_getroomcfg_ack( _info )
    dump(_info,"getRoomCfg_Ack")
    self.m_roomCfg = _info
    FriendCowPlayerData:getInstance():setRoomCfgData(_info)
end
-- 创建房间返回
function CowNewGameRoomLevelBLayer:cs_m2c_oxfriend_createroom_ack( _info )
    dump(_info,"createroom_ack")
    if _info.m_result == 0 then
        self.m_createroominfo = _info
        if bit.band(_info.m_multiCheck, FriendCowDef.CreateEnum.OX_FRIEND_CREATE_FOR_OTHERS) == 0 then -- 为自己
            FriendCowPlayerData:getInstance():setFriendCowSelfISCreateRoom(true)
            self:sendEnterGame()
        else -- 为他人创建
            TOAST("帮他人创建牌友房成功！")
            -- 关闭创建界面
        end
    else
        CowToolKit:showErrorTip(_info.m_result)
        -- TOAST("创建牌友房失败！")
    end
end

function CowNewGameRoomLevelBLayer:cs_m2c_oxfriend_enterroom_ack( _info )
    dump(_info,"enterroom_ack")
    if _info.m_result == 0 then
        if not tolua.isnull(self.m_InputRoomview) then
            self.m_InputRoomview:closeDialog()
            self.m_InputRoomview = nil
        end
        self:sendEnterGame()
    else
        if not tolua.isnull(self.m_InputRoomview) then
            TOAST("进入房间失败！")
            self.m_InputRoomview:reSetRoomID()
        end
    end
end
-------------------- 触摸 --------------------------
function CowNewGameRoomLevelBLayer:onTouchCallback( sender )
    local name = sender:getName()

    if name == "btn_back" then
        print("btn_back")
    end
end

---------------- 判断微信绑定 ---------------------
-- 检测微信
--[[
function CowNewGameRoomLevelBLayer:checkWx(callback)
    if ToolKit:checkBindWx() then
        callback()
    else
        local function bindBack(code)
            if code == 0 then
                callback()
            elseif code == 1 then
                local params = {
                    m_title = "温馨提示",
                    m_message = "检测到此微信已有绑定账号，是否使用此微信号登录",
                    m_btnyes = "是" ,
                    m_btnno = "否" ,
                }
                local _logincallback = function ()
                    ToolKit:loginByWx( function(m_code) if m_code== 0 then callback() end end )
                end
                local dlg = require("app.game.cattle.src.roompublic.src.common.CowDoubleDlgLayer").new(params, _logincallback)
                dlg:showDialog()
            end
        end 
        local params = {
                    m_title = "温馨提示",
                    m_message = "为了您更好的体验牌友房，请绑定微信",
                    m_btnyes = "打开微信" ,
                    m_btnno = "取消" ,
                }

        local bindingcallback = function ()
            ToolKit:bindingWx( bindBack)
        end
        local dlg = require("app.game.cattle.src.roompublic.src.common.CattleDoubleDlgLayer").new(params, bindingcallback)
        dlg:showDialog()
    end
      
end
--]]

function CowNewGameRoomLevelBLayer:onDestory()
    -- 注销消息
    self:endLastTimer()
    print("CowNewGameRoomLevelBLayer:onDestory()")
    TotalController:removeNetMsgCallback(self, Protocol.SceneServer, "CS_H2C_HandleMsg_Ack")
end

return CowNewGameRoomLevelBLayer
