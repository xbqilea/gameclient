--
-- Author: 
-- Date: 2018-08-07 18:17:10
--
module(..., package.seeall)
TableName = "CowGameResources"
CowGameResources = {
	[1] = "cow_game_img/p_cow_gamescene.plist",
	[2] = "cow_game_img/p_cow_poker.plist",
	[3] = "cow_game_img/p_cow_naizi.plist",
	[4] = "happycattle/res/cow_game_img/p_huashigame.plist",	-- 花式
	[5] = "cow_game_img/p_cow_account.plist",
	[6] = "cow_game_img/p_cow_newChouma.plist",
}