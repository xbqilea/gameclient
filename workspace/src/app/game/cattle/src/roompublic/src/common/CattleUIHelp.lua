--
-- Author: 
-- Date: 2018-08-07 18:17:10
--
CowUIHelp = class("CowUIHelp")
function CowUIHelp:ctor()
	
end
CowUIHelp.instance = nil
function CowUIHelp:getInstance()
	if CowUIHelp.instance == nil then
		CowUIHelp.instance = CowUIHelp.new()
	end
	return CowUIHelp.instance
end
-- 切进动画
function CowUIHelp:showRoomPushAction( _layoutRoom, _callback)
	
    -- _layoutRoom:setAnchorPoint(0.5, 0.5)
    _layoutRoom:setScale(0.5)
    _layoutRoom:setVisible(true)
    _layoutRoom:setOpacity(0)
    -- local delay = cc.DelayTime:create(0.05)
    local action1 = cc.ScaleTo:create(0.15, 1)
    local fadeIn = cc.FadeIn:create(0.15)
    local function funcnameCallback()
        if _callback then
            _callback()
        end
    end
    local actionCallFunc = cc.CallFunc:create(funcnameCallback)
    local swan = cc.Spawn:create(action1,fadeIn)
    local action = cc.Sequence:create(swan, actionCallFunc)
   _layoutRoom:runAction(action)
end

function CowUIHelp:showRoomPopAction( _layoutRoom, _callback)
	_layoutRoom:setOpacity(255)        
    local action1 = cc.FadeTo:create(0.2, 0)
    -- _layoutRoom:setScale(0.5)
    -- local action1 = cc.ScaleTo:create(0.15, 1, 1)
    local function funcnameCallback()
        if _callback then
            _callback()
        end
    end
    local actionCallFunc = cc.CallFunc:create(funcnameCallback)
    local action = cc.Sequence:create(action1, actionCallFunc)
   _layoutRoom:runAction(action)
end