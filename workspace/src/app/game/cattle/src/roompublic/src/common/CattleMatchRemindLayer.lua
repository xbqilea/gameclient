-- Author: 
-- Date: 2018-08-07 18:17:10
-- CowMatchRemindLayer
-- 比赛冲突确认框
local scheduler = require("framework.scheduler")
local XbDialog = require("app.hall.base.ui.CommonView")

local CowMatchRemindLayer = class("CowMatchRemindLayer", function()
    return XbDialog.new()
end)
CowMatchRemindLayer.showTime = 10 -- 倒计时10秒
function CowMatchRemindLayer:ctor( _param,scene)
	self:myInit( scene )
	self.m_params = _param
	self:setupViews()   
	self:showDlgTip( _param ) 
    self:setBackBtnEnable(false)
    self:enableTouch(false)
        ToolKit:registDistructor( self, handler(self, self.onDestory) )
end

function CowMatchRemindLayer:myInit( scene )
	self.m_root = nil
	self.m_layout_bg = nil
	self.m_txt_tip = nil
	self.m_handleryes = nil
	self.m_handlerno = nil
	self.m_message = nil
	self.m_btn_close = nil 
	self.m_btn_no = nil
	self.m_btn_yes = nil
	self.m_txt_title = nil
	self.m_scene = scene
	self.m_showMessage = ""
	self.m_showTime = 0
	self.m_size = cc.size(550,180)
end

function CowMatchRemindLayer:showCloseBtn()
	self.m_btn_close:setVisible(true)
end

function CowMatchRemindLayer:setupViews()
    self.m_root = UIAdapter:createNode("cow_publicroom_csb/cow_public_JoinMatchConfirmLayer_dlg.csb")
    self:addChild(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    self.m_layout_bg = self.m_root:getChildByName("layout_bg")
    self.m_txt_tip = self.m_layout_bg:getChildByName("txt_tip")
    self.m_btn_close = self.m_layout_bg:getChildByName("btn_close")
	self.m_btn_no = self.m_layout_bg:getChildByName("btn_no")
	self.m_btn_yes = self.m_layout_bg:getChildByName("btn_yes")
	self.m_txt_title = self.m_layout_bg:getChildByName("txt_title")
	self.m_btn_close:setVisible(false)
    
    self:setEnableOutline()
    -- self:initBtnEvent(_param)
end

function CowMatchRemindLayer:showDlgTip( _param )

	local _message = _param.message or ""
	self.m_message = _message .. "（" .. CowMatchRemindLayer.showTime .."）"
	self.m_showMessage = _message
	-- self.m_txt_title:setString( _param.m_title or "提示")
	if _param.m_btnyes then
		self.m_btn_yes:setTitleText( _param.m_btnyes )
	end
	if _param.m_btnno then
		self.m_btn_no:setTitleText( _param.m_btnno )
	end
	if _param.color then
		self.m_txt_tip:setColor(_param.color)
	end
	if _param.fontSize then
		self.m_txt_tip:setFontSize(_param.fontSize)
	end
    self.m_txt_tip:setString( self.m_message )
    self.m_txt_tip:ignoreContentAdaptWithSize(false)
    local tSize = self.m_txt_tip:getContentSize() 
    local tmp_width = tSize.width
    local linecount = math.ceil(tmp_width / self.m_size.width )
    local end_height = (linecount )* (tSize.height) 
    if linecount > 1 then
    	self.m_txt_tip:setTextHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT)
	    self.m_txt_tip:setString(self.m_message )
	    self.m_txt_tip:ignoreContentAdaptWithSize(false)
		tSize = self.m_txt_tip:getContentSize() 
		tmp_width = tSize.width
		linecount = math.ceil(tmp_width / self.m_size.width )
		end_height = (linecount )* (tSize.height) 
	else
		self.m_txt_tip:setTextHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER)
    end
    self.m_txt_tip:setContentSize(cc.size(self.m_size.width,end_height)) 
    self:update_time()
end

function CowMatchRemindLayer:setEnableOutline()
	self.m_btn_no:getTitleRenderer():enableOutline(cc.c4b(9, 83, 84, 255),3)
	self.m_btn_yes:getTitleRenderer():enableOutline(cc.c4b(9, 83, 84, 255),3)
	self.m_txt_title:enableOutline(cc.c4b(65, 27, 23, 255),2)
end

function CowMatchRemindLayer:cancelMatchSignUp()
	print("cancelMatchSignUp:取消进入比赛房间")
    RoomTotalController:getInstance():cancelMatchSignUp(self.m_params.enterGameId,function(__info) 
	if __info.m_ret == 0 then
	    TOAST("退赛成功")
	else
	    TOAST("退赛失败")
	end  
	end)
end

function CowMatchRemindLayer:forceEnterGame()
	print("forceEnterGame:进入比赛房间")
	local gameType = RoomData:getGameTypeByRoomId( self.m_params.enterGameId)
	if gameType then
	    if RoomData:isGameUp2Date(gameType) then
	    	-- 退出当前游戏
	    	if self.m_scene then
	    		print("self.m_scene")
	    		if self.m_scene.__cname == "CowGameScene" or self.m_scene.__cname == "HappyCowGameScene" or self.m_scene.__cname == "RobCowGameScene" then
	    			print("self.m_scene sendExitGameReq")
	    			self.m_scene:sendExitGameReq()
	    			-- self.m_scene:StrongbackGame()
	    		end
	    	end
	        RoomTotalController:getInstance():forceEnterGame(RoomData.CRAZYOX,self.m_params.enterGameId)
	        ToolKit:addLoadingDialog(5, "正在进入比赛房间...")
	    end
	end
end


function CowMatchRemindLayer:update_time()

	self.m_message = self.m_showMessage .. "（" .. (CowMatchRemindLayer.showTime-self.m_showTime) .."）"
    self.m_txt_tip:setString(self.m_message)
    self.m_showTime = self.m_showTime + 1
    if self.m_update_timer == nil then
        self.m_update_timer = scheduler.scheduleGlobal(handler(self,self.update_time),1)
    end
    if self.m_showTime >= CowMatchRemindLayer.showTime then
    	self:endupdate_timer()
		self:cancelMatchSignUp()
		self:closeDialog()
    end
end

function CowMatchRemindLayer:endupdate_timer()
    if self.m_update_timer then
        scheduler.unscheduleGlobal(self.m_update_timer)
        self.m_update_timer = nil
    end
end

-- 点击事件回调
function CowMatchRemindLayer:onTouchCallback( sender )
    local name = sender:getName()
    print("name = ",name)
    if name == "btn_yes" then
    	self:endupdate_timer()
    	-- self:closeDialog()
    	self:forceEnterGame()
		
 	elseif name == "btn_close" then

 	elseif name == "btn_no" then
 		self:endupdate_timer()
 		self:cancelMatchSignUp()
 		self:closeDialog()		
    end 
end

function CowMatchRemindLayer:onDestory()
	self:endupdate_timer()
end

function CowMatchRemindLayer:onExit()
   print("CowMatchRemindLayer:onExit")
   self:endupdate_timer()
end

return CowMatchRemindLayer