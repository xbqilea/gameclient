--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 游戏房间公用层菜单

local CowItemGameRoomMenu = class("CowItemGameRoomMenu", function ()
    return display.newLayer()
end)

function CowItemGameRoomMenu:ctor( __funcData )
    self:myInit()

    self.data = __funcData

    self:setupViews()  
    self:updateViews()
end

-- 初始化成员变量
function CowItemGameRoomMenu:myInit()
	self.sId = "self"
	self.name = ""
	self.nextLayer = ""

	self.data = nil
end

-- 初始化界面
function CowItemGameRoomMenu:setupViews()
    self.root = UIAdapter:createNode("roompublic/res/csb/horizontal/cowgamechannel/cow_item_game_room_menu.csb")
    self:addChild(self.root)

    self.img = self.root:getChildByName("img")
    self.img:setColor(cc.c3b(198, 225, 234))
    self.label = self.root:getChildByName("label")

    UIAdapter:adapter(self.root, handler(self, self.onTouchCallback))
end

-- 设置数据
function CowItemGameRoomMenu:updateViews()
	-- body
    self.img:loadTexture(self.data.iconName, 1)
    self.label:setString(self.data.name)
end

-- 点击事件回调
function CowItemGameRoomMenu:onTouchCallback( sender )
	local name = sender:getName()
	if name == "btn" then
		local quote = nil
		if self.data.quoteId and type(self.data.quoteId) == "number" then
			quote = fromFunction(self.data.quoteId)
		end
		if quote then
			sendMsg(MSG_GOTO_STACK_LAYER, {layer = quote.mobileFunction, name = quote.name, direction = DIRECTION.HORIZONTAL, funcId = quote.id})
		else
			sendMsg(MSG_GOTO_STACK_LAYER, {layer = self.data.mobileFunction, name = self.data.name, direction = DIRECTION.HORIZONTAL, funcId = self.data.id})
		end
		
	end
end

return CowItemGameRoomMenu