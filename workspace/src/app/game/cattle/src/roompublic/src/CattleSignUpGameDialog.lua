--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 报名框
local XbDialog = require("src.app.hall.base.ui.CommonView")

local CowSignUpGameDialog = class("CowSignUpGameDialog",XbDialog)

local ONE_CONDITION_HEIGHT = 78

CowSignUpGameDialog.itemType = 
{
   ty_prop = 1,       -- 道具
   ty_goldCoin = 2,  -- 金币
   ty_diamond  = 3,  -- 钻石
   ty_aBean = 4,     -- A豆
   ty_level = 5,     -- 等级     
} 

CowSignUpGameDialog.conditionType = 
{
   ty_free = 1   ,       -- 免费
   ty_haveCondition = 2 -- 有条件
}

function CowSignUpGameDialog:ctor(roomCtl)
    self._roomCtl = roomCtl
    

    self.m_isSignUp = 0    -- 是否报名
    self.m_peopleCnt = 0 -- 已经报名的人数
    self.m_selectConditionId = 0
    self.m_conditions = {}


    self:myInit()

    self:setupViews()
    
    self:setCloseCallback( handler(self,self.dealSomething4Close))

end

function CowSignUpGameDialog:dealSomething4Close()
     self._roomCtl:reqMatchCloseSignupPage()
end 

-- 条件排序
function sortConditions(conditionItemA ,conditionItemB )
    if conditionItemA.isSelect > conditionItemB.isSelect then
       return true
    elseif conditionItemA.isSelect < conditionItemB.isSelect then
       return false
    end
    
    if conditionItemA.isFit > conditionItemB.isFit then
        return true
    elseif conditionItemA.isFit < conditionItemB.isFit then
       return false
    end

    if conditionItemA.conditionId > conditionItemB.conditionId then
       return true
    end

    return false
end
-- 判断是否有满足的条件
function CowSignUpGameDialog:conditionIsFit()
     local isHave = false
     for i=1,#self.m_conditions do
         if self.m_conditions[i].isFit == 1 then
            isHave = true
            break
         end
     end
     return isHave
end 

function CowSignUpGameDialog:selectCondition( conditionId )
   for i=1,#self.m_conditions do
       if self.m_conditions[i].conditionId == conditionId then
           self.m_conditions[i].isSelect = 1
       else
           self.m_conditions[i].isSelect = 0
       end
   end
end

-- 整理条件数据
function CowSignUpGameDialog:formatConditionDatas()
   for i=1,#self.m_conditions do
      local conditionName = ""
      local conditionFit = 0
      local conditionNum = 0
      for j=1,#self.m_conditions[i].condition do
          local  conditionValue = self.m_conditions[i].condition[j]
          if conditionValue.type == CowSignUpGameDialog.itemType.ty_prop then 
                local item = GlobalItemInfoMgr:getItemInfoByID(conditionValue.itemId)
                local itemName = item:getName().."*"..conditionValue.num
                if conditionName == "" then    
                   conditionName = conditionName..itemName
                else
                   conditionName = conditionName..";"..itemName
                end
               local items = GlobalBagController:findItemByID( conditionValue.itemId )
               local totalCount = 0
               if items then
                  for i=1,items do
                     totalCount = totalCount + items[i]:getCount()
                  end
               end
               if totalCount > conditionValue.num then
                  conditionNum = conditionNum + 1
               end
          elseif conditionValue.type == CowSignUpGameDialog.itemType.ty_goldCoin then
                local  name = conditionValue.num .. "金币"
                if conditionName == "" then
                  conditionName = conditionName.. name
                else
                   conditionName = conditionName..";"..name
                end
                if Player.getGoldCoin() > conditionValue.num then
                   conditionNum = conditionNum + 1
                end
          elseif conditionValue.type == CowSignUpGameDialog.itemType.ty_diamond then
                local  name = conditionValue.num .. "钻石"
                if conditionName == "" then
                  conditionName = conditionName.. name
                else
                   conditionName = conditionName..";"..name
                end
                if Player.getDiamond() > conditionValue.num then
                   conditionNum = conditionNum + 1
                end
           elseif conditionValue.type == CowSignUpGameDialog.itemType.ty_aBean then
                local  name = conditionValue.num .. "A豆"
                if conditionName == "" then
                  conditionName = conditionName.. name
                else
                   conditionName = conditionName..";"..name
                end
                if Player.getABean() > conditionValue.num then
                   conditionNum = conditionNum + 1
                end
          elseif conditionValue.type == CowSignUpGameDialog.itemType.ty_level then
                local  name = conditionValue.num .. "等级"
                if conditionName == "" then
                  conditionName = conditionName.. name
                else
                   conditionName = conditionName..";"..name
                end
                if Player.getLevel() > conditionValue.num then
                   conditionNum = conditionNum + 1
                end
          end
      end 
      if conditionNum == #self.m_conditions[i].condition then
         conditionFit = 1
      end
      self.m_conditions[i].conditionName = conditionName
      self.m_conditions[i].isFit = conditionFit
   end
end

-- 初始化成员变量
function CowSignUpGameDialog:myInit()
	self._room = {}
    self._isOpenCondition = false

	ToolKit:registDistructor(self, handler(self, self.onDestory))
end

-- 析构函数
function CowSignUpGameDialog:onDestory()
	
end

-- 初始化界面
function CowSignUpGameDialog:setupViews()
    local root = UIAdapter:createNode("csb/horizontal/layout_sign_up.csb")
    self:addChild(root)

    UIAdapter:adapter(root, handler(self, self.onTouchCallback))

    local layoutMain = root:getChildByName("layout_main")
    local layOutTop = layoutMain:getChildByName("layout_top")
    
    self.lableTitle = layOutTop:getChildByName("lable_title")
    self.btnReturn = layOutTop:getChildByName("btn_return")
    self.btnReturn:setVisible(false)
    self.btnClose = layOutTop:getChildByName("btn_close")

    self.layoutCenter = layoutMain:getChildByName("layout_center")

    self.btns = {}
    for i = 1, 3 do
    	self.btns[i] = self.layoutCenter:getChildByName("btn_" .. i)
    end
    self.btns[1]:setTouchEnabled(false)
    local label2 = self.btns[2]:getChildByName("label")
    label2:setString(STR(51, 4))
    local label3 = self.btns[3]:getChildByName("label")
    label3:setString(STR(52, 4))

    self.imgSelectBg = self.layoutCenter:getChildByName("img_select_bg")
    --self.imgSelect = self.layoutCenter:getChildByName("img_select")
    
    self.layoutSelects = {} 
    for i=1,3 do
       self.layoutSelects[i] = {}
       local layoutSelect = string.format("layout_select_%d",i-1)
       self.layoutSelects[i].layoutSelect = self.layoutCenter:getChildByName(layoutSelect)
       self.layoutSelects[i].nameLabel = self.layoutSelects[i].layoutSelect:getChildByName("label_select")
       if i ~= 1 then
           self.layoutSelects[i].layoutSelect:setVisible(false)
       end
    end
    self.imgSelect = self.layoutSelects[1].layoutSelect:getChildByName("img_select")

    self.btnApply = self.layoutCenter:getChildByName("btn_apply")


    self.labelPeopleNum = root:getChildByName("person_num")

    -- 说明
    self.layoutExplain = layoutMain:getChildByName("layout_explain")
    self.layoutExplain:setVisible(false)
    self.m_pScrollView = self.layoutExplain:getChildByName("scroll_view")
    self.contentLabel = self.m_pScrollView:getChildByName("content_label")
    self.contentLabel:setVisible(false)
    self.m_labelWidth = self.contentLabel:getContentSize().width
    self.m_labelX = self.contentLabel:getPositionX()
end

-- 设置数据
function CowSignUpGameDialog:setData( data )
	  self._room = data 
    self.m_isSignUp = self._room.__isSignUp
    if self._room.__peopleCnt then
       self.m_peopleCnt = self._room.__peopleCnt
    end
    self.m_conditions = RoomData:getRoomConditionsByRoomData( self._room )
    self:formatConditionDatas()
    table.sort(self.m_conditions,sortConditions)
	  self:updateViews()
end

function CowSignUpGameDialog:setSignUp( isSignUp  )
    self.m_isSignUp = isSignUp
end

function CowSignUpGameDialog:setPeopleCnt( peopleCnt )
    self.m_peopleCnt = peopleCnt
end

function CowSignUpGameDialog:updateViews()
  -- 比赛名称
  self.lableTitle:setString(self._room.phoneGameName)
	-- 已报名
	if self.m_isSignUp  == 1 or self.m_isSignUp == 2 then
		self.btnApply:loadTextures("dt_btn_popup_red.png", "", "", 1)
        self.btnApply:setTitleText(STR(49, 4))

	else
		self.btnApply:loadTextures("dt_btn_popup_green.png", "", "", 1)
        self.btnApply:setTitleText(STR(50, 4))

	end

    local label = self.btns[1]:getChildByName("label")
    local data = self._room
    if data.applyLeftFirstLineTips and string.len(data.applyLeftFirstLineTips) > 0 then
        label:setString(data.applyLeftFirstLineTips)
    end

    self.labelPeopleNum:setString( self.m_peopleCnt .. STR(48, 4) )      -- 报名人数
    
    if data.conditionType == CowSignUpGameDialog.conditionType.ty_free then --免费
       self:setFreeCondition()
    elseif data.conditionType == CowSignUpGameDialog.conditionType.ty_haveCondition then
        -- 满足条件
        if self:conditionIsFit() then
            self:setCondition()
        else
            self:setUnCondition()
        end
    end
end

function CowSignUpGameDialog:setSignUpView()
    if self.m_isSignUp  == 1 or self.m_isSignUp == 2  then
        self.btnApply:loadTextures("dt_btn_popup_red.png", "", "", 1)
        self.btnApply:setTitleText(STR(49, 4))
    else
        self.btnApply:loadTextures("dt_btn_popup_green.png", "", "", 1)
        self.btnApply:setTitleText(STR(50, 4))
    end
end

function CowSignUpGameDialog:setbtnApplyEable( isEable )
     self.btnApply:setTouchEnabled( isEable )
end

function CowSignUpGameDialog:setPeopleView()
    self.labelPeopleNum:setString( self.m_peopleCnt .. STR(48, 4) )
end

-- 点击事件回调
function CowSignUpGameDialog:onTouchCallback( sender )
	local name = sender:getName()
    local tag = sender:getTag()
	if name == "btn_close" then
        self:closeDialog()
    elseif name == "btn_2" then
        local data = self._room
        self:setExplain(data.matchDetail)

    elseif name == "btn_3" then
        local data = self._room
        self:setExplain(data.matchReward)

    elseif name == "btn_return" then
        self.layoutExplain:setVisible(false)
        self.layoutCenter:setVisible(true)
        self.btnReturn:setVisible(false)
        self.btnClose:setVisible(true)

    elseif name == "layout_select_0" then
        self._isOpenCondition = not self._isOpenCondition
        local conditionId =  tag -1000
        if self:conditionIsFit() then
           self.m_selectConditionId = conditionId
           self:selectCondition( conditionId )
           table.sort(self.m_conditions,sortConditions)
           self:setCondition()
        else
           self:setExplain(data.matchDetail)
        end
    elseif name == "layout_select_1" then
        self._isOpenCondition = not self._isOpenCondition
        local conditionId =  tag -1000
        self.m_selectConditionId = conditionId
        self:selectCondition( conditionId )
        table.sort(self.m_conditions,sortConditions)
        self:setCondition()

    elseif name == "layout_select_2" then
        self._isOpenCondition = not self._isOpenCondition
        local conditionId =  tag -1000
        self.m_selectConditionId = conditionId
        self:selectCondition( conditionId )
        table.sort(self.m_conditions,sortConditions)
        self:setCondition()

    elseif name == "btn_apply" then
        if self.m_isSignUp == 1 or self.m_isSignUp == 2  then  
            self._roomCtl:reqMatchCancelSignUp()
        else
            self._roomCtl:reqMacthSignUp( self.m_selectConditionId )
        end
	end

end

function CowSignUpGameDialog:setConditionEx( conditionId )
    self.m_selectConditionId = conditionId
    self:selectCondition( conditionId )
    table.sort(self.m_conditions,sortConditions)
    self:setCondition()
end


function CowSignUpGameDialog:setExplain( str )  
    self:removeDescribe()
    self.layoutCenter:setVisible(false)
    self.btnClose:setVisible(false)
    self.btnReturn:setVisible(true)
    self.layoutExplain:setVisible(true)
    
    self.describeLabel = display.newTTFLabel( {
        text = str, 
        font = "黑体", 
        size = 24, 
        color = cc.c3b(132, 81, 36),
        dimensions = cc.size(self.m_labelWidth, 0),
    } )
    
    self.describeLabel:setAnchorPoint(cc.p(0, 1))
    local describeLabelS = self.describeLabel:getContentSize()
    local scrollViewS = self.m_pScrollView:getContentSize()
     if scrollViewS.height > describeLabelS.height then
        self.m_pScrollView:setInnerContainerSize(cc.size(scrollViewS.width, scrollViewS.height))
        self.describeLabel:setPosition(cc.p(self.m_labelX, scrollViewS.height))   
    else
        self.m_pScrollView:setInnerContainerSize(cc.size(scrollViewS.width, describeLabelS.height))
        self.describeLabel:setPosition(cc.p(self.m_labelX, describeLabelS.height))
    end

    self.m_pScrollView:addChild( self.describeLabel ) 
end

function CowSignUpGameDialog:removeDescribe()
    if self.describeLabel then 
       self.m_pScrollView:removeChild( self.describeLabel )
       self.describeLabel = nil
    end
end

-- 免费
function CowSignUpGameDialog:setFreeCondition()
    self.layoutSelects[1].layoutSelect:setVisible(true)
    self.layoutSelects[2].layoutSelect:setVisible(false)
    self.layoutSelects[3].layoutSelect:setVisible(false)
    self.imgSelect:setVisible( false )
    self.layoutSelects[1].nameLabel:setString("免费")
    local size = self.imgSelectBg:getContentSize()
    self.imgSelectBg:setContentSize(size.width, ONE_CONDITION_HEIGHT)
end

-- 设置满足条件时的显示
function CowSignUpGameDialog:setCondition()
    self.imgSelect:loadTexture("dt_bm_btn_drop.png", 1)
    if #self.m_conditions > 0 then
       self.m_selectConditionId = self.m_conditions[1].conditionId
    end
    --self.labelSelect:setColor(cc.c3b(107, 70, 45))
    for i=1,#self.m_conditions do
        self.layoutSelects[i].layoutSelect:setTag(1000 + self.m_conditions[i].conditionId)
        self.layoutSelects[i].nameLabel:setString(self.m_conditions[i].conditionName)
        --self.layoutSelects[i].layoutSelect:setTouchEnabled(self.m_conditions[i].isFit)
    end
    
    if self._isOpenCondition == false then
        self.layoutSelects[2].layoutSelect:setVisible(false)
        self.layoutSelects[3].layoutSelect:setVisible(false)
        self.imgSelect:setScaleY(1)
        local size = self.imgSelectBg:getContentSize()
        self.imgSelectBg:setContentSize(size.width, ONE_CONDITION_HEIGHT)
    else
        -- 箭头翻转
        self.imgSelect:setScaleY(-1)
        for i = 1, #self.m_conditions do
            self.layoutSelects[i].layoutSelect:setVisible(true)
        end
        local size = self.imgSelectBg:getContentSize()
        self.imgSelectBg:setContentSize(size.width, ONE_CONDITION_HEIGHT*(#self.m_conditions))
    end
end

-- 设置不满足条件时的显示
function CowSignUpGameDialog:setUnCondition()
    self.imgSelect:loadTexture("dt_bm_icon_help.png", 1)
    self.layoutSelects[1].nameLabel:setString(STR(54, 4))
    self.layoutSelects[1].nameLabel:setColor(cc.c3b(206, 20, 21))
    -- self.layoutSelect_0:setTouchEnabled(false)
end

return CowSignUpGameDialog
