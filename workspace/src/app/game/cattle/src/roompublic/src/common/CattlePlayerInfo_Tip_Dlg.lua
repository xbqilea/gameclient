--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- CowPlayerInfo_Tip_Dlg
-- 玩家信息
local CowClipperRoundHead = require("src.app.game.cattle.src.roompublic.src.common.CattleClipperRoundHead")
local XbDialog = require("app.hall.base.ui.CommonView")

local CowPlayerInfo_Tip_Dlg = class("CowPlayerInfo_Tip_Dlg", function()
    return XbDialog.new()
end)

function CowPlayerInfo_Tip_Dlg:ctor(_userid,_controll ) 
	self:myInit(_userid,_controll)
	self:setupViews()
    self:setBgOpacity(0)
end

function CowPlayerInfo_Tip_Dlg:myInit( _userid,_controll ) 
	self.m_root = nil
    self.m_userid = _userid
    self.m_controll = _controll
    self.m_tipsTb={}
end

function CowPlayerInfo_Tip_Dlg:setupViews()
    self.m_root = UIAdapter:createNode("cow_publicroom_csb/cow_playerinfo_tip_dlg.csb")
    self:addChild(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    
    self.m_laytout_headbg = self.m_root:getChildByName("laytout_headbg")
    self.m_txt_name = self.m_root:getChildByName("txt_name")
    self.m_txt_glod = self.m_root:getChildByName("txt_glod")
    self.m_txt_zhanjinum = self.m_root:getChildByName("txt_zhanjinum") 
    self.m_tipsTb[#self.m_tipsTb+1] = self.m_txt_zhanjinum
    self.m_txt_sheng = self.m_root:getChildByName("txt_sheng") 
    self.m_tipsTb[#self.m_tipsTb+1] = self.m_txt_sheng
    self.m_txt_funum = self.m_root:getChildByName("txt_funum") 
    self.m_tipsTb[#self.m_tipsTb+1] = self.m_txt_funum
    self.m_txt_fu = self.m_root:getChildByName("txt_fu")
    self.m_tipsTb[#self.m_tipsTb+1] = self.m_txt_fu
    self.m_txt_shenglvnum =  self.m_root:getChildByName("txt_shenglvnum")
    self.m_img_headbg = self.m_root:getChildByName("img_headbg")
    self.m_txt_zhanji = self.m_root:getChildByName("txt_zhanji")
    self.m_txt_shenglv = self.m_root:getChildByName("txt_shenglv")
    ------------ 先隐藏 ------------
    self.m_txt_shenglvnum:setVisible(false)
    self.m_txt_zhanji:setVisible(false)
    self.m_txt_sheng:setVisible(false)
    self.m_txt_zhanjinum:setVisible(false)
    self.m_txt_fu:setVisible(false)
    self.m_txt_funum:setVisible(false)
    self.m_txt_shenglv:setVisible(false)

    self:updateViews(self.m_userid)
end

function CowPlayerInfo_Tip_Dlg:updateViews( _userid )
    local _info = self.m_controll:getUserWinLostTimesInfo(_userid)
    if _userid and self.m_controll then
        -- 战绩
        if _info then
                self.m_txt_shenglvnum:setVisible(true)
                self.m_txt_zhanji:setVisible(true)
                self.m_txt_sheng:setVisible(true)
                self.m_txt_zhanjinum:setVisible(true)
                self.m_txt_fu:setVisible(true)
                self.m_txt_funum:setVisible(true)
                self.m_txt_shenglv:setVisible(true)
            self.m_txt_zhanjinum:setString(tostring(_info.m_winTimes))
            self.m_txt_funum:setString(tostring(_info.m_lostTimes))
            -- 胜率
            local totil = _info.m_lostTimes+_info.m_winTimes
            local pre = 0
            if totil == 0 then
                pre = 0
            else
                pre = _info.m_winTimes / (totil)
            end
            local str = math.floor(pre*100+ 0.5) --string.format(pre, 2)
            self.m_txt_shenglvnum:setString(tostring(str) .. "%")
        else
            print("战绩")
        end
        ----------------
                -- 设置位置
        local index = 6
        local offsetx = self.m_txt_zhanjinum:getPositionX() + self.m_txt_zhanjinum:getContentSize().width + index
        self.m_txt_sheng:setPositionX(offsetx)
        offsetx = offsetx+self.m_txt_sheng:getContentSize().width + index
        self.m_txt_funum:setPositionX(offsetx)
        offsetx = offsetx+self.m_txt_funum:getContentSize().width + index
        self.m_txt_fu:setPositionX(offsetx)
        -- 金币
        local data = self.m_controll:getGamePlayerInfoWithUserID(_userid)
        if data then
            self.m_txt_glod:setString(tostring(data.m_score))
            -- 头像
            self.m_clipperRoundHead = CowClipperRoundHead.new(self.m_laytout_headbg:getContentSize(),data.m_avatarId ,_userid,true)
            self.m_img_headbg:addChild(self.m_clipperRoundHead)
            self.m_clipperRoundHead:setScale(0.88)
            self.m_clipperRoundHead:setPosition(self.m_img_headbg:getContentSize().width*0.5,self.m_img_headbg:getContentSize().height*0.5)
            -- 昵称
            self.m_txt_name:setString(data.m_nickName)
        end

    end
end

-- 点击事件回调
function CowPlayerInfo_Tip_Dlg:onTouchCallback( sender )
    local name = sender:getName()
    if name == "btn_close" then
        self:closeDialog()
    end
end

function CowPlayerInfo_Tip_Dlg:onExit()
   print("CowPlayerInfo_Tip_Dlg:onExit")

end

return CowPlayerInfo_Tip_Dlg