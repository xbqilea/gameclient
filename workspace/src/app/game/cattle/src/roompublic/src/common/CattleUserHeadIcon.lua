-- Author: 
-- Date: 2018-09-5 
local UserCenterHeadIcon = require("app.hall.userinfo.view.UserHeadIcon")

local CattleUserHeadIcon = class("CattleUserHeadIcon", UserCenterHeadIcon)

function CattleUserHeadIcon:myInit(_data)

    self._showhead_img_head = nil      --头像
    self._headImgId = nil              --头像图片id
    self._headImgName = nil            --头像图片名字                  
    self._needSize = nil               --设置的头像大小
    
    
    if _data and _data._id  then
        self._headImgId = _data._id
    else
        self._headImgId = Player:getFaceID()   
    end
    
   
    if _data and _data._accountID and _data._id  then
        self.accountID =  _data._accountID  
    else
        self.accountID = Player:getAccountID()
    end
    
    if _data and _data._size then
        self._needSize = _data._size   
    end               
    
    if _data and _data._clip and  _data._clip == true  then
        self._clip = true 
    end 

    if _data and _data._stencilName  then

        self._stencilName = _data._stencilName 
    else 
    	self._stencilName = "niuniu_tx.png"
    end
    
end
--头像加载纹理
function CattleUserHeadIcon:img_headloadTexture()     
        self._showhead_img_head:loadTexture(self._headImgName, 1) 
    if self._needSize then
        local _size =  self._showhead_img_head:getContentSize()
        self._showhead_img_head:setScale(self._needSize.width/_size.width)
    end
     
    if self._clip and self._needSize then
    
        local ClipperRound = require("app.hall.base.ui.HeadIconClipperArea")
          
        --模板精灵

        local _StencilSpr =  cc.Sprite:createWithSpriteFrameName(self._stencilName)  
        if  _StencilSpr then 

	        local _size =  _StencilSpr:getContentSize()
	        
	        _StencilSpr:setScale(self._needSize.width/_size.width)
	        
	        self._showhead_img_head:removeFromParent()
	        
	        self._showhead_img_head = ccui.ImageView:create() 
	            self._showhead_img_head:loadTexture(self._headImgName, 1) 
	        
	        local _size =  self._showhead_img_head:getContentSize()
	        self._showhead_img_head:setScale(self._needSize.width/_size.width)

	        local _clipNode  = ClipperRound:clipperHead(_StencilSpr,  self._showhead_img_head)
	        self:addChild(_clipNode)   
	     end        	
     end  	
       
    self._showhead_img_head:setVisible(true)
end

return  CattleUserHeadIcon