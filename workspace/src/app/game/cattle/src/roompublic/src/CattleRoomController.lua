--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 房间控制器
require("app.game.cattle.src.roompublic.src.data.CattleData")
require("app.game.cattle.src.roompublic.src.CattleInitRoomPublicResPath")
require("app.game.cattle.src.roompublic.src.utils.CattleGlobalController")

local GamePlayerInfo = require("app.game.common.data.GamePlayerInfo")
local CowSignUpGameDialog = require("app.game.cattle.src.roompublic.src.CattleSignUpGameDialog") 
local CowGameController = require("app.game.cattle.src.crazycattle.src.controller.CattleGameController") -- 疯狂
local HappyCowGameController = require("app.game.cattle.src.happycattle.src.controller.HappyCattleGameController") --花式
local RobCowGameController = require("app.game.cattle.src.robcattle.src.controller.RobCattleGameController") --抢庄
local GAME_SCENE_PATH = "src.app.game.cattle.src.crazycattle.scene.GameScene"
require("app.game.cattle.src.roompublic.src.def.CattleRoomDef")
require("app.game.cattle.src.friendcattle.src.data.FriendCattlePlayerData") --派有方

local FriendCowGameController = require("app.game.cattle.src.friendcattle.src.game.controller.FriendCattleGameController") --派有方
local CowRoomController = class("CowRoomController")
local GlodDialog = require("app.game.cattle.src.roompublic.src.common.CattleGlodTipsDlgLayer")
function CowRoomController:ctor( __id )
	self:myInit( __id )
	ToolKit:setGameFPS(1/60.0)
	
end 

-- 初始化
function CowRoomController:myInit( __id )
	CowGlobalController:loadProtocolTemp()
	self.m_id = __id  -- 功能节点
	self.m_chageTable = false  -- 通知换桌
	self.netMsgHandlerSwitch = {}
	self.protocolList = {}
    self:initNetMsgHandlerSwitchData()
   	-- 接收场景转发消息
   	TotalController:registerNetMsgCallback(self, Protocol.SceneServer, "CS_H2C_HandleMsg_Ack", handler(self, self.sceneNetMsgHandler))
	if PublicGameMsg then
        addMsgCallBack(self, PublicGameMsg.MSG_C2H_ENTER_SCENE_ACK, handler(self, self.onEnterScene))
        addMsgCallBack(self, PublicGameMsg.MSG_PUBLIC_CONNECT_GAME_SERVER, handler(self, self.advanceEnterScene))  
    end
    
    -- 游戏协议
    -- RoomTotalController:setGamePingTime( CowRoomDef.PingCD, CowRoomDef.PingCDTimes )
end

CowRoomController.instance = nil

function CowRoomController:advanceEnterScene(_msg,_info )
	local data = RoomData:getPortalDataByAtomId(_info.m_gameAtomTypeId)
	if data.id ~= RoomData.CRAZYOX then -- 只有牛牛才判断
		return
	end
	-- dump(data,"advanceEnterScene")
	--self:setGameAtomTypeId(_info.m_gameAtomTypeId)
	if self:isCrazyCow(_info.m_gameAtomTypeId) then
		CowGameController:getInstance():setCowRoomController( self )
		self:enterCrazyCowScene( _msg,_info )
	elseif self:isHappyCow(_info.m_gameAtomTypeId) then
		HappyCowGameController:getInstance():setCowRoomController( self )
		self:enterHappyCowScene( _msg,_info )
	elseif self:isRobCow(_info.m_gameAtomTypeId) then
		RobCowGameController:getInstance():setCowRoomController( self )
		self:enterRobCowScene( _msg,_info ) 
	elseif CowToolKit:isFriendCow( _info.m_gameAtomTypeId ) then
		FriendCowGameController:getInstance():setCowRoomController( self )
		self:enterFriendCowScene( _msg,_info ) 
	end
end
-- 获取房间控制器实例
function CowRoomController:getInstance()
	if CowRoomController.instance == nil then
		CowRoomController.instance = CowRoomController.new()
	end
    return CowRoomController.instance
end

function CowRoomController:isCrazyCow( _id )
	local id = _id or self.__gameAtomTypeId
	local data = RoomData:getRoomDataById( id )
	if not data or not data.gameKindType then return end
	if data.gameKindType == CowRoomDef.CRAZYGameKindID then return true end -- gameKindType
end

function CowRoomController:isHappyCow( _id )
	local id = _id or self.__gameAtomTypeId
	local data = RoomData:getRoomDataById( id )
	if not data or not data.gameKindType then return end
	if data.gameKindType == CowRoomDef.HAPPYGameKindID then return true end -- gameKindType
end
-- 抢庄牛牛
function CowRoomController:isRobCow( _id )
	local id = _id or self.__gameAtomTypeId
	local data = RoomData:getRoomDataById( id )
	if not data or not data.gameKindType then return end
	if data.gameKindType == CowRoomDef.RobGameKindID then return true end -- gameKindType
end

function CowRoomController:enterCrazyCowScene( _msg, _info )
	CowGameController:getInstance():advanceEnterScene(_msg, _info)
end

function CowRoomController:enterHappyCowScene( _msg, _info )
	HappyCowGameController:getInstance():advanceEnterScene(_msg, _info)
end

function CowRoomController:enterRobCowScene( _msg, _info )
	RobCowGameController:getInstance():advanceEnterScene(_msg, _info)
end

function CowRoomController:enterFriendCowScene( _msg, _info )
	FriendCowGameController:getInstance():advanceEnterScene(_msg, _info)
end

function CowRoomController:setPortalId( __portalId )
	self.portalId = __portalId
end

function CowRoomController:setScenePath( __path )
	self.gameScenePath = __path
end

function CowRoomController:getMatchBeforeGameInfo()
	return self.MatchBeforeGameInfo
end

--[[
function  CowRoomController:setGameAtomTypeId( gameAtomTypeId )
    self.__gameAtomTypeId = gameAtomTypeId
end

function CowRoomController:getGameAtomTypeId()
	return self.__gameAtomTypeId
end
--]]

function CowRoomController:getMatchGameResultInfo()
	return  self.MatchGameResultInfo
end

function CowRoomController:initNetMsgHandlerSwitchData()
	self.netMsgHandlerSwitch["CS_M2C_OxFreeScene_ExitRoom_Nty"]   =  handler(self,self.exitRoomNty) --玩家离开房间通知(PC+手机)
	self.netMsgHandlerSwitch["CS_M2C_OxFreeScene_EnterTable_Nty"]   =  handler(self,self.enterTableNty) --进入桌子通知(PC+手机)
	-- 疯狂 换桌
    self.netMsgHandlerSwitch["CS_M2C_OxFreeScene_ChangeTable_Nty"]   =  handler(self,self.chageTableNty) -- 换桌
	-- 欢乐 换桌
	self.netMsgHandlerSwitch["CS_M2C_OxHappy_ChangeTable_Nty"]   =  handler(self,self.happychageTableNty) -- 换桌
	self.netMsgHandlerSwitch["CS_M2C_OxHappy_CaiChiReward_Nty"]   =  handler(self,self.caiChiPmdRewardNty) -- 彩池跑马灯
	-- 抢庄
	self.netMsgHandlerSwitch["CS_M2C_OxBid_ChangeTable_Nty"]   =  handler(self,self.robchageTableNty) -- 换桌

	self.netMsgHandlerSwitch["CS_M2C_OxHappy_ExitRoom_Nty"]   =  handler(self,self.exitRoomNty)
	self.netMsgHandlerSwitch["CS_M2C_OxHappy_EnterTable_Nty"]   =  handler(self,self.enterTableNty)

	-- self.netMsgHandlerSwitch["CS_M2C_OxHappy_EnterTable_Nty"]   =  handler(self,self.enterTableNty)

	-- RoomTotalController:getInstance():setNetMsgCallbackByProtocolList(protocolList, handler(self, self.netMsgHandler))

end

function CowRoomController:onEnterScene( _msgType , __info )
    print("CowRoomController onEnterScene 进入场景")
    if __info.m_ret ~= 0 then
	    ToolKit:removeLoadingDialog()
	end
	local data = RoomData:getPortalDataByAtomId(__info.m_gameAtomTypeId)
	if data.id ~= RoomData.CRAZYOX then -- 只有牛牛才判断
		return
	end
    if __info.m_ret == CowRoomDef.SceneErrorId.GLOD_NOT_ENOUGH then
    	-- TOAST("最小金币数量限制，不能进入游戏")
		local room = RoomData:getRoomDataById(__info.m_gameAtomTypeId)
		if room  then
			local josn = json.decode(room.roomMinScore)
	        for k,v in pairs(josn) do
	        	if v.min > Player:getGoldCoin() then
	     --    		local m_btnyesStr = "确定"
	     --    		local callback = nil
	     --    		if getFuncOpenStatus(GlobalDefine.FUNC_ID.LOBBY_SHOP ) ~= 1  then  -- 打开CowRoomDef.Fun.GOUMAI_GLOD
						-- m_btnyesStr = "前往充值" 
						-- callback = handler(self, self.gotoMallLayer)
				  --   end -- 商城(购买金币)
				    -- local params = {
				    --     m_title = "提示",
				    --     m_message = "加入房间需要" .. v.min .."金币,您的金币不足", -- todo: 换行
				    --     m_btnyes = m_btnyesStr ,
				    --     m_closeBtn = true,
				    -- }

				    -- local dlg = require("app.game.cattle.src.roompublic.src.common.CattleSignleDlgLayer").new(params, callback)
				    -- dlg:showDialog()
				    GlodDialog.new(v.min,getFuncOpenStatus(CowRoomDef.Fun.GOUMAI_GLOD ) ~= 1):showDialog()
				------------------------------------------------------------------------------------------
				    break
				end
			end
	    end
	elseif __info.m_ret == CowRoomDef.SceneErrorId.GLOD_EXCEED_MAX then
			local params = {
		        m_title = "提示",
		        m_message =  "您的金币超出了该房间" .. v.max .."限制", -- todo: 换行
		        m_btnyes = "确定",
		        m_closeBtn = true,
		    }
		local DlgAlert = require("app.game.cattle.src.roompublic.src.common.CattleSignleDlgLayer")
		local dlg = DlgAlert.new( params )
		dlg:showDialog()
	elseif __info.m_ret ==  CowRoomDef.SceneErrorId.ROOM_Maintain then -- 房间维护
		print("房间维护")
	elseif __info.m_ret ~= 0 then
		ToolKit:showErrorTip(__info.m_ret)
	elseif __info.m_ret == 0 then
		g_cowLastEnterRoomID = __info.m_gameAtomTypeId
	end
	if __info.m_ret ~= 0 then
		self:onDestory()
	end
end 

function CowRoomController:gotoMallLayer()
	local data = fromFunction(GlobalDefine.FUNC_ID.LOBBY_SHOP  )-- CowRoomDef.Fun.MallMain)
    if data then
        local stackLayer = data.mobileFunction
        if stackLayer and string.len(stackLayer) > 0 then
            sendMsg(MSG_GOTO_STACK_LAYER, {layer = stackLayer, name = data.name, funcId = CowRoomDef.Fun.MallMain})
        else
            TOAST(STR(62, 1)  )
        end
    end

end

--------------------- CCGameSceneBase 基类函数 --------------------------------------
--[[
function CowRoomController:exitGame()
	print("CowRoomController:exitGame")
end
--]]

-- 游戏结束时的退出和继续    类型，0：退出，1：继续
function CowRoomController:gameEndExitOrContinue( __type )
	
end

--正在游戏中退出
function CowRoomController:gamePlayExit()

end

--比赛房游戏退出
function CowRoomController:reqMatchExitGame()

end

-----------------------------------  析构函数  -----------------------------------------
function CowRoomController:onDestory()
	print("CowRoomController:onDestory")
	-- 游戏通知
	for k, v in pairs(self.protocolList) do
		TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, v)
	end
	TotalController:removeNetMsgCallback(self, Protocol.SceneServer, "CS_H2C_HandleMsg_Ack")
	removeMsgCallBack(self, PublicGameMsg.MSG_C2H_ENTER_SCENE_ACK)
	removeMsgCallBack(self, PublicGameMsg.MSG_PUBLIC_CONNECT_GAME_SERVER)
	CowRoomController.instance = nil
end

function CowRoomController:sceneNetMsgHandler( __idStr, __info )
	if __idStr == "CS_H2C_HandleMsg_Ack" then
		if __info.m_result == 0 then
			local gameAtomTypeId = __info.m_gameAtomTypeId
			if type(__info.m_message) == "table" then
				if next(__info.m_message) then
					local cmdId = __info.m_message[1].id
					local info = __info.m_message[1].msgs
					self:netMsgHandler(cmdId, info)
				end
			end
		else
			ToolKit:showErrorTip(__info.m_result)
			-- TOAST("找不到场景信息")
		end
	end
end

function CowRoomController:netMsgHandler( __idStr, __info )
	print("__idStr = ",__idStr)
    if self.netMsgHandlerSwitch[__idStr] then
       (self.netMsgHandlerSwitch[__idStr])( __info )
    else
       print("未注册消息:"..(__idStr or ""))
    end
end


------------------------------请求消息 --------------------------------
--离开房间请求(PC)
function CowRoomController:reqCS_C2M_OxFreeScene_ExitRoom_Req()
	ToolKit:addLoadingDialog(10, "正在请求离开房间...")
	RoomTotalController:getInstance():send2GameServer( "CS_C2M_OxFreeScene_ExitRoom_Req", {self:getGameAtomTypeId()})	
end


--------------------------- 响应消息 ---------------------------------------
--进入桌子通知(PC+手机)
function CowRoomController:enterTableNty( __info )
	if __info.m_result == 0 then
		self:setGameAtomTypeId(__info.m_gameAtomTypeId)
		print("__info.m_gameAtomTypeId = ",__info.m_gameAtomTypeId)
	else  
	end	
end

--玩家离开房间通知(PC+手机)
function CowRoomController:exitRoomNty(__info)
	if self:getGameAtomTypeId() == __info.m_gameAtomTypeId then
		print("__info.m_gameAtomTypeId = ",__info.m_reason)
		-- 跳转到大厅
	end
end

function CowRoomController:happychageTableNty( __info )
	ToolKit:removeLoadingDialog()
	HappyCowGameController:getInstance():changeTableAck( __info )
	RoomTotalController:getInstance():onContinueGame() -- 断开
	if __info.m_result == 0 then  -- 成功
		TOAST("换桌成功！")
	else
		--退出游戏，强退
		TOAST("换桌失败！",function ()
			HappyCowGameController:getInstance():StrongbackGame()
		end)	
	end
end

function CowRoomController:robchageTableNty( __info )
	ToolKit:removeLoadingDialog()
	RobCowGameController:getInstance():changeTableAck( __info )
	RoomTotalController:getInstance():onContinueGame() -- 断开
	if __info.m_result == 0 then  -- 成功
		TOAST("换桌成功！")
	else
		--退出游戏，强退
		TOAST("换桌失败！",function ()
			RobCowGameController:getInstance():StrongbackGame()
		end)	
	end
end
function CowRoomController:caiChiPmdRewardNty( __info )
	HappyCowGameController:getInstance():caiChiPmdRewardNty( __info )
end

function CowRoomController:chageTableNty( __info )
	-- self.m_chageTable = true
	ToolKit:removeLoadingDialog()
	CowGameController:getInstance():changeTableAck( __info )
	RoomTotalController:getInstance():onContinueGame() -- 断开
	if __info.m_result == 0 then  -- 成功
		TOAST("换桌成功！")
	else
		--退出游戏，强退
		TOAST("换桌失败！",function ()
			CowGameController:getInstance():StrongbackGame()
		end)	
		
	end
end
return CowRoomController