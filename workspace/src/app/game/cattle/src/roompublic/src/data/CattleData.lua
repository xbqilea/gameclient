--
-- Author: 
-- Date: 2018-08-07 18:17:10
--

function CowfromDB( _key,fileName )
    local newdata = fromCowCfgLua(fileName)
    for k, v in pairs(newdata) do
        if tostring(v.id) == tostring(_key) then
            return v.tip
        end
    end
    return "" 
end


function fromCowCfgLua( fileName )
    return require("app.game.cattle.res.data."..fileName)
end


function CowNumberChange(_number )
		local result = ""
		if  _number  >= math.pow(10,8) then -- 亿
			result = math.ceil(_number / math.pow(10,8) ) .."亿"
		elseif _number  >= math.pow(10,4) then -- 万
			result = math.ceil(_number / math.pow(10,4) ) .."万"
		else
			result = _number .. ""
		end
		return result
end
--筹码数字
function CowChouMaNumberChange(_number )
		local result = ""
		if  _number  >= math.pow(10,8) then -- 亿
			result = math.ceil(_number / math.pow(10,8) ) .."亿"
		elseif _number  >= math.pow(10,4) then -- 万
			result = math.ceil(_number / math.pow(10,4) ) .."万"
		elseif _number  >= math.pow(10,3) then -- 千
			result = math.ceil(_number / math.pow(10,3) ) .."千"
		else
			result = _number .. ""
		end
		return result
end

--抢庄牛牛分数数字
-- 1）当数量小于万时，显示全部数量；	
-- 2）当数量大于等于万，小于百万时，显示万位，并显示1位小数，显示格式为“XX.X万”，
	-- 小数点后一位为向下取整，例如130900则显示13.0万，若小数位为0，则不显示小数位；	
-- 3）当数量大于等于百万，小于亿时，显示万位，不显示小数；	
-- 4）当数量大于等于亿时，显示亿位，并显示1位小数，若小数位为0，则不显示小数位；	
function RobCowScoreNumberChange(_number )
	local numbers = math.abs(_number)
	local result = ""
	local zheng
	local xiao
	local endNum 
	if  numbers  >= math.pow(10,8) then -- 亿
		numbers = math.floor(numbers / 10^7)  
		zheng,xiao = math.modf(numbers/10) 
		if xiao ~= 0 then
			result = (string.format("%.1f", zheng+xiao).."亿")  
		else
			result = (string.format("%d", zheng).."亿")  
		end
	elseif numbers  >= math.pow(10,6) then -- 百万 
		zheng,xiao = math.modf(numbers/10^4) 
		xiao = 0
		result = (string.format("%d", zheng).."万") 
	elseif numbers  >= math.pow(10,4) then -- 万
		numbers = math.floor(numbers / 10^3)  
		zheng,xiao = math.modf(numbers/10) 
		if xiao ~= 0 then
			result = (string.format("%.1f", zheng+xiao).."万")  
		else
			result = (string.format("%d", zheng).."万")  
		end
	else
		result = numbers .. ""
	end
	print("result = ",result)
	return result,zheng,xiao
end


function createLine( info )
    -- 内容
	local lable = ccui.Text:create()
    lable:setString(info.content)
    lable:setTextHorizontalAlignment( cc.TEXT_ALIGNMENT_LEFT)
    lable:setTextVerticalAlignment( cc.TEXT_ALIGNMENT_CENTER)
    lable:setFontSize( info.fontsize or 23 )
    lable:setTextColor( info.color or cc.c4b(76, 71, 68,255) )

	lable:ignoreContentAdaptWithSize(false)
	local tSize = lable:getContentSize() 
	local tmp_width = tSize.width 
	local lineCount = tmp_width / info.width --self.m_listview_chat:getContentSize().width
	local end_height = (math.ceil(lineCount) +1 )* (tSize.height) 
	lable:setContentSize(cc.size(info.width,end_height))
	return lable 
end
-- 组装描边的文字
function enablelineLable( _str,offset,fontSize,color,endcolor,linecount )
	local arrstr = ToolKit:char2Array( _str )
	local node = display.newNode()
	local posx = 0
	local posy = (fontSize or 18) * 0.5
	for i=1,#arrstr do 
		local lable = ccui.Text:create()
	    lable:setString(info.content)
	    lable:setTextHorizontalAlignment( cc.TEXT_ALIGNMENT_LEFT)
	    lable:setTextVerticalAlignment( cc.TEXT_ALIGNMENT_CENTER)
	    lable:setFontSize( fontSize or 18 )
	    lable:setTextColor( color or cc.c4b(76, 71, 68,255) )
	    lable:enableOutline(endcolor or cc.c4b(255, 255, 255,255) ,linecount or 1)
	    node:addChild(lable)
	    lable:setAnchorPoint(0,0.5)
	    lable:setPosition(cc.p(posx,posy))
	    posx = posx + offset + lable:getContentSize().width
	end
	posx = posx - offset 
	node:setContentSize( cc.size(posx,fontSize))
	return node
end

function getRoomCellScore( _gameAtomTypeId )
	local cellScore = 1
	local roomdata = RoomData:getRoomDataById( _gameAtomTypeId )
	if roomdata then
		local josn = json.decode(roomdata.roomMinScore)
		-- dump(josn)
        for k,v in pairs(josn) do
            cellScore = k 
        end
	end
	return cellScore
end