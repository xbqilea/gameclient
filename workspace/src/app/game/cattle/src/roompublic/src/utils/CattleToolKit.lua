
--
-- Author: 
-- Date: 2018-08-07 18:17:10
--
-- 错误提示公用接口
require("app.game.cattle.src.roompublic.src.def.CattleRoomDef")
CowToolKit = class("CowToolKit")
function CowToolKit:showErrorTip( __errorId, __callback )
    local data = getErrorTipById(__errorId)
    if data then
        local dlg = nil
        -- TOAST
        if data.type == 1 then
            if data.tip then
                TOAST(data.tip, __callback)
            end
        -- 提示弹窗
        else
            local params = {
                m_title = "提示",
                m_message =  data.tip or "",
                m_btnyes = "确定",
                m_closeBtn = true,
            }
            local DlgAlert = require("app.game.cattle.src.roompublic.src.common.CattleSignleDlgLayer")
            local dlg = DlgAlert.new( params ,__callback)
            dlg:showDialog()
        end
    else
        TOAST("错误码: " .. __errorId, __callback)
    end
end

-- 数字
function CowToolKit:shorterNumber( number )
    local ret = ""
    local originNumber = tonumber(number)
    if type(originNumber) == "number" then
        if originNumber < 10^4 then
            ret = tostring(originNumber)
        elseif originNumber < 10^6 then --大于一万,小于百万,单位取万
            local shortNumber = math.floor(originNumber/10^3)
            ret = math.ceil( shortNumber / 10 ).."万"
            -- ret = string.format("%.1f",shortNumber/10).."万"
        elseif originNumber < 10^8 then --大于百万,小于一亿 单位取万
            local shortNumber = math.floor(originNumber/10^4)
            ret = tostring(shortNumber).."万"
        elseif originNumber >= 10^8 then --大于一亿,单位取亿
            local shortNumber = math.floor(originNumber/10^7)
            ret = string.format("%.1f",shortNumber/10).."亿"
        end
    end
    return ret
end
--检查是否转正的提示框
function CowToolKit:toRegularAccount(dlg,funcId)
    local callback = function ()
        local _funcId = funcId or CowRoomDef.Fun.ZHZZ
        local data = fromFunction(_funcId)
        if data then
            local stackLayer = data.mobileFunction

            if stackLayer then
                sendMsg(MSG_GOTO_STACK_LAYER, {layer = stackLayer, name = data.name, direction = DIRECTION.HORIZONTAL})
            else
                TOAST("尚未发布, 敬请期待")
            end
        end
    end

    local params = {
        m_title = "温馨提示",
        m_message = "你的账号还没转正，无法使用此功能，是否前往账号转正?",
        m_btnyes = "确定" ,
        m_btnno = "取消" ,
    }

    local dlg = require("app.game.cattle.src.roompublic.src.common.CattleDoubleDlgLayer").new(params, callback)
    dlg:enableTouch()
    dlg:showDialog()
end
--检查是否绑定手机的提示框
function CowToolKit:toPhoneBunding(dlg,funcId)
    local params = {
        m_title = "温馨提示",
        m_message = "你的账号还没绑定手机，无法使用此功能，是否前往手机绑定?",
        m_btnyes = "确定" ,
        m_btnno = "取消" ,
        
    }

    local callback = function ()
        local _funcId = funcId or CowRoomDef.Fun.MBSJ
        local data = fromFunction(_funcId)
        if data then
            local stackLayer = data.mobileFunction

            if stackLayer then
                sendMsg(MSG_GOTO_STACK_LAYER, {layer = stackLayer, name = data.name, direction = DIRECTION.HORIZONTAL, funcId = _funcid})

            else
                TOAST("尚未发布, 敬请期待")
            end
        end
    end
    local dlg = require("app.game.cattle.src.roompublic.src.common.CattleDoubleDlgLayer").new(params, callback)
    dlg:enableTouch()
    dlg:showDialog()
end
-- 疯狂牛牛
function CowToolKit:isCrazyCow( _gameAtomTypeId )
    local id = _gameAtomTypeId
    local data = RoomData:getRoomDataById( id )
    if not data or not data.gameKindType then return  false end
    if data.gameKindType == CowRoomDef.CRAZYGameKindID then return true end -- gameKindType
end

function CowToolKit:isHappyCow( _id )
    local id = _id 
    local data = RoomData:getRoomDataById( id )
    if not data or not data.gameKindType then return  false end
    if data.gameKindType == CowRoomDef.HAPPYGameKindID then return true end -- gameKindType
end
-- 抢庄牛牛
function CowToolKit:isRobCow( _id )
    local id = _id 
    local data = RoomData:getRoomDataById( id )
    if not data or not data.gameKindType then return  false end
    if data.gameKindType == CowRoomDef.RobGameKindID then return true end -- gameKindType
end
-- 牌友房
function CowToolKit:isFriendCow( _id )
    local id = _id 
    local data = RoomData:getRoomDataById( id )
    if not data or not data.gameKindType then return false end
    if data.gameKindType == CowRoomDef.FriendCowGameKindID then return true end -- gameKindType
end

-- 牌友房
function CowToolKit:isFriendCowGameType( _kindID )
    if CowRoomDef.FriendCowGameKindID == _kindID then
        return true
    end
    return false
end
