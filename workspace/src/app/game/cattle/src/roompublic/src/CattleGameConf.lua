-- Author: 
-- Date: 2018-08-07 18:17:10
--
module(..., package.seeall)
CowGameConf = {}
CowGameConf.layerConf={
	["CowLoadingLayer"] = "app.game.cattle.src.roompublic.src.CattleLoadingLayer", -- loading 界面
	["CowGameRoomLevelCLayer"] = "app.game.cattle.src.roompublic.src.CattleGameRoomLevelCLayer" -- 选择桌子界面
}

return CowGameConf
