--
-- Author: 
-- Date: 2018-08-07 18:17:10
--
local CowGuideBaseLayer = require("app.game.cattle.src.roompublic.src.function.cowguide.CattleGuideBaseLayer")
local CowGuideLayer_5 = class("CowGuideLayer_5", function ()
    return CowGuideBaseLayer.new()
end)

function CowGuideLayer_5:ctor()
	self:myInit()
	self:setupViews()
end

function CowGuideLayer_5:myInit()
	self.m_root = nil
end

function CowGuideLayer_5:setupViews()
    self.m_root = UIAdapter:createNode( "cow_guide_csb/cow_public_guide_layer_5.csb")
    	-- 添加到层
    self:addChild(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    self:setEnableOutline()
end

function CowGuideLayer_5:setEnableOutline()
    self.m_root:getChildByName("btn_pass"):getTitleRenderer():enableOutline(cc.c4b(119, 77, 49, 255),3)
    self.m_root:getChildByName("btn_start"):getTitleRenderer():enableOutline(cc.c4b(9, 83, 84, 255),3)
end

function CowGuideLayer_5:guideEnd()
	print("guideEnd")
end

function CowGuideLayer_5:onTouchCallback( sender )
    local name = sender:getName()
    if name == "btn_start" then  -- 是
        print("btn_start")
		self:guideEnd()
		self:closeDialog()
    elseif name == "btn_pass" then  --否
		self:closeDialog()
    end
end

return CowGuideLayer_5