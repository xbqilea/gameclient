--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- CowSignleDlgLayer
-- 单按钮

local XbDialog = require("app.hall.base.ui.CommonView")

local CowSignleDlgLayer = class("CowSignleDlgLayer", function()
    return XbDialog.new()
end)

function CowSignleDlgLayer:ctor( _param,_handler )
	self:myInit()
	self:setupViews()   
	self:showDlgTip( _param,_handler ) 
end

function CowSignleDlgLayer:myInit()
	self.m_root = nil
	self.m_layout_bg = nil
	self.m_txt_tip = nil
	self.m_handler = nil
	self.m_message = nil
	self.m_btn_close = nil 
	self.m_btn_yes = nil
    self.m_txt_title = nil
    self.m_end_height = 0
    self.fontSize = 30
	self.m_size = cc.size(550,180)
end

function CowSignleDlgLayer:showCloseBtn()
	self.m_btn_close:setVisible(true)
end

function CowSignleDlgLayer:setupViews()
    self.m_root = UIAdapter:createNode("cow_publicroom_csb/cow_public_signle_dlg.csb")
    self:addChild(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    self.m_layout_bg = self.m_root:getChildByName("layout_bg")
    self.m_txt_tip = self.m_layout_bg:getChildByName("txt_tip")
    self.m_btn_close = self.m_layout_bg:getChildByName("btn_close")
    self.m_btn_yes = self.m_layout_bg:getChildByName("btn_yes") 
    self.m_txt_title = self.m_layout_bg:getChildByName("txt_title")
	self.m_btn_close:setVisible(false)

end

function CowSignleDlgLayer:showDlgTip( _param,_handler )

	local _message = _param.m_message or ""
	self.m_handler = _handler
	self.m_message = _message
    self.m_txt_title:setString( _param.m_title or "提示")
    self.m_txt_tip:setString( self.m_message )
    self.m_btn_yes:setTitleText( _param.m_btnyes or "确定")
    self.m_txt_tip:ignoreContentAdaptWithSize(false)
    self.m_btn_close:setVisible(_param.m_closeBtn)
    local tSize = self.m_txt_tip:getContentSize() 
    local tmp_width = tSize.width
    local linecount = (tmp_width / self.m_size.width )
    local end_height = (linecount )* (tSize.height)  
    if linecount > 1 then
        self.m_txt_tip:setTextHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT)
        self.m_txt_tip:setString(self.m_message )
        self.m_txt_tip:ignoreContentAdaptWithSize(false)
        tSize = self.m_txt_tip:getContentSize() 
        tmp_width = tSize.width
        linecount = math.ceil(tmp_width / self.m_size.width )
        end_height = (linecount )* (tSize.height) 
    else
        self.m_txt_tip:setTextHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER)
    end

    self.m_txt_tip:setContentSize(cc.size(self.m_size.width,end_height)) 
    self.m_end_height = end_height
end

function CowSignleDlgLayer:setHandler( _handler )
    self.m_handler = _handler
end

-- 点击事件回调
function CowSignleDlgLayer:onTouchCallback( sender )
    local name = sender:getName()
    print("name = ",name)
    if name == "btn_yes" then
    	if self.m_handler then
    		self.m_handler()
    	end
 		self:closeDialog()
 	elseif name == "btn_close" then
 		self:closeDialog()
    end 
end

function CowSignleDlgLayer:onExit()
   print("CowSignleDlgLayer:onExit")
end

return CowSignleDlgLayer