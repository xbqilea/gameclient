--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 二级游戏房间(自由场，比赛场，闯关赛)

local CowShareInfoLayer = require("app.game.cattle.src.roompublic.src.common.CattleShareInfoLayer")
local CowItemGameLevelB = require("app.game.cattle.src.roompublic.src.CattleItemGameLevelB")
local CowGuideLayer_1 = require("app.game.cattle.src.roompublic.src.function.cattleguide.CattleGuideLayer_1")
local scheduler = require("framework.scheduler")
local CowGameRoomLevelBLayer = class("CowGameRoomLevelBLayer", function ()
    return CowShareInfoLayer.new()
end)

function CowGameRoomLevelBLayer:ctor(__params)
    self:myInit( __params )
    self:setupViews()

    -- 新手任务
    TotalController:registerNetMsgCallback(self, Protocol.SceneServer, "CS_H2C_HandleMsg_Ack", handler(self, self.sceneNetMsgHandler))
    
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    self:addNodeEventListener(cc.NODE_EVENT, function(event)
        if event.name == "enter" then
            self:onEnter()
        elseif event.name == "exit" then
            -- self:onExit()
        elseif event.name == "enterTransitionFinish" then
            -- self:onEnterTransitionFinish()
        elseif event.name == "exitTransitionStart" then
            -- self:onExitTransitionStart()
        elseif event.name == "cleanup" then
            -- self:onDestory()
        end
    end)
    self:setLayerName("CowGameRoomLevelBLayer")
end

-- 初始化成员变量
function CowGameRoomLevelBLayer:myInit( __params )
    self.m_portalId = nil -- 游戏类型id(来自配表, 只用于显示)
    
    self.m_root = nil
    self.m_layout_bg = nil
    self.m_offsetX = 0 -- 一个B级按钮的间隔
    self.m_width = 0 -- 一个B级按钮的宽
    self.m_layoutPosX = {}
    self:initData(__params)
end

--结束定时器
function CowGameRoomLevelBLayer:endLastTimer()
    if self.m_lastTimer then
        scheduler.unscheduleGlobal(self.m_lastTimer)
        self.m_lastTimer = nil
    end
end

function CowGameRoomLevelBLayer:onEnter()
    print("CowGameRoomLevelBLayer:onEnter")
    self:sendCS_C2S_OxSelScene_Last_EnterRoom_Req()
end

-- 请求上一次玩过的游戏ID
function CowGameRoomLevelBLayer:sendCS_C2S_OxSelScene_Last_EnterRoom_Req()
    ConnectManager:send2SceneServer( CowRoomDef.GUIDE,"CS_C2S_OxSelScene_Last_EnterRoom_Req",nil)
end

function CowGameRoomLevelBLayer:onEndAnimation()
    function delayerTime()
        self:endLastTimer()
        ConnectManager:send2SceneServer( CowRoomDef.GUIDE,"CS_C2S_OxSelScene_XinShou_Req")
    end
    if self.m_lastTimer == nil then
        self.m_lastTimer = scheduler.scheduleGlobal(delayerTime,0.1)
    end
end

function CowGameRoomLevelBLayer:onPushAnimation( __finishCallback )
    CowUIHelp:getInstance():showRoomPushAction(self.m_layout_bg, __finishCallback )
end

function CowGameRoomLevelBLayer:onPopAnimation( __finishCallback  )
    CowUIHelp:getInstance():showRoomPopAction(self.m_layout_bg, __finishCallback )
end

function CowGameRoomLevelBLayer:layoutAni()
    self.m_layout_bg:setVisible(true)
    local action = cc.ScaleTo:create(0.15,1)
    self.m_layout_bg:setScale(0.5)
    self.m_layout_bg:runAction(action)
end

function CowGameRoomLevelBLayer:sceneNetMsgHandler( __idStr, __info )
    if __idStr == "CS_H2C_HandleMsg_Ack" then
        if __info.m_result == 0 then
                -- dump(__info)
                local gameAtomTypeId = __info.m_gameAtomTypeId
                if __info.m_message  and __info.m_message[1] then
                    local cmdId = __info.m_message[1].id
                    local info = __info.m_message[1].msgs
                    if cmdId == "CS_S2C_OxSelScene_XinShou_Ack"  then  -- 0未引导，1完成
                        -- dump(info)
                        if info.m_result == 0 then
                            g_hasEndGuide = false
                        else
                            g_hasEndGuide = true
                        end
                            if  g_hasEndGuide == false then
                                g_hasEndGuide = true
                                CowGuideLayer_1.new():showGuide()
                            end
                        -- self:showCowGuide()
                    elseif cmdId == "CS_S2C_OxSelScene_Last_EnterRoom_Ack"  then  -- 0未引导，1完成
                        g_cowLastEnterRoomID = info.m_roomId
                    end
                end
        else
            -- TOAST("找不到场景信息")
            ToolKit:showErrorTip(__info.m_result)
            -- print("找不到场景信息")
        end
    end
end

function CowGameRoomLevelBLayer:showCowGuide()
    if g_hasEndGuide == false then
        g_hasEndGuide = true
        CowGuideLayer_1.new():showGuide()
    end
end

-- 初始化界面
function CowGameRoomLevelBLayer:setupViews()
    self.m_root = UIAdapter:createNode( "cow_publicroom_csb/cow_room_level_b_layer.csb")
    self:setLayoutContent(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    self.m_layout_bg = self.m_root:getChildByName("layout_bg")
    for i=1,3 do
        local _layout = self.m_layout_bg:getChildByName("layout_"..i)
        self.m_layoutPosX[#self.m_layoutPosX+1] = _layout:getPositionX()
    end
    -- print("#self.m_games = ",#self.m_games)
    for i = 1, #self.m_games do
    	self.m_layout[i] = self.m_layout_bg:getChildByName("layout_"..i)
        self.m_width = self.m_layout[i]:getContentSize().width
    	self.m_itemLevelB[i] = CowItemGameLevelB.new(self.m_games[i],i)
        self.m_itemLevelB[i]:setPosition(cc.p(self.m_width*0.5,self.m_layout[i]:getContentSize().height*0.5))
    	self.m_layout[i]:addChild(self.m_itemLevelB[i])
    end
    self:setHideNode( self.m_layout_bg )
    self:updatePos()
    -- self.m_layout_bg:setVisible(false)
end

-- 刷新界面
function CowGameRoomLevelBLayer:initData( __params )
    self.m_games = {}
    self.m_layout = {}
    self.m_itemLevelB = {}
    table.sort( __params._info.m_portalList, function( a,b )
            return a.m_sort < b.m_sort
    end )
    if self.m_protalId then
        if self.m_protalId ~= __params._info.m_portalId then
            -- print("Error Cow CowGameRoomLevelBLayer!!!")
        end
    end
    local data = fromFunction(__params._info.m_portalId)
    self:setProtalId(__params._info.m_portalId )
    self:setPreviousProtalId(data.father )
    -- print("self.m_portalId = ",self.m_protalId)
    -- dump(__params._info.m_portalList)
    for k, v in pairs(__params._info.m_portalList) do
        -- dump(v,"v")
        local data = RoomData:getGameDataById(v.m_portalId)
        if data then -- and data.father == self.m_portalId
            -- dump(v.m_attrList1)
            for i = 1, #v.m_attrList1 do
                if v.m_attrList1[i].m_pid == 1 then
                    data["peopleCnt"] = v.m_attrList1[i].m_value
                end
            end
            self.m_games[#self.m_games+1] = data
        end   
    end
end

function CowGameRoomLevelBLayer:updategameDataNty( __params )
    for i=1,#self.m_games do
        self.m_layout[i]:removeAllChildren()
    end
    self:initData(__params)
    for i = 1, #self.m_games do
        -- dump(self.m_games)
        print("i = ",i)
        self.m_layout[i] = self.m_layout_bg:getChildByName("layout_"..i)
        self.m_width = self.m_layout[i]:getContentSize().width
        self.m_itemLevelB[i] = CowItemGameLevelB.new(self.m_games[i],i)
        self.m_itemLevelB[i]:setPosition(cc.p(self.m_width*0.5,self.m_layout[i]:getContentSize().height*0.5))
        self.m_layout[i]:addChild(self.m_itemLevelB[i])
    end
    self:updatePos()
end

function CowGameRoomLevelBLayer:updatePos()
    if #self.m_games % 2 == 1 then -- 奇数
        if #self.m_games == 1 then
            self.m_layout[1]:setPositionX(self.m_layoutPosX[2])
        else-- 3个的话，不用重新设置
            for i=1,3 do
                self.m_layout[i]:setPositionX(self.m_layoutPosX[i])
            end
        end
    elseif #self.m_games % 2 == 0 then -- 偶数
        if #self.m_games == 2 then
            self.m_offsetX = 104 + self.m_width*0.5
            self.m_layout[1]:setPositionX(display.cx - self.m_offsetX )
            self.m_layout[2]:setPositionX(display.cx + self.m_offsetX )
        end
    end
end

function CowGameRoomLevelBLayer:onTouchCallback( sender )
    local name = sender:getName()
    if name == "btn_back" then
        print("btn_back")
    end
end

function CowGameRoomLevelBLayer:onDestory()
    -- 注销消息
    -- removeMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS)
    self:endLastTimer()
    TotalController:removeNetMsgCallback(self, Protocol.SceneServer, "CS_H2C_HandleMsg_Ack")
end

return CowGameRoomLevelBLayer
