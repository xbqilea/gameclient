--
-- User: lhj
-- Date: 2018/7/6 
-- Time: 20:21
-- 排行榜

local XbDialog = require("app.hall.base.ui.CommonView")
local UserCenterHeadIcon = require("app.hall.userinfo.view.UserHeadIcon")

local RankCell = import(".RankCell")

local RankMainLayer = class("RankMainLayer", function ()
    return XbDialog.new()
end)

local FUNC_TYPE = {
    INCOME              =              1 , --今日收入榜
    PHONE               =              2,  --今日话费榜
}

local FUNC_IMAGE = {
    {x = 106, y = 32},
    {x = 352, y = 32},
}

function RankMainLayer:ctor(gameId, control)
    self.m_GameId = gameId
    self.m_Control = control
    self:myInit()
    self.m_FuncType  = FUNC_TYPE.INCOME
    self:setupViews()
end

function RankMainLayer:myInit()
    ToolKit:registDistructor(self, handler(self, self.onDestroy))
    addMsgCallBack(self, MSG_TODAY_RANK_ASK, handler(self, self.updateTodayRankInfo))
    self.m_CurPage = 1  --当前页数
end

function RankMainLayer:setupViews()
    self.node = UIAdapter:createNode("res/csb/rank/rank_main_layer.csb")
    self:addChild(self.node)
    UIAdapter:adapter(self.node, handler(self, self.onTouchCallback))

    if not GlobalRankController then
       GlobalRankController = require("app.hall.rank.control.RankController").new()
    end

    performWithDelay(self,function()
            GlobalRankController:reqTdRankList({2, 1, 1}, Player:getAccountID())
        end, 0.01)
    self:showFuncType()
    self.node:getChildByName("champion_layout"):setVisible(false)

    self.node:getChildByName("item_layout"):setVisible(false)
    self.node:getChildByName("left_layout"):setPositionY(self.node:getChildByName("left_layout"):getPositionY() + 35)
    self.node:getChildByName("info_layout"):setPositionY(self.node:getChildByName("info_layout"):getPositionY() + 35)

end

function RankMainLayer:showFuncType()
    self.node:getChildByName("light_spr"):setPosition(cc.p(FUNC_IMAGE[self.m_FuncType].x, FUNC_IMAGE[self.m_FuncType].y))
    self.node:getChildByName("phoneNode"):setVisible(self.m_FuncType == FUNC_TYPE.PHONE)
    self.node:getChildByName("incomeNode"):setVisible(self.m_FuncType == FUNC_TYPE.INCOME)
end

function RankMainLayer:updateTodayRankInfo()

    self.m_TdPhoneRankData = GlobalRankController:getTdPhoneRankDatas()
    self.m_TdIncomeRankData = GlobalRankController:getTdIncomeRankDatas()

    local data = self.m_FuncType == FUNC_TYPE.PHONE and self.m_TdPhoneRankData or self.m_TdIncomeRankData
    if data == nil or table.nums(data) == 0 then return end

    self:showMyRankInfo()
    self:showChampionInfo()

    if self.m_FuncType == FUNC_TYPE.INCOME then
        self:showIncomeRankInfo()
    else
        self:showPhoneRankInfo()
    end

end

function RankMainLayer:createItem(index)
    local node = nil
    node = RankCell.new()
    node:setMainLayer(self)
    return node
end

function RankMainLayer:getTableViewData(modelData, page)
    local data = {}

    local union_Data = {}
    if page*4 >= #modelData then
        union_Data = modelData
    else
        for i=1, page*4 do
            table.insert(union_Data, modelData[i])
        end
    end

    for i=1, #union_Data do
        local cellData = {}
        cellData.data = union_Data
        cellData.size = cc.size(680, 90)
        table.insert(data, cellData)
    end

    table.insert(data, {})

    return data
end

function RankMainLayer:refreshScrollView()

    if self.m_FuncType == FUNC_TYPE.INCOME then
        if self.m_IncomeScrollView then
            local mmtData = self:getTableViewData(self.m_TdIncomeRankData.m_userList, self.m_CurPage)
            self.m_IncomeScrollView:setViewData(mmtData)
            self.m_IncomeScrollView:reloadData()
            self.m_IncomeScrollView:setItemMiddle((self.m_CurPage - 1)* 4  + 1)
        end 
    else
        if self.m_PhoneScrollView then
            local mmtData = self:getTableViewData(self.m_TdPhoneRankData.m_userList, self.m_CurPage)
            self.m_PhoneScrollView:setViewData(mmtData)
            self.m_PhoneScrollView:reloadData()
            self.m_PhoneScrollView:setItemMiddle((self.m_CurPage - 1)* 4  + 1)
        end
    end
end

function RankMainLayer:showMyRankInfo()

    local data = self.m_FuncType == FUNC_TYPE.PHONE and self.m_TdPhoneRankData or self.m_TdIncomeRankData
    if data == nil then return end
    local myRank = data.m_myRank
    local myRankResNum = data.m_myRankResNum
    self.node:getChildByName("my_rank"):setString(myRank == 0 and "未入榜" or myRank)
    self.node:getChildByName("my_spr"):setSpriteFrame(display.newSpriteFrame(self.m_FuncType == FUNC_TYPE.PHONE and "rank_tubiao_hf.png" or "public_icon_gold.png"))
    self.node:getChildByName("my_income"):setString(myRankResNum)
    self.node:getChildByName("myname"):setString(ToolKit:shorterString(Player:getNickName(),7))

    local my_head_layout = self.node:getChildByName("my_head_layout")
    my_head_layout:removeAllChildren()
    local _head_bg_size = my_head_layout:getContentSize()

    self._img_head = UserCenterHeadIcon.new( {_size =_head_bg_size ,_clip = true })
    self._img_head:setAnchorPoint(cc.p(0.5,0.5))
    my_head_layout:addChild(self._img_head)
    self._img_head:setPosition(_head_bg_size.width*0.5,_head_bg_size.height*0.5)
    self._img_head:updateTexture(Player:getFaceID(),Player:getAccountID())

end

function RankMainLayer:showPhoneRankInfo()
    local data = self.m_TdPhoneRankData.m_userList
    if  table.nums(data) <= 1  then
        return
    end

    local modelData = self:getTableViewData(self.m_TdPhoneRankData.m_userList, self.m_CurPage)

    self.m_PhoneScrollView = ToolKit:createTableView(cc.size(680,360),cc.size(680, 90), modelData, handler(self, self.createItem))
    self.m_PhoneScrollView:addTo(self.node:getChildByName("phone_tableview"))
end

function RankMainLayer:showIncomeRankInfo()
    local data = self.m_TdIncomeRankData.m_userList
    if  table.nums(data) <= 1  then
        return
    end 

    local modelData = self:getTableViewData(self.m_TdIncomeRankData.m_userList, self.m_CurPage)

    self.m_IncomeScrollView = ToolKit:createTableView(cc.size(680,360),cc.size(680, 90), modelData, handler(self, self.createItem))
    self.m_IncomeScrollView:addTo(self.node:getChildByName("income_tableview"))

end

function RankMainLayer:showChampionInfo()
    local data = self.m_FuncType == FUNC_TYPE.PHONE and self.m_TdPhoneRankData.m_userList or self.m_TdIncomeRankData.m_userList
    if table.nums(data) == 0 then
        self.node:getChildByName("champion_layout"):setVisible(false)
    else
        self.node:getChildByName("champion_layout"):setVisible(true)
        self.node:getChildByName("player_name"):setString(ToolKit:shorterString(data[1].m_strNick,7))
        self.node:getChildByName("number"):setString(data[1].m_nTodayNum)

        self.node:getChildByName("image_icon"):setSpriteFrame(display.newSpriteFrame(self.m_FuncType == FUNC_TYPE.PHONE and "rank_zi_hfsr.png" or "rank_zi_yljb.png"))

        local head_layout = self.node:getChildByName("head_layout")
        head_layout:removeAllChildren()
        local _head_bg_size = head_layout:getContentSize()

        local img_head = UserCenterHeadIcon.new( {_size =_head_bg_size ,_clip = true })
        img_head:setAnchorPoint(cc.p(0.5,0.5))
        head_layout:addChild(img_head)
        img_head:setPosition(_head_bg_size.width*0.5,_head_bg_size.height*0.5)
        img_head:updateTexture(data[1].m_nAvatarId, data[1].m_nAccountId)
    end
end

function RankMainLayer:showFunBtnBack()
    self:showFuncType()
    self:showMyRankInfo()
    self:showChampionInfo()
end

function RankMainLayer:showScrollView()
    self.m_CurPage = 1
    if self.m_FuncType == FUNC_TYPE.INCOME then

        if self.m_IncomeScrollView then
            self.m_IncomeScrollView:setVisible(true)
        end
        if self.m_PhoneScrollView then
            self.m_PhoneScrollView:setVisible(false)
        end
    else
        if self.m_PhoneScrollView then
            self.m_PhoneScrollView:setVisible(true)
        end

        if self.m_IncomeScrollView then
            self.m_IncomeScrollView:setVisible(false)
        end
    end
end

-- 点击事件回调
function RankMainLayer:onTouchCallback(sender)
    local name = sender:getName()
    if name == "exitBtn" then
        self:closeDialog()
    elseif name == "func_phone" then
        self.m_FuncType = FUNC_TYPE.PHONE
        if table.nums(GlobalRankController:getTdPhoneRankDatas()) > 0 then
            self:showFunBtnBack()
            self:showScrollView()
        else
            self:showFuncType()
            self:showScrollView()
            self.node:getChildByName("champion_layout"):setVisible(false)
            GlobalRankController:reqTdRankList({self.m_GameId, 2, 1}, Player:getAccountID())
        end
    elseif name == "func_income" then
        self.m_FuncType = FUNC_TYPE.INCOME
        self:showFunBtnBack()
        self:showScrollView()
    end
end

function RankMainLayer:onDestroy()
    --清除排行榜数据
    GlobalRankController:removeTdPhoneRankDatas()
    GlobalRankController:removeTdIncomeRankDatas()

    removeMsgCallBack(self, MSG_TODAY_RANK_ASK)
end

return RankMainLayer