--
-- Author: 
-- Date: 2018-08-07 18:17:10
--

local GameLoadingLayer = require("app.game.common.util.GameLoadingLayer")
local CowRoomResources = require("app.game.cattle.src.roompublic.src.CattleRoomResources")
local CowGameResources = require("app.game.cattle.src.roompublic.src.CattleGameResources")
local scheduler = require("framework.scheduler")

local nextLayer= 
{
	[1]= "app.game.cattle.src.roompublic.src.newviews.CattleNewGameRoomLevelBLayer", 
	[2]= "app.game.cattle.src.roompublic.src.CattleGameRoomLevelCLayer", -- 疯狂牛牛进入房间界面
	[3]= "app.game.cattle.src.crazycattle.src.scene.GameScene",
}

local CowLoadingLayer = class("CowLoadingLayer", function ( __params )
	return GameLoadingLayer.new(__params)
end)

function CowLoadingLayer:ctor( __params )
	
	--dump(__params, "__params", 10)
	
	self:myInit()
    self:setNodeEventEnabled(true)
    self.gameInfo = __params._info
    self:setupViews()
    -- dump(__params, "desciption")
    -- dump(__params._info)
    local mainRoomData = RoomData:getGameDataById(__params._info.m_portalId)
    print("mainRoomData.funcId = ",mainRoomData.funcId)
    if #__params._info.m_portalList > 0 then -- 还有下一级
    	if  __params._info.m_portalList[1] then
    		local subRoomData = RoomData:getGameDataById(__params._info.m_portalList[1].m_portalId)
    		if subRoomData.funcId  ~= "" then -- 有子集，最末一级
    			self._nextLayer = nextLayer[1]
    		else -- 还有下一级
    			self._nextLayer = nextLayer[2]
    		end
    	end
	else 
		print("-- 牛牛没有发游戏房间 -- ")
		self._nextLayer = nextLayer[1]
    end
    print("self._nextLayer::::", self._nextLayer, __params._info.m_portalId)
	self:setNextlayer(self._nextLayer)

end

function CowLoadingLayer:myInit()
	self.m_root = nil
	self.m_layout_bottom = nil
	self.m_img_loadingbg = nil
	self.m_img_loading = nil
	self.m_img_fenge = nil
	self.m_txt_loading = nil
	self.m_img_loadingbgSize = nil
	self.curIndex = 1

end

function CowLoadingLayer:setupViews()
	self.m_root = UIAdapter:createNode( "cow_loading_csb/cow_loading_layer.csb")
    self:addChild( self.m_root )
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
	self.m_layout_bottom = self.m_root:getChildByName("layout_bottom")
	
	self.m_img_loadingbg = self.m_layout_bottom:getChildByName("img_loadingbg")

	self.m_img_loading = self.m_layout_bottom:getChildByName("img_loading")
	self.m_img_loading:setPercent(0)
end

function CowLoadingLayer:onTouchCallback( sender )
	
end

function CowLoadingLayer:startLoadRes()
	local allResourcesTb =  {}
	local tRes = { 
					{ list = CowRoomResources, name = "房间资源加载中" },
					{ list = CowGameResources, name = "游戏资源加载中" },
				}
	for k,v in pairs(tRes) do
		local list = v.list[v.list.TableName]
		for i=1,#list do
			allResourcesTb[ #allResourcesTb +1 ] = list[i]
		end
	end

	self.m_tRes = {TableName = "allResources", allResources = allResourcesTb, name = "牛牛所有资源"}
	self:preloadResources( self.m_tRes, handler(self, self.onLoadCompleted), handler(self, self.onLoading) )
    self:setPercent( 0 )
end

function CowLoadingLayer:setPercent(percent)
	self.m_img_loading:setPercent(percent)
end

function CowLoadingLayer:onLoadCompleted()

	if self.gameInfo.reconnect then -- 此处是重连游戏, 不进入下一个界面, 直接调用回调
		self.gameInfo.callback()
	else
		self:gotoNextLayer(self.gameInfo, DIRECTION.HORIZONTAL)
	end
end

function CowLoadingLayer:onLoading( __sum, __index, __name )
	print("全部" .. __sum .. "个文件需加载, 当前正在加载第" .. __index .. "个文件" .. __name)
    self:setPercent(__index/__sum*100)
    if __sum == __index + 1 then
		self:setPercent(100)
    end
end

function CowLoadingLayer:onExit()
    
end



return CowLoadingLayer