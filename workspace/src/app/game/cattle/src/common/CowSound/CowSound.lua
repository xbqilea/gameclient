--
-- Author: song hua
-- Date: 2017-03-08 20:38:26
-- CowSound
local CowSound = class("CowSound")
require("app.game.cattle.src.roompublic.src.def.CattleRoomDef")
CowSound.instance = nil

CowSound.selected = ccui.CheckBoxEventType.selected

CowSound.unselected = ccui.CheckBoxEventType.unselected

CowSound.m_gamename = 
{
	[CowRoomDef.CRAZYCOW] = "crazycow", -- 疯狂牛牛
	[CowRoomDef.RobCow] = "robcow", -- 抢庄牛牛
}



CowSound.m_effect =  --音效
{	
	OxButton = "sound/OxButton.mp3", -- 按钮
	OxChooseJetton = "sound/OxChooseJetton.mp3", --选择筹码  1
	OxJetton = "sound/OxJetton.mp3",
	OxAllWin = 	"sound/OxAllWin.mp3", --全胜  1
	OxAllLose = "sound/OxAllLose.mp3", -- 全输   1
	OxGameEnd = "sound/OxGameEnd.mp3", -- 游戏结束
	OxBankerWin = "sound/OxBankerWin.mp3", -- 庄家赢   1
	OxHeJi = "sound/OxHeJi.mp3", -- 最后胜负合计   1
	OxOpenCard = "sound/OxOpenCard.mp3", -- 开牌
	OxStartAddScore = "sound/OxStartAddScore.mp3", -- 开始下注
	OxStopAddScore = "sound/OxStopAddScore.mp3", -- 停止下注
	OxWaitBanker = "sound/OxWaitBanker.mp3", -- 等待庄家
	OxWuNiu = "sound/OxWuNiu.mp3", --无牛  1
	OxNiu1 = "sound/OxNiu1.mp3", --牛1     1
	OxNiu2 = "sound/OxNiu2.mp3", --牛2     1
	OxNiu3 = "sound/OxNiu3.mp3", --牛3  
	OxNiu4 = "sound/OxNiu4.mp3", --牛4
	OxNiu5 = "sound/OxNiu5.mp3", --牛5
	OxNiu6 = "sound/OxNiu6.mp3", --牛6
	OxNiu7 = "sound/OxNiu7.mp3", --牛7
	OxNiu8 = "sound/OxNiu8.mp3", --牛8
	OxNiu9 = "sound/OxNiu9.mp3", --牛9
	OxNiuNiu = "sound/OxNiuNiu.mp3", --牛牛 10
	OxYinNiu = "sound/OxYinNiu.mp3", --银牛 11
	OxJinNiu = "sound/OxJinNiu.mp3", --金牛 12
	OxBomb = "sound/OxBomb.mp3", --炸弹     13
	----------------- 花式牛牛 音效 ----------
	Oxcaijin = "sound/Oxcaijin.mp3", -- 彩金
	Oxhulu = "sound/Oxhulu.mp3", -- 葫芦
	OXliangdui = "sound/OXliangdui.mp3", -- 两对
	Oxshunzi = "sound/Oxshunzi.mp3", -- 顺子
	Oxsantiao = "sound/Oxsantiao.mp3", -- 三条
	Oxtonghua = "sound/Oxtonghua.mp3", -- 同花
	Oxtonghuashun = "sound/Oxtonghuashun.mp3", -- 同花顺
	Oxwuxiaoniu = "sound/Oxwuxiaoniu.mp3", -- 五小牛
	------------------ 抢庄牛牛 ---------------------
	OXqzgame_lost = "sound/OXqzgame_lost.mp3", -- 
	OXqzgame_start = "sound/OXqzgame_start.mp3", -- 
	OXqzgame_win = "sound/OXqzgame_win.mp3", -- 
	OXqzgold = "sound/OXqzgold.mp3", -- 
	OXqzqiangzhuang = "sound/OXqzqiangzhuang.mp3", -- 
	OXqzyouxian = "sound/OXqzyouxian.mp3", -- 
	---------------- 新增 -------------
	Oxbuqiang = "sound/Oxbuqiang.mp3",
	Oxliangbei = "sound/Oxliangbei.mp3",
	Oxsanbei = "sound/Oxsanbei.mp3",
	Oxsibei = "sound/Oxsibei.mp3",
	Oxxiazhu = "sound/Oxxiazhu.mp3",
	Oxyibei = "sound/Oxyibei.mp3",
	OxSelfWin = "sound/OxSelfWin.mp3",
	OxSelfLose = "sound/OxSelfLose.mp3",
	OxCountTime = "sound/OxCountTime.wav",
	Ox_Card_Fly = "sound/Ox_Card_Fly.wav",
}


CowSound.m_music = --音乐
{
	OxBg = "sound/OxBg.mp3",
	Oxroombg = "sound/Oxroombg.mp3", -- 背景
	OXqzmusic_game = "sound/OXqzmusic_game.mp3", --
	OXHappy = "sound/Oxcjbg.mp3", -- 
}

function CowSound:getInstance()
	if CowSound.instance == nil then
		CowSound.instance = CowSound.new()
	end
	return CowSound.instance
end

function CowSound:ctor()
   self.m_gameNameType = CowRoomDef.CRAZYCOW
   self.m_music_type = "OxBg"
   self.m_CowSoundMusicPath = "sound/OxBg.mp3"
   self:initCowSoundState()
end

function CowSound:setGameNameType( _gameNameType )
	self.m_gameNameType = _gameNameType
end
function CowSound:initCowSoundState()
	-- for k,v in pairs(CowSound.m_gamename) do
	-- 	--背景音乐
	-- 	g_GameMusicUtil:setMusicStatus(CowSound.selected)
	-- 	--音效
	-- 	g_GameMusicUtil:setSoundStatus(CowSound.selected)
	-- end 
end
--播放音乐和音效
function CowSound:playEffect( effect_type ,loop) 
	local soundPath = self:getEffectPath(effect_type)
	g_AudioPlayer:playEffect(soundPath, loop or false) 
end

function CowSound:playBgMusic( music_type )
	self.m_music_type = music_type 
	local soundPath = self:getMusicPath(music_type)
	g_AudioPlayer:playMusic(soundPath, true)
	print("Sound.playBgMusic = ".. soundPath) 
end
 

function CowSound:getEffectPath( effect_type )
	local CowSoundEffectPath = CowSound.m_effect[effect_type]
	print("CowSoundEffectPath:",CowSoundEffectPath)
	return CowSoundEffectPath
end

function CowSound:getMusicPath( music_type )
	-- local CowSoundMusicPath = CowSound.m_music[music_type]
	print("CowSoundMusicPath:",self.m_CowSoundMusicPath)
	return self.m_CowSoundMusicPath
end

function CowSound:setMusicPath( _SoundMusicPath )
	self.m_CowSoundMusicPath = _SoundMusicPath
end

function CowSound.onExit()
	g_AudioPlayer:stopMusic() 
	CowSound.instance = nil

end

return CowSound