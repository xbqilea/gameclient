module(..., package.seeall)
--------------看牌抢庄(LB:lookBid)----------------------
--等待开始场景通知
CS_G2C_OxFriend_LB_WaitStartScene_Nty =
{
	{ 1		, 1     , 'm_priorityChair' , 'INT'        , 1    , '优先者'},
}
--抢庄场景通知
CS_G2C_OxFriend_LB_ApplyBankerScene_Nty =
{
	{ 1		, 1     , 'm_priorityChair' , 'INT'        , 1    , '优先者'},
	{ 2		, 1     , 'm_isApply' , 'INT'        , 1    , '是否已抢庄 0:否 1:是'},
	{ 3		, 1     , 'm_applyTimes' , 'INT'        , 5    , '抢庄倍数'},
	{ 4		, 1     , 'm_selfCard' , 'UBYTE'        , 5    , '自己扑克'},
	{ 5		, 1     , 'm_applyData' , 'PstOxBidApplyData'        , 5    , '抢庄信息'},
}
--下分场景通知
CS_G2C_OxFriend_LB_AddScoreScene_Nty =
{
	{ 1		, 1     , 'm_priorityChair' , 'INT'        , 1    , '优先者'},
	{ 2		, 1     , 'm_bankerChair' , 'INT'        , 1    , '庄家椅子'},
	{ 3		, 1     , 'm_bankerTimes' , 'INT'        , 1    , '庄家倍数'},
	{ 4		, 1     , 'm_isAdd' , 'INT'        , 1    , '是否已下注 0:否 1:是'},
	{ 5		, 1     , 'm_addTimes' , 'INT'        , 5    , '下分倍数'},
	{ 6		, 1     , 'm_selfCard' , 'UBYTE'        , 5    , '自己扑克'},
	{ 7		, 1     , 'm_addScoreData' , 'PstOxBidAddData'        , 5    , '下分信息'},
}
--结束场景通知
CS_G2C_OxFriend_LB_GameEndScene_Nty =
{
	{ 1		, 1     , 'm_priorityChair' , 'INT'        , 1    , '优先者'},
	{ 2		, 1     , 'm_bankerChair' , 'INT'        , 1    , '庄家椅子'},
	{ 3		, 1     , 'm_bankerTimes' , 'INT'        , 1    , '庄家倍数'},
	{ 4		, 1     , 'm_addScoreData' , 'PstOxBidAddData'        , 5    , '下分信息'},
	{ 5		, 1     , 'm_selfCard' , 'UBYTE'        , 5    , '自己扑克'},
	{ 6		, 1     , 'm_endData' , 'PstOxBidEndData'        , 5    , '结束数据'},
}
--开始抢庄通知
CS_G2C_OxFriend_LB_StartApplyBanker_Nty =
{
	{ 1		, 1     , 'm_applyTimes' , 'INT'        , 5    , '抢庄倍数'},
	{ 2		, 1     , 'm_selfCard'  	, 'UBYTE'        		, 5    , '自己扑克'},
}
--玩家抢庄申请
CS_C2G_OxFriend_LB_ApplyBanker_Req =
{
	{ 1		, 1     , 'm_times'  	, 'INT'        		, 1    , '抢庄倍数 0:不抢'},
}
--玩家抢庄响应
CS_G2C_OxFriend_LB_ApplyBanker_Ack =
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '抢庄结果'},
	{ 2		, 1     , 'm_times'  	, 'INT'        		, 1    , '抢庄倍数'},
}
--玩家抢庄通知
CS_G2C_OxFriend_LB_ApplyBanker_Nty =
{
	{ 1		, 1     , 'm_applyData'  	, 'PstOxBidApplyData'        		, 5    , '抢庄信息'},
}
--开始下分通知
CS_G2C_OxFriend_LB_StartAddScore_Nty =
{
	{ 1		, 1     , 'm_bankerChair' , 'INT'        , 1    , '庄家椅子'},
	{ 2		, 1     , 'm_bankerTimes' , 'INT'        , 1    , '庄家倍数'},
	{ 3		, 1     , 'm_addTimes' , 'INT'        , 5    , '下分倍数'},
}
--玩家下分申请
CS_C2G_OxFriend_LB_AddScore_Req =
{
	{ 1		, 1     , 'm_times'  	, 'INT'        		, 1    , '下分倍数'},
}
--玩家下分响应
CS_G2C_OxFriend_LB_AddScore_Ack =
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '下分结果'},
	{ 2		, 1     , 'm_times'  	, 'INT'        		, 1    , '下分倍数'},
}
--玩家下分通知
CS_G2C_OxFriend_LB_AddScore_Nty =
{
	{ 1		, 1     , 'm_chairId'  	, 'INT'        		, 1    , '椅子ID'},
	{ 2		, 1     , 'm_times'  	, 'INT'        		, 1    , '下分倍数'},
}
--游戏结束
CS_G2C_OxFriend_LB_GameEnd_Nty =
{
	{ 1		, 1     , 'm_selfOpenCard'  	, 'UBYTE'        		, 1    , '自己开牌数据'},
	{ 2		, 1     , 'm_endData' , 'PstOxBidEndData'        , 5    , '结束数据'},
}
--优先抢庄刷新
CS_G2C_OxFriend_LB_PriorityUpdate_Nty =
{
	{ 1		, 1     , 'm_chairId'  	, 'INT'        		, 1    , '椅子号'},
}
--------------暗牌抢庄(NLB:NotLookBid)----------------------
--等待开始场景通知
CS_G2C_OxFriend_NLB_WaitStartScene_Nty =
{
	{ 1		, 1     , 'm_priorityChair' , 'INT'        , 1    , '优先者'},
}
--抢庄场景通知
CS_G2C_OxFriend_NLB_ApplyBankerScene_Nty =
{
	{ 1		, 1     , 'm_priorityChair' , 'INT'        , 1    , '优先者'},
	{ 2		, 1     , 'm_isApply' , 'INT'        , 1    , '是否已抢庄 0:否 1:是'},
	{ 3		, 1     , 'm_applyTimes' , 'INT'        , 5    , '抢庄倍数'},
	{ 4		, 1     , 'm_selfCard' , 'UBYTE'        , 5    , '自己扑克'},
	{ 5		, 1     , 'm_applyData' , 'PstOxBidApplyData'        , 5    , '抢庄信息'},
}
--下分场景通知
CS_G2C_OxFriend_NLB_AddScoreScene_Nty =
{
	{ 1		, 1     , 'm_priorityChair' , 'INT'        , 1    , '优先者'},
	{ 2		, 1     , 'm_bankerChair' , 'INT'        , 1    , '庄家椅子'},
	{ 3		, 1     , 'm_bankerTimes' , 'INT'        , 1    , '庄家倍数'},
	{ 4		, 1     , 'm_isAdd' , 'INT'        , 1    , '是否已下注 0:否 1:是'},
	{ 5		, 1     , 'm_addTimes' , 'INT'        , 5    , '下分倍数'},
	{ 6		, 1     , 'm_selfCard' , 'UBYTE'        , 5    , '自己扑克'},
	{ 7		, 1     , 'm_addScoreData' , 'PstOxBidAddData'        , 5    , '下分信息'},
}
--结束场景通知
CS_G2C_OxFriend_NLB_GameEndScene_Nty =
{
	{ 1		, 1     , 'm_priorityChair' , 'INT'        , 1    , '优先者'},
	{ 2		, 1     , 'm_bankerChair' , 'INT'        , 1    , '庄家椅子'},
	{ 3		, 1     , 'm_bankerTimes' , 'INT'        , 1    , '庄家倍数'},
	{ 4		, 1     , 'm_addScoreData' , 'PstOxBidAddData'        , 5    , '下分信息'},
	{ 5		, 1     , 'm_selfCard' , 'UBYTE'        , 5    , '自己扑克'},
	{ 6		, 1     , 'm_endData' , 'PstOxBidEndData'        , 5    , '结束数据'},
}
--开始抢庄通知
CS_G2C_OxFriend_NLB_StartApplyBanker_Nty =
{
	{ 1		, 1     , 'm_applyTimes' , 'INT'        , 5    , '抢庄倍数'},
	{ 2		, 1     , 'm_selfCard'  	, 'UBYTE'        		, 5    , '自己扑克'},
}
--玩家抢庄申请
CS_C2G_OxFriend_NLB_ApplyBanker_Req =
{
	{ 1		, 1     , 'm_times'  	, 'INT'        		, 1    , '抢庄倍数 0:不抢'},
}
--玩家抢庄响应
CS_G2C_OxFriend_NLB_ApplyBanker_Ack =
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '抢庄结果'},
	{ 2		, 1     , 'm_times'  	, 'INT'        		, 1    , '抢庄倍数'},
}
--玩家抢庄通知
CS_G2C_OxFriend_NLB_ApplyBanker_Nty =
{
	{ 1		, 1     , 'm_applyData'  	, 'PstOxBidApplyData'        		, 5    , '抢庄信息'},
}
--开始下分通知
CS_G2C_OxFriend_NLB_StartAddScore_Nty =
{
	{ 1		, 1     , 'm_bankerChair' , 'INT'        , 1    , '庄家椅子'},
	{ 2		, 1     , 'm_bankerTimes' , 'INT'        , 1    , '庄家倍数'},
	{ 3		, 1     , 'm_addTimes' , 'INT'        , 5    , '下分倍数'},
}
--玩家下分申请
CS_C2G_OxFriend_NLB_AddScore_Req =
{
	{ 1		, 1     , 'm_times'  	, 'INT'        		, 1    , '下分倍数'},
}
--玩家下分响应
CS_G2C_OxFriend_NLB_AddScore_Ack =
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '下分结果'},
	{ 2		, 1     , 'm_times'  	, 'INT'        		, 1    , '下分倍数'},
}
--玩家下分通知
CS_G2C_OxFriend_NLB_AddScore_Nty =
{
	{ 1		, 1     , 'm_chairId'  	, 'INT'        		, 1    , '椅子ID'},
	{ 2		, 1     , 'm_times'  	, 'INT'        		, 1    , '下分倍数'},
}
--游戏结束
CS_G2C_OxFriend_NLB_GameEnd_Nty =
{
	{ 1		, 1     , 'm_selfOpenCard'  	, 'UBYTE'        		, 5    , '自己开牌数据'},
	{ 2		, 1     , 'm_endData' , 'PstOxBidEndData'        , 5    , '结束数据'},
}
--优先抢庄刷新
CS_G2C_OxFriend_NLB_PriorityUpdate_Nty =
{
	{ 1		, 1     , 'm_chairId'  	, 'INT'        		, 1    , '椅子号'},
}
--------------通比牛牛(AC:AllCompare)----------------------
--结束开牌场景通知
CS_G2C_OxFriend_AC_GameEndScene_Nty =
{
	{ 5		, 1     , 'm_selfCard' , 'UBYTE'        , 5    , '自己扑克'},
	{ 6		, 1     , 'm_endData' , 'PstOxBidEndData'        , 5    , '结束数据'},
}
--结束开牌通知
CS_G2C_OxFriend_AC_GameEnd_Nty =
{
	{ 5		, 1     , 'm_selfOpenCard' , 'UBYTE'        , 5    , '自己开牌数据'},
	{ 6		, 1     , 'm_endData' , 'PstOxBidEndData'        , 5    , '结束数据'},
}