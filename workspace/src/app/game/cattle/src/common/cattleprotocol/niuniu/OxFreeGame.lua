module(..., package.seeall)

------------------客户端-游戏服(逻辑)----------------- 
--游戏等待
CS_G2C_Ox_GameWait_Nty =
{
	
}
--开始下注倒计时
CS_G2C_Ox_StartAddScore_Nty = 
{
	{ 1		, 1     , 'm_timeCountDown'  	, 'UINT'        , 1    , '倒计时'},
	{ 2		, 1     , 'm_maxAddScore'  		, 'UINT'        , 1    , '最大下注数'},
	{ 3		, 1     , 'm_canContinueBet'  	, 'UBYTE'       , 1    , '是否能够续投 1-是 0-否'},
	{ 4		, 1		, 'm_recordId'       	,  'STRING'     , 1    , '牌局编号' },
}
--玩家下注请求
CS_C2G_Ox_AddScore_Req = 
{
	{ 1		, 1     , 'm_addArea'  		, 'UINT'        	, 1    , '下注区域'},
	{ 2		, 1     , 'm_addScore'  	, 'UINT'        	, 1    , '下注分数'},
}
--玩家下注响应(失败时才返回)
CS_G2C_Ox_AddScore_Ack = 
{
	{ 1		, 1     , 'm_result'  	, 'INT'        	, 1    , '-120000:下注部分'},
}
--玩家下注广播(成功时广播)
CS_G2C_Ox_AddScore_Nty = 
{
	{ 1		, 1     , 'm_userId'  	, 'INT'      , 1    , '玩家ID'},
	{ 2		, 1     , 'm_addArea'  	, 'UINT'     , 1    , '下注区域'},
	{ 3		, 1     , 'm_addScore'  , 'UINT'     , 1    , '下注分数'},
	{ 4		, 1     , 'm_bet'  		, 'UINT'     , 1    , '已下注'},
	{ 5		, 1     , 'm_score'  		, 'UINT'     , 1    , '自己的金币，失败则不发'},
}
--游戏结束开牌
CS_G2C_Ox_GameEnd_Nty = 
{
{ 1	, 1 , 'm_bankerRandCard' , 'PstOxGameCardData' , 1 , '庄家随机扑克'},
{ 2	, 1 , 'm_cardArray' , 'PstOxGameCardData' , 5 , '玩家扑克'},
{ 3	, 1 , 'm_jokerCardData' , 'PstOxJokerChangeData' , 2 , '扑克变牌数据'},
{ 4	, 1 , 'm_cardType' , 'UBYTE' , 5 , '扑克牌型'},
{ 5	, 1 , 'm_gameTimes' , 'INT' , 4 , '游戏输赢倍数'},
{ 6	, 1 , 'm_selfGameScore' , 'INT' , 1 , '本人游戏得分'},
{ 7	, 1 , 'm_allGameScore' , 'PstOxCalScoreData' , 5 , '排名数据'},
{ 8	, 1 , 'm_allBankerWinScore' , 'INT' , 1 , '所有庄家输赢总得分'},
{ 9	, 1 , 'm_bankerNum' , 'INT' , 1 , '庄家数量'},
{ 10	, 1 , 'm_score' , 'UINT' , 1 , '玩家当前分数'},
{ 11	, 1 , 'm_timeCountDown' , 'UINT' , 1 , '结算倒计时'},
}
--游戏空闲通知
CS_G2C_Ox_GameFree_Nty =
{
}
--申请庄家请求
CS_C2G_Ox_Apply_Banker_Req = 
{
    { 1		, 1     , 'm_applyCoin'  	, 'UINT'        		, 1    , '申请上庄金币'},
}
--申请庄家响应
CS_G2C_Ox_Apply_Banker_Ack = 
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '-120020:申请庄家部分'},
	{ 2		, 1     , 'm_data'  	, 'INT'        		, 1    , '信息'},
}
--取消上庄请求
CS_C2G_Ox_Cancel_Banker_Req = 
{
}
--取消上庄响应
CS_G2C_Ox_Cancel_Banker_Ack = 
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '-120040:退庄部分'},
}
--刷新庄家通知
CS_G2C_Ox_UpdateBankerArray_Nty =
{
	{ 1		, 1     , 'm_totalBankerCoin'  	, 'UINT'        		  , 1    , '总上庄金币'},
	{ 2		, 1     , 'm_bankerArray'  		, 'PstOxGameBankerData'   , 100  , '庄家队列'},
}

--刷新申请庄家队列通知
CS_G2C_Ox_UpdateApplyArray_Nty = 
{
	{ 1		, 1     , 'm_applyArray'  	, 'UINT'        		, 100    , '申请队列, 玩家ID'},
}

--[[
--游戏空闲现场信息通知
CS_G2C_Ox_GameFreeScene_Nty = 
{
	{ 1		, 1     , 'm_bankerArray'  		, 'PstOxGameBankerData'   , 100  , '庄家队列'},
	{ 2		, 1     , 'm_applyArray'  		, 'INT'        			  , 100  , '申请队列'},
	-- { 3		, 1     , 'm_recordArray'  		, 'PstOxGameRecordData'   , 100  , '对战记录'},
	{ 4		, 1     , 'm_totalBankerCoin'  	, 'UINT'        		  , 1    , '总上庄金币'},
	{ 5		, 1     , 'm_totalBet'  		, 'UINT'        		  , 1    , '所有玩家已下注'},
}

--游戏开始现场信息通知
CS_G2C_Ox_GameStartScene_Nty = 
{
	{ 1		, 1     , 'm_timeRemaining'     , 'UINT'        		, 1    , '剩余时间'},
	{ 2		, 1     , 'm_curCancelBanker'  	, 'INT'        			, 1    , '是否当前退庄 0:否 1:是'},
	{ 3		, 1     , 'm_bankerArray'  	    , 'PstOxGameBankerData' , 100  , '庄家队列'},
	{ 4		, 1     , 'm_applyArray'  	    , 'INT'        			, 100  , '申请队列'},
	-- { 5		, 1     , 'm_recordArray'  	    , 'PstOxGameRecordData' , 100  , '对战记录'},
	{ 6		, 1     , 'm_totalBankerCoin'  	, 'UINT'        		, 1    , '总上庄金币'},
	{ 7		, 1     , 'm_totalBet'  		, 'UINT'        		, 1    , '所有玩家已下注'},
}

--游戏下注现场信息通知
CS_G2C_Ox_GameAddScene_Nty = 
{
	{ 1		, 1     , 'm_timeRemaining' 	, 'UINT'        		, 1    , '剩余时间'},
	{ 2		, 1     , 'm_maxAddScore'  		, 'UINT'        		, 1    , '最大下注数'},
	{ 3		, 1     , 'm_selfAddScore'  	, 'UINT'        		, 4    , '本人四个区域已下注数'},
	{ 4		, 1     , 'm_arrAddScore'  		, 'UINT'   				, 4    , '所有玩家四个区域已下注数'},
	{ 5		, 1     , 'm_curCancelBanker'  	, 'INT'        			, 1    , '是否当前退庄 0:否 1:是'},
	{ 6		, 1     , 'm_bankerArray'  		, 'PstOxGameBankerData' , 100  , '庄家队列'},
	{ 7		, 1     , 'm_applyArray'  		, 'INT'        			, 100  , '申请队列'},
	-- { 8		, 1     , 'm_recordArray'  		, 'PstOxGameRecordData' , 100  , '对战记录'},
	{ 9		, 1     , 'm_totalBankerCoin'  	, 'UINT'        		, 1    , '总上庄金币'},
	{ 10	, 1     , 'm_totalBet'  		, 'UINT'        		, 1    , '所有玩家已下注'},
}
--]]

--游戏结束现场信息通知
CS_G2C_Ox_GameState_Nty = 
{
	{ 1		, 1     , 'm_timeRemaining' 	, 'UINT'        			, 1    	, '剩余时间'},
	{ 2		, 1     , 'm_status'    		, 'UINT'       				, 1  	, '0-等待 1-空闲 2-下注 3-结算'},
	{ 3		, 1     , 'm_self' 				, 'PstOxClientUserData'     , 1    	, '个人信息'},
	{ 4		, 1     , 'm_applyScore'  		, 'UINT'        			, 1     , '申请上庄最小金币'},
	{ 5		, 1     , 'm_maxApplyScore'  	, 'UINT'        			, 1     , '申请上庄最大金币'},
	{ 6		, 1     , 'm_playerMaxGameScore'  , 'UINT'        			, 1     , '闲家最大输赢上限'},
	{ 7		, 1     , 'm_bankerMaxGameScore'  , 'UINT'        			, 1     , '庄家最大输赢上限'},
	
	{ 8		, 1     , 'm_selfAddScore'  	, 'UINT'        			, 4  	, '本人四个区域已下注数'},
	{ 9		, 1     , 'm_arrAddScore'    	, 'UINT'       				, 4  	, '所有玩家四个区域已下注数'}, 
	{ 10	, 1     , 'm_curCancelBanker'  	, 'INT'        				, 1     , '是否当前退庄 0:否 1:是'},
	{ 11	, 1     , 'm_bankerArray'  		, 'PstOxGameBankerData' 	, 100   , '庄家队列'},
	{ 12	, 1     , 'm_applyArray'  		, 'UINT'        			, 100   , '申请队列'},
	{ 13	, 1     , 'm_totalBankerCoin'  	, 'UINT'        			, 1     , '总上庄金币'},
	{ 14	, 1     , 'm_totalBet'  		, 'UINT'        			, 1     , '所有玩家已下注'},
	{ 15	, 1		, 'm_winHistoryArr'	 	, 'PstOxGameRecordData'		, 10	, '今日输赢历史记录, 10局' },
	{ 16	, 1		, 'm_totalRound'		, 'UINT'					, 1		, '今日总局数' },
	{ 17	, 1		, 'm_winRoundArr'		, 'UINT'					, 4		, '今日天地玄黄四个区域赢局数， 总局数减赢局数即为输局数' },
    { 18	, 1     , 'm_canContinueBet'  	, 'UBYTE'       , 1    , '是否能够续投 1-是 0-否'},
	{ 19	, 1     , 'm_playerNum'  		, 'UINT'        , 1    , '人数'},
	{ 20	, 1		, 'm_recordId'       	,  'STRING'     , 1    , '牌局编号' },
}

--聊天消息
--[[
CS_C2G_Ox_Chat_Req =
{
	{ 1		, 1     , 'm_data'  	, 'STRING'        		, 1    , '聊天信息'},
}
--聊天推送
CS_G2C_Ox_Chat_Nty =
{
	{ 1		, 1     , 'm_chairId'  	, 'INT'        		, 1    , '玩家信息'},
	{ 2		, 1     , 'm_data'  	, 'STRING'        		, 1    , '聊天信息'},
}
--]]
--结束退出庄家庄家提示
CS_G2C_Ox_GameEnd_ExitBanker_Nty =
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '返回值'},
	{ 2		, 1     , 'm_data'  	, 'INT'        		, 1    , '信息'},
}
--结束退出申请队列提示
CS_G2C_Ox_GameEnd_ExitApply_Nty =
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '返回值'},
	{ 2		, 1     , 'm_data'  	, 'INT'        		, 1    , '信息'},
}
CS_C2G_Ox_Remain_BankerTimes_Req =
{
}
--剩余局数提示
CS_G2C_Ox_Remain_BankerTimes_Ack =
{
	{ 1		, 1     , 'm_times'  	, 'UINT'        		, 1    , '剩余局数'},
} 
------------------客户端-游戏服(流程)-----------------
--[[
--玩家游戏初始化请求
CS_C2G_Ox_GameInit_Req = 
{
}
--玩家游戏初始化请求响应
CS_G2C_Ox_GameInit_Ack = 
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '0:成功 -120240:游戏初始化结果部分(客户端请求)'},
}
--游戏初始信息通知
CS_G2C_Ox_GameInit_Nty = 
{
	{ 1		, 1     , 'm_reEnter' 				, 'INT'        				, 1    	, '重连标志 0:非重连 1:重连'},
	{ 2		, 1     , 'm_selfUserId' 			, 'UINT'        			, 1    	, '自己ID'},
	{ 3		, 1     , 'm_selfChairId' 			, 'INT'        				, 1    	, '自己椅子'},
	{ 4		, 1     , 'm_applyScore'  			, 'UINT'        			, 1     , '申请上庄最小金币'},
	{ 5		, 1     , 'm_cellScore'  			, 'UINT'        			, 10    , '底分(弃用)'},
	{ 6		, 1     , 'm_playerMaxGameScore'  	, 'UINT'        			, 1     , '闲家最大输赢上限'},
	{ 7		, 1     , 'm_bankerMaxGameScore'  	, 'UINT'        			, 1     , '庄家最大输赢上限'},
	{ 8		, 1     , 'm_arrUserData' 			, 'PstOxClientUserData'     , 1024  , '玩家信息'},
	{ 9		, 1     , 'm_maxApplyScore'  		, 'UINT'        			, 1     , '申请上庄最大金币'}
}
--]]

--玩家进入游戏通知(广播)
--[[
CS_G2C_Ox_UserEnter_Nty = 
{
	{ 1		, 1     , 'm_userData' , 'PstOxClientUserData'        		, 1    , '玩家信息'},
}
--玩家退出游戏通知(广播)
CS_G2C_Ox_UserExit_Nty = 
{
	{ 1		, 1     , 'm_chairId' , 'INT'        		, 1    , '玩家信息'},
}

CS_G2C_Ox_UserBetInfo_Nty =
{
	{1, 1, 'm_vctBetInfo', 'UINT', 10, '筹码信息'},
}
--]]

--游戏刷新积分(服务端结算后)
CS_G2C_Ox_UpdateScore_Nty = 
{
	{ 1		, 1     , 'm_arrUserData' , 'PstOxClientUserData'        		, 1024    , '玩家信息'},
}

--玩家请求离开游戏
CS_C2G_Ox_Exit_Req = 
{
}
--玩家离开游戏推送
CS_G2C_Ox_Exit_Nty = 
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '-120200:离开游戏结果部分(推送客户端)'},
}
--玩家补充金币请求
--[[
CS_C2G_Ox_Supply_Req = 
{
	{ 1		, 1     , 'm_score'  	, 'UINT'        		, 1    , '补充金币数'},
}
--玩家补充金币回应
CS_G2C_Ox_Supply_Ack = 
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '补充结果'},
}
--]]
--服务异常
CS_G2C_Ox_ServiceError_Nty =
{
}
--换桌请求
--[[
CS_C2G_Ox_ChangeTable_Req =
{
}
--]]
--场景重置请求
--[[
CS_C2G_Ox_Scene_Reset_Req =
{
}
--]]
--[[
--最近用过的筹码
CS_G2C_Ox_LastUseJetton_Nty =
{
	{ 1		, 1     , 'm_addTimes'  	, 'INT'        		, 1    , '下注倍数'},
}
--记录使用的筹码(退出游戏时记录)
CS_C2G_Ox_RecordUseJetton_Req =
{
	{ 1		, 1     , 'm_addTimes'  	, 'INT'        		, 1    , '下注倍数'},
}
--]]

-- 在线玩家列表
CS_C2G_Ox_PlayerOnlineList_Req =
{
	{ 1,	1, 'm_startIndex'        , 'UINT'       				, 1     , '开始索引(从1开始)' },
	{ 2,	1, 'm_endIndex'          , 'UINT'       				, 1     , '结束索引' },
}

CS_G2C_Ox_PlayerOnlineList_Ack = 
{
	{ 1,	1, 'm_startIndex'        , 'UINT'       				, 1     , '开始索引(从1开始)' },
	{ 2,	1, 'm_endIndex'          , 'UINT'       				, 1     , '结束索引' },
	{ 3, 	1, 'm_playerInfo'		 , 'PstOxClientUserData'  		, 64	, '在线玩家信息， 按照最近20局下注数排序，这个再发最多64个玩家，多的不发送' },	
}

CS_C2G_OX_ContinueBet_Req =
{
	--{ 1,	1, 'm_continueBetArr'	 ,  'PstOxContinueBet'	, 1024	, '玩家续押信息'},
}

CS_G2C_OX_ContinueBet_Ack =
{
	{ 1, 	1, 'm_result'		 ,  'INT'					, 1		, '0:成功, -x:失败(m_result=0时，广播此ack)' },
	{ 2, 	1, 'm_betAccountId'	 ,  'UINT'					, 1		, '下注玩家ID' },
	{ 3,	1, 'm_curCoin'		 ,  'UINT'					, 1		, '下注成功后的玩家金币(仅在m_result=0,且m_betAccountId=自己时有效)'},
	{ 4,	1, 'm_continueBetArr',  'PstOxBetData'			, 1024		, '玩家续押信息'},
	{ 5,	1, 'm_allTotalBet'   ,  'UINT'					, 1		, '所有区域、所有玩家总下注'},
}

CS_G2C_Ox_UpdatePlayerNum_Nty =
{
	{ 1,	1, 'm_playNum'			 ,  'UINT'						, 1		, '玩家人数'},
}