module(..., package.seeall)

------------------客户端-游戏服(逻辑)-----------------
--等待玩家场景通知
CS_G2C_OxShenHe_WaitUserScene_Nty =
{
}
--等待开始场景通知
CS_G2C_OxShenHe_WaitStartScene_Nty =
{
	{ 1		, 1     , 'm_timeRemaining' , 'UINT'        , 1    , '剩余时间'},
}
--抢庄场景通知
CS_G2C_OxShenHe_ApplyBankerScene_Nty =
{
	{ 1		, 1     , 'm_timeRemaining' , 'UINT'        , 1    , '剩余时间'},
	{ 2		, 1     , 'm_chairPriorityApply' , 'INT'        , 1    , '优先抢庄'},
	{ 3		, 1     , 'm_applyTimes' , 'UINT'        , 5    , '抢庄倍数'},
	{ 4		, 1     , 'm_card' , 'PstOxGameCardData'        , 1    , '扑克数据'},
	{ 5		, 1     , 'm_applyData' , 'PstOxShenHeApplyData'        , 5    , '抢庄信息'},
}
--下分场景通知
CS_G2C_OxShenHe_AddScoreScene_Nty =
{
	{ 1		, 1     , 'm_timeRemaining' , 'UINT'        , 1    , '剩余时间'},
	{ 2		, 1     , 'm_bankerChair' , 'INT'        , 1    , '庄家椅子'},
	{ 3		, 1     , 'm_addTimes' , 'UINT'        , 5    , '下分倍数'},
	{ 4		, 1     , 'm_card' , 'PstOxGameCardData'        , 1    , '扑克数据'},
	{ 5		, 1     , 'm_addScoreData' , 'PstOxShenHeAddScoreData'        , 5    , '下分信息'},
}
--开牌场景通知
CS_G2C_OxShenHe_OpenCardScene_Nty =
{
	{ 1		, 1     , 'm_timeRemaining' , 'UINT'        , 1    , '剩余时间'},
	{ 3		, 1     , 'm_bankerChair' , 'INT'        , 1    , '庄家椅子'},
	{ 4		, 1     , 'm_card' , 'PstOxGameCardData'        , 1    , '扑克数据'},
	{ 5		, 1     , 'm_openFlag' , 'INT'        , 1    , '是否开牌 0:否 1:是'},
	{ 6		, 1     , 'm_cardType' , 'UBYTE'        , 1    , '扑克牌型'},
	{ 7		, 1     , 'm_oxCard' , 'PstOxGameCardData'        , 1    , '亮牌数据'},
	{ 8		, 1     , 'm_othersOpenCard' , 'PstOxShenHeOpenCardData'        , 5    , '开牌数据'},
}
--结束场景通知
CS_G2C_OxShenHe_GameEndScene_Nty =
{
	{ 1		, 1     , 'm_timeRemaining' , 'UINT'        , 1    , '剩余时间'},
	{ 2		, 1     , 'm_bankerChair' , 'INT'        , 1    , '庄家椅子'},
	{ 3		, 1     , 'm_calData' , 'PstOxShenHeCalculateData'        , 5    , '游戏数据'},
	{ 4		, 1     , 'm_openCard' , 'PstOxShenHeOpenCardData'        , 5    , '开牌数据'},
}
--等待玩家通知
CS_G2C_OxShenHe_WaitUser_Nty =
{
}
--等待开始通知
CS_G2C_OxShenHe_WaitStart_Nty =
{
	{ 1		, 1     , 'm_timeRemaining' , 'UINT'        , 1    , '剩余时间'},
}
--开始抢庄通知
CS_G2C_OxShenHe_StartApplyBanker_Nty =
{
	{ 1		, 1     , 'm_timeRemaining' , 'UINT'        , 1    , '剩余时间'},
	{ 2		, 1     , 'm_applyTimes' , 'UINT'        , 5    , '抢庄倍数'},
	{ 3		, 1     , 'm_chairPriorityApply' , 'INT'        , 1    , '优先抢庄'},
	{ 4		, 1     , 'm_chairArrSendCard' , 'INT'        , 5    , '发牌椅子'},
	{ 5		, 1     , 'm_card'  	, 'PstOxGameCardData'        		, 1    , '自己扑克'},
}
--玩家抢庄申请
CS_C2G_OxShenHe_ApplyBanker_Req =
{
	{ 1		, 1     , 'm_times'  	, 'UINT'        		, 1    , '抢庄倍数 0:不抢'},
}
--玩家抢庄响应
CS_G2C_OxShenHe_ApplyBanker_Ack =
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '抢庄结果'},
	{ 2		, 1     , 'm_times'  	, 'UINT'        		, 1    , '抢庄倍数'},
}
--玩家抢庄通知
CS_G2C_OxShenHe_ApplyBanker_Nty =
{
	{ 1		, 1     , 'm_applyData'  	, 'PstOxShenHeApplyData'        		, 5    , '抢庄信息'},
}
--开始下分通知
CS_G2C_OxShenHe_StartAddScore_Nty =
{
	{ 1		, 1     , 'm_timeRemaining' , 'UINT'        , 1    , '剩余时间'},
	{ 2		, 1     , 'm_bankerChair' , 'INT'        , 1    , '庄家椅子'},
	{ 3		, 1     , 'm_bankerTimes' , 'INT'        , 1    , '庄家椅子'},
	{ 4		, 1     , 'm_addTimes' , 'UINT'        , 5    , '下分倍数'},
}
--玩家下分申请
CS_C2G_OxShenHe_AddScore_Req =
{
	{ 1		, 1     , 'm_times'  	, 'UINT'        		, 1    , '下分倍数'},
}
--玩家下分响应
CS_G2C_OxShenHe_AddScore_Ack =
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '下分结果'},
	{ 2		, 1     , 'm_times'  	, 'UINT'        		, 1    , '下分倍数'},
}
--玩家下分通知
CS_G2C_OxShenHe_AddScore_Nty =
{
	{ 1		, 1     , 'm_chairId'  	, 'INT'        		, 1    , '椅子ID'},
	{ 2		, 1     , 'm_times'  	, 'UINT'        		, 1    , '下分倍数'},
}
--开始开牌
CS_G2C_OxShenHe_StartOpenCard_Nty =
{
	{ 1		, 1     , 'm_timeRemaining' , 'UINT'        , 1    , '剩余时间'},
	{ 2		, 1     , 'm_card'  	, 'UBYTE'        		, 1    , '扑克数据'},
	{ 3		, 1     , 'm_chairArrSendCard' , 'INT'        , 5    , '发牌椅子'},
}
--玩家开牌请求
CS_C2G_OxShenHe_OpenCard_Req =
{
	{ 1		, 1     , 'm_card'  	, 'PstOxGameCardData'        		, 1    , '扑克数据 size==0:无牛'},
}
--玩家开牌响应
CS_C2G_OxShenHe_OpenCard_Ack =
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '亮牌结果'},
	{ 2		, 1     , 'm_type'  	, 'UBYTE'        		, 1    , '扑克牌型'},
	{ 3		, 1     , 'm_card'  	, 'PstOxGameCardData'        		, 1    , '亮牌数据'},
}
--玩家开牌通知
CS_G2C_OxShenHe_OpenCard_Nty =
{
	{ 1		, 1     , 'm_chairId'  	, 'INT'        		, 1    , '椅子ID'},
	{ 2		, 1     , 'm_type'  	, 'UBYTE'        		, 1    , '扑克牌型'},
	{ 3		, 1     , 'm_card'  	, 'PstOxGameCardData'        		, 1    , '扑克数据'},
}
--游戏结束
CS_G2C_OxShenHe_GameEnd_Nty =
{
	{ 1		, 1     , 'm_calData' , 'PstOxShenHeCalculateData'        , 5    , '游戏数据'},
}
------------------客户端-游戏服(流程)-----------------
--玩家游戏初始化请求
CS_C2G_OxShenHe_GameInit_Req = 
{
}
--玩家游戏初始化请求响应
CS_G2C_OxShenHe_GameInit_Ack = 
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '0:成功 -120240:游戏初始化结果部分(客户端请求)'},
}
--游戏初始信息通知
CS_G2C_OxShenHe_GameInit_Nty = 
{
	{ 1		, 1     , 'm_reEnter' 	, 'INT'        					, 1    	, '重连标志 0:非重连 1:重连'},
	{ 2		, 1     , 'm_selfUserId' 	, 'UINT'        			, 1    	, '自己ID'},
	{ 3		, 1     , 'm_selfChairId' 	, 'INT'        				, 1    	, '自己椅子'},
	{ 4		, 1     , 'm_arrUserData' 	, 'PstOxClientUserData'     , 1024  , '玩家信息'},
}
--玩家进入游戏通知(广播)
CS_G2C_OxShenHe_UserEnter_Nty = 
{
	{ 1		, 1     , 'm_userData' , 'PstOxClientUserData'        		, 1    , '玩家信息'},
}
--玩家退出游戏通知(广播)
CS_G2C_OxShenHe_UserExit_Nty = 
{
	{ 1		, 1     , 'm_chairId' , 'INT'        		, 1    , '玩家信息'},
}
--游戏刷新积分(服务端结算后)
CS_G2C_OxShenHe_UpdateScore_Nty = 
{
	{ 1		, 1     , 'm_arrUserData' , 'PstOxClientUserData'        		, 1024    , '玩家信息'},
}
--玩家请求离开游戏
CS_C2G_OxShenHe_Exit_Req = 
{
}
--玩家离开游戏推送
CS_G2C_OxShenHe_Exit_Nty = 
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '-120200:离开游戏结果部分(推送客户端)'},
}
--场景重置请求
CS_C2G_OxShenHe_Scene_Reset_Req =
{
}