module(..., package.seeall)
--房间初始化刷新请求(PC)
CS_C2M_OxHappy_RoomInitUpdate_Req = 
{
}
--进入桌子请求(PC)
CS_C2M_OxHappy_EnterTable_Req = 
{
	{ 1		, 1     , 'm_tableId'  	, 'STRING'        		, 1    , '桌子ID'},
}
--进入桌子通知(PC+手机)
CS_M2C_OxHappy_EnterTable_Nty = 
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '0:成功 -120060:参考进桌部分'},
}
--玩家离开桌子通知(PC)
CS_M2C_OxHappy_ExitTable_Nty = 
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '-120080:参考离桌部分'},
}
--离开房间请求(PC+手机)
CS_C2M_OxHappy_ExitRoom_Req =
{
}
--玩家离开房间通知(PC+手机)
CS_M2C_OxHappy_ExitRoom_Nty =  
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '-120100:参考离场部分'},
}
----------------信息刷新推送-------------
--刷新桌子信息通知(PC)
CS_M2C_OxHappy_UpdateTable_Nty = 
{
	{ 1		, 1     , 'm_tableDataArr'  	, 'PstOxFreeSceneTableData'        		, 1024    , '桌子列表'},
}
--刷新玩家信息(PC)
CS_M2C_OxHappy_UpdateUserData_Nty = 
{
	{ 1		, 1     , 'm_userArr'  	, 'PstOxFreeSceneUserData'        		, 1024    , '玩家信息队列'},
}
--玩家进入房间(PC)
CS_M2C_OxHappy_UserEnter_Nty = 
{
	{ 1		, 1     , 'm_userId'  	, 'UINT'        		, 1    , '玩家ID'},
}
--玩家退出房间(PC)
CS_M2C_OxHappy_UserLeave_Nty = 
{
	{ 1		, 1     , 'm_userId'  	, 'UINT'        		, 1    , '玩家ID'},
}
--刷新桌子有效状态通知(PC)
CS_M2C_OxHappy_UpdateValidTable_Nty =
{
	{ 1		, 1     , 'm_tableArr'  	, 'INT'        		, 10000    , '桌子列表'},
}
--换桌结果通知
CS_M2C_OxHappy_ChangeTable_Nty =
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '换桌结果'},
}
--刷新玩家ID(PC)
CS_M2C_OxHappy_UpdateUserId_Nty =
{
	{ 1		, 1     , 'm_userArr'  	, 'UINT'        		, 10000    , '玩家ID'},
}
--刷新桌子请求
CS_C2M_OxHappy_UpdateTableData_Req = 
{
	{ 1		, 1     , 'm_tableArr'  	, 'INT'        		, 10    , '桌子列表'},
}
--刷新玩家请求
CS_C2M_OxHappy_UpdateUserData_Req =
{
	{ 1		, 1     , 'm_userArr'  	, 'UINT'        		, 500    , '玩家ID'},
}
--快速加入(PC)
CS_C2M_OxHappy_AutoEnterTable_Req=
{
}
--彩池奖励广播
CS_M2C_OxHappy_CaiChiReward_Nty=
{
	{ 1		, 1     , 'm_data' , 'PstOxCaiChiRewardData'        , 100    , '奖励信息'},
	{ 2		, 1     , 'm_totalScore' , 'UINT'        , 1    , '奖励总积分'},
}