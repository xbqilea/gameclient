module(..., package.seeall)

------------------客户端-游戏服(逻辑)-----------------
--播放开始下注动画
CS_G2C_OxHappy_GameStartAnim_Nty = 
{
	
}
--开始下注倒计时
CS_G2C_OxHappy_StartAddScore_Nty = 
{
	{ 1		, 1     , 'm_timeCountDown'  	, 'UINT'        , 1    , '倒计时'},
	{ 2		, 1     , 'm_maxAddScore'  		, 'UINT'        , 1    , '最大下注数'},
}
--玩家下注请求
CS_C2G_OxHappy_AddScore_Req = 
{
	{ 1		, 1     , 'm_addArea'  		, 'UINT'        	, 1    , '下注区域'},
	{ 2		, 1     , 'm_addScore'  	, 'UINT'        	, 1    , '下注分数'},
}
--玩家下注响应(失败时才返回)
CS_G2C_OxHappy_AddScore_Ack = 
{
	{ 1		, 1     , 'm_result'  	, 'INT'        	, 1    , '-120000:下注部分'},
}
--玩家下注广播(成功时广播)
CS_G2C_OxHappy_AddScore_Nty = 
{
	{ 1		, 1     , 'm_chairId'  	, 'INT'     , 1    , '椅子ID'},
	{ 2		, 1     , 'm_addArea'  		, 'UINT'     , 1    , '下注区域'},
	{ 3		, 1     , 'm_addScore'  	, 'UINT'     , 1    , '下注分数'},
}
--游戏结束开牌
CS_G2C_OxHappy_GameEnd_Nty = 
{
	{ 1		, 1     , 'm_bankerRandCard'  	, 'PstOxGameCardData'   	, 1    	, '庄家随机扑克'},
	{ 2		, 1     , 'm_cardArray'  		, 'PstOxGameCardData'   	, 5    	, '玩家扑克'},
	{ 3		, 1     , 'm_cardType'  		, 'UBYTE'        			, 5    	, '扑克牌型'},
	{ 4		, 1     , 'm_gameTimes'  		, 'INT'        				, 4    	, '游戏输赢倍数'},
	{ 5		, 1     , 'm_selfGameScore'  	, 'INT'        				, 4    	, '本人游戏得分'},
	{ 6		, 1     , 'm_allGameScore'  	, 'PstOxCalScoreData'       , 1000  , '所有玩家得分'},
	{ 7		, 1     , 'm_record'  	, 'PstOxGameRecordData' , 1  , '对战记录'},
}
--游戏空闲通知
CS_G2C_OxHappy_GameFree_Nty =
{
}
--申请庄家请求
CS_C2G_OxHappy_Apply_Banker_Req = 
{
}
--申请庄家响应
CS_G2C_OxHappy_Apply_Banker_Ack = 
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '-120020:申请庄家部分'},
	{ 2		, 1     , 'm_data'  	, 'INT'        		, 1    , '信息'},
}
--取消上庄请求
CS_C2G_OxHappy_Cancel_Banker_Req = 
{
}
--取消上庄响应
CS_G2C_OxHappy_Cancel_Banker_Ack = 
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '-120040:退庄部分'},
}
--刷新庄家通知
CS_G2C_OxHappy_UpdateBankerArray_Nty =
{
	{ 1		, 1     , 'm_bankerArray'  	, 'PstOxGameBankerData'   , 100    , '庄家队列'},
}
--刷新申请庄家队列通知
CS_G2C_OxHappy_UpdateApplyArray_Nty = 
{
	{ 1		, 1     , 'm_applyArray'  	, 'INT'        		, 100    , '申请队列'},
}
--游戏空闲现场信息通知
CS_G2C_OxHappy_GameFreeScene_Nty = 
{
	{ 1		, 1     , 'm_bankerArray'  	, 'PstOxGameBankerData' , 100  , '庄家队列'},
	{ 2		, 1     , 'm_applyArray'  	, 'INT'        		, 100  , '申请队列'},
	{ 3		, 1     , 'm_recordArray'  	, 'PstOxGameRecordData' , 100  , '对战记录'},
}
--游戏开始现场信息通知
CS_G2C_OxHappy_GameStartScene_Nty = 
{
	{ 1		, 1     , 'm_timeRemaining' , 'UINT'        		, 1    , '剩余时间'},
	{ 2		, 1     , 'm_curCancelBanker'  	, 'INT'        			, 1    , '是否当前退庄 0:否 1:是'},
	{ 3		, 1     , 'm_bankerArray'  	, 'PstOxGameBankerData' , 100  , '庄家队列'},
	{ 4		, 1     , 'm_applyArray'  	, 'INT'        		, 100  , '申请队列'},
	{ 5		, 1     , 'm_recordArray'  	, 'PstOxGameRecordData' , 100  , '对战记录'},
}
--游戏下注现场信息通知
CS_G2C_OxHappy_GameAddScene_Nty = 
{
	{ 1		, 1     , 'm_timeRemaining' , 'UINT'        		, 1    , '剩余时间'},
	{ 2		, 1     , 'm_maxAddScore'  	, 'UINT'        		, 1    , '最大下注数'},
	{ 3		, 1     , 'm_selfAddScore'  , 'PstOxBetData'        , 1024    , '本人已下注数'},
	{ 4		, 1     , 'm_arrAddScore'  , 'PstOxAddScoreData'   , 1024 , '玩家已下注数'},
	{ 5		, 1     , 'm_curCancelBanker'  	, 'INT'        			, 1    , '是否当前退庄 0:否 1:是'},
	{ 6		, 1     , 'm_bankerArray'  	, 'PstOxGameBankerData' , 100  , '庄家队列'},
	{ 7		, 1     , 'm_applyArray'  	, 'INT'        		, 100  , '申请队列'},
	{ 8		, 1     , 'm_recordArray'  	, 'PstOxGameRecordData' , 100  , '对战记录'},
}
--游戏结束现场信息通知
CS_G2C_OxHappy_GameEndScene_Nty = 
{
	{ 1		, 1     , 'm_timeRemaining' 	, 'UINT'        			, 1    	, '剩余时间'},
	{ 2		, 1     , 'm_selfAddScore'  	, 'PstOxBetData'        	, 1024   , '本人已下注数'},
	{ 3		, 1     , 'm_arrAddScore'    	, 'PstOxAddScoreData'       , 1024  , '玩家已下注数'},
	{ 4		, 1     , 'm_bankerRandCard'  	, 'PstOxGameCardData'   	, 1    	, '庄家随机扑克'},
	{ 5		, 1     , 'm_cardArray'  		, 'PstOxGameCardData'   	, 5    	, '玩家扑克'},
	{ 6		, 1     , 'm_cardType'  		, 'UBYTE'        			, 5    	, '扑克牌型'},
	{ 7	, 1     , 'm_gameTimes'  		, 'INT'        				, 4    	, '游戏输赢倍数'},
	{ 8	, 1     , 'm_selfGameScore'  	, 'INT'        				, 4    	, '本人游戏得分'},
	{ 9	, 1     , 'm_allGameScore'  	, 'PstOxCalScoreData'       , 1000 	, '所有玩家得分'},
	{ 10		, 1     , 'm_curCancelBanker'  	, 'INT'        			, 1    , '是否当前退庄 0:否 1:是'},
	{ 11	, 1     , 'm_bankerArray'  		, 'PstOxGameBankerData' 	, 100   , '庄家队列'},
	{ 12	, 1     , 'm_applyArray'  		, 'INT'        				, 100   , '申请队列'},
	{ 13	, 1     , 'm_recordArray'  		, 'PstOxGameRecordData' 	, 100   , '对战记录'},
}
--聊天消息
CS_C2G_OxHappy_Chat_Req =
{
	{ 1		, 1     , 'm_data'  	, 'STRING'        		, 1    , '聊天信息'},
}
--聊天推送
CS_G2C_OxHappy_Chat_Nty =
{
	{ 1		, 1     , 'm_chairId'  	, 'INT'        		, 1    , '玩家信息'},
	{ 2		, 1     , 'm_data'  	, 'STRING'        		, 1    , '聊天信息'},
}
--结束退出庄家庄家提示
CS_G2C_OxHappy_GameEnd_ExitBanker_Nty =
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '返回值'},
	{ 2		, 1     , 'm_data'  	, 'INT'        		, 1    , '信息'},
}
--结束退出申请队列提示
CS_G2C_OxHappy_GameEnd_ExitApply_Nty =
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '返回值'},
	{ 2		, 1     , 'm_data'  	, 'INT'        		, 1    , '信息'},
}
CS_C2G_OxHappy_Remain_BankerTimes_Req =
{
}
--剩余局数提示
CS_G2C_OxHappy_Remain_BankerTimes_Ack =
{
	{ 1		, 1     , 'm_times'  	, 'UINT'        		, 1    , '剩余局数'},
}
------------------客户端-游戏服(流程)-----------------
--玩家游戏初始化请求
CS_C2G_OxHappy_GameInit_Req = 
{
}
--玩家游戏初始化请求响应
CS_G2C_OxHappy_GameInit_Ack = 
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '0:成功 -120240:游戏初始化结果部分(客户端请求)'},
}
--游戏初始信息通知
CS_G2C_OxHappy_GameInit_Nty = 
{
	{ 1		, 1     , 'm_reEnter' 	, 'INT'        					, 1    	, '重连标志 0:非重连 1:重连'},
	{ 2		, 1     , 'm_selfUserId' 	, 'UINT'        			, 1    	, '自己ID'},
	{ 3		, 1     , 'm_selfChairId' 	, 'INT'        				, 1    	, '自己椅子'},
	{ 4		, 1     , 'm_applyScore'  	, 'UINT'        		, 1    , '申请庄家积分'},
	{ 5		, 1     , 'm_arrCellScore'  	, 'UINT'        		, 10    , '底分'},
	{ 6		, 1     , 'm_playerMaxGameScore'  	, 'UINT'        		, 1    , '闲家最大输赢上限'},
	{ 7		, 1     , 'm_bankerMaxGameScore'  	, 'UINT'        		, 1    , '庄家最大输赢上限'},
	{ 8		, 1     , 'm_openCaiChi'  	, 'INT'        		, 1    , '彩池开关 0:关 1:开'},
	{ 9		, 1     , 'm_minCaiChiAddScore'  	, 'UINT'        		, 1    , '彩池下限下注'},
	{ 10		, 1     , 'm_minCaiChiScore'  	, 'UINT'        		, 1    , '彩池下限积分'},
	{ 11		, 1     , 'm_maxCaiChiScore'  	, 'UINT'        		, 1    , '彩池上限积分'},
	{ 12		, 1     , 'm_arrUserData' 	, 'PstOxClientUserData'     , 1024  , '玩家信息'},
}
--玩家进入游戏通知(广播)
CS_G2C_OxHappy_UserEnter_Nty = 
{
	{ 1		, 1     , 'm_userData' , 'PstOxClientUserData'        		, 1    , '玩家信息'},
}
--玩家退出游戏通知(广播)
CS_G2C_OxHappy_UserExit_Nty = 
{
	{ 1		, 1     , 'm_chairId' , 'INT'        		, 1    , '玩家信息'},
}
CS_G2C_OxHappy_UserBetInfo_Nty =
{
	{1, 1, 'm_vctBetInfo', 'UINT', 10, '筹码信息'},
}
--游戏刷新积分(服务端结算后)
CS_G2C_OxHappy_UpdateScore_Nty = 
{
	{ 1		, 1     , 'm_arrUserData' , 'PstOxClientUserData'        		, 1024    , '玩家信息'},
}
--玩家请求离开游戏
CS_C2G_OxHappy_Exit_Req = 
{
}
--玩家离开游戏推送
CS_G2C_OxHappy_Exit_Nty = 
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '-120200:离开游戏结果部分(推送客户端)'},
}
--玩家补充金币请求
CS_C2G_OxHappy_Supply_Req = 
{
	{ 1		, 1     , 'm_score'  	, 'UINT'        		, 1    , '补充金币数'},
}
--玩家补充金币回应
CS_G2C_OxHappy_Supply_Ack = 
{
	{ 1		, 1     , 'm_result'  	, 'INT'        		, 1    , '补充结果'},
}
--服务异常
CS_G2C_OxHappy_ServiceError_Nty =
{
}
--换桌请求
CS_C2G_OxHappy_ChangeTable_Req =
{
}
--彩池积分通知(当前彩池分)
CS_G2C_OxHappy_CaiChi_Score_Nty =
{
	{ 1		, 1     , 'm_score' , 'UINT'        , 1    , '彩池积分'},
}
--彩池奖励通知(通知玩家中奖)
CS_G2C_OxHappy_CaiChi_Reward_Nty =
{
	{ 1		, 1     , 'm_score' , 'UINT'        , 1    , '奖励积分'},
}
--重置场景
CS_C2G_OxHappy_Scene_Reset_Req =
{
}
--最近用过的筹码
CS_G2C_OxHappy_LastUseJetton_Nty =
{
	{ 1		, 1     , 'm_addTimes'  	, 'INT'        		, 1    , '下注倍数'},
}
--记录使用的筹码(退出游戏时记录)
CS_C2G_OxHappy_RecordUseJetton_Req =
{
	{ 1		, 1     , 'm_addTimes'  	, 'INT'        		, 1    , '下注倍数'},
}