--
-- Author: 
-- Date: 2018-08-07 18:17:10
--	牛牛的提示框，2个按钮
-- CowDlgTipLayer
local CowDialogBase = require("src.app.game.cattle.src.common.cattleDlgTip.CattleDialogBase")

local CowDlgTipLayer = class("CowDlgTipLayer", function()
    return CowDialogBase.new()
end)

function CowDlgTipLayer:ctor()
	self:myInit()
	self:setupViews()    
end

function CowDlgTipLayer:myInit()
	self.m_root = nil
	self.m_layout_bg = nil
	self.m_txt_tip = nil
	self.m_handler = nil
	self.m_message = nil
	self.m_btn_no = nil
	self.m_btn_yes = nil
    self.m_mustChoose = false -- 必须选择一个选项
    self.m_btn_close = nil

end

function CowDlgTipLayer:setupViews()
    self.m_root = UIAdapter:createNode("cow_game_csb/cow_tip_dlg.csb")
    self:addChild(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    self.m_layout_bg = self.m_root:getChildByName("layout_bg")
    self.m_img_bg = self.m_root:getChildByName("img_bg")
    self.m_txt_tip = self.m_layout_bg:getChildByName("txt_tip")
	self.m_btn_no = self.m_layout_bg:getChildByName("btn_no")
	self.m_btn_yes = self.m_layout_bg:getChildByName("btn_yes")
    self.m_btn_close = self.m_layout_bg:getChildByName("btn_close")
    self.m_txt_tip:ignoreContentAdaptWithSize(false)
    self.m_size = cc.size(600,180)
	self:setEnableOutline()
end

function CowDlgTipLayer:setEnableOutline()
	self.m_btn_no:getTitleRenderer():enableOutline(cc.c4b(9, 83, 84, 255),3)
	self.m_btn_yes:getTitleRenderer():enableOutline(cc.c4b(9, 83, 84, 255),3)
end

function CowDlgTipLayer:showDlgTip( _message,_handler,_params )

	_message = _message or ""
	self.m_handler = _handler
	self.m_message = _message
	self.m_txt_tip:setString( self.m_message )

    self.m_txt_tip:ignoreContentAdaptWithSize(false)
    local tSize = self.m_txt_tip:getContentSize() 
    -- dump(tSize)
    local tmp_width = tSize.width 
    local linecount = (tmp_width / self.m_size.width )
    local end_height = (linecount )* (tSize.height) 
    if linecount > 1 then
    	self.m_txt_tip:setTextHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT)
        self.m_txt_tip:setString(self.m_message )
        self.m_txt_tip:ignoreContentAdaptWithSize(false)
        tSize = self.m_txt_tip:getContentSize() 
        tmp_width = tSize.width
        linecount = math.ceil(tmp_width / self.m_size.width )
        end_height = (linecount )* (tSize.height) 
	else
		self.m_txt_tip:setTextHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER)
    end

    self.m_txt_tip:setContentSize(cc.size(self.m_size.width,end_height))
    if _params and _params.m_signle then
        self.m_btn_no:setVisible(false)
        self.m_btn_yes:setPosition(cc.p( self.m_img_bg:getPositionX(), self.m_btn_yes:getPositionY()))
    end
    if _params and _params.m_yes then
        self.m_btn_yes:setTitleText(_params.m_yes )
    end
    if _params and _params.m_no then
        self.m_btn_no:setTitleText(_params.m_no )
    end
    if _params and _params.m_close then
        self.m_btn_close:setVisible ( not _params.m_close )
    end
    if _params and _params.m_touch  then
        self:setDlgEnableTouch(false)
    end
    if _params and _params.m_mustChoose  then 
        self.m_mustChoose = _params.m_mustChoose
    end
    if self.m_mustChoose then
        self:setDlgEnableTouch(false)
    end
	self:setVisible(true)
end

function CowDlgTipLayer:getMustChoose()
    return self.m_mustChoose
end
-- 点击事件回调
function CowDlgTipLayer:onTouchCallback( sender )
    local name = sender:getName()
    print("name = ",name)
    if name == "btn_no" then
    	self:hideCowDialogBase( false )
	elseif name == "btn_yes" then
		self:hideCowDialogBase( false )
		if self.m_handler then
			self.m_handler()
		end
	elseif name == "btn_close" then
		self:hideCowDialogBase( false )
    elseif name == "layout_close" then
        self:hideCowDialogBase( false )
    end
    
end

function CowDlgTipLayer:onExit()
   print("CowDlgTipLayer:onExit")
end

return CowDlgTipLayer