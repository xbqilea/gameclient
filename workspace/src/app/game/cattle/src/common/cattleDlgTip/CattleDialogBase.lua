--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- CowDialogBase
-- 主要是不移除，只隐藏
local CowSound = require("src.app.game.cattle.src.common.CowSound.CowSound")

local CowDialogBase = class("CowDialogBase", function()
    return display.newLayer()
end)

function CowDialogBase:ctor()
	self:myInit()
	self:setupViews()
end

function CowDialogBase:myInit()
	self.m_dlgEnableTouch = true
	self.m_layout_bg = nil
	self.m_root = nil
    self.m_img_bg = nil
end

function CowDialogBase:setupViews()
    self.m_root = UIAdapter:createNode("cow_game_csb/cow_dlg_base.csb")
    self:addChild(self.m_root)
    UIAdapter:adapter(self.m_root, handler(self, self.onTouchCallback))
    self.m_layout_bg = self.m_root:getChildByName("layout_bg")
    self.m_img_bg = self.m_root:getChildByName("img_bg")
end

function CowDialogBase:setBgOpacity( _op )
    self.m_img_bg:setOpacity(_op)
end
function CowDialogBase:setDlgEnableTouch( _enable )
	self.m_dlgEnableTouch = _enable
end

function CowDialogBase:hideCowDialogBase( _hide )
	self:setVisible(_hide)
end

function CowDialogBase:getClassName()
    return "CowDialogBase"
end

function CowDialogBase:onTouchCallback( __sender )
    if __sender:getName() == "layout_bg" then
        if self.m_dlgEnableTouch then
        	CowSound:getInstance():playEffect("OxButton")
            self:hideCowDialogBase( false )
        end
    end
end

return CowDialogBase