--
-- Author: 
-- Date: 2018-08-07 18:17:10
--
-- 牛牛骨骼动画 + 牌的缩小放大

local CowAnimation =  class("CowAnimation")

CowAnimation.instance = nil
local CowArmatureResource = require ("src.app.game.cattle.src.common.animation.CattleArmatureResource")
local CowArmature = require("src.app.game.cattle.src.common.animation.CattleArmature")

CowAnimation.shengBiPaiArmatureID =  -- 游戏动画
{
    CowArmatureResource.ANI_GMAESTART_ID  ,                     --游戏开始
    CowArmatureResource.ANI_GMAEEDN_ID  ,                       --游戏结束
    CowArmatureResource.ANI_GAMEBIPAI_ID ,                      --游戏比牌
    CowArmatureResource.ANI_GAMELOST_ID ,                       --游戏负
    CowArmatureResource.ANI_GAMEWIN_ID  ,                       --游戏胜
    CowArmatureResource.ANI_GAMEXIAZHU_ID ,                     --游戏下注
    CowArmatureResource.ANI_PLAYERSHANGZHUANG_ID,               -- 等待动画
}

CowAnimation.chipArmatureID =  -- 筹码动画
{
    CowArmatureResource.ANI_CHOUMASAOGUANG_ID, --筹码扫光
}

CowAnimation.robArmatureID =  -- 抢庄牛牛动画
{
    CowArmatureResource.ROB_ANI_START_ID, -- 开始动画
    CowArmatureResource.ROB_ANI_WIN_ID, --胜利
    CowArmatureResource.ROB_ANI_LOST_ID, --失败
}
function CowAnimation:ctor()
    self.m_allArmature = {}
end

function CowAnimation:creatChipArmature( _gameBottomLayer )
    -- 创建牛牛骨骼所有动画
    for k,v in pairs(CowAnimation.chipArmatureID) do
        print("v = ",v)
        if self:getArmatureById( v ) then
            return
        end
        local cowArmature  = CowArmature.new(v)
        _gameBottomLayer:addChipAnim(cowArmature)
        self.m_allArmature[#self.m_allArmature+1] = cowArmature 
    end
end

function CowAnimation:creatShengBiPaiArmature( _shengBiPaiLayer )
    -- 创建牛牛骨骼所有动画
    for k,v in pairs(CowAnimation.shengBiPaiArmatureID) do
        print("v = ",v)
        if self:getArmatureById( v ) then
            return
        end
        local cowArmature  = CowArmature.new(v )
        _shengBiPaiLayer:addAllArmature(cowArmature)

        self.m_allArmature[#self.m_allArmature+1] = cowArmature 
    end
end

function CowAnimation:creatRobArmature(_robLayer )
        -- 创建抢庄牛牛骨骼所有动画
    for k,v in pairs(CowAnimation.robArmatureID) do
        print("v = ",v)
        if self:getArmatureById( v ) then
            return
        end
        local cowArmature  = CowArmature.new(v )
        _robLayer:addAllArmature(cowArmature)
        self.m_allArmature[#self.m_allArmature+1] = cowArmature 
    end
end

function CowAnimation:getArmatureById( _resourceById )
    print("_resourceById = ",_resourceById)
    print("self.m_allArmature =",#self.m_allArmature)
    for i=1,#self.m_allArmature do
        local _cowArmature = self.m_allArmature[i]
        local _id = _cowArmature:getresourceById() 
        if _id == _resourceById then
            return self.m_allArmature[i]
        end
    end
    return nil
end

function CowAnimation:creatPokerScaleBigToSmall( _callBack )

    local scaleBig = cc.ScaleTo:create(CowDef.ScaleTime,CowDef.ScaleMultiple)
    --[[local delay = cc.DelayTime:create(CowDef.DelayTime)--]]
    local scaleSmall = cc.ScaleTo:create(CowDef.ScaleTime,CowDef.ScaleReduction)
    local callback = cc.CallFunc:create(function() 
            if _callBack then
                _callBack()
            end      
        end)
    local sequence = cc.Sequence:create(scaleBig,delay,scaleSmall,callback)
    return sequence
end 

function CowAnimation:creatAreaPokerScaleBigToSmall(_callBack)
    local scaleSmall = cc.ScaleTo:create(CowDef.ScaleTime,CowDef.ScaleMultiple, 1)
    local scaleBig = cc.ScaleTo:create(CowDef.ScaleTime,CowDef.ScaleReduction, 1)
    local callback = cc.CallFunc:create(function() 
            if _callBack then
                _callBack()
            end      
        end)
    local sequence = cc.Sequence:create(scaleSmall,scaleBig,callback)
    return sequence
end

--播放骨骼动画
function CowAnimation:playArmatureAnimation( _resourceById ,_handerBack )
    local cowArmature  = self:getArmatureById( _resourceById )
    if cowArmature then
        cowArmature:playAnimation( _handerBack )
    end
end 

--停止播放骨骼动画,并清除骨骼动画
function CowAnimation:stopAndClearArmatureAnimation( cowArmature )
    if not tolua.isnull( cowArmature )  then
       cowArmature:stopAllActionsEx()
       cowArmature:clearArmatureFileInfo()
       cowArmature:removeFromParent()
    end
end

function CowAnimation:getInstance()
    if CowAnimation.instance == nil then
        CowAnimation.instance = CowAnimation.new()
    end
    return CowAnimation.instance
end

function CowAnimation:stopAllShengBiPaiArmature()
    for k,v in pairs(CowAnimation.shengBiPaiArmatureID) do
        local cowArmature  = self:getArmatureById( v )
        if cowArmature then
            cowArmature:stopAnimation()
        end
    end
end

function CowAnimation:stopRobAnimation()
    for k,v in pairs(CowAnimation.robArmatureID) do
        local cowArmature  = self:getArmatureById( v )
        if cowArmature then
            cowArmature:stopAnimation()
        end
    end
end

function CowAnimation:ClearAllAnimation()
    for i=1,#self.m_allArmature do
        self.m_allArmature[i]:stopAllActionsEx()
        self.m_allArmature[i]:clearArmatureFileInfo()
        self.m_allArmature[i]:removeFromParent()
    end
    self.m_allArmature = {}
    CowAnimation.instance = nil
end

return CowAnimation



