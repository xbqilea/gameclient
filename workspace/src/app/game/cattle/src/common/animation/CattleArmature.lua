-- Author: 
-- Date: 2018-08-07 18:17:10
--
-- 牛牛骨胳动画

local CowArmatureResource = require ("src.app.game.cattle.src.common.animation.CattleArmatureResource")

local scheduler = require("framework.scheduler")
local CowArmature = class("CowArmature" ,function()
	  return display.newNode()
end)

CowArmature._movementType = ccs.MovementEventType.complete  --动画类型（非循环/循环)

function CowArmature:ctor( resourceById ) 
	self.handlerBack = nil
	self.armatureResourceInfo  = CowArmatureResource:getArmatureResourceById( resourceById )
	self:loadAnimation()
end

--加载骨骼动画数据
function CowArmature:loadAnimation()
	ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( self.armatureResourceInfo.configFilePath )
	local isFileExist = cc.FileUtils:getInstance():isFileExist( self.armatureResourceInfo.configFilePath )

	self.armature = ccs.Armature:create(self.armatureResourceInfo.armatureName)

	--动画播放回调
	self.armature:getAnimation():setMovementEventCallFunc(handler(self,self.animationEvent))
	self:addChild( self.armature )
end

function CowArmature:getArmature()
	return self.armature
end

-- 播放最后一帧

function CowArmature:playgotoFrame( _handlerBack, _lastTime,_index )
	local time = _lastTime or 0
	local num = _index or 0 -- moveMentData.dr
	self.armature:getAnimation():gotoAndPause(num)
	self.handlerBack = _handlerBack
	self.m_lastTimer = scheduler.performWithDelayGlobal(handler(self,self.updateLastTimer), time)
end

function CowArmature:updateLastTimer( _dt )
	self:endLastTimer()
	if self.handlerBack then
		self.handlerBack()
	end
	self:setVisible(false)
	self.armature:stopAllActions()

end

function CowArmature:endLastTimer()
    if self.m_lastTimer then
        scheduler.unscheduleGlobal(self.m_lastTimer)
        self.m_lastTimer = nil
    end
end

--播放动画
function CowArmature:playAnimation( _handlerBack )
	self.handlerBack = _handlerBack
    
	print("self.armatureResourceInfo.animationName=",self.armatureResourceInfo.resourceById)
	if self.armatureResourceInfo.isLoop then
		self.armature:getAnimation():play( self.armatureResourceInfo.animationName, -1,  1)
	else
		self.armature:getAnimation():play( self.armatureResourceInfo.animationName)
	end
	
	self:setVisible(true)
end

-- 换帧
function CowArmature:hangeDisplay( _boneName,_skinFileName )
	local bone = self.armature:getBone(_boneName)
	local skin1 = ccs.Skin:createWithSpriteFrameName( _skinFileName )
	if bone then
		bone:addDisplay(skin1, 1)
		bone :changeDisplayWithIndex(1, true)
	end
end
function CowArmature:getresourceById()
	return self.armatureResourceInfo.resourceById
end
-- 停止
function CowArmature:stopAnimation()
	print("self.armatureResourceInfo.animationName=",self.armatureResourceInfo.resourceById)
	self:setVisible(false)
	self.armature:getAnimation():stop()
end
--动画播放回调
function CowArmature:animationEvent( armatureBack, movementType, movementID )
    --非循环播放一次
    print("movementID = ",movementID)
    if movementType == ccs.MovementEventType.complete then
       if movementID == self.armatureResourceInfo.animationName then
            if self.handlerBack then
               self.handlerBack( self.armatureResourceInfo.resourceById )
            end
            armatureBack:stopAllActions()
            if self.armatureResourceInfo.autoClean ~= true then
		        self:setVisible(false)
            end
       end
    end
 end

--清除骨骼緩存数据
function  CowArmature:clearArmatureFileInfo()
	  ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo( self.armatureResourceInfo.configFilePath )
end

--停止动画
function  CowArmature:stopAllActionsEx()
    if self.armature then
       self.armature:stopAllActions()
    end
    self:endLastTimer()
end

return  CowArmature
