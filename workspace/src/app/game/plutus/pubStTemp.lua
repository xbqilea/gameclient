module(..., package.seeall)


PstPlutusPlayerInfo = 
{
	{ 1, 	1, 'm_accountId'		, 'UINT'				, 1		, '玩家ID' },
	{ 2, 	1, 'm_nickname'			, 'STRING'				, 1 	, '昵称' },
	{ 3, 	1, 'm_faceId'			, 'UINT'				, 1 	, '头像ID' },
	{ 4, 	1, 'm_frameId'			, 'UINT'				, 1 	, '头像框ID' },
	{ 5, 	1, 'm_vipLevel'			, 'UINT'				, 1 	, 'vip等级' },
	{ 6, 	1, 'm_score'			, 'UINT'				, 1 	, '金币' },
}