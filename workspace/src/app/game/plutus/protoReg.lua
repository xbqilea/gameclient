if not LUA_VERSION or LUA_VERSION ~= "5.3" then
	 module(..., package.seeall)
end
require("src.app.game.plutus.protoID")

-- 公共结构定义文件注册
netLoaderCfg_Templates_common = {
	"src/app/game/plutus/pubStTemp",
}

-- 协议定义文件注册
netLoaderCfg_Templates	=	{	
	"src/app/game/plutus/plutus",
}


-------------------------注册协议-----------------------------
-- 公共结构协议注册
netLoaderCfg_Regs_common = 
{
	PstPlutusPlayerInfo				=	PSTID_PLUTUSPLAYERINFO,
}

-- 协议注册
netLoaderCfg_Regs	=	
{
	CS_M2C_Plutus_Start_Nty 		=	CS_M2C_PLUTUS_START_NTY,
	CS_C2M_Plutus_StartRoll_Req		=	CS_C2M_PLUTUS_STARTROLL_REQ,
	CS_M2C_Plutus_ScrollResult_Ack	=	CS_M2C_PLUTUS_SCROLLRESULT_ACK,
	CS_C2M_Plutus_SmallGameOp_Req	=	CS_C2M_PLUTUS_SMALLGAMEOP_REQ,
	CS_M2C_Plutus_SmallGameOp_Ack	=	CS_M2C_PLUTUS_SMALLGAMEOP_ACK,
	CS_M2C_Plutus_Pond_Nty			=	CS_M2C_PLUTUS_POND_NTY,
	CS_C2M_Plutus_Exit_Req			=	CS_C2M_PLUTUS_EXIT_REQ,
	CS_M2C_Plutus_Exit_Ack			=	CS_M2C_PLUTUS_EXIT_ACK,
	CS_M2C_Plutus_Kick_Nty			=	CS_M2C_PLUTUS_KICK_NTY,
	
}
--return netLoaderCfg_Regs

if LUA_VERSION  and LUA_VERSION == "5.3" then
	return netLoaderCfg_Regs
end

