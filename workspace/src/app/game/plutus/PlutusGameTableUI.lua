local HNlayer = require("src.app.newHall.HNLayer")
local scheduler = require("framework.scheduler")

local GameSetLayer= require("src.app.newHall.childLayer.SetLayer")
local ChargeLayer = require("src.app.newHall.childLayer.ZCShopView") 
local GameRecordLayer= require("src.app.newHall.childLayer.GameRecordLayer")
local ChatLayer = require("src.app.newHall.chat.ChatLayer")
local TABLEUI_PATH = "app/game/plutus/res/Games/FarmFrenzy/TableUi/Node_Table.csb"
local BTN_START_UP = "app/game/plutus/res/Games/FarmFrenzy/TableUi/Res/spin_btn_bg_iP5_up.png"
local BTN_START_DOWN = "app/game/plutus/res/Games/FarmFrenzy/TableUi/Res/spin_btn_bg_iP5_down.png"
local BTN_START_DIS= "app/game/plutus/res/Games/FarmFrenzy/TableUi/Res/spin_btn_bg_iP5_dis.png"
local BTN_START_IMG = "app/game/plutus/res/Games/FarmFrenzy/TableUi/Res/spin.png"
local BTN_STOP_UP = "app/game/plutus/res/Games/FarmFrenzy/TableUi/Res/stop_btn_bg_iP5_up.png"
local BTN_STOP_DOWN = "app/game/plutus/res/Games/FarmFrenzy/TableUi/Res/stop_btn_bg_iP5_down.png"
local BTN_STOP_DIS = "app/game/plutus/res/Games/FarmFrenzy/TableUi/Res/spin_btn_bg_iP5_dis.png"
local BTN_STOP_IMG = "app/game/plutus/res/Games/FarmFrenzy/TableUi/Res/stop.png"
local PLATFORM_PATH = "app/game/plutus/res/Games/FarmFrenzy/TableUi/Res/" 
local GAME_RULE_PATH = "app/game/plutus/res/Games/FarmFrenzy/SmallGameUi/Node_SmallGame.csb"

local GAME_FONT_1 = "common/font/littleGameCCKJ_3.fnt"
local GAME_FONT_2 = "common/font/plusYuan.fnt"

local PlutusGameTableUI = class("PlutusGameTableUI",function()
    return HNlayer.new()
end)

local PlutusGameImage = require("app.game.plutus.PlutusGameImage")

function PlutusGameTableUI:ctor()
    self:myInit()
    self:setupView()
end

function PlutusGameTableUI:myInit()
    self._autoGame = false
    self._scheduleNum = 0
    self._scheduleResNum = 0
    self._iWin = 0
    self._freeGames = 0
    self._freeGames_N = 0
    self._score_N = 0
    self._MoveSize = 0.0
    self._IsBet = false
    self._IsRunning = false
    self._autoBegin = false
    self._bFreeAutoGame = false
    self._fTimeMax = 4.0
    self._fTimeMin = 3.0
    self._imageArr1 = {}
    self._imageArr2 = {}
    self._imageArr3 = {}
    self._imageArr4 = {}
    self._imageArr5 = {}
    self._clipperArr = {}
    self._pic1 = {}
    self._pic2 = {}
    self._pic3 = {}
    self._pic4 = {}
    self._pic5 = {}
    self._particle1 = {}
    self._particle2 = {}
    self._particle3 = {}
    self._particle4 = {}
    self._particle5 = {}
    self._checkline = {}
    self._resultNum = 0
    self._animate = nil
    self._score = 0

    self._schedulePush = nil
    self._schedulePushCount = 0.0
end

function PlutusGameTableUI:setupView()
	cc.Director:getInstance():getScheduler():setTimeScale(1.0)
	local winSize = cc.Director:getInstance():getWinSize()
    self._scalex =1/display.scaleX
	self._scaley =1/display.scaleY

	local cache = cc.SpriteFrameCache:getInstance()
	cache:addSpriteFrames("app/game/plutus/res/Games/FarmFrenzy/Static_Icons/Slot_Icons.plist")
    cache:addSpriteFrames("app/game/plutus/res/Games/FarmFrenzy/Static_Icons/Slot_Icons1.plist")
    cache:addSpriteFrames("app/game/plutus/res/Games/FarmFrenzy/Static_Icons/Slot_Icons2.plist")
    cache:addSpriteFrames("app/game/plutus/res/Games/FarmFrenzy/Static_Icons/Slot_Icons3.plist")
    cache:addSpriteFrames("app/game/plutus/res/Games/FarmFrenzy/Static_Icons/Slot_Icons4.plist")
	cache:addSpriteFrames("app/game/plutus/res/Games/FarmFrenzy/Static_Icons/newUI2.plist")

		--预存储游戏配置
	local userDefault = cc.UserDefault:getInstance()
	if (not userDefault:getBoolForKey("Test_bool")) then
		userDefault:setBoolForKey("Test_bool", true)
		userDefault:setIntegerForKey("Bet_value", 50)
		userDefault:setIntegerForKey("Lines_value", 1)
	end
		--读取本地文件中保存的当前单线投注额和投注线数
	self._bet = 50
	--_lines = userDefault:getIntegerForKey("Lines_value")
	self._lines = 1

	local tableUI = UIAdapter:createNode(TABLEUI_PATH)
	local tabel = cc.CSLoader:createTimeline(TABLEUI_PATH)
	tableUI:runAction(tabel)
	tabel:gotoFrameAndPlay(0, true)
	tableUI:setPosition(cc.p(winSize.width / 2, winSize.height / 2))
	self:addChild(tableUI, 2)

	self._tableBG = tableUI:getChildByName("Image_BG")
	local x = winSize.width / self._tableBG:getContentSize().width
	local y = winSize.height / self._tableBG:getContentSize().height
	self._tableBG:setScaleY(y)
    self._Image_button = self._tableBG:getChildByName("Image_button")
    self._Image_num = 0
    self._tableBG:addTouchEventListener(handler(self, self.imageEventCallBack))

	--self._panel_particle = self._tableBG:getChildByName("Panel_particle")
    --self._panel_particle:setLocalZOrder(10000)
    self.mTextRecord = self._tableBG:getChildByName("text_record")
    self.mTextRecord:setVisible(false)
    local returnBtn = self._tableBG:getChildByName("Button_return")
    self.mTextRecord = UIAdapter:CreateRecord(nil, 24)
    self._tableBG:addChild(self.mTextRecord, 2)
    self.mTextRecord:setAnchorPoint(cc.p(0, 0.5))
    local buttonsize = returnBtn:getContentSize()
    local buttonpoint = cc.p(returnBtn:getPosition())
    self.mTextRecord:setPosition(cc.p( buttonpoint.x + buttonsize.width / 2 + 5, buttonpoint.y + buttonsize.height / 4))

	self:getUpMenu()             --初始化屏幕上方ui
	self:getDownMenu()           --初始化屏幕下方ui
	self:getFreeGamesUI()        --初始化免费游戏ui
	self:createCliper()          --绘制裁剪区域
    self:addBoxAnimation()       --载入滚动动画
	self:createPlayImage()       --绘制要滚动的图片

	self:upDataMenuEnabled(true) --初始化让按钮不可点击
	self:showTheLines(false)
    self:updateGameMessage(true)

	--_tableLogic = new GameTableLogic(this, bDeskIndex, bAutoCreate)
	--_tableLogic:sendGameInfo()

	self._checkBox_speed = self._tableBG:getChildByName("CheckBox_speed")
	self._checkBox_guaji = self._tableBG:getChildByName("CheckBox_guaji")
    self._checkBox_guaji:setVisible(false)

    self._checkBox_speed:addEventListener(handler(self, self.check1))
    --self._checkBox_guaji:addEventListener(handler(self, self.check2))
    
end

function PlutusGameTableUI:clearData()
    local cache = cc.SpriteFrameCache:getInstance()
	cache:removeSpriteFramesFromFile("app/game/plutus/res/Games/FarmFrenzy/Static_Icons/Slot_Icons.plist")
    cache:removeSpriteFramesFromFile("app/game/plutus/res/Games/FarmFrenzy/Static_Icons/Slot_Icons1.plist")
    cache:removeSpriteFramesFromFile("app/game/plutus/res/Games/FarmFrenzy/Static_Icons/Slot_Icons2.plist")
    cache:removeSpriteFramesFromFile("app/game/plutus/res/Games/FarmFrenzy/Static_Icons/Slot_Icons3.plist")
    cache:removeSpriteFramesFromFile("app/game/plutus/res/Games/FarmFrenzy/Static_Icons/Slot_Icons4.plist")
    cache:removeSpriteFramesFromFile("app/game/plutus/res/Games/FarmFrenzy/Static_Icons/newUI2.plist")
    self:clear()
	self:close()
end

function PlutusGameTableUI:onExit()
	print("PlutusGameTableUI:onExit")
    self:clearData()
end

function PlutusGameTableUI:check1(sender, eventtype)
    if sender then
        if eventtype == ccui.CheckBoxEventType.selected then
            cc.Director:getInstance():getScheduler():setTimeScale(3)
        elseif eventtype == ccui.CheckBoxEventType.unselected then
            cc.Director:getInstance():getScheduler():setTimeScale(1.0)
        end
    end
end

function PlutusGameTableUI:check2(sender, eventtype)
    if sender then
        if eventtype == ccui.CheckBoxEventType.selected then
            self._autoGame = true
            self.spinBtn:loadTextures("game_tiger_gui_gui-tiger-btn-start-2.png","game_tiger_gui_gui-tiger-btn-start-2-click.png","game_tiger_gui_gui-tiger-btn-start-2.png",1)
            self.spinBtn:setTouchEnabled(false)
            if not self._IsRunning then
                self:gameStart(0.0)
            end
        elseif eventtype == ccui.CheckBoxEventType.unselected then
            self._autoGame = false
            self.spinBtn:loadTextures("game_tiger_gui_gui-tiger-btn-start-1.png","game_tiger_gui_gui-tiger-btn-start-1-click.png","game_tiger_gui_gui-tiger-btn-start-1.png",1)
            self.spinBtn:setTouchEnabled(true)
            if self._IsRunning then
                self:compulsoryStop()
            end
        end
    end
end

function PlutusGameTableUI:armatureEvent1(armatureBack, movementType, movementID) 
	if (movementType == ccs.MovementEventType.complete) then
		armatureBack:getAnimation():stop()
		armatureBack:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function()
            armatureBack:removeFromParent()
        end), nil))
	end
end

function PlutusGameTableUI:imageEventCallBack(pSender, eventType)
    if (eventType ~= ccui.TouchEventType.ended) then
        return
    end
    local btn = pSender
    local name = btn:getName()

	if (name == "Image_BG") then
        if self._Image_num==1 then
            self._Image_button:setVisible(false)
            self._Image_num = 0
            self.downBtn:loadTextures("btn_menu1.png","btn_menu1.png","btn_menu1.png",1)
        end
	end
end


--获取上面游戏栏按钮
function PlutusGameTableUI:getUpMenu()
    local str = {}
    local winSize = cc.Director:getInstance():getWinSize()
    self._Scalex = 1280 / winSize.width
    self._Scaley = 720 / winSize.height

    local returnBtn = self._tableBG:getChildByName("Button_return")
    returnBtn:setScaleX(1/display.scaleX)
    self.downBtn = self._tableBG:getChildByName("Button_down")
    self.downBtn:setScaleX(1/display.scaleX)
    local setBtn = self._Image_button:getChildByName("Button_Set")
    --setBtn:setScaleX(1/display.scaleX)
    local recordBtn = self._Image_button:getChildByName("Button_Record")
    returnBtn:addTouchEventListener(handler(self, self.upBoxEventCallBack))
    self.downBtn:addTouchEventListener(handler(self, self.upBoxEventCallBack))
    setBtn:addTouchEventListener(handler(self, self.upBoxEventCallBack))
    recordBtn:addTouchEventListener(handler(self, self.upBoxEventCallBack))
    local moneyBox = self._tableBG:getChildByName("Image_MoneyBG")
    moneyBox:setScaleX(1/display.scaleX)
    local img_gold = moneyBox:getChildByName("Image_MoneyIco")
    img_gold:setEnabled(true)
    img_gold:addTouchEventListener(handler(self, self.upBoxEventCallBack))
    local btn_chat = self._tableBG:getChildByName("btn_chat")
    btn_chat:addTouchEventListener(handler(self, self.onChat))
    if (winSize.width / winSize.height <= 1.5) then
        moneyBox:setScaleY(self._Scaley)
    else
        moneyBox:setScaleX(self._Scalex)
    end

    self._scoreText = moneyBox:getChildByName("Image_MoneyIco"):getChildByName("text_winNote_0")
end
function PlutusGameTableUI:onChat(sender,eventType)
    if (eventType ~= ccui.TouchEventType.ended) then
        return
    end
    local ChatLayer = ChatLayer.new()
    self:addChild(ChatLayer,10000);
end

function PlutusGameTableUI:setGoldCoin(nScore)
    self._score = nScore
end

function PlutusGameTableUI:setFreeTimes(nFreeTimes)
    self._freeGames = nFreeTimes
    if self._freeGames>0 then
        self:gameReset(0.0)
    end
end

function PlutusGameTableUI:setBetScore(nBetScore)
    if nBetScore==0 then
        self._bet = 50
    else
        self._bet = nBetScore
    end
end

function PlutusGameTableUI:setWinGoldCoin(nWin)
    if(nWin > 0) then
        self._iWin = nWin
    else
        self._iWin = 0
    end
end

function PlutusGameTableUI:setRollImage(icons, freegame)
    self._icons = icons
    --self._iWin = wincoin
    self._IsRunning = true
    self._freeGames_N = freegame
    self._freeGames = freegame
    --unschedule(schedule_selector(GameTableUI::gameBetFailed))
    --_TypeScroll = *pTypeScroll
    if (self._freeGames == 0) then
        self:replaceScene(false)
        --[[self:runAction(cc.Sequence:create(cc.DelayTime:create(0.5), cc.CallFunc:create(function()
            self:resetStartMenuStatu(false, true)
        end), nil))]]--
    end
    self._imageRun = scheduler.scheduleGlobal(handler(self,self.imageRun), 0.1)  --启动图片滚动定时器
    self._imagePlay = scheduler.scheduleGlobal(handler(self,self.imagePlay), 0.01)  --启动图片滚动定时器
    self.delay1 = scheduler.performWithDelayGlobal(handler(self,self.showTheGameResult), 2.8)  --停止
end

--获取下面游戏栏按钮
function PlutusGameTableUI:getDownMenu()
    self.spinBtn = self._tableBG:getChildByName("Button_spin")
    self.guajiBtn = self._tableBG:getChildByName("btnGuaji")
    self.guajiBtn:setScaleX(1/display.scaleX)
    self.guajiBtn:setVisible(false)
    local coins1Btn = self._tableBG:getChildByName("Button_Coins1")
    local coins2Btn = self._tableBG:getChildByName("Button_Coins2")
    local payTableBtn = self._Image_button:getChildByName("Button_payTable")

    self.spinBtn:setScaleX(1/display.scaleX)
    self.spinBtn:addTouchEventListener(handler(self,self.gameSpinButtonEventCallBack))
    self.guajiBtn:addTouchEventListener(handler(self,self.guajiButtonEventCallBack))
    payTableBtn:setScaleX(1/display.scaleX)
    payTableBtn:addTouchEventListener(handler(self,self.downBoxEventCallBack))
    coins1Btn:setScaleX(1/display.scaleX)
    coins1Btn:addTouchEventListener(handler(self,self.beatEventCallBack))
    coins2Btn:setScaleX(1/display.scaleX)
    coins2Btn:addTouchEventListener(handler(self,self.beatEventCallBack))

    local depositBox = self._tableBG:getChildByName("deposit_box")

    local str = string.format("%d", self._bet)
    self._betText = cc.Label:createWithSystemFont(str, "", 20)
    self._betText:setAnchorPoint(cc.p(1, 0.5))
    self._betText:setPosition(cc.p(depositBox:getContentSize().width * 0.9, depositBox:getContentSize().height / 2))
    self._betText:setVisible(false)
    depositBox:addChild(self._betText)

    self._totalBetText = self._tableBG:getChildByName("deposit_box"):getChildByName("text_note")
    local fNote = self._bet * 0.01 * self._lines
    if self._bet>=1000 then
        str = string.format("%d",fNote)
    else
        str = string.format("%.2f",fNote)
    end
    
    self._totalBetText:setString(str)
    self._winText = self._tableBG:getChildByName("winScore_box"):getChildByName("text_winNote")
    self._winText:setString("0.00")
end

function PlutusGameTableUI:getFreeGamesUI()
    self._freeGame1 = self._tableBG:getChildByName("Text_Free1")
    self._freeGame2 = self._tableBG:getChildByName("Text_Free2")
    --self._textStart = self._tableBG:getChildByName("Button_spin"):getChildByName("Text_Start")
end

function PlutusGameTableUI:showTheLines(enabled)
    local name = ""
    local lineBG = self._tableBG:getChildByName("Panel_LinesBG")
    lineBG:setLocalZOrder(1)

    --[[for i=1,9 do
        name = string.format("Image_Line%d", i)
        local line = lineBG:getChildByName(name)
        line:stopAllActions()
        name = string.format("Button_%d", i)
        local bet1 = betLineBG1:getChildByName(name)
        local bet2 = betLineBG2:getChildByName(name)

        if (i <= self._lines) then
            line:setVisible(true)
            --bet1:setBright(true)
            --bet2:setBright(true)
        else
            line:setVisible(false)
            --bet1:setBright(false)
            --bet2:setBright(false)
        end
        if (not enabled) then
            line:stopAllActions()
            line:setVisible(false)
        end
    end]]--
end

--绘制裁剪区域
function PlutusGameTableUI:createCliper()
    local winSize = cc.Director:getInstance():getWinSize()

    for i = 1,5 do
        local shap = cc.DrawNode:create()
        local point = { cc.p(0, 0), cc.p(200, 0), cc.p(200, 520), cc.p(0, 520) }
        shap:drawPolygon1(point,{fillColor=cc.c4f(1, 1, 1, 1), borderWidth=2, borderColor=cc.c4f(1, 1, 1, 1)})
        local cliper = cc.ClippingNode:create()
        cliper:setStencil(shap)
        cliper:setAnchorPoint(cc.p(0.5, 0.5))
        cliper:ignoreAnchorPointForPosition(false)
        cliper:setPosition(cc.p(370 + 176*(i-1), self._tableBG:getContentSize().height * 0.2))

        self._tableBG:addChild(cliper,2)--把要滚动的图片加入到裁剪区域 
        self._clipperArr[i] = cliper
    end

    self._coinCountBig = ccui.TextBMFont:create("1.85",GAME_FONT_1)
    self._coinCountBig:setAnchorPoint(cc.p(0.5,0.5))
    self._coinCountBig:setPosition(cc.p(812,375))
    --[[self._coinCountBig:setScaleY(0.75)
    local winSize = cc.Director:getInstance():getWinSize()
    if (winSize.width / winSize.height > 1.78) then 
        self._coinCountBig:setScaleX(0.65)
    else
        self._coinCountBig:setScaleX(0.7)
    end]]--
    
    self._tableBG:addChild(self._coinCountBig,3)
    self._coinCountBig:setVisible(false)
end

function PlutusGameTableUI:createPlayImage()
    local cache = cc.SpriteFrameCache:getInstance()
	local winSize = cc.Director:getInstance():getWinSize()
		--往裁剪区域添加显示图片，默认添加四张交替滚动显示
	local k = 0
    math.randomseed(os.time())
	for j,child in pairs(self._clipperArr) do
	    k = k+1
		local name = ""
		for i = 0,3 do
			local num = math.random(1,12)
            while (num == 10) do
                num = math.random(1,12)
            end

			name = string.format("statik_low%d.png", num)
			local playImage = PlutusGameImage.new(name, num)           --创建要显示的图片 
			playImage:setPosition(cc.p(88, 430 - (i * 168)))

            self._imagescaleX = playImage:getScaleX()
            self._imagescaleY = playImage:getScaleY()

            local rollImage = cc.Sprite:createWithSpriteFrameName("ui_goldItem_1.png")
            rollImage:setAnchorPoint(cc.p(0.5,0.5))
            rollImage:setPosition(cc.p(88, 430 - (i * 168)))
            rollImage:runAction(cc.RepeatForever:create(self._animate))
            child:addChild(rollImage)
            rollImage:setVisible(false)

			child:addChild(playImage)
			if (k == 1) then
                self._imageArr1[i] = playImage--图片1数组
                self._pic1[i] = rollImage
                --[[if i>=1 then
                    local temp = (k-1)*3+i
                    local str = string.format("Panel_particle%d", temp)
                    self._particle1[i] = self._tableBG:getChildByName(str)
                    self._particle1[i]:setLocalZOrder(2)
                    self._particle1[i]:setVisible(false)
                end]]--
            end
			if (k == 2) then
                self._imageArr2[i] = playImage--图片2数组
                self._pic2[i] = rollImage
                --[[if i>=1 then
                    local temp = (k-1)*3+i
                    local str = string.format("Panel_particle%d", temp)
                    self._particle2[i] = self._tableBG:getChildByName(str)
                    self._particle2[i]:setLocalZOrder(2)
                    self._particle2[i]:setVisible(false)
                end]]--
            end
			if (k == 3) then
                self._imageArr3[i] = playImage--图片3数组
                self._pic3[i] = rollImage
                --[[if i>=1 then
                    local temp = (k-1)*3+i
                    local str = string.format("Panel_particle%d", temp)
                    self._particle3[i] = self._tableBG:getChildByName(str)
                    self._particle3[i]:setLocalZOrder(2)
                    self._particle3[i]:setVisible(false)
                end]]--
            end
			if (k == 4) then
                self._imageArr4[i] = playImage--图片4数组
                self._pic4[i] = rollImage
                --[[if i>=1 then
                    local temp = (k-1)*3+i
                    local str = string.format("Panel_particle%d", temp)
                    self._particle4[i] = self._tableBG:getChildByName(str)
                    self._particle4[i]:setLocalZOrder(2)
                    self._particle4[i]:setVisible(false)
                end]]--
            end
			if (k == 5) then
                self._imageArr5[i] = playImage--图片5数组
                self._pic5[i] = rollImage
                --[[if i>=1 then
                    local temp = (k-1)*3+i
                    local str = string.format("Panel_particle%d", temp)
                    self._particle5[i] = self._tableBG:getChildByName(str)
                    self._particle5[i]:setLocalZOrder(2)
                    self._particle5[i]:setVisible(false)
                end]]--
            end
		end
	end
end

function PlutusGameTableUI:upBoxEventCallBack(sender, eventType)
    local winSize = cc.Director:getInstance():getWinSize()
    if (eventType ~= ccui.TouchEventType.ended) then
        return
    end

    local btn = sender
    local name = btn:getName()

    if (name == "Button_return") then
        cc.Director:getInstance():getScheduler():setTimeScale(1.0)
		self:clear()
        local cache = cc.SpriteFrameCache:getInstance()
	    cache:removeSpriteFramesFromFile("app/game/plutus/res/Games/FarmFrenzy/Static_Icons/Slot_Icons.plist")
        cache:removeSpriteFramesFromFile("app/game/plutus/res/Games/FarmFrenzy/Static_Icons/Slot_Icons1.plist")
        cache:removeSpriteFramesFromFile("app/game/plutus/res/Games/FarmFrenzy/Static_Icons/Slot_Icons2.plist")
        cache:removeSpriteFramesFromFile("app/game/plutus/res/Games/FarmFrenzy/Static_Icons/Slot_Icons3.plist")
        cache:removeSpriteFramesFromFile("app/game/plutus/res/Games/FarmFrenzy/Static_Icons/Slot_Icons4.plist")
        cache:removeSpriteFramesFromFile("app/game/plutus/res/Games/FarmFrenzy/Static_Icons/newUI2.plist")
        g_GameController:forceExit()
    end
    if (name == "Button_down") then
        if self._Image_num==0 then
            self._Image_button:setVisible(true)
            self._Image_num = 1
            btn:loadTextures("btn_menu2.png","btn_menu2.png","btn_menu2.png",1)
        elseif self._Image_num==1 then
            self._Image_button:setVisible(false)
            self._Image_num = 0
            btn:loadTextures("btn_menu1.png","btn_menu1.png","btn_menu1.png",1)
        end
    end
    if (name == "Button_Set") then
        print("btn_set")
        local layer = GameSetLayer.new()
		self:addChild(layer, 10000)
        --layer:setScale(1/display.scaleX, 1/display.scaleY);
    end
    if (name == "Image_MoneyIco") then
        --local chargeLayer = ChargeLayer.new()
        --self:addChild(chargeLayer, 10000)
        --chargeLayer:open(ACTION_TYPE_LAYER.FADE,A self,cc.p(0,0), 999, CHILD_LAYER_STORE_TAG,true);
        --chargeLayer:setAnchorPoint(cc.p(0.5,0))
        --[[chargeLayer:setScaleX(display.scaleX)
        if (winSize.width / winSize.height > 1.78) then 
		    chargeLayer:setPositionX(winSize.width / 2 - 0.61 * winSize.width / display.scaleX);
	    end  ]]--
    end
    if name =="Button_Record" then
        local GameRecordLayer = GameRecordLayer.new(2)
        self:addChild(GameRecordLayer,10000)   
        GameRecordLayer:setScaleX(display.scaleX) 
        ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", {181})
	end  
end

function PlutusGameTableUI:gameSpinButtonEventCallBack(sender, eventType)
	if (eventType == ccui.TouchEventType.began) then
        if (self._schedulePush) then
            scheduler.unscheduleGlobal(self._schedulePush)
            self._schedulePush = nil
            self._schedulePushCount = 0
            self._schedulePush = scheduler.scheduleGlobal(handler(self, self.setTouchCount),0.3)
        else
            self._schedulePushCount = 0
            self._schedulePush = scheduler.scheduleGlobal(handler(self, self.setTouchCount),0.3)
        end
	elseif (eventType == ccui.TouchEventType.ended) then
        if (self._schedulePush) then
            scheduler.unscheduleGlobal(self._schedulePush)
            self._schedulePush = nil
            self._schedulePushCount = 0
        end
		if (not self._autoGame) then
            if (self._autoGameTime) then
                scheduler.unscheduleGlobal(self._autoGameTime)
            end
			if (not self._IsRunning) then
				g_GameMusicUtil:playSound("app/game/plutus/res/Games/FarmFrenzy/SlotSound/GameSound/spclk.wav")
				self:gameStart(0.1)
			else
				self:resetStartMenuStatu(false, false)
                if (self._showTheGameResult) then
                    scheduler.unscheduleGlobal(self._showTheGameResult)
                end
				self:compulsoryStop()
			end
		else
			if (self._IsRunning) then
				if (self._autoBegin) then
					self._autoBegin = false
					return
				end

				self._autoGame = false
				self:resetStartMenuStatu(false, false)
				if (self._showTheGameResult) then
                    scheduler.unscheduleGlobal(self._showTheGameResult)
                end
				self:compulsoryStop()
            else
                g_GameMusicUtil:playSound("app/game/plutus/res/Games/FarmFrenzy/SlotSound/GameSound/spclk.wav")
				self:gameStart(0.1)
			end
		end
    elseif (eventType == ccui.TouchEventType.canceled) then
        if (self._schedulePush) then
            scheduler.unscheduleGlobal(self._schedulePush)
            self._schedulePush = nil
            self._schedulePushCount = 0
        end
    end
end

function PlutusGameTableUI:setTouchCount()
    self._schedulePushCount = self._schedulePushCount + 0.3
    if self._schedulePushCount>1 then
        scheduler.unscheduleGlobal(self._schedulePush)
        self._schedulePush = nil
        self._schedulePushCount = 0

        self.spinBtn:setVisible(false)
        self.spinBtn:setEnabled(false)
        self.guajiBtn:setVisible(true)
        self.guajiBtn:setEnabled(true)

        local func = function (send,eventType)  end
        self.spinBtn:addTouchEventListener(func)

        g_GameMusicUtil:playSound("app/game/plutus/res/Games/FarmFrenzy/SlotSound/GameSound/spclk.wav")
        self:gameStart(0.1)

        self._autoGame = true
        --self:gameReset(0.0)
    end
end

function PlutusGameTableUI:guajiButtonEventCallBack(sender, eventType)
    if (eventType==ccui.TouchEventType.began) then
    elseif (eventType==ccui.TouchEventType.ended) then
        self._autoGame = false
        self.guajiBtn:setVisible(false)
        self.guajiBtn:setEnabled(false)
        self.spinBtn:setVisible(true)
        self.spinBtn:setEnabled(true)
        self.spinBtn:addTouchEventListener(handler(self,self.gameSpinButtonEventCallBack))

        self._tableBG:loadTexture("app/game/plutus/res/Games/FarmFrenzy/TableUi/Res/bg.jpg")
    end
end

function PlutusGameTableUI:downBoxEventCallBack(pSender, eventType)
    if (eventType ~= ccui.TouchEventType.ended) then
        return
    end
    local btn = pSender
    local name = btn:getName()

	if (name == "Button_payTable") then
			--_ruleNode:setVisible(true)
        g_GameMusicUtil:playSound("app/game/plutus/res/Games/FarmFrenzy/SlotSound/GameSound/ptable.wav")
		local payTable = self._tableBG:getChildByName("Image_payTable")
		payTable:setVisible(true)
		--self._panel_particle:setVisible(false)
		payTable:setLocalZOrder(3)
		local backBtn = payTable:getChildByName("back_game")
		local imgBG = payTable:getChildByName("Image_bg")
		local btn1 = payTable:getChildByName("Button_zhongjiang1")
		local btn2 = payTable:getChildByName("Button_zhongjiang2")
		btn1:setEnabled(true)
        btn1:setVisible(true)
        btn2:setEnabled(false)
        btn2:setVisible(false)

		backBtn:addTouchEventListener(handler(self, self.payTableEvenCallBack))
		btn1:addTouchEventListener(handler(self, self.payTableEvenCallBack))
		btn2:addTouchEventListener(handler(self, self.payTableEvenCallBack))
		imgBG:addTouchEventListener(handler(self, self.payTableEvenCallBack))
        imgBG:setSwallowTouches(true)
	end
end

--下注按钮回调
function PlutusGameTableUI:beatEventCallBack(pSender,eventType)
    if (eventType ~= ccui.TouchEventType.ended) then
        return
    end

    local btn = pSender
    local name = btn:getName()

    local betValue = {0, 50, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 5000, 10000, 50000, 100000, 250000, 500000}
    local betKey = 0
    local userDefault = cc.UserDefault:getInstance()
    for i = 1,18 do
        if (betValue[i] == self._bet) then
            betKey = i
        end
    end
    if (name == "Button_Line1") then
        self._lines = self._lines - 1
        if (self._lines < 1) then
            self._lines = 9
        end
        self:showTheLines(true)
        g_GameMusicUtil:playSound("app/game/plutus/res/Games/FarmFrenzy/SlotSound/GameSound/dec.wav")
    end
    if (name == "Button_Line2") then
        self._lines = self._lines + 1
        if (self._lines > 9) then
            self._lines = 1
        end
        self:showTheLines(true)
        g_GameMusicUtil:playSound("app/game/plutus/res/Games/FarmFrenzy/SlotSound/GameSound/inc.wav")
    end
    if (name == "Button_Coins1") then
        betKey = betKey - 1
        if (betKey == 1) then
            betKey = 18
        end
        self._bet = betValue[betKey]
        g_GameMusicUtil:playSound("app/game/plutus/res/Games/FarmFrenzy/SlotSound/GameSound/dec.wav")
    end
    if (name == "Button_Coins2") then
        betKey = betKey + 1
        if (betKey == 19) then
            betKey = 2
        end
        self._bet = betValue[betKey]
        g_GameMusicUtil:playSound("app/game/plutus/res/Games/FarmFrenzy/SlotSound/GameSound/inc.wav")
    end
    self:updateGameMessage(false)
    if (name == "Button_MaxLines") then
        self._lines = 9
        self:gameStart(0.0)
        self:updateGameMessage(true)
        g_GameMusicUtil:playSound("app/game/plutus/res/Games/FarmFrenzy/SlotSound/GameSound/spclk.wav")
    end
    userDefault:setIntegerForKey("Bet_value", self._bet)
    userDefault:setIntegerForKey("Lines_value", self._lines)
    userDefault:flush()
end

--payTable按钮回调
function PlutusGameTableUI:payTableEvenCallBack(pSender, eventType)
    if (eventType ~= ccui.TouchEventType.ended) then
        return
    end
    local btn = pSender
    local name = btn:getName()

    local payTable = self._tableBG:getChildByName("Image_payTable")
    payTable:setLocalZOrder(3)
    local backBtn = payTable:getChildByName("back_game")
	local imgBG = payTable:getChildByName("Image_bg")
	local btn1 = payTable:getChildByName("Button_zhongjiang1")
	local btn2 = payTable:getChildByName("Button_zhongjiang2")

    if (name == "back_game") then
        payTable:setVisible(false)
        --self._panel_particle:setVisible(true)
    end
    if (name == "Button_zhongjiang1") then
        btn1:setEnabled(false)
        btn1:setVisible(false)
        btn2:setEnabled(true)
        btn2:setVisible(true)
    end
    if (name == "Button_zhongjiang2") then
        btn1:setEnabled(true)
        btn1:setVisible(true)
        btn2:setEnabled(false)
        btn2:setVisible(false)
    end
    if (name == "Image_bg") then
        print("none")
    end
end

--是否自动游戏计时
function PlutusGameTableUI:autoGameTime(dt)
    self._autoGame = true
    self._autoBegin = true
    self:gameStart(0.1)
end

--强行停止
function PlutusGameTableUI:compulsoryStop()
    if (self._imageRun) then
        scheduler.unscheduleGlobal(self._imageRun)
    end
    if (self._imagePlay) then
        scheduler.unscheduleGlobal(self._imagePlay)
    end
    if (self._showTheGameResult) then
        scheduler.unscheduleGlobal(self._showTheGameResult)
    end
    if self._scheduleResult then
        scheduler.unscheduleGlobal(self._scheduleResult)
    end
    if self._schedulePush then
        scheduler.unscheduleGlobal(self._schedulePush)
    end
    g_GameMusicUtil:playSound("app/game/plutus/res/Games/FarmFrenzy/SlotSound/GameSound/f_stop.wav")
    local name = {}

    for i = 1,5 do
        local imageArr = {}
        if (i == 1) then
            imageArr = self._imageArr1
        end
        if (i == 2) then
            imageArr = self._imageArr2
        end
        if (i == 3) then
            imageArr = self._imageArr3
        end
        if (i == 4) then
            imageArr = self._imageArr4
        end
        if (i == 5) then
            imageArr = self._imageArr5
        end
        for k,image in pairs(imageArr) do
            local index = 0
            if k==0 then
                index = 1
            elseif k==1 then
                index = i
            elseif k==2 then
                index = i+5
            elseif k==3 then
                index = i+10
            end
            local name = string.format("statik_low%d.png", self._icons[index])
            if (k == 0) then
                local num2 = math.random(1,12)
                while (num2 == 10) do
                    num2 = math.random(1,12)
                end
                name = string.format("statik_low%d.png", num2)
            end
            image:imageStopAndSetFrame(k, name)
            image:setRun(false)--图片可转动属性设置为false
            image:setNumber(self._icons[index])
        end
    end
    local coins1Btn = self._tableBG:getChildByName("Button_Coins1")
    local coins2Btn = self._tableBG:getChildByName("Button_Coins2")

    coins1Btn:setTouchEnabled(true)
    coins2Btn:setTouchEnabled(true)

    local spinBtn = self._tableBG:getChildByName("Button_spin")
    spinBtn:setTouchEnabled(true)

    self:settleAccounts()
end


--本局游戏开始
function PlutusGameTableUI:gameStart(dt)
    self._iWin = 0
    for i = 0, table.nums(self._imageArr1)-1 do
        self._imageArr1[i]:stopAllActions()
        self._imageArr2[i]:stopAllActions()
        self._imageArr3[i]:stopAllActions()
        self._imageArr4[i]:stopAllActions()
        self._imageArr5[i]:stopAllActions()

        self._imageArr1[i]:setOpacity(255)
        self._imageArr2[i]:setOpacity(255)
        self._imageArr3[i]:setOpacity(255)
        self._imageArr4[i]:setOpacity(255)
        self._imageArr5[i]:setOpacity(255)
    end
    local coins1Btn = self._tableBG:getChildByName("Button_Coins1")
    local coins2Btn = self._tableBG:getChildByName("Button_Coins2")
    local spinBtn = self._tableBG:getChildByName("Button_spin")
    spinBtn:setTouchEnabled(false)

    coins1Btn:setTouchEnabled(false)
    coins2Btn:setTouchEnabled(false)

    self:showTheLines(false)--隐藏指示线条
    local cache = cc.SpriteFrameCache:getInstance()
    if (self._score < self._bet * self._lines) then
        self._autoGame = false
        --GamePromptLayer::create():showPrompt(GBKToUtf8("您的金币不够下注！"))
--MessageBox("You don't have enough coins for this bet", "Error")
        spinBtn:setTouchEnabled(true)
        coins1Btn:setTouchEnabled(true)
        coins2Btn:setTouchEnabled(true)

        self:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), cc.CallFunc:create(function()
            ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo("app/game/plutus/res/Games/FarmFrenzy/caishendao/caishendao.ExportJson")
            cc.Director:getInstance():getScheduler():setTimeScale(1.0)
            --self:backToRoom()
            return
        end), nil))
    end
     
--[[if (!HNRoomLogic::getInstance():isConnect())
{
gameBetFailed(0.0f)
return
}]]--

    g_GameMusicUtil:stopBGMusic(true)
    if (self._freeGames == 0) then
        if (self._score < self._bet * self._lines) then
            g_GameController:gameBet(self._bet,self._lines)
        else
            self._score = self._score - self._bet * self._lines
            self:setGoldCoin(self._score)
            self:setWinImageOut()
            g_GameController:gameBet(self._bet,self._lines)
            g_GameMusicUtil:playBGMusic("app/game/plutus/res/Games/FarmFrenzy/SlotSound/GameSound/reels.wav", true)
        end
    else
        --self._freeGames = self._freeGames - 1
        self:setWinImageOut()
        g_GameController:gameBet(self._bet,self._lines)
        g_GameMusicUtil:playBGMusic("app/game/plutus/res/Games/FarmFrenzy/SlotSound/GameSound/free.wav", true)
    end
end

--开启/关闭按钮点击
function PlutusGameTableUI:upDataMenuEnabled(enabled)
    local depositReduceBtn = self._tableBG:getChildByName("Button_Coins1")
    --depositReduceBtn:setBright(enabled)
    depositReduceBtn:setEnabled(enabled)
    local depositAddBtn = self._tableBG:getChildByName("Button_Coins2")
    --depositAddBtn:setBright(enabled)
    depositAddBtn:setEnabled(enabled)
    local payTableBtn = self._tableBG:getChildByName("Button_payTable")
    --payTableBtn:setBright(enabled)
    payTableBtn:setEnabled(enabled)
end

function PlutusGameTableUI:resetStartMenuStatu(bStart, enabled)
    local coins1Btn = self._tableBG:getChildByName("Button_Coins1")
    local coins2Btn = self._tableBG:getChildByName("Button_Coins2")
    local spinBtn = self._tableBG:getChildByName("Button_spin")
    spinBtn:setTouchEnabled(enabled)
    spinBtn:setEnabled(enabled)
    coins1Btn:setTouchEnabled(enabled)
    coins1Btn:setEnabled(enabled)
    coins2Btn:setTouchEnabled(enabled)
    coins2Btn:setEnabled(enabled)
end

--/*--------------------------------图片滚动-------------------------------*/
--图片顺序启动
function PlutusGameTableUI:imageRun(dt)
    self._scheduleNum = self._scheduleNum + 1
    if (self._scheduleNum == 1) then
        for k,image in pairs(self._imageArr1) do
            image:setRun(true)
        end
    end
    if (self._scheduleNum == 2) then
        for k,image in pairs(self._imageArr2) do
            image:setRun(true)
        end
    end
    if (self._scheduleNum == 3) then
        for k,image in pairs(self._imageArr3) do
            image:setRun(true)
        end
    end
    if (self._scheduleNum == 4) then
        for k,image in pairs(self._imageArr4) do
            image:setRun(true)
        end
    end
    if (self._scheduleNum == 5) then
        for k,image in pairs(self._imageArr5) do
            image:setRun(true)
        end
        scheduler.unscheduleGlobal(self._imageRun)
        self._scheduleNum = 0
    end
end
--图片顺序启动
function PlutusGameTableUI:imagePlay(dt)
    for k,image in pairs(self._imageArr1) do
        if (image:getRun()) then
            image:imageRun(-40)
        end
    end
    for k,image in pairs(self._imageArr2) do
        if (image:getRun()) then
            image:imageRun(-40)
        end
    end
    for k,image in pairs(self._imageArr3) do
        if (image:getRun()) then
            image:imageRun(-40)
        end
    end
    for k,image in pairs(self._imageArr4) do
        if (image:getRun()) then
            image:imageRun(-40)
        end
    end
    for k,image in pairs(self._imageArr5) do
        if (image:getRun()) then
            image:imageRun(-40)
        end
    end
end
--/*----------------------------------------------------------------------*/

--下注
--[[void GameTableUI::gameBeat(float dt)
{
_tableLogic:gameBet(_bet, _lines) --下注，发送数据到服务端
}]]--

--更新信息
function PlutusGameTableUI:updateGameMessage(enabled)
    local str = string.format("%d", self._lines)
    --self._linesText:setString(str)
    str = string.format("%d", self._bet)
    self._betText:setString(str)
    local fNote = self._bet*0.01*self._lines
    if self._bet>=1000 then
        str = string.format("%d", fNote)
    else
        str = string.format("%.2f", fNote)
    end
    -- self:setRecordId(g_GameController.m_sRecordId)
    self._totalBetText:setString(str)
    if (enabled) then
        local fWinNote = self._iWin*0.01
        str = string.format("%.2f",fWinNote)
        self._winText:setString(str)

        local fMoney = self._score*0.01
        str = string.format("%.2f",fMoney)
        self._scoreText:setString(str)
        -- if g_GameController.m_sRecordId and g_GameController.m_sRecordId ~= "" then
        --     str = "ID:" .. g_GameController.m_sRecordId
        --     self.mTextRecord:setString( str )
        --     self.mTextRecord:setVisible(true)
        -- end
    end

    local spinBtn = self._tableBG:getChildByName("Button_spin")

    if (self._freeGames>0) or (self._autoGame) then
        self._freeGame1:setVisible(true)
        self._freeGame2:setVisible(true)
        local _str = string.format("%d",self._freeGames)
        self._freeGame1:setString(_str)
        --self._textStart:setVisible(false)
    else
        self._freeGame1:setVisible(false)
        self._freeGame2:setVisible(false)
        --self._textStart:setVisible(true)
    end
end

--自动停止定时器调用函数，用来启动图片延迟自动停止
function PlutusGameTableUI:showTheGameResult(dt)
    self._showTheGameResult = scheduler.scheduleGlobal(handler(self,self.autoShowTheGameResult), 0.1)
    local coins1Btn = self._tableBG:getChildByName("Button_Coins1")
    local coins2Btn = self._tableBG:getChildByName("Button_Coins2")
    local spinBtn = self._tableBG:getChildByName("Button_spin")
    spinBtn:setTouchEnabled(true)
    coins1Btn:setTouchEnabled(true)
    coins2Btn:setTouchEnabled(true)
end

--图片延迟一张张自动停止
function PlutusGameTableUI:autoShowTheGameResult(dt)
    g_GameMusicUtil:playSound("app/game/plutus/res/Games/FarmFrenzy/SlotSound/GameSound/f_stop.wav")
    local imageArr = {}
    local i = 0
    if (self._scheduleNum == 0) then
        imageArr = self._imageArr1
        i = 1
    end
    if (self._scheduleNum == 1) then
        imageArr = self._imageArr2
        i = 2
    end
    if (self._scheduleNum == 2) then
        imageArr = self._imageArr3
        i = 3
    end
    if (self._scheduleNum == 3) then
        imageArr = self._imageArr4
        i = 4
    end
    if (self._scheduleNum == 4) then
        imageArr = self._imageArr5
        i = 5
    end
    local name = ""
    for k,image in pairs(imageArr) do
        local index = 0
        if k==0 then
            index = i
        elseif k==1 then
            index = i
        elseif k==2 then
            index = i+5
        elseif k==3 then
            index = i+10
        end
        name = string.format("statik_low%d.png", self._icons[index])
        if (k == 0) then
            local num2 = math.random(1,12)
            while (num2 == 10) do
                num2 = math.random(1,12)
            end
            name = string.format("statik_low%d.png", num2)
        end
        image:imageStopAndSetFrame(k, name)
        image:setRun(false)--图片可转动属性设置为false
        image:setNumber(self._icons[index])
    end
    self._scheduleNum = self._scheduleNum + 1
--如果没有可滚动的图片则停止所有定时器并结算
    if (self._scheduleNum == 5) then
        --self._score = self._score + self._iWin
        --self:setGoldCoin(self._score)
        self._scheduleNum = 0
        if (self._imageRun) then
            scheduler.unscheduleGlobal(self._imageRun)
        end
        if (self._imagePlay) then
            scheduler.unscheduleGlobal(self._imagePlay)
        end
        if (self._showTheGameResult) then
            scheduler.unscheduleGlobal(self._showTheGameResult)
        end
        if self._scheduleResult then
            scheduler.unscheduleGlobal(self._scheduleResult)
        end
        if self._schedulePush then
            scheduler.unscheduleGlobal(self._schedulePush)
        end
        self:settleAccounts()
        self:upDataMenuEnabled(true)
        self:resetStartMenuStatu(false,false)
    end
end

--结算
function PlutusGameTableUI:settleAccounts()
    g_GameMusicUtil:stopBGMusic(true)

    self:updateGameMessage(true)--更新参数显示

    local linesBG = self._tableBG:getChildByName("Panel_LinesBG")
--如果有中奖则展示中奖指示线条
    if (self._iWin > 0) then
        g_GameController:ChecKLine(self._icons, self._lines, self._bet)
        g_GameMusicUtil:playBGMusic("app/game/plutus/res/Games/FarmFrenzy/SlotSound/GameSound/g_win.wav")
    end

    self:gameReset(0.0)
end

function PlutusGameTableUI:gameReset(dt)
    self._IsRunning = false
    self._scheduleNum = 0
--如果有小游戏则进入小游戏
    --[[if (self._TypeScroll.iSmallGame) then
        scheduler.performWithDelayGlobal(handler(self,self.enterSmallGame), 3.0)
        self._TypeScroll.iSmallGame = false
        return
    end]]--

    if (self._freeGames == 0) then
        if (self._bFreeAutoGame) then--免费游戏的最后一局，重置参数
            self._bFreeAutoGame = false
            local str = string.format("%d", self._freeGames_N)
            --freeGameText:setString(str)
--sprintf(str, "%lld", _money - _money_N)
            local fMoney = (self._score - self._score_N)*0.01
            str = string.format("%.2f",fMoney)
            self:gameReset(0.0) 
            g_GameMusicUtil:playSound("app/game/plutus/res/Games/FarmFrenzy/SlotSound/GameSound/freebg.wav")
            self:replaceScene(false)
            return
        end
        if (not self._autoGame) and (not self._bFreeAutoGame) then
            self:runAction(cc.Sequence:create(cc.DelayTime:create(0.5), cc.CallFunc:create(function()
                self:upDataMenuEnabled(true)--如果没有出现免费游戏奖励和小游戏奖励则恢复按钮点击功能
                self:resetStartMenuStatu(true, true)
            end), nil))
            return
        end
    end
    if (self._freeGames > 0) then--免费游戏进行中
        self:replaceScene(true)
        self:resetStartMenuStatu(false, false)
        if (not self._bFreeAutoGame) then--免费游戏第一局
            self._bFreeAutoGame = true
            self._score_N = self._score
            self._freeGames_N = self._freeGames
            g_GameMusicUtil:playSound("app/game/plutus/res/Games/FarmFrenzy/SlotSound/GameSound/scatte.wav")
        end
        --local label = freeGame:getChildByName("freeGameText")
        --local str2 = string.format("%d", self._freeGames)
        --label:setString(str2)
--自动游戏时如果有奖励则等待2秒才重新开始，没奖励等待1秒重新开始
        if (self._iWin > 0) or (self._freeGames_N > 0) then
            local num = self._resultNum
            self.delay2 = scheduler.performWithDelayGlobal(handler(self,self.gameStart), num + 1)
        else 
            self.delay2 = scheduler.performWithDelayGlobal(handler(self,self.gameStart), 1.5)
        end
        return
    end

    if (self._autoGame) then
--自动游戏时如果有奖励则等待2秒才重新开始，没奖励等待1秒重新开始
        if (self._iWin > 0) or (self._freeGames_N > 0) then
            local num = self._resultNum
            self.delay2 = scheduler.performWithDelayGlobal(handler(self,self.gameStart), num+1)
            self:runAction(cc.Sequence:create(cc.DelayTime:create(1.5), cc.CallFunc:create(function()
                self:resetStartMenuStatu(false, true)
            end), nil))
        else
            self.delay2 = scheduler.performWithDelayGlobal(handler(self,self.gameStart), 1.5)
            self:runAction(cc.Sequence:create(cc.DelayTime:create(1.5), cc.CallFunc:create(function()
                self:resetStartMenuStatu(false, true)
            end), nil))
        end
    end
end

--进入小游戏
function PlutusGameTableUI:enterSmallGame(dt)
    g_GameMusicUtil:playSound("app/game/plutus/res/Games/FarmFrenzy/SlotSound/GameSound/bonus.wav")
    self._smallgame = PlutusSmallGameUI:createSmallGame(self._TypeScroll.iSmallImgid)
    self._smallgame:setPosition(cc.p(self._tableBG:getContentSize().width / 2, self._tableBG:getContentSize().height / 2))
    self._tableBG:addChild(self._smallgame, 4)
end

--游戏开始通知
function PlutusGameTableUI:dealGameStart()
--HNLOG_DEBUG("dealGameStart")
    self:upDataMenuEnabled(true)--收到游戏开始消息，打开按钮点击
end

function PlutusGameTableUI:replaceScene(isFree)
    if (isFree or self._autoGame) then
        self._tableBG:loadTexture("app/game/plutus/res/Games/FarmFrenzy/TableUi/Res/bg_freetime.jpg")
    else
        self._tableBG:loadTexture("app/game/plutus/res/Games/FarmFrenzy/TableUi/Res/bg.jpg")
    end
end

function PlutusGameTableUI:setShowResult(checkline, checkwin)
    self._checkline = checkline
    self._checkWin = checkwin
    self._resultNum = table.nums(checkline)
    self._scheduleResNum = 1
    for i = 0,3 do
        self._imageArr1[i]:setGray(true)
        self._imageArr2[i]:setGray(true)
        self._imageArr3[i]:setGray(true)
        self._imageArr4[i]:setGray(true)
        self._imageArr5[i]:setGray(true)
        self._pic1[i]:setVisible(false)
        self._pic2[i]:setVisible(false)
        self._pic3[i]:setVisible(false)
        self._pic4[i]:setVisible(false)
        self._pic5[i]:setVisible(false)
        --[[if i>=1 then
            self._particle1[i]:setVisible(false)
            self._particle2[i]:setVisible(false)
            self._particle3[i]:setVisible(false)
            self._particle4[i]:setVisible(false)
            self._particle5[i]:setVisible(false)
        end]]--
    end

    if self._freeGames > 0 then
        self._scheduleResult = scheduler.performWithDelayGlobal(handler(self,self.setWinAction), 1.0)
    else
        self._scheduleResult = scheduler.scheduleGlobal(handler(self,self.setWinAction), 1.0)
    end

    for k=1,#self._checkline[self._scheduleResNum] do
        self:setWinImageColor(self._checkline[self._scheduleResNum][k], k, true, self._checkWin[self._scheduleResNum])
    end
    self._scheduleResNum = self._scheduleResNum + 1
    if self._scheduleResNum>self._resultNum then
        self._scheduleResNum = 1
    end
end

function PlutusGameTableUI:setWinAction()
    if #self._checkline == 0 then
        return
    end

    for i = 0,3 do
        self._imageArr1[i]:setGray(true)
        self._imageArr2[i]:setGray(true)
        self._imageArr3[i]:setGray(true)
        self._imageArr4[i]:setGray(true)
        self._imageArr5[i]:setGray(true)
        self._pic1[i]:setVisible(false)
        self._pic2[i]:setVisible(false)
        self._pic3[i]:setVisible(false)
        self._pic4[i]:setVisible(false)
        self._pic5[i]:setVisible(false)
        --[[if i>=1 then
            self._particle1[i]:setVisible(false)
            self._particle2[i]:setVisible(false)
            self._particle3[i]:setVisible(false)
            self._particle4[i]:setVisible(false)
            self._particle5[i]:setVisible(false)
        end]]--
    end

    if self._scheduleResNum>self._resultNum then
        --[[for i,table in pairs(self._checkline) do
            for k=1,#table do
                self:setWinImageColor(table[k], k, false, self._iWin)
            end
        end]]--
        self._scheduleResNum = 1
    else
        for k=1,#self._checkline[self._scheduleResNum] do
            self:setWinImageColor(self._checkline[self._scheduleResNum][k], k, true, self._checkWin[self._scheduleResNum])
        end
    end
    self._scheduleResNum = self._scheduleResNum + 1
    if self._scheduleResNum>self._resultNum then
        self._scheduleResNum = 1
    end
end

function PlutusGameTableUI:setWinImageColor(row,col,flag,reward)
    local scaleIn = cc.ScaleTo:create(0.5, self._imagescaleX + 0.1, self._imagescaleY + 0.1)
    local scaleOut = cc.ScaleTo:create(0.5, self._imagescaleX, self._imagescaleY)

    local index = (row-1)*5+col

    if (col == 1) then
        for k = 1,3 do
            if k==row then
                self._pic1[k-1]:setVisible(true)
                --self._particle1[k]:setVisible(true)
            else
                if flag then
                    self._pic1[k-1]:setVisible(false)
                    --self._particle1[k]:setVisible(false)
                end
            end
        end
        if self._icons[index]<5 then    
            local aniIconFrames = {}
            for j=1,29 do
                local iconPng = string.format("MG_Symbol_00%d_%d.png",self._icons[index],j)
                local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(iconPng)
                local animationFrame = cc.AnimationFrame:create(frame,0.2,{})
                table.insert(aniIconFrames, animationFrame)
            end
            local animation = cc.Animation:create(aniIconFrames,0.2)
            local anim = cc.Animate:create(animation)
            self._imageArr1[row]:setPlay(anim)
        elseif self._icons[index]==12 then
            local aniIconFrames = {}
            for j=1,29 do
                local iconPng = string.format("coin_%d.png",j)
                local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(iconPng)
                local animationFrame = cc.AnimationFrame:create(frame,0.2,{})
                table.insert(aniIconFrames, animationFrame)
            end
            local animation = cc.Animation:create(aniIconFrames,0.2)
            local anim = cc.Animate:create(animation)
            self._imageArr1[row]:setPlay(anim)
        else
            self._imageArr1[row]:setPlay(cc.Sequence:create(scaleIn, scaleOut))
        end
        self._imageArr1[row]:setGray(false)
    end
    if (col == 2) then
        for k = 1,3 do
            if k==row then
                self._pic2[k-1]:setVisible(true)
                --self._particle2[k]:setVisible(true)
            else
                if flag then
                    self._pic2[k-1]:setVisible(false)
                    --self._particle2[k]:setVisible(false)
                end
            end
        end
        if self._icons[index]<5 then
            local aniIconFrames = {}
            for j=1,29 do
                local iconPng = string.format("MG_Symbol_00%d_%d.png",self._icons[index],j)
                local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(iconPng)
                local animationFrame = cc.AnimationFrame:create(frame,0.2,{})
                table.insert(aniIconFrames, animationFrame)
            end
            local animation = cc.Animation:create(aniIconFrames,0.2)
            local anim = cc.Animate:create(animation)
            self._imageArr2[row]:setPlay(anim)
        elseif self._icons[index]==12 then
            local aniIconFrames = {}
            for j=1,29 do
                local iconPng = string.format("coin_%d.png",j)
                local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(iconPng)
                local animationFrame = cc.AnimationFrame:create(frame,0.2,{})
                table.insert(aniIconFrames, animationFrame)
            end
            local animation = cc.Animation:create(aniIconFrames,0.2)
            local anim = cc.Animate:create(animation)
            self._imageArr2[row]:setPlay(anim)
        else
            self._imageArr2[row]:setPlay(cc.Sequence:create(scaleIn, scaleOut))
        end
        self._imageArr2[row]:setGray(false)
    end
    if (col == 3) then
        for k = 1,3 do
            if k==row then
                self._pic3[k-1]:setVisible(true)
                --self._particle3[k]:setVisible(true)
            else
                if flag then
                    self._pic3[k-1]:setVisible(false)
                    --self._particle3[k]:setVisible(false)
                end
            end
        end
        if self._icons[index]<5 then
            local aniIconFrames = {}
            for j=1,29 do
                local iconPng = string.format("MG_Symbol_00%d_%d.png",self._icons[index],j)
                local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(iconPng)
                local animationFrame = cc.AnimationFrame:create(frame,0.2,{})
                table.insert(aniIconFrames, animationFrame)
            end
            local animation = cc.Animation:create(aniIconFrames,0.2)
            local anim = cc.Animate:create(animation)
            self._imageArr3[row]:setPlay(anim)
        elseif self._icons[index]==12 then
            local aniIconFrames = {}
            for j=1,29 do
                local iconPng = string.format("coin_%d.png",j)
                local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(iconPng)
                local animationFrame = cc.AnimationFrame:create(frame,0.2,{})
                table.insert(aniIconFrames, animationFrame)
            end
            local animation = cc.Animation:create(aniIconFrames,0.2)
            local anim = cc.Animate:create(animation)
            self._imageArr3[row]:setPlay(anim)
        else
            self._imageArr3[row]:setPlay(cc.Sequence:create(scaleIn, scaleOut))
        end
        self._imageArr3[row]:setGray(false)
    end
    if (col == 4) then
        for k = 1,3 do 
            if k==row then
                self._pic4[k-1]:setVisible(true)
                --self._particle4[k]:setVisible(true)
            else
                if flag then
                    self._pic4[k-1]:setVisible(false)
                    --self._particle4[k]:setVisible(false)
                end
            end
        end
        if self._icons[index]<5 then
            local aniIconFrames = {}
            for j=1,29 do
                local iconPng = string.format("MG_Symbol_00%d_%d.png",self._icons[index],j)
                local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(iconPng)
                local animationFrame = cc.AnimationFrame:create(frame,0.2,{})
                table.insert(aniIconFrames, animationFrame)
            end
            local animation = cc.Animation:create(aniIconFrames,0.2)
            local anim = cc.Animate:create(animation)
            self._imageArr4[row]:setPlay(anim)
        elseif self._icons[index]==12 then
            local aniIconFrames = {}
            for j=1,29 do
                local iconPng = string.format("coin_%d.png",j)
                local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(iconPng)
                local animationFrame = cc.AnimationFrame:create(frame,0.2,{})
                table.insert(aniIconFrames, animationFrame)
            end
            local animation = cc.Animation:create(aniIconFrames,0.2)
            local anim = cc.Animate:create(animation)
            self._imageArr4[row]:setPlay(anim)
        else
            self._imageArr4[row]:setPlay(cc.Sequence:create(scaleIn, scaleOut))
        end
        self._imageArr4[row]:setGray(false)
    end
    if (col == 5) then
        for k = 1,3 do
            if k==row then
                self._pic5[k-1]:setVisible(true)
                --self._particle5[k]:setVisible(true)
            else
                if flag then
                    self._pic5[k-1]:setVisible(false)
                    --self._particle5[k]:setVisible(false)
                end
            end
        end
        if self._icons[index]<5 then
            local aniIconFrames = {}
            for j=1,29 do
                local iconPng = string.format("MG_Symbol_00%d_%d.png",self._icons[index],j)
                local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(iconPng)
                local animationFrame = cc.AnimationFrame:create(frame,0.2,{})
                table.insert(aniIconFrames, animationFrame)
            end
            local animation = cc.Animation:create(aniIconFrames,0.2)
            local anim = cc.Animate:create(animation)
            self._imageArr5[row]:setPlay(anim)
        elseif self._icons[index]==12 then
            local aniIconFrames = {}
            for j=1,29 do
                local iconPng = string.format("coin_%d.png",j)
                local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(iconPng)
                local animationFrame = cc.AnimationFrame:create(frame,0.2,{})
                table.insert(aniIconFrames, animationFrame)
            end
            local animation = cc.Animation:create(aniIconFrames,0.2)
            local anim = cc.Animate:create(animation)
            self._imageArr5[row]:setPlay(anim)
        else
            self._imageArr5[row]:setPlay(cc.Sequence:create(scaleIn, scaleOut))
        end
        self._imageArr5[row]:setGray(false)
    end

    self._coinCountBig:setVisible(true)
    local fWinNote = reward*0.01
    local str = string.format("%.2f",fWinNote)
    self._coinCountBig:setString(str)
end

function PlutusGameTableUI:setWinImageOut()
    self._checkline = {}
    self._checkWin = {}
    self._resultNum = 0
    self._scheduleResNum = 0
    if self._scheduleResult then
        scheduler.unscheduleGlobal(self._scheduleResult)
    end
    for k = 0,3 do
        self._pic1[k]:setVisible(false)
        self._pic2[k]:setVisible(false)
        self._pic3[k]:setVisible(false)
        self._pic4[k]:setVisible(false)
        self._pic5[k]:setVisible(false)
        self._imageArr1[k]:setGray(false)
        self._imageArr1[k]:stopAllActions()
        self._imageArr1[k]:setScaleX(self._imagescaleX)
        self._imageArr1[k]:setScaleY(self._imagescaleY)
        self._imageArr2[k]:setGray(false)
        self._imageArr2[k]:stopAllActions()
        self._imageArr2[k]:setScaleX(self._imagescaleX)
        self._imageArr2[k]:setScaleY(self._imagescaleY)
        self._imageArr3[k]:setGray(false)
        self._imageArr3[k]:stopAllActions()
        self._imageArr3[k]:setScaleX(self._imagescaleX)
        self._imageArr3[k]:setScaleY(self._imagescaleY)
        self._imageArr4[k]:setGray(false)
        self._imageArr4[k]:stopAllActions()
        self._imageArr4[k]:setScaleX(self._imagescaleX)
        self._imageArr4[k]:setScaleY(self._imagescaleY)
        self._imageArr5[k]:setGray(false)
        self._imageArr5[k]:stopAllActions()
        self._imageArr5[k]:setScaleX(self._imagescaleX)
        self._imageArr5[k]:setScaleY(self._imagescaleY)
        --[[if k>=1 then
            self._particle1[k]:setVisible(false)
            self._particle2[k]:setVisible(false)
            self._particle3[k]:setVisible(false)
            self._particle4[k]:setVisible(false)
            self._particle5[k]:setVisible(false)
        end]]--
    end
    self._coinCountBig:setVisible(false)
end

function PlutusGameTableUI:addBoxAnimation()
    local animationFrames = {}
    for i=1,5 do
        local iceagePng = string.format("ui_goldItem_%d.png",i)
        local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(iceagePng)
        local animationFrame = cc.AnimationFrame:create(frame,0.2,{})
        table.insert(animationFrames, animationFrame)
    end
    local animation = cc.Animation:create(animationFrames,0.2)
    self._animate = cc.Animate:create(animation)
end

function PlutusGameTableUI:addRollAnimation(num)
    self._pic[num]:runAction(cc.RepeatForever:create(self._animate))
    self._pic[num]:setVisible(true)
end

function PlutusGameTableUI:clear()
	if (self._imageRun) then
        scheduler.unscheduleGlobal(self._imageRun)
		self._imageRun = nil
    end
    if (self._imagePlay) then
        scheduler.unscheduleGlobal(self._imagePlay)
		self._imagePlay = nil
    end
    if (self._showTheGameResult) then
        scheduler.unscheduleGlobal(self._showTheGameResult)
		self._showTheGameResult = nil
    end
    if (self.delay1) then
        scheduler.unscheduleGlobal(self.delay1)
		self.delay1 = nil
    end
    if (self.delay2) then
        scheduler.unscheduleGlobal(self.delay2)
		self.delay2 = nil
    end
    if self._scheduleResult then
        scheduler.unscheduleGlobal(self._scheduleResult)
		self._scheduleResult = nil
    end
end


function PlutusGameTableUI:setRecordId(id)
    if id and id ~= "" then
        self.mTextRecord:setVisible(true)
        local str = string.format("牌局ID:%s", id)
        self.mTextRecord:setString( str )
    end
end

return PlutusGameTableUI