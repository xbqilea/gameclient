module(..., package.seeall)


CS_M2C_Plutus_Start_Nty =
{
	{ 1,	1, 'm_score'			 ,  'UINT'						, 1		, '玩家分数'},
	{ 2,	1, 'm_freeTimes'	 	 ,  'INT'						, 1		, '剩余免费游戏次数'},
	{ 3,	1, 'm_betScore'	 	 	 ,  'UINT'						, 1		, '触发免费次数时下注金额'},
	{ 4,	1, 'm_recordId'       	 ,  'STRING'       				, 1     , '牌局编号' },
}

CS_C2M_Plutus_StartRoll_Req =
{
	{ 1,	1, 'm_deposit'			 ,  'INT'						, 1		, '单线分数'},
	{ 2,	1, 'm_lineNum'		 	 ,  'INT'						, 1		, '投注线数'},
}

CS_M2C_Plutus_ScrollResult_Ack =
{
	{ 1,	1, 'm_winCoin'			 ,  'INT'						, 1		, '单线分数'},
	{ 2,	1, 'm_icons'		 	 ,  'UBYTE'						, 15	, '小图标， 分别从左到右，从上到下'},
	{ 3,	1, 'm_startSmallGame'	 ,  'UBYTE'						, 1		, '1-开启小游戏 0-不开启'},
	{ 4,	1, 'm_smallGameIcons'	 ,  'UBYTE'						, 28	, '小游戏图标'},
	{ 5,	1, 'm_freeTimes'	 	 ,  'INT'						, 1		, '剩余免费游戏次数'},
	{ 6,	1, 'm_calcFreeWinCoin'	 ,  'UINT'						, 1		, '免费次数下总共赢了多少金币'},
	{ 7,	1, 'm_curCoin'	 		 ,  'UINT'						, 1		, '玩家当前金币, 扣除免费赢得金币'},
	{ 8,	1, 'm_ret'	 		 	 ,  'INT'						, 1		, '错误码, 0表示成功'},
	{ 9,	1, 'm_recordId'       	 ,  'STRING'       				, 1     , '牌局编号' },
}

CS_C2M_Plutus_SmallGameOp_Req =
{
	{ 1,	1, 'm_point'			 ,  'INT'						, 4		, '小游戏选择结果'},
}

CS_M2C_Plutus_SmallGameOp_Ack =
{
	{ 1,	1, 'm_winCoin'			 ,  'INT'						, 1		, '奖励金币'},
	{ 2,	1, 'm_bet'			 	 ,  'INT'						, 1		, '投注额'},
	{ 3,	1, 'm_multi'			 ,  'INT'						, 1		, '盈利倍数'},
}

CS_M2C_Plutus_Pond_Nty =
{
	{ 1,	1, 'm_flag'			 	 ,  'UBYTE'						, 1		, '标记'},
	{ 2,	1, 'm_nickName'			 ,  'STRING'					, 1		, '昵称'},
	{ 3,	1, 'm_getPond'			 ,  'UINT'						, 4		, '奖池获得奖励'},
	{ 4,	1, 'm_pond'			 	 ,  'UINT'						, 4		, '奖池总金额'},
}

CS_C2M_Plutus_Exit_Req =
{
	
}

CS_M2C_Plutus_Exit_Ack =
{
	{ 1,	1, 'm_ret'			 	 ,  'INT'						, 1		, '标记'},
}

CS_M2C_Plutus_Kick_Nty =
{
	{ 1,	1, 'm_type'			 	 ,  'UINT'						, 1		, '1-房间维护,请稍后再游戏 2-你已经被系统踢出房间,请稍后重试'},
}
