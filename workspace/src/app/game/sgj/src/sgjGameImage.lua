local sgjGameImage = class("sgjGameImage" ,function()
	  return display.newNode()
end)

function sgjGameImage:ctor(name,num)
    self._run = false
    self._name = name
    self._num = num
    self._image = nil
    self._imageTitle = nil
    self._imageLight = nil
    self:loadImage()
end

function sgjGameImage:loadImage()
	local cache = cc.SpriteFrameCache:getInstance()
	self._image = display.newSprite()
    self._image:setSpriteFrame(cache:getSpriteFrame(self._name))
    self._imageTitle = display.newSprite()
    self._imageTitle:setSpriteFrame(cache:getSpriteFrame("sgj_tter.png"))
    self._imageTitle:setAnchorPoint(cc.p(0.5,0.5))
    self._imageTitle:setScale(0.85)
    self._imageTitle:setPosition(cc.p(72,30))
    self._imageLight = display.newSprite()
    self._imageLight:setSpriteFrame(cache:getSpriteFrame("sgj_guang3.png"))
    self._imageLight:setAnchorPoint(cc.p(0.5,0.5))
    self._imageLight:setScale(0.9)
    self._imageLight:setPosition(cc.p(92,45))
    if self._num==9 then
        self._imageTitle:setSpriteFrame(cache:getSpriteFrame("sgj_wild2.png"))
        self._imageTitle:setPosition(cc.p(72,35))
        self._imageLight:setPosition(cc.p(92,50))
        self._imageTitle:setVisible(true)
        self._imageLight:setVisible(true)
    elseif self._num==10 then
        self._imageTitle:setSpriteFrame(cache:getSpriteFrame("sgj_tter.png"))
        self._imageTitle:setPosition(cc.p(71,30))
        self._imageLight:setPosition(cc.p(91,45))
        self._imageTitle:setVisible(true)
        self._imageLight:setVisible(true)
    elseif self._num==11 then
        self._imageTitle:setSpriteFrame(cache:getSpriteFrame("sgj_lvzi.png"))
        self._imageTitle:setPosition(cc.p(65,40))
        self._imageLight:setPosition(cc.p(85,55))
        self._imageTitle:setVisible(true)
        self._imageLight:setVisible(true)
    else
        self._imageTitle:setVisible(false)
        self._imageLight:setVisible(false)
    end
    self:addChild(self._image)
    self._image:addChild(self._imageTitle)
    self._image:addChild(self._imageLight)
    self._imageLight:runAction(cc.RepeatForever:create(cc.RotateBy:create(6.0,360)))
end

	--图片滚动
function sgjGameImage:imageRun(speed)
	local cache = cc.SpriteFrameCache:getInstance()
	self._image:setPositionY(self._image:getPositionY() + speed) --每帧图片高度减20
	if (self._image:getPositionY() <= -80) then
		local num = math.random(1,11)
        local name = string.format("statik_low%d_2.png", num)
		--当图片完全溢出了显示区域则随机更换一个frame，并把位置移动到显示区域下方
		self._image:setSpriteFrame(cache:getSpriteFrame(name))
		self._image:setPositionY(300)
	end
end

	--图片停止并替换frame
function sgjGameImage:imageStopAndSetFrame(imageNum,name)
	local cache = cc.SpriteFrameCache:getInstance()
	self._image:setSpriteFrame(cache:getSpriteFrame(name))
	self._image:setPositionY(188 - (imageNum * 10)-20)
	self._image:runAction(cc.MoveBy:create(0.03, cc.p(0, 20)))
end

function sgjGameImage:setRun(run)
    self._run = run
end

function sgjGameImage:getRun()
    return self._run
end

function sgjGameImage:setNumber(num)
    self._num = num
end

function sgjGameImage:setGray()
    local cache = cc.SpriteFrameCache:getInstance()
    local name = string.format("statik_low%d_3.png", self._num)
    self._image:setSpriteFrame(cache:getSpriteFrame(name))
    if self._num==9 then
        self._imageTitle:setSpriteFrame(cache:getSpriteFrame("sgj_wild2_2.png"))
        self._imageTitle:setPosition(cc.p(72,35))
        self._imageTitle:setVisible(true)
        self._imageLight:setSpriteFrame(cache:getSpriteFrame("sgj_guang3_2.png"))
        self._imageLight:setPosition(cc.p(92,50))
        self._imageLight:setVisible(true)
    elseif self._num==10 then
        self._imageTitle:setSpriteFrame(cache:getSpriteFrame("sgj_tter_2.png"))
        self._imageTitle:setPosition(cc.p(71,30))
        self._imageTitle:setVisible(true)
        self._imageLight:setSpriteFrame(cache:getSpriteFrame("sgj_guang3_2.png"))
        self._imageLight:setPosition(cc.p(91,45))
        self._imageLight:setVisible(true)
    elseif self._num==11 then
        self._imageTitle:setSpriteFrame(cache:getSpriteFrame("sgj_lvzi_2.png"))
        self._imageTitle:setPosition(cc.p(65,40))
        self._imageTitle:setVisible(true)
        self._imageLight:setSpriteFrame(cache:getSpriteFrame("sgj_guang3_2.png"))
        self._imageLight:setPosition(cc.p(85,55))
        self._imageLight:setVisible(true)
    else
        self._imageTitle:setVisible(false)
        self._imageLight:setVisible(false)
    end
end

function sgjGameImage:setBright()
    local cache = cc.SpriteFrameCache:getInstance()
    local name = string.format("statik_low%d.png", self._num)
    self._image:setSpriteFrame(cache:getSpriteFrame(name))
    if self._num==9 then
        self._imageTitle:setSpriteFrame(cache:getSpriteFrame("sgj_wild2.png"))
        self._imageTitle:setPosition(cc.p(72,35))
        self._imageTitle:setVisible(true)
        self._imageLight:setSpriteFrame(cache:getSpriteFrame("sgj_guang3.png"))
        self._imageLight:setPosition(cc.p(92,50))
        self._imageLight:setVisible(true)
    elseif self._num==10 then
        self._imageTitle:setSpriteFrame(cache:getSpriteFrame("sgj_tter.png"))
        self._imageTitle:setPosition(cc.p(71,30))
        self._imageTitle:setVisible(true)
        self._imageLight:setSpriteFrame(cache:getSpriteFrame("sgj_guang3.png"))
        self._imageLight:setPosition(cc.p(91,45))
        self._imageLight:setVisible(true)
    elseif self._num==11 then
        self._imageTitle:setSpriteFrame(cache:getSpriteFrame("sgj_lvzi.png"))
        self._imageTitle:setPosition(cc.p(65,40))
        self._imageTitle:setVisible(true)
        self._imageLight:setSpriteFrame(cache:getSpriteFrame("sgj_guang3.png"))
        self._imageLight:setPosition(cc.p(85,55))
        self._imageLight:setVisible(true)
    else
        self._imageTitle:setVisible(false)
        self._imageLight:setVisible(false)
    end
end

function sgjGameImage:setTitleOut()
    local cache = cc.SpriteFrameCache:getInstance()
    local name = string.format("statik_low%d.png", self._num)
    self._image:setSpriteFrame(cache:getSpriteFrame(name))
    self._imageTitle:setVisible(false)
    self._imageLight:setVisible(false)
end

function sgjGameImage:setPlay(action)
    self._image:runAction(action)
end

return sgjGameImage