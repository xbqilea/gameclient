local scheduler = require("framework.scheduler")
local CCGameSceneBase = require("src.app.game.common.main.CCGameSceneBase")
local sgjView = require("src.app.game.sgj.src.sgjView")
local DlgAlert = require("app.hall.base.ui.MessageBox")
   
local sgjScene= class("sgjScene", function()
	return CCGameSceneBase.new()
end)

function sgjScene:ctor()
   -- cc.Texture2D:setDefaultAlphaPixelFormat( cc.TEXTURE2_D_PIXEL_FORMAT_RGB_A8888 )
    self.m_sgjMainLayer         = nil
    self.m_sgjRuleLayer         = nil
    self:myInit()
end

-- 游戏场景初始化
function sgjScene:myInit()
	-- 主ui
	self:initSgjGameMainLayer()

	self:registBackClickHandler(handler(self, self.onBackButtonClicked)) -- Android & Windows注册返回按钮
	self:registBackClickHandler(handler(self, self.onBackButtonClicked))
	addMsgCallBack(self, MSG_ENTER_BACKGROUND, handler(self, self.onEnterBackground)) -- 转后台
	addMsgCallBack(self, MSG_ENTER_FOREGROUND, handler(self, self.onEnterForeground)) -- 转前台
end

function sgjScene:socketState( __msgName, __protocol, __connectName ) 
	if __protocol == Protocol.LoginServer then
        return
	elseif __connectName == cc.net.SocketTCP.EVENT_CLOSED  then
        --ConnectManager:reconnect()
	end
end

-- 进入场景
function sgjScene:onEnter()
   print("-----------sgjScene:onEnter()-----------------")
   ToolKit:setGameFPS(1/60.0)
end

-- 帧事件函数
-- @params dt( number ) 每帧间隔
function sgjScene:update(dt)

end

-- 初始化主ui
function sgjScene:initSgjGameMainLayer()
    self.m_sgjMainLayer = sgjView.new()
    self:addChild(self.m_sgjMainLayer,1000)
end

function sgjScene:addDialog(num)
    self.winSize = cc.Director:getInstance():getWinSize()
    self.dlg = cc.CSLoader:createNode("csb/common/update_tips_layer.csb") 
    self:addChild(self.dlg, 1001)

    self.dlg:setPosition(self.winSize.width / 2, self.winSize.height / 2)

    local title = "提示"
    local content = ""
    if num == 1 then
        content = "房间维护,请稍后再游戏"
    elseif num == 2 then
        content = "你已经被系统踢出房间,请稍后重试"
    end

    local btnText1 = "确定"
    local btnText2 = "取消"
    self.callback1 = function()
        g_GameController:releaseInstance()
    end
    self.callback2 =  nil

    local labelTitle = self.dlg:getChildByName("txt_title")
    labelTitle:enableOutline({r = 255, g = 255, b = 0}, 10)
    local labelContent = self.dlg:getChildByName("txt_content")
    local btn1 = self.dlg:getChildByName("btn_1")
    local btn2 = self.dlg:getChildByName("btn_2")
    btn1:enableOutline({r = 7, g = 131, b = 13, a = 255}, 2)
    btn2:enableOutline({r = 179, g = 94, b = 11, a = 255}, 2)
    labelTitle:setString(title)
    labelContent:setString(content)
    btn1:setTitleText(btnText1)
    local func = function ( __sender, __event )
        if __event == 2 then -- ccui.TouchEventType.ended
            if __sender:getName() == "btn_1" then
                if self.callback1 then
                    self.callback1()
                end
            elseif __sender:getName() == "btn_2" then
                if self.callback2 then
                    self.callback2()
                end
            end
        end
    end
    btn1:addTouchEventListener(func)
    if self.callback2 then
        btn2:setVisible(true)
        btn2:setTitleText(btnText2)
        btn2:addTouchEventListener(func)
    else
        btn2:setVisible(false)
        btn1:setPositionX(self.dlg:getChildByName("layout_dlg_tips"):getContentSize().width / 2)
    end

    local bg = ccui.Layout:create()
    bg:setContentSize(self.winSize.width, self.winSize.height)
    bg:setAnchorPoint(cc.p(0.5,0.5))
    bg:setPosition(cc.p(self.winSize.width/2, self.winSize.height/2))
    bg:setTouchEnabled(true)
    bg:setSwallowTouches(true)
    bg:addTouchEventListener(function()
        print("clicked")
    end)
    self.dlg:addChild(bg)

    -- run action
    self.dlg:setAnchorPoint({x = 0.5, y = 0.5})
    self.dlg:setScale(0.0)
    local sc = 1.0
    local action1 = cc.ScaleTo:create(0.1, sc)
    local action2 = cc.ScaleTo:create(0.05, 0.9 * sc)
    local action3 = cc.ScaleTo:create(0.02, sc)
    self.dlg:runAction(cc.Sequence:create(action1, action2, action3))
end

function sgjScene:getMainLayer()
    return self.m_sgjMainLayer
end


-- 随机概率
-- @params numerator( number ) 分子
-- @params denominator( number ) 分母 
function sgjScene:randomOK( numerator, denominator )
      math.randomseed(tostring(os.time()):reverse():sub(1, 7)) --设置时间种子
      local value =  math.random( 1,denominator )              --产生1到100之间的随机数
      local isOK = false
      if value <= numerator then
         isOK = true
      end
      return isOK
end 


-- 退出场景
function sgjScene:onExit()
    print("--------------sgjScene:onExit()-----------------------")
	
	if self.m_sgjMainLayer then
		self.m_sgjMainLayer:onExit()
		self.m_sgjMainLayer = nil
	end
	
    --removeMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT)
    removeMsgCallBack(self, MSG_ENTER_FOREGROUND)
    removeMsgCallBack(self, MSG_ENTER_BACKGROUND)
    --removeMsgCallBack(self, POPSCENE_ACK)
end

-- 响应返回按钮事件
function sgjScene:onBackButtonClicked()
   --ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo("app/game/plutus/res/Games/FarmFrenzy/caishendao/caishendao.ExportJson")
    cc.Director:getInstance():getScheduler():setTimeScale(1.0)
	if self.m_sgjMainLayer then
		self.m_sgjMainLayer:clear()
	end
    g_GameController:forceExit()
end

-- 从后台切换到前台
function sgjScene:onEnterForeground()
   print("从后台切换到前台")
  g_GameController:setEnterForeground( true )
  g_GameController:notifyViewDataAlready()
end

-- 从前台切换到后台
function sgjScene:onEnterBackground()
	print("从前台切换到后台,游戏线程挂起!")
	g_GameController.m_BackGroudFlag = true
end

function sgjScene:clearView()
   print("sgjScene:clearView()")
end
-- 清理数据
function sgjScene:clearData()
   
end

return sgjScene
