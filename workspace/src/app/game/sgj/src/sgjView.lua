local HNlayer = require("src.app.newHall.HNLayer")
local scheduler = require("framework.scheduler")

local GameSetLayer= require("src.app.newHall.childLayer.SetLayer")
local GameRecordLayer= require("src.app.newHall.childLayer.GameRecordLayer")
local ChatLayer = require("src.app.newHall.chat.ChatLayer")
local TABLEUI_PATH = "app/game/sgj/res/Games/FruitMachine/TableUi/NodeDesk.csb"

local GAME_FONT_1 = "app/game/sgj/res/Games/FruitMachine/TableUi/Res2/font/Num_bigwin-export.fnt"
local GAME_FONT_2 = "common/font/plusYuan.fnt"

local sgjView = class("sgjView",function()
    return HNlayer.new()
end)

local sgjGameImage = require("app.game.sgj.src.sgjGameImage")

function sgjView:ctor()
    self:myInit()
    self:setupView()
end

function sgjView:myInit()
    self._autoGame = false
    self._scheduleNum = 0
    self._scheduleResNum = 0
    self._iWin = 0
    self._freeGames = 0
    self._freeGames_N = 0
    self._score_N = 0
    self._MoveSize = 0.0
    self._IsBet = false
    self._IsRunning = false
    self._autoBegin = false
    self._bFreeAutoGame = false
    self._imageArr1 = {}
    self._imageArr2 = {}
    self._imageArr3 = {}
    self._imageArr4 = {}
    self._imageArr5 = {}
    self._clipperArr = {}

    self._fntPool = nil
    self._fntName = nil
    self._fntCoin = nil
    self._fntWinCoin = nil
    self._fntYafen = nil

    self._checkline = {}
    self._checkWin = {}
    self._checkWin2 = {}
    self._nodeLines = {}
    self._linesUI = {}
    self._nodeArma = {}
    self._betValue = {90, 450, 900, 4500, 9000, 45000, 90000}
    self._resultNum = 0
    self._score = 0
    self._gamepond = 0
    self._bet = 0
    self._lastBet = 0

    self._scheduleBlink2 = nil
    self._scheduleBlinkNum2 = 0

    self._schedulePush = nil
    self._schedulePushCount = 0.0

    self._scheduleWin = nil

    self._winLayer = nil
end

function sgjView:setupView()
	cc.Director:getInstance():getScheduler():setTimeScale(1.0)
	local winSize = cc.Director:getInstance():getWinSize()
    self._scalex =1/display.scaleX
	self._scaley =1/display.scaleY

	local cache = cc.SpriteFrameCache:getInstance()
    cache:addSpriteFrames("app/game/sgj/res/Games/FruitMachine/TableUi/Res2/sgj.plist")
    cache:addSpriteFrames("app/game/sgj/res/Games/FruitMachine/TableUi/Res2/sgj3.plist")
    cache:addSpriteFrames("app/game/sgj/res/Games/FruitMachine/TableUi/Res2/TigerPlist.plist")

		--预存储游戏配置
	local userDefault = cc.UserDefault:getInstance()
	if (not userDefault:getBoolForKey("Test_bool")) then
		userDefault:setBoolForKey("Test_bool", true)
		userDefault:setIntegerForKey("Bet_value", 90)
		userDefault:setIntegerForKey("Lines_value", 1)
	end
		--读取本地文件中保存的当前单线投注额和投注线数
	self._bet = 90
    self._lastBet = 90
	--_lines = userDefault:getIntegerForKey("Lines_value")
	self._lines = 1

	local tableUI = UIAdapter:createNode(TABLEUI_PATH)
	local tabel = cc.CSLoader:createTimeline(TABLEUI_PATH)
	tableUI:runAction(tabel)
	tabel:gotoFrameAndPlay(0, true)
	tableUI:setPosition(cc.p(winSize.width / 2, winSize.height / 2))
	self:addChild(tableUI, 2)

	self._tableBG = tableUI:getChildByName("Image_BG")
	local x = winSize.width / self._tableBG:getContentSize().width
	local y = winSize.height / self._tableBG:getContentSize().height
	self._tableBG:setScale(x, y)
    self._tableBG:addTouchEventListener(handler(self, self.imageEventCallBack))

    self._fntPool = self._tableBG:getChildByName("lbl_poolcoin")
    self._fntPool:setVisible(true)
    self._fntPool:setString(self._gamepond)

    self._fntName = self._tableBG:getChildByName("Image_name"):getChildByName("lbl_name")
    self._fntName:setVisible(true)
    self._fntName:setString(Player:getNickName())

    self._fntCoin = self._tableBG:getChildByName("Image_score"):getChildByName("lbl_coin")
    self._fntCoin:setVisible(true)
    self._fntCoin:setString(self._score)

    self._fntWinCoin = self._tableBG:getChildByName("Image_win"):getChildByName("lbl_win_coin")
    self._fntWinCoin:setVisible(true)
    self._fntYafen = self._tableBG:getChildByName("Image_yafen"):getChildByName("lbl_yafen")
    self._fntYafen:setVisible(true)

    self._tableBG:getChildByName("Image_payTable"):setScaleX(1/display.scaleX)

	self:getUpMenu()             --初始化屏幕上方ui
	self:getDownMenu()           --初始化屏幕下方ui
    self:getWinLayer()
	self:getFreeGamesUI()        --初始化免费游戏ui
    self:getLinesUI()
	self:createCliper()          --绘制裁剪区域
	self:createPlayImage()       --绘制要滚动的图片

	self:upDataMenuEnabled(true) --初始化让按钮不可点击
    self:updateGameMessage(true)

    self.mTextRecord = UIAdapter:CreateRecord(nil, 24)
    self._tableBG:addChild(self.mTextRecord)
	self.mTextRecord:setPosition(cc.p(950, 720))

    g_GameMusicUtil:playBGMusic("app/game/sgj/res/Games/FruitMachine/sound/BGM6.mp3",true)
end

function sgjView:clearData()
    local cache = cc.SpriteFrameCache:getInstance()
    cache:removeSpriteFramesFromFile("app/game/sgj/res/Games/FruitMachine/TableUi/Res2/sgj.plist")
    cache:removeSpriteFramesFromFile("app/game/sgj/res/Games/FruitMachine/TableUi/Res2/sgj3.plist")
    cache:removeSpriteFramesFromFile("app/game/sgj/res/Games/FruitMachine/TableUi/Res2/TigerPlist.plist")
    self:clear()
	self:close()
end

function sgjView:onExit()
	print("sgjView:onExit")
    self:clearData()
end

--获取上面游戏栏按钮
function sgjView:getUpMenu()
    local str = {}
    local winSize = cc.Director:getInstance():getWinSize()
    self._Scalex = 1280 / winSize.width
    self._Scaley = 720 / winSize.height

    self._downRecord = 0

    self.downBtn = self._tableBG:getChildByName("Button_down")
    self.downBtn:setScaleX(1/display.scaleX)
    self.returnBtn = self._tableBG:getChildByName("Button_return")
    self.returnBtn:setScaleX(1/display.scaleX)
    self.imageBtn = self._tableBG:getChildByName("Image_btn")
    self.imageBtn:setScaleX(1/display.scaleX)
     self.chatBtn = self._tableBG:getChildByName("btn_chat")
    self.chatBtn:setScaleX(1/display.scaleX)
    self.setBtn = self._tableBG:getChildByName("Image_btn"):getChildByName("Button_Set")
    self.paytableBtn = self._tableBG:getChildByName("Image_btn"):getChildByName("Button_payTable")
    self.zhanjiBtn = self._tableBG:getChildByName("Image_btn"):getChildByName("Button_zhanji")

    self.downBtn:addTouchEventListener(handler(self, self.upBoxEventCallBack))
    self.returnBtn:addTouchEventListener(handler(self, self.upBoxEventCallBack))
    self.setBtn:addTouchEventListener(handler(self, self.upBoxEventCallBack))
    self.paytableBtn:addTouchEventListener(handler(self, self.upBoxEventCallBack))
    self.zhanjiBtn:addTouchEventListener(handler(self, self.upBoxEventCallBack))
     self.chatBtn:addTouchEventListener(handler(self, self.upBoxEventCallBack))
    self.downBtn:setEnabled(true)
    self.downBtn:setVisible(true)
    self.returnBtn:setEnabled(true)
    self.returnBtn:setVisible(true)
    self.setBtn:setVisible(true)
    self.setBtn:setEnabled(true)
    self.paytableBtn:setVisible(true)
    self.paytableBtn:setEnabled(true)
    self.zhanjiBtn:setVisible(true)
    self.zhanjiBtn:setEnabled(true)
    self.imageBtn:setVisible(false)
end

function sgjView:setGoldCoin(nScore)
    self._score = nScore
end

function sgjView:setGamePond(nGamePond)
    self._gamepond = nGamePond
end

function sgjView:setFreeTimes(nFreeTimes)
    self._freeGames = nFreeTimes
    if self._freeGames>0 then
        self:gameReset(0.0)
    end
end

function sgjView:setBetScore(nBetScore)
    if nBetScore==0 then
        self._bet = 90
    else
        self._bet = nBetScore
    end
end

function sgjView:setWinGoldCoin(nWin)
    if(nWin > 0) then
        self._iWin = nWin
    else
        self._iWin = 0
    end
end

function sgjView:setRollImage(icons, freegame)
    self._icons = icons
    --self._iWin = wincoin
    self._IsRunning = true
    self._freeGames_N = freegame
    self._freeGames = freegame
    --unschedule(schedule_selector(GameTableUI::gameBetFailed))
    --_TypeScroll = *pTypeScroll
    if (self._freeGames == 0) then
        
        self:replaceScene(false)
        --[[self:runAction(cc.Sequence:create(cc.DelayTime:create(0.5), cc.CallFunc:create(function()
            self:resetStartMenuStatu(false, true)
        end), nil))]]--
    end

    g_GameMusicUtil:playSound("app/game/sgj/res/Games/FruitMachine/sound/TurnFastStart.mp3")

    self._imageRun = scheduler.scheduleGlobal(handler(self,self.imageRun), 0.1)  --启动图片滚动定时器
    self._imagePlay = scheduler.scheduleGlobal(handler(self,self.imagePlay), 0.01)  --启动图片滚动定时器
    self.delay1 = scheduler.performWithDelayGlobal(handler(self,self.showTheGameResult), 2.8)  --停止
end

--获取下面游戏栏按钮
function sgjView:getDownMenu()
    self.spinBtn = self._tableBG:getChildByName("btnStart")
    self.spinBtn:setScaleX(1/display.scaleX)
    self.stopBtn = self._tableBG:getChildByName("btnStop")
    self.stopBtn:setScaleX(1/display.scaleX)
    self.stopBtn:setVisible(false)
    self.guajiBtn = self._tableBG:getChildByName("btnGuaji")
    self.guajiBtn:setScaleX(1/display.scaleX)
    self.guajiBtn:setVisible(false)

    self.spinBtn:addTouchEventListener(handler(self,self.gameSpinButtonEventCallBack))
    self.guajiBtn:addTouchEventListener(handler(self,self.guajiButtonEventCallBack))

    self.btnBeilv = self._tableBG:getChildByName("btnBeilv")
    self.fntBeilv = self.btnBeilv:getChildByName("fnt_beilv")
    self.btnBeilv:addTouchEventListener(handler(self,self.downBoxEventCallBack))

    self.btnMax = self._tableBG:getChildByName("btnMax")
    self.btnMax:addTouchEventListener(handler(self,self.downBoxEventCallBack))
end

function sgjView:getWinLayer()
    self._winLayer = self._tableBG:getChildByName("winlayer")
    self._winLayer:setVisible(false)

    local Image_bg = self._winLayer:getChildByName("Image_bg")
    Image_bg:addTouchEventListener(handler(self, self.payTableEvenCallBack))
end

function sgjView:getFreeGamesUI()
    self._image_free = self._tableBG:getChildByName("Image_free")
    self._fnt_freetime = self._image_free:getChildByName("fntFree")
end

function sgjView:getLinesUI()
    local str = ""
    for i = 1,9 do
        str = string.format("Node_%d",i)
        self._nodeLines[i] = self._tableBG:getChildByName(str)
        str = string.format("imgXian%d", i)
        self._linesUI[i] = self._nodeLines[i]:getChildByName(str)
        self._linesUI[i]:setVisible(false)
        self._nodeArma[i] = self._nodeLines[i]:getChildByName("Node")
        --self._linesUI[i]:setLocalZOrder(4)
    end
end

--绘制裁剪区域
function sgjView:createCliper()
    local winSize = cc.Director:getInstance():getWinSize()

    for i = 1,5 do
        local shap = cc.DrawNode:create()
        local point = { cc.p(0, 0), cc.p(200, 0), cc.p(200, 520), cc.p(0, 520) }
        shap:drawPolygon1(point,{fillColor=cc.c4f(1, 1, 1, 1), borderWidth=2, borderColor=cc.c4f(1, 1, 1, 1)})
        local cliper = cc.ClippingNode:create()
        cliper:setStencil(shap)
        cliper:setAnchorPoint(cc.p(0.5, 0.5))
        cliper:ignoreAnchorPointForPosition(false)
        cliper:setPosition(cc.p(370 + 176*(i-1), self._tableBG:getContentSize().height * 0.2))

        self._tableBG:addChild(cliper)--把要滚动的图片加入到裁剪区域 
        self._clipperArr[i] = cliper
    end

    self._coinCountBig = ccui.TextBMFont:create("1.85",GAME_FONT_1)
    self._coinCountBig:setAnchorPoint(cc.p(0.5,0.5))
    self._coinCountBig:setPosition(cc.p(812,420))

    self._tableBG:addChild(self._coinCountBig,3)
    self._coinCountBig:setVisible(false)
end

function sgjView:createPlayImage()
    local cache = cc.SpriteFrameCache:getInstance()
	local winSize = cc.Director:getInstance():getWinSize()
		--往裁剪区域添加显示图片，默认添加四张交替滚动显示
	local k = 0
    math.randomseed(os.time())
	for j,child in pairs(self._clipperArr) do
	    k = k+1
		local name = ""
		for i = 0,3 do
			local num = math.random(1,11)

			name = string.format("statik_low%d.png", num)
			local playImage = sgjGameImage.new(name, num)           --创建要显示的图片 
			playImage:setPosition(cc.p(88, 430 - (i * 168)))

            self._imagescaleX = playImage:getScaleX()
            self._imagescaleY = playImage:getScaleY()

			child:addChild(playImage)
			if (k == 1) then
                self._imageArr1[i] = playImage--图片1数组
            end
			if (k == 2) then
                self._imageArr2[i] = playImage--图片2数组
            end
			if (k == 3) then
                self._imageArr3[i] = playImage--图片3数组
            end
			if (k == 4) then
                self._imageArr4[i] = playImage--图片4数组
            end
			if (k == 5) then
                self._imageArr5[i] = playImage--图片5数组
            end
		end
	end
end

function sgjView:upBoxEventCallBack(sender, eventType)
    local winSize = cc.Director:getInstance():getWinSize()
    if (eventType ~= ccui.TouchEventType.ended) then
        return
    end

    local btn = sender
    local name = btn:getName()

    if (name == "Button_return") then
        cc.Director:getInstance():getScheduler():setTimeScale(1.0)
		self:clear()
        local cache = cc.SpriteFrameCache:getInstance()
        cache:removeSpriteFramesFromFile("app/game/sgj/res/Games/FruitMachine/TableUi/Res2/sgj.plist")
        cache:removeSpriteFramesFromFile("app/game/sgj/res/Games/FruitMachine/TableUi/Res2/sgj3.plist")
        cache:removeSpriteFramesFromFile("app/game/sgj/res/Games/FruitMachine/TableUi/Res2/TigerPlist.plist")
        g_GameController:forceExit()
    end
    if (name == "backToGame") then

    end
    if (name == "Button_Set") then
        local layer = GameSetLayer.new()
		self:addChild(layer, 10000)
    end
     if (name == "btn_chat") then
       local ChatLayer = ChatLayer.new()
        self:addChild(ChatLayer, 10000);
    end
    if (name == "Button_down") then
        if self._downRecord==0 then
            self._downRecord = 1
            self.imageBtn:setVisible(true)
            self.downBtn:loadTextures("game/tiger/gui/gui-tiger-btn-menu-pop.png","game/tiger/gui/gui-tiger-btn-menu-pop.png","game/tiger/gui/gui-tiger-btn-menu-pop.png",1)
        elseif self._downRecord==1 then
            self._downRecord = 0
            self.imageBtn:setVisible(false)
            self.downBtn:loadTextures("game/tiger/gui/gui-tiger-btn-menu-push.png","game/tiger/gui/gui-tiger-btn-menu-push.png","game/tiger/gui/gui-tiger-btn-menu-push.png",1)
        end
    end
    if (name == "Button_payTable") then
        g_GameMusicUtil:playSound("app/game/sgj/res/Games/FruitMachine/sound/Start.mp3")
		local payTable = self._tableBG:getChildByName("Image_payTable")
		payTable:setVisible(true)
		payTable:setLocalZOrder(101)
		local backBtn = payTable:getChildByName("back_game")
		local imgBG = payTable:getChildByName("Image_bg")

		backBtn:addTouchEventListener(handler(self, self.payTableEvenCallBack))
		imgBG:addTouchEventListener(handler(self, self.payTableEvenCallBack))
        imgBG:setSwallowTouches(true)
	end
    if (name == "Button_zhanji") then
        local GameRecordLayer = GameRecordLayer.new(2)
        self:addChild(GameRecordLayer,10000)   
        GameRecordLayer:setScaleX(display.scaleX) 
        ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetGameResult_Req", {202})
    end
end

function sgjView:gameSpinButtonEventCallBack(sender, eventType)
	if (eventType == ccui.TouchEventType.began) then
        if (self._schedulePush) then
            scheduler.unscheduleGlobal(self._schedulePush)
            self._schedulePush = nil
            self._schedulePushCount = 0
            self._schedulePush = scheduler.scheduleGlobal(handler(self, self.setTouchCount),0.3)
        else
            self._schedulePushCount = 0
            self._schedulePush = scheduler.scheduleGlobal(handler(self, self.setTouchCount),0.3)
        end
	elseif (eventType == ccui.TouchEventType.ended) then
        if (self._schedulePush) then
            scheduler.unscheduleGlobal(self._schedulePush)
            self._schedulePush = nil
            self._schedulePushCount = 0
        end

		if (not self._autoGame) then
            if (self._autoGameTime) then
                scheduler.unscheduleGlobal(self._autoGameTime)
            end
			if (not self._IsRunning) then
				--g_GameMusicUtil:playSound("app/game/sgj/res/Games/FruitMachine/sound/spclk.wav")
				self:gameStart(0.1)
			else
				self:resetStartMenuStatu(false, false)
                if (self._showTheGameResult) then
                    scheduler.unscheduleGlobal(self._showTheGameResult)
                end
				self:compulsoryStop()
			end
		else
			if (self._IsRunning) then
				if (self._autoBegin) then
					self._autoBegin = false
					return
				end

                --self._checkBox_guaji:setSelected(false)
				self._autoGame = false
				self:resetStartMenuStatu(false, false)
				if (self._showTheGameResult) then
                    scheduler.unscheduleGlobal(self._showTheGameResult)
                end
				self:compulsoryStop()
            else
                --g_GameMusicUtil:playSound("app/game/sgj/res/Games/FruitMachine/sound/spclk.wav")
				self:gameStart(0.1)
			end
		end
    elseif (eventType == ccui.TouchEventType.canceled) then
        if (self._schedulePush) then
            scheduler.unscheduleGlobal(self._schedulePush)
            self._schedulePush = nil
            self._schedulePushCount = 0
        end
    end
end

function sgjView:setTouchCount()
    self._schedulePushCount = self._schedulePushCount + 0.3
    if self._schedulePushCount>1 then
        scheduler.unscheduleGlobal(self._schedulePush)
        self._schedulePush = nil
        self._schedulePushCount = 0
        
        self.spinBtn:setVisible(false)
        self.spinBtn:setEnabled(false)
        self.guajiBtn:setVisible(true)
        self.guajiBtn:setEnabled(true)

        local func = function (send,eventType)  end
        self.spinBtn:addTouchEventListener(func)

        self:gameStart(0.1)

        self._autoGame = true
        --self:gameReset(0.0)
    end
end

function sgjView:guajiButtonEventCallBack(sender, eventType)
    if (eventType==ccui.TouchEventType.began) then
    elseif (eventType==ccui.TouchEventType.ended) then
        self._autoGame = false
        self.guajiBtn:setVisible(false)
        self.guajiBtn:setEnabled(false)
        self.spinBtn:setVisible(true)
        self.spinBtn:setEnabled(true)
        self.spinBtn:addTouchEventListener(handler(self,self.gameSpinButtonEventCallBack))
    end
end

function sgjView:downBoxEventCallBack(pSender, eventType)
    if (eventType ~= ccui.TouchEventType.ended) then
        return
    end
    local btn = pSender
    local name = btn:getName()

    local betKey = 0
    local userDefault = cc.UserDefault:getInstance()
    local cnt = table.nums(self._betValue)
    for i = 1,cnt do
        if (self._betValue[i] == self._bet) then
            betKey = i
        end
    end

	if (name == "btnBeilv") then
        betKey = betKey + 1
        if (betKey == cnt+1) then
            betKey = 1
        end
        self._bet = self._betValue[betKey]
    elseif (name == "btnMax") then
        betKey = cnt
        self._bet = self._betValue[betKey]
	end

    self:updateGameMessage(true)
end

function sgjView:imageEventCallBack(pSender, eventType)
    if (eventType ~= ccui.TouchEventType.ended) then
        return
    end
    local btn = pSender
    local name = btn:getName()

    print("click")
	if (name == "Image_BG") then
        if self._downRecord==1 then
            self._downRecord = 0
            self.imageBtn:setVisible(false)
            self.downBtn:loadTextures("game/tiger/gui/gui-tiger-btn-menu-push.png","game/tiger/gui/gui-tiger-btn-menu-push.png","game/tiger/gui/gui-tiger-btn-menu-push.png",1)
        end
	end
end

--payTable按钮回调
function sgjView:payTableEvenCallBack(pSender, eventType)
    if (eventType ~= ccui.TouchEventType.ended) then
        return
    end
    local btn = pSender
    local name = btn:getName()

    local payTable = self._tableBG:getChildByName("Image_payTable")
    payTable:setLocalZOrder(101)
    local backBtn = payTable:getChildByName("back_game")
	local imgBG = payTable:getChildByName("Image_bg")

    if (name == "back_game") then
        payTable:setVisible(false)
    end
    if (name == "Image_bg") then
        print("none")
    end
end

--是否自动游戏计时
function sgjView:autoGameTime(dt)
    self._autoGame = true
    self._autoBegin = true
    self:gameStart(0.1)
end

--强行停止
function sgjView:compulsoryStop()
    if (self._imageRun) then
        scheduler.unscheduleGlobal(self._imageRun)
    end
    if (self._imagePlay) then
        scheduler.unscheduleGlobal(self._imagePlay)
    end
    if (self._showTheGameResult) then
        scheduler.unscheduleGlobal(self._showTheGameResult)
    end
    if self._scheduleResult then
        scheduler.unscheduleGlobal(self._scheduleResult)
    end
    if self._scheduleBlink2 then
        scheduler.unscheduleGlobal(self._scheduleBlink2)
    end
    if self._scheduleWin then
        scheduler.unscheduleGlobal(self._scheduleWin)
    end
    if self._schedulePush then
        scheduler.unscheduleGlobal(self._schedulePush)
    end
    g_GameMusicUtil:playSound("app/game/sgj/res/Games/FruitMachine/sound/Start.mp3")
    local name = {}

    for i = 1,5 do
        local imageArr = {}
        if (i == 1) then
            imageArr = self._imageArr1
        end
        if (i == 2) then
            imageArr = self._imageArr2
        end
        if (i == 3) then
            imageArr = self._imageArr3
        end
        if (i == 4) then
            imageArr = self._imageArr4
        end
        if (i == 5) then
            imageArr = self._imageArr5
        end
        for k,image in pairs(imageArr) do
            local index = 0
            if k==0 then
                index = 1
            elseif k==1 then
                index = i
            elseif k==2 then
                index = i+5
            elseif k==3 then
                index = i+10
            end
            local name = string.format("statik_low%d.png", self._icons[index])
            if (k == 0) then
                local num2 = math.random(1,11)
                name = string.format("statik_low%d.png", num2)
            end
            image:imageStopAndSetFrame(k,name)
            image:setRun(false)--图片可转动属性设置为false
            image:setNumber(self._icons[index])
        end
    end
    self.spinBtn:setTouchEnabled(true)
    self.btnBeilv:setTouchEnabled(false)
    self.btnMax:setTouchEnabled(false)

    self:settleAccounts()
end


--本局游戏开始
function sgjView:gameStart(dt)
    self._iWin = 0
    for i = 0, table.nums(self._imageArr1)-1 do
        self._imageArr1[i]:stopAllActions()
        self._imageArr2[i]:stopAllActions()
        self._imageArr3[i]:stopAllActions()
        self._imageArr4[i]:stopAllActions()
        self._imageArr5[i]:stopAllActions()

        self._imageArr1[i]:setOpacity(255)
        self._imageArr2[i]:setOpacity(255)
        self._imageArr3[i]:setOpacity(255)
        self._imageArr4[i]:setOpacity(255)
        self._imageArr5[i]:setOpacity(255)
    end
    self.spinBtn:setTouchEnabled(false)
    self.btnBeilv:setTouchEnabled(false)
    self.btnMax:setTouchEnabled(false)

    local cache = cc.SpriteFrameCache:getInstance()
    if (self._score < self._bet * self._lines) then
        self._autoGame = false
        self.spinBtn:setTouchEnabled(true)
        self.btnBeilv:setTouchEnabled(true)
        self.btnMax:setTouchEnabled(true)

        self:runAction(cc.Sequence:create(cc.DelayTime:create(2.0), cc.CallFunc:create(function()
            ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo("app/game/sgj/res/Games/FruitMachine/caishendao/caishendao.ExportJson")
            cc.Director:getInstance():getScheduler():setTimeScale(1.0)
            return
        end), nil))
    end
    
    --g_GameMusicUtil:stopBGMusic(true)
    if (self._freeGames == 0) then
        if (self._score < self._bet * self._lines) then
            g_GameController:gameBet(self._bet,self._lines)
        else
            self._score = self._score - self._bet * self._lines
            self:setGoldCoin(self._score)
            self:setWinImageOut()
            g_GameController:gameBet(self._bet,self._lines)
            self._lastBet = self._bet
            --g_GameMusicUtil:playBGMusic("app/game/sgj/res/Games/FruitMachine/SlotSound/GameSound/reels.wav", true)
        end
    else
        --self._freeGames = self._freeGames - 1
        self:setWinImageOut()
        g_GameController:gameBet(self._bet,self._lines)
        self._lastBet = self._bet
    end
end

--开启/关闭按钮点击
function sgjView:upDataMenuEnabled(enabled)
    local depositReduceBtn = self._tableBG:getChildByName("Button_Coins1")
    local depositAddBtn = self._tableBG:getChildByName("Button_Coins2")
    local payTableBtn = self._tableBG:getChildByName("Button_payTable")
end

function sgjView:resetStartMenuStatu(bStart, enabled)
    self.spinBtn:setTouchEnabled(enabled)
    self.spinBtn:setEnabled(enabled)
    self.btnBeilv:setTouchEnabled(enabled)
    self.btnBeilv:setEnabled(enabled)
    self.btnMax:setTouchEnabled(enabled)
    self.btnMax:setEnabled(enabled)
end

--/*--------------------------------图片滚动-------------------------------*/
--图片顺序启动
function sgjView:imageRun(dt)
    self._scheduleNum = self._scheduleNum + 1
    if (self._scheduleNum == 1) then
        for k,image in pairs(self._imageArr1) do
            image:setRun(true)
        end
    end
    if (self._scheduleNum == 2) then
        for k,image in pairs(self._imageArr2) do
            image:setRun(true)
        end
    end
    if (self._scheduleNum == 3) then
        for k,image in pairs(self._imageArr3) do
            image:setRun(true)
        end
    end
    if (self._scheduleNum == 4) then
        for k,image in pairs(self._imageArr4) do
            image:setRun(true)
        end
    end
    if (self._scheduleNum == 5) then
        for k,image in pairs(self._imageArr5) do
            image:setRun(true)
        end
        scheduler.unscheduleGlobal(self._imageRun)
        self._scheduleNum = 0
    end
end
--图片顺序启动
function sgjView:imagePlay(dt)
    for k,image in pairs(self._imageArr1) do
        if (image:getRun()) then
            image:imageRun(-30)
        end
    end
    for k,image in pairs(self._imageArr2) do
        if (image:getRun()) then
            image:imageRun(-30)
        end
    end
    for k,image in pairs(self._imageArr3) do
        if (image:getRun()) then
            image:imageRun(-30)
        end
    end
    for k,image in pairs(self._imageArr4) do
        if (image:getRun()) then
            image:imageRun(-30)
        end
    end
    for k,image in pairs(self._imageArr5) do
        if (image:getRun()) then
            image:imageRun(-30)
        end
    end
end

--更新信息
function sgjView:updateGameMessage(enabled)
    local str = string.format("%.2f", self._bet*0.01)
    self._fntYafen:setString(str)

    str = string.format("%.2f", (self._bet/9) *0.01)
    self.fntBeilv:setString(str)

    if (enabled) then
        local fWinNote = self._iWin*0.01
        str = string.format("%.2f",fWinNote)
        self._fntWinCoin:setString(str)

        local fMoney = self._score*0.01
        str = string.format("%.2f",fMoney)
        self._fntCoin:setString(str)

        local fGamePond = self._gamepond*0.01
        str = string.format("%.2f", fGamePond)
        self._fntPool:setString(str)
    end

    --[[if self._freeGames > 0 then
        self.spinBtn:loadTextures("btn_spin2.png","btn_spin2.png","btn_spin2.png",1)
        self._freeGame1:setVisible(true)
        self._freeGame2:setVisible(true)
        local _str = string.format("%d次",self._freeGames)
        self._freeGame2:setString(_str)
        self._textStart:setVisible(false)
    else
        self.spinBtn:loadTextures("btn_spin1.png","btn_spin1.png","btn_spin1.png",1)
        self._freeGame1:setVisible(false)
        self._freeGame2:setVisible(false)
        self._textStart:setVisible(true)
    end]]--
end

--自动停止定时器调用函数，用来启动图片延迟自动停止
function sgjView:showTheGameResult(dt)
    self._showTheGameResult = scheduler.scheduleGlobal(handler(self,self.autoShowTheGameResult), 0.1)
    self.spinBtn:setTouchEnabled(true)
    self.btnBeilv:setTouchEnabled(true)
    self.btnMax:setTouchEnabled(true)
end

--图片延迟一张张自动停止
function sgjView:autoShowTheGameResult(dt)
    g_GameMusicUtil:playSound("app/game/sgj/res/Games/FruitMachine/sound/Stop.mp3")
    local imageArr = {}
    local i = 0
    if (self._scheduleNum == 0) then
        imageArr = self._imageArr1
        i = 1
    end
    if (self._scheduleNum == 1) then
        imageArr = self._imageArr2
        i = 2
    end
    if (self._scheduleNum == 2) then
        imageArr = self._imageArr3
        i = 3
    end
    if (self._scheduleNum == 3) then
        imageArr = self._imageArr4
        i = 4
    end
    if (self._scheduleNum == 4) then
        imageArr = self._imageArr5
        i = 5
    end
    local name = ""
    for k,image in pairs(imageArr) do
        local index = 0
        if k==0 then
            index = i
        elseif k==1 then
            index = i
        elseif k==2 then
            index = i+5
        elseif k==3 then
            index = i+10
        end
        name = string.format("statik_low%d.png", self._icons[index])
        if (k == 0) then
            local num2 = math.random(1,11)
            name = string.format("statik_low%d.png", num2)
        end
        image:imageStopAndSetFrame(k,name)
        image:setRun(false)--图片可转动属性设置为false
        image:setNumber(self._icons[index])
    end
    self._scheduleNum = self._scheduleNum + 1
--如果没有可滚动的图片则停止所有定时器并结算
    if (self._scheduleNum == 5) then
        --self._score = self._score + self._iWin
        --self:setGoldCoin(self._score)
        self._scheduleNum = 0
        if (self._imageRun) then
            scheduler.unscheduleGlobal(self._imageRun)
        end
        if (self._imagePlay) then
            scheduler.unscheduleGlobal(self._imagePlay)
        end
        if (self._showTheGameResult) then
            scheduler.unscheduleGlobal(self._showTheGameResult)
        end
        if self._scheduleResult then
            scheduler.unscheduleGlobal(self._scheduleResult)
        end
        if self._scheduleBlink2 then
            scheduler.unscheduleGlobal(self._scheduleBlink2)
        end
        if self._scheduleWin then
            scheduler.unscheduleGlobal(self._scheduleWin)
        end
        if self._schedulePush then
            scheduler.unscheduleGlobal(self._schedulePush)
        end
        self:settleAccounts()
        self:upDataMenuEnabled(true)
        self:resetStartMenuStatu(false,false)
    end
end

--结算
function sgjView:settleAccounts()
    --g_GameMusicUtil:stopBGMusic(true)

    self:updateGameMessage(true)--更新参数显示

    local linesBG = self._tableBG:getChildByName("Panel_LinesBG")
--如果有中奖则展示中奖指示线条
    if (self._iWin > 0) then
        print("start calculate")
        g_GameController:ChecKLine(self._icons, self._lines, self._bet)
        g_GameMusicUtil:playSound("app/game/sgj/res/Games/FruitMachine/sound/Win.mp3")
    end

    self:gameReset(0.0)
end

function sgjView:gameReset(dt)
    self._IsRunning = false
    self._scheduleNum = 0

    if (self._freeGames == 0) then
        if (self._bFreeAutoGame) then--免费游戏的最后一局，重置参数
            self._bFreeAutoGame = false
            local str = string.format("%d", self._freeGames_N)
            --freeGameText:setString(str)
--sprintf(str, "%lld", _money - _money_N)
            local fMoney = (self._score - self._score_N)*0.01
            str = string.format("%.2f",fMoney)
            self:gameReset(0.0) 
            g_GameMusicUtil:playSound("app/game/sgj/res/Games/FruitMachine/sound/freebg.wav")
            self:replaceScene(false)
            return
        end
        if (not self._autoGame) and (not self._bFreeAutoGame) then
            self:runAction(cc.Sequence:create(cc.DelayTime:create(0.5), cc.CallFunc:create(function()
                self:upDataMenuEnabled(true)--如果没有出现免费游戏奖励和小游戏奖励则恢复按钮点击功能
                self:resetStartMenuStatu(true, true)
            end), nil))
            return
        end
    end
    if (self._freeGames > 0) then--免费游戏进行中
        g_GameMusicUtil:playSound("app/game/sgj/res/Games/FruitMachine/sound/ScatterWin.mp3")
        self:replaceScene(true)
        self:resetStartMenuStatu(false, false)
        if (not self._bFreeAutoGame) then--免费游戏第一局
            self._bFreeAutoGame = true
            self._score_N = self._score
            self._freeGames_N = self._freeGames
            --g_GameMusicUtil:playSound("app/game/sgj/res/Games/FruitMachine/SlotSound/GameSound/scatte.wav")
        end
        --local label = freeGame:getChildByName("freeGameText")
        --local str2 = string.format("%d", self._freeGames)
        --label:setString(str2)
--自动游戏时如果有奖励则等待2秒才重新开始，没奖励等待1秒重新开始
        local temp = 0
        if (self._isJackpot==1) or ((self._iWin/self._lastBet)>2.5) then
            temp=3
        end
        if (self._iWin > 0) or (self._freeGames_N > 0) then
            local num = self._resultNum
            self.delay2 = scheduler.performWithDelayGlobal(handler(self,self.gameStart), num + 1 + temp)
        else 
            self.delay2 = scheduler.performWithDelayGlobal(handler(self,self.gameStart), 1.5 + temp)
        end
        return
    end

    if (self._autoGame) then
--自动游戏时如果有奖励则等待2秒才重新开始，没奖励等待1秒重新开始
        if (self._iWin > 0) or (self._freeGames_N > 0) then
            local num = self._resultNum
            self.delay2 = scheduler.performWithDelayGlobal(handler(self,self.gameStart), num+4)
            self:runAction(cc.Sequence:create(cc.DelayTime:create(1.5), cc.CallFunc:create(function()
                self:resetStartMenuStatu(false, true)
            end), nil))
        else
            self.delay2 = scheduler.performWithDelayGlobal(handler(self,self.gameStart), 4.5)
            self:runAction(cc.Sequence:create(cc.DelayTime:create(1.5), cc.CallFunc:create(function()
                self:resetStartMenuStatu(false, true)
            end), nil))
        end
    end
end

--进入小游戏
function sgjView:enterSmallGame(dt)
    self._smallgame = sgjSmallGameUI:createSmallGame(self._TypeScroll.iSmallImgid)
    self._smallgame:setPosition(cc.p(self._tableBG:getContentSize().width / 2, self._tableBG:getContentSize().height / 2))
    self._tableBG:addChild(self._smallgame, 4)
end

--游戏开始通知
function sgjView:dealGameStart()
    self:upDataMenuEnabled(true)--收到游戏开始消息，打开按钮点击
end

function sgjView:replaceScene(isFree)
    if (isFree) then
        self._image_free:setVisible(true)
        local str = string.format("%d",self._freeGames)
        self._fnt_freetime:setString(str)
        self._fnt_freetime:setVisible(true)
    else
        self._image_free:setVisible(false)
        self._fnt_freetime:setVisible(false)
    end
end


function sgjView:replaceLight2()
    local light1 = self._winLayer:getChildByName("kuang"):getChildByName("kuang1")
    local light2 = self._winLayer:getChildByName("kuang"):getChildByName("kuang2")
    if self._scheduleBlinkNum2==0 then
        self._scheduleBlinkNum2=1
        light1:setVisible(false)
        light2:setVisible(true)
    elseif self._scheduleBlinkNum2==1 then
        self._scheduleBlinkNum2=0
        light1:setVisible(true)
        light2:setVisible(false)
    end
end

function sgjView:setPhotoBright()
    for k = 0,3 do
        self._imageArr1[k]:setBright()
        self._imageArr2[k]:setBright()
        self._imageArr3[k]:setBright()
        self._imageArr4[k]:setBright()
        self._imageArr5[k]:setBright()
    end
end

function sgjView:setShowResult()
    for i = 0,3 do
        self._imageArr1[i]:setGray()
        self._imageArr2[i]:setGray()
        self._imageArr3[i]:setGray()
        self._imageArr4[i]:setGray()
        self._imageArr5[i]:setGray()
    end

    --self:showWinLayer(isJackPot, jackaward, wincoin)

    if self._freeGames > 0 then
        self._scheduleResult = scheduler.performWithDelayGlobal(handler(self,self.setWinAction), 1.0)
    else
        self._scheduleResult = scheduler.scheduleGlobal(handler(self,self.setWinAction), 1.0)
    end

    --[[if self._scheduleResNum==0 then
        for i=1,#self._checkWin2 do
            self._linesUI[self._checkWin2[i] ]:setVisible(true)
        end
        for i=1,#self._checkline do
            for j=1,#self._checkline[i] do
                self:setWinImageColor(self._checkline[i][j], j, true, self._iWin)
            end
        end
    else]]--
        self._linesUI[self._checkWin2[self._scheduleResNum]]:setVisible(true)
        self._nodeArma[self._checkWin2[self._scheduleResNum]]:setVisible(true)
        for k=1,#self._checkline[self._scheduleResNum] do
            self:setWinImageColor(self._checkline[self._scheduleResNum][k], k, true, self._checkWin[self._scheduleResNum])
        end
    --end

    self._scheduleResNum = self._scheduleResNum + 1
    if self._scheduleResNum>self._resultNum then
        self._scheduleResNum = 1
    end
end

function sgjView:showWinLayer(checkline, checkwin, checkwin2, isJackpot, jackaward, wincoin)
    self._checkline = checkline
    self._checkWin = checkwin
    self._checkWin2 = checkwin2
    self._resultNum = table.nums(checkline)
    self._scheduleResNum = 1
    self._isJackpot = isJackpot

    self._winLayer:setVisible(true)
    self._winLayer:setLocalZOrder(8)
    local kuang1 = self._winLayer:getChildByName("kuang"):getChildByName("kuang1")
    local kuang2 = self._winLayer:getChildByName("kuang"):getChildByName("kuang2")
    self._scheduleBlinkNum2=0
    self._scheduleBlink2 = scheduler.scheduleGlobal(handler(self, self.replaceLight2), 0.15)

    self._scheduleWin = scheduler.performWithDelayGlobal(function()
        self._winLayer:setVisible(false)
        scheduler.unscheduleGlobal(self._scheduleBlink2)
        self:setShowResult()
    end, 3)

    self._winLayerfnt = self._winLayer:getChildByName("fontCoin")
    local panel1 = self._winLayer:getChildByName("panel1")
    local panel2 = self._winLayer:getChildByName("panel2")
    local panel3 = self._winLayer:getChildByName("panel3")
    local panel4 = self._winLayer:getChildByName("panel4")
    local panel5 = self._winLayer:getChildByName("panel5")

    if isJackpot==1 then
        local str = string.format("%.2f", jackaward*0.01)
        self._winLayerfnt:setString(str)
        panel1:setVisible(false)
        panel2:setVisible(false)
        panel3:setVisible(false)
        panel4:setVisible(false)
        panel5:setVisible(true)
        g_GameMusicUtil:playSound("app/game/sgj/res/Games/FruitMachine/sound/Jackpot.mp3")
    else
        local win = wincoin/self._lastBet
        if win < 2.5 then
            scheduler.unscheduleGlobal(self._scheduleBlink2)
            scheduler.unscheduleGlobal(self._scheduleWin)
            self._winLayer:setVisible(false)
            self:setShowResult()
            g_GameMusicUtil:playSound("app/game/sgj/res/Games/FruitMachine/sound/Win.mp3")
        elseif win < 4.5 then
            local str = string.format("%.2f", wincoin*0.01)
            self._winLayerfnt:setString(str)
            panel1:setVisible(true)
            panel2:setVisible(false)
            panel3:setVisible(false)
            panel4:setVisible(false)
            panel5:setVisible(false)
            g_GameMusicUtil:playSound("app/game/sgj/res/Games/FruitMachine/sound/Amazing.mp3")
            g_GameMusicUtil:playSound("app/game/sgj/res/Games/FruitMachine/sound/Amazing_SE.mp3")
        elseif win < 7 then
            local str = string.format("%.2f", wincoin*0.01)
            self._winLayerfnt:setString(str)
            panel1:setVisible(false)
            panel2:setVisible(true)
            panel3:setVisible(false)
            panel4:setVisible(false)
            panel5:setVisible(false)
            g_GameMusicUtil:playSound("app/game/sgj/res/Games/FruitMachine/sound/BigWin.mp3")
            g_GameMusicUtil:playSound("app/game/sgj/res/Games/FruitMachine/sound/BigWin_SE.mp3")
        else
            local str = string.format("%.2f", wincoin*0.01)
            self._winLayerfnt:setString(str)
            panel1:setVisible(false)
            panel2:setVisible(false)
            panel3:setVisible(true)
            panel4:setVisible(false)
            panel5:setVisible(false)
            g_GameMusicUtil:playSound("app/game/sgj/res/Games/FruitMachine/sound/SuperWin.mp3")
            g_GameMusicUtil:playSound("app/game/sgj/res/Games/FruitMachine/sound/SuperWin_SE.mp3")
        end
    end

    local particle1 = self._winLayer:getChildByName("Particle_1")
    particle1:setVisible(true)
    particle1:resetSystem()
    local particle1 = self._winLayer:getChildByName("Particle_1")
    particle1:setVisible(true)
    particle1:resetSystem()
    local particle2 = self._winLayer:getChildByName("Particle_2")
    particle2:setVisible(true)
    particle2:resetSystem()
    local particle3 = self._winLayer:getChildByName("Particle_3")
    particle3:setVisible(true)
    particle3:resetSystem()
    local particle4 = self._winLayer:getChildByName("Particle_4")
    particle4:setVisible(true)
    particle4:resetSystem()
    local particle5 = self._winLayer:getChildByName("Particle_5")
    particle5:setVisible(true)
    particle5:resetSystem()
    local particle6 = self._winLayer:getChildByName("Particle_6")
    particle6:setVisible(true)
    particle6:resetSystem()
end

function sgjView:setWinAction()
    if #self._checkline == 0 then
        return
    end

    g_GameMusicUtil:playSound("app/game/sgj/res/Games/FruitMachine/sound/LineWin.mp3")

    for i = 0,3 do
        self._imageArr1[i]:setGray()
        self._imageArr2[i]:setGray()
        self._imageArr3[i]:setGray()
        self._imageArr4[i]:setGray()
        self._imageArr5[i]:setGray()
    end

    for i=1,9 do
        self._linesUI[i]:setVisible(false)
        self._nodeArma[i]:setVisible(false)
    end

    if self._scheduleResNum>self._resultNum then
        --[[for i=1,#self._checkWin2 do
            self._linesUI[self._checkWin2[i] ]:setVisible(true)
        end
        for i=1,#self._checkline do
            for j=1,#(self._checkline[i]) do
                self:setWinImageColor(self._checkline[i][j], j, true, self._iWin)
            end
        end]]--
        self._scheduleResNum = 1
    else
        --[[if self._scheduleResNum==0 then
            for i=1,#self._checkWin2 do
                self._linesUI[self._checkWin2[i] ]:setVisible(true)
            end
            for i=1,#self._checkline do
                for j=1,#self._checkline[i] do
                    self:setWinImageColor(self._checkline[i][j], j, true, self._iWin)
                end
            end
        else]]--
            self._linesUI[self._checkWin2[self._scheduleResNum]]:setVisible(true)
            self._nodeArma[self._checkWin2[self._scheduleResNum]]:setVisible(true)
            for k=1,#self._checkline[self._scheduleResNum] do
                self:setWinImageColor(self._checkline[self._scheduleResNum][k], k, true, self._checkWin[self._scheduleResNum])
            end
        --end
    end
    self._scheduleResNum = self._scheduleResNum + 1
    if self._scheduleResNum>self._resultNum then
        self._scheduleResNum = 1
    end
end

function sgjView:setWinImageColor(row,col,flag,reward)
    local scaleIn = cc.ScaleTo:create(0.2, 1.1, 1.1)
    local scaleOut = cc.ScaleTo:create(0.2, 1.0, 1.0)
    local index = (row-1)*5+col

    if (col == 1) then
        self._imageArr1[row]:setPlay(cc.Sequence:create(scaleIn, scaleOut))
        self._imageArr1[row]:setBright()
    end
    if (col == 2) then
        self._imageArr2[row]:setPlay(cc.Sequence:create(scaleIn, scaleOut))
        self._imageArr2[row]:setBright()
    end
    if (col == 3) then
        self._imageArr3[row]:setPlay(cc.Sequence:create(scaleIn, scaleOut))
        self._imageArr3[row]:setBright()
    end
    if (col == 4) then
        self._imageArr4[row]:setPlay(cc.Sequence:create(scaleIn, scaleOut))
        self._imageArr4[row]:setBright()
    end
    if (col == 5) then
        self._imageArr5[row]:setPlay(cc.Sequence:create(scaleIn, scaleOut))
        self._imageArr5[row]:setBright()
    end

    self._coinCountBig:setVisible(true)
    local fWinNote = reward*0.01
    local str = string.format("%.2f",fWinNote)
    self._coinCountBig:setString(str)
end

function sgjView:setWinImageOut()
    self._checkline = {}
    self._checkWin = {}
    self._checkWin2 = {}
    self._resultNum = 0
    self._scheduleResNum = 0
    if self._scheduleResult then
        scheduler.unscheduleGlobal(self._scheduleResult)
    end
    for i = 1,9 do
        self._linesUI[i]:setVisible(false)
        self._nodeArma[i]:setVisible(false)
    end
    for k = 0,3 do
        self._imageArr1[k]:setTitleOut()
        self._imageArr1[k]:stopAllActions()
        self._imageArr1[k]:setScaleX(self._imagescaleX)
        self._imageArr1[k]:setScaleY(self._imagescaleY)
        self._imageArr2[k]:setTitleOut()
        self._imageArr2[k]:stopAllActions()
        self._imageArr2[k]:setScaleX(self._imagescaleX)
        self._imageArr2[k]:setScaleY(self._imagescaleY)
        self._imageArr3[k]:setTitleOut()
        self._imageArr3[k]:stopAllActions()
        self._imageArr3[k]:setScaleX(self._imagescaleX)
        self._imageArr3[k]:setScaleY(self._imagescaleY)
        self._imageArr4[k]:setTitleOut()
        self._imageArr4[k]:stopAllActions()
        self._imageArr4[k]:setScaleX(self._imagescaleX)
        self._imageArr4[k]:setScaleY(self._imagescaleY)
        self._imageArr5[k]:setTitleOut()
        self._imageArr5[k]:stopAllActions()
        self._imageArr5[k]:setScaleX(self._imagescaleX)
        self._imageArr5[k]:setScaleY(self._imagescaleY)
    end
    self._coinCountBig:setVisible(false)
end

function sgjView:clear()
	if (self._imageRun) then
        scheduler.unscheduleGlobal(self._imageRun)
		self._imageRun = nil
    end
    if (self._imagePlay) then
        scheduler.unscheduleGlobal(self._imagePlay)
		self._imagePlay = nil
    end
    if (self._showTheGameResult) then
        scheduler.unscheduleGlobal(self._showTheGameResult)
		self._showTheGameResult = nil
    end
    if (self.delay1) then
        scheduler.unscheduleGlobal(self.delay1)
		self.delay1 = nil
    end
    if (self.delay2) then
        scheduler.unscheduleGlobal(self.delay2)
		self.delay2 = nil
    end
    if self._scheduleResult then
        scheduler.unscheduleGlobal(self._scheduleResult)
		self._scheduleResult = nil
    end
    if self._scheduleBlink2 then
        scheduler.unscheduleGlobal(self._scheduleBlink2)
        self._scheduleBlink2 = nil
    end
end

function sgjView:setRrecordId(id)
    if id and id ~= "" then
        self.mTextRecord:setString("牌局ID:"..id)
    end
end


return sgjView