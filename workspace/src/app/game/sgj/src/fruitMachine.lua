module(..., package.seeall)


CS_M2C_FruitMachine_Start_Nty =
{
	{ 1,	1, 'm_score'			 ,  'UINT'						, 1		, '玩家分数'},
	{ 2,	1, 'm_freeTimes'	 	 ,  'INT'						, 1		, '剩余免费游戏次数'},
	{ 3,	1, 'm_betScore'	 	 	 ,  'UINT'						, 1		, '触发免费次数时下注金额'},
	{ 4,	1, 'm_jackPot'	 	 	 ,  'UINT'						, 1		, '彩金池'},
	{ 5,	1, 'm_recordId'       	 ,  'STRING'       				, 1     , '牌局编号' },
}

CS_C2M_FruitMachine_StartRoll_Req =
{
	{ 1,	1, 'm_deposit'			 ,  'INT'						, 1		, '单线分数'},
}

CS_M2C_FruitMachine_ScrollResult_Ack =
{
	{ 1,	1, 'm_winCoin'			 ,  'INT'						, 1		, '单线分数'},
	{ 2,	1, 'm_icons'		 	 ,  'UBYTE'						, 15	, '小图标， 分别从左到右，从上到下'},
	{ 3,	1, 'm_freeTimes'	 	 ,  'INT'						, 1		, '剩余免费游戏次数'},
	{ 4,	1, 'm_calcFreeWinCoin'	 ,  'UINT'						, 1		, '免费次数下总共赢了多少金币'},
	{ 5,	1, 'm_curCoin'	 		 ,  'UINT'						, 1		, '玩家当前金币, 扣除免费赢得金币'},
	{ 6,	1, 'm_isJackPot'	 	,  'UINT'						, 1		, '是否触发jackpot, 1-是 0-否'},
	{ 7,	1, 'm_award'	 		 ,  'UINT'						, 1		, 'jackpot 中奖金额, 存在中奖情况下奖金池为0情况'},
	{ 8,	1, 'm_ret'	 		 	 ,  'INT'						, 1		, '错误码, 0表示成功'},
	{ 9,	1, 'm_recordId'       	 ,  'STRING'       				, 1     , '牌局编号' },
}

CS_M2C_FruitMachine_JackPotChange_Nty =
{
	{ 1,	1, 'm_jackPot'	 	 	 ,  'UINT'						, 1		, '彩金池金额'},
}

CS_C2M_FruitMachine_Exit_Req =
{
	
}

CS_M2C_FruitMachine_Exit_Ack =
{
	{ 1,	1, 'm_ret'			 	 ,  'INT'						, 1		, '标记'},
}

CS_M2C_FruitMachine_Kick_Nty =
{
	{ 1,	1, 'm_type'			 	 ,  'UINT'						, 1		, '1-房间维护,请稍后再游戏 2-你已经被系统踢出房间,请稍后重试'},
}
