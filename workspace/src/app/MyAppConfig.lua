--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 全局配置, 包括网络连接地址

GlobalConf = {}

GlobalConf.ServerType = {
    NC = 1, -- 内测
    WC = 2, -- 外测
    WW = 3, -- 外网
}

GlobalConf.APPVERSION = 8

GlobalConf.CurServerType = GlobalConf.ServerType.WW
--GlobalConf.CurServerType = GlobalConf.ServerType.WW 
GlobalConf.TOTAL_UPDATE = true -- 开启总下载
GlobalConf.UPDATELOBBY = true -- 开启更新大厅
GlobalConf.UPDATE_GAME = true -- 开启更新游戏
GlobalConf.UPDATE_APK = true -- 开启更新apk
GlobalConf.UPDATE_IPA = true -- 开启更新ipa 

if not GlobalConf.TOTAL_UPDATE then
    GlobalConf.UPDATELOBBY = false -- 开启更新大厅
    GlobalConf.UPDATE_GAME = false -- 开启更新游戏
    GlobalConf.UPDATE_APK = false -- 开启更新apk
    GlobalConf.UPDATE_IPA = false -- 开启更新ipa
end
-- GlobalConf.DEBUG_MODE = true -- 开启本地模式, 为方便调试时用的开关, 发布版本必须设成false或者直接隐藏
GlobalConf.SHOW_DEBUG_SERVER = false  --显示调试服务器

require("app.hall.config.code.HallConfig")