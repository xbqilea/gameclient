--CommonNetImageView.lua
--Creater By  2017-05-12
--网络图片/下载
local lfs = require("lfs") 
--local MD5 = require ("MD5")
local CommonNetImageView = class("CommonNetImageView",function() return ccui.ImageView:create()end)
function urlDecodeMD5( url )
	-- body
	return (string.gsub(MD5.sum(url), ".", function (c)
       return string.format("%02x", string.byte(c))
 	end))
end
function CommonNetImageView:ctor( url,node,customSize,maskSp)
	print(url,"URL")
	self.parentNode = node
	self.customSize = customSize
	self.maskSprite = maskSp
	if not url or #url == 0 then
		return
	end
	if node then
		if node:getChildByName('CommonNetImageView') then
			node:removeChildByName('CommonNetImageView')
		end
	end
	if customSize then
		self:ignoreContentAdaptWithSize(false)
		self:setContentSize(customSize)
		if node then
			local size = node:getContentSize()
			self:setPosition(cc.p(size.width/2,size.height/2))
		end
	end
	self:setName('CommonNetImageView')
	if self:checkLocalName(url) then
		--本地图片
		self:showSprite(url)
		print("本地图片检测")
		return
	end

	self.downUrl = url
	self.fileName = urlDecodeMD5(url)
	
	local old_Name =  cc.UserDefault:getInstance():getStringForKey(self.fileName,"empty")
	if old_Name == "empty" then
		local only = self:checkOnlyListener(self.fileName)
		self:addMeListener(self.fileName)
		if only then
			self:downSprite()
		end
	else
		local  path = cc.FileUtils:getInstance():getWritablePath().."ns/"..old_Name
		if not cc.FileUtils:getInstance():isFileExist(path) then
			local only = self:checkOnlyListener(self.fileName)
			self:addMeListener(self.fileName)
			if only then
				self:downSprite()
			end
		else
			self:showSprite(path, true)
		end
	end
	self:enableNodeEvents()
end

function CommonNetImageView:checkOnlyListener( name)
	if cc.Director:getInstance():getEventDispatcher():hasEventListener(name) then
		return false
	end
	return true
end

--添加逻辑
function CommonNetImageView:addMeListener( name )
	print("注册消息", name)
	addNodeListener(self, name, handler(self, self.showLoadTexture))
	addNodeListener(self, "imageDownSuccess", handler(self, self.downSuccess))
	enableListener(self)
end

function CommonNetImageView:showLoadTexture( event )
	print("消息下载显示")
	local path = event._userdata
	if path then
		self:showSprite(path)
	end
end

--检查是否为本地图片
function CommonNetImageView:checkLocalName( downUrl )
	if string.sub(downUrl,1,6) == "https:" then
		return false
	end	
	return true
end

function CommonNetImageView:getSpriteExtension()
	local extension_name = string.sub(self.downUrl,-4)
	if extension_name ~= ".png" and extension_name ~= ".jpg" then
		return ""
	end
	return extension_name
end

function CommonNetImageView:downSprite()
	print(self.downUrl,"UIR---------------")
	local xhr = cc.XMLHttpRequest:new()
	-- json数据类型
	xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING 
	xhr:open("GET", self.downUrl)
	local  path = cc.FileUtils:getInstance():getWritablePath().."ns/"
	if not io.exists(path) then
		--目录不存在，创建此目录
		lfs.mkdir(path)
	end
	xhr.path = string.format("%s%s%s",path,self.fileName,self:getSpriteExtension())
	local function onReadyStateChange( ... )
		if xhr.readyState == 4 and (xhr.status >= 200 and xhr.status < 207) then 
			-- 要考虑如果self已经销毁了的情况
			local data = xhr.responseText
			local file = io.open(xhr.path,"wb")
			file:write(data)
			file:close()
			dispatchEvent("imageDownSuccess")
		else
			print("网络连接失败或者文件不存在")
			if self.downFail then
				self:downFail()
			end
		end
		xhr:unregisterScriptHandler()
	end
	xhr:registerScriptHandler(onReadyStateChange)
	--发送请求  
	xhr:send()
end

--下载失败
function CommonNetImageView:downFail()
	print("下载失败")
end

--下载成功
function CommonNetImageView:downSuccess()
	print("下载成功")
	local realName = self.fileName..self:getSpriteExtension()
	local old_Name =  cc.UserDefault:getInstance():getStringForKey(self.fileName,"empty")
	if old_Name == "empty" then
		cc.UserDefault:getInstance():setStringForKey(self.fileName, realName)
		cc.UserDefault:getInstance():flush()
	end
	local  path = cc.FileUtils:getInstance():getWritablePath().."ns/"
	local pathReal = path..realName
	print(self.fileName,"发消息")
	dispatchEvent(self.fileName,pathReal) 
end

--显示在父节点上
function CommonNetImageView:showSprite(url, isNetShow)
	self:loadTexture(url)

	
	if not self.parentNode then
		return
	end

	if self.downOverCell and not isNetShow  then
		--这个是需要回调时的回调
		ccui.Helper:doLayout(self.parentNode)
		if self.downOverCell then
			self:downOverCell(self)
		end
	end

	if self.parentNode:getReferenceCount() == 0 then
		print("节点记数")
		return
	end
	if self.maskSprite then
		local CommonHelper = require('app.fw.common.CommHelper')
		local icon = CommonHelper:createMaskWithSprite(self.maskSprite, self:getVirtualRenderer():getSprite())
		local szpt = self.parentNode:getContentSize()
		icon:setPosition(szpt.width*0.5, szpt.height*0.5)
		self.parentNode:add(icon)
		self:hide()
	else
		-- 把sprite显示在子节点最下层
		local children = self.parentNode:getChildren()
		for _, child in pairs(children) do
			if child:getLocalZOrder() < 1 then
				child:setLocalZOrder(1)
			end
		end
		self:setLocalZOrder(0)
	end
end

return CommonNetImageView