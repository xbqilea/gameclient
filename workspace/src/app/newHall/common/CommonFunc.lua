--CommonFunc.lua
--通用方法
--Create by . 2017-05-04
-- require("app.fw.common.BaiduGameFunc")
local MD5 = require "MD5"
local luaoc
if device.platform == "ios" or device.platform == "mac" then
    luaoc = require "cocos.cocos2d.luaoc"
end

local numberConversion = require "app.utility.protobuf.numberConversion"


--发送数据到游戏服
function sendMsgToGameSVR(data)
    pcall(function ( ... )
        numberConversion:messageSend(data)
        NetController:sendDataByName("gamesvr", KKConstant.MSGTYPE.REQUEST, data._classname, data)
    end)
end
--发送数据到登陆服
function sendMsgToLoginSVR(data)
    pcall(function ( ... )
        NetController:sendDataByName("loginsvr", KKConstant.MSGTYPE.REQUEST, data._classname, data)
    end)
end

--java或oc调用的全局函数
function onJavaObjcMsg(str)
    print('@@ onJavaObjcMsg', str)
    local curScene = cc.Director:getInstance():getRunningScene()
    if curScene then
        curScene:runAction(cc.CallFunc:create(function ( ... )
            local tab = require('app.fw.common.CommHelper').stringSplit(str, '#')
            if #tab > 0 then
                print('dispatchEvent', tab[1])
                dispatchEvent(tab[1], tab)
            end
        end))
    end
end

function onLoginOut( ... )
    -- body
     local channel = getChannelID()
    --  if channel == 501 then
    --     local luaj = require "cocos.cocos2d.luaj"
    --     local className="org/cocos2dx/lua/yybao/ThirdSDKYYBAO"
    --     local args = {}
    --     local sigs = "()V"
    --     local ok = luaj.callStaticMethod(className, "logout", args, sigs)
    -- end
    require('app.fw.common.InstanceMgr').logout()
end

-------------------------------------------------------------------
--crasheye
Crasheye = {
    --设置版本号
    setAppVersion = function (version)
        if device.platform == "android" then
            local luaj = require "cocos.cocos2d.luaj"
            local className="com/xsj/crasheye/Crasheye"
            local args = {tostring(version)}
            local sigs = "(Ljava/lang/String;)V"
            local ok, ret = luaj.callStaticMethod(className,"setAppVersion",args,sigs)  
            if not ok then
                print("crasheye set setAppVersion error", version)
            end
        elseif device.platform == "ios" or device.platform == "mac" then
            local ok, ret = luaoc.callStaticMethod("Crasheye", "setAppVersion", {appVersion=tostring(version)})
            if not ok then
                print("crasheye set setAppVersion error", version)
            end
        end
    end,
    --设置渠道号
    setChannelID = function (channelID)
        if device.platform == "android" then
            local luaj = require "cocos.cocos2d.luaj"
            local className="com/xsj/crasheye/Crasheye"
            local args = {tostring(channelID)}
            local sigs = "(Ljava/lang/String;)V"
            local ok, ret = luaj.callStaticMethod(className,"setChannelID",args,sigs)  
            if not ok then
                print("crasheye set setChannelID error", channelID)
            end
        end
    end,
    --设置玩家角色id
    setUserIdentifier = function (rid)
        if device.platform == "android" then
            local luaj = require "cocos.cocos2d.luaj"
            local className="com/xsj/crasheye/Crasheye"
            local args = {tostring(rid)}
            local sigs = "(Ljava/lang/String;)V"
            local ok, ret = luaj.callStaticMethod(className,"setUserIdentifier",args,sigs)  
            if not ok then
                print("crasheye set setUserIdentifier error", rid)
            end
        elseif device.platform == "ios" or device.platform == "mac" then
            local ok, ret = luaoc.callStaticMethod("Crasheye", "setUserID", {userID=tostring(rid)})
            if not ok then
                print("crasheye set setUserIdentifier error", rid)
            end
        end
    end,
    --上报脚本错误
    sendScriptException = function (title, msg, language, file)
        if device.platform == "android" then
            local luaj = require "cocos.cocos2d.luaj"
            local className="com/xsj/crasheye/Crasheye"
            local args = {title, msg, language}
            local sigs = "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V"
            local ok, ret = luaj.callStaticMethod(className,"sendScriptException",args,sigs)  
            if not ok then
                print("crasheye set sendScriptException error", title, msg, language)
            end
        elseif device.platform == "ios" or device.platform == "mac" then
            local ok, ret = luaoc.callStaticMethod("Device_iOS", "sendScriptException", {errorTitle=title, exception=msg, file=file, language=language})
            if not ok then
                print("crasheye set sendScriptException error", title, msg, language, file)
            end
        end
    end,
    --上报日志
    setLogging = function (lines, filter)
        if device.platform == "android" then
            local luaj = require "cocos.cocos2d.luaj"
            local className="com/xsj/crasheye/Crasheye"
            local args = {lines, filter}
            local sigs = "(ILjava/lang/String;)V"
            local ok, ret = luaj.callStaticMethod(className,"setLogging",args,sigs)  
            if not ok then
                print("crasheye set setLogging error", lines, filter)
            end
        end
    end,
    --增加打点信息
    leaveBreadcrumb = function (str)
        if device.platform == "android" then
            local luaj = require "cocos.cocos2d.luaj"
            local className="com/xsj/crasheye/Crasheye"
            local args = {str}
            local sigs = "(Ljava/lang/String;)V"
            local ok, ret = luaj.callStaticMethod(className,"leaveBreadcrumb",args,sigs)  
            if not ok then
                print("crasheye set leaveBreadcrumb error", str)
            end
        elseif device.platform == "ios" or device.platform == "mac" then
            local ok, ret = luaoc.callStaticMethod("Crasheye", "leaveBreadcrumb", {breadcrumb=str})
            if ok then
                print("crasheye set leaveBreadcrumb error", str)
            end
        end
    end,
    --添加附加信息
    addExtraData = function (key, value)
        if device.platform == "android" then
            local luaj = require "cocos.cocos2d.luaj"
            local className="com/xsj/crasheye/Crasheye"
            local args = {key, value}
            local sigs = "(Ljava/lang/String;Ljava/lang/String;)V"
            local ok, ret = luaj.callStaticMethod(className,"addExtraData",args,sigs)  
            if not ok then
                print("crasheye set addExtraData error", key, value)
            end
        elseif device.platform == "ios" or device.platform == "mac" then
            local ok, ret = luaoc.callStaticMethod("Crasheye", "addExtraDataWithDic", {key=key, withValue=value})
            if not ok then
                print("crasheye set addExtraData error", key, value)
            end
        end
    end
}

--trace 捕获方法
function traceBack(msg)
    local l = {}
    for i=1,100 do
        local k, v = debug.getlocal(3, i)
        if k then
            l[k] = v
        else
            break
        end
    end

    local msgTable = string.split(msg, ":")
    local errInfo = debug.getinfo(3)
    local title = errInfo.source .."_".. errInfo.currentline
    if type(msgTable) == "table" then
        if #msgTable >= 2 then
            title = msgTable[#msgTable-1] .. "_" .. msgTable[#msgTable]
        end
    end
    local language = errInfo.what
    local file = errInfo.source
    local msg = debug.traceback(msg, 3)
    print(title, language, file,outPutKey)
    pcall(function ( ... )
        Crasheye.addExtraData("local", json.encode(l))
        Crasheye.sendScriptException(title, msg, language, file)
    end)
    print("----------------------------------------",outPutKey)
    print(msg,outPutKey)
    print("----------------------------------------",outPutKey)
    if DEBUG and DEBUG >= 1 then
        __G__TRACKBACK__RED(tostring(msg))
    end
end

--获取设备ID
function getDeviceID()
	local deviceID = ""
    if isNewAccount and KAKA_SWITCH_ENABLE then
        deviceID = cc.UserDefault:getInstance():getStringForKey("DEVICE_ID_WIN")
        if #deviceID==0 then
            deviceID = tostring(os.time())..tostring(math.random(1,1000000))
            cc.UserDefault:getInstance():setStringForKey("DEVICE_ID_WIN", deviceID)
        end
        return deviceID
    end
    
	if device.platform == "android" then
		local luaj = require "cocos.cocos2d.luaj"
        local className="org/cocos2dx/lua/AppActivity"
        local args = {}
        local sigs = "()Ljava/lang/String;"
        local ok, ret = luaj.callStaticMethod(className,"getOpenUDID",args,sigs)  
        if ok then
            deviceID = ret
        end
    elseif device.platform == "ios" then
        deviceID = cc.UserDefault:getInstance():getStringForKey("DEVICE_ID_IOS")
        if #deviceID==0 then
            local ok, ret = luaoc.callStaticMethod("Device_iOS", "GetDeviceID")
            if ok then
                deviceID = ret
                cc.UserDefault:getInstance():setStringForKey("DEVICE_ID_IOS", deviceID)
            end
        end

    elseif device.platform == "windows" or device.platform == "mac"then
        deviceID = cc.UserDefault:getInstance():getStringForKey("DEVICE_ID_WIN")
        if #deviceID==0 then
            deviceID = tostring(os.time())
            cc.UserDefault:getInstance():setStringForKey("DEVICE_ID_WIN", deviceID)
        end
    end

    local isNeedNewID = false
    if #deviceID > 0 and deviceID == "02:00:00:00:00:00" then
        isNeedNewID = true
    end
    
    if #deviceID == 0 or isNeedNewID then
        local localDeviceID = cc.UserDefault:getInstance():getStringForKey("DEVICE_ID_WIN","")
        if #localDeviceID == 0 or localDeviceID == "02:00:00:00:00:00" then
            local randomChar = ""
            for i = 1, 3 do
                randomChar = randomChar ..string.char(math.random( 65, 90))
            end
            deviceID = tostring(os.time())..string.char(math.random( 65, 90))..math.random(1,9)..randomChar
            cc.UserDefault:getInstance():setStringForKey("DEVICE_ID_WIN", deviceID)
            cc.UserDefault:getInstance():flush()
        else
            deviceID = localDeviceID
        end
    end

	return deviceID
end

-- 克隆lua对象(支持多层) by @2018-4-18
function clone(object)  
    local lookup_table = {}  
    local function _copy(object)  
        if type(object) ~= "table" then  
            return object  
        elseif lookup_table[object] then  
            return lookup_table[object]  
        end  
        local newObject = {}  
        lookup_table[object] = newObject  
        for key, value in pairs(object) do  
            newObject[_copy(key)] = _copy(value)  
        end  
        return setmetatable(newObject, getmetatable(object))  
    end  
    return _copy(object)  
end  

--获取设备信息
function getDeviceInfo(needSplit)
	local deviceInfo
	if device.platform == "android" then
		local luaj = require "cocos.cocos2d.luaj"
        local className="org/cocos2dx/lua/AppActivity"
        local args = {}
        local sigs = "()Ljava/lang/String;"
        local ok, ret = luaj.callStaticMethod(className,"getDeviceInfo",args,sigs)  
        if ok then
        	deviceInfo = ret
        end
    elseif device.platform == "ios" then
    	local ok, ret = luaoc.callStaticMethod("Device_iOS", "getDeviceInfo")
        print("call getDeviceInfo", ok, ret)
        if ok then
            deviceInfo = ret
        end
    elseif device.platform == "windows" or device.platform == "mac"  then
        deviceInfo = "OS windows;Version 1.1.1;Product KAKAGame;Device win32;UUID "..getDeviceID()..";"
	end
	if needSplit then
        local split = string.gmatch(deviceInfo, "(%S*) (%S*);")
        deviceInfo = {}
        while true do
			local a, b = split()
			if a then
				deviceInfo[a] = b
			else
				break
			end
		end
	end
    deviceInfo = string.gsub(deviceInfo, "%'", "")
	return deviceInfo
end

function getAndroidVersionCode()
    local version = 0
    local deviceInfo = getDeviceInfo(false)

    local info = {}
    for i, v in string.gmatch(deviceInfo, '(%S*) (%S*);') do
        info[i] = v
    end
    if info.VersionCode then
        version = tonumber(info.VersionCode)
    end
    return version
end

function getBaseVersion()
    local version = 0
    local deviceInfo = getDeviceInfo(false)

    local info = {}
    for i, v in string.gmatch(deviceInfo, '(%S*) (%S*);') do
        info[i] = v
    end
    if info.VersionCode then
        version = tonumber(info.VersionCode)
    end
    return version
end


-- 获取电量 (返回0-100)
function getBatteryLevel()
    if device.platform == "android" then
        local luaj = require "cocos.cocos2d.luaj"
        local className="org/cocos2dx/lua/tool/PhoneStatus"
        local args = {}
        local sigs = "()I"
        local ok, ret = luaj.callStaticMethod(className, "getBatteryLevel", args, sigs)  
        return ret
    elseif device.platform == "ios" or device.platform == "mac" then
        -- IOS 
        -- print('没有实现getBatteryLevel')
        local ok, ret = luaoc.callStaticMethod("Device_iOS", "getCurrentBatteryLevel")
        print("call getCurrentBatteryLevel", ok, ret)
        if ok then
            return ret
        end
    end 

    return 100
end

--获取网络类型
function getNetType( ... )
    local net_type = {
        "2G",
        "3G",
        "4G",
        "5G",
        "WIFI"
    }
    local net = 1
    if device.platform == "android" then
        net = 5
    elseif device.platform == "ios" or device.platform == "mac" then
        local ok, ret = luaoc.callStaticMethod("Device_iOS", "getNetworkType")
        if ok then
            net = ret
        end
    end
    if net <= 0 then
        net = 1
    end

    return net
end
-- 获取wifi信号 (返回0/25/50/75/100)
function getWifiLevel()
    if device.platform == "android" then
        local luaj = require "cocos.cocos2d.luaj"
        local className="org/cocos2dx/lua/tool/PhoneStatus"
        local args = {}
        local sigs = "()I"
        local ok, ret = luaj.callStaticMethod(className, "getWifiLevel", args, sigs)  
        return ret
    elseif device.platform == "ios" or device.platform == "mac" then
        -- IOS 
        -- print('没有实现getWifiLevel')
        local ok, ret = luaoc.callStaticMethod("Device_iOS", "getSignalStrength")
        print("call getSignalStrength", ok, ret)
        if ok then
            return ret
        end
    end 

    return 100
end
--获取渠道号
function getChannelID( ... )
    -- body
    if device.platform == "android" then
        local luaj = require "cocos.cocos2d.luaj"
        local className="org/cocos2dx/lua/AppActivity"
        local args = {}
        local sigs = "()I"
        local ok, ret = luaj.callStaticMethod(className, "getChannelID", args, sigs)  
        return ret
    elseif device.platform == "ios" then
        -- IOS 
        -- print('没有实现getWifiLevel')
        local ok, ret = luaoc.callStaticMethod("Device_iOS", "getChannelID")
        print("call getChannelID", ok, ret)
        if ok then
            return ret
        end
        return 64002
    else
        return 64001
    end 
end
--获取IDFA(IOS)
function getIfdaID( ... )
    -- body
    if device.platform == "android" then
        return " "
    elseif device.platform == "ios" or device.platform == "mac" then
        -- IOS 
        -- print('没有实现getWifiLevel')
        local ok, ret = luaoc.callStaticMethod("Device_iOS", "GetIDFAID")
        print("call GetIDFAID", ok, ret)
        if ok then
            return ret
        end
        return " "
    end 
end
--打印二进制数据
function printBin(logName, data)
    local line = ""
    for i=1, #data do
        line = line .. string.format("%02X", string.byte(data, i))
    end
    print(logName, line)
end

--URL转化为MD5值
function urlDecodeMD5( url )
	-- body
	return (string.gsub(MD5.sum(url), ".", function (c)
       return string.format("%02x", string.byte(c))
 	end))
end

function setSpriteGray( sp )  
    local vertShaderByteArray = "\n"..  
        "attribute vec4 a_position; \n" ..  
        "attribute vec2 a_texCoord; \n" ..  
        "attribute vec4 a_color; \n"..  
        "#ifdef GL_ES  \n"..  
        "varying lowp vec4 v_fragmentColor;\n"..  
        "varying mediump vec2 v_texCoord;\n"..  
        "#else                      \n" ..  
        "varying vec4 v_fragmentColor; \n" ..  
        "varying vec2 v_texCoord;  \n"..  
        "#endif    \n"..  
        "void main() \n"..  
        "{\n" ..  
        "gl_Position = CC_PMatrix * a_position; \n"..  
        "v_fragmentColor = a_color;\n"..  
        "v_texCoord = a_texCoord;\n"..  
        "}"  
  
    local flagShaderByteArray = "#ifdef GL_ES \n" ..  
        "precision mediump float; \n" ..  
        "#endif \n" ..  
        "varying vec4 v_fragmentColor; \n" ..  
        "varying vec2 v_texCoord; \n" ..  
        "void main(void) \n" ..  
        "{ \n" ..  
        "vec4 c = texture2D(CC_Texture0, v_texCoord); \n" ..  
        "gl_FragColor.xyz = vec3(0.4*c.r + 0.4*c.g +0.4*c.b); \n"..  
        "gl_FragColor.w = c.w; \n"..  
        "}"  
    local glProgram = cc.GLProgram:createWithByteArrays(vertShaderByteArray,flagShaderByteArray)  
    glProgram:bindAttribLocation(cc.ATTRIBUTE_NAME_POSITION,cc.VERTEX_ATTRIB_POSITION)  
    glProgram:bindAttribLocation(cc.ATTRIBUTE_NAME_COLOR,cc.VERTEX_ATTRIB_COLOR)  
    glProgram:bindAttribLocation(cc.ATTRIBUTE_NAME_TEX_COORD,cc.VERTEX_ATTRIB_FLAG_TEX_COORDS)  
    glProgram:link()  
    glProgram:updateUniforms()  
    sp:setGLProgram( glProgram )  
end 

function waringwait( ... )
    -- body
    local DTCommonWords = require("app.games.dt.common.DTCommonWords")
    local FloatBar = require("app.games.dt.hall.FloatBar")
    local bar = FloatBar.new(DTCommonWords.PLEASE_WAIT_NEW)
    bar:setPosition(display.center)
    cc.Director:getInstance():getRunningScene():addChild(bar)
end

-- 打开微信
function wxOpen()
    print('wxOpen')
    if device.platform == "android" then
        if not KAKA_SHARE_ENABLE then
            waringwait()
            return
        end
        local luaj = require "cocos.cocos2d.luaj"
        local className="org/cocos2dx/lua/weixin/ThirdSDKWeixin"
        local args = {}
        local sigs = "()V"
        local ok = luaj.callStaticMethod(className, "wxOpen", args, sigs)
    else
        -- IOS
        local ok, ret = luaoc.callStaticMethod("WXThirdSDKCode", "wxOpen", {})
    end
end

-- 分享微信文本
function wxShareText(text)
    print('wxShareText', text)
    if device.platform == "android" then
        if not KAKA_SHARE_ENABLE then
            waringwait()
            return
        end

        local luaj = require "cocos.cocos2d.luaj"
        local className="org/cocos2dx/lua/weixin/ThirdSDKWeixin"
        local args = {text}
        local sigs = "(Ljava/lang/String;)V"
        local ok = luaj.callStaticMethod(className, "shareText", args, sigs)  
    else
        -- IOS 
        local ok, ret = luaoc.callStaticMethod("WXThirdSDKCode", "wxShareText", {
            text = text
        })
    end  
end

-- 分享微信链接 (type 0朋友 1朋友圈)
function wxShareUrl(type, url, title, desc)

    print('wxShareUrl', table.concat({type, url, title, desc}, ":"))
  
    if device.platform == "android" then
        if not KAKA_SHARE_ENABLE then
            waringwait()
            return
        end

        local luaj = require "cocos.cocos2d.luaj"
        local className="org/cocos2dx/lua/weixin/ThirdSDKWeixin"

        local args = {type, url, title, desc}
        if type == 1 then
            args = {type, url, desc, title} -- 朋友圈分享出去无描述fix
        end

        local sigs = "(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V"
        local ok = luaj.callStaticMethod(className, "wxShareUrl", args, sigs)  
    elseif device.platform == "ios" then
        -- IOS 
        if type == 1 then
            title = title.."\n"..desc
            desc = ""
        end
        local ok, ret = luaoc.callStaticMethod("WXThirdSDKCode", "wxShareUrl", {
            shareType = type,
            url = url,
            title = title,
            des = desc
        })
    end  
end

-- 分享微信图片（注意必须提供本地路径）(type 0朋友 1朋友圈)
function wxShareImage(type, imagepath)
    print('wxShareImage', table.concat({type, imagepath}, ":"))
    if device.platform == "android" then
        if not KAKA_SHARE_ENABLE then
            waringwait()
            return
        end
        
        local luaj = require "cocos.cocos2d.luaj"
        local className="org/cocos2dx/lua/weixin/ThirdSDKWeixin"
        local args = {type, imagepath}
        local sigs = "(ILjava/lang/String;)V"
        local ok = luaj.callStaticMethod(className, "wxShareLocalImage", args, sigs)  
    else
        -- IOS 
        local ok, ret = luaoc.callStaticMethod("WXThirdSDKCode", "wxShareLocalImage", {
            shareType = type,
            imagepath = imagepath
        })
    end  
end

-- 获取app启动参数(返回lua table)
-- (注意保存，只能获取一次)
function getLaunchParams()
    if device.platform == "android" then
        local luaj = require "cocos.cocos2d.luaj"
        local className="org/cocos2dx/lua/tool/Utils"
        local args = {}
        local sigs = "()Ljava/lang/String;"
        local ok, ret = luaj.callStaticMethod(className, "getLaunchSchemeParams", args, sigs)  
        if ok then
            local stringSplit = require('app.fw.common.CommHelper').stringSplit
            local tmp = stringSplit(ret, "&")
            local t = {}
            print('getLaunchParams', ret)
            for k, v in pairs(tmp) do
                local arr = stringSplit(v, "=")
                t[arr[1]] = arr[2]
            end
            return t
        end
    else
        -- IOS 
    end  
    return {}
end

--拷贝只剪贴板
function copyToClipboard(content)
    if device.platform == "android" then
        local luaj = require "cocos.cocos2d.luaj"
        local className="org/cocos2dx/lua/AppActivity"
        local args = {content}
        local sigs = "(Ljava/lang/String;)V"
        local ok, ret = luaj.callStaticMethod(className,"copyToClipboard",args,sigs)  
        print("call copyToClipboard", ok, ret)
    elseif device.platform == "ios" or device.platform == "mac" then
        local ok, ret = luaoc.callStaticMethod("Device_iOS", "copyToClipboard", {content=content})
        print("call copyToClipboard", ok, ret)
    end
end

--OpenInstall 注册与打点统计
function OpenInstallReport(tab)
    if device.platform == "ios" or device.platform == "mac" then
        local ok, ret = luaoc.callStaticMethod("AppController", "OpenInstallReport", tab)
        print("call OpenInstallReport", ok, ret)
    end
end

--获取推送token
function getPushToken()
    if device.platform == "ios" then
        local ok, ret = luaoc.callStaticMethod("AppController", "getPushToken")
        if ok and ret then
            ret = string.sub(ret, 2, -2) 
            return ret
        end
    end
    return ""
end

--调用方式
-- local tab = 
-- {
--     bantype = {1,3},
--     activity = 
--     {
--         {type = "text",content = "测试分享"},
--         {type = "url",content = "https://www.baidu.com"},
--         -- {type = "image",content = "res/login/image/loading/text_qq.png"},
--         -- {type = "mp3",content = "res/dt/sound/bg_hall.mp3"},
--     },
--     callback = function( isSend )
--         print("......分享的返回 isSend =",isSend)
--     end
-- }
-- ShareActivityViewControlle(tab)

--app自带分享{{type,content},{type,content},.....},
--type:"text"为文字；"image"为图；"url"为链接，"mp4"为mp4，"mp3"为mp3，其中，图和视频需要传换路径
function ShareActivityViewControlle(tab)
    if device.platform == "ios" or device.platform == "mac" then
        local _shareList = ""
        for k,v in pairs(tab.activity) do
            print("..............k",k)
            local _type = v.type
            local _content = v.content
            if _type == "image" then
                -- print(".............1= "..)
                -- _content = require('app.fw.common.CommHelper').movePictocache(_content) --保存图
                -- _content = cc.FileUtils:getInstance():getWritablePath().._content --下载后的图
                _content = cc.FileUtils:getInstance():fullPathForFilename(_content)
                
            elseif  _type == "mp4" or _type == "mp3" then
                -- 
                 _content = cc.FileUtils:getInstance():fullPathForFilename(_content)
            end
            if  _shareList == "" then
                _shareList = _type.."$$".. _content
            else
                _shareList = _shareList.."##".._type.."$$".. _content
            end
        end

        local _bantype = ""
        for k,v in pairs(tab.bantype) do
            if  _bantype == "" then
                _bantype = v
            else
                _bantype = _bantype.."#"..v
            end
        end
        local sendTab = {bantype = json.encode(_bantype),shareList =json.encode(_shareList) ,callback = tab.callback}
        local ok, ret = luaoc.callStaticMethod("AppController", "setUIActivityView",sendTab)
        if ok then
            
        end
    end
end

--使用，直接在合适的时候调用，每次调用都会把之前的自定义内容删除，然后重新生成
--如果3Dtouch登陆游戏，就会有在本地保存的有其对应的type，key为3DTouchOpenType，在合适的时候取出来，进行逻辑处理
-- local DTouchOpenType =  cc.UserDefault:getInstance():getStringForKey("3DTouchOpenType","")
-- print("......DTouchOpenType = ",DTouchOpenType)

function add3DTouch()
    if device.platform == "ios" then
        --subtitle 如果没有值的话，就用空格占位
        local tab = 
        {
            {type = "cn.DM3DTouch.openSearch",title = "分享",icon="1",subtitle = "点击启动游戏，然后分享"},
            {type = "cn.DM3DTouch.openLogin",title = "登录",icon="2",subtitle = " "},
        }
        local _itemList = ""
        for k,v in pairs(tab) do
            local cellStr = v.type.."$$"..v.title.."$$"..v.icon.."$$"..v.subtitle
            if  _itemList == "" then
                _itemList = cellStr
            else
                _itemList = _itemList.."##"..cellStr
            end
        end
        local ok, ret = luaoc.callStaticMethod("AppController", "add3DTouch",{itemList =json.encode(_itemList)})
        if ok then
            
        end
    end
end

-- 获取设备所有信息
function getAllDeviceInfo()
    if device.platform == "android" then
        local luaj = require 'cocos.cocos2d.luaj'
        local className = 'org/cocos2dx/lua/emulator/EmulatorDetector'
        local args = {}
        local sigs = "()Ljava/lang/String;"
        local ok, ret = luaj.callStaticMethod(className, 'getDeviceListing', args, sigs)
        if ok and ret then
            local idx = string.find(ret, 'GL_EXTENSIONS')
            if idx and 2 < idx then
                ret = string.sub(ret, 1, idx - 1)
            end
            return ret
        end
    end
end

-- 判定是否是模拟器
function isEmulator()
    -- local bRet = false

    -- local luaj = require "cocos.cocos2d.luaj"
    -- local className="org/cocos2dx/lua/AppActivity"
    -- local args = {}
    -- local sigs = "()Z"
    -- local ok, ret = luaj.callStaticMethod(className,"isEmulator",args,sigs)
    -- --print("call isEmulator", ok, ret)
    -- if ok then
    --     bRet = ret
    -- end

    -- return bRet

    local version = 0
    local deviceInfo = getDeviceInfo(false)

    local info = {}
    for i, v in string.gmatch(deviceInfo, '(%S*) (%S*);') do
        info[i] = v
    end
    if info.VersionCode then
        version = tonumber(info.VersionCode)
    end
    if 1 < version then
        local luaj = require 'cocos.cocos2d.luaj'
        local className = 'org/cocos2dx/lua/AppActivity'
        local args = {}
        local sigs = '()Z'
        local ok, ret = luaj.callStaticMethod(className, 'isEmulator', args, sigs)
        if ok then
            return ret
        end
    end

    return false
end


-- 统一处理数字显示格式
function formatNumber(num)
    -- 数字最多到9999999.99 必须保留两位小数
    return string.format("%.02f", num)
end

------------------------------------------- 微信分享图片 ----------------------------------------------
-- 创建一个sprite
local function createRenderNodeWithPath(path, posX, posY, scale)
        local sprite = nil
        if path then
            sprite = cc.Sprite:create(path)
            sprite:setAnchorPoint(cc.p(0,0))
            sprite:setPosition(cc.p(posX,posY))
            sprite:setScale(scale)
        end
        return sprite
end

-- 两张图片纹理二合一
local function createRenderTextureWithNodes(firstRenderNode, secondRenderNode, width, height)
    local renderTexture = cc.RenderTexture:create(width, height)
    renderTexture:beginWithClear(0,0,0,0)

    if firstRenderNode then
        firstRenderNode:getTexture():setTexParameters(_G.GL_LINEAR, _G.GL_LINEAR, _G.GL_CLAMP_TO_EDGE, _G.GL_CLAMP_TO_EDGE)
    end

    if firstRenderNode then
        firstRenderNode:visit()
    end
   
    if secondRenderNode then
        secondRenderNode:visit()
    end

    renderTexture:endToLua()
    return renderTexture
end

-- 把两张图片合成一张图片，存到本地
function createImageFileWithTwoImage(firstImagePath, secondImagePath, width, height)
    local firstNode  =  createRenderNodeWithPath(firstImagePath, 0, 0, 1)
    local secondNode = createRenderNodeWithPath(secondImagePath, 437, 176, 0.63)
    local toFileName = "pandaWXSharePic.png"
    local renderTexture = createRenderTextureWithNodes(firstNode, secondNode, width, height)
    if renderTexture then
        local saveRet = renderTexture:saveToFile(toFileName, cc.IMAGE_FORMAT_JPEG, false)
        cc.Director:getInstance():getTextureCache():removeTextureForKey(cc.FileUtils:getInstance():getWritablePath()..toFileName)
        if saveRet then
           return  cc.FileUtils:getInstance():getWritablePath() .. toFileName
        else
            print("保存图片失败")
            return nil
        end
    end
end
--处理URL加密
function getLocalDayStartTime()
    local now_date = os.date("*t",now) 
    local dayEndTime = os.time{year=now_date.year, month=now_date.month, day=now_date.day, hour=0,min=0,sec=0}
    -- print(dayEndTime,"dayEndTime")
    -- local lessime = (os.time() - dayEndTime) +1
    return dayEndTime
end

function urlencodeJsonWithTime(jsonData)
    local njsonData = jsonData..getLocalDayStartTime()
    local newmd5 = urlDecodeMD5(njsonData)
    print(njsonData,"njsonData")
    return "&t="..os.time().."&sign="..newmd5
end
function qrCodeUrlMake(url)
    local head = "http://api.k780.com:88/?app=qr.get&data=%s&level=H&size=9"
    return string.format(head,url)
end
function openOlineFeedback(rid)
    -- body
    print("openOlineFeedback",rid == nil,rid)
    if device.platform == "ios" or device.platform == "mac" then
        local ok, ret = luaoc.callStaticMethod("AppController", "openOlineFeedback",{userID=tostring(rid)})
        print("openOlineFeedback", ok, ret)
    elseif device.platform == "android" then
         local luaj = require 'cocos.cocos2d.luaj'
        local className = 'org/cocos2dx/lua/AppActivity'
        local args = {tostring(rid)}
        local sigs = '(Ljava/lang/String;)V'
        local ok, ret = luaj.callStaticMethod(className, 'openOlineFeedback', args, sigs)
        if ok then
            return ret
        end
    end
end