--BaseFullLayer.lua
--全屏layer基类
--Create by . 2017-05-09
import(".AdapterUtil")
local BaseLayer = import(".BaseLayer")
local BaseFullLayer = class("BaseFullLayer",BaseLayer)
local _ = {}
local allPage = {}

function BaseFullLayer:onCreate( ... )
	_:savePage(self)
	--适配 
	--AdapterUtil.Scene(self.resourceNode_) 
	BaseFullLayer.super.onCreate(self, ...)
end

function BaseFullLayer:onCleanup( ... )
	_:deletePage(self)
end

-- 获取界面实例
function BaseFullLayer:getPage()
	return allPage[self.__cname]
end


-- 保存界面
function _:savePage(page)
	print("BaseFullLayer:savePage", page.__cname)
	allPage[page.__cname] = page
end
-- 删除界面
function _:deletePage(page)
	print("BaseFullLayer.deletePage", page.__cname)
	allPage[page.__cname] = nil
end

return BaseFullLayer