--CommHelper.lua
--Create by @2017-6-7
--工具函数

local CommonWords = import(".CommonWords")
local CommonNetImageView = import(".CommonNetImageView")
--local MD5 = require "MD5"
--local math = require('math')
local math_random = math.random

local t_sex = {[0] = "woman", [1] = "man"}

return {

    -- 序列化table成string
    -- (以后需要的时候string可由loadstring函数反序列化成table)
    serialize = function(t)
        local mark = {}
        local assign = {}

        local function ser_table(tbl, parent)
            mark[tbl] = parent
            local tmp = {}
            for k, v in pairs(tbl) do
                local key = type(k) == "number" and "["..k.."]" or k
                if type(v) == "table" then
                    local dotkey = parent..(type(k) == "number" and key or "."..key)
                    if mark[v] then
                        table.insert(assign, dotkey.."="..mark[v])
                    else
                        table.insert(tmp, key.."="..ser_table(v, dotkey))
                    end
                elseif type(v) == "string" then
                    table.insert(tmp, key.."=\"" .. tostring(v) .. "\"")
                else
                    table.insert(tmp, key.."=" .. tostring(v))
                end
            end
            return "{"..table.concat(tmp, ",") .. "}"
        end

        return "do local ret="..ser_table(t, "ret")..table.concat(assign, " ") .. " return ret end"
    end, 

    -- 动态加载脚本, release时禁用
    reloadScript = function(path)
        if DEBUG and DEBUG > 1 then
            package.loaded[path] = nil
        end
        return require(path)
    end, 
    -- 重新加载模块
    reloadModule = function (moduleName)
        assert(type(moduleName) == "string", "moduleName must string type!")
        for var in pairs(package.loaded) do
            if string.find(var, "." .. moduleName .. ".") then
                print("reloadScript", var)
                package.loaded[var] = nil
            end
        end
    end, 

    --判定两个时间是否为一天
    isSameDay = function(time1, time2)
        local date_a = os.date("*t", time1)
        local date_b = os.date("*t", time2)
        if date_a.year == date_b.year and date_a.yday == date_b.yday then
            return true
        else
            return false
        end
    end, 

    

    stringSplit = function(str, split_char)
        local resultStrList = {}
        string.gsub(str, '[^'..split_char..']+', function (w)
            table.insert(resultStrList, w)
        end)
        return resultStrList
    end, 

    -- 头像加载统一接口
    loadHeadImg = function(imgView, sex, logo, head_format)
        local path = string.format(head_format, t_sex[sex], logo)
        imgView:loadTexture(path, ccui.TextureResType.plistType)
    end, 
    -- 加载大头像
    loadHeadBigImg = function(this, imgView, sex, logo, size)
        logo = 5
        this._clearNetImg(imgView)
        if tonumber(logo) ~= nil then
            this.loadHeadImg(imgView, sex, logo, "head_b%s_%02d.png")
        else
            -- this.loadNetImage(imgView, nil, logo)
            -- CommonNetImageView:create(logo,imgView,size):retain()
            if not size then
                size = imgView:getContentSize()
            end
            this.loadHeadImg(imgView, sex, logo, "head_bwoman_05.png")
            size = imgView:getContentSize()
            imgView:addChild(CommonNetImageView:create(logo, imgView, size))
        end
    end, 
    -- 加载中头像
    loadHeadMiddleImg = function(this, imgView, sex, logo, size)
        this._clearNetImg(imgView)
        if tonumber(logo) ~= nil then
            this.loadHeadImg(imgView, sex, logo, "head_m%s_%02d.png")
        else
            -- this.loadNetImage(imgView, nil, logo)
            -- CommonNetImageView:create(logo,imgView,size):retain()
            if not size then
                size = imgView:getContentSize()
            end
            this.loadHeadImg(imgView, sex, logo, "head_mwoman_05.png")
            size = imgView:getContentSize()
            imgView:addChild(CommonNetImageView:create(logo, imgView, size))
        end
    end, 
    -- 加载小头像
    loadHeadSmallImg = function(this, imgView, sex, logo, size)
        this._clearNetImg(imgView)
        if tonumber(logo) ~= nil then
            this.loadHeadImg(imgView, sex, logo, "head_m%s_%02d.png")
        else
            -- this.loadNetImage(imgView, nil, logo)
            -- CommonNetImageView:create(logo,imgView,size):retain()
            if not size then
                size = imgView:getContentSize()
            end
            this.loadHeadImg(imgView, sex, logo, "head_mwoman_05.png")
            size = imgView:getContentSize()
            imgView:addChild(CommonNetImageView:create(logo, imgView, size))
        end
    end, 

    -- 仅内部使用：清除微信头像等加载进子节点的图片
    _clearNetImg = function(node)
        if node:getChildByName('CommonNetImageView') then
            node:removeChildByName('CommonNetImageView')
        end
    end, 

    -- 加载url图片
    loadNetImage = function(imgView, size, url)
        size = size or imgView:getContentSize()
        local fileExtension = url:match(".+%.(%w+)$")
        local saveFileName = string.format("%s.%s", urlDecodeMD5(url), fileExtension)

        local xhr = cc.XMLHttpRequest:new()
        xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
        xhr:open("GET", url)

        local path = cc.FileUtils:getInstance():getWritablePath() .. "ns/"
        if not io.exists(path) then
            require("lfs").mkdir(path)
        end
        path = path..saveFileName

        local function onReadyStateChange(...)
            print('onReadyStateChange', imgView:getReferenceCount())
            local released = imgView:getReferenceCount() <= 1
            imgView:release()
            if released then
                print('img already released...')
                return
            end

            if xhr.status == 200 then
                local file = io.open(path, "wb")
                file:write(xhr.responseText)
                file:close()
                imgView:loadTexture(path)
            else
                print("网络连接失败或者文件不存在", path)
            end
        end
        xhr:registerScriptHandler(onReadyStateChange)
        xhr:send()
        imgView:retain()
    end, 

    -- 设置VIP图标(可以在编辑器上拖vip控件，无则动态创建)
    setVIPIcon = function(this, parentNode, vipLv, showVip0)
        --this._nn = ((this._nn or 0) + 1) % 5 + 1
        -- vipLv = 5-- math.random(1, 5) -- tofix

        -- if true then
        --     print("占时未做 做了就开启VIP")
        --     return
        -- end

        local function childFn(childName, fnName, ...)
            local child = parentNode:getChildByName(childName)
            if child ~= nil then
                child[fnName](child, ...)
            end
            return child
        end

        -- 初始
        vipLv = vipLv or 0
        local size = parentNode:getContentSize()

        childFn('norFrame', 'setVisible', vipLv == 0)
        childFn('norFrame', 'setLocalZOrder', 1)
        childFn('vip', 'setVisible', vipLv > 0)
        childFn('vipFrame', 'removeFromParent')

        do return end
        if vipLv == 0 and not showVip0 then
            return
        end

        -- 图标路径
        -- local path = string.format('common/vip/VIP_jb%02d.png', vipLv)
        local path = "dt/image/personInfo/tjylc_grxx_vip.png"
        print('vip path:', path)

        -- 已存在vip icon的情况
        local vipSpr = parentNode:getChildByName('vip')
        if vipSpr ~= nil then
            vipSpr:setVisible(true)
            if vipSpr.setTexture ~= nil then
                vipSpr:setTexture(path)
            else
                vipSpr:loadTexture(path)
            end
        else
            -- 重新创建
            vipSpr = cc.Sprite:create(path)
            vipSpr:setAnchorPoint(cc.p(1, 0))
            vipSpr:setPosition(cc.p(size.width, 0))

            local sprSize = vipSpr:getContentSize()
            local scale = size.width * 0.7 / sprSize.width
            vipSpr:setScale(scale)
            vipSpr:setName('vip')
            parentNode:addChild(vipSpr)

        end

        if vipLv >= 0 then
            return
        end

        -- VIP特效框
        local layer = cc.CSLoader:createNode(string.format("dt/VipIconAnimation_%d.csb", vipLv))
        if layer == nil then
            print('warning: invalid vip frame lv:', vipLv)
            return
        end

        local framePanel = layer:getChildByName("Panel_1")
        framePanel:removeFromParent(false)
        framePanel:setName('vipFrame')
        parentNode:addChild(framePanel)

        local frameSize = framePanel:getContentSize()
        framePanel:setScale(size.width / frameSize.width, size.height / frameSize.height)
        framePanel:setPosition(cc.p(size.width / 2, size.height / 2))
        framePanel:setLocalZOrder(2)
    end, 

    -- 添加遮罩处理的头像(头像大小会变为遮罩大小)
    addMaskIcon = function(this, parent, maskSp, sex, logo)
        if tonumber(logo) ~= nil then
            local headFormat = "head_m%s_%02d.png"
            -- local size = maskSp:getContentSize()
            -- if size.width <= 70 then
            --     headFormat = "head_s%s%02d.png"
            -- elseif size.width <= 100 then
            --     headFormat = "head_m%s%02d.png"
            -- end
            local path = string.format(headFormat, t_sex[sex], logo)
            local itemSp = cc.Sprite:createWithSpriteFrameName(path)
            local icon = this:createMaskWithSprite(maskSp, itemSp)
            local szpt = parent:getContentSize()
            icon:setPosition(szpt.width * 0.5, szpt.height * 0.5)
            parent:add(icon)
        else
            parent:addChild(CommonNetImageView:create(logo, parent, parent:getContentSize(), maskSp))
        end
    end, 
    -- 创建被遮罩处理的精灵
    createMaskWithSprite = function(this, maskSp, itemSp)
        local maskSize = maskSp:getContentSize()
        local itemSize = itemSp:getContentSize()
        maskSp:setPosition(maskSize.width * 0.5, maskSize.height * 0.5)
        itemSp:setPosition(maskSize.width * 0.5, maskSize.height * 0.5)
        itemSp:setScale(maskSize.width / itemSize.width, maskSize.height / itemSize.height)
        maskSp:setBlendFunc(cc.blendFunc(GL_ONE, GL_ZERO))
        itemSp:setBlendFunc(cc.blendFunc(GL_DST_ALPHA, GL_ZERO))
        local pRt = cc.RenderTexture:create(maskSize.width, maskSize.height)
        pRt:begin()
        maskSp:visit()
        itemSp:visit()
        pRt:endToLua()
        local retval = cc.Sprite:createWithTexture(pRt:getSprite():getTexture())
        retval:setFlippedY(true)
        return retval
    end, 

    -- 创建一个使用模板sprite剪裁的头像，后面在此node下面增加子节点会自动剪裁
    -- 可选参数: framePath, frameOffset
    -- 说明: 头像框现在放到头像的上级节点, 不参与剪裁, frameOffset用来微调位置
    -- by  @2017-10-9
    createStencilAvatar = function(this, oldAvatarNode, maskPath, framePath, frameOffset)
        local stencil = cc.Sprite:create(maskPath)
        local size = oldAvatarNode:getContentSize()
        local stenSize = stencil:getContentSize()
        local sx, sy = oldAvatarNode:getScaleX(), oldAvatarNode:getScaleY()
        stencil:setScale(sx * size.width / stenSize.width, sy * size.height / stenSize.height)

        local clipper = cc.ClippingNode:create(stencil)
        clipper:setAnchorPoint(cc.p(0.5, 0.5))
        clipper:setAlphaThreshold(0.01)
        oldAvatarNode:getParent():addChild(clipper)
        -- clipper:setScale(oldAvatarNode:getScale())
        -- clipper:setScale(sx * size.width / stenSize.width, sy * size.height / stenSize.height)
        clipper:setPosition(oldAvatarNode:getPosition()) -- 替代原来的位置
        oldAvatarNode:setVisible(false)

        -- 默认资源的头像
        local headFormat = "head_m%s_%02d.png"
        if size.width <= 70 then
            headFormat = "head_m%s_%02d.png"
        elseif size.width <= 130 then
            headFormat = "head_m%s_%02d.png"
        end
        local path = string.format(headFormat, 'man', 1)
        -- local newAvatarNode = cc.Sprite:createWithSpriteFrameName(path)
        print('##fuck assert', path)
        local newAvatarNode = ccui.ImageView:create(path, ccui.TextureResType.plistType)
        clipper:addChild(newAvatarNode)
        local newAvatarNodeSize = newAvatarNode:getContentSize()
        local newSize = newAvatarNode:getContentSize()
        newAvatarNode:setScale(sx * size.width / newSize.width, sy * size.height / newSize.height)

        -- 非vip显示的头像框
        if framePath then
            local frame = cc.Sprite:create(framePath)
            frame:setName('norFrame')
            local _frameSize = frame:getContentSize()
            -- frame:setScale(newAvatarNodeSize.width / _frameSize.width, newAvatarNodeSize.height / _frameSize.height)
            frameOffset = frameOffset or cc.size(8, 8)
            frame:setContentSize(cc.size(newAvatarNodeSize.width + frameOffset.width, newAvatarNodeSize.height + frameOffset.height))
            frame:setPosition(clipper:getPosition())
            frame:setScale(newAvatarNode:getScaleX(), newAvatarNode:getScaleY())
            -- newAvatarNode:addChild(frame)
            oldAvatarNode:getParent():addChild(frame)
        end
        return newAvatarNode
    end, 

    -- 统一接口:显示玩家昵称，最多x个汉字长度，多余字符以省略号显示
    loadName = function(this, label, name, customMax)
        if not label then
            return
        end
        local maxLen = customMax or 8
        if name and #name > maxLen then
            label:setString(this:getUtfLimitStr(name, maxLen))
        else
            label:setString(name or '')
        end
    end, 
    -- 统一接口:返回显示玩家昵称，最多x个汉字长度，多余字符以省略号显示
    getloadName = function(this, name, customMax)
        local maxLen = customMax or 8
        if name and #name > maxLen then
            return this:getUtfLimitStr(name, maxLen)
        else
            return name or ''
        end
    end,
    -- 判断utf8字符byte长度
    chsize = function(char)
        if not char then
            return 0
        elseif char > 240 then
            return 4
        elseif char > 225 then
            return 3
        elseif char > 192 then
            return 2
        else
            return 1
        end
    end, 
    -- 估算出有多少个汉字长度
    getUtfNum = function(this, str)
        local len = 0
        local cur = 1
        while cur <= #str do
            local char = string.byte(str, cur)
            local size = this.chsize(char)
            cur = cur + size
            len = len + (size > 2 and 1 or 0.5)
        end
        return len
    end, 
    -- 估算出需要多少行
    getLineNum = function(this, str, lineMaxNum)
        local cnt = 0
        string.gsub(str, '[^\n]+', function (s)
            cnt = cnt + math.ceil(this:getUtfNum(s) / lineMaxNum)
        end)
        return cnt
    end, 
    -- 获取从开头到指定汉字长度的字符串
    getSubStringByNum = function(this, str, num)
        local len = 0
        local cur = 1
        while cur <= #str do
            local char = string.byte(str, cur)
            local size = this.chsize(char)
            len = len + (size > 2 and 1 or 0.5)
            if len > num then
                break
            else
                cur = cur + size
                if len == num then
                    break
                end
            end
        end
        return string.sub(str, 1, cur - 1), string.sub(str, cur)
    end, 
    -- 获取限制了宽度和行数的一组字符串数据
    getDicStrWithLimit = function(this, fontSize, maxW, maxLine, aryStr, goOnDic)
        if not goOnDic then
            goOnDic = {ary = {}, nCur = 0, lCur = 1}
        end
        local maxN = math.floor(maxW / fontSize)
        for _, dic in ipairs(aryStr) do
            local newLine = 0
            string.gsub(dic.str, '[^\n]+', function (s)
                newLine = newLine + 1
                if newLine > 1 then
                    -- 有换行符
                    goOnDic.lCur = goOnDic.lCur + 1
                    goOnDic.nCur = 0
                end
                if goOnDic.lCur > maxLine then
                    return
                end
                local num = this:getUtfNum(s)
                while true do
                    if goOnDic.lCur > maxLine then
                        return
                    end
                    if num + goOnDic.nCur > maxN then
                        local nLeft = maxN - goOnDic.nCur
                        local bolEd = goOnDic.lCur == maxLine
                        if bolEd then
                            nLeft = nLeft - 1
                        end
                        local s1, s2 = this:getSubStringByNum(s, nLeft)
                        if bolEd then
                            s1 = s1 .. '…'
                        end
                        table.insert(goOnDic.ary, {str = s1, line = goOnDic.lCur, color = dic.color})
                        s = s2
                        num = num - nLeft
                        goOnDic.lCur = goOnDic.lCur + 1
                        goOnDic.nCur = 0
                    else
                        goOnDic.nCur = goOnDic.nCur + num
                        table.insert(goOnDic.ary, {str = s, line = goOnDic.lCur, color = dic.color})
                        break
                    end
                end
            end)
        end
        return goOnDic
    end, 
    -- 假设utf8字符串为长度1 英文数字等为0.5长度
    getUtfLimitStr = function(this, str, giveLen)
        local useStr = ''
        local endStr = ''
        local max = giveLen
        local len = 0
        local cur = 1
        -- print("#str,str", #str,str)
        while cur <= #str do
            local char = string.byte(str, cur)
            local size = this.chsize(char)
            cur = cur + size
            len = len + (size > 2 and 1 or 0.5) --假设utf8字符串为长度1 英文数字等为0.5长度
            if len < max then
                useStr = useStr .. string.sub(str, cur - size, cur - 1)
            elseif #endStr == 0 then
                endStr = endStr .. string.sub(str, cur - size, cur - 1)
            elseif #endStr > 0 then
                endStr = '…'
                break
            end
            -- print("len, useStr, endStr", len, useStr, endStr)
        end
        return useStr .. endStr
    end, 


    -- 统一接口:带单位的数字
    loadNumber = function(this, label, num, isAtlas, noDian)
        if not label then
            return
        end
        if num and num ~= 0 then
            label:setString(this.getDealNumStr(num, isAtlas, noDian))
        else
            label:setString(num or '')
        end
    end, 
    -- 统一接口:银行计数法
    loadBankNumber = function(this, label, num)
        if not label then
            return
        end
        if num and math.abs(num) > 999 then
            label:setString(this.getBankNumStr(num))
        else
            label:setString(num or '')
        end
    end, 
    -- 统一接口:用listView拼接x数字的情况: 如:x10
    loadXNumber = function(this, listView, str, giveFont, givePath)
        if not listView then
            return
        end
        local path = this.getJointPath(giveFont, givePath)
        this.pushJointImage(listView, path, str or '')
    end, 
    -- 统一接口:用listView拼接带单位的数字
    loadJointNumber = function(this, listView, num, giveFont, givePath)
        if not listView then
            return
        end
        local path = this.getJointPath(giveFont, givePath)
        local str = this.getDealNumStr(num or 0, true)
        this.pushJointImage(listView, path, str)
    end, 
    -- 去掉末尾的0
    zeroOut = function(this, str)
        if string.sub(str, -1) == '0' then
            return this:zeroOut(string.sub(str, 1, #str - 1))
        end
        return str
    end, 
    -- 获取按规则处理后的数字 (cnt可为任意数)
    getDealNumStr = function(cnt, isAtlas, noDian)
        if not CC_VESION_MEG and KKConstant.NUM_KEEP_TWO_DECIMAL then
            return formatNumber(cnt)
        end

        --长度举例:99999 10.2万 102万 1020万 1.22亿 12.2亿 122亿 1220亿 1200000000亿 0.02 10.02 100.02 1000.2 10000
        --故小数最多就两位
        --持有金币＜10万，则完全以数字显示；
        --10万≤持有金币＜10亿，则缩进为以万位显示，万以下数字省略不显示，如213457，显示为21万
        --持有金币≥10亿，则缩进为以亿位显示，亿以下数字省略不显示
        if cnt == 0 then
            return '0'
        end
        local pm, str, dian, unit = '', '', '', ''
        local num = math.abs(cnt)
        if num < KKConstant.NUM.WAN * 10 then
        elseif num < KKConstant.NUM.YI then
            cnt = cnt / KKConstant.NUM.WAN
            unit = isAtlas and ':' or CommonWords.WAN
        else
            cnt = cnt / KKConstant.NUM.YI
            unit = isAtlas and ';' or CommonWords.YI
        end
        local srn = tostring(cnt)
        if string.find(srn, 'e') then
            -- 科学计数法的 转成正常计数方式
            -- 这里lua会自动四舍五入，故多取一位，后面再裁去即可保留准确的两位小数
            srn = string.format("%.03f", cnt)
        end
        if string.find(srn, '-') then
            pm = '-'
            srn = string.sub(srn, 2)
        end
        local bitDian = string.find(srn, '%.')
        if bitDian then
            str, dian = string.sub(srn, 1, bitDian - 1), string.sub(srn, bitDian + 1)
        else
            str = srn
        end
        -- str+.+dian+unit 最长6位(最多保留2位小数)
        if (not noDian) and #dian > 0 then
            local maxDian = #unit > 0 and 3 or 5
            if #str < maxDian then
                local len = math.min(maxDian - #str, 2)
                local len = 2
                local char = require('app.fw.common.CommHelper'):zeroOut(string.sub(dian, 1, len))
                if #char > 0 then
                    dian = '.'..char
                else
                    dian = ''
                end
            else
                dian = ''
            end
        else
            dian = ''
        end

        local tmpNum = str..dian
        if tonumber(tmpNum) == 0 then
            return '0'
        end
        return pm .. tmpNum .. unit
    end, 
    -- 获取把数字转换为银行计数法规则的字符串(如-1000.8 显示为-1,000.8)
    getBankNumStr = function(cnt)
        -- if KKConstant.NUM_KEEP_TWO_DECIMAL then
        --     return formatNumber(cnt)
        -- end

        local bInt, integer, decimal, pm = '', '', '', ''
        local str = tostring(cnt)
        local dian = string.find(str, '%.')
        if dian then
            integer = string.sub(str, 1, dian - 1)
            decimal = string.sub(str, dian)
        else
            integer = str
        end
        if cnt < 0 then
            pm = '-'
            integer = string.sub(integer, 2)
        end

        local max = #integer
        for i = 1, max do
            local char = string.sub(integer, -i, -i)
            if i < max and i % 3 == 0 then
                char = ',' .. char
            end
            bInt = char..bInt
        end

        return pm..bInt..decimal
    end,
    -- 获取把数字转换为银行计数法规则的字符串(如-1000.8 显示为-1,000.8)，获取整数部分
    getBankNumInt = function(bankNum)
        bankNum = bankNum .. ''
        local ret = bankNum
        local idx = string.find(bankNum, '%.')
        if idx then
            ret = string.sub(bankNum, 1, idx - 1)
        end
        return ret
    end,
    -- 拼接图片加入到列表容器里
    pushJointImage = function(listView, path, str)
        local tmp = ''
        listView:setScrollBarEnabled(false)
        listView:removeAllItems()
        for i = 1, #str do
            tmp = path..string.sub(str, i, i) .. '.png'
            listView:pushBackCustomItem(ccui.ImageView:create(tmp, UI_TEX_TYPE_LOCAL))
        end
    end, 
    -- 获取拼接图片路径
    getJointPath = function(giveFont, givePath)
        giveFont = giveFont or "jointNum1"
        givePath = givePath or "common/number/"
        return givePath..giveFont.."/"
    end, 


    --创建骨骼动画(旧的json格式)
    createJsonAnimation = function(file, name)
        print('createJsonAnimation'..name, file)
        ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(file)
        local armature = ccs.Armature:create(name)
        function armature:play(...)
            self:getAnimation():play(...)
        end

        -- zoro add at 2015.05.27
        function armature:stop()
            self:getAnimation():stop()
        end

        function armature:playWithIndex(...)
            self:getAnimation():playWithIndex(...)
        end

        function armature:setLoop(bLoop)
            self._isLoop = bLoop; 
        end

        function armature:setEndCallback(func)
            self._endCallback = func; 
            if self._isLoop == nil then
                self:setLoop(false); 
            end
        end

        function armature:setAutoRemove(isAutoRemove)
            self._isAutoRemove = isAutoRemove; 
        end

        function armature:isAutoRemove()
            return self._isAutoRemove == true; 
        end

        local function onNodeEvent(event)
            if event == "cleanup" then
                ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo(file)
            end
        end
        armature:registerScriptHandler(onNodeEvent)

        armature:setAutoRemove(true); 

        local function AnimateMovement(armatureBack, movementType, movementID)
            if movementType == ccs.MovementEventType.loopComplete then
                if armatureBack._isLoop == false then
                    movementType = ccs.MovementEventType.complete; 
                end
            end

            if movementType == ccs.MovementEventType.complete then
                if armatureBack._endCallback then
                    armatureBack._endCallback(); 
                end

                if armatureBack:isAutoRemove() then
                    armatureBack:runAction(cc.RemoveSelf:create())
                end
            end
        end
        armature:getAnimation():setMovementEventCallFunc(AnimateMovement)

        return armature
    end, 

    -- 用于对齐控件列表(横向水平居中)
    halignCtrls = function(this, ctrlList, parentNode)
        -- 如果是名称列表
        if type(ctrlList[1]) == "string" then
            local tmp = {}
            for i, name in pairs(ctrlList) do
                local ctrl = parentNode:getChildByName(name)
                if ctrl == nil then
                    print('warning: no ctrl named:', name)
                else
                    table.insert(tmp, ctrl)
                end
            end
            ctrlList = tmp
        end

        local totalLen = 0
        for i, ctrl in pairs(ctrlList) do
            if ctrl:isVisible() then
                local width = ctrl:getBoundingBox().width
                totalLen = totalLen + width
                -- ctrl:setAnchorPoint(cc.p(0, 0))
            end
        end

        -- 重新排列
        local centerPosX = parentNode:getBoundingBox().width / 2
        local startX = centerPosX - totalLen / 2
        local len = 0
        for i, ctrl in pairs(ctrlList) do
            if ctrl:isVisible() then
                local anchorX = ctrl:getAnchorPoint().x
                local width = ctrl:getBoundingBox().width
                local x = startX + len + width * anchorX
                ctrl:setPositionX(x)
                len = len + width
            end
        end

    end, 
    getMeinv = function (this)
        -- body
        if KAKA_PACKAGE_SWICH == 2 then
            if not this.Meinv then
                this.Meinv = sp.SkeletonAnimation:create("dt/image/girl/tiantanggirl.json", "dt/image/girl/tiantanggirl.atlas")
                this.Meinv:setAnimation(0, "tiantanggirl", true)
                this.Meinv:retain()
            end
            this.Meinv:removeFromParent()
            return this.Meinv
        else
            if not this.Meinv then
                this.Meinv = sp.SkeletonAnimation:create("dt/image/girl/girl.json", "dt/image/girl/girl.atlas")
                this.Meinv:setAnimation(0, "girl", true)
                this.Meinv:retain()
            end
            this.Meinv:removeFromParent()
            return this.Meinv
        end

    end, 
    playActionLoop = function (path, actionName, root, isloop)
        -- body
        isloop = isloop == nil and true or isloop
        local ac2 = cc.CSLoader:createTimeline(path)
        if ac2:IsAnimationInfoExists(actionName) == true then
            ac2:play(actionName, isloop)
        end
        root:runAction(ac2)
        return ac2
    end, 
    playActionFunc = function (path, actionName, root, isloop, func, frameName)
        -- body
        local ac2 = cc.CSLoader:createTimeline(path)
        ac2:setFrameEventCallFunc(function (frame)
            -- body
            if frame:getEvent() == frameName then
                if func then
                    func()
                end
            end
        end)
        if ac2:IsAnimationInfoExists(actionName) == true then
            ac2:play(actionName, isloop)
        end

        root:runAction(ac2)
    end, 
    --自动换行
    getLabelStringByLimitWidth = function(labelObj, labelStr, limitWidth)
        -- body
        local function getStrMap(str)
            -- body
            local strTab = {}
            for uchar in string.gfind(str, "[%z\1-\127\194-\244][\128-\191]*") do
                strTab[#strTab + 1] = uchar
            end
            return strTab
        end
        local labelStr = labelStr
        local strMap = getStrMap(labelStr)
        local setStr = ""
        local tempStr2 = ""
        for k, v in pairs(strMap) do
            local tempStr = tempStr2..v
            labelObj:setString(tempStr)
            if labelObj:getContentSize().width > limitWidth then
                tempStr2 = tempStr.."\n"
                setStr = setStr..tempStr2
                tempStr2 = ""
            else
                tempStr2 = tempStr
            end
        end
        setStr = setStr..tempStr2
        return setStr
    end, 
    -- 检查富文本字符串格式是否有错误
    checkHtmlHasError = function(htmlStr)
        local n1 = 0
        local n2 = 0
        string.gsub(htmlStr, "<font", function(s)
            n1 = n1 + 1
        end)
        string.gsub(htmlStr, "</font>", function(s)
            n2 = n2 + 1
        end)
        return n1 ~= n2
    end, 
    -- 富文本格式字符串提取出真实信息
    html2Normal = function(htmlStr)
        local new = htmlStr
        for i, v in ipairs({"<font.->", "</font>"}) do
            new = string.gsub(new, v, '')
        end
        return new
    end, 
    -- 富文本格式字符串因把"玩家名字"提出来使得成为三段
    htmlSplitByName = function(this, htmlStr, c3bDefault, c3bName)
        local ary = {}
        local _, index = string.find(htmlStr, "#0FED68.->") --根据颜色提取名字
        if index then
            local pre = string.sub(htmlStr, 1, index)
            local aft = string.sub(htmlStr, index + 1)
            local start = string.find(aft, "</font>")
            local name = string.format("【%s】", string.sub(aft, 1, start - 1))
            local last = string.sub(aft, start)
            table.insert(ary, {str = this.html2Normal(pre), color = c3bDefault})
            table.insert(ary, {str = name, color = c3bName or cc.c3b(15, 237, 104)})
            table.insert(ary, {str = this.html2Normal(last), color = c3bDefault})
        else
            table.insert(ary, {str = this.html2Normal(htmlStr), color = c3bDefault})
        end
        return ary
    end, 

    htmlSplitByName2 = function(this, htmlStr, nameColor, contentNameColor, contentColor)
        print('htmlSplitByName2@@=', htmlStr)
        local ary = {}
        local _, index = string.find(htmlStr, "#1E91E8.->") --根据颜色提取名字
        local _, index2 = string.find(htmlStr, '】：')
        if index then
            local pre = string.sub(htmlStr, 1, index)
            local aft = string.sub(htmlStr, index + 1)
            local start = string.find(aft, "</font>")
            local name = string.format("【%s】", string.sub(aft, 1, start - 1))
            local last = string.sub(aft, start)
            table.insert(ary, {str = this.html2Normal(pre), color = nameColor})
            table.insert(ary, {str = name, color = contentNameColor})
            table.insert(ary, {str = this.html2Normal(last), color = contentColor})
        elseif index2 then
            local name = string.sub(htmlStr, 1, index2)
            local last = string.sub(htmlStr, index2 + 1)
            table.insert(ary, {str = name, color = nameColor})
            table.insert(ary, {str = this.html2Normal(last), color = contentColor})
        else
            table.insert(ary, {str = this.html2Normal(htmlStr), color = nameColor})
        end
        return ary
    end, 

    -- 创建由多个label拼接的有颜色的层容器 (字体尺寸 最大宽度 htmlSplitByName返回参)
    createColorPanel = function(fontSize, maxW, aryInfo, customOffH)
        local layer = display.newLayer()
        local lastDic = aryInfo[#aryInfo]
        local lineMax = lastDic.line
        local offH = customOffH or 0
        local curX = 0
        local curL = 1
        layer:setAnchorPoint(cc.p(0, 0))
        layer:setContentSize(cc.size(maxW, (fontSize + offH) * lineMax - offH))
        for i, v in ipairs(aryInfo) do
            local labl = ccui.Text:create(v.str, "", fontSize)
            local size = labl:getContentSize()
            local nowX = v.line == curL and curX or 0
            labl:setColor(v.color)
            labl:setAnchorPoint(cc.p(0, 0))
            labl:setPosition(nowX, (size.height + offH) * (lineMax - v.line))
            layer:addChild(labl)
            curX = nowX + size.width
            curL = v.line
        end
        return layer
    end, 
    -- 判断小时分钟数在不在某一时段中
    isTimeInArea1 = function(this, infoS, infoE, cur)
        local date = os.date("*t", cur)
        local curH = date.hour
        local curM = date.min
        return this:isTimeInArea2(infoS, infoE, curH, curM)
    end, 
    -- 判断小时分钟数在不在某一时段中
    isTimeInArea2 = function(this, infoS, infoE, curH, curM)
        return this.compareHourAndMin(curH, curM, infoS.h, infoS.m) and this.compareHourAndMin(infoE.h, infoE.m, curH, curM)
    end, 
    -- 时间比较:1是否大于等于2
    compareHourAndMin = function(h1, m1, h2, m2)
        if not h1 then h1 = 0 end
        if not h2 then h2 = 0 end
        if not m1 then m1 = 0 end
        if not m2 then m2 = 0 end
        if h1 > h2 then
            return true
        elseif h1 == h2 then
            return m1 >= m2
        else
            return false
        end
    end, 
    -- 自适应内部文本宽度并让文本居中
    adaptInnerLabelWidth = function(node, label, offW)
        local width = label:getContentSize().width + offW
        node:setContentSize(width, node:getContentSize().height)
        label:setPositionX(width * 0.5)
    end, 
    -- 与美术协同工作，达成流光效果(如此一来，程序只需加一行代码，光的移动快慢时机等美术就可以制作了)
    addClippingLight = function(this, node)
        -- 约定此node内有两个sprite，名字必须是mask、light
        -- mask的坐标必须是(0,0)
        -- 会把light从node上移除，再放在裁剪节点上，所以裁剪节点的坐标必须也是(0,0)
        local mask = assert(node:getChildByName("mask"))
        local light = assert(node:getChildByName("light"))
        local cn = cc.ClippingNode:create(display.newSprite(mask:getSpriteFrame()))
        cn:setAlphaThreshold(0.9)
        light:retain() -- 在remove前，需要retain，否则在计时器中使用会报错
        light:removeFromParent()
        cn:addChild(light)
        light:release() -- 添加到父节点后，需要release，否则会导致内存泄露
        mask:hide()
        node:addChild(cn)
    end, 

    blurScreen = function (callb)
        local fileName = "printScreen.png" 
        -- 移除纹理缓存  
        display.removeImage(cc.FileUtils:getInstance():getWritablePath() ..fileName) 
        -- 截屏  
        cc.utils:captureScreen(function(succeed, outputFile) 
            if succeed then 
                if callb then
                    callb(outputFile)
                end
            else
                callb(nil) 
                print("截屏失败") 
            end 
        end, fileName)
    end, 
    gaosiblur = function (sp)
        -- body
        local vertSource = "\n" .. 
        "attribute vec4 a_position; \n" .. 
        "attribute vec2 a_texCoord; \n" .. 
        "attribute vec4 a_color; \n" .. 
        "#ifdef GL_ES \n" .. 
        "varying lowp vec4 v_fragmentColor;\n" .. 
        "varying mediump vec2 v_texCoord;\n" .. 
        "#else \n" .. 
        "varying vec4 v_fragmentColor;\n" .. 
        "varying vec2 v_texCoord;\n" .. 
        "#endif\n" .. 
        "void main()\n" .. 
        "{\n" .. 
        " gl_Position = CC_PMatrix * a_position;\n" .. 
        " v_fragmentColor = a_color;\n" .. 
        " v_texCoord = a_texCoord;\n" .. 
        "}\n"

        local fragSource = "\n" .. 
        "#ifdef GL_ES \n" .. 
        "precision mediump float; \n" .. 
        "#endif \n" .. 
        "varying vec4 v_fragmentColor; \n" .. 
        "varying vec2 v_texCoord; \n" .. 
        "uniform vec2 resolution; \n" .. 
        "uniform float blurRadius;\n" .. 
        "uniform float sampleNum; \n" .. 
        "vec4 blur(vec2);\n" .. 
        "\n" .. 

        "void main(void)\n" .. 
        "{\n" .. 
        "    vec4 col = blur(v_texCoord); //* v_fragmentColor.rgb;\n" .. 
        "    gl_FragColor = vec4(col) * v_fragmentColor;\n" .. 
        "}\n" .. 
        "\n" .. 

        "vec4 blur(vec2 p)\n" .. 
        "{\n" .. 
        "    if (blurRadius > 0.0 && sampleNum > 1.0)\n" .. 
        "    {\n" .. 
        "        vec4 col = vec4(0);\n" .. 
        "        vec2 unit = 1.0 / resolution.xy;\n" .. 
        " \n" .. 
        "        float r = blurRadius;\n" .. 
        "        float sampleStep = r / sampleNum;\n" .. 
        "\n" .. 
        "        float count = 0.0;\n" .. 
        "\n" .. 
        "        for(float x = -r; x < r; x += sampleStep)\n" .. 
        "        {\n" .. 
        "            for(float y = -r; y < r; y += sampleStep)\n" .. 
        "            {\n" .. 
        "                float weight = (r - abs(x)) * (r - abs(y));\n" .. 
        "                col += texture2D(CC_Texture0, p + vec2(x * unit.x, y * unit.y)) * weight;\n" .. 
        "                count += weight;\n" .. 
        "            }\n" .. 
        "        }\n" .. 
        "\n" .. 
        "        return col / count;\n" .. 
        "    }\n" .. 
        "\n" .. 
        "    return texture2D(CC_Texture0, p);\n" .. 
        "}\n"
        local function setShader(spr)
            local maskOpacity = 0.1
            local pProgram = cc.GLProgram:createWithByteArrays(vertSource, fragSource)
            local glprogramstate = cc.GLProgramState:getOrCreateWithGLProgram(pProgram)

            local size = spr:getTexture():getContentSizeInPixels()
            spr:setGLProgramState(glprogramstate)
            glprogramstate:setUniformVec2("resolution", cc.p(size.width, size.height))
            glprogramstate:setUniformFloat("blurRadius", 10)
            glprogramstate:setUniformFloat("sampleNum", 6)
            -- glprogramstate:setUniformVec2("pix_size", cc.p(0,0))
        end
        return setShader(sp)
    end, 
    jumpToShop = function (node, func)
        -- body
        if KAKA_QUICKBUY_ENABLE then
            func()
        else
            local DTJumpFunc = require('app.games.dt.hall.DTJumpFunc')
            if node then
                DTJumpFunc.onShop(node, 1)
            else
                local app = cc.loaded_packages.App
                DTJumpFunc.onShop(app, 1)
                -- local app = cc.loaded_packages.App
                -- if display.getRunningScene():getChildByName("DTShopView") == nil then
                --     local view = app:createLayer("app.games.dt.hall.ZCShopView")
                --     view:setName('DTShopView')
                --     display.getRunningScene():addChild(view)
                -- end
            end
        end
    end,
    -- 输入框自动上移/还原
    inputUpDown = function(inputObj, rootObj, offY)
        local function setRootMove(isUp)
            if not rootObj.inputUD_bornY then
                rootObj.inputUD_bornY = rootObj:getPositionY()
            end
            rootObj:setPositionY(rootObj.inputUD_bornY+(isUp and offY or 0))
        end
        local function clickFun(sender, eventType)
            if eventType == ccui.TextFiledEventType.detach_with_ime then
                setRootMove()
            elseif eventType == ccui.TextFiledEventType.attach_with_ime then
                setRootMove(true)
            end
        end
        inputObj:addEventListener(clickFun)
    end,
    -- 获取网络文件流
    getNetFileStream = function (url, callback)
        local xhr = cc.XMLHttpRequest:new()
        xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
        xhr:open("GET", url)

        local function onReadyStateChange(...)
            print('onReadyStateChange', ...)
            callback(xhr.status, xhr.responseText)
        end
        xhr:registerScriptHandler(onReadyStateChange)
        xhr:send()
    end,
    -- 解析已加密文件流
    decodeFileStream = function (stream)
        local key = stream:sub(5, 20) .. stream:sub(25, 40)
        local buf = stream:sub(1, 4) .. stream:sub(21, 24) .. stream:sub(41, #stream)
        local data = MD5.exor(buf, key)
        return data
    end,
    -- 加密文件流
    encodeFileStream = function (str)
        local time = tostring(os.time())
        local key = string.upper(urlDecodeMD5(time))
        local buf = MD5.exor(str, key)
        local data = buf:sub(1,4) .. key:sub(1, 16) .. buf:sub(5, 8) .. key:sub(17, #key) .. buf:sub(9, #buf)
        return data
    end,
    -- 获取登录服列表
    getLoginList = function ( this, url, callback )
        this.getNetFileStream(url, function ( state, stream )
            if state == 200 then
                local data = this.decodeFileStream(stream)
                local listFunc = loadstring(data)
                pcall(function ( ... )
                    local list = listFunc()
                    callback(list)
                end)
            end
        end)
    end,
    -- 获取网关服务器地址
    getGateIp = function ( this, url, callback )
        local xhr = cc.XMLHttpRequest:new()
        xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
        xhr.timeout = 10
        xhr:open("GET", url)

        local function onReadyStateChange(...)
            local buf = MD5.exor(xhr.responseText, "gate")
            -- callback(xhr.status, buf)
            local recvMsg = {xhr.status,buf}
            dispatchEvent('recvGetGateIp', recvMsg)
        end
        xhr:registerScriptHandler(onReadyStateChange)
        xhr:send()
    end,
    -- 获取UUID
    getUUID = function ( ... )
        local bytes = {
            [1] = math_random(1, 9),
            [2] = math_random(0, 9),
            [3] = math_random(0, 9),
            [4] = math_random(0, 9),
            [5] = math_random(0, 9),
            [6] = math_random(0, 9),
            [7] = math_random(0, 9),
            [8] = math_random(0, 9),
        }
        return tonumber(table.concat(bytes, ""))
    end,
    -- 把内存中的图保存下来，并返回路径
    movePictocache = function (pathpic)
        local iconPath = cc.FileUtils:getInstance():fullPathForFilename(pathpic)
        if string.find(iconPath, cc.FileUtils:getInstance():getWritablePath()) then
        else
            local image = cc.Image:new()
            image:initWithImageFile(iconPath)
            local picsavepath = cc.FileUtils:getInstance():getWritablePath().."res/"..pathpic
            local ts = string.reverse(picsavepath)
            local pos = string.find(ts,"/")
            pos = string.len(ts) - pos
            local directorypath = string.sub(picsavepath,1,pos)

            if cc.FileUtils:getInstance():isDirectoryExist(directorypath) then
            else
                cc.FileUtils:getInstance():createDirectory(directorypath)
            end
            if image:saveToFile(picsavepath, true) then
                print("end yes")
                iconPath = picsavepath
            else
                print("end no")

            end
        end
        return iconPath
    end,


}
