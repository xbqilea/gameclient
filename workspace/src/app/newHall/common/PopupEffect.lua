-- PopupEffect.lua
-- 对话框弹出效果
-- Create by @2017-7-3

local POP_T = 0.1
local MASK_FADE_T = 0.1
local TAG_MASK = 111

local CommonHelper = import('.CommHelper')
local PopupEffect = class("PopupEffect")
local __ = {}

-- 设置执行动画的参数
-- uiSelf UI类的实例填self, bgNode执行弹出效果的节点, maskLayout半透明层(类型必须是:ccui.Layout)
function PopupEffect.init(uiSelf, bgNode, maskLayout, bolSet, aniEndCallback)
    aniEndCallback = aniEndCallback or function() end
    local action = cc.Sequence:create(
        cc.Spawn:create(cc.EaseBackOut:create(cc.ScaleTo:create(POP_T, 1.06)), cc.FadeIn:create(POP_T)),
        cc.ScaleTo:create(0.1, 1),
        cc.CallFunc:create(aniEndCallback)
    )

    -- if bolSet then
    --     __:setAllCascadeOpacityEnabled(bgNode)
    -- end
    bgNode:setOpacity(60)
    bgNode:setScale(0.8)
    bgNode:runAction(cc.EaseSineIn:create(action))
    
    -- 如果已经有半透明层，去掉背景颜色，统一使用下面的颜色。
    if maskLayout then
        -- 上面可能有阻挡事件，故不能隐藏 改变背景填充类型即可
        maskLayout:setBackGroundColorType(LAYOUT_COLOR_NONE)
    end

    -- 颜色从淡变深
    local maskLayer = cc.LayerColor:create(cc.c4b(0, 0, 0, 0))
    maskLayer:setContentSize(cc.Director:getInstance():getRunningScene():getContentSize())
    maskLayer:runAction(cc.FadeTo:create(MASK_FADE_T, 190))
    -- maskLayer:setScale(2)
    maskLayer:setTag(TAG_MASK)
    uiSelf:removeChildByTag(TAG_MASK)
    uiSelf:addChild(maskLayer, -1)

    -- 增加关闭动画
    function close() 
        -- local action = cc.Sequence:create(
        --     cc.DelayTime:create(0.04),
        --     cc.EaseBackIn:create(cc.ScaleTo:create(POP_T, 0.6)),
        --     cc.CallFunc:create(function()
                uiSelf:removeSelf()
        --     end)
        -- )
        -- bgNode:runAction(action)
        -- maskLayer:runAction(cc.FadeTo:create(POP_T*1.1, 0))
    end

    uiSelf.onClose = close
    -- uiSelf.onBack = close
end
--模糊背景
function PopupEffect:initBlur( uiSelf, bgNode, maskLayout, bolSet, aniEndCallback)
    self.init(uiSelf, bgNode, maskLayout, bolSet, aniEndCallback)
    -- local  path = "dt/image/common/tjylc_tanchu_bg.jpg"
    -- local sprite_photo = cc.Sprite:create(path)
    -- :align(display.CENTER, display.cx, display.cy)
    -- :addTo(uiSelf,-2)
    -- sprite_photo:setScaleX(display.width/sprite_photo:getContentSize().width)
    -- sprite_photo:setScaleY(display.height/sprite_photo:getContentSize().height)
end

-- -- 让所有的节点都允许子节点的透明度岁父节点变化
-- function __:setAllCascadeOpacityEnabled(root)
--     local ary = root:getChildren()
--     for i,v in ipairs(ary) do
--         v:setCascadeOpacityEnabled(true)
--         self:setAllCascadeOpacityEnabled(v)
--     end
-- end

return PopupEffect