-- WebPay.lua
-- 网页支付（微信、支付宝等）
-- Created by @2017-11-23

local TAG_WEBVIEW = 1294
 
local CustomerWeb = class('CustomerWeb')
local HNLayer = import("..HNLayer")
local CustomerServerWebView = class("CustomerServerWebView",function () 
return HNLayer.new()

end)
CustomerServerWebView.RESOURCE_FILENAME = "dt/empty.csb"

-- 初始化UI
function CustomerServerWebView:initUI( ... )
    self.root = UIAdapter:createNode(CustomerServerWebView.RESOURCE_FILENAME) 
    self:addChild(self.root)

    -- addNodeListener(self, "event_closeCustomerServerWebView", handler(self, self.close))
    -- addNodeListener(self, "event_webViewJsCallback", handler(self, self.onWebViewJsCallback))
end

-- 初始化
function CustomerServerWebView:init(url)
    local winsize = cc.Director:getInstance():getWinSize()
    -- self:setContentSize(winsize)
    -- self:runAction(cc.FadeTo:create(0.3, 185))

    local block = ccui.Button:create()
    block:setFocused(true)
    block:ignoreContentAdaptWithSize(false)
    block:setContentSize(winsize)
    block:setAnchorPoint(cc.p(0.5,0.5))
    block:setPosition(cc.p(winsize.width / 2, winsize.height / 2))
    self:addChild(block)
    self:createWebView(url)

    block:addTouchEventListener(function()
        self:close()
    end)
end

--返回
function CustomerServerWebView:onBack( ... )
    self:close()
end


-- 网页层
function CustomerServerWebView:createWebView(url)
    if ccexp.WebView == nil then
        print('warning: do not support webview!')
        return
    end

    local winSize = cc.Director:getInstance():getVisibleSize()
    webView = ccexp.WebView:create()
    webView:setPosition(winSize.width / 2, winSize.height / 2)
    local webSize = cc.size(winSize.width * 0.75,  winSize.height * 0.9)
    webView:setContentSize(webSize)
    webView:loadURL(url)
    webView:setScalesPageToFit(true)
    webView:setTag(TAG_WEBVIEW)
    webView:setJavascriptInterfaceScheme("lua")
    -- webView:setOnJSCallback(function(_, str)
    --     print('lua js callback:', str)
    --     if str == 'lua://close_webview' then
    --         self:close()
    --     end
    -- end)

    -- 以下代码试了，层级始终不对
    -- local webCloseBtn = ccui.Button:create("common/ui/close_big.png")
    -- webCloseBtn:setPosition(webSize.width, webSize.height)
    -- webCloseBtn:setLocalZOrder(1)
    -- webView:addChild(webCloseBtn)
    -- webCloseBtn:addClickEventListener(function()
    --     self:close()
    -- end)

    webView:setOnShouldStartLoading(function(sender, url)
        print("onWebViewShouldStartLoading, url is ", url)
        return true
    end)
    webView:setOnDidFinishLoading(function(sender, url)
        print("onWebViewDidFinishLoading, url is ", url)
    end)
    webView:setOnDidFailLoading(function(sender, url)
        print("onWebViewDidFinishLoading, url is ", url)
    end)
    self:addChild(webView)
end

-- 关闭界面 
function CustomerServerWebView:close()
    self:runAction(cc.RemoveSelf:create())
end

-- js事件
function CustomerServerWebView:onWebViewJsCallback(event)
    print('onWebViewJsCallback', event._userdata)

    -- 可以从网页传些数据来
    -- 暂时没啥用    
end

-- 打开网页支付窗口
function CustomerWeb.showURL(webPayUrl)
    -- local webPayUrl = string.format(WEB_PAY_URL, 
    --     order,
    --     KAKA_SWITCH_ENABLE and 0.01 or price,
    --     goodsInfo.name,
    --     goodsInfo.propNum
    -- )

    print('webpay url:', webPayUrl)
    local view = CustomerServerWebView.new()
    view:init(webPayUrl)
    return view 
end


return CustomerWeb