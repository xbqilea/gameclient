--AdapterUtil.lua
--分辨率适配工具
--Create by . 2017-05-05

--[[
分辨率适配说明：
分辨率适配使用两种模式：1、自适应模式，2、绝对坐标模式；
其中绝对坐标模式采用引擎的SHOW_ALL模式，不做重点讨论；
自适应模式严格的来说是局部自适应，也就是说在全屏Scene和Layer中使用相对坐标，在非全屏Layer或其他布局容器中采用绝对坐标模式；
因此，在自适应模式下，有以下注意事项：
1、全屏 布局坐标使用百分比（即相对坐标）
	（1）若界面是使用代码编写，坐标采用display参考系（即displa.cx ...）；
	（2）若界面是使用cocostudio编辑，坐标采用相对坐标或者使用编辑器的固定与拉伸选项；
	（3）在工程中使用的时候，需要用AdapterUtil中对应的方法进行重新布局，否则，展示的位置不对。
		注：UI编辑器的传参为CSLoader创建出来的节点，代码编写的界面即为根节点。
2、非全屏布局控件（弹窗）
	（1）内部采用绝对坐标，参考系为设计分辨率；
	（2）被添加到界面或场景上时，在父节点上的坐标使用display系列坐标。
3、若坐标参考系为设计分辨率，则需使用AdapterUtil.Pos（在同一个node上只能使用一次）来进行坐标转换。
	略麻烦，因此，建议在编写过程中，尽量使用display参考系坐标。
4、此模式兼容绝对坐标模式。
5、建议界面尽量使用编辑器编辑，可视化，且开发效率高。
--]]
CC_DESIGN_RESOLUTION = {
    width = 1280,
    height = 720,
    autoscale = "FIXED_HEIGHT",
    callback = function(framesize)
        print("framesize", framesize.width, framesize.height, framesize.width / framesize.height)
        local ratio = framesize.width / framesize.height
        print("ratio", ratio)
        if ratio > 2 then
            IPHONE_X = true
        end
        local design = CC_DESIGN_RESOLUTION.width / CC_DESIGN_RESOLUTION.height
        if KAKA_ADAPTER_MODE == ADAPTER_MODE.ABSOLUTE then
            return {autoscale = "SHOW_ALL"}
        end
        if ratio < design then
            -- iPad 768*1024(1536*2048) is 4:3 screen
            return {autoscale = "FIXED_WIDTH"}
        else
            return {autoscale = "FIXED_HEIGHT"}
        end
    end
}
AdapterUtil = {}
--Scene适配
function AdapterUtil.Scene(node) 
		node:setContentSize(display.size)
		ccui.Helper:doLayout(node) 
end
--Layer适配
function AdapterUtil.Layer(node)
	 
		local scale_x = display.width/CC_DESIGN_RESOLUTION.width
		local scale_y = display.height/CC_DESIGN_RESOLUTION.height
		local size = node:getContentSize()
		local newSize = cc.size(size.width*scale_x, size.height*scale_y)
		node:setContentSize(newSize)
		ccui.Helper:doLayout(node)
	 
end
--坐标适配
function AdapterUtil.Pos(node)
 
		local diff_x = display.cx-CC_DESIGN_RESOLUTION.width/2
		local diff_y = display.cy-CC_DESIGN_RESOLUTION.height/2
		local pos = cc.p(node:getPosition())
		node:setPosition(cc.p(pos.x+diff_x, pos.y+diff_y))
 
end