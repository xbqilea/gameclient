--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
local scheduler =require("framework.scheduler")
local UIHelper = require("src.app.hall.base.util.UIHelper")
local HNLayer = class("HNLayer",function()
    return display.newLayer() 

end)

ACTION_TYPE_LAYER ={
		
			SCALE = 0,		-- 缩放 动作
			FADE =1,			-- 渐隐/渐现 动作
			TOP=2,			-- 顶部掉下/收回 动作
			LEFT=3,			-- 左边滑入 动作
			RIGHT=4,			-- 右边滑入 动作
                            
			NONE=5			-- 无动作直接显示
		};
function HNLayer:ctor() 
   self:myInit()
   self:setupViews()
end

function HNLayer:myInit()
    self._parent = nil
    self._colorLayer = nil;
    self._winSize = nil;
    self._releaseSchedule=nil
    self.diffY = (display.size.height - 750) / 2
    self.diffX = 145-(1624-display.size.width)/2 
end

function HNLayer:setupViews()
    self:setCascadeOpacityEnabled(true); 
	self._winSize = cc.Director:getInstance():getWinSize() 
end 
function HNLayer:setBackGroundImage(name)
	local visibleSize =  cc.Director:getInstance():getVisibleSize();
	--背景
	local loadingBG = display.newSprite(name)
	loadingBG:setPosition(visibleSize.width / 2, visibleSize.height / 2);
	local _xScale = visibleSize.width / loadingBG:getContentSize().width;
	local _yScale = visibleSize.height / loadingBG:getContentSize().height;
	loadingBG:setScaleX(_xScale);
	loadingBG:setScaleY(_yScale);
	loadingBG:setName("BackGround");
	self:addChild(loadingBG, -1);
end

function HNLayer:modifyBackGroundImage(name) 
	local sp = self:getChildByName("BackGround");
	if sp ~= nil then 
		sp:setTexture(name);
    end
end

function HNLayer:getRealScaleX(designWidth)
	local visibleSize = cc.Director:getInstance():getVisibleSize();
	return visibleSize.width / designWidth;
end

function HNLayer:getRealScaleY(designHeight) 
	local visibleSize = cc.Director:getInstance():getVisibleSize();
	return visibleSize.height / designHeight;
end

function HNLayer:enableKeyboard()
	--对手机返回键的监听
    local listener = cc.EventListenerKeyboard:create()  
    listener:registerScriptHandler(onKeyReleased, cc.Handler.EVENT_KEYBOARD_RELEASED ) 
    cc.Director:getInstance():getEventDispatcher():addEventListenerWithSceneGraphPriority(listener, self) 
end

function HNLayer:switchToLayoutControl(root,controlName, newControl)
	local tmp = ccui.Helper:seekWidgetByName(root, controlName);
	if nil == tmp then
         return false;
    end
	newControl:setPosition(tmp:getPosition());
	newControl:setTag(tmp:getTag());
	newControl:setLocalZOrder(tmp:getLocalZOrder());
	newControl:setScaleX(tmp:getScaleX());
	newControl:setScaleY(tmp:getScaleY());
	newControl:setAnchorPoint(tmp:getAnchorPoint());
	root:addChild(newControl);
	return true;
end

function HNLayer:startShade(parent,zOrder, opacity,touchCallback)
	
	if (nil == self._colorLayer) then
		
		self._colorLayer = ccui.Layout:create();
		self._colorLayer:setBackGroundColorType(1);
		self._colorLayer:setBackGroundColor(cc.c3b(0,0,0));
		self._colorLayer:setContentSize(self._winSize);
		self._colorLayer:setOpacity(0);
		self._colorLayer:setTouchEnabled(true);

		parent:addChild(self._colorLayer, zOrder - 1);

		self._colorLayer:runAction(cc.FadeTo:create(0.2, opacity));

		if (touchCallback) then
			
			self._colorLayer:addTouchEventListener(touchCallback);
		end
	end
end

function HNLayer:quicklyShade( parent,  zOrder,  opacity,  touchCallback)
	
	if (nil == self._colorLayer) then
		
		self._colorLayer = ccui.Layout:create();
		self._colorLayer:setBackGroundColorType(1);
		self._colorLayer:setBackGroundColor(0,0,0);
		self._colorLayer:setContentSize(self._winSize);
		self._colorLayer:setOpacity(0);
		self._colorLayer:setTouchEnabled(true);

		parent:addChild(self._colorLayer, zOrder - 1);

		if (touchCallback) then
			
			self._colorLayer:addClickEventListener(touchCallback);
		end
	end
end

-- 移除遮挡层
function HNLayer:removeShade( action)
	
	if (self._colorLayer) then
		
		if (action) then
			
			self._colorLayer:runAction(cc.Sequence:create(cc.FadeOut:create(0.2), cc.RemoveSelf:create(true), nil));
			 
		else
			
			self._colorLayer:removeFromParent();
		end
		self._colorLayer = nil;
	end
end
    
function HNLayer:releaseUnusedRes()
    if self._releaseSchedule==nil then
        self._schedule =scheduler.scheduleGlobal(handler(self,self.releaseGameRes),0);
    end
    --scheduleOnce(schedule_selector(HNLayer:releaseGameRes), 0.f);
end
    
    
function HNLayer:releaseGameRes( delta)
    
    cc.AnimationCache:destroyInstance();
    cc.SpriteFrameCache:getInstance():removeUnusedSpriteFrames();
    cc.Director:getInstance():getTextureCache():removeUnusedTextures();
    scheduler.unscheduleGlobal(self._releaseSchedule)
    self._releaseSchedule=nil
end

function HNLayer:open( ntype, parent, pos, zOrder, tag,showShade,  touchCallback)
	self._parent = parent
	self._winSize = cc.Director:getInstance():getWinSize()
	if (showShade) then
        self:startShade(parent, zOrder, 150, touchCallback);
    end
	local action = nil; 
	if ntype == ACTION_TYPE_LAYER.SCALE then
		
		self:setPosition(pos);
		self:setScale(0.1);
		self:runAction(cc.ScaleTo:create(0.2, 1.0));
		 
	elseif ntype == ACTION_TYPE_LAYER.FADE then
		
		self:setPosition(pos);
		self:setOpacity(0);
		self:runAction(cc.FadeIn:create(0.2));
		 
	elseif ntype == ACTION_TYPE_LAYER.TOP then
		
		self:setPosition(cc.p(pos.x, pos.y + self._winSize.height));
		self:runAction(cc.MoveBy:create(0.2, cc.p(0, -self._winSize.height))); 
	elseif ntype == ACTION_TYPE_LAYER.LEFT then
		
		self:setPosition(cc.p(pos.x - self._winSize.width, pos.y));
		self:runAction(cc.MoveBy:create(0.2, cc.p(self._winSize.width, 0))); 
	elseif ntype == ACTION_TYPE_LAYER.RIGHT then
		
		self:setPosition(cc.p(pos.x + self._winSize.width, pos.y));
		self:runAction(cc.MoveBy:create(0.2, cc.p(-self._winSize.width, 0)));
		 
	elseif ntype == ACTION_TYPE_LAYER.NONE then
		 
	end

	parent:addChild(self, zOrder, tag);
end

function HNLayer:close( ntype)
	
	g_AudioPlayer:playEffect(GAME_SOUND_CLOSE);

	self:removeShade(ntype);

	local action = nil;
     
		
	if ntype == ACTION_TYPE_LAYER.SCALE then
		action = cc.Sequence:create(cc.ScaleTo:create(0.1, 0.1), cc.CallFunc:create( handler(self,self.closeFunc)), cc.RemoveSelf:create(true), nil);
			 
	elseif ntype == ACTION_TYPE_LAYER.FADE then
		action = cc.Sequence:create(cc.FadeOut:create(0.2),cc.CallFunc:create( handler(self,self.closeFunc)), cc.RemoveSelf:create(true), nil);
			
	elseif ntype == ACTION_TYPE_LAYER.TOP then
		action = cc.Sequence:create(cc.MoveBy:create(0.2, cc.p(0, self._winSize.height)),cc.CallFunc:create(handler(self,self.closeFunc)), cc.RemoveSelf:create(true), nil);
			
	elseif ntype == ACTION_TYPE_LAYER.LEFT then
		action = cc.Sequence:create(cc.MoveBy:create(0.2, cc.p(-self._winSize.width, 0)), cc.CallFunc:create(handler(self,self.closeFunc)), cc.RemoveSelf:create(true), nil);
			
	elseif ntype == ACTION_TYPE_LAYER.RIGHT then
		action = cc.Sequence:create(cc.MoveBy:create(0.2, cc.p(self._winSize.width, 0)), cc.CallFunc:create(handler(self,self.closeFunc)), cc.RemoveSelf:create(true), nil);
			
	elseif ntype == ACTION_TYPE_LAYER.NONE then
	else
		action = cc.Sequence:create(cc.CallFunc:create( handler(self,self.closeFunc)), cc.RemoveSelf:create(true), nil);
			
	end

	self:runAction(action);
end

function HNLayer:removeFromParent()
	
	self:removeShade();

	cc.Layer:removeFromParent();
end

function HNLayer:closeFunc()
	
	 
end 

return HNLayer