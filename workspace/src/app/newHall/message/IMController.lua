--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- To change this template use File | Settings | File Templates.
-- 银行控制类
local scheduler           = require("framework.scheduler") 
local ChatMgr = require("src.app.newHall.chat.ChatMgr")
local IMController = class("IMController")

function IMController:ctor()
    self.m_groupName = ""
    self:myInit()
end

function IMController:myInit()
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_ChatRoom_Login_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_ChatRoom_Login_Nty", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_ChatRoom_Logout_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_ChatRoom_Logout_Nty", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_ChatRoom_SendMessage_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_ChatRoom_SendMessage_Nty", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_CMC_IM_Heartbeat_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_IM_InvalidTag_Nty", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_IM_Router_Nty", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_IM_kick_Nty", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_IM_disconnecting_Nty", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_Friend_AddFriend_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_Friend_AddFriend_Nty", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_Friend_AddFriend_Final_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_Friend_AddFriend_Final_Nty", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_Friend_DelOneFriend_Ack", handler(self, self.netMsgHandler)) 
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_Friend_GetAllFriendDetails_Ack", handler(self, self.netMsgHandler)) 
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_Friend_SendMessage_Nty", handler(self, self.netMsgHandler)) 
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_C2M_Friend_SendMessage_Ack", handler(self, self.netMsgHandler)) 

    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_Group_CreateGroup_Ack", handler(self, self.netMsgHandler)) 
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_Group_AddFriendToGroup_Ack", handler(self, self.netMsgHandler)) 
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_Group_AddFriendToGroup_1_Nty", handler(self, self.netMsgHandler)) 
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_Group_AddFriendToGroup_2_Nty", handler(self, self.netMsgHandler)) 
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_Group_ExitGroup_Ack", handler(self, self.netMsgHandler)) 
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_Group_ExitGroup_Nty", handler(self, self.netMsgHandler)) 
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_Group_ReadGroup_Ack", handler(self, self.netMsgHandler)) 
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_Group_ReadAllGroup_Ack", handler(self, self.netMsgHandler)) 
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_Group_DelMemberFromGroup_Ack", handler(self, self.netMsgHandler)) 
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_Group_DelMemberFromGroup_1_Nty", handler(self, self.netMsgHandler)) 
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_Group_DelMemberFromGroup_2_Nty", handler(self, self.netMsgHandler)) 
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_Group_SendMessage_Ack", handler(self, self.netMsgHandler)) 
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_Group_SendMessage_Nty", handler(self, self.netMsgHandler))  
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_Group_DeleteGroup_Ack", handler(self, self.netMsgHandler))  
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_Group_DeleteGroup_Nty", handler(self, self.netMsgHandler))    
    TotalController:registerNetMsgCallback(self, Protocol.ChatServer, "CS_M2C_ChatRoom_IsEnableMessage_Nty", handler(self, self.netMsgHandler))    
     
end 
 
 function IMController:CS_C2M_Group_SendMessage_Req(groupId,sessionID,msg, msgType)   
    ConnectManager:send2Server(Protocol.ChatServer, "CS_C2M_Group_SendMessage_Req", {groupId,sessionID,msg, msgType})
end 
 function IMController:Group_DeleteGroup_Req(groupId)   
    ConnectManager:send2Server(Protocol.ChatServer, "CS_C2M_Group_DeleteGroup_Req", {groupId})
end 

 function IMController:Group_CreateGroup_Req(groupName)  
    self.m_groupName = groupName
    ConnectManager:send2Server(Protocol.ChatServer, "CS_C2M_Group_CreateGroup_Req", {0,groupName,""})
end 
 function IMController:Group_AddFriendToGroup_Req(groupId,sessionID,group)  
    ConnectManager:send2Server(Protocol.ChatServer, "CS_C2M_Group_AddFriendToGroup_Req", {groupId,sessionID,group})
end 
 function IMController:Group_ExitGroup_Req(groupId)  
    ConnectManager:send2Server(Protocol.ChatServer, "CS_C2M_Group_ExitGroup_Req", {groupId})
end 
 function IMController:Group_ReadGroup_Req(groupId)  
    ConnectManager:send2Server(Protocol.ChatServer, "CS_C2M_Group_ReadGroup_Req", {groupId})
end 
 function IMController:Group_ReadAllGroup_Req()  
    ConnectManager:send2Server(Protocol.ChatServer, "CS_C2M_Group_ReadAllGroup_Req", {})
end 
 function IMController:Group_DelMemberFromGroup_Req(groupId,sessionID,group)  
    ConnectManager:send2Server(Protocol.ChatServer, "CS_C2M_Group_DelMemberFromGroup_Req", {groupId,sessionID,group})
end  

 function IMController:Friend_SendMessage_Req(userId,msg,sessionId, msgType)  
    ConnectManager:send2Server(Protocol.ChatServer, "CS_C2M_Friend_SendMessage_Req", {userId,msg,sessionId, msgType})
end 
function IMController:Friend_GetAllFriendDetails_Req()  
    ConnectManager:send2Server(Protocol.ChatServer, "CS_C2M_Friend_GetAllFriendDetails_Req", {})
end 
  function IMController:AddFriend_Final_Req(userId,bAgree) 
    local encryptData = ChatMgr:getInstance():getApplyFriendEncryptData(userId)
  
    ConnectManager:send2Server(Protocol.ChatServer, "CS_C2M_Friend_AddFriend_Final_Req", {userId,0,encryptData,bAgree,0})
end 

  function IMController:Friend_DelOneFriend_Req(userId) 
    ConnectManager:send2Server(Protocol.ChatServer, "CS_C2M_Friend_DelOneFriend_Req", {userId})
end
 function IMController:Friend_AddFriend_Req(userId) 
    ConnectManager:send2Server(Protocol.ChatServer, "CS_C2M_Friend_AddFriend_Req", {userId,0})
end
function IMController:ChatRoom_Login_Req(userId,roomId) 
    ChatMgr:getInstance():setMyId(userId) 
    
    ConnectManager:send2Server(Protocol.ChatServer, "CS_C2M_ChatRoom_Login_Req", {userId,roomId,"1"})
end
 
function IMController:ChatRoom_Logout_Req(RoomID) 
    ConnectManager:send2Server(Protocol.ChatServer, "CS_C2M_ChatRoom_Logout_Req", {RoomID})
end
 
function IMController:ChatRoom_SendMessage_Req(RoomID, SessionID,message, msgType) 
    ConnectManager:send2Server(Protocol.ChatServer, "CS_C2M_ChatRoom_SendMessage_Req", {RoomID, SessionID, message, msgType})
end
-- 大厅心跳包
function IMController:beginToSendHallPing(time)
	self.imOfflineCount = 0
	--每过GlobalDefine.HALL_PING_CD秒发一次心跳包
	if g_hall_im_schedule then
		scheduler.unscheduleGlobal(g_hall_im_schedule)
	end
	g_hall_im_schedule = scheduler.scheduleGlobal(handler(self, self.sendPing),time)
end

function IMController:stopToSendHallPing() -- 断开心跳
    print("IMController断开心跳")
	if g_hall_im_schedule then
		scheduler.unscheduleGlobal(g_hall_im_schedule)
		g_hall_im_schedule = nil
	end
end

--发往大厅的心跳包
function IMController:sendPing()
	self.imOfflineCount = self.imOfflineCount + 1
	
	--如果发送失败， 则直接走重连
	if self.imOfflineCount <= 2 then
		if ConnectManager:send2Server(Protocol.ChatServer, "CS_CMC_IM_Heartbeat_Req", {})  ~= 0 then-- 心跳
			self.imOfflineCount = 3
		end
	end
	
	print("PingHall:",self.imOfflineCount)
	if self.imOfflineCount > 2 then --离线心跳包次数
		print("ping timeout")
		ConnectManager:disConnectSvr(Protocol.ChatServer)
		--self:stopToSendHallPing()
		--g_LoginController._isReconnect = false 	
		if g_isAllowReconnect then
			ConnectManager:reconnect()
		end
	end
end
function IMController:netMsgHandler( __idStr, __info )
    if __idStr == "CS_M2C_ChatRoom_Login_Ack" then 
        if __info.m_status == 0 then
            ChatMgr:getInstance():setRoomMsgList(__info.m_RoomID,__info.m_Message)
            ChatMgr:getInstance():setRoomPlayers(__info.m_RoomID,__info.m_Member) 
            --  dump(__info)  

            self:Friend_GetAllFriendDetails_Req()
            self:Group_ReadAllGroup_Req()
            ConnectManager:send2Server(Protocol.ChatServer, "CS_CMC_IM_Heartbeat_Req", {}) 
            sendMsg(M2C_ChatRoom_Login_Ack, __info)
        else
            TOAST("聊天服务器连接异常，错误代码500")
        end
    elseif __idStr == "CS_M2C_ChatRoom_Login_Nty" then 
        ChatMgr:getInstance():addRoomPlayer(__info.m_RoomID,__info.m_Member)
        sendMsg(M2C_ChatRoom_Login_Nty, __info) 
     elseif __idStr == "CS_M2C_ChatRoom_Logout_Ack" then
         ChatMgr:getInstance():clear()
         ChatMgr:getInstance():releaseInstance() 
        sendMsg(M2C_ChatRoom_Logout_Ack, __info)
     elseif __idStr == "CS_M2C_ChatRoom_Logout_Nty" then
         ChatMgr:getInstance():removeRoomPlayer(__info.m_RoomID,__info.m_userid)
        sendMsg(M2C_ChatRoom_Logout_Nty, __info)
     elseif __idStr == "CS_M2C_ChatRoom_SendMessage_Ack" then
        dump(__info) 
        sendMsg(M2C_ChatRoom_SendMessage_Ack, __info)
     elseif __idStr == "CS_M2C_ChatRoom_SendMessage_Nty" then 
        ChatMgr:getInstance():addRoomMsg(__info.m_RoomID,__info.m_userid,__info.m_message)
        sendMsg(M2C_ChatRoom_SendMessage_Nty, __info)
     elseif __idStr == "CS_CMC_IM_Heartbeat_Ack" then 
        self:beginToSendHallPing(__info.m_interval) 
     elseif __idStr == "CS_M2C_IM_InvalidTag_Nty" then
        sendMsg(M2C_IM_InvalidTag_Nty, __info)
    elseif __idStr == "CS_M2C_IM_Router_Nty" then
        sendMsg(M2C_IM_Router_Nty, __info)
    elseif __idStr == "CS_M2C_IM_kick_Nty" then
        sendMsg(M2C_IM_kick_Nty, __info)
    elseif __idStr == "CS_M2C_IM_disconnecting_Nty" then
        sendMsg(M2C_IM_disconnecting_Nty, __info)
     elseif __idStr == "CS_M2C_IM_NoSuch_Nty" then
        sendMsg(M2C_IM_NoSuch_Nty, __info)
     elseif __idStr == "CS_M2C_Friend_AddFriend_Ack" then
        sendMsg(M2C_Friend_AddFriend_Ack, __info)
     elseif __idStr == "CS_M2C_Friend_AddFriend_Nty" then
        ChatMgr:getInstance():addApplyFriend(__info) 
        dump(__info)
        sendMsg(M2C_Friend_AddFriend_Nty, __info)
     elseif __idStr == "CS_M2C_Friend_AddFriend_Final_Ack" then
         if __info.m_result == 0 then
             ChatMgr:getInstance():addFriend1(__info.m_userid) 
             ChatMgr:getInstance():removeApplyFriend(__info.m_userid)
         elseif __info.m_result == -3 then
            TOAST(__info.m_reason)
         end
        sendMsg(M2C_Friend_AddFriend_Final_Ack, __info)
     elseif __idStr == "CS_M2C_Friend_AddFriend_Final_Nty" then
       --  ChatMgr:getInstance():removeApplyFriend(__info.m_user_info2.)
         if __info.m_result == 0 then
            ChatMgr:getInstance():addFriend(__info.m_user_info2) 
            sendMsg(M2C_Friend_AddFriend_Final_Nty, __info)
         end
        
    elseif __idStr == "CS_M2C_Friend_DelOneFriend_Ack" then
        ChatMgr:getInstance():removeFriend(__info.m_userid) 
        sendMsg(M2C_Friend_DelOneFriend_Ack, __info)
    elseif __idStr == "CS_M2C_Friend_GetAllFriendDetails_Ack" then  
        ChatMgr:getInstance():setFriendList(__info.m_AllFriend)  
        sendMsg(M2C_Friend_GetAllFriendDetails_Ack, __info)     
     elseif __idStr == "CS_M2C_Friend_SendMessage_Nty" then 
        ChatMgr:getInstance():setFriendMsgList(__info,__info.m_from_UserInfo.m_userid,false) 
        sendMsg(M2C_Friend_SendMessage_Nty, __info)     
      elseif __idStr == "CS_C2M_Friend_SendMessage_Ack" then  
        sendMsg(M2C_Friend_SendMessage_Ack, __info)     
    elseif __idStr == "CS_M2C_Group_CreateGroup_Ack" then  
        if __info.m_GroupID>0 then
            local group = {}
            group.m_GroupID = __info.m_GroupID
            group.m_userid = Player.getAccountID()
            group.m_GroupName = self.m_groupName
            ChatMgr:getInstance():addGroup(group)
            sendMsg(M2C_Group_CreateGroup_Ack, __info)     
        else
            TOAST("创建失败")
        end
    elseif __idStr == "CS_M2C_Group_AddFriendToGroup_1_Nty" then  
        ChatMgr:getInstance():addGroup(group)
        sendMsg(M2C_Group_AddFriendToGroup_1_Nty, __info)     
    elseif __idStr == "CS_M2C_Group_AddFriendToGroup_2_Nty" then  
        sendMsg(M2C_Group_AddFriendToGroup_2_Nty, __info)     
    elseif __idStr == "CS_M2C_Group_AddFriendToGroup_Ack" then  
        if __info.m_result == 0 then 
            sendMsg(M2C_Group_AddFriendToGroup_Ack, __info)  
        else
            TOAST("创建失败")
        end   
    elseif __idStr == "CS_M2C_Group_ExitGroup_Ack" then  
        if __info.m_result == 0 then
             ChatMgr:getInstance():removeGroup(__info.m_GroupID)
            sendMsg(M2C_Group_ExitGroup_Ack, __info)     
        else
            TOAST("退出失败")
        end
    elseif __idStr == "CS_M2C_Group_ExitGroup_Nty" then  
        sendMsg(M2C_Group_ExitGroup_Nty, __info)     
    elseif __idStr == "CS_M2C_Group_ReadGroup_Ack" then  
       if __info.m_result == 0 then
            dump(__info)
            sendMsg(M2C_Group_ReadGroup_Ack, __info)     
        else
            TOAST("查询失败")
        end
    elseif __idStr == "CS_M2C_Group_ReadAllGroup_Ack" then  
        if __info.m_result == 0 then
            ChatMgr:getInstance():setGroupList(__info.m_AllGroup) 
            sendMsg(M2C_Group_ReadAllGroup_Ack, __info)     
        end
    elseif __idStr == "CS_M2C_Group_DelMemberFromGroup_Ack" then  
        sendMsg(M2C_Group_DelMemberFromGroup_Ack, __info)     
    elseif __idStr == "CS_M2C_Group_DelMemberFromGroup_1_Nty" then  
        sendMsg(M2C_Group_DelMemberFromGroup_1_Nty, __info)     
    elseif __idStr == "CS_M2C_Group_DelMemberFromGroup_2_Nty" then  
        sendMsg(M2C_Group_DelMemberFromGroup_2_Nty, __info)     
    elseif __idStr == "CS_M2C_Group_SendMessage_Ack" then  
        dump(__info)
        sendMsg(M2C_Group_SendMessage_Ack, __info)     
    elseif __idStr == "CS_M2C_Group_SendMessage_Nty" then  
        
        ChatMgr:getInstance():setFriendMsgList(__info,__info.m_GroupID,true) 
        sendMsg(M2C_Group_SendMessage_Nty, __info)     
    elseif __idStr == "CS_M2C_Group_DeleteGroup_Ack" then  
        ChatMgr:getInstance():removeGroup(__info.m_GroupID)
        sendMsg(M2C_Group_DeleteGroup_Ack, __info)     
    elseif __idStr == "CS_M2C_Group_DeleteGroup_Nty" then  
        sendMsg(M2C_Group_DeleteGroup_Nty, __info)     
    elseif __idStr == "CS_M2C_ChatRoom_IsEnableMessage_Nty" then
        ChatMgr:getInstance():canSpeakFlag(__info.m_Flags) 
    end
end


return IMController