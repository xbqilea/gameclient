local SignHongbaoController = class("SignHongbaoController")

function SignHongbaoController:ctor()
    self:myInit()
end

function SignHongbaoController:myInit()
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_ShowSignInChannel_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_PlayerSignIn_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_ShowDepositChannel_Ack", handler(self, self.netMsgHandler))
end

---签到
function SignHongbaoController:ShowSignInChannelReq() 
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_ShowSignInChannel_Req")
end

---玩家签到
function SignHongbaoController:PlayerSignInReq() 
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_PlayerSignIn_Req")
end

---充值返利
function SignHongbaoController:ShowDepositChannelReq() 
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_ShowDepositChannel_Req")
end
 
function SignHongbaoController:netMsgHandler( __idStr, __info )
    if __idStr == "CS_H2C_ShowSignInChannel_Ack" then
        if __info.m_ret == 0 then
            sendMsg(SHOWSIGNINCHANNEL_ACK, __info)
        elseif __info.m_ret < 0 then
            ToolKit:showErrorTip(__info.m_ret)
        end
    elseif __idStr == "CS_H2C_PlayerSignIn_Ack" then
        if __info.m_ret == 0 then
            sendMsg(PLAYERSIGNIN_ACK, __info)
        elseif __info.m_ret < 0 then
            ToolKit:showErrorTip(__info.m_ret)
        end
    elseif __idStr == "CS_H2C_ShowDepositChannel_Ack" then
        sendMsg(SHOWDEPOSITCHANNEL_ACK, __info) 
    end
end
 

return SignHongbaoController