--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 登陆控制类
-- 登录注册数据管理说明：
-- 1. 游客登录 使用 getNewAccount() 创建新账号登录
-- 2. 有历史账号时 使用 getLastAccount() 获得最后一个登录成功的账号登录
-- 3. 1, 2操作(当前使用的)的账号，通过 getCurAccount() 获得
-- 4. 账号成功登陆时 调用 addAccData() 更新与保存账号数据

local GameDataController = require("app.hall.main.control.LocalDataController") 
local PlayerDataController = import(".UserInfoController")
local UserCenterController =import(".UserMainController") 

local scheduler = require("framework.scheduler")
local BulletinController = require("app.hall.notice.control.NoticeController") 
local RankController = import(".RankController")
local FreezeVerifyController = require("app.hall.login.control.ProtectLoginController")
local BankController = import(".BankController") 
local RebateController = require("app.hall.rebate.control.RebateController") 
local ClipboardTextController = require("app.hall.clipboardText.control.ClipboardTextController")
local ShareQRcodeController = require("app.hall.share.control.ShareQRcodeController")
local ExchangeRmbController = import(".ExchangeRmbController")
local openinstall = import("app.hall.base.third.openinstall")
local XYDBController = import(".XYDBController") 
local SignHongbaoControllerr = import(".SignHongbaoController")
local IMController = import(".IMController")
local Updater = require("app.assetmgr.util.AssetDownload")
local DlgAlert = require("app.hall.base.ui.MessageBox")
local platformUtils = require("app.hall.base.util.platform")  

require("app.hall.main.model.MsgBurialData")
require("app.hall.base.util.HostApiUtil")
require("app.hall.base.third.ImgUpload")
require("app.hall.base.voice.SoundRecordUtil")

local LOGIN_MODE = {
	["GUEST"]=0,
	["PASSWORD"]=1,
	["SMS"]=2
}

local LoginController = class("LoginController")

local Ver_Type = {
    none = 0,
    freeze = 1,
    unnormal = 2
}

LoginController.instance = nil

-- 获取房间控制器实例
function LoginController:getInstance()
    if LoginController.instance == nil then
        LoginController.instance = LoginController.new()
    end
    return LoginController.instance
end

function LoginController:releaseInstance()
    if LoginController.instance then
		LoginController.instance:onDestory()
		LoginController.instance = nil
		g_LoginController = nil
    end
end

function LoginController:ctor()
    self:myInit()
end

function LoginController:bindLoginScene(__scene)
	self.scene = __scene
end

function LoginController:myInit()
    self._promoterId = 0
    self._urlName=""
    local function getInstallCallBack(result)
		print(result)
        local result2 = require("framework.json").decode(result)
        if result2 then
            print(result2)
            local channelId = result2["channelId"] or 0
            local promoterId = result2["promoterId"] or 0
            self._promoterId = tonumber(promoterId)
            self._urlName = result2["locationUrl"] or ""
            print("channelID==",channelId)
        end
        print("promoterId==",self._promoterId)
    end

    openinstall:getInstall(10, getInstallCallBack)
    self.loginKey = nil            -- 登陆密钥
    self.accountID = nil           -- 玩家id
    self.account = nil             -- 玩家账号
    self.password = nil            -- 明文密码
    self.phone = nil               -- 注册时的手机号码
    self.nick = nil                -- 昵称

    self.accountCallBack = nil     -- 注册账号的回调
    self.regularType = nil         -- 转正账号类型   1：账号转正  2：手机账号转正
    --self._creatAccountType = 0   	   -- 创建账号 1-创建游客账号 2-创建手机账号
    --self._changeAccountType = "guest"  -- 切换账号 0-使用游客账号登录 1-切换手机账号登录 2-切换微信账号登录
    self._isReconnect = false      -- 断线重连,这里指的是大厅重连
    self._domainName = ""          -- 断线重连的大厅服地址
    self._port = ""                -- 断线重连的大厅服端口
	self.logonType = "guest"		--	游客登录， 默认为游客登录
	self._currentLogonType = "guest"    -- 当前登录方式 guest-游客(游客必定能够登录进去) qka-手机 wx-微信
    --self._isLogout = false		   -- 是否退出
	--self.changeWxAcc = nil		   -- 切换微信账号
	--self._isAllowReconnect = true  -- 是否允许重连
	
    -- 游戏数据管理类
    self._gameDataCtl = GameDataController.new()

    addMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT, handler(self, self.onSocketEventMsgRecived))

    -- 注册网络监听
    -- 登录服
    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_CreateAccountAck", handler(self, self.loginNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_SRP6_1_Ack", handler(self, self.loginNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_SRP6_3_Ack", handler(self, self.loginNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_LoginAck", handler(self, self.loginNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_CreateVisitorAccountAck", handler(self, self.loginNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_CreatePhoneNumAccountAck", handler(self, self.loginNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_GetNoticeSMSAck", handler(self, self.loginNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_RegisteOldPlatformAccountAck", handler(self, self.loginNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_ThirdPartyAccLogin_Ack", handler(self, self.loginNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_S2C_MaintanenceInfo_Nty", handler(self, self.loginNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_H2C_KickNotice_Nty", handler(self, self.lobbyNetMsgHandler))
    
    -- 大厅服
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_GetNoticeSMSAck", handler(self, self.lobbyNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_LoginAck", handler(self, self.lobbyNetMsgHandler))
    --TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_PortalList_Nty", handler(self, self.lobbyNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_BecomeRegularAccountAck", handler(self, self.lobbyNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_FuncSwitchNty", handler(self, self.lobbyNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_S2C_MaintanenceInfo_Nty", handler(self, self.lobbyNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_KickNotice_Nty", handler(self, self.lobbyNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_KickNty", handler(self, self.kickMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_MailBrief_Nty", handler(self, self.lobbyNetMsgHandler))

    -- 登录类型
    --[[self.loginType =
    {
            ["account"] = 1, -- 账号登录
            ["phone"] = 2, -- 手机号登录
    }]]--
    GlobalBulletinController   = GlobalBulletinController or BulletinController.new() 
    GlobalRankController       = GlobalRankController or RankController.new()
    GlobalVerifyController     = GlobalVerifyController or FreezeVerifyController.new()
    GlobalBankController       = GlobalBankController or BankController.new()
    g_UserCenterController     = g_UserCenterController or UserCenterController.new() 
    GlobalRebateController   = GlobalRebateController or RebateController.new() 
    g_ClipboardTextController = g_ClipboardTextController or ClipboardTextController.new()
    g_ShareQRcodeController = g_ShareQRcodeController or ShareQRcodeController.new()
    g_ExchangeRmbController = g_ExchangeRmbController or ExchangeRmbController.new()
    GlobalXYDBController       = GlobalXYDBController or XYDBController.new()
    g_signhongbaoController = g_signhongbaoController or SignHongbaoControllerr.new()
	 GlobalIMController   = GlobalIMController or IMController.new() 
	--恢复最后一次的登录数据
    print("import from cache....................")
	local acc = self._gameDataCtl:getLastAccountData()
	if acc then
		if acc.loginType == "wx" then
			self.wxOpenid = acc.account
			self.wxToken = acc.password
			self.logonType = "wx"
			self._currentLogonType = "wx"		--这个是已经登录成功的账号
			print("import wx cache: account:".. self.wxOpenid ..",password:"..self.wxToken)
			return
		elseif acc.loginType == "qka" then
			self.account = acc.account
			self.password = acc.password
			self.logonType = "qka"
			self._currentLogonType = "qka" 		--这个是已经登录成功的账号
			print("import normal cache: account:".. self.account ..",password:"..self.password)
			return
		else
			self.account = acc.account
			self.logonType = "guest"
			self._currentLogonType = "guest" 	--这个是已经登录成功的账号
			print("import guest cache: account:".. self.account)
			return
		end
	end
	
	--这里使用游客账号登录
	ToolKit:setLoginType(GlobalDefine.LoginType.none)
	print("import from cache end....................")
end

function LoginController:loginByGuest()
	--先查找本地是否有游客账号
	local tag = false
	local accountData = self._gameDataCtl:getAllAccounts()
	for key, var in ipairs(accountData) do
		if var.loginType == "guest" then
			self.account = var.account
			tag = true
			print("start by guest, account:"..self.account)
			break
		end
    end
		
	if not tag then
		self:registVisitorAccount()
	else
		self.logonType = "guest"
		self:login()
	end
end

-- 登录(连接登录服务器)
function LoginController:login()
    -- ToolKit:addLoadingDialog(10, STR(29, 1))
    local version = 10
    if GlobalConf.APPVERSION and GlobalConf.APPVERSION==version then
        print("************开始连接登陆服*****************")
        if PSU[Protocol.LoginServer] then
            ConnectManager:closeConnect(Protocol.LoginServer) 
        end

	    -- 连接登录服
        ConnectManager:connect2Server(Protocol.LoginServer, GlobalConf.LOGIN_SERVER_IP, GlobalConf.LOGIN_SERVER_PORT) 
    else
        local dlg = DlgAlert.showTipsAlert({title = "提示", tip = "检测到新版本，请重新下载!", tip_size = 34})
					dlg:setSingleBtn(STR(37, 5), function ()
                        device.openURL("https://983750.com/")
				end)
    end
end

-- 创建游客账号成功
function LoginController:onCreateVistorSuccess(_acc)
	local acc = self._gameDataCtl:getCurAccount()
    acc.account = _acc
	self._gameDataCtl:addAccData() 	-- 保存
    ToolKit:setLoginType(GlobalDefine.LoginType.qka)
	
	self.account = _acc
	self.password = acc.password
	self:loginAccount(LOGIN_MODE.GUEST) 		-- 注册登录一律用账号名登录
	print("create vistor success: account:".. self.account ..",password:"..self.password)
end

-- 创建手机账号成功
function LoginController:onCreatePhoneAccSuccess(_phoneNum, _pwd)
	local acc = self._gameDataCtl:getNewAccount()
    acc.account = _phoneNum
    acc.password = _pwd
	acc.tell = _phoneNum
	acc.loginType = "qka"
	self._gameDataCtl:addAccData() -- 保存
    ToolKit:setLoginType(GlobalDefine.LoginType.qka)
	
	self.account = _phoneNum
	self.password = _pwd
	self:loginAccount(LOGIN_MODE.PASSWORD) 	-- 注册登录一律用账号名登录
	print("create phone player success: account:".. self.account ..",password:"..self.password)
end

-- 注册手机号转正, 直接添加新账号
function LoginController:onVistorRegularSuccess(_phoneNum, _pwd)
	-- 创建新账号
	local acc = self._gameDataCtl:getNewAccount()
	acc.account = self.account
	acc.password = _pwd
    acc.tell = _phoneNum
	acc.loginType = "qka"			--如果原来是wx登陆， 这里标记要改为qka
	self._gameDataCtl:addAccData() -- 保存
    ToolKit:setLoginType(GlobalDefine.LoginType.qka)
	
	--绑定新账号
	self.account = _phoneNum
	self.password = _pwd
	print("regular success: account:".. self.account ..",password:"..self.password)
end

--如果之前已经用手机注册过，卸载重装， 输入手机号进入， 这里应该让其进入
function LoginController:onLoginSuccess( _account )
	--[[
	1. 如果之前已经用手机注册过，卸载重装， 输入手机号进入， 这里应该让其进入
	2. 如果之前注册了多个手机号，这里允许切换不同的手机号进入
	--]]
	if platformUtils.setJPushAlias then
	    platformUtils.setJPushAlias("" .. self.accountID, "" .. require("src.app.MyAppID").channel, 0)
	end
	--
	local acc = self._gameDataCtl:getNewAccount()
	acc.account = _account
	if self.logonType == "qka" then
		acc.loginType = "qka"
		acc.tell = self.account
		acc.password = self.password
	end
	self._gameDataCtl:addAccData() -- 保存
	
	--登录成功，记录当前登录方式
	--self._creatAccountType = 0
	self._currentLogonType =  self.logonType
	ToolKit:setLoginType(GlobalDefine.LoginType.qka)
		
	if self.logonType == "qka" then
		print("phone login success, account:".. self.account ..",password:"..self.password)
	else
		print("guest login success, account:".. self.account)
	end
end

--第三方账号登录绑定
function LoginController:onWxLoginSuccess(_acc, _token)		
	local acc = self._gameDataCtl:getNewAccount()
    if g_wxUserInfor then
        acc.account = g_wxUserInfor.openid
    else
        acc.account = self.wxOpenid
    end
	acc.password = _token --password用于存token
	acc.loginType = "wx"
    self._gameDataCtl:addAccData() 	-- 保存
	ToolKit:setLoginType(GlobalDefine.LoginType.wx) --设置下次登录方式
	
	self.account = _acc
	self.wxToken =  _token
	self.wxLoginFirst = nil
	print("wx login success: account:".. self.wxOpenid ..",password:"..self.wxToken)
end

function LoginController:onBindThirdPartyAccSucc(_token)
	local acc = self._gameDataCtl:getNewAccount()
    acc.account = g_wxUserInfor.openid
    acc.password = _token
	acc.loginType = "wx"
	g_LoginController._gameDataCtl:addAccData() 			-- 保存
	ToolKit:setLoginType(GlobalDefine.LoginType.wx) --设置下次登录方式
	
	self.wxOpenid = g_wxUserInfor.openid
	self.wxToken = _token
	self.wxRefreshToken = g_wxUserInfor.refresh_token
	print("wx bind success: account:".. self.wxOpenid ..",password:"..self.wxToken)
end

-- 登录大厅成功
function LoginController:onEnterLobbySuccess(__info)
	if __info.m_nOtherBeforeTermType ~= 0 then
		ToolKit:delayDoSomething(function()
			sendMsg(MSG_KICK_OTHER_TIPS, __info.m_nOtherBeforeTermType)
        end, 2)
    end
	
	GlobalDefine.ISLOGOUT = false
	g_isAllowReconnect = true
	ToolKit:removeLoadingDialog()
	
	sendMsg(MSG_RECONNECT_LOBBY_SERVER, "success") -- 重连成功
	
	--每过5秒发一次心跳包
    TotalController:beginToSendHallPing()
end

-- 登录大厅失败
function LoginController:onEnterLobbyFail(__info)
	--ConnectManager:closeConnect(Protocol.LobbyServer)
    --self:login()
	--重新发起连接
    --self._changeAccountType = 0
	--self.logonType = self._currentLogonType --还原原登录方式
    --self._creatAccountType = 0
    --sendMsg(MSG_LOGIN_SUCCESS,__info.m_nResult)
	ToolKit:returnToLoginScene()
end

function LoginController:onLoginFail()
	-- 登录登录服失败， 这里需要切换回原登录方式
	self.logonType = "guest"
	self._currentLogonType = "guest"		
	
	--这个是已经登录成功的账号
	local acc = self._gameDataCtl:getLastAccountData()
	if acc then
		if acc.loginType == "wx" then
			self.wxOpenid = acc.account
			self.wxToken = acc.password
			self.logonType = "wx"
			self._currentLogonType = "wx"		--这个是已经登录成功的账号
			print("import wx cache: account:".. self.wxOpenid ..",password:"..self.wxToken)
			return;
		elseif acc.loginType == "qka" then
			self.account = acc.account
			self.password = acc.password
			self.logonType = "qka"
			self._currentLogonType = "qka" 		--这个是已经登录成功的账号
			print("import normal cache: account:".. self.account ..",password:"..self.password)
			return;
		end
	end
end

function LoginController:onWxLoginFail(result)
	self:onLoginFail()
	
	local data = getErrorTipById(result)
    if data then
		local DlgAlert = require("app.hall.base.ui.MessageBox")
		local dlg = DlgAlert.showTipsAlert({title = "提示", tip = data.tip})
		dlg:setSingleBtn("退出", function ()
			ToolKit:returnToLoginScene()
		end)
		dlg:enableTouch(false)
	else
		TOAST("错误码: " .. result, function ()
			ToolKit:returnToLoginScene()
		end)
	end
	
	--local dlg = ToolKit:showErrorTip4(result, function()
	--	ToolKit:returnToLoginScene()
	--end)
	--dlg:enableTouch(false)
end

--大厅服回调
-- @param __idStr(string) 协议字符串
-- @param __info(table) 数据table
function LoginController:lobbyNetMsgHandler(__idStr, __info)
    if __idStr == "CS_GetNoticeSMSAck" then
        sendMsg(MSG_SEND_YZM_ASK,__info.m_nRetCode )
    elseif __idStr == "CS_H2C_LoginAck" then
        sendMsg(MSG_ROMOVE_LOADING )
        sendMsg(MSG_LOGIN_LOBBY_ASK,__info.m_nResult)
		dump(__info)
        if __info.m_nResult == 0 then
            --登录大厅成功
            print("*********登录大厅成功************")
			self:onEnterLobbySuccess(__info)
        else    --其他原因
			print("*********登录大厅失败************")
			dump(__info)
			self:onEnterLobbyFail(__info)
        end
    elseif __idStr == "CS_H2C_FuncSwitchNty" then -- 功能开关数据
        -- TOAST("开关数据")
        -- dump(__info, "aaaaaaaaaaaaaaaaaaaaaaaaaaaa")
        g_funcOpenList = {}
        g_promoteType = 0
        for i = 1, #__info.m_OpenNodeIDList do
            g_funcOpenList[__info.m_OpenNodeIDList[i]] = true
            g_promoteType = __info.m_promoteType
        end

		if self.scene and self.scene.loginSuccess  then
			self.scene:loginSuccess()
			self.scene = nil
		end
		
        sendMsg(MSG_FUNCSWITCH_UPDATE)

    elseif __idStr == "CS_H2C_BecomeRegularAccountAck" then
        if __info.m_nRetCode == 0 then --转正成功
			self:onVistorRegularSuccess(self.account, self.password)
        end
		sendMsg(MSG_BECOME_REGULAR_ACCOUNT, __info.m_nRetCode )
		
    elseif __idStr == "CS_S2C_MaintanenceInfo_Nty" then
        sendMsg(MSG_MAINTANENCE_INFO, __info)

    elseif __idStr == "CS_H2C_KickNotice_Nty" then
        --  踢人
        sendMsg(MSG_KICKPLAY_MAINTANENCE,__info)

    elseif __idStr == "CS_H2C_MailBrief_Nty" then

        dump(__info, "信息内容:", 10)
        sendMsg(MSG_SHOW_MAIL_RED, __info)
    end
end

--登陆服回调
-- @param __idStr(string) 协议字符串
-- @param __info(table) 数据table
function LoginController:loginNetMsgHandler( __idStr, __info )
    print("LoginController:loginNetMsgHandler", __idStr)
    dump(__info, __idStr)
    -- 创建账号返回结果
    if __idStr == "CS_L2C_CreateVisitorAccountAck" then
        sendMsg( MSG_ROMOVE_LOADING )
        if __info.m_nResult == 0 then -- 成功
			self:onCreateVistorSuccess(__info.m_strAccountName)
        else
            sendMsg(MSG_SHOW_ERROR_TIPS,__info.m_nResult)
        end
    elseif __idStr == "CS_L2C_CreatePhoneNumAccountAck" then
        if __info.m_nResult == 0 then 		-- 创建成功，登录账号
            print("***********创建手机账号成功***********")
            self:onCreatePhoneAccSuccess(self.account, self.password)
        end
        sendMsg(MSG_CREAT_PHONE_ACCOUNT, __info.m_nResult)
		
    elseif __idStr == "CS_L2C_SRP6_1_Ack" then
        print("bbbbbbbbbbbbbbbbbbbbbbbbbbbbCS_L2C_SRP6_1_Ack: ", self.password)
        local pwd = self.password
        if string.len(self.password) <= GlobalDefine.USER_PWD_LEN[2] then
            pwd = crypto.md5(self.password) -- 明文密码需要取md5
        end
        local sessionKey = qka.SRP6Util:generateSessionKey(__info.m_strSalt, __info.m_strSRPB, pwd)

        sessionKey = self:addZero(sessionKey)
        print("M1",sessionKey)
        ConnectManager:send2Server(Protocol.LoginServer, "CS_C2L_SRP6_2_Req",{sessionKey}) -- 校验
    elseif __idStr == "CS_L2C_SRP6_3_Ack" then
        if qka.SRP6Util:verifyM2(__info.m_strSessionKey) then
            print("M2 验证成功")
        else
            print("M2 is wrong")
        end
    elseif __idStr == "CS_L2C_LoginAck" then
        sendMsg( MSG_ROMOVE_LOADING )
        -- dump(__info)
		
		--登录失败, 恢复数据
		if __info.m_nResult ~= 0 then
			self:onLoginFail()
		end

		if __info.m_nResult == 0 then -- 可以登录
            self.loginKey = __info.m_nLoginKey
            self.accountID = __info.m_nAccountID
			self:onLoginSuccess(__info.m_strAccountName)
			print("login success:", self.accountID, self.loginKey)
            GlobalPlayerDataController = GlobalPlayerDataController or  PlayerDataController.new()
            self._domainName = __info.m_strDomainName -- 断线重连的大厅服地址
            self._port = __info.m_nPort -- 断线重连的大厅服端口
			
			--这里一定要完全关闭，否则，如果是用手机或者未定切换到另外一个账号，
			--connect2Server走的是重连，而不是重新创建连接
			if PSU[Protocol.LobbyServer] then
				ConnectManager:closeConnect(Protocol.LobbyServer)
			end
            ConnectManager:connect2Server(Protocol.LobbyServer, self._domainName, self._port) -- 连接大厅
            ConnectManager:connect2Server(Protocol.ChatServer, GlobalConf.IM_SERVER_IP, GlobalConf.IM_SERVER_PORT) -- 连接IM
        elseif __info.m_nResult == -305 then
            sendMsg(MSG_ACCOUNT_FROZEN, __info)
        else
            if __info.m_nVerifyReason == Ver_Type.freeze or __info.m_nVerifyReason == Ver_Type.unnormal then
                if not GlobalVerifyController:getOpenViewFlag() then
                    sendMsg(MSG_ACCOUNT_UNNORMAL, __info)
                end
            else
                --if __info.m_nResult == -2 then
                --    sendMsg(MSG_TIME_OUT_NOTICE)
                --    GlobalVerifyController:updateOpenView(true)
                --else
                --    sendMsg(MSG_SHOW_ERROR_TIPS, __info.m_nResult )
                --end
                --密码验证失败时，把账号重设为上次成功登陆的账号，以免断线重连时继续发送错误的密码
                --if (__info.m_nResult == -300 or __info.m_nResult == -301) then
                    --第一次登陆失败，给个创建游客账号的弹窗
                --    sendMsg(MSG_RE_CREATE_VISITOR)
                --end
				--进入登录界面
				local data = getErrorTipById(__info.m_nResult)
				if data then
					local DlgAlert = require("app.hall.base.ui.MessageBox")
					local dlg = DlgAlert.showTipsAlert({title = "提示", tip = data.tip})
					dlg:setSingleBtn("退出", function ()
						ToolKit:returnToLoginScene()
					end)
					dlg:enableTouch(false)
				else
					TOAST("错误码: " .. __info.m_nResult, function ()
						ToolKit:returnToLoginScene()
					end)
				end
            end
        end
    elseif __idStr == "CS_L2C_ThirdPartyAccLogin_Ack"  then
        print("************第三方登录响应***************")
        dump(__info)
        -- 第三方账号登陆响应
		
		--self.changeWxAcc = false
        if __info.m_nResult == 0 then
            --登录成功
			print("************第三方登录成功***************")
            self.loginKey = __info["m_nLoginKey"]
            self.accountID = __info["m_nAccountID"]
            self.account = __info.m_strAccount
            self.headiconURL = __info.m_strAvatarURL
            self:onWxLoginSuccess(__info.m_strAccount, __info.m_strToken)
            
			print("login success:", self.accountID, self.loginKey)
            print("打印从服务器获得的token:" .. __info.m_strToken )

            GlobalPlayerDataController = GlobalPlayerDataController or  PlayerDataController.new()
            self._domainName = __info.m_strDomainName -- 断线重连的大厅服地址
            self._port = __info.m_nPort -- 断线重连的大厅服端口
			
			--这里一定要完全关闭，否则，如果是用手机或者未定切换到另外一个账号，
			--connect2Server走的是重连，而不是重新创建连接
			if PSU[Protocol.LobbyServer] then
				ConnectManager:closeConnect(Protocol.LobbyServer)
			end
            ConnectManager:connect2Server(Protocol.LobbyServer, self._domainName, self._port) -- 连接大厅
            ConnectManager:connect2Server(Protocol.ChatServer, GlobalConf.IM_SERVER_IP, GlobalConf.IM_SERVER_PORT) -- 连接IM

        elseif __info.m_nResult == -305 then
            sendMsg(MSG_ACCOUNT_FROZEN, __info)
        elseif __info.m_nResult == -324  then
            --登录凭证已失效，重新授权登录
            print("*********登录凭证已失效，需要重新授权*********")
            
            --官网微信登录
            local function _callback( _result)
                if _result then
                        --获取授权信息成功
                        self.logonType = "wx"
						self.wxOpenid = g_wxUserInfor.openid
						self.wxToken = g_wxUserInfor.token
						self.wxRefreshToken = g_wxUserInfor.refresh_token
						self.wxLoginFirst = true
						self:login()
                    else
                        --获取授权信息失败
                        print("获取授权信息失败")
                        local function authorizwFail()
                            TOAST("微信授权失败，请退出游戏然后登录微信，微信登录成功后重新登录游戏！")
                        end
                        scheduler.performWithDelayGlobal(authorizwFail, 0.5)
                    end
                end

                ToolKit:getWechatAuthorizeCode(_callback)
        elseif __info.m_nResult == -327  then
            --已登录
            TOAST("您已登录，不要重复登录")
            return

        elseif __info.m_nResult == -328  then
            --第三方账号已解绑，需重新登陆          
            sendMsg(MSG_RE_BIND_WX)       
        else
			print("*********发生其他错误" .. __info.m_nResult .. "*********")
			self:onWxLoginFail(__info.m_nResult)
        end

    elseif __idStr == "CS_GetNoticeSMSAck" then

        sendMsg(MSG_SEND_YZM_ASK,__info.m_nRetCode )

     elseif __idStr == "CS_S2C_MaintanenceInfo_Nty" then
        sendMsg(MSG_MAINTANENCE_INFO, __info)

    elseif __idStr == "CS_H2C_KickNotice_Nty" then
        --  踢人
        sendMsg(MSG_KICKPLAY_MAINTANENCE,__info)

    end
end

--重连服务器
--[[
function LoginController:onCallbackReConnect()
    if ConnectManager:isConnectSvr( Protocol.LoginServer ) or ConnectManager:isConnectSvr( Protocol.LobbyServer ) then
        --如果大厅服和登录服没有全部断开
        print("大厅服和登录服没有全部断开")
        if self.closeTimes  then
            if self.closeTimes > 5 then
                print("断开大厅服和登录服失败")
                return
            else
                self.closeTimes = self.closeTimes
            end
        else
            self.closeTimes = 1
        end

		TotalController:stopToSendHallPing()
        --ConnectManager:closeConnect(Protocol.LobbyServer)
        --ConnectManager:closeConnect(Protocol.LoginServer)
        scheduler.performWithDelayGlobal(handler(self, self.onCallbackReConnect), 1 )
    else
        self.closeTimes = 0
        TotalController:stopToSendHallPing()
        self:login()
    end
end
--]]

-- 切换账号
-- @param account(string) 账号id
-- @param __password(string) 密码
--[[
function LoginController:switchAccount( __account, __password )
    if __account == Player:getAccountName() then
        sendMsg(MSG_SHOW_ERROR_TIPS, 1109 )
        return
    end
	
	self.account = __account -- 切换账号
    self.password = __password -- 切换密码
    self._ischangeAccount = true
	self.logonType = "qka"

    --local accountData = self._gameDataCtl:getAllAccounts()
    --for key, var in ipairs(accountData) do
    --    if var.account == __account and var._isVisitor then
    --        self._loginVisitor = true
    --    end
    --end

    print("********开始切换账号流程********")
    if ConnectManager:isConnectSvr(Protocol.LobbyServer)  then
		g_isAllowReconnect = false --允许重连
        ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_LogoutRep" )
        scheduler.performWithDelayGlobal(handler(self, self.onCallbackReConnect), 1 )
    elseif ConnectManager:isConnectSvr(Protocol.LoginServer) then
        ConnectManager:closeConnect(Protocol.LoginServer)
        scheduler.performWithDelayGlobal(handler(self, self.onCallbackReConnect), 1 )
    else
        TotalController:stopToSendHallPing()
        self:login()
    end
end
--]]

--[[
function LoginController:ThirdChangeAccount()
    print("********第三方账号开始切换账号********")
    if ConnectManager:isConnectSvr(Protocol.LobbyServer)  then
		g_isAllowReconnect = false --不允许允许重连
        ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_LogoutRep" )
        --ConnectManager:closeConnect(Protocol.LoginServer)
        --ConnectManager:closeConnect(Protocol.LobbyServer)
        scheduler.performWithDelayGlobal(handler(self, self.onCallbackReConnect), 1 )
    elseif  ConnectManager:isConnectSvr(Protocol.LoginServer) then
        ConnectManager:closeConnect(Protocol.LoginServer)
        scheduler.performWithDelayGlobal(handler(self, self.onCallbackReConnect), 1 )
    else
        TotalController:stopToSendHallPing()
        self:login()
    end
end
--]]


--使用当前微信号切换账号
-- 0-登录成功  1-登录失败
--用微信号切换账号
--[[
function LoginController:loginByWx( _callback )

    self.wxLoginCallback  = _callback
    
    self.changeWxAcc = true
	
	print("*******self.changeWxAcc = true********")

    print("********开始切换账号流程********")
    if ConnectManager:isConnectSvr(Protocol.LobbyServer)  then
		g_isAllowReconnect = false --不允许允许重连
        ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_LogoutRep" )
        scheduler.performWithDelayGlobal(handler(self, self.onCallbackReConnect), 1 )
    elseif ConnectManager:isConnectSvr(Protocol.LoginServer) then
        ConnectManager:closeConnect(Protocol.LoginServer)
        scheduler.performWithDelayGlobal(handler(self, self.onCallbackReConnect), 1 )
    else
        TotalController:stopToSendHallPing()
        self:login()
    end
end
--]]

function LoginController:kickMsgHandler( __idStr, __info )
    if __idStr == "CS_H2C_KickNty" then
        --TotalController:stopToSendHallPing()
        sendMsg(MSG_KICK_NOTIVICE, __info)
    end
end

-- 保存本地密码信息
-- 用于重置密码
function LoginController:saveAccountForResetPwd( __account, __password )
    self.account = __account
    self.password = __password
	
    local acc = self._gameDataCtl:getNewAccount()
    acc.account = self.account
    acc.password = self.password
	acc.loginType = "qka"
    self._gameDataCtl:addAccData() -- 保存
end

function LoginController:startCallBack()
    if self.accountCallBack then
        print("************开始执行回调函数*************")
        self.accountCallBack()
        self.accountCallBack = nil
    else
        self:start()
    end
end

function LoginController:onNetworkFailure()
	print("onNetworkFailure")
	ConnectManager:closeConnect(Protocol.LoginServer)
	ConnectManager:closeConnect(Protocol.LobbyServer)
    ConnectManager:closeConnect(Protocol.ChatServer)
	GlobalDefine.ISLOGOUT = true
	g_isAllowReconnect = false
	g_isNetworkInFail = true
    ToolKit:removeLoadingDialog()
    platform.setGLViewTouchEnabled(true)
    if isPortraitView() then
        local messageBox = require("app.hall.base.ui.MessageBoxEx").new("提示", "网络异常", function(index, sender)
            if index ~= 0 then
                sender:removeFromParent()
                if g_GameController then
                    g_GameController:releaseInstance()
                 end
                 if (g_BGController) then
                     g_BGController:releaseInstance()
                 end
                 if (g_CPController) then
                     g_CPController:releaseInstance()
                 end
                 if g_ThirdPartyContntroller then
                    g_ThirdPartyContntroller:releaseInstance()
                 end
                 UIAdapter:pop2RootScene()
                 UIAdapter:replaceScene("app.hall.login.view.LoginScene")
            end
        end)
        messageBox:setCloseEnabled(false)
        display.getRunningScene():addChild(messageBox, 99999)
        messageBox:addButton(STR(37, 5))
    else
        local dlg = DlgAlert.showTipsAlert({ title = "", tip = "网络异常", tip_size = 34 })
        dlg:setSingleBtn(STR(37, 5), function()
            if g_GameController then
                g_GameController:releaseInstance()
            end
            if (g_BGController) then
                g_BGController:releaseInstance()
            end
            if (g_CPController) then
                g_CPController:releaseInstance()
            end
            if g_ThirdPartyContntroller then
                g_ThirdPartyContntroller:releaseInstance()
            end
            UIAdapter:pop2RootScene()
            UIAdapter:replaceScene("app.hall.login.view.LoginScene")                
        end)
    end
end

-- socket连接状态改变
-- @param __msgName(string) 广播名称
-- @param __protocol(string) 连接标志
-- @param __connectName(string): 连接状态
function LoginController:onSocketEventMsgRecived(__msgName, __protocol, __connectName)
    if __protocol == Protocol.LoginServer then
        -- 登录服连接成功
        if __connectName == cc.net.SocketTCP.EVENT_CONNECTED then
            print("********状态：登陆服连接成功:EVENT_CONNECTED ***********")
            --self.serverConnectFailed = false
			g_isNetworkInFail = false
            self:startCallBack() 
        elseif __connectName == cc.net.SocketTCP.EVENT_CLOSE then  
            print("********状态：登陆服连接将关闭:EVENT_CLOSE ***********")
            --手动断开只收到这个
        elseif __connectName == cc.net.SocketTCP.EVENT_CONNECT_FAILURE then
            print("********状态：登陆服连接失败:EVENT_CONNECT_FAILURE ***********")
			self:onNetworkFailure()		
        elseif __connectName == cc.net.SocketTCP.EVENT_CLOSED then
            -- TOAST("网络连接失败")
            print("********状态：登陆服连接已关闭:EVENT_CLOSED ***********")
            --if self.scene then
            --   self.scene:onLoginFail()
            --end
			--self.serverConnectFailed = true
        end
    elseif __protocol == Protocol.LobbyServer then
        -- 大厅连接成功
        if  __connectName == cc.net.SocketTCP.EVENT_CONNECTED  then
            print("********状态：大厅服连接成功: EVENT_CONNECTED ***********")
			print("login acc:", self.accountID, self.loginKey)
            self:loginLobby(self.accountID, self.loginKey, self._isReconnect)
			--重置重连次数
			g_isNetworkInFail = false
			--ConnectManager:resetReconnectTimes()
        elseif __connectName == cc.net.SocketTCP.EVENT_CONNECT_FAILURE then
            print("********状态：大厅服连接失败***********")
			self:clearRollNotice()
			self:onNetworkFailure()
			
        elseif __connectName == cc.net.SocketTCP.EVENT_CLOSED then
            print("********状态：大厅服连接已关闭:EVENT_CLOSED ***********")
            self:clearRollNotice()
			
		    --print("**********打印滚动消息table************")
	        TotalController:stopToSendHallPing()
			if g_isAllowReconnect then
				ConnectManager:reconnect()
			end
        elseif __connectName == cc.net.SocketTCP.EVENT_CLOSE then
            --手动断开收到
			print("********状态：大厅服连接关闭:EVENT_CLOSE***********")
            self:clearRollNotice()
			TotalController:stopToSendHallPing()
        end
     elseif __protocol == Protocol.ChatServer then
        -- IM连接成功
        if  __connectName == cc.net.SocketTCP.EVENT_CONNECTED  then
            print("********状态：聊天服连接成功: EVENT_CONNECTED ***********") 
            GlobalIMController:ChatRoom_Login_Req(self.accountID,GlobalConf.CHANNEL_ID)
			--ConnectManager:resetReconnectTimes()
        elseif __connectName == cc.net.SocketTCP.EVENT_CONNECT_FAILURE then
            print("********状态：聊天服连接失败***********")
			self:clearRollNotice()
			-- self:onNetworkFailure()
			TOAST("聊天服连接失败")
			
        elseif __connectName == cc.net.SocketTCP.EVENT_CLOSED then
            print("********状态：聊天服连接已关闭:EVENT_CLOSED ***********")
            self:clearRollNotice()
			
		    --print("**********打印滚动消息table************")
	        TotalController:stopToSendHallPing()
			if g_isAllowReconnect then
				ConnectManager:reconnect()
			end
        elseif __connectName == cc.net.SocketTCP.EVENT_CLOSE then
            --手动断开收到
			print("********状态：大厅服连接关闭:EVENT_CLOSE***********")
            self:clearRollNotice()
			TotalController:stopToSendHallPing()
        end
    end
end


--local g_rollNoticeList =   GlobalBulletinController:getRollNoticeList()
--清空滚动通知
function LoginController:clearRollNotice()
    if GlobalBulletinController then
		print("**********清空滚动通知***********")
        local g_rollNoticeList =   GlobalBulletinController:getRollNoticeList()
        g_rollNoticeList = {}
    end

end


--查询服务器状态
-- @param _roomID(int): 房间号，如果是大厅则为0
-- @return (table): 如果返回nil则表示获取服务器状态失败
--{
--  "CurrentStatus": -1,//-1:房间id配置不存在,0:正常,1:维护
--  "StartMaintainingTime": 0,//开始维护时间(时间戳)
--  "EndMaintainingTime": 0//结束维护时间(时间戳)
--}

--[[
function LoginController:checkServerStatus( _roomID )

    -- 网站回调   sendMsg(MSG_CHECK_SERVER_STATUS, _rst )
    local  function httpCallback( __tag, event )
        if event.name == "completed" then
            sendMsg( MSG_ROMOVE_LOADING )

            local code = event.request:getResponseStatusCode()

            if code ~= 200 then
                print("failed with code: " .. code)
                -- TOAST("failed: " .. code)
                sendMsg(MSG_CHECK_SERVER_STATUS, nil  )
                return
            end
            local response =  event.request:getResponseString()
            if response then
                dump(response)
                local info = fromJson(response)
                dump(info)
                if info ~= nil then
                    sendMsg(MSG_CHECK_SERVER_STATUS, info  )
                    return
                else
                    --返回信息为nil
                    sendMsg(MSG_CHECK_SERVER_STATUS, nil  )
                    return
                end
            end
        elseif event.name == "progress" then
           -- print("progress", __tag)
        else
            sendMsg( MSG_ROMOVE_LOADING )
            print("failed!!!")
            sendMsg(MSG_CHECK_SERVER_STATUS, nil  )
            return
            --获取服务器状态信息失败
        end
    end

    local HttpUtil = require("app.hall.base.network.HttpWrap")
    local client = HttpUtil.new(HttpUrl.CHECK_SERVER)
    local paramName = "gameId"
    local data = _roomID
    client:get(paramName, data, httpCallback  , false)
end
--]]

-- 开始(连接socket成功后开始登录流程)

--状态：0.无登录账号      1.官方账号密码登录      2.微信登录

function LoginController:loginWxAccount()
	local _jrt
	if self.wxRefreshToken then
		_jrt = toJson(  {refresh_token = self.wxRefreshToken} )
    else
        _jrt = toJson(  {refresh_token = ""   } )
    end
	
	local _thirdType  = ToolKit:getThirdLoginType()
	local loginType = GlobalDefine.wxLoginType.localToken
	if self.wxLoginFirst then
		loginType = GlobalDefine.wxLoginType.authorize
	end
	self:loginByThirdParty(self.wxOpenid,_thirdType, loginType, self.wxToken, _jrt)
end

function LoginController:start()
    print("***********开始start函数**************")
	if self.logonType == "qka" then
        print("*******创建/切换手机账号******")
		self:loginAccount(LOGIN_MODE.PASSWORD)  		--登录登录服
    elseif self.logonType == "wx" then  
        print("*******微信登录******")
		self:loginWxAccount()							--使用微信信息登录
    else
        print("*******游客账号登录******")		
        self:loginAccount(LOGIN_MODE.GUEST) 							--登陆最后一次成功登陆的账号
    end
end

-- 微信授权登录
function LoginController:wxAuthorizelogin()
	local function _callback( _result)
		if _result then
            --获取授权信息成功 
            print("授权成功")
			self.logonType = "wx"
			self.wxOpenid = g_wxUserInfor.openid
			self.wxToken = g_wxUserInfor.token
			self.wxRefreshToken = g_wxUserInfor.refresh_token
			self.wxLoginFirst = true
            --ToolKit:addLoadingDialog(30, STR(29, 1))
            self:login() --这里连接登录服                          
        else
            --获取授权信息失败
            print("获取授权信息失败--")
        end
    end
    ToolKit:getWechatAuthorizeCode(_callback)
	
	--print("授权成功")
	--g_wxUserInfor = g_wxUserInfor or {}
	--self.logonType = "wx"
	--g_wxUserInfor.openid = "oJt2h54Ragc5pQXxnLHQPWqgSxB4"
	--g_wxUserInfor.token = "28_QKGqS2NUFb9mhRAYFkVkPhTatrYLHlz6PDWb50lPK06wnrfG975ckBk2aL9N90LY-Gf5wT4i5aKxjsmyRJJAS9XgpjH1amU7I64_1E_BONA"
	--g_wxUserInfor.refresh_token = "28_kRQRJBzfrOajQkmykvEsxucDVsQnCO8ZqmGq8J8GZzCT_XB8ngMqtiyaTfG2YSJGMHMBcBxyFOBvJF0cp34qq1U41z2a4W5OmLFf05y6Ay8"
	--_callback(true)
end

function LoginController:startByWx()
	
	--获取微信账号
	print("----------startByWx------------")
	if not self.wxOpenid or not self.wxToken then
		local accountData = self._gameDataCtl:getAllAccounts()
		for key, var in ipairs(accountData) do
			if var.loginType == "wx" then
				self.wxOpenid = var.account
				self.wxToken = var.password
				print("startByWx openid:"..self.wxOpenid..",token:"..self.wxToken)
				break
			end
        end
    end
	
	if self.wxOpenid and self.wxToken then
		self.logonType = "wx"
		self:login()
	else
		self:wxAuthorizelogin()
	end
end

--使用第三方信息登录
-- @param: _acc(STRING)      第三方账号
-- @param: _accType(SHORT)   第三方类型 
-- @param: _loginType(SHORT) 登陆类型 1-授权 2-本地token
-- @param: _token(STRING)    匹配登录类型  1-第三方授权验证token 2-本地token
-- @param: _ext(STRING)      备用字段 json拓展信息
function LoginController:loginByThirdParty(_acc, _accType, _loginType, _token, _ext   )
    --  ToolKit:addLoadingDialog(10, "登录中......")

    print("*************第三方开始登录服务器**************")

    ConnectManager:send2Server(Protocol.LoginServer, "CS_C2L_ThirdPartyAccLogin_Req", {_acc, _accType, _loginType ,_token,self:getDeviceInfo(), _ext})
end

-- 注册游客账号
function LoginController:registVisitorAccount()
    self.logonType = "guest"
	if not ConnectManager:isConnectSvr(Protocol.LoginServer)  then
        self.accountCallBack = handler(self,self.registVisitorAccountCallback)
        self:login()
    else
        self:registVisitorAccountCallback()
    end
end

function LoginController:registVisitorAccountCallback()
	--调用就会创建一个新的账号
	local acc = self._gameDataCtl:getNewAccount() -- 创建游客账号    
    print("***********发送游客账号协议***********")
	--print("mima",acc.password)
    ConnectManager:send2Server(Protocol.LoginServer, "CS_C2L_CreateVisitorAccountReq", {acc.password, self:getDeviceInfo()}) -- 注册
end

-- 请求手机验证码
-- @param: __type(int) 类型
-- @param: __phoneNum(string) 接收手机号
-- @param: __server(int) 大厅服-nil or 1      登录服-2
function LoginController:requestRegisterVerifyCode(__type, __phoneNum, __server) 
    if __server == nil or __server == 1 then 
        ConnectManager:send2Server(Protocol.LobbyServer, "CS_GetNoticeSMSReq", {__type, __phoneNum, 2,GlobalConf.CHANNEL_ID})
    else
        self:getSmsNotice(__type, __phoneNum)
    end
end


--请求手机验证码  使用登录服
-- @param: __type(int) 类型
-- @param: __phoneNum(string) 接收手机号
function LoginController:getSmsNotice( _type, _phone )
    self.noticeType = _type
    self.noticePhone = _phone
    self.accountCallBack = handler(self,self.getSmsNoticeCallback)
    self:login()
end

function LoginController:getSmsNoticeCallback()
    print("**********请求验证码**********")
    ToolKit:addLoadingDialog(10, STR(29, 1)) 
    ConnectManager:send2Server(Protocol.LoginServer, "CS_GetNoticeSMSReq", { self.noticeType ,self.noticePhone,2,GlobalConf.CHANNEL_ID  } ) -- 注册验证码
end

-- 注册手机账号
-- @param: __number(string) 手机号
-- @param: __pwd(string) 密码
-- @param: __name(string) 昵称
-- @param: __code(string) 验证码
function LoginController:registPhoneAccount( __number, __pwd, __name, __code )
    print("***********开始注册账号***********")
    self.account = __number
    self.phone = __number
    self.nick = __name
    self.password = __pwd
    self._code = __code
    --self._creatAccountType = 2
	self.logonType = "qka"
	
	print("***********注册手机账号***********")   
	if not ConnectManager:isConnectSvr(Protocol.LoginServer)  then
		self.accountCallBack = handler(self,self.registPhoneAccountCallback)
        self:login()
    else
		self:registPhoneAccountCallback()
	end
end

function LoginController:registPhoneAccountCallback()
    print("***********发送注册手机账号协议***********")
    ConnectManager:send2Server(Protocol.LoginServer, "CS_C2L_CreatePhoneNumAccountReq", {self.phone, self.password, self.nick, self._code, self:getDeviceInfo()})  -- 注册
end

-- 游客账号转正
-- @param: __accountType(number) 类型  '1:账号名，2：手机号'},
-- @param: __accountName(string) 新账号名
-- @param: __phoneNumber(string) 新注册手机号
-- @param: __password(string) 账号密码
-- @param: __verifyCode(string) 手机验证码
-- @param: _nickName(string) 昵称
function LoginController:becomeRegularAccountReq(__accountType,__accountName,__phoneNumber,__password,__verifyCode,_nickName)
    self.regularType = __accountType
    self.phone = __phoneNumber
    self.password = __password
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_BecomeRegularAccountReq", {__accountType, __accountName, __phoneNumber, __password, __verifyCode}) -- 游客转正
end

--[[
function LoginController:registAccount( __accountName ,__nickName , __pwd )
    print("***********发送注册账号协议***********")
    self.account = __accountName
    self.password = __pwd
    self.nick = __nickName
    self._iscreatAccount = true
	
	print("***********注册普通账号***********")
    if not ConnectManager:isConnectSvr(Protocol.LoginServer)  then
        self.accountCallBack = handler(self,self.registAccountCallback)
        self:login()
    else
        self:registAccountCallback()
    end
end


function LoginController:registAccountCallback()
    print("***********发送注册账号协议***********")
    ConnectManager:send2Server(Protocol.LoginServer, "CS_C2L_CreateAccountReq", {self.account,self.nick, self.password,  self:getDeviceInfo()})  -- 注册
end
--]]


-- 登陆账号
-- @param: __acc(string) 账号名
-- 密码存在self.password里
function LoginController:loginAccount( loginMode )
    -- print(debug.traceback()) 
 --   ToolKit:addLoadingDialog(10, STR(29, 1)) 
    --self.account = __acc
	print("*************开始登录**************")
    print(self.account, self.password)
    local A = qka.SRP6Util:generateA()
    print("A", A)
    ConnectManager:send2Server(Protocol.LoginServer, "CS_C2L_LoginReq", {self.account, A, self:getDeviceInfo(),"",loginMode}) -- 密码登录
end

-- 登录大厅
-- @param: __accountID(string) 账号
-- @param: __loginKey(string) 登录key
function LoginController:loginLobby( __accountID, __loginKey , __isReconnect)
    local isReconnect = 0
    if __isReconnect then
        isReconnect = 1
    end
     
    print("LoginController:loginLobby", __accountID, __loginKey,isReconnect)
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_LoginReq", {__accountID, __loginKey, self:getDeviceInfo(), isReconnect}) -- 登录
end

-- 析构函数
function LoginController:onDestory()
    print("LoginController:onDestory")

    removeMsgCallBack(self, MSG_SOCKET_CONNECTION_EVENT)
	
	TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_CreateAccountAck", handler(self, self.loginNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_SRP6_1_Ack", handler(self, self.loginNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_SRP6_3_Ack", handler(self, self.loginNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_LoginAck", handler(self, self.loginNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_CreateVisitorAccountAck", handler(self, self.loginNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_CreatePhoneNumAccountAck", handler(self, self.loginNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_GetNoticeSMSAck", handler(self, self.loginNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_ThirdPartyAccLogin_Ack", handler(self, self.loginNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_S2C_MaintanenceInfo_Nty", handler(self, self.loginNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_H2C_KickNotice_Nty", handler(self, self.lobbyNetMsgHandler))
    

    -- 大厅服
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_GetNoticeSMSAck", handler(self, self.lobbyNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_LoginAck", handler(self, self.lobbyNetMsgHandler))
    --TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_PortalList_Nty", handler(self, self.lobbyNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_BecomeRegularAccountAck", handler(self, self.lobbyNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_FuncSwitchNty", handler(self, self.lobbyNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_S2C_MaintanenceInfo_Nty", handler(self, self.lobbyNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_KickNotice_Nty", handler(self, self.lobbyNetMsgHandler))


    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_KickNty", handler(self, self.kickMsgHandler))


    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_MailBrief_Nty", handler(self, self.lobbyNetMsgHandler))


    TotalController:removeNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_CreateAccountAck")
    TotalController:removeNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_SRP6_1_Ack")
    TotalController:removeNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_SRP6_3_Ack")
    TotalController:removeNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_LoginAck")
	TotalController:removeNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_CreateVisitorAccountAck")
    TotalController:removeNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_CreatePhoneNumAccountAck")
    TotalController:removeNetMsgCallback(self, Protocol.LoginServer, "CS_GetNoticeSMSAck")
    TotalController:removeNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_ThirdPartyAccLogin_Ack")
	TotalController:removeNetMsgCallback(self, Protocol.LoginServer, "CS_S2C_MaintanenceInfo_Nty")
	TotalController:removeNetMsgCallback(self, Protocol.LoginServer, "CS_H2C_KickNotice_Nty")
	
    TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_GetNoticeSMSAck")
    TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_LoginAck")
    TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_BecomeRegularAccountAck")
    TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_FuncSwitchNty")
	TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_S2C_MaintanenceInfo_Nty")
	TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_KickNotice_Nty")
	TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_KickNty")

    TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_MailBrief_Nty")
end

-- 获取设备信息
-- @return (table): 设备信息
function LoginController:getDeviceInfo()
    print("*************设备信息**************")
    local terminalType = 2 --1是pc端,2是手机
    local system = 0
    if device.platform == "ios" then
        system = 1
    elseif device.platform == "android" then
        system = 2
    elseif device.platform == "winrt" or device.platform == "wp8" then
        system = 4
    end
    local strOSVersion = QkaPhoneInfoUtil:getSystemVersion()
    local nNetworkType = 0
    local netType = QkaPhoneInfoUtil:getNetType()
    if netType == "2g" then
        nNetworkType = 1
    elseif netType == "3g" then
        nNetworkType = 2
    elseif netType == "4g" then
        nNetworkType = 3
    end
    local nOperatorType = 0
    local channelID = GlobalConf.CHANNEL_ID
    local ClientVersion = "1"

    local targetPlatform = cc.Application:getInstance():getTargetPlatform()
    if (cc.PLATFORM_OS_IPHONE == targetPlatform) or (cc.PLATFORM_OS_IPAD == targetPlatform) then
        local ok, ret = LuaObjcBridge.callStaticMethod("MilaiPublicUtil","getAppData",nil)
        if not ok then
            print("luaoc getInstall error:"..ret)
        else
            if ret==nil or ret=="" then
                print("luaoc getInstall null")
            else
                print(ret)
                local result2 = require("framework.json").decode(ret)
                print(result2)
                local channelId = result2["channelId"]
                local promoterId = result2["promoterId"]
                self._promoterId = tonumber(promoterId)
                self._urlName = result2["locationUrl"]
                print("channelID==",channelId)
                print("promoterId==",self._promoterId)
            end
        end
    end

    if self._promoterId == nil then
        self._promoterId = 0
    end

    -- if self._promoterId==0 then
    --     self._urlName = GlobalConf.URL
    -- end

    if self._urlName==nil then
        self._urlName=""
    end

    if not self.deviceInfo then
        -- 2代表手机
        local callfunc = function(registrationId)
			local device = QkaPhoneInfoUtil:getIMEI()
			self.deviceInfo = {terminalType,device,QkaPhoneInfoUtil:getPhoneIP(), system, strOSVersion,nNetworkType, nOperatorType, QkaPhoneInfoUtil:getWifiName(), QkaPhoneInfoUtil:getPhoneType(),channelID,ClientVersion, 0, 0, self._promoterId, self._urlName, registrationId}
            dump(self.deviceInfo,"deviceInfo")
        end
        local registrationId = platformUtils.getUMengPushMessageToken()
        callfunc(registrationId)
    end
    return self.deviceInfo
end


function LoginController:addZero(str)
    local length = string.len(str)
    if length<40 then
        str = string.rep("0", 40-length) .. str
    end
    return str
end

--[[
--请求应用口令，应用口令用于钻石商场等身份验证
function LoginController:requestApplyToken()
    print("requestApplyToken")
    local m_nToServiceType = 1--token Auth
    local str = PSU[Protocol.LobbyServer]:makePacket("CS_C2A_GetApplyToken_Req", {}, false)
    local params = {m_nToServiceType,str}
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_TransMsg_Req", params)
    print("requestApplyTokenE")
end
--]]


--第三方账号切换账号
--[[
function g_ThirdSwitchAccount()

    print("*****g_ThirdSwitchAccount****")
    
    sendMsg( MSG_ADD_LOADING, 30,"登录中..." )
    self:ThirdChangeAccount()
end
--]]


--开始登录游戏 
--提供给andorid那边使用的全局函数
function g_ENTER_GAME()
    print("*****g_ENTER_GAME****")   
    sendMsg( MSG_ADD_LOADING, 30,"登录中..." )
    self:login()
end

--[[
function LoginController:checkLocalLoginInfor()
    if  cc.FileUtils:getInstance():isFileExist("gameState") then
    --读取旧平台的登陆账号和密码
        print("********读取旧平台的登陆账号和密码*********")
    
        local OldPlateLoginInfor = require("app.hall.base.cache.BeforeLoginData").new()
        local _acc = ""
        local _pwd = ""

        _acc, _pwd = OldPlateLoginInfor:getLastAccountAndLastPassWord()

        print("************最近登陆的账号*************")
        print(_acc)

        print("************最近登陆的密码*************")
        print(_pwd)
        
        
        local acc = self._gameDataCtl:getNewAccount()   
        acc.account = _acc
        acc.password = _pwd
        self._gameDataCtl:addAccData() -- 保存
        
        ToolKit:setLoginType(GlobalDefine.LoginType.qka)
        
        self.password = _pwd
        self:loginAccount(_acc) -- 开始登录一律用账号名登录
    else 
        self:registVisitorAccount() -- 游客注册
    end
end
--]]

--找回密码第一步，检查玩家输入的手机号
-- @param _phone(string)  :  手机号
function LoginController:findPwdCheckPhone(_phone)
    print("**********找回密码第一步************")
    self.checkphone = _phone
	if ConnectManager:isConnectSvr(Protocol.LoginServer) then
		self:onFindPwdCheckPhone()
	else
		self.accountCallBack = handler(self,self.onFindPwdCheckPhone)
		self:login()
	end
end

function LoginController:onFindPwdCheckPhone()
    ConnectManager:send2Server(Protocol.LoginServer, "CS_C2L_FindPassword_1_Req", {self.checkphone, GlobalConf.CHANNEL_ID })
end



--找回密码第二部，重设密码
-- @param _phone(string)   :  手机号
-- @param _newPwd(string)  :  新密码
-- @param _yzm(string)     :  验证码 
function LoginController:findPwdResetPwd(_phone,_newPwd,_yzm)

    print("**********找回密码第二步************")
    self.findPwdPhone = _phone
    self.findPwdNewPwd = _newPwd
    self.findPwdYzm = _yzm
    self:onFindpwdResetPwd()
end

function LoginController:onFindpwdResetPwd()
    ConnectManager:send2Server(Protocol.LoginServer, "CS_C2L_FindPassword_2_Req", {self.findPwdPhone,self.findPwdNewPwd, self.findPwdYzm})
end

function LoginController:onFindPwdAccount(account)
	self.findPwdAccount = account
end

function LoginController:saveFindPwd()
	self.account = self.findPwdAccount
    self.phone = self.findPwdPhone
    self.password = self.findPwdNewPwd
	self.logonType = "qka"
	
	print("LoginController:saveFindPwd", self.phone, self.password, self.account)
	
	local acc = self._gameDataCtl:getCurAccount()
	if not acc then
		--如果玩家未登陆任何账号， 直接就走忘记密码， 这里没有旧账号
		acc = self._gameDataCtl:getNewAccount()
	end
    acc.account = self.account
    acc.password = self.password
	acc.tell = self.phone
    self._gameDataCtl:addAccData() -- 保存
end

function LoginController:setPassword(id, pss)
    self.account = id
    self.password = pss
	self.logonType = "qka"
end

-- 获取注册信息
function LoginController:getOpenInstall(callBack)
    local function getInstallCallBack(result)
		print(result)
            local result2 = require("framework.json").decode(result)
            if result2 then
            print(result2)
            local channelId = result2["channelId"] or 0
            local promoterId = result2["promoterId"] or 0
            self._promoterId = tonumber(promoterId)
            self._urlName = result2["locationUrl"] or ""
            print("channelID==",channelId)
        end
        print("promoterId==",self._promoterId)
        callBack(result2 ~= nil)
    end

    openinstall:getInstall(10, getInstallCallBack)
end

return LoginController

