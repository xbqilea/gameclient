-- Author: 
-- Date: 2018-08-07 18:17:10
-- UserCenterController
local UserCenterController = class("UserCenterController")



function UserCenterController:ctor(__scene)
    self:myInit()
end

function UserCenterController:myInit()

    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_ModifyPassword_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_ModifyNickName_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_ChangeAvatar_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_ChangeAvatarFame_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_BindIDCard_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_BindPhoneNumAck", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_ExchangeRes2OldPlatform_1_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_ExchangeRes2OldPlatform_2_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GetExchangeRes2OldPlatformOrder_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_BindThirdPartyAcc_Ack", handler(self, self.netMsgHandler))


    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_FindPassword_1_Ack", handler(self, self.LoginNetMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_FindPassword_2_Ack", handler(self, self.LoginNetMsgHandler))
end

function UserCenterController:LoginNetMsgHandler( __idStr, __info )
    if  __idStr == "CS_L2C_FindPassword_1_Ack"  then    
        -- 找回密码响应
        print("CS_L2C_FindPassword_1_Ack")
        dump(__info)
		if __info.m_nRetCode == 0 then
			g_LoginController:onFindPwdAccount(__info.m_strAccountName)
		end
        sendMsg(MSG_FINDPWD_CHECK_PHONE_ASK, __info.m_nRetCode)
     
    elseif  __idStr == "CS_L2C_FindPassword_2_Ack"  then
              
        sendMsg(MSG_FINDPWD_RESET_ASK, __info.m_nRetCode)        
   end
end

function UserCenterController:netMsgHandler( __idStr, __info )
    if __idStr ==  "CS_H2C_BindIDCard_Ack" then
        --绑定身份证响应
        print("绑定身份证响应")
        dump(__info)
        sendMsg(MSG_BINDING_ID_SUCCES, __info.m_nRetCode )

    elseif __idStr == "CS_H2C_ChangeAvatar_Ack" then
        -- 修改头像
        print("修改头像回应")
        dump(__info)
        sendMsg(MSG_CHANGE_HEADICON_ASK, __info )
		
    elseif __idStr == "CS_H2C_ChangeAvatarFame_Ack" then
        -- 修改头像框
        print("修改头像框回应")
        dump(__info)
        sendMsg(MSG_CHANGE_HEADFRAME_ASK, __info )
		
    elseif __idStr == "CS_H2C_ModifyNickName_Ack" then
        -- 修改昵称
        print("修改昵称回应")
        dump(__info)
        sendMsg(MSG_CHANGE_NICKNAME_SUCCES, __info.m_nRetCode)

    elseif __idStr == "CS_H2C_ModifyPassword_Ack" then  -- 修改登录密码
        print("修改登录密码回应")
        dump(__info)
        if __info.m_nRetCode == 0 then
            local acc = g_LoginController._gameDataCtl:getCurAccount()
            acc.password = self._tempNewPwd
            g_LoginController._gameDataCtl:addAccData() -- 保存
        end
        sendMsg(MSG_CHANGE_LOGIN_PWD_SUCCES,  __info.m_nRetCode )
		
    elseif  __idStr == "CS_H2C_BindThirdPartyAcc_Ack" then
        print("***********绑定微信返回***********")
        dump(__info)
        if   __info.m_nResult == 0 then
			g_LoginController:onBindThirdPartyAccSucc(__info.m_strToken)
            if self.bindwxCallback then
                self.bindwxCallback(0)
            end
        elseif __info.m_nResult == -326 then
			if self.bindwxCallback then
				self.bindwxCallback(1)
            end
			sendMsg(MSG_SHOW_ERROR_TIPS,__info.m_nResult)
		else
			if self.bindwxCallback then
                self.bindwxCallback(__info.m_nResult)
            end
            sendMsg(MSG_SHOW_ERROR_TIPS,__info.m_nResult)
        end
    elseif  __idStr == "CS_L2C_FindPassword_1_Ack" then
        sendMsg(MSG_FINDBACK_PASSWORD,__info)
    elseif  __idStr == "CS_L2C_FindPassword_2_Ack" then
        sendMsg(MSG_FINDBACK_PASSWORDNEXT,__info)
    end

end


--请求类型 1-注册手机账号，2-绑定手机号 3-找回密码 4-解冻临时冻结 5-异常设备验证 6-修改银行密码 7-修改登录密码 8-锁定本机 9-解锁锁定本机，10-动态密码登录',
-- 转正和绑定手机用的一样的验证码  类型  :2 绑定手机号码
-- 现在这个接口暂时只支持 类型为1、2、6、7、
--获取短信验证码
-- @param _phone(string)  :  电话号码(必须确保电话号码是正常的)
-- @param _type(int)      :  类型
function UserCenterController:getSmsNotice(_phone, _type ) 
    if _type == 1  or  _type == 3 then

        --发给登录服
        g_LoginController:requestRegisterVerifyCode(_type, _phone, 2)

    elseif _type == 2 or _type == 6 or _type == 7   then  --

        -- 发给大厅服
        g_LoginController:requestRegisterVerifyCode(_type, _phone, 1)

    end
end

--切换账号，使用账号和密码, 切换成功后会发送消息   MSG_LOGIN_LOBBY_ASK
-- @param _account(string)  :  账号
-- @param _pwd(string)      :  密码
--[[
function UserCenterController:switchAccountByPwd(_account ,  _pwd)
    g_LoginController:switchAccount(_account,_pwd)
end
--]]

--绑定手机
-- @param _phone(string)   :  手机号
-- @param _yzm(string)     :  验证码
--[[
function UserCenterController:bindingPhone(_phone, _yzm)

    self._tempPhone = _phone
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_BindPhoneNumReq", {_phone, _yzm} )
end
--]]


--绑定身份证   消息响应： MSG_BINDING_ID_SUCCES
-- @param _id(string)     :  身份证号码
-- @param _name(string)     :  真实姓名
function UserCenterController:bindingID(_id,_name)

    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_BindIDCard_Req", {_id,_name} )
end


--注册手机账号
-- @param _phone(string)    :  电话号码
-- @param _pwd(string)      :  密码
-- @param _code(string)     :  验证码
function UserCenterController:registPhoneAccount(_phone, _pwd , _code )
    g_LoginController:registPhoneAccount( _phone, _pwd, "", _code )
end

--游客账号使用手机转正
-- @param _phone(string)    :  电话号码
-- @param _pwd(string)      :  密码
-- @param _code(string)     :  验证码
function UserCenterController:RegularAccountByPhone(_phone, _pwd , _code )
    g_LoginController:becomeRegularAccountReq(2, "", _phone ,_pwd,_code, Player:getParam( "NickName" ))
end


-- 修改登录密码  消息响应 ：MSG_CHANGE_LOGIN_PWD_SUCCES
-- @param _oldPwd(string)  :  旧密码
-- @param _newPwd(string)  :  新密码
-- @param _yzm(string)     :  验证码，  沒有的話使用""代替
function UserCenterController:changeLoginPwd(_oldPwd, _newPwd , _yzm )

    self._tempNewPwd = _newPwd
    if _yzm == "" then
        ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_ModifyPassword_Req", {_oldPwd, "", _newPwd  } )
    else
        --绑定了手机  用验证码修改
        ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_ModifyPassword_Req", {_oldPwd, _yzm, _newPwd   } )
    end
end


--查询玩家可兑换奖励   消息响应：MSG_CHECKOUT_REWARDS_SUCCESS
function UserCenterController:checkoutRewards()
    --查询可兑换的奖励
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_ExchangeRes2OldPlatform_1_Req", {})
end

--申请兑换奖励  消息响应：MSG_APPLY_EXCHANG_REWARDS_SUCCESS
function UserCenterController:applyExchangeRewards()
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_ExchangeRes2OldPlatform_2_Req", {})
end

--查询奖励兑换情况   消息响应：MSG_QUERY_REWARDS_STATUS_SUCCES
function UserCenterController:queryApplyRewards()

    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetExchangeRes2OldPlatformOrder_Req", { 1,999 })
end

--更换头像  消息响应： MSG_PLAYER_UPDATE_SUCCESS 来更新头像
-- @param _headIconId(int)      :  头像id
-- @param _optType(int)         :  更换类型 '0:正常更换 1:更新第三方头像'
-- @param _thirdType(int)       :  第三方类型  '第三方类型 '

-- CS_C2H_ChangeAvatar_Req = {
--     { 1     , 1     , 'm_nNewAvatarID'                  , 'UINT'        , 1 , '更换头像id'},
--     { 2     , 1     , 'm_nOptType'                      , 'SHORT'       , 1 , '0:正常更换 1:更新第三方头像'},
--     { 3     , 1     , 'm_nThirdType'                    , 'SHORT'       , 1 , 'platform_sdk表主键'},   
-- }
function UserCenterController:changeHeadIcon( _headIconId, _optType, _thirdType)

--    local optType = 0
--    local thirdType = 0
--
--    if ToolKit:getChannelType() ==  GlobalDefine.ChannelType.qka  then
--
--        if ToolKit:getLoginType() == GlobalDefine.LoginType.wx then
--            optType = 1
--            thirdType =   ToolKit:getThirdLoginType()
--
--        else
--            optType = 0
--            thirdType = 0
--        end
--
--    elseif ToolKit:getChannelType() == GlobalDefine.ChannelType.third   then
--        optType = 1
--        thirdType =   ToolKit:getThirdLoginType()
--    end

    if _optType == nil or _optType == 0 then
        ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_ChangeAvatar_Req", { tonumber(_headIconId) ,0 , 0  } )
    else
        ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_ChangeAvatar_Req", { tonumber(_headIconId) ,_optType , _thirdType  } )
    end

    
    
end
function UserCenterController:changeHeadFrame( _headFrameId )
     ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_ChangeAvatarFame_Req", { tonumber(_headFrameId)} ) 
end

--修改昵称  消息响应：MSG_CHANGE_NICKNAME_SUCCES
-- @param _nickname(STRING)  :  新昵称
-- @param _type(UINT)         :  '消耗道具ID 10000-金币 10001-钻石 10002-A币 其它正常道具id ',  U2A(_nickname)

function UserCenterController:changeNickname( _nickname, _type  )

    local stype = 10000
    if _type == 1  then
    
        stype = 10000
        
    elseif _type == 3 then 
    
        stype = 207
        
    elseif _type == 2 then 
    
        stype =  10001
        
    else
        stype =  _type
    end
    

    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_ModifyNickName_Req", { _nickname, stype } )
end


--绑定微信,不需要重连服务器
-- 0-成功   1-当前微信号已绑定其他账号  2-发生错误
function UserCenterController:bingdingWx(_callback)
    self.bindwxCallback = _callback
    local function callback( _result)
        if _result then
            print("获取授权信息成功")
            --获取授权信息成功
            local _openid = g_wxUserInfor["openid"]
            local _token = g_wxUserInfor["token"]
            local _refresh_token = g_wxUserInfor["refresh_token"]           
            local _jrt = toJson(  {refresh_token = _refresh_token  } )
            if device.platform == "android" then
                ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_BindThirdPartyAcc_Req", {_openid, 1 , _token, g_LoginController:getDeviceInfo(), _jrt})
            elseif device.platform == "ios"  then
                ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_BindThirdPartyAcc_Req", {_openid, 0 , _token, g_LoginController:getDeviceInfo(), _jrt})
            end
        else
            --获取授权信息失败
            print("获取授权信息失败")
            if self.bindwxCallback  then
                self.bindwxCallback(2)
            end
        end
    end

    ToolKit:getWechatAuthorizeCode(_callback)
end



--上传第三方头像  （微信头像）
--  _accId   账号id
--  _headId  头像id
-- _filepath 头像路径
function UserCenterController:updataThirdPartyHeadIcon( _accId, _headId, _filepath  )
    -- TOAST("开始上传头像")
    print("***********开始上传头像************")
    -- print("UserCenterModifyHeadicon:uploadImg = self.filePath",self.filePath)
    -- self:setFileName()
    local url = AliYunUtil.headPath .. _accId .. "/" .. _headId .. ".jpg"


    AliYunUtil:uploadFile(url, _filepath, handler(self, self.onUploadFileResult))
end

function UserCenterController:onUploadFileResult( result )
    print("UserCenterModifyHeadicon:onUploadFileResult = ",result)

    if tonumber(result) == 1 then
        print("********上传第三方头像成功**********")
        self:sendThirdPartyHeadInfoToSvr()
        -- if UpdateFunctions.isFileExist(headPath) then
        --    UpdateFunctions.removePath(headPath)
        -- end

    else
        TOAST(STR(88,1))
        print("头像上传失败,请稍后重试")
    end
end

--发送图片id到服务端
--CS_C2H_ChangeAvatar_Req = {
--    { 1     , 1     , 'm_nNewAvatarID'                  , 'UINT'        , 1 , '更换头像id'},
--    { 2     , 1     , 'm_nOptType'                      , 'SHORT'       , 1 , '0:正常更换 1:更新第三方头像'},
--    { 3     , 1     , 'm_nThirdType'                    , 'SHORT'       , 1 , 'platform_sdk表主键'},   
--}
function UserCenterController:sendThirdPartyHeadInfoToSvr()
    print("-----------headdialog:sendHeadInfoToSvr: " .. Player:getNextUploadAvatarID())

    local _thirdType = ToolKit:getThirdLoginType()
    if _thirdType then
        g_UserCenterController:changeHeadIcon( Player:getNextUploadAvatarID(),1,_thirdType)
    else
        TOAST("获取登录类型失败，无法更换头像！")
        print("***********获取登录类型失败，无法更换头像！**********")
    end
    
end



--找回密码第一步，检查玩家输入的手机号
-- @param _phone(string)  :  手机号
function UserCenterController:findPwdCheckPhone(_phone)

    g_LoginController:findPwdCheckPhone( _phone )
    
end


--找回密码第二部，重设密码
-- @param _phone(string)   :  手机号
-- @param _newPwd(string)  :  新密码
-- @param _yzm(string)     :  验证码 
function UserCenterController:findPwdResetPwd(_phone,_newPwd,_yzm)

    g_LoginController:findPwdResetPwd(_phone,_newPwd,_yzm)

end


return UserCenterController