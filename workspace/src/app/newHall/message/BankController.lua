--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- To change this template use File | Settings | File Templates.
-- 银行控制类

local BankController = class("BankController")

function BankController:ctor()
    self:myInit()
end

function BankController:myInit()
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_OperateBank_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_SetBankPasswd_Ack", handler(self, self.netMsgHandler))
end

--- 银行存款，不需密码
--- coin:金币
--- diamond:钻石
--- gameId:游戏id用于区分具体哪个游戏存款
function BankController:saveMoneyToBank(coin, diamond, gameId)
    local id = gameId or 0;
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_OperateBank_Req", {1, coin, diamond, "", id})
end

--- 银行取款，需要提供密码
--- coin:金币
--- diamond:钻石
--- password:密码
--- gameId:游戏id用于区分具体哪个游戏取款
function BankController:getMoneyFromBank(coin, diamond, password, gameId)
    local id = gameId or 0;
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_OperateBank_Req", {0, coin, diamond, password, id})
end

--- 设置银行密码
--- 或者修改银行密码
function BankController:setBankPassWd(mima, old)
    local str = old or ""
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_SetBankPasswd_Req", {mima, str})
end

function BankController:netMsgHandler( __idStr, __info )
    if __idStr == "CS_H2C_OperateBank_Ack" then
        sendMsg(MSG_BANK_UPDATE_SUCCESS, __info)
    elseif __idStr == "CS_H2C_SetBankPasswd_Ack" then
        sendMsg(MSG_SETBANK_PWD_SUCCESS, __info)
    end
end

--获取银行金币数
function BankController:getBankCoin()
    return tonumber(Player:getParam( "BankGoldCoin" ))
end

--获取银行钻石数
function BankController:getBankDiamond()
    return tonumber(Player:getParam( "BankDiamond" ))
end

return BankController