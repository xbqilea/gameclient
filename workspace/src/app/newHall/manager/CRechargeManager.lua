--region CRechargeManager.lua
--Date 2017.07.13.
--Auther JackyXu.
--Desc: 支付数据管理中心

local CommonUtils = require("common.public.CommonUtils")
local ClientConfig = require("common.public.ClientConfig")
local LuaNativeBridge = require("common.public.LuaNativeBridge")
local FloatMessage = require("common.layer.FloatMessage")
local Veil = require("common.layer.Veil")
local cjson = require("cjson")

local strPayType = { [0]="AppStore", [1]="AlipayWap", [2]="WeiXinWap", [3]="QQWap", [4]="BankWap",
                     [5]="JingDongWap", [6]="CardWap", [7]="YinYongBaoSDK", [8]="HuaWeiMarketSDK", [9]="AliMarketSDK", [10]="BankCloudWap" };


local CRechargeManager = class("CRechargeManager")

CRechargeManager.g_instance = nil
function CRechargeManager.getInstance()
    if not CRechargeManager.g_instance then
        CRechargeManager.g_instance = CRechargeManager:new()
    end
    return CRechargeManager.g_instance
end

function CRechargeManager:releaseInstance()
    CRechargeManager.g_instance = nil
end

function CRechargeManager:ctor()
    self.m_vecRechargeSort = {}
    self:clear()
end

function CRechargeManager:clear()
    self.m_bIsCharging = false
    self.m_strTcVersion = {}
    self.m_strVerifyOrderUrl = ""
    self.m_vecRechargeData = {}
    for i = 0, 10 do
        self.m_vecRechargeData[i] = {}
    end
    self.m_vecAgentInfo = {}
    --当前充值支持哪些类型
    self.m_pSupportChargeData = {}
    --记录一下上次请求充值接口的时间
    self.m_pSupportChargeTime = 0
end

--iDitchID  0:默认  1:支付宝  2:网银  3:点卡  4:手机快捷  5:appstore  6:微信
--7:GooglePlay  8:应用宝  9:QQ  10:京东  11:华为应用市场  12:阿里应用市场
function CRechargeManager:addRechargeInfo(info)
    local nDiType = {[0]=0,
    		        [1]=G_CONSTANTS.Recharge_Type.Type_Alipay,
    		        [2]=G_CONSTANTS.Recharge_Type.Type_Bank,
    		        [3]=G_CONSTANTS.Recharge_Type.Type_PointCard,
                    [4]=0,
                    [5]=0,
                    [6]=G_CONSTANTS.Recharge_Type.Type_WeChat,
                    [7]=0,
                    [8]=G_CONSTANTS.Recharge_Type.Type_YSDK,
                    [9]=G_CONSTANTS.Recharge_Type.Type_QQ,
                    [10]=G_CONSTANTS.Recharge_Type.Type_JingDong,
                    [11]=G_CONSTANTS.Recharge_Type.Type_HuaWei,
                    [12]=G_CONSTANTS.Recharge_Type.Type_AliMarket,
                    [13]=G_CONSTANTS.Recharge_Type.Type_YunShanFu, } --ditchID对应的客户端写的支付类型
    --防止没有的类型引起报错
    if nDiType[info.iDitchID] then
        local nType = nDiType[info.iDitchID]
        table.insert(self.m_vecRechargeData[nType], table.deepcopy(info))
    else
        print("出现了一种未知的支付方式=====>",info.iDitchID)
    end
end

function CRechargeManager:sortRechargeInfo()

    for i=1,#self.m_vecRechargeData do
        table.sort(self.m_vecRechargeData[i-1], function(info1, info2)
            return info1.dGiftLimit < info2.dGiftLimit
        end)
    end
    print("充值赠送排序")
end

function CRechargeManager:getRechargeDataNum(nType)
    
    local num = table.nums(self.m_vecRechargeData[nType])
    if num == 0 then 
        num = table.nums(self.m_vecRechargeData[0]) --使用默认的
    end
    return num
end

function CRechargeManager:getRechargeInfoAtIndex(nType,nIndex)
    
    if table.nums(self.m_vecRechargeData[nType]) == 0 then
        return self.m_vecRechargeData[0][nIndex],true --使用默认的
    end
    return self.m_vecRechargeData[nType][nIndex],false
end

function CRechargeManager:getRechargeGiftMoney(dMoney, fRate)
    
    return dMoney * fRate / 100
end

function CRechargeManager:getRechargeRate(dMoney)
    
    local num = #self.m_vecRechargeData[0]
    if num == 0 then 
        return 0
    end
    if dMoney <= self.m_vecRechargeData[0][1].dGiftLimit then
        return self.m_vecRechargeData[0][1].iGiftRate
    end
    if dMoney >= self.m_vecRechargeData[0][num].dGiftLimit then
        return self.m_vecRechargeData[0][num].iGiftRate
    end
    for i=1, num-1 do
        local info1 = self.m_vecRechargeData[0][i]
        local info2 = self.m_vecRechargeData[0][i+1]
        if dMoney >= info1.dGiftLimit and dMoney < info2.dGiftLimit then
            return self.m_vecRechargeData[0][i].iGiftRate
        end
    end
    return 0
end

function CRechargeManager:checkVersion(strVer, type)
    if type < 1 or type > 2 then
        return false
    end
    
    if self.m_strTcVersion[type] then
        return false;
    end

    local vecStrings = string.split(self.m_strTcVersion[type], "#")
    for _, str in ipairs(vecStrings) do
        if str == strVer then
            return true;
        end
    end
    
    return false;
end


function CRechargeManager:getAgentInfoCount()
    return #self.m_vecAgentInfo;
end

function CRechargeManager:addAgentInfo(info)
    local account = info.account;
    for index, pAgentInfo in ipairs(self.m_vecAgentInfo) do
        if pAgentInfo.type == 0 then
            return
        end
        if pAgentInfo.type == info.type and account == pAgentInfo.account then
            return
        end
    end
    self.m_vecAgentInfo[#self.m_vecAgentInfo+1] = info
end

function CRechargeManager:getAgentInfoAtIndex(nIndex)
    local info = {};
    if nIndex <= #self.m_vecAgentInfo then
        info = self.m_vecAgentInfo[nIndex];
    end
    return info;
end

function CRechargeManager:clearAgentInfo()
    self.m_vecAgentInfo = {};
end


--[[
 -----下单------
 userID
 money
 couponID   代金卷ID
 payType     支付类型：WeiXinWap、AlipayWap、AppStore,CardWap
 orderNo     可为空
 productNo  AppStore内购项
 version        客户端版本号
 ditchNumber   客户端渠道号
 clientKind         客户端类型,3-IOS,4-android
 orderExtra        扩展信息，原应用宝使用
 ]]

 --目前只有第三方sdk充值使用
function CRechargeManager:CreatedOrder(payType, money, couponID, orderExtra)

    local userID = PlayerInfo.getInstance():getUserID()
    local version = CommonUtils.getInstance():formatAppVersion()
    local ditchNumber = CommonUtils.getInstance():getAppChannel()
    local clientKind = CommonUtils.getInstance():getPlatformType()
    local productNo = "";
    local apiName = ""
    local resChannel = ClientConfig.getInstance():getResChannel()
    if resChannel == 11 then 
        apiName = "YingYongBao-1"
    elseif resChannel == 12 then
        apiName = "YingYongBao-2"
    end

    local strPostData = string.format("userID=%d&money=%f&couponID=%d&payType=%s&productNo=%s&version=%d&ditchNumber=%d&clientKind=%d&orderExtra=%s&apiName=%s", 
                                       userID, money, couponID, strPayType[payType], productNo, version, ditchNumber, clientKind, orderExtra, apiName);
    print("strPostData =", strPostData);

    -- 加密处理x
    local strDataEncrypt = SLUtils:aes256Encrypt(strPostData)
    print("strDataEncrypt:"..strDataEncrypt)
    local strPostDataEncrypt = "data="..strDataEncrypt
    print("strPostDataEncrypt = ", strPostDataEncrypt)
    
    -- 应用宝
    if payType == G_CONSTANTS.Recharge_Type.Type_YSDK then 
        self:HttpRequestOrder(strPostDataEncrypt)
    end

    return strPostDataEncrypt
end

function CRechargeManager:HttpRequestOrder(strPostDataEncrypt) 

    local url = ClientConfig:getInstance():getCreateOrderUrl()
    local strUrl = string.format("%s?%s", url, strPostDataEncrypt)
    print("\n\n(1)HttpRequestOrder(1):"..strUrl.."\n")
    local goNext = true
    local xhr = cc.XMLHttpRequest:new()
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON 
    xhr.timeout = 5
    xhr:open("GET", strUrl)
    local function onReadyStateChange()
        print("Http Status Code:"..xhr.status)
        cc.exports.Veil:getInstance():HideVeil(VEIL_LOCK)
        if(xhr.status == 200) then
            local response = xhr.response
            if(response ~= nil) then
                print("Http Response:"..response)
                local strResult = SLUtils:aes256Uncrypt(response)
                print("strResult:"..strResult)
                local httpStartPos,httpEndPos = string.find(strResult, "{\"")
                if httpStartPos == 1 then 
                    -- 解析json数据
                    local vecStrings = string.split(strResult, "}}") --如此骚操作是暂时为了解决aes256解析不准确问题
                    if vecStrings[1] == nil then 
                        return 
                    end
                    local strResultJson = table.nums(vecStrings)>1 and tostring(vecStrings[1]).."}}" or tostring(vecStrings[1])
                    print("strResultJson:"..strResultJson)
                    local jsonConf = cjson.decode(strResultJson)
                    local status = jsonConf.status
                    if tonumber(status) == 1 then 
                        --下单成功
                        local dicData = jsonConf.data.orderData
                        self.m_strVerifyOrderUrl = jsonConf.data.returnURL
                        if dicData ~= nil then
                            --if ClientConfig.getInstance():getIsYsdkChannel() then
                                local orderNo = dicData.OrderNo
                                local orderAmount = dicData.OrderAmount*10
                                local urlParams = dicData.paymentOrderData.url_params
                                LuaNativeBridge.getInstance():otherSdkRecharge(orderNo,urlParams)
--                            elseif resChannel == G_CONSTANTS.ResChannelType.RES_325HuaWei
--                                or resChannel == G_CONSTANTS.ResChannelType.RES_325AliMarket then
--                                local orderAmount = dicData.OrderAmount
--                                local paymentOrderData = table_to_jsonstr(dicData.paymentOrderData)
--                                print("paymentOrderData:"..paymentOrderData)
--                                LuaNativeBridge.getInstance():YsdkRecharge(paymentOrderData,orderAmount)
                            --end   
                        end
                    else 
                        local faileMsg = jsonConf.msg
                        FloatMessage.getInstance():pushMessage("下单失败："..faileMsg)
                    end
                else
                    FloatMessage.getInstance():pushMessage("STRING_046_2")
                end
            else
                FloatMessage.getInstance():pushMessage("STRING_046_2")
            end
        else
            FloatMessage.getInstance():pushMessage("STRING_046_2")
        end
        xhr:unregisterScriptHandler()
        print("\n\n(1)HttpRequestOrder(2):"..strUrl.."\n")
    end
    xhr:registerScriptHandler(onReadyStateChange) -- 注册脚本方法回调
    xhr:send()-- 发送
end

--[[
    ------------------------------------------------------
    支付方式：
    1.payHtml不为空,用base64解密,保存本地为html,打开本地html,跳转支付
    2.payUrl不为空,不用sdk解密,打开url进行支付
    3.payUrl不为空,apiName:ShanFuJuHe,使用sdk解密,打开url进行支付(2.2.0新增）
    ------------------------------------------------------
    错误消息：
    1.下单失败（20001）：返回数据不是json（服务器返回错误）
    2.下单失败（20002）：直接提示（服务器传过来）
    3.下单失败（20003）：保存网页数据到本地失败（本地写入文件失败）
    4.下单失败（20004）：使用闪付聚合，但没有接入sdk（服务器返回错误）
    5.下单失败（20005）：未知的支付方式（服务器返回错误）
    6.下单失败（20006）：返回数据为空（服务器返回空）
    7.下单失败（20007）：请求失败（检查网络）
    ------------------------------------------------------
    --参数
    payType   ：支付方式
    money     ：支付金额
    couponID  ：代金卷ID，暂时没有用，默认0
    orderExtra：扩展信息，原应用宝使用，暂时没有用，默认""
    product：AppStore内购项,默认"" (月卡类型)
    ------------------------------------------------------
--]]
function CRechargeManager:CreatedOrder2(payType, money, couponID, orderExtra,product)

    local userID      = PlayerInfo.getInstance():getUserID()
    local version     = CommonUtils.getInstance():formatAppVersion()
    local ditchNumber = CommonUtils.getInstance():getAppChannel()
    local clientKind  = CommonUtils.getInstance():getPlatformType()
    local productNo   = product or ""
    
    --9个参数
    local strUserID      = string.format("userID=%d", userID)                --玩家ID
    local strMoney       = string.format("&money=%f", money)                 --订单金额
    local strCoupon      = string.format("&couponID=%d", couponID)           --代金卷ID,默认0
    local strType        = string.format("&payType=%s", strPayType[payType]) --支付类型：0-AppStore/1-AlipayWap/2-WeiXinWap/3-QQWap/4-BankWap/5-JingDongWap/6-CardWap
    local strProductID   = string.format("&productNo=%s", productNo)         --AppStore内购项,默认""
    local strVersion     = string.format("&version=%d", version)             --客户端版本号
    local strDitchNumber = string.format("&ditchNumber=%d", ditchNumber)     --客户端渠道号
    local strClientKind  = string.format("&clientKind=%d", clientKind)       --客户端类型：3-IOS/4-android
    local strOrderExtra  = string.format("&orderExtra=%s", orderExtra)       --扩展信息，原应用宝使用

    --加密订单数据
    local strOrderData   = string.format("%s%s%s%s%s%s%s%s%s", strUserID, strMoney, strCoupon, strType, strProductID, strVersion, strDitchNumber, strClientKind, strOrderExtra)
    local strOrderData2  = SLUtils:aes256Encrypt(strOrderData)

    --加密sdk数据
    local strMetaData    = CommonUtils.getInstance():getRechargeMetaData()
    local strMetaData2   = string.urlencode(strMetaData)

    --提交的数据
    local strPostData    = string.format("data=%s&metadata=%s", strOrderData2, strMetaData2)
    print("strOrderData:", strOrderData)
    print("strOrderDataEncrypt:", strOrderData2)
    print("strMetaData:", strMetaData)
    print("strMetaDataEncrypt:", strMetaData2)
    print("===== strPostData =====", strPostData)

    --url
    local strUrl  = ClientConfig.getInstance():getCreateOrderUrl()
    local strTime = string.format("game_timestamp=%d", os.time())
    local url = string.format("%s/v/2?%s", strUrl, strTime) --/v/2写在代码里
    print("url", url)

    --保存html数据
    local function saveToHtml(data)
        local fullName = cc.FileUtils:getInstance():getWritablePath() .. "recharge.html"
        
        local fp = io.open(fullName, "wb") --重新写入文件
        if fp then
            fp:write(data)
            fp:close()
            print("----- saveToHtml ----- success", fullName)
            return true, fullName
        end
        print("----- saveToHtml ----- fail", fullName)
        return false, fullName
    end

    --开启遮罩
    Veil.getInstance():ShowVeil(VEIL_LOCK)

    --发起http请求
    local xhr = cc.XMLHttpRequest:new()
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON 
    xhr.timeout      = 20 --超时时间
    xhr:registerScriptHandler(function() -- 注册脚本方法回调
        --关闭遮罩
        Veil.getInstance():HideVeil(VEIL_LOCK)

        print("Http Status Code:", xhr.status)
        if xhr.status == 200 then
            if xhr.response then
                --加密数据
                print("response", xhr.response)

                --出错信息
                if string.find(xhr.response, "</html>") then
                    FloatMessage.getInstance():pushMessage("下单失败（20001）")
                    print("下单失败（20001）", "xhr.response", xhr.response)
                    return
                end

                --解密数据
                local strResult = SLUtils:aes256Uncrypt(xhr.response)
                print("response uncrypt", strResult)
                
                --转换成json
                local jsonData = cjson.decode(strResult)
                if jsonData == nil then
                    FloatMessage.getInstance():pushMessage("下单失败（20001）")
                    print("下单失败（20001）", "strResult", strResult)
                    return
                end

                --{
                --	"status": 1,
                --	"msg": "\u521b\u5efa\u8ba2\u5355\u6210\u529f",
                --	"data": {
                --		"OrderNo": "SR11811200000033",
                --		"UserID": 1,
                --		"Accounts": "MACH003",
                --		"OrderAmount": 50,
                --		"OrderStatus": 0,
                --		"ClientKind": 3,
                --		"paymentOrderData": [],
                --		"productNo": "",
                --		"apiName": "ShanFuJuHe",     // 如果等于“ShanFuJuHe”，用闪付聚合的SDK解密URL。 
                --		"payUrl": "alipays:\/\/platformapi\/startapp?appId=20000067&url=https://qr.alipay.com/fkx07301fikhtf4xvbooae3?t=1542698612067",
                --		"payHtml": "",
                --		"visibility": 1, // 是否立即显示WebView
                --		"OpenInSysBrowser": 1, // 是否调用系统浏览器打开payUrl
                --	}
                --}
                --判断方式及优先级：
                --    1). 若是指定了SDK支付（如应用宝、阿里市场、华为市场），自行决定传参吊起支付。
                --    2). 若payHtml不为空，使用payHtml 创建临时html文件并webview打开；
                --    3). 
                --        - 根据apiName判断是否闪付聚合，如果是则调用SDK解密payUrl
                --        - 判断payUrl是否有效，无效则报错。
                --        - 判断OpenInSysBrowser=1则调用系统浏览器打开， 否则内部webview打开
                --        - 如果否则内部webview打开， 判断visibility=1则立即显示webview
                --}

                --打印返回数据
                --visibility: 0 // 客户端是否立即显示WebView窗口。0隐藏（跳不动再显示），1直接显示 
                dump(jsonData)

                --下单错误
                if jsonData.status == 0 then
                    FloatMessage.getInstance():pushMessage("下单失败（20002）：" .. jsonData.msg)
                    print("下单失败（20002）", jsonData.status, jsonData.msg)
                
                --下单成功
                else

                    --1.payHtml不为空,用base64解密,保存本地为html,打开本地html,跳转支付
                    if string.len(jsonData.data.payHtml) > 0 then

                        print("下单成功", " 1.payHtml不为空")

                        --解密html
                        local crypto = require("cocos.crypto")
                        local html = crypto.decodeBase64(jsonData.data.payHtml)
                        print("=====html=====", html)

                        --保存html
                        local bSuccess, fullName = saveToHtml(html)

                        --保存失败
                        if bSuccess == false then
                            FloatMessage.getInstance():pushMessage("下单失败（20003）")
                            print("下单失败(20003)", "保存网页数据失败")
                            return
                        end

                        --去打开网页
                        local payUrl = string.format("file://%s", fullName)
                        local showWeb = jsonData.data.visibility
                        local webData = string.format("%d;%s;%s", payType, payUrl, showWeb)
                        print("webData", webData)
                        SLFacade:dispatchCustomEvent(Public_Events.MSG_RECHARGE_WEB, webData)

                    --2.payUrl不为空
                    elseif string.len(jsonData.data.payUrl) > 0 then

                        --2.1.解密闪付payUrl
                        if jsonData.data.apiName == "ShanFuJuHe" then

                            --没有闪付sdk
                            if CommonUtils.getInstance():suppoerRechargeShanFu() == false then
                                FloatMessage.getInstance():pushMessage("下单失败（20004）")
                                print("下单失败（20004）", "版本号", CommonUtils.getInstance():formatAppVersion())
                                return
                            end
                            
                            jsonData.data.payUrl = LuaNativeBridge:getRechargeUrl(jsonData.data.payUrl)
                            print("解密闪付payUrl", jsonData.data.payUrl)
                        end

                        --2.2.调用外部浏览器打开
                        if tonumber(jsonData.data.OpenInSysBrowser) == 1 then
                            print("调用外部浏览器打开", jsonData.data.payUrl)
                            LuaNativeBridge:getInstance():openURL(jsonData.data.payUrl)
                            return
                        end

                        --2.3.调用内部webview打开
                        if tonumber(jsonData.data.OpenInSysBrowser) == 0 then
                            local payUrl = jsonData.data.payUrl
                            local showWeb = jsonData.data.visibility
                            local webData = string.format("%d;%s;%s", payType, payUrl, showWeb)
                            print("调用内部webview打开", webData)
                            SLFacade:dispatchCustomEvent(Public_Events.MSG_RECHARGE_WEB, webData)
                            return
                        end

                    else --意外的支付方式
                        FloatMessage.getInstance():pushMessage("下单失败（20005）")
                        print("下单失败（20005）", "未知的支付方式")
                    end
                end

            else --返回数据为空
                FloatMessage.getInstance():pushMessage("下单失败（20006）")
                print("下单失败（20006）", "返回数据为空", "xhr.response", xhr.response)
            end

        else --请求失败
            FloatMessage.getInstance():pushMessage("下单失败（20007）")
            print("下单失败（20007）", "请求失败", "xhr.status", xhr.status)
        end
    end)
    xhr:open("POST", url)
    xhr:setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
    xhr:send(strPostData)
end

--设置当前支持的充值类型数据
function CRechargeManager:setSupportChargeData(data)
     table.insert(self.m_pSupportChargeData,data)
end

--判断某个充值类型的充值金额是否可以充值
function CRechargeManager:judgeChargeByMoney(pay_type,pay_money)
    for k,v in pairs(self.m_pSupportChargeData) do
        if strPayType[pay_type] == v.szPaymentType then --支付类型一样
            local support_money_list = v.szSupportMoneyList
            --在支持的金额中寻找 是否支持我当前充值的金额(现在只有cash)
            local all_support_count = 0
            for k,temp_data in pairs(support_money_list) do
                all_support_count = all_support_count + #temp_data
            end

            if all_support_count > 0 then
                for m,temp_data in pairs(support_money_list) do
                    for x,money in pairs(temp_data) do
                        if money == pay_money then
                            return true
                        end
                    end
                end
            else
                --如果在支持的金额中没有，判断是否在最大最小金额之间
                if pay_money >= v.dwMinAmount and pay_money <= v.dwMaxAmount then
                    return true
                end
            end
        end
    end
    return false
end

--某个充值类型支持的最小金额和最大金额
function CRechargeManager:getTypeSupportMinAndMax(pay_type)
    for k,v in pairs(self.m_pSupportChargeData) do
        if strPayType[pay_type] == v.szPaymentType then --支付类型一样
            return v.dwMinAmount,v.dwMaxAmount
        end
    end
    return 0,0
end

function CRechargeManager:requestSupportCharge(is_in_game)
    local now_time = os.time()
    --判断一下距离上次请求的时间是否超过5分钟
    if (now_time - self.m_pSupportChargeTime) < 300 or is_in_game then 
        SLFacade:dispatchCustomEvent(Public_Events.MSG_SUPPORT)
        return 
    end
    self.m_pSupportChargeTime = now_time
    -- --大厅请求
    -- if (cc.exports.isConnectHall() and cc.exports.isInHall() and PlayerInfo.getInstance():getUserID() > 0) then
    --     CMsgHall:sendNullData(G_C_CMD.MDM_SYS_PAYGIFT, G_C_CMD.SUB_GP_PAYAPI_GET)
    -- --游戏里请求
    -- elseif (cc.exports.isConnectGame() and cc.exports.isInGame() and PlayerInfo.getInstance():getEnterGame()) then
    --     CMsgGame:sendNullData(G_C_CMD.MDM_GR_USER, G_C_CMD.SUB_GR_USER_PAYAPI_REQ)
    -- end

    --清空支付接口数据
    self.m_pSupportChargeData = {}
    CMsgHall:sendNullData(G_C_CMD.MDM_SYS_PAYGIFT, G_C_CMD.SUB_GP_PAYAPI_GET)

end

function CRechargeManager:getCanChargeByType(charge_type)
    local result_value = 0
    local change_len = 8 --只显示前8个充值数据
    for i = 1, change_len do
        local rechargeData,is_default = self:getRechargeInfoAtIndex(charge_type, i)
        if rechargeData == nil or  rechargeData.dGiftLimit == nil or rechargeData.dGiftLimit == 0 then
            result_value = result_value + 1
        else
            if not is_default then --如果不是使用的默认值
                local temp_show = self:judgeChargeByMoney(charge_type,rechargeData.dGiftLimit) 
                if not temp_show then --如果当前的金额是不能充值的
                    result_value = result_value + 1
                end
            end
        end
    end
    
    return result_value == change_len    

end

function CRechargeManager:setRechargeSort(data)
    self.m_vecRechargeSort = data
end

function CRechargeManager:getRechargeSort()
    return self.m_vecRechargeSort
end

cc.exports.CRechargeManager = CRechargeManager
return CRechargeManager
