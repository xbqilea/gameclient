-- region *.lua
-- Date	 2017.04.11
-- zhiyuan
-- 此文件由[BabeLua]插件自动生成

-- 定时器


local GameSceneDataMgr = class("GameSceneDataMgr")

-- 创建实例接口
local instance = nil
function GameSceneDataMgr.getInstance()
	if instance == nil then
		instance = GameSceneDataMgr:new()
	end
	return instance
end  


function GameSceneDataMgr:ctor()

    self.m_cbGameStatus = 0
    self.mAllowLookon = false 
    self.mCurTbale = 0
    self.mTbaleStatus = 0

    self:Clean()
end

function GameSceneDataMgr:Clean()

    self.m_cbGameStatus = G_CONSTANTS.GAME_STATUS_FREE 
    self.mAllowLookon = false 
    self.mCurTbale = 0
    self.mTbaleStatus = 0
end 

function GameSceneDataMgr:setGameStatus(GameStatus)			-- 当前状态
       self.m_cbGameStatus = GameStatus
end

function GameSceneDataMgr:getGameStatus()			
       return self.m_cbGameStatus 
end

function GameSceneDataMgr:setAllowLookon(AllowLookon)			-- 允许旁观
       self.mAllowLookon = AllowLookon
end

function GameSceneDataMgr:getAllowLookon()			
       return self.mAllowLookon
end

function GameSceneDataMgr:setCurTable(CurTable)			-- 桌子id
       self.mCurTbale = CurTable
end

function GameSceneDataMgr:getCurTable()			
       return self.mCurTbale
end

function GameSceneDataMgr:setTableStatus(TableStatus)	       -- 桌子状态
      self.mTbaleStatus = TableStatus
end

function GameSceneDataMgr:getTableStatus()	       
      return self.mTbaleStatus
end

cc.exports.GameSceneDataMgr = GameSceneDataMgr

return cc.exports.GameSceneDataMgr

-- endregion
