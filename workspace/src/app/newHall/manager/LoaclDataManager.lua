--region *.lua
--Date
--
--endregion

LocalDataManager = class("LocalDataManager")

local cjson = require("cjson")

LocalDataManager.g_instance = nil

local ORDER_FILE_NAME = "OrderOpenUrl.json"

function LocalDataManager.getInstance()
    if not LocalDataManager.g_instance then
        LocalDataManager.g_instance = LocalDataManager:new()
    end
    return LocalDataManager.g_instance
end

function LocalDataManager.releaseInstance()
    LocalDataManager.g_instance = nil
end

function LocalDataManager:ctor()

    self.m_vecOrderData = {}
end

function LocalDataManager:saveOrderFile()

    local writablePath =cc.FileUtils:getInstance():getWritablePath()
    local filename = writablePath..ORDER_FILE_NAME
    local data = self.m_vecOrderData
    print("filename",filename)  
    if table.nums(data) == 0  or filename == nil then 
        return
    end 
    local f = io.open(filename, "wb+")
    if f == nil then  
        error(("Unable to write '%s': %s"):format(filename, err))  
        return
    end 
    data = cjson.encode(data)  
    print(data)
    f:write(data) 
    if f ~= nil then  
        f:close()  
    end  
end

function LocalDataManager:getOrderData()

    local writablePath = cc.FileUtils:getInstance():getWritablePath()
    local filename = writablePath..ORDER_FILE_NAME
    if not cc.FileUtils:getInstance():isFileExist(filename) then 
        return 
    end

    local data =cc.FileUtils:getInstance():getStringFromFile(filename) 
    print(data)
    self.m_vecOrderData = cjson.decode(tostring(data))
    print(self.m_vecOrderData)
end

function LocalDataManager:addOrderData(url)

    if url == nil or string.len(url) == 0 then 
        return
    end
    table.insert(self.m_vecOrderData, 1, url)
    if table.nums(self.m_vecOrderData) >= 100 then 
        table.remove(self.m_vecOrderData,table.nums(self.m_vecOrderData))
    end
    self:saveOrderFile()
end

function LocalDataManager:checkRepeatData(url)

    if url == nil or string.len(url) == 0 then 
        return false
    end
    for i=1, table.nums(self.m_vecOrderData) do
        if self.m_vecOrderData[i] == url then 
            return true
        end
    end
    return false
end

return LocalDataManager