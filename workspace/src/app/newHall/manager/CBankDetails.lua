--region *.lua
--Date
--
--endregion

local CBankDetails = class("CBankDetails")

CBankDetails.g_release = nil

function CBankDetails.getInstance()
    if not CBankDetails.g_release then
        CBankDetails.g_release = CBankDetails:new()
    end
    return CBankDetails.g_release
end

function CBankDetails:ctor()
    self:Clear()
end

function CBankDetails:Clear()
    self.m_vecBankDetails = {}
    self.m_vecBankTransfer = {}

    self:setBankDetailsTotal(0)
    self:setShowPropertyType(0)
    self:setBankDetailsTransfer(0)
    self:setBankDetailsTransferIn(0)
    self:setBankDetailsTransferOut(0)

    self:setExchargeDetail({})
    self:setExchargeQueryTime(0)

    self:setProxyScoreIn(0)
    self:setProxyScoreOut(0)
    self:setUserScoreIn(0)
    self:setUserScoreOut(0)
    self:setRefundDetail({})
    self:setRefundDetailsTotal(0)
end

function CBankDetails:setBankDetailsTotal(_num)
    self.m_iBankDetailsTotal = _num
end

function CBankDetails:getBankDetailsTotal(_num)
    return self.m_iBankDetailsTotal
end

function CBankDetails:setShowPropertyType(_num)
    self.m_nShowPropertyType = _num  
end

function CBankDetails:getShowPropertyType(_num)
    return self.m_nShowPropertyType
end

function CBankDetails:setBankDetailsTransfer(_num)
    self.m_iBankTransfersTotal = _num 
end

function CBankDetails:getBankDetailsTransfer(_num)
    return self.m_iBankTransfersTotal
end

function CBankDetails:setBankDetailsTransferIn(_num)
    self.m_iBankTransfersTotalIn = _num
end

function CBankDetails:getBankDetailsTransferIn(_num)
    return self.m_iBankTransfersTotalIn
end

function CBankDetails:setBankDetailsTransferOut(_num)
    self.m_iBankTransfersTotalOut = _num
end

function CBankDetails:getBankDetailsTransferOut(_num)
    return self.m_iBankTransfersTotalOut
end

function CBankDetails:getBankDetailsCount()
    return #self.m_vecBankDetails
end

function CBankDetails:getBankTransferCount()
    return #self.m_vecBankTransfer
end

function CBankDetails:getBankDetailsInfoAtIndex(_index)
    if _index and _index <= #self.m_vecBankDetails then
        return self.m_vecBankDetails[_index]
    end
    local record = {};
    return record
end

function CBankDetails:getBankTransferInfoAtIndex(_index)
    if _index and _index <= #self.m_vecBankTransfer then
        return self.m_vecBankTransfer[_index]
    end
    local record = {};
    return record
end

function CBankDetails:addBankDetailsInfo(info)
    if info.byTradeType == 3 then
        for k,v in pairs(self.m_vecBankTransfer) do
            if v and v.iRecordID == info.iRecordID then
                return
            end
        end
        table.insert(self.m_vecBankTransfer,info)
    end
    
    --1为存 2为取 3为转 4为赠送 5为充值 6推广返利，其他类型不添加到银行明细
    if info.byTradeType == 1 or info.byTradeType == 2 or info.byTradeType == 3 or info.byTradeType == 4 or info.byTradeType == 5 or info.byTradeType == 6 then
        for i = 1, #self.m_vecBankDetails do
            local m_info = self.m_vecBankDetails[i]
            if m_info and m_info.iRecordID == info.iRecordID then
                return
            end
        end
        table.insert(self.m_vecBankDetails,info)
    end
end

function CBankDetails:CleanTransferLog()
    self.m_vecBankTransfer = {}
end

function CBankDetails:sort()
    local len = #self.m_vecBankDetails
    local function bubble_sort()
        local temp = {}
        for i = 1, (len-1) do
             for j = 1,(len - i) do
                if self.m_vecBankDetails[j].iRecordID < self.m_vecBankDetails[j +1].iRecordID then
                    temp = self.m_vecBankDetails[j]
                    self.m_vecBankDetails[j] = self.m_vecBankDetails[j +1]
                    self.m_vecBankDetails[j +1] = temp
                end
             end
        end 
    end
    bubble_sort()
end

---------------------------
--提现记录
function CBankDetails:setExchargeDetail(msg)
    self.m_vecExchargeDetail = msg
end
function CBankDetails:getExchargeDetail()
    return self.m_vecExchargeDetail
end
function CBankDetails:getExchargeDetailByIndex(index)
    return self.m_vecExchargeDetail[index]
end
function CBankDetails:getExchargeDetailCount()
    return #self.m_vecExchargeDetail
end
function CBankDetails:setExchargeQueryTime(time)
    self.m_lExchargeQuerryTime = time
end
function CBankDetails:getExchargeQueryTime()
    return self.m_lExchargeQuerryTime
end
---------------------------
-----领取税收返利记录-------
function CBankDetails:setProxyScoreIn(score)
    self.m_llProxyScoreIn = score
end

function CBankDetails:getProxyScoreIn()
    return self.m_llProxyScoreIn
end

function CBankDetails:setProxyScoreOut(score)
    self.llProxyScoreOut = score
end

function CBankDetails:getProxyScoreOut()
    return self.llProxyScoreOut
end

function CBankDetails:setUserScoreIn(score)
    self.m_llUserScoreIn = score
end

function CBankDetails:getUserScoreIn()
    return self.m_llUserScoreIn
end

function CBankDetails:setUserScoreOut(score)
    self.llUserScoreOut = score
end

function CBankDetails:getUserScoreOut()
    return self.llUserScoreOut
end

function CBankDetails:setRefundDetail(t)
    self.m_vecRefundDetail = t
end

function CBankDetails:getRefundDetailCount()
    return table.nums(self.m_vecRefundDetail)
end

function CBankDetails:addRefundDetail(info)
    table.insert(self.m_vecRefundDetail,info)
end

function CBankDetails:getRefundDetailByIndex(index)
    return self.m_vecRefundDetail[index]
end

function CBankDetails:setRefundDetailsTotal(_num)
    self.m_iRefundDetailsTotal = _num 
end

function CBankDetails:getRefundDetailsTotal(_num)
    return self.m_iRefundDetailsTotal
end

-------------
return CBankDetails