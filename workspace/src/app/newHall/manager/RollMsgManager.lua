--CRollMsgManager *.lua
--Date:2018.10.22
--author: ouyangfeng
--系统滚动消息manger

local RollMsgManager = class("RollMsgManager")

RollMsgManager.g_instance = nil
function RollMsgManager.getInstance()
    if RollMsgManager.g_instance == nil then
        RollMsgManager.g_instance = RollMsgManager:new()
    end
    return RollMsgManager.g_instance
end

function RollMsgManager.releaseInstance()
    RollMsgManager.g_instance = nil
end

function RollMsgManager:ctor()
    self.m_listMsg = {}
end

function RollMsgManager:Clear()
    self.m_listMsg = {}
end

function RollMsgManager:getRollMsgNums()

    return table.nums(self.m_listMsg) 
end

function RollMsgManager:addRollMsg(info,isFirst)
    info.bShow = false
    if isFirst then
        table.insert(self.m_listMsg, 1, info)
    else
        table.insert(self.m_listMsg, info)
    end
end

function RollMsgManager:getRollMsgToShow()
    for k, v in pairs( self.m_listMsg ) do
        if v~=nil and (not v.bShow) then
            v.bShow = true
            return v
        end
    end
    return nil 
end
function RollMsgManager:ResetRollMsgShowState()
    for k, v in pairs( self.m_listMsg ) do
      if v ~= nil  then
          v.bShow = false
      end
    end
end
function RollMsgManager:removeRollMsg( msg )
    for k, v in pairs( self.m_listMsg ) do
        if msg == v then
            table.remove(self.m_listMsg, k)
            break
        end
    end
end

return RollMsgManager