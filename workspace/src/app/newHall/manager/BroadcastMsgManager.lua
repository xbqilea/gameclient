--CBroadcastManager *.lua
--Date:2018.10.22
--author: ouyangfeng
--系统滚动消息manger

local BroadcastMsgManager = class("BroadcastMsgManager")

BroadcastMsgManager.g_instance = nil
function BroadcastMsgManager.getInstance()
    if BroadcastMsgManager.g_instance == nil then
        BroadcastMsgManager.g_instance = BroadcastMsgManager:new()
    end
    return BroadcastMsgManager.g_instance
end

function BroadcastMsgManager.releaseInstance()
    BroadcastMsgManager.g_instance = nil
end

function BroadcastMsgManager:ctor()
    self.m_listMsg = {}
    self.num = 0
end

function BroadcastMsgManager:Clear()
    self.m_listMsg = {}
end

function BroadcastMsgManager:getBroadcastMsgNums()

    return table.nums(self.m_listMsg) 
end

function BroadcastMsgManager:addBroadcastMsg(info)
    self.num = self.num + 1
    info.bShow = false
    table.insert(self.m_listMsg, info)
end

function BroadcastMsgManager:getBroadcastMsgToShow()
    for k, v in pairs( self.m_listMsg ) do
        if v~=nil and (not v.bShow) then
            v.bShow = true
            return v
        end
    end
    return nil 
end
function BroadcastMsgManager:ResetBroadcastMsgShowState()
    for k, v in pairs( self.m_listMsg ) do
      if v ~= nil  then
          v.bShow = false
      end
    end
end
function BroadcastMsgManager:removeBroadcastMsg( msg )
    for k, v in pairs( self.m_listMsg ) do
        if msg == v then
            table.remove(self.m_listMsg, k)
            break
        end
    end
end

return BroadcastMsgManager