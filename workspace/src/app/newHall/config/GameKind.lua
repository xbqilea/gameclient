local GameKind = 
{
    {
        OpenGame = true,        --是否开放
        GameName = "飞禽走兽",  --游戏名
        KindID = 201,           --游戏id
        Classify = 1,           --游戏类型：1多人；2街机；3捕鱼；4对战
        BackToGame = false,     --登录时是否拉回游戏
        BackToHall = false,     --重连时是否返回大厅
        Orientation = 1,        --屏幕方向：1横屏；2竖屏
        AnimationPath = "hall/effect/gamelist/gamekind_201/gamekind_201.ExportJson", --游戏动画
        AnimationName = "gamekind_201",                 --动画动作
        TitlePath = "game/animals/gui/title_201.png",   --游戏标题
        RoomPath = "game/animals/gui/gui-room-%d.png",  --入场图片
        RulePath = "game/animals/gui/rule_201.png",     --规则图片
        RoomItemWidth = 310,    --宽
        RoomItemHeight = 380,   --高
        RoomItemDiffX = 0,      --左右偏移量
        RoomItemDiffY = 0,      --上下偏移量
        TableType = 0,          -- 选桌类型：0.不选桌/1.选桌
        Margin = 20,            -- 列表 cell 间隔
        LeftMargin = 0,         -- 列表左侧空白区域大小
        RightMargin = 10,       -- 列表右侧空白区域

        EnterBackGround = 2,    --进入后台方式：1.删界面/2.不删界面/3.同2
        EnterForeGround = 2,    --返回前台方式：1.显示加载/2.不显示加载/3.同2
        EnterGameSuccess = 2,   --返回登录成功：1.加载游戏/2.删除游戏,重新加载/3.不删游戏,重置游戏
        EnterNetworkFail = 2,   --断网处理方式：1.退出大厅/2.进入后台,等待重连成功/3.同2

        IphoneXDesign = 1,      --是否全面屏适配:0未适配/1已适配

        ----BroadcastData = {667, 670, 667, {0x3d, 0x31, 0x61}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)
        RollMsgData = {667, 690, 420, 36}
    },
    {  
        OpenGame = true,
        GameName = "九线拉王",
        KindID = 202,
        Classify = 2,
        BackToGame = false, --登录时是否拉回游戏
        BackToHall = false, --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "hall/effect/gamelist/gamekind_202/gamekind_202.ExportJson",
        AnimationName = "gamekind_202",
        TitlePath = "game/tiger/gui/title_202.png",
        RoomPath = "game/tiger/gui/gui-room-%d.png",
        RulePath = "game/tiger/gui/rule-202.png",
        RoomItemWidth = 300,
        RoomItemHeight = 380,
        RoomItemDiffX = 0,
        RoomItemDiffY = 0,
        TableType = 0,
        Margin = 20,
        LeftMargin = 0,
        RightMargin = 10,

        EnterBackGround = 2,    --进入后台方式：1.删界面/2.不删界面/3.同2
        EnterForeGround = 2,    --返回前台方式：1.显示加载/2.不显示加载/3.同2
        EnterGameSuccess = 2,   --返回登录成功：1.加载游戏/2.删除游戏,重新加载/3.不删游戏,重置游戏
        EnterNetworkFail = 2,   --断网处理方式：1.退出大厅/2.进入后台,等待重连成功/3.同2

        IphoneXDesign = 1,      --是否全面屏适配

        JoinActivity = 1,       --是否参加集字符活动

        --BroadcastData = {667, 670, 667, {0x50, 0x23, 0x54}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)
        RollMsgData = {667-139, 660, 352, 36}
    },
    {
        OpenGame = true,
        GameName = "摇一摇",
        KindID = 203,
        Classify = 1,
        BackToGame = false, --登录时是否拉回游戏
        BackToHall = false, --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "hall/effect/gamelist/gamekind_203/gamekind_203.ExportJson",
        AnimationName = "gamekind_203",
        TitlePath = "game/dice/gui/title_203.png",
        RoomPath = "game/dice/gui/gui-room-%d.png",
        RulePath = "game/dice/gui/rule_203.png",     --规则图片
        RoomItemWidth = 300,
        RoomItemHeight = 380,
        RoomItemDiffX = 0,
        RoomItemDiffY = 0,
        TableType = 0,
        Margin = 20,
        LeftMargin = 0,
        RightMargin = 10,

        EnterBackGround = 2,    --进入后台方式：1.删界面/2.不删界面/3.同2
        EnterForeGround = 2,    --返回前台方式：1.显示加载/2.不显示加载/3.同2
        EnterGameSuccess = 2,   --返回登录成功：1.加载游戏/2.删除游戏,重新加载/3.不删游戏,重置游戏
        EnterNetworkFail = 2,   --断网处理方式：1.退出大厅/2.进入后台,等待重连成功/3.同2

        IphoneXDesign = 1,      --是否全面屏适配

        --BroadcastData = {667, 630, 597, {0x3d, 0x31, 0x61}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)
        RollMsgData = {667, 728, 450, 36}
    },
    {
        OpenGame = false,
        GameName = "街机跑马",
        KindID = 204,
        Classify = 1,
        BackToGame = false,
        BackToHall = false,
        Orientation = 1,
        AnimationPath = "",
        AnimationName = "",
        TitlePath = "",
        RoomPath = "",
        RulePath = "",
        RoomItemWidth = 300,
        RoomItemHeight = 380,
        RoomItemDiffX = 0,
        RoomItemDiffY = 0,
        TableType = 0,
        Margin = 20,
        LeftMargin = 0,
        RightMargin = 10,

        EnterBackGround = 2,    --进入后台方式
        EnterForeGround = 2,    --返回前台方式
        EnterGameSuccess = 2,   --返回登录成功
        EnterNetworkFail = 2,   --断网处理方式

        IphoneXDesign = 1,      --是否全面屏适配
    },
    {
        OpenGame = true,
        GameName = "百人牛牛",
        KindID = 205,
        Classify = 1,
        BackToGame = false, --登录时是否拉回游戏
        BackToHall = false, --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "hall/effect/gamelist/gamekind_205/gamekind_205.ExportJson",
        AnimationName = "gamekind_205",
        TitlePath = "game/handredcattle/gui/title_205.png",
        RoomPath = "game/handredcattle/gui/gui-room-%d.png",
        RulePath = "game/handredcattle/gui/rule_205.png",     --规则图片
        RoomItemWidth = 280,
        RoomItemHeight = 460,
        RoomItemDiffX = 0,
        RoomItemDiffY = 0,
        TableType = 0,
        Margin = 47,
        LeftMargin = 13,
        RightMargin = 20,

        EnterBackGround = 2,    --进入后台方式：1.删界面/2.不删界面/3.同2
        EnterForeGround = 2,    --返回前台方式：1.显示加载/2.不显示加载/3.同2
        EnterGameSuccess = 2,   --返回登录成功：1.加载游戏/2.删除游戏,重新加载/3.不删游戏,重置游戏
        EnterNetworkFail = 2,   --断网处理方式：1.退出大厅/2.进入后台,等待重连成功/3.同2

        IphoneXDesign = 1,      --是否全面屏适配

        BroadcastData = {667, 620, 667, {0x54, 0x23, 0x24}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)
        RollMsgData = {667, 750-130, 450, 36}
    },
    {
        OpenGame = true,
        GameName = "水浒传",
        KindID = 206,
        Classify = 2,
        BackToGame = false, --登录时是否拉回游戏
        BackToHall = false, --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "",
        AnimationName = "hall/effect/gamelist/gamekind_206/gamekind_206.ExportJson",
        TitlePath = "game/watermargin/gui/title_206.png",
        RoomPath = "game/watermargin/gui/gui-room-%d.png",
        RulePath = "game/watermargin/gui/rule-206.png",
        RoomItemWidth = 320,
        RoomItemHeight = 340,
        RoomItemDiffX = 0,
        RoomItemDiffY = 10,
        TableType = 0,
        Margin = 5,
        LeftMargin = 5,
        RightMargin = 12,

        EnterBackGround = 2,    --进入后台方式
        EnterForeGround = 2,    --返回前台方式
        EnterGameSuccess = 2,   --返回登录成功
        EnterNetworkFail = 2,   --断网处理方式

        IphoneXDesign = 1,      --是否全面屏适配

        JoinActivity = 1,       --是否参加集字符活动

        --BroadcastData = {667, 630, 667, {0x73, 0x31, 0x34}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)
        RollMsgData = {667, 727, 450, 36}
    },
    {
        OpenGame = true,
        GameName = "奔驰宝马",
        KindID = 207,
        Classify = 1,
        BackToGame = false, --登录时是否拉回游戏
        BackToHall = false, --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "hall/effect/gamelist/gamekind_207/gamekind_207.ExportJson",
        AnimationName = "gamekind_207",
        TitlePath = "game/car/gui/title_207.png",
        RoomPath = "game/car/gui/gui-room-%d.png",
        RulePath = "game/car/gui/rule_207.png",     --规则图片
        RoomItemWidth = 280,
        RoomItemHeight = 350,
        RoomItemDiffX = 0,
        RoomItemDiffY = 8,
        TableType = 0,
        Margin = 45,
        LeftMargin = 0,
        RightMargin = 11,

        EnterBackGround = 2,    --进入后台方式：1.删界面/2.不删界面/3.同2
        EnterForeGround = 2,    --返回前台方式：1.显示加载/2.不显示加载/3.同2
        EnterGameSuccess = 2,   --返回登录成功：1.加载游戏/2.删除游戏,重新加载/3.不删游戏,重置游戏
        EnterNetworkFail = 2,   --断网处理方式：1.退出大厅/2.进入后台,等待重连成功/3.同2

        IphoneXDesign = 1,      --是否全面屏适配

        --BroadcastData = {667, 720, 667, {0x3d, 0x31, 0x61}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)
        RollMsgData = {667, 725, 450, 36}
    },
    {
        OpenGame = true,
        GameName = "百家乐",
        KindID = 208,
        Classify = 1,
        BackToGame = false, --登录时是否拉回游戏
        BackToHall = false, --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "hall/effect/gamelist/gamekind_208/gamekind_208.ExportJson",
        AnimationName = "gamekind_208",
        TitlePath = "game/baccarat/gui/title_208.png",
        RoomPath = "game/baccarat/gui/gui-room-%d.png",
        RulePath = "game/baccarat/gui/rule_208.png",     --规则图片
        RoomItemWidth = 280,
        RoomItemHeight = 380,
        RoomItemDiffX = 0,
        RoomItemDiffY = 0,
        TableType = 0,
        Margin = 46,
        LeftMargin = 25,
        RightMargin = 10,

        EnterBackGround = 3,    --进入后台方式:1.删界面/2.不删界面/3.不删界面
        EnterForeGround = 3,    --返回前台方式:1.显示加载/2.不显示加载/3.等待重连
        EnterGameSuccess = 3,   --返回登录成功:1.加载游戏/2.删除游戏,重新加载/3.不删游戏,重置游戏
        EnterNetworkFail = 3,   --断网处理方式:1.退出大厅/2.进入后台,等待重连成功/3.同2

        IphoneXDesign = 1,      --是否全面屏适配

        --BroadcastData = {667, 630, 600, {0x54, 0x23, 0x24}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)
        RollMsgData = {667+50, 750-132, 352, 36}
    },
    {  
        OpenGame = true, 
        GameName = "铃铛游戏",
        KindID = 209,
        Classify = 2,
        BackToGame = false, --登录时是否拉回游戏
        BackToHall = false, --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "hall/effect/gamelist/gamekind_209/gamekind_209.ExportJson",
        AnimationName = "gamekind_209",
        TitlePath = "game/fruit/gui/title_209.png",
        RoomPath = "game/fruit/gui/gui-room-%d.png",
        RulePath = "game/fruit/gui/rule-209.png",
        RoomItemWidth = 300,
        RoomItemHeight = 380,
        RoomItemDiffX = 0,
        RoomItemDiffY = 0,
        TableType = 0,
        Margin = 20,
        LeftMargin = 0,
        RightMargin = 10,

        EnterBackGround = 2,    --进入后台方式：1.删界面/2.不删界面/3.同2
        EnterForeGround = 2,    --返回前台方式：1.显示加载/2.不显示加载/3.同2
        EnterGameSuccess = 2,   --返回登录成功：1.加载游戏/2.删除游戏,重新加载/3.不删游戏,重置游戏
        EnterNetworkFail = 2,   --断网处理方式：1.退出大厅/2.进入后台,等待重连成功/3.同2

        IphoneXDesign = 1,      --是否全面屏适配

        --BroadcastData = {667, 646, 667, {0x80, 0x68, 0x8f}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)
        RollMsgData = {667, 725, 450, 36}
    },
    {
        OpenGame = true,
        GameName = "刮一刮",
        KindID = 210,
        Classify = 5,
        BackToGame = false, --登录时是否拉回游戏
        BackToHall = true, --重连时是否返回大厅
        Orientation = 1,  --刮一刮在选择界面去旋转 这里不设置
        AnimationPath = "",
        AnimationName = "",
        TitlePath = "game/tickets/gui/title_210.png",
        RoomPath = "game/tickets/gui/gui-room-%d.png",
        RulePath = "",
        RoomItemWidth = 285,
        RoomItemHeight = 380,
        RoomItemDiffX = 0,
        RoomItemDiffY = 0,
        TableType = 0,
        Margin = 46,
        LeftMargin = 25,
        RightMargin = 10,
	
		EnterBackGround = 1,    --进入后台方式
        EnterForeGround = 1,    --返回前台方式
        EnterGameSuccess = 1,   --返回登录成功
        EnterNetworkFail = 1,   --断网处理方式

        IphoneXDesign = 1,      --是否全面屏适配

        BroadcastData = {375, 1190, 667, {0xff, 0xff, 0xff}},
        RollMsgData  = {375, 1188, 400, 36 },
    },
    {
        OpenGame = true,
        GameName = "寻宝乐园",
        KindID = 212,
        Classify = 2,
        BackToGame = false, --登录时是否拉回游戏
        BackToHall = false, --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "hall/effect/gamelist/gamekind_212/gamekind_212.ExportJson",
        AnimationName = "gamekind_212",
        TitlePath = "game/diamondDrag/gui/title_212.png",
        RoomPath = "game/diamondDrag/gui/gui-room-%d.png",
        RulePath = "game/diamondDrag/gui/rule_212.png",     --规则图片
        RoomItemWidth = 320,
        RoomItemHeight = 330,
        RoomItemDiffX = 0,
        RoomItemDiffY = 10,
        TableType = 0,
        Margin = 5,
        LeftMargin = 17,
        RightMargin = 12,

        EnterBackGround = 2,    --进入后台方式
        EnterForeGround = 2,    --返回前台方式
        EnterGameSuccess = 2,   --返回登录成功
        EnterNetworkFail = 2,   --断网处理方式

        IphoneXDesign = 1,      --是否全面屏适配

        --BroadcastData = {667, 686, 667, {0xff, 0xff, 0xff}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)
        RollMsgData = {660, 750-75, 400, 36}
    },
    {
        OpenGame = false,
        GameName = "红黑大战",
        KindID = 213,
        Classify = 1,
        BackToGame = false, --登录时是否拉回游戏
        BackToHall = false, --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "hall/effect/gamelist/gamekind_213/gamekind_213.ExportJson",
        AnimationName = "gamekind_213",
        TitlePath = "game/redvsblack/gui/title_213.png",
        RoomPath = "game/redvsblack/gui/gui-room-%d.png",
        RulePath = "game/redvsblack/gui/rule_213.png",     --规则图片,
        RoomItemWidth = 280,
        RoomItemHeight = 380,
        RoomItemDiffX = 0,
        RoomItemDiffY = 25,
        TableType = 0,
        Margin = 5,
        LeftMargin = 11,
        RightMargin = 12,

        EnterBackGround = 2,    --进入后台方式：1.删界面/2.不删界面/3.同2
        EnterForeGround = 2,    --返回前台方式：1.显示加载/2.不显示加载/3.同2
        EnterGameSuccess = 2,   --返回登录成功：1.加载游戏/2.删除游戏,重新加载/3.不删游戏,重置游戏
        EnterNetworkFail = 2,   --断网处理方式：1.退出大厅/2.进入后台,等待重连成功/3.同2

        IphoneXDesign = 1,      --是否全面屏适配

        BroadcastData = {667, 580, 667, {0x2f, 0x28, 0x20}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)
        RollMsgData = {667, 750-168, 400, 36}
    },
    {
        OpenGame = true,
        GameName = "红包扫雷",
        KindID = 217,
        Classify = 1,
        BackToGame = false, --登录时是否拉回游戏
        BackToHall = false, --重连时是否返回大厅
        Orientation = 2,
        AnimationPath = "hall/effect/gamelist/gamekind_217/gamekind_217.ExportJson",
        AnimationName = "gamekind_217",
        TitlePath = "game/RedEnvelopGambling/gui/title_217.png",
        RoomPath = "game/RedEnvelopGambling/gui/gui-room-%d.png",
        RulePath = "game/RedEnvelopGambling/gui/rule_217.png",     --规则图片,
        RoomItemWidth = 280,
        RoomItemHeight = 380,
        RoomItemDiffX = 0,
        RoomItemDiffY = 25,
        TableType = 0,
        Margin = 5,
        LeftMargin = 11,
        RightMargin = 12,

        EnterBackGround = 2,    --进入后台方式：1.删界面/2.不删界面/3.同2
        EnterForeGround = 2,    --返回前台方式：1.显示加载/2.不显示加载/3.同2
        EnterGameSuccess = 2,   --返回登录成功：1.加载游戏/2.删除游戏,重新加载/3.不删游戏,重置游戏
        EnterNetworkFail = 2,   --断网处理方式：1.退出大厅/2.进入后台,等待重连成功/3.同2

        IphoneXDesign = 1,      --是否全面屏适配

        BroadcastData = {375, -50, 667, {0xff, 0xff, 0xff}},
        RollMsgData  = {375, 50, 395, 36 },
    },
    {
        OpenGame = true,
        GameName = "龙虎斗",
        KindID = 215,
        Classify = 1,
        BackToGame = false, --登录时是否拉回游戏
        BackToHall = false, --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "hall/effect/gamelist/gamekind_215/gamekind_215.ExportJson",
        AnimationName = "gamekind_215",
        TitlePath = "game/longhudazhan/gui/title_215.png",
        RoomPath = "game/longhudazhan/gui/gui-room-%d.png",
        RulePath = "game/longhudazhan/gui/rule_215.png",     --规则图片,
        RoomItemWidth = 280,
        RoomItemHeight = 380,
        RoomItemDiffX = 0,
        RoomItemDiffY = 25,
        TableType = 0,
        Margin = 5,
        LeftMargin = 11,
        RightMargin = 12,

        EnterBackGround = 2,    --进入后台方式：1.删界面/2.不删界面/3.同2
        EnterForeGround = 2,    --返回前台方式：1.显示加载/2.不显示加载/3.同2
        EnterGameSuccess = 2,   --返回登录成功：1.加载游戏/2.删除游戏,重新加载/3.不删游戏,重置游戏
        EnterNetworkFail = 2,   --断网处理方式：1.退出大厅/2.进入后台,等待重连成功/3.同2

        IphoneXDesign = 1,      --是否全面屏适配

        BroadcastData = {667, 600, 667, {0x3d, 0x31, 0x61}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)
        RollMsgData = {667, 600, 450, 36}
    },
    {   
        OpenGame = true,
        GameName = "森林舞会",
        KindID = 216,
        Classify = 1,
        BackToGame = false, --登录时是否拉回游戏
        BackToHall = false,  --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "",
        AnimationName = "",
        TitlePath = "game/diamondDrag/gui/title_216.png",
        RoomPath = "game/diamondDrag/gui/gui-room-%d.png",
        RulePath = "",
        RoomItemWidth = 300,
        RoomItemHeight = 450,
        RoomItemDiffX = 0,
        RoomItemDiffY = 25,
        TableType = 0,
        Margin = 5,
        LeftMargin = 11,
        RightMargin = 12,

        EnterBackGround = 2,    --进入后台方式
        EnterForeGround = 2,    --返回前台方式
        EnterGameSuccess = 2,   --返回登录成功
        EnterNetworkFail = 2,   --断网处理方式

        IphoneXDesign = 1,      --是否全面屏适配
        RollMsgData = {667-390, 725, 420, 36}
    },
    {   
        OpenGame = true,
        GameName = "皇家跑马",
        KindID = 219,
        Classify = 1,
        BackToGame = false, --登录时是否拉回游戏
        BackToHall = false,  --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "",
        AnimationName = "",
        TitlePath = "",
        RoomPath = "",
        RulePath = "",
        RoomItemWidth = 300,
        RoomItemHeight = 450,
        RoomItemDiffX = 0,
        RoomItemDiffY = 25,
        TableType = 0,
        Margin = 5,
        LeftMargin = 11,
        RightMargin = 12,

        EnterBackGround = 2,    --进入后台方式
        EnterForeGround = 2,    --返回前台方式
        EnterGameSuccess = 2,   --返回登录成功
        EnterNetworkFail = 2,   --断网处理方式

        IphoneXDesign = 1,      --是否全面屏适配
        RollMsgData = {224, 425, 370, 32}, --x,y,w,h
        RollMsgDataIphoneX = {154, 425, 424, 32},
        RollMsgIMG = {"game/newhorse/images/horse-laba.png","game/newhorse/images/horse-laba-bg.png"},
    },
    {
        OpenGame = true,
        GameName = "大闹天宫",
        KindID = 352,
        Classify = 3,
        BackToGame = false, --登录时是否拉回游戏
        BackToHall = true,  --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "hall/effect/gamelist/gamekind_352/gamekind_352.ExportJson",
        AnimationName = "gamekind_352",
        TitlePath = "game/monkeyfish/gui/title_352.png",
        RoomPath = "game/monkeyfish/gui/gui-room-%d.png",
        RulePath = "game/monkeyfish/gui/rule_352.png",     --规则图片
        RoomItemWidth = 300,
        RoomItemHeight = 380,
        RoomItemDiffX = 0,
        RoomItemDiffY = 10,
        TableType = 0,
        Margin = 23,
        LeftMargin = 0,
        RightMargin = 20,

        EnterBackGround  = 1,    --进入后台方式
        EnterForeGround  = 1,    --返回前台方式
        EnterGameSuccess = 1,   --返回登录成功
        EnterNetworkFail = 1,   --断网处理方式

        IphoneXDesign = 1,      --是否全面屏适配

        JoinActivity = 1,       --是否参加集字符活动

        JoinFire = 1, --是否参与无限火力

        --BroadcastData = {667, 656, 667, {0x00, 0x32, 0x60}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)
        RollMsgData = {667, 750-21, 280, 36}
    },
    {
        OpenGame = true,
        GameName = "金蟾捕鱼",
        KindID = 361,
        Classify = 3,
        BackToGame = false, --登录时是否拉回游戏
        BackToHall = true, --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "hall/effect/gamelist/gamekind_361/gamekind_361.ExportJson",
        AnimationName = "gamekind_361",
        TitlePath = "game/frogfish/gui/title_361.png",
        RoomPath = "game/frogfish/gui/gui-room-%d.png",
        RulePath = "game/frogfish/gui/rule_361.png",     --规则图片
        RoomItemWidth = 300,
        RoomItemHeight = 380,
        RoomItemDiffX = 0,
        RoomItemDiffY = 10,
        TableType = 0,
        Margin = 23,
        LeftMargin = 0,
        RightMargin = 20,

        EnterBackGround  = 1,   --进入后台方式：1.删界面/2.不删界面/3.同2
        EnterForeGround  = 1,   --返回前台方式：1.显示加载/2.不显示加载/3.同2
        EnterGameSuccess = 1,   --返回登录成功：1.加载游戏/2.删除游戏,重新加载/3.不删游戏,重置游戏
        EnterNetworkFail = 1,   --断网处理方式：1.退出大厅/2.进入后台,等待重连成功/3.同2

        IphoneXDesign = 1,      --是否全面屏适配

        JoinActivity = 1,       --是否参加集字符活动

        JoinFire = 1, --是否参与无限火力

        --BroadcastData = {667, 656, 667, {0x00, 0x32, 0x60}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)
        RollMsgData = {667, 750-21, 280, 36}
    },
    {
        OpenGame = true,
        GameName = "李逵捕鱼",
        KindID = 362,
        Classify = 3,
        BackToGame = false, --登录时是否拉回游戏
        BackToHall = true,  --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "hall/effect/gamelist/gamekind_362/gamekind_362.ExportJson",
        AnimationName = "gamekind_362",
        TitlePath = "game/lkfish/gui/title_362.png",
        RoomPath = "game/lkfish/gui/gui-room-%d.png",
        RulePath = "game/lkfish/gui/rule_362.png",     --规则图片
        RoomItemWidth = 300,
        RoomItemHeight = 380,
        RoomItemDiffX = 0,
        RoomItemDiffY = 10,
        TableType = 0,
        Margin = 23,
        LeftMargin = 0,
        RightMargin = 20,

        EnterBackGround = 1,    --进入后台方式
        EnterForeGround = 1,    --返回前台方式
        EnterGameSuccess = 1,   --返回登录成功
        EnterNetworkFail = 1,   --断网处理方式

        IphoneXDesign = 1,      --是否全面屏适配

        JoinActivity = 1,       --是否参加集字符活动

        JoinFire = 1, --是否参与无限火力

        --BroadcastData = {667, 656, 667, {0x00, 0x32, 0x60}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)
        RollMsgData = {667, 750-21, 280, 36}
    },
    {
        OpenGame = true,
        GameName = "寻龙夺宝",
        KindID = 363,
        Classify = 3,
        BackToGame = false, --登录时是否拉回游戏
        BackToHall = true,  --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "hall/effect/gamelist/gamekind_363/gamekind_363.ExportJson",
        AnimationName = "gamekind_363",
        TitlePath = "game/deepfish/gui/title_363.png",
        RoomPath = "game/deepfish/gui/gui-room-%d.png",
        RulePath = "game/deepfish/gui/rule_363.png",     --规则图片
        RoomItemWidth = 300,
        RoomItemHeight = 380,
        RoomItemDiffX = 0,
        RoomItemDiffY = 10,
        TableType = 0,
        Margin = 23,
        LeftMargin = 0,
        RightMargin = 20,

        EnterBackGround = 1,    --进入后台方式：1.删界面/2.不删界面/3.同2
        EnterForeGround = 1,    --返回前台方式：1.显示加载/2.不显示加载/3.同2
        EnterGameSuccess = 1,   --返回登录成功：1.加载游戏/2.删除游戏,重新加载/3.不删游戏,重置游戏
        EnterNetworkFail = 1,   --断网处理方式：1.退出大厅/2.进入后台,等待重连成功/3.同2

        IphoneXDesign = 1,      --是否全面屏适配

        JoinActivity = 1,       --是否参加集字符活动

        JoinFire = 1, --是否参与无限火力

        --BroadcastData = {667, 630, 667, {0x00, 0x32, 0x60}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)
        RollMsgData = {667, 750-58, 350, 36}
    },
    {
        OpenGame = true,
        GameName = "经典斗地主",
        KindID = 401,
        Classify = 4,
        BackToGame = true,  --登录时是否拉回游戏
        BackToHall = false, --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "hall/effect/gamelist/gamekind_401/gamekind_401.ExportJson",
        AnimationName = "gamekind_401",
        TitlePath = "game/lord/gui/title_401.png",
        RoomPath = "game/lord/gui/gui-room-%d.png",
        RulePath = "game/lord/gui/rule_401.png",     --规则图片
        RoomItemWidth = 320,
        RoomItemHeight = 400,
        RoomItemDiffX = 0,
        RoomItemDiffY = 20,
        TableType = 0,
        Margin = 8,
        LeftMargin = 19,
        RightMargin = 19,

        EnterBackGround = 2,    --进入后台方式：1.删界面/2.不删界面/3.同2
        EnterForeGround = 2,    --返回前台方式：1.显示加载/2.不显示加载/3.同2
        EnterGameSuccess = 2,   --返回登录成功：1.加载游戏/2.删除游戏,重新加载/3.不删游戏,重置游戏
        EnterNetworkFail = 2,   --断网处理方式：1.退出大厅/2.进入后台,等待重连成功/3.同2

        IphoneXDesign = 1,      --是否全面屏适配

        BroadcastData = {667, 620, 667, {0x80, 0x68, 0x8f}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)
        RollMsgData = {667, 605, 450, 36}
    },
    {
        OpenGame = true,
        GameName = "二人牛牛",
        KindID = 402,
        Classify = 4,
        BackToGame = true,   --登录时是否拉回游戏
        BackToHall = false,  --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "hall/effect/gamelist/gamekind_402/gamekind_402.ExportJson",
        AnimationName = "gamekind_402",
        TitlePath = "game/twoniuniu/gui/title_402.png",
        RoomPath = "game/twoniuniu/gui/gui-room-%d.png",
        RulePath = "game/twoniuniu/gui/rule_402.png",     --规则图片
        RoomItemWidth = 330,
        RoomItemHeight = 350,
        RoomItemDiffX = 0,
        RoomItemDiffY = 5,
        TableType = 1,
        Margin = 0,
        LeftMargin = 19,
        RightMargin = 19,

        EnterBackGround = 2,    --进入后台方式
        EnterForeGround = 2,    --返回前台方式
        EnterGameSuccess = 2,   --返回登录成功
        EnterNetworkFail = 2,   --断网处理方式

        IphoneXDesign = 1,      --是否全面屏适配

        --BroadcastData  = {667, 716, 667, {0x54, 0x23, 0x24}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)
        --BroadcastData2 = {667, 595, 667, {0x54, 0x23, 0x24}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)/选桌
        RollMsgData = {667, 728, 450, 36}
    },
    {
        OpenGame = true,
        GameName = "欢乐五张",
        KindID = 403,
        Classify = 4,
        BackToGame = true,  --登录时是否拉回游戏
        BackToHall = false, --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "hall/effect/gamelist/gamekind_403/gamekind_403.ExportJson",
        AnimationName = "gamekind_403",
        TitlePath = "game/twoshowhand/gui/title_403.png",
        RoomPath = "game/twoshowhand/gui/gui-room-%d.png",
        RulePath = "game/twoshowhand/gui/rule-403.png",     --规则图片
        RoomItemWidth = 308,
        RoomItemHeight = 310,
        RoomItemDiffX = 0,
        RoomItemDiffY = 20,
        TableType = 1,
        Margin = 16,
        LeftMargin = 0,
        RightMargin = 12,

        EnterBackGround = 2,    --进入后台方式：2.删界面/2.不删界面/3.同2
        EnterForeGround = 2,    --返回前台方式：2.显示加载/2.不显示加载/3.同2
        EnterGameSuccess = 2,   --返回登录成功：2.加载游戏/2.删除游戏,重新加载/3.不删游戏,重置游戏
        EnterNetworkFail = 2,   --断网处理方式：2.退出大厅/2.进入后台,等待重连成功/3.同2

        IphoneXDesign = 1,      --是否全面屏适配

        --BroadcastData = {667, 630, 667, {0xff, 0xff, 0xff}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)
        RollMsgData = {667, 728, 450, 36}
    },
    {
        OpenGame = true,
        GameName = "通比牛牛",
        KindID = 404,
        Classify = 4,
        BackToGame = true,  --登录时是否拉回游戏
        BackToHall = false, --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "hall/effect/gamelist/gamekind_404/gamekind_404.ExportJson",
        AnimationName = "gamekind_404",
        TitlePath = "game/tbnn/gui/title_404.png",
        RoomPath = "game/tbnn/gui/gui-room-%d.png",
        RulePath = "game/tbnn/gui/rule_404.png",     --规则图片
        RoomItemWidth = 336,
        RoomItemHeight = 380,
        RoomItemDiffX = 0,
        RoomItemDiffY = 0,
        TableType = 1,
        Margin = 0,
        LeftMargin = 20,
        RightMargin = 10,

        EnterBackGround = 2,    --进入后台方式：1.删界面/2.不删界面/3.同2
        EnterForeGround = 2,    --返回前台方式：1.显示加载/2.不显示加载/3.同2
        EnterGameSuccess = 2,   --返回登录成功：1.加载游戏/2.删除游戏,重新加载/3.不删游戏,重置游戏
        EnterNetworkFail = 2,   --断网处理方式：1.退出大厅/2.进入后台,等待重连成功/3.同2

        IphoneXDesign = 1,      --是否全面屏适配

        BroadcastData = {667, 650, 667, {0x54, 0x23, 0x24}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)
        RollMsgData = {667, 730, 400, 36}
    },
    {
        OpenGame = true,
        GameName = "炸金花",
        KindID = 408,
        Classify = 4,
        BackToGame = true,  --登录时是否拉回游戏
        BackToHall = false, --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "hall/effect/gamelist/gamekind_407/gamekind_407.ExportJson",
        AnimationName = "gamekind_407",
        TitlePath = "game/goldenflower/gui/title_408.png",
        RoomPath = "game/goldenflower/gui/gui-room-%d.png",
        RulePath = "game/goldenflower/gui/rule-408.png",     --规则图片
        RoomItemWidth = 280,
        RoomItemHeight = 470,
        RoomItemDiffX = 0,
        RoomItemDiffY = 30,
        TableType = 0,
        Margin = 46,
        LeftMargin = 25,
        RightMargin = 10,

        EnterBackGround = 2,    --进入后台方式：1.删界面/2.不删界面/3.同2
        EnterForeGround = 2,    --返回前台方式：1.显示加载/2.不显示加载/3.同2
        EnterGameSuccess = 2,   --返回登录成功：1.加载游戏/2.删除游戏,重新加载/3.不删游戏,重置游戏
        EnterNetworkFail = 2,   --断网处理方式：1.退出大厅/2.进入后台,等待重连成功/3.同2

        IphoneXDesign = 1,      --是否全面屏适配

        --BroadcastData = {667, 610, 667, {0x3d, 0x31, 0x61}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)
        RollMsgData = {667-330, 725, 352, 36}
    },
    {
        OpenGame = true,
        GameName = "二人雀神",
        KindID = 410,
        Classify = 4,
        BackToGame = true,  --登录时是否拉回游戏
        BackToHall = false, --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "hall/effect/gamelist/gamekind_410/gamekind_410.ExportJson",
        AnimationName = "gamekind_410",
        TitlePath = "game/hpmahjong/gui/title_410.png",
        RoomPath = "game/hpmahjong/gui/gui-room-%d.png",
        RulePath = "game/hpmahjong/gui/rule_410.png",     --规则图片
        RoomItemWidth = 290,
        RoomItemHeight = 380,
        RoomItemDiffX = 0,
        RoomItemDiffY = 0,
        TableType = 0,
        Margin = 40,
        LeftMargin = 20,
        RightMargin = 10,

        EnterBackGround = 2,    --进入后台方式
        EnterForeGround = 2,    --返回前台方式
        EnterGameSuccess = 2,   --返回登录成功
        EnterNetworkFail = 2,   --断网处理方式

        IphoneXDesign = 1,      --是否全面屏适配

        BroadcastData = {667, 630, 667, {0x12, 0x40, 0x43}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)
        RollMsgData = {667, 640, 400, 36}
    },
    {
        OpenGame = false,
        GameName = "德州扑克",
        KindID = 411,
        Classify = 4,
        BackToGame = true,   --登录时是否拉回游戏
        BackToHall = false,  --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "hall/effect/gamelist/gamekind_411/gamekind_411.ExportJson",
        AnimationName = "gamekind_411",
        TitlePath = "game/texas/gui/title_411.png",
        RoomPath = "game/texas/gui/gui-room-%d.png",
        RulePath = "game/texas/gui/rule-411.png",     --规则图片
        RoomItemWidth = 320,
        RoomItemHeight = 350,
        RoomItemDiffX = 0,
        RoomItemDiffY = 10,
        TableType = 0,
        Margin = 5,
        LeftMargin = 17,
        RightMargin = 12,

        EnterBackGround = 1,    --进入后台方式
        EnterForeGround = 1,    --返回前台方式
        EnterGameSuccess = 1,   --返回登录成功
        EnterNetworkFail = 1,   --断网处理方式

        IphoneXDesign = 0,      --是否全面屏适配

        --BroadcastData = {667, 630, 667, {0xff, 0xff, 0xff}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)
        RollMsgData = {667, 725, 450, 36}
    },
    {
        OpenGame = true,
        GameName = "抢庄牛牛",
        KindID = 413,
        Classify = 4,
        BackToGame = true,  --登录时是否拉回游戏
        BackToHall = false, --重连时是否返回大厅
        Orientation = 1,
        AnimationPath = "hall/effect/gamelist/gamekind_413/gamekind_413.ExportJson",
        AnimationName = "gamekind_413",
        TitlePath = "game/qznn/gui/title_413.png",
        RoomPath = "game/qznn/gui/gui-room-%d.png",
        RulePath = "game/qznn/gui/rule_413.png",     --规则图片
        RoomItemWidth = 285,
        RoomItemHeight = 380,
        RoomItemDiffX = 0,
        RoomItemDiffY = 0,
        TableType = 0,
        Margin = 40,
        LeftMargin = 20,
        RightMargin = 10,

        EnterBackGround = 2,    --进入后台方式
        EnterForeGround = 2,    --返回前台方式
        EnterGameSuccess = 2,   --返回登录成功
        EnterNetworkFail = 2,   --断网处理方式

        IphoneXDesign = 1,      --是否全面屏适配

        --BroadcastData = {667, 710, 667, {0x3d, 0x31, 0x61}}, --广播：x(1-1334)/y(1-750)/width(512-1334)/颜色(#ffffff)
        RollMsgData = {667, 725, 450, 36}
    },
}

return GameKind
