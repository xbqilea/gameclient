--region netConstants.lua
--Date 2017/04/10
--网络命令常亮
--endregion

G_C_CMD = {
    
   ---------------------------------------大厅相关-------------------------------------------

    --内核命令
    MDM_KN_COMMAND			            = 0,							--内核命令
        SUB_KN_DETECT_SOCKET	                = 1,						--检测命令
        SUB_KN_VALIDATE_SOCKET	                = 2,						--验证命令
        SUB_KN_SHUT_DOWN_SOCKET 	            = 3,						--关闭命令

    --广场登录
    MDM_GP_LOGON			            = 1,							--广场登录
        SUB_GP_LOGON_GAMEID		                = 1,						--I D 登录
        SUB_GP_LOGON_ACCOUNTS	                = 2,						--帐号登录
        SUB_GP_REGISTER_ACCOUNTS                = 3,						--注册帐号
        SUB_GP_PLAYLOGONDIAL		            = 4,						--玩登录转盘
        SUB_GP_MOBILEREGISTERTOKEN	            = 5,						--手机注册玩家Token用于推送
        SUB_GP_ISALREADY	                    = 6,						--判断帐号,昵称是否已经注册

        SUB_GP_GIVEAWAYS_INFO_GET	            = 10,		
        SUB_GP_GIVEAWAYS_INFO_BACK	            = 11,						--获取活动信息
        SUB_GP_SHARE_PRESENT_GET	            = 12,						--获取分享奖励
        SUB_GP_SHARE_PRESENT_BACK	            = 13,						--获取分享奖励

        SUB_GP_MOBILEREGISTERTOKEN_EX           = 23,	                    --手机注册玩家Token用于推送(关联账户)

        SUB_GP_LOGON_ACCOUNTS_VERIFY            = 24,	                    --账号登录带验证功能
        SUB_GP_REGISTER_ACCOUNTS_VERIFY         = 25,	                    --注册账号带验证功能
        SUB_GP_REGISTER_ACCOUNTS_EX             = 27,	                    --注册账号扩展方式
        SUB_GP_REGISTER_ACCOUNTS_EXEX           = 29,	                    --带手机验证码的账号注册功能
        SUB_GP_REQUEST_MOBILEPHONE_VERIFYCODE	= 30,						--请求手机验证码
        SUB_GP_REQUEST_USERMOBILE		        = 31,						--根据账号请求手机号码
        SUB_GP_GUSET_MODIFY_EXEX		        = 32,						--带手机验证码的修改游客账号信息扩展
        SUB_GP_LOGON_RECONNECT			        = 33,						--重新连接大厅
        SUB_GP_LOGOUT_ACCOUNTS			        = 34,						--账号退出
        SUB_GP_LOGON_ACCOUTNS_EX                = 35,	                    --账号登录扩展方式
        SUB_GP_GUSET_MODIFY_EX2			        = 36,						--带手机验证码的修改游客账号信息扩展

        --登录结果
        SUB_GP_LOGON_SUCCESS	                = 100,						--登录成功
        SUB_GP_LOGON_FAILURE	                = 101,						--登录失败
        SUB_GP_LOGON_FINISH		                = 102,						--登录完成
        SUB_GP_VALIDATE_MBCARD                  = 103,	                    --登录失败
        SUB_GP_RECONNECT_SUCCESS                = 104,						--重连成功
        SUB_GP_RECONNECT_FAILURE                = 105,						--重连失败
        SUB_GP_GAME_SERVER                      = 150,	 
        
        SUB_GP_ISALREADY_BACK                   = 140,                      --检查账号 昵称是否已经注册返回
        SUB_GP_MEGAWIN_MESSAGE                  = 144,                      --大奖消息(新增)(325修改143为144)
        SUB_GP_SPEAKER_MESSAGE                  = 145,                      --代理喇叭消息
        SUB_GP_SHOW_PROXYNAME                   = 146,                      --代理名称展示

        SUB_GP_REQUEST_MOBILEPHONE_VERIFYCODE_BACK = 201,                   --请求手机验证码返回 
        SUB_GP_REQUEST_USERMOBILE_BACK          = 202,                      --根据账号请求手机号码
           
    --登录命令       
    MDM_MB_LOGON                        = 100,                          --登录命令 
        --登录模式
        SUB_MB_LOGON_GAMEID			            = 1,						--I D 登录
        SUB_MB_LOGON_ACCOUNTS		            = 2,						--帐号登录
        SUB_MB_REGISTER_ACCOUNTS	            = 3,						--注册帐号
        SUB_MB_REGISTER_CONFIRMCODE	            = 4,						--申请注册验证码
        SUB_MB_GUESTLOGIN			            = 5,					    --游客登录
        SUB_MB_GUESTBINGDING		            = 6,					    --游客账号绑定
        SUB_GP_GUSET_MODIFY			            = 9,                        --修改游客账号信息
        SUB_MB_GUESTLOGIN_EX		            = 10,						--游客登录扩展方式
        SUB_MB_GUESTLOGIN_VERIFY                = 12,                       --游客登录带验证功能
        SUB_MB_GUESTLOGIN_EX1                   = 11,                       --游客登录扩展方式
        SUB_MB_GUESTLOGIN_EX2                   = 14,                       --游客登录扩展方式（可以使用昵称）
        SUB_GP_GUSET_MODIFY_EX                  = 28,                       --修改游客账号信息扩展 

        --登录结果
        SUB_MB_LOGON_SUCCESS		            = 100,						--登录成功
        SUB_MB_LOGON_FAILURE		            = 101,						--登录失败

        -- 游客绑定                               
        SUB_MB_GUESTBINDING_SUCCESS             = 110,						--绑定成功
        SUB_MB_GUESTBINDING_FAILURE	            = 111,						--绑定失败

        --升级提示
        SUB_MB_UPDATE_NOTIFY		            = 200,						--升级提示

    --列表命令
    MDM_GP_SERVER_LIST			        = 2,							--列表信息
        --获取命令
        SUB_GP_GET_LIST				            = 1,						--获取列表
        SUB_GP_GET_SERVER			            = 2,						--获取房间
        SUB_GP_GET_ONLINE			            = 3,						--获取在线
        SUB_GP_GET_COLLECTION		            = 4,						--获取收藏

        --列表信息
        SUB_GP_LIST_TYPE			            = 100,						--类型列表
        SUB_GP_LIST_KIND			            = 101,						--种类列表
        SUB_GP_LIST_NODE			            = 102,						--节点列表
        SUB_GP_LIST_PAGE			            = 103,						--定制列表
        SUB_GP_LIST_SERVER			            = 104,						--房间列表
        SUB_GP_VIDEO_OPTION			            = 105,						--视频配置
        SUB_GP_LIST_SPECAIL_FRUIT               = 106,                      --临时消息，控制移动端是否显示100分场
        SUB_GP_LIST_SPECAIL_ROOM                = 107,                      --临时消息，只有最新版本的客户端才能显示老虎机和水浒传的地摊房

        --完成信息
        SUB_GP_LIST_FINISH			            = 200,						--发送完成
        SUB_GP_SERVER_FINISH		            = 201,						--房间完成

        --在线信息
        SUB_GR_KINE_ONLINE			            = 300,						--类型在线
        SUB_GR_SERVER_ONLINE		            = 301,						--房间在线

    --用户服务
    MDM_GP_USER_SERVICE                 = 3,                            --用户服务
        SUB_GP_MODIFY_INSURE_PASS               = 102,                      --修改密码
        SUB_GP_MODIFY_VERIFI_INSER              = 104,                      --验证密码
        SUB_GP_RESET_LOGON_PASS                 = 105,                      --充值登录密码
        SUB_GP_RESET_INSURE_PASS                = 106,                      --重置银行密码

        SUB_GP_USER_INDIVIDUAL                  = 301,                      --查询信息
        SUB_GP_QUERY_INDIVIDUAL                 = 302,                      --查询信息
        SUB_GP_MODIFY_INDIVIDUAL                = 303,                      --修改资料
        SUB_GP_SET_MOBILE_PHONE                 = 305,                      --设置移动电话
        SUB_GP_MODIFY_MOBILE_PHONE_WITH_VERIFYCODE  = 307,                  --带有手机验证码的修改移动电话

        --银行服务
        SUB_GP_USER_SAVE_SCORE		            = 400,						--存款操作
        SUB_GP_USER_TAKE_SCORE		            = 401,						--取款操作
        SUB_GP_USER_TRANSFER_SCORE	            = 402,						--转账操作
        SUB_GP_USER_INSURE_INFO		            = 403,						--银行资料
        SUB_GP_QUERY_INSURE_INFO	            = 404,						--查询银行
        SUB_GP_USER_INSURE_SUCCESS	            = 405,						--银行成功
        SUB_GP_USER_INSURE_FAILURE	            = 406,						--银行失败
        SUB_GP_QUERY_USER_INFO_REQUEST	        = 407,						--查询用户
        SUB_GP_QUERY_USER_INFO_RESULT	        = 408,						--用户信息
        SUB_MB_INSURE_RECORD_GET	            = 410,						--移动端查询银行操作记录
        SUB_MB_INSURE_RECORD_BACK	            = 411,						--移动端查询银行操作记录返回
        SUB_GP_QUERY_INSURE_INFO_EX             = 412,                      --查询银行信息扩展
        SUB_GP_USER_INSURE_INFO_EX              = 413,                      --银行资料
        SUB_GP_REVENUE_REFUND_SCORE             = 414,                      --领取税收返利(银商用)
        SUB_GP_QUERY_REFUND_RECORD              = 415,                      --查询税收返利记录(银商用)
        SUB_GP_RES_INSURE_RECORD                = 416,                      --银商工具查询银行操作记录结果(银商用)
        SUB_GP_RES_INSURE_TRANSFER_SUCCESS      = 417,                      --银行转账成功
        SUB_MB_INSURE_RECORD_BACK_EX            = 418,                      --带审核状态的移动端查询银行操作记录返回
        SUB_GP_GET_INSURE_RECORD                = 420,                      --获取银行转账记录
        SUB_GP_GET_INSURE_RECORD_EX             = 421,                      --获取带有审核状态的银行转账记录
        SUB_GP_QUERY_INSURE_INFO_LITE           = 422,						--获取银行和钱包的金币
        SUB_GP_USER_INSURE_INFO_LITE            = 423,						--金币信息
        SUB_GP_USER_RECHARGE_SUCCESS            = 427,                      --充值成功
        SUB_GP_USER_BUYMEMBER_SUCCESS           = 428,                      --购买月卡成功

--        SUB_GP_GET_DOLE_GET_C                   = 421,                      --领取救济金
--        SUB_GP_GET_DOLE_GET_S                   = 422,                      --领取救济金
--        SUB_GP_GET_DOLE_QUERY_C                 = 423,                      --查询救济金
--        SUB_GP_GET_DOLE_QUERY_S                 = 424,                      --查询救济金成功

        SUB_GP_FEEDBACK				            = 500,						--发送系统反馈
        SUB_GP_FEEDBACK_FINISH_GET				= 501,						--获取意见反馈已经回复的数据
        SUB_GP_FEEDBACK_FINISH_BACK				= 502,						--返回意见反馈已经回复的数据
        SUB_GP_PROXY_COMPLAIN_GET				= 503,						--获取举报代理微信账号
        SUB_GP_PROXY_COMPLAIN_BACK				= 504,						--返回举报代理微信账号
        SUB_GP_CLIENT_CONFIG_GET				= 505,						--获取客户端配置
        SUB_GP_CLIENT_CONFIG_BACK				= 506,						--返回客户端配置
        SUB_GP_FEEDBACK_ASSESS                  = 507,                      --玩家对意见反馈的评价


        -- 代理招商/充值
        SUB_GP_C_GET_CUSTOMERSERVICE_ACCOUNT    = 606,                      -- 获取客服账号
        SUB_GP_S_GET_CUSTOMERSERVICE_ACCOUNT    = 607,                      -- 返回客服账号
        SUB_GP_C_SAVE_INVESTMENT_ACCOUNT        = 608,                      -- 保存投资者账号
        SUB_GP_S_SAVE_INVESTMENT_ACCOUNT        = 609,                      -- 响应保存投资者账号

        --操作结果
        SUB_GP_OPERATE_SUCCESS		            = 900,						--操作成功
        SUB_GP_OPERATE_FAILURE		            = 901,						--操作失败

    -- 新绑定信息 ----------
    MDM_GP_DRAWING_SERVICE              = 6,                            -- 提现服务
        SUB_GP_C_DRAWING_QUERY                  = 100,                      -- 查询提现的设置
        SUB_GP_C_DRAWING_BANK_SET               = 101,                      -- 设置提现银行卡
        SUB_GP_C_DRAWING_ALIPAY_SET             = 102,                      -- 设置提现支付宝
        SUB_GP_C_DRAWING_ACTION                 = 103,                      -- 用户提现

        SUB_GP_S_DRAWING_QUERY                  = 200,                      -- 查询提现的设置
        SUB_GP_S_DRAWING_BANK_SET               = 201,                      -- 设置提现银行卡
        SUB_GP_S_DRAWING_ALIPAY_SET             = 202,                      -- 设置提现支付宝
        SUB_GP_S_DRAWING_ACTION                 = 203,                      -- 用户提现


    --框架命令
    MDM_GF_FRAME				        = 100,						--框架命令
        SUB_SPEAKER_SEND_MSG		        = 302,                      --发送喇叭消息
        SUB_SPEAKER_SEND_MSG_BACK	        = 303,                      --接受喇叭消息
        --用户命令
        SUB_GF_GAME_OPTION			        = 1,						--游戏配置
        SUB_GF_USER_READY			        = 2,						--用户准备
        SUB_GF_LOOKON_CONFIG		        = 3,						--旁观配置

        SUB_GF_USER_CHAT                    = 10,                       --用户聊天
        SUB_GF_USER_EXPRESSION              = 11,                       --用户表情
        SUB_GR_USER_CALL                    = 12,						--用户呼叫

        SUB_GR_USER_SITDOWN_MB		        = 14,

        --游戏信息
        GAME_STATUS_FREE                    = 0,                        --空闲状态
        SUB_GF_GAME_STATUS			        = 100,						--游戏状态
        SUB_GF_GAME_SCENE			        = 101,						--游戏场景
        SUB_GF_LOOKON_STATUS		        = 102,						--旁观状态

        --系统消息
        SUB_GF_SYSTEM_MESSAGE		        = 200,						--系统消息
        SUB_GF_ACTION_MESSAGE		        = 201,						--动作消息
        SUB_GF_GAME_GIFT                    = 203,                      --领取红包 


    --修改资料
    MDM_MB_USER_INFO                    = 102,                      --用户资料
        SUB_MB_ADD_USER_INFO                = 100,                      --增加资料 （ 姓名，身份证，手机 ）
        SUB_MB_MODIFY_NICKNAME              = 102,                      --修改昵称
        SUB_MB_MODIFY_FACE                  = 103,                      --修改头像
        SUB_MB_GETACCOUNTPROTECT            = 104,                      --获取帐号保护信息
        SUB_MB_MODIFY_FACE_CIRCLE           = 105,                      --修改头像框
                                                                         
        SUB_MB_OPERATE_RESULT               = 101,                      --操作结果
        SUB_MB_USERRUN_INFO                 = 120,                      --玩家动态

    --获取消息
    MDM_MESSAGE                     = 200,                          -- 消息中心主消息
        SUB_MESSAGE_GET                     = 201,                      -- 获取消息包,发送给服务器
        SUB_MESSAGE_BACK                    = 202,                      -- 服务器发送消息包给客户端
        SUB_RANKINGLIST                     = 209,                      -- 获取排行榜
        SUB_GP_DELETE_MESSAGE               = 210,                      -- 删除消息
        SUB_GP_DEL_MSG_RESULT               = 211,                      -- 删除消息结果
        SUB_RANKINGLISTYESTERDAY            = 213,                      -- 获取财富榜 赢分榜
        SUB_GP_ACCOUNTSBUY_GET              = 214,                      -- 获取购买信息
        SUB_GP_ACCOUNTSBUY_BACK             = 215,                      -- 获取购买信息

        SUB_GP_READ_MESSAGE                 = 219,                      -- 已读消息(新增)
        SUB_GP_READ_MSG_RESULT              = 220,                      -- 消息结果(新增)

    --推广消息
    MDM_SPREAD                      = 212,                          -- 推广主消息
        SUB_SPREAD_GET                      = 213,                      -- 获取推广数据包,发送给服务器
        SUB_SPREAD_BACK                     = 214,                      -- 服务器发送推广数据包给客户端
        SUB_SPREAD_SUMGOLD_GET              = 215,                      -- 获取推广总金币,发送给服务器
        SUB_SPREAD_SUMGOLD_BACK             = 216,                      -- 服务器发送推广总金币给客户端

        SUB_SPREAD_BR_QUERY_C               = 215,                      -- 查询推广员的数据
        SUB_SPREAD_BR_GAINRATE_S            = 213,                      -- 推广员的明税推广比例
        SUB_SPREAD_BR_DETAILLIST_S	        = 216,						--推广员的明税明细
        SUB_SPREAD_COUNT_QUERY_C	        = 217,						--查询推广的总人数
        SUB_SPREAD_COUNT_QUERY_S	        = 218,						--查询推广的总人数

    --vip命令
    MDM_VIP_INFO                   = 223,						    --用户vip信息
        SUB_VIP_INFO_GET			        = 224, 
        SUB_VIP_INFO_BACK			        = 225,
        SUB_MONTH_CARD_INFO_GET             = 252,  --请求月卡信息
        SUB_MONTH_CARD_INFO_BACK            = 253,  --月卡信息返回
        SUB_SIGNED_ON_GET                   = 254,  --请求签到
        SUB_SIGNED_ON_BACK                  = 255,  --签到返回
        SUB_DOLE_GET                        = 256,  --请求领取救济金
        SUB_DOLE_BACK                       = 257,  --领取救济金返回
        SUB_SIGNED_ON_INFO_GET              = 258,  --查询签到信息
        SUB_SIGNED_ON_INFO_BACK             = 259,  --签到信息返回

    -- 查询玩家钱包
    MDM_TAKE_SCORE_INFO                 = 226,                      -- 查询用户钱包信息
        SUB_TAKE_SCORE_INFO_GET             = 227,                       -- 请求用户钱包信息
        SUB_TAKE_SCORE_INFO_BACK            = 228,                       -- 返回用户钱包信息
        SUB_TAKE_RECHARGE_TOTAL_GET         = 229,                       --   查询用户充值总金额
        SUB_TAKE_RECHARGE_TOTAL_BACK        = 230,                       --   查询用户充值总金额


    --渠道命令
    MDM_CHECK_CHANNEL_INFO              = 229,						--检查渠道开关
        SUB_CHECK_CHANNEL_INFO_GET			= 230,                     --old
        SUB_CHECK_CHANNEL_INFO_BACK			= 231,                     --old
        SUB_CHECK_CHANNEL_INFO_EX_GET		= 232,
        SUB_CHECK_CHANNEL_INFO_EX_BACK      = 234,

    --代金券命令
    MDM_GAME_GIFT_INFO                  = 241,						--代金券相关信息
        SUB_GAME_GIFT_USER_GIFTPROPERTY_GET		= 1,                    --用户代金券数据                  
        SUB_GAME_GIFT_USER_GIFTPROPERTY_BACK	= 2,                     
        SUB_GAME_GIFT_GAME_GIFTPROPERTY_GET		= 3,                    --游戏代金券信息
        SUB_GAME_GIFT_GAME_GIFTPROPERTY_BACK    = 4,

        SUB_GAME_GIFT_UNLOCK_GIFTPROPERTY_C     = 5,                --请求解锁玩家的红包
        SUB_GAME_GIFT_UNLOCK_GIFTPROPERTY_S     = 6,                --请求解锁玩家的红包
        SUB_GAME_GIFT_UNPACK_GIFTPROPERTY_C     = 7,                --请求打开玩家的红包
        SUB_GAME_GIFT_UNPACK_GIFTPROPERTY_S     = 8,                --请求打开玩家的红包

        SUB_GP_GET_PROSPEROUS_PROPERTY_C        = 9,        --请求恭喜发财活动的数量
        SUB_GP_GET_PROSPEROUS_PROPERTY_S        = 10,       --请求恭喜发财活动的数量 
        SUB_GP_ADD_PROSPEROUS_PROPERTY_C        = 11,       --请求增加财
        SUB_GP_ADD_PROSPEROUS_PROPERTY_S        = 12,       --请求增加财 
        SUB_GP_ACHIEVE_PROSPEROUS_PROPERTY_C    = 13,       --请求合成
        SUB_GP_ACHIEVE_PROSPEROUS_PROPERTY_S    = 14,       --请求合成
        SUB_GP_GET_PROSPEROUS_LEADERBOARD_C     = 15,       --请求排行榜
        SUB_GP_GET_PROSPEROUS_LEADERBOARD_S     = 16,       --请求排行榜 
        SUB_GP_GET_PROSPEROUS_AWARD_RECORD_C    = 17,       --请求开奖记录
        SUB_GP_GET_PROSPEROUS_AWARD_RECORD_S    = 18,       --请求开奖记录
        SUB_GP_GET_PROSPEROUS_AWARD_RESULT_S    = 19,       --开奖结果
        SUB_GP_GET_PROSPEROUS_ALLACHIEVE_COUNT_C = 20,       --当前全平台的合成福的总数
        SUB_GP_GET_PROSPEROUS_ALLACHIEVE_COUNT_S = 21,       --当前全平台的合成福的总数

    --客户端活动数据 242
    MDM_GAME_CLIENT_ACTIVITY            = 242,				--客户端请求活动数据
        SUB_GAME_NATIONALDAY_QUERY_INVITER_GET          = 3,       --国庆活动邀请者请求数据
        SUB_GAME_NATIONALDAY_QUERY_INVITER_BACK         = 4,       --国庆活动邀请者请求数据返回
        SUB_GAME_NATIONALDAY_GET_INVITERREAWARD_GET     = 5,       --国庆活动邀请者领取奖励
        SUB_GAME_NATIONALDAY_GET_INVITERREAWARD_BACK    = 6,       --国庆活动邀请者领取奖励返回
        SUB_GAME_NATIONALDAY_QUERY_OLDPLAYER_GET        = 7,       --国庆活动老玩家请求数据
        SUB_GAME_NATIONALDAY_QUERY_OLDPLAYER_BACK       = 8,       --国庆活动老玩家请求数据返回
        SUB_GAME_NATIONALDAY_GET_OLDPLAYERREWARD_GET    = 9,       --国庆活动老玩家领取奖励
        SUB_GAME_NATIONALDAY_GET_OLDPLAYERREWARD_BACK   = 10,      --国庆活动老玩家领取奖励返回


    --定义断开Socket
    MDM_CLIENT_MAIN                     = 9999,    
        SUB_DISCONNECT_SOCKET               = 1,   

    --系统命令
    MDM_CM_SYSTEM                       = 1000,
        --踢人
        SUB_CM_SYSTEM_MESSAGE		        = 1,					   --系统消息
        SUB_CM_ACTION_MESSAGE		        = 2,					   --动作消息
        SUB_CM_DOWN_LOAD_MODULE		        = 3,					   --下载消息

   ---------------------------------------大厅相关-------------------------------------------

   ---------------------------------------游戏相关-------------------------------------------
    MDM_GR_LOGON                        = 1,                            --登录信息
        --登录模式
        SUB_GR_LOGON_USERID			            = 1,						--I D 登录
        SUB_GR_LOGON_MOBILE			            = 2,						--手机登录
        SUB_GR_LOGON_ACCOUNTS		            = 3,						--帐户登录
        SUB_GR_LOGON_KICK_USER		            = 4,                        --登录强制踢用户
        SUB_GR_LOGON_KICK_RESPOSE	            = 5,						--登录强制踢用户响应
        SUB_GR_LOGON_KICK_USER_EX               = 6,						--带登录密码强制踢用户

        SUB_GR_LOGON_TIMER                      = 10,                       --网络监测

        --登录结果
        SUB_GR_LOGON_SUCCESS		            = 100,						--登录成功
        SUB_GR_LOGON_FAILURE		            = 101,						--登录失败
        SUB_GR_LOGON_FINISH			            = 102,						--登录完成

        --升级提示
        SUB_GR_UPDATE_NOTIFY		            = 200,						--升级提示

        --公告
        MDM_SYS_NOTICE                      = 204,                     --公告主消息
        MDM_SYS_NOTICE_GET                      = 205,                     --获取公告信息
        MDM_SYS_NOTICE_BACK                     = 206,                     --获取服务器返回的消息
        MDM_SYS_NOTICE_CHANNEL_GET              = 208,                     --按照渠道获取公告功能

        MDM_SYS_NOTICE_UPDATE                   = 209,                     --玩家更新公告状态(新增)
        MDM_SYS_NOTICE_UPDATE_BACK              = 210,                     --玩家更新公告状态(新增)

        --强制更新
        SUB_GP_CLIENTVERSION                    = 7,                       --获取服务器最新版本信息
        SUB_GP_CLIENTVERSION_BACK	            = 141,                     --返回服务器最新版本信息

    MDM_GR_CONFIG				 = 2,									--配置信息
        SUB_GR_CONFIG_COLUMN		            = 100,						--列表配置
        SUB_GR_CONFIG_SERVER		            = 101,						--房间配置
        SUB_GR_CONFIG_PROPERTY		            = 102,						--道具配置
        SUB_GR_CONFIG_FINISH		            = 103,						--配置完成
        SUB_GR_CONFIG_USER_RIGHT	            = 104,						--玩家权限

    MDM_GR_USER                         = 3,                            --用户命令
        --用户动作
        SUB_GR_USER_RULE			            = 1,				       --用户规则
        SUB_GR_USER_LOOKON			            = 2,				       --旁观请求
        SUB_GR_USER_SITDOWN			            = 3,				       --坐下请求
        SUB_GR_USER_STANDUP			            = 4,				       --起立请求
        SUB_GR_USER_INVITE			            = 5,				       --用户邀请
        SUB_GR_USER_INVITE_REQ		            = 6,				       --邀请请求
        SUB_GR_USER_REPULSE_SIT  	            = 7,				       --拒绝玩家坐下
        SUB_GR_USER_KICK_USER                   = 8,                       --踢出用户
        SUB_GR_USER_INFO_REQ                    = 9,                       --请求用户信息
        SUB_GR_USER_CHAIR_REQ                   = 10,                      --请求更换位置
        SUB_GR_USER_CHAIR_INFO_REQ              = 11,                      --请求椅子用户信息
        SUB_GR_USER_BUSINESS_ROBOT_GET          = 12,                      --请求银商机器人信息
        SUB_GR_USER_BUSINESS_ROBOT_BACK         = 13,                      --请求银商机器人信息
        SUB_GR_USER_FRAGMENT_INFO_BACK		    = 14,                      --用户碎片信息
        SUB_GR_USER_MATCH                       = 15,                      --用户请求匹配
        SUB_GR_USER_PAYAPI_REQ                  = 16,                      --用户请求充值接口信息

        --用户状态
        SUB_GR_USER_ENTER			            = 100,					   --用户进入
        SUB_GR_USER_SCORE			            = 101,					   --用户分数
        SUB_GR_USER_STATUS			            = 102,					   --用户状态
        SUB_GR_REQUEST_FAILURE		            = 103,					   --请求失败
        SUB_GR_USER_LEVUP			            = 104,					   --用户升级
        SUB_GR_OTHER_SCORE                      = 105,                     --其他玩家分数改变

        --聊天命令
        SUB_GR_USER_CHAT			            = 201,					   --聊天消息
        SUB_GR_USER_EXPRESSION		            = 202,					   --表情消息
        SUB_GR_WISPER_CHAT			            = 203,					   --私聊消息
        SUB_GR_WISPER_EXPRESSION	            = 204,					   --私聊表情
        SUB_GR_COLLOQUY_CHAT		            = 205,					   --会话消息
        SUB_GR_COLLOQUY_EXPRESSION	            = 206,					   --会话表情
        SUB_GR_MEGAWIN_MESSAGE                  = 207,                     --大奖消息(新增)
        SUB_GR_PROSPEROUS_RESULT                = 208,                     --开奖结果

        --道具命令
        SUB_GR_PROPERTY_BUY			            = 300,					   --购买道具
        SUB_GR_PROPERTY_SUCCESS		            = 301,					   --道具成功
        SUB_GR_PROPERTY_FAILURE		            = 302,					   --道具失败
        SUB_GR_PROPERTY_MESSAGE                 = 303,                     --道具消息
        SUB_GR_PROPERTY_EFFECT                  = 304,                     --道具效应
        SUB_GR_PROPERTY_TRUMPET		            = 305,                     --喇叭消息
        SUB_GR_GLAD_MESSAGE			            = 400,                     --喜报消息
        SUB_GR_USER_PAYAPI_BACK                 = 401,                     --用户请求充值接口信息返回

        --金币购买命令                                                     
        MDM_SYS_PAYGIFT				            = 208,                     --购买金币主消息
        MDM_SYS_PAYGIFT_GET		                = 209,                     --购买金币子消息
        MDM_SYS_PAYGIFT_BACK		            = 210,                     --购买金币返回消息
        MDM_SYS_PAYGIFT_BACK_NEW                = 211,                     --返回新的充值活动数据
        MDM_SYS_PAYGIFT_BACK_NEW2               = 212,                     --返回新的充值活动数据

        SUB_GP_PAYAPI_GET                       = 216,                     --获取支付接口
        SUB_GP_PAYAPI_BACK                      = 217,                     --获取支付返回
        SUB_GP_PAYAPISORT_GET                   = 218,                     --获取支付接口显示顺序
        SUB_GP_PAYAPISORT_BACK                  = 219,                     --获取支付接口显示顺序返回


    --状态命令
    MDM_GR_STATUS				        = 4,							--状态信息                         
        SUB_GR_TABLE_INFO			        = 100,					        --桌子信息
        SUB_GR_TABLE_STATUS			        = 101,					        --桌子状态

    ----用户信息
    MDM_GR_INSURE				        = 5,							--用户信息

        --银行命令
        SUB_GR_QUERY_INSURE_INFO	            = 1,						--查询银行
        SUB_GR_SAVE_SCORE_REQUEST	            = 2,						--存款操作
        SUB_GR_TAKE_SCORE_REQUEST	            = 3,						--取款操作
        SUB_MB_QUERY_CUSTOMER_REQUEST           = 8,                        --获取客服
                                    
        SUB_GR_USER_INSURE_INFO		            = 100,						--银行资料
        SUB_GR_USER_INSURE_SUCCESS	            = 101,						--银行成功
        SUB_GR_USER_INSURE_FAILURE	            = 102,						--银行失败
        SUB_MB_QUERY_CUSTOMER_BACK              = 104,                      --获取客服


    --游戏命令
    MDM_GF_GAME					        = 200,							--游戏命令

        -- 飞禽走兽 201-----------------------------------------------------------------
        CMD_ChipStart_S_P				=101,				        --下注开始
        CMD_ChipStop_S_P				=102,				        --下注结束
        CMD_GameStart_S_P				=103,				        --游戏开始
        CMD_GameEnd_S_P					=104,				        --游戏结束
        CMD_UpdataChip_S_P				=105,				        --更新下注
        CMD_ChipSucc_S_P				=106,				        --下注成功
        CMD_SysMessage_S_P				=107,				        --系统消息
        CMD_GameInfo_S_P				=109,				        --游戏信息
        CMD_GameResultInfo_S_P			=110,				        --游戏结算信息
        CMD_ContinueChip_S_P			=111,				        --续投
        CMD_GameOpenLog_C_P			    =112,			            --游戏开奖纪录
        CMD_ClearChip_S_P				=113,				        --清空投注回应
        CMD_BankerScoreNeed_S_P         =122,                       --玩家上庄最低金币要求 

        CMD_UserChip_C_P				=151,				        --玩家下注
        CMD_ContinueChip_C_P			=152,			            --玩家续投回应
        CMD_ClearChip_C_P				=153,			            --清空投注

        BANKER_REQ_RESULT_SC		    =8001,				        --申请结果
        BANKER_SITUSERINFO_SC		    =8002,				        --坐庄信息
        BANKER_REQUPBANKER_CS		    =8051,				        --申请上庄
        -- 飞禽走兽 201-----------------------------------------------------------------

        -- 骰宝 203-----------------------------------------------------------------
        CMD_SB_UserChip_C_P					 =351,				--玩家下注
        CMD_SB_RequestBanker_C_P			 =352,				--申请庄家
        CMD_SB_ContinueChip_C_P				 =353,				--玩家续投
        CMD_SB_CancelChip_C_P				 =354,				--取消投注

        CMD_SB_GameInfo_S_P					 =301,				--游戏信息
        CMD_SB_SysMessage_S_P				 =302,				--系统消息
        CMD_SB_UpdateUserInfo_S_P			 =303,				--玩家信息
        CMD_SB_ChipSucc_S_P					 =304,				--下注成功
        CMD_SB_GameResultInfo_S_P			 =305,				--游戏结算信息
        CMD_SB_RequestBankerList_S_P		 =306,				--申请庄家列表(进入房间时发送)
        CMD_SB_RequestBanker_S_P			 =307,				--申请庄家(单人申请庄家时发送)
        CMD_SB_UpdateBanker_S_P				 =308,				--更新庄家信息(每回合结束都会更新)
        CMD_SB_HistoryRecord_S_P			 =309,				--发送历史记录
        CMD_SB_ContiueChip_S_P				 =310,				--玩家续投结果
        CMD_SB_LastSecChip_S_P				 =311,				--上一秒的投注总和
        CMD_SB_CancelChip_S_P				 =312,				--取消投注结果

        -- 骰宝 203-----------------------------------------------------------------


        ----- 百人牛牛 205--------------------------------------
        --服务器命令结构
        SUB_S_GameInfo					    = 301,                --游戏信息
        SUB_S_SysMessage				    = 302,                --系统消息
        SUB_S_SendTableUser				    = 303,                --上桌玩家信息
        SUB_S_ChipSucc					    = 304,                --下注成功
        SUB_S_GameResultInfo			    = 305,                --游戏结算信息
        SUB_S_RequestBanker                 = 307,                --申请庄家(单人申请庄家时发送)
        SUB_S_UpdateBanker                  = 308,                --更新庄家信息(每回合结束都会更新)
        SUB_S_ContiueChip				    = 310,                --玩家续投结果
        SUB_S_UpdateChip				    = 311,                --上一秒的投注总和
        SUB_S_CancelChip				    = 312,                --取消投注
        SUB_S_GOLD_OVER                     = 313,                --库存被赢光
        SUB_S_CurrentBanker                 = 314,                --当前庄家信息

        --客户端命令结构
        SUB_C_UserChip					    = 351,				  --玩家下注
        SUB_C_RequestBanker                 = 352,				  --申请庄家
        SUB_C_ContinueChip                  = 353,				  --玩家续投
        SUB_C_CancelChip				    = 354,				  --取消投注
        ----- 百人牛牛 205--------------------------------------

        -- 水浒传 206-----------------------------------------------------
        SUB_C_ADD_CREDIT_SCORE              =1,               -- 加注
        SUB_C_REDUCE_CREDIT_SCORE           =2,               -- 减注
        SUB_C_SCENE1_START                  =3,               -- 场景1开始
        SUB_C_SCENE2_BUY_TYPE               =4,               -- 买大小
        SUB_C_SCORE                         =5,               -- 得分
        SUB_C_SCENE3_START                  =6,               -- 场景3开始
        SUB_C_GLOBAL_MESSAGE                =7,               --
        --SUB_C_STOCK_OPERATE                 =8               --
        SUB_C_PRO_INQUIRY                   =9,               -- 概率查询
        SUB_C_SAVE_PRO                      =10,
        SUB_C_PERSON_CONTROL                =11,              -- 个人控制
        SUB_C_GameEnd_WATER                 =12,              -- 游戏结束(水浒传游戏结束)
        SUB_C_SCENE1_START_NEW              =13,              -- 新场景1开始(传下注倍率)
        SUB_C_GET_LUCKY_RANK                =20,              -- 获取幸运玩家排行榜
        SUP_C_LUCKY_RANK_NEW_OK             =21,              --新入围的点赞
        SUP_C_LUCKY_RANK_NEW_PLAY           =22,              --新入围的播放
        SUP_C_LUCKY_RANK_HISTORY_OK         =23,              --历史的点赞
        SUP_C_LUCKY_RANK_HISTORY_PLAY       =24,              --历史的播放
        SUP_C_LUCKY_RANK_GAMEDATA           =25,              --幸运玩家播放数据

        SUB_S_SCENE1_START                  =100,             --滚动结果
        SUB_S_SCENE2_RESULT                 =101,             --骰子结果(比大小)
        SUB_S_SCENE3_RESULT                 =102,             --玛丽结果
        SUB_S_STOCK_RESULT                  =103,             --库存操作结果
        SUB_S_PRO_INQUIRY_RESULT            =104,             --概率查询结果
        SUB_S_PERSON_RESULT                 =105,             --个人结果
        SUB_S_DOUBLE_RECORD                 =106,             --比倍记录
        SUB_S_CREDIT_SCORE_UPDATE           =107,             --分数控制
        SUB_S_GAME_END                      =109,             --游戏结束
        SUB_S_SERVER_MESSAGE                =111,             --跑马灯
        SUB_S_RETURN_SCENE1                 =112,             --回到主场景1(子游戏中断网)
        SUP_S_TP_MESSAGE_INFO               =113,             --大奖消息
        SUP_S_LUCKY_RANK_NEW                =115,             --新入围名单
        SUP_S_LUCKY_RANK_HISTORY            =116,             --历史名单
        SUP_S_LUCKY_RANK_TIPS               =117,             --新入围滚动名单
        SUP_S_LUCKY_RANK_NEW_OK             =118,             --点赞返回
        SUP_S_LUCKY_RANK_NEW_PLAY           =119,             --请求播放返回
--        SUP_S_LUCKY_RANK_HISTORY_OK         =120,             --历史的点赞
--        SUP_S_LUCKY_RANK_HISTORY_PLAY       =121,             --历史的播放
        SUP_S_LUCKY_RANK_GAMEDATA           =122,             --幸运玩家播放数据

        -- 水浒传 206-----------------------------------------------------


        -- 奔驰宝马 207---------------------------------------------------
		SUB_C_Car_UserChip				=351,					---- 玩家下注
		SUB_C_Car_RequestBanker			=352,					---- 申请庄家
		SUB_C_Car_ContinueChip			=353,					---- 玩家续投
		SUB_C_Car_CancelChip			=354,					---- 取消投注

		SUB_S_Car_GameInfo				=301,				    ---- 游戏信息
		SUB_S_Car_SysMessage			=302,				    ---- 系统消息
		SUB_S_Car_UserChipNotify		=304,				    ---- 下注通知
		SUB_S_Car_GameResultInfo		=305,				    ---- 游戏结算信息
		SUB_S_Car_RequestBanker			=307,				    ---- 申请庄家(单人申请庄家时发送)
		SUB_S_Car_UpdateBanker			=308,				    ---- 更新庄家信息
		SUB_S_Car_HistoryRecord			=309,				    ---- 发送历史记录
		SUB_S_Car_ContiueChip			=310,				    ---- 玩家续投结果
		SUB_S_Car_CancelChip			=312,				    ---- 取消投注
        SUB_S_Car_UpdateBankerScore     =313,                   -----更新庄家分数
        -- 奔驰宝马 207---------------------------------------------------

        ------ 百家乐 208-------------------------------------
        SUB_C_PLACE_JETTON			    = 1,	    --用户下注
        SUB_C_APPLY_BANKER			    = 2,	    --申请庄家
        SUB_C_CANCEL_BANKER			    = 3,	    --取消申请
        SUB_C_CONTINUES				    = 5,	    --续投
        SUB_C_CANCEL_DOWN			    = 6,	    --清除投注
    
        SUB_S_SysMessage_BACCARAT       = 302,      --系统消息
        SUB_S_GAME_FREE_BACCARAT        = 99,       --游戏空闲
        SUB_S_GAME_START_BACCARAT		= 100,      --游戏开始
        SUB_S_PLACE_JETTON              = 101,      --用户下注
        SUB_S_GAME_END_BACCARAT         = 102,      --游戏结束
        SUB_S_APPLY_BANKER              = 103,      --申请庄家
        SUB_S_CHANGE_BANKER             = 104,      --切换庄家
        SUB_S_CHANGE_USER_SCORE         = 105,      --更新分数
        SUB_S_SEND_RECORD               = 106,      --游戏记录
        SUB_S_PLACE_JETTON_FAIL         = 107,      --下注失败
        SUB_S_CANCEL_BANKER             = 108,      --取消申请
        SUB_S_CONTINUE_NOTIFY           = 110,      --续投通知
        SUB_S_CANCELDOWN_NOTIFY         = 111,      --清除投注
        SUB_S_CANCEL_BANKER_SUCCESS     = 112,      --取消庄家 
        ------ 百家乐 208-------------------------------------

        --水果机 209--------------------------------------------------------------------
        SUB_S_GAME_RESULT_FRUIT			=200,                   ---- 游戏普通开奖
        SUB_S_GAME_RESULT_RANDBOMB		=201,					---- GoodLuck开奖
        SUB_S_UPDATE_JETTONDOWN			=202,					---- 更新玩家下注
        SUB_S_UPDATE_DOWNSCORE			=203,					---- 更新上下分
        SUB_S_COMPETITION_RESULT		=204,					---- 比倍结果
        SUB_S_TIGER_RESULT				=205,					---- 老虎机开奖结果
        SUB_S_UPDATE_GOLD_SCORE			=206,					---- 更新奖池
        SUB_S_GAME_END_FRUIT			=207,					---- 游戏结束
        SUP_S_SERVER_MESSAGE_FRUIT		=220,					---- 服务器消息
        SUP_S_TP_MESSAGE_INFO_FRUIT     =210,                   ---- 大奖消息


        SUB_C_GAME_START_FRUIT			=251,					---- 开始
        SUB_C_DOWN_JETTON_FRUIT			=252,					---- 下注
        SUB_C_ALL_ADD_JETTON            =253,					---- 全部区域＋1
        SUB_C_CHANGE_SCORE              =254,					---- 上下分
        SUB_C_COMPETITION               =255,					---- 比大小
        SUB_C_ROTATE_FINISH             =256,					---- 游戏结束
        SUB_C_COMPETITION_FINISH        =257,                   ---- 比倍完成
        SUB_C_NEW_GAME_START            =260,                   ---- 开始
        SUB_C_GAME_START_EX             =261,                   ---- 最新开始
        --水果机 209--------------------------------------------------------------------


        ----- 连环夺宝 212--------------------------------------
        SUB_S_SCENE_INFO               = 100,						--场景信息
        SUB_S_LOTTERY_DRAW             = 101,						--开奖结果
        SUB_S_LOTTERY_FILL		       = 102,						--补充数组
        SUB_S_JackPot				   = 103,						--奖池信息
        SUB_S_DRAGONBALL_AWARD		   = 104,						--进入龙珠探宝消息
        SUB_S_CURRENT_GAME_SCORE       = 105,						--玩家当前评价

        SUB_C_BETTING				   = 1,		                    --押注线条
        SUB_C_CONTINUE				   = 2,		                    --继续消除
        SUB_C_TRUSTEE				   = 3,		                    --托管
        SUB_C_SAVEPROFILE			   = 4,		                    --保管存档
        SUB_C_GAMEEND				   = 5,		                    --游戏结束写分
        SUB_C_Test					   = 9,		                    --测试
        ----- 连环夺宝 212--------------------------------------

        ----- 红黑大战 213--------------------------------------
        --服务器命令结构
        SUB_S_GameInfo					    = 301,                --游戏信息
        SUB_S_SysMessage				    = 302,                --系统消息
        SUB_S_SendTableUser				    = 303,                --上桌玩家信息
        SUB_S_ChipSucc					    = 304,                --下注成功
        SUB_S_GameResultInfo			    = 305,                --游戏结算信息
        SUB_S_RequestBankerList			    = 306,                --申请庄家列表
        SUB_S_RequestBanker                 = 307,                --申请庄家(单人申请庄家时发送)
        SUB_S_UpdateBanker                  = 308,                --更新庄家信息(每回合结束都会更新)
        SUB_S_ContiueChip				    = 310,                --玩家续投结果
        SUB_S_UpdateChip				    = 311,                --上一秒的投注总和
        SUB_S_CancelChip				    = 312,                --取消投注

        --客户端命令结构
        SUB_C_UserChip					    = 351,				  --玩家下注
        SUB_C_RequestBanker                 = 352,				  --申请庄家
        SUB_C_ContinueChip                  = 353,				  --玩家续投
        SUB_C_CancelChip				    = 354,				  --取消投注
        ----- 红黑大战 213--------------------------------------


        ------ 捕鱼 362---------------------------------------
	    SUB_C_USER_FIRE                 =    1,
	    SUB_C_EXCHANGE_FISHSCORE        =    2,
	    SUB_C_TIMER_SYNC                =    3,
	    SUB_C_STOCK_OPERATE             =    4,
	    SUB_C_ADMIN_CONTROL             =    5,
	    SUB_C_CATCH_FISH                =    6,
	    SUB_C_BGLU_FISH                 =    7,
	    SUB_C_USER_READY                =    8,
	    SUB_C_USER_FISH_SYNC            =    9,
        SUB_C_USER_CUR_SCORE            =    10,

	    SUB_S_SCENE_FISH				=   100,
	    SUB_S_SCENE_BULLETS				=   101,
	    SUB_S_EXCHANGE_FISHSCORE		=   102,
	    SUB_S_USER_FIRE					=	103,
	    SUB_S_BULLET_DOUBLE_TIMEOUT	    =   104,
	    SUB_S_DISTRIBUTE_FISH			=	105,
	    SUB_S_SWITCH_SCENE				=   106,
	    SUB_S_CATCH_CHAIN				=   107,
	    SUB_S_CATCH_FISH_GROUP			=   108,
	    SUB_S_FORCE_TIMER_SYNC			=   109,
	    SUB_S_TIMER_SYNC				=   110,
	    SUB_S_STOCK_OPERATE_RESULT		=   111,
	    SUB_S_ADMIN_CONTROL			    =   112 ,
	    SUB_S_DISTRIBUTE_FISH_TEAM		=   113,
	    SUB_S_DISTRIBUTE_FISH_CIRCLE	=	114,
	    SUB_S_RETURN_BULLET_SCORE		=   115,
	    SUB_S_USER_FISH_SYNC            =   116,
        SUB_S_USER_SCORE_SYNC           =   117, --同步金币
        SUB_S_USER_FIRE_ERROR			=	118, --发射子弹错误
        ------ 捕鱼 362---------------------------------------

	------ 寻龙夺宝 363 ----------------------------------
	SUB_SL_C_NONE				= 99,
	SUB_SL_C_USER_READY		    = 100,	-- 用户准备
	SUB_SL_S_USER_READY		    = 101,  -- 准备回复
	SUB_SL_C_USER_FIRE			= 102,  -- 用户发射炮弹请求
	SUB_SL_S_USER_FIRE			= 103,	-- 发射炮弹回复
	SUB_SL_C_CATCH_FISH		    = 104,	-- 捕鱼
	SUB_SL_S_CATCH_FISH		    = 105,	-- 捕鱼回复

	SUB_SL_S_SCENE_INDEX		= 106,	-- 当前场景索引
	SUB_SL_S_SWITCH_SCENE		= 107,	-- 切换场景
	SUB_SL_S_SCENE_FISHS		= 108,	-- 场景鱼
	SUB_SL_S_SCENE_BULLETS		= 109,	-- 场景子弹
	SUB_SL_S_DISTRIBUTE_FISH	= 110,	-- 发送鱼组
	SUB_SL_S_BOSS_ATTACK		= 111,	-- BOSS来袭
	SUB_SL_S_SCORE_NO_ENOUGH	= 112,	-- 金币不够
	SUB_SL_S_FROZEN             = 113,	-- 冰冻鱼
	SUB_SL_C_USER_FORWARD       = 114,	-- 自定义消息
	SUB_SL_S_USER_FORWARD       = 115,	-- 自定义消息回复
	SUB_SL_S_USER_RETURN_BULLET = 116,  -- 玩家子弹打的鱼已经不存在，需要返还玩家金币
	------ 寻龙夺宝 363 ----------------------------------

        ----- 斗地主 401--------------------------------------
        SUB_C_CALL_SCORE			    = 1,	        --用户叫分
        SUB_C_OUT_CARD				    = 2,	        --用户出牌
        SUB_C_PASS_CARD				    = 3,	        --用户放弃
        SUB_C_TRUSTEESHIP			    = 4,	        --玩家托管
        SUB_C_ADD_TIMES                 = 5,            --农民加倍

        SUB_S_GAME_START_LANDLORD	    = 100,	        --游戏开始
        SUB_S_CALL_SCORE			    = 101,	        --用户叫分
        SUB_S_BANKER_INFO			    = 102,	        --庄家信息
        SUB_S_OUT_CARD				    = 103,	        --用户出牌
        SUB_S_PASS_CARD				    = 104,	        --用户放弃
        SUB_S_GAME_CONCLUDE			    = 105,	        --游戏结束
        SUB_S_SET_BASESCORE			    = 106,	        --设置基数
        SUB_S_TRUSTEESHIP_NOTICE	    = 107,	        --玩家托管通知
        SUB_S_USER_GENDER               = 108,	        --玩家性别
        SUB_S_ADD_TIMES_NOTIFY          = 112,          --加倍通知（通知农民的加倍操作）
        SUB_S_USER_OUTCARD_FAIL		    = 113,          --玩家出牌失败
        ----- 斗地主 401--------------------------------------


        ----- 二人牛牛 402--------------------------------------
        SUB_S_SysMessage_TwoNiuNiu	    = 220,				--系统消息
        SUB_S_CALL_BANKER               = 100,				--开始叫庄
        SUB_S_NOT_CALL                  = 101,				--玩家不叫
        SUB_S_DOWN_JETTON			    = 102,				--用户下注
        SUB_S_SEND_CARD				    = 103,				--庄家信息
        SUB_S_USER_FINISH			    = 104,				--配牌完成
        SUB_S_GAME_END_TWONIUNIU        = 105,				--游戏结束

        SUB_C_CALL_BANKER               = 1,                --用户叫庄
        SUB_C_DOWN_JETTON_TWONIUNIU     = 2,                --用户下注
        SUB_C_TAKE_CARD                 = 3,                --提交扑克
        ----- 二人牛牛 402--------------------------------------



        ----- 二人梭哈 403--------------------------------------
        --服务器命令结构
        SUB_S_GAME_START_TWOSHOWHAND    = 100,              --游戏开始
        SUB_S_ADD_SCORE                 = 101,              --加注结果
        SUB_S_GIVE_UP                   = 102,              --放弃跟注
        SUB_S_SEND_CARD                 = 103,              --发牌消息
        SUB_S_SHOW_HAND                 = 104,              --玩家梭哈
        SUB_S_GAME_END_TWOSHOWHAND      = 105,              --游戏结束
        SUB_S_WAIT_OPEN_CARD            = 106,              --等待开牌
        SUB_S_OPEN_CARD_NOTIFY          = 107,              --开牌通知
        SUB_S_LOOK_CARD_NOTIFY          = 108,              --看牌通知
        SUB_S_LOOK_ENEMY_CARD           = 110,              --看对方底牌
        SUB_S_SysMessage_TWOSHOWHAND    = 220,              --系统消息

        -- 玩家操作
        SUB_C_ADD_SCORE                 = 1,                --用户加注
        SUB_C_NOT_ADD                   = 2,                --用户不加
        SUB_C_FOLLOW                    = 3,                --用户跟注
        SUB_C_GIVE_UP                   = 4,                --放弃跟注
        SUB_C_SHOW_HAND                 = 5,                --梭哈
        SUB_C_OPEN_CARD                 = 6,                --开牌
        SUB_C_LOOK_BASIC_CARD           = 7,                --看牌
        ----- 二人梭哈 --------------------------------------


        ----- 炸金花 407--------------------------------------

        --客户端命令结构
        GF_SUB_C_ADD_SCORE              = 1,			--用户加注
        GF_SUB_C_GIVE_UP		        = 2,			--放弃消息
        GF_SUB_C_COMPARE_CARD	        = 3,			--比牌消息
        GF_SUB_C_LOOK_CARD		        = 4,			--看牌消息
        GF_SUB_C_OPEN_CARD		        = 5,			--开牌消息
        GF_SUB_C_WAIT_COMPARE           = 6,			--等待比牌
        GF_SUB_C_FINISH_FLASH	        = 7,			--完成动画
        SUB_C_SHOW_CARD                 = 122,
        SUB_C_WAIT_COMPARE_CANCEL		= 9,			--取消等待比牌
	    --机器人命令
        GF_SUB_A_GETALLCARD             = 8,		    --获取扑克
        SUB_C_ANIMATE_END               = 118,          --发牌动画完成
        SUB_S_USE_PROPERTY              = 120,          --使用道具
        SUB_S_NO_COMPARE_STATUS         = 121,          --禁止比牌的状态
        SUB_C_USE_PROPERTY              = 8,            --玩家使用道具
                  
        --服务器命令结构
        GF_SUB_S_GAME_START             = 100,          --游戏开始
        GF_SUB_S_ADD_SCORE		        = 101,			--加注结果
        GF_SUB_S_GIVE_UP		        = 102,			--放弃跟注
        GF_SUB_S_COMPARE_CARD	        = 105,			--比牌跟注
        GF_SUB_S_LOOK_CARD		        = 106,			--看牌跟注
        GF_SUB_S_SEND_CARD		        = 103,			--发牌消息
        GF_SUB_S_GAME_END		        = 104,			--游戏结束
        GF_SUB_S_PLAYER_EXIT	        = 107,			--用户强退
        GF_SUB_S_OPEN_CARD		        = 108,			--开牌消息
        GF_SUB_S_WAIT_COMPARE	        = 109,			--等待比牌
        SUB_S_BACK_SCORE                = 110,          --给玩家返还金币
        GF_SUB_S_GETALLCARD             = 115,			--获取玩家扑克
        SUB_S_GAME_READ                 = 119,          --游戏准备     
        --GF_SUB_S_WRITE_USER_SCORE	    = 120,			--游戏写分
        GF_SUB_S_ANDROID_GET_CARD	    = 112,		    --机器人专用命令:获取扑克
        SUB_S_SHOW_CARD                 = 123,          --显示自己的牌
        SUB_S_CURRENT_CHAIRID           = 124,          --当前说话的玩家
        SUB_S_WAIT_COMPARE_CANCEL		= 125,			--取消等待比牌

        ----- 炸金花 407--------------------------------------

        ------ 血拼牛牛 409-----------------------------------
        SUB_S_SysMessage_XPNN			= 220,          --系统消息
        SUB_S_GAME_START_XPNN			= 100,          --开始叫庄
        SUB_S_CALL_BANKER_NOTIFY        = 101,          --玩家抢庄通知
        SUB_S_DOWN_JETTON_XPNN			= 102,          --通知进入下注阶段
        SUB_S_DOWN_JETTON_NOTIFY        = 103,          --玩家下注通知
        SUB_S_SEND_CARD_XPNN            = 104,          --发最后一张牌
        SUB_S_USER_FINISH_XPNN			= 105,          --玩家完成配牌
        SUB_S_GAME_END_XPNN				= 106,          --游戏结束
        
        SUB_C_CALL_BANKER_XPNN			= 1,            --用户叫庄
        SUB_C_DOWN_JETTON_XPNN			= 2,            --用户下注
        SUB_C_TAKE_CARD_XPNN            = 3,            --提交扑克	
        ------ 血拼牛牛 409-----------------------------------

        ------ 火拼麻将 410-----------------------------------
        --接收服务器
        SUB_S_SYSMESSAGE_HPMJ			= 220,          --系统消息
        SUB_S_GAME_START_HPMJ	        = 100,          --游戏开始
        SUB_S_OUT_CARD_NOTIFY_HPMJ		= 101,          --出牌通知
        SUB_S_SEND_SINGLE_CARD_HPMJ		= 102,			--摸牌通知
        SUB_S_OPERATE_MASK_HPMJ	    	= 103,			--操作提示
        SUB_S_OPERATE_NOTIFY_HPMJ       = 104,			--操作通知
        SUB_S_USER_ADD_NOTIFY_HPMJ      = 105,			--加倍通知
        SUB_S_GAME_END_HPMJ				= 106,			--游戏结束
        SUB_S_FINISH_TASK_HPMJ			= 107,			--完成任务通知
        SUB_S_TING_CARD_NOTIFY_HPMJ		= 108,			--听牌通知
        SUB_S_FAN_COUNT_NOTIFY_HPMJ		= 109,			--胡牌番数通知
        SUB_S_LISTEN_CARD_INFO_HPMJ	    = 110,			--听牌信息
        SUB_S_OPP_CARD_DATA_NOTIFY_HPMJ = 111,			--对面数据通知
        SUB_S_FORCE_TRUSTEE_SHIP_HPMJ	= 112,			--强制托管
        SUB_S_SERVER_TRUSTEE_SHIP_HPMJ  = 113,			--服务器托管
        SUB_S_LOOK_ENEMY_CARD_HPMJ      = 114,			--看对方明牌
        SUB_S_SEND_LIB_CARD             = 115,			--库存牌

        --客户端发送
        SUB_C_USER_OUT_CARD_HPMJ		= 1,			--用户出牌
        SUB_C_USER_OPERATION_HPMJ		= 2,			--用户操作
        SUB_C_LISTEN_CARD_HPMJ			= 3,			--听牌
        SUB_C_USER_ADD_TIMES_HPMJ		= 4,			--加倍
        SUB_C_GET_LIB_CARD		        = 5,			--获取库存牌
        SUB_C_WANNA_CARD		        = 6,			--请求下一次发想要的牌
        SUB_S_CANCEL_TRUSTEE_SHIP_HPMJ	= 255,			--取消托管
        ------ 火拼麻将 410-----------------------------------

        ------ 抢庄牛牛 413-----------------------------------
        SUB_S_SysMessage_QZNN			= 220,          --系统消息
        SUB_S_GAME_START_QZNN			= 100,			--开始叫庄
        SUB_S_CALL_BANKER_NOTIFY_QZNN   = 101,	        --玩家抢庄通知
        SUB_S_DOWN_JETTON_QZNN			= 102,			--通知进入下注阶段
        SUB_S_DOWN_JETTON_NOTIFY_QZNN	= 103,	        --玩家下注通知
        SUB_S_SEND_CARD_QZNN			= 104,			--发牌
        SUB_S_USER_FINISH_QZNN			= 105,			--某个玩家完成配牌
        SUB_S_GAME_END_QZNN			    = 106,			--游戏结束
        
        SUB_C_CALL_BANKER_QZNN			= 1,			--用户叫庄
        SUB_C_DOWN_JETTON_QZNN			= 2,			--用户下注
        SUB_C_TAKE_CARD_QZNN			= 3,			--提交扑克
        ------ 抢庄牛牛 413-----------------------------------

        ------ 通比牛牛 404-----------------------------------
        SUB_S_SysMessage_TBNN			= 220,			--系统消息
        SUB_S_GAME_START_TBNN			= 100,			--开始
        SUB_S_TAKE_NOTIFY_TBNN          = 101,			--开牌
        SUB_S_GAME_END_TBNN				= 105,			--游戏结束
        SUB_S_UPDATA_JP_TBNN            = 106,          --更新彩金池
        SUB_S_ADD_JP_RECORD_TBNN        = 107,          --添加彩金记录
        SUB_S_YOUR_JP_REWARD_TBNN       = 108,          --你的彩金消息
        SUB_S_AI_Ready                  = 110,

        SUB_C_TAKE_CARD_TBNN			= 1				--提交扑克
        ------ 通比牛牛 404-----------------------------------

    --------------------------------------- 游戏相关 -------------------------------------------
}
