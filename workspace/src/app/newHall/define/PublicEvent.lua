--region *.lua
--Date
--
--endregion

local prefixFlag = "Public_Event_"

cc.exports.Public_Events =
{
    MSG_UPDATE_USER_INSURE  = prefixFlag .. "updateUserInsure",                --更新存款
    MSG_UPDATE_USER_SCORE   = prefixFlag .. "updateUserScore",                 --更新金币
    MSG_UDPATE_USR_SCORE    = prefixFlag .. "updateUsrScore1",                 --更新金币
    MSG_BANK_INFO           = prefixFlag .. "bankInfo",                        --更新银行
    MSG_BANK_STATUS         = prefixFlag .. "bankStatus",                      --更新银行状态
    MSG_BANK_SUCCESS_INFO   = prefixFlag .. "bankSuccessInfo",                 --操作银行成功
    MSG_BANK_CLEAR_INFO     = prefixFlag .. "bankClearInfo",                   --清除银行信息
    MSG_SHOW_MESSAGEBOX     = prefixFlag .. "showMessageBox",                  --显示金币不足弹框
    MSG_SHOW_SHOP           = prefixFlag .. "showShop",                        --显示商店
    MSG_SHOW_SHOP_HALL      = prefixFlag .. "showShopAtHall",                  --显示商店
    MSG_SHOW_BANK           = prefixFlag .. "showBank",                        --显示银行
    MSG_SHOW_REGISTER       = prefixFlag .. "showRegister",                    --显示注册页面
    MSG_UPDATE_USER_STATUS  = prefixFlag .. "updateUserStatus",                --更新用户状态
    MSG_SIT_SUCCESS         = prefixFlag .. "sitSuccess",                      --坐下成功
    MSG_USER_READY          = prefixFlag .. "userReady",                       --用户准备
    MSG_USER_LEAVE          = prefixFlag .. "userLeave",                       --用户离开
    MSG_USER_SIT            = prefixFlag .. "userSit",                         --用户坐下
    MSG_USER_COMEBACK       = prefixFlag .. "userComeBack",                    --用户回来
    MSG_UPDATE_TABLESTATE   = prefixFlag .. "updateTableState",                --更新桌子状态
    MSG_USER_FREE           = prefixFlag .. "userFree",                        --用户空闲状态
    MSG_GO_SIT              = prefixFlag .. "goSit",                           --发送坐下
    MSG_GAME_EXIT           = prefixFlag .. "gameExit",                        --退出游戏
    MSG_NETWORK_FAILURE     = prefixFlag .. "disConnect",                      --网络连接失败
    MSG_APP_RESIGN_ACTIVE   = prefixFlag .. "resignactive",                    --app失去活跃状态(IOS接电话,双击home键等,没有退到后台)
    MSG_APP_SYNCH_GAME_DATA = prefixFlag .. "synchGameData",                   --app失去活跃状态一定时间后，需要同步游戏数据  
    MSG_GET_AGENT           = prefixFlag .. "getAgentBack",                    --获取代理人信息回调
    MSG_GAME_LOGIN_SUCCESS  = prefixFlag .. "loginSuccess",                    --非对战游戏进入游戏

    MSG_GAME_LOAD_SUCCESS   = prefixFlag .. "loadSuccess",                     --加载游戏完成
    MSG_GAME_TABLE_SUCCESS  = prefixFlag .. "tableSuccess",                    --选桌完成

    MSG_GAME_ENTER_MAIN     = prefixFlag .. "entergamemian",                   --进入游戏主界面
    MSG_GAME_LEAVE_GAME     = prefixFlag .. "leavegamemian",                   --离开游戏主界面
    MSG_GAME_ENTER_TBCHOOSE = prefixFlag .. "entertablechoose",                --进入游戏选桌界面
    MSG_UPDATE_ROLL_MSG     = prefixFlag .. "updateRollMsg",                   --更新滚动消息
    
    MSG_FISH_BROADCAST_FRST = prefixFlag .. "fishBroadCastFirst",              --广播消息(需要首先播放）
    MSG_UPDATE_CHAT_MSG_TEXT = prefixFlag .. "updateChatMsgText",              --更新聊天消息
    MSG_GET_GIFT            = prefixFlag .. "getGift",                         --领取红包
    MSG_FISG_DEL_PLAYER     = prefixFlag .. "delPlayer",                       --删除玩家
    MSG_APPSTORE_STATUS     = prefixFlag .. "getappstorestatus",               -- 收到appstore充值信息
    MSG_SHOW_FISH_EFFECT    = prefixFlag .. "showfisheffect",                  -- 显示捕鱼特效

    -- add by nick -------------------------------------------------------
    MSG_USER_ENTER          = prefixFlag .. "userEnter",        --用户进入
    MSG_USER_LOOKON         = prefixFlag .. "userlookon",       --用户旁观
    MSG_USER_OFFLINE        = prefixFlag .. "userOffline",      --用户离线
    MSG_USER_WAIT           = prefixFlag .. "userWait",         --用户等待
    MSG_USER_RELOGIN        = prefixFlag .. "relogin",          --重新登录

    MSG_LOGON_KICK_RESPOSE  = prefixFlag .. "kick_repose",      --踢人
    MSG_TIMER_CHECKER       = prefixFlag .. "timer_checker",    --网络检测

    MSG_GAME_LOGIN_SUCCESS  = prefixFlag .. "login_success",     --登录成功（进入游戏）
    MSG_GAME_LOGIN_FAIL     = prefixFlag .. "login_fail",       --登录失败

    MSG_HALL_RELOGIN_SUCCESS= prefixFlag .. "relogin_success",  --重连成功
    MSG_HALL_RELOGIN_FAIL   = prefixFlag .. "relogin_fail",     --重连失败

    MSG_SHOW_VEIL           = prefixFlag .. "showVeil",         --显示遮罩

    --其他玩家列表金币变化
    MSG_UPDATE_OTHER_USER_GOLD      = prefixFlag .. "other_user_gold",
    MSG_UPDATE_OTHER_USER_GOLD_GR   = prefixFlag .. "other_user_gold_gr",

    --自定义玩家消息

    --通用后台消息
    MSG_ENTER_FOREGROUND    = prefixFlag .. "enter_foreground", --恢复前台
    MSG_ENTER_BACKGROUND    = prefixFlag .. "enter_background", --进入后台
    MSG_ENTER_FOREGROUND_RECHARGE    = prefixFlag .. "enter_foreground_recharge", --恢复前台
    --通用网络消息
    MSG_SOCKET_DISCONNECT   = prefixFlag .. "socket_disconnect", --socket断开
    MSG_SOCKET_RECONNECT    = prefixFlag .. "socket_reconnect",  --socket重连
    MSG_SOCKET_NONETWORK    = prefixFlag .. "socket_nonetwork",  --socket超时
    MSG_NETWORK_CHANGE      = prefixFlag .. "network_change",    --网络变化

    --遮罩相关
    MSG_VEIL_TIME_OUT       = prefixFlag .. "veil_timeout", --遮罩超时

    --返回大厅
    MSG_SHOW_CONFIRM        = prefixFlag .. "show_confirm", --返回大厅

    --取消热更
    MSG_UPDATE_CANCEL       = prefixFlag .. "update_cancel",    --取消强更

    --登录切大厅
    MSG_LOGIN_TO_HALL       = prefixFlag .. "login_to_hall",    --登录切大厅

    Login_Entry             = "Hall_Event_loginEntry",  --登录
    Hall_Entry              = "Hall_Event_hallEntry",   --大厅
    Load_Entry              = "Hall_Event_loadEnter",   --重连

    MSG_FISH_CLOSE         = prefixFlag .. "fish_close", --关闭捕鱼

    MSG_UPDATE_HALL            = "Update_hall", --更新大厅

    MSG_CHECK_UPDATE_DONE     = prefixFlag .. "check_update_done",  --检查更新完


    MSG_GAME_TO_TABLE = prefixFlag .. "game_to_table", --离开游戏返回选桌

    --断线重连新增事件
    MSG_GAME_NETWORK_FAILURE    = prefixFlag .. "game_network_failure",
    MSG_GAME_ENTER_BACKGROUND   = prefixFlag .. "game_enter_background",
    MSG_GAME_ENTER_FOREGROUND   = prefixFlag .. "game_enter_foreground",
    MSG_GAME_RELOGIN_SUCCESS    = prefixFlag .. "game_reload_success",

    --新增获取公告时间(18年10月)
    MSG_GET_NOTICE_BACK   = prefixFlag .. "msg_get_notice_back",
    MSG_GET_NOTICE_STATUS = prefixFlag .. "msg_get_notice_status",
    MSG_CLOSE_NOTICE_LAYER  = prefixFlag .. "msg_close_notice_layer",

    --关闭公告/消息事件
    MSG_GET_MESSGE_BACK     = prefixFlag .. "msg_get_message_back",
    MSG_GET_MESSGE_STATUS   = prefixFlag .. "msg_get_message_status",
    MSG_DEL_MESSAGE_BACK    = prefixFlag .. "msg_del_message_back",
    MSG_CLOSE_MESSAGE_LAYER = prefixFlag .. "msg_close_message_layer",

    --重启游戏
    MSG_GAME_REBOOT         = prefixFlag .. "msg_reboot_game",

    --重加载动画
    MSG_RESET_ANIMATION     = prefixFlag .. "msg_reset_animation",

    --充值成功
    MSG_RECHARGE_SUCCESS    = prefixFlag .. "msg_recharge_success",

    --提现记录返回
    MSG_EXCHARGE_QUERY      = prefixFlag .. "msg_excharge_query",

    --登录时需要手机校验码
    MSG_LOING_VERIFY_PHONE  = prefixFlag .. "msg_login_verify",

    --打开大厅界面
    MSG_OPEN_HALL_LAYER     = prefixFlag .. "msg_open_hall_layer", 

    --打开大厅提示
    MSG_OPEN_HALL_NOTICE     = prefixFlag .. "msg_open_hall_notice", 

    MSG_SHOW_USER_ID = prefixFlag .. "show_user_id",
    MSG_SHOW_ROLLMSG_BOX = prefixFlag .. "show_roll_msg_box",

    --恢复背景音乐
    MSG_PLAY_BACKGROUND_MUSIC = prefixFlag .. "msg_play_background_music",

    --新手引导
    MSG_GUIDE_PLAY_GAME = prefixFlag .. "msg_guide_play_game",
    -- add by nick -------------------------------------------------------

    -- add by goblin -----------------------------------------------------
    MSG_RECHARGE_WEB      = prefixFlag .. "recharge_web",   --网页充值
    MSG_OPEN_URL          = prefixFlag .. "open_url",    --open url
    -- add by goblin -----------------------------------------------------

    -- add by danhuang -----------------------------------------------------
    MSG_START_UPDATE                = prefixFlag .. "start_update",             --开始热更
    MSG_RETRY_UPDATE                = prefixFlag .. "retry_update",             --重试热更

    MSG_FORCE_UPDATE_RESPONSE       = prefixFlag .. "forceupdate_reponse",      --强更http响应返回
    MSG_FORCE_UPDATE_START          = prefixFlag .. "forceupdate_start",        --更新app开始
    MSG_FORCE_UPDATE_FINISH         = prefixFlag .. "forceupdate_finish",       --更新app结束
    MSG_FORCE_UPDATE_PROCESS        = prefixFlag .. "forceupdate_process",      --更新app进度

    MSG_UPDATE_REQUEST_HOST         = prefixFlag .. "reques_host_finish",       --请求http获得ip完成

    MSG_UPDATE_REQUEST_IP           = prefixFlag .. "reques_ip_finish",         --请求http获得新ip完成

    MSG_RETRY_CHECK_ADDRESS         = prefixFlag .. "retry_check_address",      --重新检查服务器地址

    MSG_COMFIRM_REQUEST_BANKER      = prefixFlag .. "comfirm_request_banker",   --确认上庄

    MSG_RECHARGE_DETAIL_BACK        = prefixFlag .. "recharge_detail_back",     --充值详情返回

    MSG_QUERY_RECHARGE              = prefixFlag .. "query_recharge",           --查询充值结果

    MSG_BANK_OPERATE_SUCCESS        = prefixFlag .. "operate_bank_success",     --操作银行结果

    MSG_BANK_OPERATE_FAILED         = prefixFlag .. "operate_bank_failed",      --操作银行结果

    MSG_MYSELF_FREE                 = prefixFlag .. "myself_free",              --自己起立成功

    MSG_CLOSE_SUB_VIEW              = prefixFlag .. "close_sub_view",           --清除游戏内通用界面

    MSG_LOGIN_NETWORK_FAILURE     = prefixFlag .. "disConnectLogin",            --登录中网络连接失败
    
    MSG_LOING_VERIFY_PHONE          = prefixFlag .. "login_verify_phone",       --登录验证手机

    MSG_CLOSE_LOGIN_BEAN            = prefixFlag .. "close_login_bean_view",    --关闭登录注册流程中的子界面

    MSG_UPDATE_ONLINE               = prefixFlag .. "update_online",            --更新在线人数

    MSG_USER_DELETE                 = prefixFlag .. "user_delete",              --删除同房间玩家

    MSG_SHOW_INGAMEBANK             = prefixFlag .. "show_ingamebank",          --游戏显示银行

    MSG_GAME_TABLE_EXIT             = prefixFlag .. "game_table_exit",          --退出选桌游戏

    MSG_LOGIN_NETWORK_FAILURE       = prefixFlag .. "disConnectLogin",          --登录中网络连接失败

    MSG_POP_NOTICE                  = prefixFlag .. "pop_notice",               --公告弹窗

    MSG_GET_GAME_GIFT               = prefixFlag .. "get_game_gift",            --游戏中获得红包


    MSG_BACK_HALL_CLEAR             = prefixFlag .. "back_hall_clear",        --游戏中获得红包

    MSG_HIDE_INGAMEBANK             = prefixFlag .. "HIDE_ingamebank",          --游戏内显示银行

    MSG_OPEN_BROADCAST              = prefixFlag .. "open_broadcast",          --游戏内显示银行
    -- add by danhuang -----------------------------------------------------


    MSG_CLOSE_CURRENT_DIALOG        = prefixFlag .. "close_current_dialog",   --关闭对话框

    MSG_LOGON_FINISH                = prefixFlag .. "logon_finish",           --登录完成

    MSG_NULL_CLOSE_ENABLED          = prefixFlag .. "null_close_enabled",     --设置响应空白处关闭
    ---------------

    MSG_LOCAL_PHOTO                 = prefixFlag .. "local_photo_save",         --本地选择照片

    MSG_SUPPORT                     = prefixFlag .. "msg_support", --获取到当前支持充值类型消息

    MSG_PC_REFRESH_SCORE            = prefixFlag .. "pcRefreshUserScore",          --pc刷新金币
}
return cc.exports.Public_Events
