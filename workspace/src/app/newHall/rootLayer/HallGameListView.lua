local UpdateController = require("src.app.assetmgr.main.control.RenewController")

 import("..data.GameSwitchData")

local HNlayer = import("..HNLayer")
local HallGameListView = class("HallGameListView", HNlayer)

local DownLoadResView = import("..bean.DownLoadResView")
local DownloadResMgr = import("..manager.DownloadResMgr")

local CGameClassifyDataMgr  = import("..manager.CGameClassifyDataMgr")
local GameListManager       = import("..manager.GameListManager")
local SpineManager      = import("..manager.SpineManager")

function HallGameListView:ctor(parent)
    --self:enableNodeEvents()

    self.m_pParent = parent

    self.m_root_lv = nil
    self.m_lv_items = {}
    self.room_list_data = {}
   
    self.m_pDownloadItem = {}  
    self.m_listMaxWidth = 1334

    self.m_cellWidth = 320   -- 每个游戏图标的宽度   

    self._currentSelectedGame = nil --当前选择游戏
    self.progress = {}              --游戏下载进度条
    self.progressBg = {}            --进度条背景
    self.m_pImageTimer = {}
    self.proLabel = {}              --进度条数字
    self.gameBtn = {}               --游戏按钮

    self.__downloadStatus = 0 		-- 没下载
    if GlobalConf.UPDATE_GAME == false then
        self.__downloadStatus = 2
    end
    
    --游戏列表
    self.m_root_lv = ccui.ListView:create()
    self:addChild(self.m_root_lv)
    self.m_root_lv:setAnchorPoint(0, 0)
    self.m_root_lv:setPosition(0, 0)
    self.m_pStopScroll = false
    self.m_gamelistPosArray = {}
    self:InitGamelistPosArray()

    self.m_pGameClassifyTitleLight = nil
    self.m_titleLightPosx = 1334 + (display.size.width - 1334)/2 - 110
    self.m_titleLightPosy = 560

    self.m_fTouchTime = {}

    self._winSize = cc.Director:getInstance():getWinSize() 

    ToolKit:registDistructor(self, handler(self, self.onDestory))
end


function HallGameListView:init(nGameType, showClassify, bCheck)
    if nGameType == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_ALL then
        if CommonUtils.getIsBaiduCheck() then
            self.room_list_data = {408,208,403,205,215}
        else
            self.room_list_data = GameListManager.getInstance():getAllGameKindId()
        end 
    elseif nGameType == 1 then
        self.room_list_data = {1,4,3,8,6,9,13,14,}
    elseif nGameType == 2 then
        -- self.room_list_data = {2,12,0}
        self.room_list_data = {2,12,0,11,16,7,5}
    elseif nGameType == 3 then
        self.room_list_data = {200,201,202,203,204,205}
    elseif nGameType == 4 then
        -- self.room_list_data = {11,16,7,5} -- 隐藏通比牛牛
        -- --self.room_list_data = {11,16,7,5,10,196} --测试使用，通比牛牛，跑得快
        self.room_list_data = {}
    elseif nGameType == 5 then
        self.room_list_data = {15}
    elseif 6 == nGameType then --第三方捕鱼
    elseif 7 == nGameType then --第三方电子
    elseif 8 == nGameType then --第三方体育
    end
    self:initRoomList( true, showClassify )
    if self.m_pGameClassifyTitleLight == nil then
        local pSpine = "hall/effect/325_fengleitubiao_donghua/325_fengleitubiao_donghua"
        self.m_pGameClassifyTitleLight = sp.SkeletonAnimation:createWithBinaryFile(pSpine .. ".skel", pSpine .. ".atlas", 1)
        self.m_pGameClassifyTitleLight:addTo(self)
    end
    if self.m_pGameClassifyTitleLight ~= nil then
        self.m_pGameClassifyTitleLight:setPosition(cc.p(self.m_titleLightPosx, self.m_titleLightPosy))
        local strAnimation = "animation1"
        if nGameType == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_ARCADE       then strAnimation = "animation3"
        elseif nGameType == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_TIGER    then strAnimation = "animation4"
        elseif nGameType == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_FISH     then strAnimation = "animation1"
        elseif nGameType == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_CARD     then strAnimation = "animation2"
        elseif nGameType == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_CASUAL   then strAnimation = "animation5"
        end
        self.m_pGameClassifyTitleLight:setAnimation(0, strAnimation, true)
        self.m_pGameClassifyTitleLight:update(0)   
        self:SetGameClassifyTitleShow( true, 0.4 )     
    end
    if bCheck then 
        self.m_root_lv:setVisible(false);
        self.m_pGameClassifyTitleLight:setVisible(false)
    end
    
    if G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_CASUAL == nGameType then
        self.m_root_lv:setVisible(false);
        local pUI = require("src.app.hall.MatchGameList.view.MatchGameListView").new();
        self.m_pMatchGameListView = pUI;
        self:addChild(pUI);
        local size = self:getContentSize();
        pUI:setPositionY(-110);
        --pUI:setPositionX(size.width - 1334);
        
        -- local MatchController = require("src.app.hall.MatchGameList.control.MatchController");
        -- local pUI = MatchController.getInstance():openView("MatchGameListView");
        self.m_pGameClassifyTitleLight:setVisible(false)
    elseif 6 == nGameType or 7 == nGameType or 8 == nGameType then
        self.m_root_lv:setVisible(false);
        self.m_pGameClassifyTitleLight:setVisible(false)
        local pUI = require("src.app.newHall.rootLayer.HallThirdPartyGameLayer").new(nGameType)
        self.m_thirdGameLayer = pUI
        self:addChild(pUI)
        pUI:setPositionY(-110)
    end

    addMsgCallBack(self, MSG_GAME_UPDATE_PROGRESS, handler(self, self.onGameUpdateProgress))
    addMsgCallBack(self, MSG_REQ_DOWANLOADSTATE, handler(self, self.getDownloadStatus))
    addMsgCallBack(self, MSG_UPDATE_OR_DOWNLOAD_GAME, handler(self, self.onUpdateOrDownLoadGame))
end

function HallGameListView:onDestory()
    removeMsgCallBack(self, MSG_GAME_UPDATE_PROGRESS)
    removeMsgCallBack(self, MSG_UPDATE_OR_DOWNLOAD_GAME)
    removeMsgCallBack(self, MSG_REQ_DOWANLOADSTATE)
end

function HallGameListView:shutDown()
    if not tolua.isnull( self.m_pMatchGameListView) then
        self:removeChild(self.m_pMatchGameListView);
    end
    self.m_pMatchGameListView = nil;
    if not tolua.isnull(self.m_thirdGameLayer) then
        self:removeChild(self.m_thirdGameLayer)
    end
    self.m_thirdGameLayer = nil

    for i in pairs(self.m_lv_items) do
        self.m_lv_items[i]:removeFromParent()
        self.m_lv_items[i] = nil
    end
    self.progress = {}
    self.progressBg = {}
    self.m_pImageTimer = {}
    self.proLabel = {}
    self.gameBtn = {}
end


function HallGameListView:SetGameClassifyTitleShow( bShow, showTime )
    if self.m_pGameClassifyTitleLight == nil then return end
    if bShow then
        self.m_pGameClassifyTitleLight:setVisible(true)
        self.m_pGameClassifyTitleLight:setOpacity(0)
        local posx, posy = self.m_titleLightPosx, self.m_titleLightPosy
        self.m_pGameClassifyTitleLight:setPosition( cc.p(posx, posy + 200) )
        local moveAct = cc.EaseBackOut:create( cc.MoveTo:create(showTime, cc.p(posx,posy)) )
        local spawnAct = cc.Spawn:create(cc.FadeIn:create(showTime), moveAct)
        self.m_pGameClassifyTitleLight:runAction( spawnAct )
    else
        local moveAct = cc.MoveTo:create(0.2, cc.p(self.m_titleLightPosx, self.m_titleLightPosy + 200))
        local spawnAct = cc.Spawn:create(cc.FadeOut:create(showTime), moveAct)
        local callback = cc.CallFunc:create(function ()
            self.m_pGameClassifyTitleLight:setVisible(false)
        end)
        local seq = cc.Sequence:create(spawnAct,callback)
        self.m_pGameClassifyTitleLight:runAction( seq )
    end
   -- print("SetGameClassifyTitleShow: x: " .. self.m_pGameClassifyTitleLight:getPositionX() .. " y: " .. self.m_pGameClassifyTitleLight:getPositionY() )
end

function HallGameListView:PlayCloseGameListAnimation( clearTime )
-- 如果之前有显示列表，先做该列表的渐隐效果
    local GameKindID = GameListManager.getInstance():getGameType()
    self.m_pStopScroll = true
    for i in pairs(self.m_lv_items) do
        local itemNode = self.m_lv_items[i]
        if itemNode ~= nil then
            itemNode:setTouchEnabled(false)
            itemNode:stopAllActions()
            if GameKindID == itemNode:getTag() then 
                itemNode:runAction( cc.FadeTo:create(clearTime, 180) )
            else                
                itemNode:runAction( cc.FadeTo:create(clearTime, 60) )
            end
        end
    end   
    self:SetGameClassifyTitleShow(false, clearTime)
end
function HallGameListView:PlayAnimationBackToHall()   
    --local nStartX = self.m_listMaxWidth + 500 -- 先设置位置在列表显示区域右则
    --local scrollPos = self.m_root_lv:getInnerContainerPosition()
    --local scrollIndex = math.floor( math.abs(scrollPos.x) / self.m_cellWidth )
    local listTotal = table.nums( self.m_lv_items )
    for i in pairs(self.m_lv_items) do
        --local posIndex = math.floor((i-1)/2)
        local posIndex = i
        local perTime = 0.04
        if listTotal > 6 then
            posIndex = math.floor((i-1)/2)
            perTime = 0.05
        end
        local fDelayTime = 0.1 + posIndex * perTime
        -- local fMoveTime =  0.3 + posIndex * 0.18
        --  local diffX = 150 + posIndex * 3
        local fMoveTime = 0.3
        local diffX = 80
        local itemNode = self.m_lv_items[i]
        local pos = self:GetItemPosByIndex( listTotal, i )
        itemNode:setPosition( cc.p(pos.x + diffX, pos.y) )
            
        itemNode:setOpacity(0)
        local ease = cc.EaseBackOut:create(cc.MoveTo:create(fMoveTime, pos))                
        local fade = cc.FadeIn:create(fMoveTime * 0.3) 
        local spwan = cc.Spawn:create(ease, fade)
        local action = cc.Sequence:create(
            cc.DelayTime:create(fDelayTime),
            spwan
        )
        local TAG_ACTION = 10101
        action:setTag(TAG_ACTION)
        if itemNode ~= nil then
            itemNode:setTouchEnabled(true)    --这里要注意，在渐隐效果里面有设置不可点击，所以这里强制设置可以点击
            itemNode:stopActionByTag(TAG_ACTION)
            --itemNode:setPositionX(nStartX)
            itemNode:runAction(action)
        end
    end
end
 

function HallGameListView:onScrollEvent(sender, _event)
    -- 设置箭头显示
    if self.m_pStopScroll then return end
    local pos = sender:getInnerContainerSize()
    if self.m_root_lv:getContentSize().width >= self.m_listMaxWidth - 50 then
        self.m_pParent:refreshArrow(false, false)
    elseif math.floor( pos.x ) <=  math.floor( self.m_root_lv:getContentSize().width - self.m_listMaxWidth ) then
        self.m_pParent:refreshArrow(true, false)
    elseif pos.x >= 0 then
        self.m_pParent:refreshArrow(false, true)
    end
end

function HallGameListView:initRoomList(bAnimation, showClassify)
    self.m_pStopScroll = false
    for i in pairs(self.m_lv_items) do
        self.m_lv_items[i]:removeFromParent()
        self.m_lv_items[i] = nil
    end
    self.m_root_lv:setVisible(true);
    self.m_root_lv:removeAllItems()
    self.m_lv_items = {}
    self.m_pDownloadItem = {}
    self:reload(bAnimation, showClassify)
    self:PlayAnimationBackToHall()
end

function HallGameListView:InitGamelistPosArray()
    self.m_gamelistPosArray[1] = { cc.p(450, 150) }
    self.m_gamelistPosArray[2] = { cc.p(200, 150), cc.p(700, 150) }
    self.m_gamelistPosArray[3] = { cc.p(50, 150), cc.p(450, 150), cc.p(850, 150) }
    self.m_gamelistPosArray[4] = { cc.p(200, 250),   cc.p(700, 250),   cc.p(200, 0),  cc.p(700, 0) }
    self.m_gamelistPosArray[5] = { cc.p(50, 250), cc.p(450, 250), cc.p(850, 250), cc.p(250, 0),  cc.p(650, 0) }
    self.m_gamelistPosArray[6] = { cc.p(50, 250), cc.p(450, 250), cc.p(850, 250), cc.p(50, 0), cc.p(450, 0), cc.p(850, 0) }
end

-- 根据索引获取对象坐标
function HallGameListView:GetItemPosByIndex( listTotal, index )
    if listTotal == 0 then return end
    local resultPt = cc.p(0, 0)
    local startx = 330   -- x轴起始坐标
    local starty = 160   -- y轴起始坐标
     
    if listTotal <= #self.m_gamelistPosArray and listTotal >= index then
    --配置的坐标
       local listPos = self.m_gamelistPosArray[listTotal]
       if index <= #listPos then
            resultPt.x = listPos[index].x + startx + 40
            resultPt.y = listPos[index].y + starty
       end
    else 
    --程序计算的坐标
        local offx =  math.floor((index-1)/2)*self.m_cellWidth
        local offy =  0
        if index%2 ~= 0 then
            offy = 250
        end
        resultPt.x = offx + startx
        resultPt.y = offy + starty
    end
    return resultPt
end

-- 获取滑动框的size
function HallGameListView:GetScrollSize(showClassify)
    local scrool_height = 540                                               --高度    
    local scrollViewSize = cc.size(1700, scrool_height)        --滑动框范围       
--    if LuaUtils.isIphoneXDesignResolution() then
--        scrollViewSize.width = scrollViewSize.width - 50
--    end
--    if showClassify then  -- 减去列表栏的宽度
--        scrollViewSize.width = scrollViewSize.width - 110
--    end
    return scrollViewSize
end

-- 重置滑动区域大小
function HallGameListView:ReSizeScrollView( showClassify )
    if self.m_root_lv == nil then return end

    local prePosition = self.m_root_lv:getInnerContainerPosition()
    if prePosition.x > 0 then
        prePosition.x = 0  -- 如果切换的时候，向左拖动的位置要及时重置0点，否则左边有空隙
    end
    self.m_root_lv:setContentSize( self:GetScrollSize(showClassify) )
    self.m_root_lv:setInertiaScrollEnabled(true)
    self.m_root_lv:jumpToPercentHorizontal(0)
    self.m_root_lv:setGravity(0)
    self.m_root_lv:setBounceEnabled(true)--回弹
    self.m_root_lv:setDirection(ccui.ScrollViewDir.horizontal)--水平方向
    self.m_root_lv:setScrollBarEnabled(false)-- 隐藏滚动条
    self.m_root_lv:setItemsMargin(0.0)
    self.m_root_lv:setInnerContainerPosition( prePosition )
end

function HallGameListView:reload( bAnimation, showClassify )    
    self.m_pParent:refreshArrow(false, false)

    do --fix 初始化一些删掉的图片资源
        --local name = { "hall/plist/gui-gamekind.plist",  "hall/plist/gui-gamekind.png"}
        --display.loadSpriteFrames(name[1], name[2])

        local name = { "hall/plist/gui-hall.plist", "hall/plist/gui-hall.png", }
        --display.loadSpriteFrames(name[1], name[2])
        cc.SpriteFrameCache:getInstance():addSpriteFrames(name[1], name[2])
--        local manager = ccs.ArmatureDataManager:getInstance()
--        manager:addArmatureFileInfo("hall/effect/gamekind_effect/gamekind_effect.ExportJson")
--        manager:addArmatureFileInfo("hall/effect/shaoguangAnimation/shaoguangAnimation.ExportJson") 
    end     

    local extenWidth = 350
    --[[if not LuaUtils.isIphoneXDesignResolution() then 
        extenWidth = 450
    end]]--

    local scroll_width = math.ceil((#self.room_list_data) / 2 ) * self.m_cellWidth + extenWidth --填充内容范围宽度
    local _Node = ccui.Layout:create()  
    _Node:setContentSize(cc.size(scroll_width, 540))
    _Node:setAnchorPoint(0, 0)
    _Node:setPosition(cc.p(0, 0))    
    self.m_root_lv:pushBackCustomItem(_Node)

    local btnSize = cc.size(310, 250) -- 按纽大小 


    --local maxWidth = scroll_width
    -- 各游戏入口按纽
    local listTotal = #self.room_list_data
    for index = 1, #self.room_list_data do   
        local nKindId = self.room_list_data[index]
        local format = self:getGameKindPath(nKindId) --常规版/和谐版
        local fileName = string.format(format, nKindId)
        print( fileName )
        if cc.FileUtils:getInstance():isFileExist(fileName) then
            local _btnNode =  self.m_pParent.m_pParentHallSceneLayer.m_pNodeRootUI:getChildByName("game_btn"):clone()
            local image = _btnNode:getChildByName("game")
            image:loadTexture(fileName, ccui.TextureResType.localType)
         --   _btnNode:loadTextureNormal(fileName, ccui.TextureResType.localType)
         --   _btnNode:setZoomScale(0)
            self.gameBtn[nKindId] = _btnNode
            self.gameBtn[nKindId].kindId = nKindId+10
            _btnNode:addTouchEventListener(handler(self,self.enterRoomEventCallBack))

            _btnNode:setAnchorPoint(0.5, 0.5)
            local pos = self:GetItemPosByIndex( listTotal, index )
            _btnNode:setPosition( pos )
            _btnNode:setTag( nKindId )
            _btnNode:addTo(_Node)
            _btnNode:setCascadeOpacityEnabled(true)
            _btnNode:setLocalZOrder(listTotal - index + 1)
           self.m_lv_items[index] = _btnNode 
           self:initBtnView(_btnNode, index, btnSize, cc.p(0,0), fileName, bAnimation)
        --    self:showDianJi(nKindId)
        end
    end

    local scrollViewSize = self:GetScrollSize(showClassify)
    self.m_listMaxWidth = scroll_width
    self.m_root_lv:addScrollViewEventListener(handler(self, self.onScrollEvent))
    self.m_root_lv:setContentSize(scrollViewSize)
    self.m_root_lv:setInertiaScrollEnabled(true)
    self.m_root_lv:jumpToPercentHorizontal(0)
    self.m_root_lv:setGravity(0)
    self.m_root_lv:setBounceEnabled(true)--回弹
    self.m_root_lv:setDirection(ccui.ScrollViewDir.horizontal)--水平方向
    --self.m_root_lv:setScrollBarEnabled(false)-- 隐藏滚动条
    self.m_root_lv:setItemsMargin(0.0)
    
    if self.m_root_lv:getContentSize().width >= self.m_listMaxWidth - 50 then
        self.m_pParent:refreshArrow(false, false)
    else
        self.m_pParent:refreshArrow(false, true)
    end
end

function HallGameListView:getGameKindPath(kindID) --游戏入口图
    return "hall/image/gamekind/gamekind-%d.png"
end

function HallGameListView:initBtnView(_btnNode, index, size, pos, fileName, bAnimation )
    --动画
    local nKindId = self.room_list_data[index]    
    --local posIndex = math.floor((index-1)/2)
    --posIndex = 0.3 + posIndex * 0.2

    local actionTime = 0.4

--    --按钮图片
--    local norsp = _btnNode:getRendererNormal()
--    if norsp ~= nil and bAnimation then
--        norsp:setOpacity(0)
--        norsp:runAction( cc.FadeIn:create(actionTime) )
--    end

    --下载动画(图片背景+图标+下载进度文字）
    --[[local pDownloadPos = cc.p(size.width / 2, 90)
    local pDownloadRes = DownLoadResView.getGameDownRes(nKindId, fileName, pDownloadPos)
    if pDownloadRes then
        pDownloadRes:setName("DownLoadResView")
        pDownloadRes:setTag(101)
        pDownloadRes:addTo(_btnNode)
        pDownloadRes:setOpacity(0)
        pDownloadRes:runAction( cc.FadeIn:create(actionTime) )
        self.m_pDownloadItem[index] = pDownloadRes
    end]]--

    --中秋活动动画
--    local isCloseCollect = GameListManager.getInstance():getGameSwitch(G_CONSTANTS.GAMESWITCH.CLOSE_ACTIVITY_DEFAULT_10)
--    local bJoind = CGameClassifyDataMgr.getInstance():getJoinActivity(nKindId)
--    local joinCollect = false
--    if not isCloseCollect and bJoind then 
--        joinCollect = true
--        local pSpine = "hall/effect/325_zhongqiujiaobiao/325_zhongqiujiaobiao"
--        local pAni = sp.SkeletonAnimation:createWithBinaryFile(pSpine .. ".skel", pSpine .. ".atlas", 1)
--        pAni:move(290,200)
--        pAni:addTo(_btnNode)
--        pAni:setAnimation(0, "animation", true)
--    end

--    --无限活力活动动画
--    local isCloseFire = GameListManager.getInstance():getGameSwitch(G_CONSTANTS.GAMESWITCH.CLOSE_ACTIVITY_DEFAULT_9)
--    local bJoindFire = CGameClassifyDataMgr.getInstance():getJoinFire(nKindId)
--    if not isCloseFire and bJoindFire and not joinCollect then
--        local pSpine = "hall/effect/325_activeFire/325_datingjiaobiaodonghua"
--        local pAni = self:getActivityAni(pSpine,"animation")
--        pAni:move(280,190)
--        pAni:addTo(_btnNode)
--    end 

    --游戏logo
    local pLogo = self:getGameLogo(nKindId)
    if pLogo then
        pLogo:setTag(102)
        pLogo:addTo(_btnNode)
        pLogo:setOpacity(0)
        pLogo:runAction( cc.FadeIn:create(actionTime) )
    end

    --按钮扫光
--    local saoguang = ccs.Armature:create("gamekind_effect")
--    local animation = string.format("Animation%d", math.random(1,3))
--    saoguang:setPosition(cc.p(129, 121))
--    saoguang:setTag(103)
--    saoguang:setVisible(false)
--    saoguang:addTo(_btnNode)

--    --动作完
--    _btnNode:setTouchEnabled(false)
--    local action1 = cc.Sequence:create(
--        cc.DelayTime:create(actionTime), 
--        cc.CallFunc:create(function()
--            --按钮可点
--            _btnNode:setTouchEnabled(true)
--        end),
--        cc.DelayTime:create(math.random(0, 1)),
--        cc.CallFunc:create(function()
--            --显示扫光
--            saoguang:setVisible(true)
--            saoguang:getAnimation():play(animation)
--        end))
--    _btnNode:runAction(action1)
end

function HallGameListView:getGameLogo(nKindId)
    
    local info = GameListManager.getInstance():getGameListInfo(nKindId)
    if info and info.dwStatus > 0 then

        --右上角节点
        local pNode = cc.Node:create()
        pNode:setAnchorPoint(1.0, 1.0)
        pNode:setPosition(60, 175)
        pNode:setCascadeOpacityEnabled(true)

        --右上角图标
        local pLogo = nil
        if info.dwStatus == 1 then
            pLogo = cc.Sprite:createWithSpriteFrameName("hall/plist/hall/gui-logo-new.png")
        elseif info.dwStatus == 2 then
            pLogo = cc.Sprite:createWithSpriteFrameName("hall/plist/hall/gui-logo-hot.png")
        else
            pLogo = cc.Sprite:createWithSpriteFrameName("hall/plist/hall/gui-logo-new.png")
        end
        pLogo:setCascadeOpacityEnabled(true)
        pLogo:addTo(pNode)

        --右上角图标扫光
--        local manager = ccs.ArmatureDataManager:getInstance()
--        manager:addArmatureFileInfo("hall/effect/shaoguangAnimation/shaoguangAnimation.ExportJson")
--        local pArmature = ccs.Armature:create("shaoguangAnimation")
--        pArmature:getAnimation():play("Animation1", -1, 1)
--        pArmature:setAnchorPoint(0.5, 0.5)
--        pArmature:setPosition(0, 0)
--        pArmature:setScaleX(-1)
--        pArmature:setCascadeOpacityEnabled(true)
--        pArmature:addTo(pNode)

        return pNode
    end
    return nil
end


function HallGameListView:getActivityAni(pSpine,animation)
    local m_pAni = SpineManager.getInstance():getSpineByBinary(pSpine)
    m_pAni:setAnimation(0, animation, true)
    return m_pAni
end

function HallGameListView:TouchListItem(_nGameKindID)
    g_GameMusicUtil:playSound("hall/sound/sound-hall-selected.mp3")

    --防止下载调用curl过多造成程序奔溃------------------------------------
    if self.m_fTouchTime[_nGameKindID] == nil then
        self.m_fTouchTime[_nGameKindID] = cc.exports.gettime()
    else
        local touchTime = cc.exports.gettime()
        if touchTime - self.m_fTouchTime[_nGameKindID] < 0.5 then
            FloatMessage.getInstance():pushMessage("您的点击太快了，请稍后重试")
            return
        else
            self.m_fTouchTime[_nGameKindID] = cc.exports.gettime()
        end
    end
    -------------------------------------------------------------------

    -- 统一处理:进入游戏房间
    local _event = {
        name = Hall_Events.GAMELIST_CLICKED_GAME,
        packet = {nKindId = _nGameKindID},
    }
    SLFacade:dispatchCustomEvent(Hall_Events.GAMELIST_CLICKED_GAME,_event)
end

--更新按钮
function HallGameListView:updateDownloadState(_event)

    -----------------------------------
    local _userdata = unpack(_event._userdata)
    if not _userdata then
        return
    end
    local eventID = _userdata.name
    local msg = _userdata.packet
    -----------------------------------
    local nKindId = msg.nKindId
    -----------------------------------
    for index, kindID in pairs(self.room_list_data) do
        if nKindId == kindID then
            if self.m_pDownloadItem[index] == nil then
                return
            end
            -- 需要下载资源
            if DownloadResMgr.getInstance():checkIsDownloadResoure(nKindId) then 
                local per = DownloadResMgr.getInstance():getDownLoadProgressPer(nKindId)
                if per and per > 0 then
                    self.m_pDownloadItem[index]:updatePercent(per)
                else
                    self.m_pDownloadItem[index]:setNeedDownLoadState()
                end
            else
                self.m_pDownloadItem[index]:setDownLoadDoneState()
            end
        end
    end
end

-- 更新下载进度
function HallGameListView:updateDownLoadPercent(_event)

    -----------------------------------
    local _userdata = unpack(_event._userdata)
    if not _userdata then
        return
    end
    local eventID = _userdata.name
    local msg = _userdata.packet
    -----------------------------------
    local nKindId = msg.nKindId;
    local nPercent = msg.percent;
    -----------------------------------
    for index, kindId in pairs(self.room_list_data) do
        if nKindId == kindId then
            if self.m_pDownloadItem[index] == nil then
                return
            end
            self.m_pDownloadItem[index]:updateDownload(nKindId, nPercent)
        end
    end
end

-- 下载失败
function HallGameListView:updateDownLoadError(_event)

    -----------------------------------
    local _userdata = unpack(_event._userdata)
    if not _userdata then
        return
    end
    local eventID = _userdata.name
    local msg = _userdata.packet
    -----------------------------------
    local nKindId = msg.nKindId;
    -----------------------------------
    for index, kindId in pairs(self.room_list_data) do
        if nKindId == kindId then
            if self.m_pDownloadItem[index] == nil then
                return
            end
            self.m_pDownloadItem[index]:setNeedDownLoadState()
        end
    end
end

function HallGameListView:enterRoomEventCallBack(pSender,ntype)
    if eventType==ccui.TouchEventType.canceled then
        pSender:setScale(1.0)
    end
     if ntype == ccui.TouchEventType.began then
        pSender:setScale(1.0)
        self._xTouch = pSender:getParent():convertToWorldSpace(cc.p(pSender:getPositionX(), pSender:getPositionY())).x
     end
     if ntype == ccui.TouchEventType.moved then

     end
	 if ntype ==ccui.TouchEventType.ended  then
        self._xTouchend = pSender:getParent():convertToWorldSpace(cc.p(pSender:getPositionX(), pSender:getPositionY())).x
        if (math.abs(self._xTouch - self._xTouchend) > 25) then
            return
        end
        self._currentSelectedGame = pSender
		g_GameMusicUtil:playSound("hall/sound/sound-hall-selected.mp3")
        --游戏是否开放
        local flag = cc.UserDefault:getInstance():getStringForKey("First_Hall")   
        --if flag ~="" then
        if not isRoomData(self._currentSelectedGame.kindId) then
            --                TOAST("游戏未开放" .. self._currentSelectedGame.kindId)
            if self._currentSelectedGame.kindId == 20 then
                TOAST("游戏未开放")
            else
                -- 视讯预加载
                if RoomData:isGameExist(RoomData:getGameByBtnId(self._currentSelectedGame.kindId)) then
                    if RoomData:isGameUp2Date(RoomData:getGameByBtnId(self._currentSelectedGame.kindId)) then -- 游戏已经是最新
                        self.__downloadStatus = 2
                    else
                        self.__downloadStatus = 0
                    end
                else
                    self.__downloadStatus = 0
                end
                -- 判断状态
                if self.__downloadStatus == 0 then
                    self:doProloadBGDownLoad()
                elseif self.__downloadStatus == 1 then
                    print("下载中")
                elseif self.__downloadStatus == 2 then
                    print("已下载")
                    self:enterShiXun()
                end
            end

        else
            if RoomData:isGameExist(RoomData:getGameByBtnId(self._currentSelectedGame.kindId)) then
                if RoomData:isGameUp2Date(RoomData:getGameByBtnId(self._currentSelectedGame.kindId)) then -- 游戏已经是最新
                    self.__downloadStatus = 2
                else
                    self.__downloadStatus = 0
                end
            else
                self.__downloadStatus = 0
            end

            if self.__downloadStatus == 0 then
                self:doDownLoad()
            elseif self.__downloadStatus == 1 then
                print("下载中")
            elseif self.__downloadStatus == 2 then
                print("已下载")
                self:enterGame()
            end
        end
    end
end

function HallGameListView:getNetworkStatus()

    if device.platform == "windows"  then
        return  1

    else
        return   cc.Network:getInternetConnectionStatus()
    end
end

function HallGameListView:onUpdateOrDownLoadGame(msmName, _infor)
    if  self.updateAndEntryRoom  or self.__downloadStatus == 1 then
        TOAST("正在下载，请稍后！")
        return
    end

    self.quickNodeId = _infor.nodeId
    
   
    local _rootID =  ToolKit:getRootFuncid(self.quickNodeId)

    if _rootID ~= RoomData:getGameByBtnId(self._currentSelectedGame.kindId) then
        return
    end

    self.option =  _infor.option

    local path = RoomData:getEnterScenePath(RoomData:getGameByBtnId(self._currentSelectedGame.kindId))
    if not path or  string.len(path) <= 0 then
        -- 没有配置游戏.
        TOAST(STR(64,1))
        return
    end


    if RoomData:isGameExist(RoomData:getGameByBtnId(self._currentSelectedGame.kindId)) then
        --游戏已经下载
        if RoomData:isGameUp2Date(RoomData:getGameByBtnId(self._currentSelectedGame.kindId)) then
            -- 游戏已经是最新,直接进入房间
            self:enterGame()

        else
            --需要更新
            local _network = self:getNetworkStatus()

            if _network == 0  then
                --无法访问互联网
                TOAST("您的手机现在没有连上网络，请连上网络后重试！")
                return

            else
                --通过wifi或3g访问互联网
                self.updateControlller = UpdateController.new()
                if not self.updateControlller:updateGame(RoomData:getPackageIdByPortalId(RoomData:getGameByBtnId(self._currentSelectedGame.kindId)), true) then
                    --不能更新  直接进入房间
                    self:enterGame()
                else
                    --开始更新
                    self.updateAndEntryRoom = true
                    self:onStartUpdate(self._currentSelectedGame.kindId - 10)
                end
            end
        end
    else
        --游戏没有下载,开始下载
        local _network = self:getNetworkStatus()

        if _network == 0  then
            --无法访问互联网
            TOAST("您的手机现在没有连上网络，请连上网络后重试！")
            return

        elseif _network == 1  then
            --通过wifi访问互联网

            self.updateControlller = UpdateController.new()
            if not self.updateControlller:updateGame(RoomData:getPackageIdByPortalId(RoomData:getGameByBtnId(self._currentSelectedGame.kindId)), true) then
                --不能更新  直接进入房间
                self:enterGame()
            else
                -- 开始下载 ，显示下载界面
                self.updateAndEntryRoom = true
                self:onStartUpdate(self._currentSelectedGame.kindId - 10)
            end

        else
            --通过3g访问互联网,下载提示
            local _gameName = ""
            local data = fromFunction(RoomData:getGameByBtnId(self._currentSelectedGame.kindId))
            if data then
                _gameName = data.name
            else
                TOAST("获取游戏信息失败！")
                return
            end


            local params = {
                title = "提示",
                message =  _gameName .. "尚未安装，是否下载安装游戏？"  ,
                leftStr =  "取消",
                rightStr = "下载",
            }

            local dlg = require("app.hall.base.ui.MessageBox").new()

            local _leftCallback = function (  )
                return
            end

            local _rightcallback = function ()

                self.updateControlller = UpdateController.new()
                if not self.updateControlller:updateGame(RoomData:getPackageIdByPortalId(RoomData:getGameByBtnId(self._currentSelectedGame.kindId)), true) then
                    --不能更新  直接进入房间
                    self:enterGame()
                else
                    -- 开始下载 ，显示下载界面
                    self.updateAndEntryRoom = true
                    self:onStartUpdate(self._currentSelectedGame.kindId - 10)
                end
            end

            dlg:TowSubmitAlert(params, _leftCallback, _rightcallback)
            dlg:enableTouch( false )
            dlg:showDialog()
        end
    end
end

function HallGameListView:enterGame() 
    if not isOpenGame(self._currentSelectedGame.kindId) then
        TOAST("即将开放，敬请期待")
        return
    end
    
    local isQuickGame = isQuickGame(self._currentSelectedGame.kindId)
    local roomList = getRoomList(self._currentSelectedGame.kindId)
    
    --是否快速进入游戏、
    if isQuickGame then
        if roomList[1].controllerPath ==nil or  roomList[1].controllerPath =="" then 
            TOAST("敬请期待")
            return
        end
        ToolKit:addLoadingDialog(5,"正在进入游戏，请稍等......")
		if g_GameController then
			g_GameController:releaseInstance()
		end
        g_GameController = require(roomList[1].controllerPath):getInstance()
		g_GameController:bindGameTypeId(roomList[1].gameId)
        ConnectManager:send2SceneServer(roomList[1].gameId, "CS_C2M_Enter_Req", {roomList[1].gameId, "" })
    else
        if roomList[1].roomLayerPath==nil or roomList[1].roomLayerPath=="" then
            TOAST("敬请期待")
            return
        end
        local gameRoom = require(roomList[1].roomLayerPath).new(roomList)
        self.m_pParent:addChild(gameRoom)
    end
end

function HallGameListView:enterShiXun()
    local nKindId = self._currentSelectedGame.kindId
    ToolKit:addLoadingDialog(5,"正在进入游戏，请稍等......")
	
	if not ConnectManager:isConnectSvr(Protocol.LobbyServer) then
        return
    end
	
    --
    g_BGController:setCurrentGameId(nKindId)
    local scene = UIAdapter:pushScene("src.app.game.shixun.shixunScene", DIRECTION.HORIZONTAL )  
    scene:getMainLayer():loadUrl(nKindId, self)
end

function HallGameListView:doDownLoad()
    -- 每次只读取UpdateInfo即可判断是否需要下载
    self.updateControlller = UpdateController.new()
    if not self.updateControlller:updateGame(RoomData:getPackageIdByPortalId(RoomData:getGameByBtnId(self._currentSelectedGame.kindId)), true) then
        self:enterGame()
    else
        self:onStartUpdate(self._currentSelectedGame.kindId - 10)
    end
end

-- 临时函数，判断预加载视讯
function HallGameListView:doProloadBGDownLoad()
    -- 每次只读取UpdateInfo即可判断是否需要下载
    self.updateControlller = UpdateController.new()
    if not self.updateControlller:updateGame(RoomData:getPackageIdByPortalId(RoomData:getGameByBtnId(self._currentSelectedGame.kindId)), true) then
        self:enterShiXun()
    else
        self:onStartUpdate(self._currentSelectedGame.kindId - 10)
    end
end

function HallGameListView:onStartUpdate(num)
    self:createProgress(num)
    self.progress[num]:setPercentage(0)
    self.__downloadStatus = 1
    if g_entry then
        g_entry = false
    end
end

function HallGameListView:createProgress(num)
    if self.progress[num] == nil then
        -- 创建精灵时，如果图片资源在大图里，可用createWithSpriteFrameName创建
        cc.SpriteFrameCache:getInstance():addSpriteFrames("hall/plist/gui-hall.plist", "hall/plist/gui-hall.png")

        local sliderName = "hall/plist/hall/image-download-res-pro.png"
        local sliderBgName = "hall/plist/hall/image-download-res-bg.png"

    --图片进度
        local fileName = string.format("hall/image/gamekind/gamekind-%d.png",num)
        local progressImg = cc.Sprite:create(fileName)
        self.m_pImageTimer[num] = cc.ProgressTimer:create(progressImg)
        self.m_pImageTimer[num]:setAnchorPoint(cc.p(0.5, 0.5))
        self.m_pImageTimer[num]:setPosition(cc.p(self.gameBtn[num]:getContentSize().width/2, self.gameBtn[num]:getContentSize().height/2))
        self.m_pImageTimer[num]:setType(cc.PROGRESS_TIMER_TYPE_BAR) -- 设置为条
        self.m_pImageTimer[num]:setMidpoint(cc.p(0, 0))             -- 设置起点为条形左下方
        self.m_pImageTimer[num]:setBarChangeRate(cc.p(0, 1))        -- 设置为竖直方向
        self.m_pImageTimer[num]:setOpacity(80)
        self.m_pImageTimer[num]:setColor(cc.c3b(0, 0, 0))
        self.m_pImageTimer[num]:setPercentage(100)
        self.m_pImageTimer[num]:setCascadeOpacityEnabled(true)
        self.gameBtn[num]:addChild(self.m_pImageTimer[num])

        local sprite = cc.Sprite:createWithSpriteFrameName(sliderName)
        self.progressBg[num] = cc.Sprite:createWithSpriteFrameName(sliderBgName)
        self.progressBg[num]:setAnchorPoint(cc.p(0.5, 0.5))
        self.progressBg[num]:setPosition(cc.p(self.gameBtn[num]:getContentSize().width/2, self.gameBtn[num]:getContentSize().height/2))
        self.gameBtn[num]:addChild(self.progressBg[num])
        self.progress[num] = cc.ProgressTimer:create(sprite)
        self.progress[num]:setType(cc.PROGRESS_TIMER_TYPE_RADIAL)
        self.progress[num]:setBarChangeRate(cc.p(1,0))
        self.progress[num]:setMidpoint(cc.p(0.5,0.5)) 
        self.progress[num]:setAnchorPoint(cc.p(0.5,0.5))
        self.progress[num]:setPosition(cc.p(self.gameBtn[num]:getContentSize().width/2, self.gameBtn[num]:getContentSize().height/2))
        self.proLabel[num] = display.newTTFLabel({
            text = "0%",
            size = 22,
            align = cc.TEXT_ALIGNMENT_CENTER,
            color = cc.c3b(255, 255, 255),
        }):addTo(self.progress[num]):align(display.CENTER, sprite:getContentSize().width*0.5, 30)
        self.gameBtn[num]:addChild(self.progress[num])

        --[[if num==5 then
            if self.gameBtn[15] then
                --图片进度
                if self.m_pImageTimer[15]==nil then
                    local fileName = string.format("hall/image/gamekind/gamekind-%d.png",15)
                    local progressImg = cc.Sprite:create(fileName)
                    self.m_pImageTimer[15] = cc.ProgressTimer:create(progressImg)
                    self.m_pImageTimer[15]:setAnchorPoint(cc.p(0.5, 0.5))
                    self.m_pImageTimer[15]:setPosition(cc.p(self.gameBtn[15]:getContentSize().width/2, self.gameBtn[15]:getContentSize().height/2))
                    self.m_pImageTimer[15]:setType(cc.PROGRESS_TIMER_TYPE_BAR) -- 设置为条
                    self.m_pImageTimer[15]:setMidpoint(cc.p(0, 0))             -- 设置起点为条形左下方
                    self.m_pImageTimer[15]:setBarChangeRate(cc.p(0, 1))        -- 设置为竖直方向
                    self.m_pImageTimer[15]:setOpacity(80)
                    self.m_pImageTimer[15]:setColor(cc.c3b(0, 0, 0))
                    self.m_pImageTimer[15]:setPercentage(100)
                    self.m_pImageTimer[15]:setCascadeOpacityEnabled(true)
                    self.gameBtn[15]:addChild(self.m_pImageTimer[15])
                end
                self.m_pImageTimer[15]:setVisible(true)

                if self.progress[15]==nil then
                    self.progressBg[15] = cc.Sprite:createWithSpriteFrameName(sliderBgName)
                    self.progressBg[15]:setAnchorPoint(cc.p(0.5, 0.5))
                    self.progressBg[15]:setPosition(cc.p(self.gameBtn[15]:getContentSize().width/2, self.gameBtn[15]:getContentSize().height/2))
                    self.gameBtn[15]:addChild(self.progressBg[15])
                    self.progress[15] = cc.ProgressTimer:create(sprite)
                    self.progress[15]:setType(cc.PROGRESS_TIMER_TYPE_RADIAL)
                    self.progress[15]:setBarChangeRate(cc.p(1,0))
                    self.progress[15]:setMidpoint(cc.p(0.5,0.5)) 
                    self.progress[15]:setAnchorPoint(cc.p(0.5,0.5))
                    self.progress[15]:setPosition(cc.p(self.gameBtn[15]:getContentSize().width/2, self.gameBtn[15]:getContentSize().height/2))
                    self.proLabel[15] = display.newTTFLabel({
                        text = "0%",
                        size = 22,
                        align = cc.TEXT_ALIGNMENT_CENTER,
                        color = cc.c3b(255, 255, 255),
                    }):addTo(self.progress[15]):align(display.CENTER, sprite:getContentSize().width*0.5, 30)
                    self.gameBtn[15]:addChild(self.progress[15])
                end
                self.progressBg[15]:setVisible(true)
                self.progress[15]:setVisible(true)
            end
        elseif num==15 then
            if self.gameBtn[5] then
                --图片进度
                if self.m_pImageTimer[5]==nil then
                    local fileName = string.format("hall/image/gamekind/gamekind-%d.png",5)
                    local progressImg = cc.Sprite:create(fileName)
                    self.m_pImageTimer[5] = cc.ProgressTimer:create(progressImg)
                    self.m_pImageTimer[5]:setAnchorPoint(cc.p(0.5, 0.5))
                    self.m_pImageTimer[5]:setPosition(cc.p(self.gameBtn[5]:getContentSize().width/2, self.gameBtn[5]:getContentSize().height/2))
                    self.m_pImageTimer[5]:setType(cc.PROGRESS_TIMER_TYPE_BAR) -- 设置为条
                    self.m_pImageTimer[5]:setMidpoint(cc.p(0, 0))             -- 设置起点为条形左下方
                    self.m_pImageTimer[5]:setBarChangeRate(cc.p(0, 1))        -- 设置为竖直方向
                    self.m_pImageTimer[5]:setOpacity(80)
                    self.m_pImageTimer[5]:setColor(cc.c3b(0, 0, 0))
                    self.m_pImageTimer[5]:setPercentage(100)
                    self.m_pImageTimer[5]:setCascadeOpacityEnabled(true)
                    self.gameBtn[5]:addChild(self.m_pImageNode[5])
                end
                self.m_pImageTimer[5]:setVisible(true)

                if self.progress[5]==nil then
                    self.progressBg[5] = cc.Sprite:createWithSpriteFrameName(sliderBgName)
                    self.progressBg[5]:setAnchorPoint(cc.p(0.5, 0.5))
                    self.progressBg[5]:setPosition(cc.p(self.gameBtn[5]:getContentSize().width/2, self.gameBtn[5]:getContentSize().height/2))
                    self.gameBtn[5]:addChild(self.progressBg[5])
                    self.progress[5] = cc.ProgressTimer:create(sprite)
                    self.progress[5]:setType(cc.PROGRESS_TIMER_TYPE_RADIAL)
                    self.progress[5]:setBarChangeRate(cc.p(1,0))
                    self.progress[5]:setMidpoint(cc.p(0.5,0.5)) 
                    self.progress[5]:setAnchorPoint(cc.p(0.5,0.5))
                    self.progress[5]:setPosition(cc.p(self.gameBtn[5]:getContentSize().width/2, self.gameBtn[5]:getContentSize().height/2))
                    self.proLabel[5] = display.newTTFLabel({
                        text = "0%",
                        size = 22,
                        align = cc.TEXT_ALIGNMENT_CENTER,
                        color = cc.c3b(255, 255, 255),
                    }):addTo(self.progress[5]):align(display.CENTER, sprite:getContentSize().width*0.5, 30)
                    self.gameBtn[5]:addChild(self.progress[5])
                end
                self.progressBg[5]:setVisible(true)
                self.progress[5]:setVisible(true)
            end
        end]]--
    end

    self.progressBg[num]:setVisible(true)
    self.progress[num]:setVisible(true)

end


function HallGameListView:onGameUpdateProgress( msgName, gameKindType, progress )
    if self._currentSelectedGame == nil then
        return;
    end
    local num = RoomData:getBtnIdByGameId(gameKindType) - 10
    if num==5 and self._currentSelectedGame.kindId==25 then
        num=15
    end
    if self.gameBtn[num]==nil then
        return
    end

    local funcShowProgress = function(num)
    --if gameKindType == RoomData:getPackageIdByPortalId(RoomData:getGameByBtnId(self._currentSelectedGame.kindId)) then
        -- and self.progress then
        if self.progress[num] == nil then
            self:createProgress(num)
            self.__downloadStatus = 1
            --[[if num==5 then
                if self.gameBtn[15] then
                    self:createProgress(15)
                end
            elseif num==15 then
                if self.gameBtn[5] then
                    self:createProgress(5)
                end
            end]]--
        end
        print("download game: ", progress)
        if progress == -1 then -- 游戏更新失败
            TOAST(STR(63,1))

            self.progress[num]:setVisible(false)
            self.progressBg[num]:setVisible(false)
            self.m_pImageTimer[num]:setVisible(false)
            --[[if num==5 then
                if self.gameBtn[15] then
                    self.progress[15]:setVisible(false)
                    self.progressBg[15]:setVisible(false)
                    self.m_pImageTimer[15]:setVisible(false)
                end
            elseif num==15 then
                if self.gameBtn[5] then
                    self.progress[5]:setVisible(false)
                    self.progressBg[5]:setVisible(false)
                    self.m_pImageTimer[5]:setVisible(false)
                end
            end]]--
            -- self.btnIcon:setColor(cc.c3b(255, 255, 255))
            self.__downloadStatus = 0
            self.proLabel:setString("0%")

            self.updateAndEntryRoom = false

        elseif progress <= 100 and progress > 0 then
            self.progress[num]:setPercentage(progress)
            self.proLabel[num]:setString(string.format("%2d",progress) .. "%")
            self.m_pImageTimer[num]:setPercentage(100-progress)
            --[[if num==5 then
                if self.gameBtn[15] then
                    self.progress[15]:setPercentage(progress)
                    self.proLabel[15]:setString(string.format("%2d",progress) .. "%")
                    self.m_pImageTimer[15]:setPercentage(100-progress)
                end
            elseif num==15 then
                if self.gameBtn[5] then
                    self.progress[5]:setPercentage(progress)
                    self.proLabel[5]:setString(string.format("%2d",progress) .. "%")
                    self.m_pImageTimer[5]:setPercentage(100-progress)
                end
            end]]--
        elseif progress == 1000 then -- 下载完成

            self.progress[num]:setVisible(false)
            self.progressBg[num]:setVisible(false)
            self.m_pImageTimer[num]:setVisible(false)
            --[[if num==5 then
                if self.gameBtn[15] then
                    self.progress[15]:setVisible(false)
                    self.progressBg[15]:setVisible(false)
                    self.m_pImageTimer[15]:setVisible(false)
                end
            elseif num==15 then
                if self.gameBtn[5] then
                    self.progress[5]:setVisible(false)
                    self.progressBg[5]:setVisible(false)
                    self.m_pImageTimer[5]:setVisible(false)
                end
            end]]--
            --self.btnIcon:setColor(cc.c3b(255, 255, 255))
            self.__downloadStatus = 0

            self:showDianJi(num)

            if self.updateAndEntryRoom  then

                self.updateAndEntryRoom = false
                if cc.Director:getInstance():getRunningScene().__cname == "TotalScene" and #GlobalViewBaseStack==1 then
                    --self:enterGame()
                end

            else
                if cc.Director:getInstance():getRunningScene().__cname == "TotalScene" then
                    if not  g_entry then
                        g_entry = true
                        --self:enterGame()
                    end

                end
            end
        end
    --end
    end
    -- 显示进度条
    if num >= 200 and num <= 205 then
       funcShowProgress(200)
       funcShowProgress(201)
       funcShowProgress(202)
       funcShowProgress(203)
       funcShowProgress(204)
       funcShowProgress(205)
    else
       funcShowProgress(num)
    end
end

function HallGameListView:getDownloadStatus(msgName, _gameKindType)
    if _gameKindType == self.gameKindType then
        sendMsg(MSG_GET_DOWANLOADSTATE, {gameDownState = self.__downloadStatus, gameKindType = RoomData:getGameByBtnId(self._currentSelectedGame.kindId)})
    end
end

function HallGameListView:showDianJi(num)
    local pDianJi = self.gameBtn[num].m_pDianJi;
    if nil == pDianJi and self:isVisible() then
        local btnSize = self.gameBtn[num]:getContentSize()

        pDianJi = CacheManager:addSpineTo(self.gameBtn[num], "res/hall/effect/dianyidian/dianyidian",1,".json", 0.7)
        self.gameBtn[num].m_pDianJi = pDianJi
        pDianJi:setTimeScale(1.5)
        pDianJi:setAnimation(0, "animation", true);
        pDianJi:setPosition(cc.p(80, 40))
        pDianJi:runAction(cca.seq({cca.delay(5), cca.fadeOut(0.1), cca.callFunc(function()
            self.gameBtn[num]:removeChild(self.gameBtn[num].m_pDianJi)
            self.gameBtn[num].m_pDianJi = nil
        end)}))

        local pLabel = display.newTTFLabel({text = "游戏下载完成咯！\n继续点击进入游戏!",size = 18, font = UIAdapter.TTF_FZCYJ})
        pLabel:setSkewX(8)
        pDianJi:addChild(pLabel)
        pLabel:setPosition(cc.p(60, -50))
        pLabel:enableOutline(cc.c4b(0x2f, 0x76, 0xa0, 255), 2)
    end
end

return HallGameListView
