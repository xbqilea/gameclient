--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

--region HallViewClassify.lua
--Date
--
--endregion
local ActivityRecommend = import("..bean.ActivityRecommend")        --推荐活动
local HallGameListView  = import(".HallGameListView")     --游戏列表

local MatchController = require("src.app.hall.MatchGameList.control.MatchController");
local MatchEvent = require("src/app/hall/MatchGameList/control/MatchEvent");
local DlgAlert = require("app.hall.base.ui.MessageBox")
local platformUtils = require("app.hall.base.util.platform")  

--大厅
local HNlayer = import("..HNLayer")
local HallViewClassify = class("HallViewClassify", HNlayer)

local UPDATE_ONLINE = 30.0 * 5 --5分钟更新在线人数

G_CONSTANTS = {
    EGAME_TYPE_CODE = 
    {
        EGAME_TYPE_FLYWALK          = 201,       --飞禽走兽
        EGAME_TYPE_TIGER            = 202,       --老虎机
        EGAME_TYPE_DICE             = 203,       --骰宝
        EGAME_TYPE_HORSE            = 204,       --赛马
        EGAME_TYPE_NIUNIU           = 205,       --百人牛牛
        EGAME_TYPE_WATERMARGIN      = 206,       --水浒传
        EGAME_TYPE_CAR              = 207,       --车行
        EGAME_TYPE_BACCARAT         = 208,       --百家乐
        EGAME_TYPE_FRUIT            = 209,       --水果机
        EGAME_TYPE_TICKETS          = 210,       --刮刮乐
        EGAME_TYPE_3DHORSE          = 211,       --皇家赛马
        EGAME_TYPE_DIAMOND_DRAG     = 212,       --连环夺宝
        EGAME_TYPE_REDVSBLACK       = 213,       --红黑大战
        EGAME_TYPE_MATCHGAMBLING    = 214,       --全名世界杯
        EGAME_TYPE_LONGHUDAZHAN     = 215,       --龙虎斗
        EGAME_TYPE_BOOMGAMBLING     = 217,       --红包扫雷
        EGAME_TYPE_FISHING          = 352,       --大闹天宫
        EGAME_TYPE_FROG_FISH        = 361,       --金蟾捕鱼
        EGAME_TYPE_LK_FISH          = 362,       --李逵捕鱼
        EGAME_TYPE_LONG_FISH        = 363,       --深海捕龙
        EGAME_TYPE_LANDLORD         = 401,       --斗地主
        EGAME_TYPE_TWONIUNIU        = 402,       --二人牛牛
        EGAME_TYPE_TWOSHOWHAND      = 403,       --二人梭哈
        EGAME_TYPE_TONGBINIUNIU     = 404,       --通比牛牛
        EGAME_TYPE_ZHAJINHUA        = 407,       --扎金花
        EGAME_TYPE_XPNN             = 409,       --血拼牛牛
        EGAME_TYPE_HPMAHJONG        = 410,       --火拼麻将 
	    EGAME_TYPE_TEXAS            = 411,       --德州扑克
        EGAME_TYPE_PAODEKUAI        = 412,       --跑得快
        EGAME_TYPE_QZNN             = 413,       --抢庄牛牛
    },

    -----------大厅中游戏种类类型定义-----------
    --游戏种类
    GameClassifyType = {

        GAME_CLASSIFY_MAIN      = 0,        --推荐游戏
        GAME_CLASSIFY_ARCADE    = 1,        --多人游戏
        GAME_CLASSIFY_TIGER     = 2,        --单机游戏
        GAME_CLASSIFY_FISH      = 3,        --捕鱼游戏（真人）
        GAME_CLASSIFY_CARD      = 4,        --对战游戏（电竞）
        GAME_CLASSIFY_CASUAL    = 5,        --休闲

        GAME_CLASSIFY_OTHER_FISH = 6,       --第三方捕鱼
        GAME_CLASSIFY_OTHER_ELECTRONIC = 7, --第三方电子
        GAME_CLASSIFY_OTHER_SPORTS = 8, --第三方体育

        GAME_CLASSIFY_ALL       = 10,       --所有游戏
    },

    MAX_CHANNEL_STATUS_COUNT   = 200,
    MAX_CLIENT_ACTIVITY        = 10,
}


Hall_Events = {
    MSG_CLASSIFY_OPEN_VIEW_GAME_CLASSIFY_ARCADE = "MSG_HALL_EVENTS_MSG_CLASSIFY_OPEN_VIEW_GAME_CLASSIFY_ARCADE", -- 打开多人游戏
    -- MSG_CLASSIFY_OPEN_GAME_DANTIAO = "MSG_HALL_EVENTS_MSG_CLASSIFY_OPEN_GAME_DANTIAO" -- 打开单挑
}


function HallViewClassify:ctor()
    --self:enableNodeEvents()
    --self.m_bClassifyShow = false
    self.m_iSelectIndex = 1
    self.m_pParentHallSceneLayer = nil
    self.m_bCheck = false
    self:init()
    ToolKit:registDistructor(self, handler(self, self.onDestroy))
    -- addMsgCallBack(self, MatchEvent.MSG_CLOSE_MATCH_LIST_VIEW, handler(self, self.on_CLOSE_MATCH_LIST_VIEW));
    addMsgCallBack(self, Hall_Events.MSG_CLASSIFY_OPEN_VIEW_GAME_CLASSIFY_ARCADE, function()
        self.m_iSelectIndex = 1
        self:showGameList()
    end)
end

function HallViewClassify:onDestroy()
    removeMsgCallBack(self, Hall_Events.MSG_CLASSIFY_OPEN_VIEW_GAME_CLASSIFY_ARCADE)
end

function HallViewClassify:onEnter()
    self:init()

    --房间数据更新
    SLFacade:addCustomEventListener(Hall_Events.MSG_GET_ROOM_LIST, handler(self, self.onUpdateState), self.__cname)
    

    --定时更新房间数据
    if self.m_schudlerUpdate == nil then
        self.m_schudlerUpdate = scheduler.scheduleGlobal(function()
            if cc.exports.isConnectHall() and cc.exports.isSocketConnect() then
                CMsgHall:sendGetOnlineList()
            end
        end, UPDATE_ONLINE)
    end
end

function HallViewClassify:onExit()
    self:clear()

    --房间数据更新
    SLFacade:removeCustomEventListener(Hall_Events.MSG_GET_ROOM_LIST, self.__cname)

    --定时更新房间数据
    if self.m_schudlerUpdate == nil then
        scheduler.unscheduleGlobal(self.m_schudlerUpdate)
        self.m_schudlerUpdate = nil
    end

    --removeMsgCallBack(self, MatchEvent.MSG_CLOSE_MATCH_LIST_VIEW);
end

function HallViewClassify:init()
    self:initCSB()
    self:initBtnEvent()
    self:showActivity()
    self:createArrow()
    --self:showActivity()
    MatchController.releseInstance();
    -- MatchController.getInstance():setViewClassify(self);
    --addMsgCallBack(self, MSG_BACK_HALL_VIEW_CLASSIFY, handler(self, self.onMsgBackHallViewClassify));
end

function HallViewClassify:clear()

end

function HallViewClassify:initCSB()
    
    --self
    self:setPosition(0, 0)

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)
   
    --init csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/HallViewMain.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    --self.m_rootUI:setPosition(cc.p((display.width - 1334) / 2,(display.height - 750) / 2))

--    self.m_pAction = cc.CSLoader:createTimeline("hall/csb/HallViewMain.csb")
--    self.m_pathUI:runAction(self.m_pAction) 
     local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    
    self.m_pNodeMain = self.m_pathUI:getChildByName("HallViewClassify"):getChildByName("node_main")
    self.m_pNodeMain:setPositionX(diffX)
--    --适配全面屏,全面屏有刘海要左移
--    local diffX = 0
--    if display.size.width == 1334 then
--        diffX = 145
--    else
--        --为了刘海右移50个像素
--        diffX = (1624-display.size.width)/2 + 50
--    end
--    self.m_pNodeMain:setPositionX(diffX)

    self.m_pNodeClasify = self.m_pNodeMain:getChildByName("node_classify")
    --self.m_pImageMenu   = self.m_pNodeClasify:getChildByName("Image_Menu")
    --self.m_pBtnAll      = self.m_pImageMenu:getChildByName("BTN_classify_all")
    
    --多人（棋牌&捕鱼）
    self.m_pBtnMulti     = self.m_pNodeClasify:getChildByName("Node_classify_multi"):getChildByName("BTN_classify_multi")
    --彩票
    self.m_pBtnFight     = self.m_pNodeClasify:getChildByName("Node_classify_fight"):getChildByName("BTN_classify_fight")
    --真人（真人&电子）
    self.m_pBtnFish      = self.m_pNodeClasify:getChildByName("Node_classify_fish"):getChildByName("BTN_classify_fish")
    --电玩（体育&电竞）
    self.m_pBtnArcade    = self.m_pNodeClasify:getChildByName("Node_classify_arcade"):getChildByName("BTN_classify_arcade")
    --比赛
    self.m_pBtnCasual    = self.m_pNodeClasify:getChildByName("Node_classify_casual"):getChildByName("BTN_classify_casual")

    self.m_pAllBtns     = {self.m_pBtnMulti, self.m_pBtnFight, self.m_pBtnFish, self.m_pBtnArcade, self.m_pBtnCasual}

    local classify_images = {"hall/image/classify/classify_1.png", "", "", "hall/image/classify/classify_4.png" ,""}

    local funBtnSpine = function(spineName)
        local spineAnim = sp.SkeletonAnimation:createWithJsonFile(
        "hall/effect/hall_animation_gameClassify/" .. spineName .. ".json", 
        "hall/effect/hall_animation_gameClassify/" .. spineName .. ".atlas")
        spineAnim:setAnchorPoint(cc.p(0.5,0.5))
        spineAnim:setPosition(cc.p(0, 0))
        spineAnim:setAnimation( 0, "animation", true)
        spineAnim:setName("Armature_classify")
        spineAnim:setScale(1.0)
        return spineAnim
    end
    local tInfo = {
        -- dr = self.m_pBtnMulti,  
        cp = self.m_pBtnFight,  
        -- ZRSX = self.m_pBtnFish,  
        dzdw = self.m_pBtnArcade,  
        -- bsc = self.m_pBtnCasual,  
    }
    for k,v in pairs( tInfo ) do
        v:getParent():getChildByName("Armature_classify"):removeFromParent()
        v:getParent():addChild(funBtnSpine(k))
        v:getParent():getChildByName("Armature_classify"):setScale(1)
    end

    self.m_pBtnFish:getParent():getChildByName("Armature_classify"):removeFromParent()
    local spineAnim = sp.SkeletonAnimation:createWithJsonFile("hall/effect/hall_animation_gameClassify/ZRSX.json", "hall/effect/hall_animation_gameClassify/ZRSX.atlas")
    spineAnim:setAnchorPoint(cc.p(0.5,0.5))
    spineAnim:setPosition(cc.p(0, 0))
    spineAnim:setAnimation( 0, "animation", true)
    spineAnim:setName("Armature_classify")
    spineAnim:setScale(1.1)
    self.m_pBtnFish:getParent():addChild(spineAnim)

    self.m_pBtnMulti:getParent():getChildByName("Armature_classify"):setScale(0.98)
    self.m_pBtnMulti:getParent():setPositionY(self.m_pBtnMulti:getParent():getPositionY()-10)

    self.m_pNodeGame = self.m_pNodeMain:getChildByName("node_game")
    self.m_pNodeRecommend = self.m_pNodeClasify:getChildByName("node_activity") 
    self.m_pNodeGameList = self.m_pNodeGame:getChildByName("node_gamelist") 
    self.m_pNodeGameList:setVisible(false)

    self._scale = {}
    for i = 1, 5 do
        local Armature_classify = self.m_pAllBtns[i]:getParent():getChildByName("Armature_classify")
        local buttonParent = self.m_pAllBtns[i]:getParent()
        self.m_pAllBtns[i]:setPositionX(0)
        local btnSize = self.m_pAllBtns[i]:getContentSize()
        if i == 4 then
            buttonParent:setPositionX(785)
            Armature_classify:setPositionY(-1)
        elseif i == 5 then
            buttonParent:setPositionX(1054)
            Armature_classify:setPosition(cc.p(-8,-10))
        elseif i == 1 then
            buttonParent:setPositionY(520)
        end
        self._scale[i] = Armature_classify:getScale()
        if classify_images[i] ~= "" then
            Armature_classify:setVisible(false)
            local imageView = ccui.ImageView:create(classify_images[i])
            buttonParent:addChild(imageView)
            -- imageView:setScale(0.95)
            -- imageView:setPosition(cc.p(btnSize.width / 2, btnSize.height / 2))
        end
    end

    self.m_pButtonBackClassify = self.m_pNodeGameList:getChildByName("Button_back_classify")
  --  if not LuaUtils.isIphoneXDesignResolution() then 
        self.m_pButtonBackClassify:setPositionX( self.m_pButtonBackClassify:getPositionX() + 130 )
  --  end
    self.m_buttonBackPosx = self.m_pButtonBackClassify:getPositionX()
    self.m_buttonBackPosy = self.m_pButtonBackClassify:getPositionY()
        
    self.m_pBtnMulti:getParent():setPositionY(self.m_pBtnMulti:getParent():getPositionY() + 10)
    self.m_pBtnFish:getParent():setPositionY(self.m_pBtnFish:getParent():getPositionY() + 10)
    self.m_pBtnFish:getParent():setPositionX(self.m_pBtnFish:getParent():getPositionX() + 10)

    self:onUpdateState() --刷新人数状态
end

function HallViewClassify:onUpdateState() --刷新人数状态

    --游戏在在线人数状态图片
    local tag_classify = {
        [1] = "Node_classify_multi",   --多人游戏
        [2] = "Node_classify_arcade",  --单机游戏
        [3] = "Node_classify_fish",    --捕鱼游戏
        [4] = "Node_classify_fight",   --对战游戏
        [5] = "Node_classify_casual",  --休闲游戏
    }
    local images = {}
    for i, v in pairs(tag_classify) do
        images[i] = self.m_pNodeClasify:getChildByName(v):getChildByName("Image_state")
    end

    --游戏在在线人数
    --local count_player = GameListManager.getInstance():getGameClassifyPlayer()

    --刷新图片
    cc.SpriteFrameCache:getInstance():addSpriteFrames("hall/plist/gui-roomChoose.plist", "hall/plist/gui-roomChoose.png")
    local path_player = 
    {
        "hall/plist/roomChoose/gui-image-game-hot.png",    --火爆
        "hall/plist/roomChoose/gui-image-game-more.png",   --拥挤
        "hall/plist/roomChoose/gui-image-game-normal.png", --正常
        "hall/plist/roomChoose/gui-image-game-fast.png",   --流畅
    }

    --[[for i, v in pairs(images) do
        if count_player[i] >= 1000 then
            v:loadTexture(path_player[1], ccui.TextureResType.plistType)
        elseif count_player[i] >= 800 then
            v:loadTexture(path_player[2], ccui.TextureResType.plistType)
        elseif count_player[i] >= 500 then
            v:loadTexture(path_player[3], ccui.TextureResType.plistType)
        else
            v:loadTexture(path_player[4], ccui.TextureResType.plistType)
        end
        v:setCascadeOpacityEnabled(true)
    end]]--
end

function HallViewClassify:IsClasifyShow()
    if self.m_pNodeClasify ~= nil then
        return self.m_pNodeClasify:isVisible()
    end
    return false
end
--收到开关信息了现实游戏分类界面
function HallViewClassify:showGameView()
    self:showActivity()
    --self:updateClassifyButton()
    self:createArrow()
    --self:showGameList()
    self:SetNodeClasifyShow(true)
    SLFacade:dispatchCustomEvent(Hall_Events.UPDATE_ROLLMSG_POSITION,0)
end

--显示推荐展示
function HallViewClassify:showActivity()
    local pageSize = self.m_pNodeRecommend:getContentSize()

    if self.m_pRecommend then
        self.m_pRecommend:removeFromParent()
        self.m_pRecommend = nil
    end

    self.m_pRecommend = ActivityRecommend.new(pageSize)
    self.m_pNodeRecommend:addChild(self.m_pRecommend)
end

--显示游戏列表
function HallViewClassify:showGameList()
    local classify = 
    {
        G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_ARCADE,  --多人游戏
        G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_CARD,
        G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_FISH,
        G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_TIGER,
        G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_CASUAL,
        G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_OTHER_FISH,       --第三方捕鱼
        G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_OTHER_ELECTRONIC, --第三方电子
        G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_OTHER_SPORTS, --第三方体育
        G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_ALL,
    }

    --SLFacade:dispatchCustomEvent(Hall_Events.UPDATE_ROLLMSG_POSITION,1)
    --多人（棋牌-1&捕鱼-6）-1 self.m_pBtnMulti
    --彩票（彩票-2）       -2 self.m_pBtnFight
    -- --真人（真人-3&电子-7）-3 self.m_pBtnFish
    --电玩（体育-8&电竞-4）-4 self.m_pBtnArcade
    --比赛（比赛-5）       -5 self.m_pBtnCasual
    local funcTemp = function()
        -- 显示游戏列表
        if self.m_pGameListView == nil then
            self.m_pGameListView = HallGameListView.new(self)
            self.m_pGameListView:setVisible(true)
            self.m_pGameListView:addTo(self.m_pNodeGameList)
        end
        
        self.m_pGameListView:init(classify[self.m_iSelectIndex], false, self.m_bCheck)
        self.m_pNodeGameList:setVisible(true)  

        self:showNodeClasify()

        -- 动画显示
        if self.m_pParentHallSceneLayer ~= nil then
            self.m_pParentHallSceneLayer:showRightNodeBar( false )
            self.m_pParentHallSceneLayer:showTopNodeBar( false )
        end
        if self.m_bCheck then -- 审核模式
            --隐藏返回按钮
            if self.m_pButtonBackClassify ~= nil then
                self.m_pButtonBackClassify:setVisible(false)
            end
        else
            self:PlayBackClassifyAnimation(true)
        end

        self:getParent():setBankVisible(self.m_iSelectIndex ~= 3)
        if (self.m_iSelectIndex == 3 or self.m_iSelectIndex == 1) then
            self:showCurrentGold()
        end
        if (g_BGController and g_BGController.setGameEnterBgAck) then
            g_BGController:initNetMsgHandlerSwitchData()
            g_BGController:setGameEnterBgAck(function() 
--                if (self.m_iSelectIndex == 3 or self.m_iSelectIndex == 1) then
                    self:showCurrentGold()
--                end
            end)
        end
    end
    
    -- 真人视讯发送消息
    if (self.m_iSelectIndex == 3) then
        ToolKit:addLoadingDialog(3,"正在进入视讯，请稍等......")
        if (g_BGController == nil) then
            g_BGController = require("app/game/shixun/shixunGameController"):getInstance()
        end
        g_BGController:setGameEnterBgAck(funcTemp)
        g_BGController:gameEnterBgReq()
    -- elseif 5 == self.m_iSelectIndex then
    --     MatchController.getInstance():openView("MatchGameListView");
    elseif (self.m_iSelectIndex == 2) then
        -- if true then 
        --     TOAST("即将开放，敬请期待")
        --     return
        -- end
        -- 
        local funcXXTemp = function(instance, info)
            if (g_CPController and g_CPController.setGameEnterAck) then
                g_CPController:setGameEnterAck(function(info) end)
                g_CPController:setGameUrlInfo(info)
                -- 
                local scene = UIAdapter:pushScene("src.app.game.caipiao.caipiaoScene", DIRECTION.HORIZONTAL)
                -- scene:getMainLayer():loadUrl(info, self)
            end
        end
        ToolKit:addLoadingDialog(10, "正在进入彩票，请稍等......")
        if (g_CPController == nil) then
            g_CPController = require("app/game/caipiao/caipiaoGameController"):getInstance()
        end
        g_CPController:setGameEnterAck(funcXXTemp)
        g_CPController:gameEnterReq()
    elseif 6 == self.m_iSelectIndex or 7 == self.m_iSelectIndex  or 8 == self.m_iSelectIndex then
        local thirdType = 0
        if 6 == self.m_iSelectIndex then
            ToolKit:addLoadingDialog(10, "正在进入捕鱼，请稍等......")
            thirdType = 5
        elseif 7 == self.m_iSelectIndex then
            ToolKit:addLoadingDialog(10, "正在进入电子，请稍等......")
            thirdType = 3
        elseif 8 == self.m_iSelectIndex then
            ToolKit:addLoadingDialog(10, "正在进入体育，请稍等......")
            thirdType = 4
        end

        local thirdFuncTmp = function(_id, _cmd)
            if _id == "EnterCP_Ack" then
                ToolKit:removeLoadingDialog()
                if _cmd.m_nRet == 0 then
                    g_ThirdPartyContntroller:setEventHandler(nil)
                    funcTemp()
                end
            end
        end

        -- 彩票：1，真人:2，电子：3， 体育:4， 捕鱼：5
        if g_ThirdPartyContntroller == nil then
            g_ThirdPartyContntroller = require("app.game.caipiao.src.thirdPartyGameContntroller"):getInstance()
        end
        g_ThirdPartyContntroller:setThirdType(thirdType)
        g_ThirdPartyContntroller:setEventHandler(thirdFuncTmp)
        g_ThirdPartyContntroller:sendEnterReq()
        -- funcTemp()
    else
        funcTemp()
    end
end

function HallViewClassify:showCurrentGold()
    -- local url = string.format("http://%s/api/itf/bg/getUserbalance", "172.16.11.116:9999")
    local url = string.format("%sapi/itf/bg/getUserbalance", GlobalConf.DOMAIN)
    local t_data = {
        userId="" .. Player:getAccountID(),
        startTime="",
        endTime="",
    }
    local s_data = json.encode(t_data)
    local xhr = cc.XMLHttpRequest:new()
    xhr:setRequestHeader("Content-Type", "application/json")
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON
    xhr:open("POST", url)
    print(url, s_data)
    local function onReadyStateChange()
       print("Http Status Code:" .. xhr.status .. xhr.status .. xhr.statusText)
       print("xhr.response", xhr.response, self.m_iSelectIndex)
       if xhr.response~="" then
            if xhr.readyState == 4 and (xhr.status >= 200 and xhr.status < 207) then
                local response = xhr.response
                local output = json.decode(response)
                if (output.code == 0 and output.blance) then
                    if output.blance.sys then 
                        Player:setGoldCoin(output.blance.sys)
                        self:getParent().TXT_userScore:setString(output.blance.sys)
                    end
                    if output.blance.bg then
                        Player:setBGGoldCoin(output.blance.bg)
                        self:getParent().TXT_bgUserScore:setString(output.blance.bg)
                    end
                    local userInfoLayer = self:getParent():getChildByName("userInfoLayer")
                    if (userInfoLayer) then
                        userInfoLayer.m_pLbGold:setString(sSum)
                    end
                end
            end
       end
   end
   xhr:registerScriptHandler(onReadyStateChange)
   xhr:send(s_data)
end

function HallViewClassify:showNodeClasify()
    if self.m_pNodeClasify ~= nil then       
        self.m_pNodeClasify:setVisible(true)
        local fade = cc.FadeOut:create(0.2)
        local callback = cc.CallFunc:create(function ()
            self.m_pNodeClasify:setVisible(false)
        end)
        local seq = cc.Sequence:create(fade,callback)
        self.m_pNodeClasify:runAction( seq )
    end
end

function HallViewClassify:getGameListView()
    return self.m_pGameListView
end
local _={}
function _:setBtnEnabled(btn, enabled)
    btn:setEnabled(enabled)
    btn:setTouchEnabled(enabled) 
end

-- 防止频繁点击
function _:disableQuickClick(btn)  
    _:setBtnEnabled(btn, false)
    performWithDelay(btn, function()
        _:setBtnEnabled(btn, true)
    end, 2)
end
function HallViewClassify:initBtnEvent()
    --多人（棋牌-1&捕鱼-6）-1 self.m_pBtnMulti
    --彩票（彩票-2）       -2 self.m_pBtnFight
    --真人（真人-3&电子-7）-3 self.m_pBtnFish
    --电玩（体育-8&电竞-4）-4 self.m_pBtnArcade
    --比赛（比赛-5）       -5 self.m_pBtnCasual

    local buttonCall = function(sender, eventType)
        local btnLight = sender:getParent():getChildByName("Armature_classify")
        local stateIcon = sender:getParent():getChildByName("Image_state")
        local index = sender.index
        if eventType==ccui.TouchEventType.began then 
            if btnLight then
                btnLight:stopAllActions()
                btnLight:runAction(cc.ScaleTo:create(0.05, self._scale[index]+0.08))
            end
            if stateIcon then
                stateIcon:stopAllActions()
                stateIcon:runAction(cc.ScaleTo:create(0.05, 1.08))
            end
        elseif eventType==ccui.TouchEventType.canceled then
            if btnLight then
                btnLight:stopAllActions()
                btnLight:setScale(self._scale[index])
            end
            if stateIcon then
                stateIcon:stopAllActions()
                stateIcon:setScale(1.0)
            end
        elseif eventType==ccui.TouchEventType.ended then 
            self:onItemClicked(index)
            if btnLight then
                btnLight:stopAllActions()
                btnLight:setScale(self._scale[index])
            end
            if stateIcon then
                stateIcon:stopAllActions()
                stateIcon:setScale(1.0)
            end
            _:disableQuickClick(sender)
        end
    end
    local buttonCallEx = function(sender, eventType)
        local btnLight = sender:getParent():getChildByName("Armature_classify")
        local stateIcon = sender:getParent():getChildByName("Image_state")
        local index = sender.index
        if eventType==ccui.TouchEventType.began then 
            if btnLight then
                btnLight:stopAllActions()
                btnLight:runAction(cc.ScaleTo:create(0.05, self._scale[index]+0.08))
            end
            if stateIcon then
                stateIcon:stopAllActions()
                stateIcon:runAction(cc.ScaleTo:create(0.05, 1.08))
            end
        elseif eventType==ccui.TouchEventType.canceled then
            if btnLight then
                btnLight:stopAllActions()
                btnLight:setScale(self._scale[index])
            end
            if stateIcon then
                stateIcon:stopAllActions()
                stateIcon:setScale(1.0)
            end
        elseif eventType==ccui.TouchEventType.ended then 
            local beginPoint = sender:convertToNodeSpace(cc.p(sender:getTouchBeganPosition()))
            local endPoint = sender:convertToNodeSpace(cc.p(sender:getTouchEndPosition()))
            
            local size = sender:getContentSize()
            local selIndex = index
            if beginPoint.x <= size.width / 2 and endPoint.x <= size.width / 2 then
                if index == 1 then
                    selIndex = 1
                elseif index == 4 then
                    selIndex = 8
                end
            else
                if index == 1 then
                    selIndex = 6
                elseif index == 4 then
                    selIndex = 4
                end
            end

            self:onItemClicked(selIndex)
            if btnLight then
                btnLight:stopAllActions()
                btnLight:setScale(self._scale[index])
            end
            if stateIcon then
                stateIcon:stopAllActions()
                stateIcon:setScale(1.0)
            end
            _:disableQuickClick(sender)
        end
    end
    -- 绑定按钮
    for i = 1, 5 do      
        self.m_pAllBtns[i].index = i
        if i == 2 or i == 3 then
            self.m_pAllBtns[i]:addTouchEventListener(buttonCall)
        else
            self.m_pAllBtns[i]:addTouchEventListener(buttonCallEx)
        end
--        self.m_pAllBtns[i]:addClickEventListener(function()
--            self:onItemClicked(i)
--        end)
    end
    self.m_pButtonBackClassify:addTouchEventListener(handler(self,self.onBackClassifyClicked))
end

function HallViewClassify:onBackClassifyClicked(pSender,ntype)
    if ntype ==ccui.TouchEventType.ended  then
        local funcTemp = function()
            g_GameMusicUtil:playSound("public/sound/sound-close.mp3")

            if self.m_pNodeClasify ~= nil then
                self.m_pNodeClasify:setVisible(true)
            end
            local delaytime = 0.2
            if self.m_pNodeGameList ~= nil then        
                if self.m_pGameListView ~= nil then
                    self.m_pGameListView:PlayCloseGameListAnimation( delaytime )
                end

                local callback = cc.CallFunc:create(function ()
                    self.m_pNodeGameList:setVisible(false)
                    self.m_pGameListView:shutDown()
                end)
                local seq = cc.Sequence:create( cc.DelayTime:create(delaytime) ,callback)
                self.m_pNodeGameList:runAction( seq )
            end
            self:PlayBackClassifyAnimation( false )
            if self.m_pParentHallSceneLayer ~= nil then
                self.m_pParentHallSceneLayer:showRightNodeBar( true )
                self.m_pParentHallSceneLayer:showTopNodeBar( true )
            end
            self:SetNodeClasifyShow(true)
            self:refreshArrow(false, false)

            --SLFacade:dispatchCustomEvent(Hall_Events.UPDATE_ROLLMSG_POSITION,0)

            --刷新人数状态
            self:onUpdateState()

            self:getParent():setBankVisible(true)
            
            ToolKit:removeLoadingDialog()  

            if (self.m_iSelectIndex == 3 and g_BGController) then
                self:showCurrentGold()
                g_BGController:releaseInstance()
            end
        end

        if (self.m_iSelectIndex == 3) then
        	if g_isNetworkInFail then
                if (g_BGController) then
                    g_BGController:releaseInstance()
                end
                ToolKit:returnToLoginScene()
                return ;
            end
            if (g_BGController) then
                ToolKit:addLoadingDialog(3,"正在退出视讯，请稍等......")
				g_BGController:setGameExitBgAck(funcTemp)
                g_BGController:gameExitBgReq()
            end
        elseif 6 == self.m_iSelectIndex or 7 == self.m_iSelectIndex  or 8 == self.m_iSelectIndex then
            local thirdType = 0
            if 6 == self.m_iSelectIndex then
                ToolKit:addLoadingDialog(10, "正在退出捕鱼，请稍等......")
                thirdType = 5
            elseif 7 == self.m_iSelectIndex then
                ToolKit:addLoadingDialog(10, "正在退出电子，请稍等......")
                thirdType = 3
            elseif 8 == self.m_iSelectIndex then
                ToolKit:addLoadingDialog(10, "正在退出体育，请稍等......")
                thirdType = 4
            end
            if g_isNetworkInFail then
                if (g_ThirdPartyContntroller) then
                    g_ThirdPartyContntroller:releaseInstance()
                end
                ToolKit:returnToLoginScene()
                return ;
            end
            -- 彩票：1，真人:2，电子：3， 体育:4， 捕鱼：5
            g_ThirdPartyContntroller:setEventHandler(function(_id, _cmd)
                if _id == "ExitCP_Ack" then
                    ToolKit:removeLoadingDialog()
                    -- if _cmd.m_nRet == 0 then
                        if g_ThirdPartyContntroller then
                            g_ThirdPartyContntroller:releaseInstance()
                        end
                        funcTemp()
                    -- end
                end
            end)
            g_ThirdPartyContntroller:sendExitReq()
        else
            if 5 == self.m_iSelectIndex then
                MatchController.getInstance():closeView("MatchGameListView");
            end
            funcTemp()
        end
    end
end

function HallViewClassify:onMsgBackHallViewClassify()
    --g_GameMusicUtil:playSound("public/sound/sound-close.mp3")

    if self.m_pNodeClasify ~= nil then
        self.m_pNodeClasify:setVisible(true)
    end
    local delaytime = 0.2
    if self.m_pNodeGameList ~= nil then        
        if self.m_pGameListView ~= nil then
            self.m_pGameListView:PlayCloseGameListAnimation( delaytime )
        end

        local callback = cc.CallFunc:create(function ()
            self.m_pNodeGameList:setVisible(false)
            self.m_pGameListView:shutDown()
        end)
        local seq = cc.Sequence:create( cc.DelayTime:create(delaytime) ,callback)
        self.m_pNodeGameList:runAction( seq )
    end
    self:PlayBackClassifyAnimation( false )
    if self.m_pParentHallSceneLayer ~= nil then
        self.m_pParentHallSceneLayer:showRightNodeBar( true )
        self.m_pParentHallSceneLayer:showTopNodeBar( true )
    end
    self:SetNodeClasifyShow(true)
    self:refreshArrow(false, false)

    --SLFacade:dispatchCustomEvent(Hall_Events.UPDATE_ROLLMSG_POSITION,0)

    --刷新人数状态
    self:onUpdateState()

    self:getParent():setBankVisible(true)
    
    ToolKit:removeLoadingDialog()  
end


-- function HallViewClassify:on_CLOSE_MATCH_LIST_VIEW()
--     self:backClassify();
-- end


function HallViewClassify:SetNodeClasifyShow( bShow )
    if self.m_pNodeClasify ~= nil then
        if bShow then
            self.m_pNodeClasify:setVisible(true)
            self.m_pNodeClasify:setOpacity(0)
            local posx, posy = self.m_pNodeClasify:getPosition()
            self.m_pNodeClasify:setPosition( posx - 250, posy )
            local moveAct = cc.EaseBackOut:create( cc.MoveTo:create(0.4, cc.p(posx,posy)) )
            local spawnAct = cc.Spawn:create(cc.FadeIn:create(0.4), moveAct)
            self.m_pNodeClasify:runAction( spawnAct )
        else 
            local fade = cc.FadeOut:create(0.2)
            local callback = cc.CallFunc:create(function ()
                self.m_pNodeClasify:setVisible(false)
            end)
            local seq = cc.Sequence:create(fade,callback)
            self.m_pNodeClasify:runAction( seq )
        end
    end
end
function HallViewClassify:createArrow()
    local pBkgImg = cc.Sprite:createWithSpriteFrameName("hall/plist/hall/gui-hall-arrow-left.png")
    local pNormalSprite = ccui.Scale9Sprite:createWithSpriteFrame(pBkgImg:getSpriteFrame())
    local pClickSprite = ccui.Scale9Sprite:createWithSpriteFrame(pBkgImg:getSpriteFrame())
    self.m_pBtnLeftArrow = cc.ControlButton:create(pNormalSprite)
    self.m_pBtnLeftArrow:setBackgroundSpriteForState(pClickSprite,cc.CONTROL_STATE_HIGH_LIGHTED)
    self.m_pBtnLeftArrow:addTo(self.m_pNodeMain)
    self.m_pBtnLeftArrow:setAnchorPoint(cc.p(0, 0.5))
    self.m_pBtnLeftArrow:setPosition(cc.p(10 - (display.width - 1334) / 2, 380))
    local seq = cc.Sequence:create(cc.MoveBy:create(0.4, cc.p(-8,0)),cc.MoveBy:create(0.4, cc.p(8,0)))
    self.m_pBtnLeftArrow:runAction(cc.RepeatForever:create(seq))
    self.m_pBtnLeftArrow:registerControlEventHandler(handler(self, self.onLeftArrowClicked), cc.CONTROL_EVENTTYPE_TOUCH_UP_INSIDE);

    local pBkgImg2 = cc.Sprite:createWithSpriteFrameName("hall/plist/hall/gui-hall-arrow-right.png")
    local pNormalSprite2 = ccui.Scale9Sprite:createWithSpriteFrame(pBkgImg2:getSpriteFrame())
    local pClickSprite2 = ccui.Scale9Sprite:createWithSpriteFrame(pBkgImg2:getSpriteFrame())
    self.m_pBtnRightArrow = cc.ControlButton:create(pNormalSprite2)
    self.m_pBtnRightArrow:setBackgroundSpriteForState(pClickSprite2,cc.CONTROL_STATE_HIGH_LIGHTED)
    self.m_pBtnRightArrow:addTo(self.m_pNodeMain)
    self.m_pBtnRightArrow:setAnchorPoint(cc.p(1,0.5))
    self.m_pBtnRightArrow:setPosition(cc.p(display.width - 10 - (display.width - 1334) / 2, 380))
    local seq2 = cc.Sequence:create(cc.MoveBy:create(0.4, cc.p(8,0)),cc.MoveBy:create(0.4, cc.p(-8,0)))
    self.m_pBtnRightArrow:runAction(cc.RepeatForever:create(seq2))
    self.m_pBtnRightArrow:registerControlEventHandler(handler(self, self.onRightArrowClicked), cc.CONTROL_EVENTTYPE_TOUCH_UP_INSIDE)

    self.m_pBtnLeftArrow:setVisible(false)
    self.m_pBtnRightArrow:setVisible(false)

    --if LuaUtils.isIphoneXDesignResolution() then 
    --    self.m_pBtnLeftArrow:setPositionX( self.m_pBtnLeftArrow:getPositionX() - 140 )
    --    self.m_pBtnRightArrow:setPositionX( self.m_pBtnRightArrow:getPositionX() - 80 )
    --end
end

function HallViewClassify:refreshArrow(leftVisible, rightVisible)
    self.m_pBtnLeftArrow:setVisible(leftVisible)
    self.m_pBtnRightArrow:setVisible(rightVisible)
end

function HallViewClassify:playArrowAction(isShowClassify)
    if not isShowClassify then 
        self.m_pBtnLeftArrow:runAction( cc.EaseBackIn:create(cc.MoveBy:create(0.25, cc.p(-110, 0))) )
    else
        self.m_pBtnLeftArrow:runAction( cc.EaseBackOut:create(cc.MoveBy:create(0.25, cc.p(110, 0))) )
    end
end

--进入分类列表 bCheck：审核或推广状态
function HallViewClassify:onEnterGameClass(index, bCheck)
    self.m_iSelectIndex = index
    self.m_bCheck = bCheck
    self:showGameList()
end

----------------------
---按钮事件响应
----------------------
function HallViewClassify:onItemClicked(index)
    -- 防连点
    local nCurTime = ToolKit:getTimeMs()/1000
    --[[if self.m_nLastTouchTime and nCurTime - self.m_nLastTouchTime <= 1 then
        return
    end]]--
    self.m_nLastTouchTime = nCurTime
  --  g_GameMusicUtil:playSound("hall/sound/sound-hall-selected.mp3")
    self:onEnterGameClass(index)
end

--播放游戏分类返回按钮动画
function HallViewClassify:PlayBackClassifyAnimation( bShow )
    if self.m_pButtonBackClassify ~= nil then
        if bShow then
            self.m_pButtonBackClassify:setVisible(true)
            self.m_pButtonBackClassify:setEnabled( false )
            self.m_pButtonBackClassify:setOpacity(0)
            local posx, posy = self.m_buttonBackPosx, self.m_buttonBackPosy
            self.m_pButtonBackClassify:setPosition( posx, posy + 200)
            local moveAct = cc.EaseBackOut:create( cc.MoveTo:create(0.4, cc.p(posx,posy)) )
            local spawnAct = cc.Spawn:create(cc.FadeIn:create(0.4), moveAct)

            local callback = cc.CallFunc:create(function ()
                self.m_pButtonBackClassify:setEnabled( true )
            end)
            local seq = cc.Sequence:create(spawnAct,callback)
            self.m_pButtonBackClassify:runAction( seq )
        else
            local moveAct = cc.MoveTo:create(0.2, cc.p(self.m_buttonBackPosx, self.m_buttonBackPosy + 200))
            local spawnAct = cc.Spawn:create(cc.FadeOut:create(0.2), moveAct)
            local callback = cc.CallFunc:create(function ()
                self.m_pButtonBackClassify:setVisible(false)
            end)
            local seq = cc.Sequence:create(spawnAct,callback)
            self.m_pButtonBackClassify:runAction( seq )
        end
    end
end

function HallViewClassify:onClassifyClicked() -- 游戏分类
    g_GameMusicUtil:playSound("public/sound/sound-button.mp3")

    local actionName = "showClassify"
--    if self.m_bClassifyShow then 
--        actionName = "hideClassify"
--    end
--    self.m_bClassifyShow = not self.m_bClassifyShow
--    self:playArrowAction(self.m_bClassifyShow)

    self.m_pAction:play(actionName, false)
    if self.m_pGameListView then
        self.m_pGameListView:ReSizeScrollView( false )
    end
    local eventFrameCall = function(frame)  
        self.m_pAction:clearLastFrameCallFunc()
        self.m_pAction = nil
    end
    self.m_pAction:setLastFrameCallFunc(eventFrameCall)
end

function HallViewClassify:onLeftArrowClicked(pSender)
    g_GameMusicUtil:playSound("public/sound/sound-button.mp3")
    self.m_pGameListView:scrollToPercent(0)
end

function HallViewClassify:onRightArrowClicked(pSender)
    g_GameMusicUtil:playSound("public/sound/sound-button.mp3")
    self.m_pGameListView:scrollToPercent(100)
end

----------------------
---界面更新
----------------------
--function HallViewClassify:updateClassifyButton() -- 更新分类按钮
--    for i = 1, 5 do
--        self.m_pAllBtns[i]:setEnabled(true)
--        if i == self.m_iSelectIndex then
--            self.m_pAllBtns[i]:setEnabled(false)
--        end
--    end
--end

function HallViewClassify:PlayAnimationBackToHall()
    if self.m_pGameListView ~= nil then
        self.m_pGameListView:PlayAnimationBackToHall()
        if not self.m_bCheck then 
            self.m_pGameListView:SetGameClassifyTitleShow( true, 0.4 ) 
        end
    end
    --推荐pageView
    local nodeX, nodeY = self.m_pNodeRecommend:getPosition()
    self.m_pNodeRecommend:setPosition(cc.p(nodeX - 250, nodeY))
    self.m_pNodeRecommend:runAction( cc.EaseBackOut:create(cc.MoveTo:create(0.5, cc.p(nodeX, nodeY))) )

    --self.m_pBtnClassify
--    if not self.m_bClassifyShow  then
--        local btnX, btnY = self.m_pBtnClassify:getPosition()
--        self.m_pBtnClassify:setPosition(cc.p(btnX, btnY - 160))
--        self.m_pBtnClassify:runAction( cc.EaseBackOut:create(cc.MoveTo:create(0.33, cc.p(btnX, btnY))) )
--    end
end

function HallViewClassify:playGameListHideORAppear( bShow )
    if not self.m_bCheck then
        self:PlayBackClassifyAnimation( bShow )
    end
    if self.m_pParentHallSceneLayer ~= nil then
        self.m_pParentHallSceneLayer:showBottomNodeBar( bShow )
    end
    if not bShow and self.m_pGameListView ~= nil then
        self.m_pGameListView:PlayCloseGameListAnimation( 0.12 )
    end
end

return HallViewClassify


--endregion

