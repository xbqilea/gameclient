local UrlImage = require("app.hall.base.ui.UrlImage")
local HallThirdPartyGameLayer = class("HallThirdPartyGameLayer", function()
    return display.newLayer()
end)

function HallThirdPartyGameLayer:ctor(gameClassifyType)
    self.nGameType = gameClassifyType
    self.nPageCount = 0
    self.nCurrentIndex = 0
    local offsetX = (display.width - 1624) / 2
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    self:setContentSize(display.size)
    self.m_Root = display.newNode()
    self:addChild(self.m_Root)
    self.m_Root:setPosition(cc.p(display.width / 2 - offsetX, display.height / 2 + 35))

    self.m_PageView = ccui.PageView:create()
    self.m_Root:addChild(self.m_PageView)
    self.m_PageView:setContentSize({ width = 1200, height = 460 })
    self.m_PageView:setAnchorPoint(cc.p(0.5, 0.5))
    self.m_PageView:setPosition(cc.p(0, 0))
    self.m_PageView:setLayoutType(2)
    -- self.m_PageView:setBackGroundColorType(1)
    -- self.m_PageView:setBackGroundColor(cc.c3b(0,0,0))
    self.m_PageView:setTouchEnabled(false)

    local image_path = "hall/image/classify/button_next.png"
    self.m_ButtonLeft = ccui.Button:create(image_path, image_path, image_path)
    self.m_Root:addChild(self.m_ButtonLeft)
    self.m_ButtonLeft:setScaleX(-1)
    self.m_ButtonLeft:setPosition(cc.p(-630, 0))
    self.m_ButtonLeft:setVisible(false)
    UIAdapter:registClickCallBack(self.m_ButtonLeft, handler(self, self.onLeftClick))

    self.m_ButtonRight = ccui.Button:create(image_path, image_path, image_path)
    self.m_Root:addChild(self.m_ButtonRight)
    self.m_ButtonRight:setPosition(cc.p(630, 0))
    self.m_ButtonRight:setVisible(false)
    UIAdapter:registClickCallBack(self.m_ButtonRight, handler(self, self.onRightClick))

    self.m_DotNode = display.newNode()
    self.m_Root:addChild(self.m_DotNode)
    self.m_DotNode:setPosition(cc.p(0, -230))
    self.m_DotNode:setAnchorPoint(cc.p(0.5, 1))

    -- AG电子	ag
    -- MG 电子	mg
    -- QT电子	qt
    -- CQ9电子	cq9
    -- JDB电子	jdb
    -- PT电子	pt
    -- 捕鱼游戏	by
    if 6 == self.nGameType then --第三方捕鱼
        self:loadFishItems()
    elseif 7 == self.nGameType then --第三方电子
        self:loadElectronicItems()
    elseif 8 == self.nGameType then --第三方体育
        self:loadSportsItems()
    end

    if self.nPageCount > 1 then
        self.m_ButtonRight:setVisible(true)
        self:initDotNode()
    end
end

function HallThirdPartyGameLayer:onDestory()

end

--------------------------------------------------------------
---按钮回调
--------------------------------------------------------------
function HallThirdPartyGameLayer:onLeftClick()
    local index = self.nCurrentIndex - 1
    if index <= 0 then
        index = 0
        -- self.m_ButtonLeft:setVisible(false)
    else
        -- self.m_ButtonRight:setVisible(true)
    end
    self:setCurrentPage(index)
end

function HallThirdPartyGameLayer:onRightClick()
    local index = self.nCurrentIndex + 1
    if index >= self.nPageCount - 1 then
        index = self.nPageCount - 1
        -- self.m_ButtonRight:setVisible(false)
    else
        -- self.m_ButtonLeft:setVisible(true)
    end
    self:setCurrentPage(index)
end

--------------------------------------------------------------
---私有函数
--------------------------------------------------------------
function HallThirdPartyGameLayer:initDotNode()
    self.m_DotNode:removeAllChildren()
    -- sel "hall/plist/hall/gui-hall-page-active.png"
    -- un "hall/plist/hall/gui-hall-page-normal.png"
    -- plist "hall/plist/gui-hall"
    display.loadSpriteFrames("hall/plist/gui-hall.plist", "hall/plist/gui-hall.png")
    local _width = self.nPageCount * 34
    local _height = 32
    self.m_DotNode:setContentSize({ width = _width, height = _height })

    for index = 1, self.nPageCount do
        local item = ccui.ImageView:create("hall/plist/hall/gui-hall-page-normal.png", ccui.TextureResType.plistType)
        self.m_DotNode:addChild(item)
        item:setPosition(cc.p(index * 34 - 17, 16))
        item:setTag(index)
        if index == 1 then
            item:loadTexture("hall/plist/hall/gui-hall-page-active.png", ccui.TextureResType.plistType)
            item:setTouchEnabled(false)
        else
            item:setTouchEnabled(true)
        end

        UIAdapter:registClickCallBack(item, function(sender)
            local tag = sender:getTag()
            self:setCurrentPage(tag - 1)
        end)
    end
end

function HallThirdPartyGameLayer:setCurrentPage(index)
    if index == self.nCurrentIndex then
        return
    end
    if self.nPageCount < 1 then
        self.m_ButtonLeft:setVisible(false)
        self.m_ButtonRight:setVisible(false)
    else
        if index <= 0 then
            self.m_ButtonLeft:setVisible(false)
            self.m_ButtonRight:setVisible(true)
        elseif index >= self.nPageCount - 1 then
            self.m_ButtonLeft:setVisible(true)
            self.m_ButtonRight:setVisible(false)
        else
            self.m_ButtonLeft:setVisible(true)
            self.m_ButtonRight:setVisible(true)
        end
        self.m_PageView:scrollToPage(index)
    end

    local item = self.m_DotNode:getChildByTag(self.nCurrentIndex + 1)
    if item then
        item:loadTexture("hall/plist/hall/gui-hall-page-normal.png", ccui.TextureResType.plistType)
        item:setTouchEnabled(true)
    end

    item = self.m_DotNode:getChildByTag(index + 1)
    if item then
        item:loadTexture("hall/plist/hall/gui-hall-page-active.png", ccui.TextureResType.plistType)
        item:setTouchEnabled(false)
    end

    self.nCurrentIndex = index
end

function HallThirdPartyGameLayer:createUrlImageView(imgUrl, buttonCall)
    local item = display.newNode()
    item.mImage = ccui.ImageView:create("hall/image/promote/ImageDefault.png")
    item:addChild(item.mImage, 1)
    item.mImage:ignoreContentAdaptWithSize(false)
    item.mImage:setContentSize({ width = 200, height = 200 })

    -- if string.find(imgUrl, ".jpg") or string.find(imgUrl, ".png") then
    --     local _onLoadCall = function(_isSuccess, _file, _sender)
    --         if _isSuccess then
    --             item.mImage:loadTexture(_file)
    --             item.mImage:ignoreContentAdaptWithSize(false)
    --             item.mImage:setContentSize({ width = 200, height = 200 })
    --         end
    --     end
    --     item.mUrlImage = UrlImage.new(g_ThirdPartyContntroller.mDOMAIN .. imgUrl, false, _onLoadCall, false)
    --     item:addChild(item.mUrlImage, 0)
    --     item.mUrlImage:setVisible(false)
    --     item.mUrlImage:setScaleSize(200, 200)
    -- end

    if buttonCall then
        item.mImage.buttonCall = buttonCall
        item.mImage:setTouchEnabled(true)
        UIAdapter:registClickCallBack(item.mImage, function(sender)
            sender.buttonCall(sender:getParent())
        end)
    end
    return item
end

function HallThirdPartyGameLayer:loadImageFile()
    if self.mImageUrls and self.mLoadIndex <= #self.mImageUrls then
        local index = self.mLoadIndex
        local item = self.mImageUrls[index]
        self.mLoadIndex = self.mLoadIndex + 1
        local imgUrl = item.info.imgUrl
        if string.find(imgUrl, ".jpg") or string.find(imgUrl, ".png") then
            local _onLoadCall = function(_isSuccess, _file, _sender)
                if _isSuccess then
                    -- item.mImage:loadTexture(_file)
                    -- item.mImage:ignoreContentAdaptWithSize(false)
                    -- item.mImage:setContentSize({ width = 200, height = 200 })
                    item.mImage:setOpacity(0)
                end
                self:loadImageFile()
            end
            item.mUrlImage = UrlImage.new(g_ThirdPartyContntroller.mDOMAIN .. imgUrl, false, _onLoadCall, false)
            item:addChild(item.mUrlImage, 0)
            -- item.mUrlImage:setVisible(false)
            item.mUrlImage:setScaleSize(200, 200)
        else
            self:loadImageFile()
        end
    end
end

--捕鱼
function HallThirdPartyGameLayer:loadFishItems()
    ToolKit:addLoadingDialog(10, "获取游戏列表。。。")
    --self:sendPost(g_ThirdPartyContntroller.mDOMAIN.."api/getDzGameItem.do", "code=by", function (_response, _json, _success)
    self:sendGet(g_ThirdPartyContntroller.mDOMAIN .. "api/getDzGameItem.do?code=by", function(_response, _json, _success)
        ToolKit:removeLoadingDialog()
        if _success then
            self.mLoadIndex = 1
            self.mImageUrls = {}
            self.m_PageView:removeAllPages()
            local pageSize = self.m_PageView:getContentSize()
            local infos = _json["content"]
            local count = #infos
            local i = 1
            local layout = nil
            self.nPageCount = 0

            print("捕鱼游戏 count：" .. count)
            for index = 1, count do
                if i == 1 then
                    layout = ccui.Layout:create()
                    layout:setContentSize(pageSize)
                    self.m_PageView:addPage(layout)
                    self.nPageCount = self.nPageCount + 1
                end
                -- typeid:"qt"
                -- imgUrl:"common/template/third/newEgame/images/by/RTG-fishcatch.png"
                -- displayEnName:"null"
                -- gameId:"RTG-fishcatch"
                -- buttonImagePath:"by/RTG-fishcatch.png"
                -- platType:4
                -- displayName:"捕鱼"
                local info = infos[index]
                local item = self:createUrlImageView(info.imgUrl, function(sender)
                    ToolKit:addLoadingDialog(10, "正在进入，请稍等......")
                    g_ThirdPartyContntroller:sendEnterThirdPartReq(sender.info.typeid, sender.info.platType, sender.info.gameId)
                end)
                layout:addChild(item)
                item.info = info
                table.insert(self.mImageUrls, item)

                local w = pageSize.width / 5
                local h = pageSize.height / 2
                local x = w * i - w / 2
                local y = h + h / 2
                if i > 5 then
                    x = w * (i - 5) - w / 2
                    y = h / 2
                end
                item:setPosition(cc.p(x, y))

                if i == 10 then
                    i = 1
                else
                    i = i + 1
                end
            end
            self:loadImageFile()
            self:initDotNode()
            self.nCurrentIndex = -1
            self:setCurrentPage(0)
        else
            TOAST("获取游戏列表失败")
        end
    end)

    -- local pageSize = self.m_PageView:getContentSize()
    -- local items = {}
    -- local count = 18
    -- local i = 1
    -- local layout = nil
    -- for index = 1, count do
    --     if i == 1 then
    --         layout = ccui.Layout:create()
    --         layout:setContentSize(pageSize)
    --         self.m_PageView:addPage(layout)
    --         self.nPageCount = self.nPageCount + 1
    --     end
    --     -- local item = items[index]
    --     local imageView = ccui.ImageView:create("hall/image/gamekind/gamekind-"..math.random(1, 15)..".png")
    --     imageView:setSwallowTouches(true)
    --     imageView:setScale(0.8)
    --     layout:addChild(imageView)
    --     local w = pageSize.width / 5
    --     local h = pageSize.height / 2
    --     local x = w * i - w / 2
    --     local y = pageSize.height - h / 2
    --     if i > 5 then
    --         x = w * (i - 5) - w / 2
    --         y = h / 2
    --     end
    --     imageView:setPosition(cc.p(x, y))
    --     if i == 10 then
    --         i = 1
    --     else 
    --         i = i + 1
    --     end
    -- end
end

--电子
function HallThirdPartyGameLayer:loadElectronicItems()
end

--体育
function HallThirdPartyGameLayer:loadSportsItems()
    local pageSize = self.m_PageView:getContentSize()
    local config = {
        { name = "皇冠体育", playCode = "HG", platType = 0, gameId = "", kindId = 801 },
        { name = "沙巴体育", playCode = "IBC", platType = 10, gameId = "", kindId = 802 },
        { name = "BB体育", playCode = "BBIN", platType = 2, gameId = "", kindId = 803 },
    }
    local count = #config
    local i = 1
    local layout = nil

    for index = 1, count do
        if i == 1 then
            layout = ccui.Layout:create()
            layout:setContentSize(pageSize)
            self.m_PageView:addPage(layout)
            self.nPageCount = self.nPageCount + 1
        end
        local info = config[index]
        local imageView = ccui.ImageView:create("hall/image/gamekind/gamekind-" .. info.kindId .. ".png")
        imageView:setSwallowTouches(true)
        -- imageView:setScale(0.8)
        layout:addChild(imageView)
        imageView:setTag(info.kindId)
        imageView.info = info
        local w = pageSize.width / 4
        local h = pageSize.height / 2
        local x = w * i - w / 2
        local y = h
        imageView:setPosition(cc.p(x, y))

        if i == 4 then
            i = 1
        else
            i = i + 1
        end

        imageView:setTouchEnabled(true)
        imageView:setSwallowTouches(false)
        UIAdapter:registClickCallBack(imageView, function(sender)
            local info = sender.info
            ToolKit:addLoadingDialog(10, "正在进入，请稍等......")
            g_ThirdPartyContntroller:sendEnterThirdPartReq(info.playCode, info.platType, info.gameId)
        end)
    end
end

--------------------------------------------------------------
---工具
--------------------------------------------------------------
local urlEncode = function(s)
    s = string.gsub(s, "([^%w%.%- ])", function(c) return string.format("%%%02X", string.byte(c)) end)
    return string.gsub(s, " ", "+")
end

local urlDecode = function(s)
    s = string.gsub(s, '%%(%x%x)', function(h) return string.char(tonumber(h, 16)) end)
    return s
end

function HallThirdPartyGameLayer:sendPost(_url, _data, _handler)
    local xhr = cc.XMLHttpRequest:new()
    xhr:setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
    -- xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
    xhr:open("POST", _url)
    xhr:registerScriptHandler(function()
        if tolua.isnull(self) then return end
        if nil == xhr.response or "" == xhr.response then
            _handler(xhr.response, nil, false)
            return
        end
        print("HallThirdPartyGameLayer:sendPost:::" .. xhr.response .. "\n")
        local _json = json.decode(xhr.response)
        local isSuccess = _json["success"]
        if isSuccess == nil or not isSuccess then
            _handler(xhr.response, _json, false)
            return
        end
        _handler(xhr.response, _json, true)
    end)
    xhr:send(_data)
end

function HallThirdPartyGameLayer:sendGet(_url, _handler)
    local xhr = cc.XMLHttpRequest:new()
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
    xhr:open("GET", _url)
    xhr:registerScriptHandler(function()
        if tolua.isnull(self) then return end
        if nil == xhr.response or "" == xhr.response then
            _handler(xhr.response, nil, false)
            return
        end
        print("HallThirdPartyGameLayer:sendGet:::" .. xhr.response .. "\n")
        local _json = json.decode(xhr.response)
        local isSuccess = _json["success"]
        if isSuccess == nil or not isSuccess then
            _handler(xhr.response, _json, false)
            return
        end
        _handler(xhr.response, _json, true)
    end)
    xhr:send()
end

return HallThirdPartyGameLayer