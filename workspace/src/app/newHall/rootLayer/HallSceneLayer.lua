 

local UpdateController = require("src.app.assetmgr.main.control.RenewController")

local PLATFORM_ZORDER_UI			= 100;
local PLATFORM_UI_ZORDER_TOP		= PLATFORM_ZORDER_UI + 1;		--
local PLATFORM_UI_ZORDER_BOTTOM	= PLATFORM_ZORDER_UI + 1;		--
local PLATFORM_UI_ZORDER_NOTICE	= PLATFORM_ZORDER_UI + 1;		--
local PLATFORM_UI_ZORDER_POPULARISE	= PLATFORM_ZORDER_UI + 5;	--推广
local ROOM_LAYER_ZORDER			= PLATFORM_ZORDER_UI + 2;		-- 房间列表层级
local GAMELIST_LAYER_ZORDER		= PLATFORM_ZORDER_UI + 8;		-- 游戏列表层级
local DESKLIST_LAYER_ZORDER		= PLATFORM_ZORDER_UI + 3;		-- 桌子列表层级
local CHILD_LAYER_ZORDER			= PLATFORM_ZORDER_UI + 4;		-- 子节点弹出框层级 
local CHILD_LAYER_TAG			= 1000;
local CHILD_NEXTLAYER_TAG		= 100;
local CHILD_LAYER_USERHEAD_TAG	= CHILD_LAYER_TAG + 1;			-- 图像层标签
local CHILD_LAYER_STORE_TAG		= CHILD_LAYER_TAG + 2;			-- 商店层标签
local CHILD_LAYER_SET_TAG		= CHILD_LAYER_TAG + 3;			-- 设置层标签
local CHILD_LAYER_BANK_TAG		= CHILD_LAYER_TAG + 4;			-- 银行层标签
local CHILD_LAYER_SERVICE_TAG	= CHILD_LAYER_TAG + 5;			-- 服务层标签
local CHILD_LAYER_ROOM_TAG		= CHILD_LAYER_TAG + 6;			-- 房间层标签
local CHILD_LAYER_GAMES_TAG		= CHILD_LAYER_TAG + 7;			-- 游戏层标签
local CHILD_LAYER_DESKS_TAG		= CHILD_LAYER_TAG + 8;			-- 桌子层标签
local CHILD_LAYER_EXIT_TAG		= CHILD_LAYER_TAG + 9;			-- 退出层标签
local CHILD_LAYER_RANK_TAG		= CHILD_LAYER_TAG + 12;			-- 排行榜层标签
local CHILD_LAYER_SIGN_TAG		= CHILD_LAYER_TAG + 13;			-- 签到层标签
local CHILD_LAYER_ONLINE_TAG		= CHILD_LAYER_TAG + 14;			-- 在线奖励层标签
local CHILD_LAYER_SPREAD_TAG		= CHILD_LAYER_TAG + 15;			-- 推广员层标签
local CHILD_LAYER_MATCH_TAG		= CHILD_LAYER_TAG + 16;			-- 比赛报名层标签
local CHILD_LAYER_PASS_TAG		= CHILD_NEXTLAYER_TAG;			-- 修改密码层标签
local CHILD_LAYER_PHONE_TAG		= CHILD_NEXTLAYER_TAG + 1;		-- 绑定手机层标签
local CHILD_LAYER_RECORD_TAG		= CHILD_NEXTLAYER_TAG + 2;		-- 战绩列表层标签
local CHILD_LAYER_GIFT_TAG		= CHILD_NEXTLAYER_TAG + 3;		-- 礼品兑换层标签
local CHILD_LAYER_CLUB_TAG		= CHILD_NEXTLAYER_TAG + 4;		-- 俱乐部层标签
--endregion

local GAME_SOUND_BUTTON     = "hall/sound/sound-hall-selected.mp3";       --点击弹出按钮音效 

local PLATFORMUI_CSB	= "hall/csb/HallViewNew.csb";

 import("..data.GameSwitchData")
 local HNLayer = import("..HNLayer")
 local scheduler = require("framework.scheduler")
local enter_room_outtime_timer_time = 30.0;
local GAME_ITEM_UI	= "platform/lobbyUi/gameItem_Node.csb"
local request_room_list_text = "获取房间列表......";
 
local StackLayer = require("app.hall.base.ui.StackLayer")
local ChargeLayer = import("..childLayer.ZCShopView")  
local GameUserHead = import(".UserHeadLayer")
local BankLayer =  import("..childLayer.BankLayer")
local ExchangeLayer =  import("..childLayer.ExchangeLayer")
local UserInfoLayer = import("..childLayer.UserCenterLayer")
local RankLayer = import("..childLayer.RankLayer")
local AgentLayer =  import("..childLayer.AgentLayer")
local RegisterGoldLayer=  import("..childLayer.RegisterGoldLayer") 
local MailLayer=  import("..childLayer.MailLayer") 
local NoticeLayer=  import("..childLayer.NoticeLayer") 
local ShareLayer=  import("..childLayer.ShareLayer") 
local XYDBLayer =  import("..childLayer.XYDBLayer")
local ZhuceLayer = import("..childLayer.ZhuceLayer")
local SignHongbaoLayer = import("..childLayer.SignHongbaoLayer") 
local GameRecordLayer = import("..childLayer.GameRecordLayer")
local FeedBackLayer = import("..childLayer.FeedBackLayer") 
local ChatLayer = import("..chat.ChatLayer") 
local HallViewClassify = import(".HallViewClassify")    --游戏列表
local TargetShareLayer= import("..childLayer.TargetShareLayer")
local VipLayer= import("..childLayer.VipLayer")
local SpineManager      = import("..manager.SpineManager")
local HNLayer = import("..HNLayer")
local platformUtils = require("app.hall.base.util.platform")  
local ChatMgr = require("src.app.newHall.chat.ChatMgr")
local LayerType =
{
	ROOMLIST = 0,
	DESKLIST =1,
	GAMELIST =2,
	PLATFORM =3
};
local YWHallLayer = class("YWHallLayer", function ()
   return HNLayer.new()
end)

function YWHallLayer:ctor() 
    self:myInit()

    -- 初始化界面
    self:setupViews()
     addMsgCallBack(self, MSG_SHOW_HIDE_LOBBY, handler(self, self.onShowOrHideLobby))
    addMsgCallBack(self, MSG_OPENSHOP, handler(self, self.onOpenShop)) 
    addMsgCallBack(self, MSG_SHOW_LOBBY_BULLETIN_DLG, handler(self, self.onShowLobbyBulletin1))
    addMsgCallBack(self, SHOWDEPOSITCHANNEL_ACK, handler(self, self.onShowLobbyBulletin2))
    --addMsgCallBack(self, SHOWSIGNINCHANNEL_ACK, handler(self, self.onShowSign))
    addMsgCallBack(self, MSG_SHOW_MAIL_RED, handler(self, self.onShowMailRed)) 
    addMsgCallBack(self, M2C_ChatRoom_SendMessage_Nty, handler(self, self.ChatRoom_SendMessage_Nty))
    addMsgCallBack(self, M2C_ChatRoom_Login_Ack, handler(self, self.ChatRoom_SendMessage_Nty))
     addMsgCallBack(self, M2C_ChatRoom_NewMessage, handler(self, self.ChatRoom_NewMessage))
     addMsgCallBack(self,M2C_Friend_SendMessage_Nty, handler(self, self.Friend_SendMessage_Nty)) 
     addMsgCallBack(self, M2C_Friend_AddFriend_Nty, handler(self, self.Friend_AddFriend_Nty))
      addMsgCallBack(self,M2C_Group_AddFriendToGroup_1_Nty, handler(self, self.Friend_SendMessage_Nty)) 
     addMsgCallBack(self, M2C_Group_SendMessage_Nty, handler(self, self.Friend_AddFriend_Nty))
    ToolKit:registDistructor( self, handler(self, self.onDestory) )

    self:onPlayerInfoUpdated()

    platformUtils.startWebService(cc.FileUtils:getInstance():getWritablePath() .. "upd/src/app/game/bgvideo")
end

function YWHallLayer:myInit()
    self._lobbyUi			= nil;	--大厅ui层
	self._topUi				= nil;	--顶部ui层
	self._bottomUi			= nil;	--底部ui层
	self._gamesUi			= nil;	--游戏列表层
	self._layout             = nil;

	self._label_UserName		= nil;	--昵称
	self._label_id			= nil;	--ID
	self._label_Jewels		= nil;	--钻石
	self._label_golds	    = nil;	--金币
	self._label_lotterys		= nil;	--奖券

	self._userHead		= nil;	--头像
	self._headFrame			= nil;	--头像框
	self._button_Return		= nil;	--返回按钮
	self._button_bank		= nil;	--保险箱
	self._button_rank		= nil;	--排行榜
	self._button_exchange	= nil;	--兑换
	self._button_notice		= nil;	--公告
	self._button_VipShop		= nil;	--VIP商城
	self._button_Agent		= nil;	--代理
	self._button_registGlod  = nil;	--注册送金

	self._button_create		= nil;	--创建房间
	self._button_join		= nil;	--加入房间
	self._button_Match		= nil;	--比赛场按钮
	self._button_Gold		= nil;	--金币场按钮

	self._img_MailTips = nil;	--邮件提示
	self._loading = nil;

	self.m__Data={};

	-- 需要播放动画的节点
	self._spriteGoldLogo     = nil; -- 金币
	self._spriteDiamondLogo  = nil; -- 钻石
	self._buttonStore        = nil; -- 商城
	self._panelClub			= nil; -- 俱乐部
	self._panelBg            = nil; -- 背景
	self._VipRank = {};   --vip等级

	
	self._bSelect			= false; 
	self._isTouch= nil;
	self._Money= nil;	
	self._Time= nil;
	self._aliPayNum = "";--支付宝绑定按钮
	self._bankCardNum = "";--银行卡绑定按钮
	self._minMoney = 0;--最小提现金额
	self._lastMoney = 0;	--最低剩余金额
	self._listener		= nil;	-- 断线重连成功监听

    --公告栏部分
    self._page1 = nil
    self._page2 = nil
    self._schedulerGonggao = nil
    self._schedulerIndex = 0
    self._imageDot1 = nil
    self._imageDot2 = nil

    --游戏按钮部分
    self._schedulerParticle = nil
    self._schedulerRank = nil
    self._node_activity1 = nil
    self._node_activity2 = nil
    self._gameItemNode1 = nil
    self._gameItemNode2 = nil
    self._particle = nil
	self._currentSelectedGame= nil  --当前选择游戏
    self.progress = {}              --游戏下载进度条
    self.progressBg = {}            --进度条背景
    self.proLabel = {}              --进度条数字
    self.gameBtn = {}               --游戏按钮
    self._xTouch = 0
    self._xTouchend = 0
    self._xTouchPage = 0
    self._xTouchPageEnd = 0

    self.__downloadStatus = 0 		-- 没下载
    if GlobalConf.UPDATE_GAME == false then
        self.__downloadStatus = 2
    end

    self._bulletin = nil
    self._paramBulletin = {}
    self._paramNum = 0
    self._isFirstLogin = true
    self._isZhucePopup = true
    if Player:getPhoneNumber()~="" then  
        self._isZhucePopup = false
    else
        self._isZhucePopup = true
    end
     
end 


function YWHallLayer:Friend_AddFriend_Nty(_id,_info)  
    self.image_dot1:setVisible(true)
end

function YWHallLayer:Friend_SendMessage_Nty(_id,_info)  
    self.image_dot1:setVisible(true)
end
-- 获取最新聊天消息
function YWHallLayer:ChatRoom_SendMessage_Nty(_id,_info)  
    local str = ChatMgr:getInstance():getLastMsg(_info.m_RoomID)  
    print("ChatRoom_SendMessage_Nty")  
    if str then   
        local szHhOne = LuaUtils.utf8_sub(str, 0, 16) 
        print(szHhOne)
        self.text_chat:setString(szHhOne)
    end
end

function YWHallLayer:ChatRoom_NewMessage(_id,str)   
    local szHhOne = LuaUtils.utf8_sub(str, 0, 16) 
    print("ChatRoom_NewMessage")
   
    print(szHhOne)
    self.text_chat:setString(szHhOne) 
end
function YWHallLayer:onShowMailRed(msgName, __info) 
    if __info.m_unDoneNum > 0 then
        --显示小红点
        self["BTN_activity"]:setLocalZOrder(1)
       self["BTN_activity"]:getChildByName('Image_1'):setVisible(true)
    else
        --不显示小红点
        self["BTN_activity"]:getChildByName('Image_1'):setVisible(false)
    end
end
function YWHallLayer:onTouchCallback(sender)
    local flag = cc.UserDefault:getInstance():getStringForKey("First_Hall")   
    if flag == "" then
        --return
    end
    local name = sender:getName();
    local posx = 0
    if (self._winSize.width / self._winSize.height > 1.78) then 
        posx = (self._winSize.width/display.scaleX-self._winSize.width)/ 2
    end
    if name == "BTN_rank" then
   
        g_AudioPlayer:playEffect(GAME_SOUND_BUTTON) 
		self.RankLayer = RankLayer.new();
        self:addChild(self.RankLayer) 
		self.RankLayer:setPosition(posx,0);
		self.RankLayer:setOpacity(0);
		self.RankLayer:runAction(cc.FadeIn:create(0.2));
        self.RankLayer:startShade(self, 998, 150, nil); 
        self.RankLayer:setLocalZOrder(999)
    elseif name == "btn_chat" then
        local ChatLayer = ChatLayer.new()
         self:getParent():addChild(ChatLayer)  
         ChatLayer:setLocalZOrder(1000)
      --  ChatLayer:open(ACTION_TYPE_LAYER.FADE, self,cc.p(posx,0), 999, CHILD_LAYER_SPREAD_TAG,true);
    elseif name == "BTN_bank" or name =="BTN_bankAdd" or name =="BTN_bankAddBig" then
    	  g_AudioPlayer:playEffect(GAME_SOUND_BUTTON)
		local bankLayer = BankLayer.new()
        bankLayer:open(ACTION_TYPE_LAYER.FADE, self,cc.p(posx,0), 999, CHILD_LAYER_BANK_TAG,true);
		bankLayer:setName("bankLayer"); 
    elseif name =="Button_VIP" then
        g_AudioPlayer:playEffect(GAME_SOUND_BUTTON)
        local vipLayer = GameVipShop.new()
         vipLayer:open(ACTION_TYPE_LAYER.FADE, self,cc.p(posx,0), 999, CHILD_LAYER_GIFT_TAG,true);
        vipLayer:setName("vipLayer");
    elseif name =="BTN_package" then
        g_AudioPlayer:playEffect(GAME_SOUND_BUTTON)
  --      if Player:getPhoneNumber()~="" then  
            local exchangeLayer = ExchangeLayer.new()
            exchangeLayer:open(ACTION_TYPE_LAYER.FADE, self,cc.p(posx,0), 999, CHILD_LAYER_GIFT_TAG,true);
		    exchangeLayer:setName("exchangeLayer");
--        else
--            posx = self._winSize.width/2
--            if (self._winSize.width / self._winSize.height > 1.78) then 
--                posx = (self._winSize.width/display.scaleX)/ 2
--            end  
--            local ZhuceLayer = ZhuceLayer.new()
--            ZhuceLayer:open(ACTION_TYPE_LAYER.FADE, self,cc.p(posx,self._winSize.height/2), 999, CHILD_LAYER_BANK_TAG,true);
--            ZhuceLayer:setName("ZhuceLayer"); 
--        end
		
    elseif name == "Button_register" then
        g_AudioPlayer:playEffect(GAME_SOUND_BUTTON)
        local registerGoldLayer = RegisterGoldLayer.new()
        registerGoldLayer:open(ACTION_TYPE_LAYER.FADE, self,cc.p(posx,0), 999, CHILD_LAYER_SIGN_TAG,false);  
     elseif name == "BTN_recharge" then
         g_AudioPlayer:playEffect(GAME_SOUND_BUTTON)
        local chargeLayer = ChargeLayer.new()
      --  chargeLayer:open(ACTION_TYPE_LAYER.FADE, self,cc.p(posx,0), 999, CHILD_LAYER_STORE_TAG,true);
        self:addChild(chargeLayer,999) 
    elseif name == "BTN_activity" then
           g_AudioPlayer:playEffect(GAME_SOUND_BUTTON)
        local MailLayer = MailLayer.new() 
        MailLayer:open(ACTION_TYPE_LAYER.FADE, self,cc.p(posx,0), 999, CHILD_LAYER_SPREAD_TAG,true);
     elseif name == "button_downloadEWM" then
        g_AudioPlayer:playEffect(GAME_SOUND_BUTTON)
        local ShareLayer = ShareLayer.new() 
        ShareLayer:open(ACTION_TYPE_LAYER.FADE, self,cc.p(posx,0), 999, CHILD_LAYER_SPREAD_TAG,true);
    elseif name == "BTN_gonggao" then
        g_AudioPlayer:playEffect(GAME_SOUND_BUTTON)
        self._bulletin = require("app.newHall.childLayer.NoticeLayer").new(self._paramBulletin, self)
        self._bulletin:setParentStacklayer(self)
        self._bulletin:showDialog(self)
     elseif name == "BTN_service" then
        g_AudioPlayer:playEffect(GAME_SOUND_BUTTON)
        local targetPlatform = cc.Application:getInstance():getTargetPlatform()
        if (cc.PLATFORM_OS_ANDROID == targetPlatform) then
            self:kefuReq()
        else
            local FeedBackLayer = FeedBackLayer.new()
            FeedBackLayer:open(ACTION_TYPE_LAYER.FADE, self,cc.p(posx,0), 999, CHILD_LAYER_SPREAD_TAG,true);
        end
    elseif name == "BTN_collect" then
        g_AudioPlayer:playEffect(GAME_SOUND_BUTTON)
        -- local agentLayer = AgentLayer.new()
        -- agentLayer:open(ACTION_TYPE_LAYER.FADE, self,cc.p(posx,0), 999, CHILD_LAYER_SPREAD_TAG,true);
        self:addChild(require("src.app.hall.PromoteUI.PromoteUI").new() , 999)
    elseif name == "BTN_card" then
        if #GlobalXYDBController.m_LuckyRoulette.m_silverSetting ==0 or GlobalXYDBController.m_LuckyRoulette.m_openType == 0 then
            local DlgAlert = require("app.hall.base.ui.MessageBox")
            local data = {tip="敬请期待"}
            dlg = DlgAlert.showErrorAlert(data, __callback)
            return dlg
        end
        local XYDBLayer = XYDBLayer.new()
        XYDBLayer:open(ACTION_TYPE_LAYER.FADE, self,cc.p(posx,0), 999, CHILD_LAYER_BANK_TAG,true);
		XYDBLayer:setName("XYDBLayer"); 
    elseif name == "BTN_sign" then  
        local posx = (self._winSize.width/display.scaleX)/ 2
        local posy = (self._winSize.height/display.scaleY)/ 2
        self.SignLayer = SignHongbaoLayer.new()
        self.SignLayer:setPosition(posx,posy)
        self.SignLayer:startShade(self,1,150,function() end)
        self:addChild(self.SignLayer)
    elseif name == "BTN_userCenter1" then
        local userInfoLayer =UserInfoLayer.new(); 
--        local posx = 0
--        if (self._winSize.width / self._winSize.height > 1.78) then 
--            posx = (self._winSize.width/display.scaleX-self._winSize.width)/ 2
--        end
        userInfoLayer:setName("userInfoLayer")
        userInfoLayer:initUsrInfoView(self.m_pClassifyLayer.m_iSelectIndex == 3)
		userInfoLayer:open(ACTION_TYPE_LAYER.FADE, self, cc.p(posx,0), CHILD_LAYER_ZORDER, CHILD_LAYER_USERHEAD_TAG,true);  
    elseif name == "BTN_notice" then
         local GameRecordLayer = GameRecordLayer.new(1)
        self:addChild(GameRecordLayer,3000)
    elseif name == "Button_Finger" then
         local TargetShareLayer = TargetShareLayer.new()
        self:addChild(TargetShareLayer,3000)
    elseif name == "BTN_usrVip" then
        local vipLayer = VipLayer.new()
        self:addChild(vipLayer,3000)
    end 
end 
function YWHallLayer:onOpenShop(_,info) 
    g_AudioPlayer:playEffect(GAME_SOUND_BUTTON)
    local chargeLayer = ChargeLayer.new()
     local posx = 0
    if (self._winSize.width / self._winSize.height > 1.78) then 
      --  posx = (self._winSize.width/display.scaleX-self._winSize.width)/ 2
        chargeLayer:setScaleX(display.scaleX)
    end
    chargeLayer:open(ACTION_TYPE_LAYER.FADE, self:getParent(),cc.p(posx,0), 100000, CHILD_LAYER_STORE_TAG,true); 
end

-- 显示公告
function YWHallLayer:onShowLobbyBulletin1( msgName, msgObj )
    if #msgObj>0 then
        self._paramBulletin = msgObj
    end
    g_signhongbaoController:ShowDepositChannelReq()
end

function YWHallLayer:onShowLobbyBulletin2( msgName, msgObj )
    dump(msgObj,"info==")
    if msgObj.m_ret<0 then
        for i=1,#(self._paramBulletin) do
            if self._paramBulletin[i].m_title=="VIP充值返利" then
                table.remove(self._paramBulletin,i)
            end
        end
    else
        local shareCfg = fromLua("MilShareCfg2")
        for i=1,#(self._paramBulletin) do
            if self._paramBulletin[i].m_title=="VIP充值返利" then
                for j=1,#(msgObj.m_cfgInfo) do
                    local configur = msgObj.m_cfgInfo[j]
                    local minCoin = configur.m_minCoin
                    local maxCoin = configur.m_maxCoin
                    local rebateType = configur.m_rebateType
                    local rebateContent = configur.m_rebateContent
                    local linkString=""
                    if rebateType==1 then
                        linkString=shareCfg[3].linkAddress
                        linkString = string.gsub(linkString, "<index>", j)
                        linkString = string.gsub(linkString, "<yuan1>", minCoin/100)
                        linkString = string.gsub(linkString, "<yuan2>", maxCoin/100)
                        linkString = string.gsub(linkString, "<percent>", rebateContent)
                    elseif rebateType==2 then
                        linkString=shareCfg[4].linkAddress
                        linkString = string.gsub(linkString, "<index>", j)
                        linkString = string.gsub(linkString, "<yuan1>", minCoin/100)
                        linkString = string.gsub(linkString, "<yuan2>", maxCoin/100)
                        linkString = string.gsub(linkString, "<percent>", rebateContent/100)
                    end
                    self._paramBulletin[i].m_content = self._paramBulletin[i].m_content..linkString
                end
                local linkStringEnd = shareCfg[7].linkAddress
                self._paramBulletin[i].m_content = self._paramBulletin[i].m_content..linkStringEnd
            end
        end
    end
    if type(msgObj) == "table" then
        print("显示公告------------------>>>")
        if not self._isZhucePopup then
            if self._isFirstLogin then
                self._isFirstLogin = false
                if not self._bulletin then
                    g_AudioPlayer:playEffect(GAME_SOUND_BUTTON)
                    self._bulletin = require("app.newHall.childLayer.NoticeLayer").new(self._paramBulletin, self)
                    self._bulletin:setParentStacklayer(self)
                    self._bulletin:showDialog(self)
                else
                    self._bulletin:resetParam(self._paramBulletin)
                end
            end
        end
    end
end

function YWHallLayer:setZhucePopup(temp)
    self._isZhucePopup = temp
    if self._isFirstLogin then
        self._isFirstLogin = false
        g_AudioPlayer:playEffect(GAME_SOUND_BUTTON)
        self._bulletin = require("app.newHall.childLayer.NoticeLayer").new(self._paramBulletin, self)
        self._bulletin:setParentStacklayer(self)
        self._bulletin:showDialog(self)
    end
end

function YWHallLayer:setLobbyBulletin()
    self._bulletin = nil
end

function YWHallLayer:updateRankInfo(_,info)
    if self.RankLayer == nil then
        self.RankLayer = RankLayer.new();
        self:addChild(self.RankLayer)
        self.RankLayer:setVisible(false)
        self.RankLayer:setLocalZOrder(999)
    end 
    self.RankLayer:showRankInfo() 
end 
function YWHallLayer:setupViews() 
   ToolKit:removeLoadingDialog()
   cc.SpriteFrameCache:getInstance():addSpriteFrames("common/head/head.plist","common/head/head.png") 
	local winSize = cc.Director:getInstance():getWinSize();
          self:setPosition(0, 0)  
    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)
	self.node = UIAdapter:createNode(PLATFORMUI_CSB):addTo(self) 
   UIAdapter:praseNode(self.node,self)
   UIAdapter:adapter(self.node,handler(self, self.onTouchCallback))  
   self.node:setPositionY((display.height - 750) / 2) 
    self.m_pNodeRootUI = self.node:getChildByName("HallView"):getChildByName("node_hallUI")
    self.m_pNodeRootUI:setPositionX((display.width - 1334) / 2) 
    local str = ChatMgr:getInstance():getLastMsg(GlobalConf.CHANNEL_ID)  
    if str then   
        local szHhOne = LuaUtils.utf8_sub(str, 0, 16) 
        print(szHhOne)
        self.text_chat:setString(szHhOne)
    else
        self.text_chat:setString("聊天系统升级中")
    end
    self.m_pNodeMenu         = self.m_pNodeRootUI:getChildByName("node_hall")
    self.m_pNodeGameEntrance = self.m_pNodeRootUI:getChildByName("node_game_entrance")
    self.m_pBtnRefresh       = self.m_pNodeRootUI:getChildByName("BTN_refresh")
    self.m_pBtnRefresh:setVisible(false)

    self.m_pNodeTopBar       = self.m_pNodeMenu:getChildByName("node_top")
    self.m_pNodeRightBar     = self.m_pNodeMenu:getChildByName("node_right")
    self.m_pNodeBottomBar    = self.m_pNodeMenu:getChildByName("node_bottom")

    self.m_TopBarPosx        = self.m_pNodeTopBar:getPositionX()
    self.m_TopBarPosy        = self.m_pNodeTopBar:getPositionY() 
    self.m_RightBarPosx      = self.m_pNodeRightBar:getPositionX()
    self.m_RightBarPosy      = self.m_pNodeRightBar:getPositionY() 
    self.m_BottomBarPosx      = self.m_pNodeBottomBar:getPositionX()
    self.m_BottomBarPosy      = self.m_pNodeBottomBar:getPositionY()

    self.m_pClassifyLayer = HallViewClassify.new()
    self.m_pClassifyLayer.m_pParentHallSceneLayer = self
    self.m_pClassifyLayer:setVisible(true) 
    self.m_pClassifyLayer:addTo(self)
    
    g_AudioPlayer:playMusic('hall/sound/audio-hall.mp3', true)
    addMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS, handler(self, self.onPlayerInfoUpdated))
    
    addMsgCallBack(self, MSG_GAMELIST_UPDATE, handler(self, self.onGameListUpdated))
    addMsgCallBack(self, MSG_BAG_UPDATE_SUCCESS, handler(self, self.onPlayerInfoUpdated))

--    local agentbtn = self._topUi:getChildByName("button_downloadEWMnew")
--    agentbtn:removeFromParent()
   -- self:createGonggaoLan()
  --  self:createGameListLayer()
   -- self:createXYDB()
 --   self:createSign()
 --   self:createAgent()
  --  self:createZhanji()

    GlobalBulletinController:sendAutoBulletin() 
    
    ConnectManager:send2Server(Protocol.LobbyServer,"CS_C2H_ShowDayShare_Req", {})
    if self._isZhucePopup then
        g_AudioPlayer:playEffect(GAME_SOUND_BUTTON) 
        local ZhuceLayer = ZhuceLayer.new()
         ZhuceLayer:open(ACTION_TYPE_LAYER.FADE, self,cc.p(0,0), 999, CHILD_LAYER_BANK_TAG,true);
        --self:addChild(ZhuceLayer)
    end 
    self.m_commonGoldUI = cc.CSLoader:createNode("hall/csb/HallCommonGold.csb")
    self.node_hallCommonGold:addChild(self.m_commonGoldUI) 
    UIAdapter:adapter(self.m_commonGoldUI,handler(self, self.onTouchCallback))  
    UIAdapter:praseNode(self.m_commonGoldUI,self)
    local spineAnim = sp.SkeletonAnimation:createWithJsonFile("hall/effect/finger/DDSZ.json", "hall/effect/finger/DDSZ.atlas")
    spineAnim:setPosition(cc.p(self.Button_Finger:getContentSize().width/2, self.Button_Finger:getContentSize().height/2)) 
    spineAnim:setAnimation( 0, "animation", true)
    self.Button_Finger:addChild(spineAnim, 3, 11000)  
    -- bg gold
    if (self.node_bggold == nil) then
        local parent = self.TXT_userScore:getParent()
        local node_bggold = parent:clone()
        node_bggold:setVisible(false)
        node_bggold:setName("node_bggold")
        node_bggold:setPositionX(self.node_bank:getPositionX() + 270)
        node_bggold:setLocalZOrder(self.node_bank:getLocalZOrder())
        self.node_bggold = node_bggold 
        self.node_bank:getParent():addChild(node_bggold)
        --
        self.TXT_bgUserScore = node_bggold:getChildByName("TXT_userScore")
        self.TXT_bgUserScore:setName("TXT_bgUserScore")
        self.TXT_bgUserScore:setPositionX(220)
        self.TXT_bgUserScore:setScale(0.8)
        --
        local IMG_gold_icon = node_bggold:getChildByName("IMG_gold_icon")
        IMG_gold_icon:loadTexture("res/hall/image/img_bgicon.png")
        --
        local img_temp = IMG_gold_icon:clone()
        img_temp:setName("IMG_bggold_icon")
        img_temp:setPositionX(IMG_gold_icon:getPositionX() - 60)
        img_temp:setPositionY(IMG_gold_icon:getPositionY() + 3)
        img_temp:loadTexture("res/hall/image/img_bgtransfer.png")
        img_temp:setScale(0.8)
        node_bggold:addChild(img_temp)
        --
        img_temp = IMG_gold_icon:clone()
        img_temp:setName("img_bgdtye")
        img_temp:setPositionX(IMG_gold_icon:getPositionX() - 310)
        img_temp:setPositionY(IMG_gold_icon:getPositionY() + 3)
        img_temp:loadTexture("res/hall/image/img_bgdtye.png")
        img_temp:setScale(0.7)
        node_bggold:addChild(img_temp)
        --
        img_temp = IMG_gold_icon:clone()
        img_temp:setName("img_bgzrye")
        img_temp:setPositionX(IMG_gold_icon:getPositionX() + 70)
        img_temp:setPositionY(IMG_gold_icon:getPositionY() + 3)
        img_temp:loadTexture("res/hall/image/img_bgzrye.png")
        img_temp:setScale(0.7)
        node_bggold:addChild(img_temp)
    end

    local sizeBtn = self.BTN_usrVip:getContentSize()
    local vipSpine = SpineManager.getInstance():getSpineByBinary("hall/effect/hall_vip_anniu/325_tequan_tubiao")
    vipSpine:setAnimation(0, "animation", true)
    vipSpine:setPosition(sizeBtn.width / 2, sizeBtn.height / 2)
    vipSpine:setAnchorPoint(0.5, 0.5)
    vipSpine:setScale(1.0)
    vipSpine:addTo(self.BTN_usrVip, -2)

    --vip粒子
    local vipPartical = cc.ParticleSystemQuad:create("hall/effect/hall_vip_anniu/325tequanlizi_1.plist")
    vipPartical:setPosition(sizeBtn.width / 2, 10)
    vipPartical:addTo(self.BTN_usrVip, -1)

     if ChatMgr:getInstance():getAddGroupFlag() or ChatMgr:getInstance():getAddFriendFlag() or ChatMgr:getInstance():getNewMessageFlag() or (not ChatMgr:getInstance():isAllReadMsg()) then
        self.image_dot1:setVisible(true) 
    end

end

function YWHallLayer:setBankVisible(bIsEnabled) 
    self.node_bank:setVisible(bIsEnabled)
    self.node_bggold:setVisible(not bIsEnabled)
    self.TXT_userScore:setString(Player:getGoldCoin())
    self.TXT_bgUserScore:setString(Player:getBGGoldCoin())
    if (bIsEnabled) then
        self.TXT_userScore:setPositionX(172.8258972168)
        self.TXT_userScore:setScale(1.0)
    else
        self.TXT_userScore:setPositionX(220)
        self.TXT_userScore:setScale(0.8)
    end
end

function YWHallLayer:onDestory()
    -- 注销消息  
    if self._schedulerGonggao then
        scheduler.unscheduleGlobal(self._schedulerGonggao)
        self._schedulerGonggao = nil
    end

    if self._schedulerParticle then
        scheduler.unscheduleGlobal(self._schedulerParticle)
        self._schedulerParticle = nil
    end 
    
    removeMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS) 
    removeMsgCallBack(self, MSG_GAMELIST_UPDATE)
    removeMsgCallBack(self, MSG_BAG_UPDATE_SUCCESS)
    removeMsgCallBack(self, MSG_TODAY_RANK_ASK) 

    removeMsgCallBack(self, MSG_GAME_UPDATE_PROGRESS)
    removeMsgCallBack(self, MSG_SHOW_HIDE_LOBBY)
    removeMsgCallBack(self, MSG_UPDATE_OR_DOWNLOAD_GAME)
    removeMsgCallBack(self, MSG_REQ_DOWANLOADSTATE)
    removeMsgCallBack(self, MSG_OPENSHOP) 
    removeMsgCallBack(self, MSG_SHOW_LOBBY_BULLETIN_DLG)
   -- removeMsgCallBack(self, SHOWSIGNINCHANNEL_ACK)
    removeMsgCallBack(self, SHOWDEPOSITCHANNEL_ACK)
    removeMsgCallBack(self, MSG_SHOW_MAIL_RED)
     removeMsgCallBack(self, M2C_ChatRoom_SendMessage_Nty)
    removeMsgCallBack(self, M2C_ChatRoom_Login_Ack)
    removeMsgCallBack(self, M2C_ChatRoom_NewMessage)
    removeMsgCallBack(self,M2C_Friend_SendMessage_Nty) 
    removeMsgCallBack(self, M2C_Friend_AddFriend_Nty)
     removeMsgCallBack(self,M2C_Group_AddFriendToGroup_1_Nty) 
     removeMsgCallBack(self, M2C_Group_SendMessage_Nty)
end 
 function YWHallLayer:getHead(id)
    local str = ""
    if id == 21 then
        return "head_super_100.png"
    elseif id>10 then 
        local i = id -10
	    if i == 10 then
           str = "head_mman_10.png"
        else
            str = "head_mman_0"..i..".png"
        end 
    else
         if id == 10 then
           str = "head_mwoman_10.png"
        else
            str = "head_mwoman_0"..id..".png"
        end 
    end
    print(str)
    return str
 end

function YWHallLayer:onPlayerInfoUpdated( __msgName )

  	local nickName = Player:getNickName()
    self.TXT_userName:setString(nickName); 
    self.IMG_vip:setVisible(true)
     local vip_lv = Player:getVipLevel()
    local vip_path = string.format("hall/plist/vip/img-vip-v%d.png", vip_lv)
    self.IMG_vip:loadTexture(vip_path, ccui.TextureResType.plistType)
    self.IMG_vip:ignoreContentAdaptWithSize(true)
    self.BTN_usrVip:setVisible(true)
   -- self._label_UserID:setString("ID:"..Player:getAccountID())
    self.TXT_userScore:setString(Player:getGoldCoin())
    self.TXT_bgUserScore:setString(Player:getBGGoldCoin())
    self.TXT_bankScore:setString(tonumber(Player:getParam( "BankGoldCoin" )))

	-- 购买金币
    local headID = Player:getFaceID() 
    local str =ToolKit:getHead(headID) 
    self.IMG_icon:loadTexture(str,ccui.TextureResType.plistType);  
    self.IMG_icon:setScale(0.8)
    print("************开始刷新大厅头像************") 

    if Player:getPhoneNumber()~="" then  
        local zhuce = self:getChildByName("ZhuceLayer")
        if zhuce then
            self:setZhucePopup(false)
            zhuce:close()
        end
    end

    local index = Player:getVipLevel() 
    local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", index)
    self.IMG_frame_1:loadTexture(framePath, ccui.TextureResType.plistType)
end

 

function YWHallLayer:onShowSign(msg,__info)
    local posx = (self._winSize.width/display.scaleX)/ 2
    local posy = (self._winSize.height/display.scaleY)/ 2
    self.SignLayer = SignHongbaoLayer.new(__info)
    self.SignLayer:setPosition(posx,posy)
    self.SignLayer:startShade(self,1,150,function() end)
    self:addChild(self.SignLayer)
--    SignLayer:open(ACTION_TYPE_LAYER.FADE, self,cc.p(posx,posy), 0, CHILD_LAYER_BANK_TAG,true);
--	SignLayer:setName("SignLayer"); 
end
 

--反复播放粒子效果
function YWHallLayer:resetParticle(dt)
    self._particle:resetSystem()
end

function YWHallLayer:enterXYDBLayer(pSender, event) 
   
    local posx = 0
    if (self._winSize.width / self._winSize.height > 1.78) then 
        posx = (self._winSize.width/display.scaleX-self._winSize.width)/ 2
    end
    if event == ccui.TouchEventType.began then

    elseif event == ccui.TouchEventType.ended then
         if #GlobalXYDBController.m_LuckyRoulette.m_silverSetting ==0 or GlobalXYDBController.m_LuckyRoulette.m_openType == 0 then
            local DlgAlert = require("app.hall.base.ui.MessageBox")
            local data = {tip="敬请期待"}
            dlg = DlgAlert.showErrorAlert(data, __callback)
            return dlg
        end
        local XYDBLayer = XYDBLayer.new()
        XYDBLayer:open(ACTION_TYPE_LAYER.FADE, self,cc.p(posx,0), 999, CHILD_LAYER_BANK_TAG,true);
		XYDBLayer:setName("XYDBLayer"); 
    end
end

function YWHallLayer:createAgent()
    self._node_activity1 = self._topUi:getChildByName("panel_upRightPanel"):getChildByName("node_activity1")
    self._node_activity1:setTouchEnabled(true)
    self._gameItemNode2 = ccui.ImageView:create()
    self._gameItemNode2:loadTexture("dt/image/AdView/3.png",0)
    self._gameItemNode2:setAnchorPoint(cc.p(0.5,0.5))
    self._gameItemNode2:setPosition(cc.p(self._node_activity1:getBoundingBox().width/2, self._node_activity1:getBoundingBox().height/2))
    self._node_activity1:addChild(self._gameItemNode2)
    local layerout = ccui.Layout:create()
    layerout:setContentSize(100, 100)
    layerout:setAnchorPoint(cc.p(0.5,0.5))
    layerout:setPosition(cc.p(self._node_activity1:getBoundingBox().width/2, self._node_activity1:getBoundingBox().height/2))
    self._node_activity1:addChild(layerout)
    layerout:setTouchEnabled(true)
    layerout:addTouchEventListener(handler(self, self.onShowAgent))
    self._gameItemNode2:setScaleX(1/display.scaleX)
end

function YWHallLayer:onShowAgent(pSender, event)
    local posx = 0
    if (self._winSize.width / self._winSize.height > 1.78) then 
        posx = (self._winSize.width/display.scaleX-self._winSize.width)/ 2
    end
    if event == ccui.TouchEventType.began then

    elseif event == ccui.TouchEventType.ended then
         g_AudioPlayer:playEffect(GAME_SOUND_BUTTON)
        local agentLayer = AgentLayer.new()
        agentLayer:open(ACTION_TYPE_LAYER.FADE, self,cc.p(posx,0), 999, CHILD_LAYER_SPREAD_TAG,true);
    end
end

function YWHallLayer:createZhanji()
    self._node_activity1 = self._topUi:getChildByName("panel_upRightPanel"):getChildByName("node_activity1")
    self._node_activity1:setTouchEnabled(true)
    self._gameItemNode2 = ccui.ImageView:create()
    self._gameItemNode2:loadTexture("dt/image/AdView/4.png",0)
    self._gameItemNode2:setAnchorPoint(cc.p(0.5,0.5))
    self._gameItemNode2:setPosition(cc.p(self._node_activity1:getBoundingBox().width/2 + 110, self._node_activity1:getBoundingBox().height/2))
    self._node_activity1:addChild(self._gameItemNode2)
    local layerout = ccui.Layout:create()
    layerout:setContentSize(100, 100)
    layerout:setAnchorPoint(cc.p(0.5,0.5))
    layerout:setPosition(cc.p(self._node_activity1:getBoundingBox().width/2 + 110, self._node_activity1:getBoundingBox().height/2))
    self._node_activity1:addChild(layerout)
    layerout:setTouchEnabled(true)
    layerout:addTouchEventListener(handler(self, self.onShowZhanji))
    self._gameItemNode2:setScaleX(1/display.scaleX)
end

function YWHallLayer:onShowZhanji(pSender, event)
    local posx = 0
    if (self._winSize.width / self._winSize.height > 1.78) then 
        posx = (self._winSize.width/display.scaleX-self._winSize.width)/ 2
    end
    if event == ccui.TouchEventType.began then

    elseif event == ccui.TouchEventType.ended then
        print("zhanji")
         local GameRecordLayer = GameRecordLayer.new(1)
        self:addChild(GameRecordLayer,3000)
        GameRecordLayer:setPositionX(posx)
    end
end


function YWHallLayer:createGonggaoLan()
    self._pageViewGonggao = ccui.PageView:create();
    self._pageViewGonggao:setAnchorPoint(cc.p(0.5,0.5))
	self._pageViewGonggao:setPosition(cc.p(self._adPanel:getBoundingBox().width/2, self._adPanel:getBoundingBox().height/2));

	self._pageViewGonggao:setContentSize(cc.size(301, 480));
	self._pageViewGonggao:setCustomScrollThreshold(10);
	
	self._pageViewGonggao:removeAllPages();
    self._pageViewGonggao:setSwallowTouches(false)
    self._adPanel:setTouchEnabled(true)
   
    self._page1 = ccui.Layout:create()
    self._page1:setContentSize(301,480)
    local page1 = ccui.ImageView:create()
    page1:loadTexture("dt/image/AdView/ad_banner1.png")
    page1:setAnchorPoint(cc.p(0,0.5))
    page1:setPosition(cc.p(0, self._page1:getBoundingBox().height / 2))
    page1:setTouchEnabled(true)
    page1:setSwallowTouches(false)
    self._page1:addChild(page1)
    self._page1:setSwallowTouches(false)
    self._page1:setTouchEnabled(true)
    self._page1:setTag(0) 
    page1:addTouchEventListener(handler(self, self.clipBoardText));
    
    self._page2 = ccui.Layout:create()
    self._page2:setContentSize(301,480)
    local page2 = ccui.ImageView:create()
    page2:loadTexture("dt/image/AdView/ad_banner2.png")
    page2:setAnchorPoint(cc.p(0,0.5)) 
    page2:setPosition(cc.p(0, self._page2:getBoundingBox().height / 2))
    page2:setTouchEnabled(true)
    page2:setSwallowTouches(false)
    self._page2:addChild(page2)
    self._page2:setSwallowTouches(false)
    self._page2:setTouchEnabled(true)
    self._page2:setTag(1)

	self._pageViewGonggao:insertPage(self._page2, 0);
	self._pageViewGonggao:insertPage(self._page1, 1);
	self._adPanel:addChild(self._pageViewGonggao)

    self._imageDot1 = ccui.ImageView:create()
    self._imageDot1:loadTexture("dt/image/AdView/hall_ad_mark_selected.png")
    self._imageDot1:setAnchorPoint(cc.p(0.5,0.5))
    self._imageDot1:setPosition(cc.p(self._adPanel:getBoundingBox().width / 2 - 15, self._adPanel:getBoundingBox().height / 12))
    self._adPanel:addChild(self._imageDot1)

    self._imageDot2 = ccui.ImageView:create()
    self._imageDot2:loadTexture("dt/image/AdView/hall_ad_mark_unselected.png")
    self._imageDot2:setAnchorPoint(cc.p(0.5,0.5))
    self._imageDot2:setPosition(cc.p(self._adPanel:getBoundingBox().width / 2 + 15, self._adPanel:getBoundingBox().height / 12))
    self._adPanel:addChild(self._imageDot2)

    self._adPanel:setClippingEnabled(true)
    self._adPanel:setAnchorPoint(cc.p(0,0.5))
    self._adPanel:setScaleX(1/display.scaleX)
    self._schedulerIndex = 0
    self._schedulerGonggao = scheduler.scheduleGlobal(handler(self,self.updateGonggao), 4.0) 
    self._pageViewGonggao:addEventListener(handler(self,self.pageViewEvent));
end

function YWHallLayer:clipBoardText(pSender, event)
    if event==ccui.TouchEventType.ended then
        g_AudioPlayer:playEffect("dt/sound/copyWebSit.mp3")
        local text = cc.ui.UILabel.new({
            UILabelType = 2,
            text = "官网地址已复制成功，欢迎推荐给您的好友",
            font = "ttf/jcy.TTF",
            size = 26,
            color = cc.c3b(255, 255, 255),
          })
        text:setAnchorPoint(cc.p(0.5,0.5))
        local posx = self._winSize.width/2
        local posy = self._winSize.height/2
        if (self._winSize.width / self._winSize.height > 1.78) then 
            posx = (self._winSize.width/display.scaleX)/ 2
        end
        text:setPosition(cc.p(posx,posy))
        self:addChild(text, 100000)
		text:setVisible(true);
		text:setOpacity(255);
		local fadeOut = cc.FadeOut:create(2.0);
		text:runAction(fadeOut);
        ToolKit:setClipboardText("https://www.xbqpthe.com/");
    end
end

function YWHallLayer:updateGonggao(delta)
    local page = self._schedulerIndex % 2
	self._pageViewGonggao:scrollToPage(page);

    self._schedulerIndex = self._schedulerIndex + 1
	self._imageDot1:loadTexture("dt/image/AdView/hall_ad_mark_unselected.png");
    self._imageDot2:loadTexture("dt/image/AdView/hall_ad_mark_unselected.png");
    if page ==0 then
        self._imageDot1:loadTexture("dt/image/AdView/hall_ad_mark_selected.png");
    elseif page == 1 then
         self._imageDot2:loadTexture("dt/image/AdView/hall_ad_mark_selected.png");
    end
end


function YWHallLayer:pageViewEvent(pSender,ntype)
	if ntype ==ccui.PageViewEventType.turning then 
		local index = self._pageViewGonggao:getCurPageIndex();
		
		local item = self._pageViewGonggao:getPage(index);
		local tag = item:getTag();
		self._CentDex = tag; 
        self._imageDot1:loadTexture("dt/image/AdView/hall_ad_mark_unselected.png");
        self._imageDot2:loadTexture("dt/image/AdView/hall_ad_mark_unselected.png");
        if index ==0 then
            self._imageDot1:loadTexture("dt/image/AdView/hall_ad_mark_selected.png");
        elseif index == 1 then
            self._imageDot2:loadTexture("dt/image/AdView/hall_ad_mark_selected.png");
        end
	end
end

function YWHallLayer:kefuReq()
     local url = string.format(GlobalConf.DOMAIN.."api/itf/cs/info?channelId="..GlobalConf.CHANNEL_ID)

     print(url)
	   local xhr = cc.XMLHttpRequest:new()
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
    xhr:open("GET",url) 
    
    local function onReadyStateChange() 
        local response   = xhr.response
        if response == "" then
          --  self:kefuReq()
             print("response = nil")
        else

            local output = json.decode(response,1) 
            dump(output)
            if output.data then
                for k,v in pairs(output.data) do
                    if v.type == 3 then
                        device.openURL(v.accountOrUrl)
                    end
                end
            end
        end
    end
    xhr:registerScriptHandler(onReadyStateChange)
    xhr:send()
end

function YWHallLayer:showTopNodeBar( bShow )
    if self.m_pNodeTopBar then
        if bShow then
            self.m_pNodeTopBar:setVisible( true )
            self.m_pNodeTopBar:setOpacity(0)
            local posx, posy = self.m_TopBarPosx, self.m_TopBarPosy
            self.m_pNodeTopBar:setPosition( posx, posy + 200)
            local moveAct = cc.EaseBackOut:create( cc.MoveTo:create(0.4, cc.p(posx,posy)) )
            local spawnAct = cc.Spawn:create(cc.FadeIn:create(0.4), moveAct)
            self.m_pNodeTopBar:runAction( spawnAct )
        else 
            local moveAct = cc.MoveTo:create(0.2, cc.p(self.m_TopBarPosx, self.m_TopBarPosy + 200))
            local spawnAct = cc.Spawn:create(cc.FadeOut:create(0.2), moveAct)
            local callback = cc.CallFunc:create(function ()
                self.m_pNodeTopBar:setVisible(false)
            end)
            local seq = cc.Sequence:create(spawnAct,callback)
            self.m_pNodeTopBar:runAction( seq )
        end 
        print("1111111111111111111111")
    end
end
function YWHallLayer:showRightNodeBar( bShow )
    if self.m_pNodeRightBar ~= nil then
        if bShow then
            self.m_pNodeRightBar:setVisible( true )
            self.m_pNodeRightBar:setOpacity(0)
            local posx, posy = self.m_RightBarPosx, self.m_RightBarPosy
         --   self.m_pNodeRightBar:setPosition( posx + 200, posy)
            local moveAct = cc.EaseBackOut:create( cc.MoveTo:create(0.4, cc.p(posx,posy)) )
            local spawnAct = cc.Spawn:create(cc.FadeIn:create(0.4), moveAct)
            self.m_pNodeRightBar:runAction( spawnAct )
        else 
            local moveAct = cc.MoveTo:create(0.2, cc.p(self.m_RightBarPosx + 200, self.m_RightBarPosy))
            local spawnAct = cc.Spawn:create(cc.FadeOut:create(0.2), moveAct)
            local callback = cc.CallFunc:create(function ()
                self.m_pNodeRightBar:setVisible(false)
            end)
            local seq = cc.Sequence:create(spawnAct,callback)
            self.m_pNodeRightBar:runAction( seq )
        end
    end
end

function YWHallLayer:showBottomNodeBar( bShow )
    if self.m_pNodeBottomBar then
        if bShow then
            self.m_pNodeBottomBar:setVisible( true )
            self.m_pNodeBottomBar:setOpacity(0)
            local posx, posy = self.m_BottomBarPosx, self.m_BottomBarPosy
            self.m_pNodeBottomBar:setPosition( posx, posy - 200)
            --local moveAct = cc.EaseBackOut:create( cc.MoveTo:create(0.4, cc.p(posx,posy)) )
            local moveAct = cc.MoveTo:create(0.4, cc.p(posx,posy)) 
            local spawnAct = cc.Spawn:create(cc.FadeIn:create(0.4), moveAct)
            self.m_pNodeBottomBar:runAction( spawnAct )
        else 
            local moveAct = cc.MoveTo:create(0.2, cc.p(self.m_BottomBarPosx, self.m_BottomBarPosy - 200))
            local spawnAct = cc.Spawn:create(cc.FadeOut:create(0.2), moveAct)
            local callback = cc.CallFunc:create(function ()
                self.m_pNodeBottomBar:setVisible(false)
            end)
            local seq = cc.Sequence:create(spawnAct,callback)
            self.m_pNodeBottomBar:runAction( seq )
        end
    end
end
 
return YWHallLayer