--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 登录显示页
local _={}
local scheduler = require("framework.scheduler")
require("app.assetmgr.util.RenewUtil")
local PhoneRegisterLayer = import("..childLayer.PhoneRegisterLayer")
local HNLayer = import("..HNLayer")
local LoginLayer = class("LoginLayer", function ()
    return HNLayer.new()
end)

function LoginLayer:ctor( __scene )
    self:myInit()
    self:setupViews(__scene)
end

-- 初始化数据
function LoginLayer:myInit()
    self.scale = 1 -- 缩放比
    self.isClickLogin = false --是否点击登录
    self.checkSelect = true  --默认选中
end

--客服
function LoginLayer:onCustomChat()
	local url = "https://energy.lu6000.com/chat/chatClient/chatbox.jsp?companyID=365026755&configID=574&jid=7756209912&s=1"
    print(url)
	local xhr = cc.XMLHttpRequest:new()
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
    xhr:open("GET",url) 

    local function onReadyStateChange() 
        local response   = xhr.response
        if response == "" then
            print("response = nil")
        else
            local output = json.decode(response,1) 
            dump(output)
            if output.data then
                for k,v in pairs(output.data) do
                    if v.type == 3 then
                        device.openURL(v.accountOrUrl)
                    end
                end
            end
        end
    end
    xhr:registerScriptHandler(onReadyStateChange)
    xhr:send()
end

--游客方式登录
function LoginLayer:onTouchCallback(sender)
    local name =sender:getName()
     _:disableQuickClick(sender)
     
    if name =="button_loginYouke" then
		g_LoginController:loginByGuest()
        self:setBtnOut()
    elseif name == "button_loginAccount" then
        self.loginView:setVisible(true)
    elseif name == "button_loginWeixin" then
        self:setWxLoginBtn()
    elseif name == "button_customBtn" then
        self:onCustomChat()
    end
end

function LoginLayer:onTouchCallback1(sender)
    local name =sender:getName()
    if name =="button_forgetBtn" then 
        local phoneRegisterLayer = PhoneRegisterLayer.new(OpenType.Reset)
        self:addChild(phoneRegisterLayer,3000) 
    elseif name == "button_closeBtn" then
        self.loginView:setVisible(false)
        self.textField_accountField:setString("")
        self.textField_passwordField:setString("")
    elseif name == "button_register" then 
        local phoneRegisterLayer = PhoneRegisterLayer.new(OpenType.Register)
        self:addChild(phoneRegisterLayer,3000)
        if (self._winSize.width / self._winSize.height > 1.78) then 
            phoneRegisterLayer:setScaleX(display.scaleX)
            phoneRegisterLayer:setPositionX((self._winSize.width-self._winSize.width*display.scaleX)/2)
        end
    elseif name == "button_loginBtn" then 
          
        local id = self.textField_accountField:getString()
        local psd =  self.textField_passwordField:getString() 
        if #id == 0 or #psd == 0 then
		    TOAST("账号或密码为空")
		    return
	    end
	    if #id < 11 or tonumber(id) == nil then
		    TOAST("手机号格式不正确")
		    return
	    end
		
	    local function splitQuotationMark(s)
		    s = string.gsub(s, "% ", "")
            s = string.gsub(s, "%\n", "")
		    return s
	    end

	    if splitQuotationMark(psd) ~= psd then
		    TOAST("密码不能有特殊空格或者换行符")
		    return
	    end
		
		--g_LoginController._changeAccountType = 1
		g_LoginController.logonType = "qka"
        g_LoginController:setPassword(id, psd)
		g_LoginController:login()
    end
end

-- 初始化界面
function LoginLayer:setupViews(__scene)
    self.scene = __scene  
  --  g_AudioPlayer:playMusic("dt/sound/bg_new.mp3", true)
    local winSize = cc.Director:getInstance():getWinSize();
    --local flag = cc.UserDefault:getInstance():getStringForKey("First_Login")
	local loginType = ToolKit:getLoginType()
	
    if loginType ~= GlobalDefine.LoginType.none and not GlobalDefine.ISLOGOUT then
		local logo = display.newSprite("hall/image/file/launch-image.jpg")
		self:addChild(logo)
		logo:setPosition(winSize.width / 2, winSize.height / 2);  
		--logo:setScale(display.scaleX, display.scaleY);
		--local image_bg = cc.Sprite:create("login/image/zcmdwc_loading/logo_3.png")
		--image_bg:setPosition(cc.p(winSize.width/2, winSize.height*0.65))
		--self:addChild(image_bg)
		
		local pSpine = "hall/effect/325_denglu_tunvlangdizhu/325_denglu_tunvlangdizhu"
		self.skeletonNode = sp.SkeletonAnimation:createWithBinaryFile(pSpine .. ".skel", pSpine .. ".atlas")
		self.skeletonNode:setPosition(cc.p(1624 / 2, 750 / 2))
		self.skeletonNode:setAnimation(0, "animation", true)
		self.skeletonNode:update(0)
		self.skeletonNode:addTo(logo)

        local imagebg = ccui.ImageView:create()
        imagebg:loadTexture("hall/login/login-bg-icon.png")
        imagebg:ignoreContentAdaptWithSize(false)
        imagebg:setContentSize(cc.size(1624,262))
        imagebg:setAnchorPoint(cc.p(0.5,0))
        imagebg:setPosition(cc.p(812,-15))
        logo:addChild(imagebg)

        local textup = ccui.Text:create("正在加载资源(加载资源不消耗流量)","hall/font/fzft.ttf",24)
        textup:ignoreContentAdaptWithSize(false)
        textup:setAnchorPoint(cc.p(0.5,0.5))
        textup:setPosition(cc.p(812,120))
        imagebg:addChild(textup)

        local imagebar = ccui.ImageView:create()
        imagebar:loadTexture("hall/login/progress_bar_bg.png")
        imagebar:ignoreContentAdaptWithSize(false)
        imagebar:setAnchorPoint(cc.p(0.5,0.5))
        imagebar:setPosition(cc.p(812,80))
        imagebg:addChild(imagebar)

        local startper = math.random(15,50)
        local sprite = cc.Sprite:create("hall/login/progress_bar.png")
        local progressbar = cc.ProgressTimer:create(sprite)
        progressbar:setType(cc.PROGRESS_TIMER_TYPE_BAR)
        progressbar:setBarChangeRate(cc.p(1,0))
        progressbar:setMidpoint(cc.p(0,0)) 
        progressbar:setAnchorPoint(cc.p(0.5,0.5))
        progressbar:setPosition(cc.p(812,79.5))
        progressbar:setPercentage(startper)
        imagebg:addChild(progressbar)

        performWithDelay(progressbar, function()
            progressbar:setPercentage(100)
        end, 0.22)
		return
    end
	 
    self.root = cc.CSLoader:createNode("login/LoginScene.csb")
    local center = self.root:getChildByName("Layout_1")
    self.btnYouke = center:getChildByName("nodeButton"):getChildByName("button_loginYouke")
    self.btnWeixin = center:getChildByName("nodeButton"):getChildByName("button_loginWeixin")
    self.btnAccount = center:getChildByName("nodeButton"):getChildByName("button_loginAccount")
    self:addChild(self.root) 
    UIAdapter:adapter(self.root,handler(self,self.onTouchCallback))
    UIAdapter:praseNode(self.root,self)
    local diffY = (display.size.height - 750) / 2
    self.root:setPosition(cc.p(0,diffY)) 
    local diffX = 145 - (1624-display.size.width)/2
    center:setPositionX(diffX)
--     local image_bg = cc.Sprite:create("login/image/zcmdwc_loading/logo_3.png")
--        image_bg:setPosition(cc.p(winSize.width/2, winSize.height*0.65))
--        self:addChild(image_bg) 
   -- self.root:setPositionY((display.height - 750) / 2)
    self.nodeButton:setLocalZOrder(100)
    --Image_1:setPositionX((display.width - 1334) / 2+diffX)
    local pSpine = "hall/effect/325_denglu_tunvlangdizhu/325_denglu_tunvlangdizhu"
    self.skeletonNode = sp.SkeletonAnimation:createWithBinaryFile(pSpine .. ".skel", pSpine .. ".atlas")
   -- self.skeletonNode:setPosition(cc.p(1624 / 2, 750 / 2))
    self.skeletonNode:setAnimation(0, "animation", true)
    self.skeletonNode:update(0)
    self.skeletonNode:addTo(self.Node_1) 
    self.loginView = UIAdapter:createNode("hall/csb/AccountLoginDialog.csb")
    self:addChild(self.loginView)  
    local diffY = (display.size.height - 750) / 2
    self.loginView:setPosition(cc.p(0,diffY))
    self.m_pNodeRoot    = self.loginView:getChildByName("AccountLogin")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)
    UIAdapter:adapter(self.loginView ,handler(self,self.onTouchCallback1))
    self.loginView:setVisible(false)
    UIAdapter:praseNode(self.loginView,self)  
    self.textField_accountField:setInputMode(cc.EDITBOX_INPUT_MODE_PHONENUMBER)  
    self.textField_passwordField:setInputMode(cc.EDITBOX_INPUT_FLAG_PASSWORD)
end  

--微信已解绑，需要使用微信重新登录
function LoginLayer:onReLoginWx()
    local params = {
        title = "提示",
        message = "您的微信号已解绑，您需要用微信重新登录！", -- todo: 换行
        leftStr = "重新登录" ,
        rightStr = "退出",
    }

    local _rightcallback = function ()
        TotalController:onExitApp()
    end

    local  _leftCallback = function ()
        --设置登录方式
        --ToolKit:setLoginType(GlobalDefine.LoginType.wx)         
        g_LoginController:wxAuthorizelogin()
    end

    local dlg = require("app.hall.base.ui.MessageBox").new()
    dlg:TowSubmitAlert(params, _leftCallback, _rightcallback)
    dlg:showDialog(self)
end

function LoginLayer:onDestory()
    -- 注销消息
    removeMsgCallBack(self, MSG_RE_BIND_WX)
    removeMsgCallBack(self, MSG_RE_CREATE_VISITOR)
    removeMsgCallBack(self, MSG_LOGIN_ERROR_CLOSE_GAME)
end

-- 获取屏幕缩放比
-- return (number) 屏幕尺寸较小的缩放比
function LoginLayer:getSizeScale()
    return self.scale
end

-- 显示进度条
-- params __percent(number) 显示进度
function LoginLayer:showProgress( __percent )
--    local progress = self.root:getChildByName("Image_1")
--    progress:setPercent(__percent)
--    local percentLabel = self.root:getChildByName("txt_percent")
--    percentLabel:setString(__percent .. "%")
end

--登录方式
function LoginLayer:showLoginType()
    print("-------------LoginLayer:showLoginType()--------------")
--    self.protolLayout:setVisible(true)
--    self.bg_layout:setVisible(true)

    if UpdateFunctions.platform == "windows" then
        self.touristBtn:setVisible(true)
   
        --self.touristBtn:setVisible(true)
    end
end


function LoginLayer:showLoginWayView()
    --self.logonNode:setVisible( true )
end

--[[-- 显示加载动画(中间圆圈转动)
function LoginLayer:showLoading()
    self.root:getChildByName("img_cycle"):runAction(cc.RepeatForever:create(cc.RotateBy:create(1, 360)))
end--]]

-- 显示提示文本
-- params __str(string) 显示的文本内容
function LoginLayer:showTipsStr( __str )
--    local tipsLabel = self.root:getChildByName("txt_tips")
--    tipsLabel:setString(__str)
--    local layoutProgress = self.root:getChildByName("layout_progress")
--    layoutProgress:setVisible(true)
end

-- 显示大提示文本
-- params __str(string) 显示的文本内容
function LoginLayer:showBigTipsStr( __str )
--    local layoutProgress = self.root:getChildByName("layout_progress")
--    layoutProgress:setVisible(false)
end

-- 设置底部进度条显示属性
-- params __isVisible(bool) 是否显示
function LoginLayer:setBottomVisible( __isVisible )
   -- self.root:getChildByName("layout_bottom"):setVisible(__isVisible)
end

function LoginLayer:setProgressLayoutVisible( __isVisible )
  --  self.root:getChildByName("layout_progress"):setVisible(__isVisible)
end

-- 显示区分标志(内测, 外测, 外网)
-- params __showPackageName(bool) 是否显示包名后缀
function LoginLayer:showPakcageInfo( __showPackageName )
--    if GlobalConf.CurServerType ~= GlobalConf.ServerType.WW then
--        local label = cc.Label:create()
--        label:setSystemFontSize(30)
--        label:setAnchorPoint({x = 0, y = 0.5})
--        label:setPosition(0, self.logo:getPositionY() - 100)
--        self.logo:getParent():addChild(label)

--        local showStr = ""
--        if GlobalConf.CurServerType == GlobalConf.ServerType.NC then
--            showStr = "内测"
--        elseif GlobalConf.CurServerType == GlobalConf.ServerType.WC then
--            showStr = "外测"
--        end

--        if __showPackageName then
--            local oriName = "com.milai.milaigame"
--            local packageName = platform.getAppPackageName()
--            if packageName ~= oriName then
--                showStr = showStr .. "(" .. packageName .. ")"
--            end
--        end
--        --label:setString(showStr)
--    end 
end

function LoginLayer:setMsgListener()

    --登陆时才会调用，在这里注册监听
    addMsgCallBack(self, MSG_RE_BIND_WX,  handler(self, self.onReLoginWx)) 
    addMsgCallBack(self, MSG_RE_CREATE_VISITOR,  handler(self, self.onCreateVisitorAccount))
    addMsgCallBack(self, MSG_LOGIN_ERROR_CLOSE_GAME,  handler(self, self.onCloseGame)) 
    
    ToolKit:registDistructor( self, handler(self, self.onDestory) )
end
 
--设置微信登录按钮
function LoginLayer:setWxLoginBtn()
    self.isClickLogin = true
    if self.scene.isLoadResCompleted == false then
        ToolKit:addLoadingDialog(1.5, "加载中，请稍候......", self.scene)
    elseif self.checkSelect == false then
        TOAST("您未阅读并同意《用户协议》")
    else
        g_LoginController:startByWx()
        if ToolKit:getLoginType() == GlobalDefine.LoginType.wx then
            self:setBtnOut()
        end
    end
end

--密码错误，创建游客账号
function LoginLayer:onCreateVisitorAccount() 

    local DlgAlert = require("app.hall.base.ui.MessageBox")
    local dlg = DlgAlert.showTipsAlert({title = "提示", tip = "密码验证错误，请联系客服"})
    dlg:setSingleBtn("退出", function ()
        --TotalController:onExitApp()
		--print("onCreateVisitorAccount ........")
        --g_LoginController._creatAccountType = 0
		--g_LoginController.logonType = "guest"
        --g_LoginController._changeAccountType = 0
        --g_LoginController.account = ""
        --g_LoginController.password = ""
        --local acc = g_LoginController._gameDataCtl:getLastAccountData()  
        --if acc then
        --    g_LoginController._gameDataCtl:deleteAccount(acc.account)
        --end
        --ToolKit:setLoginType(GlobalDefine.LoginType.none)
		ToolKit:returnToLoginScene()
    end)
    dlg:enableTouch(false)
end

--第三方渠道登录出错，关闭游戏重新登录
function LoginLayer:onCloseGame()
    local DlgAlert = require("app.hall.base.ui.MessageBox")
    local dlg = DlgAlert.showTipsAlert({title = "出错提示", tip = "登录出错，请联系客服或者关闭游戏重新登录！"})
    dlg:setSingleBtn("关闭", function ()
        TotalController:onExitApp()
    end)
    dlg:enableTouch(false)
end

function LoginLayer:setBtnOut()
        self.btnYouke:setVisible(false)
        self.btnWeixin:setVisible(false)
        self.btnAccount:setVisible(false)

        local diffX =  - (1624-display.size.width)/2

        local imagebg = ccui.ImageView:create()
        imagebg:loadTexture("hall/login/login-bg-icon.png")
        imagebg:ignoreContentAdaptWithSize(false)
        imagebg:setContentSize(cc.size(1624,262))
        imagebg:setAnchorPoint(cc.p(0,0))
        imagebg:setPosition(cc.p(diffX,-15))
        self:addChild(imagebg)

        local textup = ccui.Text:create("正在加载资源(加载资源不消耗流量)","hall/font/fzft.ttf",24)
        textup:ignoreContentAdaptWithSize(false)
        textup:setAnchorPoint(cc.p(0.5,0.5))
        textup:setPosition(cc.p(812,120))
        imagebg:addChild(textup)

        local imagebar = ccui.ImageView:create()
        imagebar:loadTexture("hall/login/progress_bar_bg.png")
        imagebar:ignoreContentAdaptWithSize(false)
        imagebar:setAnchorPoint(cc.p(0.5,0.5))
        imagebar:setPosition(cc.p(812,80))
        imagebg:addChild(imagebar)

        local startper = math.random(15,50)
        local sprite = cc.Sprite:create("hall/login/progress_bar.png")
        local progressbar = cc.ProgressTimer:create(sprite)
        progressbar:setType(cc.PROGRESS_TIMER_TYPE_BAR)
        progressbar:setBarChangeRate(cc.p(1,0))
        progressbar:setMidpoint(cc.p(0,0)) 
        progressbar:setAnchorPoint(cc.p(0.5,0.5))
        progressbar:setPosition(cc.p(812,79.5))
        progressbar:setPercentage(startper)
        imagebg:addChild(progressbar)

        performWithDelay(progressbar, function()
            progressbar:setPercentage(100)
        end, 0.22)
end

function _:setBtnEnabled(btn, enabled)
    btn:setEnabled(enabled)
    btn:setTouchEnabled(enabled)
 --   btn:setBright(enabled)
end

-- 防止频繁点击
function _:disableQuickClick(btn)
    _:setBtnEnabled(btn, false)
    
    performWithDelay(btn, function()
        _:setBtnEnabled(btn, true)
    end, 2)
end

return LoginLayer
