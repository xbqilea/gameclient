--region 
--此文件由[BabeLua]插件自动生成
--
--Desc: 游戏加载界面父类

--参考AnimalLoadingLayer
--实现initLoading/startLoading/stopLoading

local LoadingLayer = class("LoadingLayer", cc.Layer)

function LoadingLayer:onEnter()

    cc.exports.g_SchulerOfLoading = nil

    if self.bLoad then
        self:startLoading()
    end
end

function LoadingLayer:onExit()

    if cc.exports.g_SchulerOfLoading then
        scheduler.unscheduleGlobal(cc.exports.g_SchulerOfLoading)
        cc.exports.g_SchulerOfLoading = nil
    end
end

function LoadingLayer:initPlistArray(plist) self.paths_plist  = plist end
function LoadingLayer:initImageArray(image) self.paths_image  = image end
function LoadingLayer:initEffectArray(anim) self.paths_effect = anim  end
function LoadingLayer:initSoundArray(sound) self.paths_sound  = sound end
function LoadingLayer:initMusicArray(music) self.paths_music  = music end
function LoadingLayer:initOtherArray(array) self.paths_other  = array end

function LoadingLayer:initLoadingWord(node)
    
    local kind = PlayerInfo.getInstance():getKindID()
    local word = Localization_cn["Loading_" .. kind]
    if nil == word then return end
    math.randomseed(tostring(os.time()):reverse():sub(1, 6))
    local index = math.random(1, #word)
    local text = word[index]
    local pWord = cc.Label:createWithTTF("", FONT_TTF_PATH, 30)
    pWord:setString(text)
    pWord:setAnchorPoint(0.5, 0.5)
    pWord:setPosition(display.cx, 80)
    pWord:setTextColor(cc.WHITE)
    pWord:enableShadow(cc.BLACK, cc.size(2, -3), 0)
    pWord:addTo(node)
end

function LoadingLayer:initProgressBar(node)
    
    --进度条背景
    local m_ploadingBg = display.newSprite("hall/login/progress_bar_bg.png")
    local size = m_ploadingBg:getContentSize()
    m_ploadingBg:setAnchorPoint(0, 0.5)
    m_ploadingBg:setPosition(display.cx - size.width / 2, display.cy * 0.3)
    m_ploadingBg:addTo(node, 2)

    --进度条
    local m_pProgressTimer = cc.ProgressTimer:create(display.newSprite("hall/login/progress_bar.png"))
    m_pProgressTimer:setType(cc.PROGRESS_TIMER_TYPE_BAR) -- 设置为条
    m_pProgressTimer:setMidpoint(cc.p(0, 0))           -- 设置起点为条形坐下方
    m_pProgressTimer:setBarChangeRate(cc.p(1, 0))      -- 设置为竖直方向
    m_pProgressTimer:setPercentage(0)                  -- 设置初始进度为0
    m_pProgressTimer:setAnchorPoint(0, 0.5)            -- 设置描点为左中
    m_pProgressTimer:setPosition(-4, 7)                -- 设置偏移
    m_pProgressTimer:addTo(m_ploadingBg)
    self.m_pProgressTimer = m_pProgressTimer

    --提示文字
    local m_pLabelLoading = cc.Label:createWithTTF("正在进入游戏", FONT_TTF_PATH, 30)
    m_pLabelLoading:setAnchorPoint(0.5, 0.5)
    m_pLabelLoading:setPosition(size.width / 2, 50)
    m_pLabelLoading:addTo(m_ploadingBg)

    --更新进度条
    self.updatePercent = function(self, nPercent)
        self.m_pProgressTimer:setPercentage(nPercent)
    end
end

function LoadingLayer:executeLoading()

    -------------------------------------------------------
    --记录执行
    local func = {}
    for i = 1, #self.paths_sound do --音效
        table.insert(func, function()
            AudioManager.getInstance():preloadEffect({ self.paths_sound[i] })
        end)
    end
    for i = 1, #self.paths_music do --音乐
        --table.insert(func, function()
            --AudioManager.getInstance():preloadMusic({ self.paths_music[i] })
        --end)
    end
    for i = 1, #self.paths_plist do --碎图
        table.insert(func, function()
            display.loadSpriteFrames(self.paths_plist[i][1], self.paths_plist[i][2])
        end)
    end
    for i = 1, #self.paths_image do --大图
        table.insert(func, function()
            display.loadImage(self.paths_image[i])
        end)
    end
    for i = 1, #self.paths_effect do --动画
        table.insert(func, function()
            ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(self.paths_effect[i])
        end)
    end
    if self.paths_other and next(self.paths_other) then --其他
        for i = 1, #self.paths_other do
            table.insert(func, function()
                self.paths_other[i]()
            end)
        end
    end
    table.insert(func, function()
        display.loadSpriteFrames("hall/plist/gui-vip.plist", "hall/plist/gui-vip.png")
    end)
    table.insert(func, function()
        display.loadSpriteFrames("hall/plist/gui-userinfo.plist", "hall/plist/gui-userinfo.png")
    end)
    -------------------------------------------------------
    --开始加载
    local time_load = cc.exports.gettime()
    local time_start = 0
    local time_stop = 0
    local time_frame = cc.Director:getInstance():getAnimationInterval()
    local len = #func
    local index = 0
    local count = 0
    cc.exports.g_SchulerOfLoading = nil
    cc.exports.g_SchulerOfLoading = scheduler.scheduleUpdateGlobal(function()
        if index < len then
            --一帧开始时间
            time_start = cc.exports.gettime()
            --一帧结束时间
            time_stop = time_start
            --一帧加载数量
            count = 0
            --一帧时间未完,继续加载
            while time_stop - time_start < time_frame and index < len do
                
                --计数
                count = count + 1
                index = index + 1
                print("-- load --", index, len, index / len, count)
                
                --更新界面
                if self.updatePercent then
                    self:updatePercent(index / len * 100)
                end

                --加载一个动作
                func[index]()
                
                --重新计时
                time_stop = cc.exports.gettime()
            end
        else
            print("----- load over ------", cc.exports.gettime() - time_load)
            if self.updatePercent then
                self:updatePercent(100)
            end

            scheduler.unscheduleGlobal(cc.exports.g_SchulerOfLoading)
            cc.exports.g_SchulerOfLoading = nil

            if self.stopLoading then
                self:stopLoading()
            end
        end
    end)
end

return LoadingLayer

--endregion
