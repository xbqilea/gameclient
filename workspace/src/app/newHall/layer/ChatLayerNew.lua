--region 新版聊天和玩家信息界面
--Date 2019.02.28
--by xiaoyema

local Public_Events = require("common.define.PublicEvent")
local CUserManager  = require("common.manager.CUserManager")
local UsrMsgManager = require("common.manager.UsrMsgManager")

local MaxViewChat        = 20  -- 最多显示多少条聊天消息
local MaxTextWidth       = 410 -- 每行消息显示最大宽度
local MsgLabelFontFamily = "Helvetica" -- 消息字体
local MsgLabelFontSize   = 24 -- 消息字号

--[[
    -----------------------help--------------------
    --------1 左边的聊天 2 左边的玩家 3 右边的聊天 4 右边的玩家
    a:1和3 2和4 不能同时存在(是相同的界面只是位置不同)
    b:initData 初始化需要创建界面的位置
    c:setBtnVisible 初始化按钮是否显示

    local m_newChatLayer = ChatLayerTwo:create()
    m_newChatLayer:initData({1,4})
    m_newChatLayer:setBtnVisible(true,true,true,true)
]]

local ChatLayerNew = class("ChatLayerNew", cc.Layer)

ChatLayerNew.instance = nil
function ChatLayerNew.create()
    ChatLayerNew.instance = ChatLayerNew:new()
    return ChatLayerNew.instance
end

function ChatLayerNew:ctor()
    self:enableNodeEvents()
    self:init()
end

function ChatLayerNew:onEnter()
    self:initEvent()  --监听事件
end

function ChatLayerNew:onExit()
    self:clearEvent()   --移除事件
    UsrMsgManager.getInstance():clearChatData()
    -- 聊天窗口使用的较多 不释放plist?
    display.removeSpriteFrames("hall/plist/gui-chat.plist", "hall/plist/gui-chat.png")
end

function ChatLayerNew:clearEvent()
    for key in pairs(self.__event) do
        SLFacade:removeCustomEventListener(key, self.__cname)
    end
end

function ChatLayerNew:initEvent()
    self.__event = 
    {
        [Public_Events.MSG_UPDATE_CHAT_MSG_TEXT] = { func = self.event_update_chat, log = "更新聊天信息", },
        [Public_Events.MSG_USER_DELETE] = { func = self.event_update_user_list, log = "更新玩家列表", },
        [Public_Events.MSG_USER_ENTER] = { func = self.event_update_user_list, log = "更新玩家列表", },
    }
    local function onMsgEvent(event)
        local name = event:getEventName()
        local msg = unpack(event._userdata)
        self.__event[name].func(self, msg)
    end

    for key, var in pairs(self.__event) do
        SLFacade:addCustomEventListener(key, onMsgEvent, self.__cname)
    end
end


function ChatLayerNew:init()
    display.loadSpriteFrames("hall/plist/gui-chat.plist", "hall/plist/gui-chat.png")
    display.loadSpriteFrames("hall/plist/gui-vip.plist", "hall/plist/gui-vip.png")
    display.loadSpriteFrames("hall/plist/gui-userinfo.plist", "hall/plist/gui-userinfo.png")

    local strPath = isPortraitView() and "hall/csb/newchatlayer_p.csb" or "hall/csb/newchatlayer.csb"
    self.m_pathUI = cc.CSLoader:createNode(strPath):addTo(self)

    self.m_pEmpty = self.m_pathUI:getChildByName("Panel_empty")
    local empty_size = cc.size(self.m_pEmpty:getContentSize().width, self.m_pEmpty:getContentSize().height)

    self.m_pEmpty:addClickEventListener(handler(self,self.shrinkView))
    
    self.m_pBtnLeftMsg = self.m_pathUI:getChildByName("Btn_msg_left")
    self.m_pBtnLeftMsg:setOpacity(255*0.3)
    self.m_pBtnRightMsg = self.m_pathUI:getChildByName("Btn_msg_right")
    self.m_pBtnRightMsg:setOpacity(255*0.3)

    self.m_pBtnLeftPlayer = self.m_pathUI:getChildByName("Btn_player_left")
    self.m_pOnLineLeftNum = self.m_pBtnLeftPlayer:getChildByName("Text_OLCount")
    self.m_pBtnRightPlayer = self.m_pathUI:getChildByName("Btn_player_right")
    self.m_pOnLineRightNum = self.m_pBtnRightPlayer:getChildByName("Text_OLCount")

    self.m_pBtnLeftMsg:addClickEventListener(handler(self,self.onShowViewClicked))
    self.m_pBtnLeftPlayer:addClickEventListener(handler(self,self.onShowViewClicked))
    self.m_pBtnRightMsg:addClickEventListener(handler(self,self.onShowViewClicked))
    self.m_pBtnRightPlayer:addClickEventListener(handler(self,self.onShowViewClicked))

    self.m_pNodeAll = self.m_pathUI:getChildByName("NodeAll")

    self.m_pNodeLeftMsg = self.m_pNodeAll:getChildByName("NodeMsg_left")
    self.m_pNodeRightMsg = self.m_pNodeAll:getChildByName("NodeMsg_right")

    self.m_pNodeLeftPlayer = self.m_pNodeAll:getChildByName("NodePlayes_left")
    self.m_pNodeRightPlayer = self.m_pNodeAll:getChildByName("NodePlayes_right")

    self.m_pNodeTable = {}
    self.m_pNodeTable[1] = self.m_pNodeLeftMsg:getChildByName("Image_1"):getChildByName("NodeTableMsg")
    self.m_pNodeTable[2] = self.m_pNodeLeftPlayer:getChildByName("Image_1"):getChildByName("NodeTablePlayers")
    self.m_pNodeTable[3] = self.m_pNodeRightMsg:getChildByName("Image_1"):getChildByName("NodeTableMsg")
    self.m_pNodeTable[4] = self.m_pNodeRightPlayer:getChildByName("Image_1"):getChildByName("NodeTablePlayers")
    
    --左右玩家界面上的玩家数量
    self.m_pLbLeftOnLineNum = self.m_pNodeLeftPlayer:getChildByName("Image_1"):getChildByName("Text_OnLineNum")
    self.m_pLbRightOnLineNum = self.m_pNodeRightPlayer:getChildByName("Image_1"):getChildByName("Text_OnLineNum")

    --左右聊天界面的消息发送按钮
    self.m_pBtnLeftSend = self.m_pNodeLeftMsg:getChildByName("Image_1"):getChildByName("Btn_send")
    self.m_pBtnRightSend = self.m_pNodeRightMsg:getChildByName("Image_1"):getChildByName("Btn_send")
    self.m_pBtnLeftSend:addClickEventListener(handler(self,self.onLabaClicked))
    self.m_pBtnRightSend:addClickEventListener(handler(self,self.onLabaClicked))

    --左右关闭弹窗按钮
    self.m_pNodeLeftMsg:getChildByName("Image_1"):getChildByName("Btn_return"):addClickEventListener(handler(self,self.onReturnClicked))
    self.m_pNodeLeftPlayer:getChildByName("Image_1"):getChildByName("Btn_return"):addClickEventListener(handler(self,self.onReturnClicked))
    self.m_pNodeRightMsg:getChildByName("Image_1"):getChildByName("Btn_return"):addClickEventListener(handler(self,self.onReturnClicked))
    self.m_pNodeRightPlayer:getChildByName("Image_1"):getChildByName("Btn_return"):addClickEventListener(handler(self,self.onReturnClicked))


    self.m_pEmpty:hide()
    self.m_pNodeAll:hide()

    self.m_pShowViewList = {false,false,false,false}
    self.m_nViewType = 0
    self.m_pTableView = {}
    self.m_pMoving = false
    self.m_vecTextHeight = {}
    self.m_vecPlayersList = {}
    self.m_pKeepBtnShowList ={false,false,false,false} --弹出窗口后 按钮是否可见
    self.m_pUpdateAll = true --玩家列表是否是当前游戏所有玩家
    self.m_pUserData = CUserManager.getInstance():getUserInfoTable()
    self.m_NodeAllOffset = {0,0}
    self.m_pShowBtn = true --是否显示对应的按钮

    -- 消息动画
    local jsonName = "public/effect/chatanimation/GetNewMessage.ExportJson"
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(jsonName)
    local sz = self.m_pBtnLeftMsg:getContentSize()
    self.m_pLeftMsgArmature = ccs.Armature:create("GetNewMessage")
    self.m_pLeftMsgArmature:getAnimation():play("Animation1",-1,1)
    self.m_pLeftMsgArmature:setPosition(cc.p(sz.width/2,sz.height/2))
    self.m_pLeftMsgArmature:setName("NewMsgAnimation")
    self.m_pLeftMsgArmature:addTo(self.m_pBtnLeftMsg)
    self.m_pLeftMsgArmature:setVisible(false)

    sz = self.m_pBtnRightMsg:getContentSize()
    self.m_pRightMsgArmature = ccs.Armature:create("GetNewMessage")
    self.m_pRightMsgArmature:getAnimation():play("Animation1",-1,1)
    self.m_pRightMsgArmature:setPosition(cc.p(sz.width/2,sz.height/2))
    self.m_pRightMsgArmature:setName("NewMsgAnimation")
    self.m_pRightMsgArmature:addTo(self.m_pBtnRightMsg)
    self.m_pRightMsgArmature:setVisible(false)

     -- 初始化一个字符所占尺寸
    local sz = LuaUtils.getAsciiCharSize("Arial", MsgLabelFontSize, false)
    self.m_asciiWidth = sz.width
    self.m_asciiHeight = sz.height

    self.m_olChangeCbFun = nil -- 在线人数发生变化时的回调
    self:event_update_user_list()
end

function ChatLayerNew:initData(show_table)
    local deal_table = {}
    if not show_table or #show_table == 0 then
        deal_table = {1,2}
    else 
        deal_table = show_table
    end
    for i=1,#deal_table do
        self.m_pShowViewList[deal_table[i]] = true
    end

    dump(self.m_pShowViewList,"m_pShowViewList")
    self:initTableView()


end

function ChatLayerNew:initTableView()

    if self.m_pTableView[1] == nil and self.m_pShowViewList[1] then
        self.m_pTableView[1] = cc.TableView:create(self.m_pNodeTable[1]:getContentSize());
        self.m_pTableView[1]:setAnchorPoint(cc.p(0,0));
        self.m_pTableView[1]:setPosition(cc.p(0,1));
        self.m_pTableView[1]:setIgnoreAnchorPointForPosition(false);
        self.m_pTableView[1]:setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN);
        self.m_pTableView[1]:setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
        self.m_pTableView[1]:setDelegate()
        self.m_pTableView[1]:registerScriptHandler(handler(self,self.cellSizeForTable), CCTableView.kTableCellSizeForIndex)
        self.m_pTableView[1]:registerScriptHandler(handler(self,self.tableCellAtIndex), CCTableView.kTableCellSizeAtIndex)
        self.m_pTableView[1]:registerScriptHandler(handler(self,self.numberOfCellsInTableView), CCTableView.kNumberOfCellsInTableView)
        self.m_pTableView[1]:registerScriptHandler(handler(self,self.tableCellTouched), CCTableView.kTableCellTouched)
        self.m_pNodeTable[1]:addChild(self.m_pTableView[1]);
    end
    if self.m_pTableView[2] == nil and self.m_pShowViewList[2] then
        self.m_pTableView[2] = cc.TableView:create(self.m_pNodeTable[2]:getContentSize());
        self.m_pTableView[2]:setAnchorPoint(cc.p(0,0));
        self.m_pTableView[2]:setPosition(cc.p(0,1));
        self.m_pTableView[2]:setIgnoreAnchorPointForPosition(false);
        self.m_pTableView[2]:setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN);
        self.m_pTableView[2]:setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
        self.m_pTableView[2]:setDelegate()
        self.m_pTableView[2]:registerScriptHandler(handler(self,self.cellSizeForTable2), CCTableView.kTableCellSizeForIndex)
        self.m_pTableView[2]:registerScriptHandler(handler(self,self.tableCellAtIndex2), CCTableView.kTableCellSizeAtIndex)
        self.m_pTableView[2]:registerScriptHandler(handler(self,self.numberOfCellsInTableView2), CCTableView.kNumberOfCellsInTableView)
        self.m_pTableView[2]:registerScriptHandler(handler(self,self.tableCellTouched2), CCTableView.kTableCellTouched)
        self.m_pNodeTable[2]:addChild(self.m_pTableView[2]);
    end
    if not self.m_pTableView[3] and self.m_pShowViewList[3] then
        self.m_pTableView[3] = cc.TableView:create(self.m_pNodeTable[3]:getContentSize());
        self.m_pTableView[3]:setAnchorPoint(cc.p(0,0));
        self.m_pTableView[3]:setPosition(cc.p(0,1));
        self.m_pTableView[3]:setIgnoreAnchorPointForPosition(false);
        self.m_pTableView[3]:setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN);
        self.m_pTableView[3]:setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
        self.m_pTableView[3]:setDelegate()
        self.m_pTableView[3]:registerScriptHandler(handler(self,self.cellSizeForTable), CCTableView.kTableCellSizeForIndex)
        self.m_pTableView[3]:registerScriptHandler(handler(self,self.tableCellAtIndex), CCTableView.kTableCellSizeAtIndex)
        self.m_pTableView[3]:registerScriptHandler(handler(self,self.numberOfCellsInTableView), CCTableView.kNumberOfCellsInTableView)
        self.m_pTableView[3]:registerScriptHandler(handler(self,self.tableCellTouched), CCTableView.kTableCellTouched)
        self.m_pNodeTable[3]:addChild(self.m_pTableView[3]);
    end
    if not self.m_pTableView[4] and self.m_pShowViewList[4] then
        self.m_pTableView[4] = cc.TableView:create(self.m_pNodeTable[4]:getContentSize());
        self.m_pTableView[4]:setAnchorPoint(cc.p(0,0));
        self.m_pTableView[4]:setPosition(cc.p(0,1));
        self.m_pTableView[4]:setIgnoreAnchorPointForPosition(false);
        self.m_pTableView[4]:setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN);
        self.m_pTableView[4]:setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
        self.m_pTableView[4]:setDelegate()
        self.m_pTableView[4]:registerScriptHandler(handler(self,self.cellSizeForTable2), CCTableView.kTableCellSizeForIndex)
        self.m_pTableView[4]:registerScriptHandler(handler(self,self.tableCellAtIndex2), CCTableView.kTableCellSizeAtIndex)
        self.m_pTableView[4]:registerScriptHandler(handler(self,self.numberOfCellsInTableView2), CCTableView.kNumberOfCellsInTableView)
        self.m_pTableView[4]:registerScriptHandler(handler(self,self.tableCellTouched2), CCTableView.kTableCellTouched)
        self.m_pNodeTable[4]:addChild(self.m_pTableView[4]);

    end
end

function ChatLayerNew:cellSizeForTable(table, idx)
    local width = 450
    local height = 115
    if (idx+1) <= #self.m_vecTextHeight then
        height = self.m_vecTextHeight[idx+1]
    end
    return width, height
end

function ChatLayerNew:tableCellAtIndex(table, idx)
    local cell = table:cellAtIndex(idx)
    if not cell then
        cell = cc.TableViewCell:new()
    else
        cell:removeAllChildren()
    end

    self:initChatCellView(cell, idx);

    return cell
end

function ChatLayerNew:initChatCellView(cell,nIdx)

    local chatnum = UsrMsgManager.getInstance():getChatDataNum()
    if chatnum > MaxViewChat then
        chatnum = chatnum - MaxViewChat
    else
        chatnum = 0
    end

    local tmp = UsrMsgManager.getInstance():getChatDataAtIndex(nIdx + chatnum + 1)

    local strNick = tmp.strSenderName
    local strContent = tmp.strMsg
    local strText = ""
    if tmp.nMsgType == 0 then
        strText = string.format("%s：%s",strNick, strContent)
    elseif tmp.nMsgType == 1 then
        strText = string.format(LuaUtils.getLocalString("CHAT_8"), strNick, tmp.dwSendUserID, strContent)
    elseif  tmp.nMsgType == 2 then
        strText = string.format("(%s)%s",strNick, strContent)
    end

    -- local lbTmp = cc.Label:createWithTTF(strText,  FONT_TTF_PATH, MsgLabelFontSize)
    local lbTmp = cc.Label:createWithSystemFont(strText, "Arial", MsgLabelFontSize)
    lbTmp:setAnchorPoint(cc.p(0, 0));
    lbTmp:setPosition(20, 25);
    lbTmp:setDimensions(MaxTextWidth,0)
    local color = LuaUtils.To_RGB(tmp.dwChatClolor)
    lbTmp:setColor(color)
    cell:addChild(lbTmp,1)

    local spBg = cc.Sprite:createWithSpriteFrameName("hall/plist/chat/chat_line.png")
    spBg:setAnchorPoint(cc.p(0,0))
    spBg:setPosition(cc.p(15, 5))
    spBg:setScaleY(1.5)
    cell:addChild(spBg, 2)

end

function ChatLayerNew:numberOfCellsInTableView(table)
    local nNum = UsrMsgManager.getInstance():getChatDataNum()
    nNum = nNum > MaxViewChat and MaxViewChat or nNum
    return nNum
end

function ChatLayerNew:tableCellTouched(table, cell)
end

function ChatLayerNew:cellSizeForTable2(table, idx)
    return 458,120
end

function ChatLayerNew:tableCellAtIndex2(table, idx)
    local cell = table:cellAtIndex(idx)
    if not cell then
        cell = cc.TableViewCell:new()
    else
        cell:removeAllChildren()
    end

    self:initPlayersCellView(cell, idx);

    return cell
end

function ChatLayerNew:initPlayersCellView(cell,nIdx)

    local kindID = PlayerInfo.getInstance():getKindID()
    local nClassifyType = CGameClassifyDataMgr.getInstance():getClassifyTypeByKindId(kindID)
    local bViewId = false
    local offsety = bViewId and 0 or 10
    local temp = self.m_vecPlayersList[nIdx+1]

    -- 头像
    local headPath = string.format("hall/image/file/gui-icon-head-%02d.png", temp.wFaceID % 10 + 1)
    local spHead = cc.Sprite:create(headPath)
    spHead:setAnchorPoint(cc.p(0.5,0.5))
    spHead:setPosition(cc.p(100, 65))
    spHead:setScale(0.7)
    cell:addChild(spHead)

    -- 头像框
    local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", temp.wFaceCircleID)
    local spHeadBg = cc.Sprite:createWithSpriteFrameName(framePath)
    spHeadBg:setAnchorPoint(cc.p(0.5,0.5))
    spHeadBg:setPosition(cc.p(100, 65))
    spHeadBg:setScale(0.7)
    cell:addChild(spHeadBg)

    -- 昵称
    local strNick = LuaUtils.getDisplayNickName(temp.szNickName, 6, true)
    local lbNick = cc.Label:createWithSystemFont(strNick, "Arial", MsgLabelFontSize)
    lbNick:setAnchorPoint(cc.p(0, 0.5))
    lbNick:setPosition(220, 90 - offsety)
    cell:addChild(lbNick)

    --vip 暂时最高等级最高显示10级
    local vip_level = temp.nVipLev >= 10 and 10 or temp.nVipLev
    local vipPath = string.format("hall/plist/vip/img-vip%d.png",vip_level)
    local spVip = cc.Sprite:createWithSpriteFrameName(vipPath)
    spVip:setPosition(190, 80)
    spVip:addTo(cell)

    if bViewId then 
        -- 用户id
        local strGameID = string.format("ID:%d",temp.dwGameID)
        local lbGameID = cc.Label:createWithSystemFont(strGameID, "Arial", MsgLabelFontSize)
        lbGameID:setAnchorPoint(cc.p(0, 0.5));
        lbGameID:setPosition(220, 57);
        lbGameID:setColor(cc.c3b(231,208,124))
        cell:addChild(lbGameID)
    end

    -- 金币图片
    local spGold = cc.Sprite:createWithSpriteFrameName("hall/plist/chat/chat_coin.png");
    spGold:setAnchorPoint(cc.p(0,0.5))
    spGold:setPosition(cc.p(170, 25 + offsety))
    spGold:setScale(0.5)
    cell:addChild(spGold)

    -- 用户金币
    local strGold = LuaUtils.getFormatGoldAndNumberAndZi(temp.lScore)
    local lbGold = cc.Label:createWithBMFont("hall/font/jb_num.fnt", strGold)
    lbGold:setAnchorPoint(cc.p(0, 0.5))
    lbGold:setPosition(cc.p(209, 23 + offsety))
    lbGold:setScale(0.7)
    lbGold:addTo(cell)

    -- 等级框
    local spLevelBg = cc.Sprite:createWithSpriteFrameName("hall/plist/chat/chat_bgheitao.png");
    spLevelBg:setPosition(cc.p(407, 61))
    cell:addChild(spLevelBg)

    -- 经验等级
    local strLV = string.format("%d",temp.nLevel)
    local lbLV = cc.Label:createWithTTF(strLV, FONT_TTF_PATH, 28)
    lbLV:setAnchorPoint(cc.p(0.5, 0.5))
    lbLV:setPosition(407, 61)
    cell:addChild(lbLV)

    local spBg = cc.Sprite:createWithSpriteFrameName("hall/plist/chat/chat_line.png")
    spBg:setAnchorPoint(cc.p(0,0))
    spBg:setPosition(cc.p(15, 5))
    spBg:setScaleY(1.5)
    cell:addChild(spBg, 2)

end

function ChatLayerNew:numberOfCellsInTableView2(table)

    local nNum = #self.m_vecPlayersList
    return nNum;
end

function ChatLayerNew:tableCellTouched2(table, cell)
end


function ChatLayerNew:updateShowView(nType)
    self.m_nViewType = nType
    print("-------viewtype-----",self.m_nViewType)
    if nType == 1 then 
        self.m_pNodeLeftMsg:show()
        self.m_pNodeLeftPlayer:hide()
        self.m_pNodeRightMsg:hide()
        self.m_pNodeRightPlayer:hide()
        local allHeight = self:calculateChatHeight()
        --将列表重置到最下
        self.m_pTableView[1]:reloadData()
        if allHeight > self.m_pNodeTable[1]:getContentSize().height then
            self.m_pTableView[1]:setContentOffset(cc.p(self.m_pTableView[1]:getContentOffset().x,4))
        end

    elseif nType == 2 then
        self.m_pNodeLeftMsg:hide()
        self.m_pNodeLeftPlayer:show()
        self.m_pNodeRightMsg:hide()
        self.m_pNodeRightPlayer:hide()
        self:updatePlayersList()
        self.m_pTableView[2]:reloadData()
    elseif nType == 3 then
        self.m_pNodeLeftMsg:hide()
        self.m_pNodeLeftPlayer:hide()
        self.m_pNodeRightMsg:show()
        self.m_pNodeRightPlayer:hide()
        local allHeight = self:calculateChatHeight()
        --将列表重置到最下
        self.m_pTableView[3]:reloadData()
        if allHeight > self.m_pNodeTable[3]:getContentSize().height then
            self.m_pTableView[3]:setContentOffset(cc.p(self.m_pTableView[3]:getContentOffset().x,4))
        end
    elseif nType == 4 then
        self.m_pNodeLeftMsg:hide()
        self.m_pNodeLeftPlayer:hide()
        self.m_pNodeRightMsg:hide()
        self.m_pNodeRightPlayer:show()
        self:updatePlayersList()
        self.m_pTableView[4]:reloadData()
    end
end

function ChatLayerNew:calculateChatHeight()

    local chatnum = UsrMsgManager.getInstance():getChatDataNum()
    if chatnum > MaxViewChat then
        chatnum = chatnum - MaxViewChat
    else
        chatnum = 0
    end

    self.m_vecTextHeight = {}
    local allHeight = 0
    local nCount = UsrMsgManager.getInstance():getChatDataNum()
    for i= chatnum + 1, nCount do
        local tmp = UsrMsgManager.getInstance():getChatDataAtIndex(i)
        local strNick = tmp.strSenderName
        local strContent = tmp.strMsg
        local strText = ""
        if tmp.nMsgType == 0 then
            strText = string.format("%s：%s",strNick, strContent)
        elseif tmp.nMsgType == 1 then
            strText = string.format(LuaUtils.getLocalString("CHAT_8"), strNick, tmp.dwSendUserID, strContent)
        elseif  tmp.nMsgType == 2 then
            strText = string.format("(%s)%s",strNick, strContent)
        end
        local hegiht = self:_getLabelHeightByString(strText)
        table.insert(self.m_vecTextHeight, hegiht)
        allHeight = allHeight + hegiht
    end
    return allHeight
end

function ChatLayerNew:_getLabelHeightByString(strText)
    local strvec = string.split(strText,"\n") -- 处理包含换行符的段落
    local height = 0
    for i = 1, #strvec do
        local strwidth = LuaUtils.getStrPlaceHolder(strvec[i]) * self.m_asciiWidth
        local rows = math.floor(strwidth / MaxTextWidth) + 1
        height = height + rows * self.m_asciiHeight
    end
    height = height + 45
    return height
end

function ChatLayerNew:updatePlayersList()

    if self.m_pUpdateAll then
        self.m_pUserData = CUserManager.getInstance():getUserInfoTable()
    end

    local onLineCount = #self.m_pUserData
    local strOnLine =  string.format("当前在线人数：%d", onLineCount)
    self.m_pLbLeftOnLineNum:setString(strOnLine)
    self.m_pLbRightOnLineNum:setString(strOnLine)

    self.m_vecPlayersList = {}
    for i=1, onLineCount do 
        local userInfo = self.m_pUserData[i]
        if userInfo.dwUserID == PlayerInfo.getInstance():getUserID() then --自己放到最上面
            table.insert(self.m_vecPlayersList, 1, userInfo)
        else
            table.insert(self.m_vecPlayersList, userInfo)
        end
    end

    local userID = PlayerInfo.getInstance():getUserID()
    table.sort(self.m_vecPlayersList, function(lhr, rhr)
        local vipLv = lhr.dwUserID == userID and 19 or lhr.nVipLev
        local vipLv2 = rhr.dwUserID == userID and 19 or rhr.nVipLev
        return vipLv > vipLv2
    end)

end


function ChatLayerNew:event_update_chat(msg)
    local show_msg_index = 0
    if self.m_pShowViewList[1] then
        show_msg_index = 1
        if self.m_pLeftMsgArmature then
            self.m_pLeftMsgArmature:show()
        end
        self.m_pBtnLeftMsg:setOpacity(255)
    elseif self.m_pShowViewList[3] then
        show_msg_index = 3
        if self.m_pRightMsgArmature then
            self.m_pRightMsgArmature:show()
        end
        self.m_pBtnRightMsg:setOpacity(255)
    end

    if self.m_pNodeAll:isVisible() == false or (self.m_nViewType%2) == 0 then
        return
    end 
    self:updateShowView(show_msg_index)


end

function ChatLayerNew:event_update_user_list(msg)
    if not self.m_pUpdateAll then return end
    local onLineCount = CUserManager.getInstance():getUsrListCount()
    self.m_pOnLineLeftNum:setString(onLineCount)
    self.m_pOnLineRightNum:setString(onLineCount)
    if self.m_olChangeCbFun then self.m_olChangeCbFun(onLineCount) end
end

-- 设置在线人数发生变化时候的回调 回调参数为在线人数
function ChatLayerNew:setOLChangeCB(cbFun)
    if "function" == type(cbFun) then
        self.m_olChangeCbFun = cbFun
    end
end

function ChatLayerNew:onLabaClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    --竖屏不提供充值界面
    if isPortraitView() then 
        FloatMessage:getInstance():pushMessage(LuaUtils.getLocalString("STRING_243"))
        return
    end
    --显示vip等级不足
    SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_MESSAGEBOX, "up-vip") 
end

-- 收缩聊天窗口
function ChatLayerNew:onReturnClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    self:shrinkView()
end

--设置回调函数
function ChatLayerNew:setChatCallFunc(call_func)
    self.callFunc = call_func
end

function ChatLayerNew:onShowViewClicked(sender)
    print("=========sender=======>",sender == self.m_pBtnLeftMsg,sender == self.m_pBtnLeftPlayer,sender == self.m_pBtnRightMsg,sender == self.m_pBtnRightPlayer)
    if self.m_bMoving then
        return
    end
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    self.m_pEmpty:show()
    if self.callFunc and type(self.callFunc) == "function" then
        self.callFunc()----为了解决点击其他玩家按钮时可以把游戏界面的 下拉按钮收缩
    end

    if sender == self.m_pBtnLeftMsg then
        self.m_pBtnLeftMsg:setVisible(self.m_pKeepBtnShowList[1])
    elseif sender == self.m_pBtnLeftPlayer then
        self.m_pBtnLeftPlayer:setVisible(self.m_pKeepBtnShowList[2])
    elseif sender == self.m_pBtnRightMsg then
        self.m_pBtnRightMsg:setVisible(self.m_pKeepBtnShowList[3])
    elseif sender == self.m_pBtnRightPlayer then
        self.m_pBtnRightPlayer:setVisible(self.m_pKeepBtnShowList[4])
    end


    self.m_bMoving = true
    self.m_pNodeAll:show()
    self.m_pNodeAll:stopAllActions()

    local add_move_len = -670
    local node_all_pos_x = self.m_NodeAllOffset[1]
    if sender == self.m_pBtnLeftMsg then
        self:updateShowView(1)
    elseif sender == self.m_pBtnLeftPlayer then
        self:updateShowView(2)
    elseif sender == self.m_pBtnRightMsg then
        self:updateShowView(3)
        add_move_len = -add_move_len
        node_all_pos_x = - node_all_pos_x
    elseif sender == self.m_pBtnRightPlayer then
        self:updateShowView(4)
        add_move_len = -add_move_len
        node_all_pos_x = - node_all_pos_x
    end

    self.m_pNodeAll:setPosition(cc.p(add_move_len+node_all_pos_x,self.m_pNodeAll:getPositionY()+self.m_NodeAllOffset[2]))
    local move = cc.MoveTo:create(0.6, cc.p(node_all_pos_x,self.m_pNodeAll:getPositionY()))
    local ease = cc.EaseExponentialOut:create(move)
    local callback = cc.CallFunc:create(function ()
        self.m_bMoving = false
    end)
    local seq = cc.Sequence:create(ease, callback);
    self.m_pNodeAll:runAction(seq)
end

function ChatLayerNew:shrinkView()
    if self.m_bMoving then
        return
    end

    self.m_pEmpty:hide()

    if self.m_pLeftMsgArmature then
        self.m_pLeftMsgArmature:hide()
    end
    if self.m_pRightMsgArmature then
        self.m_pRightMsgArmature:hide()
    end

    local add_move_len = -670
    local node_all_pos_x = self.m_NodeAllOffset[1]
    if self.m_nViewType == 1 then
        self.m_pBtnLeftMsg:setOpacity(255*0.3)
        self.m_pBtnLeftMsg:setVisible(true and self.m_pShowBtn)
    elseif self.m_nViewType == 2 then
        self.m_pBtnLeftPlayer:setVisible(true and self.m_pShowBtn)
    elseif self.m_nViewType == 3 then
        self.m_pBtnRightMsg:setOpacity(255*0.3)
        self.m_pBtnRightMsg:setVisible(true and self.m_pShowBtn)
        add_move_len = -add_move_len
        node_all_pos_x = - node_all_pos_x
    elseif self.m_nViewType == 4 then
        self.m_pBtnRightPlayer:setVisible(true and self.m_pShowBtn)
        add_move_len = -add_move_len
        node_all_pos_x = - node_all_pos_x
    end

    self.m_pShowBtn = true

    self.m_bMoving = true
    self.m_pNodeAll:stopAllActions()
    self.m_pNodeAll:setPosition(cc.p(node_all_pos_x,self.m_pNodeAll:getPositionY()))
    local move = cc.MoveTo:create(0.6, cc.p(add_move_len+node_all_pos_x,self.m_pNodeAll:getPositionY()))
    local ease = cc.EaseExponentialOut:create(move)
    local callback = cc.CallFunc:create(function ()
        self.m_pNodeAll:setVisible(false)
        self.m_pNodeAll:setPosition(cc.p(node_all_pos_x,self.m_pNodeAll:getPositionY()-self.m_NodeAllOffset[2]))
        self.m_bMoving = false
    end)
    local seq = cc.Sequence:create(ease, callback);
    self.m_pNodeAll:runAction(seq)
end

--设置玩家列表是否是所有玩家
function ChatLayerNew:setPlayerUpdateWay(state_way)
    self.m_pUpdateAll = state_way
end

function ChatLayerNew:updatePlayerNumber(player_num)
    self.m_pOnLineLeftNum:setString(player_num)
    self.m_pOnLineRightNum:setString(player_num)
    if self.m_olChangeCbFun then self.m_olChangeCbFun(player_num) end
end

function ChatLayerNew:updateOutPlayerData(player_data)
    self.m_pUserData = player_data
    self:updatePlayerNumber(#player_data)
end

--供其他类调用
function ChatLayerNew:onOutLeftMsgClicked()
    self.m_pShowBtn = false
    self:onShowViewClicked(self.m_pBtnLeftMsg)
end

function ChatLayerNew:onOutLeftPlayerClicked()
    self.m_pShowBtn = false
    self:onShowViewClicked(self.m_pBtnLeftPlayer)
end

function ChatLayerNew:onOutRightMsgClicked()
    self.m_pShowBtn = false
    self:onShowViewClicked(self.m_pBtnRightMsg)
end

function ChatLayerNew:onOutRightPlayerClicked()
    self.m_pShowBtn = false 
    self:onShowViewClicked(self.m_pBtnRightPlayer)
end

function ChatLayerNew:setBtnVisible(left_msg_vis,left_player_vis,right_msg_vis,right_player_vis)
    --获取远程数据是否开启
    local is_hide_msg = GameListManager.getInstance():getGameSwitch(G_CONSTANTS.GAMESWITCH.SPEAKER)
    local is_hide_player = GameListManager.getInstance():getGameSwitch(G_CONSTANTS.GAMESWITCH.USERLIST)
    self.m_pBtnLeftMsg:setVisible(left_msg_vis and not is_hide_msg)
    self.m_pBtnLeftPlayer:setVisible(left_player_vis and not is_hide_player)
    self.m_pBtnRightMsg:setVisible(right_msg_vis and not is_hide_msg)
    self.m_pBtnRightPlayer:setVisible(right_player_vis and not is_hide_player)
end

function ChatLayerNew:setNodeAllOffset(offX, offY)
    self.m_NodeAllOffset[1] = offX
    self.m_NodeAllOffset[2] = offY
end

function ChatLayerNew:setLeftBtnOffsetX(offx)
    self.m_pBtnLeftMsg:setPositionX(self.m_pBtnLeftMsg:getPositionX()+offx)
    self.m_pBtnLeftPlayer:setPositionX(self.m_pBtnLeftPlayer:getPositionX()+offx)
end

function ChatLayerNew:setRightBtnOffsetX(offx)
    self.m_pBtnRightMsg:setPositionX(self.m_pBtnRightMsg:getPositionX()+offx)
    self.m_pBtnRightPlayer:setPositionX(self.m_pBtnRightPlayer:getPositionX()+offx)
end

function ChatLayerNew:setLeftBtnOffsetY(offy)
    self.m_pBtnLeftMsg:setPositionY(self.m_pBtnLeftMsg:getPositionY()+offy)
    self.m_pBtnLeftPlayer:setPositionY(self.m_pBtnLeftPlayer:getPositionY()+offy)
end

function ChatLayerNew:setRightBtnOffsetY(offy)
    self.m_pBtnRightMsg:setPositionY(self.m_pBtnRightMsg:getPositionY()+offy)
    self.m_pBtnRightPlayer:setPositionY(self.m_pBtnRightPlayer:getPositionY()+offy)
end

function ChatLayerNew:getLeftBtnPosX()
    --默认是左边两个横坐标相同
    return self.m_pBtnLeftMsg:getPositionX()
end

function ChatLayerNew:getLeftBtnPosY()
    --默认是左边两个纵坐标间距相同
    return self.m_pBtnLeftPlayer:getPositionY()
end

function ChatLayerNew:getRightBtnPosX()
    --默认是右边两个横坐标相同
    return self.m_pBtnRightMsg:getPositionX()
end

function ChatLayerNew:getRightBtnPosY()
    --默认是左边两个纵坐标间距相同
    return self.m_pBtnRightPlayer:getPositionY()
end

function ChatLayerNew:setLeftBtnMsgOffsetY(offy)
    self.m_pBtnLeftMsg:setPositionY(self.m_pBtnLeftMsg:getPositionY()+offy)
end

function ChatLayerNew:setLeftBtnPlayerOffsetY(offy)
    self.m_pBtnLeftPlayer:setPositionY(self.m_pBtnLeftPlayer:getPositionY()+offy)
end

function ChatLayerNew:setShowBtnVisible(vis_1,vis_2,vis_3,vis_4)
    self.m_pKeepBtnShowList = {vis_1,vis_2,vis_3,vis_4}
end


return ChatLayerNew