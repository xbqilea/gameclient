
local HNlayer = import("..HNLayer")
local CommonRoom = class("CommonRoom",function()
    return HNlayer.new()
end) 
local BankLayer = import("..childLayer.BankLayer")
local GameListConfig    = import("..config.GameList")         --等级场配置

local UPDATE_ROOM_TIME = 120.0 --更新房间数据时间

function CommonRoom:ctor() 
    self._roomList = nil
        addMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS, handler(self, self.onPlayerInfoUpdated))
    ToolKit:registDistructor( self, handler(self, self.onDestory) )
end

function CommonRoom:init(child)
    self.m_nGameKindID = 0
    self.m_vecRoomList = {}
    self.m_child = child

    --var
    self.m_RoomView = nil
    self.m_RooMLayer = nil
    self.m_ImageHead = nil
    self.m_pImageBank = nil
    self.m_ImageLevel = nil
    self.m_ImageState = nil
    self.m_bState = nil
    self.m_LabelName = nil
    self.m_LabelGold = nil
    self.m_LabelBank = nil
    self.m_pNodeListTop = nil
    self.m_pNodeListBar = nil
    self.m_pNodeMM = nil
    self.m_pNodeListGame = nil
    self.m_pBtnLeftArrow = nil
    self.m_pBtnRightArrow = nil
    self.m_PathMusic = nil

    --进房间清空体验房类型
    --PlayerInfo.getInstance():setServerType(0)
end  
function CommonRoom:onDestory()  
    removeMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS)  
end

function CommonRoom:setScrollView(roomView, roomLayer, roomList) --设置滑动界面

    self.m_RoomView = roomView
    self.m_RooMLayer = roomLayer
    self.m_pNodeListGame = roomList
    self.m_vecRoomList = self:getGameRoomList()

    --房间列表适配
    local size_inner = self.m_RooMLayer:getContentSize()
    self.m_RoomView:setContentSize(cc.size(display.width, size_inner.height))
    --self.m_RoomView:setInnerContainerSize(size_inner)
    self.m_RoomView:setScrollBarEnabled(false)

    if roomList == nil then return end

    --房间列表位置
    local vec_posX = {}
    for i, room in pairs(self.m_pNodeListGame) do
        room:setVisible(false)
        room:setTag(self.m_ImageState[i]:getTag())
        vec_posX[i] = room:getPositionX()
    end

    --房间显示列表
    local vec_room = {}
    for _, room in pairs(self.m_pNodeListGame) do
        for _, data in pairs(self.m_vecRoomList) do
            if data.dwBaseScore == room:getTag() then
                room:setVisible(true)
                table.insert(vec_room, room)
                break
            end
        end
    end

    --房间列表移动位置
    for i, room in pairs(vec_room) do
        room:setPositionX(vec_posX[i])

        if i == #vec_room then
            local size_inner = self.m_RoomView:getContentSize()
            local size_last = room:getContentSize().width / 2
            local size_width = math.max(vec_posX[i] + size_last, display.width)
            local size_reset = cc.size(size_width, size_inner.height)
            self.m_RoomView:setInnerContainerSize(size_reset)
        end
    end
end

function CommonRoom:setButtonReturn(button) --设置返回
    button:addTouchEventListener(handler(self, self.onReturnClicked))
    button:setPositionX( 100 )
    button:setPositionY( 680 )
end

function CommonRoom:setButtonRule(button) --设置规则
    button:addTouchEventListener(handler(self, self.onRuleClicked))
    button:setPositionX( 240 )
    button:setPositionY( 680 )
    button:setVisible(false)
end

function CommonRoom:setButtonStart(button) --设置开始
    button:addTouchEventListener(handler(self, self.onQuickClicked))
end

function CommonRoom:setButtonRoom(buttons) --设置房间(多个按钮controlButton)
    for i, button in pairs(buttons) do
        print("button tag"..button:getTag())
        button:addTouchEventListener(handler(self, self.onRoomTouchInside))
    end
end

function CommonRoom:setButtonRoom2(buttons) --设置房间(多个按钮controlButton)
    for i, button in pairs(buttons) do
        button:registerControlEventHandler(handler(self, self.onRoomTouchDown),    cc.CONTROL_EVENTTYPE_TOUCH_DOWN)
        button:registerControlEventHandler(handler(self, self.onRoomTouchOutside), cc.CONTROL_EVENTTYPE_TOUCH_UP_OUTSIDE)
        button:registerControlEventHandler(handler(self, self.onRoomTouchInside2),  cc.CONTROL_EVENTTYPE_TOUCH_UP_INSIDE)
    end
end

function CommonRoom:setButtonBank(button)
    button:addTouchEventListener(handler(self, self.onBankClicked))
end

function CommonRoom:setImageLevel(image) --设置等级
    if self.m_ImageLevel == nil then
        self.m_ImageLevel = image
    end
     self.m_ImageLevel:setVisible(false)
    local vipLv = 0--PlayerInfo.getInstance():getVipLevel()
    local vip_path = string.format("hall/plist/vip/img-vip%d.png", vipLv)
    --self.m_ImageLevel:loadTexture(vip_path, ccui.TextureResType.plistType)
end

function CommonRoom:setImageHead(image) --设置头像

    if self.m_ImageHead == nil then
        self.m_ImageHead = image
    end 
	local index = Player:getFaceID()
	local str =ToolKit:getHead(index) 
	self.m_ImageHead:loadTexture(str,1)
end

function CommonRoom:setImageFrame(image) --设置头像框
    local frame = 0--PlayerInfo.getInstance():getFrameID()
    local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", frame)
    --image:loadTexture(framePath, ccui.TextureResType.plistType)
end

function CommonRoom:setImageState(images, small) --设置状态(多个图片imageview)
    
    if self.m_ImageState == nil then
        self.m_ImageState = images
    end
    if self.m_bState == nil then
        self.m_bState = small
    end

    self.m_vecRoomList = self:getGameRoomList()

    local roomData = {}
    for i, v in pairs(self.m_ImageState) do
        local tag = v:getTag()
        roomData[tag] = { [1] = 0, [2] = 0, }
    end
    for i, v in pairs(self.m_vecRoomList) do
        if roomData[v.dwBaseScore] then
            roomData[v.dwBaseScore][1] = roomData[v.dwBaseScore][1] + v.dwOnLineCount
            roomData[v.dwBaseScore][2] = roomData[v.dwBaseScore][2] + v.dwFullCount
        end
    end
    for i, v in pairs(self.m_ImageState) do
        local tag = v:getTag()
        local path = self:getImageByNumber(roomData[tag][1], roomData[tag][2], self.m_bState)
        v:loadTexture(path, ccui.TextureResType.plistType)
    end
end

function CommonRoom:setLabelName(label) --设置昵称

    if self.m_LabelName == nil then
        self.m_LabelName = label
    end

    local strName = Player:getNickName()
    self.m_LabelName:setString(strName)
end

function CommonRoom:setLabelGold(label) --设置金币

    if self.m_LabelGold == nil then
        self.m_LabelGold = label
    end

    local goldNum = Player:getGoldCoin()
    self.m_LabelGold:setString(goldNum)
end
function CommonRoom:onPlayerInfoUpdated( __msgName )
   
  	 local goldNum = Player:getGoldCoin() 
    self.m_LabelGold:setString(goldNum)
end
function CommonRoom:setLabelBank(label) --设置存款

    if self.m_LabelBank == nil then
        self.m_LabelBank = label
    end

    local goldNum = 0--PlayerInfo.getInstance():getUserInsure()
    --local strGold = LuaUtils.getFormatGoldAndNumber(goldNum)
    --self.m_LabelBank:setString(strGold)
end

function CommonRoom:adaptInfoTop(nodeLeft, nodeRight) --top自适应位置
    
    local diffX = (display.width - 1624) / 2

    for i, v in pairs(nodeLeft) do
        v:setPositionX(v:getPositionX() + diffX)
    end
    for i, v in pairs(nodeRight) do
        v:setPositionX(v:getPositionX() + diffX)
    end

    local diffY = (display.height - 750) / 2

    for i, v in pairs(nodeLeft) do
        v:setPositionY(v:getPositionY() + diffY)
    end
    for i, v in pairs(nodeRight) do
        v:setPositionY(v:getPositionY() + diffY)
    end
end

--function CommonRoom:adaptInfoBar(nodeList) --bar自适应位置,平均分散 
--    local posList = {}
--    for i, v in pairs(nodeList) do
--        posList[i] = {}
--        posList[i].width = v:getContentSize().width
--        posList[i].posX = v:getPositionX()
--    end

--    local tempWidth = 0
--    for i, v in pairs(posList) do
--        tempWidth = tempWidth + v.width
--    end

--    local spaceWidth = math.floor((display.width - tempWidth) / (#nodeList))
--    spaceWidth = math.max(spaceWidth, 0)
--    for i = 1, #nodeList do
--        if i == 1 then
--            posList[i].nextX = spaceWidth / 2
--        else
--            posList[i].nextX = posList[i - 1].nextX + posList[i - 1].width + spaceWidth
--        end
--    end

--    local diffX = (display.width - 1624) / 2
--    for i = 1, #nodeList do
--        posList[i].nextX = posList[i].nextX + diffX
--    end

--    for i, v in pairs(nodeList) do
--        v:setPositionX(posList[i].nextX)
--    end
--end

function CommonRoom:adaptInfoBar(nodeList) --bar自适应位置,开始靠右,存款右移,名字左移 
    --开始按钮紧靠右边
    local nodeStart = nodeList[#nodeList]
    local nodeStartX = nodeStart:getPositionX()
    local nodeSize = nodeStart:getContentSize()
    local nodeStartX2 = display.width - nodeSize.width - (display.width - 1624) / 2
    nodeStart:setPositionX(nodeStartX2)

    --玩家框向左移动/金币框不动位置/银行框向右移动
    local nodeOffset = nodeStartX2 - nodeStartX
    nodeList[1]:setPositionX(nodeList[1]:getPositionX() - nodeOffset / 2)
    nodeList[3]:setPositionX(nodeList[3]:getPositionX() + nodeOffset / 2)
    --nodeList[4]:setPositionX(nodeList[3]:getPositionX() + nodeOffset / 4)
end

function CommonRoom:adaptInfoBar3(nodeList) --bar自适应位置,开始右移,其他左移
    
    --玩家框向左移动/金币框向左移动/银行框不动/开始向右移动
    local nodeOffsetX = (display.width - 1334) / 2
    nodeList[1]:setPositionX(nodeList[1]:getPositionX() - nodeOffsetX / 2)
    nodeList[2]:setPositionX(nodeList[2]:getPositionX() - nodeOffsetX / 4)
    nodeList[4]:setPositionX(nodeList[4]:getPositionX() + nodeOffsetX / 2)
end

function CommonRoom:adaptRoomList()
    

end

function CommonRoom:playActionTop(nodeList, distance) --top进场动画
    
    local distance = distance or -150

    if self.m_pNodeListTop == nil then
        self.m_pNodeListTop = nodeList
    end

    local function runTopAction(node)
        local actionStart = cc.MoveBy:create(0, cc.p(0, - distance))
        local actionStop = cc.EaseBackOut:create(cc.MoveBy:create(0.4, cc.p(0, distance)))
        local action = cc.Sequence:create(actionStart, actionStop)
        node:runAction(action)
    end

    for i, v in pairs(self.m_pNodeListTop) do
        runTopAction(v)
    end
end

function CommonRoom:playActionBar(nodeList, distance) --bar进场动画

    local distance = distance or 250

    if self.m_pNodeListBar == nil then
        self.m_pNodeListBar = nodeList
    end

    local function runTopAction(node)
        --local actionStart = cc.MoveBy:create(0, cc.p(0, - distance))
        --local actionStop = cc.EaseBackOut:create(cc.MoveBy:create(0.4, cc.p(0, distance)))
        --local action = cc.Sequence:create(actionStart, actionStop)
        --node:runAction(action)

        node:setOpacity(0)
        local posx, posy = node:getPosition()
        node:setPosition( posx, posy - 200)
        local moveAct = cc.MoveTo:create(0.4, cc.p(posx,posy)) 
        local spawnAct = cc.Spawn:create(cc.FadeIn:create(0.4), moveAct)
        node:runAction( spawnAct )
    end

    for i, v in pairs(self.m_pNodeListBar) do
        runTopAction(v)
    end
end

function CommonRoom:playActionList(nodeList) --房间进场动画(向左移动回弹)
    
    local function runListAction(node, i)
        local fDelayTime = 0.1
        local fMoveTime = 0.35
        local fMoveDistance = 300
        local start = cc.MoveBy:create(0, cc.p(fMoveDistance, 0))
        local delay = cc.DelayTime:create(fDelayTime + (i - 1) * 0.03)
        local show = cc.Show:create()
        local fade = cc.FadeIn:create(fMoveTime * 0.5)
        local move = cc.EaseBackOut:create(cc.MoveBy:create(fMoveTime, cc.p(-fMoveDistance, 0)))
        local spawn = cc.Spawn:create(move, fade)
        local action = cc.Sequence:create(start, delay, show, spawn)
        node:setOpacity(0)
        node:setVisible(false)
        node:runAction(action)
    end

    for i, node in pairs(self.m_pNodeRoomList) do
        if node:isVisible() then
            runListAction(node, i)
        end
    end
end

function CommonRoom:playActionList2(nodeList) --房间进场动画(放大回弹)

    if self.m_pNodeListGame == nil then
        self.m_pNodeListGame = nodeList
    end

    local function runListAction(node, i)
        --放大节点
        local actionDelay = cc.DelayTime:create((i - 1) * 0.05)
        local actionShow = cc.Show:create()
        local actionScale = cc.EaseBackOut:create(cc.ScaleTo:create(0.25, 1.0))
        local action = cc.Sequence:create(actionDelay, actionShow, actionScale)
        local posX, posY = node:getPosition()
        local size = node:getContentSize()
        node:setScale(0.01)
        node:setPosition(posX + size.width / 2, posY + size.height / 2)
        node:setAnchorPoint(0.5, 0.5)
        node:setVisible(false)
        node:runAction(action)
    end
    for i, node in pairs(self.m_pNodeListGame) do
        runListAction(node, i)
    end
end

function CommonRoom:playActionMM(node) --进场美女动画(右移)
    
    if self.m_pNodeMM == nil then
        self.m_pNodeMM = node
    end

    local fDelayTime = 0.1
    local fMoveTime = 0.35
    local fMoveDistance = -300
    local start = cc.MoveBy:create(0, cc.p(fMoveDistance, 0))
    local delay = cc.DelayTime:create(fDelayTime)
    local show = cc.Show:create()
    local fade = cc.FadeIn:create(fMoveTime * 0.5)
    local move = cc.EaseBackOut:create(cc.MoveBy:create(fMoveTime, cc.p(-fMoveDistance, 0)))
    local spawn = cc.Spawn:create(move, fade)
    local action = cc.Sequence:create(start, delay, show, spawn)
    self.m_pNodeMM:setOpacity(0)
    self.m_pNodeMM:setVisible(false)
    self.m_pNodeMM:runAction(action)
end

function CommonRoom:createArrow(nodeView, nodeLayer, nodeRoom, moveCall)

    if nodeView:getInnerContainerSize().width <= display.width then
        nodeView:addEventListener(function(sender, event)
            local pos = sender:getInnerContainerPosition()
            --print("pos", pos.x, pos.y)
            if moveCall then
                moveCall(pos.x)
            end
        end)
        return
    end

    --左箭头
    local pBkgImg = cc.Sprite:createWithSpriteFrameName("hall/plist/hall/gui-hall-arrow-left.png")
    local pNormalSprite = cc.Scale9Sprite:createWithSpriteFrame(pBkgImg:getSpriteFrame())
    local pClickSprite = cc.Scale9Sprite:createWithSpriteFrame(pBkgImg:getSpriteFrame())
    self.m_pBtnLeftArrow = cc.ControlButton:create(pNormalSprite)
    self.m_pBtnLeftArrow:setBackgroundSpriteForState(pClickSprite,cc.CONTROL_STATE_HIGH_LIGHTED)
    self.m_pBtnLeftArrow:setAnchorPoint(cc.p(0, 0.5))
    self.m_pBtnLeftArrow:setPosition(0 - display.cx + 10, 0)
    self.m_pBtnLeftArrow:addTo(nodeRoom)
    
    --左箭头动作
    local seq = cc.Sequence:create(cc.MoveBy:create(0.4, cc.p(-8,0)),cc.MoveBy:create(0.4, cc.p(8,0)))
    self.m_pBtnLeftArrow:runAction(cc.RepeatForever:create(seq))

    --左箭头监听
    self.m_pBtnLeftArrow:registerControlEventHandler(function()
        g_GameMusicUtil:playSound("public/sound/sound-button.mp3")
        nodeView:scrollToPercentHorizontal(0, 0.5, true)
    end, cc.CONTROL_EVENTTYPE_TOUCH_UP_INSIDE)
  
    --右箭头
    local pBkgImg2 = cc.Sprite:createWithSpriteFrameName("hall/plist/hall/gui-hall-arrow-right.png")
    local pNormalSprite2 = cc.Scale9Sprite:createWithSpriteFrame(pBkgImg2:getSpriteFrame())
    local pClickSprite2 = cc.Scale9Sprite:createWithSpriteFrame(pBkgImg2:getSpriteFrame())
    self.m_pBtnRightArrow = cc.ControlButton:create(pNormalSprite2)
    self.m_pBtnRightArrow:setBackgroundSpriteForState(pClickSprite2,cc.CONTROL_STATE_HIGH_LIGHTED)
    self.m_pBtnRightArrow:addTo(nodeRoom)
    self.m_pBtnRightArrow:setAnchorPoint(cc.p(1, 0.5))
    self.m_pBtnRightArrow:setPosition(display.cx - 10, 0)

    --右箭头动作
    local seq2 = cc.Sequence:create(cc.MoveBy:create(0.4, cc.p(8,0)),cc.MoveBy:create(0.4, cc.p(-8,0)))
    self.m_pBtnRightArrow:runAction(cc.RepeatForever:create(seq2))

    --右箭头监听
    self.m_pBtnRightArrow:registerControlEventHandler(function()
        g_GameMusicUtil:playSound("public/sound/sound-button.mp3")
        nodeView:scrollToPercentHorizontal(100, 0.5, true)
    end, cc.CONTROL_EVENTTYPE_TOUCH_UP_INSIDE)

    --列表监听
    nodeView:addEventListener(function(sender, event)
        local pos = sender:getInnerContainerPosition()
        --print("pos", pos.x, pos.y)

        if 0 <= pos.x and pos.x < 80 then
            self.m_pBtnLeftArrow:setVisible(false)
            self.m_pBtnRightArrow:setVisible(true)
        elseif nodeView:getInnerContainerSize().width + pos.x <= display.width then
            self.m_pBtnLeftArrow:setVisible(true)
            self.m_pBtnRightArrow:setVisible(false)
        end

        if moveCall then
            moveCall(pos.x)
        end
    end)

    self.m_pBtnLeftArrow:setVisible(false)
    self.m_pBtnRightArrow:setVisible(true)
end

function CommonRoom:playBGMusic(pathMusic)
    
    if self.m_PathMusic == nil then
        self.m_PathMusic = pathMusic
    end

    if type(self.m_PathMusic) == "table" then
        local nRandom = math.random(1, #self.m_PathMusic)
        local music = self.m_PathMusic[nRandom]
        g_GameMusicUtil:playBGMusic(music)
        print("Room Play", music)
    end

    if type(self.m_PathMusic) == "string" then
        local music = self.m_PathMusic
        g_GameMusicUtil:playBGMusic(music)
        print("Room Play", music)
    end
end

--公共方法
------------------------------------------------------------
--按钮回调
function CommonRoom:__touch_event__() end

function CommonRoom:onReturnClicked(sender,eventType) --点击返回
    if eventType == ccui.TouchEventType.ended then 
      --  g_GameMusicUtil:playSound("public/sound/sound-close.mp3") 
        --[[if PlayerInfo:getInstance():getIsExperienceGuide() then
            return
        end]]--

        local delayTime = 0.12
        if self.m_pNodeListTop ~= nil then
            for i, v in pairs(self.m_pNodeListTop) do
                v:runAction( cc.MoveBy:create(delayTime, cc.p(0, 200)))
            end
        end
        if self.m_pNodeListBar ~= nil then
            for i, v in pairs(self.m_pNodeListBar) do
                v:runAction( cc.MoveBy:create(delayTime, cc.p(0, -200)))
            end
        end
        if self.m_pNodeListGame ~= nil then
            for i, v in pairs(self.m_pNodeListGame) do
                v:runAction( cc.FadeOut:create( delayTime ))
            end
        end
        if self.m_pNodeMM ~= nil then
            local moveAct = cc.MoveBy:create(delayTime, cc.p(-300, 0))
            local spawnAct = cc.Spawn:create(cc.FadeOut:create(delayTime), moveAct)        
            self.m_pNodeMM:runAction( spawnAct )
        end
        local callback = cc.CallFunc:create(function ()
            self:close()
            --g_GameMusicUtil:playBGMusic()
        end)
        local seq = cc.Sequence:create( cc.DelayTime:create(delayTime) ,callback)
        self:runAction(seq)
    end
end

function CommonRoom:onRuleClicked(sender,eventType) --点击规则
   if eventType~=ccui.TouchEventType.ended then
        return
    end
    g_GameMusicUtil:playSound("public/sound/sound-button.mp3")

    self.m_nGameKindID = self:getGameKindID()

    ----------------------------------------
    if self.m_nGameKindID == 202 --九线拉王
    or self.m_nGameKindID == 206 --水浒传
    or self.m_nGameKindID == 209 --水果机
    or self.m_nGameKindID == 212 --连环夺宝
    ----------------------------------------
    or self.m_nGameKindID == 401 --斗地主
    or self.m_nGameKindID == 402 --二人牛牛
    or self.m_nGameKindID == 403 --二人梭哈
    or self.m_nGameKindID == 404 --通比牛牛
    or self.m_nGameKindID == 408 --炸金花
    or self.m_nGameKindID == 410 --火拼麻将
    or self.m_nGameKindID == 411 --德州扑克
    or self.m_nGameKindID == 413 --抢庄牛牛
    ---------------------------------------
    or self.m_nGameKindID == 352 --大闹天宫
    or self.m_nGameKindID == 361 --金蝉捕鱼
    or self.m_nGameKindID == 362 --李逵捕鱼
    or self.m_nGameKindID == 363 --寻龙夺宝
    ---------------------------------------
    then
        local GameView = require("common.app.GameView")
        local RuleView = GameView.rule(self.m_nGameKindID)
        local diffX = (display.width-1624)/2
        RuleView:setPositionX(diffX)
        RuleView:addTo(self:getParent():getParent(), 49)
            
--    else
--        local CommonRule = require("common.layer.CommonRule")
--        local pRule = CommonRule.new(self.m_nGameKindID)
--        pRule:setPositionX((1624 - display.width) / 2)
--        pRule:addTo(self, 999)
    end
end

function CommonRoom:onQuickClicked(sender,eventType) --点击开始
    if eventType~=ccui.TouchEventType.ended then
        return
    end
    g_GameMusicUtil:playSound("public/sound/sound-button.mp3")
    if g_GameController then
	    g_GameController:releaseInstance()
    end
    g_GameController = require(self._roomList[1].controllerPath):getInstance()
    g_GameController:bindGameTypeId(self._roomList[1].gameId)
    ToolKit:addLoadingDialog(3,"正在进入游戏，请稍等......")
    ConnectManager:send2SceneServer(self._roomList[1].gameId, "CS_C2M_Enter_Req", {self._roomList[1].gameId, "" })
    --房间数据
    --[[self.m_vecRoomList = self:getGameRoomList()

    --上一个游戏是该游戏时
    local nServerID = PlayerInfo.getInstance():getCurrentServerID()
    if nServerID > 0 then
        local room = GameListManager.getInstance():getClientGameByServerId(nServerID)
        if room.wKindID ~= nil and room.wKindID == self.m_nGameKindID then
            local index = 0
            for k, v in pairs(self.m_vecRoomList) do
                if v.wServerID == nServerID --上个房间
                and v.dwMinEnterScore <= PlayerInfo.getInstance():getUserScore() --够分数时
                then
                    PlayerInfo.getInstance():setIsQuickStart(true)
                    self:sendLoginGameMessage(v)
                    return
                end
            end
        end
    end

    local userScore = PlayerInfo.getInstance():getUserScore()
    local score = 0
    --从大往小，默认登录最大分数场
    for i = #self.m_vecRoomList, 1, -1 do
        if userScore >= self.m_vecRoomList[i].dwMinEnterScore then
            score = self.m_vecRoomList[i].dwBaseScore
            break
        end
    end

    --快速登录
    self:loginGame(score, true)]]--

end

function CommonRoom:onRoomTouchDown(sender,eventType) --点击房间
     if eventType~=ccui.TouchEventType.ended then
        return
    end
    g_GameMusicUtil:playSound("public/sound/sound-button.mp3")
    
    print("touch down room", sender:getTag())

--    if sender.nodeClick then
--        sender.nodeClick:setScale(1.1)
--    elseif sender.touchDown then
--        sender:touchDown() --自定义
--    end
end

function CommonRoom:onRoomTouchOutside(sender) --点击房间
    
    print("touch cancel room", sender:getTag())

--    if sender.nodeClick then
--        sender.nodeClick:setScale(1.0)
--    elseif sender.touchOutside then
--        sender:touchOutside() --自定义
--    end
end

function CommonRoom:onRoomTouchInside(pSender, event) --点击房间
    print("touch room", sender:getTag())
    if event == ccui.TouchEventType.began then
        pSender.nodeClick:setScale(1.1)
    elseif event == ccui.TouchEventType.canceled then
        pSender.nodeClick:setScale(1.0)
    elseif event == ccui.TouchEventType.ended then
        pSender.nodeClick:setScale(1.0)
        print("touch click room", sender:getTag())
    end
end

function CommonRoom:onRoomTouchInside2(sender) --点击房间
    local tag = sender:getTag()
    print("touch click room", tag)
    if (not isGameAtomOpen(self._roomList[tag].gameId)) then
        TOAST("即将开放，敬请期待")
    else
        if g_GameController then
            g_GameController:releaseInstance()
        end
        g_GameController = require(self._roomList[tag].controllerPath):getInstance()
        g_GameController:bindGameTypeId(self._roomList[tag].gameId)
        ToolKit:addLoadingDialog(3,"正在进入游戏，请稍等......")
        ConnectManager:send2SceneServer(self._roomList[tag].gameId, "CS_C2M_Enter_Req", {self._roomList[tag].gameId, "" })
    end
end

function CommonRoom:onLeftArrowClicked(pSender,eventType)
     if eventType~=ccui.TouchEventType.ended then
        return
    end
    g_GameMusicUtil:playSound("public/sound/sound-button.mp3")

    self.m_pGameListView:scrollToPercent(0)
end

function CommonRoom:onRightArrowClicked(pSender,eventType)
     if eventType~=ccui.TouchEventType.ended then
        return
    end
    g_GameMusicUtil:playSound("public/sound/sound-button.mp3")

    self.m_pGameListView:scrollToPercent(100)
end

function CommonRoom:onBankClicked(pSender,eventType)
    if eventType~=ccui.TouchEventType.ended then
        return
    end
    g_GameMusicUtil:playSound("public/sound/sound-button.mp3") 
    local BankLayer = BankLayer.new()
    self:addChild(BankLayer)
    --打开银行
--    self:showBankerView()
end

function CommonRoom:showBankerView()
    local GameConfig = GameListConfig[self.m_nGameKindID]
    local msg = nil
    if (GameConfig.SHOWTYPE == 1) then
        msg = 
        {
            strPath = GameConfig.BANKPATH or "hall/csb/BankInGame.csb", 
            show_type = GameConfig.SHOWTYPE,
        }
    else
        msg = GameConfig.BANKPATH or "hall/csb/BankInGame.csb"
    end
    SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_INGAMEBANK, msg)
end

--按钮回调
--------------------------------------------
--事件通知
function CommonRoom:__send_recieve__() end

function CommonRoom:loginGame(score, bQuick)

    PlayerInfo.getInstance():setIsQuickStart(bQuick)

    local rooms = self:getGameRoomByScore(score)

    if table.maxn(rooms) == 0 then
        FloatMessage.getInstance():pushMessage("暂未开放")

    elseif table.maxn(rooms) == 1 then

        -- 请求进入游戏
        self:sendLoginGameMessage(rooms[1])
    else
        --弹出选择房间框
        self:sendShowChooseRoomDlg(rooms[1])

        --寻找符合条件的房间进入
        --local room  =  self:getGameRoomMatch(rooms)
        --self:sendLoginGameMessage(room)
    end
end

--选择房间弹窗
function CommonRoom:sendShowChooseRoomDlg(tmp)
    
    local _event = {
        name = Hall_Events.SHOW_CHOOSE_ROOM_DLG,
        packet = tmp,
   }
   SLFacade:dispatchCustomEvent(Hall_Events.SHOW_CHOOSE_ROOM_DLG,_event)
end

function CommonRoom:sendLoginGameMessage(room) --发送登录游戏请求
    
    local _event = {
        name = Hall_Events.MSG_HALL_CLICKED_GAME,
        packet = room,
    }
    SLFacade:dispatchCustomEvent(Hall_Events.GAMELIST_CLICKED_GAME, _event)
end

function CommonRoom:sendGetRoomList() --刷新房间人数
    
    if self:checkIsNeedUpdateRoom() then 
        local m_nGameKindID = self:getGameKindID()
        CMsgHall:sendGetServerList(self.m_nGameKindID)
    end
end

function CommonRoom:onMsgRecharge() --更新vip等级
    self:setImageLevel()
end

function CommonRoom:onMsgUpdateScore() --更新金币
    self:setLabelGold()
    self:setLabelBank()
end

function CommonRoom:onMsgGetRoomList() --更新房间状态
    self:setImageState()
end

function CommonRoom:onMsgPlayMusic() --恢复背景音乐
    self:playBGMusic()
end

--事件通知
---------------------------------------
--数据存取
function CommonRoom:__data_store__() end

function CommonRoom:checkIsNeedUpdateRoom() --是否更新房间数据
    
    --登录游戏不检查
    if GameListManager.getInstance():getIsLoginGameSucFlag() then
        return false
    end

    --未加载完不检查
    local m_nGameKindID = self:getGameKindID()
    local m_vecGameList = self:getGameRoomList()
    if m_nGameKindID == 0 or table.nums(m_vecGameList) == 0 then
        return false
    end

    --等级场超过一个房间,检查更新
    for i, gameServer in pairs(self.m_vecRoomList) do
        local rooms = self:getGameRoomByScore(gameServer.dwBaseScore)
        if table.maxn(rooms) > 1 then
            return true
        end
    end
    return false
end

function CommonRoom:getImageByNumber(currNumber, fullNumber, bSmall) --获取状态图片
    
    local nIndex = (bSmall == true) and 2 or 1

    local DATA_ROOM_STATE = 
    {
        [1] = { --正常size
            { 100, "hall/plist/roomChoose/gui-image-sign-hot.png",    },
            {  50, "hall/plist/roomChoose/gui-image-sign-more.png",   },
            {  0,  "hall/plist/roomChoose/gui-image-sign-normal.png", },
        },
        [2] = { --小的size
            { 100, "hall/plist/roomChoose/gui-image-sign2-hot.png",    },
            {  50, "hall/plist/roomChoose/gui-image-sign2-more.png",   },
            {   0, "hall/plist/roomChoose/gui-image-sign2-normal.png", },
        },
    }
    currNumber = (currNumber > fullNumber) and fullNumber or currNumber
    
    --local percent = math.floor(currNumber / fullNumber * 100)

    --使用在线数量(火爆>=100/拥挤>=50/正常>=0)
    for i = 1, 3 do
        if currNumber >= DATA_ROOM_STATE[nIndex][i][1] then
            return DATA_ROOM_STATE[nIndex][i][2]
        end
    end
    return DATA_ROOM_STATE[nIndex][1][2]
end

function CommonRoom:getGameKindID() --获取游戏id

    self.m_nGameKindID = CGameClassifyDataMgr.getInstance():getSelectKindID()
    return self.m_nGameKindID
end

function CommonRoom:getGameRoomList() --获取游戏list

    local m_nGameKindID = self:getGameKindID()
    self.m_vecRoomList = GameListManager.getInstance():getStructRoomsByKindIDAndScore(m_nGameKindID, 0)
    return self.m_vecRoomList
end

function CommonRoom:getGameRoomByScore(score) --获取游戏liist

    local m_nGameKindID = self:getGameKindID()
    local rooms = GameListManager.getInstance():getStructRoomsByKindIDAndScore(m_nGameKindID, score)
    return rooms
end

function CommonRoom:getGameRoomMatch(rooms) --获取登录房间

    return GameListManager.getInstance():getMatchRoom(rooms)
end

--数据存储
---------------------------------------

return CommonRoom