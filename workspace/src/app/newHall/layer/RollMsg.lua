--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local GameDefine = require("common.config.GameDefine")
local RollMsgManager    = require("common.manager.RollMsgManager")

local RollMsg = class("RollMsg", cc.Node)

local scheduler = cc.exports.scheduler

--每帧文字移动距离
--1/60秒2个像素->1秒 120个像素
local ROLL_OFFEST = 200
local TIME_PER = 0.6

local RECT_ROLLMSG = cc.rect(0, 0, 450, 36) --剪切框大小
local COLOR_TOUCH = cc.c3b(230, 208, 124) --触摸颜色
local STATE_IDLE = 0 --无消息状态
local STATE_PLAY = 1 --播放状态

local PNG_ICON = "public/image/gui-main-rollmsg-icon.png"
local PNG_MSG = "public/image/gui-main-rollmsg-bg.png"
local PNG_BUTTON = "public/image/gui-texture-null.png"

function RollMsg:ctor()
    self:enableNodeEvents()
    
    self.m_root = nil
    self.m_pLbString = nil
    self.m_nState = nil 
    self.handle = nil
    self.m_vText = {}
    self.m_rollRect = RECT_ROLLMSG
    self.m_CurrentX = 0
    self.m_fCurTime = 0
    self.m_fContentWidth = 0
    self.m_updaeFrameTime = 0
end

function RollMsg:onEnter()
    
--    self:init()
--    self.handle = scheduler.scheduleGlobal(handler(self, self.updateView), 1/60)
end

function RollMsg:onExit()

    if self.handle then
        scheduler.unscheduleGlobal(self.handle)
        self.handle = nil
    end
end

function RollMsg:init(kind)

    --节点
    self.m_root = display.newNode()
    self.m_root:addTo(self)

    --构造界面
    self:initWithSize(kind)

    --初始化
    self.m_nState = STATE_IDLE
    self.m_root:setVisible(false)

    self.handle = scheduler.scheduleGlobal(handler(self, self.updateView), 1/60)
end

function RollMsg:initWithSize(kind)

    local strFileIcon = PNG_ICON
    local strFileBg = PNG_MSG
    local x, y, width, height = self:getPosAndWidth(kind)
    local icon, bg = self:getIMG(kind)
    if icon and bg then 
        strFileIcon = icon
        strFileBg = bg
    end
    self.m_root:setPosition(x , y)
    self.m_rollRect = cc.rect(0,0,width,height)

    self.m_root:removeAllChildren()
    --背景
    local bg = cc.Scale9Sprite:create(strFileBg)
    bg:setPreferredSize(cc.size(self.m_rollRect.width+60,self.m_rollRect.height))
    bg:setPosition(cc.p(0, 0))
    bg:setInsetTop(15)
    bg:setInsetBottom(15)
    bg:setInsetLeft(15)
    bg:setInsetRight(15)
    bg:setName("RollMsg_Bg")
    self.m_root:addChild(bg)

    local icon = cc.Sprite:create(strFileIcon)
    icon:setPosition(cc.p(-bg:getContentSize().width/2+20, 0))
    icon:setName("RollMsg_Icon")
    self.m_root:addChild(icon)

    --剪切窗口
    local stencil = display.newSprite()
    stencil:setTextureRect(self.m_rollRect)

    --剪切节点
    local clipNode = cc.ClippingNode:create(stencil)
    clipNode:setInverted(false)
    clipNode:setPosition(cc.p(10, 0))
    clipNode:addTo(self.m_root)

    --滚动字
    self.m_pLbString = cc.Label:createWithSystemFont("", "Helvetica", 22)
    self.m_pLbString:setAnchorPoint(cc.p(0, 0.5)) 
    self.m_pLbString:addTo(clipNode)
    self.m_pLbString:setVisible(false)
    
    --按钮
    local pButton = ccui.Button:create()
    pButton:loadTextureNormal(PNG_BUTTON, ccui.TextureResType.localType)
    pButton:loadTexturePressed(PNG_BUTTON, ccui.TextureResType.localType)
    pButton:loadTextureDisabled(PNG_BUTTON, ccui.TextureResType.localType)

    self.onTouchMsg = function(layer, sender ,eventType)
        if eventType == ccui.TouchEventType.began then
            if self == nil then return end
            self.m_pColor = self.m_pLbString:getColor()
            self.m_pLbString:setColor(COLOR_TOUCH)
        elseif eventType == ccui.TouchEventType.moved then
        elseif eventType == ccui.TouchEventType.ended then
            if self == nil then return end
            if type(self.m_pColor) ~= "table" then return end
            AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
            SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_ROLLMSG_BOX, self.m_pLbString:getString())
            self.m_pLbString:setColor(self.m_pColor)
            self.m_pColor = cc.WHITE
        elseif eventType == ccui.TouchEventType.canceled then
            if self == nil or self.m_pLbString == nil then return end
            if type(self.m_pColor) ~= "table" then return end
            self.m_pLbString:setColor(self.m_pColor)
            self.m_pColor = cc.WHITE
        end
    end
    pButton:addTouchEventListener(handler(self, self.onTouchMsg))
    self.m_pButton = pButton

    pButton:setAnchorPoint(0.5, 0.5)
    pButton:ignoreContentAdaptWithSize(true)
    pButton:setScale9Enabled(true)
    pButton:setSize(cc.rect(0,0,self.m_rollRect.width,self.m_rollRect.height+20))
    pButton:setPosition(0, 0)
    pButton:addTo(clipNode)

end

--function RollMsg:setShowPosition(x, y)
--    local pos_x = x or display.cx
--    local pos_y = y or display.height - 25.0
--    self.m_root:setPosition(pos_x , pos_y)
--end

--function RollMsg:setShowSize(rect)
--    self:initWithSize(rect)
--end

----重置
--function RollMsg:resetUI(gamekind)
--    local x, y, width, height = self:getPosAndWidth(gamekind)
--    self:setShowPosition(x,y)
--    local rect = cc.rect(0,0,width,height)
--    self:setShowSize(rect)
--end

function RollMsg:addMessage(msg) --添加显示
    if msg == nil then return end

    self.m_pColor = msg.color ~= nil and  LuaUtils.To_RGB(msg.color) or cc.c3b(255,255,255)
    table.insert(self.m_vText, msg)

    local strText = LuaUtils.removeAnthorLine(msg.text)
    local textColor = msg.color ~= nil and LuaUtils.To_RGB(msg.color) or cc.c3b(255,255,255)
    self.m_pLbString:setString(strText)
    self.m_pLbString:setColor(textColor)
end

function RollMsg:showMsg(msg) --显示文字

    self.m_pLbString:setPosition(cc.p(self.m_rollRect.width / 2, 0))
    self.m_CurrentX = self.m_pLbString:getPositionX()
    self.m_fContentWidth = self.m_pLbString:getContentSize().width
    self.m_fCurTime = cc.exports.gettime()
    self.m_pLbString:setVisible(true)
end

function RollMsg:startRoll( kind ) --开始
    
--    self:resetUI(gamekind)
    self:init(kind)
    --播放喇叭
    RollMsgManager.getInstance():ResetRollMsgShowState()
    local msg = RollMsgManager.getInstance():getRollMsgToShow()
    if msg then
        self:addMessage(msg)
        self:openRoll(msg)
    end
end

function RollMsg:openRoll(msg) --打开

    if msg == nil then return end

    self.m_nState = STATE_PLAY
    self.m_root:setVisible(true)
    local bg = self.m_root:getChildByName("RollMsg_Bg")
    local icon = self.m_root:getChildByName("RollMsg_Icon")
    self.m_pLbString:setVisible(false)
    bg:setScale(0.05)
    icon:setVisible(false)

    local time_x = 0.85
    local time_y = 0.25
    local act0 = cc.DelayTime:create(0.2)
    local act1 = cc.Show:create()
    local act2 = cc.ScaleTo:create(time_x, 1.0, 0.03)
    local act3 = cc.ScaleTo:create(time_y, 1.0, 1.00)
    local act4 = cc.CallFunc:create(function()
        self.m_nState = STATE_PLAY
        icon:setVisible(true)
        self:showMsg(msg)
    end)
    local seq = cc.Sequence:create(act0, act1, act2, act3, act4)
    bg:runAction(seq)
end

function RollMsg:closeRoll() --停止滚动
    if self.m_nState == STATE_PLAY then
        self.m_nState = STATE_CLOSE
    else
        return
    end
    local bg = self.m_root:getChildByName("RollMsg_Bg")
    local icon = self.m_root:getChildByName("RollMsg_Icon")
    self.m_pLbString:setVisible(true)
    icon:setVisible(false)

    local time_x = 0.80
    local time_y = 0.20
    local act1 = cc.ScaleTo:create(time_y, 1.00, 0.03)
    local act2 = cc.ScaleTo:create(time_x, 0.01, 0.03)
    local act3 = cc.CallFunc:create(function()
        self.m_root:setVisible(false)
        self.m_nState = STATE_IDLE
    end)
    local act4 = cc.Hide:create()
    local seq = cc.Sequence:create(act1, act2, act3, act4)
    bg:runAction(seq)
end

--移动
function RollMsg:updateView()
    if self.m_nState == STATE_IDLE then
        local curTime = cc.exports.gettime()
        if curTime > self.m_updaeFrameTime + 1 then
            self.m_updaeFrameTime = curTime
            local msg = RollMsgManager.getInstance():getRollMsgToShow()
            if msg ~= nil then
                self:addMessage(msg)
                self:openRoll(msg)
            end
        end
    elseif self.m_nState == STATE_PLAY then
        self:scrollMsg()
    end
end

function RollMsg:scrollMsg() --每帧更新
    if self.m_nState ~= STATE_PLAY then
        return
    end
    if not self.m_pLbString:isVisible() then 
        return
    end
    local beginX =  self.m_CurrentX
    local curTime = cc.exports.gettime()
    local offestTime = curTime - self.m_fCurTime
    local endPosX = beginX - ROLL_OFFEST*offestTime
    endPosX = beginX * (1.0-TIME_PER) + endPosX * TIME_PER

    self.m_pLbString:setPositionX(endPosX)

    self.m_CurrentX = endPosX
    self.m_fCurTime = curTime

    if self.m_CurrentX + self.m_fContentWidth < 0 - RECT_ROLLMSG.width / 2 then
        --移动完成
        RollMsgManager.getInstance():removeRollMsg(self.m_vText[1])
        if RollMsgManager.getInstance():getRollMsgNums() > 0 then
            local msg = RollMsgManager.getInstance():getRollMsgToShow()
            if msg ~= nil then 
                self:addMessage(msg)
                self:showMsg(msg)
            else
                self:closeRoll()
            end
        else
            self:closeRoll()
        end
    end
end
----------------------------------------------------------
function RollMsg:getPosAndWidth(gamekind)
    
    local KindID = gamekind or PlayerInfo.getInstance():getKindID()
    local diffX = (display.width - 1334) / 2
--    --选桌
--    if cc.exports.isInTableChoose() then
--        local data = CGameClassifyDataMgr.getInstance():getLocalClassifyBroadcast_table(KindID)
--        if data and data[1] and data[2] and data[3] and data[4] then
--            return data[1], data[2], data[3], data[4]
--        end
--    end

    --游戏内
    if KindID > 0 then
        if LuaUtils.isIphoneXDesignResolution() then 
            local data = CGameClassifyDataMgr.getInstance():getLocalClassifyRollMsgIphoneX(KindID)
            if data and data[1] and data[2] and data[3] and data[4] then
                return data[1], data[2], data[3], data[4]
            end
        end
        local data = CGameClassifyDataMgr.getInstance():getLocalClassifyRollMsg(KindID)
        if data and data[1] and data[2] and data[3] and data[4] then
            return data[1], data[2], data[3], data[4]
        end
    end

    return 667+diffX, 700, RECT_ROLLMSG.width, RECT_ROLLMSG.height  --默认大厅
end

function RollMsg:getIMG(gamekind)
    local KindID = gamekind or PlayerInfo.getInstance():getKindID()
    if KindID > 0 then
        local data = CGameClassifyDataMgr.getInstance():getLocalClassifyRollMsgIMG(KindID)
        if data and next(data)~= nil then 
            return data[1], data[2]
        end
    end
    return nil
end

cc.exports.RollMsg = RollMsg
return RollMsg
    
--endregion
    
