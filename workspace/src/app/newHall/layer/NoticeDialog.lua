--region NoticeDialog.lua
--Date 2017.06.19.
--Auther JackyXu.
--Desc 游戏公用提示框. 相当于原工程中的 NoticeView.

local NoticeDialog = class("NoticeDialog", FixLayer)

local GameListConfig    = require("common.config.GameList")    --等级场配置

NoticeDialog.NOTICE_LOGOUT         = 1 --切换账号
NoticeDialog.NOTICE_UPDATE         = 2 --游戏有更新
NoticeDialog.NOTICE_GOLD_ARRIVAL   = 3 --充值到帐
NoticeDialog.NOTICE_NO_TRANSFER    = 4 --游客不能转账
NoticeDialog.NOTICE_UNLOCK_GIFT    = 5 --解锁红包
NoticeDialog.NOTICE_UNFINISH_GAME  = 6 --异常中断的小玛丽和钻石免费拉回
NoticeDialog.NOTICE_YUECARD_TIP    = 7 --月卡到期提示
NoticeDialog.NOTICE_COMFIRM_TRANSFER = 8 --PC转账二次确认框

function NoticeDialog:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()

    self.m_pImgBg = nil --背景根节点
    self.m_pBtnOK = nil
    self.m_pInfoLabel = nil
    self.m_pInfoLabel2 = nil
    self.m_nNoticeType = 0
    self.m_nLastTouchTime = 0

    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)

    self:init()
end

function NoticeDialog:init()
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_NORMAL, FixLayer.HIDE_DLG_NORMAL)

    --init csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/NoticeDialog.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("NoticeDialog")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pImageBg     = self.m_pNodeRoot:getChildByName("Image_bg")
    self.m_pImageTitle  = self.m_pImageBg:getChildByName("Image_title")
    self.m_pBtnOK       = self.m_pImageBg:getChildByName("Button_confirm")
    self.m_pBtnCancel   = self.m_pImageBg:getChildByName("Button_cancel")
    self.m_pInfoLabel   = self.m_pImageBg:getChildByName("Label_text")
    self.m_pBtnClose    = self.m_pImageBg:getChildByName("Button_close")
    self.m_pBtnNull        = self.m_pathUI:getChildByName("Panel_2")--空白处关闭

    self.m_pBtnOK:addClickEventListener(handler(self, self.onBtnOkClick))
    self.m_pBtnCancel:addClickEventListener(handler(self, self.onBtnCloseClick))
    self.m_pBtnClose:addClickEventListener(handler(self, self.onBtnCloseClick))
    self.m_pBtnNull:addClickEventListener(handler(self, self.onBtnCloseClick))

    self.m_size = self.m_pImageBg:getContentSize()
end

function NoticeDialog:onEnter()
    self.super:onEnter()
    self:showWithStyle()
    if device.platform == "windows" then
        self:initKeyboard()  
    end
end

function NoticeDialog:onExit()
    self.super:onExit()
    if device.platform == "windows" then
        self:cleanKeyboard()
    end
end

--监听按键
function NoticeDialog:initKeyboard() 
    local function onKeyReleased(keyCode, event)
        event:stopPropagation()
        if keyCode == cc.KeyCode.KEY_ENTER or keyCode == cc.KeyCode.KEY_KP_ENTER then 
            self:onBtnOkClick()
        end
        --双击判断
        local nCurTime = os.time()
        if self.lastKeyCode and self.lastKeyCode == keyCode and self.lastKeyTime and nCurTime - self.lastKeyTime <= 1 then 
            if keyCode == cc.KeyCode.KEY_SPACE then 
                self:onBtnOkClick()
            end
        end
        self.lastKeyCode = keyCode
        self.lastKeyTime = os.time()
    end

    local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
    self.m_pKeyboardListener = cc.EventListenerKeyboard:create()
    self.m_pKeyboardListener:registerScriptHandler(onKeyReleased, cc.Handler.EVENT_KEYBOARD_RELEASED)
    pEventDispatcher:addEventListenerWithFixedPriority(self.m_pKeyboardListener, -132)
end

--移除按键监听
function NoticeDialog:cleanKeyboard()

    if self.m_pKeyboardListener then
        local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
        pEventDispatcher:removeEventListener(self.m_pKeyboardListener)
        self.m_pKeyboardListener = nil
    end
end

function NoticeDialog:setNoticeType(nIndex, data)
    self.m_nNoticeType = nIndex
    if(self.m_pInfoLabel == nil) then
        return
    end

    if (nIndex == NoticeDialog.NOTICE_LOGOUT) then
        -- 帐号登出提示
        self.m_pInfoLabel:setString(LuaUtils.getLocalString("NOTICE_DIALOG_1"))

    elseif (nIndex == NoticeDialog.NOTICE_UPDATE) then
        -- 是否更新到最新版本？
        self.m_pBtnCancel:setVisible(false)
        self.m_pBtnOK:setPosition(self.m_size.width / 2, 120)
        self.m_pInfoLabel:setString(LuaUtils.getLocalString("NOTICE_DIALOG_2"))

    elseif (nIndex == NoticeDialog.NOTICE_GOLD_ARRIVAL) then
        --充值到帐提示
        self.m_pBtnCancel:setVisible(false)
        self.m_pBtnOK:setPosition(self.m_size.width / 2, 120)

        self.m_pInfoLabel:setString(data)

    elseif(nIndex == NoticeDialog.NOTICE_NO_TRANSFER) then
        -- 游客不能转账提示
        local strTips = LuaUtils.getLocalString("GUEST_TRANSFER_NOTICE");
        if PlayerInfo.getInstance():getAccountsType() == 3 then 
            strTips = LuaUtils.getLocalString("GUEST_TRANSFER_NOTICE_1")
        end
        self.m_pInfoLabel:setString(strTips)
        self.m_pBtnOK:setPositionX(self.m_size.width / 2)
        self.m_pBtnCancel:setVisible(false)

    elseif (nIndex == NoticeDialog.NOTICE_UNLOCK_GIFT) then
        --分享解锁红包
        self.m_pInfoLabel:setString(LuaUtils.getLocalString("NOTICE_DIALOG_3"))
        self.m_pBtnCancel:setVisible(false)
        self.m_pBtnOK:setPositionX(self.m_size.width / 2)
    elseif (nIndex == NoticeDialog.NOTICE_UNFINISH_GAME) then
        -- 提示有未完的小玛丽或者钻石免费
        local unServerID = GameListManager.getInstance():getUnfinishedGameKindAndServer()
        local room = GameListManager.getInstance():getClientGameByServerId(unServerID)
        if room.wKindID == nil then 
            self:onMoveExitView(FixLayer.HIDE_NO_STYLE)
            return
        end
        local str = {}
        local GameConfig = GameListConfig[room.wKindID]
        str[1] = GameConfig.GameName
        str[2] = GameConfig[room.dwBaseScore].RoomName
        str[3] = tostring(GameListManager.getInstance():getRoomNumber(room.wKindID,room.dwBaseScore,room.wServerID))
        local strNotice = string.format(LuaUtils.getLocalString("NOTICE_DIALOG_5"),str[1],str[2],str[3])
        self.m_pInfoLabel:setString("")

        local pRichText = ccui.RichText:create()
        pRichText:setPosition(cc.p(self.m_pInfoLabel:getPositionX()+10,self.m_pInfoLabel:getPositionY()-50))
        pRichText:ignoreContentAdaptWithSize(false)
        pRichText:setAnchorPoint(cc.p(0.5,0.5))
        pRichText:setContentSize(cc.size(600,220))--cc.c3b(249,214,32)
        local re1 = ccui.RichElementText:create(0, cc.WHITE, 255, LuaUtils.getLocalString("NOTICE_DIALOG_4"), "hall/font/fzft.ttf", 28)
        local re2 = ccui.RichElementText:create(0, cc.c3b(93,253,255), 255, strNotice, "hall/font/fzft.ttf", 28)
        local re3 = ccui.RichElementText:create(0, cc.WHITE, 255, LuaUtils.getLocalString("NOTICE_DIALOG_6"), "hall/font/fzft.ttf", 28)
        pRichText:pushBackElement(re1)
        pRichText:pushBackElement(re2)
        pRichText:pushBackElement(re3)
        pRichText:addTo(self.m_pInfoLabel:getParent())

        self.m_pBtnCancel:setVisible(false)
        self.m_pBtnOK:setPositionX(self.m_size.width / 2)
    elseif nIndex == NoticeDialog.NOTICE_YUECARD_TIP then
        self.m_pBtnCancel:loadTextureNormal("hall/plist/dialog/gui-card-btn_cancel.png", ccui.TextureResType.plistType)
        self.m_pBtnOK:loadTextureNormal("hall/plist/dialog/gui-card-btn_sure.png", ccui.TextureResType.plistType)
        self.m_pInfoLabel:setString("您的月卡特权即将到期，是否继续购买？")
    elseif nIndex == NoticeDialog.NOTICE_COMFIRM_TRANSFER then
        self.m_pInfoLabel:setString(data)
    end

    return self
end

------------------
-- 按纽响应
function NoticeDialog:onBtnCloseClick()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")

    self:onMoveExitView()

    if self.m_nNoticeType == NoticeDialog.NOTICE_UNFINISH_GAME 
        or self.m_nNoticeType == NoticeDialog.NOTICE_YUECARD_TIP then
        SLFacade:dispatchCustomEvent(Public_Events.MSG_OPEN_HALL_NOTICE)
    end
end

function NoticeDialog:onBtnOkClick()
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
    -- 防连点
    local nCurTime = os.time();
    if self.m_nLastTouchTime and nCurTime - self.m_nLastTouchTime <= 0 then
        return
    end
    self.m_nLastTouchTime = nCurTime

    local nType = self.m_nNoticeType

    if(nType == NoticeDialog.NOTICE_LOGOUT) then 
        -- 切换账号
        self.m_pBtnOK:setEnabled(false)
        --切换帐号，收到0-3后，再返回登录界面
        PlayerInfo.getInstance():Logout()
        self:onMoveExitView(FixLayer.HIDE_NO_STYLE)
        --如果断开，则不能返回登录
        SLFacade:dispatchCustomEvent(Hall_Events.Login_Entry)

    elseif nType == NoticeDialog.NOTICE_UPDATE then
        -- 更新版本
        self:onMoveExitView(FixLayer.HIDE_NO_STYLE)
        cc.exports.closeConnectSocket()
        SLFacade:dispatchCustomEvent(Public_Events.MSG_GAME_REBOOT)

    elseif nType == NoticeDialog.NOTICE_GOLD_ARRIVAL then

        self:onMoveExitView(FixLayer.HIDE_NO_STYLE)

    elseif nType == NoticeDialog.NOTICE_NO_TRANSFER then
        --弹出注册
        self:onMoveExitView(FixLayer.HIDE_NO_STYLE)
        SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_REGISTER)

   elseif nType == NoticeDialog.NOTICE_UNLOCK_GIFT then
       if device.platform == "windows" then
            FloatMessage.getInstance():pushMessage("STRING_244")
            return
        end
        --解锁红包
        if not LuaNativeBridge:getInstance():isWXInstall() then
            FloatMessage.getInstance():pushMessage("ACTIVITY_9")
            return
        end
        self:onMoveExitView(FixLayer.HIDE_NO_STYLE)
        local strConent = LuaUtils.getLocalString("ACTIVITY_11")
        local strUrl = PlayerInfo.getInstance():getWXShareURL()
        LuaNativeBridge.getInstance():shareToWX(2, strConent, strUrl)
    elseif self.m_nNoticeType == NoticeDialog.NOTICE_UNFINISH_GAME then
        --进入未完的游戏
        self:onMoveExitView(self.HIDE_NO_STYLE)
        local unServerID = GameListManager.getInstance():getUnfinishedGameKindAndServer()
        local room = GameListManager.getInstance():getClientGameByServerId(unServerID)
        local _event = {
            name = Hall_Events.MSG_HALL_CLICKED_GAME,
            packet = room,
        }
        SLFacade:dispatchCustomEvent(Hall_Events.GAMELIST_CLICKED_GAME, _event)
    elseif self.m_nNoticeType == NoticeDialog.NOTICE_YUECARD_TIP then
        self:onMoveExitView(self.HIDE_NO_STYLE)
        SLFacade:dispatchCustomEvent(Hall_Events.OPEN_YUECARD_VIEW)
    elseif self.m_nNoticeType == NoticeDialog.NOTICE_COMFIRM_TRANSFER then
        self:onMoveExitView(self.HIDE_NO_STYLE)
        SLFacade:dispatchCustomEvent(Hall_Events.MSG_PC_COMFIRM_TRANSFER)
    end
end

return NoticeDialog
