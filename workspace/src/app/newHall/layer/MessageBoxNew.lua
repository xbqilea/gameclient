-- region *.lua
-- Date
-- 此文件由[BabeLua]插件自动生成
-- endregion

local MessageBoxNew = class("MessageBoxNew", FixLayer)

local MsgBoxPreBiz = require("common.public.MsgBoxPreBiz")
local scheduler    = require("cocos.framework.scheduler")

--local ViewConfig = {
--    ContentMaxSize = cc.size(585, 140), -- 文本框尺寸
--    ContentNormalPos = cc.p(0, 1), -- 普通状态文本显示坐标
--    ContentUpPos = cc.p(0, 25), -- 需要倒计时状态文本显示坐标
--    TimerTextPos = cc.p(0, -72),-- 倒计时文本坐标
--}
--[Comment]
-- msg 消息正文(Localization_cn中索引key,MsgBoxPreBiz中预定义字符串或自定义字符串)
-- useTimer 是否使用倒计时,默认true
-- time 倒计时时间，默认10
-- cbFun 点击确定后的回调
function MessageBoxNew.create(msg, cbFun, useTimer, time)
    return MessageBoxNew:new():init(msg, cbFun, useTimer, time)
end

function MessageBoxNew:ctor()
    self:enableNodeEvents()
    self.super:ctor(self)
end

function MessageBoxNew:init(msg, okFun, useTimer, time)
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_NORMAL, FixLayer.HIDE_DLG_NORMAL)
    
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    local strPath = isPortraitView() and "hall/csb/message-box-p.csb" or "hall/csb/message-box.csb"
    self.m_pathUI = cc.CSLoader:createNode(strPath)
    self.m_pathUI:addTo(self.m_rootUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("MessageBoxDialog")
    local diffX = 145 - (1624-display.width)/2
    if isPortraitView() then 
        self.m_pNodeRoot:setPositionY(diffX)
    else
        self.m_pNodeRoot:setPositionX(diffX)
    end

    self.m_pImgBg           = self.m_pNodeRoot:getChildByName("Image_bg")
    self.m_pImageTitle      = self.m_pImgBg:getChildByName("Image_title")
    self.m_pLbText   = self.m_pImgBg:getChildByName("LB_notice_text")
    self.m_plTimer    = self.m_pImgBg:getChildByName("LB_count_down")

    self.m_pBtnOk = self.m_pImgBg:getChildByName("BTN_sure")
    self.m_pBtnOk:addClickEventListener(handler(self,self.onSureClicked))

    self.m_pBtnNo = self.m_pNodeRoot:getChildByName("BTN_cancel")
    self.m_pBtnNo:addClickEventListener(handler(self,self.onCloseClicked))

    self.m_pBtnClose = self.m_pImgBg:getChildByName("BTN_close")
    self.m_pBtnClose:addClickEventListener(handler(self,self.onCloseClicked))

    self.m_okFun = nil      -- 点击确定回调
    self.m_closeFun = nil   -- 点击关闭回调
    self.m_timeoutFun = nil -- 倒计时结束回调
    self.m_autoClose = true -- 自动关闭

    self.m_time = time and time or 10
    if "function" == type(okFun) then self.m_okFun = okFun end
    if "table" == type(msg) then msg = msg.packet end

    self:initView(msg, useTimer)

    return self
end 

function MessageBoxNew:initView(msg, useTimer)
    local str = nil
    local bizhandle = MsgBoxPreBiz.PreStringBiz[msg]
    if PlayerInfo.getInstance():IsInExperienceRoom() and msg == "go-recharge" then 
        bizhandle = MsgBoxPreBiz.PreStringBizExpe[msg]
    end
    if bizhandle then -- 首先查询有无预定义处理业务
        if bizhandle.ViewStr then
            str = bizhandle.ViewStr()
        end
        if bizhandle.CB_Ok then
            self.m_okFun = bizhandle.CB_Ok
        end
        if bizhandle.CB_Close then
            self.m_closeFun = bizhandle.CB_Close
        end
        if bizhandle.CB_Timeout then
            self.m_timeoutFun = bizhandle.CB_Timeout
        end
        if bizhandle.NoAutoClose then 
            self.m_autoClose = false
        end
    else
        str = LuaUtils.getLocalString(msg) -- 获取本地化配置
    end

    if nil == str then str = msg end

    if self.m_autoClose then -- 需要倒计时
        self.m_plTimer:setVisible(true)
        self.handle = scheduler.scheduleGlobal(handler(self, self.update), 1)
    else
        self.m_plTimer:setVisible(false)
    end

    self.m_pLbText:setString(str)

    if msg == "experience-tips-3" then
        display.loadSpriteFrames("hall/plist/gui-shop.plist", "hall/plist/gui-shop.png")
        self.m_pBtnOk:loadTextureNormal("hall/plist/shop/gui-shop-btn-commit.png", ccui.TextureResType.plistType)
    end

--    local textpos = ViewConfig.ContentNormalPos
--    local label = self:getViewTextNode(str)
--    label:setAnchorPoint(0.5, 0.5)
--    label:setPosition(textpos)
--    label:addTo(self.m_pImgBg)
end

-- 构造能正确显示文本信息的节点
--function MessageBoxNew:getViewTextNode(msg)
--    local label = cc.Label:createWithSystemFont(msg, "Arial", 30)
--    local row = math.floor(label:getContentSize().width / ViewConfig.ContentMaxSize.width)
--    if row > 0 then -- 需要多行显示
--        row = row + 1
--        label:setString(msg)
--        --label:setDimensions(ViewConfig.ContentMaxSize.width, row * label:getContentSize().height)
--        label:setDimensions(ViewConfig.ContentMaxSize.width, -1)
--        label:setAlignment(cc.TEXT_ALIGNMENT_LEFT) -- 多行文字 居左显示
--    else
--        label:setAlignment(cc.TEXT_ALIGNMENT_CENTER) -- 单行文字 居中显示
--    end
--    return label
--end

function MessageBoxNew:onEnter()
    self.super:onEnter()
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_BIG, FixLayer.HIDE_DLG_BIG)
    self:showWithStyle()
    self:setVeilAlpha(200)
    if device.platform == "windows" then
        self:initKeyboard()  
    end
end

function MessageBoxNew:onExit()
    self.super:onExit()
    if self.handle then
        scheduler.unscheduleGlobal(self.handle)
        self.handle = nil
    end
    if device.platform == "windows" then
        self:cleanKeyboard()
    end
end

--监听按键
function MessageBoxNew:initKeyboard() 
    local function onKeyReleased(keyCode, event)
        event:stopPropagation()
        if keyCode == cc.KeyCode.KEY_ENTER or keyCode == cc.KeyCode.KEY_KP_ENTER then 
            self:onSureClicked()
        end
    end

    local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
    self.m_pKeyboardListener = cc.EventListenerKeyboard:create()
    self.m_pKeyboardListener:registerScriptHandler(onKeyReleased, cc.Handler.EVENT_KEYBOARD_RELEASED)
    pEventDispatcher:addEventListenerWithFixedPriority(self.m_pKeyboardListener, -132)
end

--移除按键监听
function MessageBoxNew:cleanKeyboard()

    if self.m_pKeyboardListener then
        local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
        pEventDispatcher:removeEventListener(self.m_pKeyboardListener)
        self.m_pKeyboardListener = nil
    end
end

function MessageBoxNew:onSureClicked()
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
    if self.m_okFun then self.m_okFun() end
    if self.m_autoClose and self.onMoveExitView then
        self:onMoveExitView(self.HIDE_NO_STYLE)
    end
end

function MessageBoxNew:onCloseClicked()
    AudioManager.getInstance():playSound("public/sound/sound-close.mp3")
    if self.m_closeFun then self.m_closeFun() end
    if self.onMoveExitView then
        self:onMoveExitView()
    end
end

function MessageBoxNew:update(value)
    if not self.m_plTimer or not self.m_plTimer:isVisible() then return end

    if self.m_time <= 0 then
        if self.m_timeoutFun then
            self.m_timeoutFun()
        elseif self.m_closeFun then
            self.m_closeFun()
        end

        if self.onMoveExitView then
            self:onMoveExitView()
        end
        return
    else
        -- 使用自减倒计时方式，可能因为外界干扰不准确？
        -- 使用os.time()时间差方式，可能因为用户修改系统时间导致未知业务问题？
        self.m_time = self.m_time - 1
        if self.m_plTimer then
            self.m_plTimer:setString(string.format(LuaUtils.getLocalString("STRING_029"), self.m_time))
        end
    end
end

--设置确定按钮点击回调
function MessageBoxNew:setOkCallBack(cbFun)
    if "function" == type(cbFun) then
        self.m_okFun = cbFun
    else
        print("MessageBoxNew:setOkCallBack need function parm...")
    end
end

--设置关闭按钮点击回调
function MessageBoxNew:setCloseCallBack(cbFun)
    if "function" == type(cbFun) then
        self.m_closeFun = cbFun
    else
        print("MessageBoxNew:setCloseCallBack need function parm...")
    end
end

--设置倒计时结束回调，若不设置，则默认使用关闭按钮回调
function MessageBoxNew:setTimeOutCallBack(cbFun)
    if "function" == type(cbFun) then
        self.m_timeoutFun = cbFun
    else
        print("MessageBoxNew:setTimeOutCallBack need function parm...")
    end
end

return MessageBoxNew