--region Confirm.lua
--Date
--此文件由[BabeLua]插件自动生成
--
--Desc: 确认提示框 （小框,只有确认按纽）

local Confirm = class("Confirm", FixLayer)

function Confirm:ctor()
    self.super:ctor(self)
    -- 底层阴影
    self.m_shadowUI = display.newColorLayer(cc.c4b(0, 0, 0, 200))
    self.m_shadowUI:setCascadeOpacityEnabled(true)
    self.m_shadowUI:setContentSize(_LAYER_SIZE)
    self.m_shadowUI:addTo(self, -1)

    --初始化主界面
    --todo:换DialogConfirm.csb

    --弹出
    self:setTargetShowHideStyle(self, self.SHOW_POPUP, self.HIDE_POPOUT)

    self:showWithStyle()
end

function Confirm:setStringLabel(strText)
      if self.m_pTextLabel ~= nil then 
	    self.m_pTextLabel:setString(strText)
      end 
end

function Confirm:addConfirm( cb )
    
    self.m_callback = cb
    return self
end

function Confirm:addCancel( cb )
    
    self.m_callback_cancel = cb
    return self
end

function Confirm:onClickConfirm()

    self:onMoveExitView()
end

cc.exports.Confirm = Confirm
return cc.exports.Confirm

--endregion
