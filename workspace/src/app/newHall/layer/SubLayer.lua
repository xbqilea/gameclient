--region
--Date
--此文件由[BabeLua]插件自动生成
--
--Desc: 1.初始化公共界面；2.监听和界面有关的事件

local SubLayer = class("SubLayer", cc.Node)

local Hall_Events   = require("hall.scene.HallEvent")
local Public_Events = require("common.define.PublicEvent")
local MessageBoxNew  = require("common.layer.MessageBoxNew")
local ErrorLogView  = require("common.layer.ErrorLogView")
local RollMsgView = require("hall.bean.RollMsgView")
local IngameBankView = require("hall.bean.IngameBankView")
local RechargeLayer       = require("hall.layer.RechargeLayer")
local OtherRechargeView = require("hall.bean.OtherRechargeView")

--子界面层级
SubLayer.ORDER_GIFT        = 1     --获得红包展示
SubLayer.ORDER_ROLLMSG     = 2     --银商喇叭
SubLayer.ORDER_BROADCAST   = 3     --大奖广播
SubLayer.ORDER_INGAME_BANK = 4     --游戏内银行
SubLayer.ORDER_RECHARGE    = 5     --充值界面
SubLayer.ORDER_MESSAGEBOX  = 10    --系统弹窗
SubLayer.ORDER_ROLLMSG_BOX = 20    --银商喇叭内容弹窗
SubLayer.ORDER_FLOAT_MSG   = 30    --浮动消息
SubLayer.ORDER_VEIL        = 40    --遮罩
SubLayer.ORDER_ERROR_LOG   = 50    --错误日志

function SubLayer:ctor()
    self:enableNodeEvents()
end

function SubLayer:onEnter()
    self:initLayer()  --子界面
    self:initEvent()  --监听事件
end

function SubLayer:onExit()
    self:clearLayer()   --移除界面
    self:clearEvent()   --移除事件
end

-- layer -----------------------
function SubLayer:initLayer()

    -- 浮动消息 ------------------
    local FloatMessage = require("common.layer.FloatMessage")
    FloatMessage.releaseInstance()
    local f = FloatMessage.getInstance()
    f:setPosition(0, -330)
    f:addTo(self, SubLayer.ORDER_FLOAT_MSG) -- fix 适配iphoneX

    -- 遮罩 --------------
    local v =  require("common.layer.Veil").new()
    cc.exports.Veil.getInstance = function() return v end
    v:setPosition(0, 0)
    v:addTo(self, SubLayer.ORDER_VEIL)

    --获得道具展示
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pLayerGift = cc.Layer:create()
    self.m_pLayerGift:setPosition(cc.p(diffX,0))
    self:addChild(self.m_pLayerGift,SubLayer.ORDER_GIFT)
end

function SubLayer:clearLayer()
end

function SubLayer:initEvent()
    
    self.__event = 
    {
        [Public_Events.MSG_SHOW_MESSAGEBOX]  = { func = self.event_message_box,     log = "系统弹窗",},
        [Public_Events.MSG_CLOSE_SUB_VIEW]   = { func = self.event_close_sub,       log = "清除子界面",},
        [Public_Events.MSG_SHOW_ROLLMSG_BOX] = { func = self.event_rollmsg_box,     log = "显示喇叭内容",   },
        [Public_Events.MSG_SHOW_INGAMEBANK]  = { func = self.event_show_ingamebank, log = "游戏内银行", },
        [Public_Events.MSG_SHOW_SHOP]        = { func = self.event_show_shop,       log = "充值界面", },
        --[Public_Events.MSG_GET_GAME_GIFT]    = { func = self.event_get_game_gift,   log = "获得字符",},

        [Hall_Events.MSG_SHOW_ERROR]         = { func = self.event_open_error,      log = "错误日志",},
    }
    local function onMsgEvent(event)
        local name = event:getEventName()
        local msg = unpack(event._userdata)
        self.__event[name].func(self, msg)
    end
    for key, var in pairs(self.__event) do
        SLFacade:addCustomEventListener(key, onMsgEvent, self.__cname)
    end
end

function SubLayer:clearEvent()
    for key in pairs(self.__event) do
        SLFacade:removeCustomEventListener(key, self.__cname)
    end
end

function SubLayer:event_message_box(msg)
    
    if msg == nil then 
        return
    end
    local msgbox = self:getChildByName("MessageBox")
    if msgbox then
        msgbox:onMoveExitView(msgbox.HIDE_NO_STYLE)
    end
    -- 弹出提示框
    local msgbox = MessageBoxNew.create(msg)
    msgbox:setName("MessageBox")
    msgbox:addTo(self,SubLayer.ORDER_MESSAGEBOX)
end

function SubLayer:event_close_sub(msg)

    local msgbox = self:getChildByName("MessageBox")
    if msgbox then
        msgbox:onMoveExitView(FixLayer.HIDE_NO_STYLE)
    end

    local ingameBankView = self:getChildByName("IngameBankView")
    if ingameBankView then
        ingameBankView:onMoveExitView(FixLayer.HIDE_NO_STYLE)
    end
end

--获得字符券
function SubLayer:event_get_game_gift(msg)
    ----集字符
    --local tag = tonumber(msg)
    --AudioManager.getInstance():playSound("public/sound/sound-get-gift.mp3")
    --local pSpine = "hall/effect/hall_jizi_huodewenzi/hall_jizi_huodewenzi"
    --local pAniChar = sp.SkeletonAnimation:create(pSpine .. ".json", pSpine .. ".atlas", 1)
    --pAniChar:setPosition(cc.p(667,375))
    --local strAni = "animation"..math.random(1,2)
    --pAniChar:setAnimation(0, strAni, false)
    --local skin = {"gong", "xi", "fa", "cai"}
    --pAniChar:setSkin(skin[tag])
    --pAniChar:addTo(self.m_pLayerGift, Z_ORDER_TOP)
    --local callback = cc.CallFunc:create(function (pAniChar)
    --    pAniChar:removeFromParent()
    --end)
    --local seq = cc.Sequence:create(cc.DelayTime:create(3.0), callback)
    --pAniChar:runAction(seq)

---月饼
   if not cc.exports.isOnlyInGame() then --没在游戏中
       return
   end
   local userID = tonumber(msg)
   AudioManager.getInstance():playSound("public/sound/sound-get-gift.mp3")

   --红包出场动画
   local pSpine = "hall/effect/325_zhongqiuhuodong_yuebing/325_zhongqiuhuodong_yuebing"
   local m_pGiftAni = sp.SkeletonAnimation:createWithBinaryFile(pSpine .. ".skel", pSpine .. ".atlas", 1)
   m_pGiftAni:setPosition(cc.p(667,375))
   self.m_pLayerGift:addChild(m_pGiftAni)
   m_pGiftAni:setAnimation(0, "animation1", false)
   --红包停留动画
   local callback = cc.CallFunc:create(function (animation)
        animation:setAnimation(0, "animation2", true)
   end)
   --红包目标位置
   local desPos = cc.p(80,50)
   local kindID =PlayerInfo.getInstance():getKindID()
   if kindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_FISHING 
    or kindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_FROG_FISH
    or kindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_LK_FISH  then 
        --大闹天宫 李逵 金蟾
        local chairID = CUserManager:getInstance():getUserInfoByUserID(userID).wChairID
        if chairID ~= nil then 
            local viewChair = CUserManager:getInstance():SwitchFishViewChairID(chairID)
            desPos = CUserManager:getInstance():getFishDefinePostion(viewChair)
        end
   elseif kindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_LONG_FISH then
        --寻龙夺宝
        local chairID =CUserManager:getInstance():getUserInfoByUserID(userID).wChairID
        if chairID ~= nil then 
            local viewChair = CUserManager:getInstance():SwitchFishViewChairID(chairID)
            local dp = { cc.p(170,50), cc.p(170+860,50), cc.p(170+860,700), cc.p(170,700) }
            desPos = dp[viewChair+1]
        end
   end
   --移动
   local move = cc.EaseExponentialInOut:create(cc.MoveTo:create(0.7,desPos))
   local scale = cc.EaseExponentialInOut:create(cc.ScaleTo:create(0.7,0.01))
   local pawn = cc.Spawn:create(move,scale)
   local callback2 = cc.CallFunc:create(function (animation)
        animation:removeFromParent()
   end)
   local seq = cc.Sequence:create(cc.DelayTime:create(2.0), callback, pawn, callback2 )
   m_pGiftAni:runAction(seq)
end

--银商喇叭内容弹窗
function SubLayer:event_rollmsg_box(msg)
    
    local msgbox = self:getChildByName("MessageBox")
    if msgbox then
        msgbox:onMoveExitView(FixLayer.HIDE_NO_STYLE)
    end
    local pView = self:getChildByName("RollMsgView")

    if pView and pView:isVisible() then
        return
    end

    if pView == nil then
        pView = RollMsgView.create()
        pView:setName("RollMsgView")
        pView:addTo(self,SubLayer.ORDER_ROLLMSG_BOX)
    end

    pView:initWithMsg(msg)
    pView:setVisible(true)
end

--游戏内银行
function SubLayer:event_show_ingamebank(msg)
    
    local ingameBankView = self:getChildByName("IngameBankView")
    if ingameBankView then
        ingameBankView:setVisible(true)
        return
    end
    local ingameBankView = IngameBankView:create()
    ingameBankView:init(msg)
    ingameBankView:setName("IngameBankView")
    ingameBankView:setPosition((display.width - 1624) / 2, 0)
    self:addChild(ingameBankView, SubLayer.ORDER_INGAME_BANK)
end

--充值界面
function SubLayer:event_show_shop(msg)
    
    --pc判断打开网页
    if device.platform == "windows" and not ClientConfig:getInstance():getDebugMode() then
        local isClosedAppStore = GameListManager.getInstance():getGameSwitch(G_CONSTANTS.GAMESWITCH.CLOSE_RECHARGE_APPSTORE)
        if not isClosedAppStore then 
            FloatMessage.getInstance():pushMessage("RECHARGE_5")
            return 
        end
        --跳转网页
        local url = ClientConfig:getInstance():getCreateOrderUrl()
        cc.Application:getInstance():openURL(url)
        return
    end

    --应用宝渠道
    local isClosedYsd = GameListManager.getInstance():getGameSwitch(G_CONSTANTS.GAMESWITCH.CLOSE_RECHARGE_YSDK)
    if ClientConfig.getInstance():getIsYsdkChannel() and not isClosedYsd then 
        local pRecharge = OtherRechargeView:create()
        pRecharge:setName("RechargeLayer")
        pRecharge:addTo(self, SubLayer.ORDER_RECHARGE)
        return
    end

    local isCloseRecharge = GameListManager.getInstance():getGameSwitch(G_CONSTANTS.GAMESWITCH.RECHARGE)
    if isCloseRecharge then
        return
    end
    --体验房提示
    if cc.exports.isConnectGame() and PlayerInfo.getInstance():IsInExperienceRoom() then
        FloatMessage.getInstance():pushMessage("体验房不可充值")
        return
    end
    if device.platform == "windows" and not ClientConfig.getInstance():getDebugMode() then
        --跳转网页
        local url = ClientConfig:getInstance():getCreateOrderUrl()
        cc.Application:getInstance():openURL(url)
        return
    end
    local pRecharge = self:getChildByName("RechargeLayer")
    if pRecharge then
        pRecharge:setVisible(true)
        return
    end
    pRecharge = RechargeLayer:create()
    pRecharge:setCastomData(true)
    pRecharge:setName("RechargeLayer")
    pRecharge:addTo(self, SubLayer.ORDER_RECHARGE)
end

function SubLayer:event_open_error(text) --错误日志
    
    --正式app不显示
    if ClientConfig.getInstance():getErrorLogOpen() == false then
        return
    end

    --显示错误界面
    self.m_pErrorLogView = ErrorLogView.new()
    self.m_pErrorLogView:setTextError(text)
    self.m_pErrorLogView:setName("errorLog")
    self.m_pErrorLogView:addTo(self, SubLayer.ORDER_ERROR_LOG)
end

--游戏返回大厅需要清除的界面
function SubLayer:back_hall_clear(msg)

    self.m_pLayerGift:stopAllActions()
    self.m_pLayerGift:removeAllChildren()

    local msgbox = self:getChildByName("MessageBox")
    if msgbox then
        msgbox:onMoveExitView(FixLayer.HIDE_NO_STYLE)
    end

    local ingameBankView = self:getChildByName("IngameBankView")
    if ingameBankView then
        ingameBankView:onMoveExitView(FixLayer.HIDE_NO_STYLE)
    end

    local pRecharge = self:getChildByName("RechargeLayer")
    if pRecharge then
        pRecharge:onMoveExitView(FixLayer.HIDE_NO_STYLE)
    end
end
return SubLayer

--endregion
