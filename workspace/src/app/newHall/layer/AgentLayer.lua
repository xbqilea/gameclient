--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion 

local AGENT_PATH = "hall/ShareWindow.csb";
local _ = {}
local HNlayer = import("..HNLayer")
local AgentLayer = class("AgentLayer",function()
    return HNlayer.new()
end) 
function AgentLayer:ctor()
     ToolKit:registDistructor( self, handler(self, self.onDestroy))
    self:myInit()
    self:setupViews()
     addMsgCallBack(self, MSG_REBATE_DATA_ASK, handler(self, self.updateRebateInfo))
    addMsgCallBack(self, MSG_REBATE_BALANCE_DATA_ASK, handler(self, self.updateRebateBalanceInfo))
    addMsgCallBack(self, MSG_REBATE_DETAIL_ASK, handler(self, self.updateRebateDetailInfo))
    addMsgCallBack(self, MSG_REBATE_WEEKRANK_ASK, handler(self, self.updateRebateWeekRankInfo))
    addMsgCallBack(self, MSG_REBATE_RECORDE_ASK, handler(self, self.updateRebateRecordInfo))
    addMsgCallBack(self, REFRESH_QRCODE, handler(self, self.refreshQRCode))
     
end
function AgentLayer:refreshQRCode()
    local qrcode = g_ShareQRcodeController:getQRcode()     
    if qrcode then
        qrcode:setAnchorPoint(0.5,0.5) 
        qrcode:setScale(0.7)
        self.node_qrcodeNode:removeAllChildren()
        self.node_qrcodeNode:addChild(qrcode)
    end
     local panel_center = self.QRCodeLayer:getChildByName("panel_center")
    local node_qrcodeNode =panel_center:getChildByName("node_qrcodeNode")
      local qrcode = g_ShareQRcodeController:getQRcode1()    
      if qrcode then
        node_qrcodeNode:removeAllChildren()
        node_qrcodeNode:addChild(qrcode)  
      end
end
function AgentLayer:myInit()
   self.m_panelList = {}
   self.m_contentList = {}
   self.m_emptyList = {}
   self.m_pageIndex =1
end

function AgentLayer:updateRebateWeekRankInfo()
      if self.m_contentList[4]==nil then
        return
    end
    local data =  GlobalRebateController:getRebateWeekRankDatas()
    local ListView_1 = self.m_contentList[4]:getChildByName("ListView_1")
    if table.nums(data)<=0 then
    --    self.m_emptyList[2]:setVisible(true)
        return
    else 
        
        self.button_cellBtn = self.node:getChildByName("button_cellBtn")
       local t = ccui.Widget:create();
        for k,v in pairs(data) do

		    local pContain = t:clone()
		    pContain:setContentSize(self.button_cellBtn:getContentSize());
	    --	pContain:setAnchorPoint(0, 1);
		    local node = self:showRankItem(v,k);
		    --node:setAnchorPoint(0, 0.5);
		    node:setPosition(445,30);
		    pContain:addChild(node);
		   ListView_1:pushBackCustomItem(pContain);
	    end  
    end
end

function AgentLayer:showRankItem(data,index)   
    local button_cellBtn = self.button_cellBtn:clone()
    local image_numBgImg =button_cellBtn:getChildByName("image_numBgImg");
    local text_rank = image_numBgImg:getChildByName("atlas_rankLabel"); 
    text_rank:setVisible(false);
--    local image_darkImg = button_cellBtn:getChildByName("image_darkImg");
--    image_darkImg:setScaleX(4) 
    --前三排名
    local sp_rank = button_cellBtn:getChildByName("image_rankSpr"); 
	
    sp_rank:setVisible(false);
     
    local text_coin = button_cellBtn:getChildByName("text_goldBmf");
    local text_time = button_cellBtn:getChildByName("text_time");
    -- 昵称
    local text_name = button_cellBtn:getChildByName("text_name");  
    if  index < 4 then
			
	    sp_rank:setVisible(true);
	    text_rank:setVisible(false);
	    sp_rank:loadTexture(string.format("hall/image/rank/tjylc_phb_hg%d.png", index)); 
        image_numBgImg:setVisible(false);
    else 
	    sp_rank:setVisible(false);
	    text_rank:setVisible(true);
       image_numBgImg:setVisible(true);
	    text_rank:setString(index); 
	    --img_jia:setVisible(true);		
    end
				 
		 
		 
    text_name:setString(data.m_nickName);  
    
	    text_coin:setString(data.m_rebateCoin*0.01); 
    return button_cellBtn
end
function AgentLayer:updateRebateBalanceInfo()
     local info =GlobalRebateController:getRebateBalanceDatas()
     self.text_text7:setString(info.m_takeAward*0.01)
     self.text_text6:setString(info.m_historyAward*0.01)
      if info.m_takeAward==0 then
        self.Button_1:setEnabled(false)
        self.Button_1:loadTextures("hall/image/agent/unget.png")
    else
         self.Button_1:setEnabled(true)
          self.Button_1:loadTextures("hall/image/agent/get.png")
    end
end
function AgentLayer:updateRebateDetailInfo()
     if self.m_contentList[2]==nil then
        return
    end
    
    local data =  GlobalRebateController:getRebateDetailDatas()
    local listView_lvReward = self.m_contentList[2]:getChildByName("listView_lvReward")
    if table.nums(data)<=0 then
    --    self.m_emptyList[1]:setVisible(true)
        return
    else
        if self.m_pageIndex == 1 then
            local image_detailTitle = self.node:getChildByName("image_detailTitle")
            local detail =image_detailTitle:clone()
            listView_lvReward:pushBackCustomItem(detail)
            detail:setPosition(0,0)
        end
        local panel_listItemDetail= self.node:getChildByName("panel_listItemDetail")
        for k,v in pairs(data) do
            local panel = panel_listItemDetail:clone()
            local text_nickname = panel:getChildByName("text_nickname")
            local text_todayGongxian = panel:getChildByName("text_todayGongxian")
             local text_id = panel:getChildByName("text_id")
            local text_detailCanGet = panel:getChildByName("text_detailCanGet")
            text_nickname:setString(v.m_nickName)
            text_id:setString(v.m_accountId)
            text_todayGongxian:setString(v.m_histContribute*0.01)
            text_detailCanGet:setString(v.m_yesterdayContribute*0.01)
            listView_lvReward:pushBackCustomItem(panel) 
            if v.m_num>0 then
                local  panel1 = panel_listItemDetail:clone()
                 local text_nickname = panel1:getChildByName("text_nickname")
                local text_todayGongxian = panel1:getChildByName("text_todayGongxian")
                local text_detailCanGet = panel1:getChildByName("text_detailCanGet")
                 text_nickname:setString(string.format("其他玩家(%d)",v.m_num))
                text_todayGongxian:setString(v.m_otherHistContrib*0.01)
                text_detailCanGet:setString(v.m_otherYestdayOtherContrib*0.01)
                listView_lvReward:pushBackCustomItem(panel1)
               
            end
        end
    end
end
function AgentLayer:updateRebateRecordInfo()
    --刷新记录

    print("--刷新记录--")

    self.m_RebateRecordData = GlobalRebateController:getRebateRecordDatas()
     

    local image_bg = self.ShareRecord:getChildByName("image_bg")
    local panel_empty = image_bg:getChildByName("panel_empty")
    if  table.nums(self.m_RebateRecordData) <= 0  then
       -- panel_empty:setVisible(true)
        return
    else
        panel_empty:setVisible(false)
    end  
  
    local ListView_1 =  image_bg:getChildByName("ListView_1")
    ListView_1:removeAllItems()
    local button_cellBtn1 = self.ShareRecord:getChildByName("button_cellBtn1")
    local t = ccui.Widget:create();
    for k,v in pairs(self.m_RebateRecordData) do

		local pContain = t:clone()
		pContain:setContentSize(button_cellBtn1:getContentSize()); 
		local button = button_cellBtn1:clone() 
		button:setPosition(420,30);
		pContain:addChild(button);
          local text_date = button:getChildByName("text_date")
        local text_goldBmf = button:getChildByName("text_goldBmf")
        local time = os.date("%Y-%m-%d",v.m_time)
        text_date:setString(time)
        text_goldBmf:setString(v.m_rebateCoin*0.01)
		ListView_1:pushBackCustomItem(pContain);
	end  
end
function AgentLayer:onDestroy()
	--清除返利数据 
    GlobalRebateController:removeRebateDatas()
    GlobalRebateController:removeRebateBalanceDatas()
    GlobalRebateController:removeRebateDetailDatas()
    GlobalRebateController:removeRebateRecordDatas()
     GlobalRebateController:removeRebateWeekRankDatas() 

    removeMsgCallBack(self, MSG_REBATE_DATA_ASK)
    removeMsgCallBack(self, MSG_REBATE_BALANCE_DATA_ASK)
    removeMsgCallBack(self, MSG_REBATE_DETAIL_ASK)
    removeMsgCallBack(self, MSG_REBATE_RECORDE_ASK)
    removeMsgCallBack(self, MSG_REBATE_WEEKRANK_ASK)
    removeMsgCallBack(self, REFRESH_QRCODE)
end
 function AgentLayer:onTouchCallback(sender)
    local picUrl = cc.UserDefault:getInstance():getStringForKey( "erweima__url" ,"http://www.baidu.com")
    local name = sender:getName()
    if name == "button_closeBtn" then
        self:close()  
    elseif name == "button_copyQQ" then
         if device.platform ~= "ios" then
           device.openURL(string.format("mqqwpa://im/chat?chat_type=wpa&uin=%s",GlobalConf.CHANNEL_QQ))
        else
            local kl = "mqq://im/chat?chat_type=wpa&uin=%s&version=1&src_type=web"
            local content = string.format(kl,GlobalConf.CHANNEL_QQ)
            device.openURL(content)
        end
    elseif name == "button_yybz" then
        self.ShareWindow_Help:setVisible(true)
    elseif name == "button_tqjl" then
         GlobalRebateController:reqRebateRecordInfo()
        self.ShareRecord:setVisible(true)
    elseif name == "Button_1" then
        local num = tonumber(self.text_text7:getString())
        if num == 0 then
            TOAST("奖励已领取")
            return
        end
        GlobalRebateController:reqRebateBalanceInfo() 
    elseif name == "button_qrcodeImg" then
            if self.QRCodeLayer ~= nil then
                self.QRCodeLayer:setVisible(true)
            end
     elseif name == "button_refresh" then
         g_ShareQRcodeController:checkQRCode()
         _:disableQuickClick(sender)  
    elseif name == "button_wxhaoyou" then
        print("button_wxhaoyou")
        local m_callback = function(success)end
        local picPath1 = "res/ui/img.jpg"
        local picPath2 = cc.UserDefault:getInstance():getStringForKey("erweima")
        local picPath = self:createImageFileWithTwoImage(picPath1,picPath2,1093,613)
        if device.platform == "android" then
    	    local javaClassName = "com/XinBo/LoveLottery/wxapi/WXEntryActivity"
            local javaMethodName = "OnShare"
            local javaParams = {
                "",
                picPath,
                "",
                "",
                false,
            }
            local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V"
            luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        elseif device.platform == "ios" then
            local args = { num1 = "", num2 = "", num3 = picPath, num4 = picUrl, num5 = m_callback }
            local luaoc = require "cocos.cocos2d.luaoc"
            local className = "MilaiPublicUtil"
            local ok, ret  = luaoc.callStaticMethod(className,"shareWX",args)
            if not ok then
                cc.Director:getInstance():resume()
            else
                print("The ret is:",tostring(ret))
                --callback(tostring(ret))
            end

            local function callback_ios2lua(param)
                if "success" == param then
                    print("object c call back success")
                end
            end
            luaoc.callStaticMethod(className,"registerScriptHandler", {scriptHandler = callback_ios2lua } )
            luaoc.callStaticMethod(className,"callbackScriptHandler")
        end
    elseif name == "button_pyquan" then
        print("button_pyquan")
        local m_callback = function(success)end
        local picPath1 = "res/ui/img.jpg"
        local picPath2 = cc.UserDefault:getInstance():getStringForKey("erweima")
        local picPath = self:createImageFileWithTwoImage(picPath1,picPath2,1093,613)
        if device.platform == "android" then
    	    local javaClassName = "com/XinBo/LoveLottery/wxapi/WXEntryActivity"
            local javaMethodName = "OnShare"
            local javaParams = {
                "",
                picPath,
                "",
                "",
                true,
            }
            local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V"
            luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        elseif device.platform == "ios" then
            local args = { num1 = "", num2 = "", num3 = picPath, num4 = picUrl, num5 = m_callback }
            local luaoc = require "cocos.cocos2d.luaoc"
            local className = "MilaiPublicUtil"
            local ok, ret = luaoc.callStaticMethod(className,"shareWXTimeLine",args)
            if not ok then
                cc.Director:getInstance():resume()
            else
                print("The ret is:",tostring(ret))
                --callback(tostring(ret))
            end

            local function callback_ios2lua(param)
                if "success" == param then
                    print("object c call back success")
                end
            end
            luaoc.callStaticMethod(className,"registerScriptHandler", {scriptHandler = callback_ios2lua } )
            luaoc.callStaticMethod(className,"callbackScriptHandler")
        end
    elseif sender:getTag()<=4 and sender:getTag()>=1 then 
        self.m_emptyList[2]:setVisible(false)
         self.m_emptyList[1]:setVisible(false)
        for k,v in pairs(self.m_panelList) do
            local btntext_unSelected = v:getChildByName("unlight")
            local btntext_Selected = v:getChildByName("light")
            
            if sender:getTag() == k then
              
                btntext_unSelected:setVisible(false)
                btntext_Selected:setVisible(true)
                self.m_contentList[k]:setVisible(true)
                if k==4 or k==2 then
                    local record =  GlobalRebateController:getRebateDetailDatas()
                    local rank = GlobalRebateController:getRebateWeekRankDatas()
                    if table.nums(rank)<=0 and k== 4 then
                        self.m_emptyList[2]:setVisible(true) 
                    end
                     if table.nums(record)<=0 and k== 2 then
                        self.m_emptyList[1]:setVisible(true) 
                    end
                end
            else 
                 btntext_unSelected:setVisible(true)
                btntext_Selected:setVisible(false)
                self.m_contentList[k]:setVisible(false)
            end
        end
    end
 end  

function AgentLayer:createImageFileWithTwoImage(firstImagePath, secondImagePath, width, height)
    local firstNode  =  self:createRenderNodeWithPath(firstImagePath, 0, 0, 1)
    local secondNode = self:createRenderNodeWithPath(secondImagePath, 680, 95, 1)
    local toFileName = "pandaWXSharePic.png"
    local renderTexture = self:createRenderTextureWithNodes(firstNode, secondNode, width, height)
    if renderTexture then
        local saveRet = renderTexture:saveToFile(toFileName, cc.IMAGE_FORMAT_JPEG, false)
        cc.Director:getInstance():getTextureCache():removeTextureForKey(cc.FileUtils:getInstance():getWritablePath()..toFileName)
        if saveRet then
           return  cc.FileUtils:getInstance():getWritablePath() .. toFileName
        else
            print("保存图片失败")
            return nil
        end
    end
end

function AgentLayer:createRenderNodeWithPath(path, posX, posY, scale)
        local sprite = nil
        if path then
            sprite = cc.Sprite:create(path)
            sprite:setAnchorPoint(cc.p(0,0))
            sprite:setPosition(cc.p(posX,posY))
            sprite:setScale(scale)
        end
        return sprite
end

-- 两张图片纹理二合一
function AgentLayer:createRenderTextureWithNodes(firstRenderNode, secondRenderNode, width, height)
    local renderTexture = cc.RenderTexture:create(width, height)
    renderTexture:beginWithClear(0,0,0,0)

    if firstRenderNode then
        firstRenderNode:getTexture():setTexParameters(0x2601, 0x2601, 0x812f, 0x812f)
    end

    if firstRenderNode then
        firstRenderNode:visit()
    end
   
    if secondRenderNode then
        secondRenderNode:visit()
    end

    renderTexture:endToLua()
    return renderTexture
end

function AgentLayer:listViewListener(sender,eventType)
	if eventType == ccui.ScrollviewEventType.scrollToBottom then
        print("SCROLL_TO_BOTTOM")
        
        local maxPage = GlobalRebateController:getTotalPage()
        if self.m_pageIndex== maxPage  then
            return
        end
        self.m_pageIndex = self.m_pageIndex+1 
        GlobalRebateController:reqRebateDetailInfo(self.m_pageIndex)
    end
end
function AgentLayer:setupViews()
     g_AudioPlayer:playEffect("public/sound/share_audio.mp3") 
    self.node = UIAdapter:createNode(AGENT_PATH); 
    self:addChild(self.node,10000); 
    UIAdapter:adapter(self.node, handler(self, self.onTouchCallback))
    UIAdapter:praseNode(self.node,self) 
    self.listView_lvReward:addScrollViewEventListener( handler(self, self.listViewListener) )
    local center = self.node:getChildByName("center") 
     local diffY = (display.size.height - 750) / 2
    self.node:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
    local image_Bg =center:getChildByName("image_Bg")
     
    local Image_background = image_Bg:getChildByName("Image_background")
     for i=1,4 do
        local panel = Image_background:getChildByName("panel_panel"..i)
        if i == 1 then
            local kuang_erweima_32 = panel:getChildByName("kuang_erweima_32")
            local button_qrcodeImg = kuang_erweima_32:getChildByName("node_qrcodeNode") 
            local qrcode = g_ShareQRcodeController:getQRcode()     
            if qrcode then
                qrcode:setAnchorPoint(0.5,0.5)
              --  qrcode:setPositionY(qrcode:getPositionY()-5)
                qrcode:setScale(0.7)
                button_qrcodeImg:addChild(qrcode)
            end
            
        end
        table.insert(self.m_contentList,panel)  

        local panel = Image_background:getChildByName("panel_empty_"..i) 
        if panel then
            panel:setVisible(false)
        end
        table.insert(self.m_emptyList,panel)   
    end
    
      --  end1()
    local topTab = image_Bg:getChildByName("Layout_Top")
      for i=1,4 do
        local panel = topTab:getChildByName("Layout_"..i)
        table.insert(self.m_panelList,panel)
        panel:setTag(i)
    end
    self:onTouchCallback(self.m_panelList[1])
    self.ShareRecord = UIAdapter:createNode("hall/ShareRecord.csb");
    self.node:addChild(self.ShareRecord)
    self.ShareRecord:setVisible(false)
      local center =  self.ShareRecord:getChildByName("Layer") 
     local diffY = (display.size.height - 750) / 2
    self.ShareRecord:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
    UIAdapter:adapter( self.ShareRecord, handler(self, self.onTouchCallback2))
    self.ShareWindow_Help = UIAdapter:createNode("hall/ShareWindow_Help.csb");
    self.node:addChild(self.ShareWindow_Help)
      local center =  self.ShareWindow_Help:getChildByName("Panel_1") 
     local diffY = (display.size.height - 750) / 2
    self.ShareWindow_Help:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
    self.ShareWindow_Help:setVisible(false)
      UIAdapter:adapter(self.ShareWindow_Help, handler(self, self.onTouchCallback1))

    self.QRCodeLayer= UIAdapter:createNode("hall/ShareSysViewDetail.csb")
    local panel_center = self.QRCodeLayer:getChildByName("panel_center")
    local node_qrcodeNode =panel_center:getChildByName("node_qrcodeNode")
      local qrcode = g_ShareQRcodeController:getQRcode1()    
      if qrcode then
        node_qrcodeNode:addChild(qrcode) 
        self.node:addChild(self.QRCodeLayer)
        self.QRCodeLayer:setVisible(false)
        UIAdapter:adapter(self.QRCodeLayer, handler(self, self.onTouchCallback3))
      end
      
    GlobalRebateController:reqRebateDetailInfo(self.m_pageIndex)
     GlobalRebateController:reqRebateInfo() 
     GlobalRebateController:reqRebateWeekRankInfo() 
    self.text_qqCode:setString("财富号："..GlobalConf.CHANNEL_QQ) 
     if g_promoteType ==2 then

        self.Text_32_0_0_0_0:setVisible(false)
        self.bg_18:setTexture("hall/image/agent/bg1.png")
        self.board_left:setTexture("hall/image/agent/back1.png")
        self.img_lszjl_6:setPositionY(self.img_lszjl_6:getPositionY()+40)
        self.img_ktqjl_5:setPositionY(self.img_ktqjl_5:getPositionY()+40)
        self.text_text4:setVisible(false)
        self.text_text5:setVisible(false)
         --text_text3:setString(info.m_ydDirectAward*0.01)
        self.Text_32_0:setPositionY(self.Text_32_0:getPositionY()-30)
        self.Text_32:setPositionY(self.Text_32:getPositionY()-30)
        self.Text_32_0_0_0:setPositionY(self.Text_32_0_0_0:getPositionY()-30)
        self.Text_32_0_0:setPositionY(self.Text_32_0_0:getPositionY()-30)
    end
   
end
function AgentLayer:updateRebateInfo()
   if self.m_contentList[1]==nil then
        return
    end
   local info = GlobalRebateController:getRebateDatas()
    local board_left =self.m_contentList[1]:getChildByName("board_left")
    local button_lingqu = board_left:getChildByName("button_lingqu") 
    local text_text1 = board_left:getChildByName("text_text1")
    text_text1:setString(info.m_ydTotalAward*0.01)
    local text_text2 = board_left:getChildByName("text_text2")
    text_text2:setString(info.m_directNum)
    local text_text3 = board_left:getChildByName("text_text3")
     text_text3:setString(info.m_indirectNum)
    local text_text4 = board_left:getChildByName("text_text4")
    text_text4:setString(info.m_ydDirectAward*0.01)
    local text_text5 = board_left:getChildByName("text_text5")
    text_text5:setString(info.m_ydIndirectAward*0.01)
    local img_lszjl_6 = board_left:getChildByName("img_lszjl_6")
     self.text_text7 = img_lszjl_6:getChildByName("text_text7")
     self.text_text7:setString(info.m_takeAward*0.01)
     if info.m_takeAward==0 then
        self.Button_1:setEnabled(false)
        self.Button_1:loadTextures("hall/image/agent/unget.png")
    else
         self.Button_1:setEnabled(true)
          self.Button_1:loadTextures("hall/image/agent/get.png")
    end
    local img_ktqjl_5= board_left:getChildByName("img_ktqjl_5")
    self.text_text6 = img_ktqjl_5:getChildByName("text_text6")
    self.text_text6:setString(info.m_historyAward*0.01)
    local first =info.m_firstLevelRate*0.01
    local other =info.m_otherLevelRate*0.01
    self.Text_32:setString("您的返利=1级返利+2级返利+..N级返利。\n1级返利=对应下线*"..first.."%   其它级返利=对应下线*"..other.."%")
     self.Text_32_0_0_0_0:setString("您的返利=10000*"..first.."%+（20000*"..first.."%）*"..other.."%")  
      if g_promoteType ==2 then
        self.Text_32:setString("您的返利=1级返利。\n1级返利=对应下线*"..first.."%")
        self.Text_32_0_0_0:setString("您的返利=10000*"..first.."%") 
        text_text3:setString(info.m_ydDirectAward*0.01)
    end
end
  function AgentLayer:onTouchCallback1(sender)
     local name = sender:getName()
     if name == "button_close" then
        self.ShareWindow_Help:setVisible(false)
    end
  end
    function AgentLayer:onTouchCallback2(sender)
     local name = sender:getName()
     if name == "button_close" then
        self.ShareRecord:setVisible(false)
    end
  end

      function AgentLayer:onTouchCallback3(sender)
     local name = sender:getName()
     if name == "panel_mask" then
        self.QRCodeLayer:setVisible(false)
    end
  end

  
function _:setBtnEnabled(btn, enabled)
    btn:setEnabled(enabled)
    btn:setTouchEnabled(enabled) 
    
end



-- 防止频繁点击
function _:disableQuickClick(btn)
    _:setBtnEnabled(btn, false) 
    performWithDelay(btn, function()
        _:setBtnEnabled(btn, true)
    end, 2)
end
return AgentLayer