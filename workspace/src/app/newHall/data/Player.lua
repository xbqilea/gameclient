--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 玩家数据类
Player = Player or {}

local BaseInfo = {
--注意：BaseInfo里Info,Info_,Param都是保留字
---------------------新版本信息
	AccountID = 0, --账号ID
	AccountName = "", --账户名
    Password = "",
	NickName = "", --昵称
	PhoneNumber = "", --手机号
	FaceID = 0, --头像ID
    FrameID = 0,
	Sex = 0, --性别
	VipLevel = 0, --会员等级
    VipMoney = 0, --vip充值的钱
--	Exp = 0, --经验
--	Level = 0, --等级
--Diamond = 0, --钻石
--	ABean = 0, --A豆
	GoldCoin = 0, --金币
    BGGoldCoin = 0, --视讯BG金币
--	BankDiamond = 0, --银行钻石
--	BankABean = 0, --银行A豆
	BankGoldCoin = 0, --银行金币
	IDCard = "", --身份证
	IsModifiedNick = 0, --是否修改过昵称
--	AuditingAvatarID = 0, --审核头像ID
--	CustomAvatarList = {}, --自定义头像id列表
	IsRegularAccount = 0, --是否正式账号
	IsModifiedBankPwd = 0, --是否修改过银行密码
--	UnlockSystemAvatarList = {},  -- 已解锁的系统头像 
	--VectCustomParam = {},
	NextUploadAvatarID = 0, --下一个上传头像id
	LockDevice = "",
	ExchangeLeftTimes = 0,
	ExchangeGoldTimes = 0,
	SmsCoolTimeList = {}, -- 验证码冷却时间
	Address = {},	--用户兑换地址
	Result = 0, --登录返回码，[0]成功[-1]密钥错误[-x]其他原因
    BindThirdAccList  = "",  --json对象数组字符串 格式 第三方账户信息  {"type":1,"thirdAcc":"aaaa"} type:第三方账户类型比如微信 thirdAcc:第三方账号唯一标识 比如微信openid'
    ApplyToken = "", --应用口令，比如钻石商城购买时用这个口令代表登录验证
    TokenInfo = {}, --代币信息-- { 'm_nDayToken'今日获得token数据'},{'m_nCalcToken''累计获得代币数'},
    TokenRoomInning = 0, --代币房局数
    --PromoteNum = 0, --代币推广人数量
   
    StrAlipayID ="", --支付宝账号
    StrBankID = "", --银行账号
    RegularAward = 0,
    CurBetScore=0,
    TomorrowBetScore=0,
	DamaAmount = 0,
	DamaNeed = 0,
    RealName = "",
	 --  StrBankRealName ="",
	----------------------------旧版本信息
	-- UserID = 0,-- 角色id
	-- GameID = 0,		-- 游戏 I D
	-- FaceID = 0,		-- 头像索引
	-- SysFaceId = 0, 	-- 最近使用的系统头像
	-- Gender = 0,		-- 用户性别
	-- Member = 0,		-- 会员等级
	-- Experience = 0,	-- 用户经验
	-- ExpPercent = 0,	-- 经验百分比
	-- Diamond = 0,		-- 钻石
	-- ABean = 0,			-- A豆
	-- Score = 0,			-- 金币
	-- Lv = 0,			-- 等级
	-- CustomFaceVer = 0,	-- 头像版本
	-- MailCount = 0,		--
	-- BankScore = 0,		-- 银行金币
	-- BankDiamond = 0,	-- 银行钻石
	-- BankABean = 0,		-- 银行ABean
	-- IsOnlyForCheck = 0,-- 是否只是为了应付检查的账户（1：只是应付检查的账户）
	-- MarketFee = 0,		-- 市场交易服务费
	-- IsNotVerifyed = 0,	-- 是否未验证
	-- IsModifyPwd = 0,	-- 是否修改密码 0-未修改
	-- TaskCount = 0,		-- 
	-- SpreadId = 0,
	-- AgentId = 0,
	-- IsAward = 0,		-- 是否领奖 0-未领
	-- Password = "",		-- 密码
	-- Accounts = "",		-- 账号
	-- UnderWrite = "",	-- 个性签名
	-- NickName = "",		-- 昵称
	-- ModifyNickNameCount = 0, -- 修改昵称次数
	-- Mobile = 0, -- 绑定手机号
	-- Identerty = 0, -- 绑定身份证号
	-- UnReadMailCount = 0, -- 未读邮件数
	-- BindMobileTime = 0, -- 绑定手机号时间
	-- UnBindMobileTime = 0, -- 解绑手机时间
	-- TotalTelCard = 0, -- 电话卡总数
	-- TitleLv = 0,	-- 头街
	-- TitleExperience = 0, -- 当前头街进度
	-- Token = "",
	-- Area = "", -- 所在地区
	-- ExchangeHuafeiNum = 0 ,  --兑换话费次数
	-- MachineOnLineTime = 0,
}

--方便setInfo批量导入用的别名，别名就是协议里的变量名
local alias = {
--格式:别名="BaseInfo里的变量名"
--说明：不用中文字符串转换的都放在这里
	m_nAccountID = "AccountID",
	m_strAccountName = "AccountName",
	m_strNickName = "NickName",
	nickName = "NickName",
	m_strPhoneNumber = "PhoneNumber",
	m_nSex = "Sex",
	m_nMemberLevel = "MemberLevel",
    m_nRechargeMoney ="VipMoney",
	-- m_strGroupName = "GroupName",
	-- m_strUnderWrite = "UnderWrite",
--	m_nExp = "Exp",
--	m_nLevel = "Level",
	m_vipLevel = "VipLevel",
   -- m_nVipMoney = "VipMoney",
--	m_nDiamond = "Diamond",
--	m_nABean = "ABean",
	m_nGoldCoin = "GoldCoin",
	m_nBankDiamond = "BankDiamond",
	--bankDIamond = "BankDiamond",
	--m_nBankABean = "BankABean",
	m_nBankGoldCoin = "BankGoldCoin",
	bankGoldCoin = "BankGoldCoin",
	m_nResult = "Result",
	m_strIDCard = "IDCard",
	idcard = "IDCard",
	accountID = "AccountID",
	accountName = "AccountName",
    Password = "Password",
	phoneNumber = "PhoneNumber",
	m_nFaceID = "FaceID",
	avatarID = "FaceID",
    m_nAvatarFrameID ="FrameID",
	sex = "Sex",
	m_nMemberLevel = "VipLevel",
	exp = "Exp",
	level = "Level",
	diamond = "Diamond",
	goldCoin = "GoldCoin",
	aBean = "ABean",
	m_nIsModifiedNick = "IsModifiedNick",
	--m_nAuditingAvatarID = "AuditingAvatarID",
	--m_customAvatarList = "CustomAvatarList",
	m_nIsRegularAccount = "IsRegularAccount",
	m_nIsModifiedBankPwd = "IsModifiedBankPwd",
	--m_unlockAvatarList = "UnlockSystemAvatarList",
	-- m_customAvatarList = "CustomAvatarList",
	m_vectCustomParam = "VectCustomParam",
--	m_nextUploadAvatarID = "NextUploadAvatarID",
	m_strLockDevice = "LockDevice",
	m_smsCoolTimeList = "SmsCoolTimeList",
	
    m_strBindThirdAccList = "BindThirdAccList",
    m_nTokenRoundInning = "TokenRoomInning",
    
    m_strAlipayID ="StrAlipayID", 
    m_strBankID = "StrBankID",  
    m_regularAward = "RegularAward",
    m_curBetScore = 		"CurBetScore",
    m_tomorrowBetScore	="TomorrowBetScore",
	m_damaAmount ="DamaAmount",
	m_damaNeed =  "DamaNeed",
    m_strRealName = "RealName",
    --  m_strBankRealName = "StrBankRealName",
}

--Unicode转换成UTF8的别名表
local A2U_alias = {
	--格式:别名="BaseInfo里的变量名"
	--说明：需要用中文字符串转换的都放在这里
}

local BaseInfoClone = clone(BaseInfo)
--重置内容
function Player:reset()
	BaseInfo = clone(BaseInfoClone)
end

--打印内容
function Player:dump(_tag)
	local tag = _tag or "Player"
	dump(BaseInfo,tag)
end

--批量生产getter setter
for k,v in pairs(BaseInfo) do
	Player["set"..k] = function(Player,value)
		BaseInfo[k] = value
	end
	Player["get"..k] = function(Player)
		return BaseInfo[k]
	end
end

--通用getter setter方便批量导入导出数据
function Player:setParam( _paramName, _paramValue ) 
	if BaseInfo[_paramName] and Player["set".._paramName] then
		-- BaseInfo[_paramName] = _paramValue
        if _paramName =="GoldCoin" or _paramName =="BankGoldCoin" then
            _paramValue = _paramValue*0.01
        end  
        print(_paramName.." = "..tostring(_paramValue))
        Player["set".._paramName](Player,_paramValue)
		return true
	else
		return false
	end
end
function Player:getParam( _paramName )
	if BaseInfo[_paramName] and Player["get".._paramName] then
		-- return BaseInfo[_paramName]
		return Player["get".._paramName](Player)
	end
end

-- 是否已经登录，有效用户
function Player:isLogin()
	return BaseInfo.AccountID ~= 0
end

--通过info这个table批量设置(不使用别名)
function Player:setInfo_(_info)
	for k,v in pairs(_info) do
		if Player:setParam(k,v) == false then
			print(k.." not found in BaseInfo, Player:setInfo_()")
		end
	end
end
--通过info这个table批量设置(使用别名)
function Player:setInfo(_info) 
	for k,v in pairs(_info) do
		if alias[k] then
			if Player:setParam(alias[k],v) == false then
				print(k.." (alias) not found in BaseInfo, Player:setInfo()")
			end
		elseif A2U_alias[k] then
			if Player:setParam(A2U_alias[k],A2U(v)) == false then
				print(k.." (alias) not found in BaseInfo, Player:setInfo()A2U")
			end
		else
			if Player:setParam(k,v) == false then
				print(k.." not found in BaseInfo, Player:setInfo()")
			end
		end
	end
end


return Player