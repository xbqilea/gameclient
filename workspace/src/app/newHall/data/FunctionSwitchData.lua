--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
local config ={
 [1]={name='个人信息-登出',key='logout',kindId=1,resource='',},
 [2]={name='个人信息-注册正式账号',key='regular',kindId=1,resource='',},
 [3]={name='个人信息-设置',key='set',kindId=1,resource='',},
 [4]={name='公告-游戏公告',key='builtin',kindId=2,resource='',},
 [5]={name='公告-在线客服',key='service',kindId=2,resource='',},
 [6]={name='VIP',key='vip',kindId=3,resource='',},
 [7]={name='排行榜-今日赢金币',key='rank_today_win',kindId=4,resource='',},
 [8]={name='排行榜-今日在线时长',key='rank_today_online_time',kindId=4,resource='',},
 [9]={name='排行榜-等级',key='rank_level',kindId=4,resource='',},
 [10]={name='兑换-兑换到支付宝',key='exchange_alipay',kindId=5,resource='',},
 [11]={name='兑换-兑换到银行卡',key='exchange_bank',kindId=5,resource='',},
 [12]={name='银行-存款',key='bank_withdrawals',kindId=6,resource='',},
 [13]={name='银行-取款',key='bank_deposit',kindId=6,resource='',},
 [14]={name='注册送金',key='regular_gift',kindId=7,resource='',},
 [15]={name='全民代理',key='agent',kindId=8,resource='',},
 [16]={name='立即充值-支付宝',key='recharge_alipay',kindId=9,resource='',},
 [17]={name='立即充值-VIP充值',key='recharge_vip',kindId=9,resource='',}, 
 }
  
return config