--region BankPropertyView.lua
--Date 2017.04.25.
--Auther JackyXu.
--Desc: 银行存入/取出 view

local FloatMessage  = require("common.layer.FloatMessage")
local Veil          = require("common.layer.Veil")
local BankRefundLogView = require("hall.bean.BankRefundLogView")

local TAG_OUT = 1
local TAG_IN  = 2
local MAX_PERCENT = 10

local BankPropertyView = class("BankPropertyView", cc.Layer)

function BankPropertyView:ctor(idx)

    self:enableNodeEvents()

    self.m_nType = idx
    self.m_llInputScore = 0
end

function BankPropertyView:init()
    
    self:initCSB()
    self:initEvent()
    self:initBtnEvent()
    self:initEditBox()
    self:initNode()
    if device.platform == "windows" then
        self:initKeyboard()  
    end
end

function BankPropertyView:clean()
    self:cleanEvent()
    if device.platform == "windows" then
        self:cleanKeyboard()
    end
end

function BankPropertyView:initCSB()
    
    -- root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    -- csb
    local strPath = "hall/csb/BankViewInOut.csb"
    if CommonUtils.getInstance():isPCMode() then
        strPath = "hall/csb/pc-BankViewInOut.csb"
    end
    self.m_pathUI = cc.CSLoader:createNode(strPath)
    self.m_pathUI:addTo(self.m_rootUI)

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("Panel_1")

    local Node_1 = self.m_pNodeRoot:getChildByName("Node_1")
    local Node_2 = self.m_pNodeRoot:getChildByName("Node_2")
    local Node_3 = self.m_pNodeRoot:getChildByName("Node_3")
    local Node_4 = self.m_pNodeRoot:getChildByName("Node_4")
    local Node_5 = self.m_pNodeRoot:getChildByName("Node_5")

    --node 1
    local Node_Gold = Node_1:getChildByName("Node_6")
    local Node_Bank = Node_1:getChildByName("Node_7")
    self.m_pLabelTake = Node_Gold:getChildByName("BitmapFontLabel_1")
    self.m_pLabelSave = Node_Bank:getChildByName("BitmapFontLabel_2")

    --node 2
    self.m_pLabelInOut = {}
    self.m_pLabelInOut[1] = Node_2:getChildByName("Text_1")
    self.m_pLabelInOut[2] = Node_2:getChildByName("Text_2")
    self.m_pLabelInput = Node_2:getChildByName("node_input")
    self.m_pBtnGold = Node_2:getChildByName("Button_1")
    self.m_pLabelCHValue = Node_2:getChildByName("Text_3")

    --node 3
    self.m_pSliderGold = Node_3:getChildByName("Slider_1")
    self.m_pImageCurrent = Node_3:getChildByName("Sprite_19")
    self.m_pBtnMax = Node_3:getChildByName("Button_2")
    self.m_vecPoint, self.m_vecPercent = {}, {}
    for i = 0, 10 do
        self.m_vecPoint[i] = Node_3:getChildByName("Sprite_18_" .. i)
        self.m_vecPercent[i] = Node_3:getChildByName("Text_18_" .. i)
    end

    --node 4
    self.m_pBtnReset    = Node_4:getChildByName("Button_3")
    self.m_pBtnConfirm  = Node_4:getChildByName("Button_4")

    --pc
    if CommonUtils.getInstance():isPCMode() then
        self.m_pBtnRefresh = Node_1:getChildByName("Button_7")
        --node 5
        self.m_pBtnGet          = Node_5:getChildByName("Button_5")
        self.m_pBtnGetRecord    = Node_5:getChildByName("Button_6")
        self.m_pLbRefundScore   = Node_5:getChildByName("Text_4")
        --存款界面 并且是代理才显示可领取返利
        if self.m_nType == TAG_IN and PlayerInfo.getInstance():isProxy() then
            Node_5:setVisible(true)
            self:onUpdateRefundValue()
        else
            Node_5:setVisible(false)
        end
    end
end

function BankPropertyView:onEnter()
    self:init()
end

function BankPropertyView:onExit()

    self:clean()
end

--监听按键
function BankPropertyView:initKeyboard() 
    local function onKeyReleased(keyCode, event)
        event:stopPropagation()
        if keyCode == cc.KeyCode.KEY_ENTER or keyCode == cc.KeyCode.KEY_KP_ENTER then 
            self:onSureClicked()
        elseif keyCode == cc.KeyCode.KEY_TAB then
            self.m_pEditBox:touchDownAction(self.m_pEditBox,ccui.TouchEventType.ended);
        end
    end

    local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
    self.m_pKeyboardListener = cc.EventListenerKeyboard:create()
    self.m_pKeyboardListener:registerScriptHandler(onKeyReleased, cc.Handler.EVENT_KEYBOARD_RELEASED)
    pEventDispatcher:addEventListenerWithFixedPriority(self.m_pKeyboardListener, -129)
end

--移除按键监听
function BankPropertyView:cleanKeyboard()

    if self.m_pKeyboardListener then
        local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
        pEventDispatcher:removeEventListener(self.m_pKeyboardListener)
        self.m_pKeyboardListener = nil
    end
end

function BankPropertyView:initEvent()
    SLFacade:addCustomEventListener(Public_Events.MSG_BANK_INFO, handler(self, self.onMsgBankInfo), self.__cname)
    SLFacade:addCustomEventListener(Public_Events.MSG_BANK_SUCCESS_INFO, handler(self, self.onMsgBankSuccess), self.__cname)
    SLFacade:addCustomEventListener(Public_Events.MSG_BANK_CLEAR_INFO, handler(self, self.onMsgBankSuccess), self.__cname)
    if CommonUtils.getInstance():isPCMode() then --pc 有刷新按钮
        SLFacade:addCustomEventListener(Public_Events.MSG_PC_REFRESH_SCORE, handler(self, self.onMsgRefreshScore), self.__cname)
    end
end

function BankPropertyView:cleanEvent()
    SLFacade:removeCustomEventListener(Public_Events.MSG_BANK_INFO, self.__cname)
    SLFacade:removeCustomEventListener(Public_Events.MSG_BANK_SUCCESS_INFO, self.__cname)
    SLFacade:removeCustomEventListener(Public_Events.MSG_BANK_CLEAR_INFO, self.__cname) 
    if CommonUtils.getInstance():isPCMode() then 
        SLFacade:removeCustomEventListener(Public_Events.MSG_PC_REFRESH_SCORE, self.__cname)
    end
end

function BankPropertyView:initBtnEvent()

    self.m_pBtnConfirm:addClickEventListener(handler(self, self.onSureClicked))
    self.m_pBtnReset:addClickEventListener(handler(self, self.onCleanClicked))
    self.m_pBtnMax:addClickEventListener(handler(self, self.onMaxClicked))
    self.m_pBtnGold:addClickEventListener(handler(self, self.onAddGoldClicked))
    self.m_pSliderGold:addEventListener(handler(self, self.onSliderClicked))
    if CommonUtils.getInstance():isPCMode() then
        self.m_pBtnGet:addClickEventListener(handler(self, self.onGetClicked))
        self.m_pBtnGetRecord:addClickEventListener(handler(self, self.onGetRecordClicked))
        self.m_pBtnRefresh:addClickEventListener(handler(self, self.onRefreshClicked))
    end
end

function BankPropertyView:initEditBox()

    local str = 
    { 
        [TAG_OUT] = LuaUtils.getLocalString("BANK_15"),  --1/输入取出金额
        [TAG_IN]  = LuaUtils.getLocalString("BANK_14"),  --2/输入存入金额
    }
    local default_length = 15
    local input_return   = cc.KEYBOARD_RETURNTYPE_DONE
    local input_mode     = cc.EDITBOX_INPUT_MODE_NUMERIC
    local input_flag     = cc.EDITBOX_INPUT_FLAG_SENSITIVE
    local default_tag    = self.m_nType
    local default_size   = self.m_pLabelInput:getContentSize()
    local default_string = str[self.m_nType]

    if self.m_pEditBox == nil then
        self.m_pEditBox = self:createEditBox(default_length, input_return, input_mode, input_flag, default_tag, default_size, default_string)
        self.m_pEditBox:setText("")
        self.m_pEditBox:setPositionY(5)
        self.m_pEditBox:addTo(self.m_pLabelInput)
    end
end

function BankPropertyView:createEditBox(maxLength, keyboardReturnType, inputMode, inputFlag, tag, size, placestr)
    local sprite1 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite2 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite3 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    size = cc.size(size.width, 40)
    local editBox = cc.EditBox:create(size, sprite1, sprite2, sprite3)
    editBox:setMaxLength(maxLength)
    editBox:setReturnType(keyboardReturnType)
    editBox:setInputMode(inputMode)
    editBox:setInputFlag(inputFlag)
    editBox:setTag(tag)
    editBox:setFont("Helvetica", 28)
    editBox:setFontColor(G_CONSTANTS.INPUT_COLOR)
    editBox:setPlaceHolder(placestr)
    editBox:setPlaceholderFontSize(28)
    editBox:setPlaceholderFontColor(G_CONSTANTS.PLACE_COLOR)
    editBox:setAnchorPoint(cc.p(0, 0))
    editBox:setPositionY(5)
    editBox:registerScriptEditBoxHandler(handler(self, self.onEditBoxClicked))
   
    return editBox
end


function BankPropertyView:initNode()

    self.m_lUsrInsure = PlayerInfo.getInstance():getUserInsure()
    self.m_lUsrScore  = PlayerInfo.getInstance():getUserScore()
    
    for i = TAG_OUT, TAG_IN do
        self.m_pLabelInOut[i]:setVisible(i == self.m_nType)
--        self.m_pImageInOut[i]:setVisible(i == self.m_nType)
    end

    local stringScore =  LuaUtils.getFormatGoldAndNumber(self.m_lUsrScore)
    self.m_pLabelTake:setString(stringScore)

    local stringInsure =  LuaUtils.getFormatGoldAndNumber(self.m_lUsrInsure)
    self.m_pLabelSave:setString(stringInsure)

    self.m_pSliderGold:setMaxPercent(MAX_PERCENT)
    self.m_pSliderGold:setPercent(0)

    self.m_pLabelCHValue:setString("")

    --开关f
    local bCloseRecharge = GameListManager.getInstance():getGameSwitch(G_CONSTANTS.GAMESWITCH.RECHARGE)
    if bCloseRecharge then
        self.m_pBtnGold:setVisible(false)
    end

    self:onUpdateLabelPercent(0)
end

-----------------------------------------------------------
--收到数据

function BankPropertyView:onMsgRefreshScore(pUserdata)
    FloatMessage.getInstance():pushMessage("刷新成功")
    self:onUpdateUserScore()
    self:onUpdateUserInsure()
end

function BankPropertyView:onMsgBankInfo()

    --银商税收返利
    self:onUpdateRefundValue() 
    --从游戏返回大厅后，一直收到消息
    if self.m_lUsrInsure == PlayerInfo.getInstance():getUserInsure()
    and self.m_lUsrScore == PlayerInfo.getInstance():getUserScore()
    then
        return
    else
        self.m_lUsrInsure = PlayerInfo.getInstance():getUserInsure()
        self.m_lUsrScore = PlayerInfo.getInstance():getUserScore()
    end

    self.m_llInputScore = 0
    self:onUpdateUserScore()
    self:onUpdateUserInsure()
    self:onCleanEditBox()
    self:onUpdatePercent()
    self:onUpdateCHValue()
end

function BankPropertyView:onMsgBankSuccess(msg)

    --从游戏返回大厅后，一直收到消息
    if self.m_lUsrInsure == PlayerInfo.getInstance():getUserInsure()
    and self.m_lUsrScore == PlayerInfo.getInstance():getUserScore()
    then
        return
    else
        self.m_lUsrInsure = PlayerInfo.getInstance():getUserInsure()
        self.m_lUsrScore = PlayerInfo.getInstance():getUserScore()
    end

    self.m_llInputScore = 0
    self:onUpdateUserScore()
    self:onUpdateUserInsure()
    self:onCleanEditBox()
    self:onUpdatePercent()
    self:onUpdateCHValue()
end

-----------------------------------------------------------

function BankPropertyView:takeOut()
    local gold = self.m_llInputScore --tonumber(validStr)

    --请输入金额
    if gold == 0 then
        FloatMessage.getInstance():pushMessageWarning("BANK_17")
        return
    end

    --银行余额不足
    if tonumber(gold) > tonumber(PlayerInfo.getInstance():getUserInsure()) then
        FloatMessage.getInstance():pushMessageWarning("BANK_26")
        return
    end
   
    --开启遮罩
    Veil:getInstance():ShowVeil(VEIL_LOCK)
   
    --发送请求
    local strPwd = PlayerInfo.getInstance():getInsurePass()
    CMsgHall:sendTakeOut(gold, strPwd)
end

function BankPropertyView:saveIn()

    local gold = self.m_llInputScore --tonumber(validStr)

    --请输入金额
    if gold == 0 then     
        FloatMessage.getInstance():pushMessageWarning("BANK_17")
        return
    end
    
    --钱包余额不足
    if tonumber(gold) > tonumber(PlayerInfo.getInstance():getUserScore()) then
        FloatMessage.getInstance():pushMessageWarning("BANK_23")
        return
    end
    
    --开启遮罩
    Veil:getInstance():ShowVeil(VEIL_LOCK)
    
    --发送请求
    CMsgHall:sendSaveIn(gold)
end

function BankPropertyView:getGoldValidStr(gold)
    if not tonumber(gold) then
        FloatMessage.getInstance():pushMessageWarning("BANK_42")
        return ""
    end
    
    local pos = string.find(gold, ".", 1, true)
    if pos then -- 如果有小数点，要判断两个小数点有效数字
        local vecStr = string.split(tostring(gold), ".")
        if #vecStr > 2 then
            FloatMessage.getInstance():pushMessageWarning("BANK_42")
            return ""
        end
        
        if #vecStr[2] > 2 then
            FloatMessage.getInstance():pushMessageWarning("BANK_48")
            return ""
        elseif #vecStr[2] == 2 then
            return "" .. vecStr[1] .. vecStr[2]
        elseif #vecStr[2] == 1 then
            return "" .. vecStr[1] .. vecStr[2] .. "0"
        elseif #vecStr[2] == 0 then
            return "" .. vecStr[1] .. vecStr[2] .. "00"
        end
    else
       return  gold .. "00"
    end

    return gold
end

---------------------------------------------------------------------
--更新界面

function BankPropertyView:onUpdateUserScore()
    
    local str = LuaUtils.getFormatGoldAndNumber(self.m_lUsrScore)
    self.m_pLabelTake:setString(str)
end

function BankPropertyView:onUpdateUserInsure()
    
    local str = LuaUtils.getFormatGoldAndNumber(self.m_lUsrInsure)
    self.m_pLabelSave:setString(str)
end

--高亮百分比图片
function BankPropertyView:onUpdateImagePercent(percent)
    
    --灰色小点
    for i = 0, 10 do
        self.m_vecPoint[i]:setVisible(i ~= percent)
    end

    --绿色小点
    local posX, posY = self.m_vecPoint[percent]:getPosition()
    self.m_pImageCurrent:setPositionX(posX)
end

--高亮百分比文字
function BankPropertyView:onUpdateLabelPercent(percent)

    --百分比颜色
    for i = 0, 10 do
        if i == percent then
            self.m_vecPercent[i]:setColor(cc.c3b(110, 238, 83)) --绿色
        else
            self.m_vecPercent[i]:setColor(cc.c3b(116, 185, 255)) --浅色
        end
    end
end

--更新滑动条进度
function BankPropertyView:onUpdateSliderPercent(percent)
    
    self.m_pSliderGold:setPercent(percent)
end

--更新输入框数字
function BankPropertyView:onUpdateEditBoxPercent(percent)

    local number = 0 
    if self.m_nType == TAG_OUT then
        number = self.m_lUsrInsure 
    else
        number = self.m_lUsrScore
    end
    if percent == 0 then
        self.m_llInputScore = 0
        self.m_pEditBox:setText("")
    else
        self.m_llInputScore = math.floor(number * percent / MAX_PERCENT)
        local string_number = LuaUtils.getFormatGoldAndNumber(self.m_llInputScore) --string.format("%0.2f",  number * percent / MAX_PERCENT / 100)
        self.m_pEditBox:setText(string_number)
    end
end

--清空输入框数字
function BankPropertyView:onCleanEditBox()
    self.m_pEditBox:setText("")
end

--更新进度
function BankPropertyView:onUpdatePercent()

    local number = self.m_llInputScore --tonumber(self.m_pEditBox:getText()) or 0
    local total = (self.m_nType == TAG_OUT) and self.m_lUsrInsure or self.m_lUsrScore
    local percent = total > 0 and (number/ total) or 0
    local percent_string = string.format("%.1f", percent)
    local percent_show = tonumber(percent_string * MAX_PERCENT)

    --print(number, percent, percent_string, percent_show)
    --FloatMessage.getInstance():pushMessage(number .. "," .. percent .. "," .. percent_string .. "," .. percent_show)

    if type(percent) ~= "number" or percent < 0 or 10 < percent then
        FloatMessage.getInstance():pushMessage("STRING_212_0") --输入有错误
        self.m_pEditBox:setText("") 
        percent_show = 0
    end

    self:onUpdateImagePercent(percent_show)
    self:onUpdateLabelPercent(percent_show)
    self:onUpdateSliderPercent(percent_show)
end
--更新金额大写中文
function BankPropertyView:onUpdateCHValue()

    local strCH = self.m_llInputScore == 0 and "" or LuaUtils.numberToCN(self.m_llInputScore)
    self.m_pLabelCHValue:setString(strCH)
end

--银商可领取返利
function BankPropertyView:onUpdateRefundValue()
    if not CommonUtils.getInstance():isPCMode() then
        return
    end
    local refundScore = PlayerInfo.getInstance():getRevenueRefundScore()
    self.m_pLbRefundScore:setString("当前可领取返回金币："..LuaUtils.getFormatGoldAndNumber(refundScore))
end

-------------------------------

-------------------------------
-- 按纽响应

--输入框
function BankPropertyView:onEditBoxClicked(event, pSender)
    if "began" == event then
        AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
        SLFacade:dispatchCustomEvent(Public_Events.MSG_NULL_CLOSE_ENABLED,"0")
    elseif event == "changed"
    or     event == "ended"
    then
        self:editBoxEditingDidEnd(pSender)
    elseif event == "return" then 
        --延迟设为可点击 防止还是响应到关闭界面
        local callback = cc.CallFunc:create(function ()
            SLFacade:dispatchCustomEvent(Public_Events.MSG_NULL_CLOSE_ENABLED,"1")
        end)
        local seq = cc.Sequence:create(cc.DelayTime:create(0.16), callback)
        self:runAction(seq)
    end
end

function BankPropertyView:editBoxEditingDidEnd(pSender)

    local str = self.m_pEditBox:getText()
    self.m_llInputScore = math.floor(LuaUtils.ConverNumber(str))
    local maxScore = self.m_nType == TAG_OUT and self.m_lUsrInsure or self.m_lUsrScore
    if self.m_llInputScore >= maxScore then 
        if maxScore == 0 and self.m_llInputScore ~= 0 then 
            local strTips = self.m_nType == 1 and "BANK_23" or "BANK_26"
            FloatMessage.getInstance():pushMessage(strTips)
        end
        self.m_llInputScore = maxScore
    end
    if self.m_llInputScore > 0 then
        local strScore = LuaUtils.getFormatGoldAndNumber(self.m_llInputScore)
        pSender:setText(strScore)
    else
        pSender:setText("")
    end
    self:onUpdatePercent()
    self:onUpdateCHValue()
end

--滑动条
function BankPropertyView:onSliderClicked(sender, eventType)

    print(sender, eventType)

    --提示错误
    if eventType == ccui.SliderEventType.slideBallDown then --按下
        if self.m_nType == TAG_IN and self.m_lUsrScore == 0 then
            FloatMessage.getInstance():pushMessageWarning("BANK_50") --钱包没有余额,无法存款
            return
        elseif self.m_nType == TAG_OUT and self.m_lUsrInsure == 0 then
            FloatMessage.getInstance():pushMessageWarning("BANK_51") --银行没有余额,无法取款
            return
        end
    end

    --错误返回
    if eventType == ccui.SliderEventType.percentChanged  --拖动
    or eventType == ccui.SliderEventType.slideBallCancel --取消
    or eventType == ccui.SliderEventType.slideBallUp     --抬起
    then
        if self.m_nType == TAG_IN and self.m_lUsrScore == 0
        or self.m_nType == TAG_OUT and self.m_lUsrInsure == 0
        then
            self:onUpdateSliderPercent(0)
            return
        end
    end


    --更新界面
    local percent = self.m_pSliderGold:getPercent()
    self:onUpdateImagePercent(percent)
    self:onUpdateLabelPercent(percent)
    self:onUpdateEditBoxPercent(percent)
    self:onUpdateCHValue()
end

--最大
function BankPropertyView:onMaxClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    if self.m_nType == TAG_IN and self.m_lUsrScore == 0 then 
        FloatMessage.getInstance():pushMessageWarning("BANK_50") --钱包没有余额,无法存款
        return
    elseif self.m_nType == TAG_OUT and self.m_lUsrInsure == 0 then
        FloatMessage.getInstance():pushMessageWarning("BANK_51") --银行没有余额,无法取款
        return
    end

    self:onUpdateImagePercent(MAX_PERCENT)
    self:onUpdateLabelPercent(MAX_PERCENT)
    self:onUpdateSliderPercent(MAX_PERCENT)
    self:onUpdateEditBoxPercent(MAX_PERCENT)
    self:onUpdateCHValue()
end

--重置
function BankPropertyView:onCleanClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    self:onUpdateImagePercent(0)
    self:onUpdateLabelPercent(0)
    self:onUpdateSliderPercent(0)
    self:onUpdateEditBoxPercent(0)
    self:onUpdateCHValue()
end

--确定
function BankPropertyView:onSureClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    -- 防连点 ----------------
    local nCurTime = os.time();
    if self.m_nLastTouchTime and nCurTime - self.m_nLastTouchTime <= 0 then
        return
    end
    self.m_nLastTouchTime = nCurTime
    ---------------------------
    if     self.m_nType == TAG_OUT then self:takeOut()
    elseif self.m_nType == TAG_IN  then self:saveIn()
    end
end

-- 充值
function BankPropertyView:onAddGoldClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    local event = {
        name = Public_Events.MSG_SHOW_SHOP_HALL,
        packet = {},
    }
    SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_SHOP_HALL, event)
end

--领取
function BankPropertyView:onGetClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    CMsgHall:sendRefund()
end

--领取记录
function BankPropertyView:onGetRecordClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    local pRefundLog = self:getParent():getParent():getParent():getParent():getChildByName("BankRefundLogView")
    if pRefundLog then
        pRefundLog:setVisible(true)
        return
    end
    pRefundLog = BankRefundLogView:create()
    pRefundLog:setName("BankRefundLogView")
    self:getParent():getParent():getParent():getParent():addChild(pRefundLog, 501)

end

--刷新按钮
function BankPropertyView:onRefreshClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    CMsgHall:sendQueryInsureInfoLite()
end
-------------------------------

return BankPropertyView
