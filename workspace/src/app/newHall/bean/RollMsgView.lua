
local RollMsgView = class("RollMsgView", FixLayer)

RollMsgView.instance_ = nil
function RollMsgView:create()
    RollMsgView.instance_ = RollMsgView.new()
    return RollMsgView.instance_
end

function RollMsgView:ctor()
    self:enableNodeEvents()
    self.super:ctor(self)
    self:init()
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_BIG, FixLayer.HIDE_DLG_BIG)
end

function RollMsgView:onEnter()
    self.super:onEnter()
    self:showWithStyle()
end

function RollMsgView:onExit()
    self.super:onExit()
end

function RollMsgView:init()
    self:initCCB()
end

function RollMsgView:initCCB()
    
    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)

    --init csb
    local strPath = isPortraitView() and "hall/csb/RollMsgView-p.csb" or "hall/csb/RollMsgView.csb"
    self.m_pathUI = cc.CSLoader:createNode(strPath)
    self.m_pathUI:addTo(self.m_rootUI)

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("RollMsgDialog")
    local diffX = 145 - (1624-display.width)/2
    if isPortraitView() then 
        self.m_pNodeRoot:setPositionY(diffX)
    else
        self.m_pNodeRoot:setPositionX(diffX)
    end

    self.m_pImgBg       = self.m_pNodeRoot:getChildByName("Image_bg")
    self.Label_msg      = self.m_pImgBg:getChildByName("LB_text")

    self.m_pBtnClose1 = self.m_pNodeRoot:getChildByName("BTN_close_0")
    self.m_pBtnClose2 = self.m_pImgBg:getChildByName("BTN_close")
    self.m_pBtnClose1:addClickEventListener(handler(self,self.onCloseClicked))
    self.m_pBtnClose2:addClickEventListener(handler(self,self.onCloseClicked))
end

function RollMsgView:initWithMsg(msg)

    self.Label_msg:setString(msg)
end

function RollMsgView:onCloseClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    self:onMoveExitView()

    if PlayerInfo.getInstance():getExperienceGuide() == 10 then
        PlayerInfo.getInstance():setExperienceGuide(11)
        local _event = {
            name = Public_Events.MSG_OPEN_HALL_NOTICE,
        }
        SLFacade:dispatchCustomEvent(Public_Events.MSG_OPEN_HALL_NOTICE, _event)
    end
end

return RollMsgView
