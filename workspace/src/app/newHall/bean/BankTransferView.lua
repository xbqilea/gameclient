--region BankTransferView.lua
--Date 2017.04.26.
--Auther JackyXu.
--Desc: 转帐 view

local UsrMsgManager = require("common.manager.UsrMsgManager")

local BankTransferLogView = require("hall.bean.BankTransferLogView")
local TransferDetailDialog = require("hall.bean.TransferDetailDialog")
local NoticeDialog      = require("common.layer.NoticeDialog")  --提示框

local BankTransferView = class("BankTransferView", cc.Layer)

function BankTransferView:ctor()

    self:enableNodeEvents()

    self.m_llInputScore = 0
    self.m_pEditBox = {}
    self.m_nEditBoxTouchCount = 0 -- 输入框点击次数（安卓机bug）

    self.m_nTransferScore = 0
    self.m_strTargetID = ""
    self.m_strTargetName = ""
    self.m_nSelectIndex = 0

    self.m_pllAddGold = { 100000, 1000000, 10000000, 100000000 }
end

function BankTransferView:init()
    
    self:initCSB()
    self:initEvent()
    self:initBtnEvent()
    self:initEditBox()
    self:initNode()
    if device.platform == "windows" then
        self:initKeyboard()  
    end
end

function BankTransferView:clean()
    self:cleanEvent()
    if device.platform == "windows" then
        self:cleanKeyboard()
    end
end

function BankTransferView:initCSB()
    
    -- root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    -- csb
    local strPath = "hall/csb/BankViewTransfer.csb"
    if CommonUtils.getInstance():isPCMode() then
        strPath = "hall/csb/pc-BankViewTransfer.csb"
    end
    self.m_pathUI = cc.CSLoader:createNode(strPath)
    self.m_pathUI:addTo(self.m_rootUI)

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("Panel_1")

    local Node_1 = self.m_pNodeRoot:getChildByName("Node_1")
    local Node_2 = self.m_pNodeRoot:getChildByName("Node_2")
    self.Node_3 = self.m_pNodeRoot:getChildByName("Node_3")
    local Node_4 = self.m_pNodeRoot:getChildByName("Node_4")

    --node 1
    self.m_pLabelTake = Node_1:getChildByName("BitmapFontLabel_1")
    self.m_pLabelSave = Node_1:getChildByName("BitmapFontLabel_2")

    --node 2
    self.m_pNodeEdit = {}
    self.m_pNodeEdit[1] = Node_2:getChildByName("Node_1"):getChildByName("node_input1")
    self.m_pNodeEdit[2] = Node_2:getChildByName("Node_2"):getChildByName("node_input2")
    self.m_pLbTargetName    = Node_2:getChildByName("Text_2")
    self.m_pCHValue     = Node_2:getChildByName("Text_3")
    self.m_pRateGold    = Node_2:getChildByName("Text_4")

    --node 3
    self.m_pBtnAdd = {}
    self.m_pLbAdd = {}
    for i=1,4 do
        self.m_pBtnAdd[i] = self.Node_3:getChildByName("Node_"..i):getChildByName("Button_"..i)
        self.m_pBtnAdd[i]:setTag(i)
        self.m_pLbAdd[i] = self.Node_3:getChildByName("Node_"..i):getChildByName("BitmapFontLabel_3")
    end
    self.m_pBtnRecord = self.Node_3:getChildByName("Button_5")

    --node 4
    self.m_pBtnReset    = Node_4:getChildByName("Button_3")
    self.m_pBtnConfirm  = Node_4:getChildByName("Button_4")

    --pc
    if CommonUtils.getInstance():isPCMode() then
        local Node_5 = self.m_pNodeRoot:getChildByName("Node_5")
        --node 5
        self.m_pBtnSet = Node_5:getChildByName("Button_1")
        self.m_pBtnRefresh = Node_5:getChildByName("Button_2")
        self.m_pNodeSet = Node_5:getChildByName("Node_1")
        self.m_pNodeSet:setVisible(false)
        self.m_pBtnSetCancel = self.m_pNodeSet:getChildByName("Button_1")
        self.m_pBtnSetConfirm = self.m_pNodeSet:getChildByName("Button_2")
        --玩家设置的转账额度
        local strKey = tostring(PlayerInfo.getInstance():getUserID()).."SetTansferGold"
        local strValus = cc.UserDefault:getInstance():getStringForKey(strKey, "")
        local vecStr = string.split(tostring(strValus), ",")
        if #vecStr == 4 then
            for i=1, 4 do
                self.m_pllAddGold[i] = tonumber(vecStr[i])
            end
        end
        for i=1, 4 do
            self.m_pLbAdd[i]:setScale(0.8)
            self.m_pLbAdd[i]:setPositionX(-4)
        end
        self:onUpdateBtnGold()
    end

    return self
end

function BankTransferView:onEnter()

    self:init()
end

function BankTransferView:onExit()

    self:clean()
end

--监听按键
function BankTransferView:initKeyboard() 
    local function onKeyReleased(keyCode, event)
        event:stopPropagation()
        if keyCode == cc.KeyCode.KEY_ENTER or keyCode == cc.KeyCode.KEY_KP_ENTER then 
            print("KEY ENTER.")
            if self.m_pNodeSet:isVisible() then 
                self:onSetConfirmClicked()
            else
                self:startTransfer()
            end
        elseif keyCode == cc.KeyCode.KEY_TAB then
            print("KEY TAB.")
            self:tabToNext()
        elseif keyCode == cc.KeyCode.KEY_SPACE then
            print("KEY_SPACE.")
            self:onAddScore(self.m_nSelectIndex)
        end
    end

    local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
    self.m_pKeyboardListener = cc.EventListenerKeyboard:create()
    self.m_pKeyboardListener:registerScriptHandler(onKeyReleased, cc.Handler.EVENT_KEYBOARD_RELEASED)
    pEventDispatcher:addEventListenerWithFixedPriority(self.m_pKeyboardListener, -129)
end

--移除按键监听
function BankTransferView:cleanKeyboard()

    if self.m_pKeyboardListener then
        local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
        pEventDispatcher:removeEventListener(self.m_pKeyboardListener)
        self.m_pKeyboardListener = nil
    end
end

function BankTransferView:tabToNext()
    
    local max = self.m_pNodeSet:isVisible() and 6 or 2
    local index = self.m_nEditBoxTouchCount+1 > max and 1 or self.m_nEditBoxTouchCount+1
    if self.m_nEditBoxTouchCount > 0 then 
        self.m_pEditBox[self.m_nEditBoxTouchCount]:touchDownAction(self.m_pEditBox[self.m_nEditBoxTouchCount],ccui.TouchEventType.canceled);
    end
    self.m_pEditBox[index]:touchDownAction(self.m_pEditBox[index],ccui.TouchEventType.ended);
    self.m_nEditBoxTouchCount = index
end
-------------------

function BankTransferView:initEvent()

    SLFacade:addCustomEventListener(Public_Events.MSG_BANK_INFO, handler(self, self.onMsgBankInfo), self.__cname)
    SLFacade:addCustomEventListener(Hall_Events.MSG_BANK_USER_INFO, handler(self, self.onMsgBankUserInfo), self.__cname)
    SLFacade:addCustomEventListener(Public_Events.MSG_BANK_SUCCESS_INFO, handler(self, self.onMsgClear), self.__cname)
    SLFacade:addCustomEventListener(Public_Events.MSG_BANK_CLEAR_INFO, handler(self, self.onMsgClear), self.__cname)
    SLFacade:addCustomEventListener(Hall_Events.MSG_TRANSFER_DETAIL, handler(self, self.onMsgTransferDetail), self.__cname)
    if CommonUtils.getInstance():isPCMode() then --pc 有刷新按钮
        SLFacade:addCustomEventListener(Public_Events.MSG_PC_REFRESH_SCORE, handler(self, self.onMsgRefreshScore), self.__cname)
        SLFacade:addCustomEventListener(Hall_Events.MSG_PC_COMFIRM_TRANSFER, handler(self, self.onMsgComfirmTransfer), self.__cname)
    end
end

function BankTransferView:cleanEvent()

    SLFacade:removeCustomEventListener(Public_Events.MSG_BANK_INFO, self.__cname)
    SLFacade:removeCustomEventListener(Hall_Events.MSG_BANK_USER_INFO, self.__cname)
    SLFacade:removeCustomEventListener(Public_Events.MSG_BANK_SUCCESS_INFO, self.__cname)
    SLFacade:removeCustomEventListener(Public_Events.MSG_BANK_CLEAR_INFO, self.__cname)
    SLFacade:removeCustomEventListener(Hall_Events.MSG_TRANSFER_DETAIL, self.__cname)
    if CommonUtils.getInstance():isPCMode() then 
        SLFacade:removeCustomEventListener(Public_Events.MSG_PC_REFRESH_SCORE, self.__cname)
        SLFacade:removeCustomEventListener(Hall_Events.MSG_PC_COMFIRM_TRANSFER, self.__cname)
    end
end

function BankTransferView:initBtnEvent()

    self.m_pBtnConfirm:addClickEventListener(handler(self, self.onSureClicked))
    self.m_pBtnReset:addClickEventListener(handler(self, self.onCleanClicked))
    self.m_pBtnRecord:addClickEventListener(handler(self, self.onLogClicked))
    for i=1,4 do
        self.m_pBtnAdd[i]:addClickEventListener(handler(self, self.onAddClicked))
    end
    if CommonUtils.getInstance():isPCMode() then
        self.m_pBtnSet:addClickEventListener(handler(self, self.onSetClicked))
        self.m_pBtnRefresh:addClickEventListener(handler(self, self.onRefreshClicked))
        self.m_pBtnSetCancel:addClickEventListener(handler(self, self.onSetCancelClicked))
        self.m_pBtnSetConfirm:addClickEventListener(handler(self, self.onSetConfirmClicked))
    end
end

function BankTransferView:initEditBox()
    -- body
    local RateStr = string.format(LuaUtils.getLocalString("BANK_3"), self:getTransferRate())
    if self:getTransferRate() == 0 or PlayerInfo.getInstance():getTransferUserIsProxy() then
        RateStr = LuaUtils.getLocalString("BANK_41")
    end
     local PrerequisiteStr
     local  vipInfo = UsrMsgManager.getInstance():getVipInfoByLevel(PlayerInfo.getInstance():getVipLevel())
     if vipInfo.lTransferMoney == -1 then
           -- 无限制
         PrerequisiteStr = string.format(LuaUtils.getLocalString("STRING_119"),PlayerInfo.getInstance():getVipLevel())
     else
         local lastTransferMoney = PlayerInfo.getInstance():getLastTransferMoney()
         local str =  LuaUtils.getFormatGoldAndNumber(lastTransferMoney)
         PrerequisiteStr = string.format(LuaUtils.getLocalString("STRING_117"), str)
      end

    local str = {RateStr, PrerequisiteStr}
    local lenght = {9,15}
    for i=1,2 do
        if not self.m_pEditBox[i] then
            self.m_pEditBox[i] = self:createEditBox(lenght[i],
                                      cc.KEYBOARD_RETURNTYPE_DONE,
                                      cc.EDITBOX_INPUT_MODE_NUMERIC,
                                      cc.EDITBOX_INPUT_FLAG_SENSITIVE,
                                      i,
                                      self.m_pNodeEdit[i]:getContentSize(),
                                      str[i])
            self.m_pEditBox[i]:setPosition(cc.p(0, 5))
            self.m_pEditBox[i]:setText("")
            self.m_pNodeEdit[i]:addChild(self.m_pEditBox[i])
        end
    end

    --self.m_pEditBoxSet = {}
    if CommonUtils.getInstance():isPCMode() then 
        for i=3,6 do 
            local sp = self.m_pNodeSet:getChildByName("Node_2"):getChildByName("Sprite_"..i-2)
            self.m_pEditBox[i] = self:createEditBox(10,
                                          cc.KEYBOARD_RETURNTYPE_DONE,
                                          cc.EDITBOX_INPUT_MODE_NUMERIC,
                                          cc.EDITBOX_INPUT_FLAG_SENSITIVE,
                                          i,
                                          cc.size(sp:getContentSize().width-20,sp:getContentSize().hegiht),
                                          "请输入")
            self.m_pEditBox[i]:setPosition(cc.p(10, 10))
            self.m_pEditBox[i]:setText(tostring(self.m_pllAddGold[i-2]))
            sp:addChild(self.m_pEditBox[i])
        end
    end
end

function BankTransferView:createEditBox(maxLength, keyboardReturnType, inputMode, inputFlag, tag, size, placestr)
    local sprite1 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite2 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite3 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    size = cc.size(size.width, 40)
    local editBox = cc.EditBox:create(size, sprite1, sprite2, sprite3)
    editBox:setMaxLength(maxLength)
    editBox:setReturnType(keyboardReturnType)
    editBox:setInputMode(inputMode)
    editBox:setInputFlag(inputFlag)
    editBox:setTag(tag)
    editBox:setFont("Microsoft Yahei", 28)
    editBox:setFontColor(G_CONSTANTS.INPUT_COLOR)
    editBox:setPlaceHolder(placestr)
    editBox:setPlaceholderFontSize(28)
    editBox:setPlaceholderFontColor(G_CONSTANTS.PLACE_COLOR)
    editBox:setAnchorPoint(cc.p(0, 0))
    editBox:setPositionY(5)
    editBox:registerScriptEditBoxHandler(handler(self, self.onEditBoxClicked))
    
--    -- 模拟点击输入框解决 android 手机首次输入看不到输入内容BUG。
--    if device.platform == "android" then
--        self.m_nEditBoxTouchCount = 0
--        editBox:touchDownAction(editBox,ccui.TouchEventType.ended);
--        editBox:touchDownAction(editBox,ccui.TouchEventType.canceled);
--    else
--        self.m_nEditBoxTouchCount = 3
--    end

    return editBox
end

function BankTransferView:onEditBoxClicked(event, pSender)
    -- body
    if "began" == event then
        AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
        SLFacade:dispatchCustomEvent(Public_Events.MSG_NULL_CLOSE_ENABLED,"0")
        self.m_nEditBoxTouchCount = pSender:getTag()
    elseif event == "ended" then
        self:editBoxEditingDidEnd(pSender)
    elseif event == "changed" then 
        if  pSender:getTag()==2 then
            self:editBoxEditingDidEnd(pSender)
        end
    elseif "return" == event then
        --延迟设为可点击 防止还是响应到关闭界面
        local callback = cc.CallFunc:create(function ()
            SLFacade:dispatchCustomEvent(Public_Events.MSG_NULL_CLOSE_ENABLED,"1")
        end)
        local seq = cc.Sequence:create(cc.DelayTime:create(0.16), callback)
        self:runAction(seq)
    elseif "next" == event then
        self:tabToNext()
    end
end

function BankTransferView:editBoxEditingDidEnd(editBox)
    local nTag = editBox:getTag()
    if nTag == 1 then
        self:sendRequestTargetName()
    elseif nTag == 2 then
        self.m_llInputScore = math.floor(LuaUtils.ConverNumber(self.m_pEditBox[2]:getText()))
        local maxScore = PlayerInfo.getInstance():getUserInsure()
        if self.m_llInputScore >= maxScore then 
            if maxScore == 0  and self.m_llInputScore ~= 0 then  
                FloatMessage.getInstance():pushMessage("BANK_26")
            end
            self.m_llInputScore = maxScore
        end
        if self.m_llInputScore > 0 then
            self:updateTransferRate()
            local strScore = LuaUtils.getFormatGoldAndNumber(self.m_llInputScore)
            self.m_pEditBox[2]:setText(strScore)
            --中文数字
            local strCH = LuaUtils.numberToCN(self.m_llInputScore)
            self.m_pCHValue:setString(strCH)
        else
            self.m_pRateGold:setString("")
            self.m_pEditBox[2]:setText("")
            self.m_pCHValue:setString("")
        end
    end
end

function BankTransferView:initNode()

    self.m_pLbTargetName:setString("")
    self.m_pCHValue:setString("")
    self:onUpdateUserInsure()
    self:onUpdateUserScore()
end
---------------------------msg------------------------------
function BankTransferView:onMsgRefreshScore(pUserdata)
    FloatMessage.getInstance():pushMessage("刷新成功")
    self:onUpdateUserScore()
    self:onUpdateUserInsure()
end

function BankTransferView:onMsgComfirmTransfer(pUserdata)
    self:doSendTransfer()
end

function BankTransferView:onMsgBankInfo(pUserdata)
    self:onUpdateUserInsure()
end

function BankTransferView:onMsgBankUserInfo(_event)

    local _userdata = unpack(_event._userdata);
    if not _userdata then
        return
    end
    local eventID = _userdata.name
    local msg = _userdata.packet
    self.m_strTargetName = LuaUtils.getDisplayStringByHideLength(msg.nickName,3,"...")
    self.m_pLbTargetName:setString(self.m_strTargetName)
    self:updateTransferRate()
end

function BankTransferView:onMsgClear(pUserdata)
    for i=1, 2 do
        self.m_pEditBox[i]:setText("")
    end
    self.m_llInputScore = 0
    self.m_pLbTargetName:setString("")
    self.m_pCHValue:setString("")
    self.m_pRateGold:setString(string.format(LuaUtils.getLocalString("BANK_52"), self:getTransferRate()))
end

function BankTransferView:onMsgTransferDetail(_event)
    
    if not CommonUtils.getInstance():isPCMode() then
        return
    end
    Veil:getInstance():HideVeil(VEIL_LOCK)
    local _userdata = unpack(_event._userdata)
    if not _userdata then
        return
    end
    self:showTransferDetailDlg(_userdata)
end
----------------------------------------------------------------
function BankTransferView:onUpdateUserScore()
    
    self.m_lUsrScore  = PlayerInfo.getInstance():getUserScore()
    local str = LuaUtils.getFormatGoldAndNumber(self.m_lUsrScore)
    self.m_pLabelTake:setString(str)
end

function BankTransferView:onUpdateUserInsure()
    
    self.m_lUsrInsure = PlayerInfo.getInstance():getUserInsure()
    local str = LuaUtils.getFormatGoldAndNumber(self.m_lUsrInsure)
    self.m_pLabelSave:setString(str)
end

--请求被转账方昵称
function BankTransferView:sendRequestTargetName()
    self.m_pLbTargetName:setString("")
    local strRecipientID = self.m_pEditBox[1]:getText()
    if string.len(strRecipientID) <= 0 then
        return
    end
    
    if not tonumber(strRecipientID) then
         FloatMessage.getInstance():pushMessage("BANK_40")
        return
    end
    
    CMsgHall:sendRequesUserInfo(strRecipientID)
end

function BankTransferView:getTransferRate()
    local bankRate = PlayerInfo.getInstance():getBankTransferRate()
    local rate = bankRate/1000.0*100 --[[LuaUtils.DataConvertToPercent(1000, bankRate)]]
    return rate
end

-- 转账手续费
function BankTransferView:updateTransferRate()

    local curMoney = self.m_llInputScore 
    if curMoney > 0 and PlayerInfo.getInstance():getTransferUserVipLevel() < 10 then -- svip不收取手续费
        local rateLimit = PlayerInfo.getInstance():getTransferLimit()
        local transferRate = 0
        local rate = self:getTransferRate()
        if rateLimit > 0 then
            -- 有转账手续费最大限额
            transferRate = (curMoney*rate/100 > rateLimit and rateLimit or curMoney*rate/100)
        else
            transferRate = curMoney*rate/100
        end
        local strGold = LuaUtils.getFormatGoldAndNumber(transferRate)
        local str = string.format(LuaUtils.getLocalString("BANK_46"), strGold)
        self.m_pRateGold:setString(str)
    else
        self.m_pRateGold:setString("")
    end
end

function BankTransferView:startTransfer()

    local strRecipientID = self.m_pEditBox[1]:getText() or "" 
    local strOwnGameID = string.format("%d", PlayerInfo.getInstance():getGameID())
    -- 不能向自己转账
    if strOwnGameID == strRecipientID then
         FloatMessage.getInstance():pushMessage("BANK_34")
        return
    end
    -- 请输入接收人ID
    if string.len(strRecipientID) <= 0 then
        FloatMessage.getInstance():pushMessage("BANK_11")
        return
    elseif string.len(strRecipientID) > 9 then
        -- 输入接收人的 ID 不能超过9位
        FloatMessage.getInstance():pushMessage("BANK_29")
        return
    end

    local score = self.m_llInputScore
    -- 请输入转账金额
    if score <= 0 then 
       FloatMessage.getInstance():pushMessage("BANK_12")
       return       
    end

    --转账金额不足
    if score > PlayerInfo.getInstance():getUserInsure() then
        FloatMessage.getInstance():pushMessage("BANK_26")
        return
    end

    --单笔限制9,999,999,999
    if score > 9999999999 then
        FloatMessage.getInstance():pushMessage("BANK_44")
        return
    end

     --vip 限制
     if PlayerInfo.getInstance():getTransferUserIsUnlimit() ~= -1 then
         local vipInfo = UsrMsgManager.getInstance():getVipInfoByLevel(PlayerInfo.getInstance():getVipLevel())
         if score > PlayerInfo.getInstance():getLastTransferMoney() and vipInfo.lTransferMoney ~= -1 then        
             SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_MESSAGEBOX, "transfer-no-balance")   
             return
         end
     end
    
     self.m_nTransferScore = score
     self.m_strTargetID = strRecipientID
     if CommonUtils.getInstance():isPCMode() then
        --pc二次确认框
        local strTrScore = LuaUtils.getFormatGoldAndNumber(self.m_nTransferScore)
        local str = string.format("您确定要给【%s】（%s），转账%s金币吗？",self.m_strTargetID, self.m_strTargetName, strTrScore)
        SLFacade:dispatchCustomEvent(Hall_Events.MSG_PC_SHOW_COMFIRM_TRANSFER, str)
    else
        self:doSendTransfer()
    end
end

function BankTransferView:doSendTransfer()
    Veil:getInstance():ShowVeil(VEIL_LOCK)
    CMsgHall:sendTransfer(self.m_nTransferScore,self.m_strTargetID)
end

function BankTransferView:onUpdateBtnGold()
    
    for i=1,4 do
        self.m_pLbAdd[i]:setString("+"..LuaUtils.getGoldNumberZH2(self.m_pllAddGold[i]))
    end
end

--转账凭证界面
function BankTransferView:showTransferDetailDlg(data)

    local pTransferDetailDlg = TransferDetailDialog:create()
    pTransferDetailDlg:setDetailInfo(data)
    pTransferDetailDlg:setName("TransferDetailDialog")
    self:getParent():getParent():getParent():getParent():addChild(pTransferDetailDlg, 500)
end

function BankTransferView:onAddScore(index)
    if index == 0 then 
        return
    end
    self.m_nSelectIndex = index
    local curGold = self.m_llInputScore + self.m_pllAddGold[index]
    if curGold > 9999999999 then
        FloatMessage.getInstance():pushMessage("BANK_44")
        return
    end
    if curGold > PlayerInfo.getInstance():getUserInsure() then 
        FloatMessage.getInstance():pushMessage("BANK_26")
        return
    end
    self.m_llInputScore = curGold
    local strTextGold = LuaUtils.getFormatGoldAndNumber(self.m_llInputScore)
    self.m_pEditBox[2]:setText(strTextGold)
    --中文数字
    local strCH = LuaUtils.numberToCN(self.m_llInputScore)
    self.m_pCHValue:setString(strCH)
end
----------------------------
-- 按纽响应

-- 确定转账
function BankTransferView:onSureClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    self:startTransfer()
end

function BankTransferView:onAddClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    local index = pSender:getTag()
    --按钮选中状态
    if index ~= self.m_nSelectIndex then 
        if self.m_nSelectIndex ~= 0 then 
            self.m_pBtnAdd[self.m_nSelectIndex]:loadTextureNormal("hall/plist/bank/gui-bank-btn-1.png", ccui.TextureResType.plistType)
        end
        self.m_pBtnAdd[index]:loadTextureNormal("hall/plist/bank/gui-bank-btn-2.png", ccui.TextureResType.plistType)
    end
    self:onAddScore(index)
end

-- 转账记录
function BankTransferView:onLogClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    local pTransferLog = self:getParent():getParent():getParent():getParent():getChildByName("BankTransferLogView")
    if pTransferLog then
        pTransferLog:setVisible(true)
        return
    end
    pTransferLog = BankTransferLogView:create()
    pTransferLog:setName("BankTransferLogView")
    self:getParent():getParent():getParent():getParent():addChild(pTransferLog, 500)
end

--清除
function BankTransferView:onCleanClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    
    self.m_pEditBox[2]:setText("")
    self.m_llInputScore = 0
    self.m_pCHValue:setString("")
end

--设置转装按钮金额
function BankTransferView:onSetClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    
    self.m_pNodeSet:setVisible(true)
    self.Node_3:setVisible(false)
    self.m_pBtnSet:setVisible(false)
    self.m_pBtnRefresh:setVisible(false)
    for i=3,6 do
        self.m_pEditBox[i]:setText(tostring(self.m_pllAddGold[i-2]))
    end
end

--刷新按钮
function BankTransferView:onRefreshClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    CMsgHall:sendQueryInsureInfoLite()
end

function BankTransferView:onSetCancelClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    
    self.m_pNodeSet:setVisible(false)
    self.Node_3:setVisible(true)
    self.m_pBtnSet:setVisible(true)
    self.m_pBtnRefresh:setVisible(true)
end

function BankTransferView:onSetConfirmClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    
    self.m_pNodeSet:setVisible(false)
    self.Node_3:setVisible(true)
    self.m_pBtnSet:setVisible(true)
    self.m_pBtnRefresh:setVisible(true)
    local strValues = ""
    for i=3,6 do
        local strText = self.m_pEditBox[i]:getText()
        local gold = tonumber(strText)
        if gold ~= nil and gold > 0 then
            self.m_pllAddGold[i-2] = gold
        end
        local str = i == 6 and tostring(self.m_pllAddGold[i-2]) or tostring(self.m_pllAddGold[i-2]).."," 
        strValues = strValues..str
    end
    self:onUpdateBtnGold()
    --保存设置的金额
    local strKey = tostring(PlayerInfo.getInstance():getUserID()).."SetTansferGold"
    cc.UserDefault:getInstance():setStringForKey(strKey, strValues)
end

return BankTransferView
