--region BankTransferLogView.lua
--Date 2017.04.29.
--Auther JackyXu.
--Desc: 银行转帐记录 dialog

local BankTransferLogTimeTableView = require("hall.bean.BankTransferLogTimeTableView")
local CBankDetails = require "common.manager.CBankDetails"

local BankTransferLogView = class("BankTransferLogView", cc.exports.FixLayer)
local HallSceneRes  = require("hall.scene.HallSceneRes")

BankTransferLogView.instance_ = nil
function BankTransferLogView.create()
    BankTransferLogView.instance_ = BankTransferLogView.new():init()
    return BankTransferLogView.instance_
end

function BankTransferLogView:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()

    self.m_pNodeTable = nil
    self.m_pBtnClose = nil
    -- self.m_lb_transfer_count = nil
    self.m_pNodeTimeTable = nil
    self.m_pLbSelectTime = nil
    self.m_pSpSelcetTime = {}
    self.m_pBtnSelcetType = {}
    self.m_bIsCanReq = true
    self.m_fOffsetY = 0
    self.m_nDays = 0
    self.m_nType = 0
    self.m_nOldMsgID = 0
    self.m_bIsReloaded = false
    self.m_nAllMsgNo = -1
    self.m_nLastCellNum = 0

    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    -- 拦截触摸事件向下传递
    local touchListener = cc.EventListenerTouchOneByOne:create()
    touchListener:setSwallowTouches(true)
    touchListener:registerScriptHandler(function(touch, event)
        -- body
        event:stopPropagation()
        return true
    end, cc.Handler.EVENT_TOUCH_BEGAN)
    local eventDispatcher = self.m_rootUI:getEventDispatcher()
    eventDispatcher:addEventListenerWithSceneGraphPriority(touchListener, self.m_rootUI)
end

function BankTransferLogView:init()
    
    --init csb
    --self:setTargetShowHideStyle(self, self.SHOW_POPUP, self.HIDE_POPOUT)
        local strPath = "hall/csb/TransferLogDlg.csb"
    if CommonUtils.getInstance():isPCMode() then
        strPath = "hall/csb/pc-TransferLogDlg.csb"
    end
    self.m_pathUI = cc.CSLoader:createNode(strPath)
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("TransferLogDlg")
    local diffX = 145-(1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pBtnNull  = self.m_pathUI:getChildByName("Panel_2") --空白处关闭

    self.m_pImgBg           = self.m_pNodeRoot:getChildByName("IMG_bg")
    self.m_pBtnClose        = self.m_pImgBg:getChildByName("BTN_close")
    self.m_pNode1           = self.m_pImgBg:getChildByName("Node_1")
    self.m_pNode2           = self.m_pImgBg:getChildByName("Node_2")
    self.m_pNodeTable       = self.m_pImgBg:getChildByName("node_table")
    self.m_pNodeTimeTable   = self.m_pImgBg:getChildByName("node_timeTable")
    --node1
    self.m_pBtnSelcetType[1]= self.m_pNode1:getChildByName("BTN_all")
    self.m_pBtnSelcetType[2]= self.m_pNode1:getChildByName("BTN_out")
    self.m_pBtnSelcetType[3]= self.m_pNode1:getChildByName("BTN_in")
    --node2
    self.m_pBtnSelectTime   = self.m_pNode2:getChildByName("BTN_selectTime")
    self.m_pLbSelectTime    = self.m_pNode2:getChildByName("LB_date")
    self.m_pSpSelcetTime[1] = self.m_pNode2:getChildByName("IMG_up")
    self.m_pSpSelcetTime[2] = self.m_pNode2:getChildByName("IMG_down")

    self.m_pBtnTimeClose    = self.m_pImgBg:getChildByName("Button_5")
    self.m_pImgTableTitle   = self.m_pImgBg:getChildByName("IMG_titleBg")

    --pc
    if CommonUtils.getInstance():isPCMode() then 
        self.m_pNode3 = self.m_pImgBg:getChildByName("Node_3")
        self.m_pLbDetails = {}
        for i=1,4 do
            self.m_pLbDetails[i] = self.m_pNode3:getChildByName("Text_"..i)
            self.m_pLbDetails[i]:setString("")
        end
        if PlayerInfo.getInstance():isProxy() then 
            self.m_pImgTableTitle:loadTexture("hall/plist/bank/pc-gui-bank-image-transfer-proxy.png", ccui.TextureResType.plistType)
            self.m_pImgTableTitle:setPositionY(460)
            self.m_pNodeTable:setContentSize(cc.size(1515,380))
        else
            self.m_pNode3:setVisible(false)
        end
    end

    -- 按纽响应
    self.m_pBtnClose:addClickEventListener(handler(self, self.onReturnClicked))
    self.m_pBtnNull:addClickEventListener(handler(self, self.onReturnClicked))
    self.m_pBtnSelectTime:setZoomScale(0) -- 禁用缩放
    self.m_pBtnSelectTime:addClickEventListener(handler(self, self.onSelectTimeClicked))
    for i=1,3 do
        self.m_pBtnSelcetType[i]:setTag(i-1)
        self.m_pBtnSelcetType[i]:addClickEventListener(handler(self, self.onSelectTypeBtnClick))
    end
    self.m_pBtnTimeClose:addClickEventListener(handler(self, self.onCloseSelectTimeView))
    self.m_pBtnTimeClose:setVisible(false)

    self:updateButtonStatus(1)

    return self
end

function BankTransferLogView:onEnter()
    self.super:onEnter()

    --打开动画
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_BIG, FixLayer.HIDE_DLG_BIG)
    self:showWithStyle()

    self.m_selectTime_listener = SLFacade:addCustomEventListener(Hall_Events.BANK_TRANSFER_SELECT_DAYS, handler(self, self.Handle_Customer_Event))
    self.m_closeSelectTime_listener = SLFacade:addCustomEventListener(Hall_Events.CLOSE_TRANSFER_SELECT_DAYS, handler(self, self.Handle_Customer_Event))
    self.bank_detail_listener = SLFacade:addCustomEventListener(Hall_Events.MSG_BANK_DETAILS, handler(self, self.updateMsgView))    
    self.bank_refund_listener = SLFacade:addCustomEventListener(Hall_Events.MSG_REFUND_DETAILS, handler(self, self.onMsgRefundDetailsInfo), self.__cname)               

    self.m_nType = 0
    
    local shap = cc.DrawNode:create()
    local pointArr = {cc.p(0,0), cc.p(0+201, 0), cc.p(0+201, 0+268), cc.p(0, 0+268)}
    shap:drawPolygon(pointArr, 4, cc.c4f(255, 255, 255, 255), 2, cc.c4f(255, 255, 255, 255))
    self.m_pClippingMenu = cc.ClippingNode:create(shap)
    self.m_pClippingMenu:setAnchorPoint(cc.p(0,0))
    self.m_pClippingMenu:setPosition(cc.p(0,0))
    self.m_pNodeTimeTable:addChild(self.m_pClippingMenu) -- 122 155 305 335
    
     local serverTime = PlayerInfo.getInstance():getServerTime()
    local t = CommonUtils.getInstance():LocalTime(serverTime)
    local strDate = string.format(LuaUtils.getLocalString("STRING_201"),t.tm_mon,t.tm_mday)
    self.m_pLbSelectTime:setString(strDate)
    
    CBankDetails.getInstance():Clear()
    self:updateMinMsgIdx()
    self:initTableView()
    
    self.m_pSpSelcetTime[2]:setVisible(true)
    self.m_pSpSelcetTime[1]:setVisible(false)

    self.m_bIsCanReq = false
    --默认请求当天转账记录
    cc.exports.Veil:getInstance():ShowVeil(VEIL_WAIT)
    self:sendGetTransferLog(0,0,0)
    --pc代理请求转入转出总数
    if CommonUtils.getInstance():isPCMode() and PlayerInfo.getInstance():isProxy() then
        CMsgHall:sendRefundRecord(0)
    end
end

function BankTransferLogView:onExit()
    self.super:onExit()

    cc.exports.Veil:getInstance():HideVeil(VEIL_WAIT)
    SLFacade:removeEventListener(self.bank_detail_listener)
    SLFacade:removeEventListener(self.m_selectTime_listener)
    SLFacade:removeEventListener(self.m_closeSelectTime_listener)
    SLFacade:removeEventListener(self.bank_refund_listener)

    BankTransferLogView.instance_ = nil
end

function BankTransferLogView:cellSizeForTable(table, idx)
    return 1100, 62
end

function BankTransferLogView:tableCellAtIndex(table, idx)
    local cell = table:dequeueCell()--cellAtIndex(idx)
    if not cell then
        cell = cc.TableViewCell:new()
    else
        cell:removeAllChildren()
    end

    self:initTableViewCell(cell, idx)

    return cell
end

function BankTransferLogView:updateMinMsgIdx()
    self.m_nAllMsgNo = CBankDetails.getInstance():getBankTransferCount()
end


function BankTransferLogView:numberOfCellsInTableView(table)
	local sumNum = CBankDetails.getInstance():getBankTransferCount()
    if sumNum ~= self.m_nLastCellNum then 
        self.m_nLastCellNum = sumNum
        if self.m_pTableView then 
            self.m_pTableView:reloadData()
        end 
    end 
    return sumNum
end

function BankTransferLogView:tableCellTouched(table, cell)
end

function BankTransferLogView:initTableViewCell(cell, nIdx)

    local info = CBankDetails.getInstance():getBankTransferInfoAtIndex(nIdx + 1)
    if not info or next(info) == nil then return end
    
    --时间
    local strTime = string.format("%d/%d/%d %02d:%02d:%02d", info.time.wYear, info.time.wMonth, info.time.wDay,info.time.wHour, info.time.wMinute, info.time.wSecond)
    local strSourceNick = tostring(info.szSourceNickName)
    local strSourceID =string.format("(ID:%d)",info.dwSourceGameID)
    local strGold = LuaUtils.getFormatGoldAndNumber(info.lSwapScore)
    local strTargetNick = tostring(info.szTargetNickName)
    local strTargetID = string.format("(ID:%d)",info.dwTargetGameID)
    local sourceID = info.dwSourceGameID
    local targetID = info.dwTargetGameID

    local strPath = "hall/csb/TransferLogItem.csb"
    if CommonUtils.getInstance():isPCMode() then
        if PlayerInfo.getInstance():isProxy() then
            strPath = "hall/csb/pc-TransferLogProxyItem.csb"
        else
            strPath = "hall/csb/pc-TransferLogItem.csb"
        end
    end
    local item = cc.CSLoader:createNode(strPath)
    local node = item:getChildByName("Panel_1")
    item.labelDate   = node:getChildByName("Text_1")
    item.labelSourceNick  = node:getChildByName("Text_2")
    item.labelSourceID  = node:getChildByName("Text_3")
    item.labelGold = node:getChildByName("Text_4")
    item.labelTargetNick  = node:getChildByName("Text_5")
    item.labelTargetID  = node:getChildByName("Text_6")

    item.labelDate:setString(strTime)
    item.labelSourceNick:setString(strSourceNick)
    item.labelSourceID:setString(strSourceID)
    item.labelGold:setString(strGold)
    item.labelTargetNick:setString(strTargetNick)
    item.labelTargetID:setString(strTargetID)

    --pc
    if CommonUtils.getInstance():isPCMode()then 
        item.btnCopy1  = node:getChildByName("Button_1")
        item.btnCopy2  = node:getChildByName("Button_2")
        --复制按钮1
        local w1 = item.labelSourceNick:getContentSize().width
        local w2 = item.labelSourceID:getContentSize().width
        local w = w1 > w2 and w1 or w2
        item.btnCopy1:addClickEventListener(function(sender)
            FloatMessage.getInstance():pushMessage(string.format("已复制ID『%s』",tostring(sourceID)))
            LuaNativeBridge.getInstance():setCopyContent(tostring(sourceID))
        end)
        local sx1 = PlayerInfo.getInstance():isProxy() and 455 or 505
        item.btnCopy1:setPositionX(sx1+w/2+20)
        --复制按钮2
        w1 = item.labelTargetNick:getContentSize().width
        w2 = item.labelTargetID:getContentSize().width
        w = w1 > w2 and w1 or w2
        item.btnCopy2:addClickEventListener(function(sender)
            FloatMessage.getInstance():pushMessage(string.format("已复制ID『%s』",tostring(targetID)))
            LuaNativeBridge.getInstance():setCopyContent(tostring(targetID))
        end)
        local sx2 = PlayerInfo.getInstance():isProxy() and 1090 or 1235
        item.btnCopy2:setPositionX(sx2+w/2+20)
        --代理
        if PlayerInfo.getInstance():isProxy() then 
            item.labelStatus  = node:getChildByName("Text_7")
            --审核状态
            local trStatus = { "审核中", "审核成功", "审核失败", "撤销" }
            local cStatus = { cc.WHITE, cc.c3b(118,220,68), cc.c3b(254,78,78), cc.c3b(254,78,78) }
            local strStatus = tostring(trStatus[info.byTradeCheckStatus+1])
            item.labelStatus:setString(strStatus)
            item.labelStatus:setColor(cStatus[info.byTradeCheckStatus+1])
        end
    end

    cell:addChild(item)
end

function BankTransferLogView:scrollViewDidScroll(pView)
    if self.m_fOffsetY == pView:getContentOffset().y then
        return
    end

    self.m_fOffsetY = pView:getContentOffset().y
    local count = CBankDetails.getInstance():getBankTransferCount()
    if count == 0 then return end
    local ry = 60 
    if device.platform == "windows" then
        ry = 0
    end
    if self.m_fOffsetY >= ry and self.m_bIsCanReq and count < 100 then
        local requestID = CBankDetails.getInstance():getBankTransferInfoAtIndex(count).iRecordID
        if (self.m_nOldMsgID == 0 or (requestID < self.m_nOldMsgID and count < CBankDetails.getInstance():getBankDetailsTransfer())) then
            self.m_nOldMsgID = requestID
            self.m_bIsCanReq = false
            self:updateMinMsgIdx()

            self:sendGetTransferLog(requestID,self.m_nType,self.m_nDays)
        end
    end
end

function BankTransferLogView:initTableView()
    if not self.m_pTableView then
        self.m_pTableView = cc.TableView:create(cc.size(self.m_pNodeTable:getContentSize().width, self.m_pNodeTable:getContentSize().height))
        self.m_pTableView:setIgnoreAnchorPointForPosition(false)
        self.m_pTableView:setAnchorPoint(cc.p(0,0))
        self.m_pTableView:setPosition(cc.p(0, 0))
        self.m_pTableView:setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN)
        self.m_pTableView:setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL)
        self.m_pTableView:setDelegate()
        self.m_pTableView:registerScriptHandler(handler(self,self.scrollViewDidScroll), CCTableView.kTableViewScroll)
        self.m_pTableView:registerScriptHandler(handler(self,self.cellSizeForTable), CCTableView.kTableCellSizeForIndex)
        self.m_pTableView:registerScriptHandler(handler(self,self.tableCellAtIndex), CCTableView.kTableCellSizeAtIndex)
        self.m_pTableView:registerScriptHandler(handler(self,self.numberOfCellsInTableView), CCTableView.kNumberOfCellsInTableView)
        self.m_pTableView:registerScriptHandler(handler(self,self.tableCellTouched), CCTableView.kTableCellTouched)
        self.m_pNodeTable:addChild(self.m_pTableView)
        if CommonUtils.getInstance():isPCMode() then 
            if PlayerInfo.getInstance():isProxy() then 
                self.m_pTableView:createScrollBar(HallSceneRes.logSliderTrack3, HallSceneRes.sliderThumb)
            else
                self.m_pTableView:createScrollBar(HallSceneRes.logSliderTrack2, HallSceneRes.sliderThumb)
            end
            self.m_pTableView:setScrollBarVisible(false)
        end
    else
        self.m_pTableView:reloadData()
    end
end

----------------------
-- 消息响应
-- 自定义消息
function BankTransferLogView:Handle_Customer_Event(_event)
    local _userdata = unpack(_event._userdata)
    if not _userdata then
        return
    end

    local eventID = _userdata.name
    local msg = _userdata.packet

    if eventID == Hall_Events.BANK_TRANSFER_SELECT_DAYS then
        self:onSelectTimeCallBack(msg)
        -- 下一帧再关闭选择页面
        self:runAction(cc.Sequence:create(
                            cc.DelayTime:create(0.1),
                            cc.CallFunc:create( function()
                                self:onCloseSelectTimeView()
                            end)
                        )
                    )
    elseif eventID == Hall_Events.CLOSE_TRANSFER_SELECT_DAYS then
        self:onCloseSelectTimeView()
    end
end

-- 选择天
function BankTransferLogView:onSelectTimeCallBack(pMsg)
    if not pMsg then
        return
    end

    local days = tonumber(pMsg.selectIndex)
    if days == self.m_nDays then
        return
    end
    self.m_nDays = -days
    
    local serverTime = PlayerInfo.getInstance():getServerTime() - days*86400
    local t = CommonUtils.getInstance():LocalTime(serverTime)
    local strDate = string.format(LuaUtils.getLocalString("STRING_201"),t.tm_mon,t.tm_mday)
    self.m_pLbSelectTime:setString(strDate)
    
    self.m_nOldMsgID = 0
    CBankDetails.getInstance():CleanTransferLog()
    
    self.m_bIsCanReq = false

    cc.exports.Veil:getInstance():ShowVeil(VEIL_WAIT)
    self:sendGetTransferLog(0,self.m_nType,self.m_nDays)
end

function BankTransferLogView:onCloseSelectTimeView()
    -- body
    if self.m_pLogTimeTable then
        self.m_pLogTimeTable:removeFromParent()
        self.m_pLogTimeTable = nil
        self.m_pSpSelcetTime[2]:setVisible(true)
        self.m_pSpSelcetTime[1]:setVisible(false)
        self.m_pBtnTimeClose:setVisible(false)
    end
    --pc
    if CommonUtils.getInstance():isPCMode() then 
        local count = CBankDetails.getInstance():getBankTransferCount()
        local min = PlayerInfo.getInstance():isProxy() and 6 or 7
        self.m_pTableView:setMouseScrollEnabled(count>min)
    end
end

function BankTransferLogView:updateMsgView(pUserdata)

    if self.m_pTableView ~= nil and not self.m_bIsReloaded then
        self.m_bIsReloaded = true
        self:runAction(cc.Sequence:create(
                            cc.DelayTime:create(1),
                            cc.CallFunc:create( function()
                                self.m_bIsReloaded = false
                                self:reloadTable()
                                cc.exports.Veil:getInstance():HideVeil(VEIL_WAIT)
                            end)
                        ))
    end
end

function BankTransferLogView:onMsgRefundDetailsInfo(pUserdata)
    if not CommonUtils.getInstance():isPCMode() then
        return
    end
    local score = { CBankDetails.getInstance():getProxyScoreIn(), CBankDetails.getInstance():getProxyScoreOut(),
                   CBankDetails.getInstance():getUserScoreIn(), CBankDetails.getInstance():getUserScoreOut() }
    for i=1,4 do
        self.m_pLbDetails[i]:setString(LuaUtils.getFormatGoldAndNumber(score[i]))
    end
end

function BankTransferLogView:reloadTable(dt)
    self.m_bIsCanReq = true
    if self.m_pTableView then
        self.m_pTableView:reloadData()
    end
   if CommonUtils.getInstance():isPCMode() then 
        local count = CBankDetails.getInstance():getBankTransferCount()
        local min = PlayerInfo.getInstance():isProxy() and  6 or 7
        self.m_pTableView:setScrollBarVisible(count>min)
        if not self.m_pLogTimeTable then
            self.m_pTableView:setMouseScrollEnabled(count>min)
        end
    end
end

function BankTransferLogView:onSelectTypeBtnClick(pSender)

    local nTag = pSender:getTag()
    if nTag == self.m_nType then -- 已选中的按纽不能再点击
        return
    end

    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    local self = BankTransferLogView.instance_
    -- 按钮点击延迟 防止消息发送频繁断开网络
    if not self.m_bIsCanReq then
        return
    end
    
    self:updateButtonStatus(nTag+1)
    self.m_nType = nTag
    self.m_nOldMsgID = 0
    CBankDetails.getInstance():CleanTransferLog() -- just for test.

    self.m_bIsCanReq = false
    cc.exports.Veil:getInstance():ShowVeil(VEIL_WAIT)
    self:sendGetTransferLog(0,self.m_nType,self.m_nDays)
end

function BankTransferLogView:updateButtonStatus(nIndex)

    for i=1,3 do
        if i == nIndex then
            self.m_pBtnSelcetType[i]:setEnabled(false)
        else
            self.m_pBtnSelcetType[i]:setEnabled(true)
        end
    end
end

----------------------
function BankTransferLogView:sendGetTransferLog(requestId, nType, nDay)

    if device.platform == "windows" and PlayerInfo.getInstance():isProxy() then
        --代理获取有审核状态的转账记录
        CMsgHall:sendGetTransferLogEx(requestId,nType,nDay)
    else
        CMsgHall:sendGetTransferLog(requestId,nType,nDay)
    end
end
----------------------
-- 页面按纽响应
-- 时间选择
function BankTransferLogView:onSelectTimeClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    -- body
    if self.m_pLogTimeTable then
        self:onCloseSelectTimeView()
    else
        if device.platform == "windows" then
            self.m_pTableView:setMouseScrollEnabled(false)
        end
        self.m_pLogTimeTable = BankTransferLogTimeTableView.create(-self.m_nDays)
        self.m_pLogTimeTable:setAnchorPoint(cc.p(0.5,0.5))
        self.m_pClippingMenu:addChild(self.m_pLogTimeTable)
        
        -- move animation
        self.m_pLogTimeTable:setPosition(cc.p(0,350))
        local move = cc.MoveTo:create(0.15, cc.p(0,0))
        self.m_pLogTimeTable:runAction(move)
    
        self.m_pSpSelcetTime[2]:setVisible(false)
        self.m_pSpSelcetTime[1]:setVisible(true)
        self.m_pBtnTimeClose:setVisible(true)
    end
end

-- 返回
function BankTransferLogView:onReturnClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    self:onMoveExitView()
end

return BankTransferLogView