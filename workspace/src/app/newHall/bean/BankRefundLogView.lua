--region BankRefundLogView.lua
--Date 2017.04.29.
--Auther JackyXu.
--Desc: 银行转帐记录 dialog

local BankTransferLogTimeTableView = require("hall.bean.BankTransferLogTimeTableView")
local CBankDetails = require "common.manager.CBankDetails"
local HallSceneRes  = require("hall.scene.HallSceneRes")

local BankRefundLogView = class("BankRefundLogView", cc.exports.FixLayer)

BankRefundLogView.instance_ = nil
function BankRefundLogView.create()
    BankRefundLogView.instance_ = BankRefundLogView.new():init()
    return BankRefundLogView.instance_
end

function BankRefundLogView:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()

    self.m_pNodeTable = nil
    self.m_pBtnClose = nil

    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)
end

function BankRefundLogView:init()
    
    --init csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/pc-RefundLogDlg.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("RefundLogDlg")
    local diffX = 145-(1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pBtnNull  = self.m_pathUI:getChildByName("Panel_2") --空白处关闭
    self.m_pImgBg           = self.m_pNodeRoot:getChildByName("IMG_bg")
    self.m_pBtnClose        = self.m_pImgBg:getChildByName("BTN_close")
    self.m_pNodeTable       = self.m_pImgBg:getChildByName("node_table")

    self.m_pNode1       = self.m_pImgBg:getChildByName("Node_1")
    --领取详情
    self.m_pLbDetails = {}
    for i=1,4 do
        self.m_pLbDetails[i] = self.m_pNode1:getChildByName("Text_"..i)
        self.m_pLbDetails[i]:setString("")
    end

    -- 按纽响应
    self.m_pBtnClose:addClickEventListener(handler(self, self.onReturnClicked))
    self.m_pBtnNull:addClickEventListener(handler(self, self.onReturnClicked))

    return self
end

function BankRefundLogView:onEnter()
    self.super:onEnter()

    --打开动画
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_BIG, FixLayer.HIDE_DLG_BIG)
    self:showWithStyle()
   
    SLFacade:addCustomEventListener(Hall_Events.MSG_REFUND_DETAILS, handler(self, self.onMsgRefundDetailsInfo), self.__cname)                 

    self.m_nOldMsgID = 0
    CBankDetails.getInstance():setRefundDetail({})
    self:initTableView()

    self.m_bIsCanReq = false
    cc.exports.Veil:getInstance():ShowVeil(VEIL_WAIT)
    CMsgHall:sendRefundRecord(0)
end

function BankRefundLogView:onExit()
    self.super:onExit()

    cc.exports.Veil:getInstance():HideVeil(VEIL_WAIT)
    SLFacade:removeCustomEventListener(Hall_Events.MSG_REFUND_DETAILS, self.__cname)

    BankRefundLogView.instance_ = nil
end

function BankRefundLogView:cellSizeForTable(table, idx)
    return 1100, 56
end

function BankRefundLogView:tableCellAtIndex(table, idx)
    local cell = table:dequeueCell()--cellAtIndex(idx)
    if not cell then
        cell = cc.TableViewCell:new()
    else
        cell:removeAllChildren()
    end

    self:initTableViewCell(cell, idx)

    return cell
end

function BankRefundLogView:updateMinMsgIdx()
    self.m_nAllMsgNo = CBankDetails.getInstance():getRefundDetailCount()
end


function BankRefundLogView:numberOfCellsInTableView(table)
	local sumNum = CBankDetails.getInstance():getRefundDetailCount()
    if sumNum ~= self.m_nLastCellNum then 
        self.m_nLastCellNum = sumNum
        if self.m_pTableView then 
            self.m_pTableView:reloadData()
        end 
    end 
    return sumNum
end

function BankRefundLogView:tableCellTouched(table, cell)
end

function BankRefundLogView:initTableViewCell(cell, nIdx)

    local info = CBankDetails.getInstance():getRefundDetailByIndex(nIdx + 1)
    if not info or next(info) == nil then return end
    
    local strTime = string.format("%d/%d/%d %02d:%02d:%02d", info.time.wYear, info.time.wMonth, info.time.wDay,info.time.wHour, info.time.wMinute, info.time.wSecond)
    local strNick = tostring(info.szSourceNickName)
    local strID = string.format("ID:%d", info.dwSourceGameID)
    local strGold = LuaUtils.getFormatGoldAndNumber(info.lSwapScore)

    local item = cc.CSLoader:createNode("hall/csb/pc-RefundLogItem.csb")
    local node = item:getChildByName("Panel_1")
    item.labelDate   = node:getChildByName("Text_1")
    item.labelNick  = node:getChildByName("Text_2")
    item.labelID  = node:getChildByName("Text_3")
    item.labelGold = node:getChildByName("Text_4")

    item.labelDate:setString(strTime)
    item.labelNick:setString(strNick)
    item.labelID:setString(strID)
    item.labelGold:setString(strGold)

    item:setPositionX(5)
    cell:addChild(item)
end

function BankRefundLogView:scrollViewDidScroll(pView)
    if self.m_fOffsetY == pView:getContentOffset().y then
        return
    end

    self.m_fOffsetY = pView:getContentOffset().y
    local count = CBankDetails.getInstance():getRefundDetailCount()
    if count == 0 then return end
    if self.m_fOffsetY > 60 and self.m_bIsCanReq and count < 100 then
        local requestID = CBankDetails.getInstance():getRefundDetailByIndex(count).iRecordID
        local total = CBankDetails.getInstance():getRefundDetailsTotal()
        if (self.m_nOldMsgID == 0 or (requestID < self.m_nOldMsgID and count < total)) then
            self.m_nOldMsgID = requestID
            self.m_bIsCanReq = false
            self:updateMinMsgIdx()

            CMsgHall:sendRefundRecord(requestID)
        end
    end
end

function BankRefundLogView:initTableView()
    if not self.m_pTableView then
        self.m_pTableView = cc.TableView:create(cc.size(self.m_pNodeTable:getContentSize().width, self.m_pNodeTable:getContentSize().height))
        self.m_pTableView:setIgnoreAnchorPointForPosition(false)
        self.m_pTableView:setAnchorPoint(cc.p(0,0))
        self.m_pTableView:setPosition(cc.p(0, 0))
        self.m_pTableView:setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN)
        self.m_pTableView:setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL)
        self.m_pTableView:setDelegate()
        self.m_pTableView:registerScriptHandler(handler(self,self.scrollViewDidScroll), CCTableView.kTableViewScroll)
        self.m_pTableView:registerScriptHandler(handler(self,self.cellSizeForTable), CCTableView.kTableCellSizeForIndex)
        self.m_pTableView:registerScriptHandler(handler(self,self.tableCellAtIndex), CCTableView.kTableCellSizeAtIndex)
        self.m_pTableView:registerScriptHandler(handler(self,self.numberOfCellsInTableView), CCTableView.kNumberOfCellsInTableView)
        self.m_pTableView:registerScriptHandler(handler(self,self.tableCellTouched), CCTableView.kTableCellTouched)
        self.m_pNodeTable:addChild(self.m_pTableView)
        if CommonUtils.getInstance():isPCMode() then
            self.m_pTableView:createScrollBar(HallSceneRes.logSliderTrack2, HallSceneRes.sliderThumb)
            self.m_pTableView:setScrollBarVisible(false)
        end
    else
        self.m_pTableView:reloadData()
    end
end

----------------------
-- 消息响应
-- 自定义消息
function BankRefundLogView:onMsgRefundDetailsInfo(pUserdata)

    cc.exports.Veil:getInstance():HideVeil(VEIL_WAIT)
    if self.m_pTableView ~= nil and not self.m_bIsReloaded then
        self.m_bIsReloaded = true
        self:runAction(cc.Sequence:create(
                            cc.DelayTime:create(1),
                            cc.CallFunc:create( function()
                                self.m_bIsReloaded = false
                                self:reloadTable()
                                cc.exports.Veil:getInstance():HideVeil(VEIL_WAIT)
                            end)
                        ))
    end

    local score = { CBankDetails.getInstance():getProxyScoreIn(), CBankDetails.getInstance():getProxyScoreOut(),
                   CBankDetails.getInstance():getUserScoreIn(), CBankDetails.getInstance():getUserScoreOut() }
    for i=1,4 do
        self.m_pLbDetails[i]:setString(LuaUtils.getFormatGoldAndNumber(score[i]))
    end
end

function BankRefundLogView:reloadTable(dt)
    self.m_bIsCanReq = true
    if self.m_pTableView then
        self.m_pTableView:reloadData()
    end
    if device.platform == "windows" then
        local count = CBankDetails.getInstance():getRefundDetailCount()
        self.m_pTableView:setScrollBarVisible(count>8)
        self.m_pTableView:setMouseScrollEnabled(count>8)
    end
end

function BankRefundLogView:updateDetails()
    
end

----------------------
-- 页面按纽响应
-- 返回
function BankRefundLogView:onReturnClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    self:onMoveExitView()
end

return BankRefundLogView