--region BankPwdSettingView.lua
--Date 2017.04.26.
--Auther JackyXu.
--Desc: 设置银行密码 view

local BankPwdSettingView = class("BankPwdSettingView",FixLayer)

BankPwdSettingView.instance_ = nil
function BankPwdSettingView.create()
    BankPwdSettingView.instance_ = BankPwdSettingView.new():init()
    return BankPwdSettingView.instance_
end

function BankPwdSettingView:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()
    self.m_pEditBox = {}
    self.m_pNodeEdit = {}
    self.m_bSettingPWD = false

    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)

    self.m_nEditBoxTouchCount = 0 -- 输入框点击次数
end

function BankPwdSettingView:init()
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_NORMAL, FixLayer.HIDE_DLG_NORMAL)

    --init csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/BankPswSettingDlg.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("BankPswSettingDlg")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pImgBg      = self.m_pNodeRoot:getChildByName("IMG_bg")
    self.m_pTitle      = self.m_pImgBg:getChildByName("IMG_title")
    self.m_pBtnClose   = self.m_pImgBg:getChildByName("BTN_close")
    self.m_pBtnSure    = self.m_pImgBg:getChildByName("BTN_sure")
    self.m_pBtnNull  = self.m_pathUI:getChildByName("Panel_2") --空白处关闭
    self.m_pBtnClose:addClickEventListener(handler(self, self.onReturnClicked))
    self.m_pBtnNull:addClickEventListener(handler(self, self.onReturnClicked))
    self.m_pBtnSure:addClickEventListener(handler(self, self.onConfirmClicked))

    for i = 1,2 do 
        self.m_pNodeEdit[i] = self.m_pImgBg:getChildByName("node_input"..i)
    end

    --常规版：银行/和谐版：保险箱------------------------------
    if ClientConfig.getInstance():getIsOtherChannel() then
        self.m_pTitle:loadTexture("hall/plist/bank/gui-bank-text-title-bank-small-2.png", ccui.TextureResType.plistType)
        self.m_pTitle:ignoreContentAdaptWithSize(true)

        self.m_pLB_tips_1 = self.m_pImgBg:getChildByName("LB_tips_1")
        self.m_pLB_tips_1:setString("温馨提示：1.使用保险箱前请先设置保险箱密码")

        self.m_pLB_tips_2 = self.m_pImgBg:getChildByName("LB_tips_2")
        self.m_pLB_tips_2:setString("2.保险箱密码不能与登录密码一致")
    end
    --------------------------------------------------------
    
    self:initEditBox()
    return self
end

function BankPwdSettingView:onEnter()
    self.super:onEnter()
    self:showWithStyle()
    if device.platform == "windows" then
        self:initKeyboard()  
    end
    self.change_pwd_listener = SLFacade:addCustomEventListener(Hall_Events.MSG_CHANGE_BANK_PWD, handler(self, self.Handle_Custom_Ack))
end

function BankPwdSettingView:onExit()
    self.super:onExit()
    if device.platform == "windows" then
        self:cleanKeyboard()
    end
    SLFacade:removeEventListener(self.change_pwd_listener)

    BankPwdSettingView.instance_ = nil
end

--监听按键
function BankPwdSettingView:initKeyboard() 
    local function onKeyReleased(keyCode, event)
        event:stopPropagation()
        if keyCode == cc.KeyCode.KEY_ENTER or keyCode == cc.KeyCode.KEY_KP_ENTER then 
            self:onConfirmClicked()
        elseif keyCode == cc.KeyCode.KEY_TAB then
            self:tabToNext()
        end
    end

    local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
    self.m_pKeyboardListener = cc.EventListenerKeyboard:create()
    self.m_pKeyboardListener:registerScriptHandler(onKeyReleased, cc.Handler.EVENT_KEYBOARD_RELEASED)
    pEventDispatcher:addEventListenerWithFixedPriority(self.m_pKeyboardListener, -129)
end

--移除按键监听
function BankPwdSettingView:cleanKeyboard()

    if self.m_pKeyboardListener then
        local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
        pEventDispatcher:removeEventListener(self.m_pKeyboardListener)
        self.m_pKeyboardListener = nil
    end
end

function BankPwdSettingView:tabToNext()
    
    local index = self.m_nEditBoxTouchCount+1 > 2 and 1 or self.m_nEditBoxTouchCount+1
    if self.m_nEditBoxTouchCount > 0 then 
        self.m_pEditBox[self.m_nEditBoxTouchCount]:touchDownAction(self.m_pEditBox[self.m_nEditBoxTouchCount],ccui.TouchEventType.canceled);
    end
    self.m_pEditBox[index]:touchDownAction(self.m_pEditBox[index],ccui.TouchEventType.ended);
    self.m_nEditBoxTouchCount = index
end
-----------------

function BankPwdSettingView:Handle_Custom_Ack()
    local strpwd = self.m_pEditBox[1]:getText()
    PlayerInfo.getInstance():setInsurePass(strpwd)
    PlayerInfo.getInstance():setIsInsurePass(1)
    SLFacade:dispatchCustomEvent(Hall_Events.MSG_BANK_REFRESH)
end

function BankPwdSettingView:initEditBox()
    local str = {LuaUtils.getLocalString("BANK_1"), LuaUtils.getLocalString("BANK_2")}
    local lenght = {20, 20}
    for i=1,2 do
        if not self.m_pEditBox[i] then
            self.m_pEditBox[i] = self:createEditBox(lenght[i],
                                        cc.KEYBOARD_RETURNTYPE_DONE,
                                        cc.EDITBOX_INPUT_MODE_SINGLELINE,
                                        cc.EDITBOX_INPUT_FLAG_PASSWORD,
                                        i,
                                        cc.size(320,50),
                                        str[i])
            self.m_pEditBox[i]:setText("")
            self.m_pEditBox[i]:setPosition(0, 5)
            self.m_pNodeEdit[i]:addChild(self.m_pEditBox[i])
        end
    end
end

function BankPwdSettingView:createEditBox(maxLength, keyboardReturnType, inputMode, inputFlag, tag, size, placestr)
    local sprite1 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite2 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite3 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    size = cc.size(size.width, 40)
    local editBox = cc.EditBox:create(size, sprite1, sprite2, sprite3)
    editBox:setMaxLength(maxLength)
    editBox:setReturnType(keyboardReturnType)
    editBox:setInputMode(inputMode)
    editBox:setInputFlag(inputFlag)
    editBox:setTag(tag)
    editBox:setFont("", 28)
    editBox:setFontColor(G_CONSTANTS.INPUT_COLOR)
    editBox:setPlaceHolder(placestr)
    editBox:setPlaceholderFontSize(28)
    editBox:setPlaceholderFontColor(G_CONSTANTS.PLACE_COLOR)
    editBox:setAnchorPoint(cc.p(0, 0))
    editBox:registerScriptEditBoxHandler(handler(self, self.onEditBoxEventHandle))
    
--    -- 模拟点击输入框解决 android 手机首次输入看不到输入内容BUG。
--    if device.platform == "android" then
--        self.m_nEditBoxTouchCount = 0
--        editBox:touchDownAction(editBox,ccui.TouchEventType.ended);
--        editBox:touchDownAction(editBox,ccui.TouchEventType.canceled);
--    else
--        self.m_nEditBoxTouchCount = 3
--    end

    return editBox
end

function BankPwdSettingView:onEditBoxEventHandle(event, pSender)
    if "began" == event then
       AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
       self.m_pBtnNull:stopAllActions()
       self.m_pBtnNull:setEnabled(false)
       self.m_nEditBoxTouchCount = pSender:getTag()
    elseif "ended" == event then
    elseif "return" == event then
        --延迟设为可点击 防止还是响应到关闭界面
        local callback = cc.CallFunc:create(function ()
            self.m_pBtnNull:setEnabled(true)
        end)
        local seq = cc.Sequence:create(cc.DelayTime:create(0.16), callback)
        self.m_pBtnNull:runAction(seq)
    elseif "changed" == event then

    elseif "next" == event then
        self:tabToNext()
    end
end

-- 返回
function BankPwdSettingView:onReturnClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    
    self:onMoveExitView()
end

-- 确定
function BankPwdSettingView:onConfirmClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    -- body
    local str1 = self.m_pEditBox[1]:getText()
    local str2 = self.m_pEditBox[2]:getText()
    
    --密码不能为空
    if str1 == "" or str2 == "" then
        FloatMessage.getInstance():pushMessageWarning("BANK_36")
        return
    end
    
    --密码由6-20位字母+数字组成
    if string.len(str1) < 6 or string.len(str1) > 20 or string.len(str2) < 6 or string.len(str2) > 20 then 
        FloatMessage.getInstance():pushMessageWarning("BANK_28")
        return
    end
    
    --两次输入的密码不一致
    if str2 ~= str1 then
        FloatMessage.getInstance():pushMessageWarning("BANK_7")
        return
    end
    
   --密码必须包含字母和数字
   if not LuaUtils.check_paypassword(str1) or not  LuaUtils.check_paypassword(str2) then
       FloatMessage.getInstance():pushMessageWarning("REGISTER_16")
       return
   end
    
    local strNewPwdMD5 = SLUtils:MD5(str2)
    CMsgHall:sendModifyBankPwd(strNewPwdMD5)
    self.m_bSettingPWD = true
end

return BankPwdSettingView
