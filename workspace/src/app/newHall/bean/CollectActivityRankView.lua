--region CollectActivityRankView.lua
--Date 2019.04.02.
--Auther JackyMa.

local CCollectMgr    = require("common.manager.CCollectMgr")

local CollectActivityRankView = class("CollectActivityRankView", cc.Layer)

function CollectActivityRankView:ctor(idx)
    self:enableNodeEvents()

    self.m_pTableView = nil
end

function CollectActivityRankView:onEnter()
    self:init()
    --请求活动排行榜数据
    CMsgHall:sendGetProsperousLeaderboard()
    Veil:getInstance():ShowVeil(VEIL_WAIT)
end

function CollectActivityRankView:onExit()
    self:clean()
end

function CollectActivityRankView:init()
    self:initCSB()
    self:initEvent()
end

function CollectActivityRankView:clean()
    self:cleanEvent()
end

function CollectActivityRankView:initCSB()
    -- root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    -- csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/CollectActivityRank.csb")
    self.m_pathUI:addTo(self.m_rootUI)

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("CollectActivityRank")
    self.m_pImgBg       = self.m_pNodeRoot:getChildByName("Image_bg")
    self.m_pImgNoRank   = self.m_pImgBg:getChildByName("Image_2") --暂无记录

    self.m_pNodeRank    = self.m_pImgBg:getChildByName("Node_rank")
    self.m_pSpTitile    = self.m_pNodeRank:getChildByName("Sprite_1")
    self.m_pLbPeriod    = self.m_pSpTitile:getChildByName("Bitmap_1")
    self.m_pLbFuNum     = self.m_pSpTitile:getChildByName("Bitmap_2")
    self.m_pLbOpenTime  = self.m_pSpTitile:getChildByName("Text_1")
    self.m_pPanelTable  = self.m_pNodeRank:getChildByName("Panel_table")

    --init node 
    self.m_pNodeRank:setVisible(false)
    self.m_pImgNoRank:setVisible(false)
end

function CollectActivityRankView:initEvent()
    SLFacade:addCustomEventListener(Hall_Events.MSG_GET_PROSPEROUS_RANK_BACK, handler(self, self.onMsgRankInfo), self.__cname)
end

function CollectActivityRankView:cleanEvent()
    SLFacade:removeCustomEventListener(Hall_Events.MSG_GET_PROSPEROUS_RANK_BACK, self.__cname)
end

function CollectActivityRankView:initTableView()
    if not self.m_pTableView then
        self.m_pTableView = cc.TableView:create(self.m_pPanelTable:getContentSize())
        self.m_pTableView:setIgnoreAnchorPointForPosition(false)
        self.m_pTableView:setAnchorPoint(cc.p(0,0))
        self.m_pTableView:setPosition(cc.p(0, 0))
        self.m_pTableView:setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN)
        self.m_pTableView:setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL)
        self.m_pTableView:setDelegate()
        self.m_pTableView:registerScriptHandler(handler(self,self.cellSizeForTable), CCTableView.kTableCellSizeForIndex)
        self.m_pTableView:registerScriptHandler(handler(self,self.tableCellAtIndex), CCTableView.kTableCellSizeAtIndex)
        self.m_pTableView:registerScriptHandler(handler(self,self.numberOfCellsInTableView), CCTableView.kNumberOfCellsInTableView)
        self.m_pTableView:registerScriptHandler(handler(self,self.tableCellTouched), CCTableView.kTableCellTouched)
        self.m_pPanelTable:addChild(self.m_pTableView)
    end
    self.m_pTableView:reloadData()
end

function CollectActivityRankView:cellSizeForTable(table, idx)
    return 765, 59
end

function CollectActivityRankView:tableCellAtIndex(table, idx)
    local cell = table:cellAtIndex(idx)
    if not cell then
        cell = cc.TableViewCell:new()
    else
        cell:removeAllChildren()
    end

    self:initTableViewCell(cell, idx)

    return cell
end

function CollectActivityRankView:numberOfCellsInTableView(table_)
    return CCollectMgr.getInstance():getActivityRankNum()
end

function CollectActivityRankView:tableCellTouched(table, cell)
    
end

function CollectActivityRankView:initTableViewCell(cell, nIdx)
    local node = self:getRankNode(nIdx+1)
    cell:addChild(node)
end

-----------------------------------------------------------

function CollectActivityRankView:getRankNode(nIndex)
    local rankNode = ccui.Layout:create()
    rankNode:setContentSize(cc.size(765, 59))

    --item
    local item = cc.CSLoader:createNode("hall/csb/CollectRankItem.csb")
    item:addTo(rankNode)
    item:setPositionX(14)

    --node
    local image_bg = item:getChildByName("image_bg")
    local label_rank = image_bg:getChildByName("label_rank")    --排名
    local label_rank2 = image_bg:getChildByName("label_rank_2")
    local label_name = image_bg:getChildByName("label_name")    --玩家昵称
    local label_score = image_bg:getChildByName("label_value")  --获得金币

    --数据
    local data  = CCollectMgr.getInstance():getActivityRankAtIndex(nIndex)
    if data.dwUserID == PlayerInfo.getInstance():getUserID() then 
        image_bg:loadTexture("hall/plist/collect/collect-rank-bg-2.png",ccui.TextureResType.plistType)
    end
    if nIndex <= 3 then 
        label_rank:setString(tostring(nIndex))
        label_rank2:setVisible(false)
    else
        label_rank:setVisible(false)
        label_rank2:setString(tostring(nIndex))
    end
    label_name:setString(data.szNickName)
    label_score:setString(LuaUtils.getFormatGoldAndNumber(data.llScore))

    return rankNode
end

--------------------------------------------------------------------------------------------
--收到数据
function CollectActivityRankView:onMsgRankInfo()
    Veil:getInstance():HideVeil()
    local num = CCollectMgr.getInstance():getActivityRankNum() 
    if num > 0 then 
        self.m_pNodeRank:setVisible(true)
        local nPeriod = CCollectMgr.getInstance():getRankPeriodNo()
        self.m_pLbPeriod:setString(tostring(nPeriod))
        local nFuNum = CCollectMgr.getInstance():getRankAllAcheieveCount()
        self.m_pLbFuNum:setString(tostring(nFuNum))
        local sysOpenTime = CCollectMgr.getInstance():getRankOpenTime()
        local strOpneTime = string.format("开奖时间：%d-%02d-%02d %02d:%02d", sysOpenTime.wYear, sysOpenTime.wMonth, sysOpenTime.wDay, sysOpenTime.wHour, sysOpenTime.wMinute)
        self.m_pLbOpenTime:setString(strOpneTime)

        self:initTableView()
    else
        self.m_pImgNoRank:setVisible(true)
    end
end
-------------------------------

return CollectActivityRankView
