
local YueCardSignSuccess = class("YueCardSignSuccess",FixLayer)

function YueCardSignSuccess:ctor()
	self.super:ctor(self)
	self:enableNodeEvents()
	self:init()
	self:initBtnEvent()
end

function YueCardSignSuccess:init()
	display.loadSpriteFrames("hall/plist/gui-card.plist", "hall/plist/gui-card.png")

	self.m_pathUI = cc.CSLoader:createNode("hall/csb/HallYueCardSignSuccess.csb")
    self:addChild(self.m_pathUI)

    local diffY = (display.height - 750)/2
    self.m_pathUI:setPositionY(diffY)

    self.m_pNodeRoot = self.m_pathUI:getChildByName("Image_bg")
    self.m_pNodeRoot:setPositionX(display.width/2)

    self.m_pPanelClose = self.m_pathUI:getChildByName("Panel_1")
    self.m_pBtnClose = self.m_pNodeRoot:getChildByName("Button_close")

    self.m_pVipAdd = self.m_pNodeRoot:getChildByName("txt_add")
end

function YueCardSignSuccess:initBtnEvent()
	self.m_pBtnClose:addClickEventListener(handler(self,self.onCloseClicked))
	self.m_pPanelClose:addClickEventListener(handler(self,self.onCloseClicked))
end

function YueCardSignSuccess:onEnter()
	self.super:onEnter()
	self:setTargetShowHideStyle(self,FixLayer.SHOW_DLG_BIG,FixLayer.HIDE_DLG_BIG)
	self:showWithStyle()
	self.m_pBgLayer:hide() --不显示背板
end

function YueCardSignSuccess:onExit()
	self.super:onExit()
end

function YueCardSignSuccess:onCloseClicked()
	SLFacade:dispatchCustomEvent(Hall_Events.DELETE_SIGN_ANIMATION)
	AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
	self:onMoveExitView()
end

function YueCardSignSuccess:setGoldData(gold_txt,add_txt)
	gold_txt = tonumber(gold_txt) or 0
	add_txt = tonumber(add_txt) or 0

	if self.m_pAddRichText then
		self.m_pAddRichText:removeFromParent()
		self.m_pAddRichText = nil
	end

	self.m_pAddRichText = ccui.RichText:create()
    local color = cc.WHITE
    local opacity = 255

    local temp_sprite = cc.Sprite:createWithSpriteFrameName("hall/plist/yueka/gui-card-relief-gold-icon.png")
    local sp = ccui.RichElementCustomNode:create(0, color, opacity, temp_sprite)
    self.m_pAddRichText:pushBackElement(sp)

    local gold_label = cc.Label:createWithBMFont("hall/font/sign_ok_gold.fnt", string.format("%d",gold_txt))
    local txt1 = ccui.RichElementCustomNode:create(0, color, opacity, gold_label)
    self.m_pAddRichText:pushBackElement(txt1)


    if add_txt >= 0 then
	    local add_label = cc.Label:createWithBMFont("hall/font/sign_ok_add_gold.fnt", string.format("+%d",add_txt))
	    local txt2 = ccui.RichElementCustomNode:create(0, color, opacity, add_label)
	    self.m_pAddRichText:pushBackElement(txt2)
    end
    self.m_pAddRichText:setAnchorPoint(0.5,0.5)
    self.m_pAddRichText:move(215.5,140)
    self.m_pNodeRoot:addChild(self.m_pAddRichText)

    --设置vip等级加成
	local temp_num = math.floor((add_txt/gold_txt)*100)
	self.m_pVipAdd:setString(string.format("%d%%",temp_num))

end

return YueCardSignSuccess