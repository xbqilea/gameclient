--region *.lua
--Date
--
--endregion
-- Desc: 忘记密码
-- modified by JackyXu on 2017.05.16.

local ForgetDialog = class("ForgetDialog",FixLayer)

function ForgetDialog:ctor(nType)
    self.super:ctor(self)
    self:enableNodeEvents()

    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)

    self.m_nCountDown = -1
    self.typeDialog = 2 --默认银行功能
    self.m_strAccount = PlayerInfo.getInstance():getStrUserAccount()
    self.m_nEditBoxTouchCount = 0

    self:init(nType)
end

function ForgetDialog:init(nType)
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_NORMAL, FixLayer.HIDE_DLG_NORMAL)
    self:setVeilAlpha(0)

    self.m_nVerifyType = nType --1登录/2银行
    --init csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/FindPasswordDialog.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("FindPassWordDialog")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pImgBg        = self.m_pNodeRoot:getChildByName("Image_bg")
    self.m_pBtnGetNumber = self.m_pImgBg:getChildByName("BTN_get_number")
    self.m_pLbCountDown  = self.m_pImgBg:getChildByName("TXT_countDown")
    self.m_pBtnClose     = self.m_pImgBg:getChildByName("Button_close")
    self.m_pBtnConfirm   = self.m_pImgBg:getChildByName("Button_confirm")
    self.m_pSpTitle      = self.m_pImgBg:getChildByName("Image_title")
    self.m_pBtnNull     = self.m_pathUI:getChildByName("Panel_2") --空白处关闭

    self.m_pBtnClose:addClickEventListener(handler(self,self.onCloseClicked))
    self.m_pBtnNull:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnGetNumber:addClickEventListener(handler(self,self.onClickedGetNumber))
    self.m_pBtnConfirm:addClickEventListener(handler(self,self.onConfirmClicked))

    self.m_pLbCountDown:setVisible(false)

    self.m_pImgInput = {}
    for i = 1,5 do
        local nodeName = string.format("image_input%d",i)
        self.m_pImgInput[i] = self.m_pImgBg:getChildByName(nodeName) --tolua.cast(self[nodeName], "cc.Node")
    end

    return self
end

function ForgetDialog:onEnter()
    self.super:onEnter()
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_NORMAL, FixLayer.HIDE_DLG_NORMAL)
    self:showWithStyle()
    if device.platform == "windows" then
        self:initKeyboard()  
    end
    self.m_verifyBankPwd = SLFacade:addCustomEventListener(Hall_Events.MSG_BANK_VERIFY_PWD, handler(self, self.onMsgVerifyBankPwd))
    self.m_getMobile = SLFacade:addCustomEventListener(Hall_Events.MSG_GET_MOBILE, handler(self, self.onMsgGetMobile))

    self:initEditBox()
    
    self.handleTime = scheduler.scheduleGlobal(handler(self, self.updateCountDown), 1)

    if (self:getType() == 2) then --找回银行密码
        --根据账号获取绑定手机号
        local szPassword = PlayerInfo.getInstance():getLoginPwd()
        cc.exports.CMsgHall:sendRequestPhoneNumber(self.m_strAccount, szPassword, 1)
        cc.exports.Veil:getInstance():ShowVeil(VEIL_WAIT)
    end


    if self.m_strAccount ~= "" then
        self.m_pEditBox5:setText(self.m_strAccount)
    end

    --self.m_pEditBox1:setEnabled(false)
    if (self:getType() == 1) then --找回登录密码
        self.m_pSpTitle:loadTexture("hall/plist/login/gui-login-text-findpd.png",ccui.TextureResType.plistType)
    elseif (self:getType() == 2) then --找回银行密码
        self.m_pEditBox1:setEnabled(false)
        self.m_pEditBox5:setEnabled(false)
        --常规版：显示找回银行密码
        if ClientConfig.getInstance():getIsMainChannel() then
            self.m_pSpTitle:loadTexture("hall/plist/bank/gui-bank-text-findpd.png",ccui.TextureResType.plistType)
        end
        --和谐版：显示找回保险箱密码
        if ClientConfig.getInstance():getIsOtherChannel() then
            self.m_pSpTitle:loadTexture("hall/plist/bank/gui-bank-text-findpd-2.png",ccui.TextureResType.plistType)
        end
        self.m_pSpTitle:ignoreContentAdaptWithSize(true)
    end
end

function ForgetDialog:onExit()
    self.super:onExit()

    scheduler.unscheduleGlobal(self.handleTime)
    SLFacade:removeEventListener(self.m_verifyBankPwd)
    SLFacade:removeEventListener(self.m_getMobile)
    if device.platform == "windows" then
        self:cleanKeyboard()
    end
end

--监听按键
function ForgetDialog:initKeyboard() 
    local function onKeyReleased(keyCode, event)
        event:stopPropagation()
        if keyCode == cc.KeyCode.KEY_ENTER or keyCode == cc.KeyCode.KEY_KP_ENTER then 
            self:onConfirmClicked()
        elseif keyCode == cc.KeyCode.KEY_TAB then
            self:tabToNext()
        end
    end

    local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
    self.m_pKeyboardListener = cc.EventListenerKeyboard:create()
    self.m_pKeyboardListener:registerScriptHandler(onKeyReleased, cc.Handler.EVENT_KEYBOARD_RELEASED)
    pEventDispatcher:addEventListenerWithFixedPriority(self.m_pKeyboardListener, -130)
end

--移除按键监听
function ForgetDialog:cleanKeyboard()

    if self.m_pKeyboardListener then
        local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
        pEventDispatcher:removeEventListener(self.m_pKeyboardListener)
        self.m_pKeyboardListener = nil
    end
end

function ForgetDialog:tabToNext()
    
    local index = self.m_nEditBoxTouchCount+1 > 5 and 1 or self.m_nEditBoxTouchCount+1
    if self:getType() == 2 then 
        index = self.m_nEditBoxTouchCount+1 > 5 and 1 or self.m_nEditBoxTouchCount+1
        index = index < 3 and 3 or index
    end
    if self.m_nEditBoxTouchCount > 0 then 
        self.m_pEditBox[self.m_nEditBoxTouchCount]:touchDownAction(self.m_pEditBox[self.m_nEditBoxTouchCount],ccui.TouchEventType.canceled);
    end
    self.m_pEditBox[index]:touchDownAction(self.m_pEditBox[index],ccui.TouchEventType.ended);
    self.m_nEditBoxTouchCount = index
end
-----------------

function ForgetDialog:initEditBox()
     self.m_pEditBox1 = self:createEditBox(11, cc.KEYBOARD_RETURNTYPE_DONE, 2, self.m_pImgInput[1]:getContentSize(),LuaUtils.getLocalString("REGISTER_27"), true)
--    self.m_pEditBox1:setFontColor(cc.c3b(238, 228, 152))
    self.m_pEditBox1:setPosition(self.m_pImgInput[1]:getPositionX() + 10, self.m_pImgInput[1]:getPositionY() + 11)
    self.m_pEditBox1:setInputMode(cc.EDITBOX_INPUT_MODE_NUMERIC)

    self.m_pEditBox2 = self:createEditBox(6, cc.KEYBOARD_RETURNTYPE_DONE, 3, self.m_pImgInput[2]:getContentSize(), LuaUtils.getLocalString("REGISTER_19"))
--    self.m_pEditBox2:setFontColor(cc.c3b(238, 228, 152))
    self.m_pEditBox2:setPosition(self.m_pImgInput[2]:getPositionX() + 10, self.m_pImgInput[2]:getPositionY() + 11)
    self.m_pEditBox2:setInputMode(cc.EDITBOX_INPUT_MODE_NUMERIC)

    self.m_pEditBox3 = self:createEditBox(20, cc.KEYBOARD_RETURNTYPE_DONE, 4, self.m_pImgInput[3]:getContentSize(), LuaUtils.getLocalString("REGISTER_2"))
    self.m_pEditBox3:setPosition(self.m_pImgInput[3]:getPositionX() + 10, self.m_pImgInput[3]:getPositionY() + 11)
    self.m_pEditBox3:setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD)
    self.m_pEditBox3:setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE)

    self.m_pEditBox4 = self:createEditBox(20, cc.KEYBOARD_RETURNTYPE_DONE, 5, self.m_pImgInput[4]:getContentSize(), LuaUtils.getLocalString("REGISTER_2"))
    self.m_pEditBox4:setPosition(self.m_pImgInput[4]:getPositionX() + 10, self.m_pImgInput[4]:getPositionY() + 11)
    self.m_pEditBox4:setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD)
    self.m_pEditBox4:setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE)

    self.m_pEditBox5 = self:createEditBox(32, cc.KEYBOARD_RETURNTYPE_DONE, 1, self.m_pImgInput[5]:getContentSize(), LuaUtils.getLocalString("LOGIN_10"))
    self.m_pEditBox5:setPosition(self.m_pImgInput[5]:getPositionX() + 10, self.m_pImgInput[5]:getPositionY() + 11)
    self.m_pEditBox5:setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE)

    self.m_pImgBg:addChild(self.m_pEditBox1)
    self.m_pImgBg:addChild(self.m_pEditBox2)
    self.m_pImgBg:addChild(self.m_pEditBox3)
    self.m_pImgBg:addChild(self.m_pEditBox4)
    self.m_pImgBg:addChild(self.m_pEditBox5)

    self.m_pEditBox = { self.m_pEditBox5, self.m_pEditBox1, self.m_pEditBox2, self.m_pEditBox3, self.m_pEditBox4}
end

function ForgetDialog:createEditBox(maxLength, keyboardReturnType, tag, size, placestr, noNeedTouch)
    local sprite1 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite2 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite3 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    size = cc.size(size.width - 25, 40)
    local editBox = cc.EditBox:create(size, sprite1, sprite2, sprite3)
    editBox:setMaxLength(maxLength)
    editBox:setReturnType(keyboardReturnType)
    editBox:setTag(tag)
    editBox:setPlaceHolder(placestr)
    editBox:setPlaceholderFontName("Microsoft Yahei")
    editBox:setPlaceholderFontSize(28)
    editBox:setPlaceholderFontColor(G_CONSTANTS.PLACE_COLOR)
    editBox:setFontColor(G_CONSTANTS.INPUT_COLOR)
    editBox:setFont("Microsoft Yahei", 28)
    editBox:setFontColor(G_CONSTANTS.INPUT_COLOR)
    editBox:setAnchorPoint(cc.p(0, 0))
    editBox:registerScriptEditBoxHandler(handler(self, self.onEditBoxEventHandle))
    
    return editBox
end

function ForgetDialog:onEditBoxEventHandle(event, pSender)
    print("onEditBoxEventHandle")
    if "began" == event then
        AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
        self.m_pBtnNull:stopAllActions()
        self.m_pBtnNull:setEnabled(false)
        self.m_nEditBoxTouchCount = pSender:getTag()
    elseif "ended" == event then
    elseif "return" == event then
        --延迟设为可点击 防止还是响应到关闭界面
        local callback = cc.CallFunc:create(function ()
            self.m_pBtnNull:setEnabled(true)
        end)
        local seq = cc.Sequence:create(cc.DelayTime:create(0.16), callback)
        self.m_pBtnNull:runAction(seq)
    elseif "changed" == event then

    elseif "next" == event then
        self:tabToNext()
    end
end

function ForgetDialog:onCloseClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")

--    if self.typeDialog == 1 then --打开登录界面
--        self:getParent():getParent():onLoginClicked()
--    elseif self.typeDialog == 2 then --打开银行提示界面
--         self:getParent():getParent():showPwdDialog()
--    end

    self:onMoveExitView()
end

function ForgetDialog:onConfirmClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    self:verify()
end

function ForgetDialog:onClickedGetNumber()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    -------------------------
    -- modifie by JackyXu
    local curTime = os.time()
    if self.tlast and curTime - self.tlast < 1 then
        return
    end
    self.tlast = curTime
    -------------------------
    
    self:sendRequestPhoneCode()
end

function ForgetDialog:sendRequestPhoneCode()
    --请求手机验证码
    local strCellPhone = self.m_pEditBox1:getText()
    print("ForgetDialog:sendRequestPhoneCode strCellPhone:", strCellPhone)
    print("ForgetDialog:sendRequestPhoneCode #strCellPhone:", string.len(strCellPhone))
    if string.len(strCellPhone) == 0 then
        FloatMessage.getInstance():pushMessageWarning("REGISTER_17")
        return
    end

    if string.len(strCellPhone) ~= 11 or not LuaUtils.string_phoneNumber(strCellPhone) then
        FloatMessage.getInstance():pushMessage("REGISTER_18")
        return 
    end

    local nType = 5 --找回登录密码
    if self:getType() == 2 then --找回银行密码
        nType = 6
    end
    self.m_strAccount = self.m_pEditBox5:getText()
    CMsgHall:sendRequestPhoneVerifyCode(0, strCellPhone, self.m_strAccount, nType)
    --发送就开始倒计时
    self:onMsgVerifyDone()
end
    
function ForgetDialog:onMsgVerifyDone()
    self.m_pBtnGetNumber:setVisible(false)
    self.m_pLbCountDown:setVisible(true)
    self.m_nCountDown = 60

end

--重新获取倒计时
function ForgetDialog:updateCountDown()
    if (self.m_nCountDown == 0) then
        self.m_pLbCountDown:setVisible(false)
        self.m_pBtnGetNumber:setVisible(true)
    elseif(self.m_nCountDown > 0) then
        self.m_nCountDown = self.m_nCountDown - 1
        self.m_pLbCountDown:setString( string.format(LuaUtils.getLocalString("REGISTER_22"),self.m_nCountDown))
    end
end

--校验
function ForgetDialog:verify()
    local strCellPhone = self.m_pEditBox1:getText()
    local strVerifyCode = self.m_pEditBox2:getText()
    local strPassword1 = self.m_pEditBox3:getText()
    local strPassword2 = self.m_pEditBox4:getText()
    
    local strAccount = self.m_pEditBox5:getText()
    if string.len(strAccount) == 0 then --账户空
        FloatMessage.getInstance():pushMessageWarning("LOGIN_10")
        return
    end
    if string.len(strAccount) < 6 or string.len(strAccount) > 16 or LuaUtils.check_account(strAccount) == false then
        FloatMessage.getInstance():pushMessageWarning("REGISTER_5");
        return;
    end

    if string.len(strCellPhone) == 0 then
        FloatMessage.getInstance():pushMessageWarning("REGISTER_17")
        return
    end
    
    if string.len(strCellPhone) ~= 11 then
        FloatMessage.getInstance():pushMessageWarning("REGISTER_18")
        return 
    end
    
    if string.len(strVerifyCode) == 0 then
        FloatMessage.getInstance():pushMessageWarning("REGISTER_19")
        return
    end
    
    if not strPassword1 or strPassword1 == "" then
        FloatMessage.getInstance():pushMessageWarning("REGISTER_14")
        return
    end
    
    if not strPassword2 or strPassword2 == "" then
        FloatMessage.getInstance():pushMessageWarning("REGISTER_20")
        return
    end
    
    if strPassword1 ~= strPassword2 then
        FloatMessage.getInstance():pushMessageWarning("REGISTER_7")
        return
    end

    if string.len(strPassword1) < 6 or string.len(strPassword1) > 16 then
        FloatMessage.getInstance():pushMessageWarning("REGISTER_2")
        return
    end

    if LuaUtils.check_include_chinese(strPassword1) then
        FloatMessage.getInstance():pushMessageWarning("REGISTER_26")
        return
    end

    if not LuaUtils.check_paypassword(strPassword1) then
       FloatMessage.getInstance():pushMessageWarning("REGISTER_16")
       return
   end
    
    self:sendFindPwd()
end

function ForgetDialog:sendFindPwd()
    self.m_strAccount = self.m_pEditBox5:getText()
    local strAccount = self.m_strAccount --self.m_pEditBox1:getText()
    local strVerifyCode = self.m_pEditBox2:getText()
    local strPwd = self.m_pEditBox3:getText()
    local strPwdMD5 = SLUtils:MD5(strPwd)
    local strPhone = self.m_pEditBox1:getText()
    
    if (self:getType() == 1) then
        CMsgHall:sendResetLogonPwd(strAccount, strPwdMD5, strVerifyCode, strPhone )
    elseif (self:getType() == 2) then
        CMsgHall:sendResetBankPwd(strAccount, strPwdMD5, strVerifyCode)
    end
end

function ForgetDialog:getType()
    return self.typeDialog or 1
end

function ForgetDialog:onMsgVerifyBankPwd()
    
--    if self.typeDialog == 1 then --打开登录界面
--        self:getParent():getParent():onLoginClicked()
--    elseif self.typeDialog == 2 then --打开银行提示界面
--        PlayerInfo.getInstance():setInsurePass(self.m_pEditBox3:getText())
--        PlayerInfo.getInstance():setIsInsurePass(1)

--        self:getParent():getParent():showBankView()
--    end

    if self.typeDialog == 2 then --保存银行密码
        PlayerInfo.getInstance():setInsurePass(self.m_pEditBox3:getText())
        PlayerInfo.getInstance():setIsInsurePass(1)
    end

    self:onMoveExitView()
end

function ForgetDialog:onMsgGetMobile(_event)

    local _userdata = unpack(_event._userdata)
    if not _userdata then
        return
    end
    local strPhone = _userdata
    if (self:getType() == 2) then
        self.m_pEditBox1:setText(strPhone)
    end
end

function ForgetDialog:setAccountText(phone)
    self.m_pEditBox1:setText(phone)
end

return ForgetDialog
