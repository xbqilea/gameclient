local YueCardHelpView = require("hall.bean.YueCardHelpView")
local YueCardPayView = require("hall.bean.YueCardPayView")
local RechargeWebView = require("hall.bean.RechargeWebView")
local SpineManager = require("common.manager.SpineManager")

local YueCardShopView = class("YueCardShopView",FixLayer)

function YueCardShopView:ctor()
	self.super:ctor(self)
	self:enableNodeEvents()
	self:init()
	self:initBtnEvent()
	self:initMsgEvent()
end

function YueCardShopView:init()
	display.loadSpriteFrames("hall/plist/gui-card.plist", "hall/plist/gui-card.png")

	self.m_pathUI = cc.CSLoader:createNode("hall/csb/HallYueCardShopView.csb")
    self:addChild(self.m_pathUI)

    local diffY = (display.height - 750)/2
    self.m_pathUI:setPositionY(diffY)

    self.m_pNodeRoot = self.m_pathUI:getChildByName("Image_bg")
    self.m_pNodeRoot:setPositionX(display.width/2)


    self.m_pPanelClose = self.m_pathUI:getChildByName("Panel_1")
    self.m_pBtnClose = self.m_pNodeRoot:getChildByName("Button_close")
    self.m_pBtnHelp = self.m_pNodeRoot:getChildByName("Button_help")

    self.m_pZhouRoot = self.m_pNodeRoot:getChildByName("Image_zhou")
    self.m_pYueRoot = self.m_pNodeRoot:getChildByName("Image_yue")

    self.m_pBtnZhou = self.m_pZhouRoot:getChildByName("Button_1")
    self.m_pBtnZhou:setTouchEnabled(false)
    self.m_pBtnYue = self.m_pYueRoot:getChildByName("Button_1")
    self.m_pBtnYue:setTouchEnabled(false)

    self.m_pZhouTag = self.m_pZhouRoot:getChildByName("Image_5")
    self.m_pZhouTag:hide()
    self.m_pYueTag = self.m_pYueRoot:getChildByName("Image_5")
    self.m_pYueTag:hide()

    self.m_pZhouMoney = self.m_pZhouRoot:getChildByName("txt_money")
    self.m_pZhouMoney:hide()
    self.m_pYueMoney = self.m_pYueRoot:getChildByName("txt_money")
    self.m_pYueMoney:hide()

    self.m_pZhouMask = self.m_pZhouRoot:getChildByName("Image_1")
    self.m_pZhouMask:setLocalZOrder(5)
    self.m_pZhouMask:hide()
    self.m_pYueMask = self.m_pYueRoot:getChildByName("Image_1")
    self.m_pYueMask:setLocalZOrder(5)
    self.m_pYueMask:hide()

    self.m_pNodeRoot:getChildByName("Image_2"):setLocalZOrder(5)

     --添加标题特效
    local m_pTitleAni= SpineManager.getInstance():getSpineByBinary("hall/effect/325_yuekabiaotidonghua/325_biaotidonghua")
    m_pTitleAni:move(557,340)
    m_pTitleAni:setAnimation(0,"animation",true)
    self.m_pNodeRoot:addChild(m_pTitleAni)
    --添加周卡特效
    local m_pZhouAni= SpineManager.getInstance():getSpineByBinary("hall/effect/325_zhoukadonghua/325_zhoukadonghua")
    m_pZhouAni:move(215,130)
    m_pZhouAni:setAnimation(0,"animation",true)
    self.m_pZhouRoot:addChild(m_pZhouAni)
    --添加月卡特效
    local m_pCardAni= SpineManager.getInstance():getSpineByBinary("hall/effect/325_yuekadonghua/325_yuekadonghua")
    m_pCardAni:move(215,130)
    m_pCardAni:setAnimation(0,"animation",true)
    self.m_pYueRoot:addChild(m_pCardAni)
end

function YueCardShopView:initBtnEvent()
	self.m_pBtnClose:addClickEventListener(handler(self,self.onCloseClicked))
	self.m_pBtnHelp:addClickEventListener(handler(self,self.onHelpClicked))
	self.m_pBtnZhou:addTouchEventListener(handler(self,self.onZhouClicked))
	self.m_pBtnYue:addTouchEventListener(handler(self,self.onYueClicked))
	self.m_pPanelClose:addClickEventListener(handler(self,self.onCloseClicked))
end

function YueCardShopView:initMsgEvent()
	self.msg_event = {
        [Hall_Events.MSG_UPDATE_SHOP_VIEW] = {func = self.updateShopView,log = "更新月卡商城界面"},
		[Public_Events.MSG_RECHARGE_WEB] = {func = self.showRechargeWebView,log = "购买web界面"},
        [Public_Events.MSG_SUPPORT] = {func = self.onMsgUpdateSupport,log = "获取充值接口"},
	}

	local function onMsgUpdate(event)  --接收自定义事件
        local name = event:getEventName()
        local msg = unpack(event._userdata)
        local func = self.msg_event[name].func
        func(self, msg)
    end

	for key, event in pairs(self.msg_event) do   --监听事件
         SLFacade:addCustomEventListener(key, onMsgUpdate, self.__cname)
    end
end

function YueCardShopView:deleteMsgEvent()
    for key in pairs(self.msg_event) do   --监听事件
         SLFacade:removeCustomEventListener(key, self.__cname)
    end
    self.msg_event = {}
end

function YueCardShopView:onEnter()
    -- performWithDelay(self.m_pathUI,function ()
    --     --请求一下支持支付金额
    --     CRechargeManager.getInstance():requestSupportCharge()
    -- end,1.0/30)
    self:onMsgUpdateSupport()

    local wait_action = cc.Sequence:create(cc.DelayTime:create(1.0),cc.CallFunc:create(function ()
        Veil:getInstance():ShowVeil(VEIL_WAIT)
    end))
    self.m_pNodeRoot:runAction(wait_action)

	self.super:onEnter()
	self:setTargetShowHideStyle(self,FixLayer.SHOW_DLG_BIG,FixLayer.HIDE_DLG_BIG)
	self:showWithStyle()
end

function YueCardShopView:onExit()
    self:stopWaitAction()
    Veil:getInstance():HideVeil(VEIL_WAIT)
	self.super:onExit()
	self:deleteMsgEvent()
end

function YueCardShopView:stopWaitAction()
    self.m_pNodeRoot:stopAllActions()
end

function YueCardShopView:onCloseClicked()
	AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
	self:onMoveExitView()
end

function YueCardShopView:onHelpClicked()
	AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
	local help_view = YueCardHelpView.new()
	self.m_pathUI:addChild(help_view,80)
end

function YueCardShopView:onZhouClicked(sender,eventType)
    if eventType == ccui.TouchEventType.began then
        AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
        self.m_pZhouRoot:setScale(1.03)
    elseif eventType == ccui.TouchEventType.moved then
    elseif eventType == ccui.TouchEventType.ended then
        local card_data = PlayerInfo.getInstance():getMonthDataByType("month_card_7days")
        if not card_data or not card_data.bSwitch then return end
        self:showPayView(card_data.dPrice,"month_card_7days")
        self.m_pZhouRoot:setScale(1)
    elseif eventType == ccui.TouchEventType.canceled then
        self.m_pZhouRoot:setScale(1)
    end
end

function YueCardShopView:onYueClicked(sender,eventType)
    if eventType == ccui.TouchEventType.began then
        AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
        self.m_pYueRoot:setScale(1.03)
    elseif eventType == ccui.TouchEventType.moved then
    elseif eventType == ccui.TouchEventType.ended then
        local card_data = PlayerInfo.getInstance():getMonthDataByType("month_card_30days")
        if not card_data or not card_data.bSwitch then return end
        self:showPayView(card_data.dPrice,"month_card_30days")
        self.m_pYueRoot:setScale(1)
    elseif eventType == ccui.TouchEventType.canceled then
        self.m_pYueRoot:setScale(1)
    end
end

--选择支付方式界面
function YueCardShopView:showPayView(pay_money,pay_product)
    if device.platform == "windows" and not ClientConfig.getInstance():getDebugMode() then
        FloatMessage.getInstance():pushMessage("请前往移动端购买月卡！")
        return
    end
	local pay_view = YueCardPayView.new()
	pay_view:setPayData(pay_money,pay_product)
	self.m_pathUI:addChild(pay_view,80)
end

function YueCardShopView:updateTipsData()
    local card_data = PlayerInfo.getInstance():getMonthCardData()
	local end_time = PlayerInfo.getInstance():getMonthCardTime()
    local end_time_num = 0
    if card_data and card_data.bMonthCardUser then
    	local temp_time = os.time({ year = end_time.wYear, month= end_time.wMonth, day = end_time.wDay, hour = end_time.wHour, min = end_time.wMinute, sec = end_time.wSecond, isdst = false })
        end_time_num = os.difftime(temp_time,os.time()+PlayerInfo.getInstance():getTimeOffset()+LuaUtils.getWithGMT8Time())
	end

    if self.m_pTipRichText  then
        self.m_pTipRichText:removeFromParent()
        self.m_pTipRichText = nil
    end

    local size = 22
    local opacity = 255
    local font = ""

    --时间label
    self.m_pTipRichText = ccui.RichText:create()

    if end_time_num > 0 then
        local temp_str = string.format("当前特权有效期：%d年%d月%d日%02d:%02d",end_time.wYear,end_time.wMonth,end_time.wDay,end_time.wHour,end_time.wMinute) 
        local tip_text1 = cc.Label:createWithSystemFont(temp_str, font, size)
        local lb = ccui.RichElementCustomNode:create(0, cc.c3b(0xfa,0xe1,0x77), opacity, tip_text1)
        self.m_pTipRichText:pushBackElement(lb)

        temp_str = string.format(" (%s)",LuaUtils.getCardDateStr(end_time_num))

        local tip_text2 = cc.Label:createWithSystemFont(temp_str, font, size)
        local lb2 = ccui.RichElementCustomNode:create(0, cc.c3b(0x60,0xff,0xea), opacity, tip_text2)
        self.m_pTipRichText:pushBackElement(lb2)

    else
        local temp_str = "当前尚未拥有月卡特权！"
        local tip_text1 = cc.Label:createWithSystemFont(temp_str, font, size)
        local lb = ccui.RichElementCustomNode:create(0, cc.c3b(0xfa,0xe1,0x77), opacity, tip_text1)
        self.m_pTipRichText:pushBackElement(lb)
    end

    self.m_pTipRichText:setAnchorPoint(0,0.5)
    self.m_pTipRichText:move(280,25)
    self.m_pNodeRoot:addChild(self.m_pTipRichText)

end

function YueCardShopView:updateShopView()
    self:stopWaitAction()
    Veil:getInstance():HideVeil(VEIL_WAIT)
    --更新一下特权时间
    self:updateTipsData()
    --更新一下月卡数据
    local card_data = PlayerInfo.getInstance():getMonthCardData()
    if not card_data then return end
    -- local card_info = card_data.monthCardInfo


    local zhou_data = PlayerInfo.getInstance():getMonthDataByType("month_card_7days")
    if zhou_data then
        self.m_pZhouMoney:setString(string.format("周卡：%s元",zhou_data.dPrice))

        --周卡获得金币label
        if self.m_pZhouRichText then
            self.m_pZhouRichText:removeFromParent()
        end

        self.m_pZhouRichText = self:createCardRichText(1,zhou_data.dGrandScore)
        self.m_pZhouRichText:setAnchorPoint(1,0.5)
        self.m_pZhouRichText:move(413,48)
        self.m_pZhouRoot:addChild(self.m_pZhouRichText)

        self.m_pBtnZhou:setTouchEnabled(zhou_data.bSwitch)
        self.m_pZhouMoney:setVisible(zhou_data.bSwitch)
        self.m_pZhouTag:setVisible(zhou_data.bSwitch)
        self.m_pZhouRichText:setVisible(zhou_data.bSwitch)
        self.m_pZhouMask:setVisible(not zhou_data.bSwitch)

        local isShowAliPay = CRechargeManager.getInstance():judgeChargeByMoney(1,zhou_data.dPrice)
        local isShowWeChat = CRechargeManager.getInstance():judgeChargeByMoney(2,zhou_data.dPrice)
        local isShowBank = CRechargeManager.getInstance():judgeChargeByMoney(4,zhou_data.dPrice)
        local isShowYSF = CRechargeManager.getInstance():judgeChargeByMoney(10,zhou_data.dPrice)
        if isShowAliPay or isShowWeChat or isShowBank or isShowYSF then
        else
            self.m_pBtnZhou:setTouchEnabled(false)
            self.m_pZhouMoney:setVisible(false)
            self.m_pZhouTag:setVisible(false)
            self.m_pZhouRichText:setVisible(false)
            self.m_pZhouMask:setVisible(true)
        end
    else
        self.m_pBtnZhou:setTouchEnabled(false)
        self.m_pZhouMoney:setVisible(false)
        self.m_pZhouTag:setVisible(false)
        self.m_pZhouMask:setVisible(true)
    end

    local yue_data = PlayerInfo.getInstance():getMonthDataByType("month_card_30days")

    if yue_data then
        self.m_pYueMoney:setString(string.format("月卡：%s元",yue_data.dPrice))

        --月卡获得金币label
        if self.m_pYueRichText then
            self.m_pYueRichText:removeFromParent()
        end

        self.m_pYueRichText = self:createCardRichText(2,yue_data.dGrandScore)
        self.m_pYueRichText:setAnchorPoint(1,0.5)
        self.m_pYueRichText:move(413,48)
        self.m_pYueRoot:addChild(self.m_pYueRichText)

        self.m_pBtnYue:setTouchEnabled(yue_data.bSwitch)
        self.m_pYueMoney:setVisible(yue_data.bSwitch)
        self.m_pYueTag:setVisible(yue_data.bSwitch)
        self.m_pYueRichText:setVisible(yue_data.bSwitch)
        self.m_pYueMask:setVisible(not yue_data.bSwitch)

        local isShowAliPay = CRechargeManager.getInstance():judgeChargeByMoney(1,yue_data.dPrice)
        local isShowWeChat = CRechargeManager.getInstance():judgeChargeByMoney(2,yue_data.dPrice)
        local isShowBank = CRechargeManager.getInstance():judgeChargeByMoney(4,yue_data.dPrice)
        local isShowYSF = CRechargeManager.getInstance():judgeChargeByMoney(10,yue_data.dPrice)

        if isShowAliPay or isShowWeChat or isShowBank or isShowYSF then
        else
            self.m_pBtnYue:setTouchEnabled(false)
            self.m_pYueMoney:setVisible(false)
            self.m_pYueTag:setVisible(false)
            self.m_pYueRichText:setVisible(false)
            self.m_pYueMask:setVisible(true)
        end
    else
        self.m_pBtnYue:setTouchEnabled(false)
        self.m_pYueMoney:setVisible(false)
        self.m_pYueTag:setVisible(false)
        self.m_pYueMask:setVisible(true)
    end
end

--创建月卡赠送金币RichText
function YueCardShopView:createCardRichText(card_type,gold_num)
    local opacity = 255
    local color = cc.WHITE

    local tempRichText = ccui.RichText:create()

    local font_str = card_type == 1 and "hall/font/yksz3.fnt" or "hall/font/yksz2.fnt"
    local temp_label = cc.Label:createWithBMFont(font_str, "赠送")
    local lb = ccui.RichElementCustomNode:create(0,color,opacity,temp_label)
    tempRichText:pushBackElement(lb)

    local temp_sprite = cc.Sprite:createWithSpriteFrameName("hall/plist/yueka/gui-card-relief-gold-icon1.png")
    local sp = ccui.RichElementCustomNode:create(0, color, opacity, temp_sprite)
    tempRichText:pushBackElement(sp)

    local temp_gold = cc.Label:createWithBMFont("hall/font/yksz1.fnt", self:getGoldStr(gold_num))
    local lb2 = ccui.RichElementCustomNode:create(0,color,opacity,temp_gold)
    tempRichText:pushBackElement(lb2)

    return tempRichText
end

function YueCardShopView:getGoldStr(gold)
    local result_str = ""
    if gold >= 100000000 then
        result_str = string.format("%d亿",gold/100000000)
    elseif  gold >=10000 then
        result_str = string.format("%d万",gold/10000)
    else
        result_str = gold
    end
    return result_str
end

function YueCardShopView:onMsgUpdateSupport()
    --请求月卡信息
    CMsgHall:sendUserIdData(G_C_CMD.MDM_VIP_INFO, G_C_CMD.SUB_MONTH_CARD_INFO_GET)
end

function YueCardShopView:showRechargeWebView(strUserData)
    if not strUserData or strUserData == "" then return end

    local vecStrings = string.split(strUserData, ";")
    if table.nums(vecStrings) == 0 then
        return
    end

    local nType    = tonumber(vecStrings[1])
    local strUrl   = tostring(vecStrings[2])
    local bVisible = tonumber(vecStrings[3])
    print("YueCardShopView:showRechargeWebView", nType, strUrl, bVisible)

    if self.m_pRechargeWebView then
        self.m_pRechargeWebView:removeFromParent()
        self.m_pRechargeWebView = nil
    end

    self.m_pRechargeWebView = RechargeWebView:create(self)
    self.m_pRechargeWebView:setWebViewInfoPost(nType, strUrl, bVisible)
    self.m_pRechargeWebView:addTo(self.m_pathUI)
end

function YueCardShopView:removeWebView()
    self.m_pRechargeWebView:removeFromParent()
    self.m_pRechargeWebView = nil
end

return YueCardShopView