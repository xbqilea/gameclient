--region RegisterView.lua
--Date 2017.04.26.
--Auther JackyXu.
--Desc: 注册正式帐号 view

local scheduler = require("cocos.framework.scheduler")
local TreatyView = require("hall.bean.TreatyView")

local UsrNick = require("common.public.UsrNick")

local RegisterView = class("RegisterView",FixLayer)

local G_RegisterView = nil
function RegisterView.create(nType)
    G_RegisterView = RegisterView.new():init(nType)
    return G_RegisterView
end

function RegisterView:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()

    self.m_pShengmFirstPic = nil
    self.m_pNodeInput = {}
    self.m_pEditBox = {}

    self.m_pLbCountDown = nil
    self.m_pBtnGetNumber = nil
    self.m_nCountDown = -1
    self.m_bShengmFirst = false
    self.m_nLastTouchTime = 0
    self.m_nEditBoxTouchCount = 0
    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)

    self.m_rootUI:addTo(self)
end

function RegisterView:init(nType)
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_NORMAL, FixLayer.HIDE_DLG_NORMAL)

    self.m_nType = nType

    --init csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/RegisterDialog.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))


    self.m_pNodeRoot    = self.m_pathUI:getChildByName("RegisterDialog")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pImageBg         = self.m_pNodeRoot:getChildByName("Image_bg")
    for i=1, 4 do
        local nodeName = "Image_input" .. i
        self.m_pNodeInput[i] = self.m_pImageBg:getChildByName(nodeName)
    end
    self.m_pShengmFirstPic  = self.m_pImageBg:getChildByName("IMG_select")
    self.m_pLbCountDown     = self.m_pImageBg:getChildByName("TXT_countDown")
    self.m_pBtnGetNumber    = self.m_pImageBg:getChildByName("BTN_acctoun")
    self.m_pBtnConfirm      = self.m_pImageBg:getChildByName("BTN_confirm")
    self.m_pBtnClose        = self.m_pImageBg:getChildByName("BTN_close")
    self.m_pBtnCheck        = self.m_pImageBg:getChildByName("BTN_check")
    self.m_pBtnDetail       = self.m_pImageBg:getChildByName("BTN_detail")
    self.m_pBtnNull         = self.m_pathUI:getChildByName("Panel_2") --空白处关闭

    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnNull:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnConfirm:addClickEventListener(handler(self, self.onRegisterClicked))
    self.m_pBtnGetNumber:addClickEventListener(handler(self, self.onClickedGetNumber))
    self.m_pBtnCheck:addClickEventListener(handler(self, self.onShengmFirstBtnClicked))
    self.m_pBtnDetail:addClickEventListener(handler(self, self.onTreatyClicked))

    return self
end

function RegisterView:onEnter()
    self.super:onEnter()
    self:showWithStyle()
    if device.platform == "windows" then
        self:initKeyboard()  
    end
    self:initEditBox()

    self.m_pLbCountDown:setVisible(false)
    self.m_pBtnGetNumber:setVisible(true)
    self.m_pShengmFirstPic:setVisible(true)

    self.handleTime = scheduler.scheduleGlobal(handler(self, self.updateCountDown), 1)
    cc.exports.SLFacade:addCustomEventListener(Hall_Events.MSG_CLOSE_REGISTER, handler(self, self.onMsgCloseRegsiter), self.__cname)
end

function RegisterView:onExit()
    cc.exports.SLFacade:removeCustomEventListener(Hall_Events.MSG_CLOSE_REGISTER, self.__cname)
    scheduler.unscheduleGlobal(self.handleTime)
    self.super:onExit()
    if device.platform == "windows" then
        self:cleanKeyboard()
    end
    G_RegisterView = nil
end

--监听按键
function RegisterView:initKeyboard() 
    local function onKeyReleased(keyCode, event)
        event:stopPropagation()
        if keyCode == cc.KeyCode.KEY_ENTER or keyCode == cc.KeyCode.KEY_KP_ENTER then 
            self:onRegisterClicked()
        elseif keyCode == cc.KeyCode.KEY_TAB then
            self:tabToNext()
        end
    end

    local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
    self.m_pKeyboardListener = cc.EventListenerKeyboard:create()
    self.m_pKeyboardListener:registerScriptHandler(onKeyReleased, cc.Handler.EVENT_KEYBOARD_RELEASED)
    pEventDispatcher:addEventListenerWithFixedPriority(self.m_pKeyboardListener, -129)
end

--移除按键监听
function RegisterView:cleanKeyboard()

    if self.m_pKeyboardListener then
        local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
        pEventDispatcher:removeEventListener(self.m_pKeyboardListener)
        self.m_pKeyboardListener = nil
    end
end

function RegisterView:tabToNext()
    
    local index = self.m_nEditBoxTouchCount+1 > 4 and 1 or self.m_nEditBoxTouchCount+1
    if self.m_nEditBoxTouchCount > 0 then 
        self.m_pEditBox[self.m_nEditBoxTouchCount]:touchDownAction(self.m_pEditBox[self.m_nEditBoxTouchCount],ccui.TouchEventType.canceled);
    end
    self.m_pEditBox[index]:touchDownAction(self.m_pEditBox[index],ccui.TouchEventType.ended);
    self.m_nEditBoxTouchCount = index
end
--------------------

function RegisterView:initEditBox()
    -----------------
    -- 1、手机号码
    -- 2、验证码
    -- 3、密码
    -- 4、确认密码
    -----------------
    local InputMode = {
        cc.EDITBOX_INPUT_MODE_NUMERIC,
        cc.EDITBOX_INPUT_MODE_NUMERIC,
        cc.EDITBOX_INPUT_MODE_SINGLELINE,
        cc.EDITBOX_INPUT_MODE_SINGLELINE,
        cc.EDITBOX_INPUT_MODE_SINGLELINE,
    }
    local InputFlag = {
        [1] = cc.EDITBOX_INPUT_FLAG_SENSITIVE,
        [2] = cc.EDITBOX_INPUT_FLAG_SENSITIVE,
        [3] = cc.EDITBOX_INPUT_FLAG_PASSWORD,
        [4] = cc.EDITBOX_INPUT_FLAG_PASSWORD,
        [5] = cc.EDITBOX_INPUT_FLAG_SENSITIVE,
    }
    local maxLen = {11,6,20,20,20}
    local strPlace = { 
        LuaUtils.getLocalString("REGISTER_17"), 
        LuaUtils.getLocalString("REGISTER_19"),
        LuaUtils.getLocalString("REGISTER_2"),
        LuaUtils.getLocalString("REGISTER_2"),
        LuaUtils.getLocalString("REGISTER_3"),
    };
    local fontColor = {
        G_CONSTANTS.INPUT_COLOR,
        G_CONSTANTS.INPUT_COLOR,
        G_CONSTANTS.INPUT_COLOR,
        G_CONSTANTS.INPUT_COLOR,
        G_CONSTANTS.INPUT_COLOR,
    }

    for i=1,4 do
        self.m_pEditBox[i] = self:createEditBox(maxLen[i],
                                      cc.KEYBOARD_RETURNTYPE_DONE,
                                      InputMode[i],
                                      InputFlag[i],
                                      i,
                                      self.m_pNodeInput[i]:getContentSize(),
                                      strPlace[i],
                                      fontColor[i])

        local pos = cc.p(self.m_pNodeInput[i]:getPositionX() + 15, self.m_pNodeInput[i]:getPositionY() + 11)
        self.m_pEditBox[i]:setPosition(pos)
        if i == 2 then
            self.m_pImageBg:addChild(self.m_pEditBox[i], 50)
        else
            self.m_pImageBg:addChild(self.m_pEditBox[i])
        end
    end
end

function RegisterView:createEditBox(maxLength, keyboardReturnType, inputMode, inputFlag, tag, size, placestr, color)
    local sprite1 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite2 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite3 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    size = cc.size(size.width - 25, 40)
    local editBox = cc.EditBox:create(size, sprite1, sprite2, sprite3)
    editBox:setMaxLength(maxLength)
    editBox:setReturnType(keyboardReturnType)
    editBox:setInputMode(inputMode)
    editBox:setInputFlag(inputFlag)
    editBox:setTag(tag)
    editBox:setPlaceHolder(placestr)
    editBox:setPlaceholderFontName("Helvetica")
    editBox:setPlaceholderFontSize(28)
    editBox:setPlaceholderFontColor(G_CONSTANTS.PLACE_COLOR)
    editBox:setFont("Helvetica", 28)
    editBox:setFontColor(color)
    editBox:setAnchorPoint(cc.p(0, 0))
    editBox:registerScriptEditBoxHandler(handler(self, self.onEditBoxEventHandle))

--    -- 模拟点击输入框解决 android 手机首次输入看不到输入内容BUG。
--    if device.platform == "android" then
--        self.m_nEditBoxTouchCount = 0
--        editBox:touchDownAction(editBox,ccui.TouchEventType.ended)
--        editBox:touchDownAction(editBox,ccui.TouchEventType.canceled)
--    else
--        self.m_nEditBoxTouchCount = 6
--    end

    return editBox
end

function RegisterView:onEditBoxEventHandle(event, pSender)
    if "began" == event then
         AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
         self.m_pBtnNull:stopAllActions()
         self.m_pBtnNull:setEnabled(false)
         self.m_nEditBoxTouchCount = pSender:getTag()
    elseif "ended" == event then
    elseif "return" == event then
         --延迟设为可点击 防止还是响应到关闭界面
        local callback = cc.CallFunc:create(function ()
            self.m_pBtnNull:setEnabled(true)
        end)
        local seq = cc.Sequence:create(cc.DelayTime:create(0.16), callback)
        self.m_pBtnNull:runAction(seq)
    elseif "changed" == event then

    elseif "next" == event then
        self:tabToNext()
    end
end

-- 请求手机验证码
function RegisterView:sendRequestPhoneCode()
    
    local strCellPhone = self.m_pEditBox[1]:getText()
    if string.len(strCellPhone) == 0 then
        FloatMessage.getInstance():pushMessageWarning("REGISTER_17")
        return
    end
    if not LuaUtils.CheckIsMobile(strCellPhone) then
        FloatMessage.getInstance():pushMessageWarning("REGISTER_18")
        return 
    end
    
    local verifyIndex = 0
    local strIndex = ""
    if (not PlayerInfo.getInstance():getIsGuest()) and 
       (not GameListManager.getInstance():getHttpGameSwitch(G_CONSTANTS.GAMESWITCH.CLOSE_REGISTER_VERIFY)) then
        -- 验证码
        local count = 0
        for i=1,MAX_VERIFY_CODE_COUNT do
            if PlayerInfo.getInstance():getVerifyCodeChooseAtIndex(i) then
                count = count + 1
                strIndex = strIndex .. i
            end
        end
        verifyIndex = tonumber(strIndex)
    end
    
    -- TODO send msg.

    --11 => '注册',(在用)
    --13 => '绑定正式账号',(在用)

    --string2char16(strCellPhone, szAccounts, LEN_MOBILE_PHONE)

    if self.m_nType == 1 then
        CMsgHall:sendRequestPhoneVerifyCode(verifyIndex, strCellPhone, "", 11) --注册账号验证码
    elseif self.m_nType == 2 then
        local szAccounts = PlayerInfo:getInstance():getStrUserAccount()
        CMsgHall:sendRequestPhoneVerifyCode(verifyIndex, strCellPhone, szAccounts, 13) --绑定账号验证码
    end
    
    -- 发送就开始倒计时
    self:onMsgVerifyDone(nil)
end

function RegisterView:onMsgVerifyDone(pUserdata)
    -- 请求手机验证码消息返回处理
    -- print(" -- register -- todo: -- ")
    self.m_pBtnGetNumber:setVisible(false)
    self.m_pLbCountDown:setVisible(true)
    
    self.m_nCountDown = 60
end

function RegisterView:onMsgCloseRegsiter()
    --登录成功，取消遮罩
    cc.exports.Veil:getInstance():HideVeil(VEIL_CONNECT)

    local password = self.m_pEditBox[4]:getText()
    local pwdMD5 = SLUtils:MD5(password)
    PlayerInfo.getInstance():setLoginPwd(pwdMD5)

    --注册完成
    self:onMoveExitView()
end

function RegisterView:updateCountDown(dt)
    if self.m_nCountDown == 0 then
        self.m_pLbCountDown:setVisible(false)
        self.m_pBtnGetNumber:setVisible(true)
    elseif self.m_nCountDown > 0 then
        self.m_nCountDown = self.m_nCountDown - 1
        local strTip = string.format(LuaUtils.getLocalString("REGISTER_22"), self.m_nCountDown)
        self.m_pLbCountDown:setString(strTip)
    end
end

-- 校验
function RegisterView:_verify()
    local strCellPhone = self.m_pEditBox[1]:getText()
    local strVerifyCode = self.m_pEditBox[2]:getText()
    local strPassword1 = self.m_pEditBox[3]:getText()
    local strPassword2 = self.m_pEditBox[4]:getText()
    
    if string.len(strCellPhone) == 0 then
        FloatMessage.getInstance():pushMessageWarning("REGISTER_17")
        return
    end
    
    if string.len(strCellPhone) ~= 11 or not LuaUtils.string_number2(strCellPhone) then
        FloatMessage.getInstance():pushMessageWarning("REGISTER_18")
        return 
    end
    
    if string.len(strVerifyCode) == 0 then
        FloatMessage.getInstance():pushMessageWarning("REGISTER_19")
        return
    end
    
    if string.len(strPassword1) <= 0 then
        FloatMessage.getInstance():pushMessageWarning("REGISTER_14")
        return
    end
    
    if string.len(strPassword2) <= 0 then
        FloatMessage.getInstance():pushMessageWarning("REGISTER_20")
        return
    end
    
    if strPassword1 ~= strPassword2 then
        FloatMessage.getInstance():pushMessageWarning("REGISTER_7")
        return
    end
    
    if string.len(strPassword1) < 6 or string.len(strPassword1) > 16 then
        FloatMessage.getInstance():pushMessageWarning("REGISTER_2")
    end

    if LuaUtils.check_include_chinese(strPassword1) then
        FloatMessage.getInstance():pushMessageWarning("REGISTER_26")
        return
    end
    
    if not LuaUtils.check_paypassword(strPassword1) then
        FloatMessage.getInstance():pushMessageWarning("REGISTER_16")
        return
    end

    local strNickName = ""
    if PlayerInfo.getInstance():getAccountsType() ~= 0 then --第三方登陆的账号升级昵称不随机
        strNickName = PlayerInfo.getInstance():getNameNick()
    else
        local nFirstCount = UsrNick:getInstance():getNumFirstName()
        local nLastCount = UsrNick:getInstance():getNumLastName()
        while true do
            local nFidx = math.random(1, nFirstCount)
            local nLidx = math.random(1, nLastCount)
            strNickName = UsrNick:getInstance():getNickNameByIdx(nFidx, nLidx)
            if LuaUtils.getCharLength(strNickName) <= 16 then
                break
            end
        end
    end
    print("rand nickname："..strNickName)

    --账号就是手机号
    local strAccount = strCellPhone

    cc.exports.scheduler.performWithDelayGlobal(function()
        if (PlayerInfo.getInstance():getUserID() > 0 and PlayerInfo.getInstance():getIsGuest()) then
            cc.exports.CMsgHall:sendGuestRegister(strNickName, strCellPhone, strPassword1, strCellPhone, strVerifyCode)
        else
            cc.exports.CMsgHall:sendRegister(strNickName, strCellPhone, strPassword1, strCellPhone, strVerifyCode)
        end
    end, 1.0)

--    cc.UserDefault:getInstance():setStringForKey("loginName", strCellPhone)
--    cc.UserDefault:getInstance():setStringForKey("password", strPassword1)

    cc.exports.Veil:getInstance():ShowVeil(VEIL_LOCK)
end

------------------------------------
-- 按纽响应
-- 返回
function RegisterView:onCloseClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")

    self:onMoveExitView()

--    if PlayerInfo.getInstance():getNeedPopNotice() then
--        PlayerInfo.getInstance():setNeedPopNotice(false)
--        SLFacade:dispatchCustomEvent(Public_Events.MSG_POP_NOTICE)
--    end
    SLFacade:dispatchCustomEvent(Public_Events.MSG_OPEN_HALL_NOTICE)
end

-- 获取验证码
function RegisterView:onClickedGetNumber(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    -- body
    print("RegisterView.onClickedGetNumber")
    -- 防连点
    local nCurTime = os.time()
    if self.m_nLastTouchTime and nCurTime - self.m_nLastTouchTime <= 0 then
        return
    end
    self.m_nLastTouchTime = nCurTime
    
    local strCellPhone = self.m_pEditBox[1]:getText()
    if string.len(strCellPhone) == 0 then
        FloatMessage.getInstance():pushMessageWarning("REGISTER_17")
        return
    end
    if string.len(strCellPhone) ~= 11 then
        FloatMessage.getInstance():pushMessageWarning("REGISTER_18")
        return
    end
    
   if (not GameListManager.getInstance():getHttpGameSwitch(G_CONSTANTS.GAMESWITCH.CLOSE_REGISTER_VERIFY)) and 
      (not PlayerInfo.getInstance():getIsGuest()) then

       -- 开启注册获取手机验证码并且非游客绑定 弹出图形验证码界面
       -- MsgCenter:getInstance():postMsg(MSG_SHOW_VIEW, __String:create("loginverify-3"))
   else
        self:sendRequestPhoneCode()
   end

end

-- 游戏协议复选框
function RegisterView:onShengmFirstBtnClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
--    local self = G_RegisterView

    self.m_bShengmFirst = not self.m_bShengmFirst
    if self.m_bShengmFirst then
        self.m_pShengmFirstPic:setVisible(false)
    else
        self.m_pShengmFirstPic:setVisible(true)
    end
end

-- 游戏协议
function RegisterView:onTreatyClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    local pTreatyView = self.m_rootUI:getChildByName("TreatyView")
    if pTreatyView then
        pTreatyView:setVisible(true)
        return
    end
    pTreatyView = TreatyView:create()
    pTreatyView:setName("TreatyView")
    self.m_rootUI:addChild(pTreatyView)
end

-- 注册
function RegisterView:onRegisterClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    -- body
    self:_verify()
end

return RegisterView
