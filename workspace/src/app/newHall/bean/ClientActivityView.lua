--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

local ClientActivityView = class("ClientActivityView", FixLayer)

local G_ClientActivityView = nil
function ClientActivityView.create()
    G_ClientActivityView = ClientActivityView.new():init()
    return G_ClientActivityView
end

function ClientActivityView:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()

    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)
end

function ClientActivityView:init()

    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_BIG, FixLayer.HIDE_DLG_BIG)
    
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/ClientActivityView.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("ClientActivityLayer")
    local diffX = 145-(1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pBtnEnter    = self.m_pNodeRoot:getChildByName("Button_1")
    self.m_pBtnClose    = self.m_pNodeRoot:getChildByName("Button_2")

    --动画
    local pathJson = "hall/effect/325_dianjidongha/325_dianjidongha.json"
    local pathAtlas = "hall/effect/325_dianjidongha/325_dianjidongha.atlas"
    self.m_pAni = sp.SkeletonAnimation:createWithJsonFile(pathJson, pathAtlas)
    self.m_pAni:setPosition(cc.p(667,375))
    self.m_pNodeRoot:addChild(self.m_pAni)
    self.m_pAni:setAnimation(0, "animation", true)


    --按钮响应
    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnEnter:addClickEventListener(handler(self, self.onEnterClicked))

    return self
end

function ClientActivityView:onEnter()
    self.super:onEnter()
    self:showWithStyle()
end

function ClientActivityView:onExit()

    G_ClientActivityView = nil
    self.super:onExit()
end

-----------------clicked
function ClientActivityView:onCloseClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    local self = G_ClientActivityView

    self:onMoveExitView(self.HIDE_NO_STYLE)

    SLFacade:dispatchCustomEvent(Public_Events.MSG_OPEN_HALL_NOTICE)
end

function ClientActivityView:onEnterClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    local self = G_ClientActivityView

    self:getParent():getParent():showActivityView(10)
    self:onMoveExitView(self.HIDE_NO_STYLE)
end

return ClientActivityView

--endregion
