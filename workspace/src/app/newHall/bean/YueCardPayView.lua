
local YueCardPayView = class("YueCardPayView",FixLayer)

function YueCardPayView:ctor()
	self.super:ctor(self)
	self:enableNodeEvents()
	self:initVar()
	self:init()
	self:initBtnEvent()
end

function YueCardPayView:initVar()
	self.m_pMoney = 0 --当前需要支付的钱
	self.m_pProduct = "" --当前购买月卡种类的标识
	self.m_pBtnPosList = {} --存放充值按钮的位置
end

function YueCardPayView:init()
	self.m_pathUI = cc.CSLoader:createNode("hall/csb/HallYueCardPayView.csb")
    self:addChild(self.m_pathUI)

    local diffY = (display.height - 750)/2
    self.m_pathUI:setPositionY(diffY)

    self.m_pNodeRoot = self.m_pathUI:getChildByName("Image_bg")
    self.m_pNodeRoot:setPositionX(display.width/2)

    self.m_pPanelClose = self.m_pathUI:getChildByName("Panel_1")
    self.m_pBtnWeChat = self.m_pNodeRoot:getChildByName("Button_1")
    self.m_pBtnPosList[1] = cc.p(self.m_pBtnWeChat:getPosition())
    self.m_pBtnWeChat:hide()
    self.m_pBtnAliPay = self.m_pNodeRoot:getChildByName("Button_2")
    self.m_pBtnPosList[2] = cc.p(self.m_pBtnAliPay:getPosition())
    self.m_pBtnAliPay:hide()
    self.m_pBtnBank = self.m_pNodeRoot:getChildByName("Button_3")
    self.m_pBtnPosList[3] = cc.p(self.m_pBtnBank:getPosition())
    self.m_pBtnBank:hide()
    self.m_pBtnYSF = self.m_pNodeRoot:getChildByName("Button_4")
    self.m_pBtnPosList[4] = cc.p(self.m_pBtnYSF:getPosition())
    self.m_pBtnYSF:hide()

    self.m_pBtnClose = self.m_pNodeRoot:getChildByName("Button_close")
  
end

function YueCardPayView:initBtnEvent()
	self.m_pBtnClose:addClickEventListener(handler(self,self.onCloseClicked))
	self.m_pPanelClose:addClickEventListener(handler(self,self.onCloseClicked))
	self.m_pBtnWeChat:addClickEventListener(handler(self,self.onWeChatClicked))
	self.m_pBtnAliPay:addClickEventListener(handler(self,self.onAliPayClicked))
	self.m_pBtnBank:addClickEventListener(handler(self,self.onBankClicked))
	self.m_pBtnYSF:addClickEventListener(handler(self,self.onYSFClicked))
end

function YueCardPayView:onEnter()
	self.super:onEnter()
	self:setTargetShowHideStyle(self,FixLayer.SHOW_DLG_BIG,FixLayer.HIDE_DLG_BIG)
	self:showWithStyle()
end

function YueCardPayView:onExit()
	self.super:onExit()
end

function YueCardPayView:onCloseClicked()
	AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
	self:onMoveExitView()
end

--微信支付
function YueCardPayView:onWeChatClicked()
	if LuaNativeBridge:getInstance():isWXInstall() then
		self:goPay(G_CONSTANTS.Recharge_Type.Type_WeChat,0)
	else
		FloatMessage.getInstance():pushMessage("STRING_046_1")
	end
end

--支付宝支付
function YueCardPayView:onAliPayClicked()
	if LuaNativeBridge:getInstance():isAlipayInstall() then
		self:goPay(G_CONSTANTS.Recharge_Type.Type_Alipay,0)
	else
		FloatMessage.getInstance():pushMessage("STRING_150")
	end
end

--银行卡支付
function YueCardPayView:onBankClicked()
	self:goPay(G_CONSTANTS.Recharge_Type.Type_Bank,0)
end

function YueCardPayView:onYSFClicked()
	self:goPay(G_CONSTANTS.Recharge_Type.Type_YunShanFu,0)
end

function YueCardPayView:goPay(pay_type,userGiftID)
	CRechargeManager:getInstance():CreatedOrder2(pay_type, self.m_pMoney, userGiftID, "",self.m_pProduct)
	self:onMoveExitView()
end

function YueCardPayView:setPayData(pay_money,pay_product)
	self.m_pMoney = pay_money
	self.m_pProduct = pay_product

	--判断一下当前这种金额那些充值类型可以
	local isShowAliPay = CRechargeManager.getInstance():judgeChargeByMoney(1,pay_money)
	local isShowWeChat = CRechargeManager.getInstance():judgeChargeByMoney(2,pay_money)
	local isShowBank = CRechargeManager.getInstance():judgeChargeByMoney(4,pay_money)
	local isShowYSF = CRechargeManager.getInstance():judgeChargeByMoney(10,pay_money)
	
	local temp_index = 0
	if isShowWeChat then
		self.m_pBtnWeChat:show()
		temp_index = temp_index + 1
	end

	if isShowAliPay then
		self.m_pBtnAliPay:show()
		self.m_pBtnAliPay:move(self.m_pBtnPosList[temp_index+1])
		temp_index = temp_index + 1
	end

	if isShowBank then
		self.m_pBtnBank:show()
		self.m_pBtnBank:move(self.m_pBtnPosList[temp_index+1])
		temp_index = temp_index + 1
	end

	if isShowYSF then
		self.m_pBtnYSF:show()
		self.m_pBtnYSF:move(self.m_pBtnPosList[temp_index+1])
		temp_index = temp_index + 1
	end

end

return YueCardPayView