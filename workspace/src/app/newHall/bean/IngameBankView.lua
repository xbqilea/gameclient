--region *.lua
--Date
--Desc 游戏内银行
--endregion

local FloatMessage      = require("common.layer.FloatMessage")
--local CommonGoldView    = require("hall.bean.CommonGoldView")
local GameListConfig    = require("common.config.GameList")    --等级场配置

local MAX_PERCENT = 10

local IngameBankView = class("IngameBankView", FixLayer)

function IngameBankView:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()

    self.m_llInputScore = 0
    self.show_type = 0 -- 0 刻度条为图片 1 刻度条不为图片
    self.m_strPwd = ""
    self.m_nEditBoxTouchCount = 0
end

function IngameBankView:init(initData)
    self:initCSB(initData)
    self:initEvent()
    self:initBtnEvent()
    self:initEditBox()
    self:initNode()
end

function IngameBankView:clean()
    self:cleanEvent()
end

function IngameBankView:initCSB(initData)
    local strPath = "hall/csb/BankInGame.csb"
    if initData and type(initData) == "table" then
        strPath = initData.strPath
        self.show_type = initData.show_type
    elseif initData then
        strPath = initData
    end

    -- root
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    -- csb
    self.m_pathUI = cc.CSLoader:createNode(strPath)
    self.m_pathUI:addTo(self.m_rootUI)

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("Panel_1")

    local Node_0 = self.m_pNodeRoot:getChildByName("Node_0")
    local Node_1 = self.m_pNodeRoot:getChildByName("Node_1")
    self.Node_2 = self.m_pNodeRoot:getChildByName("Node_2")
    self.Node_3 = self.m_pNodeRoot:getChildByName("Node_3")
    local Node_4 = self.m_pNodeRoot:getChildByName("Node_4")
    local Node_5 = self.m_pNodeRoot:getChildByName("Node_5")

    --node 0
    self.m_pBtnClose = Node_0:getChildByName("BTN_close")

    --node 1
    self.m_pLabelTake = Node_1:getChildByName("BitmapFontLabel_1")
    self.m_pLabelSave = Node_1:getChildByName("BitmapFontLabel_2")

    --node 2
    self.m_pLabelInput = self.Node_2:getChildByName("node_input")
    self.m_pBtnGold = self.Node_2:getChildByName("Button_1")
    self.m_pLabelCHValue = self.Node_2:getChildByName("Text_3")
    self.m_pLabelPlaceholderColor = self.Node_2:getChildByName("Node_100")

    --node 3
    self.m_pLabelInput2 = self.Node_3:getChildByName("node_input") --密码输入框

    --node 4
    self.m_pSliderGold = Node_4:getChildByName("Slider_1")
    self.m_pBtnMax = Node_4:getChildByName("Button_2")

    Node_4:getChildByName("Sprite_31"):setVisible(self.show_type == 0 and true or false)

    if self.show_type == 1  then
        self.m_pImageCurrent = Node_4:getChildByName("Image_select")
        self.m_vecPoint, self.m_vecPercent, self.m_vecPercentFnt = {}, {}, {}
        for i=1,11 do
            self.m_vecPoint[i-1] = Node_4:getChildByName("Image_" .. i)
            self.m_vecPercent[i-1] = Node_4:getChildByName("Text_" .. i)
            self.m_vecPercentFnt[i-1] = Node_4:getChildByName("BitmapFontLabel_" .. i)
        end
    end

    --node 5
    self.m_pBtnReset    = Node_5:getChildByName("Button_3")
    self.m_pBtnConfirm  = Node_5:getChildByName("Button_4")

    --不显示密码输入框
    local strPwd = PlayerInfo.getInstance():getInsurePass()
    if strPwd ~= "" then 
        if self.Node_3:isVisible() then
            self.Node_2:setPositionY(self.Node_2:getPositionY()-36)
        end
        self.Node_3:setVisible(false)
    end

    --空白处
    self.touchLayer = self.m_pNodeRoot:getChildByName("Panel_close")
        or self.m_pathUI:getChildByName("Panel_close")
    if self.touchLayer then
        self.touchLayer:addClickEventListener(handler(self, self.onCloseClicked))
    end
end

function IngameBankView:onEnter()
    self.super:onEnter()

    if device.platform == "windows" then
        self:initKeyboard()  
    end

    --打开动画
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_BIG, FixLayer.HIDE_DLG_BIG)
    self:showWithStyle()

    if GameListManager.getInstance():getIsLoginGameSucFlag() then
        CMsgGame:sendBankQueryInsureInfo()
    else
        CMsgHall:sendQueryInsureInfoLite()
    end
end

function IngameBankView:onExit()

    self:clean()
    self.super:onExit()
    if device.platform == "windows" then
        self:cleanKeyboard()
    end
end

--监听按键
function IngameBankView:initKeyboard() 
    local function onKeyReleased(keyCode, event)
        event:stopPropagation()
        if keyCode == cc.KeyCode.KEY_ENTER or keyCode == cc.KeyCode.KEY_KP_ENTER then 
            if not self.m_pBtnConfirm:isEnabled() then 
                FloatMessage.getInstance():pushMessage("BANK_30")
            else
                self:onSureClicked()
            end
        elseif keyCode == cc.KeyCode.KEY_TAB then
            if not self.Node_3:isVisible() then
                self.m_pEditBox:touchDownAction(self.m_pEditBox,ccui.TouchEventType.ended);
            else
                self:tabToNext()
            end
        end
    end

    local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
    self.m_pKeyboardListener = cc.EventListenerKeyboard:create()
    self.m_pKeyboardListener:registerScriptHandler(onKeyReleased, cc.Handler.EVENT_KEYBOARD_RELEASED)
    pEventDispatcher:addEventListenerWithFixedPriority(self.m_pKeyboardListener, -129)
end

--移除按键监听
function IngameBankView:cleanKeyboard()

    if self.m_pKeyboardListener then
        local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
        pEventDispatcher:removeEventListener(self.m_pKeyboardListener)
        self.m_pKeyboardListener = nil
    end
end

function IngameBankView:tabToNext()
    
    if not self.Node_3:isVisible() then
        return
    end
    local index = self.m_nEditBoxTouchCount+1 > 2 and 1 or self.m_nEditBoxTouchCount+1
    if self.m_nEditBoxTouchCount > 0 then 
        self.m_pEditBoxAll[self.m_nEditBoxTouchCount]:touchDownAction(self.m_pEditBoxAll[self.m_nEditBoxTouchCount],ccui.TouchEventType.canceled);
    end
    self.m_pEditBoxAll[index]:touchDownAction(self.m_pEditBoxAll[index],ccui.TouchEventType.ended);
    self.m_nEditBoxTouchCount = index
end
-----------------

function IngameBankView:initEvent()
    --SLFacade:addCustomEventListener(Public_Events.MSG_BANK_INFO, handler(self, self.onMsgBankInfo), self.__cname)
    SLFacade:addCustomEventListener(Public_Events.MSG_BANK_OPERATE_SUCCESS, handler(self, self.onMsgBankSuccess), self.__cname)
    SLFacade:addCustomEventListener(Public_Events.MSG_BANK_OPERATE_FAILED, handler(self, self.onMsgBankFailed), self.__cname)
    SLFacade:addCustomEventListener(Public_Events.MSG_HIDE_INGAMEBANK, handler(self, self.onCloseClicked), self.__cname)

    SLFacade:addCustomEventListener(Public_Events.MSG_BANK_STATUS, handler(self, self.onMsgBankStatus), self.__cname)
end

function IngameBankView:cleanEvent()
    --SLFacade:removeCustomEventListener(Public_Events.MSG_BANK_INFO, self.__cname)
    SLFacade:removeCustomEventListener(Public_Events.MSG_BANK_OPERATE_SUCCESS, self.__cname)
    SLFacade:removeCustomEventListener(Public_Events.MSG_BANK_OPERATE_FAILED, self.__cname)
    SLFacade:removeCustomEventListener(Public_Events.MSG_HIDE_INGAMEBANK, self.__cname) 
    SLFacade:removeCustomEventListener(Public_Events.MSG_BANK_STATUS, self.__cname) 
end

function IngameBankView:initBtnEvent()

    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnConfirm:addClickEventListener(handler(self, self.onSureClicked))
    self.m_pBtnReset:addClickEventListener(handler(self, self.onCleanClicked))
    self.m_pBtnMax:addClickEventListener(handler(self, self.onMaxClicked))
    self.m_pBtnGold:addClickEventListener(handler(self, self.onAddGoldClicked))
    self.m_pSliderGold:addEventListener(handler(self, self.onSliderClicked))
end


function IngameBankView:initEditBox()

    local str = 
    { 
        [1] = LuaUtils.getLocalString("BANK_15"),  --1/输入取出金额
        [2] = LuaUtils.getLocalString("BANK_16"),  --1/输入取出金额
    }
    local default_length = 15
    local input_return   = cc.KEYBOARD_RETURNTYPE_DONE
    local input_mode     = cc.EDITBOX_INPUT_MODE_NUMERIC
    local input_flag     = cc.EDITBOX_INPUT_FLAG_SENSITIVE
    local default_tag    = 1
    local default_size   = self.m_pLabelInput:getContentSize()
    local default_string = str[1]

    if self.m_pEditBox == nil then
        self.m_pEditBox = self:createEditBox(default_length, input_return, input_mode, input_flag, default_tag, default_size, default_string)
        self.m_pEditBox:setText("")
        self.m_pEditBox:setPositionY(5)
        self.m_pEditBox:addTo(self.m_pLabelInput)
    end

    --密码输入框
    default_length = 20
    input_mode     = cc.EDITBOX_INPUT_MODE_SINGLELINE
    input_flag     = cc.EDITBOX_INPUT_FLAG_PASSWORD
    default_tag   = 2
    default_size  = self.m_pLabelInput2:getContentSize()
    default_string = str[2]
    if self.m_pEditBoxPwd == nil then
        self.m_pEditBoxPwd = self:createEditBox(default_length, input_return, input_mode, input_flag, default_tag, default_size, default_string)
        self.m_pEditBoxPwd:setText("")
        self.m_pEditBoxPwd:setPositionY(5)
        self.m_pEditBoxPwd:addTo(self.m_pLabelInput2)
    end

    self.m_pEditBoxAll = { self.m_pEditBox, self.m_pEditBoxPwd }
end

function IngameBankView:createEditBox(maxLength, keyboardReturnType, inputMode, inputFlag, tag, size, placestr)
    local sprite1 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite2 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite3 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    size = cc.size(size.width, 40)
    local editBox = cc.EditBox:create(size, sprite1, sprite2, sprite3)
    editBox:setMaxLength(maxLength)
    editBox:setReturnType(keyboardReturnType)
    editBox:setInputMode(inputMode)
    editBox:setInputFlag(inputFlag)
    editBox:setTag(tag)
    editBox:setFont("Helvetica", 28)
    local color = G_CONSTANTS.INPUT_COLOR
    if PlayerInfo.getInstance():getGameID() == 213 then 
        color = cc.WHITE
    end
    editBox:setFontColor(color)
    editBox:setPlaceHolder(placestr)
    editBox:setPlaceholderFontSize(28)
    color = G_CONSTANTS.PLACE_COLOR
    if self.m_pLabelPlaceholderColor ~= nil then 
        color = self.m_pLabelPlaceholderColor:getColor()
    end
    editBox:setPlaceholderFontColor(color)
    editBox:setAnchorPoint(cc.p(0, 0))
    editBox:setPositionY(5)
    editBox:registerScriptEditBoxHandler(handler(self, self.onEditBoxClicked))
    
   
    return editBox
end


function IngameBankView:initNode()

    self.m_lUsrInsure = PlayerInfo.getInstance():getUserInsure()
    self.m_lUsrScore  = PlayerInfo.getInstance():getUserScore()
    

    local stringScore =  LuaUtils.getFormatGoldAndNumber(self.m_lUsrScore)
    self.m_pLabelTake:setString(stringScore)

    local stringInsure =  LuaUtils.getFormatGoldAndNumber(self.m_lUsrInsure)
    self.m_pLabelSave:setString(stringInsure)

    self.m_pSliderGold:setMaxPercent(MAX_PERCENT)
    self.m_pSliderGold:setPercent(0)

    self.m_pLabelCHValue:setString("")

    --开关f
    local bCloseRecharge = GameListManager.getInstance():getGameSwitch(G_CONSTANTS.GAMESWITCH.RECHARGE)
    if bCloseRecharge then
        self.m_pBtnGold:setVisible(false)
    end

    self:onUpdateImagePercent(0)
    self:onUpdateLabelPercent(0)
end

function IngameBankView:resetAll()

    self.m_llInputScore = 0
    self:onCleanEditBox()
    self:onUpdatePercent()
    self:onUpdateCHValue()
end
-----------------------------------------------------------
--收到数据

--function IngameBankView:onMsgBankInfo()

--    Veil:getInstance():HideVeil()

--    self.m_lUsrScore = PlayerInfo.getInstance():getUserScore()
--    self.m_lUsrInsure = PlayerInfo.getInstance():getUserInsure()

--    self:onUpdateUserScore()
--    self:onUpdateUserInsure()
--end

--取钱成功
function IngameBankView:onMsgBankSuccess()

    Veil:getInstance():HideVeil()
    PlayerInfo.getInstance():setInsurePass(self.m_strPwd)
    --隐藏密码框
    if self.Node_3:isVisible() then
        self.Node_2:setPositionY(self.Node_2:getPositionY()-36)
    end
    self.Node_3:setVisible(false)

    self.m_lUsrScore = PlayerInfo.getInstance():getUserScore()
    self.m_lUsrInsure = PlayerInfo.getInstance():getUserInsure()
    self:onUpdateUserScore()
    self:onUpdateUserInsure()
    self:resetAll()
end

--取钱失败
function IngameBankView:onMsgBankFailed()

    Veil:getInstance():HideVeil()
    self:resetAll()
end
-----------------------------------------------------------

function IngameBankView:takeOut()
    local gold = self.m_llInputScore --tonumber(validStr)

    --请输入金额
    if gold == 0 then
        FloatMessage.getInstance():pushMessageWarning("BANK_17")
        return
    end

    --银行余额不足
    if tonumber(gold) > tonumber(PlayerInfo.getInstance():getUserInsure()) then
        FloatMessage.getInstance():pushMessageWarning("BANK_26")
        return
    end
    
    local strPwd = PlayerInfo.getInstance():getInsurePass()
    if strPwd == "" then 
        --未输入密码
        strPwd = self.m_pEditBoxPwd:getText()
        if strPwd == "" then
            FloatMessage.getInstance():pushMessage("BANK_13")
            return
        end
    end

    --开启遮罩
    Veil:getInstance():ShowVeil(VEIL_LOCK)
   
   self.m_strPwd = strPwd
    --发送请求
    if GameListManager.getInstance():getIsLoginGameSucFlag() then 
        CMsgGame:sendBankTakeScore(gold, strPwd)
    else
        CMsgHall:sendTakeOut(gold, strPwd)
    end
end

function IngameBankView:saveIn()

    local gold = self.m_llInputScore

    --请输入金额
    if gold == 0 then     
        FloatMessage.getInstance():pushMessageWarning("BANK_17")
        return
    end
    
    --钱包余额不足
    if tonumber(gold) > tonumber(PlayerInfo.getInstance():getUserScore()) then
        FloatMessage.getInstance():pushMessageWarning("BANK_23")
        return
    end
    
    --开启遮罩
    Veil:getInstance():ShowVeil(VEIL_LOCK)
    
    --发送请求
    CMsgHall:sendSaveIn(gold)
end

function IngameBankView:getGoldValidStr(gold)
    if not tonumber(gold) then
        FloatMessage.getInstance():pushMessageWarning("BANK_42")
        return ""
    end
    
    local pos = string.find(gold, ".", 1, true)
    if pos then -- 如果有小数点，要判断两个小数点有效数字
        local vecStr = string.split(tostring(gold), ".")
        if #vecStr > 2 then
            FloatMessage.getInstance():pushMessageWarning("BANK_42")
            return ""
        end
        
        if #vecStr[2] > 2 then
            FloatMessage.getInstance():pushMessageWarning("BANK_48")
            return ""
        elseif #vecStr[2] == 2 then
            return "" .. vecStr[1] .. vecStr[2]
        elseif #vecStr[2] == 1 then
            return "" .. vecStr[1] .. vecStr[2] .. "0"
        elseif #vecStr[2] == 0 then
            return "" .. vecStr[1] .. vecStr[2] .. "00"
        end
    else
       return  gold .. "00"
    end

    return gold
end

---------------------------------------------------------------------
--更新界面

function IngameBankView:onUpdateUserScore()
    
    local str = LuaUtils.getFormatGoldAndNumber(self.m_lUsrScore)
    self.m_pLabelTake:setString(str)
end

function IngameBankView:onUpdateUserInsure()
    
    local str = LuaUtils.getFormatGoldAndNumber(self.m_lUsrInsure)
    self.m_pLabelSave:setString(str)
end

function IngameBankView:onMsgBankStatus(_event)
    local _userdata = unpack(_event._userdata)
    if not _userdata then
        return
    end
    local eventID = _userdata.name
    local _key = _userdata.packet

    if not _key then return end
    local nKey = tonumber(_key)
    if nKey == 0 then
        FloatMessage.getInstance():pushMessageWarning("BANK_30")
        if self.m_pBtnConfirm then 
            self.m_pBtnConfirm:setEnabled(false)
        end
    else
        if self.m_pBtnConfirm then
            self.m_pBtnConfirm:setEnabled(true)
        end
    end

    cc.exports.scheduler.performWithDelayGlobal(function()
        cc.exports.Veil:getInstance():HideVeil(VEIL_LOCK)
    end, 1.5)
end
--高亮百分比图片
function IngameBankView:onUpdateImagePercent(percent)
    if self.show_type == 0 then return end

   --灰色小点
   for i = 0, 10 do
       self.m_vecPoint[i]:setVisible(i ~= percent)
   end

   --绿色小点
   local posX, posY = self.m_vecPoint[percent]:getPosition()
   self.m_pImageCurrent:setPositionX(posX)
end

--高亮百分比文字
function IngameBankView:onUpdateLabelPercent(percent)
    if self.show_type == 0 then return end
    local m_nGameKindID = CGameClassifyDataMgr.getInstance():getSelectKindID()
    local GameConfig = GameListConfig[m_nGameKindID]
    local select_color = GameConfig.BANK_SELECT_COLOR or cc.c3b(110,238,83)
    local normal_color = GameConfig.BANK_NORMAL_COLOR or cc.c3b(116,185,255)
    --百分比颜色
    for i = 0, 10 do
        if i == percent then
            if GameConfig.FONT_TTF == 1 then
                self.m_vecPercent[i]:setTextColor(select_color)
            elseif GameConfig.FONT_FNT == 1 then
                self.m_vecPercentFnt[i]:setFntFile(GameConfig.FONT_FNT_FILE_2)
            else
                self.m_vecPercent[i]:setColor(select_color)
            end
       else
            if GameConfig.FONT_TTF == 1 then
                self.m_vecPercent[i]:setTextColor(normal_color)
            elseif GameConfig.FONT_FNT == 1 then
                self.m_vecPercentFnt[i]:setFntFile(GameConfig.FONT_FNT_FILE_1)
            else
                self.m_vecPercent[i]:setColor(normal_color)
            end
       end
   end
end

--更新滑动条进度
function IngameBankView:onUpdateSliderPercent(percent)
    
    self.m_pSliderGold:setPercent(percent)
end

--更新输入框数字
function IngameBankView:onUpdateEditBoxPercent(percent)

    local number = self.m_lUsrInsure 
    if percent == 0 then
        self.m_llInputScore = 0
        self.m_pEditBox:setText("")
    else
        self.m_llInputScore = math.floor(number * percent / MAX_PERCENT)
        local string_number = LuaUtils.getFormatGoldAndNumber(self.m_llInputScore) --string.format("%0.2f",  number * percent / MAX_PERCENT / 100)
        self.m_pEditBox:setText(string_number)
    end
end

--清空输入框数字
function IngameBankView:onCleanEditBox()
    self.m_pEditBox:setText("")
    self.m_pEditBoxPwd:setText("")
end

--更新进度
function IngameBankView:onUpdatePercent()

    local number = self.m_llInputScore 
    local total = self.m_lUsrInsure
    local percent = total > 0 and (number/ total) or 0
    local percent_string = string.format("%.1f", percent)
    local percent_show = tonumber(percent_string * MAX_PERCENT)

    --print(number, percent, percent_string, percent_show)
    --FloatMessage.getInstance():pushMessage(number .. "," .. percent .. "," .. percent_string .. "," .. percent_show)

    if type(percent) ~= "number" or percent < 0 or 10 < percent then
        FloatMessage.getInstance():pushMessage("STRING_212_0") --输入有错误
        self.m_pEditBox:setText("") 
        percent_show = 0
    end

    self:onUpdateImagePercent(percent_show)
    self:onUpdateLabelPercent(percent_show)
    self:onUpdateSliderPercent(percent_show)
end
--更新金额大写中文
function IngameBankView:onUpdateCHValue()

    local strCH = self.m_llInputScore == 0 and "" or LuaUtils.numberToCN(self.m_llInputScore)
    self.m_pLabelCHValue:setString(strCH)
end
-------------------------------

-------------------------------
-- 按纽响应

--输入框
function IngameBankView:onEditBoxClicked(event, pSender)
    if "began" == event then
        AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
        self.touchLayer:setEnabled(false)
        self.m_nEditBoxTouchCount = pSender:getTag()
    elseif event == "changed"
    or     event == "ended"
    then
        if pSender:getTag() == 1 then  --取出金额
            self:editBoxEditingDidEnd(pSender)
        end
    elseif event == "return" then 
        --延迟设为可点击 防止还是响应到关闭界面
        local callback = cc.CallFunc:create(function ()
            self.touchLayer:setEnabled(true)
        end)
        local seq = cc.Sequence:create(cc.DelayTime:create(0.16), callback)
        self:runAction(seq)
    elseif "next" == event then
        self:tabToNext()
    end
end

function IngameBankView:editBoxEditingDidEnd(pSender)

    local str = self.m_pEditBox:getText()
    self.m_llInputScore = math.floor(LuaUtils.ConverNumber(str))
    local maxScore = self.m_lUsrInsure
    if self.m_llInputScore >= maxScore then 
        if maxScore == 0 and self.m_llInputScore ~= 0 then 
            local strTips = "BANK_26"
            FloatMessage.getInstance():pushMessage(strTips)
        end
        self.m_llInputScore = maxScore
    end
    if self.m_llInputScore > 0 then
        local strScore = LuaUtils.getFormatGoldAndNumber(self.m_llInputScore)
        pSender:setText(strScore)
    else
        pSender:setText("")
    end
    self:onUpdatePercent()
    self:onUpdateCHValue()
end

--滑动条
function IngameBankView:onSliderClicked(sender, eventType)

    print(sender, eventType)

    --提示错误
    if eventType == ccui.SliderEventType.slideBallDown then --按下
        if self.m_lUsrInsure == 0 then
            FloatMessage.getInstance():pushMessageWarning("BANK_51") --银行没有余额,无法取款
            return
        end
    end

    --错误返回
    if eventType == ccui.SliderEventType.percentChanged  --拖动
    or eventType == ccui.SliderEventType.slideBallCancel --取消
    or eventType == ccui.SliderEventType.slideBallUp     --抬起
    then
        if self.m_lUsrInsure == 0 then
            self:onUpdateSliderPercent(0)
            return
        end
    end


    --更新界面
    local percent = self.m_pSliderGold:getPercent()
    self:onUpdateImagePercent(percent)
    self:onUpdateLabelPercent(percent)
    self:onUpdateEditBoxPercent(percent)
    self:onUpdateCHValue()
end

--最大
function IngameBankView:onMaxClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    if self.m_lUsrInsure == 0 then
        FloatMessage.getInstance():pushMessageWarning("BANK_51") --银行没有余额,无法取款
        return
    end

    self:onUpdateImagePercent(MAX_PERCENT)
    self:onUpdateLabelPercent(MAX_PERCENT)
    self:onUpdateSliderPercent(MAX_PERCENT)
    self:onUpdateEditBoxPercent(MAX_PERCENT)
    self:onUpdateCHValue()
end

--重置
function IngameBankView:onCleanClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    self:onUpdateImagePercent(0)
    self:onUpdateLabelPercent(0)
    self:onUpdateSliderPercent(0)
    self:onUpdateEditBoxPercent(0)
    self:onUpdateCHValue()
end

--确定
function IngameBankView:onSureClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    self:takeOut()
end

-- 充值
function IngameBankView:onAddGoldClicked(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_SHOP)
end

function IngameBankView:onCloseClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    
    self:onMoveExitView()
end
-------------------------------

return IngameBankView
