
local RankCopyView = class("RankCopyView", FixLayer)

RankCopyView.instance_ = nil
function RankCopyView:create()
    RankCopyView.instance_ = RankCopyView.new()
    return RankCopyView.instance_
end

function RankCopyView:ctor()
    self:enableNodeEvents()
    self:init()
end

function RankCopyView:onEnter()
    self.super:onEnter()
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_BIG, FixLayer.HIDE_DLG_BIG)
    self:showWithStyle()
end

function RankCopyView:onExit()

    
end

function RankCopyView:init()
    self:initCCB()
end

function RankCopyView:initCCB()
    
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    self.m_pathUI = cc.CSLoader:createNode("hall/csb/RankCopyDialog.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("RankCopyDialog")
    local diffX = 145-(1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pImgBg       = self.m_pNodeRoot:getChildByName("Image_bg")
    self.m_pBtnNull     = self.m_pathUI:getChildByName("Panel_2") --空白处关闭
    self.m_pBtnClose    = self.m_pImgBg:getChildByName("Button_close")

    --node user
    self.node_user      = self.m_pImgBg:getChildByName("Node_1")
    self.Image_head     = self.node_user:getChildByName("Sprite_1")
    self.Label_name     = self.node_user:getChildByName("Text_1")

    --node cpoy id
    self.node_copy_ID   = self.m_pImgBg:getChildByName("Node_2")
    self.Label_ID       = self.node_copy_ID:getChildByName("Text_1")
    self.BTN_copy_ID    = self.node_copy_ID:getChildByName("Button_1")

    --node copy wx
    self.node_copy_wx   = self.m_pImgBg:getChildByName("Node_3")
    self.Label_wx       = self.node_copy_wx:getChildByName("Text_1")
    self.BTN_copy_wx       = self.node_copy_wx:getChildByName("Button_1")
    self.Label_kk       = self.node_copy_wx:getChildByName("Text_2")
    self.BTN_copy_kk       = self.node_copy_wx:getChildByName("Button_2")

    --node go wx
    self.node_go_wx   = self.m_pImgBg:getChildByName("Node_4")
    self.Label_go_wx   = self.node_go_wx:getChildByName("Text_5")
    self.BTN_go_wx   = self.node_go_wx:getChildByName("Button_4")

    --node go kk
    self.node_go_kk   = self.m_pImgBg:getChildByName("Node_5")
    self.Label_go_kk   = self.node_go_kk:getChildByName("Text_5")
    self.BTN_go_kk   = self.node_go_kk:getChildByName("Button_4")


    self.vecPosition = {}
    self.vecPosition[1] = cc.p(self.Label_wx:getPosition())
    self.vecPosition[2] = cc.p(self.BTN_copy_wx:getPosition())
    self.vecPosition[3] = cc.p(self.Label_kk:getPosition())
    self.vecPosition[4] = cc.p(self.BTN_copy_kk:getPosition())

    --按钮响应
    self.m_pBtnNull:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseClicked))
    self.BTN_copy_ID:addClickEventListener(handler(self, self.onCopyIDClicked))
    self.BTN_copy_wx:addClickEventListener(handler(self, self.onCopyWechatClicked))
    self.BTN_copy_kk:addClickEventListener(handler(self, self.onCopyQQClicked))
    self.BTN_go_wx:addClickEventListener(handler(self, self.onGoWechatClicked))
    self.BTN_go_kk:addClickEventListener(handler(self, self.onGoQQClicked))
end

function RankCopyView:initWithMsg(msg)
    if msg.msgType == "copyID" then

        local bShowGameID = false
        local bShowWechat = false
        local bShowQQ = false
        if type(msg.msgInfo) == "table" then
            if string.len(msg.msgInfo.szQQ) == 0 and string.len(msg.msgInfo.szWeChat) == 0 then
                bShowGameID = true
            else
                bShowGameID = false
                bShowWechat = string.len(msg.msgInfo.szWeChat) > 0
                bShowQQ = string.len(msg.msgInfo.szQQ) > 0
            end
        end
        
        self.node_user:setVisible(true)
        self.node_copy_ID:setVisible(bShowGameID)
        self.node_copy_wx:setVisible(bShowWechat or bShowQQ)
        self.node_go_wx:setVisible(false)
        self.node_go_kk:setVisible(false)

        self.Label_name:setString(msg.msgInfo.szNickName)
        self.Label_ID:setString("ID:" .. msg.msgInfo.dwGameID)
        self.Label_wx:setString("微信号:" .. msg.msgInfo.szWeChat)
        self.Label_kk:setString("QQ:" .. msg.msgInfo.szQQ)

        self.Label_wx:setVisible(bShowWechat)
        self.BTN_copy_wx:setVisible(bShowWechat)    
        self.Label_kk:setVisible(bShowQQ)
        self.BTN_copy_kk:setVisible(bShowQQ)

        local curPosition = 0
        if bShowWechat then
            curPosition = curPosition + 1
            self.Label_wx:setPosition(self.vecPosition[curPosition])
            curPosition = curPosition + 1
            self.BTN_copy_wx:setPosition(self.vecPosition[curPosition])
        end
        if bShowQQ then
            curPosition = curPosition + 1
            self.Label_kk:setPosition(self.vecPosition[curPosition])
            curPosition = curPosition + 1
            self.BTN_copy_kk:setPosition(self.vecPosition[curPosition])
        end

        self.string_gameID = msg.msgInfo.dwGameID
        self.string_wx = msg.msgInfo.szWeChat
        self.string_kk = msg.msgInfo.szQQ
    
    elseif msg.msgType == "goWechat" then
        self.node_user:setVisible(false)
        self.node_copy_ID:setVisible(false)
        self.node_copy_wx:setVisible(false)
        self.node_go_wx:setVisible(true)
        self.node_go_kk:setVisible(false)

        local str = string.format("微信号『%s』已拷贝，是否打开微信？", self.string_wx)
        self.Label_go_wx:setString(str)

    elseif msg.msgType == "goQQ" then
        self.node_user:setVisible(false)
        self.node_copy_ID:setVisible(false)
        self.node_copy_wx:setVisible(false)
        self.node_go_wx:setVisible(false)
        self.node_go_kk:setVisible(true)

        local str = string.format("QQ号『%s』已拷贝，是否打开QQ？", self.string_kk)
        self.Label_go_kk:setString(str)
    end
end

function RankCopyView:onCopyIDClicked() 
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    local self = RankCopyView.instance_

    LuaNativeBridge.getInstance():setCopyContent(self.string_gameID)
    local str = string.format("已复制ID『%s』", self.string_gameID)
    FloatMessage.getInstance():pushMessage(str)
end

function RankCopyView:onCopyWechatClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    local self = RankCopyView.instance_

    LuaNativeBridge.getInstance():setCopyContent(self.string_wx)
    local msg = {
        msgType = "goWechat",
    }
    self:initWithMsg(msg)
end

function RankCopyView:onCopyQQClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    local self = RankCopyView.instance_

    LuaNativeBridge.getInstance():setCopyContent(self.string_kk)
    local msg = {
        msgType = "goQQ",
    }
    self:initWithMsg(msg)
end

function RankCopyView:onGoWechatClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    local self = RankCopyView.instance_

    if device.platform == "windows" then 
        local bOpenWeChat  = Utils:pcOpenWeChat() 
        if bOpenWeChat == false then 
            FloatMessage.getInstance():pushMessage("STRING_046_1")
        end
        return
    end
    local canOpen = LuaNativeBridge:getInstance():isWXInstall()
    if canOpen then
        cc.Application:getInstance():openURL("weixin://")
    else
        FloatMessage.getInstance():pushMessage("STRING_046_1")
    end
end

function RankCopyView:onGoQQClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    local self = RankCopyView.instance_

    if device.platform == "windows" then 
        local bOpenQQ   = Utils:pcOpenQQ() 
        if bOpenQQ == false then 
            FloatMessage.getInstance():pushMessage("STRING_046_6")
        end
        return
    end
    local canOpen = LuaNativeBridge:getInstance():isQQInstall()
    local canOpen2 = LuaNativeBridge:getInstance():isQQInstall2()
    if canOpen then
        cc.Application:getInstance():openURL("mqqwpa://im/chat?chat_type=wpa&uin=".. self.string_kk .."&version=1")
    elseif canOpen2 then
        cc.Application:getInstance():openURL("mqq://im/chat?chat_type=wpa&uin=".. self.string_kk .."&version=1")
    else
        FloatMessage.getInstance():pushMessage("STRING_046_6")
    end
end

function RankCopyView:onCloseClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    local self = RankCopyView.instance_
    self:onMoveExitView()
end

return RankCopyView
