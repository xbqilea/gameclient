--region BankDetailSubDlg.lua
--Date 2018.01.15.
--Auther JackyXu.
--Desc: 银行明细 转帐凭证 dlg.

local BankDetailSubDlg = class("BankDetailSubDlg", FixLayer)

BankDetailSubDlg.instance_ = nil
function BankDetailSubDlg.create()
    BankDetailSubDlg.instance_ = BankDetailSubDlg.new():init()
    return BankDetailSubDlg.instance_
end

function BankDetailSubDlg:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()

    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)
end

function BankDetailSubDlg:init()
    
    --self:setTargetShowHideStyle(self, FixLayer.SHOW_POPUP, FixLayer.HIDE_POPOUT)

    self.m_pathUI = cc.CSLoader:createNode("hall/csb/TransferDetailSubDlg.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("TransferDetailSubDlg")
    local diffX = 145-(1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pImgBg       = self.m_pNodeRoot:getChildByName("IMG_bg")

    self.m_pBtnClose   = self.m_pImgBg:getChildByName("BTN_close")
    self.m_pBtnNull  = self.m_pathUI:getChildByName("Panel_2") --空白处关闭
    self.m_pBtnClose:addClickEventListener(handler(self, self.onReturnClicked))
    self.m_pBtnNull:addClickEventListener(handler(self, self.onReturnClicked))

    self.m_pLbAccount  = self.m_pImgBg:getChildByName("LB_account")
    self.m_pLbScore    = self.m_pImgBg:getChildByName("LB_score")
    self.m_pLbCost     = self.m_pImgBg:getChildByName("LB_cost")
    self.m_pLbRecScore = self.m_pImgBg:getChildByName("LB_rec_score")
    self.m_pLbTrideId  = self.m_pImgBg:getChildByName("LB_rec_trideID")
    self.m_pLbRecUsr   = self.m_pImgBg:getChildByName("LB_rec_user")
    self.m_pLbTime     = self.m_pImgBg:getChildByName("LB_rec_trideTime")

    -- 初始化
    self.m_pLbAccount:setString("")
    self.m_pLbScore:setString("")
    self.m_pLbCost:setString("")
    self.m_pLbRecScore:setString("")
    self.m_pLbTrideId:setString("")
    self.m_pLbRecUsr:setString("")
    self.m_pLbTime:setString("")

    return self
end

function BankDetailSubDlg:onEnter()
    self.super:onEnter()

    --打开动画
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_BIG, FixLayer.HIDE_DLG_BIG)
    self:showWithStyle()

    CMsgHall:sendBankDetails(0)
end

function BankDetailSubDlg:onExit()
    self.super:onExit()

    BankDetailSubDlg.instance_ = nil
end

function BankDetailSubDlg:setDetailInfo(info)
    self.m_pLbAccount:setString(string.format("(ID%d)%s",info.dwSourceGameID, info.szSourceNickName))
    self.m_pLbScore:setString(LuaUtils.getFormatGoldAndNumber(info.lSwapScore))
    self.m_pLbCost:setString(LuaUtils.getFormatGoldAndNumber(info.lRevenue))
    self.m_pLbRecScore:setString(LuaUtils.getFormatGoldAndNumber(info.lSwapScore - info.lRevenue))
    self.m_pLbTrideId:setString(string.format("%.10d",info.iRecordID))
    self.m_pLbRecUsr:setString(string.format("(ID%d)%s",info.dwTargetGameID, info.szTargetNickName))
    local strFmt = LuaUtils.getLocalString("BANK_43") --"%d-%2d-%2d %2d:%2d:%2d"
    self.m_pLbTime:setString(string.format(strFmt, info.time.wYear,info.time.wMonth, info.time.wDay,info.time.wHour,info.time.wMinute,info.time.wSecond))
end

------------------------
-- 按纽响应
-- 返回
function BankDetailSubDlg:onReturnClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    self:onMoveExitView()
end

return BankDetailSubDlg
