--region *.lua
--Date
--此文件由[BabeLua]插件自动生成

--region *.lua
--Date
 local scheduler = require("framework.scheduler")
 local PageViewEx = require("src.app.hall.base.ui.PageViewEx")
local ActivityRecommend = class("ActivityRecommend", function()
    return display.newNode()
end)

local GameListManager   = import("..manager.GameListManager")
local MatchController = require("src/app/hall/MatchGameList/control/MatchController")
local AUTO_SCROLL_TIME = 5

function ActivityRecommend:ctor(pageSize)
    --self:enableNodeEvents()
    self.m_fWidth = pageSize.width
    self.m_fHeight = pageSize.height
    self.m_pItems = {}
    self.m_pNodeCircle = nil

    self:init()
    ToolKit:registDistructor(self, handler(self, self.onDestory))
end

function ActivityRecommend:onDestory()
    self:stopAutoScroll()
end

function ActivityRecommend:onEnter()
    
end

function ActivityRecommend:onExit()
    self:stopAutoScroll()
end

function ActivityRecommend:init()
    --self:setCascadeOpacityEnabled(true)
    self.m_rootUI = display.newNode()
    self.m_rootUI:setPosition(cc.p(0,0))
    self:addChild(self.m_rootUI)
    self.m_rootUI:setCascadeOpacityEnabled(true)
    
    --官网
--    local isClosedWebSite = GameListManager.getInstance():getGameSwitch(G_CONSTANTS.GAMESWITCH.CLOSE_WEBSITE_SHOW)
--    if not isClosedWebSite then
--        self:pushPageLayout(self:getWebsiteLayout())
--    end

    --推荐游戏
--    local recommand = PlayerInfo.getInstance():getRecommandGameKindID()
--    local isGameExist = GameListManager.getInstance():isGameKindExist(recommand)
--    local strPath = string.format("hall/image/file/gui-activity-game%d.png", recommand)
--    local isFileExist =  cc.FileUtils:getInstance():isFileExist(strPath)
--    if isGameExist and isFileExist then
--        self:pushPageLayout(self:getGameLayout())
--    end
    --备份
--    local isCloseBackUp = GameListManager.getInstance():getGameSwitch(G_CONSTANTS.GAMESWITCH.CLOSE_BACKUP)
--    if not isCloseBackUp then
--        if CommonUtils.getInstance():getPlatformType() == G_CONSTANTS.CLIENT_KIND_IOS then
--            self:pushPageLayout(self:getSaveLayout())
--        end
--    end
    -- self:pushPageLayout(self:getStarsLayout())
    self:pushPageLayout(self:getWebsiteLayout())
    self:pushPageLayout(self:getSaveLayout())
    self.m_nTotalActivity = table.nums(self.m_pItems)

--     self.m_pPageView = ccui.PageView:create()
--     self.m_pPageView:setCascadeOpacityEnabled(true)
--   --  self.m_pPageView:setAutoScrollStopEpsilon(0.005)
--     self.m_pPageView:addTo(self.m_rootUI)

    self.m_pPageView = PageViewEx.new()
    self.m_pPageView:addTo(self.m_rootUI)
    self.m_pPageView:setSize(self.m_fWidth, self.m_fHeight)
    -- self.m_pPageView:setScale(0.9)
    self.m_pPageView:setAnchorPoint(cc.p(0,0))
    self.m_pPageView:setPosition(0, 45)
    -- self.m_pPageView:setDot("hall/plist/hall/gui-hall-page-active.png", "hall/plist/hall/gui-hall-page-normal.png", "hall/plist/gui-hall")
    -- self.m_pPageView:setInterval(5)
    -- self.m_pPageView:setDotOffset(0, 0)

    if self.m_nTotalActivity > 1 then
        self.m_pPageView:setTouchEnabled(true)
    else
        self.m_pPageView:setTouchEnabled(false)
    end
    self:initPagePanel()
    -- self:initCirCle()
    -- self.m_pPageView:setSelect(3)
end

function ActivityRecommend:startAutoScroll()
    if self.m_pAutoHandler then
         scheduler.unscheduleGlobal(self.m_pAutoHandler)
        self.m_pAutoHandler = nil
    end
    
    -- self.m_pAutoHandler = scheduler.performWithDelayGlobal(function()
    --     if self.m_pPageView == nil then
    --         return
    --     end
    --     local curIdx = self.m_pPageView:getCurPageIndex()
    --     self.m_pPageView:scrollToPage(curIdx + 1)
    -- end, AUTO_SCROLL_TIME)

    self.m_pAutoHandler = scheduler.scheduleGlobal(function ()
        local index = self.m_pPageView:getNextIndex()
        self.m_pPageView:setSelect(index)
    end, AUTO_SCROLL_TIME)

end

function ActivityRecommend:stopAutoScroll()
    if self.m_pAutoHandler then
        scheduler.unscheduleGlobal(self.m_pAutoHandler)
        self.m_pAutoHandler = nil
    end
end

function ActivityRecommend:scrollToNext()
    --滚动到下一个
    local curIdx = self.m_pPageView:getCurrentPageIndex()
    self.m_pPageView:scrollToPage(curIdx + 1)
end

function ActivityRecommend:initPagePanel()
    local pages = table.nums(self.m_pItems) or 0
     -- 监听滑动事件
    -- self.m_pPageView:addEventListener(function(sender, eventType)
    --     if eventType == ccui.PageViewEventType.turning then
    --         self:onPageViewEvent(sender, eventType)
    --     end
    -- end)

    self.m_nTotalPage = pages + 1
    if 1 == pages then
        self:addCustomPageView(1, 0)
    elseif pages >= 2 then
        -- self:addCustomPageView(pages, 0)
        for i = 1, pages do
            self:addCustomPageView(i, i)
        end
        -- self:addCustomPageView(1, pages + 1)
      --  self.m_pPageView:jumpToPercentHorizontal(1/self.m_nTotalPage*100)
        self:startAutoScroll()
    end
end

-- @realIndex:     该页显示的内容索引(数组的索引1 ~ pages)
-- @pageIndex:     插入位置（pageView的页码索引）
function ActivityRecommend:addCustomPageView(realIndex, pageIndex)
	local layout = self.m_pItems[realIndex]:clone()
    layout:setCascadeOpacityEnabled(true)
    --self.m_pPageView:insertPage(layout, pageIndex)
    self.m_pPageView:addItem(layout)
end

function ActivityRecommend:pushPageLayout(layout)
    layout:retain()
    table.insert(self.m_pItems, layout)
end

function ActivityRecommend:onPageViewEvent(sender, eventType)
    local curPage = self.m_pPageView:getCurPageIndex()
    --print("========onPageViewEvent=======  curPage = %d",curPage)
    function ActionDelay(_delegate, call, time)
    
        local delay = cc.DelayTime:create(time or 0.3)
        local callback = cc.CallFunc:create(handler(_delegate, call))
        local seq = cc.Sequence:create(delay, callback)
        _delegate:runAction(seq)
    end
    local realIndex = curPage
    if eventType == ccui.PageViewEventType.turning then
        --滑到最后一个移动到第2个
        if curPage == self.m_nTotalPage then
            realIndex = 1
            ActionDelay(self.m_pPageView, function()
                self.m_pPageView:scrollToPage(1)
                self:startAutoScroll()
            end, 0.02) 
        elseif curPage == 0 then --滑动到第一个移动到倒数第二个
            realIndex = self.m_nTotalActivity
            ActionDelay(self.m_pPageView, function()
                self.m_pPageView:scrollToPage(self.m_nTotalPage - 1)
                self:startAutoScroll()
            end, 0.02) 
        else
            self:startAutoScroll()
        end
        --滑动后下面小圆点的状态刷新
        self:updateCirCle(realIndex)
    end
end

--推荐展示-官网
function ActivityRecommend:getWebsiteLayout(layout) 
    local layout=ccui.Layout:create()
    layout:setContentSize(self.m_fWidth, self.m_fHeight)
    layout:setPosition(0,0)
    layout:setTouchEnabled(true)
    layout:addTouchEventListener(function(sender, eventType)
        -- if eventType == ccui.TouchEventType.began or
        --    eventType == ccui.TouchEventType.moved then
        --     -- self:stopAutoScroll()
        -- elseif eventType == ccui.TouchEventType.ended then

        if eventType == ccui.TouchEventType.began then
            layout.m_beganPoint = cc.p(sender:getTouchBeganPosition())
        elseif eventType == ccui.TouchEventType.moved then
            -- self:stopAutoScroll()
        elseif eventType == ccui.TouchEventType.ended then
            layout.m_endPoint = cc.p(sender:getTouchEndPosition())

            if math.abs(layout.m_endPoint.x - layout.m_beganPoint.x) > 10 or math.abs(layout.m_endPoint.y - layout.m_beganPoint.y) > 10 then
                return
            end
            g_AudioPlayer:playEffect("hall/sound/sound-hall-selected.mp3")
            TOAST("官网地址已复制成功，欢迎推荐给您的好友")
             
            ToolKit:setClipboardText("www.983750.com/");
        end
    end )  
    --bg
    local image = ccui.ImageView:create("hall/image/file/gui-activity-website.png", ccui.TextureResType.localType)
    image:setCascadeOpacityEnabled(true)
    image:setAnchorPoint(cc.p(0.5,0))
    image:setPosition(cc.p(self.m_fWidth/2 + 5, -5))
    layout:addChild(image)
    -- 官网
    local lbWebsite = ccui.Text:create("www.983750.com/", FONT_TTF_PATH, 26)
    lbWebsite:setPosition(cc.p(172, 30))
    lbWebsite:setAnchorPoint(cc.p(0.5, 0))
    lbWebsite:setScale(0.95)
    lbWebsite:setColor(cc.c3b(0xFF,0xFF,0xFF))
    image:addChild(lbWebsite)

    return layout
end

--推荐展示-推荐游戏
--《九线拉王》、《寻龙夺宝》、《龙虎斗》、《抢庄牛牛》
function ActivityRecommend:getGameLayout(layout)
    local recommand = PlayerInfo.getInstance():getRecommandGameKindID()
    local layout=ccui.Layout:create()
    layout:setCascadeOpacityEnabled(true)
    layout:setContentSize(self.m_fWidth, self.m_fHeight)
    layout:setPosition(0,0)
    layout:setTouchEnabled(true)
    layout:addTouchEventListener(function(sender, eventType)
        if eventType == ccui.TouchEventType.began or
           eventType == ccui.TouchEventType.moved then
            -- self:stopAutoScroll()
        elseif eventType == ccui.TouchEventType.ended or
           eventType == ccui.TouchEventType.canceled then
            -- self:startAutoScroll()
        end
    end ) 
    layout:addClickEventListener(function()
        AudioManager.getInstance():playSound("hall/sound/sound-hall-selected.mp3")
        local _event = {
            name = Hall_Events.ENTER_RECOMMEND_GAME,
            packet = {gamekind = recommand},
        }
        SLFacade:dispatchCustomEvent(Hall_Events.ENTER_RECOMMEND_GAME, _event)
    end) 
    --bg
    local strPath = string.format("hall/image/file/gui-activity-game%d.png", recommand)
    local image = ccui.ImageView:create(strPath, ccui.TextureResType.localType)
    image:setCascadeOpacityEnabled(true)
    image:setAnchorPoint(cc.p(0.5,0.5))
    image:setPosition(cc.p(self.m_fWidth/2, self.m_fHeight/2))
    layout:addChild(image)
    --需要给推荐游戏加上new hot
    local info = GameListManager.getInstance():getGameListInfo(recommand)
    if info and info.dwStatus > 0 then
        local pLogo = nil
        if info.dwStatus == 1 then
            pLogo = ccui.ImageView:create("hall/plist/hall/gui-logo-new.png", ccui.TextureResType.plistType)
        elseif info.dwStatus == 2 then
            pLogo = ccui.ImageView:create("hall/plist/hall/gui-logo-hot.png", ccui.TextureResType.plistType)
        else
            pLogo = ccui.ImageView:create("hall/plist/hall/gui-logo-new.png", ccui.TextureResType.plistType)
        end
        pLogo:setPosition(cc.p(85, 453))
        pLogo:addTo(image)
        pLogo:setCascadeOpacityEnabled(true)

--        if pLogo then
--            local manager = ccs.ArmatureDataManager:getInstance()
--            manager:addArmatureFileInfo("hall/effect/shaoguangAnimation/shaoguangAnimation.ExportJson")    
--            local pArmature = ccs.Armature:create("shaoguangAnimation")
--            if pArmature then
--                pArmature:getAnimation():play("Animation1", -1, 1)
--                --pArmature:setAnchorPoint(0.5, 0.5)
--                pArmature:setPosition(cc.p(40, 40))
--                pArmature:addTo(pLogo)
--            end
--        end
    end

    return layout
end

--推荐展示-官网备份
function ActivityRecommend:getSaveLayout(layout)
    local layout=ccui.Layout:create()
    layout:setContentSize(self.m_fWidth, self.m_fHeight)
    layout:setPosition(0,0)
    layout:setTouchEnabled(true)
    layout:addTouchEventListener(function(sender, eventType)
        if eventType == ccui.TouchEventType.began or
           eventType == ccui.TouchEventType.moved then
            -- self:stopAutoScroll()
        elseif eventType == ccui.TouchEventType.ended or
           eventType == ccui.TouchEventType.canceled then
            -- self:startAutoScroll()
        end
    end ) 
--    layout:addClickEventListener(function()
--         local backupPath = CommonUtils.getInstance():getBackUpMobileConfigURL()
--         if backupPath ~= nil and backupPath ~= "" then
--            LuaNativeBridge:getInstance():openURL(backupPath)
--         end
--    end) 
    --bg
    local image = ccui.ImageView:create("hall/image/file/gui-activity-save.png", ccui.TextureResType.localType)
    image:setCascadeOpacityEnabled(true)
    image:setAnchorPoint(cc.p(0.5,0))
    image:setPosition(cc.p(self.m_fWidth/2 + 5, -5))
    layout:addChild(image)

    return layout
end

--五星宏辉
function ActivityRecommend:getStarsLayout(layout)
    local layout=ccui.Layout:create()
    layout:setContentSize(self.m_fWidth, self.m_fHeight)
    layout:setPosition(0,0)
    layout:setTouchEnabled(true)
    layout:addTouchEventListener(function(sender, eventType)

        if eventType == ccui.TouchEventType.began then
            layout.m_beganPoint = cc.p(sender:getTouchBeganPosition())
        elseif eventType == ccui.TouchEventType.moved then
            -- self:stopAutoScroll()
        elseif eventType == ccui.TouchEventType.ended then
            layout.m_endPoint = cc.p(sender:getTouchEndPosition())

            if math.abs(layout.m_endPoint.x - layout.m_beganPoint.x) > 10 or math.abs(layout.m_endPoint.y - layout.m_beganPoint.y) > 10 then
                return
            end
            g_AudioPlayer:playEffect("hall/sound/sound-hall-selected.mp3")
            -- local state = MatchController.getInstance():enterGame(141001);
            -- if 2 ~= state then
            --     TOAST("游戏资源下载中，请等待！")
            -- end
            if RoomData:isGameExist(RoomData:getGameByBtnId(13)) then
                if RoomData:isGameUp2Date(RoomData:getGameByBtnId(13)) then -- 游戏已经是最新
                    enterSelectGame(13, nil)
                else
                    -- TOAST("请进入多人栏目，下载“五星宏辉”游戏")
                    sendMsg(Hall_Events.MSG_CLASSIFY_OPEN_VIEW_GAME_CLASSIFY_ARCADE)
                end
            else
                -- TOAST("请进入多人栏目，下载“五星宏辉”游戏")
                sendMsg(Hall_Events.MSG_CLASSIFY_OPEN_VIEW_GAME_CLASSIFY_ARCADE)
            end
        end
    end )
    --bg
    local image = ccui.ImageView:create("hall/image/file/gui-activity-fivestars.png", ccui.TextureResType.localType)
    image:setCascadeOpacityEnabled(true)
    image:setAnchorPoint(cc.p(0.5,0.5))
    image:setPosition(cc.p(self.m_fWidth/2, self.m_fHeight/2))
    layout:addChild(image)

    return layout
end

function ActivityRecommend:initCirCle()
    --滑动点
    self.m_pNodeCircle = display.newNode()
    self.m_pNodeCircle:setCascadeOpacityEnabled(true)
    self.m_pNodeCircle:setPosition(cc.p(self.m_fWidth/2, 20))
    self.m_pNodeCircle:addTo(self.m_rootUI)

    if self.m_nTotalActivity <= 1 then
        return
    end

    local beginX = 0
    if self.m_nTotalActivity % 2 == 0 then --偶数个
        beginX = 15 - 30*self.m_nTotalActivity/2
    else --奇数个
        beginX = - 30*math.floor(self.m_nTotalActivity/2)
    end
    for i = 1, self.m_nTotalActivity  do
        local sp = cc.Sprite:createWithSpriteFrameName("hall/plist/hall/gui-hall-page-normal.png")
        sp:setTag(i)
        sp:setPosition(cc.p( beginX + (i-1)*30, 0))
        sp:setCascadeOpacityEnabled(true)
        self.m_pNodeCircle:addChild(sp)
    end
    self:updateCirCle(1)
end

function ActivityRecommend:updateCirCle(realIndx)
    if self.m_nTotalActivity <= 1 then
        return
    else
        --防止缓存没有图
        local name = { "hall/plist/gui-hall.plist", "hall/plist/gui-hall.png", }
        display.loadSpriteFrames(name[1], name[2])

        for i = 1, self.m_nTotalActivity do
        local sp = self.m_pNodeCircle:getChildByTag(i)
        if realIndx == i then
            sp:setSpriteFrame("hall/plist/hall/gui-hall-page-active.png")
        else
            sp:setSpriteFrame("hall/plist/hall/gui-hall-page-normal.png")
        end
    end
    end
end

return ActivityRecommend


--endregion
