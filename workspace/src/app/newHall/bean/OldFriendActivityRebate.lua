--region OldFriendActivityRebate.lua
--Date 2019.04.02.
--Auther JackyMa.
--Desc: 集字符活动主页面

local UsrMsgManager     = require("common.manager.UsrMsgManager")

local OldFriendActivityRebate = class("OldFriendActivityRebate", cc.Layer)

function OldFriendActivityRebate:ctor()
    self:enableNodeEvents()

    self:init()
end

function OldFriendActivityRebate:onEnter()
    CMsgHall:sendOldPlayerGetData()
end

function OldFriendActivityRebate:onExit()
    self:clean()
end

function OldFriendActivityRebate:init()
    
    self:initCSB()
    self:initEvent()
end

function OldFriendActivityRebate:clean()
    self:cleanEvent()
end

function OldFriendActivityRebate:initCSB()
    
    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)
    self.m_rootUI:setPositionX(-145)
    --csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/OldFriendActivityRebate.csb")
    self.m_pathUI:addTo(self.m_rootUI)

    --node
    self.m_pSpComplete = self.m_pathUI:getChildByName("Sprite_complete")
    self.m_pLbMissionScore = self.m_pathUI:getChildByName("Sprite_3"):getChildByName("BitmapFontLabel_target")
    self.m_pBtnRecharge = self.m_pathUI:getChildByName("Button_recharge")
    self.m_pLoadingBarBg = self.m_pathUI:getChildByName("Sprite_loadingbar")
    self.m_pLoadingBar = self.m_pLoadingBarBg:getChildByName("LoadingBar_mission")
    self.m_pLbProgress = self.m_pLoadingBarBg:getChildByName("Text_progress")
    self.m_pLbMissionTip = self.m_pathUI:getChildByName("Text_3")
    self.m_pLbRechargeScoreMin = self.m_pathUI:getChildByName("BitmapFontLabel_gold")

    --init
    self.m_pSpComplete:setVisible(false)
    self.m_pLbMissionScore:setString("")
    self.m_pLbProgress:setString("")
    self.m_pLoadingBar:setPercent(0)
    self.m_pLbRechargeScoreMin:setString("")

    --initBtnEvent
    self.m_pBtnRecharge:addClickEventListener(handler(self, self.onGoRechargeClicked))
end

function OldFriendActivityRebate:initEvent()
    SLFacade:addCustomEventListener(Hall_Events.MSG_OLDPLAYER_DATA_BACK, handler(self, self.onMsgDataBack), self.__cname)                  
end

function OldFriendActivityRebate:cleanEvent()
    SLFacade:removeCustomEventListener(Hall_Events.MSG_OLDPLAYER_DATA_BACK, self.__cname)
end

function OldFriendActivityRebate:onMsgDataBack()
    local data = UsrMsgManager.getInstance():getOldPlayerMsgData()
    if data.byStatus == 1 and  data.llTodayWinScore > data.llTargetWinScore then 
        --任务完成
        self.m_pSpComplete:setVisible(true)
        self.m_pLbMissionTip:setString("奖励将于次日发放！")
    else
        self.m_pSpComplete:setVisible(false)
    end
    self.m_pLbRechargeScoreMin:setString(LuaUtils.getGoldNumberNounZH(data.llRechargeScoreMin))
    self.m_pLbMissionScore:setString(LuaUtils.getGoldNumberNounZH(data.llTargetWinScore))
    if data.llTodayWinScore > data.llTargetWinScore then 
        self.m_pLbProgress:setString(string.format("%d/%d",data.llTargetWinScore,data.llTargetWinScore))
    else
        self.m_pLbProgress:setString(string.format("%d/%d",data.llTodayWinScore,data.llTargetWinScore))
    end
    local percent = data.llTodayWinScore/data.llTargetWinScore*100
    self.m_pLoadingBar:setPercent(percent)
end

----点击事件----
function OldFriendActivityRebate:onGoRechargeClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    local event = {
        name = Public_Events.MSG_SHOW_SHOP_HALL,
        packet = "open_recharge",
    }
    SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_SHOP_HALL, event)
end

return OldFriendActivityRebate
