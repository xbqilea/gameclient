--region AccountVerifyLayer.lua
--Date 
--
--endregion

-- Desc: 帐号登录 view
-- modified by JackyXu on 2017.05.16.


local FloatMessage = require("common.layer.FloatMessage")

local AccountVerifyLayer = class("AccountVerifyLayer", FixLayer)

function AccountVerifyLayer:ctor(account, password)
    self.super:ctor(self)

    self:enableNodeEvents()

    self.m_strAccount = account
    self.m_strPassword = password

    self.m_nEditBoxTouchCount = 0 -- 输入框点击次数
    self.m_nCountDown = 0

    self:init()
end

function AccountVerifyLayer:init()

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)

    self.m_pathUI = cc.CSLoader:createNode("hall/csb/AccountVerifyDialog.csb")
    self.m_pathUI:addTo(self.m_rootUI)

    self.m_pathUI:setPositionY((display.height - 750) / 2)

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("AccountVerify")
    self.m_pNodeRoot:setPositionX((display.width - 1334) / 2)

    self.m_pImgBg       = self.m_pNodeRoot:getChildByName("Image_bg")
    self.m_pBtnClose    = self.m_pImgBg:getChildByName("BTN_close")
    self.m_pBtnVerify   = self.m_pImgBg:getChildByName("Button_code")
    self.m_pBtnSignin   = self.m_pImgBg:getChildByName("Button_login")
    self.m_pTextCount   = self.m_pImgBg:getChildByName("Text_count")

    local nodeName      =  self.m_pImgBg:getChildByName("Image_input1")
    local nodePassWord  =  self.m_pImgBg:getChildByName("Image_input2")

    self.m_pBtnNull     = self.m_pathUI:getChildByName("Panel_2") --空白处关闭

    --init editbox
    self.m_accountEditBox = self:createEditBox(20,
                                        cc.KEYBOARD_RETURNTYPE_DONE,
                                        cc.EDITBOX_INPUT_MODE_SINGLELINE,
                                        cc.EDITBOX_INPUT_FLAG_SENSITIVE,
                                        1,
                                        nodeName:getContentSize(),
                                        LuaUtils.getLocalString("LOGIN_1"),
                                        G_CONSTANTS.INPUT_COLOR)
    self.m_accountEditBox:setPosition(cc.p(10, 10))
    self.m_accountEditBox:addTo(nodeName, 1)

    self.m_passwordEditBox = self:createEditBox(6,
                                        cc.KEYBOARD_RETURNTYPE_DONE,
                                        cc.EDITBOX_INPUT_MODE_NUMERIC,
                                        cc.EDITBOX_INPUT_FLAG_SENSITIVE,
                                        2,
                                        nodePassWord:getContentSize(),
                                        LuaUtils.getLocalString("REGISTER_4"),
                                        G_CONSTANTS.INPUT_COLOR)
    self.m_passwordEditBox:setPosition(cc.p(10, 10))
    self.m_passwordEditBox:addTo(nodePassWord, 1)

    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnNull:addClickEventListener(handler(self, self.onCloseClicked))
    self.m_pBtnVerify:addClickEventListener(handler(self, self.onVerifyClicked))
    self.m_pBtnSignin:addClickEventListener(handler(self, self.onLoginClicked))

    self.m_pEditBox = { self.m_accountEditBox, self.m_passwordEditBox }

    self.m_accountEditBox:setTouchEnabled(false)

    self.m_pTextCount:setString("")
    self.m_pBtnVerify:setVisible(true)
end

function AccountVerifyLayer:onEnter()
    self.super:onEnter()
    if device.platform == "windows" then
        self:initKeyboard()  
    end
    self.handleTime = scheduler.scheduleGlobal(handler(self, self.updateCountDown), 1)

    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_NORMAL, FixLayer.HIDE_NO_STYLE)
    self:showWithStyle()

    self.m_getMobile = SLFacade:addCustomEventListener(Hall_Events.MSG_GET_MOBILE, handler(self, self.onMsgGetMobile), self.__cname)

    if OtherLoginMgr.getInstance():getOtherLoginType()~=0 then
        --微信绑定手机号需要验证的根据openudid获取手机号
        local mdOpenUdid = PlayerInfo.getInstance():getOtherOpenUDID()
        cc.exports.CMsgHall:sendRequestPhoneNumber(mdOpenUdid, "", 2)
   else
        --根据账号获取绑定手机号
        cc.exports.CMsgHall:sendRequestPhoneNumber(self.m_strAccount, self.m_strPassword, 1)
        cc.exports.Veil:getInstance():ShowVeil(VEIL_LOCK)
    end
end

function AccountVerifyLayer:onExit()

    scheduler.unscheduleGlobal(self.handleTime)

    SLFacade:removeEventListener(self.m_getMobile)

    self:stopAllActions()
    self.super:onExit()
    if device.platform == "windows" then
        self:cleanKeyboard()
    end
end

--监听按键
function AccountVerifyLayer:initKeyboard() 
    local function onKeyReleased(keyCode, event)
        event:stopPropagation()
        if keyCode == cc.KeyCode.KEY_ENTER or keyCode == cc.KeyCode.KEY_KP_ENTER then 
            self:onLoginClicked()
        elseif keyCode == cc.KeyCode.KEY_TAB then
            self:tabToNext()
        end
    end

    local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
    self.m_pKeyboardListener = cc.EventListenerKeyboard:create()
    self.m_pKeyboardListener:registerScriptHandler(onKeyReleased, cc.Handler.EVENT_KEYBOARD_RELEASED)
    pEventDispatcher:addEventListenerWithFixedPriority(self.m_pKeyboardListener, -129)
end

--移除按键监听
function AccountVerifyLayer:cleanKeyboard()

    if self.m_pKeyboardListener then
        local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
        pEventDispatcher:removeEventListener(self.m_pKeyboardListener)
        self.m_pKeyboardListener = nil
    end
end

function AccountVerifyLayer:tabToNext()
    
    local index = self.m_nEditBoxTouchCount+1 > 2 and 1 or self.m_nEditBoxTouchCount+1
    if self.m_nEditBoxTouchCount > 0 then 
        self.m_pEditBox[self.m_nEditBoxTouchCount]:touchDownAction(self.m_pEditBox[self.m_nEditBoxTouchCount],ccui.TouchEventType.canceled);
    end
    self.m_pEditBox[index]:touchDownAction(self.m_pEditBox[index],ccui.TouchEventType.ended);
    self.m_nEditBoxTouchCount = index
end
-----------------

function AccountVerifyLayer:createEditBox(maxLength, keyboardReturnType, inputMode, inputFlag, tag, size, placestr, color)
    local sprite1 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite2 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite3 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    size = cc.size(size.width - 20, 40)
    local editBox = cc.EditBox:create(size, sprite1, sprite2, sprite3)
    editBox:setMaxLength(maxLength)
    editBox:setReturnType(keyboardReturnType)
    editBox:setInputMode(inputMode)
    editBox:setInputFlag(inputFlag)
    editBox:setTag(tag)
    editBox:setPlaceHolder(placestr)
    editBox:setPlaceholderFontName("Helvetica")
    editBox:setPlaceholderFontSize(28)
    editBox:setPlaceholderFontColor(G_CONSTANTS.PLACE_COLOR)
    editBox:setFontSize(28)
    editBox:setFontColor(color)
    editBox:setAnchorPoint(cc.p(0,0))
    editBox:registerScriptEditBoxHandler(handler(self, self.onEditBoxEventHandle))

    return editBox
end

function AccountVerifyLayer:onEditBoxEventHandle(event, pSender)
    if "began" == event then
        AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
        self.m_pBtnNull:setEnabled(false)
        self.m_nEditBoxTouchCount = pSender:getTag()
    elseif "ended" == event then
    elseif "return" == event then
        --延迟设为可点击 防止还是响应到关闭界面
        local callback = cc.CallFunc:create(function ()
            self.m_pBtnNull:setEnabled(true)
        end)
        local seq = cc.Sequence:create(cc.DelayTime:create(0.16), callback)
        self:runAction(seq)
    elseif "changed" == event then

    elseif "next" == event then
        self:tabToNext()
    end
end

function AccountVerifyLayer:onMsgGetMobile(_event)
    
    local _userdata = unpack(_event._userdata)
    if not _userdata then
        return
    end
    local strPhone = _userdata
    self.m_accountEditBox:setText(strPhone)
end

function AccountVerifyLayer:onCloseClicked()
    AudioManager.getInstance():playSound("public/sound/sound-close.mp3")

    -- 防连点 ---------------
    local nCurTime = os.time()
    if self.m_nLastTouchTime and nCurTime - self.m_nLastTouchTime <= 0 then
        return
    end
    self.m_nLastTouchTime = nCurTime
    ------------------------

    --手动关闭时，显示关闭动画
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_NORMAL, FixLayer.HIDE_DLG_NORMAL)
    ------------------------

    self:onMoveExitView()
end

function AccountVerifyLayer:onVerifyClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    -- 防连点 ---------------
    local nCurTime = os.time()
    if self.m_nLastTouchTime and nCurTime - self.m_nLastTouchTime <= 0 then
        return
    end
    self.m_nLastTouchTime = nCurTime
    ------------------------

    --请求验证码
    self:sendRequestPhoneCode()
end

function AccountVerifyLayer:onLoginClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    -- 防连点 ---------------
    local nCurTime = os.time()
    if self.m_nLastTouchTime and nCurTime - self.m_nLastTouchTime <= 0 then
        return
    end
    self.m_nLastTouchTime = nCurTime
    ------------------------

    --带验证码登录
	self:accountLogin()
end

function AccountVerifyLayer:updateCountDown(dt)
    if self.m_nCountDown == 0 then
        self.m_pTextCount:setString("")
        self.m_pBtnVerify:setVisible(true)

    elseif self.m_nCountDown > 0 then
        self.m_nCountDown = self.m_nCountDown - 1

        local strFormat = LuaUtils.getLocalString("REGISTER_22")
        local strTip = string.format(strFormat, self.m_nCountDown)
        self.m_pTextCount:setString(strTip)
        
        self.m_pBtnVerify:setVisible(false)
    end
end

-- 请求手机验证码
function AccountVerifyLayer:sendRequestPhoneCode()
    
    --开始倒计时    
    self.m_nCountDown = 60
    self.m_pBtnVerify:setVisible(false)

    --12 => '登录'(在用)
    local strCellPhone =self.m_accountEditBox:getText()
    if OtherLoginMgr.getInstance():getOtherLoginType()~=0 then 
        local mdOpenUdid = PlayerInfo.getInstance():getOtherOpenUDID()
        CMsgHall:sendRequestPhoneVerifyCode(0, strCellPhone, mdOpenUdid, 12, 2)
    else
        CMsgHall:sendRequestPhoneVerifyCode(0, strCellPhone, self.m_strAccount, 12, 1)
    end
end

--带验证码登录
function AccountVerifyLayer:accountLogin()

    local verifyCode = self.m_passwordEditBox:getText()

    if string.len(verifyCode) == 0 then --请输入验证码
        FloatMessage.getInstance():pushMessageWarning("REGISTER_19")
        return
    end
    if type(tonumber(verifyCode)) ~= "number" then --验证码输入错误
        FloatMessage.getInstance():pushMessageWarning("REGISTER_10")
        return 
    end

    local strPhone =self.m_accountEditBox:getText()
    if OtherLoginMgr.getInstance():getOtherLoginType()~=0 then 
        local mdOpenUdid = PlayerInfo.getInstance():getOtherOpenUDID()
        CMsgHall:sendGuestLogin(3, mdOpenUdid, "", strPhone, verifyCode)
    else
        CMsgHall:sendAccountLogin(self.m_strAccount, self.m_strPassword, 0, strPhone, verifyCode)
    end
end

return AccountVerifyLayer
