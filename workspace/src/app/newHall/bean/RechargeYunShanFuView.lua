--region RechargeYunShanFuView.lua
--Date 2017.04.25.
--Auther JackyXu.
--Desc: 通用充值 view

local CRechargeManager  = cc.exports.CRechargeManager

local RechargeYunShanFuView = class("RechargeYunShanFuView", cc.Layer)

local G_TagRechargeYunShanFuViewMsgEvent = "TagRechargeYunShanFuViewMsgEvent" -- 事件监听 Tag

--local Common_Money = {10,20,30,50,100,300,500} --常用金额

local G_RechargeYunShanFuView = nil
function RechargeYunShanFuView.create()
    G_RechargeYunShanFuView = RechargeYunShanFuView.new():init()
    return G_RechargeYunShanFuView
end

function RechargeYunShanFuView:ctor()
    self:enableNodeEvents()

    self.m_nInputMoney = 0

    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)
    
    self.m_nEditBoxTouchCount = 0 -- 输入框点击次数
end

function RechargeYunShanFuView:init()
    
    --init csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/RechargeYunShanFuView.csb")
    self.m_rootUI:addChild(self.m_pathUI)

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("YunShanFuView")

    self.m_pBtnDownload = self.m_pNodeRoot:getChildByName("Image_5_0"):getChildByName("Button_download")

    self.m_pNodeInputGold = self.m_pNodeRoot:getChildByName("Node_input_gold")
    self.m_pNodeInput   = self.m_pNodeInputGold:getChildByName("IMG_inputBg")
    self.m_pBtnClean    = self.m_pNodeInputGold:getChildByName("BTN_clean")

   self.m_pBtnCommit   = self.m_pNodeRoot:getChildByName("BTN_commit")
   self.m_pBtnProcess   = self.m_pNodeRoot:getChildByName("Button_process")

   self.m_pNodeAdd   = self.m_pNodeRoot:getChildByName("Node_add")
   self.m_vecBtnAdd = {}
   self.m_pLbMoney = {}
   for i = 1, 10 do
       self.m_vecBtnAdd[i] = self.m_pNodeAdd:getChildByName("Button_" .. i)
       self.m_pLbMoney[i]  = ccui.Helper:seekWidgetByName(self.m_vecBtnAdd[i], "BitmapFontLabel")
       self.m_vecBtnAdd[i]:addTouchEventListener(function(sender,eventType)
                if not self.m_vecBtnAdd[i]:isVisible() then return end
                if eventType==ccui.TouchEventType.began then
                    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
                    self.m_vecBtnAdd[i]:setScale(1.05)
                elseif eventType==ccui.TouchEventType.canceled then
                    self.m_vecBtnAdd[i]:setScale(1.0)
                elseif eventType==ccui.TouchEventType.ended then 
                    self.m_vecBtnAdd[i]:setScale(1.0)
                    -- 响应选中
                    self:onClickedAdd(i)
                end
        end)
    end

    -- 按纽响应
    self.m_pBtnClean:addClickEventListener(handler(self, self.onClickedClear))
    self.m_pBtnCommit:addClickEventListener(handler(self, self.onClickedCommit))
    self.m_pBtnDownload:addClickEventListener(handler(self, self.onClickedDownload))
    self.m_pBtnProcess:addClickEventListener(handler(self, self.onClickedProcess))

    return self
end

function RechargeYunShanFuView:onEnter()
    self:initEditBox()
    self:updateSupportMoney()
end

function RechargeYunShanFuView:onExit()
    G_RechargeYunShanFuView = nil
end

function RechargeYunShanFuView:initEditBox()
    if not self.m_pEditBox then
        local editsize = cc.size(self.m_pNodeInput:getContentSize().width-80,self.m_pNodeInput:getContentSize().height)
        self.m_pEditBox = self:createEditBox(5,
                                          cc.KEYBOARD_RETURNTYPE_DONE,
                                          cc.EDITBOX_INPUT_MODE_NUMERIC,
                                          cc.EDITBOX_INPUT_FLAG_SENSITIVE,
                                          0,
                                          editsize,
                                          LuaUtils.getLocalString("RECHARGE_7"))
        self.m_pEditBox:setPosition(cc.p(15, 10))
        self.m_pNodeInput:addChild(self.m_pEditBox)
    end
end

function RechargeYunShanFuView:createEditBox(maxLength, keyboardReturnType, inputMode, inputFlag, tag, size, placestr)
    local sprite1 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite2 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite3 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    size = cc.size(size.width, 40)
    local editBox = cc.EditBox:create(size, sprite1, sprite2, sprite3)    
    editBox:setMaxLength(maxLength);
    editBox:setReturnType(keyboardReturnType);
    editBox:setInputMode(inputMode);
    editBox:setInputFlag(inputFlag);
    editBox:setTag(tag);
    editBox:setPlaceHolder(placestr);
    editBox:setPlaceholderFontName("Helvetica")
    editBox:setPlaceholderFontSize(28)
    editBox:setPlaceholderFontColor(G_CONSTANTS.PLACE_COLOR)
    editBox:setFont("Helvetica", 28)
    editBox:setFontColor(G_CONSTANTS.INPUT_COLOR)
    editBox:setAnchorPoint(cc.p(0, 0))
    editBox:registerScriptEditBoxHandler(handler(self, self.onEditBoxEventHandle))

    return editBox;
end

function RechargeYunShanFuView:onEditBoxEventHandle(event, pSender)
    if "began" == event then
        AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
        SLFacade:dispatchCustomEvent(Public_Events.MSG_NULL_CLOSE_ENABLED,"0")
    elseif "ended" == event then
        local str = pSender:getText()
        if str == "" or tonumber(str) == nil then 
            self.m_nInputMoney = 0
        else
            self.m_nInputMoney = tonumber(str)
        end
    elseif "return" == event then
        --延迟设为可点击 防止还是响应到关闭界面
        local callback = cc.CallFunc:create(function ()
            SLFacade:dispatchCustomEvent(Public_Events.MSG_NULL_CLOSE_ENABLED,"1")
        end)
        local seq = cc.Sequence:create(cc.DelayTime:create(0.16), callback)
        self:runAction(seq)
    elseif "changed" == event then
    end
end

function RechargeYunShanFuView:updateSupportMoney(pSender)
    local index = 0
    for i = 1, 10 do
        local rechargeData,is_default = CRechargeManager.getInstance():getRechargeInfoAtIndex(G_CONSTANTS.Recharge_Type.Type_YunShanFu, i)
        if rechargeData == nil or  rechargeData.dGiftLimit == nil or rechargeData.dGiftLimit == 0 then 
            self.m_vecBtnAdd[i]:setVisible(false)
        else
            local bSupport = CRechargeManager.getInstance():judgeChargeByMoney(G_CONSTANTS.Recharge_Type.Type_YunShanFu,rechargeData.dGiftLimit)
            if bSupport then 
                self.m_vecBtnAdd[i]:setVisible(true)
                self.m_vecBtnAdd[i]:setPosition(cc.p(170*(index%5),-math.floor(index/5)*75))
                index = index+1
                self.m_pLbMoney[i]:setString(tostring(rechargeData.dGiftLimit))
            else
                self.m_vecBtnAdd[i]:setVisible(false)
            end
        end
    end
end
----------------------------------
-- 按纽响应
function RechargeYunShanFuView:onClickedDownload(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    --跳转下载app
    if device.platform == "android" then
        cc.Application:getInstance():openURL("https://youhui.95516.com/hybrid_v3/html/help/download.html")
    elseif device.platform == "ios" then
        cc.Application:getInstance():openURL("https://apps.apple.com/cn/app/id600273928")
    end
end

function RechargeYunShanFuView:onClickedProcess(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    SLFacade:dispatchCustomEvent(Hall_Events.MSG_YUNSANFU_DIALOG)
end

function RechargeYunShanFuView:onClickedAdd(nIndex)

    local rechargeData,is_default = CRechargeManager.getInstance():getRechargeInfoAtIndex(G_CONSTANTS.Recharge_Type.Type_YunShanFu, nIndex)
    if rechargeData == nil or  rechargeData.dGiftLimit == nil or rechargeData.dGiftLimit == 0 then 
        return
    end
    self.m_nInputMoney = self.m_nInputMoney + rechargeData.dGiftLimit
    self.m_pEditBox:setText(tostring(self.m_nInputMoney))
end

function RechargeYunShanFuView:onClickedClear(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    self.m_nInputMoney = 0
    self.m_pEditBox:setText("")
end

function RechargeYunShanFuView:onClickedCommit(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    local money = self.m_nInputMoney
    if money <= 0 then 
        FloatMessage.getInstance():pushMessageWarning("STRING_056")
        return
    end
    --获取当前的充值金额是否可以充值
    local can_charge = CRechargeManager.getInstance():judgeChargeByMoney(G_CONSTANTS.Recharge_Type.Type_YunShanFu,money)
    if not can_charge then
        FloatMessage.getInstance():pushMessageWarning("当前充值金额暂不支持")
        return
    end

    CRechargeManager:getInstance():CreatedOrder2(G_CONSTANTS.Recharge_Type.Type_YunShanFu, money, 0, "")
end

return RechargeYunShanFuView
