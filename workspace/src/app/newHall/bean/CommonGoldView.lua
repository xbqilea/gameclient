--region CommonGoldView.lua
--Date 2017.08.09.
--Auther Goblin.
--Modify by JackyXu on 2018.01.06
--Desc:  大厅通用顶部金币 view

local CommonGoldView = class("CommonGoldView", cc.Node)

function CommonGoldView:ctor(tag)
    self:enableNodeEvents()
    self.tag = tag
    self:init()
end

function CommonGoldView:init()

    --init csb
    self.m_commonGoldUI = cc.CSLoader:createNode("hall/csb/HallCommonGold.csb")
    self:addChild(self.m_commonGoldUI)

    local pNodeRoot         = self.m_commonGoldUI:getChildByName("HallCommonGold")
    self.m_pBtnAdd         = pNodeRoot:getChildByName("node_bank"):getChildByName("BTN_bankAdd")    
    self.m_pBtnAddBig         = pNodeRoot:getChildByName("node_bank"):getChildByName("BTN_bankAddBig")
    self.m_pLbBankMoney     = pNodeRoot:getChildByName("node_bank"):getChildByName("TXT_bankScore")
    self.m_pLbGold          = pNodeRoot:getChildByName("node_gold"):getChildByName("TXT_userScore")
    self.m_pImageGold       = pNodeRoot:getChildByName("node_bank"):getChildByName("IMG_bank_icon_0")
    --self.m_pImageAdd        = self.m_pImageGold:getChildByName("IMG_bank_icon_0")

    self.m_pBtnAdd:addTouchEventListener(handler(self,self.onAddBtnClicked))
    self.m_pBtnAddBig:addClickEventListener(handler(self,self.onAddBtnBigClicked))
    self:onMsgUpdateSocre()
end

function CommonGoldView:setAddBtnEnabled(bIsEnabled)
    bIsEnabled = (bIsEnabled and true or false)
    self.m_pBtnAdd:setVisible(bIsEnabled)
    self.m_pImageGold:setVisible(bIsEnabled)
end

function CommonGoldView:onEnter()
    SLFacade:addCustomEventListener(Public_Events.MSG_BANK_INFO, handler(self, self.onMsgUpdateSocre), self.tag)
    SLFacade:addCustomEventListener(Hall_Events.MSG_EXCHANGE_SUCCESS, handler(self, self.onMsgUpdateSocre), self.tag)
    SLFacade:addCustomEventListener(Public_Events.MSG_UDPATE_USR_SCORE, handler(self, self.onMsgUpdateSocre), self.tag)
    SLFacade:addCustomEventListener(Public_Events.MSG_UPDATE_USER_SCORE, handler(self, self.onMsgUpdateSocre), self.tag)
end

function CommonGoldView:onExit()
    SLFacade:removeCustomEventListener(Public_Events.MSG_BANK_INFO, self.tag)
    SLFacade:removeCustomEventListener(Hall_Events.MSG_EXCHANGE_SUCCESS, self.tag)
    SLFacade:removeCustomEventListener(Public_Events.MSG_UDPATE_USR_SCORE, self.tag)
    SLFacade:removeCustomEventListener(Public_Events.MSG_UPDATE_USER_SCORE, self.tag)
end

-- 点击 "+" 响应
function CommonGoldView:onAddBtnClicked(sender, eventType)
    if eventType == ccui.TouchEventType.began then
        --AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
        
        self.m_pImageGold:setScale(1.1)

    elseif eventType == ccui.TouchEventType.canceled then
        self.m_pImageGold:setScale(1.0)
        
    elseif eventType == ccui.TouchEventType.ended then
        self.m_pImageGold:setScale(1.0)

        local _event = {
            name = Public_Events.MSG_SHOW_BANK,
            packet = nil,
        }
        SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_BANK, _event)
        -- SLFacade:dispatchCustomEvent(Public_Events.MSG_CLOSE_CURRENT_DIALOG)
    end
end

function  CommonGoldView:onAddBtnBigClicked()
        local _event = {
            name = Public_Events.MSG_SHOW_BANK,
            packet = nil,
        }
        SLFacade:dispatchCustomEvent(Public_Events.MSG_SHOW_BANK, _event)
end

-- 更新项部金币
function CommonGoldView:onMsgUpdateSocre()
    --用户金币
    local goldNum = PlayerInfo.getInstance():getUserScore()
    local strGold = LuaUtils.getFormatGoldAndNumber(goldNum)
    self.m_pLbGold:setString(strGold)

    -- 保险箱金币
    goldNum = PlayerInfo.getInstance():getUserInsure()
    self.m_pLbBankMoney:setString(LuaUtils.getFormatGoldAndNumber(goldNum))
end

return CommonGoldView
