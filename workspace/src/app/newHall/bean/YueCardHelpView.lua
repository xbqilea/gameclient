
local YueCardHelpView = class("YueCardHelpView",FixLayer)

function YueCardHelpView:ctor()
	self.super:ctor(self)
	self:enableNodeEvents()
	self:init()
	self:initBtnEvent()
end

function YueCardHelpView:init()
	self.m_pathUI = cc.CSLoader:createNode("hall/csb/HallYueCardHelpView.csb")
    self:addChild(self.m_pathUI)

    local diffY = (display.height - 750)/2
    self.m_pathUI:setPositionY(diffY)
    self.m_pNodeRoot = self.m_pathUI:getChildByName("Image_1")
    self.m_pNodeRoot:setPositionX(display.width/2)

    self.m_pPanelClose = self.m_pathUI:getChildByName("Panel_1")
    self.m_pBtnClose = self.m_pNodeRoot:getChildByName("Button_1")

    local scroll_view = self.m_pNodeRoot:getChildByName("Node_2"):getChildByName("ScrollView_1")
    scroll_view:setScrollBarEnabled(false)
end

function YueCardHelpView:initBtnEvent()
	self.m_pBtnClose:addClickEventListener(handler(self,self.onCloseClicked))
	self.m_pPanelClose:addClickEventListener(handler(self,self.onCloseClicked))
end

function YueCardHelpView:onEnter()
	self.super:onEnter()
	self:setTargetShowHideStyle(self,FixLayer.SHOW_DLG_BIG,FixLayer.HIDE_DLG_BIG)
	self:showWithStyle()
end

function YueCardHelpView:onExit()
	self.super:onExit()
end

function YueCardHelpView:onCloseClicked()
	AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
	self:onMoveExitView()
end

return YueCardHelpView