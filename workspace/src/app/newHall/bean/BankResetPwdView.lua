local BankResetPwdView = class("BankResetPwdView", cc.Layer)

function BankResetPwdView:ctor()
    self:enableNodeEvents()

    self:init()
end

function BankResetPwdView:onEnter()
    if device.platform == "windows" then
        self:initKeyboard()  
    end

end

function BankResetPwdView:onExit()
    self:cleanMsgEvent()
    if device.platform == "windows" then
        self:cleanKeyboard()
    end
end

function BankResetPwdView:init()
	self:initVar()
	self:initCSB()
    self:initMsgEvent()
	self:initBtnEvent()
end

function BankResetPwdView:initVar()
	self.m_nEditBoxTouchCount = 0 --当前选中的输入框
end

function BankResetPwdView:initCSB()
	self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)

	local strPath = "hall/csb/BankResetPswDlg.csb"
    if CommonUtils.getInstance():isPCMode() then
        strPath = "hall/csb/pc-BankResetPswDlg.csb"
    end

    self.m_pathUI = cc.CSLoader:createNode(strPath)
    self.m_pathUI:addTo(self.m_rootUI)

    self.m_pNodeOld = self.m_pathUI:getChildByName("Node_old")
    self.m_pNodeNew = self.m_pathUI:getChildByName("Node_new")
    self.m_pNodeSure = self.m_pathUI:getChildByName("Node_sure")

    self.m_pBtnSure = self.m_pathUI:getChildByName("Node_other"):getChildByName("Button_sure")

    local diffY = 8
    local old_input = self.m_pNodeOld:getChildByName("Panel_input")
    --创建旧密码输入框
    self.m_pEditOld = self:createEditBox(20,
    									cc.KEYBOARD_RETURNTYPE_DONE,
    									cc.EDITBOX_INPUT_MODE_SINGLELINE,
    									cc.EDITBOX_INPUT_FLAG_PASSWORD,
    									1,
    									old_input:getContentSize(),
    									"请输入旧密码")
    self.m_pEditOld:setText("")
    self.m_pEditOld:move(cc.p(old_input:getPositionX(),old_input:getPositionY()+diffY))
    self.m_pNodeOld:addChild(self.m_pEditOld)

    local new_input = self.m_pNodeNew:getChildByName("Panel_input")
    --创建新密码输入框
    self.m_pEditNew = self:createEditBox(20,
    									cc.KEYBOARD_RETURNTYPE_DONE,
    									cc.EDITBOX_INPUT_MODE_SINGLELINE,
    									cc.EDITBOX_INPUT_FLAG_PASSWORD,
    									2,
    									new_input:getContentSize(),
    									LuaUtils.getLocalString("BANK_1"))
    self.m_pEditNew:setText("")
    self.m_pEditNew:move(cc.p(new_input:getPositionX(),new_input:getPositionY()+diffY))
    self.m_pNodeNew:addChild(self.m_pEditNew)

    local sure_input = self.m_pNodeSure:getChildByName("Panel_input")
    --创建确认密码输入框
    self.m_pEditSure = self:createEditBox(20,
    									cc.KEYBOARD_RETURNTYPE_DONE,
    									cc.EDITBOX_INPUT_MODE_SINGLELINE,
    									cc.EDITBOX_INPUT_FLAG_PASSWORD,
    									3,
    									sure_input:getContentSize(),
    									"再次输入密码")
    self.m_pEditSure:setText("")
    self.m_pEditSure:move(cc.p(sure_input:getPositionX(),sure_input:getPositionY()+diffY))
    self.m_pNodeSure:addChild(self.m_pEditSure)

end

function BankResetPwdView:initMsgEvent()
    SLFacade:addCustomEventListener(Hall_Events.MSG_CHANGE_BANK_PWD, handler(self, self.onMsgChangeBankPwd), self.__cname)
end

function BankResetPwdView:cleanMsgEvent()
    SLFacade:removeCustomEventListener(Hall_Events.MSG_CHANGE_BANK_PWD, self.__cname)
end


function BankResetPwdView:initBtnEvent()
	self.m_pBtnSure:addClickEventListener(handler(self,self.onConfirmClicked))
end

--监听按键
function BankResetPwdView:initKeyboard() 
    local function onKeyReleased(keyCode, event)
        event:stopPropagation()
        if keyCode == cc.KeyCode.KEY_ENTER or keyCode == cc.KeyCode.KEY_KP_ENTER then 
            self:onConfirmClicked()
        elseif keyCode == cc.KeyCode.KEY_TAB then
            self:tabToNext()
        end
    end

    local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
    self.m_pKeyboardListener = cc.EventListenerKeyboard:create()
    self.m_pKeyboardListener:registerScriptHandler(onKeyReleased, cc.Handler.EVENT_KEYBOARD_RELEASED)
    pEventDispatcher:addEventListenerWithFixedPriority(self.m_pKeyboardListener, -129)
end

--移除按键监听
function BankResetPwdView:cleanKeyboard()
    if self.m_pKeyboardListener then
        local pEventDispatcher = cc.Director:getInstance():getEventDispatcher()
        pEventDispatcher:removeEventListener(self.m_pKeyboardListener)
        self.m_pKeyboardListener = nil
    end
end

function BankResetPwdView:createEditBox(maxLength, keyboardReturnType, inputMode, inputFlag, tag, size, placestr)
    local sprite1 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite2 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    local sprite3 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    size = cc.size(size.width, 40)
    local editBox = cc.EditBox:create(size, sprite1, sprite2, sprite3)
    editBox:setMaxLength(maxLength)
    editBox:setReturnType(keyboardReturnType)
    editBox:setInputMode(inputMode)
    editBox:setInputFlag(inputFlag)
    editBox:setTag(tag)
    editBox:setFont("Microsoft Yahei", 28)
    editBox:setFontColor(G_CONSTANTS.INPUT_COLOR)
    editBox:setPlaceHolder(placestr)
    editBox:setPlaceholderFontSize(28)
    editBox:setPlaceholderFontColor(G_CONSTANTS.PLACE_COLOR)
    editBox:setAnchorPoint(cc.p(0, 0))
    -- editBox:setPositionY(5)
    editBox:registerScriptEditBoxHandler(handler(self, self.onEditBoxClicked))
    
    return editBox
end

function BankResetPwdView:onEditBoxClicked(event, pSender)
    if "began" == event then
       AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
       self.m_nEditBoxTouchCount = pSender:getTag()
    elseif "ended" == event then
    elseif "return" == event then
    elseif "changed" == event then
    elseif "next" == event then
        self:tabToNext()
    end
end

function BankResetPwdView:tabToNext()
    local btn_list = {self.m_pEditOld,self.m_pEditNew,self.m_pEditSure}

    if self.m_nEditBoxTouchCount > 0 then
    	btn_list[self.m_nEditBoxTouchCount]:touchDownAction(btn_list[self.m_nEditBoxTouchCount],ccui.TouchEventType.canceled)
    end

    self.m_nEditBoxTouchCount = self.m_nEditBoxTouchCount >= 3 and 0 or self.m_nEditBoxTouchCount

    local index = self.m_nEditBoxTouchCount + 1 
   	btn_list[index]:touchDownAction(btn_list[index],ccui.TouchEventType.ended);
    self.m_nEditBoxTouchCount = index

end

function BankResetPwdView:onConfirmClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    local old_str = self.m_pEditOld:getText()
    local new_str = self.m_pEditNew:getText()
    local sure_str = self.m_pEditSure:getText()

    --密码不能为空
    if old_str == "" or new_str == "" or sure_str == "" then
    	FloatMessage.getInstance():pushMessageWarning("BANK_36")
        return
    end
    
    --密码由6-20位字母+数字组成
    if string.len(new_str) < 6 or string.len(new_str) > 20
    	or string.len(sure_str) < 6 or string.len(sure_str) > 20 then 
        FloatMessage.getInstance():pushMessageWarning("BANK_28")
        return
    end
    
    --设置的新密码两次输入不一致
    if new_str ~= sure_str then
        FloatMessage.getInstance():pushMessageWarning("两次新密码输入不一致")
        return
    end
    
   --密码必须包含字母和数字
   	if not LuaUtils.check_paypassword(new_str) or not  LuaUtils.check_paypassword(sure_str) then
       	FloatMessage.getInstance():pushMessageWarning("新密码必须包含字母和数字")
       	return
   	end

    if old_str == new_str then
        FloatMessage.getInstance():pushMessageWarning("新密码和旧密码不能一样")
        return
    end
    
    local strOldPwdMD5 = SLUtils:MD5(old_str)
    local strNewPwdMD5 = SLUtils:MD5(new_str)

    CMsgHall:sendModifyBankPwd(strNewPwdMD5,strOldPwdMD5)
end

function BankResetPwdView:onMsgChangeBankPwd(_event)
    local user_data =  unpack(_event._userdata)
    if not user_data then return end
    local msg = user_data.packet
    local lResultCode = tonumber(msg.lResultCode)

    if lResultCode == 0 then
        PlayerInfo.getInstance():setInsurePass(self.m_pEditSure:getText())
        PlayerInfo.getInstance():setIsDefaultPass(1)
    end

    self.m_pEditOld:setText("")
    self.m_pEditNew:setText("")
    self.m_pEditSure:setText("")
end

return BankResetPwdView