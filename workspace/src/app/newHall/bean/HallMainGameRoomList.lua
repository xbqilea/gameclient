-- region HallMainGameRoomList.lua
-- Date 2018.01.18
-- Desc: 游戏等级场选择列表 （三级页面）
-- modified by JackyXu

local HallMainGameRoomList = class("HallMainGameRoomList", cc.Node)

local CommonGoldView   = require("hall.bean.CommonGoldView")  --通用金币框
local GameListConfig   = require("common.config.GameList")    --等级场配置

function HallMainGameRoomList:ctor()
    self:enableNodeEvents()

    self.m_nGameKindID = 0
    self.m_vecRoomList = {}
    self.m_vecRoomBtn  = {}
    self.m_nLastTouchTime = nil
    self.m_pEnterAction = nil

    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    self:initCSB()
end

function HallMainGameRoomList:initCSB()
    
    --init csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/HallMainGameRoomList.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("HallMainGameRoomList")
    self.m_pNodeRootUI  = self.m_pNodeRoot:getChildByName("node_rootUI")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRootUI:setPositionX(diffX)

    self.m_pNodeRoot:setSwallowTouches(false)
    self.m_pNodeRootUI:setSwallowTouches(false)

    self.m_pNodeTable  = self.m_pNodeRootUI:getChildByName("node_list")
end

--显示时初始化
function HallMainGameRoomList:init()
    self.m_nGameKindID = CGameClassifyDataMgr.getInstance():getAppStoreGame()

    --游戏房间数据
--    if(self.m_nGameKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_NIUNIU) then
--        local HoxDataMgr = require("game.handredcattle.manager.HoxDataMgr")
--        local bFour = HoxDataMgr.getInstance():getModeType() == 4
--        self.m_vecRoomList = GameListManager.getInstance():getStructRoomByKindIDWithBaseScoreAndMode(self.m_nGameKindID, bFour)
--    else
--        self.m_vecRoomList = GameListManager.getInstance():getStructRoomByKindIDWithOnlyBaseScore(self.m_nGameKindID)
--    end

    self.m_vecRoomList = GameListManager.getInstance():getStructRoomByKindIDWithOnlyBaseScore(self.m_nGameKindID)

    --排序房间
    table.sort(self.m_vecRoomList, function(a,b)
        return a.dwBaseScore < b.dwBaseScore
    end)

    --优化：房间不止一个才更新房间数据-----------------------
    --local bNeedUpdate = false
    --for i, gameServer in pairs(self.m_vecRoomList) do
    --    local rooms = {}
    --    if self.m_nGameKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_NIUNIU  then
    --        local HoxDataMgr = require("game.handredcattle.manager.HoxDataMgr")
    --        local bFour = (HoxDataMgr.getInstance():getModeType() == 4)
    --        rooms = GameListManager.getInstance():getStructRoomByKindIDAndMode(gameServer.wKindID, gameServer.dwBaseScore, bFour)
    --    else
    --        rooms = GameListManager.getInstance():getStructRoomByKindID(gameServer.wKindID, gameServer.dwBaseScore)
    --    end
    --    if table.maxn(rooms) > 1 then
    --        bNeedUpdate = true
    --        break
    --    end
    --end
    --if bNeedUpdate then --刷新房间人数
    --    CMsgHall:sendGetServerList(self.m_nGameKindID)
    --end
    --优化：房间不止一个才更新房间数据-----------------------

    -- 创建等级场列表
    self:createScrollRoomList()
end

function HallMainGameRoomList:onEnter()

    SLFacade:addCustomEventListener(Hall_Events.MSG_GET_ROOM_LIST, handler(self, self.onMsgGetRoomList), self.__cname)
end

function HallMainGameRoomList:onExit()

    SLFacade:removeCustomEventListener(Hall_Events.MSG_GET_ROOM_LIST,self.__cname)
end

function HallMainGameRoomList:onMsgGetRoomList(_event)

    --重新获取游戏房间数据
--    if(self.m_nGameKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_NIUNIU) then
--        local HoxDataMgr = require("game.handredcattle.manager.HoxDataMgr")
--        local bFour = HoxDataMgr.getInstance():getModeType() == 4
--        self.m_vecRoomList = GameListManager.getInstance():getStructRoomByKindIDWithBaseScoreAndMode(self.m_nGameKindID, bFour)
--    else
--        self.m_vecRoomList = GameListManager.getInstance():getStructRoomByKindIDWithOnlyBaseScore(self.m_nGameKindID)
--    end
    self.m_vecRoomList = GameListManager.getInstance():getStructRoomByKindIDWithOnlyBaseScore(self.m_nGameKindID)

    --重新排序
    table.sort(self.m_vecRoomList, function(a,b)
        return a.dwBaseScore < b.dwBaseScore
    end)
end

function HallMainGameRoomList:checkIsHasExperience(nBaseScore)
    for i = 1, #self.m_vecRoomList do
        local tmp = self.m_vecRoomList[i]
        if tmp.dwBaseScore == nBaseScore then
            self.m_nExperienceBaseScore = nBaseScore

            -- 体验场放第一个
            local copyRoom = table.copy(self.m_vecRoomList[i])
            table.remove(self.m_vecRoomList, i)
            table.insert(self.m_vecRoomList, 1, copyRoom)
            break
        end
    end
end

function HallMainGameRoomList:touchListView(sender, _type)
    if ccui.ListViewEventType.ONSELECTEDITEM_END == _type then
        local index = sender:getCurSelectedIndex() + 1
        if index > 1 and self.TouchListItem then
            self:TouchListItem(index-1)
        end
    end
end

function HallMainGameRoomList:TouchListItem(index)
    PlayerInfo.getInstance():setIsQuickStart(false)
    self:loginGame(index)
end

function HallMainGameRoomList:createScrollRoomList()
    if table.maxn(self.m_vecRoomList) == 0 then return end
    if not GameListConfig[self.m_nGameKindID] then return end

    local GameConfig = GameListConfig[self.m_nGameKindID]
    local scrollViewSize = cc.size(1334, 490) -- 滑动区域大小
    local maxWidth = scrollViewSize.width

    local nRoomCount = #self.m_vecRoomList
    local nPageCount = math.floor(nRoomCount / 6)
    if nPageCount > 0 then
        maxWidth = 390 * math.ceil(nRoomCount / 2) + 35;
    end

    self.m_InnerNode = cc.Node:create()
    self.m_InnerNode:setContentSize(cc.size(maxWidth, scrollViewSize.height))

    local STARTX = (nPageCount > 0 and 210 or 275)-- 只显示一屏，则列表更居中
    local nStartX, nStartY = STARTX, 358; -- 起始位置
    local nOffX, nOffY = 390, -235; -- 偏移
    local btnSize = cc.size(384, 234) -- 按纽大小

    local strPath = CGameClassifyDataMgr.getInstance():getLocalRoomListPath(self.m_nGameKindID)

    self.m_vecRoomBtn = {}
    for index = 1, nRoomCount do

        local _btnNode = cc.Node:create()
        _btnNode:setContentSize(btnSize)
        _btnNode:setPosition(nStartX, nStartY)
        _btnNode:setVisible(false)
        self.m_InnerNode:addChild(_btnNode, 1, index)
        self.m_vecRoomBtn[index] = _btnNode

        local roomInfo = self.m_vecRoomList[index]
        local nBaseScore = roomInfo.dwBaseScore

        --按钮
        --local strFileName = "gui-room-bg-icon1.png"
        --if nRoomCount == 4 and index == 4 then -- 美术要求只有4个等级场时，第4个等级场用第5个的图
        --    strFileName = "gui-room-bg-icon5.png"
        --elseif index > 6 then
        --    strFileName = string.format("gui-room-bg-icon%d.png", index - 6)
        --else
        --    strFileName = string.format("gui-room-bg-icon%d.png", index)
        --end
        --local celNodeBg = cc.Sprite:createWithSpriteFrameName(strFileName)
        --修改:使用游戏内的资源
        local frameName = string.format(strPath, self.m_vecRoomList[index].dwBaseScore)
        local celNodeBg = display.newSprite(frameName)
        local pNormalSprite = cc.Scale9Sprite:createWithSpriteFrame(celNodeBg:getSpriteFrame())
        local pClickSprite = cc.Scale9Sprite:createWithSpriteFrame(celNodeBg:getSpriteFrame())
        local pBgBtn = cc.ControlButton:create(pNormalSprite)
        local pBtnSize = pBgBtn:getContentSize()
        -- [[pBgBtn:setZoomOnTouchDown(false)]]
        pBgBtn:setBackgroundSpriteForState(pClickSprite,cc.CONTROL_STATE_HIGH_LIGHTED)
        pBgBtn:setScrollSwallowsTouches(false)
        -- [[pBgBtn:setCheckScissor(true)]]

        pBgBtn:setPosition(cc.p(0,0))
        pBgBtn:setTag(index);
        _btnNode:addChild(pBgBtn);
        -- 添加响应
        local function onButtonClicked(sender)
            AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
            self:TouchListItem(index)
        end
        pBgBtn:registerControlEventHandler(onButtonClicked, cc.CONTROL_EVENTTYPE_TOUCH_UP_INSIDE);

        local bgSize = celNodeBg:getContentSize()

        -- 等级场名称
        local pLbRoomName = cc.Label:createWithBMFont("hall/font/sz_jbdtzt4.fnt","")
        local strName = string.format("场次 %d",index)
        if GameConfig[nBaseScore] and GameConfig[nBaseScore].RoomName then
            strName = GameConfig[nBaseScore].RoomName
        end
        pLbRoomName:setString(strName)
        pLbRoomName:setAnchorPoint(cc.p(0.5, 0.5))
        pLbRoomName:setPosition(cc.p(bgSize.width/2, bgSize.height - 45))
        pBgBtn:addChild(pLbRoomName)

        -- 中央显示入场分数
        local pLbRoomScore = cc.Label:createWithBMFont("hall/font/sz_jbdtzt5.fnt","")
        local strScore = ""
        if GameConfig[nBaseScore] and GameConfig[nBaseScore].centerScore then
            strScore = GameConfig[nBaseScore].centerScore
        end
        pLbRoomScore:setString(strScore)
        pLbRoomScore:setAnchorPoint(cc.p(0.5, 0.5))
        pLbRoomScore:setPosition(cc.p(bgSize.width/2, bgSize.height/2 -15))
        pBgBtn:addChild(pLbRoomScore)

        -- 最小注
        local strLimitScore = ""
        if GameConfig[nBaseScore] and GameConfig[nBaseScore].limitScore then
            strLimitScore = GameConfig[nBaseScore].limitScore
        end
        local lbLimitScore = cc.Label:createWithTTF(strLimitScore, FONT_TTF_PATH, 20)
        lbLimitScore:setAnchorPoint(cc.p(0, 0.5));
        lbLimitScore:setPosition(cc.p(50, 33));
        pBgBtn:addChild(lbLimitScore)

        -- 入场分数
        local strEnterScore = ""
        if GameConfig[nBaseScore] and GameConfig[nBaseScore].enterScore then
            strEnterScore = GameConfig[nBaseScore].enterScore
        end
        local lbEnterScore = cc.Label:createWithTTF(strEnterScore, FONT_TTF_PATH, 20)
        lbEnterScore:setAnchorPoint(cc.p(0, 0.5));
        lbEnterScore:setPosition(cc.p(245, 33));
        pBgBtn:addChild(lbEnterScore)

        -- 下一个按纽的位置
        if nPageCount > 0 then -- 超过一屏
            nStartX = nStartX + nOffX
            if index % 4 == 0 then
                nStartX = STARTX;
                nStartY = nStartY + nOffY;
            end
        else
            nStartX = nStartX + nOffX
            if index % 3 == 0 then
                nStartX = STARTX;
                nStartY = nStartY + nOffY;
            end
        end

        if index == nRoomCount then
            self:runAction(cc.Sequence:create(cc.DelayTime:create(0.05), cc.CallFunc:create(function()
                self:showListLoadAction()
            end)))
        end
    end

    -- 创建滑动节点
    self.m_root_SV = cc.ScrollView:create(scrollViewSize, self.m_InnerNode)
    self.m_root_SV:setDirection(0) -- 0 水平， 1 垂直， 2 都是可以
    self.m_root_SV:setContentSize(cc.size(maxWidth, scrollViewSize.height))
    self.m_root_SV:setContentOffset(cc.p(0, 0))
    self.m_root_SV:setPosition(cc.p(0,0))
    self.m_root_SV:addTo(self.m_pNodeTable)

    self.m_root_SV:setDelegate()
    self.m_root_SV:setClippingToBounds(false)
    local bIsBounceable = (nPageCount > 0 and true or false)
    self.m_root_SV:setBounceable(bIsBounceable) -- 设置回弹效果
end

--列表动态加载效果
function HallMainGameRoomList:showListLoadAction()
    local roomSize = table.maxn(self.m_vecRoomBtn)
    if table.maxn(self.m_vecRoomBtn) == 0 then return end

    local nRoomCount = #self.m_vecRoomList
    local nPageCount = math.floor(nRoomCount / 6)
    
    local nStartX = display.width + 50

    local nStep = (nPageCount > 0 and 4 or 3)
    local fDelayTime = 0.0
    local fMoveTime = 0.15
    local diffX = 45
    for i=1, nStep do
        if not self.m_vecRoomBtn[i] then break end
        print("showListLoadAction", i, #self.m_vecRoomBtn)
        local pos = cc.p(self.m_vecRoomBtn[i]:getPosition())
        self.m_vecRoomBtn[i]:stopAllActions()
        self.m_vecRoomBtn[i]:setPositionX(nStartX)
        self.m_vecRoomBtn[i]:runAction(cc.Sequence:create(
            cc.Show:create(),
            cc.DelayTime:create(fDelayTime),
            cc.EaseIn:create(cc.MoveTo:create(0.2, cc.p(pos.x-diffX,pos.y)), 0.4),
            cc.EaseBackOut:create(cc.MoveTo:create(fMoveTime, pos))))

        if self.m_vecRoomBtn[i+nStep] then
            print("showListLoadAction", i+nStep, #self.m_vecRoomBtn)
            local pos = cc.p(self.m_vecRoomBtn[i+nStep]:getPosition())
            self.m_vecRoomBtn[i+nStep]:stopAllActions()
            self.m_vecRoomBtn[i+nStep]:setPositionX(nStartX) -- 先设置位置在列表显示区域右则
            self.m_vecRoomBtn[i+nStep]:runAction(cc.Sequence:create(
                cc.Show:create(),
                cc.DelayTime:create(fDelayTime),
                cc.EaseIn:create(cc.MoveTo:create(0.2, cc.p(pos.x-diffX,pos.y)), 0.4),
                cc.EaseBackOut:create(cc.MoveTo:create(fMoveTime, pos))))
        end
        print(i, "delay", fDelayTime, "move", fMoveTime, "diffX", diffX)
        fDelayTime = fDelayTime + 0.2
        fMoveTime = fMoveTime + 0.5
        diffX = diffX - 5
    end
end

--快速开始
function HallMainGameRoomList:onQuickStartClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    local roomSize = table.maxn(self.m_vecRoomList)
    if roomSize == 0 then return end

    --上一个游戏是该游戏时
    local sID = PlayerInfo.getInstance():getCurrentServerID()
    if sID > 0 then
        local room = GameListManager.getInstance():getClientGameByServerId(sID)
        if room.wKindID == self.m_nGameKindID then
            local index = 0
            for k, v in pairs(self.m_vecRoomList) do
                if v.wServerID == sID then
                    index = k
                    break
                end
            end
            --快速登录
            if 0 < index and index < table.nums(self.m_vecRoomList) then
                PlayerInfo.getInstance():setIsQuickStart(true)
                self:loginGame(index)
                return
            end
        end
    end

    local index = 0
    --从大往小，默认登录最大分数场
    for i = roomSize, 1, -1 do
        local room = self.m_vecRoomList[i]
        local needScore = (room.dwBaseScore == 1) and 0 or room.dwMinEnterScore
        if PlayerInfo.getInstance():getUserScore() >= needScore then
            index = i
            break
        end
    end

    --如无匹配，默认登录第一个
    index = (index == 0) and 1 or index

    --快速登录
    PlayerInfo.getInstance():setIsQuickStart(true)
    self:loginGame(index)
end

function HallMainGameRoomList:loginGame(_index)

    --房间数据
    local tmp = self.m_vecRoomList[_index]
    local rooms = {}
    local gameServer = GameListManager.getInstance():getSuitAbleServer(self.m_nGameKindID,tmp.dwBaseScore)
--    if self.m_nGameKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_NIUNIU  then
--        local HoxDataMgr = require("game.handredcattle.manager.HoxDataMgr")
--        local bModeFour = HoxDataMgr.getInstance():getModeType() == 4
--        rooms = GameListManager.getInstance():getStructRoomByKindIDAndMode(gameServer.wKindID, gameServer.dwBaseScore, bModeFour)
--    else
--        rooms = GameListManager.getInstance():getStructRoomByKindID(gameServer.wKindID, gameServer.dwBaseScore)
--    end
    rooms = GameListManager.getInstance():getStructRoomByKindID(gameServer.wKindID, gameServer.dwBaseScore)

    --超过一个房间时
    if table.nums(rooms) > 1 then

        --弹出选择房间框
        --self:sendShowChooseRoomDlg(rooms[1])

        --寻找符合条件的房间进入
        local room  =  GameListManager.getInstance():getMatchRoom(rooms)
        self:sendLoginGameMessage(room)
    else
        self:sendLoginGameMessage(tmp)
    end
end

function HallMainGameRoomList:sendLoginGameMessage(tmp)
    
    --放到hallSceneLayer 发送登录游戏请求
    local _event = {
        name = Hall_Events.MSG_HALL_CLICKED_GAME,
        packet = tmp,
    }
    SLFacade:dispatchCustomEvent(Hall_Events.GAMELIST_CLICKED_GAME, _event)
end


return HallMainGameRoomList
