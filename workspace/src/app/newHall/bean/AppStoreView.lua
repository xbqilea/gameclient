--region AppStoreView.lua
--Date 2017.04.25.
--Auther JackyXu.
--Desc: 苹果充值 dialog

local G_TagAppStoreViewMsgEvent = "G_TagAppStoreViewMsgEvent" -- 事件监听 Tag

local AppStoreView = class("AppStoreView", FixLayer)
local CommonUtils = require("common.public.CommonUtils")
local ClientConfig = require("common.public.ClientConfig")
local CommonGoldView = require("hall.bean.CommonGoldView")

AppStoreView.instance_ = nil
function AppStoreView.create()
    AppStoreView.instance_ = AppStoreView.new():init()
    return AppStoreView.instance_
end

function AppStoreView:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()

    self.m_pLbBankMoney = nil

    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)
end

function AppStoreView:init()

    self.m_pathUI = cc.CSLoader:createNode("hall/csb/AppleStoreDialog.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pBtnNull     = self.m_pathUI:getChildByName("Panel_2") --空白处关闭
    self.m_pNodeRoot         = self.m_pathUI:getChildByName("AppleShop")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pImgBg            = self.m_pNodeRoot:getChildByName("IMG_bg")

    self.m_pLbGold = self.m_pImgBg:getChildByName("Node_4"):getChildByName("BitmapFontLabel_1")
    self.m_pLbBankMoney = self.m_pImgBg:getChildByName("Node_5"):getChildByName("BitmapFontLabel_1")

    self.m_vecBtnBuy = {}
    for i=1,5 do
        self.m_vecBtnBuy[i]  = self.m_pImgBg:getChildByName("BTN_score"..i)
        self.m_vecBtnBuy[i]:addClickEventListener(function()
            self:onClickedRecharge(i)
        end)
    end
    self.m_pBtnClose         = self.m_pImgBg:getChildByName("BTN_close")
    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseBtnClicked))
    self.m_pBtnNull:addClickEventListener(handler(self, self.onCloseBtnClicked))

    --title特效
    local pSpine = "hall/effect/325_shangchengjiemiandonghua/325_shangchengjiemiandonghua"
    self.m_pAniTitle = sp.SkeletonAnimation:createWithBinaryFile(pSpine .. ".skel", pSpine .. ".atlas", 1)
    self.m_pAniTitle:setPosition(cc.p(695,368))
    self.m_pNodeRoot:addChild(self.m_pAniTitle)
    self.m_pAniTitle:setAnimation(0, "animation1", true)

    self:onMsgUpdateSocre()

    return self
end

function AppStoreView:onEnter()
    self.super:onEnter()

    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_BIG, FixLayer.HIDE_DLG_BIG)
    self:showWithStyle()

    SLFacade:addCustomEventListener(Public_Events.MSG_APPSTORE_STATUS, handler(self, self.onMsgAppStore),G_TagAppStoreViewMsgEvent)
    SLFacade:addCustomEventListener(Public_Events.MSG_UDPATE_USR_SCORE, handler(self, self.onMsgUpdateSocre), G_TagAppStoreViewMsgEvent)
    SLFacade:addCustomEventListener(Public_Events.MSG_BANK_INFO, handler(self, self.onMsgUpdateSocre), G_TagAppStoreViewMsgEvent)
    ---------------------------------
    ----app store 回调lua
    ---------------------------------
    local eventDispatcher = cc.Director:getInstance():getEventDispatcher()
    local listener1 = cc.EventListenerCustom:create("MSG_ORDER_FINISH", handler(self, self.onMsgOrderFinish))
    eventDispatcher:addEventListenerWithFixedPriority(listener1, 1)
    local listener2 = cc.EventListenerCustom:create("MSG_ORDER_CREATE_FAILED", handler(self, self.onMsgCreateOrderFailed))
    eventDispatcher:addEventListenerWithFixedPriority(listener2, 1)
    local listener3 = cc.EventListenerCustom:create("MSG_ORDER_FAILED", handler(self, self.onMsgOrderFailed))
    eventDispatcher:addEventListenerWithFixedPriority(listener3, 1)
end

function AppStoreView:onExit()
    self.super:onExit()
    AppStoreView.instance_ = nil

    SLFacade:removeCustomEventListener(Public_Events.MSG_APPSTORE_STATUS, G_TagAppStoreViewMsgEvent)
    SLFacade:removeCustomEventListener(Public_Events.MSG_UDPATE_USR_SCORE, G_TagAppStoreViewMsgEvent)
    SLFacade:removeCustomEventListener(Public_Events.MSG_BANK_INFO, G_TagAppStoreViewMsgEvent)

    local eventDispatcher = cc.Director:getInstance():getEventDispatcher()
    eventDispatcher:removeCustomEventListeners("MSG_ORDER_FINISH")
    eventDispatcher:removeCustomEventListeners("MSG_ORDER_FAILED")
    eventDispatcher:removeCustomEventListeners("MSG_ORDER_CREATE_FAILED")
end

function AppStoreView:onMsgOrderFinish()
    cc.exports.scheduler.performWithDelayGlobal(function()
        FloatMessage.getInstance():pushMessage("STRING_047");
        --app store充值成功,刷新界面
        if GameListManager.getInstance():getIsLoginGameSucFlag() then
            CMsgGame:sendBankQueryInsureInfo()
        else
            CMsgHall:sendQueryInsureInfoLite()
        end
    end, 0.5)

    cc.exports.scheduler.performWithDelayGlobal(function()
        cc.exports.Veil:getInstance():HideVeil(VEIL_LOCK)
    end, 1.0)
end

function AppStoreView:onMsgOrderFailed()
    cc.exports.scheduler.performWithDelayGlobal(function()
        FloatMessage.getInstance():pushMessageWarning("STRING_046")
    end, 0.5)
    cc.exports.scheduler.performWithDelayGlobal(function()
        cc.exports.Veil:getInstance():HideVeil(VEIL_LOCK)
    end, 1.0)
end

function AppStoreView:onMsgCreateOrderFailed()
    cc.exports.scheduler.performWithDelayGlobal(function()
        FloatMessage.getInstance():pushMessageWarning("STRING_046_2")
    end, 0.5)
    cc.exports.scheduler.performWithDelayGlobal(function()
        cc.exports.Veil:getInstance():HideVeil(VEIL_LOCK)
    end, 1.0)
end

-- 更新项部金币
function AppStoreView:onMsgUpdateSocre()
    --用户金币
    local goldNum = PlayerInfo.getInstance():getUserScore()
    local strGold = LuaUtils.getFormatGoldAndNumber(goldNum)
    self.m_pLbGold:setString(strGold)

    -- 保险箱金币
    goldNum = PlayerInfo.getInstance():getUserInsure()
    self.m_pLbBankMoney:setString(LuaUtils.getFormatGoldAndNumber(goldNum))
end

-- 下单
-- 6/12/50/188/488
function AppStoreView:onClickedRecharge(nIndex)

    cc.exports.Veil:getInstance():ShowVeil(VEIL_LOCK)

    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    -- body
    local MONEY = {6,12,50,188,488};
    print("AppStore MONEY:", MONEY[nIndex]);

    local productIDs = CommonUtils.getInstance():getAppStoreProductIDs()
    print("AppStore productIDs:", productIDs[nIndex]);
    
    if device.platform == "ios" then
        -- TODO open apple pay.
        local strUrl = ClientConfig:getInstance():getCreateOrderIPV6Url()
        local userID = PlayerInfo.getInstance():getUserID()
        local productNo = productIDs[nIndex]
        local nMoney = MONEY[nIndex]
        local version = CommonUtils.getInstance():formatAppVersion()
        local ditchNumber = CommonUtils.getInstance():getAppChannel()
        local clientKind = CommonUtils.getInstance():getPlatformType()
        local payType = "AppStore"
        local strFormat = "userID=%d&money=%d&couponID=%d&payType=%s&productNo=%s&version=%d&ditchNumber=%d&clientKind=%d"
        local strPostData = string.format(strFormat, userID, nMoney, 0, payType, productNo, version, ditchNumber, clientKind)
        LuaNativeBridge:getInstance():onRechargeForAppStore(strUrl,strPostData)
    end
end

function AppStoreView:onCloseBtnClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    self:onMoveExitView()
end

---------------------------
-- 消息处理
function AppStoreView:onMsgAppStore(_event)
    local statusString = unpack(_event._userdata)
    if not statusString then
        return
    end

    print(statusString)
    --app store充值成功,刷新界面
    if statusString == "success" then
        if GameListManager.getInstance():getIsLoginGameSucFlag() then
            CMsgGame:sendBankQueryInsureInfo()
        else
            CMsgHall:sendQueryInsureInfoLite()
        end
    end

    if statusString == "failed" then
        
    end
    cc.exports.scheduler.performWithDelayGlobal(function()
        cc.exports.Veil:getInstance():HideVeil(VEIL_LOCK)
    end, 1.0)

end

return AppStoreView
