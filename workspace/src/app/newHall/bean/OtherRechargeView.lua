--region OtherRechargeLayer.lua
--Date 2017.04.24.
--Auther JackyXu.
--Desc: 充值中心主页面

local G_TagRechargeMsgEvent = "TagOtherRechargeMsgEvent" -- 事件监听 Tag


local UsrMsgManager         = require("common.manager.UsrMsgManager")


local OtherRechargeLayer = class("OtherRechargeLayer", FixLayer)

local G_OtherRechargeLayer = nil
function OtherRechargeLayer.create()
    G_OtherRechargeLayer = OtherRechargeLayer.new():init()
    return G_OtherRechargeLayer
end

function OtherRechargeLayer:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()
    
    self.m_pNodeSub = nil
    self.m_pButtonTable = {}
    self.m_buttonPos = {}

    self.m_tmLastClicked = 0
    self.m_bBankSubViewMoving = false
    self.m_pViewIndex = 0
    self.m_RechargeHandle = nil

    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)
end

function OtherRechargeLayer:init()
    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_BIG, FixLayer.HIDE_DLG_BIG)

    --init csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/OtherRechargeView.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("ShopDialog")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pBtnNull          = self.m_pathUI:getChildByName("Panel_2") --空白处关闭
    self.m_pImgBg            = self.m_pNodeRoot:getChildByName("IMG_bg")
    self.m_pImgTitle         = self.m_pImgBg:getChildByName("IMG_tittle")
    self.m_pBtnClose         = self.m_pImgBg:getChildByName("BTN_close")
    self.m_pLbBankMoney      = self.m_pImgBg:getChildByName("Node_5"):getChildByName("BitmapFontLabel_1")
    self.m_pNodeContent      = self.m_pImgBg:getChildByName("node_content")

    self.m_vecBtnScore = {}
	self.m_pLbRechargeMoney = {}
    self.m_pLbRechargeRate = {}
    for i = 1, 8 do
        local strNodeName = "BTN_score" .. i
        self.m_vecBtnScore[i]       = self.m_pNodeContent:getChildByName("Node_gold"):getChildByName(strNodeName)
        self.m_pLbRechargeMoney[i]  = ccui.Helper:seekWidgetByName(self.m_vecBtnScore[i], "FNT_btnTx")
        self.m_pLbRechargeRate[i]  = ccui.Helper:seekWidgetByName(self.m_vecBtnScore[i], "BitmapFontLabel_5")
        self.m_vecBtnScore[i]:addTouchEventListener(function(sender,eventType)
                if not self.m_vecBtnScore[i]:isVisible() then return end
                if eventType==ccui.TouchEventType.began then
                    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
                    self.m_vecBtnScore[i]:setScale(1.05)
                elseif eventType==ccui.TouchEventType.canceled then
                    self.m_vecBtnScore[i]:setScale(1.0)
                elseif eventType==ccui.TouchEventType.ended then 
                    self.m_vecBtnScore[i]:setScale(1.0)
                    -- 响应选中
                    self:onClickedPay(i)
                end
            end)
    end

    -- 下方 tips
    self.m_pLbTips           = self.m_pImgBg:getChildByName("LB_tipsTxt2")

    --和谐版：显示保险箱
    if ClientConfig.getInstance():getIsOtherChannel() then
        self.m_pLbTips:setString(LuaUtils.getLocalString("RECHARGE_TIPS_2"))
    end

    --title特效
    local pSpine = "hall/effect/325_shangchengjiemiandonghua/325_shangchengjiemiandonghua"
    self.m_pAniTitle = sp.SkeletonAnimation:createWithBinaryFile(pSpine .. ".skel", pSpine .. ".atlas", 1)
    self.m_pAniTitle:setPosition(cc.p(695,368))
    self.m_pNodeRoot:addChild(self.m_pAniTitle)
    self.m_pAniTitle:setAnimation(0, "animation1", true)

    --常规版：充值/和谐版：商城
    local pathTitle = ChannelConfig:getRechargeImage()
    self.m_pImgTitle:loadTexture(pathTitle, ccui.TextureResType.plistType)

    --银行金币
    self.m_lUsrInsure = PlayerInfo.getInstance():getUserInsure()
    self.m_pLbBankMoney:setString(LuaUtils.getFormatGoldAndNumber(self.m_lUsrInsure))

    --设置充值数据
    self.m_RechargeType = G_CONSTANTS.Recharge_Type.Type_YSDK
    self:setRechargeInfo()
    return self
end

function OtherRechargeLayer:onEnter()
    self.super:onEnter()
    self:showWithStyle()

    self:initBtnEvent()

    SLFacade:addCustomEventListener(Public_Events.MSG_ENTER_FOREGROUND_RECHARGE,    handler(self, self.onEnterForeground),     self.__cname)
    SLFacade:addCustomEventListener(Public_Events.MSG_BANK_INFO,                    handler(self, self.onMsgBankInfo),         self.__cname)
    SLFacade:addCustomEventListener(Public_Events.MSG_UDPATE_USR_SCORE,             handler(self, self.onMsgBankInfo),         self.__cname)

    --请求当前金币信息
    self:getBankInfo()
end

function OtherRechargeLayer:onExit()

    SLFacade:removeCustomEventListener(Public_Events.MSG_ENTER_FOREGROUND_RECHARGE,     self.__cname)
    SLFacade:removeCustomEventListener(Public_Events.MSG_BANK_INFO,             	self.__cname)
    SLFacade:removeCustomEventListener(Public_Events.MSG_UDPATE_USR_SCORE,             	self.__cname)

    if self.m_RechargeHandle ~= nil then
        scheduler.unscheduleGlobal(self.m_RechargeHandle)
        self.m_RechargeHandle = nil
    end

    self.super:onExit()

    G_OtherRechargeLayer = nil
end

function OtherRechargeLayer:initBtnEvent()
    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseBtnClicked))
    self.m_pBtnNull:addClickEventListener(handler(self, self.onCloseBtnClicked))
end

function OtherRechargeLayer:setRechargeInfo()
    for i = 1, 8 do
        local rechargeData = CRechargeManager.getInstance():getRechargeInfoAtIndex(self.m_RechargeType, i)
        if rechargeData == nil or  rechargeData.dGiftLimit == nil or rechargeData.dGiftLimit == 0 then 
            break
        end
        local strMoney = string.format("%.2f元",rechargeData.dGiftLimit)
        self.m_pLbRechargeMoney[i]:setString(strMoney)

        if rechargeData.iGiftRate == 0 then
            local strRate =  string.format("%d万金",rechargeData.dGiftLimit)
            self.m_pLbRechargeRate[i]:setString(string.format("%d万金",rechargeData.dGiftLimit))
        else
            local giftMoney = CRechargeManager.getInstance():getRechargeGiftMoney(rechargeData.dGiftLimit,rechargeData.iGiftRate) 
            giftMoney = giftMoney + rechargeData.dGiftLimit
            self.m_pLbRechargeRate[i]:setString(string.format("%0.1f万",giftMoney))
        end
	end
end

function OtherRechargeLayer:getBankInfo()
    if GameListManager.getInstance():getIsLoginGameSucFlag() then
        CMsgGame:sendBankQueryInsureInfo()
    else
        CMsgHall:sendQueryInsureInfoLite()
    end
end 

function OtherRechargeLayer:onEnterForeground(_event)
    
    --SLFacade:dispatchCustomEvent(Public_Events.MSG_QUERY_RECHARGE)
end

function OtherRechargeLayer:onMsgBankInfo()
    print("=========OtherRechargeLayer onMsgBankInfo==========")
    self.m_lUsrInsure = PlayerInfo.getInstance():getUserInsure()
    self.m_pLbBankMoney:setString(LuaUtils.getFormatGoldAndNumber(self.m_lUsrInsure))
end 
----------------
-- 按纽响应
-- 关闭
function OtherRechargeLayer:onCloseBtnClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    
    self:onMoveExitView()
end

function OtherRechargeLayer:onClickedPay(nIndex)
    local rechargeData = CRechargeManager.getInstance():getRechargeInfoAtIndex(self.m_RechargeType, nIndex)
    local money =  rechargeData.dGiftLimit
   
    --应用宝支付
    local orderExtra = OtherLoginMgr.getInstance():YsdkOrderExtra()
    if orderExtra == "" then 
        FloatMessage.getInstance():pushMessageWarning("登录信息过期，请重新登录，再进行充值")
        return
    end
    cc.exports.Veil:getInstance():ShowVeil(VEIL_LOCK)
    print("other sdk recharge money:", money)
    CRechargeManager:getInstance():CreatedOrder(self.m_RechargeType, money, 0, orderExtra)
end  
 

return OtherRechargeLayer
