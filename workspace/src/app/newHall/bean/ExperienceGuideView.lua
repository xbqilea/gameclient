-- region ExperienceGuideView.lua

local ExperienceGuideView = class("ExperienceGuideView", cc.Node)

local STEP_ALL = 5

function ExperienceGuideView:ctor()
    self:enableNodeEvents()
    
    self.m_iStep = 0
    self.m_vecStep = {}

    self:init()
end

function ExperienceGuideView:onEnter()
    self:setStep(1, 5)
end

function ExperienceGuideView:onExit()
end

function ExperienceGuideView:init()
    self:initCSB()
    self:initBtn()
    self:initNode()
    self:initSpine()
end

function ExperienceGuideView:initCSB()
    
    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    self.m_pathUI = cc.CSLoader:createNode("hall/csb/ExperienceGuide.csb")
    self.m_pathUI:setPositionX((display.width - 1334) / 2)
    self.m_pathUI:addTo(self.m_rootUI)

    self.m_pShadowLayer = self.m_pathUI:getChildByName("ShadowLayer")
    self.m_pGuideLayer = self.m_pathUI:getChildByName("LayerGuide")
    self.m_pTopLayer = self.m_pathUI:getChildByName("LayerTop")

    self.m_vecShadow = {}
    for i = 0, 9 do
        self.m_vecShadow[i] = self.m_pShadowLayer:getChildByName("Panel_" .. i)
    end

    self.m_vecGuide = {}
    for i = 1, STEP_ALL do
        self.m_vecGuide[i] = self.m_pGuideLayer:getChildByName("Step" .. i)
    end

    self.m_pBtnSkip = self.m_pTopLayer:getChildByName("Button_Skip")
    self.m_pBtnStep = self.m_pTopLayer:getChildByName("Button_Step")
end

function ExperienceGuideView:initBtn()
    
    self.m_pBtnSkip:addClickEventListener(handler(self, self.onSkipClicked))
    self.m_pBtnStep:addClickEventListener(handler(self, self.onStepClicked))

    self.m_vecShadow[5]:setColor(cc.c3b(0xff, 0xff, 0xff))
end

function ExperienceGuideView:initNode()
    
    for i = 1, STEP_ALL do
        self.m_vecGuide[i]:setVisible(false)
    end
end

function ExperienceGuideView:initSpine()

    self.m_pFigure = cc.Node:create()
    self.m_pFigure:addTo(self.m_pGuideLayer)

    local path = "hall/effect"
    local name = "325_dianji"
    self.m_pFigureSpine = createSpineByBinary(path, name)
    self.m_pFigureSpine:setAnimation(0, "animation", true)
    self.m_pFigureSpine:setPosition(-130, 10)
    self.m_pFigureSpine:addTo(self.m_pFigure)

    local path1 = "hall/effect"
    local path2 = "vip_mm"
    local path3 = "325_vipjiem_renwu"
    self.m_pMM = createSpineByBinary2(path1, path2, path3)
    self.m_pMM:setPosition(250, 160)
    self.m_pMM:setScale(0.75)
    self.m_pMM:setAnimation(0, "animation2", true)
    self.m_pMM:addTo(self.m_pTopLayer)
end

function ExperienceGuideView:setStep(start, stop)
    
    self.m_iStep = start
    self.m_iStep2 = stop
    self:onUpdateStep(self.m_iStep)
end

--[[
1.引导排行榜
2.指引排行榜-财富榜
3.指引排行榜-赢分榜
4.指引排行榜-添加代理
--]]
function ExperienceGuideView:onUpdateStep(step)
    
    self.m_iStep = step

    if self.m_iStep > self.m_iStep2 then --结束
        return self:onCloseLayer()
    end

    -----------------------------------------
    --显示位置
    if self.m_iStep == 1 then

    elseif self.m_iStep == 2 then

        local _event = {
            name = Hall_Events.ROOMLIST_BACK_TO_HALL,
        }
        SLFacade:dispatchCustomEvent(Hall_Events.ROOMLIST_BACK_TO_HALL, _event)

        --手位置
        local hallLayer = require("hall.scene.HallScene").instance.m_pHallView
        local btnRank = hallLayer.m_pBtnRank
        local diff = (display.width - 1334) / 2
        local posRank = btnRank:getParent():convertToWorldSpaceAR(cc.p(btnRank:getPosition()))
        local posHand = cc.p(posRank.x - 200 - diff, posRank.y)
        self.m_pFigure:setPosition(posHand)

        --排行榜按钮
        local diff = (display.width - 1334) / 2
        local imageRank = self.m_vecGuide[2]:getChildByName("Image_Rank")
        imageRank:setPosition(posRank.x - 200 - diff, posRank.y)
        imageRank:setVisible(true)

    elseif self.m_iStep == 3 then
        --显示排行榜
        local hallLayer = require("hall.scene.HallScene").instance.m_pHallView
        hallLayer:showRankView()

    elseif self.m_iStep == 4 then
        --显示赢分榜按钮
        local rankButton = self.m_vecGuide[4]:getChildByName("Image_Rank")
        local posX, posY = rankButton:getPosition()

        --手位置
        local posFigure = cc.p(posX + 80, posY + 30)
        self.m_pFigure:setPosition(posFigure)

    elseif self.m_iStep == 5 then
        --显示赢分榜内容
        local hallLayer = require("hall.scene.HallScene").instance.m_pHallView
        local rankLayer = hallLayer.m_rootUI:getChildByName("RankLayer")
        rankLayer:onRankTagClicked(rankLayer.m_pBtnWin)
    end
    -----------------------------------------

    -----------------------------------------
    --当前步骤
    for i = 1, STEP_ALL do
        self.m_vecGuide[i]:setVisible(i == self.m_iStep)
    end
    if self.m_iStep == 1 then --显示无
        self.m_pFigure:setVisible(false)
        self:onUpdateShadow()

    elseif self.m_iStep == 2 then --显示排行榜按钮
        self.m_pFigure:setVisible(true)
        self:onUpdateShadow()

    elseif self.m_iStep == 3 then --显示排行榜内容
        self.m_pFigure:setVisible(false)
        self:onUpdateShadow()

    elseif self.m_iStep == 4 then --显示赢分榜按钮
        self.m_pFigure:setVisible(true)
        self:onUpdateShadow()

    elseif self.m_iStep == 5 then --显示排行榜内容
        self.m_pFigure:setVisible(false)
        self:onUpdateShadow()
    end

    self.m_pBtnStep:setVisible(false)
    self.m_pBtnStep:runAction(cc.Sequence:create(
        cc.DelayTime:create(1.0),
        cc.Show:create()))
    -----------------------------------------
end

function ExperienceGuideView:onUpdateShadow()

    for i = 0, 9 do
        self.m_vecShadow[i]:setVisible(i == 0)
    end
end

--[rect ]：显示信息
--[bShow]：是否穿透
function ExperienceGuideView:onUpdateShadow9(rect_)

    --test------------------
    local posX = math.random(400, 600)
    local posY = math.random(300, 450)
    local length = math.random(200, 250)
    local height = math.random(150, 200)
    local rect = rect_ or cc.rect(posX, posY, length, height)
    --test------------------

    local point1 = cc.p(0,                   0)
    local point2 = cc.p(rect.x,              0)
    local point3 = cc.p(rect.x + rect.width, 0)
    local point4 = cc.p(0,                   rect.y)
    local point5 = cc.p(rect.x,              rect.y)
    local point6 = cc.p(rect.x + rect.width, rect.y)
    local point7 = cc.p(0,                   rect.y + rect.height)
    local point8 = cc.p(rect.x,              rect.y + rect.height)
    local point9 = cc.p(rect.x + rect.width, rect.y + rect.height)
    local width1 = rect.x
    local width2 = rect.width
    local width3 = 1624 - width1 - width2
    local height1 = rect.y
    local height2 = rect.height
    local height3 = 1624 - height1 - height2

    local rects = {}
    rects[1] = cc.rect(point1.x, point1.y, width1, height1)
    rects[2] = cc.rect(point2.x, point2.y, width2, height1)
    rects[3] = cc.rect(point3.x, point3.y, width3, height1)
    rects[4] = cc.rect(point4.x, point4.y, width1, height2)
    rects[5] = cc.rect(point5.x, point5.y, width2, height2)
    rects[6] = cc.rect(point6.x, point6.y, width3, height2)
    rects[7] = cc.rect(point7.x, point7.y, width1, height3)
    rects[8] = cc.rect(point8.x, point8.y, width2, height3)
    rects[9] = cc.rect(point9.x, point9.y, width3, height3)

    for i = 1, 9 do
        self.m_vecShadow[i]:setPosition(rects[i].x, rects[i].y)
        self.m_vecShadow[i]:setContentSize(rects[i].width, rects[i].height)
    end
    for i = 0, 9 do
        self.m_vecShadow[i]:setVisible(i > 0)
    end
end

function ExperienceGuideView:onCloseLayer()
    self:removeFromParent()
end

function ExperienceGuideView:onSkipClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    self:onCloseLayer()
end

function ExperienceGuideView:onStepClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    self:onUpdateStep(self.m_iStep + 1)
end

function ExperienceGuideView:getTouchRect(node, scale)

    local pos1 = cc.p(node:getPosition())
    local pos2 = node:getParent():convertToWorldSpaceAR(pos1)
    local pos3 = cc.p(0, 0)
    local size = node:getContentSize()
    
    pos3.x = pos2.x - size.width / 2 * scale
    pos3.y = pos2.y - size.height / 2 * scale
    size.width = size.width * scale
    size.height = size.height * scale
    return cc.rect(pos3.x, pos3.y, size.width, size.height)
end

return ExperienceGuideView
