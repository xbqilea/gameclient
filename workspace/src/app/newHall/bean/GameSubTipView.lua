local GameSubTipView = class("GameSubTipView", FixLayer)

local SERVICE_QQ = 800833325
function GameSubTipView:ctor()
	self.super:ctor(self)
	self:enableNodeEvents()
	self:init()
	self:initBtnEvent()
end

function GameSubTipView:init()
	self:initCSB()
end

function GameSubTipView:initCSB()
	self.m_pathUI = cc.CSLoader:createNode("hall/csb/GameSubTip.csb")
    self:addChild(self.m_pathUI)

    local diffY = (display.height - 750)/2
    self.m_pathUI:setPositionY(diffY)

    self.m_pBtnClose = self.m_pathUI:getChildByName("Node_sub"):getChildByName("Image_bg"):getChildByName("btn_close")
    self.m_pBtnSure = self.m_pathUI:getChildByName("Node_sub"):getChildByName("Image_bg"):getChildByName("btn_sure")

end

function GameSubTipView:initBtnEvent()
	self.m_pBtnClose:addClickEventListener(handler(self,self.onCloseClicked))
	self.m_pBtnSure:addClickEventListener(handler(self,self.onSureClicked))
end

function GameSubTipView:onCloseClicked()
	AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
	self:onMoveExitView()
end

function GameSubTipView:onSureClicked()
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    LuaNativeBridge.getInstance():setCopyContent(SERVICE_QQ)

    if device.platform == "windows" then 
        local bOpenQQ   = Utils:pcOpenQQ() 
        if bOpenQQ == false then 
            FloatMessage.getInstance():pushMessage("STRING_046_6")
        else
            self:onMoveExitView()
        end
        return
    end

    --检测是否安装qq
    local canOpen = LuaNativeBridge:getInstance():isQQInstall()
    local canOpen2 = LuaNativeBridge:getInstance():isQQInstall2()
    if canOpen then
        cc.Application:getInstance():openURL("mqqwpa://im/chat?chat_type=wpa&uin=".. SERVICE_QQ .."&version=1")
        self:onMoveExitView()
    elseif canOpen2 then
        cc.Application:getInstance():openURL("mqq://im/chat?chat_type=wpa&uin=".. SERVICE_QQ .."&version=1")
        self:onMoveExitView()
    else
        FloatMessage.getInstance():pushMessage("STRING_046_6")
    end
end

function GameSubTipView:onEnter()
	self.super:onEnter()
	self:setTargetShowHideStyle(self,FixLayer.SHOW_DLG_BIG,FixLayer.HIDE_DLG_BIG)
	self:showWithStyle()
end

function GameSubTipView:onExit()
	self.super:onExit()
end


return GameSubTipView
