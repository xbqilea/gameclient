--region BankDetailsView.lua
--Date 2017.04.26.
--Auther JackyXu.
--Desc: 银行明细 view

local CBankDetails     = require("common.manager.CBankDetails")
local BankDetailSubDlg = require("hall.bean.BankDetailSubDlg")

local TAG_IDEL = 0
local TAG_ABLE = 1

local BankDetailsView = class("BankDetailsView", cc.Layer)
local HallSceneRes  = require("hall.scene.HallSceneRes")

function BankDetailsView:ctor()
    self:enableNodeEvents()

    self.m_pNodeTable = nil
    self.m_nOldMsgID = 0
    self.m_bIsCanReq = true
    self.m_nLastCount = 0
    self.m_iState = TAG_IDEL

    self:init()
end

function BankDetailsView:onEnter()
    CBankDetails.getInstance():Clear()
    CMsgHall:sendBankDetails(0)
end

function BankDetailsView:onExit()
    self:clean()
end

function BankDetailsView:init()
    
    self:initCSB()
    self:initEvent()
end

function BankDetailsView:clean()
    self:cleanEvent()
    self:cleanTableView()
end

function BankDetailsView:initCSB()
    
    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)

    --csb
    local strPath = "hall/csb/BankViewDetail.csb"
    if CommonUtils.getInstance():isPCMode() then
        strPath = "hall/csb/pc-BankViewDetail.csb"
    end
    self.m_pathUI = cc.CSLoader:createNode(strPath)
    self.m_pathUI:addTo(self.m_rootUI)

    --node
    self.m_pNodeNone = self.m_pathUI:getChildByName("Panel_1"):getChildByName("Text_9")
    self.m_pNodeTable = self.m_pathUI:getChildByName("Panel_2")

    self.m_pNodeNone:setVisible(false)
end

function BankDetailsView:initEvent()
    SLFacade:addCustomEventListener(Hall_Events.MSG_BANK_DETAILS, handler(self, self.onMsgBankDetailsInfo), self.__cname)                 
end

function BankDetailsView:cleanEvent()
    SLFacade:removeCustomEventListener(Hall_Events.MSG_BANK_DETAILS, self.__cname)
end

function BankDetailsView:initTableView()
    if not self.m_pTableView then
        self.m_pTableView = cc.TableView:create(self.m_pNodeTable:getContentSize())
        self.m_pTableView:setIgnoreAnchorPointForPosition(false)
        self.m_pTableView:setAnchorPoint(cc.p(0,0))
        self.m_pTableView:setPosition(cc.p(0, 0))
        self.m_pTableView:setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN)
        self.m_pTableView:setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL)
        self.m_pTableView:setDelegate()
        self.m_pTableView:registerScriptHandler(handler(self,self.scrollViewDidScroll), CCTableView.kTableViewScroll)
        self.m_pTableView:registerScriptHandler(handler(self,self.cellSizeForTable), CCTableView.kTableCellSizeForIndex)
        self.m_pTableView:registerScriptHandler(handler(self,self.tableCellAtIndex), CCTableView.kTableCellSizeAtIndex)
        self.m_pTableView:registerScriptHandler(handler(self,self.numberOfCellsInTableView), CCTableView.kNumberOfCellsInTableView)
        self.m_pTableView:registerScriptHandler(handler(self,self.tableCellTouched), CCTableView.kTableCellTouched)
        self.m_pTableView:addTo(self.m_pNodeTable)
        if CommonUtils.getInstance():isPCMode() then
            self.m_pTableView:createScrollBar(HallSceneRes.logSliderTrack1, HallSceneRes.sliderThumb)
        end
        self.m_pTableView:reloadData()
    else

        --重载前
        local offset  = self.m_pTableView:getContentOffset() --内容位移
        local rect    = self.m_pTableView:getViewSize()      --内容大小
        local content = self.m_pTableView:getContentSize()   --界面大小

        self.m_pTableView:reloadData()

        --重载后
        local offset2  = self.m_pTableView:getContentOffset() --内容位移
        local rect2    = self.m_pTableView:getViewSize()      --内容大小
        local content2 = self.m_pTableView:getContentSize()   --界面大小

        self.m_pTableView:setContentOffset(cc.p(0, offset.y + content.height - content2.height))

    end

    if device.platform == "windows" then
        local count = CBankDetails.getInstance():getBankDetailsCount()
        self.m_pTableView:setScrollBarVisible(count>8)
        self.m_pTableView:setMouseScrollEnabled(count>8)
    end
end

function BankDetailsView:cleanTableView()
    if self.m_pTableView then
        self.m_pTableView:removeFromParent()
        self.m_pTableView = nil
    end
end

function BankDetailsView:getTableItem()
    
    local strPath = "hall/csb/BankViewDetailItem.csb"
    if CommonUtils.getInstance():isPCMode() then
        strPath = "hall/csb/pc-BankViewDetailItem.csb"
    end
    local item = cc.CSLoader:createNode(strPath)
    local node = item:getChildByName("Panel_1")
    item.spriteType  = node:getChildByName("Image_2")
    item.labelDate   = node:getChildByName("Text_1")
    item.labelTitle  = node:getChildByName("Text_2")
    item.labelValue  = node:getChildByName("Text_3")
    item.labelInsure = node:getChildByName("Text_4")
    item.spCertification = node:getChildByName("Sprite_1")

    return item
end

local PATH_TYPE = --1为存 2为取 3为转 4赠送 5充值 6推广收益
{
    [1] = { "hall/plist/bank/gui-bank-logo-type-1.png", LuaUtils.getLocalString("BANK_19"), },
    [2] = { "hall/plist/bank/gui-bank-logo-type-2.png", LuaUtils.getLocalString("BANK_20"), },
    [3] = { "hall/plist/bank/gui-bank-logo-type-3.png", LuaUtils.getLocalString("BANK_21"), },
    [4] = { "hall/plist/bank/gui-bank-logo-type-4.png", "", },
    [5] = { "hall/plist/bank/gui-bank-logo-type-5.png", LuaUtils.getLocalString("BANK_33"), },
    [6] = { "hall/plist/bank/gui-bank-logo-type-6.png", LuaUtils.getLocalString("BANK_47"), },
}

function BankDetailsView:setTableItem(item, data)

    --时间
    local string_date = string.format("%02d-%02d", data.time.wMonth, data.time.wDay)
    local string_time = string.format("%02d:%02d:%02d", data.time.wHour, data.time.wMinute, data.time.wSecond)
    local string_show = string.format("%s %s", string_date, string_time)
    item.labelDate:setString(string_show)

    --类型
    if data.byTradeType == 4 then 
        local strTradeType = LuaUtils.trim(tostring(data.szCollectNote))
        item.labelTitle:setString(strTradeType)
    else
        local config_type = PATH_TYPE[data.byTradeType]
        --item.spriteType:loadTexture(config_type[1], ccui.TextureResType.plistType)
        item.labelTitle:setString(config_type[2])
    end

    --金额
    local string_value = LuaUtils.getFormatGoldAndNumber(data.lSwapScore)
    if data.byTradeType == 1 then
        --存入 
        string_value = "+"..string_value
        item.labelValue:setTextColor(cc.c3b(118,220,68))
    elseif data.byTradeType == 2 then
        --取出 
        string_value = "-"..string_value
        item.labelValue:setTextColor(cc.c3b(255,106,106))
    elseif data.byTradeType == 3 then
        if data.dwSourceUserID == PlayerInfo.getInstance():getUserID() then 
            --转账方
            string_value = "-"..string_value
            item.labelValue:setTextColor(cc.c3b(255,106,106))
        else
            --被转账
            string_value = "+"..string_value
            item.labelValue:setTextColor(cc.c3b(118,220,68))
        end
    else
        string_value = "+"..string_value
        item.labelValue:setTextColor(cc.c3b(118,220,68))
    end
    item.labelValue:setString(string_value)

    --凭证图标
    item.spCertification:setVisible(data.byTradeType == 3)

    --余额
    local bTargetSelf = data.dwTargetGameID == PlayerInfo.getInstance():getGameID()
    local gold = bTargetSelf and data.lTargetBank or data.lSourceBank
    local strGold = LuaUtils.getFormatGoldAndNumber(gold)
    local prefix = LuaUtils.getLocalString("BANK_22")
    local string_insure = string.format("%s%s", prefix, strGold)
    item.labelInsure:setString(string_insure)
end

function BankDetailsView:cellSizeForTable(table, idx)
    return 875, 58
end

function BankDetailsView:tableCellAtIndex(table, idx)
    local cell = table:cellAtIndex(idx)
    if not cell then
        cell = cc.TableViewCell:new()
    else
        cell:removeAllChildren()
    end

    self:initTableViewCell(cell, idx)

    return cell
end

function BankDetailsView:numberOfCellsInTableView()
    local nCount = CBankDetails.getInstance():getBankDetailsCount()
    return nCount
end

function BankDetailsView:tableCellTouched(table, cell)
    print(table, cell)
    local tag = cell:getTag()
    local data = CBankDetails.getInstance():getBankDetailsInfoAtIndex(tag + 1)
    if data.byTradeType == 3 then 
        print("查看凭证")
        self:showTransferDetailDlg(data)
    end
end

function BankDetailsView:initTableViewCell(cell, nIdx)
    local data = CBankDetails.getInstance():getBankDetailsInfoAtIndex(nIdx + 1)
    local node = self:getTableItem()
    if CommonUtils.getInstance():isPCMode() then
        node:setPositionX(80)
    end
    self:setTableItem(node, data)
    cell:addChild(node)
    cell:setTag(nIdx)
end

function BankDetailsView:scrollViewDidScroll(sender, eventType)

    local offset  = self.m_pTableView:getContentOffset() --内容位移
    local content = self.m_pTableView:getContentSize()   --内容大小
    local rect    = self.m_pTableView:getViewSize()      --界面大小
    local count = CBankDetails.getInstance():getBankDetailsCount() --已保存数
    local total = CBankDetails.getInstance():getBankDetailsTotal() --全部数量
    
    print(offset.y, content.height, rect.height, count, total)

    --不满5个不用请求
    if total <= 5 then
        return
    end

    --请求完毕不用请求
    if count == total then
        return
    end

    local ry = 60 
    if device.platform == "windows" then
        ry = 0
    end
    if self.m_iState == TAG_IDEL then
        if offset.y >= ry then --达到条件
            self.m_iState = TAG_ABLE
            --FloatMessage.getInstance():pushMessageDebug("达到条件" .. offset.y)

            if total - count > 0 then 
                local requestID = CBankDetails.getInstance():getBankDetailsInfoAtIndex(count).iRecordID
                if self.m_nOldMsgID == 0 or self.m_nOldMsgID > requestID then
                    self.m_bIsCanReq = false
                    self.m_nOldMsgID = requestID
                    self.m_nLastCount = count
                    CMsgHall:sendBankDetails(requestID)

                    --FloatMessage.getInstance():pushMessageDebug("请求数据" .. requestID)
                end
            end

        else
            return --未达到条件
        end
    end

    if self.m_iState == TAG_ABLE then

        if (offset.y == 0 and content.height > rect.height)
        or (offset.y == rect.height - content.height and content.height < rect.height)
        then --恢复完毕
            self.m_iState = TAG_IDEL

            --FloatMessage.getInstance():pushMessageDebug("恢复完毕" .. offset.y)
        else
            return --未恢复完毕
        end
    end
end

function BankDetailsView:onMsgBankDetailsInfo(msg)

    --没有数据
	local count = CBankDetails.getInstance():getBankDetailsCount()
    self.m_pNodeNone:setVisible(count <= 0)

	CBankDetails.getInstance():sort()

    self:initTableView()

	cc.exports.scheduler.performWithDelayGlobal(function()
		self.m_bIsCanReq = true
	end, 1.0)

end

--转账凭证界面
function BankDetailsView:showTransferDetailDlg(data)

    local pTransferDetailDlg = BankDetailSubDlg:create()
    pTransferDetailDlg:setDetailInfo(data)
    pTransferDetailDlg:setName("BankDetailSubDlg")
    self:getParent():getParent():getParent():getParent():addChild(pTransferDetailDlg, 500)
end

return BankDetailsView
