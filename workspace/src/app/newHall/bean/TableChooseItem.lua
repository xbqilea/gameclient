--region *.lua
--Date
--
--endregion

local CommonUserInfo = require "common.layer.CommonUserInfo"

local TableChooseItem = class("TableChooseItem", cc.Layer)

function TableChooseItem.create(_tableID)
    local TableChooseItem = TableChooseItem:new():init(_tableID)
    return TableChooseItem
end

function TableChooseItem:ctor()
    self:enableNodeEvents()

    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    self.m_pathUI = cc.CSLoader:createNode("hall/csb/TableChooseItem.csb")
    self.m_pathUI:addTo(self.m_rootUI)

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("TableChooseItem")

    self.m_cbTableID = 0
    self.m_spImage = {}
    self.m_userIcon = {}
    self.m_btnUser = {}
    for i = 1, 2 do
        self.m_spImage[i] = nil
    end
    self.m_spTable = nil
    self.handler = nil
    self.cheskUser = false
end

function TableChooseItem:init(tableID)

    self.m_spTable = self.m_pNodeRoot:getChildByName("IMG_table") 
    for i = 1,2 do
        self.m_userIcon[i] = self.m_pNodeRoot:getChildByName(string.format("IMG_user_%d",(i-1))) 
        self.m_btnUser[i] = self.m_userIcon[i]:getChildByName(string.format("BTN_user_%d",(i-1))) 
        self.m_spImage[i] = self.m_userIcon[i]:getChildByName(string.format("IMG_user_icon_%d",(i-1))) 
        self.m_btnUser[i]:setSwallowTouches(false)
        self.m_userIcon[i]:setVisible(false)
        self.m_btnUser[i]:addClickEventListener(function()
            self.cheskUser = true 
            cc.exports.scheduler.performWithDelayGlobal(function()
                if self and self.onChairClicked then
                    self:onChairClicked((i-1))
                end
            end,0.1)
        end)
    end

    self.m_tableBtn = self.m_pNodeRoot:getChildByName("BTN_table")
    self.m_tableBtn:setSwallowTouches(false)
    self.m_tableBtn:addClickEventListener(function()
        cc.exports.scheduler.performWithDelayGlobal(function()
            if self and self.onTableClicked then
                self:onTableClicked()
            end
        end,0.1)
    end)

    self.spNumBg= self.m_pNodeRoot:getChildByName("IMG_num")

    local tableInfo = CUserManager.getInstance():getTableInfo()
    if (tableID >= tableInfo.wTableCount) then
        return false
    end

    --self.m_spTableLogo = self.m_pNodeRoot:getChildByName("IMG_logo") 
    local kindID = PlayerInfo.getInstance():getKindID()
    self.m_spTableLogo:loadTexture( string.format("hall/image/table/gui-table-logo-%d.png",kindID))

    self.m_cbTableID = tableID  
    self:updateTableStatus()

    local lbNum = cc.Label:createWithCharMap("hall/image/table/gui-common-suzi-orange-26.png", 26, 34, 48)
    lbNum:setPosition(cc.p(0, -113))
    self:addChild(lbNum)
    lbNum:setString( string.format("%d",tableID+1))

--    self:updateUserImage()

    self.update_listener = SLFacade:addCustomEventListener(Public_Events.MSG_UPDATE_USER_STATUS, handler(self, self.onMsgUpdateUserStatus))

    return self
end

function TableChooseItem:setHandler(handler)
    self.handler = handler
end

function TableChooseItem:onExit()
    for i = 1, 2 do
        local tag = 100 * (self.m_cbTableID + 1) + i - 1
        CUserManager.getInstance():deleteUserDialogByTag(tag)
    end
    SLFacade:removeEventListener(self.update_listener)
end

function TableChooseItem:onTableClicked()
    if self.cheskUser then 
        self.cheskUser = false 
        return 
    end
    if not self.handler or not self.handler.onClick then return end
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
    local userIDTable = {}
    for i = 1, 2 do
        userIDTable[i] = CUserManager.getInstance():getUserIDByChairID2(self.m_cbTableID, (i - 1))
    end

    if userIDTable[1] == 0 then
        local _event = {
            name = Public_Events.MSG_GO_SIT,
            packet = string.format("%d,%d",self.m_cbTableID,0),
        }
        SLFacade:dispatchCustomEvent(Public_Events.MSG_GO_SIT, _event)
        return 
    elseif  userIDTable[2] == 0 then
        local _event = {
            name = Public_Events.MSG_GO_SIT,
            packet = string.format("%d,%d",self.m_cbTableID,1),
        }
        SLFacade:dispatchCustomEvent(Public_Events.MSG_GO_SIT, _event)
        return
    else
        -- add by JackyXu.
        FloatMessage.getInstance():pushMessage("STRING_185")
    end
end

function TableChooseItem:onChairClicked(_index)
    if not self.handler or not self.handler.onClick then return end
    AudioManager.getInstance():playSound("public/sound/sound-button.mp3")
    local chairID = _index
    local userID = CUserManager.getInstance():getUserIDByChairID2(self.m_cbTableID, chairID)
    if (userID ~= 0) then
        local node = self:getParent()
        if node then
            local tag = 100*(self.m_cbTableID+1)+chairID
            if (CUserManager.getInstance():checkDialogByTag(tag)) then
                return
            end
            local pos = (chairID == 0) and cc.p(-60,-60) or cc.p(-240, -200)
            --保留dialog信息
            local user = CUserManager.getInstance():getUserInfoByUserID(userID)
            local info = {}
            info.nTag = tag
            info.cbTableID = self.m_cbTableID
            info.cbChairID = chairID
            info.fShowTimes = 0
            info.szNickName =  user.szNickName
            info.dwGameID = user.dwGameID
            info.lScore = user.lScore
            info.nLevel = user.nLevel
            info.dwWinCount = user.dwWinCount
            info.dwDrawCount = user.dwDrawCount
            info.dwFleeCount = user.dwFleeCount
            info.dwLostCount = user.dwLostCount
            CUserManager.getInstance():addUserDialog(info)
            
            local posWorld = self.m_btnUser[_index+1]:convertToWorldSpace(cc.p(self.handler:getPosition()))
            posWorld.y = posWorld.y - (display.height - 750)/2
            local diffX = 0
            local diffY = 0
            if posWorld.y < 230 then 
                diffY = 80
            else
                diffY = -190
            end

            if chairID == 1 and (posWorld.x > 1115 and diffY < 0) then
                diffX = -160
            elseif chairID == 1 and diffY > 0 then
                diffX = -190
            else
                diffX = -25
            end

            self.handler:showUserInfoDialog(cc.pAdd(posWorld, cc.p(diffX, diffY)),tag,info)
        end
    else

    end
end

function TableChooseItem:showUserDialog()
    for i = 1, 2 do
        local tag = 100 * (self.m_cbTableID + 1) + i - 1
        local node = self:getParent()
        if (node and CUserManager.getInstance():checkDialogByTag(tag)) then
            local pos = (i == 1) and cc.p(-240,-185) or cc.p(120, -200)
            local info = CUserManager.getInstance():getUserDialogByTag(tag);
            if info.fShowTimes < 3.5 then
                local userDialog = CommonUserInfo.create(1)
                userDialog:setPosition(cc.pAdd(cc.p(self:getPosition()), pos))
                userDialog:setTag(tag)
                node:addChild(userDialog, G_CONSTANTS.Z_ORDER_COMMON)
                userDialog:updateUserInfoByInfo(info)
            else
                CUserManager.getInstance():deleteUserDialogByTag(tag);
            end
        end
    end
end

function TableChooseItem:onMsgUpdateUserStatus()
    if not self.m_userIcon then 
        self.m_userIcon = {}
    end
    for i = 1, 2 do
        if self.m_userIcon[i] then
            self.m_userIcon[i]:setVisible(false)
        end
    end
    if self.updateTableStatus then
        self:updateTableStatus()
    end
end

function TableChooseItem:updateTableStatus()
    local users = CUserManager.getInstance():getUserInfoInTable(self.m_cbTableID);
    local bPlay = false;
    if #users >= 2 then
        local user1 = users[1]
        local user2 = users[2]
        if (user1.cbUserStatus >= G_CONSTANTS.US_READY and user2.cbUserStatus >= G_CONSTANTS.US_READY) then
            bPlay = true;
        end
    end
    self.m_spTable:setVisible(bPlay)
    --self.m_spTableLogo:setVisible(not bPlay)
    self:updateUserImage()
end

function TableChooseItem:updateUserImage()
    local users = CUserManager.getInstance():getUserInfoInTable(self.m_cbTableID)
    for i = 1, #users do
        local userInfo = users[i]
        local wFaceID = userInfo.wFaceID or 0
        local index = userInfo.wChairID%2 + 1
        if index > 0 and index < 3 then
            local strHeadIcon = string.format("hall/image/file/gui-icon-head-%02d.png", wFaceID % G_CONSTANTS.FACE_NUM + 1)
            self.m_spImage[index]:loadTexture(strHeadIcon,ccui.TextureResType.localType)
            self.m_userIcon[index]:setVisible(true)
        end
    end
end

return TableChooseItem
