--region RotateMenuLayer.lua
--Date 2017.08.09.
--Auther Goblin.
--Desc:  旋转按钮view

local RotateMenuLayer = class("RotateMenuLayer", cc.Node)

local DownLoadResView   = require("hall.bean.DownLoadResView")    --下载界面
local DownloadResMgr    = require("common.manager.DownloadResMgr")

local RECOMMON_GAME_KIND = CGameClassifyDataMgr:getRecommendGame() --主推游戏

function RotateMenuLayer:ctor()
    self:enableNodeEvents()

    self.m_pRotateMenu = nil

    self:init()
end

function RotateMenuLayer:init()

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:setPosition(cc.p(800,410))
    self.m_rootUI:addTo(self)

    --旋转列表
    self.m_pRotateMenu = cc.RotateMenu:create()
    self.m_pRotateMenu:setAddOpacity(0.4)
    self.m_pRotateMenu:setAddScale(0.1)
    self.m_pRotateMenu:addTo(self.m_rootUI)

    local gameCount = 0 --游戏分类个数
    local m_pListTable = {} --游戏分类动画资源路经
    local m_armatureNameArr = {} --游戏分类动画名字
    local m_GameTag = {} --游戏分类

    gameCount = 5
    m_GameTag = { 
        G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_MAIN,
        G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_CARD,
        G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_ARCADE,
        G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_TIGER,
        G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_FISH,
    }

    for i, v in pairs(m_GameTag) do
        local data = CGameClassifyDataMgr:getLocalClassifyDataByTag(v)
        table.insert(m_pListTable, data.AnimationPath)
        table.insert(m_armatureNameArr, data.AnimationName)
    end

    --从游戏返回大厅时,移动位置
    local offset = 0
    local kindID = PlayerInfo.getInstance():getKindID4Rotate()
    if kindID ~= 0 and kindID ~= RECOMMON_GAME_KIND then
        local typeID = CGameClassifyDataMgr.getInstance():getClassifyTypeByKindId(kindID)
        for i = 1, gameCount do
            if m_GameTag[i] ~= typeID then
                table.insert(m_GameTag, m_GameTag[i])
                table.insert(m_pListTable, m_pListTable[i])
                table.insert(m_armatureNameArr, m_armatureNameArr[i])
                offset = offset + 1
            else
                break
            end
        end
        for i = offset, 1, -1 do
            table.remove(m_GameTag, i)
            table.remove(m_pListTable, i)
            table.remove(m_armatureNameArr, i)
        end
    end
    PlayerInfo:getInstance():setKindID4Rotate(0)

    for i = 1, gameCount do
        --初始化 按钮
        local strPath = "hall/image/file/gui-menu-null.png";
        local spNormal = cc.Sprite:create(strPath)
        local spSelect = cc.Sprite:create(strPath)
        local spDisable = cc.Sprite:create(strPath)
        local item = cc.MenuItemSprite:create(spNormal, spSelect, spDisable)
        item:setTag(m_GameTag[i])
        item:registerScriptTapHandler(handler(self, self.onClassfiyClicked))

        --初始化 动画
        ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(m_pListTable[i])
        local armature = ccs.Armature:create(m_armatureNameArr[i])
        armature:getAnimation():playWithIndex(0)
        local size = spNormal:getContentSize()
        armature:setPosition(cc.p(size.width/2, size.height/2))
        armature:addTo(item)

        --主推游戏 下载动画
        if m_GameTag[i] == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_MAIN then
            local pDownloadRes = self:addDownLoadView(RECOMMON_GAME_KIND)
            if pDownloadRes then
                pDownloadRes:setAnchorPoint(cc.p(0.5, 0))
                pDownloadRes:setPosition(cc.p(size.width/2, 10))
                pDownloadRes:setName("DownLoadResView")
                pDownloadRes:addTo(item)
                self.m_pDownloadRes = pDownloadRes
            end
        end

        --添加
        if m_GameTag[i] == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_MAIN then --主推游戏
            local rooms = GameListManager.getInstance():getStructRoomByKindIDWithOnlyBaseScore(RECOMMON_GAME_KIND)
            if table.nums(rooms) > 0 then --有斗地主才显示
                self.m_pRotateMenu:addMenuItem(item)
            end
        elseif CGameClassifyDataMgr.getInstance():getGameClassifyServerCount(m_GameTag[i]) > 0 then --有游戏
            self.m_pRotateMenu:addMenuItem(item)
        end
    end
    self.m_pRotateMenu:updatePositionWithAnimation()
end
--下载显示
function RotateMenuLayer:addDownLoadView(nKindId)
    
    local pDownloadRes = DownLoadResView.create()
    pDownloadRes:setVisible(false)

    -- 下载进度
    if DownloadResMgr.getInstance():checkIsDownloadResoure(nKindId) then
        -- 需要下载资源
        pDownloadRes:setVisible(true)
        local per = DownloadResMgr.getInstance():getDownLoadProgressPer(nKindId)
        if per and per > 0 then
            pDownloadRes:updatePercent(per)
        else
            pDownloadRes:setNeedDownLoadState()
        end
    elseif DownloadResMgr.getInstance():getIsShowDownloadDone(nKindId) then
        -- 已完成下载
        pDownloadRes:setDownLoadDoneState()
    end

    --pDownloadRes:setVisible(true) --测试显示
    return pDownloadRes
end
function RotateMenuLayer:updateDownloadState(nKindId)
    
    if nKindId == RECOMMON_GAME_KIND then
        local bNeedDown = DownloadResMgr.getInstance():checkIsDownloadResoure(nKindId)
        if bNeedDown then -- 需要下载资源
            self.m_pDownloadRes:setVisible(true)
            local per = DownloadResMgr.getInstance():getDownLoadProgressPer(nKindId)
            if per and per > 0 then
                self.m_pDownloadRes:updatePercent(per)
            else
                self.m_pDownloadRes:setNeedDownLoadState()
            end
        end
    end
end
--更新下载
function RotateMenuLayer:updateDownLoadPercent(_nGameKindID, nPercent)
    
    if _nGameKindID == RECOMMON_GAME_KIND then
        if self.m_pDownloadRes then
            self.m_pDownloadRes:updateDownload(_nGameKindID, nPercent)
        end
    end
end
--更新出错（未测试过）
function RotateMenuLayer:updateDownLoadError(_nGameKindID)
    if _nGameKindID == RECOMMON_GAME_KIND then
        if self.m_pDownloadRes and self.m_pDownloadRes:isVisible() then
            self.m_pDownloadRes:setVisible(true)
            self.m_pDownloadRes:setNeedDownLoadState()
        end
    end
end
--点击进入游戏
function RotateMenuLayer:onClassfiyClicked(idx, sender)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    local tag = sender:getTag()
    --print("onClassfiyClicked tag:" .. tag) 
    
    --进入主推游戏
    if tag == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_MAIN then 
        
        local msg = {
            name = Hall_Events.MSG_GMAE_ENTER_RECOMMEND,
            packet = RECOMMON_GAME_KIND, 
        }
        cc.exports.SLFacade:dispatchCustomEvent(Hall_Events.MSG_HALL_CLICKED_GAME, msg)
    
    --进入游戏列表
    else
        local msg = {
            name =  Hall_Events.MSG_GMAE_ENTER_LIST,
            packet = idx,
        }
        cc.exports.SLFacade:dispatchCustomEvent(Hall_Events.MSG_HALL_CLICKED_GAME, msg)
    end
end

return RotateMenuLayer
