--region GameRoomChooseDlg.lua
--Date 2017/05/08
--Auther JackyXu
--Desc: 游戏房间选择 弹框 (三级页面)

local GameRoomChooseDlg = class("GameRoomChooseDlg", FixLayer)

GameRoomChooseDlg.instance = nil
function GameRoomChooseDlg.create()
    GameRoomChooseDlg.instance = GameRoomChooseDlg:new()
    return GameRoomChooseDlg.instance
end

function GameRoomChooseDlg:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()

    self.m_rootUI = display.newNode()
    self.m_rootUI:addTo(self)

    self.m_InnerNode = nil   -- scrollview 内容器
    self.m_root_SV = nil

    self.m_nGameKindID = 0
    self.m_nBaseScore = 0
    self.m_vecRoomList = {}
    self.m_nLastTouchTime = nil
    self.m_nShowArrowNeedCount = 4
    self.m_nIsTouchOut = false

    self:setTargetShowHideStyle(self, self.SHOW_DLG_BIG, self.HIDE_DLG_BIG)

    display.loadSpriteFrames("hall/image/gui-room.plist", "hall/image/gui-room.png")
    display.loadSpriteFrames("hall/plist/gui-roomChoose.plist", "hall/plist/gui-roomChoose.png")

    self:init()
end

function GameRoomChooseDlg:onEnter()
    self.super:onEnter()
    self:showWithStyle()
    self:createSlider()
    SLFacade:addCustomEventListener(Hall_Events.MSG_GET_ROOM_LIST, handler(self, self.onMsgGetRoomList), self.__cname)
    SLFacade:addCustomEventListener(Public_Events.MSG_GAME_LOGIN_SUCCESS, handler(self, self.onMsgLoginGameSuccess), self.__cname)
end

function GameRoomChooseDlg:onExit()
    SLFacade:removeCustomEventListener(Hall_Events.MSG_GET_ROOM_LIST,self.__cname)
    SLFacade:removeCustomEventListener(Public_Events.MSG_GAME_LOGIN_SUCCESS,self.__cname)
    self.super:onExit()

    GameRoomChooseDlg.instance = nil
end

function GameRoomChooseDlg:init()

    self.m_pathUI = cc.CSLoader:createNode("hall/csb/ChooseRoomDialog.csb")
    self.m_rootUI:addChild(self.m_pathUI)
    local diffY = (display.size.height - 750) / 2
    self.m_pathUI:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.m_pathUI:getChildByName("ChooseRoomDialog")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)

    self.m_pNodeShadow  = self.m_pNodeRoot:getChildByName("node_shade")
    self.m_pNodeRootUI  = self.m_pNodeRoot:getChildByName("node_rootUI")
    self.m_pSVNode      = self.m_pNodeRootUI:getChildByName("node_SV")
    self.m_pBtnClose    = self.m_pNodeRootUI:getChildByName("BTN_close")
    self.m_pBtnClose:addClickEventListener(handler(self, self.onCloseBtnClick))

--    self.m_pSVNode = tolua.cast(self["node_SV"], "cc.Node")
--    self.m_pBtnUpArrow = tolua.cast(self["BTN_UP_ARROW"], "cc.ControlButton")
--    self.m_pBtnDownArrow = tolua.cast(self["BTN_DOWN_ARROW"], "cc.ControlButton")

    --上箭头
--    local seq = cc.Sequence:create(cc.MoveBy:create(0.4, cc.p(0,-8)),cc.MoveBy:create(0.4, cc.p(0,8)));
--    self.m_pBtnUpArrow:runAction(cc.RepeatForever:create(seq));
--    self.m_pBtnUpArrow:setVisible(false)
    --下箭头
--    local seq2 = cc.Sequence:create(cc.MoveBy:create(0.4, cc.p(0,8)),cc.MoveBy:create(0.4, cc.p(0,-8)));
--    self.m_pBtnDownArrow:runAction(cc.RepeatForever:create(seq2));
    
    self.m_pNodeShadow:setVisible(false)
end

function GameRoomChooseDlg:setGameKind(GameKindID, BaseScore)
    self.m_nGameKindID = GameKindID
    self.m_nBaseScore = BaseScore

    --房间列表
    self:createScrollRoomList()

--    if self:getScrollViewCellCount() < self.m_nShowArrowNeedCount then
--        self.m_pBtnDownArrow:setVisible(false)
--    end

    --后发送
    CMsgHall:sendGetServerList(self.m_nGameKindID)
end

function GameRoomChooseDlg:onBtnArrowClicked(_upDown)
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")
    local self = GameRoomChooseDlg.instance

    local cellCount = self:getScrollViewCellCount()
    local maxHight = 140 * cellCount

    --1 点击上 2 点击下
    if _upDown == 1 then
        self.m_root_SV:setContentOffset(cc.p(0, -(maxHight+1 - 140 * 3)))
    else
        self.m_root_SV:setContentOffset(cc.p(0, 0))
    end
    self.m_pBtnUpArrow:setVisible(_upDown == 2)
    self.m_pBtnDownArrow:setVisible(_upDown == 1)
end

function GameRoomChooseDlg:onScrollEvent(sender, _event)
    print("onScrollEvent")
--    if not self.m_pBtnUpArrow or not self.m_pBtnDownArrow then
--        return
--    end

    -- 设置箭头显示
--    local pos = sender:getInnerContainerPosition()
--    if pos.y <= self.m_root_SV:getContentSize().height - 70 then
--        self.m_pBtnUpArrow:setVisible(true)
--        self.m_pBtnDownArrow:setVisible(false)
--    elseif pos.yx == 0 then
--        self.m_pBtnUpArrow:setVisible(false)
--        self.m_pBtnDownArrow:setVisible(true)
--    end
end

function GameRoomChooseDlg:onMsgLoginGameSuccess()
    --删掉界面
    self:removeFromParent()
end

function GameRoomChooseDlg:onMsgGetRoomList(_event)
    self:onUpdateRoomOnline()
end

function GameRoomChooseDlg:getRoomList(gameKind, baseSocre)
--    if self.m_nGameKindID == G_CONSTANTS.EGAME_TYPE_CODE.EGAME_TYPE_NIUNIU then
--        local HoxDataMgr = require("game.handredcattle.manager.HoxDataMgr")
--        local bFour = HoxDataMgr.getInstance():getModeType() == 4
--        return GameListManager.getInstance():getStructRoomByKindIDAndMode(self.m_nGameKindID, self.m_nBaseScore, bFour)
--    else
--        return GameListManager.getInstance():getStructRoomByKindID(self.m_nGameKindID, self.m_nBaseScore)
--    end
    return GameListManager.getInstance():getStructRoomByKindID(self.m_nGameKindID, self.m_nBaseScore)
end

function GameRoomChooseDlg:createScrollRoomList()
    
    self.m_vecRoomList = self:getRoomList(self.m_nGameKindID, self.m_nBaseScore)
    local scrollViewSize = self.m_pSVNode:getContentSize() -- cc.size(600,340) -- 滑动区域大小
    local roomCount = #self.m_vecRoomList
    local cellCount = self:getScrollViewCellCount()

    self.m_InnerNode = cc.Node:create()
    local maxHight = 135 * cellCount;
    if maxHight < scrollViewSize.height then
        maxHight = scrollViewSize.height
    end
    self.m_InnerNode:setContentSize(cc.size(scrollViewSize.width, maxHight))

    local STARTX = 0
    local nStartX, nStartY = STARTX, maxHight - 135; -- 起始位置
    local nOffX, nOffY = 300, - 135; -- 偏移
    local btnSize = cc.size(300, 135) -- 按纽大小

    for index=1,roomCount do

        local _btnNode = cc.Node:create()
        _btnNode:setPosition(nStartX, nStartY)
        self.m_InnerNode:addChild(_btnNode, 1, index)

        local roomInfo = self.m_vecRoomList[index]

        --按钮
        local celNodeBg = cc.Sprite:createWithSpriteFrameName("hall/plist/dialog/gui-chooseRoom.png")
        local pNormalSprite = cc.Scale9Sprite:createWithSpriteFrame(celNodeBg:getSpriteFrame())
        local pClickSprite = cc.Scale9Sprite:createWithSpriteFrame(celNodeBg:getSpriteFrame())
        local pBgBtn = cc.ControlButton:create(pNormalSprite)
        local pBtnSize = pBgBtn:getContentSize()
        pBgBtn:setZoomOnTouchDown(false)
        pBgBtn:setBackgroundSpriteForState(pClickSprite,cc.CONTROL_STATE_HIGH_LIGHTED)
        pBgBtn:setScrollSwallowsTouches(false)
        pBgBtn:setCheckScissor(true)

        pBgBtn:setPosition(cc.p(btnSize.width/2, btnSize.height/2))
        pBgBtn:setTag(index);
        _btnNode:addChild(pBgBtn);
        -- 添加响应
        local function onButtonClicked(sender)
            self:TouchListItem(index)
        end
        pBgBtn:registerControlEventHandler(onButtonClicked, cc.CONTROL_EVENTTYPE_TOUCH_UP_INSIDE);

		-- 房间名称
        local roomName = cc.Label:createWithBMFont("hall/font/xz_fh_num.fnt","")
        local str = string.format("%d号房",index)
		roomName:setString(str)
		roomName:setAnchorPoint(cc.p(0.5, 0))
		roomName:setPosition(cc.p(150, 70))
		_btnNode:addChild(roomName)

		-- 在线人数
        --local strNum = string.format(LuaUtils.getLocalString("STRING_188"),roomInfo.dwOnLineCount, roomInfo.dwFullCount)
        --local lbUsersNum = cc.Label:createWithSystemFont(strNum, "Helvetica", 24);
        --lbUsersNum:setName("OnLineCount")
        --lbUsersNum:setAnchorPoint(cc.p(0.5, 0.5));
        --lbUsersNum:setPosition(cc.p(145, 38));
        --_btnNode:addChild(lbUsersNum)

--        local number = self:getOnlineCountString(roomInfo.dwOnLineCount, roomInfo.dwFullCount)
--        local lbUsersNum = cc.Label:createWithSystemFont(strNum, "Helvetica", 24);
--        lbUsersNum:setName("OnLineCount")
--        lbUsersNum:setAnchorPoint(cc.p(0.5, 0.5));
--        lbUsersNum:setPosition(cc.p(125, 38));
--        _btnNode:addChild(lbUsersNum)

        --在线状态
        local path_player = 
        {
            "hall/plist/roomChoose/gui-image-game-hot.png",
            "hall/plist/roomChoose/gui-image-game-more.png",
            "hall/plist/roomChoose/gui-image-game-normal.png",
            "hall/plist/roomChoose/gui-image-game-fast.png",
        }
        local percent = roomInfo.dwOnLineCount / roomInfo.dwFullCount * 100

        local spPath = path_player[4]
        if     percent >= 80 then spPath = path_player[1]
        elseif percent >= 60 then spPath = path_player[2]
        elseif percent >= 40 then spPath = path_player[3]
        end
        local spState = cc.Sprite:createWithSpriteFrameName(spPath)
        spState:setPosition(150, 40)
        spState:addTo(_btnNode)

        --full标志
--        local spFull = cc.Sprite:createWithSpriteFrameName("gui-room-icon.png");
--		spFull:setPosition(cc.p(190, 101))
--        spFull:setName("FullFlag")
--		_btnNode:addChild(spFull)
--		if roomInfo.dwOnLineCount >= roomInfo.dwFullCount then
--			spFull:setVisible(true)
--        else
--            spFull:setVisible(false)
--		end

        --是否彩金房
        local isCj = string.find(roomInfo.szServerName, "彩金")
        if isCj then
            local spFlag = cc.Sprite:createWithSpriteFrameName("hall/plist/hall/image-jackpot-flag.png")
		    spFlag:setPosition(cc.p(btnSize.width - 65, 65)) 
            spFlag:addTo(_btnNode)
        end

        -- 下一个按纽的位置
        nStartX = nStartX + nOffX
        if index % 2 == 0 then
            nStartX = STARTX;
            nStartY = nStartY + nOffY;
        end
    end

    -- 创建滑动节点
    self.m_root_SV = cc.ScrollView:create(scrollViewSize, self.m_InnerNode)
    self.m_root_SV:setDirection(1) -- 0 水平， 1 垂直， 2 都是可以
    self.m_root_SV:setContentSize(cc.size(scrollViewSize.width, maxHight))
    self.m_root_SV:setContentOffset(cc.p(0, -(maxHight - scrollViewSize.height)))
    self.m_pSVNode:addChild(self.m_root_SV)
    --self.m_root_SV:setTouchEnabled(true)
    local function scrollViewDidScroll()
--        if self:getScrollViewCellCount() < self.m_nShowArrowNeedCount then
--            return
--        end
        self.isScroll = true
        local offest = self.m_root_SV:getContentOffset()
--        print(offest.x, offest.y )
        local slider = tolua.cast(self.m_pNodeRootUI:getChildByTag(100), "cc.ControlSlider")
        if not slider then
            return
        end
    
        local max = 138 * (self:getScrollViewCellCount() - 3)
        slider:setValue((offest.y + max))

--        if offest.y > - 140 then
--            self.m_pBtnUpArrow:setVisible(true)
--            self.m_pBtnDownArrow:setVisible(false)
--        end

--    if offest.y <  - 140* ( self:getScrollViewCellCount() - self.m_nShowArrowNeedCount )  then
--            self.m_pBtnUpArrow:setVisible(false)
--            self.m_pBtnDownArrow:setVisible(true)
--        end
    end
    self.m_root_SV:setDelegate()
    self.m_root_SV:setClippingToBounds(true)
    self.m_root_SV:setBounceable(true)
    self.m_root_SV:registerScriptHandler(scrollViewDidScroll,cc.SCROLLVIEW_SCRIPT_SCROLL)
end

--返回滑动列表显示行数
function GameRoomChooseDlg:getScrollViewCellCount()
    self.m_vecRoomList = self:getRoomList(self.m_nGameKindID, self.m_nBaseScore)
    local roomCount = #self.m_vecRoomList
    local cellCount = (roomCount%2 == 0 and roomCount/2 or math.ceil(roomCount/2))
    return cellCount
end

-- 选中房间
function GameRoomChooseDlg:TouchListItem(_index)

    if self.m_nIsTouchOut then
        return
    end

    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    -- 防连点
    local nCurTime = os.time()
    if self.m_nLastTouchTime and nCurTime - self.m_nLastTouchTime <= 0 then
        return
    end
    self.m_nLastTouchTime = nCurTime

    -- 房间不存在
    local tmp = self.m_vecRoomList[_index]
    if next(tmp) == nil then 
        return 
    end

    self:setCallBackInClose(function()
        --到hallSceneLayer 发送登录游戏请求
        local _event = {
            name = Hall_Events.MSG_HALL_CLICKED_GAME,
            packet = tmp,
        }
        SLFacade:dispatchCustomEvent(Hall_Events.GAMELIST_CLICKED_GAME, _event)
    end)

    self:onMoveExitView()
end

function GameRoomChooseDlg:onUpdateRoomOnline()
    self.m_vecRoomList = self:getRoomList(self.m_nGameKindID, self.m_nBaseScore)

    for k,v in pairs(self.m_vecRoomList) do
        local node = self.m_InnerNode:getChildByTag(k)
        if node then 
            local roomInfo = v

--            local num = node:getChildByName("OnLineCount")
--            local strNum = self:getOnlineCountString(roomInfo.dwOnLineCount, roomInfo.dwFullCount)
--            num:setString(strNum)

--            local flag = node:getChildByName("FullFlag")
--            if roomInfo.dwOnLineCount >= roomInfo.dwFullCount then
--                flag:setVisible(true)
--            else
--                flag:setVisible(false) 
--            end
        end
    end
end

function GameRoomChooseDlg:onCloseBtnClick(pSender)
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    local self = GameRoomChooseDlg.instance

    self:onMoveExitView()
end

function GameRoomChooseDlg:createSlider()
    local spBg = cc.Sprite:createWithSpriteFrameName("gui-room-scroll-bg.png")
    local pgSp = cc.Sprite:createWithSpriteFrameName("gui-room-null.png")
    local spTub = cc.Sprite:createWithSpriteFrameName("gui-room-scroll-btn.png")
    local spTub1 = cc.Sprite:createWithSpriteFrameName("gui-room-scroll-btn.png")
    local slider = cc.ControlSlider:create(spBg, pgSp, spTub, spTub1)
    if not slider then
        return false
    end
    
    slider:setAnchorPoint(cc.p(0.5, 0.5))
    slider:setMinimumValue(0)
    local max = 138 * (self:getScrollViewCellCount() - 3)
    local maxVaule = (max > 0 and max or 0)
    slider:setMaximumValue(maxVaule)
    slider:setPosition(cc.p(1055, 344))
    slider:setRotation(90)
    slider:setValue(slider:getMinimumValue())
    slider:setTag(100)
    slider:setScaleX(0.9)
    slider:setEnabled(false)
    self.m_pNodeRootUI:addChild(slider, 50)

    if (self:getScrollViewCellCount() - 3) <= 0 then
        slider:setVisible(false)
    end

--    local function onValuedChanged(sender)
--        if self.isScroll then self.isScroll = false return end
--        local num = sender:getValue()
--        print(" onValuedChanged ", num)
--        local y = num - 138
--        local percent = y / (138 * (self:getScrollViewCellCount() - 3))
--        self.m_root_SV:jumpToPercentHorizontal(percent)
--    end
--    slider:registerControlEventHandler(onValuedChanged, cc.CONTROL_EVENTTYPE_VALUE_CHANGED)
end

function GameRoomChooseDlg:onMsgLoginGameSuccess()
    --删掉界面
    self:removeFromParent()
end

function GameRoomChooseDlg:getOnlineCountString(dwOnLineCount, dwFullCount)
    
    local nCount = dwOnLineCount / dwFullCount * 100
    local strNum = "空闲"
    if     nCount < 50 then strNum = "空闲"
    elseif nCount < 80 then strNum = "拥挤"
    else                    strNum = "爆满"
    end
    return strNum
end

function GameRoomChooseDlg:getOnlineCountPng(dwOnLineCount, dwFullCount)

    local nCount = dwOnLineCount / dwFullCount * 100
    local strNum = "空闲"
    if     nCount < 50 then strNum = "icon-room-empty.png"
    elseif nCount < 80 then strNum = "icon-room-busy.png"
    else                    strNum = "icon-room-full.png"
    end
    return strNum
end

return GameRoomChooseDlg
