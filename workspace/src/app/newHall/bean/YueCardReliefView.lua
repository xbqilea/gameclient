
local YueCardReliefView = class("YueCardReliefView",FixLayer)

function YueCardReliefView:ctor()
	self.super:ctor(self)
	self:enableNodeEvents()
	self:init()
	self:initBtnEvent()
    self:initMsgEvent()
end

function YueCardReliefView:init()
	self.m_pathUI = cc.CSLoader:createNode("hall/csb/HallYueCardReliefView.csb")
    self:addChild(self.m_pathUI)

    local diffY = (display.height - 750)/2
    self.m_pathUI:setPositionY(diffY)
    self.m_pNodeRoot = self.m_pathUI:getChildByName("Image_bg")
    self.m_pNodeRoot:setPositionX(display.width/2)

    self.m_pPanelClose = self.m_pathUI:getChildByName("Panel_1")
    self.m_pBtnClose = self.m_pNodeRoot:getChildByName("Button_sure")
    self.m_pGoldNum = self.m_pNodeRoot:getChildByName("gold_num")

    local tipRichText = ccui.RichText:create()
    local size = 26
    local opacity = 255
    local font = ""

    local tip_text1 = cc.Label:createWithSystemFont("系统第", font, size)
   	local lb = ccui.RichElementCustomNode:create(0, cc.c3b(0xff,0xff,0xff), opacity, tip_text1)
    tipRichText:pushBackElement(lb)

    self.tip_text2 = cc.Label:createWithSystemFont("", font, size)
    local lb2 = ccui.RichElementCustomNode:create(0, cc.c3b(0x1e,0xf8,0xdc), opacity, self.tip_text2)
    tipRichText:pushBackElement(lb2)


    self.tip_text3 = cc.Label:createWithSystemFont("次【V8用户每日", font, size)
   	local lb3 = ccui.RichElementCustomNode:create(0, cc.c3b(0xff,0xff,0xff), opacity, self.tip_text3)
    tipRichText:pushBackElement(lb3)

    self.tip_text4 = cc.Label:createWithSystemFont("", font, size)
   	local lb4 = ccui.RichElementCustomNode:create(0, cc.c3b(0x1e,0xf8,0xdc), opacity, self.tip_text4)
    tipRichText:pushBackElement(lb4)

    local tip_text5 = cc.Label:createWithSystemFont("次】赠送金币：", font, size)
   	local lb5 = ccui.RichElementCustomNode:create(0, cc.c3b(0xff,0xff,0xff), opacity, tip_text5)
    tipRichText:pushBackElement(lb5)

    tipRichText:setAnchorPoint(0,0.5)
    tipRichText:move(370,224)
    self.m_pNodeRoot:addChild(tipRichText)
end

function YueCardReliefView:initMsgEvent()
    self.msg_event = {
        [Hall_Events.CHANGE_LANDSCAPE_SCREEN] = {func = self.changeLandscapeScreen,log = "屏幕旋转"},
    }

    local function onMsgUpdate(event)  --接收自定义事件
        local name = event:getEventName()
        local msg = unpack(event._userdata)
        local func = self.msg_event[name].func
        func(self, msg)
    end

    for key, event in pairs(self.msg_event) do   --监听事件
         SLFacade:addCustomEventListener(key, onMsgUpdate, self.__cname)
    end
end

function YueCardReliefView:deleteMsgEvent()
    for key in pairs(self.msg_event) do   --监听事件
         SLFacade:removeCustomEventListener(key, self.__cname)
    end
    self.msg_event = {}
end

function YueCardReliefView:initBtnEvent()
	self.m_pBtnClose:addClickEventListener(handler(self,self.onCloseClicked))
	self.m_pPanelClose:addClickEventListener(handler(self,self.onCloseClicked))
end

function YueCardReliefView:onEnter()
	self.super:onEnter()
	self:setTargetShowHideStyle(self,FixLayer.SHOW_DLG_BIG,FixLayer.HIDE_DLG_BIG)
	self:showWithStyle()
end

function YueCardReliefView:onExit()
	self.super:onExit()
    self:deleteMsgEvent()
end

function YueCardReliefView:onCloseClicked()
	AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
	self:onMoveExitView()
end

function YueCardReliefView:initData()
    local yue_card_data = PlayerInfo.getInstance():getMonthCardData()
    if not yue_card_data then return end
	self.tip_text2:setString(yue_card_data.usHaveDoledTimes)
	self.tip_text3:setString(string.format("次【V%d用户每日",PlayerInfo.getInstance():getVipLevel()))
	self.tip_text4:setString(yue_card_data.usAllCanDoleTimes)
    --设置领取的金币数量
    local gold_score = PlayerInfo.getInstance():getReliefScore()
    self.m_pGoldNum:setString(gold_score)
end

function YueCardReliefView:changeLandscapeScreen()
    if self.m_pBgLayer then
        self.m_pBgLayer:setContentSize(cc.size(1624,750))
    end
end

return YueCardReliefView