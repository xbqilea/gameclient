--region AnnouncementMsg.lua

local UsrMsgManager     = require("common.manager.UsrMsgManager")
local CBroadcastManager = require("common.manager.CBroadcastManager")

local AnnouncementMsg = class("AnnouncementMsg", FixLayer)

local TAG_IN_HALL = 0
local TAG_IN_NOTICE = 1

function AnnouncementMsg:ctor()
    self.super:ctor(self)
    self:enableNodeEvents()
    self:init()
end

function AnnouncementMsg:onEnter()
    self.super:onEnter()

    self:setTargetShowHideStyle(self, FixLayer.SHOW_DLG_BIG, FixLayer.HIDE_DLG_BIG)
    self:showWithStyle()
end

function AnnouncementMsg:onExit()
    self.super:onExit()
    if self.m_nType == TAG_IN_HALL and CBroadcastManager.getInstance():getPopBroadcastCount() > 0 then 
        SLFacade:dispatchCustomEvent(Public_Events.MSG_POP_NOTICE)
    end
end

function AnnouncementMsg:init()
    self:initCSB()
    self:initNode()
    self:initBtnEvent()
end

function AnnouncementMsg:initCSB()

    --root
    self.m_rootUI = display.newNode()
    self.m_rootUI:setCascadeOpacityEnabled(true)
    self.m_rootUI:addTo(self)

    --csb
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/AnnouncementMsg.csb")
    self.m_pathUI:setPositionY((display.height - 750) / 2)
    self.m_pathUI:addTo(self.m_rootUI)

    --offset
    self.m_pNodeRoot = self.m_pathUI:getChildByName("MessageView")
    self.m_pNodeRoot:setPositionX((display.width - 1334) / 2)

    --node
    self.m_pImageBg = self.m_pNodeRoot:getChildByName("Image_bg")
    self.m_pMsgView = self.m_pImageBg:getChildByName("ScrollView_Msg")
    self.m_pBtnReturn  = self.m_pImageBg:getChildByName("BTN_close")
    self.m_pTextWebsite = self.m_pImageBg:getChildByName("Text_Website")
    self.m_pBtnNull     = self.m_pathUI:getChildByName("Panel_2") --空白处关闭

    --size
    self.m_size_view = self.m_pMsgView:getContentSize()
end

function AnnouncementMsg:initNode()
    
    self.m_pMsgView:setScrollBarEnabled(false)

    self.m_pLayerView = cc.Layer:create()
    self.m_pLayerView:addTo(self.m_pMsgView)
end

function AnnouncementMsg:initBtnEvent()

    self.m_pBtnReturn:addClickEventListener(handler(self, self.onReturnClicked))
    self.m_pBtnNull:addClickEventListener(handler(self, self.onReturnClicked))
end

-- 关闭
function AnnouncementMsg:onReturnClicked()
    AudioManager:getInstance():playSound("public/sound/sound-close.mp3")
    
    --关闭公告
    cc.exports.SLFacade:dispatchCustomEvent(Public_Events.MSG_CLOSE_NOTICE_LAYER, self.m_msg)

    --关闭界面
    self:onMoveExitView()
end

--官网
function AnnouncementMsg:onSiteTouch(sender,eventType)
    if eventType ~= ccui.TouchEventType.ended then
           return
    end
    AudioManager:getInstance():playSound("public/sound/sound-button.mp3")

    local strWebsite = PlayerInfo.getInstance():getOfficialWebsite()
    LuaNativeBridge.getInstance():setCopyContent(strWebsite)
    FloatMessage.getInstance():pushMessage("STRING_036_1")
end

-- 设置内容 type 0:大厅弹出  1：公告界面弹出
function AnnouncementMsg:setMessage(msg, nType)

    self.m_nType  = nType
    self.m_msg    = msg
    local title   = self.m_msg.title
    local content = self.m_msg.content

    --notice
    --local strWebsite = PlayerInfo.getInstance():getOfficialWebsite()
    --local string_notice = {
    --    "欢迎关注玖玖棋牌游戏，请牢记官网链接：",
    --    strWebsite,
    --    "，系统赠送你",
    --    "3.00元",
    --    "，赶紧去体验游戏吧。",
    --}

    --local color = {
    --    cc.c3b(108, 57, 29),
    --    cc.c3b(91, 156, 193), --蓝色
    --    cc.c3b(108, 57, 29),
    --    cc.c3b(255, 120, 0),  --橙色
    --    cc.c3b(108, 57, 29),
    --}

    --local lb1 = ccui.RichText:create()
    --for i = 1, table.nums(string_notice) do 
    --    local richStr = nil
    --    if i == 2 then 
    --        richStr = ccui.RichElementText:create(0, color[i], 255, string_notice[i], "Helvetica", 24, 4, string_notice[i], color[i])
    --    else
    --        richStr = ccui.RichElementText:create(0, color[i], 255, string_notice[i], "Helvetica", 24)
    --    end 
    --    lb1:pushBackElement(richStr)
    --end
    --lb1:ignoreContentAdaptWithSize(false)
    --lb1:setContentSize(cc.size(650,80))
    --lb1:setAnchorPoint(0, 1)
    --lb1:addTo(self.m_pLayerView)

--    --置顶
--    local string_notice = "　　欢迎来到我们的游戏，我们秉承公平、公正、安全、稳定的服务宗旨，竭诚为广大牌友提供一个绝对公平的在线游戏互动平台。"
--    local lb1 = cc.Label:createWithTTF(string_notice, FONT_TTF_PATH, 26) 
--    lb1:setAnchorPoint(0, 1)
--    lb1:setDimensions(690, 0)
--    lb1:setColor(cc.c3b(0xff, 0xff, 0xff))
--    lb1:addTo(self.m_pLayerView)

    --title
    local lb2 = cc.Label:createWithTTF(title, FONT_TTF_PATH, 26) 
    lb2:setAnchorPoint(0, 1)
    lb2:setDimensions(650, 0)
    lb2:setColor(cc.c3b(0x5a, 0xfd, 0xff))
    lb2:addTo(self.m_pLayerView)

    --content
    local lb3 = cc.Label:createWithTTF(content, FONT_TTF_PATH, 26) 
    lb3:setAnchorPoint(0, 1)
    lb3:setDimensions(590, 0)
    lb3:setColor(cc.c3b(0xff, 0xff, 0xff))
    lb3:addTo(self.m_pLayerView)

    local height =  5
    local height2 = lb2:getContentSize().height + 15
    local height3 = lb3:getContentSize().height
    local h = math.max(height + height2 + height3, self.m_size_view.height)
    --lb1:setPosition(30, h)
    lb2:setPosition(30 + 50, h - height)
    lb3:setPosition(30 + 50, h - height - height2)
    self.m_pMsgView:setInnerContainerSize(cc.size(self.m_size_view.width, h))

    --官网
    self.m_pTextWebsite:setString("官网地址:"..PlayerInfo.getInstance():getOfficialWebsite())
    self.m_pTextWebsite:addTouchEventListener(handler(self, self.onSiteTouch))
    --官网下面的线
    local lineWidth = self.m_pTextWebsite:getContentSize().width
    local posX = self.m_pTextWebsite:getPositionX()
    local lineNode = cc.DrawNode:create()
    lineNode:drawSolidRect(cc.p(0, 0), cc.p(lineWidth, 2), cc.c4f(90/255,1,1,1))
    lineNode:setPosition(cc.p(posX - lineWidth/2, 65))
    self.m_pImageBg:addChild(lineNode)

    --隐藏官网
    local index = G_CONSTANTS.GAMESWITCH.CLOSE_WEBSITE_SHOW
    local isClosedWebSite = GameListManager.getInstance():getGameSwitch(index)
    if isClosedWebSite then
        self.m_pTextWebsite:setVisible(false)
        lineNode:setVisible(false)
    end
end

return AnnouncementMsg
