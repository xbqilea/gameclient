--ZCShopView
--招财猫商城
local NetSprite = require("app.hall.base.ui.UrlImage")
local BaseFullLayer = import("..common.BaseFullLayer")
local DTCommonWords = import("..common.DTCommonWords") 
local CommonNetImageView =import("..common.CommonNetImageView")  
local PopupEffect = import("..common.PopupEffect") 
local PayViewLayer = import(".PayViewLayer")
local ZCShopTip = import(".ZCShopTip")
local platformUtils = require("app.hall.base.util.platform") 
local FeedBackLayer = import("..childLayer.FeedBackLayer")
import("..data.PayData")
local uiCheck = nil
local btnHelp = nil
local editTxt = nil
local _ = {}
local PayItem = class("PayItem")
local HNLayer = import("..HNLayer")
local ZCShopView = class("ZCShopView",function () 
return HNLayer.new()

end)
 
 import("..common.AdapterUtil")
ZCShopView.RESOURCE_FILENAME = "hall/ZCShop.csb"

local VIP_TAB = 1
local TAB_QQQrCode = 13
local TAB_DIAN_KA = 14
local TAB_BEIYONG_PAY = 16

local PAY_INFO = {
	ALIPAY = { id = 1, image = "zhifubao.png" },		--00000001：支持支付宝
	WECHAT = { id = 2, image = "weichat.png" },		--00000010：支持微信
	YUN_SHAN = { id = 4, image = "yunshanfu.png" }, 		--00000100：支持云闪付
	UNION_PAY = { id = 8, image = "yinlian.png" }, 	--00001000：支持银联
}


-- local ZXKF_URL = "http://192.168.1.184:8996/pay-front/" 
-- local ZXKF_URL = GlobalConf.DOMAIN
local ZXKF_URL = "https://payfront.xbqpthe.com/pay-front/"

local getZXKFKey = function()
	if 1020001001 == GlobalConf.CHANNEL_ID then
		return "fz90jceev7ovszp3"
	elseif 1001 == GlobalConf.CHANNEL_ID then
		-- return "lpvgm8u5y4lefr0z"
	elseif 1002 == GlobalConf.CHANNEL_ID then
		return "ncy9dou8daetwjso"
	elseif 1003 == GlobalConf.CHANNEL_ID then
		return "brfdavh2vuajqp3h"
	elseif 1004 == GlobalConf.CHANNEL_ID then
		return "rdrww4lyvkxf6cwz"
	elseif 1005 == GlobalConf.CHANNEL_ID then
	else
	end
	return nil
end

function ZCShopView:ctor() 
    self.root = UIAdapter:createNode(ZCShopView.RESOURCE_FILENAME)
    
    
    self:addChild(self.root)
 --   AdapterUtil.Scene( self.root)
    local center = self.root:getChildByName("center")
     local diffY = (display.size.height - 750) / 2
    self.root:setPosition(cc.p(0,diffY)) 
    local diffX = 145 - (1624-display.size.width)/2
    center:setPositionX(diffX)
    UIAdapter:parseCSDNode(self.root,self)
    UIAdapter:adapter(self.root, handler(self, self.onTouchCallback)) 
   --  UIAdapter:praseNode(self.root,self)
         local image_Bg = center:getChildByName("image_Bg") 
    self.defaultPlaceHolder = ""
    self.payType1 = {}
    self.vipAccount = {}
    self.zfbAccount = {}
    self.wxAccount = {}
    self.byAccount = {}
	self.yhkAccount = {}
	self.zxkfAccout = {}

	self.shopListData = {}

	self.nowPayType = 0
	self.waitLabel:setVisible(false)
	self.samplePayPanel:setVisible(false)
	self.vipPayPanel:setVisible(false)
	self.qqQRCodePanel:setVisible(false)
	self.dianKaPanel:setVisible(false)
	self.beiYongPanel:setVisible(false)
    self.vipRefBtn:setVisible(false)
	self.btnComplain:setVisible(false)
	self.zxkfPanel:setVisible(false)
	
    self:requestShopList() 
    self.topBanner:addTouchEventListener(handler(self, self.jumpToDeasPay))
--	--cc.loaded_packages.DTHallLogic:requestShopList()

--	self.closeBtn:addTouchEventListener(handler(self, self.onClose)) 
--	self.payBtn:addTouchEventListener(handler(self, self.payCall))
--	self.clearInputBtn:addTouchEventListener(handler(self, self.clearInput))
--	self.inputBg:addTouchEventListener(handler(self, self.openInput))
--	self.changeDianKaBtn:addTouchEventListener(handler(self, self.showAllDiankaList))
--	self.copyBtn:addTouchEventListener(handler(self, self.onCopyAccount))
	--self.tabBtnList:setScrollBarEnabled(false)
--	self.vipRefBtn:addTouchEventListener(handler(self, self.onVipRefRank))


	self.clearInputBtn:setLocalZOrder(1)

	self.payField:addEventListener(handler(self, self.updataInsert))
     local pSpine = "hall/effect/325_shangchengjiemiandonghua/325_shangchengjiemiandonghua"
    self.m_pAniTitle = sp.SkeletonAnimation:createWithBinaryFile(pSpine .. ".skel", pSpine .. ".atlas", 1)
    self.m_pAniTitle:setPosition(cc.p(display.width/2,368))
    self.root:addChild(self.m_pAniTitle)
	self.m_pAniTitle:setAnimation(0, "animation1", true)


	self.zxkfItemTmp = self.zxkfPanel:getChildByName("zxkfItemTmp")
	self.zxkfScrollView = self.zxkfPanel:getChildByName("zxkfScrollView")
	-- self.zxkfContent = self.zxkfScrollView:getChildByName("zxkfContent")
	self.zxkfScrollView:removeChildByName("zxkfContent")
	-- self.zxkfContent = nil
	self.zxkfItemTmp:setVisible(false)

	local node_3 = self.zxkfPanel:getChildByName("Node_3")
	node_3:setVisible(false)
	-- for index = 2, 4 do
	-- 	self.zxkfPanel["CheckBox_"..index] = node_3:getChildByName("CheckBox_"..index)
	-- 	local tabel = self.zxkfPanel["CheckBox_"..index]:getChildByName("tabel")
	-- 	self.zxkfPanel["CheckBox_"..index].tabel = tabel
	-- 	tabel:setName("CheckBox_"..index)
	-- 	UIAdapter:registClickCallBack(self.zxkfPanel["CheckBox_"..index], handler(self, self.onCheckBoxClick))
	-- 	UIAdapter:registClickCallBack(tabel, handler(self, self.onCheckBoxClick))
	-- end
	-- -- self.zxkfPanel.CheckBox_2 = node_3:getChildByName("CheckBox_2")
	-- -- self.zxkfPanel.CheckBox_3 = node_3:getChildByName("CheckBox_3")
	-- -- self.zxkfPanel.CheckBox_4 = node_3:getChildByName("CheckBox_4")
	-- self.zxkfPanel.CheckBox_4:setEnabled(false)
	-- self.zxkfPanel.CheckBox_4.bIsSelected = true
	-- self.zxkfPanel.RefreshButton = node_3:getChildByName("RefreshButton")
	-- UIAdapter:registClickCallBack(self.zxkfPanel.RefreshButton, function(taget)
	-- 	local end_time = os.clock()
	-- 	if taget.clock_time and (end_time - taget.clock_time) < 0.2 then
	-- 		return
	-- 	end	
	-- 	taget.clock_time = end_time
	-- 	self.zxkfPanel.RefreshButton:setEnabled(false)
	-- 	if self.chatInitData then
	-- 		self:initZXKFLIST()
	-- 	else
	-- 		self:initZXKF()
	-- 	end
		
	-- end, true)
	
	local panelSize = self.zxkfPanel:getContentSize()
	local imageView = ccui.ImageView:create("hall/image/chongzhi/zxkf/shuoming.png")
	self.zxkfPanel:addChild(imageView, 1)
	imageView:ignoreContentAdaptWithSize(true)
	imageView:setAnchorPoint(cc.p(0.5, 0))
	imageView:setPosition(cc.p(panelSize.width / 2, 0))

	local imageViewSize = imageView:getContentSize()

	local button = ccui.Button:create("hall/image/chongzhi/zxkf/tousu.png","hall/image/chongzhi/zxkf/tousu.png","hall/image/chongzhi/zxkf/tousu.png")
	button:ignoreContentAdaptWithSize(true)
	imageView:addChild(button)
	button:setPosition(cc.p(imageViewSize.width - 100, imageViewSize.height / 2))

	UIAdapter:registClickCallBack(button, function() 
		--客服界面
		local targetPlatform = cc.Application:getInstance():getTargetPlatform()
		if (cc.PLATFORM_OS_ANDROID == targetPlatform) then
            self:kefuReq()
        else
			local FeedBackLayer = FeedBackLayer.new()
			FeedBackLayer:open(ACTION_TYPE_LAYER.FADE, self,cc.p(0, 0), 999, 1000,true);
		end
	end, false)

	self.zxkfScrollView:setAnchorPoint(cc.p(0.5, 0))
	self.zxkfScrollView:setContentSize({width=965, height=455})
	self.zxkfScrollView:setPosition(cc.p(panelSize.width / 2, imageViewSize.height + 3))
	self.zxkfScrollView:setItemsMargin(3)
	if getZXKFKey() ~= nil then
		self:initZXKF()
	end
end

function ZCShopView:kefuReq()
    local url = string.format(GlobalConf.DOMAIN .. "api/itf/cs/info?channelId=" .. GlobalConf.CHANNEL_ID)

    print(url)
    local xhr = cc.XMLHttpRequest:new()
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
    xhr:open("GET", url)

    local function onReadyStateChange()
        local response = xhr.response
        if tolua.isnull(self) or response == "" then
            --  self:kefuReq()
            print("response = nil")
        else

            local output = json.decode(response, 1)
            dump(output)
            if output.data then
                for k, v in pairs(output.data) do
                    if v.type == 3 then
                        device.openURL(v.accountOrUrl)
                    end
                end
            end
        end
    end
    xhr:registerScriptHandler(onReadyStateChange)
    xhr:send()
end


function ZCShopView:requestShopList()
     local xhr = cc.XMLHttpRequest:new()
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
    xhr:open("GET", GlobalConf.DOMAIN.."api/itf/pay/channel?channelId="..GlobalConf.CHANNEL_ID)
    print(GlobalConf.DOMAIN.."api/itf/pay/channel?channelId="..GlobalConf.CHANNEL_ID) 
    ToolKit:addLoadingDialog(5,"正在加载中，请稍等......")
    local function onReadyStateChange()  
		ToolKit:removeLoadingDialog()
		if not tolua.isnull(self) then
			if xhr.response == nil or xhr.response == "" then
				-- self:requestShopList()
				print("response = nil")
				self:initTabSelectByURL({})
			else
				local response   = xhr.response
				local output = json.decode(response,1) 
				dump(output)  
				self.shopListData = output
				self:initTabSelectByURL(output)
			end
		end
    end
    xhr:registerScriptHandler(onReadyStateChange)
    xhr:send()       
    print("waiting...")
end

function ZCShopView:initZXKF()
	local xhr = cc.XMLHttpRequest:new()
	-- xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
	xhr:setRequestHeader("Content-Type", "application/json")
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON
	local jsonData = {
		channelId = GlobalConf.CHANNEL_ID,
		nickName = Player:getNickName(),
		platformType = 1,
		userId = Player:getAccountID(),
		key = getZXKFKey()
	}
	local jsonstr = "channelId="..GlobalConf.CHANNEL_ID.."&nickName="..Player:getNickName().."&platformType=1&userId="..Player:getAccountID().."&key="..getZXKFKey()
	jsonData.sgin = crypto.md5(jsonstr)
	jsonstr = json.encode(jsonData)
	print(jsonstr)
	
	-- local postData = "platformType:1,userId:"..Player:getAccountID()..",channelId:"..GlobalConf.CHANNEL_ID..",nickName:"..Player:getNickName()
	-- postData = postData..",sgin:"..crypto.md5(postData)
    xhr:open("POST", ZXKF_URL.."chatApi/initUser")
    -- print(ZXKF_URL.."chatApi/initUser"..postData)
    -- ToolKit:addLoadingDialog(5,"正在加载中，请稍等......")
    local function onReadyStateChange()  
		--ToolKit:removeLoadingDialog()
		local response   = xhr.response
		if tolua.isnull(self) then
			return
		end
        if response == nil or response == "" then
          	-- self:requestShopList()
			  print("response = nil")
			--   self.zxkfPanel.RefreshButton:setEnabled(true)
        else
            
            local output = json.decode(response,1) 
			dump(output)
			if output["code"] == 0 then
				self.chatInitData = output["data"]
				self:initZXKFLIST()
			else
				-- self.zxkfPanel.RefreshButton:setEnabled(true)
				TOAST(output["msg"])
			end
			
            -- self:initTabSelectByURL(output)
        end
    end
    xhr:registerScriptHandler(onReadyStateChange)
    xhr:send(jsonstr)
end

function ZCShopView:initZXKFLIST()
	if self.chatInitData == nil then
		return
	end

	local xhr = cc.XMLHttpRequest:new()
	-- xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
	xhr:setRequestHeader("Content-Type", "application/json")
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON
	local jsonData = {
		channelId = GlobalConf.CHANNEL_ID,
		isOnLine =  3,
		--orgId = self.chatInitData.id,
		key = getZXKFKey()
	}
	-- local jsonstr = "channelId="..GlobalConf.CHANNEL_ID.."&isOnLine=3&orgId="..self.chatInitData.id.."&key="..getZXKFKey()
	local jsonstr = "channelId="..GlobalConf.CHANNEL_ID.."&isOnLine=3&key="..getZXKFKey()
	jsonData.sgin = crypto.md5(jsonstr)
	jsonstr = json.encode(jsonData)
	print(jsonstr)

    xhr:open("POST", ZXKF_URL.."chatApi/getChannelCsList")
    print(ZXKF_URL.."chatApi/getChannelCsList?"..jsonstr)
    -- ToolKit:addLoadingDialog(5,"正在加载中，请稍等......")
    local function onReadyStateChange()  
		--  ToolKit:removeLoadingDialog()
		if tolua.isnull(self) then
			return
		end
		-- self.zxkfPanel.RefreshButton:setEnabled(true)
		local response   = xhr.response
        if response == nil or response == "" then
          	-- self:requestShopList()
          	print("response = nil")
        else
            
            local output = json.decode(response,1) 
			dump(output)
			if output["code"] == 0 then
				local data = output["data"]
				self.zxkfAccout = data
				-- local online2 = {}
				-- for __,info in pairs(data) do
				-- 	if info.isOnline == 0 then
				-- 		table.insert(online2, info)
				-- 	else
				-- 		table.insert(self.zxkfAccout, info)
				-- 	end
				-- end
				-- for __,info in pairs(online2) do
				-- 	table.insert(self.zxkfAccout, info)
				-- end

				-- self.zxkfAccout = output["data"]
				self:updateZXKFData()
				-- self:initTabSelectByURL(self.shopListData)
			else
				TOAST(output["msg"])
			end
            -- self:initTabSelectByURL(output)
        end
    end
    xhr:registerScriptHandler(onReadyStateChange)
    xhr:send(jsonstr)
end

function ZCShopView:onCheckBoxClick(sender, eventType)
	local name = sender:getName()
	for index = 2, 4 do
		local keyName = "CheckBox_"..index 
		if keyName ~= name then
			self.zxkfPanel["CheckBox_"..index]:setEnabled(true)
			self.zxkfPanel["CheckBox_"..index].tabel:setEnabled(true)
			if self.zxkfPanel["CheckBox_"..index].bIsSelected then
				self.zxkfPanel["CheckBox_"..index]:setSelected(false)
				self.zxkfPanel["CheckBox_"..index].bIsSelected = false
			end
		end
		self.zxkfPanel[name].tabel:setEnabled(false)
		self.zxkfPanel[name]:setEnabled(false)
		self.zxkfPanel[name].bIsSelected = true
		if self.zxkfPanel[name] ~= sender then
			self.zxkfPanel[name]:setSelected(true)
		end
		
	end
	self:updateZXKFData()
end

function ZCShopView:onTouchCallback(sender)
	local name = sender:getName()
    print(name)
	if name == "button_closeBtn" then
		self:close()
 
	elseif name == "button_clearInputBtn" then
	      self:clearInput()
	elseif name == "button_payBtn" then
		self:payCall() 
    elseif name == "buttion_copyBtn" then
        self:onCopyAccount()
    elseif name == "button_copyRid" then
        self:onCopyRid()
    elseif name == "copyId" then
        self:onCopyBeiYongRid()
     elseif name == "openbeiyongPay" then
        self:onBeiyongPay()
    elseif name == "button_btnCopyQq" then
        self:onCopyQq()
--    elseif name == "button_vipRefBtn" then
--        self:onVipRefRank()
    elseif name == "button_changeDianKaBtn" then
        self:showAllDiankaList()
    elseif name == "button_dkService" then
        self:onClickDianKaBuyGuide()
    elseif name == "button_dkNameItem" then
        self:onClickDianKaName()
    elseif name == "button_dkOk" then
        self:onClickDianKaOk()
    elseif name == "button_btnDkClearNum" then
        self.dkNum:setString('')
    elseif name == "button_btnDkClearPsw" then
        self.dkPsw:setString('')
	elseif name == "CheckBox_2" or name == "CheckBox_3" or name == "CheckBox_4" then
		for index = 2, 4 do
			local keyName = "CheckBox_"..index 
			if keyName ~= name then
				self.zxkfPanel["CheckBox_"..index]:setEnabled(true)
				if self.zxkfPanel["CheckBox_"..index].bIsSelected then
					self.zxkfPanel["CheckBox_"..index]:setSelected(false)
					self.zxkfPanel["CheckBox_"..index].bIsSelected = false
				end
			end
			self.zxkfPanel[name]:setEnabled(false)
			self.zxkfPanel[name].bIsSelected = true
		end
		self:updateZXKFData()
	end
end

function ZCShopView:initDianKaUi()
	 
 
	for i = 1, 8 do
		local node = self['dkC' .. i]
		local bmf = self["dkJinE"..i]
		local paneltouch = self["touchNode_"..i]
		bmf.node = node
		paneltouch.node = node 
		paneltouch:addTouchEventListener(handler(self, self.onClickDianKaJinEBMF))
		bmf:addTouchEventListener(handler(self, self.onClickDianKaJinEBMF))
		node:addEventListener(handler(self, self.onClickDianKaJinE))
		node:setSelected(false)
	end
end

function ZCShopView:openInput( ... ) 
end
--输入框事件处理  
function ZCShopView:editboxHandle(strEventName,sender)  
    if strEventName == "began" then  
    	sender:setPlaceHolder("")                                    --光标进入，清空内容/选择全部  
    elseif strEventName == "ended" then  
        if sender:getText() then
        	sender:setPlaceHolder(self.defaultPlaceHolder) 
        end                                                        --当编辑框失去焦点并且键盘消失的时候被调用  
    elseif strEventName == "return" then  
                                                                --当用户点击编辑框的键盘以外的区域，或者键盘的Return按钮被点击时所调用  
    elseif strEventName == "changed" then  
           self:updataInsert()             	                                        --输入内容改变时调用   
    end  
end  

function ZCShopView:updataInsert( ... )
	-- body
	local inputString = self.payField:getString()
	local moneyNum = tonumber(inputString) or 0
	if not tonumber(inputString) and inputString ~= "" then
		TOAST("输入了非法字符,请正确输入")
		moneyNum = -1
	end

	if moneyNum > 0  and math.floor(moneyNum) < moneyNum  then
		print("小数啦")
		TOAST("不可输入小数,请正确输入")
		moneyNum = -1
	end

	if moneyNum == -1 then
		self.payField:setString("")
	end
end

 

function ZCShopView:onCopyAccount( ... )
	-- body 
    TOAST("复制游戏账号成功,发给代理即可完成充值")
	ToolKit:setClipboardText(self.roleID:getString())
end

function ZCShopView:onCopyRid() 
    TOAST("复制成功")
	ToolKit:setClipboardText(self.ridOfQq:getString())
end

function ZCShopView:onCopyBeiYongRid() 
     TOAST("复制成功")
	ToolKit:setClipboardText(self.rid:getString())
end

function ZCShopView:onCopyQq()  
      TOAST("复制成功")
	ToolKit:setClipboardText(self.qq:getString())
end

function ZCShopView:onBuyDianKa()
 
end
function ZCShopView:onBeiyongPay()
	-- 备用充值按钮
	if CCApplication.openURL then
		local kl = self.beiyongUrl.."?id=%s"
		local content = string.format(kl,self.rid:getString())
		CCApplication:sharedApplication():openURL(content)
	end
end 

function ZCShopView:showPromptDlg()
    local ZCShopTip = ZCShopTip.new()
    ZCShopTip:open(ACTION_TYPE_LAYER.FADE, self:getParent(),cc.p(0,0), self:getLocalZOrder() + 1, CHILD_LAYER_BANK_TAG,true);
end

function ZCShopView:initTabSelectByURL(baseConfg)
    self:showPromptDlg()
 
	self.waitLabel:setVisible(false)
 	self.tabBtnList:removeAllItems()
	-- local baseConfg = cc.loaded_packages.DTCommonModel:getCommonCfg("payBankCfg")
	--local baseConfg = cc.loaded_packages.DTCommonModel:getNewShopDataInfo()
	if not baseConfg then
	--	self.waitLabel:setVisible(true)
		self.samplePayPanel:setVisible(false)
		self.vipPayPanel:setVisible(false)
		self.qqQRCodePanel:setVisible(false)
		return
	end
	-- dump(baseConfg,"baseConfg")
	-- dump(baseConfg.data, "baseConfg.data")
	-- printTable(baseConfg,"baseConfg")

	local allTab = {}
    local byTab = {}
    local wxTab = {}
    local zfbTab = {}
	local yhkTab = {}
	local zxkfTab = {}
	local function itemSelected(item, bol) 
		item:getChildByName("light"):setVisible(bol)
		item:getChildByName("unlight"):setVisible(not bol)
		if bol then
			self:showPayPanel(item.myTp.id, item.myTp.configId)
		end
	end
	local function Btn_SelectTab(obj,eventType)
        if eventType  == ccui.TouchEventType.ended then 
		    uiCheck:updateGroup(obj.tab, obj)
            self.wxItem:setVisible(false)
            self.zfbItem:setVisible(false)
            self.yhkItem:setVisible(false)
			self.byItem:setVisible(false)
			self.zxkfItem:setVisible(false)
            if obj.listView then
                obj.listView:setVisible(true)
            end
            if self.byItem:isVisible() then
                self.byItem:getParent():getChildByName("image_zhuangXiang"):setVisible(true)
            else
                self.byItem:getParent():getChildByName("image_zhuangXiang"):setVisible(false)
			end
			
        end
	end

	local function initTab(tabBtn, payTypeId,listView,tab)
		tabBtn:setVisible(true)
		listView:pushBackCustomItem(tabBtn)
		tabBtn.myTp = {id = payTypeId, configId = tabBtn.configId}
		tabBtn.setSelected = itemSelected
        tabBtn.tab = tab 
      --  tabBtn.tab.listView = listView
		tabBtn:addTouchEventListener(Btn_SelectTab)
		table.insert(tab, tabBtn)
	end 
	if baseConfg.data and #baseConfg.data >= 1 then
		 
	else
		--self.waitLabel:setVisible(true)
		self.samplePayPanel:setVisible(false)
		self.vipPayPanel:setVisible(false)
		self.qqQRCodePanel:setVisible(false)

		local urls = {n = "hall/image/chongzhi/zxkfbanner.png", configId = 0, Type= 0}
		local item = self:createTabItem(urls)
		item.listView = self.zxkfItem
		initTab(item, 0, self.tabBtnList, allTab)
		uiCheck:updateGroup(allTab, allTab[1])
		return
	end
    for _, v in pairs(baseConfg.data) do 
		if v.type~=0 then 
            table.insert(self.vipAccount,v)
        end
    end  

     for _, v in pairs(baseConfg.data) do 
         if v.type == 0 then
		    if v.payType==17 or v.payType==4 or v.payType==8  then
        
            else
		        local tabType = PayData[v.payType].tabType
                 if GlobalConf.CHANNEL_ID == 1003 then
                    if v.company ~= "北洋支付" then
                       if tabType ==1 then
                            table.insert(self.wxAccount,v)
                        elseif tabType == 2 then
                            table.insert(self.zfbAccount,v)
                        elseif tabType == 4 then
                            table.insert(self.yhkAccount,v)
                        end
					else 
						if v.payType ~= 16 then --闪云不需要在推荐中
						 	table.insert(self.byAccount,v)
						end
                    end
                else
                    if tabType ==1 then
						table.insert(self.wxAccount,v)
                    elseif tabType == 2 then
                        table.insert(self.zfbAccount,v)
                    elseif tabType == 4 then
						table.insert(self.yhkAccount,v)
					end
                end
            end
        end
	end 
	local zxkfFlag = false

	local _addZxkfFunction = function ()
		if zxkfFlag ~= true then
			if getZXKFKey() ~= nil then
				local urls = {n = "hall/image/chongzhi/zxkfbanner.png", configId = 0, Type= 0}
				local item = self:createTabItem(urls)
				item.listView = self.zxkfItem
				initTab(item, 0, self.tabBtnList, allTab)
			end
		end
		zxkfFlag = true
	end

     local flag = true
	for _, v in pairs(baseConfg.data) do

	    if v.type == 0 then
		    if v.payType==17 or v.payType==4 or v.payType==8  then
        
            else
                local tabType = PayData[v.payType].tabType
                
                local PanelType = PayData[v.payType].PanelType
                local iconPath = PayData[v.payType].iconPath
                local BannerPath = nil
                BannerPath = PayData[v.payType].BannerPath
                if GlobalConf.CHANNEL_ID == 1003 then
                    if v.company == "北洋支付" then
                        if v.payType == 0 then
                            BannerPath ="dt/image/zcm_chongzhi/banner_wx2.png"
                        elseif v.payType == 7 then
                            BannerPath ="dt/image/zcm_chongzhi/banner_zfb2.png"
                        elseif v.payType == 13 then
                            BannerPath ="dt/image/zcm_chongzhi/banner_yhk2.png"
                        end
                    end 
                end
                
                local payList = nil
                if v.payValueConfig then
                    payList = {}
                    local list=json.decode(v.payValueConfig,1)  
                    for k,v in pairs(list) do
                        table.insert(payList,v.payValue)
                    end
                end 
                local min = v.min
                local max = v.max
                self.payType1[v.id] = {}
                self.payType1[v.id].PanelType = PanelType
                self.payType1[v.id].iconPath = iconPath
                self.payType1[v.id].BannerPath = BannerPath
                self.payType1[v.id].payList = payList
                self.payType1[v.id].min = min
                self.payType1[v.id].max = max
                self.payType1[v.id].rate = v.returnRate
                self.payType1[v.id].tabType = tabType
                if v.payType == 16 then 
                    flag = false
                    if #self.byAccount>0 then
                        local v = self.byAccount[1]
                        local info = self.payType1[v.id]
                        local urls = {n = "hall/image/chongzhi/bybanner.png",configId = v.id,Type= v.type,rate=v.returnRate,tabTpye = 2}  
                        local item = self:createTabItem(urls)
                        item.listView =self.byItem
                        initTab(item, v.payType,self.tabBtnList,allTab) 
                        for k,v in pairs(self.byAccount) do
                            local info = self.payType1[v.id]
                            local urls = {n = info.iconPath,configId = v.id,Type= v.type,rate=v.returnRate,name=v.payName} 
                            local item = self:createSubItem(urls)
                            item.listView =self.byItem
                            initTab(item, v.payType,self.byItem,byTab) 
                        end
						uiCheck:updateGroup(byTab, byTab[1])

						_addZxkfFunction()
					end
					_addZxkfFunction()
                    local info = self.payType1[v.id]
                    local urls = {n = info.iconPath,configId = v.id,Type= v.type,rate=v.returnRate} 
                    local item = self:createTabItem(urls)
                    initTab(item, v.payType,self.tabBtnList,allTab)
                end 
		     end
        end
	end 
     if flag then
        if #self.byAccount>0 then
            local v = self.byAccount[1]
            local info = self.payType1[v.id]
            local urls = {n = "hall/image/chongzhi/bybanner.png",configId = v.id,Type= v.type,rate=v.returnRate,tabTpye = 2}  
            local item = self:createTabItem(urls)
            item.listView =self.byItem
            initTab(item, v.payType,self.tabBtnList,allTab) 
            for k,v in pairs(self.byAccount) do
                local info = self.payType1[v.id]
                local urls = {n = info.iconPath,configId = v.id,Type= v.type,rate=v.returnRate,name=v.payName} 
                local item = self:createSubItem(urls)
                item.listView =self.byItem
                initTab(item, v.payType,self.byItem,byTab) 
            end
            uiCheck:updateGroup(byTab, byTab[1])
        end
	end
	_addZxkfFunction()
	

    local info =PayData[17]
    local urls = { n = info.iconPath,configId = 6,Type= 1}  
	local item = self:createTabItem(urls)
	initTab(item, 17,self.tabBtnList,allTab) 

	
	-- if #self.zxkfAccout > 0 then
	-- 	local v = self.zxkfAccout[1]
	-- 	local info = self.payType1[v.id]
	-- 	local urls = {n = "hall/image/chongzhi/zxkfbanner.png",configId = v.id,Type= v.type,rate=v.returnRate,tabTpye = 3}
	-- 	local item = self:createTabItem(urls)
	-- 	item.listView = self.zxkfItem
	-- 	initTab(item, v.payType, self.tabBtnList, allTab)
	-- 	for k,v in pairs(self.zxkfAccout) do
	-- 		local info = self.payType1[v.id]
	-- 		local urls = {n = info.iconPath,configId = v.id,Type= v.type,rate=v.returnRate,name=v.payName}
	-- 		local item = self:createSubItem(urls)
	-- 		item.listView = self.zxkfItem
	-- 		initTab(item, v.payType, self.zxkfItem, zxkfTab)
	-- 	end
	-- 	uiCheck:updateGroup(zxkfTab, zxkfTab[1])
	-- end

    if #self.zfbAccount>0 then
        local v = self.zfbAccount[1]
        local info = self.payType1[v.id]
        local urls = {n = info.iconPath,configId = v.id,Type= v.type,rate=v.returnRate,tabTpye = 2}  
        local item = self:createTabItem(urls)
        item.listView =self.zfbItem
        initTab(item, v.payType,self.tabBtnList,allTab) 
        for k,v in pairs(self.zfbAccount) do
            local info = self.payType1[v.id]
            local urls = {n = info.iconPath,configId = v.id,Type= v.type,rate=v.returnRate,name=v.payName} 
            local item = self:createSubItem(urls)
            item.listView =self.zfbItem
            initTab(item, v.payType,self.zfbItem,zfbTab) 
        end
        uiCheck:updateGroup(zfbTab, zfbTab[1])
    end
    if #self.yhkAccount>0 then
        local v = self.yhkAccount[1]
        local info = self.payType1[v.id]
        local urls = {n = info.iconPath,configId = v.id,Type= v.type,rate=v.returnRate,name=v.payName,tabTpye = 4} 
        local item = self:createTabItem(urls)
        item.listView =self.yhkItem
        initTab(item, v.payType,self.tabBtnList,allTab) 
          for k,v in pairs(self.yhkAccount) do
            local info = self.payType1[v.id]
            local urls = {n = info.iconPath,configId = v.id,Type= v.type,rate=v.returnRate,name=v.payName} 
            local item = self:createSubItem(urls)
             item.listView =self.yhkItem
            initTab(item, v.payType,self.yhkItem,yhkTab) 
        end
        uiCheck:updateGroup(yhkTab, yhkTab[1])
    end  
     if #self.wxAccount>0 then
        local v = self.wxAccount[1] 
        local info = self.payType1[v.id]
        local urls = {n = info.iconPath,configId = v.id,Type= v.type,rate=v.returnRate,tabTpye = 1} 
        local item = self:createTabItem(urls)
        item.listView =self.wxItem
        initTab(item, v.payType,self.tabBtnList,allTab) 
        for k,v in pairs(self.wxAccount) do
            local info = self.payType1[v.id]
            local urls = {n = info.iconPath,configId = v.id,Type= v.type,rate=v.returnRate,name=v.payName} 
            local item = self:createSubItem(urls)
            item.listView =self.wxItem
            initTab(item, v.payType,self.wxItem,wxTab) 
        end
        uiCheck:updateGroup(wxTab, wxTab[1])
	end
	
	

	if #allTab > 0 then
		uiCheck:updateGroup(allTab, allTab[1])
	end
	
end

function ZCShopView:createTabItem( urls)
	-- body
	local item = self.tabItem:clone() 
    print("createTabItem   "..urls.n)
	local sprite = display.newSprite(urls.n)
	item:addChild(sprite)
    sprite:setPosition(130,50)
    item.configId =urls.configId
    item.Type =urls.Type 
    if urls.rate and urls.rate > 0 then
        item:getChildByName("rate"):setVisible(true)
		item:getChildByName("rate"):getChildByName("text"):setString("赠"..urls.rate.."%")
	else
		item:getChildByName("rate"):setVisible(false)
	end
	
	item:setContentSize({width = 260, height = 90})
	local back = item:getChildByName("light")
	back:setPositionY(40)

	return item
end
function ZCShopView:createSubItem( urls)
	-- body
	local item = self.subItem:clone() 
--    print("createsubItem   "..urls.n)
--	local sprite = display.newSprite(urls.n)
--	item:addChild(sprite)
--    sprite:setPosition(130,40)
	local text_payName = item:getChildByName("text_payName")
	text_payName:setString(urls.name)
	local text_payName_size = text_payName:getContentSize()
	if text_payName_size.width > 196 then
		text_payName:setScale(196 / text_payName_size.width)
	else
		text_payName:setScale(1)
	end

    item.configId =urls.configId
    item.Type =urls.Type 
    if urls.rate and urls.rate > 0 then
        item:getChildByName("rate"):setVisible(true)
		item:getChildByName("rate"):getChildByName("Text_4"):setString("赠"..urls.rate.."%")
	else
		item:getChildByName("rate"):setVisible(false)
    end
	return item
end
function ZCShopView:showPayPanel(payType,configId)
	-- body
    local info
    if payType==17 or payType==4 or payType==8 then
        info = PayData[payType]
    else
        info = self.payType1[configId]
    end
	dump(info,"单个选项值")

	self.nowPayType = payType
    self.nowConfigId = configId
	self:canshowPanel()

	if payType == 0 and configId == 0 then
		self.samplePayPanel:setVisible(false)
		self.vipPayPanel:setVisible(false)
		self.zxkfPanel:setVisible(true)
		--self:updateZXKFData()
		return
	end

	if info then
		if info.PanelType == 1 then 
			self.topBanner:loadTexture(info.BannerPath)
			if payType == 16 then
				self.topBanner:setVisible(true)
				
			else
				self.topBanner:setVisible(false) 
			end
			self:adjustInputUI(1)
			self:clearInput()
			
			local min = info.min  or 0  
			local max = info.max
			self.defaultPlaceHolder = string.format(DTCommonWords.RECHARGE_MIN_INPUT, min,max)  
			self:showBtns(min,max,configId)
			self.payField:setPlaceHolderColor(cc.c4b(184,187,192,255))
			self.payField:setPlaceHolder(self.defaultPlaceHolder)
		else
			self.samplePayPanel:setVisible(false)
			self.vipPayPanel:setVisible(true)
			self:showVippay()
		end
	end
end

function ZCShopView:updateZXKFData()
	local datas = {}
	local datas_2 = {}
	for __, info in pairs(self.zxkfAccout) do
		if info.isOnline == 1 then
			table.insert(datas, info)
		else
			table.insert(datas_2, info)
		end
	end
	self.zxkfScrollView:removeAllItems()

	for index = 1, #datas do
		local item = self:createZXKFItem(datas[index])
		self.zxkfScrollView:pushBackCustomItem(item)
	end
	-- if #datas > 0 then
	-- 	self.zxkfScrollView:jumpToTop()
	-- end
end

function ZCShopView:createZXKFItem(info)
	-- "orgName": "五一娱乐",
	-- "nickName": "哎哎",
	-- "isOnline": 0,
	-- "userName": "10019",
	-- "userId": 75,
	-- "orgId": "1"
	-- "avatar": "http://xxxx.com"
	-- "rechargeNum": 0
	-- "supportedPms": 00000000 充值类型 {}
	-- local itemSize = {width = 100, height = 100}
	local item = ccui.ImageView:create("hall/image/chongzhi/zxkf/back1.png")
	item.info = info
	item:ignoreContentAdaptWithSize(true)
	local itemSize = item:getContentSize()
	local avaterSize = {width = 93, height = 93}
	local image_path = "hall/image/chongzhi/zxkf/"
	local left_x = 121
	local right_x = itemSize.width - 30

	local avater = NetSprite.new(info.avatar, true, function(isDont, fileName, sender)
		if not isDont or "https://xinboim.oss-cn-hangzhou.aliyuncs.com/images/default_user_avatar.png" == info.avatar then
			sender:runAction(cc.Sequence:create(cc.DelayTime:create(0.05), cc.CallFunc:create(function()
				-- local index = math.random( 1, 2 )
				sender:updateTexture("hall/image/chongzhi/zxkf/icon1.png")
			end), nil))
		end
		sender:setScaleSize(avaterSize.width, avaterSize.height)
	end)

	item:addChild(avater)
	avater:setPosition(cc.p(avaterSize.width / 2 + 15, itemSize.height - avaterSize.height / 2 - 15))

	local online = "*****(离线)"
	if info.isOnline == 1 then
		online = "*****(在线)"
	end

	local name = display.newTTFLabel({
		text = info.nickName..online,
		font = cc.exports.FONT_TTF_PATH,
		size = 32,
		color = cc.c3b(0x12, 0x71, 0x9f),
	})
	item:addChild(name)
	name:setAnchorPoint(cc.p(0, 0.5))
	name:setPosition(cc.p(left_x, itemSize.height - 33))

	local star = ccui.ImageView:create(image_path.."star.png")
	item:addChild(star)
	star:ignoreContentAdaptWithSize(true)
	star:setAnchorPoint(cc.p(0,0))
	star:setPosition(cc.p(left_x, 10))
	local starSize = star:getContentSize()
	local nNumber = info.rechargeNum or 0
	local rechargeNum = display.newTTFLabel({
		text = "月充值 "..nNumber.."笔",
		font = cc.exports.FONT_TTF_PATH,
		size = 24,
		color = cc.c3b(255, 255, 255),
	})
	item:addChild(rechargeNum)
	rechargeNum:setAnchorPoint(cc.p(0, 0))
	rechargeNum:setPosition(cc.p(starSize.width + star:getPositionX() + 10, star:getPositionY()))

	local slist = {"guanfang.png"}

	if bit.band(info.supportedPms, PAY_INFO["YUN_SHAN"].id) > 0 then
		table.insert(slist, "xinyongka.png")
	end

	if bit.band(info.supportedPms, PAY_INFO["ALIPAY"].id) > 0 then
		table.insert(slist, "huabei.png")
	end

	local x = left_x
	local y = starSize.height + star:getPositionY() + 3
	for index = 1, #slist do
		local renzheng = ccui.ImageView:create(image_path..slist[index])
		item:addChild(renzheng)
		renzheng:ignoreContentAdaptWithSize(true)
		renzheng:setAnchorPoint(cc.p(0,0))
		renzheng:setPosition(cc.p(x, y))
		local size = renzheng:getContentSize()
		x = x + size.width + 3
	end


	local pay_list = {"ALIPAY", "WECHAT", "YUN_SHAN", "UNION_PAY"}
	x = right_x
	for index = 1, #pay_list do
		local payInfo = PAY_INFO[pay_list[index]]
		if bit.band(payInfo.id , info.supportedPms) > 0 then
			local image = ccui.ImageView:create(image_path..payInfo.image)
			item:addChild(image)
			image:ignoreContentAdaptWithSize(true)
			image:setAnchorPoint(cc.p(1, 0.5))
			image:setPosition(cc.p(x, name:getPositionY()))
			x = x - image:getContentSize().width - 3
		end
	end


	local button = ccui.ImageView:create(image_path.."button.png")
	item:addChild(button)
	button.info = info
	button:ignoreContentAdaptWithSize(true)
	button:setTouchEnabled(true)
	local buttonSize = button:getContentSize()
	button:setPosition(cc.p(right_x - buttonSize.width / 2, buttonSize.height / 2 + 15))
	button:addTouchEventListener(handler(self, self.onShowKefuPayClick))
	-- UIAdapter:registClickCallBack(button, handler(self, self.onShowKefuPayClick), false)
	return item
end

function ZCShopView:onShowKefuPayClick(taget, eventType)
	-- 客服充值
	local info = taget.info
	if eventType == ccui.TouchEventType.began then
		taget.m_beganPoint = cc.p(taget:getTouchBeganPosition())
	elseif eventType == ccui.TouchEventType.moved then
	elseif eventType == ccui.TouchEventType.ended then
		taget.m_endPoint = cc.p(taget:getTouchEndPosition())

		if math.abs(taget.m_endPoint.x - taget.m_beganPoint.x) > 10 or math.abs(taget.m_endPoint.y - taget.m_beganPoint.y) > 10 then
			return
		end
		
		local end_clock = os.clock()
		if taget.button_clock and (end_clock - taget.button_clock) < 0.2 then
			return
		end
		taget.button_clock = end_clock
		local urldata = "channelId="..GlobalConf.CHANNEL_ID.."&fromUserId="..self.chatInitData.id.."&toUserId="..taget.info.userId
		urldata = urldata.."&sgin="..crypto.md5(urldata.."&key="..getZXKFKey())
		local url = ZXKF_URL.."chatApi/chat?"..urldata
		print(url)
		platform.openWebViewFillUp(url, 2)
	end
end

function ZCShopView:canshowPanel()
	-- body
--	local isVip = payType == VIP_TAB
--	local isQqQrCode = payType == TAB_QQQrCode
--	local isDianKa = payType == TAB_DIAN_KA
--	local isBeiYong = payType == TAB_BEIYONG_PAY
 	self.samplePayPanel:setVisible(true)
	self.vipPayPanel:setVisible(false)
	self.qqQRCodePanel:setVisible(false)
	self.dianKaPanel:setVisible(false)
	self.beiYongPanel:setVisible(false)
	self.zxkfPanel:setVisible(false)
end

function ZCShopView:adjustInputUI(isquota) 

	-- 处理定额的UI
	-- local postfix = string.sub(payName, #payName)
	local flag = isquota == 1   
	-- self.chooseTitle:setVisible(not flag)
	self.clearInputBtn:setVisible( flag)
	self.btnCont:setPositionY(flag and 0 or 0)
	self.readonlyInput:setVisible(not flag)
	-- editTxt:setVisible(not flag)
	self.payField:setVisible( flag)
	self.inputBg:setTouchEnabled( flag)

end

function ZCShopView:showAlipay( payType )
	-- body
	self.topBanner:loadTexture("dt/image/zcm_chongzhi/zcm_cz6.png")
	self:showBtns(payType)
end

function ZCShopView:showWXpay( payType )
	-- body
	self.topBanner:loadTexture("dt/image/zcm_chongzhi/zcm_cz25.png")
	self:showBtns(payType)
end

function ZCShopView:mathSort(t)
	if type(t)~="table" then
		return
	end
	local tab={}
	local index=1
	while #t~=0 do
		local n=math.random(0,#t)
		if t[n]~=nil then
			tab[index]=t[n]
			table.remove(t,n)
			index=index+1
		end
	end
	return tab
end

function ZCShopView:onVipRefRank()
	_:disableQuickClick(self.vipRefBtn)
	self:showVippay()
end

function ZCShopView:showVippay( ... )
	-- body
	print("----------showVippay-----------")
--	local roleID = cc.loaded_packages.CommonModel:getRoleID()
--	self.roleID:setString(roleID)
	self.contentList:removeAllItems()
	local proxyInfo = self.vipAccount
	dump(cfgInfo,"showVippay--------data")  
	local max = math.ceil(#proxyInfo/3)
	print(max,"payProxy")
	for i = 1, max do
		local datas = {}
		for ii = 1 , 3 do
			if proxyInfo[(i - 1) * 3 + ii] then
				table.insert(datas,proxyInfo[(i - 1) * 3 + ii])
			end
		end
		local colItem = self.itemcol:clone()
		self.contentList:pushBackCustomItem(colItem)
		self:initColItem(colItem,datas)
	end
end

function ZCShopView:showQQQrCodePanel()
	local roleID = cc.loaded_packages.CommonModel:getRoleID()
	self.ridOfQq:setString(roleID)
	local data = _.getPayCfg(TAB_QQQrCode)
	if not data or not data.commonData then
		return
	end

	local jData = data.commonData
	self.qq:setString(jData.qq or '')
	self.qrCodeNode:removeAllChildren()
	self.qrCodeNode:addChild(CommonNetImageView.new(jData.scanimg, self.qrCodeNode, cc.size(184, 184)))
end

function ZCShopView:showDianKaPanel()
	local data = _.getPayCfg(TAB_DIAN_KA)
	dump(data, "showDianKaPanel")
	if not data or not data.commonData then
		return
	end
	-- local jData = json.decode(data.commonData)
	-- if not jData then
	-- 	return
	-- end
	print("解析 showDianKaPanel")
	self.dianKaData = data.commonData

	self.allDianKPanel:setVisible(true)
	self.dealPanel:setVisible(false)
	-- self:updateDianKaList()
	self:showDianKaView()
end

function ZCShopView:showDianKaView()
	self.allDianKListView:removeAllChildren()
	self.allDianKPanel:setVisible(true)
	self.dealPanel:setVisible(false)

	local data = _.getPayCfg(TAB_DIAN_KA)
	if not data or not data.commonData then
		return
	end
	local jData = data.commonData
	if jData and #jData > 1 then
		table.sort(jData, function (a,b)
			if a.rankInfo and b.rankInfo  then
				return a.rankInfo < b.rankInfo
			end
			return  false
		end )
	end
	-- local rankTypeName = cc.UserDefault:getInstance():getStringForKey("dk_rank", '')
	-- if rankTypeName and #rankTypeName > 1 then
	-- 	local firstData 
	-- 	for i = 1, #jData do
	-- 		if jData[i].dianKaType == rankTypeName then
	-- 			firstData = jData[i]
	-- 			table.remove(jData,i )
	-- 			break
	-- 		end
	-- 	end
	-- 	if firstData then
	-- 		table.insert(jData,1,firstData)
	-- 	end
	-- end
	if jData[1] and jData[1].buyurl then
		self.dianKaBuyUrl = jData[1].buyurl
	end

	local allData = {} --拆检数据
	local tData = {}
	local index = #jData
	for k,v in pairs(jData) do
		if (k -1) % 3 == 0 and k ~= 1 then
			table.insert(allData,tData )
			tData = {}
		end
		table.insert(tData, v)
		index = index - 1
		if index == 0 and #tData > 0 then
			table.insert(allData,tData )
		end
	end
	for k,v in pairs(allData) do
		local item = self:createLineOne(v)
		self.allDianKListView:pushBackCustomItem(item)
	end
end
function ZCShopView:showBeiYongPayPanel()
	local data = _.getPayCfg(TAB_BEIYONG_PAY)
	dump(data, "showBeiYongPanel")
	if not data or not data.commonData then
		return
	end
	self.beiyongUrl = data.commonData.skipUrl
	print("meepo_test",self.beiyongUrl)
	local info =  cc.loaded_packages.DTCommonModel:getUserInfo()
	self.rid:setString(info.rid)
	local len = string.len(self.beiyongUrl)
	local idx = string.find(self.beiyongUrl,'://')
	local url = string.sub(self.beiyongUrl,idx+3,len)
	self.bYUrl:setString(url)
	self.beiYongPanel:setVisible(true)
	self.dealPanel:setVisible(false)
end
function ZCShopView:createLineOne(data)
	local line = self.itemcol2:clone()
	local lineWidth = line:getContentSize().width
	for k,v in pairs(data) do
		local cl = self.dkItem:clone()
		if v.picurl then
			cl:addChild(CommonNetImageView:create(v.picurl, cl,cc.size(202,110)))
		end
		cl.info = v
		cl:addTouchEventListener(handler(self, self.jumpToDeasPay))
		cl:setPosition(cc.p(lineWidth/3 * k -  cl:getContentSize().width*0.55, line:getContentSize().height/2))
		line:addChild(cl)
	end
	return line
end

function ZCShopView:showAllDiankaList()
	self:turnDiank(true)
	self:showDianKaView()
	self.dkNum:setString("")
	self.dkPsw:setString("")
	for i = 1, 8 do
		local node = self['dkC' .. i]
		node:addEventListenerCheckBox(handler(self, self.onClickDianKaJinE))
		node:setSelected(false)
	end
end

function ZCShopView:turnDiank(isVisible)
	self.allDianKPanel:setVisible(isVisible)
	self.dealPanel:setVisible(not isVisible)
end

function ZCShopView:jumpToDeasPay(sender,eventType)
    if eventType  == ccui.TouchEventType.ended then 
        if self.nowPayType == 16 then
	        device.openURL("https://youhui.95516.com/hybrid_v3/html/help/download.html?code=62039")
        end
    end
end

function ZCShopView:showOneDK(info)
	self:updateDianKaJinE(info)
	self:onClickDianKaName(info)
	self.dkOk.payInfo = info
	-- cc.UserDefault:getInstance():setStringForKey("dk_rank",info.dianKaType)
	-- cc.UserDefault:getInstance():flush()
end

function ZCShopView:updateDianKaList()
	local data = self.dianKaData
	if not data then
		return
	end
	local preferType = cc.UserDefault:getInstance():getStringForKey("DIAN_KA_PREFER_TYPE", '1')
	preferType = tonumber(preferType)

	self.dkSelectList:removeAllChildren()
	for i, v in ipairs(data) do
		local node = self.dkNameItem:clone()
		node:getChildByName('dkName'):setString(v.dianKaName)
		node:getChildByName('arrow'):setVisible(false)
		node:addTouchEventListener(handler(self, self.onClickDianKaName))
		node.dataIdx = i
		self.dkSelectList:pushBackCustomItem(node)
		if preferType == i then
			self:onClickDianKaName(node)
		end
	end
	local num = #data
	if 5 < num then
		num = 5.5
	end
	local size = self.dkSelectList:getContentSize()
	size.height = 45 * num
	self.dkSelectList:setContentSize(size)

	-- self:updateDianKaJinE()
end

function ZCShopView:updateDianKaJinE(info)
	-- if not self.dianKaData or not self.dkNameItem.dataIdx then
	-- 	return
	-- end
	-- local data = self.dianKaData[self.dkNameItem.dataIdx]
	-- if not data then
	-- 	return		
	-- end
	for i = 1, 8 do
		local value = info.chargeAmounts[i]
		local checkNode = self['dkC' .. i]
		if value then
			checkNode:setVisible(true)
			self['dkJinE' .. i]:setString(value .. ':')
		else
			checkNode:setVisible(false)
		end
	end
end

function ZCShopView:onClickDianKaName(info)
	-- if not self.dianKaData then
	-- 	return
	-- end
	-- self.dkNameItem.dataIdx = node.dataIdx or 1
	-- cc.UserDefault:getInstance():setStringForKey("DIAN_KA_PREFER_TYPE", self.dkNameItem.dataIdx .. '')
	-- local data = self.dianKaData[self.dkNameItem.dataIdx]
	-- if not data then
	-- 	return
	-- end
	-- local visible = self.dkSelectList:isVisible()
	-- self.dkSelectList:setVisible(not visible)
	-- if self.dkNameItem ~= node then
	-- 	self.dkSelectList:setVisible(false)
	-- end
	-- self.dkNameItem:getChildByName('dkName'):setString(info.dianKaName)
	self.diankaName:setString(info.dianKaName)
	self.dkPercent:setString(info.dianKaFee .. '%')
	-- self:updateDianKaJinE()
end

function ZCShopView:onClickDianKaBuyGuide()
	if CCApplication.openURL then
		if self.dianKaBuyUrl then
			CCApplication:sharedApplication():openURL(self.dianKaBuyUrl)
			return
		end
		local kl = "https://heyi.m.tmall.com/shop/shop_auction_search.htm?ajson=1&_tm_source=tmallsearch&spm=a320p.7692363.0.0.60c55388SmiVJL&sort=default&shop_cn=%E6%98%93%E5%85%85%E7%BA%B5%E6%B8%B8%E5%8D%A1&ascid=1361062252&scid=1361062252"
		CCApplication:sharedApplication():openURL(kl)
	end
end

function ZCShopView:onClickDianKaJinEBMF(event)
	local node = event.node
	for i = 1, 8 do
		local other = self['dkC' .. i]
		if node ~= other then
			other:setSelected(false)
		else
			other:setSelected(true)
		end
	end
end

function ZCShopView:onClickDianKaJinE(node, state)
	for i = 1, 8 do
		local other = self['dkC' .. i]
		if node ~= other then
			other:setSelected(false)
		else
			other:setSelected(true)
		end
	end
end

function ZCShopView:onClickDianKaBuy(node)
	if not self.dianKaData then
		return
	end
	local data = self.dianKaData[self.dkNameItem.dataIdx]
	if not data then
		return
	end
	require('app.games.dt.hall.pay.WebPay').openUrl(data.buyurl)
end

function ZCShopView:onClickDianKaOk(node)
	-- if true then
	-- 	cc.loaded_packages.DTHallLogic:reqUseDianKa(1, 'dianka', '1604112108902167', '6150100946239880', 'JW')
	-- 	return
	-- end
	local typeData = _.getPayCfg(TAB_DIAN_KA)
	if not typeData then
		TOAST("无效数据")
		return
	end

	-- if not self.dianKaData or not self.dkNameItem.dataIdx then
	-- 	TOAST("无效数据")
	-- 	return
	-- end
	local data = node.payInfo
	if not data then
		TOAST("无效数据")
		return		
	end

	local no = self.dkNum:getString()
	if not no or '' == no then
		TOAST("请输入点卡号码")
		return
	end
	local psw = self.dkPsw:getString()
	if not psw or '' == psw then
		TOAST("请输入密码")
		return
	end

	local value
	for i = 1, 8 do
		local n = self['dkC' .. i]
		if n:isSelected() then
			value = data.chargeAmounts[i]
			break
		end
	end
	if not value then
		TOAST("请选择金额")
		return
	end

	cc.loaded_packages.DTHallLogic:reqUseDianKa(value, typeData.payName, no, psw, data.dianKaType, TAB_DIAN_KA)
end

function ZCShopView:clearInput( ... )
	self.payField:setString("")
	self.readonlyInput:setString('')
end
 

function ZCShopView:showBar( content )
	-- body
	local FloatBar = require("app.games.dt.hall.FloatBar")
	local bar = FloatBar.new(content)
	self:addChild(bar)
	bar:setPosition(display.center)	
end

function ZCShopView:payCall()
	-- body
	-- self.payField:string()
	if self.payField:getString() == "" then
		TOAST("请输入金额")
		return
	end

    local baseInfo
    if self.nowConfigId==17 or self.nowConfigId==4 or self.nowConfigId==8 then
        baseInfo = PayData[self.nowPayType]
    else
        baseInfo = self.payType1[self.nowConfigId]
    end
    
	

	if not baseInfo then
		print(self.nowPayType)
		TOAST("未知错误")
		return
	end 
    if tonumber(self.payField:getString()) >baseInfo.max then
		TOAST("输入金额不能超过最大可充值金额")
		return
	end 
    if tonumber(self.payField:getString()) <baseInfo.min then
		TOAST("输入金额不能少于最小可充值金额")
		return
	end 
    self:payReq()
	
end
function ZCShopView:payReq()
     local url = string.format(GlobalConf.DOMAIN.."api/itf/pay/doOnline?channelId=%d&type=%d&userId=%d&configId=%d&payFee=%d",GlobalConf.CHANNEL_ID,G_Type,Player:getAccountID(),G_ConfigId,tonumber(self.payField:getString()))

     print(url)
	   local xhr = cc.XMLHttpRequest:new()
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
    xhr:open("GET",url) 
    ToolKit:addLoadingDialog(10,"正在加载中，请稍等......")
    local path = ""
    if self.byItem:isVisible() then
        path = "hall/image/chongzhi/tjcztitle.png"
    elseif  self.zfbItem:isVisible() then
            path = "hall/image/chongzhi/zfbtitle.png"
    elseif  self.wxItem:isVisible() then
            path = "hall/image/chongzhi/wxtitle.png"
    elseif  self.yhkItem:isVisible() then
			path = "hall/image/chongzhi/title.png"
	elseif self.zxkfItem:isVisible() then
		path = "hall/image/chongzhi/zxkftitle.png"
    else
         path = "hall/image/chongzhi/ysftitle.png"
    end
--     if self.byItem:isVisible() then
   
    local function onReadyStateChange() 
        ToolKit:removeLoadingDialog()
        local response   = xhr.response
        if response == "" then
     --       self:payReq()
             print("response = nil")
        else

            local output = json.decode(response,1) 
            dump(output)
            if output.payUrl then
               if self.byItem:isVisible() then
                    if device.platform == "android" or device.platform == "ios" then
                        platformUtils.showPurchaseWebViewUrl(output.payUrl, "", function ( ... ) end)
                    else
                        local PayViewLayer = PayViewLayer.new(path)
                        self:addChild(PayViewLayer)
                        PayViewLayer:loadUrl(output.payUrl)
                    end
               else
                   device.openURL(output.payUrl) 
               end
            end
           
        end 
    end
    xhr:registerScriptHandler(onReadyStateChange)
    xhr:send()
end
function ZCShopView:showWaringDialogCall( content  )
	-- body
	local Lang = require "app.games.dt.common.DTCommonWords"
	local showDialog = require('app.fw.common.DialogBox').show
   	showDialog('', content, Lang.OK, Lang.CANCEL,
   		function ( ... )
   		-- body
   	end)
end

function ZCShopView:moneyBtnCall( event )
	-- body
	print(event.value,"点击值")
	-- editTxt:setText(event.value)
	self.payField:setString(event.value)
	--self.readonlyInput:setString(event.value)
end

function ZCShopView:showBtns( min,max,payType )
	-- body
	-- editTxt:setText("") 
	for i = 1, 12 do
		self["btn_"..i]:removeAllChildren()
	end  
	local data =self.payType1[payType].payList
    if data  == nil then 
        if max<=100 then
            data = PayList[1]
        elseif max<= 1000 then
            data = PayList[50]
        elseif max<= 5000 then
            data = PayList[50]
        else
            data = PayList[50]
        end
	 end
     local tab ={}
	for i = 1, 8 do
        local t = data[i] or 0
        if t>=min and t<=max then
           table.insert(tab,t) 
        end 
	end 
    dump(tab)
    for k,v in pairs(tab) do
        local item = self.rechangeItem:clone()
		btnHelp:initItem(item,v,k)
		item:setPosition(cc.p(0,80))
		self["btn_"..k]:addChild(item) 
        
		item:addTouchEventListener(handler(self, self.moneyBtnCall))
    end
end

function ZCShopView:initColItem( colItem ,datas)
	-- body
	local rowSize = self.itemOne:getContentSize()
	local llsize = self.itemcol:getContentSize()

	dump(datas,"VIP数据")

	-- local posRates = {0, 1, 2.01}
	for i = 1, 3 do
		local row = math.ceil(i / 3)
        local col = (i - 1) % 3 + 1 -- 1~3
		local item = PayItem.new(self)
		item:setData(datas[i] or nil)
		item.node:setAnchorPoint(cc.p(0,0.5))
		item.node:setPosition(cc.p(900/ 3 * (col - 1)  , rowSize.height * 0.5))
		-- if col == 1 then
		-- 	item.node:setPosition(cc.p(posRates[col] * rowSize.width + 5 , rowSize.height * 0.5))
		-- else
		-- 	item.node:setPosition(cc.p(posRates[col] * rowSize.width + 7 , rowSize.height * 0.5))
		-- end
		colItem:addChild(item.node)
	end
end

function ZCShopView:onPayCell( data )
	-- body
	self:showTpsLayer(data)
end


function ZCShopView:showTpsLayer( data )
	-- body
	local tpsView =ZCShopTpsView.new()
	tpsView:setData(data)
	self:addChild(tpsView)
end

 
function PayItem:ctor( shopClass )
	-- body
	self.node = shopClass.itemOne:clone()
	UIAdapter:parseCSDNode(self.node, self)
	self.node:setAnchorPoint(cc.p(0.5, 0.5))
	self.shopClass = shopClass
end

function PayItem:setData( data)
	-- body
	if data then
        local str = "hall/image/chongzhi/wx.png"
		self.node:setVisible(true)  
		 local imgType =  1 --payFrom:1(微信支付)，2(支付宝支付)，3(花呗支付)，4(QQ支付)，5(信用卡支付) 
        if  GlobalConf.CHANNEL_ID == 1003 then 
             self.name:setString(data.payDesc)
              if data.payType == 8 then
                imgType=2 
                elseif data.payType == 11 then 
                    imgType=4
                elseif data.payType== 17  then
                    imgType=5
                end 
        else
           
           
            if data.payType == 8 or data.payType == 5 or data.payType == 7 or data.payType == 6 then
                imgType=2
                str = "hall/image/chongzhi/zfb.png" 
            elseif data.payType == 11 then
            --     str = "QQ:\n"
                imgType=4
            elseif data.payType== 17 or data.payType== 12 or data.payType== 13 or data.payType== 14 or data.payType== 15 or data.payType== 16 then
                 str = "hall/image/chongzhi/yhk.png"
                imgType=5
            end 
            self.name:setString(data.account)
        end 
        self.name:setContentSize(cc.size(200,80))
        self.node:getChildByName("itemOne"):loadTexture(str)
         self.name:setPositionY(self.name:getPositionY()+5)
		--self.payCellBtn:loadTextures(string.format("hall/image/chongzhi/btn%d.png",imgType))
        if data.returnRate~=nil and data.returnRate > 0 then
            self.node:getChildByName("rate"):setVisible(true)
			self.node:getChildByName("rate"):getChildByName("text"):setString("赠"..data.returnRate.."%")
		else
			self.node:getChildByName("rate"):setVisible(false)
        end
		self.payCellBtn:addTouchEventListener( function (sender,eventType) 
              if eventType == ccui.TouchEventType.ended then 
                if  GlobalConf.CHANNEL_ID == 1003 then 
	                ToolKit:setClipboardText(self.name:getString()) 
                    TOAST("复制成功")
                      if data.payDesc~="" and GlobalConf.CHANNEL_ID == 1003 then 
                        return
                    end
                    if imgType ==1 then 
                        ToolKit:openWX()
                    elseif imgType ==2 then 
                        ToolKit:openZFB()
                    elseif imgType ==4 then 
                        if device.platform ~= "ios" then
                            device.openURL(string.format("http://wpa.qq.com/msgrd?v=3&uin=%s&site=qq&menu=yes",data.account))
                        else
                            local kl = "mqqwpa://im/chat?chat_type=wpa&uin=%s&version=1&src_type=web&web_src=oicqzone.com"
                            local content = string.format(kl,data.account)
                            device.openURL(content)
                        end
                    end
                else
                    local sVipBank = string.format(GlobalConf.DOMAIN.."front/pay/doPay?configId=%s&channelId=%s&userId=%s",data.id,GlobalConf.CHANNEL_ID,Player:getAccountID())
                    if device.platform == "android" or device.platform == "ios" then
                        platformUtils.showPurchaseWebViewUrl(sVipBank, "", function ( ... ) end)
                    else
                        device.openURL(sVipBank)
                    end
                end
              end
             end)
			--self.shopClass:onPayCell(data)
 
	else
		self.node:setVisible(false)
	end
end

G_ConfigId =1
G_Type =1
uiCheck = {
	updateGroup = function(this, checkArray, checkCur)
		for i,v in pairs(checkArray) do
			if v~=checkCur then
				v:setSelected(false) 
			--	v:setTouchEnabled(true) 
			end
		end  
		--checkCur:setTouchEnabled(false) 
		checkCur:setSelected(true) 
        G_ConfigId =checkCur.configId
        G_Type =checkCur.Type
	end
}

btnHelp = {
	initItem = function ( this, item, data,cnt )
		-- body
		if data == 0 then
			item:setVisible(false)
			return
		end
		local money = item:getChildByName("money")
        local image_money = item:getChildByName("image_money")
        image_money:loadTexture(string.format("hall/image/chongzhi/hall_plist_appstore_gui-shop-icon-%d.png",cnt))
		money:setString(data.."元")
		item.value = data 
	end
}

function _.getPayCfg( payType )
	-- body
	local payConfg = cc.loaded_packages.DTCommonModel:getNewShopDataInfo()
	if not payConfg then
		return nil
	end
	for _, v in pairs(payConfg.datas) do
		if v.payType == payType then
			return v
		end
	end
	return nil
end

function _:setBtnEnabled(btn, enabled)
    btn:setEnabled(enabled)
    btn:setTouchEnabled(enabled)
    btn:setBright(enabled)
end

-- 防止频繁点击
function _:disableQuickClick(btn)
    _:setBtnEnabled(btn, false)
    performWithDelay(btn, function()
        _:setBtnEnabled(btn, true)
    end, 2)
end

return ZCShopView
