--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion 
-- 布局文件
local platformUtils = require("app.hall.base.util.platform")  

local _ = {}
local FEEDBACK_UI_PATH = "hall/PayView.csb";
local HNlayer = import("..HNLayer")
local PayViewLayer = class("PayViewLayer",function()
    return HNlayer.new()
end)
function PayViewLayer:ctor(path) 
    self:setupViews(path) 
end 
 function PayViewLayer:onTouchCallback(sender)
  local name = sender:getName()
    if name == "button_closeBtn" then
         if device.platform == "android" then
             ToolKit:removeLoadingDialog()
             platformUtils.closePurchaseWebViewUrl("", "")
         end
        self:close() 
    end
 end
function PayViewLayer:setupViews(path) 
    
	local node = UIAdapter:createNode(FEEDBACK_UI_PATH);
   
    UIAdapter:adapter(node, handler(self, self.onTouchCallback))
    UIAdapter:praseNode(node,self)
	self:addChild(node);
    local center =  node:getChildByName("center");  
    local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY)) 
    local diffX = 145 - (1624-display.size.width)/2
    center:setPositionX(diffX)
	self._csNode = center:getChildByName("image_Bg");  
    UIAdapter:praseNode(node,self)
   
    self.Sprite_33:setTexture(path)
   -- self:kefuReq()
end 
function PayViewLayer:loadUrl(url)
     if device.platform == "ios" or device.platform == "android" then
        local webView = ccexp.WebView:create()
        webView:setContentSize( self.listView_contentList:getContentSize())
        webView:loadURL(url)
        webView:setScalesPageToFit(true) 
        webView:setJavascriptInterfaceScheme("lua")
        webView:setOnDidFinishLoading(function(sender, url)
            print("setOnDidFinishLoading, url is ", url) 
            ToolKit:removeLoadingDialog()
        end)
        webView:setOnDidFailLoading(function(sender, url)
            print("setOnDidFailLoading, url is ", url) 
            TOAST("加载失败")
            ToolKit:removeLoadingDialog()
        end)
        self.listView_contentList:pushBackCustomItem(webView) 
     end
end
return PayViewLayer
