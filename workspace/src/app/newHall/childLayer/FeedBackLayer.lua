--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion 
-- 布局文件
local _ = {}
local FEEDBACK_UI_PATH = "hall/FeedbackView.csb";
local HNlayer = import("..HNLayer")
local FeedBackLayer = class("FeedBackLayer",function()
    return HNlayer.new()
end)
function FeedBackLayer:ctor() 
    self:setupViews() 
end 
 function FeedBackLayer:onTouchCallback(sender)
  local name = sender:getName()
    if name == "button_closeBtn" then
        self:close() 
    end
 end
function FeedBackLayer:setupViews() 
    
	local node = UIAdapter:createNode(FEEDBACK_UI_PATH);
   
    UIAdapter:adapter(node, handler(self, self.onTouchCallback))
	self:addChild(node);
    local center =  node:getChildByName("center");  
    local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY)) 
    local diffX = 145 - (1624-display.size.width)/2
    center:setPositionX(diffX)
	self._csNode = center:getChildByName("image_Bg");  
    UIAdapter:praseNode(node,self)
    self:kefuReq()
end
 function FeedBackLayer:kefuReq()
     local url = string.format(GlobalConf.DOMAIN.."api/itf/cs/info?channelId="..GlobalConf.CHANNEL_ID)
     ToolKit:addLoadingDialog(5,"正在加载中，请稍等......")
     print(url)
	   local xhr = cc.XMLHttpRequest:new()
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
    xhr:open("GET",url) 
    
    local function onReadyStateChange() 
        local response   = xhr.response
        if response == "" then
          --  self:kefuReq()
             print("response = nil")
        else

            local output = json.decode(response,1) 
            dump(output)
            if output.data then
                for k,v in pairs(output.data) do
                    if v.type == 3 then
                        -- device.openURL(v.accountOrUrl)
                        self:openWebView(v.accountOrUrl)
                        break
                    elseif v.type == 4 then
                        -- channelId=1020001001  渠道编码
                        -- fromUserId=1026122  用户在棋牌的ID
                        -- nickName=sdfsdfsd  用户在棋牌的昵称(不能有特殊符号)
                        -- sgin=24670d16ed81eb6149aac9bf54b884ea (签名)
                        local urldata = "channelId="..GlobalConf.CHANNEL_ID.."&fromUserId="..Player:getAccountID().."&nickName="..Player:getNickName()
                        urldata = urldata.."&sgin="..crypto.md5(urldata)
                        self:openWebView(v.accountOrUrl..urldata)
                        break
                    end
                end
            end
        end
    end
    xhr:registerScriptHandler(onReadyStateChange)
    xhr:send()
end

function FeedBackLayer:openWebView(_accountOrUrl)
    if device.platform == "android" or device.platform == "ios" then
        local webView = ccexp.WebView:create()
        webView:setContentSize( self.listView_contentList:getContentSize())
        webView:loadURL(_accountOrUrl)
        webView:setScalesPageToFit(true)
        webView:setJavascriptInterfaceScheme("lua")
        self.listView_contentList:pushBackCustomItem(webView)
        webView:setOnDidFinishLoading(function(sender, url)
            print("setOnDidFinishLoading, url is ", url)
            ToolKit:removeLoadingDialog()
        end)
        webView:setOnDidFailLoading(function(sender, url)
            print("setOnDidFailLoading, url is ", url)
            TOAST("加载失败")
            ToolKit:removeLoadingDialog()
        end)
    else
        ToolKit:removeLoadingDialog()
    end
end

return FeedBackLayer
