--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--
-- User: lhj
-- Date: 2018/7/14 
-- Time: 14:19
-- 排行榜 cell

local TabCellBase = require("app.hall.base.ui.TabCellBase") 

local RankCell = class("RankCell", TabCellBase)

function RankCell:ctor()
	self:myInit()
	self:setContentSize(cc.size(960, 90))
end

function RankCell:myInit()
	self.node = UIAdapter:createNode("platform/rankingList/MoneyRankItem.csb")
    self.node:setPosition(320,40) 
	self:addChild(self.node)
end

function RankCell:setMainLayer(layer)
	self.m_MainLayer = layer
end

function RankCell:initData(ite, idx)
	--刷新数据
	local sp_rank =self.node:getChildByName("Sprite_rank");
	sp_rank:setVisible(false);
	local text_rank = self.node:getChildByName("AtlasLabel_rank");
	text_rank:setVisible(false);

	if (idx < 3) then
	
		sp_rank:setVisible(true);
		text_rank:setVisible(false);

		sp_rank:setTexture(string.format("platform/rankingList/res/paiming%d.png", idx));
	else
	
		sp_rank:setVisible(false);
		text_rank:setVisible(true);

		text_rank:setString(idx );
	end

	-- 昵称
	local text_nick = self.node:getChildByName("Text_nicheng"); 
	text_nick:setString(ite.m_strNick); 
	 

	-- 金币
	local text_gold = self.node:getChildByName("Text_jinbi"); 
	text_gold:setString(ite.m_nVal * 0.01);

	--vip等级
	local text_vip = self.node:getChildByName("AtlasLabel_vip");
	text_vip:setString( ite.m_nVipLevel);
	--头像，头像框
	local headUrl = string.format("platform/userData/res/head/icon_%d.png", ite.m_nAvatarId);
	local headFrameUrl = string.format("platform/userData/res/frame/frame_%d.png", ite.m_nFrameId);
	local userHead = self.node:getChildByName("Image_head");
	local userHeadKuang = self.node:getChildByName("Image_headFrame");
	userHead:loadTexture(headUrl);
    userHead:setScale(0.7)
	userHeadKuang:loadTexture(headFrameUrl);
    userHeadKuang:setScale(0.7)
end

function RankCell:refresh(index, data)

	print("index:::::::", index)

	local page = 1

	if index % 10 == 0 then
		page = index / 10
	else
		page = math.floor(index / 10) + 1
	end

	if page > self.m_MainLayer.m_CurPage then
		self.m_MainLayer.m_CurPage = page
		self.m_MainLayer:refreshScrollView()
	end

	if not GlobalRankController then
       GlobalRankController = require("app.hall.rank.control.RankController").new()
    end

	if self.m_MainLayer.m_FuncType == 1 then
		local data = GlobalRankController:getTdIncomeRankDatas()
		if index >= #data.m_userList then
			self:setVisible(false)
		else
			self:setVisible(true)
			self:initData(data.m_userList[index], index)
		end
	else
		local data = GlobalRankController:getTdPhoneRankDatas()
		if index >= #data.m_userList then
			self:setVisible(false)
		else
			self:setVisible(true)
			self:initData(data.m_userList[index], index)
		end
	end

end

return RankCell