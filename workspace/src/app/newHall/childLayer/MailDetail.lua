--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion 
local KAKA_DEFAULT_FONT = "DroidSansFallback"
local CHANGE_PATH = "hall/MailDetailNew.csb";

local HNlayer = import("..HNLayer")
local MailDetail = class("MailDetail",function()
    return HNlayer.new()
end) 
function MailDetail:ctor()
    self:myInit()
    self:setupViews()
end
function MailDetail:myInit()
     ToolKit:registDistructor(self, handler(self, self.onDestroy))
    addMsgCallBack(self, MSG_MAIL_REWARD_SUCCESS, handler(self, self.showMailAward))
end
function MailDetail:onDestroy()
	removeMsgCallBack(self, MSG_MAIL_REWARD_SUCCESS)
end
 function MailDetail:onTouchCallback(sender)
    local name = sender:getName()
    if name == "button_closeBtn" then
        self:close() 
    elseif name == "button_deteleMailBtn" or name == "button_lingqu" then
       if self.m_data:getState()==1 or self.m_data:getState()==2 then
            if self.m_data:getIsTextMail()== 1 then
                GlobalMailController:deleteMail(self.index)
                self:close() 
            else
                GlobalMailController:getMailReward(self.index)
            end
        else
            GlobalMailController:deleteMail(self.index)
             self:close() 
        end
    end
 end  
 
function MailDetail:onTouchCallback1(sender)
    local name = sender:getName()
    if name == "button_closeLayer" then
         self:close() 
    end
 end  
function MailDetail:showMailAward()
    if self.node then
        local function playAction( path,actionName,root )
		-- body	
		    print(path , actionName)
		    local ac2 = cc.CSLoader:createTimeline(path)
		    if ac2:IsAnimationInfoExists(actionName) == true then
			    ac2:play(actionName, false)
		    end
		    root:runAction(ac2)
	    end
	    local nodePath = "hall/image/action/Action.csb"

	    playAction(nodePath, "animation0", self.fileNode_action)
	     self.node:setVisible(true) 
         local t = self.m_data:getRewardList() 
        self.text_Num:setString(t[1].m_num/100)
    end
end
function MailDetail:setupViews()

	 
	local node = UIAdapter:createNode(CHANGE_PATH);
	
	self:addChild(node,10000);  
	  UIAdapter:adapter(node, handler(self, self.onTouchCallback)) 
       local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = node:getChildByName("Panel")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)
    local Image_1 = self.m_pNodeRoot:getChildByName("Image_1"); 
    local node_fujian = Image_1:getChildByName("node_fujian"); 
    self.text_coinNum = node_fujian:getChildByName("text_coinNum"); 
    self.button_deteleMailBtn = Image_1:getChildByName("button_deteleMailBtn"); 
    self.button_deteleMailBtn:setVisible(true)
    self.button_lingqu = Image_1:getChildByName("button_lingqu"); 
    self.button_lingqu:setVisible(false)
    self.scrollView_contentScroll= Image_1:getChildByName("scrollView_contentScroll"); 
    self.node_fujian = Image_1:getChildByName("node_fujian"); 
    self.text_coinNum =self.node_fujian:getChildByName("text_coinNum"); 
    self.text_coinNum:setString("")
    self.node = UIAdapter:createNode("hall/Lingqutexiao.csb");  
    local diffY = (display.size.height - 750) / 2
    self.node:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = self.node:getChildByName("panel_mask")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)
    self:addChild(self.node,10000); 
    self.node:setVisible(false)
    local panel_mask =  self.node:getChildByName("panel_mask")
    local Image_1 =  panel_mask:getChildByName("Image_1")
    self.text_Num = Image_1:getChildByName("text_Num")
    self.fileNode_action =Image_1:getChildByName("fileNode_action")
     UIAdapter:adapter(self.node, handler(self, self.onTouchCallback1))
end
 function MailDetail:updateContent(sender)
    self.m_data =sender.value
    self.index =sender.index
     local Image_1 =self.button_deteleMailBtn:getChildByName("Image_1")
     local state = sender.value:getState()  
    if sender.value:getIsTextMail()== 1 then  
        self.node_fujian:setVisible(false)
        self.button_deteleMailBtn:setVisible(true)
    else 
        self.node_fujian:setVisible(true) 
        local t = sender.value:getRewardList() 
        self.text_coinNum:setString(t[1].m_num/100)
        if state == 1 or state==2 then
             self.button_lingqu:setVisible(true)
        end
    end
   
    self.scrollView_contentScroll:removeAllChildren()
	local size = self.scrollView_contentScroll:getContentSize()
	local content = cc.Label:createWithSystemFont(self:trimText(sender.value:getContent()) or "", KAKA_DEFAULT_FONT, 24, cc.size(size.width, 0))
	content:setAnchorPoint(cc.p(0, 1))
	content:setTextColor(cc.c4b(254, 254, 254, 255))
	local contentSize = content:getContentSize()
    content:setPositionY(math.max(size.height, contentSize.height))
	self.scrollView_contentScroll:setInnerContainerSize(contentSize)
	self.scrollView_contentScroll:addChild(content)
	self.scrollView_contentScroll:jumpToTop()
 end
function MailDetail:trimText(text)
	local trimText = string.gsub(text,"<br>","\n")
	trimText = string.gsub(trimText,"<(.-)>","")
	trimText = string.gsub(trimText,"&nbsp;"," ") 
	return trimText
end  
return MailDetail