local SIGN_PATH = "hall/HongBao.csb"; 
local TaskData = import("..data.TaskData")
local HNlayer = import("..HNLayer")
local RichLabel = require("src.app.hall.base.ui.ViewRichTextLabel")
local SignHongbaoLayer = class("SignHongbaoLayer",function()
    return HNlayer.new()
end) 
function SignHongbaoLayer:ctor(__info)
     self._Datas1 = {
        [1] = { 
            m_title        = '每日任务',
            m_type         = 1,
        },
        [2] = { 
            m_title        = '签到红包',
            m_type         = 2,
        },
        [3] = { 
            m_title        = '救济金',
            m_type         = 2,
        }
    }
    self._panelBtn={}
    self.m_Type = 0
    ToolKit:registDistructor( self, handler(self, self.onDestroy))
    self:myInit()
    self:setupViews(__info)


end

function SignHongbaoLayer:myInit()
   self.itemList = {}
   addMsgCallBack(self, PLAYERSIGNIN_ACK, handler(self, self.onShowSign))
    addMsgCallBack(self, SHOWSAFEGOLD_ACK, handler(self, self.showSafeGold))  
    addMsgCallBack(self, RECEIVESAFEGOLD_ACK, handler(self, self.receiveSafeGold))  
    addMsgCallBack(self, MSG_TASKLIST, handler(self, self.showTaskList))  
    addMsgCallBack(self, MSG_TASKREWARD, handler(self, self.taskRewardAck))   
    addMsgCallBack(self, SHOWSIGNINCHANNEL_ACK, handler(self, self.onShowSign1))
end

function SignHongbaoLayer:onDestroy() 
      removeMsgCallBack(self, SHOWSAFEGOLD_ACK)  
    removeMsgCallBack(self, RECEIVESAFEGOLD_ACK)  
    removeMsgCallBack(self, PLAYERSIGNIN_ACK)
     removeMsgCallBack(self, MSG_TASKLIST)
      removeMsgCallBack(self, MSG_TASKREWARD)
       removeMsgCallBack(self, SHOWSIGNINCHANNEL_ACK)
end
function SignHongbaoLayer:taskRewardAck(_,info)  
    if info.m_retCode == 0 then
        TOAST("领取成功")
         for k,v in pairs(self.m_taskList) do
            if info.m_taskId == v.m_taskId then
                  v:getChildByName("Button_Reward"):loadTexture("hall/image/task/unget.png")
                  v:setEnabled(false) 
            end
         end
    else
        TOAST("领取失败")
    end
end
function SignHongbaoLayer:onShowSign1(_,__info)  
 self.fontdays:setString(#(__info.m_reciveArr))
    self.fntAll:setString(__info.m_totalAward/100)
        if __info.m_isCirculate==1 then
        self.labelTips:loadTexture("hall/image/huodong/text1_1.png",0)
    else
        self.labelTips:loadTexture("hall/image/huodong/text1_2.png",0)
    end

    if __info.m_curDay>#(__info.m_reciveArr) then
        self.imagesign:loadTexture("hall/image/huodong/text_sign1.png")
        self.btnget:loadTextures("hall/image/huodong/anniu1.png","hall/image/huodong/anniu1.png","hall/image/huodong/anniu1.png")
        local textget = self.btnget:getChildByName("Image_title")
        textget:loadTexture("hall/image/huodong/text_get2.png")
    else
        self.imagesign:loadTexture("hall/image/huodong/text_sign2.png")
        self.btnget:loadTextures("hall/image/huodong/anniu2.png","hall/image/huodong/anniu2.png","hall/image/huodong/anniu2.png")
        local textget = self.btnget:getChildByName("Image_title")
        textget:loadTexture("hall/image/huodong/text_get1.png")
    end

    local channelInfo = __info.m_channelInfo
    local curDay = __info.m_curDay
    for i=1,#channelInfo do
        self.itemList[i] = self.tabItem:clone()
        self.itemList[i]:setVisible(true)
        local image1 = self.itemList[i]:getChildByName("image_1")
        local image2 = self.itemList[i]:getChildByName("image_2")
        local image2_1 = image2:getChildByName("image2_1")
        local image2_2 = image2:getChildByName("image2_2")
        image1:setVisible(false)
        image2:setVisible(true)
        if channelInfo[i].m_additionalAward==0 then
            image2_1:setVisible(true)
            image2_2:setVisible(false)
        else
            image2_1:setVisible(false)
            image2_2:setVisible(true)
            local font3=image2_2:getChildByName("font3")
            font3:setString(channelInfo[i].m_additionalAward/100)
        end
        local font1 = image2:getChildByName("font1")
        local font2 = image2:getChildByName("font2")
        font1:setString(channelInfo[i].m_award/100)
        font2:setString(channelInfo[i].m_day)
        self.scrollItem:pushBackCustomItem(self.itemList[i])

        if curDay==i then
            self.fntThis:setString(channelInfo[i].m_betLimit/100)
        end
    end

    local reciveArr =  __info.m_reciveArr
    for k,v in pairs(reciveArr) do 
        local image1 = self.itemList[v]:getChildByName("image_1")
        local image2 = self.itemList[v]:getChildByName("image_2")
        image1:setVisible(true)
        image2:setVisible(false)
        local image1_1 = image1:getChildByName("image1_1")
        local image1_2 = image1:getChildByName("image1_2")
        if channelInfo[v].m_additionalAward==0 then
            image1_1:setVisible(true)
            image1_2:setVisible(false)
        else
            image1_1:setVisible(false)
            image1_2:setVisible(true)
            local font3=image1_2:getChildByName("font3")
            font3:setString(channelInfo[v].m_additionalAward/100)
        end
    end

    if __info.m_isCirculate==0 then
        if curDay>(#channelInfo) then
            self.imagesign:setVisible(false)
            self.btnget:setVisible(false)
            local texttoday = self.node:getChildByName("Image_1")
            texttoday:setVisible(false)
            local Image_2 = self.node:getChildByName("Image_2")
            Image_2:setVisible(true)
        else
            self.imagesign:setVisible(true)
            self.btnget:setVisible(true)
            local texttoday = self.node:getChildByName("Image_1")
            texttoday:setVisible(true)
            local Image_2 = self.node:getChildByName("Image_2")
            Image_2:setVisible(false)
        end
    end 
end

function SignHongbaoLayer:showTaskList(_,info)   
    if info.m_retCode==0 then 
        table.sort( info.m_taskList ,function(v1, v2) 

                if v1.m_isAcceptedReward == 0 and v2.m_isAcceptedReward == 0 then
					return v1.m_curProcess/v1.m_targetNum > v2.m_curProcess/v2.m_targetNum
				elseif v1.m_isAcceptedReward == 0 and v2.m_isAcceptedReward == 1 then
					return true
				else
					return false
				end
            end
            )
        --table.sort( info.m_taskList ,function(v1, v2) return v2.m_isAcceptedReward ==1  end  )
        dump(info.m_taskList)
        self.m_taskList = {}
        self.ListView_Task:removeAllItems()
        for k,v in pairs(info.m_taskList) do
            local item = self.Task_item:clone()
            item.m_taskId = v.m_taskId 
            if TaskData[v.m_taskId].convert == 1 then
                v.m_targetNum = v.m_targetNum*0.01
                v.m_curProcess = v.m_curProcess*0.01
            end 
            item.m_isFinsh = v.m_isFinsh
            item.m_isAcceptedReward = v.m_isAcceptedReward
            local panel =item:getChildByName("Panel_Reward")
            local btn = panel:getChildByName("Button_Reward")
            panel.m_isFinsh = v.m_isFinsh
            panel.m_taskId = v.m_taskId 
            panel:addTouchEventListener(function(sender,eventType)
                if eventType == ccui.TouchEventType.ended then 
                    if sender.m_isFinsh == 1 then
                        print("sender.m_taskId  "..sender.m_taskId)
                        ConnectManager:send2Server(Protocol.LobbyServer,"CS_C2H_AcceptTaskReward_Req", {sender.m_taskId})
                    else 
                        jumpToGame(TaskData[v.m_taskId].gameId,self)
                    end
                end    
            end)
            if v.m_isAcceptedReward == 1 then
                btn:loadTexture("hall/image/task/unget.png")
                panel:setEnabled(false) 
            end 
            if v.m_isFinsh == 0 then 
                 btn:loadTexture("hall/image/task/get1.png") 
            end
            local icon = item:getChildByName("icon")
            local proccess = item:getChildByName("proccess")
            proccess:setPercent(v.m_curProcess/v.m_targetNum*100)   
            icon:loadTexture(string.format("hall/image/task/%d.png",TaskData[v.m_taskId].gameId)) 
            local title = item:getChildByName("title")
            local titleFontSize = title:getFontSize()
           
            local continueTaskCoinLimit = math.floor( v.m_continueTaskCoinLimit / 100 )
            local contentStr = string.format(TaskData[v.m_taskId].content,v.m_targetNum, continueTaskCoinLimit)

            local richText = ccui.RichText:create()
            item:addChild(richText)
            richText:setPosition(cc.p(title:getPosition()))

            local str = ""
            local texts = RichLabel.getTransformText(contentStr)
            for i,v in ipairs(texts) do
                local element = ccui.RichElementText:create(i, v.color, 255, v.text, "ttf/jcy.TTF", titleFontSize)
                str = str..v.text
                richText:pushBackElement(element)
            end
            title:setString(str)
            local titleSize = title:getContentSize()
            local titleMaxWidth = 365.0
            if (titleSize.width > titleMaxWidth) then
               local titleScale = math.max(titleMaxWidth / titleSize.width, 0.6)
               richText:setScale(titleScale)
               richText:setPositionX(title:getPositionX() + 7)
            end
            title:setVisible(false)

            -- local continueTaskCoinLimit = v.m_continueTaskCoinLimit / 100
            -- title:setString(string.format(TaskData[v.m_taskId].content,v.m_targetNum, math.floor(continueTaskCoinLimit)))

            -- local titleSize = title:getContentSize()
            -- local titleMaxWidth = 366.0
            -- if (titleSize.width > titleMaxWidth) then
            --    local titleScale = math.max(titleMaxWidth / titleSize.width, 0.6)
            --    title:setScale(titleScale)
            --    title:setPositionX(title:getPositionX() + 6)
            -- end
            
            local content = item:getChildByName("content")
            content:setString(v.m_curProcess.."/"..v.m_targetNum)
            local reward = item:getChildByName("reward")
            reward:setString(v.m_reward*0.01)
            table.insert(self.m_taskList,panel)
            self.ListView_Task:pushBackCustomItem(item)     
        end
    elseif info.m_retCode==-1235 then
        TOAST("任务活动未开放")
    elseif info.m_retCode==-1100 then
        TOAST("任务不存在")
     elseif info.m_retCode==-1101 then
        TOAST("任务奖励已领取")
     elseif info.m_retCode==-1102 then
        TOAST("任务未完成不可领取奖励") 
    end 
    
end
function SignHongbaoLayer:showSafeGold(_,info) 
    dump(info)
    if info.m_result==0 then
         self.viptext:setString(info.m_vipLevel)
        self.gettext:setString(info.m_totalCnt)
        self.lefttext:setString(info.m_leftCnt)
        self.Text_6:setString(string.format("少于%d金币",info.m_coinLimit*0.01))
        self.Text_8:setString(string.format("多%d次，非VIP用户领取需要分享到微信。",info.m_totalCnt))
        self.Text_4:setString(info.m_succourAward*0.01)
        if info.m_isCanRecive ==0 then
            if info.m_reciveCnt+1 >= info.m_shareConditionCnt then
                  if Player:getVipLevel() == 0 then
                        self.Button_Get:loadTextures("hall/image/safegold/unshare.png","","",0)
                    else
                        self.Button_Get:loadTextures("hall/image/safegold/unget.png","","",0)
                    end
            else
                self.Button_Get:loadTextures("hall/image/safegold/unget.png","","",0)
            end
            self.Button_Get:setEnabled(false)
        else
            if info.m_reciveCnt+1 >= info.m_shareConditionCnt then
                if Player:getVipLevel() == 0 then
                    self.Button_Get:loadTextures("hall/image/safegold/share.png","","",0)
                    self.m_Type = 1
                else
                    self.Button_Get:loadTextures("hall/image/safegold/get.png","","",0)
                end  
            else
                self.Button_Get:loadTextures("hall/image/safegold/get.png","","",0)
                self.m_Type = 0
            end
            self.Button_Get:setEnabled(true)
        end
    elseif info.m_result==-1237 then
        TOAST("救济金功能未开放")
    elseif info.m_result==-1238 then
        TOAST("您没有破产,不符合领取救济金条件")
    elseif info.m_result==-1239 then
        TOAST("尊贵的VIP会员，您今日领取救济金次数已达上限")
    elseif info.m_result==-1240 then
        TOAST("您今日领取救济金次数已达上限")
    elseif info.m_result==-1241 then
        TOAST("您今日已领取过救济金,请分享朋友圈可以继续领取")
    elseif -1250 == info.m_result then
        TOAST("您金币在视讯中,不符合领取救济金条件")
    elseif -1251 == info.m_result then
        TOAST("您今日提现金额大于充值金额,不符合领取救济金条件")
    end
end
function SignHongbaoLayer:receiveSafeGold(_,info)
    if 0 == info.m_result then
        if info.m_isCanRecive == 0 then
            self.Button_Get:setEnabled(false)
            if Player:getVipLevel() == 0 then
                self.Button_Get:loadTextures("hall/image/safegold/unshare.png","","",0)
            else
                self.Button_Get:loadTextures("hall/image/safegold/unget.png","","",0)
            end
        else
            self.Button_Get:setEnabled(true)
            if Player:getVipLevel() == 0 then
                self.Button_Get:loadTextures("hall/image/safegold/share.png","","",0)
                self.m_Type = 1
            else
                self.Button_Get:loadTextures("hall/image/safegold/get.png","","",0)
                self.m_Type = 0
            end
        end
        g_AudioPlayer:playEffect("hall/sound/dropcoin.mp3")
       
        self.lefttext:setString(info.m_leftCnt)
        if self.m_pJieSuanEffect ~= nil then
            self.m_pJieSuanEffect:resetSystem()
        else
            self.m_pJieSuanEffect = cc.ParticleSystemQuad:create("hall/image/safegold/jiangjinyu_lizi4.plist") 
            --self.m_pJieSuanEffect:setAutoRemoveOnFinish(true)
            --self.m_pJieSuanEffect:setPosition(cc.p(viewSize.width/2, -50))
            self.m_pJieSuanEffect:setPosition(cc.p(0,0))
            self.node:addChild(self.m_pJieSuanEffect, 0)
        end
    elseif info.m_result==-1237 then
        TOAST("救济金功能未开放")
    elseif info.m_result==-1238 then
        TOAST("您没有破产,不符合领取救济金条件")
    elseif info.m_result==-1239 then
        TOAST("尊贵的VIP会员，您今日领取救济金次数已达上限")
    elseif info.m_result==-1240 then
        TOAST("您今日领取救济金次数已达上限")
    elseif info.m_result==-1241 then
        TOAST("您今日已领取过救济金,请分享朋友圈可以继续领取")
    elseif -1250 == info.m_result then
        TOAST("您金币在视讯中,不符合领取救济金条件")
    elseif -1251 == info.m_result then
        TOAST("您今日提现金额大于充值金额,不符合领取救济金条件")
    end
end
function SignHongbaoLayer:onTouchCallback(sender)
    local posx = 0
    if (self._winSize.width / self._winSize.height > 1.78) then 
        posx = (self._winSize.width/display.scaleX-self._winSize.width)/ 2
    end
    local name = sender:getName()
    local num = sender:getTag()
    if name == "button_close" then
        self:close() 
    elseif name == "button_get" then
        g_signhongbaoController:PlayerSignInReq()
     elseif name == "panel1" then 
        for k,v in pairs(self._panelBtn) do 
            if k==num then
                self._panelBtn[k]:getChildByName("light"):setVisible(true)
                self._panelBtn[k]:getChildByName("unlight"):setVisible(false)
                self.m_panelList[k]:setVisible(true)
            --    self._panelBtn[k]:getChildByName("text"):setTextColor(cc.c4b(126,66,13,255))
            else
                self._panelBtn[k]:getChildByName("light"):setVisible(false)
                self._panelBtn[k]:getChildByName("unlight"):setVisible(true)
                self.m_panelList[k]:setVisible(false)
          --      self._panelBtn[k]:getChildByName("text"):setTextColor(cc.c4b(222,194,148,255))
            end
        end
    elseif name == "Button_Get" then
        if self.m_Type == 0 then
             ConnectManager:send2Server(Protocol.LobbyServer,"CS_C2H_ReciveSuccour_Req", {})
        else
            -- local _hasWeChat = self:hasWeChat()
            -- if not _hasWeChat then
            --     TOAST("未安装微信，无法使用微信分享")
            --     return
            -- end

            local m_callback = function(success)
                if (success) then
                    ConnectManager:send2Server(Protocol.LobbyServer,"CS_C2H_DayShare_Req", {})
                    ConnectManager:send2Server(Protocol.LobbyServer,"CS_C2H_ReciveSuccour_Req", {})
                -- else
                --     TOAST("-------------------")
                end
            end
            local picPath1 = "res/ui/img.jpg"
            local picPath2 = cc.UserDefault:getInstance():getStringForKey("erweima")
            if cc.FileUtils:getInstance():isFileExist(picPath2) then
            else
                picPath2 = "res/ui/white.jpg"
            end
            local picPath = self:createImageFileWithTwoImage(picPath1,picPath2,1093,613)
            print(picPath)
            if device.platform == "android" then

    	        local javaClassName = "com/XinBo/LoveLottery/wxapi/WXEntryActivity"
                local javaMethodName = "OnShare"
                local javaParams = {
                    m_callback,
                    "",
                    picPath,
                    "",
                    "",
                    true,
                }
                local javaMethodSig = "(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V"
                luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
            elseif device.platform == "ios" then
                local args = { num1 = "", num2 = "", num3 = picPath, num4 = "", num5 = m_callback }
                local luaoc = require "cocos.cocos2d.luaoc"
                local className = "MilaiPublicUtil"
                local ok, ret = luaoc.callStaticMethod(className,"shareWXTimeLine",args)
                if not ok then
                    cc.Director:getInstance():resume()
                else
                    print("The ret is:",tostring(ret))
                    --callback(tostring(ret))
                end

                local function callback_ios2lua(param)
                    if "success" == param then
                        print("object c call back success")
                    end
                end
                luaoc.callStaticMethod(className,"registerScriptHandler", {scriptHandler = callback_ios2lua } )
                luaoc.callStaticMethod(className,"callbackScriptHandler")
            -- elseif "windows" == device.platform then
            --     m_callback("success")
            end
       end
    end
end
function SignHongbaoLayer:hasWeChat()
    if device.platform == "android" then
        local className = "com/XinBo/LoveLottery/wxapi/WXEntryActivity"
        local sig = "()Z"
        local ok, ret = luaj.callStaticMethod(className, "hasWeChat", nil, sig)
        if ok then
            return ret
        end
    elseif device.platform == "ios" then
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "MilaiPublicUtil"
        local ok, ret = luaoc.callStaticMethod(className,"hasWeChat", nil)
        if ok then
            return ret
        end
    end
	return true
end
function SignHongbaoLayer:createRenderNodeWithPath(path, posX, posY, scale)
        local sprite = nil
        if path then
            sprite = cc.Sprite:create(path)
            sprite:setAnchorPoint(cc.p(0,0))
            sprite:setPosition(cc.p(posX,posY))
            sprite:setScale(scale)
        end
        return sprite
end
function SignHongbaoLayer:createImageFileWithTwoImage(firstImagePath, secondImagePath, width, height)
    local firstNode  =  self:createRenderNodeWithPath(firstImagePath, 0, 0, 1)
    local secondNode = self:createRenderNodeWithPath(secondImagePath, 680, 95, 1)
    local toFileName = "pandaWXSharePic.png"
    local renderTexture = self:createRenderTextureWithNodes(firstNode, secondNode, width, height)
    if renderTexture then
        local saveRet = renderTexture:saveToFile(toFileName, cc.IMAGE_FORMAT_JPEG, false)
        cc.Director:getInstance():getTextureCache():removeTextureForKey(cc.FileUtils:getInstance():getWritablePath()..toFileName)
        if saveRet then
            return  cc.FileUtils:getInstance():getWritablePath() .. toFileName
        else
            print("保存图片失败")
            TOAST("创建分享图片失败")
            return nil
        end
    end
end
function SignHongbaoLayer:createRenderTextureWithNodes(firstRenderNode, secondRenderNode, width, height)
    local renderTexture = cc.RenderTexture:create(width, height)
    renderTexture:beginWithClear(0,0,0,0)

    if firstRenderNode then
        firstRenderNode:getTexture():setTexParameters(0x2601, 0x2601, 0x812f, 0x812f)
    end

    if firstRenderNode then
        firstRenderNode:visit()
    end
   
    if secondRenderNode then
        secondRenderNode:visit()
    end

    renderTexture:endToLua()
    return renderTexture
end
function SignHongbaoLayer:setupViews()
    dump(__info,"info==")
    self.node = UIAdapter:createNode(SIGN_PATH); 
    self:addChild(self.node,10000); 
    UIAdapter:adapter(self.node, handler(self, self.onTouchCallback))
    UIAdapter:praseNode(self.node,self)

    if (self._winSize.width / self._winSize.height > 1.78) then 
        self.node:setScaleX(1/display.scaleX)
    end

    self.fontdays = self.days
    self.imagesign = self.Image_sign
    self.btnget = self.button_get
    self.tabItem = self.node_hongbao
    self.scrollItem = self.list_hongbao
    self.fntAll = self.font_all
    self.fntThis = self.font_this
    self.labelTips = self.text_tip

   
    --self.fntThis:setString(__info.m_betLimit/100)
     self._tabItem = self["panel_tabItem"]
     local temp_text = ccui.Text:create()
    temp_text:setFontName("ttf/jcy.TTF")
    temp_text:setFontSize(30)
    temp_text:setString("123456")
    temp_text:setAnchorPoint(cc.p(0.5,0.5))
    temp_text:setPosition(cc.p(130,40.5))
    temp_text:setName("text")
    self._tabItem:addChild(temp_text)
    self.m_panelList = {self.Panel_Task,self.Panel_Sign,self.Panel_SafeGold}
    for k,v in pairs(self._Datas1) do
        self._panelBtn[k] = self._tabItem:clone() 
        self._panelBtn[k]:setName("panel1")
        self._panelBtn[k]:setTag(k)
        if k==1 then
            self._panelBtn[k]:getChildByName("light"):setVisible(true)
            self._panelBtn[k]:getChildByName("unlight"):setVisible(false) 
            self.m_panelList[k]:setVisible(true)
           -- self._panelBtn[k]:getChildByName("text"):setTextColor(cc.c4b(126,66,13,255))
        else
            self._panelBtn[k]:getChildByName("light"):setVisible(false)
            self._panelBtn[k]:getChildByName("unlight"):setVisible(true) 
            self.m_panelList[k]:setVisible(false)
          --  self._panelBtn[k]:getChildByName("text"):setTextColor(cc.c4b(222,194,148,255))
        end
        self._panelBtn[k]:getChildByName("text"):setString(self._Datas1[k].m_title)
        self.list_button:pushBackCustomItem(self._panelBtn[k])
    end
    
    --救济金默认显示
    self.viptext:setString(0)
    self.gettext:setString(0)
    self.lefttext:setString(0)
    self.Text_6:setString("少于30金币")
    self.Text_8:setString("多1次，非VIP用户领取需要分享到微信。")
    self.Text_4:setString(0)
    -- if info.m_isCanRecive ==0 then
        -- if info.m_reciveCnt+1 >= info.m_shareConditionCnt then
        --     if Player:getVipLevel() == 0 then
        --             self.Button_Get:loadTextures("hall/image/safegold/unshare.png","","",0)
        --         else
        --             self.Button_Get:loadTextures("hall/image/safegold/unget.png","","",0)
        --         end
        -- else
            
        -- end
    self.Button_Get:loadTextures("hall/image/safegold/unget.png","","",0)
    self.Button_Get:setEnabled(false)

     ConnectManager:send2Server(Protocol.LobbyServer,"CS_C2H_ShowSuccour_Req", {})
     ConnectManager:send2Server(Protocol.LobbyServer,"CS_C2H_GetTaskList_Req", {})
     g_signhongbaoController:ShowSignInChannelReq()
     
end

function SignHongbaoLayer:onShowSign(msg,__info)
    TOAST("签到成功!")
    local i = __info.m_day
    local image1 = self.itemList[i]:getChildByName("image_1")
    local image2 = self.itemList[i]:getChildByName("image_2")
    image1:setVisible(true)
    image2:setVisible(false)
    local image1_1 = image1:getChildByName("image1_1")
    local image1_2 = image1:getChildByName("image1_2")
    if __info.m_additionAward==0 then
        image1_1:setVisible(true)
        image1_2:setVisible(false)
    else
        image1_1:setVisible(false)
        image1_2:setVisible(true)
        local font3=image1_2:getChildByName("font3")
        font3:setString(__info.m_additionAward/100)
    end
    self.fontdays:setString(#(__info.m_reciveArr))
    self.imagesign:loadTexture("hall/image/huodong/text_sign2.png")
    self.btnget:loadTextures("hall/image/huodong/anniu2.png","hall/image/huodong/anniu2.png","hall/image/huodong/anniu2.png")
    local textget = self.btnget:getChildByName("Image_title")
    textget:loadTexture("hall/image/huodong/text_get1.png")
end

return SignHongbaoLayer