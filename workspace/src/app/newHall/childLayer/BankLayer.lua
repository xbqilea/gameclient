--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion 
-- 布局文件
local _ = {}
local BANK_UI_PATH = "hall/SafeBoxView.csb";
local HNlayer = import("..HNLayer")
local BankLayer = class("BankLayer",function()
    return HNlayer.new()
end)
function BankLayer:ctor()
    self:myInit()
    self:setupViews()
      self:setNodeEventEnabled(true)
    addMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS, handler(self, self.onPlayerInfoUpdated))
    addMsgCallBack(self, MSG_BANK_UPDATE_SUCCESS, handler(self, self.netMsgHandler))
    -- 注册析构函数
    ToolKit:registDistructor( self, handler(self, self.onDestory) )
end
function BankLayer:onPlayerInfoUpdated()
    self._acc_coin = tonumber(Player:getParam( "GoldCoin" ))
    self._bank_coin = tonumber(Player:getParam( "BankGoldCoin" ))
    self._curMoney:setString(self._acc_coin)
    self._depositMoney:setString(self._bank_coin) 
    print(self._acc_coin)
     print(self._bank_coin)
end

function BankLayer:myInit()
    self._selectType =1
    self._csNode = nil
    self._storeLayout = nil
    self._curMoney = nil
    self._depositMoney = nil
    self.textField_shuru= nil
    self.m_bankType =1 
end
function BankLayer:onTouchCallback(sender)
    local name = sender:getName()
    print("name ==", name)
    if name == "ok" then 
         _:disableQuickClick(sender)
        if self.m_bankType == 1 then
            self:handlePopBank()
        else
            self:handlePushBank()
        end
       
    elseif name == "button_closeBtn" then
        self:close() 
     elseif name == "panel_tab2" then 
		self.textField_shuru:setString(""); 
        self.light2:setVisible(true) 
        self.light1:setVisible(false) 
         self.unlight2:setVisible(false) 
        self.unlight1:setVisible(true) 
        self.sprite_goldSpr:setTexture("hall/image/safebox/savemoney.png")
        self.ok:loadTextures("hall/image/safebox/save.png")
        self.m_bankType =2
    elseif name == "panel_tab1" then
        self.light2:setVisible(false) 
        self.light1:setVisible(true) 
         self.unlight2:setVisible(true) 
        self.unlight1:setVisible(false) 
		self.textField_shuru:setString(""); 
	    self.sprite_goldSpr:setTexture("hall/image/safebox/getmoney.png")
        self.ok:loadTextures("hall/image/safebox/get.png")
        self.m_bankType =1
    elseif name == "button_clearInputBtn" then
        self.textField_shuru:setString(""); 
    elseif name == "back" then
        local str = self.textField_shuru:getString()
        if str~="" then
            str = string.sub(str,1, string.len(str)-1)
        end 
        self.textField_shuru:setString(str);  
    elseif sender:getTag()<10 and sender:getTag()>=0 then
        local str = self.textField_shuru:getString()
        local text = str..sender:getTag()
        if self.m_bankType== 1 then 
            if tonumber(text)<=self._bank_coin  then 
                if  tonumber(text)==0 then
                     TOAST("输入金额不能为0") 
                     return
                end
                self.textField_shuru:setString(text);  

            else
                TOAST("输入金额不能大于已存金币")
            end
        else
            if tonumber(text)<=self._acc_coin then
                 if  tonumber(text)==0 then
                     TOAST("输入金额不能为0") 
                     return
                end
                self.textField_shuru:setString(text);  
            else
                 TOAST("输入金额不能大于当前金币")
            end
        end
    end
end 
function BankLayer:setupViews() 
    
	local node = UIAdapter:createNode(BANK_UI_PATH);
   
    UIAdapter:adapter(node, handler(self, self.onTouchCallback))
	self:addChild(node);
    local center =  node:getChildByName("center"); 
	self._csNode = center:getChildByName("image_Bg");  
     local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
	-- 关闭
--	local closeBtn = self._csNode:getChildByName("Button_close");
--	closeBtn:addTouchEventListener(function()
--		self:close();
--	end);
     
	-- 内容面板
	self._storeLayout = self._csNode:getChildByName("panel_takeOutPanel");
 

	-- 金额显示
	self._curMoney = self._storeLayout:getChildByName("text_goldInHand"); 
	self._depositMoney = self._storeLayout:getChildByName("text_goldInBox");
    local Image_92 =self._storeLayout:getChildByName("Image_92");
    self.sprite_goldSpr = Image_92:getChildByName("sprite_goldSpr");
    local Image_3 = Image_92:getChildByName("Image_3");
    self.textField_shuru =  Image_3:getChildByName("textField_shuru"); 
    local Image_1 = self._csNode:getChildByName("Image_1");
    local panel_tab1 = Image_1:getChildByName("panel_tab1")
   local panel_tab2 = Image_1:getChildByName("panel_tab2")
    self.light1 = panel_tab1:getChildByName("light"); 
    self.unlight1 = panel_tab1:getChildByName("unlight");
    self.unlight2 = panel_tab2:getChildByName("unlight");
    self.light2 = panel_tab2:getChildByName("light"); 
	self.light2:setVisible(false) 
    self.unlight1:setVisible(false) 
    local node_keyboard = Image_92:getChildByName("node_keyboard");
    self.ok = node_keyboard:getChildByName("ok"); 
    for i=0,9 do 
        local btn = node_keyboard:getChildByName("num"..i)
        btn:setTag(i)
    end
   self:onPlayerInfoUpdated() 
end
 

 
 
---------- 服务器回调 -----------
function BankLayer:netMsgHandler( __idStr, __info )
    if __info.m_optCode == 0 then
        -- 存款回调
        --[[if __info.m_optType == 1 and self._isPasswordLayer == false then
            TOAST(STR(10, 1))
            --self:cleanAllNumber()
        end--]]

        if __info.m_optType == 1 then
            TOAST(STR(10, 1))
        elseif __info.m_optType == 0 then
            TOAST(STR(6, 1))
        end
    else
        print("错误码提示----------->", __info.m_optCode)
        ToolKit:showErrorTip(__info.m_optCode)
    end
end

function BankLayer:isRightStr(edit)
    local str = edit:getString()
    if str ~= "" and not string.find(str, "^[+-]?%d+$") then
        edit:setString("")
        TOAST(STR(61, 1))
        return false
    end
    return true
end

function BankLayer:handlePopBank()
 -- print("self._curMoney:getName()", self._curMoney:getName())
    if self:getNumberForEditString(self.textField_shuru) == 0 then
        self.textField_shuru:setString("");
        TOAST(STR(59, 1))
        return        
    elseif self:isRightStr(self.textField_shuru)  == false then
        return
    elseif tonumber(self.textField_shuru:getString()) > Player:getParam( "BankGoldCoin" ) then
        TOAST(STR(4, 1))
        return
    elseif tonumber(string.sub(self.textField_shuru:getString(), 1)) == 0 then
        self.textField_shuru:setString("");
        TOAST(STR(59, 1))
        return 
    end
    local gold = tonumber(self.textField_shuru:getString())
    GlobalBankController:getMoneyFromBank(gold*100, 0, 123456, 0)
end

function BankLayer:getNumberForEditString(edit)
    local str = edit:getString()
    if str == "" then
        return 0
    end
    return tonumber(str)
end


function BankLayer:handlePushBank()
    if self:getNumberForEditString(self.textField_shuru) == 0 then
        TOAST(STR(60, 1))
        return     
    elseif self:isRightStr(self.textField_shuru) == false then
        return
    elseif tonumber(self.textField_shuru:getString()) > Player:getParam( "GoldCoin" ) then
        TOAST(STR(9, 1))
        return
    elseif tonumber(string.sub(self.textField_shuru:getString(), 1, 1)) == 0 then
        TOAST(STR(60, 1))
        return 
    end

    local gold = tonumber(self.textField_shuru:getString())
    GlobalBankController:saveMoneyToBank(gold*100, 0, 0)
end
------- 析构函数 ------
function BankLayer:onDestory()
    removeMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS)
    removeMsgCallBack(self, MSG_BANK_UPDATE_SUCCESS)
end

function _:setBtnEnabled(btn, enabled)
    btn:setEnabled(enabled)
    btn:setTouchEnabled(enabled)
   -- btn:setBright(enabled)
    
end



-- 防止频繁点击
function _:disableQuickClick(btn)
    _:setBtnEnabled(btn, false)
  --  btn:getChildByName("button_changeBtnFont"):loadTexture("dt/image/zcm_duihuan/zcm_dh7gray.png")
    performWithDelay(btn, function()
        _:setBtnEnabled(btn, true)
    end, 2)
end
return BankLayer