 

local HNlayer = import("..HNLayer")
local TargetShareLayer = class("TargetShareLayer",function()
    return HNlayer.new()
end) 

function TargetShareLayer:ctor()
    self:myInit()
    self:setupViews()
    ToolKit:registDistructor( self, handler(self, self.onDestroy))
end
function TargetShareLayer:onDestroy()
    removeMsgCallBack(self, MSG_REBATE_DAY_SHARE_ASK)  
    removeMsgCallBack(self, MSG_REBATE_DAY_SHAREGOLD_ASK)  
end
function TargetShareLayer:myInit()
    addMsgCallBack(self, MSG_REBATE_DAY_SHARE_ASK, handler(self, self.updateRebateDayShareInfo))  
    addMsgCallBack(self, MSG_REBATE_DAY_SHAREGOLD_ASK, handler(self, self.updateRebateDayShareGoldInfo))  
end
function TargetShareLayer:sendShareWechatReq()
    ConnectManager:send2Server(Protocol.LobbyServer,"CS_C2H_DayShare_Req", {})
end

function TargetShareLayer:sendShareGold()
    ConnectManager:send2Server(Protocol.LobbyServer,"CS_C2H_ShowDayShare_Req", {})
end
function TargetShareLayer:updateRebateDayShareInfo()
    print("updateRebateDayShareInfo", GlobalRebateController:getRebateDayShareDatas())
end
function TargetShareLayer:updateRebateDayShareGoldInfo(_,gold)
    
     self.text_gold:setString(gold*0.01)
end
function TargetShareLayer:createImageFileWithTwoImage(firstImagePath, secondImagePath, width, height)
    local firstNode  =  self:createRenderNodeWithPath(firstImagePath, 0, 0, 1)
    local secondNode = self:createRenderNodeWithPath(secondImagePath, 680, 95, 1)
    local toFileName = "pandaWXSharePic.png"
    local renderTexture = self:createRenderTextureWithNodes(firstNode, secondNode, width, height)
    if renderTexture then
        local saveRet = renderTexture:saveToFile(toFileName, cc.IMAGE_FORMAT_JPEG, false)
        cc.Director:getInstance():getTextureCache():removeTextureForKey(cc.FileUtils:getInstance():getWritablePath()..toFileName)
        if saveRet then
           return  cc.FileUtils:getInstance():getWritablePath() .. toFileName
        else
            print("保存图片失败")
            return nil
        end
    end
end
function TargetShareLayer:createRenderTextureWithNodes(firstRenderNode, secondRenderNode, width, height)
    local renderTexture = cc.RenderTexture:create(width, height)
    renderTexture:beginWithClear(0,0,0,0)

    if firstRenderNode then
        firstRenderNode:getTexture():setTexParameters(0x2601, 0x2601, 0x812f, 0x812f)
    end

    if firstRenderNode then
        firstRenderNode:visit()
    end
   
    if secondRenderNode then
        secondRenderNode:visit()
    end

    renderTexture:endToLua()
    return renderTexture
end
function TargetShareLayer:createRenderNodeWithPath(path, posX, posY, scale)
        local sprite = nil
        if path then
            sprite = cc.Sprite:create(path)
            sprite:setAnchorPoint(cc.p(0,0))
            sprite:setPosition(cc.p(posX,posY))
            sprite:setScale(scale)
        end
        return sprite
end
function TargetShareLayer:onTouchCallback(sender)
    local picUrl = cc.UserDefault:getInstance():getStringForKey( "erweima__url" ,"http://www.baidu.com")
    local posx = 0
   
    local name = sender:getName()
    if name == "btn_close" then
        g_AudioPlayer:playEffect(GAME_SOUND_CLOSE) 
        self:close() 
    elseif name == "btn_target_1" then
          print("button_wxhaoyou")
        local m_callback = function(success) if (success) then self:sendShareWechatReq() end end
        local picPath1 = "res/ui/img.jpg"
        local picPath2 = cc.UserDefault:getInstance():getStringForKey("erweima")
        if cc.FileUtils:getInstance():isFileExist(picPath2) then
        else
            picPath2 = "res/ui/white.jpg"
        end
        local picPath = self:createImageFileWithTwoImage(picPath1,picPath2,1093,613)
        print(picPath)
        if device.platform == "android" then
    	    local javaClassName = "com/XinBo/LoveLottery/wxapi/WXEntryActivity"
            local javaMethodName = "OnShare"
            local javaParams = {
                m_callback,
                "",
                picPath,
                "",
                "",
                false,
                
            }
            local javaMethodSig = "(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V"
            luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        elseif device.platform == "ios" then
            local args = { num1 = "", num2 = "", num3 = picPath, num4 = picUrl, num5 = m_callback }
            local luaoc = require "cocos.cocos2d.luaoc"
            local className = "MilaiPublicUtil"
            local ok, ret  = luaoc.callStaticMethod(className,"shareWX",args)
            if not ok then
                cc.Director:getInstance():resume()
            else
                print("The ret is:",tostring(ret))
                --callback(tostring(ret))
            end

            local function callback_ios2lua(param)
                if "success" == param then
                    print("object c call back success")
                end
            end
            luaoc.callStaticMethod(className,"registerScriptHandler", {scriptHandler = callback_ios2lua } )
            luaoc.callStaticMethod(className,"callbackScriptHandler")
        end
    elseif name == "btn_target_2" then   
         print("button_pyquan")
        local m_callback = function(success) if (success) then self:sendShareWechatReq() end end
        local picPath1 = "res/ui/img.jpg"
        local picPath2 = cc.UserDefault:getInstance():getStringForKey("erweima")
        if cc.FileUtils:getInstance():isFileExist(picPath2) then
        else
            picPath2 = "res/ui/white.jpg"
        end
        local picPath = self:createImageFileWithTwoImage(picPath1,picPath2,1093,613)
        print(picPath)
        if device.platform == "android" then
    	    local javaClassName = "com/XinBo/LoveLottery/wxapi/WXEntryActivity"
            local javaMethodName = "OnShare"
            local javaParams = {
                m_callback,
                "",
                picPath,
                "",
                "",
                true,
            }
            local javaMethodSig = "(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V"
            luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        elseif device.platform == "ios" then
            local args = { num1 = "", num2 = "", num3 = picPath, num4 = picUrl, num5 = m_callback }
            local luaoc = require "cocos.cocos2d.luaoc"
            local className = "MilaiPublicUtil"
            local ok, ret = luaoc.callStaticMethod(className,"shareWXTimeLine",args)
            if not ok then
                cc.Director:getInstance():resume()
            else
                print("The ret is:",tostring(ret))
                --callback(tostring(ret))
            end

            local function callback_ios2lua(param)
                if "success" == param then
                    print("object c call back success")
                end
            end
            luaoc.callStaticMethod(className,"registerScriptHandler", {scriptHandler = callback_ios2lua } )
            luaoc.callStaticMethod(className,"callbackScriptHandler")
        end
    end
end

function TargetShareLayer:setupViews()
    --g_AudioPlayer:playEffect("dt/sound/binding_sound.mp3")
	local node = UIAdapter:createNode("hall/TargetShareLayer.csb");
	 g_AudioPlayer:playEffect("hall/sound/wuxianjinbi.mp3") 
	self:addChild(node); 
     node:setPositionY((display.height - 750) / 2) 
     UIAdapter:adapter(node, handler(self, self.onTouchCallback))
     UIAdapter:praseNode(node,self)
    --node
    self.m_pNodeRoot    = node:getChildByName("center")
    self.m_pNodeRoot:setPositionX((display.width - 1334) / 2) 
    self:sendShareGold()
end
 
  
return TargetShareLayer