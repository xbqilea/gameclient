--
-- 兑换
--
local _ = {}
local BindYHKLayer = import(".BindYHKLayer")
local BindZFBLayer = import(".BindZFBLayer")
local CustomerWeb =require('app.newHall.common.CustomerWeb')
local HNlayer = import("..HNLayer")
local editTxt = nil
local ExchangeRmbLayer = class("ExchangeRmbLayer", function ()
    return HNlayer.new()
end)

ExchangeRmbLayer.exchangeType = 
{
    zfb = 1 ,  -- 支付宝
    yhk = 2    -- 银行卡
}

function ExchangeRmbLayer:ctor()
    print("ExchangeRmbLayer:ctor")
  --  dump( __params )
   -- self.params = __params
    self.m_exchangeType = ExchangeRmbLayer.exchangeType.zfb
        self:setNodeEventEnabled(true)
    addMsgCallBack(self, MSG_EXCHANGE2MONEY_ACK, handler(self, self.onExchange2Money ))
    addMsgCallBack(self, MSG_ALIPAYBIND_ACK, handler(self, self.onAlipayBindEx ))
    addMsgCallBack(self, MSG_BANKBIND_ACK, handler(self, self.onBankBindEx )) 
    addMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS, handler(self, self.onPlayerInfoUpdated))
    self:myInit()
    self:setupViews()
end
function ExchangeRmbLayer:onPlayerInfoUpdated()
    self.goldNumLabel:setString( (Player:getGoldCoin()) )
    local t = Player:getDamaNeed()
    local t1 = Player:getDamaAmount()
     self.Text_6:setString(string.format("%0.2f",Player:getDamaNeed()*0.01))
    self.Text_8:setString(string.format("%0.2f",Player:getDamaAmount()*0.01))
end
function ExchangeRmbLayer:onEnter()
    print("*******ExchangeRmbLayer:onEnter()********")
end

function ExchangeRmbLayer:myInit()
    self.root = nil
    self._imageBG = nil
    self.goldNumLabel= nil
    self.tfGoldNum = nil
    self.goldSlider_1 = nil
    self._btnToAlipay= nil
    self._btnToBankCard= nil
    self._btn_ToAliLayer =nil
    self._btn_ToBankLayer =nil
    self._bindNumByAliPay = nil
    self._bindNumByBankCard = nil
     self.m_exchangeType =ExchangeRmbLayer.exchangeType.yhk
end
function ExchangeRmbLayer:openInputEditBox( ... )
	-- body
	--self.payField:attachWithIME()

   if UpdateFunctions.platform == "ios" then
        editTxt = ccui.EditBox:create(cc.size(355,45), "hall/image/bank/zcm_zc1_2.png")
    else
        editTxt = ccui.EditBox:create(cc.size(355,70), "hall/image/bank/zcm_zc1_2.png")
    end
    editTxt:setName("inputTxt")  
    editTxt:setAnchorPoint(0.5,0.5)  
    editTxt:setPosition(cc.p(self.image_openInput:getPositionX() / 2 - 30,self.image_openInput:getPositionY() / 2 - 65))                        --设置输入框的位置  
    editTxt:setFontSize(26)                            --设置输入设置字体的大小  
    editTxt:setMaxLength(10)                             --设置输入最大长度
    editTxt:setFontColor(cc.c4b(255,255,255,255))         --设置输入的字体颜色  
    editTxt:setFontName("sim2542hei")                       --设置输入的字体为simhei.ttf
    if UpdateFunctions.platform == "ios" then
        editTxt:setPlaceholderFontSize(16)
    else
        editTxt:setPlaceholderFontSize(24)
    end
	editTxt:setPlaceholderFontColor(cc.c4b(184,187,182,255))  
    editTxt:setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE)
    -- editTxt:setInputMode(cc.EDITBOX_INPUT_MODE_NUMERIC) --设置数字符号键盘  
	editTxt:setPlaceHolder("输入兑换金额")               --设置预制提示文本  
    editTxt:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE)  --输入键盘返回类型，done，send，go等KEYBOARD_RETURNTYPE_DONE  
    -- editTxt:setInputMode(cc.EDITBOX_INPUT_MODE_NUMERIC) --输入模型，如整数类型，URL，电话号码等，会检测是否符合  
    editTxt:registerScriptEditBoxHandler(function(eventname,sender) self:editboxHandle(eventname,sender) end) --输入框的事件，主要有光标移进去，光标移出来，以及输入内容改变等  
    self.image_openInput:addChild(editTxt,5)  
--  editTxt:setHACenter() --输入的内容锚点为中心，与anch不同，anch是用来确定控件位置的，而这里是确定输入内容向什么方向展开(。。。说不清了。。自己测试一下)  
end
function ExchangeRmbLayer:editboxHandle(strEventName,sender)  
    if strEventName == "began" then  
    	sender:setPlaceHolder("")                                    --光标进入，清空内容/选择全部  
    elseif strEventName == "ended" then  
        if sender:getText() then
        	sender:setPlaceHolder("输入兑换金额") 
        end                                                        --当编辑框失去焦点并且键盘消失的时候被调用  
    elseif strEventName == "return" then  
         self:updataPercent()                                                       --当用户点击编辑框的键盘以外的区域，或者键盘的Return按钮被点击时所调用  
    elseif strEventName == "changed" then
    	 print(sender:getText(), "sender")
    	if  #sender:getText() > 0 then
          	self:updataPercent() 
  		end                                                     --输入内容改变时调用   
    end  
end 

function ExchangeRmbLayer:updataPercent( value)
	-- body
	local function reset( ... )
		-- body
		self.text_perentLabel:setString("0%")
		self.goldSlider_1:setPercent(0)
		editTxt:setText("")
	end

	local inputString = editTxt:getText()
	print("inputString",inputString)
	local percent = 0
	if #inputString == 0 then
		reset()
		return
	end

	if not tonumber(inputString) then
		TOAST("输入了非法字符,请正确输入")
		reset()
		return
	end

	if string.match(inputString, "%.") then
		reset()
		TOAST("不可输入小数")
		return
	end
	local insertNum = tonumber(inputString) 
	if math.floor(insertNum) < insertNum then
		TOAST("不可输入小数")
		reset()
		return
	end
 
      
    if  insertNum > Player:getGoldCoin() then
        insertNum = math.floor(Player:getGoldCoin())
    end
   
    
     print("insertNum",insertNum)
    if insertNum > 0 then
        editTxt:setText( insertNum )
    else
        editTxt:setText("")
    end
    local per = math.ceil((insertNum/Player:getGoldCoin()) * 100)
     print("per   ",per)
    self.goldSlider_1:setPercent( per ) 
    self.text_perentLabel:setString(per.."%")
 
end
-- 初始化界面
function ExchangeRmbLayer:setupViews()
    self.root = UIAdapter:createNode("hall/ChangeView.csb")
    
    self:addChild(self.root)
  --  self.root:setPosition(self._winSize.width / 2,self._winSize.height / 2);
    self.ChangeRecord = UIAdapter:createNode("hall/ChangeRecord.csb");
    local center =self.ChangeRecord:getChildByName("center"); 
    self._imageBG =center:getChildByName("image_Bg");  
     local diffY = (display.size.height - 750) / 2
    self.ChangeRecord:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
     local image_bg = self.ChangeRecord:getChildByName("image_bg")
     local panel_empty = image_bg:getChildByName("panel_empty")
    panel_empty:setVisible(false)
    self.root:addChild(self.ChangeRecord)
    UIAdapter:adapter(self.root, handler(self, self.onTouchCallback))
    UIAdapter:praseNode(self.root,self)
  --  UIAdapter:parseCSDNode(self.root,self)
     local center =self.root:getChildByName("center"); 
    self._imageBG =center:getChildByName("image_Bg");  
     local diffY = (display.size.height - 750) / 2
    self.root:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
  
    -- 金币数量
    self.goldNumLabel = self._imageBG:getChildByName("bmfont_myMoneyLabel")
    self.goldNumLabel:setString( (Player:getGoldCoin()) )
    
    -- 兑换金币的数量
   -- local operationNode = self._imageBG:getChildByName("TextField_number")
  --  local gold_num_img = operationNode:getChildByName("gold_num_img") 
  local panel_contentPanel = self._imageBG:getChildByName("panel_contentPanel")
  self.image_openInput = panel_contentPanel:getChildByName("image_openInput")
  local Image_15 = panel_contentPanel:getChildByName("Image_15")
  local Image_16 = Image_15:getChildByName("Image_16")
  self.text_alipayAccount = Image_16:getChildByName("text_alipayAccount")
    self.Text_12:setString("收款银行卡")
    self.tfGoldNum =   self.image_openInput:getChildByName("TextField_number") 
    self.tfGoldNum:setVisible(false)
   -- self.tfGoldNum:setInputMode(cc.EDITBOX_INPUT_MODE_PHONENUMBER)
  --  self.tfGoldNum:addEventListener( handler( self, self.onccTextFieldCallback ) )

    
    -- 滑动条
    self.goldSlider_1 = panel_contentPanel:getChildByName("slider_moneySlider")
    self.goldSlider_1:addEventListener(handler(self,self.onTouchSliderCallBack))
    self.text_perentLabel =panel_contentPanel:getChildByName("text_perentLabel")
    self.text_perentLabel:setString("0%")
   local Image_left =  self._imageBG:getChildByName("Image_left")
   local listView_listTabs = Image_left:getChildByName("listView_listTabs")
   local panel_tab1 = listView_listTabs:getChildByName("panel_tab1")
   local panel_tab2 = listView_listTabs:getChildByName("panel_tab2")
    self.light1 = panel_tab1:getChildByName("light");
    self.image1 = panel_tab1:getChildByName("image");
    self.unlight1 = panel_tab1:getChildByName("unlight")
    self.light2 = panel_tab2:getChildByName("light");
    self.image2 = panel_tab2:getChildByName("image");
    self.unlight2 = panel_tab2:getChildByName("unlight")
	self.light2:setVisible(false)
    self.image2:setVisible(false)
--	panel_tab1:addTouchEventListener(handler(self,self.creatBindingCallBack));
--	panel_tab2:addTouchEventListener(handler(self,self.creatBindingCallBack));
	 
     
	
     self.btnBind = Image_15:getChildByName("button_bindAlipayBtn") 
     self.image_changeImg = self.btnBind:getChildByName("image_changeImg") 
   
	if Player:getStrBankID()~="" then 
          local t = Player:getStrBankID()
        local tbl = require("cjson").decode( t )
		self.text_alipayAccount:setString(tbl["payId"]);  
       self.btnBind:loadTextures("hall/image/duihuan/refer.png","hall/image/duihuan/refer.png","hall/image/duihuan/refer.png")
         
	else 
	    self.btnBind:loadTextures("hall/image/duihuan/bangding.png","hall/image/duihuan/bangding.png","hall/image/duihuan/bangding.png")
		self.text_alipayAccount:setString("未绑定银行卡");
	end 
   
	 
    -- 注册析构函数
    ToolKit:registDistructor( self, handler(self, self.onDestory) )
    --self:enableTouch(false)
    
    self.ChangeRecord:setVisible(false)
     UIAdapter:adapter( self.ChangeRecord, handler(self, self.onTouchCallback2))
     self:openInputEditBox()
   self.Text_6:setString(string.format("%0.2f",Player:getDamaNeed()*0.01))
    self.Text_8:setString(string.format("%0.2f",Player:getDamaAmount()*0.01))
    local str = ""
    if Player:getDamaNeed() <= Player:getDamaAmount() then
        str = "是（投注量已达标，可以兑换）"
    else
         str = "否（投注量未达标，不能兑换）"
    end
    self.Text_10:setString(str)
end
 function ExchangeRmbLayer:onTouchCallback2(sender)
     local name = sender:getName()
     if name == "button_close" then
        self.ChangeRecord:setVisible(false)
    end
end
function ExchangeRmbLayer:updateChangeRecordInfo(info)
    --刷新记录

    print("--刷新记录--")
    
    self.m_ChangeRecordData =info.data
     

    local image_bg = self.ChangeRecord:getChildByName("image_bg")
   
    
  
    local ListView_1 =  image_bg:getChildByName("ListView_1")
    ListView_1:setClippingEnabled(true)
    ListView_1:removeAllItems()
    local button_cellBtn1 = self.ChangeRecord:getChildByName("button_cellBtn1")
    local t = ccui.Widget:create();
    for k,v in pairs(self.m_ChangeRecordData) do

		local pContain = t:clone()
		pContain:setContentSize(button_cellBtn1:getContentSize()); 
		local button = button_cellBtn1:clone() 
		button:setPosition(420,30);
		pContain:addChild(button);
          local text_date = button:getChildByName("text_date")
        local text_goldBmf = button:getChildByName("text_goldBmf")
        local image_status = button:getChildByName("image_status")
        local text_status = button:getChildByName("text_status")
       -- local time = os.date("%Y-%m-%d",v.m_time)
        text_date:setPositionX(text_date:getPositionX() - 5)
        text_date:setAnchorPoint(cc.p(0.5, 0.5))
        text_date:setScale(0.7)
        text_date:setString(v.createTime)
        text_goldBmf:setString(v.cashFee)
        image_status:setVisible(true)
        text_status:setVisible(false)
        if v.status ==0 then
            image_status:loadTexture("hall/image/agent/examine.png")
        elseif v.status == 1 then
             image_status:loadTexture("hall/image/agent/success.png")
        elseif v.status == 2 then
            if v.note=="" then
                image_status:loadTexture("hall/image/agent/fail.png")
            else
                text_status:setVisible(true)
                image_status:setVisible(false)
                text_status:setString(v.note)
            end
        end
        local text_cnt =  button:getChildByName("text_cnt")
        text_cnt:setString(k)
		ListView_1:pushBackCustomItem(pContain);
	end  
end
-- 点击事件回调
function ExchangeRmbLayer:onTouchCallback( sender )
    local name = sender:getName()
    print("name=--",name)
     g_GameMusicUtil:playSound(GAME_SOUND_BUTTON);
    if name == "button_recordBtn" then  -- 兑换记录
   --   sendMsg(MSG_GOTO_STACK_LAYER, {layer = "app.hall.exchangeRmb.ExchangeRmbRecordLayer" ,  direction = DIRECTION.HORIZONTAL })
        local url = string.format(GlobalConf.DOMAIN.."api/itf/cash/info?userId=%d&cashType=%d",Player:getAccountID(),self.m_exchangeType) 
       local xhr = cc.XMLHttpRequest:new()
        xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
        xhr:open("GET",url) 
         ToolKit:addLoadingDialog(5,"正在加载中，请稍等......")
        local function onReadyStateChange()  
            if  xhr.response == "" then
              --  self:requestShopList()
              print("response = nil")
            else
            
                local response   = xhr.response
                local output = json.decode(response,1) 
                dump(output)
                if output.data~="" then
                    self:updateChangeRecordInfo(output)
                end
            end
             ToolKit:removeLoadingDialog()
        end
        xhr:registerScriptHandler(onReadyStateChange)
        xhr:send()
        self.ChangeRecord:setVisible(true)
    elseif name == "panel_tab2" then 
		editTxt:setText("");
		self.goldSlider_1:setPercent(0);
        self.light2:setVisible(true)
     --   self.image2:setVisible(true)
        self.unlight2:setVisible(false)
        self.light1:setVisible(false)
         self.unlight1:setVisible(true)
     --   self.image1:setVisible(false)
        self.Text_12:setString("收款支付宝")
        if (Player:getStrAlipayID()~="") then
	           local tbl = require("cjson").decode( Player:getStrAlipayID() )
		    self.text_alipayAccount:setString(tbl.payId); 
	        self.btnBind:loadTextures("hall/image/duihuan/refer.png","hall/image/duihuan/refer.png","hall/image/duihuan/refer.png")
	    else
	        self.btnBind:loadTextures("hall/image/duihuan/bangding.png","hall/image/duihuan/bangding.png","hall/image/duihuan/bangding.png")
		    self.text_alipayAccount:setString("未绑定支付宝");
	    end
         self.text_perentLabel:setString("0%")
          self.m_exchangeType =ExchangeRmbLayer.exchangeType.zfb
    elseif name == "panel_tab1" then
        self.light2:setVisible(false)
        self.unlight2:setVisible(true)
     --   self.image2:setVisible(false)
        self.light1:setVisible(true)
        self.unlight1:setVisible(false)
      --  self.image1:setVisible(true)
		editTxt:setText("");
		self.goldSlider_1:setPercent(0);
	     self.Text_12:setString("收款银行卡")
         if Player:getStrBankID()~="" then
	          self.btnBind:loadTextures("hall/image/duihuan/refer.png","hall/image/duihuan/refer.png","hall/image/duihuan/refer.png")
                local tbl = require("cjson").decode( Player:getStrBankID() )
		    self.text_alipayAccount:setString(tbl["payId"]);  
	    else
	         self.btnBind:loadTextures("hall/image/duihuan/bangding.png","hall/image/duihuan/bangding.png","hall/image/duihuan/bangding.png")
		    self.text_alipayAccount:setString("未绑定银行卡");
	    end 
         self.text_perentLabel:setString("0%")
           self.m_exchangeType =ExchangeRmbLayer.exchangeType.yhk
    elseif name == "button_maxBtn" then       -- 最大

      local goldNum = math.floor(Player:getGoldCoin())
     -- local goldNum1,goldNum2 = math.ceil(goldNum)
      if goldNum > 0 then
        editTxt:setText(  goldNum )
      else
        editTxt:setText( "" )
      end
--      local leftGold =goldNum- goldNum1
--      self.goldNumLabel:setString( leftGold )
   --   local per = (goldNum/Player:getGoldCoin()) * 100
      self.goldSlider_1:setPercent( 100 ) 
      self.text_perentLabel:setString("100%")
    elseif name == "button_closeBtn" then     -- 关闭
      self:close() 
    elseif name =="button_clearInput" then 
		self.goldSlider_1:setPercent(0);
		editTxt:setText("");
		editTxt:setPlaceHolder("输入兑换金额");
         self.text_perentLabel:setString("0%")
    elseif name == "button_changeBtn" then 
      
          if self.m_exchangeType == ExchangeRmbLayer.exchangeType.zfb and Player:getStrAlipayID() =="" then
                TOAST("请先绑定支付宝")
                return 
           end   
            if self.m_exchangeType == ExchangeRmbLayer.exchangeType.zfb and Player:getStrAlipayID() ~="" and GlobalConf.CHANNEL_ID == 1003 then
                TOAST("当前支付宝支付不稳定，请兑换到银行卡")
                return 
           end         
         if self.m_exchangeType == ExchangeRmbLayer.exchangeType.yhk and Player:getStrBankID() =="" then
                TOAST("请先绑定银行卡")
                return
           end   
            if editTxt:getText() == "" then
            TOAST("请输入兑换金额")
            return
        end       
        _:disableQuickClick(sender)  
        print(tonumber(editTxt:getText()))
 	    g_ExchangeRmbController:reqExchange2Money({ self.m_exchangeType,tonumber(editTxt:getText())*100})
	   
    elseif name == "button_bindAlipayBtn" then

       if self.m_exchangeType == ExchangeRmbLayer.exchangeType.zfb then                    --支付宝
            self.bindZFB = BindZFBLayer.new()
            self:addChild(self.bindZFB)
        else
            self.bindYHK = BindYHKLayer.new()
            self:addChild(self.bindYHK)
        --    BindYHKLayer:open(ACTION_TYPE_LAYER.FADE, self,cc.p(0,0), 999, 1,true);
        end
    end
end
 

function ExchangeRmbLayer:onTouchSliderCallBack( sender ,eventType )
    print("ExchangeRmbLayer:onTouchSliderCallBac")
    print("eventType=", eventType)
    if eventType == 0 then
       local goldNum =  math.floor(sender:getPercent() * Player:getGoldCoin()*0.01)
     --  local goldNum1,goldNum2 = math.modf(goldNum)
       local per = math.ceil((goldNum/math.floor(Player:getGoldCoin())) * 100)
       self.goldSlider_1:setPercent( per ) 
       if goldNum  > 0 then
           editTxt:setText( goldNum  )
        else
           editTxt:setText( "" )
        end
         self.text_perentLabel:setString(per.."%")
--       local  leftGold = Player:getGoldCoin() - goldNum
--       self.goldNumLabel:setString( ToolKit:getNumberFormat( leftGold ) )
    end
end

function ExchangeRmbLayer:onccTextFieldCallback( ref ,  eventType)
     print("eventType-----",eventType)
     if eventType == ccui.TextFiledEventType.attach_with_ime then
        print("获得焦点")
     elseif eventType ==  ccui.TextFiledEventType.detach_with_ime then
        print("失去焦点")
        if self.tfGoldNum:getString() ~= ""  then
            local inputGoldNum =  tonumber( self.tfGoldNum:getString() ) 
            if inputGoldNum<0 then
                TOAST("不能输入负数")
                return
            end
             if inputGoldNum<1 then
                TOAST("兑换金额不能小于1")
                return
            end
            if  inputGoldNum > Player:getGoldCoin() then
               inputGoldNum = Player:getGoldCoin()
            end
            print("inputGoldNum",inputGoldNum)
            local goldNum1,goldNum2 = math.modf(inputGoldNum/10000)
            inputGoldNum = goldNum1 * 10000
            if inputGoldNum > 0 then
               self.tfGoldNum:setString( inputGoldNum )
            else
                self.tfGoldNum:setString( "" )
            end
            local per = (inputGoldNum/Player:getGoldCoin()) * 100
            self.goldSlider_1:setPercent( per ) 
--            local  leftGold = Player:getGoldCoin() - inputGoldNum
--            self.goldNumLabel:setString( ToolKit:getNumberFormat( leftGold ) )
        end
        
     elseif eventType == ccui.TextFiledEventType.insert_text then

     elseif eventType == ccui.TextFiledEventType.delete_backward then

     end
end

function ExchangeRmbLayer:onExchange2Money( msgType , __info )
   if __info.m_ret == 0 then
      TOAST("提交申请成功")
      self.tfGoldNum:setString( "" )
      self.goldSlider_1:setPercent( 0 ) 
      self.text_perentLabel:setString( "0%" )
      
   else
      ToolKit:showErrorTip( __info.m_ret )
   end
end

function ExchangeRmbLayer:onAlipayBindEx( msgType , __info )
   if __info.m_nRetCode == 0 then
     TOAST("绑定成功!")
     if self.m_exchangeType == ExchangeRmbLayer.exchangeType.zfb then                    --支付宝
        if Player:getStrAlipayID() ~="" then
               local t = Player:getStrAlipayID()
             local tbl = require("cjson").decode( t ) 
            self.text_alipayAccount:setString(tbl.payId); 
            self.btnBind:loadTextures("hall/image/duihuan/refer.png","hall/image/duihuan/refer.png","hall/image/duihuan/refer.png")
        end 
     end
   else
      ToolKit:showErrorTip( __info.m_nRetCode )
   end
end

function ExchangeRmbLayer:onBankBindEx( msgType , __info) 
    if __info.m_nRetCode == 0 then
         TOAST("绑定成功!")
       if self.m_exchangeType == ExchangeRmbLayer.exchangeType.yhk then                    --银行卡
          if Player:getStrBankID() ~="" then 
               local t = Player:getStrBankID()
             local tbl = require("cjson").decode( t )
             self.text_alipayAccount:setString(tbl["payId"]); 
             self.btnBind:loadTextures("hall/image/duihuan/refer.png","hall/image/duihuan/refer.png","hall/image/duihuan/refer.png")
          end
       end 
   else
      ToolKit:showErrorTip( __info.m_nRetCode )
   end
end


function ExchangeRmbLayer:onDestory()
    print("*******ExchangeRmbLayer:onDestory()*******")
    removeMsgCallBack(self,  MSG_EXCHANGE2MONEY_ACK )
    removeMsgCallBack(self,  MSG_ALIPAYBIND_ACK )
    removeMsgCallBack(self,  MSG_BANKBIND_ACK )
     removeMsgCallBack(self,  MSG_PLAYER_UPDATE_SUCCESS )
    
end


function _:setBtnEnabled(btn, enabled)
    btn:setEnabled(enabled)
    btn:setTouchEnabled(enabled) 
    
end



-- 防止频繁点击
function _:disableQuickClick(btn)
    _:setBtnEnabled(btn, false) 
    performWithDelay(btn, function()
        _:setBtnEnabled(btn, true)
    end, 2)
end
return ExchangeRmbLayer

