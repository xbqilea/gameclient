--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion 

local CHANGE_PATH = "hall/ModifyName.csb";

local HNlayer = import("..HNLayer")
local ChangeNameLayer = class("ChangeNameLayer",function()
    return HNlayer.new()
end) 
function ChangeNameLayer:ctor()
    self:myInit()
    self:setupViews()
end
function ChangeNameLayer:myInit()
   
end
 function ChangeNameLayer:onTouchCallback(sender)
    local name = sender:getName()
    if name == "button_close" then
        self:close() 
    elseif name == "button_btnOk" then
        if self:juageTFOK() then
            local str = self.textField_name:getText()
             ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_ModifyNickName_Req", { str, 0 } )
        end
    elseif name == "button_clearInputBtn" then
        self.textField_name:setText("")
    end
 end 
 function ChangeNameLayer:juageTFOK()   

    local _Str = self.textField_name:getText()
    
    local lenth = string.len(U2A( _Str ))
   
    if lenth < GlobalDefine.USER_NICK_LEN[1] or lenth > GlobalDefine.USER_NICK_LEN[2] then
        --提示长度不符合标准  
        TOAST("新昵称为6~12位字符或者3~6个汉字" )     
        return false
    end
    
--    if   ToolKit:checkIsSensitive(_Str) then
--        --提示有敏感字符 
--        TOAST("昵称中包含敏感字符，请重新输入！")
--        return false
--    end  
     
    return true
end
function ChangeNameLayer:setupViews()

	 
	local node = UIAdapter:createNode(CHANGE_PATH);
	
	self:addChild(node); 
    local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY))

    self.m_pNodeRoot    = node:getChildByName("Layer")
    local diffX = 145 - (1624-display.size.width)/2
    self.m_pNodeRoot:setPositionX(diffX)
	  UIAdapter:adapter(node, handler(self, self.onTouchCallback))  
      UIAdapter:praseNode(node,self)
    self.textField_name:removeFromParent()
    local editTxt = nil
   if UpdateFunctions.platform == "ios" then
        editTxt = ccui.EditBox:create(cc.size(425,45), "hall/image/bank/zcm_zc1_2.png")
    else
        editTxt = ccui.EditBox:create(cc.size(425,70), "hall/image/bank/zcm_zc1_2.png")
    end
    editTxt:setName("inputTxt")  
    editTxt:setAnchorPoint(0.5,0.5)  
    editTxt:setPosition(cc.p(self.image_inputbg:getPositionX() / 2 + 50 ,self.image_inputbg:getPositionY() / 2 - 25))
    editTxt:setFontSize(26)
    editTxt:setMaxLength(12)
    editTxt:setFontColor(cc.c4b(255,255,255,255))
    editTxt:setFontName("ttf/jcy.ttf")
    if UpdateFunctions.platform == "ios" then
        editTxt:setPlaceholderFontSize(16)
    else
        editTxt:setPlaceholderFontSize(24)
    end
	editTxt:setPlaceholderFontColor(cc.c4b(184,187,182,255))  
	editTxt:setPlaceHolder("最多6个汉字或12位英文数字")
    editTxt:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE)
    editTxt:setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE)
    editTxt:registerScriptEditBoxHandler(function(eventname,sender) self:editboxHandle(eventname,sender) end)
    self.image_inputbg:addChild(editTxt,5)  
    self.textField_name = editTxt
end
 
function ChangeNameLayer:editboxHandle(strEventName,sender)  
    if strEventName == "began" then  
    	sender:setPlaceHolder("")                                    --光标进入，清空内容/选择全部  
    elseif strEventName == "ended" then  
        if sender:getText() then
        	sender:setPlaceHolder("最多6个汉字或12位英文数字") 
        end                                                        --当编辑框失去焦点并且键盘消失的时候被调用  
    elseif strEventName == "return" then  
         print(sender:getText(), "sender")                         --当用户点击编辑框的键盘以外的区域，或者键盘的Return按钮被点击时所调用  
    elseif strEventName == "changed" then
    	 print(sender:getText(), "sender")                         --输入内容改变时调用   
    end  
end 
  
return ChangeNameLayer