

local ACTIVITY_PATH = "hall/newActivity.csb"
local ACTIVITY_BUTTON_1 = "hall/image/common/2.png"
local ACTIVITY_BUTTON_2 = "hall/image/common/3.png"
local ACTIVITY_TEXT_1_1 = "hall/image/common/gonggao_021.png"
local ACTIVITY_TEXT_1_2 = "hall/image/common/hdgg_001.png"
local ACTIVITY_TEXT_2_1 = "hall/image/common/gonggao_023.png"
local ACTIVITY_TEXT_2_2 = "hall/image/common/hdgg_002.png"

local XbDialog = require("app.hall.base.ui.CommonView")
local XYDBLayer =  import(".XYDBLayer")
local ChargeLayer = import(".ZCShopView")
local AgentLayer =  import(".AgentLayer")
local NoticeLayer = class("NoticeLayer", function() return XbDialog.new() end)

function NoticeLayer:ctor(params, parent)
    self._parent = parent
    self._Datas1 = params
    self._Datas2 = {
        [1] = {
            m_bulletinType = 1,
            m_content      = '',
            m_finish       = 1559318400,
            m_hyperLink    = 'hall/image/gonggao/6.jpg',
            m_id           = 'd0ae996ca69b4781a858253f410ba201',
            m_start        = 1554134400,
            m_title        = '重要通知',
            m_type         = 7,
        },
        [2] = {
            m_bulletinType = 1,
            m_content      = '',
            m_finish       = 1559318400,
            m_hyperLink    = 'hall/image/gonggao/7.jpg',
            m_id           = 'd0ae996ca69b4781a858253f410ba201',
            m_start        = 1554134400,
            m_title        = '郑重声明',
            m_type         = 1,
        },
        [3] = {
            m_bulletinType = 1,
            m_content      = '',
            m_finish       = 1559318400,
            m_hyperLink    = 'hall/image/gonggao/11.png',
            m_id           = 'd0ae996ca69b4781a858253f410ba201',
            m_start        = 1554134400,
            m_title        = '新玩家必读',
            m_type         = 1,
        },
        [4] = {
            m_bulletinType = 1,
            m_content      = '',
            m_finish       = 1559318400,
            m_hyperLink    = 'hall/image/gonggao/12.png',
            m_id           = 'd0ae996ca69b4781a858253f410ba201',
            m_start        = 1554134400,
            m_title        = '反作弊公告',
            m_type         = 1,
        },
    }

    self._panelBtn = {}
    self._text1_1 = {}
    self._text1_2 = {}
    self._text2_1 = {}
    self._text2_2 = {}
    self._NoticeImage1 = {}
    self._NoticeImage2 = {}
    self._xTouch = 0
    self._xTouchend = 0
    self._yTouch = 0
    self._yTouchend = 0
    self:myInit()
    self:setupViews()
end

function NoticeLayer:myInit()
   
end

function NoticeLayer:onTouchCallback(sender)
    local name = sender:getName()
    local num = sender:getTag()
    if name == "button_closeBtn" then
        g_AudioPlayer:playEffect(GAME_SOUND_CLOSE)
        self._parent:setLobbyBulletin()
        self:closeDialog()
    elseif name == "panel1" then
        self._texttitleWord:setString(self._Datas1[num].m_title)
        self._scrollWord:removeAllChildren()
	    local size = self._scrollWord:getContentSize()
        if self._Datas1[num].m_hyperLink~="" then
            if self._Datas1[num].m_type>0 then
                self:useLocalImage(self._Datas1[num].m_hyperLink, self._Datas1[num].m_type)
            else
                self:downloadNoticeImage1(self._Datas1[num].m_hyperLink,1)
            end
        else
            local content = ccui.Text:create(self._Datas1[num].m_content or "", "ttf/yt.TTF", 24)
	        content:setAnchorPoint(cc.p(0, 1))
	       -- content:setTextColor(cc.c4b(222, 194, 148, 255))
	        local contentSize = content:getContentSize()
            content:setPositionY(math.max(size.height, contentSize.height))
            content:ignoreContentAdaptWithSize(false)
            content:setContentSize(650, contentSize.height)
	        self._scrollWord:setInnerContainerSize(contentSize)
	        self._scrollWord:addChild(content)
	        self._scrollWord:jumpToTop()
            self._scrollWord:setTouchEnabled(true)
            self._scrollWord:addTouchEventListener(handler(self, self.noUse))
        end
        for k,v in pairs(self._panelBtn) do 
            if k==num then
                self._panelBtn[k]:getChildByName("light"):setVisible(true)
                self._panelBtn[k]:getChildByName("unlight"):setVisible(false)
            --    self._panelBtn[k]:getChildByName("text"):setTextColor(cc.c4b(126,66,13,255))
            else
                self._panelBtn[k]:getChildByName("light"):setVisible(false)
                self._panelBtn[k]:getChildByName("unlight"):setVisible(true)
          --      self._panelBtn[k]:getChildByName("text"):setTextColor(cc.c4b(222,194,148,255))
            end
        end
    elseif name == "panel2" then
        self._texttitleWord:setString(self._Datas2[num].m_title)
        self._scrollWord:removeAllChildren()
	    local size = self._scrollWord:getContentSize()
	    if self._Datas2[num].m_hyperLink~="" then
            if self._Datas2[num].m_type>0 then
                self:useLocalImage(self._Datas2[num].m_hyperLink, self._Datas2[num].m_type)
            else
                self:downloadNoticeImage2(self._Datas2[num].m_hyperLink,1)
            end
        else
            local content = ccui.Text:create(self._Datas2[num].m_content or "", "ttf/yt.TTF", 24)
	        content:setAnchorPoint(cc.p(0, 1))
	    --    content:setTextColor(cc.c4b(222, 194, 148, 255))
	        local contentSize = content:getContentSize()
            content:setPositionY(math.max(size.height, contentSize.height))
            content:ignoreContentAdaptWithSize(false)
            content:setContentSize(650, contentSize.height)
	        self._scrollWord:setInnerContainerSize(contentSize)
	        self._scrollWord:addChild(content)
	        self._scrollWord:jumpToTop()
            self._scrollWord:setTouchEnabled(true)
            self._scrollWord:addTouchEventListener(handler(self, self.noUse))
        end
        for k,v in pairs(self._panelBtn) do 
            if k==num then
                self._panelBtn[k]:getChildByName("light"):setVisible(true)
                self._panelBtn[k]:getChildByName("unlight"):setVisible(false)
           --     self._panelBtn[k]:getChildByName("text"):setTextColor(cc.c4b(126,66,13,255))
            else
                self._panelBtn[k]:getChildByName("light"):setVisible(false)
                self._panelBtn[k]:getChildByName("unlight"):setVisible(true)
           --     self._panelBtn[k]:getChildByName("text"):setTextColor(cc.c4b(222,194,148,255))
            end
        end 
    end
end

function NoticeLayer:setupViews()
    GlobalBulletinController:CfgInfoReq()
    local node = UIAdapter:createNode(ACTIVITY_PATH); 
    self:addChild(node); 

    self._winSize = cc.Director:getInstance():getWinSize()

    local imagebg = node:getChildByName("center"):getChildByName("image_Bg")
    local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    node:getChildByName("center"):setPositionX(diffX)
    imagebg:setSwallowTouches(false)

    local image1 = imagebg:getChildByName("Image_1")
   
    self._wordPanel = imagebg:getChildByName("panel_wordPanel")
     self._wordPanel:setVisible(true)
    self._imagePanel = imagebg:getChildByName("panel_imagePanel")
    self._textwaitLabel = imagebg:getChildByName("text_waitLabel")
    self._tabBtnList = image1:getChildByName("listView_tabBtnList")
    self._tabItem = image1:getChildByName("panel_tabItem")
    self._texttitleWord = self._wordPanel:getChildByName("Image_19"):getChildByName("text_titleWord")
    self._scrollWord = self._wordPanel:getChildByName("scrollView_contentScroll")
    self._scrollImage = self._imagePanel:getChildByName("scrollView_bigScrollView")

    self._tabBtnList:setContentSize(self._tabBtnList:getContentSize().width, self._tabBtnList:getContentSize().height-10)

   -- self._wordPanel:setVisible(true)
    self._imagePanel:setVisible(false)

    local temp_text = ccui.Text:create()
    temp_text:setFontName("ttf/yt.TTF")
    temp_text:setFontSize(30)
    temp_text:setString("123456")
    temp_text:setAnchorPoint(cc.p(0.5,0.5))
    temp_text:setPosition(cc.p(130,40.5))
    temp_text:setName("text")
    self._tabItem:addChild(temp_text)

    UIAdapter:adapter(node, handler(self, self.onTouchCallback))

   
    local Panel_2 = imagebg:getChildByName("Panel_2");
    local panel_tab1 = Panel_2:getChildByName("panel_tab1");
    local light1 = panel_tab1:getChildByName("light"); 
    local panel_tab2 = Panel_2:getChildByName("panel_tab2");
    local light2 = panel_tab2:getChildByName("light"); 
    light2:setVisible(false) 
    panel_tab1:getChildByName("Text_1"):setColor(cc.c3b(255,255,255))
    panel_tab2:getChildByName("Text_2"):setColor(cc.c3b(120,171,255))
	panel_tab1:addTouchEventListener(function(sender,event)
        if event == ccui.TouchEventType.ended then 
            light2:setVisible(false) 
            light1:setVisible(true) 
            panel_tab1:setEnabled(false)
            panel_tab2:setEnabled(true)
            panel_tab1:getChildByName("Text_1"):setColor(cc.c3b(255,255,255))
            panel_tab2:getChildByName("Text_2"):setColor(cc.c3b(120,171,255))
             self:setPage1()
        end
	end);

 	panel_tab2:addTouchEventListener(function(sender,event)
         if event == ccui.TouchEventType.ended then  
            light2:setVisible(true) 
            light1:setVisible(false) 
            panel_tab1:getChildByName("Text_1"):setColor(cc.c3b(120,171,255))
            panel_tab2:getChildByName("Text_2"):setColor(cc.c3b(255,255,255))
            panel_tab1:setEnabled(true)
            panel_tab2:setEnabled(false)
             self:setPage2() 
        end
 	end);  
     if #self._Datas1 == 0 then 
          light2:setVisible(true) 
            light1:setVisible(false) 
            panel_tab1:getChildByName("Text_1"):setColor(cc.c3b(120,171,255))
            panel_tab2:getChildByName("Text_2"):setColor(cc.c3b(255,255,255))
            panel_tab1:setEnabled(true)
            panel_tab2:setEnabled(false)
        self:setPage2()
    else 
         light2:setVisible(false) 
        light1:setVisible(true) 
        panel_tab1:setEnabled(false)
        panel_tab2:setEnabled(true)
        panel_tab1:getChildByName("Text_1"):setColor(cc.c3b(255,255,255))
        panel_tab2:getChildByName("Text_2"):setColor(cc.c3b(120,171,255))
        self:setPage1()
    end
end

function NoticeLayer:setPage1()
    self._tabBtnList:removeAllChildren()
    self._panelBtn = {}
    self._scrollWord:removeAllChildren()
    for k,v in pairs(self._Datas1) do
        self._panelBtn[k] = self._tabItem:clone()
        self._panelBtn[k]:setName("panel1")
        self._panelBtn[k]:setTag(k)
        if k==1 then
            self._panelBtn[k]:getChildByName("light"):setVisible(true)
            self._panelBtn[k]:getChildByName("unlight"):setVisible(false)
            self._panelBtn[k]:getChildByName("redimage"):setVisible(false)
           -- self._panelBtn[k]:getChildByName("text"):setTextColor(cc.c4b(126,66,13,255))
        else
            self._panelBtn[k]:getChildByName("light"):setVisible(false)
            self._panelBtn[k]:getChildByName("unlight"):setVisible(true)
            self._panelBtn[k]:getChildByName("redimage"):setVisible(false)
          --  self._panelBtn[k]:getChildByName("text"):setTextColor(cc.c4b(222,194,148,255))
        end
        if v.m_type==20 then
            self._panelBtn[k]:getChildByName("text"):setFontSize(25)
        end
        local imagetag = ccui.ImageView:create()
        if v.m_content=="1" then
            imagetag:loadTexture("hall/image/gonggao/1.png",0)
            imagetag:setAnchorPoint(cc.p(0.5,0.5))
            imagetag:setPosition(cc.p(215,65))
            imagetag:setScale(0.82)
            self._panelBtn[k]:addChild(imagetag)
        elseif v.m_content=="2" then
            imagetag:loadTexture("hall/image/gonggao/2.png",0)
            imagetag:setAnchorPoint(cc.p(0.5,0.5))
            imagetag:setPosition(cc.p(215,65))
            imagetag:setScale(0.82)
            self._panelBtn[k]:addChild(imagetag)
        elseif v.m_content=="3" then
            imagetag:loadTexture("hall/image/gonggao/3.png",0)
            imagetag:setAnchorPoint(cc.p(0.5,0.5))
            imagetag:setPosition(cc.p(215,65))
            imagetag:setScale(0.82)
            self._panelBtn[k]:addChild(imagetag)
        end
        self._panelBtn[k]:getChildByName("text"):setString(self._Datas1[k].m_title)
        self._tabBtnList:pushBackCustomItem(self._panelBtn[k])
    end

    if #self._Datas1>0 then
        self._texttitleWord:setString(self._Datas1[1].m_title)
        self._scrollWord:removeAllChildren()
	    local size = self._scrollWord:getContentSize()
        if self._Datas1[1].m_hyperLink~="" then
            if self._Datas1[1].m_type>0 then
                self:useLocalImage(self._Datas1[1].m_hyperLink, self._Datas1[1].m_type)
            else
                self:downloadNoticeImage1(self._Datas1[1].m_hyperLink,1)
            end
        else
            local content = ccui.Text:create(self._Datas1[1].m_content or "", "ttf/yt.TTF", 24)
	        content:setAnchorPoint(cc.p(0, 1))
	      --  content:setTextColor(cc.c4b(222, 194, 148, 255))
	        local contentSize = content:getContentSize()
            content:setPositionY(math.max(size.height, contentSize.height))
            content:ignoreContentAdaptWithSize(false)
            content:setContentSize(907, contentSize.height)
	        self._scrollWord:setInnerContainerSize(contentSize)
	        self._scrollWord:addChild(content)
	        self._scrollWord:jumpToTop()
            self._scrollWord:setTouchEnabled(true)
            self._scrollWord:addTouchEventListener(handler(self, self.noUse))
        end
    else
        self._texttitleWord:setString("暂无活动")
    end
end

function NoticeLayer:setPage2()
    self._tabBtnList:removeAllChildren()
    self._panelBtn = {}
    self._scrollWord:removeAllChildren()
    for k,v in pairs(self._Datas2) do
        self._panelBtn[k] = self._tabItem:clone()
        self._panelBtn[k]:setName("panel2")
        self._panelBtn[k]:setTag(k)
        if k==1 then
            self._panelBtn[k]:getChildByName("light"):setVisible(true)
            self._panelBtn[k]:getChildByName("unlight"):setVisible(false)
            self._panelBtn[k]:getChildByName("redimage"):setVisible(false)
         --   self._panelBtn[k]:getChildByName("text"):setTextColor(cc.c4b(126,66,13,255))
        else
            self._panelBtn[k]:getChildByName("light"):setVisible(false)
            self._panelBtn[k]:getChildByName("unlight"):setVisible(true)
            self._panelBtn[k]:getChildByName("redimage"):setVisible(false)
          --  self._panelBtn[k]:getChildByName("text"):setTextColor(cc.c4b(222,194,148,255))
        end
        if v.m_type==20 then
            self._panelBtn[k]:getChildByName("text"):setFontSize(25)
        end
        local imagetag = ccui.ImageView:create()
        if v.m_content=="1" then
            imagetag:loadTexture("hall/image/gonggao/1.png",0)
            imagetag:setAnchorPoint(cc.p(0.5,0.5))
            imagetag:setPosition(cc.p(215,65))
            imagetag:setScale(0.82)
            self._panelBtn[k]:addChild(imagetag)
        elseif v.m_content=="2" then
            imagetag:loadTexture("hall/image/gonggao/2.png",0)
            imagetag:setAnchorPoint(cc.p(0.5,0.5))
            imagetag:setPosition(cc.p(215,65))
            imagetag:setScale(0.82)
            self._panelBtn[k]:addChild(imagetag)
        elseif v.m_content=="3" then
            imagetag:loadTexture("hall/image/gonggao/3.png",0)
            imagetag:setAnchorPoint(cc.p(0.5,0.5))
            imagetag:setPosition(cc.p(215,65))
            imagetag:setScale(0.82)
            self._panelBtn[k]:addChild(imagetag)
        end
        self._panelBtn[k]:getChildByName("text"):setString(self._Datas2[k].m_title)
        self._tabBtnList:pushBackCustomItem(self._panelBtn[k])
    end

    if #self._Datas2>0 then
        self._texttitleWord:setString(self._Datas2[1].m_title)
        self._scrollWord:removeAllChildren()
	    local size = self._scrollWord:getContentSize()
        if self._Datas2[1].m_hyperLink~="" then
            if self._Datas2[1].m_type>0 then
                self:useLocalImage(self._Datas2[1].m_hyperLink, self._Datas2[1].m_type)
            else
                self:downloadNoticeImage2(self._Datas2[1].m_hyperLink,1)
            end
        else
            local content = ccui.Text:create(self._Datas2[1].m_content or "", "ttf/yt.TTF", 24)
	        content:setAnchorPoint(cc.p(0, 1))
	    --    content:setTextColor(cc.c4b(222, 194, 148, 255))
	        local contentSize = content:getContentSize()
            content:setPositionY(math.max(size.height, contentSize.height))
            content:ignoreContentAdaptWithSize(false)
            content:setContentSize(907, contentSize.height)
	        self._scrollWord:setInnerContainerSize(contentSize)
	        self._scrollWord:addChild(content)
	        self._scrollWord:jumpToTop()
            self._scrollWord:setTouchEnabled(true)
            self._scrollWord:addTouchEventListener(handler(self, self.noUse))
        end
    else
        self._texttitleWord:setString("暂无公告")
    end
end

function NoticeLayer:resetParam(param)
    self._Datas1 = param
    dump(self._Datas1, "bullData")
end

function NoticeLayer:downloadNoticeImage1(_url,num)
    local picname = string.format("gonggao%d_%d",1,num)
    local originPath = cc.UserDefault:getInstance():getStringForKey(picname, "")
    if originPath~="" then
        UpdateFunctions.removePath(originPath)
    end

    local url = _url
    print("**************公告图片下载，url: ", url)

    local dic

    if device.platform == "android" then
        dic = device.writablePath.."../cache/netSprite/"
    else
        dic = device.writablePath.."netSprite/"
    end

    if not io.exists(dic) then
        lfs.mkdir(dic)
    end

    local picPath = dic .. crypto.md5(url) .. ".png"

    print("**********公告图片保存路径**********")
    print(picPath)

    cc.UserDefault:getInstance():setStringForKey(picname, picPath)
    cc.UserDefault:getInstance():flush()

    local NetSprite = require("app.hall.mail.view.NetSprite")

    self._NoticeImage1[num] = NetSprite.new(url, true, handler(self, self.downloadImageCallBack), false)   
    self._NoticeImage1[num]:retain() 
end

function NoticeLayer:downloadNoticeImage2(_url,num)
    local url = _url
    local picname = string.format("gonggao%d_%d",2,num)
    local originPath = cc.UserDefault:getInstance():getStringForKey(picname, "")
    if originPath~="" then
        print(crypto.md5(url))
        print(UpdateFunctions.fileMd5(originPath))
        if crypto.md5(url)==UpdateFunctions.fileMd5(originPath) then
            return
        else
            UpdateFunctions.removePath(originPath)
        end
    end

    print("**************公告图片下载，url: ", url)

    local dic

    if device.platform == "android" then
        dic = device.writablePath.."../cache/netSprite/"
    else
        dic = device.writablePath.."netSprite/"
    end

    if not io.exists(dic) then
        lfs.mkdir(dic)
    end

    local picPath = dic .. crypto.md5(url) .. ".png"

    print("**********公告图片保存路径**********")
    print(picPath)

    cc.UserDefault:getInstance():setStringForKey(picname, picPath)
    cc.UserDefault:getInstance():flush()

    local NetSprite = require("app.hall.mail.view.NetSprite")

    self._NoticeImage2[num] = NetSprite.new(url, true, handler(self, self.downloadImageCallBack), false)   
    self._NoticeImage2[num]:retain() 
end


function NoticeLayer:getNoticeImage()
    return self.NoticeImage
end

function NoticeLayer:downloadImageCallBack(isSuccess, path)
    print("NoticeImage:downloadImageCallBack:",path)
    if self._scrollWord == nil then
        return
    end
    local size = self._scrollWord:getContentSize() 
    local content = ccui.ImageView:create(path)
	content:setAnchorPoint(cc.p(0, 1))
	local contentSize = content:getContentSize()
    content:setPositionY(math.max(size.height, contentSize.height))
    content:ignoreContentAdaptWithSize(false)
    content:setContentSize(660, contentSize.height)
	self._scrollWord:setInnerContainerSize(contentSize)
	self._scrollWord:addChild(content)
	self._scrollWord:jumpToTop()
    self._scrollWord:setTouchEnabled(true)
    self._scrollWord:addTouchEventListener(handler(self, self.noUse))
end

function NoticeLayer:useLocalImage(path,ntype)
    print("NoticeImage:LocalImageCallBack:",path)
    local size = self._scrollWord:getContentSize()
    local content = ccui.ImageView:create(path)
	content:setAnchorPoint(cc.p(0, 1))
	local contentSize = content:getContentSize()
    content:setPositionY(math.max(size.height, contentSize.height))
    content:ignoreContentAdaptWithSize(false)
    content:setContentSize(1007, 580)
    if ntype==21 then
        content:ignoreContentAdaptWithSize(false)
        content:setContentSize(1007,2626)
    end
    
	self._scrollWord:setInnerContainerSize(contentSize)
	self._scrollWord:addChild(content)
	self._scrollWord:jumpToTop()

    if ntype==2 then
        self._scrollWord:setTouchEnabled(true)
        self._scrollWord:addTouchEventListener(handler(self, self.kefuReq))
    elseif ntype==3 then
        self._scrollWord:setTouchEnabled(true)
        self._scrollWord:addTouchEventListener(handler(self, self.shopReq))
    elseif ntype==4 then
        self._scrollWord:setTouchEnabled(true)
        self._scrollWord:addTouchEventListener(handler(self, self.XYDBReq))
    elseif ntype==5 then
        self._scrollWord:setTouchEnabled(true)
        self._scrollWord:addTouchEventListener(handler(self, self.SignReq))
    elseif ntype==6 then
        local info = GlobalBulletinController:getCfgInfo()
        local str1 = string.format("%d元彩金",info.m_selfReward*0.01)
        local str2 = string.format("%d元彩金",info.m_upperReward*0.01)
        local str3 = string.format("%d",info.m_firstTotalDeposit*0.01)
        local label1 = ccui.Text:create(str1, "ttf/yt.TTF", 21)
        local label2 = ccui.Text:create(str2, "ttf/yt.TTF", 21)
        local label3 = ccui.Text:create(str3, "ttf/yt.TTF", 24)
        label3:setTextColor(cc.c4b(253, 238, 137, 255))
       -- content:addChild(label1)
       local icon = ccui.ImageView:create("hall/image/gonggao/icon.png")
       icon:setPosition(280,320)
       icon:setScale(0.9)
       label1:setPosition(505,271)
       label2:setPosition(660,271)
        label3:setPosition(580,222) 
         content:addChild(label1)
        content:addChild(label2)
         content:addChild(label3)
         content:addChild(icon)
        self._scrollWord:setTouchEnabled(true)
        self._scrollWord:addTouchEventListener(handler(self, self.agentReq))
    elseif ntype==7 then
        self._scrollWord:setTouchEnabled(true)
        self._scrollWord:addTouchEventListener(handler(self, self.clipReq))
    elseif ntype==19 then
        self._scrollWord:setTouchEnabled(true)
        self._scrollWord:addTouchEventListener(handler(self, self.QQReq))
    else
        self._scrollWord:setTouchEnabled(true)
        self._scrollWord:addTouchEventListener(handler(self, self.noUse))
    end
end

function NoticeLayer:kefuReq(pSender, event)
    if event == ccui.TouchEventType.began then
        self._xTouch = pSender:getTouchBeganPosition().x
        self._yTouch = pSender:getTouchBeganPosition().y
    elseif event == ccui.TouchEventType.ended then
        self._xTouchend = pSender:getTouchEndPosition().x
        self._yTouchend = pSender:getTouchEndPosition().y
        if (math.abs(self._xTouch - self._xTouchend) > 25) or (math.abs(self._yTouch - self._yTouchend) > 25) then
            return
        end
        local url = string.format(GlobalConf.DOMAIN.."api/itf/cs/info?channelId="..GlobalConf.CHANNEL_ID)

        print(url)
        local xhr = cc.XMLHttpRequest:new()
        xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
        xhr:open("GET",url) 
    
        local function onReadyStateChange() 
            local response   = xhr.response
            if response == "" then
            --  self:kefuReq()
                print("response = nil")
            else

                local output = json.decode(response,1) 
                dump(output)
                if output.data then
                    for k,v in pairs(output.data) do
                        if v.type == 3 then
                            device.openURL(v.accountOrUrl)
                        end
                    end
                end
            end
        end
        xhr:registerScriptHandler(onReadyStateChange)
        xhr:send()
    end
end

function NoticeLayer:XYDBReq(pSender, event) 
    if event == ccui.TouchEventType.began then
        self._xTouch = pSender:getTouchBeganPosition().x
        self._yTouch = pSender:getTouchBeganPosition().y
    elseif event == ccui.TouchEventType.ended then
        self._xTouchend = pSender:getTouchEndPosition().x
        self._yTouchend = pSender:getTouchEndPosition().y
        if (math.abs(self._xTouch - self._xTouchend) > 25) or (math.abs(self._yTouch - self._yTouchend) > 25) then
            return
        end
         if #GlobalXYDBController.m_LuckyRoulette.m_silverSetting ==0 or GlobalXYDBController.m_LuckyRoulette.m_openType == 0 then
            local DlgAlert = require("app.hall.base.ui.MessageBox")
            local data = {tip="敬请期待"}
            dlg = DlgAlert.showErrorAlert(data, __callback)
            return dlg
        end
        local posx = 0
        if (self._winSize.width / self._winSize.height > 1.78) then 
            posx = (self._winSize.width/display.scaleX-self._winSize.width)/ 2
        end
        local XYDBLayer = XYDBLayer.new()
        XYDBLayer:open(ACTION_TYPE_LAYER.FADE, self._parent,cc.p(posx,0), 200000, CHILD_LAYER_BANK_TAG,true);
		XYDBLayer:setName("XYDBLayer"); 
    end
end

function NoticeLayer:SignReq(pSender, event) 
    if event == ccui.TouchEventType.began then
        self._xTouch = pSender:getTouchBeganPosition().x
        self._yTouch = pSender:getTouchBeganPosition().y
    elseif event == ccui.TouchEventType.ended then
        self._xTouchend = pSender:getTouchEndPosition().x
        self._yTouchend = pSender:getTouchEndPosition().y
        if (math.abs(self._xTouch - self._xTouchend) > 25) or (math.abs(self._yTouch - self._yTouchend) > 25) then
            return
        end
        --g_signhongbaoController:ShowSignInChannelReq()
    end
end

function NoticeLayer:clipReq(pSender, event)
    if event == ccui.TouchEventType.began then
        self._xTouch = pSender:getTouchBeganPosition().x
        self._yTouch = pSender:getTouchBeganPosition().y
    elseif event == ccui.TouchEventType.ended then
        self._xTouchend = pSender:getTouchEndPosition().x
        self._yTouchend = pSender:getTouchEndPosition().y
        if (math.abs(self._xTouch - self._xTouchend) > 25) or (math.abs(self._yTouch - self._yTouchend) > 25) then
            return
        end
        g_AudioPlayer:playEffect("hall/sound/copyWebSit.mp3")
        local text = cc.ui.UILabel.new({
            UILabelType = 2,
            text = "官网地址已复制成功，欢迎推荐给您的好友",
            font = "ttf/yt.TTF",
            size = 26,
            color = cc.c3b(255, 255, 255),
          })
        text:setAnchorPoint(cc.p(0.5,0.5))
        local posx = self._winSize.width/2
        local posy = self._winSize.height/2
        if (self._winSize.width / self._winSize.height > 1.78) then 
            posx = (self._winSize.width/display.scaleX)/ 2
        end
        text:setPosition(cc.p(posx,posy))
        self:addChild(text, 100000)
		text:setVisible(true);
		text:setOpacity(255);
		local fadeOut = cc.FadeOut:create(2.0);
		text:runAction(fadeOut);
        ToolKit:setClipboardText("https://www.xbqpthe.com/");
    end
end

function NoticeLayer:shopReq(pSender, event) 
    if event == ccui.TouchEventType.began then
        self._xTouch = pSender:getTouchBeganPosition().x
        self._yTouch = pSender:getTouchBeganPosition().y
    elseif event == ccui.TouchEventType.ended then
        self._xTouchend = pSender:getTouchEndPosition().x
        self._yTouchend = pSender:getTouchEndPosition().y
        if (math.abs(self._xTouch - self._xTouchend) > 25) or (math.abs(self._yTouch - self._yTouchend) > 25) then
            return
        end
        local chargeLayer = ChargeLayer.new()
        self:addChild(chargeLayer,999) 
    end
end

function NoticeLayer:agentReq(pSender, event)
    if event == ccui.TouchEventType.began then
        self._xTouch = pSender:getTouchBeganPosition().x
        self._yTouch = pSender:getTouchBeganPosition().y
    elseif event == ccui.TouchEventType.ended then
        self._xTouchend = pSender:getTouchEndPosition().x
        self._yTouchend = pSender:getTouchEndPosition().y
        if (math.abs(self._xTouch - self._xTouchend) > 25) or (math.abs(self._yTouch - self._yTouchend) > 25) then
            return
        end
        local posx = 0
        if (self._winSize.width / self._winSize.height > 1.78) then 
            posx = (self._winSize.width/display.scaleX-self._winSize.width)/ 2
        end
        -- local agentLayer = AgentLayer.new()
        -- agentLayer:open(ACTION_TYPE_LAYER.FADE, self._parent,cc.p(posx,0), 200000, CHILD_LAYER_SPREAD_TAG,true);
        self._parent:addChild(require("src.app.hall.PromoteUI.PromoteUI").new() , 200000)
    end
end

function NoticeLayer:QQReq(pSender, event)
    if event == ccui.TouchEventType.ended then
        local DlgAlert                = require("app.hall.base.ui.MessageBox")
        local dlg = DlgAlert.showTipsAlert({title = "提示", tip = "在QQ中打开? (请确认QQ已登录)" })
        dlg:setBtnAndCallBack("确定", "取消", 
            function ()
                dlg:closeDialog()
                -- 打开客服
                if device.platform ~= "ios" then
                    device.openURL(string.format("mqqwpa://im/chat?chat_type=wpa&uin=%s","800876354"))
                else
                    local kl = "mqq://im/chat?chat_type=wpa&uin=%s&version=1&src_type=web"
                    local content = string.format(kl,"800876354")
                    device.openURL(content)
                end
            end,
            function () 
                dlg:closeDialog()
        end)
        dlg:enableTouch(false)
        dlg:setBackBtnEnable(false)
    end
end

function NoticeLayer:noUse(pSender, event)
    if event==ccui.TouchEventType.ended then
        
    end
end

return NoticeLayer