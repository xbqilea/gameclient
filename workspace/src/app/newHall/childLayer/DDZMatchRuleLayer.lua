 
local HNLayer= require("src.app.newHall.HNLayer") 
local DDZMatchRuleLayer = class("DDZMatchRuleLayer",function ()
     return HNLayer.new()
end)

function DDZMatchRuleLayer:ctor()
    self:myInit()
    self:setupView()
end
function DDZMatchRuleLayer:onTouchCallback(sender)
    local name = sender:getName()
    if name == "button_close" then
        self:close()
    end
end
function DDZMatchRuleLayer:setupView() 
    local node = UIAdapter:createNode("hall/ddzMatchHelp.csb")
      local center = node:getChildByName("Layer"); 
     local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
    local image_bg = node:getChildByName("image_bg") 
    local scrollView_scroll = image_bg:getChildByName("scrollView_scroll") 
   -- panel:setPosition(0,0)
     UIAdapter:adapter(node,handler(self, self.onTouchCallback)) 
	 self:addChild(node)
end 
  
function DDZMatchRuleLayer:myInit()  
end

return DDZMatchRuleLayer