--region UserCenterLayer.lua
--Date 2017.04.24.
--Auther JackyXu.
--Desc: 大厅个人中心
 
--local NoticeDialog      = require("common.layer.NoticeDialog")
local ChangeNameLayer= import(".changeNameLayer")
local PhoneRegisterLayer = import(".PhoneRegisterLayer")
local BindYHKLayer = import(".BindYHKLayer")
local BindZFBLayer = import(".BindZFBLayer") 
local ZhuceLayer = import(".ZhuceLayer")
local SettingView       = import(".SetLayer")
local HNLayer = import("..HNLayer")          
local UserCenterLayer   = class("UserCenterLayer", function()
    return HNLayer.new()
 end)

function UserCenterLayer:ctor() 
    self.m_nLastTouchTime       = 0
    self.m_nEditBoxTouchCount   = 0 -- 输入框点击次数
    self.m_bIsModifying         = false
    self.m_pEditBox             = nil
    self._HeadID = 0
    self._HeadCount = 10
    self._HeadType = 0
    if GlobalConf.CHANNEL_ID == 1003 then --金银岛
        if Player:getAccountID() == 1025257 then
            self._HeadCount = 11
        end
    end
    addMsgCallBack(self, MSG_CHANGE_HEADICON_ASK, handler(self, self.onChangeHeadIconAsk)) 
    addMsgCallBack(self, MSG_CHANGE_NICKNAME_SUCCES, handler(self, self.onChangeNameAsk))
    addMsgCallBack(self, MSG_ALIPAYBIND_ACK, handler(self, self.onAlipayBindEx ))
    addMsgCallBack(self, MSG_BANKBIND_ACK, handler(self, self.onBankBindEx )) 
    addMsgCallBack(self, MSG_BECOME_REGULAR_ACCOUNT, handler(self, self.onPhoneBind))
    ToolKit:registDistructor( self, handler(self, self.onDestory) )
    self:init()
end
 function UserCenterLayer:onDestory()
    removeMsgCallBack(self, MSG_CHANGE_HEADICON_ASK) 
    removeMsgCallBack(self, MSG_CHANGE_NICKNAME_SUCCES)
    removeMsgCallBack(self, MSG_ALIPAYBIND_ACK) 
    removeMsgCallBack(self, MSG_BANKBIND_ACK)
    removeMsgCallBack(self, MSG_BECOME_REGULAR_ACCOUNT)
 end 

  function UserCenterLayer:onPhoneBind( msgType , _code )
   if _code == 0 then
        TOAST("绑定成功!")               --手机号
        local t = Player:getPhoneNumber() 
        if Player:getPhoneNumber() ~="" then  
           	self.Label_Phone:setString(Player:getPhoneNumber());  
            self.Button_Phone:setVisible(false)
        end  
   end
end

 function UserCenterLayer:onAlipayBindEx( msgType , __info )
   if __info.m_nRetCode == 0 then
     TOAST("绑定成功!")               --支付宝
        if Player:getStrAlipayID() ~="" then
               local t = Player:getStrAlipayID()
             local tbl = require("cjson").decode( t ) 
            self.Button_ZFB:loadTextures("hall/image/genghuan.png")
            self.Label_zfb:setString(tbl.payId); 
        end  
   end
end

function UserCenterLayer:onBankBindEx( msgType , __info) 
    if __info.m_nRetCode == 0 then
         TOAST("绑定成功!")                 --银行卡
          if Player:getStrBankID() ~="" then 
                local t = Player:getStrBankID()
                local tbl = require("cjson").decode( t )
                self.Label_YHK:setString(tbl["payId"]);  
                self.Button_YHK:loadTextures("hall/image/genghuan.png","hall/image/genghuan.png","hall/image/genghuan.png",0)
          end  
   end
end


function UserCenterLayer:onChangeHeadIconAsk(_,info)
    print(info.m_nNewAvatarID)
    self._HeadID =  info.m_nNewAvatarID
    local str =ToolKit:getHead(self._HeadID)  
    self.m_pImgHead:loadTexture(str,ccui.TextureResType.plistType); 
   -- local t =  Player:getSex() 
--     if Player:getSex() ==1 then
--        self.image_nan:setVisible(true)
--        self.image_nv:setVisible(false)
--    else
--         self.image_nan:setVisible(false)
--        self.image_nv:setVisible(true)
--    end
 end
function UserCenterLayer:onChangeNameAsk(_,ret) 
    if ret==0 then
        local name = Player:getNickName()
        self.Text_Name:setString(Player:getNickName());
        self._changeNameLayer:close()  
        TOAST("修改成功！")
    else
           local data = getErrorTipById(ret)
           if data.tip then
             TOAST(data.tip)
            end
    end
 end
  function UserCenterLayer:onTouchCallback(sender)
    local name = sender:getName()
    print(name)
    if name == "Button_return" then
        self:close()
   
    elseif name == "Button_Act" then
        --GlobalDefine.ISLOGOUT =true   
		--g_isAllowReconnect = false --不允许允许重连
        --ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_LogoutRep" ) -- 退出
		ToolKit:returnToLoginScene()	
    elseif name == "Button_Phone" then
        local phoneRegisterLayer = PhoneRegisterLayer.new(OpenType.ZhuanZheng)
        self:addChild(phoneRegisterLayer,3000)
 		--phoneRegisterLayer:open(ACTION_TYPE_LAYER.FADE, self,cc.p(0,0), 3000, 3000,true);
 		--phoneRegisterLayer:showRegistLayer();
    elseif name == "Button_YHK" then
        if Player:getPhoneNumber()~="" then
            local BindYHKLayer = BindYHKLayer.new()
            self:addChild(BindYHKLayer)
        else
            
            local ZhuceLayer = ZhuceLayer.new()
            ZhuceLayer:open(ACTION_TYPE_LAYER.FADE, self._parent, cc.p(0,0), 999, CHILD_LAYER_BANK_TAG,true);
            ZhuceLayer:setName("ZhuceLayer"); 
            
        end
    elseif name == "Button_ZFB" then
        if Player:getPhoneNumber()~="" then
            local BindZFBLayer = BindZFBLayer.new()
            self:addChild(BindZFBLayer)
        else 
            local ZhuceLayer = ZhuceLayer.new()
           ZhuceLayer:open(ACTION_TYPE_LAYER.FADE, self._parent, cc.p(0,0), 999, CHILD_LAYER_BANK_TAG,true);
            ZhuceLayer:setName("ZhuceLayer"); 
        end
   elseif name == "Button_Edit" then
          self._changeNameLayer = ChangeNameLayer.new()
		    self:addChild(self._changeNameLayer, 2);
    elseif name == "Button_copy" then
        TOAST("复制成功")
		ToolKit:setClipboardText(tostring(Player:getAccountID())) 
    elseif name == "Button_head_modify" then
         self.changeFrameNode:setVisible(true)  
    elseif name == "Button_Sys" then
        local SettingView = SettingView.new()
         self:addChild(SettingView)
    elseif name == "Button_wx" then
        if ToolKit:getLoginType()~=GlobalDefine.LoginType.wx then
            g_LoginController:startByWx()
        else
            TOAST("已绑定微信,请不要重复绑定")
        end
    end
 end
 function  UserCenterLayer:onTouchCallback1(sender)
    local name = sender:getName()
    if name == "Button_close" then
         g_UserCenterController:changeHeadIcon( self._HeadID)
          self.changeFrameNode:setVisible(false)    
    elseif name == "checkBox_nvBtn" then
        self._HeadType = 0
        local imageName = "head_mwoman_01.png"
        for i=1,self._HeadCount do
            local btn = self.LayerHead:getChildByName("Image_"..(i-1))
            if i == 10 then
                btn:loadTexture("head_mwoman_10.png",ccui.TextureResType.plistType)
            elseif i == 11 then
                btn:loadTexture("head_super_100.png",ccui.TextureResType.plistType)
            else
                btn:loadTexture("head_mwoman_0"..i..".png",ccui.TextureResType.plistType)
            end 
             
            if i == 1 or i == 11 then 
                self.Image_Select:setPosition(btn:getPosition())
                self._HeadID = i
                if i == 1 then 
                    imageName = "head_mwoman_01.png" 
                    self._HeadID = i 
                elseif i == 11 then 
                    imageName = "head_super_100.png"
                    self._HeadID = 21
                end
            end
        end
         self.Image_head:loadTexture(imageName,ccui.TextureResType.plistType)
    elseif name == "checkBox_nanBtn" then
        self._HeadType = 1
        local imageName = "head_mman_01.png"
        for i=1,self._HeadCount do
            local btn = self.LayerHead:getChildByName("Image_"..(i-1))
            if i == 10 then
                btn:loadTexture("head_mman_10.png",ccui.TextureResType.plistType)
            elseif i == 11 then
                btn:loadTexture("head_super_100.png",ccui.TextureResType.plistType)
            else
                btn:loadTexture("head_mman_0"..i..".png",ccui.TextureResType.plistType)
            end 

            if i == 1 or i == 11 then 
                self.Image_Select:setPosition(btn:getPosition())
                
                if i == 1 then 
                    imageName = "head_mman_01.png" 
                    self._HeadID = i+10 
                elseif i == 11 then 
                    imageName = "head_super_100.png"
                    self._HeadID = 21  
                end
            end
        end 
        self.Image_head:loadTexture(imageName,ccui.TextureResType.plistType)
    elseif sender:getTag()<=self._HeadCount and sender:getTag()>=1 then
        local index = sender:getTag()
        local btn = self.LayerHead:getChildByName("Image_"..(index-1))
        self.Image_Select:setPosition(btn:getPosition())
        if index == 11 then
            self._HeadID = 21
            self.Image_head:loadTexture("head_super_100.png",ccui.TextureResType.plistType)
        elseif self._HeadType == 1 then
            self._HeadID = 10 + index
            if index== 10 then
                self.Image_head:loadTexture("head_mman_10.png",ccui.TextureResType.plistType)
            else
                self.Image_head:loadTexture("head_mman_0"..sender:getTag()..".png",ccui.TextureResType.plistType)
            end
        else
            self._HeadID = index
            if index == 10 then
                self.Image_head:loadTexture("head_mwoman_10.png",ccui.TextureResType.plistType)
            else
               self.Image_head:loadTexture("head_mwoman_0"..sender:getTag()..".png",ccui.TextureResType.plistType)
            end
        end

        
    end
 end
function UserCenterLayer:init()

  
    self.m_pathUI = cc.CSLoader:createNode("hall/csb/HallViewUserInfo.csb") 
    self.m_pathUI:setPositionY((display.height - 750) / 2)
    self:addChild(self.m_pathUI)
     UIAdapter:adapter(self.m_pathUI, handler(self, self.onTouchCallback))
    UIAdapter:praseNode(self.m_pathUI,self)
    self.m_pNodeRoot    = self.m_pathUI:getChildByName("HallViewUerInfo")
    self.m_pNodeRoot:setPositionX((display.width - 1334) / 2)

    self.m_pBtnNull        = self.m_pathUI:getChildByName("Panel_2")--空白处关闭

    --背景---------------------------------------------------------------------------
    self.m_pImageBg             = self.m_pNodeRoot:getChildByName("Image_bg")
    self.m_pBtnReturn           = self.m_pImageBg:getChildByName("Button_return")
    self.m_pLbTips              = self.m_pImageBg:getChildByName("LB_tips")
    --左边用户设置-------------------------------------------------------------------
    local nodeLeft              = self.m_pImageBg:getChildByName("Panel_setting")
    local nodeHead              = nodeLeft:getChildByName("Node_head")
    --修改头像
    self.m_pImgHead             = nodeHead:getChildByName("Image_userIcon")
    self.m_pImgFrame            = nodeHead:getChildByName("Image_Frame")
    self.m_pBtnModifyHead       = nodeHead:getChildByName("Button_head_modify")
    --vip信息
    self.m_pImgVip              = nodeLeft:getChildByName("VipInfo")
    self.m_pBarVip              = nodeLeft:getChildByName("Bar_Vip")
    self.m_pLabelValue          = nodeLeft:getChildByName("Label_value")
    self.m_pImageVipProBg       = nodeLeft:getChildByName("Img_Vip")
    --系统设置按钮
    self.m_pBtnSysSetting       = nodeLeft:getChildByName("Button_Sys")
    --切换账号按钮
    self.m_pBtnLogout           = nodeLeft:getChildByName("Button_Act")
    --右边用户信息----------------------------------------------------------------
    local nodeRight            = self.m_pImageBg:getChildByName("Panel_info")
    --用户GameID
    local nodeGameID           = nodeRight:getChildByName("Node_ID")
    self.m_pLbGameID           = nodeGameID:getChildByName("Label_ID")
    self.m_pBtnCopyID          = nodeGameID:getChildByName("Button_copy")
    --用户昵称
    local nodeName             = nodeRight:getChildByName("Node_Name")
    self.m_pImgNameEdit        = nodeName:getChildByName("Sprite_NameEdit")
    self.m_pBtnNameEdit        = nodeName:getChildByName("Button_Edit")
    --用户金币
    local nodeGold             = nodeRight:getChildByName("Node_Gold")
    self.m_pLbGold             = nodeGold:getChildByName("Label_Gold") 
     --用户银行金币
    local nodeBankGold         = nodeRight:getChildByName("Node_Bank")
    self.m_pLbBankGold         = nodeBankGold:getChildByName("Label_Gold")
     

    --月卡信息数据
    self.m_pNodeCard           = nodeRight:getChildByName("Node_Card")
    self.m_pCardTxt            = self.m_pNodeCard:getChildByName("leave_time")
    self.m_pBtnCard            = self.m_pNodeCard:getChildByName("Image_card"):getChildByName("Panel_1")
    -------------------------------------------------------------------------------- 
     
   
   	--加载更换头像cab
	self.changeFrameNode = UIAdapter:createNode("hall/csb/HallViewUserHead.csb"); 
      UIAdapter:adapter(self.changeFrameNode, handler(self, self.onTouchCallback1))
      UIAdapter:praseNode(self.changeFrameNode,self)
       local diffY = (display.size.height - 750) / 2
    self.changeFrameNode:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    self.HallViewUserHead:setPositionX(diffX)
--	changeFrameNode:setPosition(640, 360);
	self:addChild(self.changeFrameNode); 
	self.changeFrameNode:setVisible(false)
    self.Image_Frame:setVisible(true)
    local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", Player:getVipLevel())
    self.Image_Frame:loadTexture(framePath, ccui.TextureResType.plistType)
    self.Image_Frame:ignoreContentAdaptWithSize(true)
    local str =ToolKit:getHead(Player:getFaceID()) 
    self.Image_head:loadTexture(str,ccui.TextureResType.plistType)
    for i=1,self._HeadCount do
        local btn = self.LayerHead:getChildByName("Image_"..(i-1))
        if i == 10 then
            btn:loadTexture("head_mwoman_10.png",ccui.TextureResType.plistType)
        elseif i == 11 then
            btn:setVisible(true)
            btn:loadTexture("head_super_100.png",ccui.TextureResType.plistType)
        else
            btn:loadTexture("head_mwoman_0"..i..".png",ccui.TextureResType.plistType)
        end
       
        btn:setTag(i) 
        if i == 1 or i == 11 then
            self.Image_Select:setPosition(btn:getPosition())
            if i == 1 then self._HeadID = i elseif i == 11 then self._HeadID = 21 end
        end
    end
         self.checkBox_nanBtn:addEventListener(function(sender,event)
         if event == 0 then 
            self.checkBox_nvBtn:setSelected(false)
            self.checkBox_nanBtn:setEnabled(false)
             self.checkBox_nvBtn:setEnabled(true)
         end
     end)
      self.checkBox_nvBtn:addEventListener(function(sender,event)
         if event == 0 then 
            self.checkBox_nanBtn:setSelected(false)
            self.checkBox_nanBtn:setEnabled(true)
             self.checkBox_nvBtn:setEnabled(false)
         end
     end)
    --title特效
    local pSpine = "hall/effect/325_shangchengjiemiandonghua/325_shangchengjiemiandonghua"
    self.m_pAniTitle = sp.SkeletonAnimation:createWithBinaryFile(pSpine .. ".skel", pSpine .. ".atlas", 1)
    self.m_pAniTitle:setAnimation(0, "animation2", true)
    self.m_pAniTitle:setPosition(cc.p(715,375))
    self.m_pAniTitle:addTo(self.m_pNodeRoot)
--    self:initUsrInfoView()
   -- CMsgHall:sendUserQueryInsureInfo() --查询银行总充值
end
 
   

-------------------------init----------------------------
function UserCenterLayer:initUsrInfoView(bIsShiXunLayer)  
    --------------------------------------------------------------------
    --更新玩家信息
    --id 
    self.Text_Name:setString(Player:getNickName())
    self.m_pLbGameID:setString(Player:getAccountID())
    --gold 
    self.m_pLbGold:setString(Player:getGoldCoin())
    --bank 
    self.m_pLbBankGold:setString(Player:getBankGoldCoin())
    --head
    local str =ToolKit:getHead(Player:getFaceID()) 
    self.m_pImgHead:loadTexture(str,ccui.TextureResType.plistType)
     if Player:getPhoneNumber()~="" then  
		self.Label_Phone:setString(Player:getPhoneNumber());  
      self.Button_Phone:setVisible(false)
          
	end 

     if Player:getStrBankID()~="" then 
          local t = Player:getStrBankID() 
        local tbl = require("cjson").decode( t ) 
		self.Label_YHK:setString(tbl["payId"]);  
        self.Button_YHK:loadTextures("hall/image/genghuan.png","hall/image/genghuan.png","hall/image/genghuan.png",0)
         
	else 
	   -- self.Button_YHK:loadTextures("dt/image/zcm_duihuan/zcm_dh12.png")
		self.Label_YHK:setString("未绑定");
	end 
     
     if Player:getStrAlipayID()~="" then 
         local t = Player:getStrAlipayID()
        local tbl = require("cjson").decode( t )
		self.Button_ZFB:loadTextures("hall/image/genghuan.png")
        self.Label_zfb:setString(tbl.payId); 
         
	else 
	    self.Label_zfb:setString("未绑定");
	--	self.Button_ZFB:setTexture("dt/image/zcm_duihuan/zcm_dh12.png")
	end 
    --frame
--    local nFrame = PlayerInfo.getInstance():getFrameID()
--    local strFrameIcon = string.format("hall/plist/userinfo/gui-frame-v%d.png", nFrame)
--    self.m_pImgFrame:loadTexture(strFrameIcon, ccui.TextureResType.plistType)
    --------------------------------------------------------------------
   -- self:onMsgUpdateInfo()
  --  self:setYueCardData()
end

--------------------------click-----------------------------
-- 关闭
function UserCenterLayer:onCloseClicked()
   g_AudioPlayer:playEffect("public/sound/sound-close.mp3")

    self:onMoveExitView()
end
-- 头像
function UserCenterLayer:onHeadClicked()
   g_AudioPlayer:playEffect("public/sound/sound-button.mp3")

    UserHeadView:create():addTo(self.m_rootUI, G_CONSTANTS.Z_ORDER_TOP)
end
-- 打开vip界面
function UserCenterLayer:onVipClicked()
   g_AudioPlayer:playEffect("public/sound/sound-button.mp3")
    
    SLFacade:dispatchCustomEvent(Public_Events.MSG_OPEN_HALL_LAYER, "vip")
end
--系统设置
function UserCenterLayer:onSettingClicked()
   g_AudioPlayer:playEffect("public/sound/sound-button.mp3")

    SettingView.create():addTo(self)
end
--切换账号
function UserCenterLayer:onLogoutClicked()
   g_AudioPlayer:playEffect("public/sound/sound-button.mp3")
    
    local pNoticeDlg = NoticeDialog.create()
    pNoticeDlg:setName("NoticeDialog")
    pNoticeDlg:setNoticeType(NoticeDialog.NOTICE_LOGOUT)
    pNoticeDlg:addTo(self, G_CONSTANTS.Z_ORDER_TOP)
end 
-- 复制 gameID
function UserCenterLayer:onCopyGameID()
   g_AudioPlayer:playEffect("public/sound/sound-button.mp3")

    local gameId = PlayerInfo.getInstance():getGameID()
    LuaNativeBridge.getInstance():setCopyContent(gameId)
    FloatMessage.getInstance():pushMessage("STRING_036_0")
end
-- 打开注册界面
function UserCenterLayer:onBindMobileClicked()
   g_AudioPlayer:playEffect("public/sound/sound-button.mp3")

    if not PlayerInfo.getInstance():getIsGuest() then  
        SLFacade:dispatchCustomEvent(Public_Events.MSG_OPEN_HALL_LAYER, "mobile")
    else
        SLFacade:dispatchCustomEvent(Public_Events.MSG_OPEN_HALL_LAYER, "register")
    end
end
-- 打开绑定公众号界面
function UserCenterLayer:onBindWechatClicked()
   g_AudioPlayer:playEffect("public/sound/sound-button.mp3")

    SLFacade:dispatchCustomEvent(Public_Events.MSG_OPEN_HALL_LAYER, "wechat")
end

--打开月卡商城界面
function UserCenterLayer:onCardShopClicked()
   g_AudioPlayer:playEffect("public/sound/sound-button.mp3")
    SLFacade:dispatchCustomEvent(Hall_Events.OPEN_YUECARD_VIEW)
    self:onMoveExitView()    
end

--更新月卡数据(不用实时刷新)
function UserCenterLayer:setYueCardData()
    local card_data = PlayerInfo.getInstance():getMonthCardData()
    --如果没有月卡数据 就隐藏
    if not card_data or not card_data.bMonthCardUser then
        self.m_pNodeCard:hide()
        return
    end

    local end_time = PlayerInfo.getInstance():getMonthCardTime()
    end_time = os.time({ year = end_time.wYear, month= end_time.wMonth, day = end_time.wDay, hour = end_time.wHour, min = end_time.wMinute, sec = end_time.wSecond, isdst = false })
    local leave_time = os.difftime(end_time,os.time()+PlayerInfo.getInstance():getTimeOffset()+LuaUtils.getWithGMT8Time())
    local str = LuaUtils.getCardDateStr(leave_time,true)
    if not str then
        self.m_pNodeCard:hide()
    else
        self.m_pCardTxt:setString(str)
        self.m_pNodeCard:show()
    end
end

return UserCenterLayer
