--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion 

local MAIL_PATH = "hall/MailView.csb";
local MailDetail =import(".MailDetail")
local HNlayer = import("..HNLayer")
local MailLayer = class("MailLayer",function()
    return HNlayer.new()
end) 
function MailLayer:ctor()
    self:myInit()
    self:setupViews()
    addMsgCallBack(self, MSG_MAIL_UPDATE_SUCCESS, handler(self, self.updateMailViews))
    ToolKit:registDistructor(self, handler(self, self.onDestroy))
end
function MailLayer:myInit()
   
end
 function MailLayer:onTouchCallback(sender)
    local name = sender:getName()
    if name == "button_closeBtn" then
        self:close() 
     
    end
 end  

 function MailLayer:updateMailViews() 
    self.m_MailList = GlobalMailController:getMailList()
    	table.sort(self.m_MailList,function(a,b)  
		return a:getState()<b:getState()  
	end)
    self.listView_contentList:removeAllItems() 
    if table.nums(self.m_MailList) == 0 then
        self.text_mailtishi:setVisible(true)
		return
	end 
       self.listView_contentList:setItemsMargin(0)
         self.listView_contentList:setGravity(0)
    self.listView_contentList:jumpToTop()
    for key, value in ipairs(self.m_MailList) do 
        local item = self.image_item:clone()   
        item:ignoreContentAdaptWithSize(false) ;
        item:setContentSize(self.image_item:getContentSize()) 
		item.index = key
        item.value = value
		ToolKit:listenerNodeTouch(self, item, handler(self, self.mailCellBtnBack))
        item.image_bgUnread = item:getChildByName("image_bgUnread") 
          item.image_bgUnread:ignoreContentAdaptWithSize(false) ;
        item.image_bgUnread:setContentSize(self.image_item:getContentSize()) 
        local text_timeLabel = item:getChildByName("text_timeLabel")
        text_timeLabel:setString( os.date("%Y/%m/%d %H:%M:%S",value:getSendDate())) 

        item.text_yjbt =  item:getChildByName("text_yjbt")
        item.image_readFlag= item:getChildByName("image_readFlag")
        item.image_content = item:getChildByName("image_content")
        item.image_flagMail =  item:getChildByName("image_flagMail")
        if value:getState() == 2 then
            item.image_content:setVisible(false)
            item.image_readFlag:setVisible(true)
            item.image_flagMail:loadTexture("hall/image/email/2.png")
            item.image_bgUnread:setVisible(false)
        elseif value:getState() == 3 then
             item.image_content:setVisible(false)
            item.text_yjbt:setVisible(true)
             item.image_readFlag:setVisible(true)
             item.image_bgUnread:setVisible(false)
            item.image_flagMail:loadTexture("hall/image/email/2.png")
        end
		item.text_yjbt:setString(value:getTitle())
       -- item:setPosition(-350,-70)
        item:setSwallowTouches(false) 
        self.listView_contentList:pushBackCustomItem(item)
	end  
 
  
 end

 
function MailLayer:mailCellBtnBack(sender)
	g_AudioPlayer:playEffect("audio/click_audio.mp3") 
	GlobalMailController:readMail(sender.index) 
    if sender.value:getState() == 2 then
        sender.image_content:setVisible(false)
        sender.image_readFlag:setVisible(true)
        sender.image_flagMail:loadTexture("hall/image/email/2.png")
    elseif sender.value:getState() == 3 then
        sender.image_content:setVisible(false)
        sender.text_yjbt:setVisible(true)
        sender.image_flagMail:loadTexture("hall/image/email/2.png")
    end 
        self:removeChildByTag(1000)
        self.MailDetail =MailDetail.new()
        self:addChild(self.MailDetail,20000,1000) 
    self.MailDetail:updateContent(sender)

end
function MailLayer:setupViews()

	 
    local node = UIAdapter:createNode(MAIL_PATH); 
    self:addChild(node,10000); 
   
    UIAdapter:adapter(node, handler(self, self.onTouchCallback))
    
    local center = node:getChildByName("center")
    local image_Bg = center:getChildByName("image_Bg") 
    local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY))
     
    local center = node:getChildByName("center") 
    local diffX = 145 - (1624-display.size.width)/2
    center:setPositionX(diffX)
    local panel_answerPanel = image_Bg:getChildByName("panel_answerPanel")
    self.listView_contentList = panel_answerPanel:getChildByName("listView_contentList")
    self.text_mailtishi = panel_answerPanel:getChildByName("text_mailtishi")

    self.image_item = node:getChildByName("image_item")
    self:showMailView()
end
 
  function MailLayer:showMailView()
	if not GlobalMailController then
	   GlobalMailController = require("app.hall.mail.control.MailController").new()
	end
	GlobalMailController:requestUpdateMail(1)
end

function MailLayer:onEndAnimation()
	GlobalMailController:requestUpdateMail(1)
end

function MailLayer:onDestroy()
	removeMsgCallBack(self, MSG_MAIL_UPDATE_SUCCESS)
end

return MailLayer