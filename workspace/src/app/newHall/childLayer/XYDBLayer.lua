--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion 

local XYDBHelpLayer = import(".XYDBHelpLayer")
local XYDBTurnLayer = import(".XYDBTurnLayer")
local TurntableName = {"白银轮盘","黄金轮盘","钻石轮盘"}
local XYDB_PATH = "xingyunduobao/XYDB_newMain.csb";

local HNlayer = import("..HNLayer")
local XYDBLayer = class("XYDBLayer",function()
    return HNlayer.new()
end) 

function XYDBLayer:ctor()
    self:myInit()
    self:setupViews()
end

function XYDBLayer:myInit()
   self._turnBtn1 = nil
   self._turnBtn2 = nil
   self._turnBtn3 = nil
   self._dajiangBtn = nil
   self._gerenBtn = nil
   self._dajiangPanel = nil
   self._gerenPanel = nil
   self._dajiangList = nil
   self._gerenList = nil
   self._dajiangText = nil
   self._gerenText = nil
    addMsgCallBack(self, LOADALLROULETTEHIST_ACK, handler(self, self.enterGame))
    addMsgCallBack(self, LOADROULETTEHIST_ACK, handler(self, self.updataGerenlist))
    addMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS, handler(self, self.updataMyBetInfo))  
     addMsgCallBack(self, NEWALLROULETTEHIST_ACK, handler(self, self.updatalist))  
      ToolKit:registDistructor( self, handler(self, self.onDestory) )
end
function XYDBLayer:onDestory()
    

    removeMsgCallBack(self, LOADALLROULETTEHIST_ACK) 
    removeMsgCallBack(self, LOADROULETTEHIST_ACK)
    removeMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS)
    removeMsgCallBack(self, NEWALLROULETTEHIST_ACK) 
end
function XYDBLayer:onTouchCallback(sender)
    local posx = 0
    if (self._winSize.width / self._winSize.height > 1.78) then 
        posx = (self._winSize.width/display.scaleX-self._winSize.width)/ 2
    end
    local name = sender:getName()
    if name == "button_close" then
        self:close() 
    elseif name == "button_helpbtn" then
        local XYDBHelpLayer = XYDBHelpLayer.new()
        self:addChild(XYDBHelpLayer, 2)
    elseif name == "button_turnTable_1" then
        local XYDBTurnLayer = XYDBTurnLayer.new(1)
        self:addChild(XYDBTurnLayer)
    elseif name == "button_turnTable_2" then
        local XYDBTurnLayer = XYDBTurnLayer.new(2)
        self:addChild(XYDBTurnLayer)
    elseif name == "button_turnTable_3" then
        local XYDBTurnLayer = XYDBTurnLayer.new(3)
        self:addChild(XYDBTurnLayer)
    elseif name == "button_showdajiang" then
--        self._dajiangBtn:loadTextures("xingyunduobao/image/newimage/xydb_060.png","xingyunduobao/image/newimage/xydb_060.png","xingyunduobao/image/newimage/xydb_060.png")
--        self._gerenBtn:loadTextures("xingyunduobao/image/newimage/null2.png","xingyunduobao/image/newimage/null2.png","xingyunduobao/image/newimage/null2.png")
--        self._dajiangText:loadTexture("xingyunduobao/image/newimage/xydb_057.png")
--        self._gerenText:loadTexture("xingyunduobao/image/newimage/xydb_054.png")
        self.geren:setVisible(false)
        self.dajiang:setVisible(true)
    elseif name == "button_showgeren" then
--        self._dajiangBtn:loadTextures("xingyunduobao/image/newimage/null2.png","xingyunduobao/image/newimage/null2.png","xingyunduobao/image/newimage/null2.png")
--        self._gerenBtn:loadTextures("xingyunduobao/image/newimage/xydb_060.png","xingyunduobao/image/newimage/xydb_060.png","xingyunduobao/image/newimage/xydb_060.png")
--        self._dajiangText:loadTexture("xingyunduobao/image/newimage/xydb_056.png")
--        self._gerenText:loadTexture("xingyunduobao/image/newimage/xydb_055.png")
         self.geren:setVisible(true)
        self.dajiang:setVisible(false)
    end
end

function XYDBLayer:setupViews()
	local node = UIAdapter:createNode(XYDB_PATH);
	
	self:addChild(node); 
    local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY))
     local center = node:getChildByName("center") 
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
    local image2 = center:getChildByName("image_bg"):getChildByName("Image_2")
  --  image2:setScaleX(1/display.scaleX)
    local btn_close = center:getChildByName("image_bg"):getChildByName("button_close")
  --  btn_close:setScaleX(1/display.scaleX)
    local btn_help = center:getChildByName("image_bg"):getChildByName("button_helpbtn")
  --  btn_help:setScaleX(1/display.scaleX)

    self._turnBtn1 = center:getChildByName("image_bg"):getChildByName("button_turnTable_1")
   -- self._turnBtn1:setScaleX(1/display.scaleX)
    self._turnBtn2 = center:getChildByName("image_bg"):getChildByName("button_turnTable_2")
   -- self._turnBtn2:setScaleX(1/display.scaleX)
    self._turnBtn3 = center:getChildByName("image_bg"):getChildByName("button_turnTable_3")
    --self._turnBtn3:setScaleX(1/display.scaleX)
     self._turnBtn1:getChildByName("TextAtlas_1"):setString(GlobalXYDBController.m_LuckyRoulette.m_silverCost)
    self._turnBtn2:getChildByName("TextAtlas_2"):setString(GlobalXYDBController.m_LuckyRoulette.m_goldCost)
    self._turnBtn3:getChildByName("TextAtlas_3"):setString(GlobalXYDBController.m_LuckyRoulette.m_diamondCost) 
    self._turnBtn1:setBright(true)
    self._turnBtn2:setBright(true)
    self._turnBtn3:setBright(true)

    local image18 = node:getChildByName("Image_18")
   -- image18:setScaleX(1/display.scaleX)
    local image_myinfo = node:getChildByName("image_myinfo")
  --  image_myinfo:setScaleX(1/display.scaleX)

--    self._dajiangBtn = image18:getChildByName("button_showdajiang")
--    self._gerenBtn = image18:getChildByName("button_showgeren")
--    self._dajiangText = self._dajiangBtn:getChildByName("Image_23_0")
--    self._gerenText = self._gerenBtn:getChildByName("Image_23_0")
--    self._dajiangPanel = image18:getChildByName("Image_19"):getChildByName("node_dajiang")
--    self._gerenPanel = image18:getChildByName("Image_19"):getChildByName("node_geren")
--    self._dajiangList = self._dajiangPanel:getChildByName("listview_dajianglist")
--    self._gerenList = self._gerenPanel:getChildByName("listview_gerenlist")

--    self._dajiangBtn:loadTextures("xingyunduobao/image/newimage/xydb_060.png","xingyunduobao/image/newimage/xydb_060.png","xingyunduobao/image/newimage/xydb_060.png")
--    self._gerenBtn:loadTextures("xingyunduobao/image/newimage/null2.png","xingyunduobao/image/newimage/null2.png","xingyunduobao/image/newimage/null2.png")
--    self._dajiangText:loadTexture("xingyunduobao/image/newimage/xydb_057.png")
--    self._gerenText:loadTexture("xingyunduobao/image/newimage/xydb_054.png")
--    self._dajiangPanel:setVisible(true)
--    self._gerenPanel:setVisible(false)

--    if (self._winSize.width / self._winSize.height > 1.78) then 
--		node:setPositionX((self._winSize.width-self._winSize.width/display.scaleX)/2);
--	end
    UIAdapter:parseCSDNode(node,self)
	UIAdapter:adapter(node, handler(self, self.onTouchCallback))
    
    self.geren:setVisible(false)
    self.dajiang:setVisible(true)
    self:initList()
    self:updataMyBetInfo()
    GlobalXYDBController:LoadRouletteHistReq()
end

--
function XYDBLayer:initList()
    -- dajiang
    self.dajianglist = self.dajiang:getChildByName("listview_dajianglist")
    self.dajianglist:setItemModel(self.upItem)

    self.dajianglist:removeAllItems()
  --  self.dajianglist:setScrollBarEnabled(false)

    -- geren
    self.gerenlist = self.geren:getChildByName("listview_gerenlist")
    self.gerenlist:setItemModel(self.upSelfItem)
    self.gerenlist:removeAllItems()
 --   self.gerenlist:setScrollBarEnabled(false)
end

--进入游戏的时候
function XYDBLayer:enterGame(_,info)
    --延迟0.2s加载数据是为了防止弹窗动画和界面更新同时进行会造成卡顿问题
    performWithDelay(self,function()  
        self:updateView(info)     
    end,0.3)  
   

end

function XYDBLayer:updateView(info)
    -- body
    self:updataDajianglist(info.m_vBigHistory)
    self:updataXialist(info.m_vNormalHistory)
    --self:updataMyBetInfo()

--    --表示还没显示
--    if tonumber(cc.UserDefault:getInstance():getStringForKey("wheelreward_isShow", 0)) == 1 then
--        local dtCommonModel = cc.loaded_packages.DTCommonModel
--        local signinStatus = dtCommonModel:getSigninStatus()
--        -- signinStatus.luckywheelreward = 3000
--        -- Player:setTodaybetcnt(3000)

--        local add = math.floor(signinStatus.luckywheelreward)

--        if add > 0 then
--            cc.UserDefault:getInstance():setStringForKey("wheelreward_isShow", 0)
--            cc.UserDefault:getInstance():flush()

--            TOAST(string.format(Lang.SignIn_Add, add))
--            --明日有效积分
--            local eachLabel = self.myinfo:getChildByName("atlas_torJifen")
--            local base  = Player:getTodaybetcnt() -  add

--            local function int(x) 
--                return x>=0 and math.floor(x) or math.ceil(x)
--            end
--            local curTime = 0
--            local function updateLabelNum(dt)
--                curTime = curTime + dt
--                local time = curTime / 3
--                if time < 1 then 
--                    local tempNum = int(( add - 1) *time) 
--                    local num = math.floor(base + tempNum)
--                    eachLabel:setString(num)
--                else
--                    eachLabel:setString(Player:getTodaybetcnt())
--                    eachLabel:unscheduleUpdate()  
--                end
--            end
--            eachLabel:scheduleUpdate(updateLabelNum)
--        end
--    end
end

--我自己的中奖列表返回
function XYDBLayer:selfRecordListRes()
    self.isGetMe = true
    self.geren:setVisible(true)
    self.dajiang:setVisible(false)
    self:updataGerenlist()
end
--有玩家中大奖信息推过来，因为玩家中奖列表是每秒轮换的，不需要推送更新
function XYDBLayer:LuckyWheelDaJiangNtc()
    self:updataDajianglist(true)
end
-- 更新中大奖的列表信息
function XYDBLayer:updataDajianglist(info,isadd)

    local function updataCell(item,cellData )

        local  times = cellData.m_nTime
        if string.len(times) == 13 then
            cellData.m_nTime = math.floor(cellData.m_nTime/1000)
        end
        local timeStr = os.date("%m/%d  %H:%M", cellData.m_nTime)
        item:getChildByName("time"):setString(timeStr)
        item:getChildByName("name"):setString(cellData.m_nick) 
        item:getChildByName("money"):setString(cellData.m_award.." 元")
        --中奖类型
        item:getChildByName("turntype"):setString(TurntableName[cellData.m_type])
        item:getChildByName("money_icon"):setVisible(true)
    end
     
    if isadd then
        --收到新数据则只在第一条上面插入一条新数据
        self.dajianglist:insertDefaultItem(0)
        local item = self.dajianglist:getItem(0)
        updataCell(item,info)
        local allItems = self.dajianglist:getItems()
        if #allItems > 50 then
            self.dajianglist:removeLastItem()
        end
    else
        if #info > 0 then
            for i,v in ipairs(info) do
                self.dajianglist:pushBackDefaultItem()
                local item = self.dajianglist:getItem(i-1)
                updataCell(item,v)
            end
        end
    end
end

--自己的中奖信息的返回
function XYDBLayer:spinWheelRes(event)
    local tab = event._userdata
    if  tab._hit ~= -2 then
        self:updataGerenlist(true)
    end
    self:updataMyBetInfo()
end

-- 更新右下的信息
function XYDBLayer:updataMyBetInfo()
    --当前积分
    local nowJifen = self.myinfo:getChildByName("atlas_nowJifen")
    nowJifen:setString(Player:getCurBetScore())

    --明日有效积分
    local eachLabel = self.myinfo:getChildByName("atlas_torJifen")
    
    eachLabel:setString(Player:getTomorrowBetScore())
end 

function XYDBLayer:updatalist(msg,info)
    if info.m_isBigAward== 1 then
        self:updataDajianglist(info.m_newHistory,true)
   end
    if info.m_newHistory.m_accountId==Player:getAccountID() then 
        self:updataGerenlist(msg,info)
    end 
end
-- 更新我的中奖
function XYDBLayer:updataGerenlist(msg,info)

    local function updataCell(item,cellData )
        local  times = cellData.m_nTime
        if string.len(times) == 13 then
            cellData.m_nTime = math.floor(cellData.m_nTime/1000)
        end
        local timeStr = os.date("%m/%d  %H:%M", cellData.m_nTime)
        item:getChildByName("time"):setString(timeStr)

        item:getChildByName("money"):setString(cellData.m_award.." 元")
        item:getChildByName("turntype"):setString(TurntableName[cellData.m_type])
        item:getChildByName("money_icon"):setVisible(true)
    end  
    self.notips:setVisible(false) 
     if msg == LOADROULETTEHIST_ACK then 
          self.gerenlist:removeAllItems() 
        if #info.m_vHistory > 0 then
            for i,v in ipairs(info.m_vHistory) do
                if v then
                    self.gerenlist:pushBackDefaultItem()
                    local item = self.gerenlist:getItem(i-1)
                    updataCell(item,v)
                end
            end
        else
            self.notips:setVisible(true)
        end
    else
        local allItems = self.gerenlist:getItems()
        if #allItems > 50 then
            self.gerenlist:removeLastItem()
        end
        
        local item = self.gerenlist:getItem(0) 
        self.gerenlist:insertDefaultItem(0)
        local item = self.gerenlist:getItem(0)
        updataCell(item,info.m_newHistory) 
    end
end
function XYDBLayer:getAllWinning()

	local temp = GlobalXYDBController.allWinning[#GlobalXYDBController.allWinning]

	table.insert( GlobalXYDBController.allWinning,1,temp)
	table.remove(GlobalXYDBController.allWinning,#GlobalXYDBController.allWinning)

	return GlobalXYDBController.allWinning
end
-- 更新玩家的中奖
function XYDBLayer:updataXialist(info)
    local function updataCell(item,cellData )
        local wanjia =  item:getChildByName("name") 
        local str = "恭喜["..cellData.m_nick.."]在"..TurntableName[cellData.m_type].."中得"
        wanjia:setString(str) 
        local jinbi =  item:getChildByName("money")
        jinbi:setString(cellData.m_award.." 元")
    end
     self.data = self:getAllWinning()
    local function showListFunc ()
        local data = info
        local maxNum = #data  > 5 and 5 or #data
        self.moveIndex = 0
        
        local panelSize = self.xialistPanel:getContentSize()
        local addHight = self.downItem:getContentSize().height
        local dp = 5
        local PosAttr = {}
        table.insert(PosAttr, cc.p(panelSize.width/2 ,panelSize.height + dp + addHight + addHight/2))
        for i = 1, maxNum do
            local item = self.downItem:clone()
            item:setAnchorPoint(cc.p(0.5,0.5))
            updataCell(item, data[i])
            item:setPosition(cc.p(panelSize.width/2, panelSize.height - addHight * (i - 1) -(((i - 1)) *dp) - addHight/2 ))
            table.insert(PosAttr, cc.p(item:getPositionX() , item:getPositionY() ) )
            self.xialistPanel:addChild(item)
        end 
        if maxNum >= 5 then
            table.insert(PosAttr,cc.p(panelSize.width/2, panelSize.height - addHight * 5 -(5*dp)+ addHight/2) )
        end
        if #data > 5 then
            self.moveIndex = 5
            schedule(self.xialistPanel, function ()
                data = self.data
                if self.moveIndex == #data then
                    self.data = self:getAllWinning()
                    data = self.data
                    self.moveIndex = 0
                end
                self.moveIndex = self.moveIndex + 1
                local item = self.downItem:clone()
                updataCell(item, data[self.moveIndex])
                item:setAnchorPoint(cc.p(0.5,0.5))
                item:setPosition(PosAttr[1]) --panelSize.height - addHight * (6 - 1) )
                self.xialistPanel:addChild(item)
                item:runAction(cc.Sequence:create(cc.DelayTime:create(0.2), cc.CallFunc:create(
                    function()
                        local allNode = self.xialistPanel:getChildren()
                        table.sort(allNode, function ( a,b )
                            -- body
                            return a:getPositionY() > b:getPositionY()
                        end )
                        for k ,v in pairs(allNode) do
                            if k < 7 then
                                if PosAttr[k] then
                                    v:runAction(cc.Sequence:create(cc.MoveTo:create(1,cc.p(panelSize.width/2,PosAttr[k + 1].y ))))
                                else
                                    print(k,"索引值")
                                end
                            else
                                if v then
                                    if PosAttr[k] then
                                        v:runAction(cc.Sequence:create(cc.MoveTo:create(1,cc.p(panelSize.width/2,PosAttr[k].y )), cc.RemoveSelf:create()))
                                    end
                                end
                            end
                        end
                    end
                )))
            end , 1.5 )
        end
    end
    showListFunc()
end
 
 
return XYDBLayer