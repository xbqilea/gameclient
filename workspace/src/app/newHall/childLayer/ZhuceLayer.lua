local PhoneRegisterLayer = import(".PhoneRegisterLayer")

local ZHUCE_PATH = "hall/image/zcmdwc_zhuce/zcmdwc_zhuce.csb";

local HNlayer = import("..HNLayer")
local ZhuceLayer = class("ZhuceLayer",function()
    return HNlayer.new()
end) 

function ZhuceLayer:ctor()
    self:myInit()
    self:setupViews()
end

function ZhuceLayer:myInit()

end

function ZhuceLayer:onTouchCallback(sender)
    local posx = 0
   
    local name = sender:getName()
    if name == "button_close" then
        g_AudioPlayer:playEffect(GAME_SOUND_CLOSE)
        self:getParent():setZhucePopup(false)
        self:close() 
    elseif name == "button_registerBtn" then
        local phoneRegisterLayer = PhoneRegisterLayer.new(OpenType.ZhuanZheng)  
        self:addChild(phoneRegisterLayer) 
        --self:close()
    end
end

function ZhuceLayer:setupViews()
    --g_AudioPlayer:playEffect("dt/sound/binding_sound.mp3")
	local node = UIAdapter:createNode(ZHUCE_PATH);
	
	self:addChild(node); 
     node:setPositionY((display.height - 750) / 2) 
     UIAdapter:adapter(node, handler(self, self.onTouchCallback))
     UIAdapter:praseNode(node,self)
    --node
    self.m_pNodeRoot    = node:getChildByName("node_parentNode")
    self.m_pNodeRoot:setPositionX((display.width - 1334) / 2)
    local gold_num = Player:getRegularAward()/100 
     self.txt_Gold:setString(gold_num)
     if gold_num>=10 then
        self.txt_Gold:setScale(0.6)
     end
	
end
 
  
return ZhuceLayer