
local XYDB_HELP_PATH = "xingyunduobao/XYDB_Help.csb";

local HNlayer = import("..HNLayer")
local XYDBHelpLayer = class("XYDBHelpLayer",function()
    return HNlayer.new()
end) 

function XYDBHelpLayer:ctor()
    self:myInit()
    self:setupViews()
end

function XYDBHelpLayer:myInit()
   
end

function XYDBHelpLayer:onTouchCallback(sender)
    local name = sender:getName()
    if name == "button_close" then
        self:close() 
    elseif name == "button_blocktouch" then
        self:close()
    end
end

function XYDBHelpLayer:setupViews()
	local node = UIAdapter:createNode(XYDB_HELP_PATH);
	
	local center = node:getChildByName("Layer") 
    local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY))
     
    local diffX = 145-(1624-display.size.width)/2 
    center:setPositionX(diffX)
    
    self:addChild(node); 
	UIAdapter:adapter(node, handler(self, self.onTouchCallback))
end
 
  
return XYDBHelpLayer