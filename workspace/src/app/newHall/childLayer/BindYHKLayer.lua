   
    
local BankList = {"中国银行","工商银行","农业银行","交通银行","建设银行","招商银行","兴业银行","民生银行","平安银行","中信银行","浦发银行","光大银行","中国邮储银行"}
local YHK_PATH = "hall/BindBankpayView.csb";

local HNlayer = import("..HNLayer")
local BindYHKLayer = class("BindYHKLayer",function()
    return HNlayer.new()
end) 
function BindYHKLayer:ctor()
    self:myInit()
    self:setupViews()
     ToolKit:registDistructor( self, handler(self, self.onDestory) )
      addMsgCallBack(self, MSG_BANKBIND_ACK, handler(self, self.onBankBindEx )) 
end
function BindYHKLayer:myInit() 
end
function BindYHKLayer:onDestory()
    print("*******ExchangeRmbLayer:onDestory()*******") 
    removeMsgCallBack(self,  MSG_BANKBIND_ACK )
end
function BindYHKLayer:onBankBindEx( msgType , __info) 
    if __info.m_nRetCode == 0 then 
        self:close()  
    else
        ToolKit:showErrorTip(__info.m_nRetCode)
   end
end
 function BindYHKLayer:onTouchCallback(sender)
    local name = sender:getName()
    if name == "button_closeBtn" then
        self:close()  
    elseif name == "button_bindBtn" then
         g_AudioPlayer:playEffect(GAME_SOUND_BUTTON);
--          if self.textField_cityInput:getString() == "" or self.textField_openCardBankInput:getString() == "" or self.textField_bankCodeInput:getString() == "" or self.text_bankName:getString() == ""
--            or self.textField_nameInput:getString() == "" then
--            TOAST("您输入的信息不完整，请核对后重新提交");
--            return
--        end 
            local function checkInfoAll( ... )
		    -- body --and #openCardBankInput:getText()  > 0 
		    if #self.textField_nameInput:getText() > 0 and #self.textField_bankCodeInput:getText()  > 0 and 
			    #self.textField_bankCodeInput:getText()  > 0 
			     and #self.text_bankName:getString() > 0 then
			    return true
		    end
		    return false
	    end

	    -- local function checkID( ... )
	    -- 	-- body
	    -- 	if #idNumCodeInput:getText()  == 18 then
	    -- 		return true
	    -- 	end
	    -- 	return false
	    -- end

	    local function checkBankNum( ... )
		    -- body
		    if #self.textField_bankCodeInput:getText()  > 12 then
			    return true
		    end
		    return false
	    end

	    if not checkInfoAll() then
		    TOAST("账号信息未全部填写")
		    return
	    end
	    if  not checkBankNum() then
		    TOAST("银行卡位数不正确")
		    return
	    end
        local function StringToTable(s)  
            local tb = {}  
      
            --[[  
            UTF8的编码规则：  
            1. 字符的第一个字节范围： 0x00—0x7F(0-127),或者 0xC2—0xF4(194-244); UTF8 是兼容 ascii 的，所以 0~127 就和 ascii 完全一致  
            2. 0xC0, 0xC1,0xF5—0xFF(192, 193 和 245-255)不会出现在UTF8编码中   
            3. 0x80—0xBF(128-191)只会出现在第二个及随后的编码中(针对多字节编码，如汉字)   
            ]]  
            for utfChar in string.gmatch(s, "[%z\1-\127\194-\244][\128-\191]*") do  
                table.insert(tb, utfChar)  
            end  
      
            return tb  
        end 
        local sTable = StringToTable(self.textField_bankCodeInput:getText())  
        for i=1,#sTable do  
            local utfCharLen = string.len(sTable[i])  
            if utfCharLen > 1 then -- 长度大于1的就认为是中文  
 			    TOAST("银行卡号不能为中文")
 			    return
            end  
        end 
         g_ExchangeRmbController:reqBankBind({self.textField_bankCodeInput:getText(),self.textField_nameInput:getText(), self.text_bankName:getString() , self.textField_openCardBankInput:getText(),
          self.textField_provinceInput:getString(),self.textField_cityInput:getString() }  )
    elseif name == "button_openBankList" then 
        local flag = self.panel_bankListBg:isVisible()
         self.panel_bankListBg:setVisible( not flag)
       
    end
 end  
function BindYHKLayer:setupViews()
 
    local node = UIAdapter:createNode(YHK_PATH); 
   
    self:addChild(node); 
    UIAdapter:adapter(node, handler(self, self.onTouchCallback))
    local center =node:getChildByName("center"); 
    self._imageBG =center:getChildByName("image_Bg");  
     local center =node:getChildByName("center"); 
    local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,diffY)) 
    local diffX = 145 - (1624-display.size.width)/2
    center:setPositionX(diffX)
   local image_nameInputBg =  self._imageBG:getChildByName("image_nameInputBg"); 
   self.textField_nameInput = image_nameInputBg:getChildByName("textField_nameInput"); 
   local image_openBankImg = self._imageBG:getChildByName("image_openBankImg"); 
   local image_bankcode = self._imageBG:getChildByName("image_bankcode"); 
   local image_openCardBankImg = self._imageBG:getChildByName("image_openCardBankImg"); 
   local image_cityImg= self._imageBG:getChildByName("image_cityImg"); 
   local image_provinceImg =self._imageBG:getChildByName("image_provinceImg"); 
   self.textField_provinceInput =image_provinceImg:getChildByName("textField_provinceInput"); 
   self.textField_cityInput =image_cityImg:getChildByName("textField_cityInput"); 
   self.textField_openCardBankInput = image_openCardBankImg:getChildByName("textField_openCardBankInput"); 
   self.textField_bankCodeInput = image_bankcode:getChildByName("textField_bankCodeInput"); 
  -- self.text_bankName = image_openBankImg:getChildByName("text_bankName");  
   self.panel_bankListBg = node:getChildByName("panel_bankListBg"); 
   self.text_bankName = image_openBankImg:getChildByName("TextField_1");  
   local editTxt1 = nil
   if UpdateFunctions.platform == "ios" then
        editTxt1 = ccui.EditBox:create(cc.size(360,45), "hall/image/bank/zcm_zc1_2.png")
    else
        editTxt1 = ccui.EditBox:create(cc.size(360,70), "hall/image/bank/zcm_zc1_2.png")
    end
    editTxt1:setName("inputName")
    editTxt1:setAnchorPoint(0.5,0.5)  
    editTxt1:setPosition(cc.p(image_nameInputBg:getBoundingBox().width / 2,image_nameInputBg:getBoundingBox().height / 2))
    editTxt1:setFontSize(26)
    editTxt1:setMaxLength(10)
    editTxt1:setFontColor(cc.c4b(255,255,255,255))
    editTxt1:setFontName("ttf/jcy.ttf")
    if UpdateFunctions.platform == "ios" then
        editTxt1:setPlaceholderFontSize(16)
    else
        editTxt1:setPlaceholderFontSize(24)
    end
    editTxt1:setPlaceholderFontColor(cc.c4b(184,187,182,255))  
    if Player:getRealName()~= "" then
        editTxt1:setText( Player:getRealName())
        editTxt1:setEnabled(false)
    else
	    editTxt1:setPlaceHolder("持卡人姓名")
    end
    editTxt1:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE)
    editTxt1:setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE)
    editTxt1:registerScriptEditBoxHandler(function(eventname,sender) self:editboxHandle(eventname,sender) end)
    image_nameInputBg:addChild(editTxt1,5)
    self.textField_nameInput:removeFromParent()
    self.textField_nameInput = editTxt1

    local editTxt2 = nil
   if UpdateFunctions.platform == "ios" then
        editTxt2 = ccui.EditBox:create(cc.size(360,45), "hall/image/bank/zcm_zc1_2.png")
    else
        editTxt2 = ccui.EditBox:create(cc.size(360,70), "hall/image/bank/zcm_zc1_2.png")
    end
    editTxt2:setName("inputBankCode")
    editTxt2:setAnchorPoint(0.5,0.5)  
    editTxt2:setPosition(cc.p(image_bankcode:getBoundingBox().width / 2,image_bankcode:getBoundingBox().height / 2))
    editTxt2:setFontSize(26)
    editTxt2:setMaxLength(30)
    editTxt2:setFontColor(cc.c4b(255,255,255,255))
    editTxt2:setFontName("ttf/jcy.ttf")
    if UpdateFunctions.platform == "ios" then
        editTxt2:setPlaceholderFontSize(16)
    else
        editTxt2:setPlaceholderFontSize(24)
    end
    editTxt2:setPlaceholderFontColor(cc.c4b(184,187,182,255))  
	editTxt2:setPlaceHolder("银行卡账号")
    editTxt2:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE)
    editTxt2:setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE)
    editTxt2:registerScriptEditBoxHandler(function(eventname,sender) self:editboxHandle(eventname,sender) end)
    image_bankcode:addChild(editTxt2,5)
    self.textField_bankCodeInput:removeFromParent()
    self.textField_bankCodeInput = editTxt2

    local editTxt3 = nil
   if UpdateFunctions.platform == "ios" then
        editTxt3 = ccui.EditBox:create(cc.size(360,45), "hall/image/bank/zcm_zc1_2.png")
    else
        editTxt3 = ccui.EditBox:create(cc.size(360,70), "hall/image/bank/zcm_zc1_2.png")
    end
    editTxt3:setName("inputOpenBank")
    editTxt3:setAnchorPoint(0.5,0.5)  
    editTxt3:setPosition(cc.p(image_openCardBankImg:getBoundingBox().width / 2,image_openCardBankImg:getBoundingBox().height / 2))
    editTxt3:setFontSize(26)
    editTxt3:setMaxLength(20)
    editTxt3:setFontColor(cc.c4b(255,255,255,255))
    editTxt3:setFontName("ttf/jcy.ttf")
    if UpdateFunctions.platform == "ios" then
        editTxt3:setPlaceholderFontSize(16)
    else
        editTxt3:setPlaceholderFontSize(24)
    end
    editTxt3:setPlaceholderFontColor(cc.c4b(184,187,182,255))  
	editTxt3:setPlaceHolder("请输入开户行支行信息")
    editTxt3:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE)
    editTxt3:setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE)
    editTxt3:registerScriptEditBoxHandler(function(eventname,sender) self:editboxHandle(eventname,sender) end)
    image_openCardBankImg:addChild(editTxt3,5)
    self.textField_openCardBankInput:removeFromParent()
    self.textField_openCardBankInput = editTxt3

  -- panel_bankListBg:setVisible(false)
   self.listView_bankList =self.panel_bankListBg:getChildByName("listView_bankList"); 
    self.panel_bankListBg:setVisible(false)
   self.panel_Item =  node:getChildByName("panel_Item");  
    local t = ccui.Widget:create();
    for k,v in pairs(BankList) do 
		local pContain = t:clone()
		pContain:setContentSize(self.panel_Item:getContentSize()); 
		local node =  self.panel_Item:clone()
        local bankName = node:getChildByName("bankName"); 
        bankName:setString(v) 
		node:setPosition(0,0);
		pContain:addChild(node);
		self.listView_bankList:pushBackCustomItem(pContain);
	end  
    self.listView_bankList:addEventListener(function(sender,event)
        if event == 1 then
            local index = self.listView_bankList:getCurSelectedIndex()
            self.text_bankName:setString(BankList[index+1])
            self.panel_bankListBg:setVisible(false)
        end
    end)
    local Image_9 = self._imageBG:getChildByName("Image_9")
    if Player:getStrBankID() =="" then
        Image_9:loadTexture("hall/image/bank/bankyhk.png")
    else
         Image_9:loadTexture("hall/image/bank/changeyhk.png")
    end

end

function BindYHKLayer:editboxHandle(strEventName,sender)  
    local name = sender:getName()
    if strEventName == "began" then  
    	sender:setPlaceHolder("")                                    --光标进入，清空内容/选择全部  
    elseif strEventName == "ended" then  
        if sender:getText() then
            if name=="inputName" then
                sender:setPlaceHolder("持卡人姓名") 
            elseif name=="inputBankCode" then
                sender:setPlaceHolder("银行卡账号") 
            elseif name=="inputOpenBank" then
                sender:setPlaceHolder("请输入开户行支行信息") 
            end
        end                                                        --当编辑框失去焦点并且键盘消失的时候被调用  
    elseif strEventName == "return" then  
         print(sender:getText(), "sender")                         --当用户点击编辑框的键盘以外的区域，或者键盘的Return按钮被点击时所调用  
    elseif strEventName == "changed" then
    	 print(sender:getText(), "sender")                         --输入内容改变时调用   
    end  
end 
 
  
return BindYHKLayer; 