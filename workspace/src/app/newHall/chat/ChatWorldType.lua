--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion
--/**
-- * Created by dongenlai on 18/4/24.
-- * 聊天文字模块儿  (仅限于 文字 和 文字 ＋ 表情组合）
-- * type = 1;
-- */
EmoteList = {"[抠鼻]","[喜欢]","[惊讶]","[害羞]","[无语]","[哭泣]","[生气]","[鼓掌]","[鄙视]","[再见]","[委屈]","[偷笑]"}
local ChatImageType = import(".ChatImageType")

local ChatWorldType = class("ChatWorldType",function()
    return ccui.ImageView:create()
end)
--    /*
--       @ parma jsonType {type1:"你好"， type2:"3"}
--    */
function ChatWorldType:ctor(textStr, isSelf) 
    local res = ""
    if isSelf then 
        res = "hall/image/chat/ltzj1.png"
    else
         res = "hall/image/chat/lt1.png"
    end   
    self:loadTexture(res)  
--    
     self:ignoreContentAdaptWithSize(false)
    if isSelf == true then
        self:setAnchorPoint(1, 1);
    else
        self:setAnchorPoint(0, 1);
    end
    self:initWorld(textStr, isSelf); 
end
 
function ChatWorldType:initWorld(textStr, isSelf) 
    self:updateLine(textStr);
--    -- 底框 箭头
     local res = "1"
   if isSelf then 
        res = "hall/image/chat/ltzj2.png"
    else
         res = "hall/image/chat/lt2.png"
    end   
    local sp =  cc.Sprite:create(res );
    local width = 0
     if isSelf then 
        width = self:getContentSize().width + 2
    else
         width = -2
    end
    sp:setPosition(width,  self:getContentSize().height - 13);
    self:addChild(sp);
end
stringSplit = function(str, split_char)
        local resultStrList = {}
        string.gsub(str, split_char, function (w)
            table.insert(resultStrList, w)
        end)
        return resultStrList
    end
--    /*
--       当前切分字符串是按照一个字符切的 耗费性能 但是切割坐标更加精确一些 （聊天发送的时候判断一下是否是纯文本消息）
--    */
function ChatWorldType:updateLine(textStr)
    --方案 一
    local richText =  ccui.RichText:create();
    richText:ignoreContentAdaptWithSize(false);
    richText:setContentSize(400,80)

--    richText.width = 400;
--    richText.height = 80;
    richText:setAnchorPoint(cc.p(0, 1));
    --判断一下是否是表情关键字 
    local t = stringSplit(textStr,"%b[]");      
    local str = string.gsub(textStr,"%b[]","-+-")
    local f = string.split(str,"-+-")   
    local isEmotflag =false
    for k,v in pairs(t) do
        if ChatWorldType.checkStrIsEmotion(v) then
            isEmotflag = true
        end
    end
    if not isEmotflag then
        if(textStr~= "") then
            -- local re1 = ccui.RichElementText:create(1, cc.color.RED, 255, str, "Helvetica", 24);
            local re1 = ccui.RichElementText:create(1, cc.c3b(0,0,0), 255, textStr, "Helvetica", 20);
            richText:pushBackElement(re1);
        end
    else
--        local emote = {}
--        for k,v in pairs(t) do
--            if ChatWorldType.checkStrIsEmotion(v) then
--                table.insert(emote,v)
--            end
--        end
        local tab = {}
         local i =1
        for k=1,#f do
            if f[k]== "" then
                table.insert(tab,t[i])
                i=i+1
            else
                table.insert(tab,f[k])
                if i==k then
                     table.insert(tab,t[i])
                     i=i+1
                end
            end
        end 
        local str = "";
        for k,v in pairs(tab) do 
            if ChatWorldType.checkStrIsEmotion(v) then
                local sprite =  cc.Sprite:create(string.format("hall/image/chat/emotion/chat_face_%d.png",ChatWorldType.checkStrIsEmotion(v)));
                sprite:setAnchorPoint(cc.p(0, 0.5));
                sprite:setContentSize(cc.size(30,30)); 
                sprite:setScale(0.5)
                local reimg =  ccui.RichElementCustomNode:create(1, cc.c3b(0,0,0), 255, sprite);
                richText:pushBackElement(reimg); 
            else
                 local re1 = ccui.RichElementText:create(1, cc.c3b(0,0,0), 255, v, "Helvetica", 20);
                 richText:pushBackElement(re1);
            end
        end
    end

--    local subStrAry = string.split(textStr,'/(\[[^[\]]+\])/');
--    local str = "";
--    for k = 1, #subStrAry[1] do
--        local curSub = subStrAry[1][k];
--        if(curSub[0] == '[' and curSub[3] == ']') then
--            --判断一下是否是表情关键字
--            local emoStr = curSub.substr(1, 2);
--            local isEmotion = ChatWorldType.checkStrIsEmotion('[' + emoStr + ']');
--            --表情之前的字符
--            if(str ~= "") then
--                -- local re1 = ccui.RichElementText:create(1, cc.color.RED, 255, str, "Helvetica", 30);
--                local re1 = ccui.RichElementText:create(1, cc.color(0,0,0), 255, str, "Helvetica", 30);
--                richText:pushBackElement(re1);
--                str = "";
--            end
--            if(isEmotion) then
--                local sprite =  cc.Sprite:create(isEmotion);
--                sprite:setAnchorPoint(cc.p(0, 0.5));
--                sprite.setScale(0.8);
--                sprite.width = sprite.width*0.85;
--                sprite.height = sprite.height*0.85;
--                local reimg =  ccui.RichElementCustomNode:create(1, cc.color.WHITE, 255, sprite);
--                richText:pushBackElement(reimg); 
--            else
--                str = str+curSub;
--            end
--        else
--            str = str+curSub;
--        end
--    end
--    local str = textStr
--    if(str~= "") then
--        -- local re1 = ccui.RichElementText:create(1, cc.color.RED, 255, str, "Helvetica", 24);
--        local re1 = ccui.RichElementText:create(1, cc.c3b(0,0,0), 255, str, "Helvetica", 24);
--        richText:pushBackElement(re1);
--    end
    self:addChild(richText, 100);
    local size =  richText:getRealSize() 
    local lastWidth = richText:getRealSize().width or 100;
    local lastHeight = richText:getRealSize().height or 100;
    self:setContentSize(lastWidth + 23, lastHeight + 23 );
    if lastHeight>50 then
        self:setScale9Enabled(true);
    end 
    richText:setPosition(10,self:getContentSize().height-10) 

end
function ChatWorldType.checkStrIsEmotion(str)
    for k,v in pairs(EmoteList) do
        if str == v then
            return k
        end
    end
    return false
end

function ChatWorldType.createRechText(textStr, _size, _fontSize)
    --方案 一
    local fontSize = _fontSize or 20
    local rechSize = _size or {width=400, height=80}
    local richText =  ccui.RichText:create();
    richText:ignoreContentAdaptWithSize(false);
    richText:setContentSize(rechSize)

--    richText.width = 400;
--    richText.height = 80;
    richText:setAnchorPoint(cc.p(0, 1));
    --判断一下是否是表情关键字 
    local t = stringSplit(textStr,"%b[]");      
    local str = string.gsub(textStr,"%b[]","-+-")
    local f = string.split(str,"-+-")   
    local isEmotflag =false
    for k,v in pairs(t) do
        if ChatWorldType.checkStrIsEmotion(v) then
            isEmotflag = true
        end
    end
    if not isEmotflag then
        if(textStr~= "") then
            -- local re1 = ccui.RichElementText:create(1, cc.color.RED, 255, str, "Helvetica", 24);
            -- local re1 = ccui.RichElementText:create(1, cc.c3b(0,0,0), 255, textStr, "Helvetica", fontSize);
            local re1 = ccui.RichElementText:create(1, cc.c3b(255,255,255), 255, textStr, "hall/font/fzft.ttf", fontSize);
            richText:pushBackElement(re1);
        end
    else
--        local emote = {}
--        for k,v in pairs(t) do
--            if ChatWorldType.checkStrIsEmotion(v) then
--                table.insert(emote,v)
--            end
--        end
        local tab = {}
         local i =1
        for k=1,#f do
            if f[k]== "" then
                table.insert(tab,t[i])
                i=i+1
            else
                table.insert(tab,f[k])
                if i==k then
                     table.insert(tab,t[i])
                     i=i+1
                end
            end
        end 
        local str = "";
        for k,v in pairs(tab) do 
            if ChatWorldType.checkStrIsEmotion(v) then
                local sprite =  cc.Sprite:create(string.format("hall/image/chat/emotion/chat_face_%d.png",ChatWorldType.checkStrIsEmotion(v)));
                sprite:setAnchorPoint(cc.p(0, 0.5));
                local spriteSize = sprite:getContentSize()
                sprite:setContentSize(cc.size(38,38));
                sprite:setScale(38/spriteSize.height)
                local reimg =  ccui.RichElementCustomNode:create(1, cc.c3b(0,0,0), 255, sprite);
                richText:pushBackElement(reimg); 
            else
                 local re1 = ccui.RichElementText:create(1, cc.c3b(0,0,0), 255, v, "Helvetica", fontSize);
                 richText:pushBackElement(re1);
            end
        end
    end
    return richText
end

return ChatWorldType
--function ChatWorldType:changeLine()
--    local allChildren = self:getChildren();
--    for k=1,#allChildren do
--        local curChild = allChildren[k];
--        if(curChild ~= self:getChildByName("arrow") and curChild != self:ss1){
--            curChild.y = (curChild.posY + self:height - 29);
--        }
--    }
--},

--/*
--    聊天内容是否存在 '['关键字  判别是否表情和文字混排第一步骤
--*/
--function getBracketStr(str) 
--    for k = 1,str.length do
--        local curStr = str[k];
--        if(curStr == '[') then
--            return true;
--        end
--    end
--    return false;
--end

----/*
----    存在中括号  并且获取括号外部的字符 例如  '你好[开心]么'   返回的就是：你好么
----    @ tag 0 代表左侧括号 1代表 右侧括号
----*/
--getBracketExternalStr:function (str, tag) {
--    if(!str) return false;
--    local tempStr = "";
--    for(local k = 0,len = str.length; k < len; ++k){
--        local curStr = str[ (tag == 0) ? k : len - 1 - k ];
--        if(curStr === ( ( tag == 0 ) ? '[': ']' ) ){
--            return tempStr;
--        }else{
--            tempStr += curStr;
--        }
--    }
--    return false;
--},

--/*
--    存在中括号 获取括号之间的表情类别字符 例如  '你好[开心]么'   返回的就是：开心
--    @ tag 0 代表左侧括号 1代表 右侧括号
--*/

--getBrackEmotionStr:function (str, tag) {
--    if(!str) return false;
--    local tempStr = "";
--    for(local k = 0,len = str.length; k < len; ++k){
--        local curStr = str[ (tag == 1) ? k : len - 1 - k ];
--        if(curStr === ( ( tag == 0 ) ? '[': ']' ) ){
--            return tempStr;
--        }else{
--            tempStr += curStr;
--        }
--    }
--    return false;
--},

--/*
--    @param str '难过'
--*/

--function  ChatWorldType:checkStrIsEmotion(str) 
--       for(local k in emotionRes){
--           local keyStr = emotionRes[k].name;
--           if(keyStr === str){
--               return emotionRes[k].url;
--           }
--       }
--       return false
--    },

--    --是否是中文字符
--    funcChina:function(str) {
--        if (/.*[\u4e00-\u9fa5]+.*/.test(str)) {
--            return true;
--        } else {
--            return false;
--        }
--    },

    -- 截取字符，必满乱码的处理；
--    subString:function(str , start , end)
--    {
--        if(cc.isString(str) && str.length > 0)
--        {
--            local len = str.length;
--            local tmp = "";
--            --先把str里的汉字和英文分开
--            local dump = [];
--            local i = 0;
--            while(i < len)
--            {
--                -- if (self:funcChina(str[i]))
--                -- {
--                --     dump.push(str.substr(i, 3));  --1 切割字符的长度
--                --     i = i + 3;
--                -- }
--                -- else
--                -- {
--                    dump.push(str.substr(i,1));
--                    i = i + 1;
--                -- }
--            }
--            local iDumpSize = dump.length;
--            end = end > 0 ? end : iDumpSize;
--            if(start < 0 or start > end)
--                return "";
--            for(i = start; i <= end; i++)
--            {
--                tmp += dump[i-1];
--            }
--            return dump;
--            -- return tmp;
--        }
--        else
--        {
--            ngc.log.info("str is not string\n");
--            return "";
--        }
--    },

--});
