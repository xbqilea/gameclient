local ChatMgr = require("src.app.newHall.chat.ChatMgr")
local ChatWorldType = import("app.newHall.chat.ChatWorldType")
local MessageBoxEx = require("app.hall.base.ui.MessageBoxEx")
local ChatMessageLayer = class("ChatMessageLayer", function()
    return display.newNode()
end)

function ChatMessageLayer:setReturnEvent(callFunc)
    self.mReturnEvent = callFunc
end

function ChatMessageLayer:setTilteText(str)
    self.mTextTitle:setString(str)
end

----------------------------------------
---私有函数
----------------------------------------
function ChatMessageLayer:ctor(info, messageType)
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    self.mInfo = info
    self.mLastMsg = nil
    self.mMessageType = messageType

    local layer = UIAdapter:createNode("hall/csb/chatUI/ChatMessageLayer.csb")
    self:addChild(layer)
    if LuaUtils.isIphoneXDesignResolution() then
        local viewSize = { width = display.size.width, height = display.size.height - 80 }
        layer:setContentSize(viewSize)
    else
        layer:setContentSize(display.size)
    end
    UIAdapter:adapterEx(layer)
    local view = layer:getChildByName("Panel")
    self.mPanel = view
    self.mListView = view:getChildByName("ListViewMessage")
    self.mPanelDown = view:getChildByName("PanelDown")
    self.mTextTitle = UIAdapter:getChildByName(view, "PanelTop/TextTitle")
    self.mImageHead = UIAdapter:getChildByName(view, "PanelTop/PanelHead/ImageHead")
    self.mImageFrame = UIAdapter:getChildByName(view, "PanelTop/PanelHead/ImageFrame")
    self.mImageVIP = UIAdapter:getChildByName(view, "PanelTop/PanelHead/ImageVIP")
    self.mMyItemTmp = view:getChildByName("PanelMyItemTmp")
    self.mTheirItemTmp = view:getChildByName("PanelTheirItemTmp")
    self.mListViewMoji = self.mPanelDown:getChildByName("ListViewMoji")
    self.mMojiContainer = self.mListViewMoji:getChildByName("PanelMojiContainer")

    self.mButtonInput = self.mPanelDown:getChildByName("ButtonInput")
    self.mButtonSend = self.mPanelDown:getChildByName("ButtonSend")
    self.mButtonMoji = self.mPanelDown:getChildByName("ButtonMoji")
    self.mButtonReturn = UIAdapter:getChildByName(view, "PanelTop/ButtonReturn")

    self.mMyItemTmp:setVisible(false)
    self.mTheirItemTmp:setVisible(false)
    self.mListViewMoji:setVisible(false)
    self.mImageHead:setTouchEnabled(true)

    self.mEditBox = UIAdapter:createEditBox({ parent = self.mButtonInput, fontSize = 26, alignRect = { left = 3, right = 3, top = 0, down = 0 } })
    self.mEditBox:registerScriptEditBoxHandler(handler(self, self.onEditBoxClicked))

    UIAdapter:registClickCallBack(self.mButtonSend, handler(self, self.onSendClick))
    UIAdapter:registClickCallBack(self.mButtonMoji, handler(self, self.onMojiClick))
    UIAdapter:registClickCallBack(self.mButtonReturn, handler(self, self.onReturnClick))

    for index = 1, 12 do
        local moji = ccui.ImageView:create("hall/image/chat/emotion/chat_face_" .. index .. ".png")
        moji:ignoreContentAdaptWithSize(true)
        moji:addTo(self.mMojiContainer)
        moji:setTag(index)
        moji:setScale(1.6)

        if index < 7 then
            local x = (750 / 6 * index - 750 / 6 / 2)
            moji:setPosition(cc.p(x, 150))
        else
            local x = (750 / 6 * (index - 6) - 750 / 6 / 2)
            moji:setPosition(cc.p(x, 50))
        end
        moji:setTouchEnabled(true)
        UIAdapter:registClickCallBack(moji, handler(self, self.onSelectMojiClick))
    end

    if self.mMessageType == "Room" then
        self.mImageVIP:setVisible(false)
        self.mImageFrame:setVisible(false)
        self.mImageHead:loadTexture("hall/image/chat/newchat/group_icon.png")
        self.mImageHead:setScale(1.3)
        local msgList = ChatMgr.getInstance():getRoomMsgList(GlobalConf.CHANNEL_ID)
        if msgList then
            for k, item in pairs(msgList) do
                self:addItem(item)
            end
        end
        self:delayTime(0.02, function()
            self.mListView:jumpToBottom()
        end)
        addMsgCallBack(self, M2C_ChatRoom_SendMessage_Ack, handler(self, self.on_ChatRoom_SendMessage_Ack))
        addMsgCallBack(self, M2C_ChatRoom_SendMessage_Nty, handler(self, self.on_ChatRoom_SendMessage_Nty))
        addMsgCallBack(self, M2C_ChatRoom_Login_Nty, handler(self, self.ON_CHATROOM_LOGIN_NTY))
        addMsgCallBack(self, M2C_ChatRoom_Logout_Nty, handler(self, self.ON_CHATROOM_LOGOUT_NTY))
    elseif self.mMessageType == "Friend" then
        self.mImageHead:loadTexture(ToolKit:getHead(info.m_head), ccui.TextureResType.plistType)
        local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", info.m_vip)
        self.mImageFrame:loadTexture(framePath, ccui.TextureResType.plistType)
        local vip_path = string.format("hall/plist/vip/img-vip-v%d.png", info.m_vip)
        self.mImageVIP:loadTexture(vip_path, ccui.TextureResType.plistType)
        local msgList = ChatMgr.getInstance():getFriendMsg(info.m_userid)
        if msgList then
            for k, item in pairs(msgList) do
                self:addItem(item)
            end
        end
        self:delayTime(0.02, function()
            self.mListView:jumpToBottom()
        end)
        UIAdapter:registClickCallBack(self.mImageHead, handler(self, self.onSettingClick))
        addMsgCallBack(self, M2C_Friend_SendMessage_Ack, handler(self, self.on_Friend_SendMessage_Ack))
        addMsgCallBack(self, M2C_Friend_SendMessage_Nty, handler(self, self.on_Friend_SendMessage_Nty))

    elseif self.mMessageType == "Group" then
        self.mImageVIP:setVisible(false)
        self.mImageFrame:setVisible(false)
        self.mImageHead:loadTexture("hall/image/chat/newchat/group_icon.png")
        self.mImageHead:setScale(1.3)
        local msgList = ChatMgr.getInstance():getFriendMsg(info.m_GroupID)
        if msgList then
            for k, item in pairs(msgList) do
                self:addItem(item)
            end
        end
        self:delayTime(0.02, function()
            self.mListView:jumpToBottom()
        end)
        UIAdapter:registClickCallBack(self.mImageHead, handler(self, self.onSettingClick))
        addMsgCallBack(self, M2C_Group_SendMessage_Ack, handler(self, self.on_Group_SendMessage_Ack))
        addMsgCallBack(self, M2C_Group_SendMessage_Nty, handler(self, self.on_Group_SendMessage_Nty))
        addMsgCallBack(self, M2C_Group_EmptyMessage, handler(self, self.on_Group_EmptyMessage))
    end

    self:setPosition(cc.p(display.width, 0))
    self:runAction(cc.MoveTo:create(0.15, cc.p(0, 0)))
end

function ChatMessageLayer:onDestory()
    removeMsgCallBack(self, M2C_ChatRoom_SendMessage_Ack)
    removeMsgCallBack(self, M2C_ChatRoom_SendMessage_Nty)
    removeMsgCallBack(self, M2C_Friend_SendMessage_Ack)
    removeMsgCallBack(self, M2C_Friend_SendMessage_Nty)
    removeMsgCallBack(self, M2C_Group_SendMessage_Ack)
    removeMsgCallBack(self, M2C_Group_SendMessage_Nty)
    removeMsgCallBack(self, M2C_Group_EmptyMessage)
    removeMsgCallBack(self, M2C_ChatRoom_Login_Nty)
    removeMsgCallBack(self, M2C_ChatRoom_Logout_Nty)
end

function ChatMessageLayer:addItem(_cmd, _isJumpToBottom)
    local isSelf = false
    local info = _cmd.m_from_UserInfo or _cmd.m_Member
    if info.m_userid == Player:getAccountID() then
        isSelf = true
    end
    local item = nil
    if isSelf then
        item = self.mMyItemTmp:clone()
    else
        item = self.mTheirItemTmp:clone()
    end
    local itemSize = item:getContentSize()
    item:setVisible(true)
    local imageBack = item:getChildByName("ImageBack")
    local pPanelHead = item:getChildByName("PanelHead")
    local imageHead = pPanelHead:getChildByName("ImageHead")
    local imageFrame = pPanelHead:getChildByName("ImageFrame")
    local imageVip = pPanelHead:getChildByName("ImageVIP")
    local textName = item:getChildByName("TextName")
    local imageBubble = item:getChildByName("ImageBubble")
    local imageLeftBubble = imageBubble:getChildByName("ImageLeftBubble")
    local textMessage = imageBubble:getChildByName("TextValue")
    textMessage:setVisible(false)
    imageBubble:ignoreContentAdaptWithSize(false)
    imageBack:ignoreContentAdaptWithSize(false)
    imageBack:setContentSize(itemSize)

    imageHead:loadTexture(ToolKit:getHead(info.m_head), ccui.TextureResType.plistType)
    local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", info.m_vip)
    imageFrame:loadTexture(framePath, ccui.TextureResType.plistType)
    imageVip:setVisible(false)
    -- if _cmd.m_time == nil then
    --     _cmd.m_time = os.time()
    -- end
    local time = os.date("%H:%M", _cmd.m_time)

    local rechTextMessage = ChatWorldType.createRechText(_cmd.m_message, { width = 450, height = 80 }, 32)
    -- textMessage:setString(_cmd.m_message)
    imageBubble:addChild(rechTextMessage)

    local nameSize = textName:getContentSize()
    local msgSize = rechTextMessage:getRealSize()
    local bubbleSize = { width = msgSize.width + 40, height = msgSize.height + 20 }
    -- if bubbleSize.width < 200 then
    --     bubbleSize.width = 200
    -- end
    imageBubble:setContentSize(bubbleSize)

    local isPaste = false
    if _cmd.m_time then
        if self.mLastMsg and os.date("%H:%M", self.mLastMsg.m_time) == time then
            local lastInfo = self.mLastMsg.m_from_UserInfo or self.mLastMsg.m_Member
            if lastInfo.m_userid == info.m_userid then
                isPaste = true
            end
        end
    end
    if isPaste then
        local itemHeight = bubbleSize.height
        -- if itemHeight > itemSize.height then
        itemSize.height = itemHeight
        -- end
        item:setContentSize(itemSize)
        pPanelHead:setVisible(false)
        imageLeftBubble:setVisible(false)
        textName:setVisible(false)
        imageBubble:setPositionY(itemSize.height)
    else
        local itemHeight = bubbleSize.height + 40
        if itemHeight > itemSize.height then
            itemSize.height = itemHeight
        end
        item:setContentSize(itemSize)
        textName:setPositionY(itemSize.height - 5)
        imageBubble:setPositionY(itemSize.height - 40)
        imageLeftBubble:setPositionY(bubbleSize.height - 8)
    end
    if isSelf then
        if _cmd.m_time then
            textName:setString(time .. ", " .. info.m_name)
        else
            textName:setString(info.m_name)
        end
        rechTextMessage:setPosition(cc.p(bubbleSize.width - 20 - msgSize.width, bubbleSize.height - 10))
        imageLeftBubble:setPositionX(bubbleSize.width)
    else
        textName:setString(info.m_name .. ", " .. time)
        rechTextMessage:setPosition(cc.p(20, bubbleSize.height - 10))
    end

    pPanelHead:setPositionY(itemSize.height - 45)
    self.mLastMsg = _cmd
    self.mListView:pushBackCustomItem(item)
    if _isJumpToBottom then
        self:delayTime(0.02, function()
            self.mListView:jumpToBottom()
        end)
    end
end

function ChatMessageLayer:ButtonShake(btn, delay)
    btn:setEnabled(false)
    btn:stopAllActions()
    btn:runAction(cc.Sequence:create(
    cc.DelayTime:create(delay),
    cc.CallFunc:create(function()
        btn:setEnabled(true)
    end),
    nil))
end

function ChatMessageLayer:delayTime(delay, callBack_)
    self:runAction(cc.Sequence:create(cc.DelayTime:create(delay), cc.CallFunc:create(callBack_), nil))
end

----------------------------------------
---按钮回调
----------------------------------------
function ChatMessageLayer:onEditBoxClicked(strEventName, sender)
    if strEventName == "began" then
        sender:setPlaceHolder("")                                    --光标进入，清空内容/选择全部  
    elseif strEventName == "ended" then
        if sender:getText() then
            sender:setPlaceHolder("请输入文字")
        end                                                        --当编辑框失去焦点并且键盘消失的时候被调用  
    elseif strEventName == "return" then
        print(sender:getText(), "sender")                         --当用户点击编辑框的键盘以外的区域，或者键盘的Return按钮被点击时所调用  
    elseif strEventName == "changed" then
        print(sender:getText(), "sender")                         --输入内容改变时调用   
    end
end

function ChatMessageLayer:onMojiClick(sender)
    local viewSize = self.mPanel:getContentSize()
    local listViewSize = { width = viewSize.width, height = viewSize.height - 187 }
    local container = self.mListView:getInnerContainer()
    local containerPoint = cc.p(container:getPosition())
    if self.mListViewMoji:isVisible() then
        self.mPanelDown:setPositionY(0)
        self.mListViewMoji:setVisible(false)
        self.mListView:setPositionY(107)
        listViewSize.height = viewSize.height - 187
    else
        self.mPanelDown:setPositionY(200)
        self.mListViewMoji:setVisible(true)
        self.mListView:setPositionY(307)
        listViewSize.height = viewSize.height - 387
    end
    self.mListView:setContentSize(listViewSize)
    container:setPosition(containerPoint)
end

function ChatMessageLayer:onSendClick(sender)
    local chatStr = self.mEditBox:getText()
    if chatStr == "" then
        TOAST("请输入文字")
        return
    elseif string.utf8len(chatStr) > 150 then
        TOAST("输入不能超过150字")
        return
    end
    self:ButtonShake(sender, 0.5)
    local sessionID = ChatMgr.getInstance():getSessionID()
    if self.mMessageType == "Room" then
        GlobalIMController:ChatRoom_SendMessage_Req(GlobalConf.CHANNEL_ID, sessionID, chatStr, 1)
    elseif self.mMessageType == "Friend" then
        GlobalIMController:Friend_SendMessage_Req(self.mInfo.m_userid, chatStr, sessionID, 1)
    elseif self.mMessageType == "Group" then
        GlobalIMController:CS_C2M_Group_SendMessage_Req(self.mInfo.m_GroupID, sessionID, chatStr, 1)
    end
end

function ChatMessageLayer:onSelectMojiClick(sender)
    local tag = sender:getTag()
    local str = self.mEditBox:getText()
    str = str .. EmoteList[tag]
    self.mEditBox:setText(str)
end

function ChatMessageLayer:onReturnClick()
    local moveTo = nil
    moveTo = cc.MoveTo:create(0.15, cc.p(display.width, 0))
    self:runAction(cc.Sequence:create(moveTo, cc.CallFunc:create(function()
        self:setVisible(false)
        local callBack = self.mReturnEvent
        self.mReturnEvent = nil
        if callBack then
            callBack(self)
        end
    end), nil))
end

function ChatMessageLayer:onSettingClick()
    if self.mMessageType == "Friend" then
        local pUI = MessageBoxEx.new(nil, "是否清除聊天记录？")
        self:addChild(pUI, 2)
        pUI:addButton("清除", "hall/image/chat/newchat/button_red.png")
        pUI:addButton("取消", "hall/image/chat/newchat/button_blue.png")
        pUI:setButtonCall(function(index)
            if index == 1 then
                -- GlobalIMController:Friend_DelOneFriend_Req(self.mPanelSelectFriend.info.m_userid)
                if self.mMessageType == "Friend" then
                    ChatMgr.getInstance():removeFriendMsg(self.mInfo.m_userid)
                    self:onReturnClick()
                    sendMsg(M2C_Friend_EmptyMessage)
                end
            end
            if index ~= 0 then
                pUI:removeFromParent()
            end
        end)
    elseif self.mMessageType == "Group" then
        local ChatGroupInfoLayer = require("app.newHall.chat.newChat.ChatGroupInfoLayer")
        local ui = ChatGroupInfoLayer.new("Message", self.mInfo)
        self:addChild(ui, 2)
    end
end

----------------------------------------
---消息处理
----------------------------------------
function ChatMessageLayer:on_ChatRoom_SendMessage_Ack(_id, _cmd)
    if _cmd.m_status == 0 then
        -- ChatMgr.getInstance():addRoomMsg(info.m_RoomID,Player:getAccountID(),self.textField_name:getText())
        local item = ChatMgr.getInstance():getLastMsgItem(_cmd.m_RoomID)
        dump(item)
        -- self:showMessage(item)
        -- sendMsg(M2C_ChatRoom_NewMessage, self.mEditBox:getText())
        self.mEditBox:setText("")
    else
        TOAST(_cmd.m_reason)
    end
end

function ChatMessageLayer:on_ChatRoom_SendMessage_Nty(_id, _cmd)
    local item = ChatMgr.getInstance():getLastMsgItem(_cmd.m_RoomID)
    self:addItem(item, true)

end

function ChatMessageLayer:on_Friend_SendMessage_Ack(_id, _cmd)
    if _cmd.m_result == 0 then
        _cmd.m_message = self.mEditBox:getText()
        _cmd.m_time = os.time()
        _cmd.m_from_UserInfo = {}
        _cmd.m_from_UserInfo.m_head = Player:getFaceID()
        _cmd.m_from_UserInfo.m_name = Player:getNickName()
        _cmd.m_from_UserInfo.m_userid = Player:getAccountID()
        _cmd.m_from_UserInfo.m_vip = Player:getVipLevel()
        ChatMgr.getInstance():setFriendMsgList(_cmd, _cmd.m_to_userid, false)
        ChatMgr.getInstance():setNewMessageFlag(false)
        -- self:updateList()
        local item = ChatMgr.getInstance():getLastFriendMsg(_cmd.m_to_userid)
        self:addItem(item, true)
        self.mEditBox:setText("")
    else
        TOAST(_cmd.m_reason)
    end
end

function ChatMessageLayer:on_Friend_SendMessage_Nty(_id, _cmd)
    local info = ChatMgr.getInstance():getLastFriendMsg(_cmd.m_from_UserInfo.m_userid)
    self:addItem(info, true)
end

function ChatMessageLayer:on_Group_SendMessage_Ack(_id, _cmd)
    if _cmd.m_result == 0 then
        _cmd.m_message = self.mEditBox:getText()
        _cmd.m_from_UserInfo = {}
        _cmd.m_from_UserInfo.m_head = Player:getFaceID()
        _cmd.m_from_UserInfo.m_name = Player:getNickName()
        _cmd.m_from_UserInfo.m_userid = Player:getAccountID()
        _cmd.m_from_UserInfo.m_vip = Player:getVipLevel()
        _cmd.m_time = os.time()
        local name = ChatMgr.getInstance():getGroupName(_cmd.m_GroupID)
        print("Friend_SendMessage_Ack")
        print(name)
        _cmd.m_GroupName = name
        -- ChatMgr.getInstance():setFriendMsgList(_cmd, _cmd.m_GroupID, true)
        -- ChatMgr.getInstance():setNewMessageFlag(false)
        -- self:updateChatList()
        -- local item = ChatMgr.getInstance():getLastFriendMsg(info.m_GroupID)
        -- self:showMessage(item)
        self.mEditBox:setText("")
        -- self:addMsgItem(info.m_to_userid)
    else
        TOAST(info.m_reason)
    end
end

function ChatMessageLayer:on_Group_SendMessage_Nty(_id, _cmd)
    local item = ChatMgr.getInstance():getLastFriendMsg(_cmd.m_GroupID)
    self:addItem(item, true)
end

function ChatMessageLayer:on_Group_EmptyMessage(_id, _cmd)
    self.mListView:removeAllItems()
end

function ChatMessageLayer:ON_CHATROOM_LOGIN_NTY(_id, _cmd)
    if self.mMessageType == "Room" then
        local list = ChatMgr.getInstance():getRoomPlayerList(GlobalConf.CHANNEL_ID) or {}
        self:setTitleText("综合聊天群（"..#list.."）")
    end
end
function ChatMessageLayer:ON_CHATROOM_LOGOUT_NTY(_id, _cmd)
    if self.mMessageType == "Room" then
        local list = ChatMgr.getInstance():getRoomPlayerList(GlobalConf.CHANNEL_ID) or {}
        self:setTitleText("综合聊天群（"..#list.."）")
    end
end

return ChatMessageLayer