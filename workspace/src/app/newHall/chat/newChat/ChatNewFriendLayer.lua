local ChatMgr= require("src.app.newHall.chat.ChatMgr")
local ChatNewFriendLayer = class("ChatNewFriendLayer", function()
    return display.newNode()
end)

----------------------------------------
---私有函数
----------------------------------------
function ChatNewFriendLayer:ctor()
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    ChatMgr.getInstance():setAddFriendFlag(false)

    self.mPanel = UIAdapter:createNode("hall/csb/chatUI/ChatUI_PanelNewFriendLayer.csb")
    self:addChild(self.mPanel)
    if LuaUtils.isIphoneXDesignResolution() then
        local viewSize = {width = display.size.width, height = display.size.height - 80}
        self.mPanel:setContentSize(viewSize)
    else
        self.mPanel:setContentSize(display.size)
    end
    UIAdapter:adapterEx(self.mPanel)
    self.mItemTmp = self.mPanel:getChildByName("PanelItemTmp")
    self.mPanelSerchFriend = self.mPanel:getChildByName("PanelSerchFriend")
    self.mListView = self.mPanel:getChildByName("ListView")
    self.mPanelFindTmp = self.mPanel:getChildByName("PanelFindTmp")

    local imageInput = self.mPanelSerchFriend:getChildByName("ImageInput")
    self.mEditBox = UIAdapter:createEditBox({ parent = imageInput, fontSize = 26, alignRect = { left = 3, right = 3, top = 0, down = 0 } })
    self.mEditBox:registerScriptEditBoxHandler(handler(self, self.onEditBoxClicked))

    local buttonReturn = UIAdapter:getChildByName(self.mPanel, "PanelTop/ButtonReturn")
    local buttonSearch = imageInput:getChildByName("ButtonSearch")
    local searchTitle = imageInput:getChildByName("TextTitle")

    self.mPanelFindTmp:setVisible(false)
    self.mItemTmp:setVisible(false)
    self.mPanelSerchFriend:setVisible(false)
    self.mPanelSerchFriend:getChildByName("mask"):setOpacity(0)
    searchTitle:setFontName(FONT_TTF_PATH)
    searchTitle:setColor(cc.c3b(0x1A, 0x1A, 0x1A))
    searchTitle:setFontSize(32)

    self:updateApplyFriendList()

    self:setPosition(cc.p(display.width, 0))
    self:runAction(cc.MoveTo:create(0.15, cc.p(0, 0)))

    UIAdapter:registClickCallBack(self.mPanelSerchFriend, function()
        self.mPanelSerchFriend:setVisible(false)
    end)
    -- UIAdapter:registClickCallBack(buttonAddFriend, handler(self, self.onAddFriendClick))
    UIAdapter:registClickCallBack(buttonReturn, handler(self, self.onReturnClick))
    UIAdapter:registClickCallBack(buttonSearch, handler(self, self.onSearchClick))
    self.mListView:addEventListener(handler(self, self.onListViewCall))

    addMsgCallBack(self, M2C_Friend_AddFriend_Ack, handler(self, self.on_Friend_AddFriend_Ack))
    addMsgCallBack(self, M2C_Friend_AddFriend_Nty, handler(self, self.on_Friend_AddFriend_Nty))
    addMsgCallBack(self, M2C_Friend_AddFriend_Final_Ack, handler(self, self.on_Friend_AddFriend_Final_Ack))
end

function ChatNewFriendLayer:onDestory()
    removeMsgCallBack(self, M2C_Friend_AddFriend_Ack)
    removeMsgCallBack(self, M2C_Friend_AddFriend_Nty)
    removeMsgCallBack(self, M2C_Friend_AddFriend_Final_Ack)
end

function ChatNewFriendLayer:updateApplyFriendList()
    local playerList = ChatMgr.getInstance():getApplyFriendList()
    if playerList == nil then
        return
    end
    self.mListView:removeAllItems()
    local findFriendItem = self.mPanelFindTmp:clone()
    findFriendItem:setVisible(true)
    findFriendItem:setTag(-1)
    findFriendItem:setName("FindFriendItem")
    local itemSize = self.mPanelFindTmp:getContentSize()
    local back = findFriendItem:getChildByName("ImageFindBack")
    back:ignoreContentAdaptWithSize(false)
    back:setContentSize(itemSize)
    self.mListView:pushBackCustomItem(findFriendItem)

    for k, v in pairs(playerList) do
        self:addApplyFriend(v.m_user_info1)
    end
    self:delayTime(0.02, function()
        self.mListView:jumpToBottom()
    end)
end

function ChatNewFriendLayer:removeApplyFriend(userId)
    ChatMgr.getInstance():removeApplyFriend(userId)
    for k, v in pairs(self.mListView:getItems()) do
        if v.info and v.info.m_userid == userId then
            self.mListView:removeItem(k - 1)
            return
        end
    end
end

function ChatNewFriendLayer:addApplyFriend(info)
    local item = self.mItemTmp:clone()
    item:setVisible(true)
    item:setTag(-1)
    item.info = info

    local itemSize = self.mItemTmp:getContentSize()
    local back = item:getChildByName("ImageBack")
    back:ignoreContentAdaptWithSize(false)
    back:setContentSize(itemSize)
    local imageHead = UIAdapter:getChildByName(item, "PanelHead/ImageHead")
    local imageFrame = UIAdapter:getChildByName(item, "PanelHead/ImageFrame")
    local imageVip = UIAdapter:getChildByName(item, "PanelHead/ImageVIP")
    local textName = item:getChildByName("TextName")

    local buttonAdd = item:getChildByName("ImageAdd")
    local buttonDel = item:getChildByName("ImageDel")

    local str = ToolKit:getHead(info.m_head)
    imageHead:loadTexture(str, ccui.TextureResType.plistType)
    local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", info.m_vip)
    imageFrame:loadTexture(framePath, ccui.TextureResType.plistType)
    local vip_path = string.format("hall/plist/vip/img-vip-v%d.png", info.m_vip)
    imageVip:loadTexture(vip_path, ccui.TextureResType.plistType)
    textName:setString(info.m_name)

    buttonAdd.m_userid = info.m_userid
    buttonDel.m_userid = info.m_userid

    buttonAdd:setTouchEnabled(true)
    buttonDel:setTouchEnabled(true)

    buttonAdd:addTouchEventListener(handler(self, self.onAgreeClick))
    buttonDel:addTouchEventListener(handler(self, self.onRefuseClick))

    self.mListView:pushBackCustomItem(item)
end

function ChatNewFriendLayer:delayTime(delay, callBack_)
    self:runAction(cc.Sequence:create(cc.DelayTime:create(delay), cc.CallFunc:create(callBack_), nil))
end

----------------------------------------
---按钮回调
----------------------------------------
function ChatNewFriendLayer:onEditBoxClicked(strEventName, sender)
    if strEventName == "began" then
        sender:setPlaceHolder("")                                    --光标进入，清空内容/选择全部  
    elseif strEventName == "ended" then
        if sender:getText() then
            sender:setPlaceHolder("请输入玩家ID")
        end                                                        --当编辑框失去焦点并且键盘消失的时候被调用  
    elseif strEventName == "return" then
        print(sender:getText(), "sender")                         --当用户点击编辑框的键盘以外的区域，或者键盘的Return按钮被点击时所调用  
    elseif strEventName == "changed" then
        print(sender:getText(), "sender")                         --输入内容改变时调用   
    end
end

function ChatNewFriendLayer:onReturnClick()
    local moveTo = cc.MoveTo:create(0.15, cc.p(display.width, 0))
    self:runAction(cc.Sequence:create(moveTo, cc.CallFunc:create(function()
        self:removeFromParent()
    end), nil))
end

function ChatNewFriendLayer:onSearchClick()
    local chatStr = self.mEditBox:getText()
    if chatStr == "" then
        TOAST("请输入玩家ID")
        return
    end
    GlobalIMController:Friend_AddFriend_Req(tonumber(chatStr))
end

function ChatNewFriendLayer:onAgreeClick(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        GlobalIMController:AddFriend_Final_Req(sender.m_userid, 0)
    end
end
function ChatNewFriendLayer:onRefuseClick(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        GlobalIMController:AddFriend_Final_Req(sender.m_userid, 1)
    end
end

function ChatNewFriendLayer:onListViewCall(taget, eventType)
    if eventType ~= 1 then
        return
    end

    local index = self.mListView:getCurSelectedIndex()
    local item = self.mListView:getItem(index)
    if item then
        if item:getName() == "FindFriendItem" then
            self.mPanelSerchFriend:setVisible(true)
        end
    end
end
----------------------------------------
---消息处理
----------------------------------------
function ChatNewFriendLayer:on_Friend_AddFriend_Ack(_id, info)
    if info.m_result == 0 then
        TOAST("申请成功")
        self.mEditBox:setText("")
        self.mEditBox:setPlaceHolder("请输入玩家ID")
    else
        TOAST(info.m_reason)
    end
end

function ChatNewFriendLayer:on_Friend_AddFriend_Nty(_id, info)
    self:updateApplyFriendList()
end

function ChatNewFriendLayer:on_Friend_AddFriend_Final_Ack(_id, info)
    dump(info)
    -- if info.m_result == 0 then
    --     local player = ChatMgr.getInstance():getFriendById(info.m_userid)
    --     self:addFriend(player)
    -- end
    self:removeApplyFriend(info.m_userid)
end

return ChatNewFriendLayer