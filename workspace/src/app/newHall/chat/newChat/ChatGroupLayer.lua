local ChatMgr = require("src.app.newHall.chat.ChatMgr")
----------------------------------------
---ChatGroupLayer
----------------------------------------
local ChatGroupLayer = class("ChatGroupLayer", function()
    return display.newNode()
end)

----------------------------------------
---私有函数
----------------------------------------
function ChatGroupLayer:ctor(info)
    self.mPanel = UIAdapter:createNode("hall/csb/chatUI/ChatUI_PanelGroupLayer.csb")
    self:addChild(self.mPanel)
    if LuaUtils.isIphoneXDesignResolution() then
        local viewSize = { width = display.size.width, height = display.size.height - 80 }
        self.mPanel:setContentSize(viewSize)
    else
        self.mPanel:setContentSize(display.size)
    end
    UIAdapter:adapterEx(self.mPanel)
    -- self.mPanel:setContentSize(display.size)
    self.mListView = self.mPanel:getChildByName("ListView")
    self.mPanelFindTmp = self.mPanel:getChildByName("PanelFindTmp")
    self.mItemTmp = self.mPanel:getChildByName("PanelItemTmp")

    self.mPanelFindTmp:setVisible(false)
    self.mItemTmp:setVisible(false)

    local buttonReturn = UIAdapter:getChildByName(self.mPanel, "PanelTop/ButtonReturn")

    ToolKit:registDistructor(self, handler(self, self.onDestory))
    self.mListView:addEventListener(handler(self, self.onListViewCall))
    UIAdapter:registClickCallBack(buttonReturn, handler(self, self.onReturnClick))

    self:updateList()

    self:setPosition(cc.p(display.width, 0))
    self:runAction(cc.MoveTo:create(0.15, cc.p(0, 0)))

    addMsgCallBack(self, M2C_Group_DeleteGroup_Ack, handler(self, self.on_Group_DeleteGroup_Ack))
    addMsgCallBack(self, M2C_Group_ExitGroup_Ack, handler(self, self.on_Group_ExitGroup_Ack))
    addMsgCallBack(self, M2C_Group_ReadAllGroup_Ack, handler(self, self.on_Group_ReadAllGroup_Ack))
    addMsgCallBack(self, M2C_Group_CreateGroup_Ack, handler(self, self.on_Group_CreateGroup_Ack))

    GlobalIMController:Group_ReadAllGroup_Req()
end

function ChatGroupLayer:onDestory()
    removeMsgCallBack(self, M2C_Group_DeleteGroup_Ack)
    removeMsgCallBack(self, M2C_Group_ExitGroup_Ack)
    removeMsgCallBack(self, M2C_Group_ReadAllGroup_Ack)
    removeMsgCallBack(self, M2C_Group_CreateGroup_Ack)
end

function ChatGroupLayer:addItem(info)
    local item = self.mItemTmp:clone()
    item.info = info
    item:setVisible(true)
    local itemSize = item:getContentSize()
    local panelHead = item:getChildByName("PanelHead")
    local imageHead = panelHead:getChildByName("ImageHead")
    local imageHD = panelHead:getChildByName("ImageHD")
    local textName = item:getChildByName("TextName")
    local imageBack = item:getChildByName("ImageBack")

    imageBack:ignoreContentAdaptWithSize(false)
    imageBack:setContentSize(itemSize)

    imageHD:setVisible(false)
    imageHead:loadTexture("hall/image/chat/newchat/group_icon.png")
    imageHead:setScale(1.6)
    textName:setString(info.m_GroupName)

    self.mListView:pushBackCustomItem(item)
end

function ChatGroupLayer:removeItem(info)
end

function ChatGroupLayer:updateList()
    local playerList = ChatMgr.getInstance():getGroupList()
    if playerList == nil then
        return
    end
    self.mListView:removeAllItems()

    local findFriendItem = self.mPanelFindTmp:clone()
    findFriendItem:setVisible(true)
    findFriendItem:setTag(-1)
    findFriendItem:setName("FindFriendItem")
    local itemSize = self.mPanelFindTmp:getContentSize()
    local back = findFriendItem:getChildByName("ImageFindBack")
    back:ignoreContentAdaptWithSize(false)
    back:setContentSize(itemSize)
    self.mListView:pushBackCustomItem(findFriendItem)

    for k, v in pairs(playerList) do
        self:addItem(v)
    end

    self:delayTime(0.02, function()
        self.mListView:jumpToBottom()
    end)
end

function ChatGroupLayer:delayTime(delay, callBack_)
    self:runAction(cc.Sequence:create(cc.DelayTime:create(delay), cc.CallFunc:create(callBack_), nil))
end

----------------------------------------
---按钮回调
----------------------------------------
function ChatGroupLayer:onReturnClick()
    local moveTo = cc.MoveTo:create(0.15, cc.p(display.width, 0))
    self:runAction(cc.Sequence:create(moveTo, cc.CallFunc:create(function()
        self:removeFromParent()
    end), nil))
end

function ChatGroupLayer:onListViewCall(taget, eventType)
    if eventType ~= 1 then
        return
    end

    local index = self.mListView:getCurSelectedIndex()
    local item = self.mListView:getItem(index)
    if nil == item then
        return
    end

    local name = item:getName()
    if name == "FindFriendItem" then
        local ui = require("app.newHall.chat.newChat.ChatCreateGroupLayer").new()
        self:addChild(ui)
    else
        local ChatGroupInfoLayer = require("app.newHall.chat.newChat.ChatGroupInfoLayer")
        local ui = ChatGroupInfoLayer.new("Group", item.info)
        self:addChild(ui)
    end
end
----------------------------------------
---消息处理
----------------------------------------
function ChatGroupLayer:on_Group_DeleteGroup_Ack(_id, _cmd)
    self:delayTime(0.02, function()
        self:updateList()
    end)
end

function ChatGroupLayer:on_Group_ExitGroup_Ack(_id, _cmd)
    self:delayTime(0.02, function()
        self:updateList()
    end)
end

function ChatGroupLayer:on_Group_ReadAllGroup_Ack(_id, _cmd)
    self:delayTime(0.02, function()
        self:updateList()
    end)
end

function ChatGroupLayer:on_Group_CreateGroup_Ack(_id, _cmd)
    self:delayTime(0.02, function()
        self:updateList()
    end)
end

return ChatGroupLayer