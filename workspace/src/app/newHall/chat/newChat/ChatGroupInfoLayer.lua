local ChatMgr = require("src.app.newHall.chat.ChatMgr")
local ChatMessageLayer = require("app.newHall.chat.newChat.ChatMessageLayer")
local MessageBoxEx = require("app.hall.base.ui.MessageBoxEx")

----------------------------------------
---ChatGroupInfoLayer
----------------------------------------
local ChatGroupInfoLayer = class("ChatGroupInfoLayer", function()
    return display.newNode()
end)

----------------------------------------
---私有函数
----------------------------------------
function ChatGroupInfoLayer:ctor(type, info)
    self.mType = type
    self.mInfo = info
    local viewSize = display.size
    self.mPanel = UIAdapter:createNode("hall/csb/chatUI/ChatGroupInfoLayer.csb")
    self:addChild(self.mPanel)
    if LuaUtils.isIphoneXDesignResolution() then
        local viewSize = {width = display.size.width, height = display.size.height - 80}
        self.mPanel:setContentSize(viewSize)
    else
        self.mPanel:setContentSize(display.size)
    end
    UIAdapter:adapterEx(self.mPanel)

    local mask = self.mPanel:getChildByName("BackMask")
    local imageBack = self.mPanel:getChildByName("ImageView")
    self.mListView = imageBack:getChildByName("listview_delfriend")
    self.mTextTitle = imageBack:getChildByName("text_groupname")
    self.mTextMemberCount = imageBack:getChildByName("text_num")
    self.mGroupMemberTmp = self.mPanel:getChildByName("panel_groupCell")
    self.mAddGroupTmp = self.mPanel:getChildByName("panel_addgroup")

    local buttonClose = imageBack:getChildByName("ButtonClose")
    self.mButtonLeft = imageBack:getChildByName("ButtonSendMessage")
    self.mButtonRight = imageBack:getChildByName("ButtonRelease")
    

    self.mGroupMemberTmp:setVisible(false)
    self.mAddGroupTmp:setVisible(false)
    mask:setOpacity(0)


    if self.mType == "Message" then
        self.mButtonLeft:setTitleText("清空记录")
        self.mButtonRight:setVisible(false)
        self.mButtonLeft:setPositionX(imageBack:getContentSize().width / 2)
    else
        self.mButtonLeft:setTitleText("发送消息")
    end

    ToolKit:registDistructor(self, handler(self, self.onDestory))
    UIAdapter:registClickCallBack(mask, handler(self, self.onCloseViewClick))
    UIAdapter:registClickCallBack(buttonClose, handler(self, self.onCloseViewClick))
    UIAdapter:registClickCallBack(self.mButtonLeft, handler(self, self.onLeftClick))
    UIAdapter:registClickCallBack(self.mButtonRight, handler(self, self.onRightClick))

    addMsgCallBack(self, M2C_Group_ReadGroup_Ack, handler(self, self.on_Group_ReadGroup_Ack))
    addMsgCallBack(self, M2C_Group_DelMemberFromGroup_Ack, handler(self, self.on_Group_DelMemberFromGroup_Ack))
    addMsgCallBack(self, M2C_Group_DelMemberFromGroup_Nty, handler(self, self.on_Group_DelMemberFromGroup_Nty))
    addMsgCallBack(self, M2C_Group_DeleteGroup_Ack, handler(self, self.on_Group_DeleteGroup_Ack))
    addMsgCallBack(self, M2C_Group_ExitGroup_Ack, handler(self, self.on_Group_ExitGroup_Ack))

    GlobalIMController:Group_ReadGroup_Req(self.mInfo.m_GroupID)
end

function ChatGroupInfoLayer:onDestory()
    removeMsgCallBack(self, M2C_Group_ReadGroup_Ack)
    removeMsgCallBack(self, M2C_Group_DelMemberFromGroup_Ack)
    removeMsgCallBack(self, M2C_Group_DelMemberFromGroup_Nty)
    removeMsgCallBack(self, M2C_Group_DeleteGroup_Ack)
    removeMsgCallBack(self, M2C_Group_ExitGroup_Ack)
end

function ChatGroupInfoLayer:updateMemberList(data)
    self.mListView:removeAllItems()
    if self.m_flag and self.mType ~= "Message" then
        local item = self.mAddGroupTmp:clone()
        item:setVisible(true)
        self.mListView:pushBackCustomItem(item)
        item:addTouchEventListener(handler(self, self.onAddMemberClick))
    end
    for k, v in pairs(data) do
        if v.m_userid ~= Player:getAccountID() then
            self:addGroupMember(v)
        end
    end
    self:delayTime(0.02, function()
        self.mListView:jumpToBottom()
    end)
end

function ChatGroupInfoLayer:addGroupMember(player, _isJumpToBottom)
    local t = self.mGroupMemberTmp:clone()
    t:setVisible(true)
    local image_head = t:getChildByName("image_head")
    local str = ToolKit:getHead(player.m_head)
    image_head:loadTexture(str, ccui.TextureResType.plistType);

    local image_frame = t:getChildByName("image_frame")
    local image_vip = t:getChildByName("image_vip")
    local text_name = t:getChildByName("text_name")
    local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", player.m_vip)
    image_frame:loadTexture(framePath, ccui.TextureResType.plistType)
    local vip_path = string.format("hall/plist/vip/img-vip-v%d.png", player.m_vip)
    image_vip:loadTexture(vip_path, ccui.TextureResType.plistType)
    image_vip:ignoreContentAdaptWithSize(true)
    image_head:setScale(0.5)
    image_frame:setScale(0.5)
    image_vip:setScale(0.5)
    text_name:setString(player.m_name)
    local image_del = t:getChildByName("image_del")
    t.m_userid = player.m_userid
    t.m_head = player.m_head
    t.m_vip = player.m_vip
    t.m_name = player.m_name
    local image_select = t:getChildByName("select")
    local image_unselect = t:getChildByName("unselect")
    image_select:setVisible(false)
    image_unselect:setVisible(false)
    image_del.m_userid = player.m_userid
    if self.m_flag then
        image_del:setVisible(true)
        image_del:addTouchEventListener(handler(self, self.onDelEvent))
    end
    self.mListView:pushBackCustomItem(t)

    if _isJumpToBottom then
        self:delayTime(0.02, function()
            self.mListView:jumpToBottom()
        end)
    end
end

function ChatGroupInfoLayer:delayTime(delay, callBack_)
    self:runAction(cc.Sequence:create(cc.DelayTime:create(delay), cc.CallFunc:create(callBack_), nil))
end

----------------------------------------
---按钮回调
----------------------------------------
function ChatGroupInfoLayer:onCloseViewClick()
    self:removeFromParent()
end

function ChatGroupInfoLayer:onLeftClick()
    if self.mType == "Message" then
        -- self.mButtonLeft:setTitleText("清空记录")
        local pUI = MessageBoxEx.new(nil, "是否确定清楚聊天记录？")
        self:getParent():addChild(pUI, 2)
        pUI:addButton("清除", "hall/image/chat/newchat/button_red.png")
        pUI:addButton("取消", "hall/image/chat/newchat/button_blue.png")
        pUI:setButtonCall(function(index)
            if index == 1 then
                ChatMgr.getInstance():removeFriendMsg(self.mInfo.m_GroupID)
                sendMsg(M2C_Group_EmptyMessage)
            end
            if index ~= 0 then
                pUI:removeFromParent()
            end
        end)
    else
        -- self.mButtonLeft:setTitleText("发送消息")
        local messageLayer = ChatMessageLayer.new(self.mInfo, "Group")
        self:getParent():addChild(messageLayer, 2)
        messageLayer:setReturnEvent(function(sender)
            sender:removeFromParent()
        end)
        -- local num = table.nums(self.m_curGroupMember) - 1
        messageLayer:setTilteText(self.mInfo.m_GroupName)
        self:removeFromParent()
    end
end

function ChatGroupInfoLayer:onRightClick()
    if self.m_flag then
        GlobalIMController:Group_DeleteGroup_Req(self.mInfo.m_GroupID)
    else
        GlobalIMController:Group_ExitGroup_Req(self.mInfo.m_GroupID)
    end
end

function ChatGroupInfoLayer:onDelEvent(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        local pUI = MessageBoxEx.new(nil, "是否确定踢出群聊？")
        self:addChild(pUI, 2)
        pUI:addButton("确定", "hall/image/chat/newchat/button_red.png")
        pUI:addButton("取消", "hall/image/chat/newchat/button_blue.png")
        pUI:setButtonCall(function(index)
            if index == 1 then
                self.m_curGroupDelId = sender.m_userid
                local sessionID = ChatMgr.getInstance():getSessionID()
                GlobalIMController:Group_DelMemberFromGroup_Req(self.mInfo.m_GroupID, sessionID, { sender.m_userid })
            end
            if index ~= 0 then
                pUI:removeFromParent()
            end
        end)
    end
end

function ChatGroupInfoLayer:onAddMemberClick(sender, eventType)
    if ccui.TouchEventType.ended == eventType then
        local ui = require("app.newHall.chat.newChat.ChatGroupAddMemberLayer").new(self.mInfo.m_GroupID, self.m_curGroupMember)
        self:getParent():addChild(ui, 2)
        self:onCloseViewClick()
    end
end

----------------------------------------
---消息处理
----------------------------------------
function ChatGroupInfoLayer:on_Group_ReadGroup_Ack(_id, _cmd)
    local num = table.nums(_cmd.m_Member) - 1

    self.mTextTitle:setString(self.mInfo.m_GroupName)
    self.mTextMemberCount:setString(num .. "位参与者")

    self.m_flag = false
    if self.mInfo.m_userid == Player:getAccountID() then
        self.mButtonRight:setTitleText("删除并退出")
        self.m_flag = true
    else
        self.mButtonRight:setTitleText("退出")
    end
    self.m_curGroupMember = _cmd.m_Member
    self:updateMemberList(_cmd.m_Member)
end

function ChatGroupInfoLayer:on_Group_DelMemberFromGroup_Ack(_id, _cmd)
    for k, v in pairs(self.mListView:getItems()) do
        if v.m_userid == self.m_curGroupDelId then
            self.mListView:removeItem(k - 1)
        end
    end
    for k, v in pairs(self.m_curGroupMember) do
        if v.m_userid == self.m_curGroupDelId then
            table.remove(self.m_curGroupMember, k)
        end
    end
end

function ChatGroupInfoLayer:on_Group_DelMemberFromGroup_Nty(_id, _cmd)
end

function ChatGroupInfoLayer:on_Group_DeleteGroup_Ack(_id, _cmd)
    self:removeFromParent()
end

function ChatGroupInfoLayer:on_Group_ExitGroup_Ack(_id, _cmd)
    self:removeFromParent()
end

return ChatGroupInfoLayer