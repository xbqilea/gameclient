local ChatCreateGroupLayer = class("ChatCreateGroupLayer", function()
    return display.newNode()
end)

----------------------------------------
---私有函数
----------------------------------------
function ChatCreateGroupLayer:ctor()
    local viewSize = display.size
    self.mPanel = UIAdapter:createNode("hall/csb/chatUI/ChatCreateGrouplayer.csb")
    self:addChild(self.mPanel)
    if LuaUtils.isIphoneXDesignResolution() then
        local viewSize = {width = display.size.width, height = display.size.height - 80}
        self.mPanel:setContentSize(viewSize)
    else
        self.mPanel:setContentSize(display.size)
    end
    UIAdapter:adapterEx(self.mPanel)
    local mask = self.mPanel:getChildByName("BackMask")
    local imageBack = self.mPanel:getChildByName("ImageView")
    local buttonClose = imageBack:getChildByName("button_groupclose")
    local buttonNext = imageBack:getChildByName("button_groupnext")
    local inputNode = imageBack:getChildByName("image_input")

    mask:setOpacity(0)

    self.mEditBox = UIAdapter:createEditBox({ parent = inputNode, fontSize = 26, placestr = "创建群组", alignRect = { left = 15, right = 15, top = 0, down = 0 } })

    ToolKit:registDistructor(self, handler(self, self.onDestory))
    UIAdapter:registClickCallBack(buttonClose, handler(self, self.onCloseClick))
    UIAdapter:registClickCallBack(mask, handler(self, self.onCloseClick))
    UIAdapter:registClickCallBack(buttonNext, handler(self, self.onNextClick))
    self.mEditBox:registerScriptEditBoxHandler(handler(self, self.onEditBoxClicked))

    addMsgCallBack(self, M2C_Group_CreateGroup_Ack, handler(self, self.on_Group_CreateGroup_Ack))
end

function ChatCreateGroupLayer:onDestory()
    removeMsgCallBack(self, M2C_Group_CreateGroup_Ack)
end

function ChatCreateGroupLayer:delayTime(delay, callBack_)
    self:runAction(cc.Sequence:create(cc.DelayTime:create(delay), cc.CallFunc:create(callBack_), nil))
end
----------------------------------------
---按钮回调
----------------------------------------
function ChatCreateGroupLayer:onCloseClick()
    self:delayTime(0.02, function()
        self:removeFromParent()
    end)
end

function ChatCreateGroupLayer:onNextClick()
    if self.mEditBox:getText() == "" then
        TOAST("请输入群组名称")
    else
        GlobalIMController:Group_CreateGroup_Req(self.mEditBox:getText())
    end
end

function ChatCreateGroupLayer:onEditBoxClicked(strEventName, sender)
    if strEventName == "began" then
        sender:setPlaceHolder("")                                    --光标进入，清空内容/选择全部  
    elseif strEventName == "ended" then
        if sender:getText() then
            sender:setPlaceHolder("创建群组")
        end                                                        --当编辑框失去焦点并且键盘消失的时候被调用  
    elseif strEventName == "return" then
        print(sender:getText(), "sender")                         --当用户点击编辑框的键盘以外的区域，或者键盘的Return按钮被点击时所调用  
    elseif strEventName == "changed" then
        --输入内容改变时调用
    end
end

----------------------------------------
---消息处理
----------------------------------------
function ChatCreateGroupLayer:on_Group_CreateGroup_Ack(_id, _cmd)
    local ui = require("app.newHall.chat.newChat.ChatGroupAddMemberLayer").new(_cmd.m_GroupID, {})
    self:getParent():addChild(ui, 2)
    self:onCloseClick()
end

return ChatCreateGroupLayer