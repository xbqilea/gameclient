local ChatLayerEx = class("ChatLayerEx", function ()
    return display.newLayer()
end)
----------------------------------------
---公开函数
----------------------------------------
function ChatLayerEx:setExitEvent(callfunc)
    self.mExitCall = callfunc
end

function ChatLayerEx:setTitleText(text)
    self.mTextTitle:setString(text)
end

----------------------------------------
---私有函数
----------------------------------------
function ChatLayerEx:ctor(direction)
    self.mDirection = direction or 0
    self.mViews = {}
    self.mCurrentView = nil
    self.mViewCinfig = {
        [1]={button="ButtonMessage", script="ChatMessageView"},
        [2]={button="ButtonFriend", script="ChatAddressBook"},
        [3]={button="ButtonOnliePlayer", script="ChatOnlineView"},
    }
    ToolKit:registDistructor(self, handler(self, self.onDestory))


    self.mRoot = UIAdapter:createNode("hall/csb/chatUI/ChatLayerEx.csb")
    self.mRoot:setTouchEnabled(false)
    self.mRoot:addTo(self)
    self.mRoot:setContentSize(display.size)
    self.mRoot:getChildByName("mask"):setContentSize(display.size)
    local view = self.mRoot:getChildByName("center")
    if LuaUtils.isIphoneXDesignResolution() then
        local viewSize = {width = display.size.width, height = display.size.height - 80}
        view:setContentSize(viewSize)
        view:setPositionY(30)
    else
        view:setContentSize(display.size)
        view:setPositionY(0)
    end
    UIAdapter:adapterEx(view)

    self.mPanel = view
    self.mTextTitle = UIAdapter:getChildByName(self.mPanel, "PanelTop/TextTitle")
    self.mButtonReturn = UIAdapter:getChildByName(self.mPanel, "PanelTop/ButtonReturn")

    UIAdapter:registClickCallBack(self.mButtonReturn, handler(self, self.onReturnClick))
    for key, v in pairs(self.mViewCinfig) do
        local button = UIAdapter:getChildByName(self.mPanel, "PanelDown/"..v.button)
        button:setTag(key)
        UIAdapter:registClickCallBack(button, handler(self, self.onSelectViewClick))
    end
    self:showView(1)

    self.mRoot:setPosition(cc.p(display.width, 0))
    self.mRoot:runAction(cc.MoveTo:create(0.15, cc.p(0, 0)))
end

function ChatLayerEx:onDestory()

end

function ChatLayerEx:showView(tag)
    if self.mCurrentView == tag then
        return
    end
    if self.mCurrentView ~= nil then
        local config = self.mViewCinfig[self.mCurrentView]
        self.mViews[config.script]:setVisible(false)
        local button = UIAdapter:getChildByName(self.mPanel, "PanelDown/"..config.button)
        local buttonSize = button:getContentSize()
        button:loadTextures("hall/image/image_opacity.png","hall/image/image_opacity.png","hall/image/image_opacity.png")
        button:ignoreContentAdaptWithSize(false)
        button:setContentSize(buttonSize)
        button:setEnabled(true)
    end
    self.mCurrentView = tag
    local config = self.mViewCinfig[self.mCurrentView]
    local view = self.mViews[config.script]
    if nil == view then
        local script = require("app/newHall/chat/newChat/Script/"..config.script)
        view = script.new(self)
        view.mChatLayer = self
        view:addTo(self.mPanel)
        self.mViews[config.script] = view
    end
    local button = UIAdapter:getChildByName(self.mPanel, "PanelDown/"..config.button)
    local buttonSize = button:getContentSize()
    button:loadTextures("hall/image/chat/newchat/botton.png", "hall/image/chat/newchat/botton.png", "hall/image/chat/newchat/botton.png")
    button:ignoreContentAdaptWithSize(false)
    button:setContentSize(buttonSize)
    button:setEnabled(false)
    self.mTextTitle:setString(view:getTitleName())
    view:setVisible(true)
end

----------------------------------------
---按钮回调
----------------------------------------
function ChatLayerEx:onReturnClick()
    if self.mExitCall then
        self.mExitCall()
    end
    -- self:removeFromParent()
end

function ChatLayerEx:onSelectViewClick(sender)
    local tag = sender:getTag()
    self:showView(tag)
end
----------------------------------------
---消息处理
----------------------------------------


return ChatLayerEx