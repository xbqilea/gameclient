local ChatMgr = require("src.app.newHall.chat.ChatMgr")
local ChatGroupAddMemberLayer = class("ChatGroupAddMemberLayer", function()
    return display.newNode()
end)


----------------------------------------
---私有函数
----------------------------------------
function ChatGroupAddMemberLayer:ctor(groupId, members)
    self.mGroupID = groupId
    self.mCurMember = members

    local viewSize = display.size
    self.mPanel = UIAdapter:createNode("hall/csb/chatUI/ChatGroupAddMemberLayer.csb")
    self:addChild(self.mPanel)
    if LuaUtils.isIphoneXDesignResolution() then
        local viewSize = {width = display.size.width, height = display.size.height - 80}
        self.mPanel:setContentSize(viewSize)
    else
        self.mPanel:setContentSize(display.size)
    end
    UIAdapter:adapterEx(self.mPanel)

    local imageBack = self.mPanel:getChildByName("ImageView")
    local mask = self.mPanel:getChildByName("BackMask")
    local panelInput = imageBack:getChildByName("PanelInput")
    self.mListView = imageBack:getChildByName("listview_groupfriend")
    self.mItemTmp = self.mPanel:getChildByName("panel_groupCell")

    self.mEditBox = UIAdapter:createEditBox({ parent = panelInput, fontSize = 26, placestr = "搜索联系人", alignRect = { left = 45, right = 15, top = 0, down = 0 } })

    local buttonFinish = imageBack:getChildByName("button_finish")
    local buttonClose = UIAdapter:getChildByName(self.mPanel, "ImageView/button_groupback")

    self.mItemTmp:setVisible(false)
    mask:setOpacity(0)

    self:updateList()

    ToolKit:registDistructor(self, handler(self, self.onDestory))
    UIAdapter:registClickCallBack(buttonClose, handler(self, self.onCloseClick))
    UIAdapter:registClickCallBack(mask, handler(self, self.onCloseClick))
    UIAdapter:registClickCallBack(buttonFinish, handler(self, self.onFinishClick))
    self.mEditBox:registerScriptEditBoxHandler(handler(self, self.onEditBoxClicked))

    addMsgCallBack(self, M2C_Group_AddFriendToGroup_Ack, handler(self, self.on_Group_AddFriendToGroup_Ack))
    -- addMsgCallBack(self, M2C_Group_AddFriendToGroup_1_Nty, handler(self, self.on_Group_AddFriendToGroup_1_Nty))
end

function ChatGroupAddMemberLayer:onDestory()
    removeMsgCallBack(self, M2C_Group_AddFriendToGroup_Ack)
    -- removeMsgCallBack(self, M2C_Group_AddFriendToGroup_1_Nty)
end

function ChatGroupAddMemberLayer:updateList()
    local playerList = ChatMgr.getInstance():getFriendList()
    if playerList == nil then
        return
    end
    self.mListView:removeAllItems()
    for k, v in pairs(playerList) do
        self:addGroupFriend(v)
    end
end

function ChatGroupAddMemberLayer:addGroupFriend(player)
    local t = self.mItemTmp:clone()
    t:setVisible(true)
    local image_head = t:getChildByName("image_head")
    local str = ToolKit:getHead(player.m_head)
    image_head:loadTexture(str, ccui.TextureResType.plistType);

    local image_frame = t:getChildByName("image_frame")
    local image_vip = t:getChildByName("image_vip")
    local text_name = t:getChildByName("text_name")
    local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", player.m_vip)
    image_frame:loadTexture(framePath, ccui.TextureResType.plistType)
    local vip_path = string.format("hall/plist/vip/img-vip-v%d.png", player.m_vip)
    image_vip:loadTexture(vip_path, ccui.TextureResType.plistType)
    image_vip:ignoreContentAdaptWithSize(true)
    image_head:setScale(0.5)
    image_frame:setScale(0.5)
    image_vip:setScale(0.5)
    text_name:setString(player.m_name)
    t.m_userid = player.m_userid
    t.m_head = player.m_head
    t.m_vip = player.m_vip
    t.m_name = player.m_name
    if self.mCurMember then
        for k, v in pairs(self.mCurMember) do
            if v.m_userid == player.m_userid then
                local image_select = t:getChildByName("select")
                local image_unselect = t:getChildByName("unselect")
                image_select:setVisible(true)
                image_unselect:setVisible(false)
                t:setEnabled(false)
            end
        end
    end
    t:addTouchEventListener(handler(self, self.onSelectEvent))
    self.mListView:pushBackCustomItem(t)
end

function ChatGroupAddMemberLayer:delayTime(delay, callBack_)
    self:runAction(cc.Sequence:create(cc.DelayTime:create(delay), cc.CallFunc:create(callBack_), nil))
end


----------------------------------------
---按钮回调
----------------------------------------
function ChatGroupAddMemberLayer:onCloseClick()
    self:delayTime(0.02, function ()
        self:removeFromParent()
    end)
end


function ChatGroupAddMemberLayer:onFinishClick()
    local groupList = {}
    for k, v in pairs(self.mListView:getItems()) do
        local image_select = v:getChildByName("select")
        if image_select:isVisible() then
            table.insert(groupList, v.m_userid)
        end
    end
    local sessionID = ChatMgr.getInstance():getSessionID()
    GlobalIMController:Group_AddFriendToGroup_Req(self.mGroupID, sessionID, groupList)
end

function ChatGroupAddMemberLayer:onSelectEvent(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        local image_select = sender:getChildByName("select")
        local image_unselect = sender:getChildByName("unselect")
        image_select:setVisible(not image_select:isVisible())
        image_unselect:setVisible(not image_unselect:isVisible())
    end
end

function ChatGroupAddMemberLayer:onEditBoxClicked(strEventName, sender)
    if strEventName == "began" then
        sender:setPlaceHolder("")                                    --光标进入，清空内容/选择全部  
    elseif strEventName == "ended" then
        if sender:getText() then
            sender:setPlaceHolder("搜索联系人")
        end                                                        --当编辑框失去焦点并且键盘消失的时候被调用  
    elseif strEventName == "return" then
        print(sender:getText(), "sender")                         --当用户点击编辑框的键盘以外的区域，或者键盘的Return按钮被点击时所调用  
    elseif strEventName == "changed" then
        local playerList = ChatMgr.getInstance():getFriendList()
        local findList = {}
        for k, v in pairs(playerList) do
            if string.find(v.m_name, sender:getText()) then
                table.insert(findList, v)
            end
        end
        if #findList == 0 then
            self:updateList()
        else
            self.mListView:removeAllItems()
            for k, v in pairs(findList) do
                self:addGroupFriend(v)
            end
        end
        print(sender:getText(), "sender")                         --输入内容改变时调用   
    end
end

----------------------------------------
---消息处理
----------------------------------------
function ChatGroupAddMemberLayer:on_Group_AddFriendToGroup_Ack(_id, _cmd)
    self:onCloseClick()
end

-- function ChatGroupAddMemberLayer:on_Group_AddFriendToGroup_1_Nty(_id, _cmd)
--     ChatMgr.getInstance():setAddGroupFlag(true)
-- end
return ChatGroupAddMemberLayer