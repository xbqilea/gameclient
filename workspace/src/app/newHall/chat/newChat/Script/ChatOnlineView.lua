local ChatMgr= require("src.app.newHall.chat.ChatMgr")
local ChatOnlineView = class("ChatOnlineView", function ()
    return display.newNode()
end)

----------------------------------------
---共有函数
----------------------------------------
function ChatOnlineView:getTitleName()
    local list = ChatMgr.getInstance():getRoomPlayerList(GlobalConf.CHANNEL_ID) or {}
    return "在线玩家（"..#list.."）"
end

----------------------------------------
---私有函数
----------------------------------------
function ChatOnlineView:ctor(chatLayer)
    ToolKit:registDistructor(self, handler(self, self.onDestory))

    self.mPanel = UIAdapter:createNode("hall/csb/chatUI/ChatUI_PanelOnline.csb")
    self.mPanel:setTouchEnabled(false)
    self:addChild(self.mPanel)
    if LuaUtils.isIphoneXDesignResolution() then
        local viewSize = {width = display.size.width, height = display.size.height - 80}
        self.mPanel:setContentSize(viewSize)
    else
        self.mPanel:setContentSize(display.size)
    end
    UIAdapter:adapterEx(self.mPanel)

    self.mListView = self.mPanel:getChildByName("ListView")
    self.mItemTmp = self.mPanel:getChildByName("PanelItemTmp")
    self.mItemTmp:setVisible(false)

    self.mPlayerList = ChatMgr.getInstance():getRoomPlayerList(GlobalConf.CHANNEL_ID) or {}
    self.mListView:removeAllItems()
    self.mCurIndex = 1
    self:onAddPlayers()

    addMsgCallBack(self, M2C_ChatRoom_Login_Nty, handler(self, self.ON_CHATROOM_LOGIN_NTY))
    addMsgCallBack(self, M2C_ChatRoom_Logout_Nty, handler(self, self.ON_CHATROOM_LOGOUT_NTY))

end

function ChatOnlineView:onDestory()
    removeMsgCallBack(self, M2C_ChatRoom_Login_Nty)
    removeMsgCallBack(self, M2C_ChatRoom_Logout_Nty)
end

function ChatOnlineView:onAddPlayers()
    local lastIndex = #self.mListView:getItems() + 1
    local nextIndex = lastIndex + 15
    local playerList = ChatMgr.getInstance():getRoomPlayerList(GlobalConf.CHANNEL_ID) or {}
    if #playerList < nextIndex then
        nextIndex = #playerList
    end
    if lastIndex >= nextIndex then
        return
    end

    for index=lastIndex, nextIndex do
        self:addItem(playerList[index])
    end
    if #playerList > nextIndex then
        self:runAction(cc.Sequence:create(cc.DelayTime:create(0.05), cc.CallFunc:create(function()
            self:onAddPlayers()
        end) ,nil))
    end
end

function ChatOnlineView:addItem(info)
    if nil == info then
        return
    end
    local item = self.mItemTmp:clone()
    item:setVisible(true)
    item.info = info
    local itemSize = self.mItemTmp:getContentSize()
    local back = item:getChildByName("ImageBack")
    back:ignoreContentAdaptWithSize(false)
    back:setContentSize(itemSize)
    local head = UIAdapter:getChildByName(item, "PanelHead/ImageHead")
    local frame = UIAdapter:getChildByName(item, "PanelHead/ImageFrame")
    local vip= UIAdapter:getChildByName(item, "PanelHead/ImageVIP")
    local textName = item:getChildByName("TextName")
    local textMessage = item:getChildByName("TextMessage")
    local imageHD = UIAdapter:getChildByName(item, "PanelHead/ImageHD")
    imageHD:setVisible(false)
    textMessage:setVisible(false)
    textName:setPositionY(itemSize.height/2)
    -- head:setScale(0.5)
    -- frame:setScale(0.5)
    local str =ToolKit:getHead(info.m_head)
    head:loadTexture(str,ccui.TextureResType.plistType)
    local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", info.m_vip)
    frame:loadTexture(framePath, ccui.TextureResType.plistType)
    local vip_path = string.format("hall/plist/vip/img-vip-v%d.png", info.m_vip)
    vip:loadTexture(vip_path, ccui.TextureResType.plistType)
    textName:setString(info.m_name)
    self.mListView:pushBackCustomItem(item)
end

function ChatOnlineView:removeItem(info)
    for k,v in pairs( self.mListView:getItems()) do
        if v.info.m_userid == info.m_userid then
            self.mListView:removeItem(k-1)
        end
    end
end

----------------------------------------
---按钮回调
----------------------------------------
function ChatOnlineView:onAddClick()

end

----------------------------------------
---消息处理
----------------------------------------
function ChatOnlineView:ON_CHATROOM_LOGIN_NTY(_id, _cmd)
    local player = ChatMgr.getInstance():getLastRoomPlayer(_cmd.m_RoomID)
    self:addItem(player)
    self.mChatLayer:setTitleText(self:getTitleName())
end
function ChatOnlineView:ON_CHATROOM_LOGOUT_NTY(_id, _cmd)
    self:removeItem(_cmd)
    self.mChatLayer:setTitleText(self:getTitleName())
end

return ChatOnlineView