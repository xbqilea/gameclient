local ChatMgr = require("src.app.newHall.chat.ChatMgr")
local ChatMessageLayer = require("app.newHall.chat.newChat.ChatMessageLayer")
local MessageBoxEx = require("app.hall.base.ui.MessageBoxEx")
local ChatNewFriendLayer = require("app.newHall.chat.newChat.ChatNewFriendLayer")
local ChatGroupLayer = require("app.newHall.chat.newChat.ChatGroupLayer")
local ChatAddressBook = class("ChatAddressBook", function()
    return display.newNode()
end)

----------------------------------------
---共有函数
----------------------------------------
function ChatAddressBook:getTitleName()
    return "通讯录"
end

----------------------------------------
---私有函数
----------------------------------------
function ChatAddressBook:ctor(chatLayer)
    ToolKit:registDistructor(self, handler(self, self.onDestory))

    self.mTableType = {
        Type_Friend = "Friend",
        Type_Group = "Group",
        Type_NewFriend = "NewFriend",
    }
    self.mCurPage = self.mTableType.Type_Friend

    self.mPanel = UIAdapter:createNode("hall/csb/chatUI/ChatUI_PanelAddressBook.csb")
    self.mPanel:setTouchEnabled(false)
    self:addChild(self.mPanel)
    if LuaUtils.isIphoneXDesignResolution() then
        local viewSize = {width = display.size.width, height = display.size.height - 80}
        self.mPanel:setContentSize(viewSize)
    else
        self.mPanel:setContentSize(display.size)
    end
    UIAdapter:adapterEx(self.mPanel)

    self.mListView = self.mPanel:getChildByName("ListView")
    self.mItemTmp = self.mPanel:getChildByName("PanelItemTmp")
    self.mPanelSelectFriend = self.mPanel:getChildByName("PanelSelectFriend")

    local buttonMaskBack = self.mPanelSelectFriend:getChildByName("MaskBack")
    local buttonDel = UIAdapter:getChildByName(self.mPanelSelectFriend, "Panel/ButtonDel")
    local buttonMessage = UIAdapter:getChildByName(self.mPanelSelectFriend, "Panel/ButtonMessage")

    self.mPanelSelectFriend:setVisible(false)
    self.mItemTmp:setVisible(false)
    buttonMaskBack:setVisible(false)

    self.mListView:addEventListener(handler(self, self.onListViewCall))
    UIAdapter:registClickCallBack(buttonDel, handler(self, self.onDelFriendClick))
    UIAdapter:registClickCallBack(buttonMessage, handler(self, self.onFriendMessageClick))
    UIAdapter:registClickCallBack(self.mPanelSelectFriend, handler(self, self.onHideSelectFriendClick))

    addMsgCallBack(self, M2C_Friend_AddFriend_Ack, handler(self, self.on_Friend_AddFriend_Ack))
    addMsgCallBack(self, M2C_Friend_AddFriend_Nty, handler(self, self.on_Friend_AddFriend_Nty))
    addMsgCallBack(self, M2C_Friend_AddFriend_Final_Ack, handler(self, self.on_Friend_AddFriend_Final_Ack))
    addMsgCallBack(self, M2C_Friend_AddFriend_Final_Nty, handler(self, self.on_Friend_AddFriend_Final_Nty))
    addMsgCallBack(self, M2C_Friend_DelOneFriend_Ack, handler(self, self.on_Friend_DelOneFriend_Ack))
    addMsgCallBack(self, M2C_Friend_GetAllFriendDetails_Ack, handler(self, self.on_Friend_GetAllFriendDetails_Ack))

    self:updateFrindList()
end

function ChatAddressBook:onDestory()
    removeMsgCallBack(self, M2C_Friend_AddFriend_Ack)
    removeMsgCallBack(self, M2C_Friend_AddFriend_Nty)
    removeMsgCallBack(self, M2C_Friend_AddFriend_Final_Ack)
    removeMsgCallBack(self, M2C_Friend_AddFriend_Final_Nty)
    removeMsgCallBack(self, M2C_Friend_DelOneFriend_Ack)
    removeMsgCallBack(self, M2C_Friend_GetAllFriendDetails_Ack)
end

function ChatAddressBook:addItem(info, itemType)
    local item = self.mItemTmp:clone()
    item:setVisible(true)
    item.info = info
    item.itemType = itemType
    local itemSize = self.mItemTmp:getContentSize()
    local back = item:getChildByName("ImageBack")
    back:ignoreContentAdaptWithSize(false)
    back:setContentSize(itemSize)
    local imageHead = UIAdapter:getChildByName(item, "PanelHead/ImageHead")
    local imageFrame = UIAdapter:getChildByName(item, "PanelHead/ImageFrame")
    local imageVip = UIAdapter:getChildByName(item, "PanelHead/ImageVIP")
    local textName = item:getChildByName("TextName")
    local imageHD = UIAdapter:getChildByName(item, "PanelHead/ImageHD")
    imageHD:setVisible(false)
    textName:setPositionY(itemSize.height / 2)

    if item.itemType == self.mTableType.Type_Friend then
        local str = ToolKit:getHead(info.m_head)
        imageHead:loadTexture(str, ccui.TextureResType.plistType)
        local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", info.m_vip)
        imageFrame:loadTexture(framePath, ccui.TextureResType.plistType)
        local vip_path = string.format("hall/plist/vip/img-vip-v%d.png", info.m_vip)
        imageVip:loadTexture(vip_path, ccui.TextureResType.plistType)
        textName:setString(info.m_name)
    elseif item.itemType == self.mTableType.Type_Group then
        item:setName(item.itemType)
        textName:setString("群聊")
        imageFrame:setVisible(false)
        imageVip:setVisible(false)
        imageHead:loadTexture("hall/image/chat/newchat/group_icon.png")
        imageHead:setScale(1.6)
    elseif item.itemType == self.mTableType.Type_NewFriend then
        item:setName(item.itemType)
        textName:setString("新的好友")
        imageFrame:setVisible(false)
        imageVip:setVisible(false)
        imageHead:loadTexture("hall/image/chat/newchat/friend_icon.png")
        imageHead:setScale(1.6)
        imageHD:setVisible(ChatMgr.getInstance():getAddFriendFlag())
    end
    self.mListView:pushBackCustomItem(item)
end

function ChatAddressBook:removeFriendItem(userId)
    local items = self.mListView:getItems()
    for index = 1, #items do
        local item = items[index]
        if item.itemType == self.mTableType.Type_Friend and item.info.m_userid == userId then
            self.mListView:removeChild(item)
            return
        end
    end
end

function ChatAddressBook:updateFrindList()
    self.mListView:removeAllItems()
    self:addItem({}, self.mTableType.Type_NewFriend)
    self:addItem({}, self.mTableType.Type_Group)
    local playerList = ChatMgr.getInstance():getFriendList()
    if playerList == nil then
        return
    end
    for k, v in pairs(playerList) do
        self:addItem(v, self.mTableType.Type_Friend)
    end
end

function ChatAddressBook:selectFrind(info)
    self.mPanelSelectFriend:setVisible(true)
    self.mPanelSelectFriend.info = info

    local panel = self.mPanelSelectFriend:getChildByName("Panel")
    local panelHead = panel:getChildByName("PanelHead")
    local imageHead = panelHead:getChildByName("ImageHead")
    local imageFrame = panelHead:getChildByName("ImageFrame")
    local imageVip = panelHead:getChildByName("ImageVIP")
    local textName = panel:getChildByName("TextName")

    textName:setColor(cc.c3b(0x1A, 0x1A, 0x1A))
    local str = ToolKit:getHead(info.m_head)
    imageHead:loadTexture(str, ccui.TextureResType.plistType)
    local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", info.m_vip)
    imageFrame:loadTexture(framePath, ccui.TextureResType.plistType)
    local vip_path = string.format("hall/plist/vip/img-vip-v%d.png", info.m_vip)
    imageVip:loadTexture(vip_path, ccui.TextureResType.plistType)
    textName:setString(info.m_name)
end

----------------------------------------
---按钮回调
----------------------------------------
function ChatAddressBook:onHideSelectFriendClick(sender)
    self.mPanelSelectFriend:setVisible(false)
end

function ChatAddressBook:onFriendMessageClick(sender)
    self.mPanelSelectFriend:setVisible(false)
    if self.mMessageLayer == nil then
        self.mMessageLayer = ChatMessageLayer.new(self.mPanelSelectFriend.info, "Friend")
        self:addChild(self.mMessageLayer, 1)
        self.mMessageLayer:setTilteText(self.mPanelSelectFriend.info.m_name)
        self.mMessageLayer:setReturnEvent(function()
            self.mMessageLayer:removeFromParent()
            self.mMessageLayer = nil
        end)
    end
end

function ChatAddressBook:onDelFriendClick(sender)
    self.mPanelSelectFriend:setVisible(false)
    local pUI = MessageBoxEx.new(nil, "是否确定删除该好友？")
    self:addChild(pUI, 2)
    pUI:addButton("删除", "hall/image/chat/newchat/button_red.png")
    pUI:addButton("取消", "hall/image/chat/newchat/button_blue.png")
    pUI:setButtonCall(function(index)
        if index == 1 then
            GlobalIMController:Friend_DelOneFriend_Req(self.mPanelSelectFriend.info.m_userid)
        end
        if index ~= 0 then
            pUI:removeFromParent()
        end
    end)
    -- local params = { 
    --     message = "是否确定删除该好友？" -- todo: 换行 
    -- }
    -- if self.m_DlgAlertDlg == nil then     
    --     self.m_DlgAlertDlg = require("app.hall.base.ui.MessageBox").new()
    --     local _leftCallback = function ()   
    --         GlobalIMController:Friend_DelOneFriend_Req(self.m_curUserId) 
    --         self.m_DlgAlertDlg = nil
    --     end
    --     local _rightcallback = function () 
    --         self.m_DlgAlertDlg:closeDialog()
    --         self.m_DlgAlertDlg = nil
    --     end
    --     self.m_DlgAlertDlg:TowSubmitAlert(params, _leftCallback, _rightcallback)
    --     self.m_DlgAlertDlg:showDialog()
    -- end
end

function ChatAddressBook:onListViewCall(taget, eventType)
    if eventType ~= 1 then
        return
    end

    local index = self.mListView:getCurSelectedIndex()
    local item = self.mListView:getItem(index)
    if nil == item then
        return
    end
    if item.itemType == self.mTableType.Type_Friend then
        -- getMessageLayer_():setTilteText(item.info.m_name)
        self:selectFrind(item.info)
    elseif item.itemType == self.mTableType.Type_Group then
        local ui = ChatGroupLayer.new()
        self:addChild(ui, 2)
    elseif item.itemType == self.mTableType.Type_NewFriend then
        local ui = ChatNewFriendLayer.new()
        self:addChild(ui, 2)
    end

end



----------------------------------------
---消息处理
----------------------------------------
function ChatAddressBook:on_Friend_AddFriend_Ack(_id, _cmd)
    -- if info.m_result == 0 then
    --     TOAST("申请成功")
    --     -- self.textField_search:setText("") 
    --     -- self.textField_search:setPlaceHolder("请输入玩家ID")
    -- else
    --     TOAST(info.m_reason)
    -- end
end

function ChatAddressBook:on_Friend_AddFriend_Nty(_id, _cmd)
    --添加红点
    local item = self.mListView:getChildByName(self.mTableType.Type_NewFriend)
    if item then
        local imageHD = UIAdapter:getChildByName(item, "PanelHead/ImageHD")
        imageHD:setVisible(true)
    end
end

function ChatAddressBook:on_Friend_AddFriend_Final_Ack(_id, _cmd)
    if _cmd.m_result == 0 then
        local player = ChatMgr.getInstance():getFriendById(_cmd.m_userid)
        self:addItem(player, self.mTableType.Type_Friend)
    end
end

function ChatAddressBook:on_Friend_AddFriend_Final_Nty(_id, _cmd)
    self:addItem(_cmd.m_user_info2, self.mTableType.Type_Friend)
end

function ChatAddressBook:on_Friend_DelOneFriend_Ack(_id, _cmd)
    if _cmd.m_result == 0 then
        self:removeFriendItem(_cmd.m_userid)
    end
end

function ChatAddressBook:on_Friend_GetAllFriendDetails_Ack(_id, _cmd)
    self:updateFrindList()
end

return ChatAddressBook