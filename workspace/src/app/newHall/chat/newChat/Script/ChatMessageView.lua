local ChatMgr = require("src.app.newHall.chat.ChatMgr")
local ChatMessageLayer = require("app.newHall.chat.newChat.ChatMessageLayer")
local ChatMessageView = class("ChatMessageView", function()
    return display.newNode()
end)

----------------------------------------
---共有函数
----------------------------------------
function ChatMessageView:getTitleName()
    return "信息"
end

----------------------------------------
---私有函数
----------------------------------------
function ChatMessageView:ctor(chatLayer)
    self.mMessageList = {}

    ToolKit:registDistructor(self, handler(self, self.onDestory))

    self.mPanel = UIAdapter:createNode("hall/csb/chatUI/ChatUI_PanelMessage.csb")
    self.mPanel:setTouchEnabled(false)
    self:addChild(self.mPanel)
    if LuaUtils.isIphoneXDesignResolution() then
        local viewSize = {width = display.size.width, height = display.size.height - 80}
        self.mPanel:setContentSize(viewSize)
    else
        self.mPanel:setContentSize(display.size)
    end
    UIAdapter:adapterEx(self.mPanel)

    self.mListView = self.mPanel:getChildByName("ListView")
    self.mItemTmp = self.mPanel:getChildByName("PanelItemTmp")
    self.mItemTmp:setVisible(false)

    self.mListView:addEventListener(handler(self, self.onListViewCall))

    -- addMsgCallBack(self, M2C_ChatRoom_NewMessage, handler(self, self.ON_M2C_CHATROOM_NEWMESSAGE))
    -- addMsgCallBack(self, M2C_ChatRoom_SendMessage_Ack, handler(self, self.ON_CHATROOM_SENDMESSAGE_ACK))
    addMsgCallBack(self, M2C_ChatRoom_SendMessage_Nty, handler(self, self.on_ChatRoom_SendMessage_Nty))
    addMsgCallBack(self, M2C_Friend_SendMessage_Nty, handler(self, self.on_Friend_SendMessage_Nty))
    addMsgCallBack(self, M2C_Friend_SendMessage_Ack, handler(self, self.on_Friend_SendMessage_Nty))
    addMsgCallBack(self, M2C_Friend_DelOneFriend_Ack, handler(self, self.on_Friend_DelOneFriend_Ack))
    addMsgCallBack(self, M2C_Friend_EmptyMessage, handler(self, self.on_M2C_Friend_EmptyMessage))
    addMsgCallBack(self, M2C_Group_SendMessage_Ack, handler(self, self.on_Group_SendMessage_Nty))
    addMsgCallBack(self, M2C_Group_SendMessage_Nty, handler(self, self.on_Group_SendMessage_Nty))
    addMsgCallBack(self, M2C_Group_EmptyMessage, handler(self, self.on_Group_EmptyMessage))
    addMsgCallBack(self, M2C_Group_DeleteGroup_Ack, handler(self, self.on_Group_DeleteGroup_Ack))
    addMsgCallBack(self, M2C_Group_ExitGroup_Ack, handler(self, self.on_Group_ExitGroup_Ack))

    self:updateList()
end

function ChatMessageView:onDestory()
    -- removeMsgCallBack(self, M2C_ChatRoom_NewMessage)
    removeMsgCallBack(self, M2C_ChatRoom_SendMessage_Nty)
    removeMsgCallBack(self, M2C_Friend_SendMessage_Nty)
    removeMsgCallBack(self, M2C_Friend_SendMessage_Ack)
    removeMsgCallBack(self, M2C_Friend_DelOneFriend_Ack)
    removeMsgCallBack(self, M2C_Friend_EmptyMessage)
    removeMsgCallBack(self, M2C_Group_SendMessage_Ack)
    removeMsgCallBack(self, M2C_Group_SendMessage_Nty)
    removeMsgCallBack(self, M2C_Group_EmptyMessage)
    removeMsgCallBack(self, M2C_Group_DeleteGroup_Ack)
    removeMsgCallBack(self, M2C_Group_ExitGroup_Ack)
end

function ChatMessageView:addItem(info, itemType)
    local item = self.mItemTmp:clone()
    item:setVisible(true)
    item.info = info
    item.itemType = itemType
    item:setTag(-1)
    local itemSize = self.mItemTmp:getContentSize()
    local back = item:getChildByName("ImageBack")
    back:ignoreContentAdaptWithSize(false)
    back:setContentSize(itemSize)
    local imageHead = UIAdapter:getChildByName(item, "PanelHead/ImageHead")
    local imageFrame = UIAdapter:getChildByName(item, "PanelHead/ImageFrame")
    local imageVip = UIAdapter:getChildByName(item, "PanelHead/ImageVIP")
    local textName = item:getChildByName("TextName")
    local textMessage = item:getChildByName("TextMessage")
    local imageHD = UIAdapter:getChildByName(item, "PanelHead/ImageHD")
    imageHD:setVisible(false)
    if item.itemType == "Room" then
        item:setName("Room")
        textName:setString("综合聊天群")
        imageFrame:setVisible(false)
        imageVip:setVisible(false)
        imageHead:loadTexture("hall/image/chat/newchat/group_icon.png")
        imageHead:setScale(1.6)
        local str = ChatMgr.getInstance():getLastMsg(GlobalConf.CHANNEL_ID)
        if str then
            local szHhOne = LuaUtils.utf8_sub(str, 0, 16)
            print(szHhOne)
            textMessage:setString(szHhOne)
        else
            textMessage:setString("...")
        end
    elseif item.itemType == "Group" then
        --群聊
        item:setName("Group")
        textName:setString(info.m_name)
        imageFrame:setVisible(false)
        imageVip:setVisible(false)
        imageHead:loadTexture("hall/image/chat/newchat/group_icon.png")
        imageHead:setScale(1.6)

        item.info = {
            m_time = info.m_time,
            m_GroupID = info.m_userid,
            m_GroupName = info.m_name
        }
        local msgItem = ChatMgr.getInstance():getLastFriendMsg(info.m_userid)
        if msgItem then
            textMessage:setString(msgItem.m_message)
        else
            textMessage:setString("...")
        end
        imageHD:setVisible(not info.m_bRead)
    elseif item.itemType == "Friend" then
        --私聊
        item:setName("Friend_" .. info.m_userid)
        textName:setString(info.m_name)
        local str = ToolKit:getHead(info.m_head)
        imageHead:loadTexture(str, ccui.TextureResType.plistType);
        local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", info.m_vip)
        imageFrame:loadTexture(framePath, ccui.TextureResType.plistType)
        local vip_path = string.format("hall/plist/vip/img-vip-v%d.png", info.m_vip)
        imageVip:loadTexture(vip_path, ccui.TextureResType.plistType)
        local msgItem = ChatMgr.getInstance():getLastFriendMsg(info.m_userid)
        if msgItem then
            textMessage:setString(msgItem.m_message)
        else
            textMessage:setString("...")
        end
        imageHD:setVisible(not info.m_bRead)
    end
    self.mMessageList[item:getName()] = item
    self.mListView:pushBackCustomItem(item)
end

function ChatMessageView:updateList()
    self.mListView:removeAllItems()
    self:addItem({}, "Room")
    local msgList = ChatMgr.getInstance():getFriendMsgList()
    if msgList == nil then
        return
    end
    print("updateChatList")
    --dump(msgList)
    for k, v in pairs(msgList) do
        if v.m_bGroup then
            self:addItem(v, "Group")
        else
            self:addItem(v, "Friend")
        end
    end
end

function ChatMessageView:delayTime(delay, callBack_)
    self:runAction(cc.Sequence:create(cc.DelayTime:create(delay), cc.CallFunc:create(callBack_), nil))
end

----------------------------------------
---按钮回调
----------------------------------------
function ChatMessageView:onAddClick()
end

function ChatMessageView:onListViewCall(taget, eventType)
    if eventType ~= 1 then
        return
    end

    local index = self.mListView:getCurSelectedIndex()
    local item = self.mListView:getItem(index)
    if item then
        if self.mMessageLayer == nil then
            self.mMessageLayer = ChatMessageLayer.new(item.info, item.itemType)
            self:addChild(self.mMessageLayer, 1)
            self.mMessageLayer:setReturnEvent(function()
                self.mMessageLayer:removeFromParent()
                self.mMessageLayer = nil
                self:updateList()
            end)
        end
        local textName = item:getChildByName("TextName")
        if item.itemType == "Room" then
            local list = ChatMgr.getInstance():getRoomPlayerList(GlobalConf.CHANNEL_ID) or {}
            self.mMessageLayer:setTilteText("综合聊天群（" .. #list .. "）")
        elseif item.itemType == "Friend" then
            self.mMessageLayer:setTilteText(textName:getString())
            ChatMgr:getInstance():setIsReadMsg(item.info.m_userid)
        elseif item.itemType == "Group" then
            self.mMessageLayer:setTilteText(textName:getString())
            ChatMgr:getInstance():setIsReadMsg(item.info.m_GroupID)
        end
        self.mMessageLayer:setVisible(true)
    end
end

----------------------------------------
---消息处理
----------------------------------------
function ChatMessageView:on_ChatRoom_SendMessage_Nty(_id, _cmd)
    local item = self.mMessageList["Room"]
    if item then
        local msg = ChatMgr.getInstance():getLastMsgItem(_cmd.m_RoomID)
        local textMessage = item:getChildByName("TextMessage")
        textMessage:setString(msg.m_message or "...")
    end
end

function ChatMessageView:on_Friend_SendMessage_Nty(_id, _cmd)
    self:delayTime(0.02, function()
        self:updateList()
    end)
end

function ChatMessageView:on_Friend_DelOneFriend_Ack(_id, _cmd)
    self:delayTime(0.02, function()
        self:updateList()
    end)
end

function ChatMessageView:on_Group_SendMessage_Nty(_id, _cmd)
    self:delayTime(0.02, function()
        self:updateList()
    end)
end

function ChatMessageView:on_Group_EmptyMessage(_id, _cmd)
    self:delayTime(0.02, function()
        self:updateList()
    end)
end

function ChatMessageView:on_M2C_Friend_EmptyMessage(_id, _cmd)
    self:delayTime(0.02, function ()
        self:updateList()
    end)
end

function ChatMessageView:on_Group_DeleteGroup_Ack(_id, _cmd)
    self:delayTime(0.02, function ()
        self:updateList()
    end)
end

function ChatMessageView:on_Group_ExitGroup_Ack(_id, _cmd)
    self:delayTime(0.02, function ()
        self:updateList()
    end)
end

return ChatMessageView