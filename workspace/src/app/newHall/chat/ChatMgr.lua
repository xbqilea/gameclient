local ChatMgr = class("ChatMgr")

ChatMgr_instance = nil
function ChatMgr.getInstance()
    if ChatMgr_instance == nil then  
        ChatMgr_instance = ChatMgr.new()
    end  
    return ChatMgr_instance  
end

function ChatMgr.releaseInstance()
    if ChatMgr_instance then
        ChatMgr_instance.m_RoomMsgList = {} --房间消息
        ChatMgr_instance.m_RoomPlayerList = {} --房间玩家列表  
        ChatMgr_instance.m_FriendList = {} --好友列表 
        ChatMgr_instance.m_GroupList = {} -- 群组列表
        --    json.saveFile(tostring(ChatMgr_instance.m_MyId).."applyList.txt",ChatMgr_instance.m_ApplyFriendList) --好友申请列表   
        --    json.saveFile(tostring(ChatMgr_instance.m_MyId).."msgList.txt",ChatMgr_instance.m_FriendMsgList)  
        cc.UserDefault:getInstance():setStringForKey(tostring(ChatMgr_instance.m_MyId) .. "applyList", require("cjson").encode(ChatMgr_instance.m_ApplyFriendList))
        cc.UserDefault:getInstance():setStringForKey(tostring(ChatMgr_instance.m_MyId) .. "applyList", require("cjson").encode(ChatMgr_instance.m_ApplyFriendList))
        ChatMgr_instance = nil
    end
end

function ChatMgr:ctor()
    self:clear()
end

function ChatMgr:clear()
    self.m_RoomMsgList={} --房间消息
    self.m_RoomPlayerList={} --房间玩家列表  
    self.m_FriendList = {} --好友列表 
    self.m_FriendMsgList = {}
    self.m_ApplyFriendList = {}
    self.m_GroupList = {} -- 群组列表
    self.m_canSpeakFlag = 0
    self.m_bNewMessage =false
end
function ChatMgr:setMyId(userId)
    self.m_MyId =userId 
    self.m_bAddFriend = cc.UserDefault:getInstance():getStringForKey(tostring(self.m_MyId).."bAddFriend")  
    self.m_ApplyFriendList = require("cjson").decode(cc.UserDefault:getInstance():getStringForKey(tostring(self.m_MyId).."applyList")) or {} --好友申请列表  
    self.m_FriendMsgList =require("cjson").decode(cc.UserDefault:getInstance():getStringForKey(tostring(self.m_MyId).."msgList"))  or {} 
    print("setMyId") 
end

function ChatMgr:getSpeakFlag()
    return self.m_canSpeakFlag
end
function ChatMgr:canSpeakFlag(flag)
    self.m_canSpeakFlag = flag
end
function ChatMgr:setRoomMsgList(roomID,msgList) 
--    for k,v in pairs(msgList) do 
--        local t = v.m_Member
--        t.m_message =v.m_message
--        t.m_time = 0
--        table.insert(self.m_RoomPlayerList[roomID],t)
--    end
    self.m_RoomMsgList[roomID] = msgList
    print("setRoomMsgList")
  --  dump(self.m_RoomMsgList[roomID])
end
function ChatMgr:getRoomMsgList(roomID)
    print("getRoomMsgList")
 --   dump(self.m_RoomMsgList[roomID])
    return self.m_RoomMsgList[roomID] 
end
function ChatMgr:setRoomPlayers(roomID,playerList)
    self.m_RoomPlayerList[roomID] = playerList
    table.sort(self.m_RoomPlayerList[roomID],function(a,b) return a.m_userid ==self.m_MyId end)
     print("setRoomPlayers")
 --   dump(self.m_RoomPlayerList[roomID])
end

function ChatMgr:addRoomMsg(roomID,userid,msg)
    print("addRoomMsg") 
    if self.m_RoomPlayerList[roomID] == nil then
        return
    end
--    dump(self.m_RoomPlayerList[roomID])  
    for k,v in pairs(self.m_RoomPlayerList[roomID]) do
        if userid == v.m_userid then 
            local t = {}
--            t.m_userid = v.m_userid
--            t.m_name = v.m_name
--            t.m_head = v.m_head
--            t.m_vip = v.m_vip
            t.m_from_UserInfo ={}
            t.m_from_UserInfo.m_userid = v.m_userid
            t.m_from_UserInfo.m_name = v.m_name
            t.m_from_UserInfo.m_head = v.m_head
            t.m_from_UserInfo.m_vip = v.m_vip
            t.m_message = msg
            t.m_time = os.time()
            table.insert( self.m_RoomMsgList[roomID],t) 
        end
    end
    
end
function ChatMgr:getLastMsg(roomID)
    
   -- dump(self.m_RoomMsgList[roomID])
  -- print(self.m_RoomMsgList[roomID])
    if self.m_RoomMsgList[roomID] == nil then
        return
    end
     print("getLastMsg")
    if  self.m_RoomMsgList[roomID][#self.m_RoomMsgList[roomID]] then
        return self.m_RoomMsgList[roomID][#self.m_RoomMsgList[roomID]].m_message
    else
        return nil
    end
end
function ChatMgr:getLastMsgItem(roomID)
     print("getLastMsgItem")
     if self.m_RoomMsgList[roomID] == nil then
        return
    end
  --  dump(self.m_RoomMsgList[roomID])
    if  self.m_RoomMsgList[roomID][#self.m_RoomMsgList[roomID]] then
        return self.m_RoomMsgList[roomID][#self.m_RoomMsgList[roomID]]
    else
        return nil
    end
end
function ChatMgr:addRoomPlayer(roomID,player)
    if self.m_RoomPlayerList[roomID] == nil then
        return
    end
     table.insert( self.m_RoomPlayerList[roomID],player) 
     print("addRoomPlayer")
  --  dump(self.m_RoomPlayerList[roomID])
end

function ChatMgr:removeRoomPlayer(roomID,userid)
    if self.m_RoomPlayerList[roomID] == nil then
        return
    end
    for k,v in pairs(self.m_RoomPlayerList[roomID]) do
        if userid == v.m_userid then
            table.remove(self.m_RoomPlayerList[roomID],k)
        end
    end
      print("removeRoomPlayer")
  --  dump(self.m_RoomPlayerList[roomID])
end
function ChatMgr:getLastRoomPlayer(roomID)
        print("getLastRoomPlayer")
        if self.m_RoomPlayerList[roomID] == nil then
            return nil
        end
  --  dump(self.m_RoomPlayerList[roomID])
    return self.m_RoomPlayerList[roomID][#self.m_RoomPlayerList[roomID]]
end


function ChatMgr:getRoomPlayerList(roomID) 
       print("getRoomPlayerList")
  --  dump(self.m_RoomPlayerList[roomID])
    return self.m_RoomPlayerList[roomID]
end

function ChatMgr:setFriendList(friendList) 
    self.m_FriendList = friendList
end

function ChatMgr:getFriendList()
   return self.m_FriendList
end
function ChatMgr:getFriendName(userId)
   for k,v in pairs(self.m_FriendList) do
        if userId == v.m_userid then
            return v.m_name
        end
    end
end
function ChatMgr:addFriend(friend)
    table.insert(self.m_FriendList,friend)
end
function ChatMgr:addFriend1(userId)
    for k,v in pairs(self.m_ApplyFriendList) do
        if userId == v.m_user_info1.m_userid then 
            table.insert(self.m_FriendList,v.m_user_info1) 
        end
    end 
    print("addFriend1")
    dump(self.m_FriendList)
end
function ChatMgr:removeFriend(userid)
    for k,v in pairs(self.m_FriendList) do
        if userid == v.m_userid then
            table.remove(self.m_FriendList,k)
        end
    end
    self:removeFriendMsg(userid)
end
 function ChatMgr:getFriendById(userId) 
    for k,v in pairs(self.m_FriendList) do
        if userId == v.m_userid then 
            return v
        end
    end
 end
function ChatMgr:getApplyFriendList()  
   return self.m_ApplyFriendList
end
function ChatMgr:getApplyFriendEncryptData(userId) 
     for k,v in pairs(self.m_ApplyFriendList) do
        if userId == v.m_user_info1.m_userid then 
            return v.m_EncryptData
        end
    end 
    return nil
end
function ChatMgr:addApplyFriend(friend)
    for k,v in pairs(self.m_ApplyFriendList) do
        if v.m_userid == friend.m_userid then
            return
        end
    end
    self:setAddFriendFlag(true)
    table.insert(self.m_ApplyFriendList,friend)
   -- json.saveFile(tostring(self.m_MyId).."applyList.txt",self.m_ApplyFriendList) --好友申请列表   
    cc.UserDefault:getInstance():setStringForKey(tostring(self.m_MyId).."applyList", require("cjson").encode(self.m_ApplyFriendList))
  --  json.saveFile("applyFriendList",friend) 
end

function ChatMgr:setAddFriendFlag(bFlag)
     
    if bFlag then
        self.m_bAddFriend = "1"
    else
         self.m_bAddFriend = "0"
    end
    cc.UserDefault:getInstance():setStringForKey(tostring(self.m_MyId).."bAddFriend",self.m_bAddFriend)
end
function ChatMgr:getAddFriendFlag()
    if self.m_bAddFriend == "1" then
        return true
    else
        return false
    end
    return self.m_bAddFriend
end
function ChatMgr:setAddGroupFlag(bFlag)
    self.m_bAddGroup = bFlag
end
function ChatMgr:getAddGroupFlag()
    return self.m_bAddGroup
end
function ChatMgr:setNewMessageFlag(bFlag) 
    self.m_bNewMessage = bFlag
end
function ChatMgr:getNewMessageFlag()
    return self.m_bNewMessage
end
function ChatMgr:removeApplyFriend(userid)
    for k,v in pairs(self.m_ApplyFriendList) do
        if userid == v.m_user_info1.m_userid then
            table.remove(self.m_ApplyFriendList,k)
        end
    end
  
end 
function ChatMgr:setFriendMsgList(info,userid,bflag) 

    self.m_bNewMessage = true
    local userId = tostring(userid)
    if self.m_FriendMsgList[userId] ==nil then
        self.m_FriendMsgList[userId] = {}   
        self.m_FriendMsgList[userId].msgList = {} 
        self.m_FriendMsgList[userId].m_userid = userid
    end  
    if bflag then
            self.m_FriendMsgList[userId].m_name = info.m_GroupName
            if info.m_from_UserInfo.m_userid == self.m_MyId then
                self.m_FriendMsgList[userId].m_bRead = true
            else
                self.m_FriendMsgList[userId].m_bRead = false
            end
    else
        if info.m_from_UserInfo.m_userid == self.m_MyId then
            self.m_FriendMsgList[userId].m_bRead = true
            for k,v in pairs(self.m_FriendList) do
                if v.m_userid == userid then
                    self.m_FriendMsgList[userId].m_head = v.m_head
                    self.m_FriendMsgList[userId].m_vip = v.m_vip
                    self.m_FriendMsgList[userId].m_name = v.m_name
                end
            end
        else
            self.m_FriendMsgList[userId].m_bRead = false
            self.m_FriendMsgList[userId].m_head = info.m_from_UserInfo.m_head
            self.m_FriendMsgList[userId].m_vip = info.m_from_UserInfo.m_vip
            self.m_FriendMsgList[userId].m_name = info.m_from_UserInfo.m_name
        end
            
    end
    self.m_FriendMsgList[userId].m_bGroup = bflag
    self.m_FriendMsgList[userId].m_time = os.time()  
   
    table.insert(self.m_FriendMsgList[userId].msgList,info)   
    cc.UserDefault:getInstance():setStringForKey(tostring(self.m_MyId).."msgList", require("cjson").encode(self.m_FriendMsgList))
  --  dump(self.m_FriendMsgList)
    --json.saveFile(tostring(self.m_MyId).."msgList",self.m_FriendMsgList)  
end
function ChatMgr:isAllReadMsg() 
    for k,v in pairs(self.m_FriendMsgList) do 
        if v.m_bRead == false then
            return false
        end
    end
    
    return true
end
function ChatMgr:setIsReadMsg(userId)
    local userId = tostring(userId)
    self.m_FriendMsgList[userId].m_bRead = true
    cc.UserDefault:getInstance():setStringForKey(tostring(self.m_MyId).."msgList", require("cjson").encode(self.m_FriendMsgList))
end
function ChatMgr:removeFriendMsg(userId)
     local userId = tostring(userId)
     if self.m_FriendMsgList[userId] then
        print("22222222222222222")
        self.m_FriendMsgList[userId] = nil 
        cc.UserDefault:getInstance():setStringForKey(tostring(self.m_MyId).."msgList", require("cjson").encode(self.m_FriendMsgList))
    end
end
function ChatMgr:getLastFriendMsg(userId)  
    local userId = tostring(userId)
   return self.m_FriendMsgList[userId].msgList[#self.m_FriendMsgList[userId].msgList]
end
function ChatMgr:getFriendMsg(userId) 
    local userId = tostring(userId)
    if self.m_FriendMsgList[userId] then
        return self.m_FriendMsgList[userId].msgList
    else
        return {}
    end
end
function ChatMgr:getMsgFriendById(userId)
    local userId = tostring(userId) 
    return self.m_FriendMsgList[userId]
end
function ChatMgr:getFriendMsgList()  
   local sortTab = {}
   for k,v in pairs(self.m_FriendMsgList) do
        table.insert(sortTab,v)
    end
     table.sort(sortTab,function(a,b) return a.m_time>b.m_time end)  
   return sortTab
end

--群组
function ChatMgr:setGroupList(groupList) 
    self.m_GroupList = groupList
end

function ChatMgr:getGroupList()
   return self.m_GroupList
end
function ChatMgr:getGroupName(groupId)
   for k,v in pairs(self.m_GroupList) do
        if groupId == v.m_GroupID then
            return v.m_GroupName
        end
    end
end
function ChatMgr:addGroup(group)
    table.insert(self.m_GroupList,group)
end
 
function ChatMgr:removeGroup(groupId)
    for k,v in pairs(self.m_GroupList) do
        if groupId == v.m_GroupID then
            table.remove(self.m_GroupList,k)
        end
    end
    
    self:removeFriendMsg(groupId)
end
 function ChatMgr:getGroupById(groupId) 
    for k,v in pairs(self.m_GroupList) do
        if groupId == v.m_GroupID then 
            return v
        end
    end
 end

function ChatMgr:getSessionID()
    if self.m_SessionID == nil then
        self.m_SessionID = 0
    end
    self.m_SessionID = self.m_SessionID + 1
    return self.m_SessionID
end
function ChatMgr:emptySessionID()
    self.m_SessionID = 0
end

return ChatMgr