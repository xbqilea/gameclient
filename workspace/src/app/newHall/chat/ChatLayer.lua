--region *.lua
--Date
--此文件由[BabeLua]插件自动生成



--endregion 
-- 布局文件 
local CHAT_UI_PATH = "hall/ChatLayer.csb";
local ChatWorldType =import(".ChatWorldType")
local ChatImageType =import(".ChatImageType") 
local ChatMgr= require("src.app.newHall.chat.ChatMgr")
local HNlayer = import("..HNLayer")
local ChatLayer = class("ChatLayer",function()
    return HNlayer.new()
end)
function ChatLayer:setExitEvent(callfunc)
    self.mExitCall = callfunc
end

function ChatLayer:ctor() 
    self.m_SessionID= 0
    self.m_curRoomId= GlobalConf.CHANNEL_ID
    self.btns={}
     ToolKit:registDistructor( self, handler(self, self.onDestroy))
    self:setupViews()

    addMsgCallBack(self, M2C_ChatRoom_Login_Nty, handler(self, self.ChatRoom_Login_Nty)) 
    addMsgCallBack(self, M2C_ChatRoom_Logout_Nty, handler(self, self.ChatRoom_Logout_Nty))
    addMsgCallBack(self, M2C_ChatRoom_SendMessage_Ack, handler(self, self.ChatRoom_SendMessage_Ack))
    addMsgCallBack(self, M2C_ChatRoom_SendMessage_Nty, handler(self, self.ChatRoom_SendMessage_Nty)) 
    addMsgCallBack(self, M2C_Friend_AddFriend_Ack, handler(self, self.Friend_AddFriend_Ack)) 
    addMsgCallBack(self, M2C_Friend_AddFriend_Nty, handler(self, self.Friend_AddFriend_Nty)) 
    addMsgCallBack(self,M2C_Friend_AddFriend_Final_Nty, handler(self, self.Friend_AddFriend_Final_Nty)) 
    addMsgCallBack(self,M2C_Friend_AddFriend_Final_Ack, handler(self, self.Friend_AddFriend_Final_Ack)) 
    addMsgCallBack(self,M2C_Friend_GetAllFriendDetails_Ack, handler(self, self.Friend_GetAllFriendDetails_Ack)) 
    addMsgCallBack(self,M2C_Friend_SendMessage_Ack, handler(self, self.Friend_SendMessage_Ack)) 
    addMsgCallBack(self,M2C_Friend_SendMessage_Nty, handler(self, self.Friend_SendMessage_Nty)) 
    addMsgCallBack(self,M2C_Friend_DelOneFriend_Ack, handler(self, self.Friend_DelOneFriend_Ack)) 
    addMsgCallBack(self,M2C_Group_CreateGroup_Ack, handler(self, self.Group_CreateGroup_Ack)) 
    addMsgCallBack(self,M2C_Group_ReadAllGroup_Ack, handler(self, self.Group_ReadAllGroup_Ack)) 
    addMsgCallBack(self,M2C_Group_ReadGroup_Ack, handler(self, self.Group_ReadGroup_Ack)) 
    addMsgCallBack(self,M2C_Group_AddFriendToGroup_Ack, handler(self, self.Group_AddFriendToGroup_Ack)) 
    addMsgCallBack(self,M2C_Group_DelMemberFromGroup_Ack, handler(self, self.Group_DelMemberFromGroup_Ack)) 
    addMsgCallBack(self,M2C_Group_DelMemberFromGroup_Nty, handler(self, self.Group_DelMemberFromGroup_Nty)) 
    addMsgCallBack(self,M2C_Group_DeleteGroup_Ack, handler(self,  self.Group_DeleteGroup_Ack)) 
     addMsgCallBack(self,M2C_Group_SendMessage_Ack, handler(self, self.Group_SendMessage_Ack)) 
     addMsgCallBack(self,M2C_Group_SendMessage_Nty, handler(self, self.Group_SendMessage_Nty)) 
     addMsgCallBack(self,M2C_Group_AddFriendToGroup_1_Nty, handler(self, self.Group_AddFriendToGroup_1_Nty)) 
     addMsgCallBack(self,M2C_Group_ExitGroup_Ack, handler(self, self.Group_ExitGroup_Ack)) 
    
end 

function ChatLayer:onDestroy()
    removeMsgCallBack(self, M2C_ChatRoom_Login_Nty) 
    removeMsgCallBack(self, M2C_ChatRoom_Logout_Nty)
    removeMsgCallBack(self, M2C_ChatRoom_SendMessage_Ack)
    removeMsgCallBack(self, M2C_ChatRoom_SendMessage_Nty) 
    removeMsgCallBack(self, M2C_Friend_AddFriend_Ack) 
    removeMsgCallBack(self, M2C_Friend_AddFriend_Nty) 
    removeMsgCallBack(self,M2C_Friend_AddFriend_Final_Nty) 
    removeMsgCallBack(self,M2C_Friend_AddFriend_Final_Ack) 
    removeMsgCallBack(self,M2C_Friend_GetAllFriendDetails_Ack) 
    removeMsgCallBack(self,M2C_Friend_SendMessage_Ack)
    removeMsgCallBack(self,M2C_Friend_SendMessage_Nty)
    removeMsgCallBack(self,M2C_Friend_DelOneFriend_Ack)
    removeMsgCallBack(self,M2C_Group_CreateGroup_Ack)
    removeMsgCallBack(self,M2C_Group_ReadAllGroup_Ack)
    removeMsgCallBack(self,M2C_Group_ReadGroup_Ack)
    removeMsgCallBack(self,M2C_Group_AddFriendToGroup_Ack)
    removeMsgCallBack(self,M2C_Group_DelMemberFromGroup_Ack)
    removeMsgCallBack(self,M2C_Group_DeleteGroup_Ack)
    removeMsgCallBack(self,M2C_Group_SendMessage_Ack)
    removeMsgCallBack(self,M2C_Group_SendMessage_Nty)
    removeMsgCallBack(self,M2C_Group_AddFriendToGroup_1_Nty) 
    removeMsgCallBack(self,M2C_Group_ExitGroup_Ack) 
end

function ChatLayer:Group_ExitGroup_Ack(_id,info)
   TOAST("退出成功")
   self.panel_groupmsg:setVisible(false)
end
function ChatLayer:Group_AddFriendToGroup_1_Nty(_id,info)
    self.image_dot0:setVisible(true)
    ChatMgr:getInstance():setAddGroupFlag(true)
end
function ChatLayer:Group_DeleteGroup_Ack(_id,info)   
    TOAST("删除成功")
    self.panel_creategroup1:setVisible(false)
    self.panel_creategroup2:setVisible(false)
    self.panel_groupmsg:setVisible(false)
end

function ChatLayer:Group_DelMemberFromGroup_Ack(_id,info)   
   for k,v in pairs( self.listview_delfriend:getItems()) do  
        if v.m_userid == self.m_curGroupDelId then   
            self.listview_delfriend:removeItem(k-1)
        end
    end
   for k,v in pairs( self.m_curGroupMember) do  
        if v.m_userid == self.m_curGroupDelId then   
           table.remove(self.m_curGroupMember,k)
        end
    end
    
end
function ChatLayer:Group_AddFriendToGroup_Ack(_id,info)  
    self.m_curGroupMember = nil
    TOAST("添加成功")
    self.panel_creategroup1:setVisible(false)
    self.panel_creategroup2:setVisible(false)
    self.panel_groupmsg:setVisible(false)
end
function ChatLayer:Group_ReadGroup_Ack(_id,info)  
    self.panel_groupmsg:setVisible(true) 
    local text_groupname = self.panel_groupmsg:getChildByName("text_groupname")
    text_groupname:setString(self.m_curGroupName)
    local num = table.nums(info.m_Member)-1
    local text_num = self.panel_groupmsg:getChildByName("text_num")
    text_num:setString(num.."位参与者") 
    self.m_flag =false
    if self.m_userid == Player:getAccountID() then
        self.button_release:loadTextureNormal("hall/image/chat/group/delquit.png", ccui.TextureResType.localType)
        self.m_flag = true
    else
        self.button_release:loadTextureNormal("hall/image/chat/group/del.png", ccui.TextureResType.localType)
      
    end 
    self.m_curGroupMember = info.m_Member
      self:updateGroupMsgList(info.m_Member)
end
function ChatLayer:updateGroupMsgList(data)
    
    self.listview_delfriend:removeAllItems()
    if self.m_flag then
        self.listview_delfriend:pushBackCustomItem(self.panel_addgroup1:clone())
    end
    for k,v in pairs(data) do
        if v.m_userid ~= Player:getAccountID() then
            self:addGroupMember(v) 
        end
    end
end
 function ChatLayer:addGroupMember(player)
     local t = self.panel_groupCell:clone() 
    local image_head = t:getChildByName("image_head")
    local str =ToolKit:getHead(player.m_head)   
    image_head:loadTexture(str,ccui.TextureResType.plistType);  
    
    local image_frame = t:getChildByName("image_frame")
    local image_vip = t:getChildByName("image_vip")
    local text_name = t:getChildByName("text_name")
    local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", player.m_vip)
    image_frame:loadTexture(framePath, ccui.TextureResType.plistType)
    local vip_path = string.format("hall/plist/vip/img-vip-v%d.png", player.m_vip)
    image_vip:loadTexture(vip_path, ccui.TextureResType.plistType)
    image_vip:ignoreContentAdaptWithSize(true)
    image_head:setScale(0.5)
    image_frame:setScale(0.5)
    image_vip:setScale(0.5)
    text_name:setString(player.m_name)
    local image_del = t:getChildByName("image_del") 
    t.m_userid = player.m_userid
    t.m_head = player.m_head
    t.m_vip = player.m_vip
    t.m_name = player.m_name 
    local image_select = t:getChildByName("select")
    local image_unselect = t:getChildByName("unselect")
    image_select:setVisible(false)
    image_unselect:setVisible(false)
    image_del.m_userid = player.m_userid
    if self.m_flag then
        image_del:setVisible(true)
        image_del:addTouchEventListener(handler(self, self.onDelEvent))
    end
    self.listview_delfriend:pushBackCustomItem(t)
 end
function ChatLayer:onDelEvent(sender,eventType)
    if eventType== ccui.TouchEventType.ended then    
        local params = { 
            message = "是否确定踢出群聊？" -- todo: 换行 
        }
        if self.m_DlgAlertDlg == nil then     
            self.m_DlgAlertDlg = require("app.hall.base.ui.MessageBox").new()
            local _leftCallback = function ()   
                self.m_curGroupDelId =sender.m_userid 
                GlobalIMController:Group_DelMemberFromGroup_Req(self.m_curGroupId,self.m_SessionID,{sender.m_userid})
                self.m_DlgAlertDlg = nil
            end

            local _rightcallback = function () 
                self.m_DlgAlertDlg:closeDialog()
                self.m_DlgAlertDlg = nil
            end

            self.m_DlgAlertDlg:TowSubmitAlert(params, _leftCallback, _rightcallback)
            self.m_DlgAlertDlg:showDialog()
        end
       
    end
end
function ChatLayer:Group_ReadAllGroup_Ack(_id,info)   
    self:updateGroupList()
end

function ChatLayer:updateGroupList()
    local playerList = ChatMgr:getInstance():getGroupList()  
    if playerList == nil then
        return
    end
    self.listview_group:removeAllItems()
    for k,v in pairs(playerList) do
        self:addGroupItem(v) 
    end
end

 function ChatLayer:addGroupItem(player)
    local t = self.panel_addCell:clone() 
    local image_head = t:getChildByName("image_head") 
    image_head:loadTexture("hall/image/chat/group/qun.png");   
    local image_frame = t:getChildByName("image_frame")
    local image_vip = t:getChildByName("image_vip")
    local text_name = t:getChildByName("text_name") 
    image_frame:setVisible(false) 
    image_vip:setVisible(false) 
    image_head:setScale(1.5)
--    image_frame:setScale(0.6)
--    image_vip:setScale(0.6)
    text_name:setString(player.m_GroupName)
    t.m_GroupID = player.m_GroupID
    t.m_userid = player.m_userid 
    t.m_GroupName = player.m_GroupName  
    local button_addFriend =  t:getChildByName("button_addFriend")
    button_addFriend:setVisible(false)
    
    local button_delFriend =  t:getChildByName("button_delFriend")
    button_delFriend:setVisible(false)
    t:addTouchEventListener(handler(self, self.onSelectGroup))
    self.listview_group:pushBackCustomItem(t)
 end

 function ChatLayer:onSelectGroup(sender,eventType)
    if eventType== ccui.TouchEventType.ended then    
        self.panel_selectFriend:setVisible(false)
        self.panel_group:setVisible(false)
        self.panel_addFriendList:setVisible(false)
        self.panel_serchFriend:setVisible(false)
        self.panel_friendchat:setVisible(true) 
        self.panel_block:setVisible(false)
        self.textField_name:setPlaceHolder("请输入文字")
        
        self.m_curGroupId = sender.m_GroupID
        self.m_curSelectId=nil
        local data = ChatMgr:getInstance():getMsgFriendById(self.m_curGroupId)
        local name = ChatMgr:getInstance():getGroupName(self.m_curGroupId)
        self.text_friendname:setString(name)
        self.m_curGroupName= sender.m_GroupName
        self.m_userid = sender.m_userid
        self.button_more:setVisible(true)
      --  if data then
            self:showFriendMsg(self.m_curGroupId) 
     --   end
        
    end
 end
function ChatLayer:Group_CreateGroup_Ack(_id,info)  
    self.panel_creategroup2:setVisible(true)
    self.m_curGroupId = info.m_GroupID
    self.m_curUserId = nil
    self:updateGroupFriendList()
end

function ChatLayer:Friend_GetAllFriendDetails_Ack(_id,info)  
    self:updateFriendList() -- 好友列表
end
function ChatLayer:Friend_DelOneFriend_Ack(_id,info) 
    if info.m_result == 0 then
        self:removeFriend(info.m_userid)
        self.panel_selectFriend:setVisible(false)
    end
end
function ChatLayer:Friend_SendMessage_Ack(_id,info)  
     if info.m_result == 0 then
        info.m_message = self.textField_name:getText()
        info.m_from_UserInfo = {}
        info.m_from_UserInfo.m_head = Player:getFaceID()
        info.m_from_UserInfo.m_name = Player:getNickName()
        info.m_from_UserInfo.m_userid = Player:getAccountID()
        info.m_from_UserInfo.m_vip = Player:getVipLevel()
        
        ChatMgr:getInstance():setFriendMsgList(info,info.m_to_userid,false) 
        ChatMgr:getInstance():setNewMessageFlag(false)
        self:updateChatList()
        local item = ChatMgr:getInstance():getLastFriendMsg(info.m_to_userid)
        self:showMessage(item) 
        self.textField_name:setText("")  
       -- self:addMsgItem(info.m_to_userid)
    else
        TOAST(info.m_reason)
    end
end
function ChatLayer:Group_SendMessage_Ack(_id,info)  
     if info.m_result == 0 then
        info.m_message = self.textField_name:getText()
        info.m_from_UserInfo = {}
        info.m_from_UserInfo.m_head = Player:getFaceID()
        info.m_from_UserInfo.m_name = Player:getNickName()
        info.m_from_UserInfo.m_userid = Player:getAccountID()
        info.m_from_UserInfo.m_vip = Player:getVipLevel()
        local name = ChatMgr:getInstance():getGroupName(info.m_GroupID)
        print("Friend_SendMessage_Ack")
        print(name)
        info.m_GroupName = name
        ChatMgr:getInstance():setFriendMsgList(info,info.m_GroupID,true) 
        ChatMgr:getInstance():setNewMessageFlag(false)
        -- self:updateChatList()
        -- local item = ChatMgr:getInstance():getLastFriendMsg(info.m_GroupID)
        -- self:showMessage(item) 
        self.textField_name:setText("")  
       -- self:addMsgItem(info.m_to_userid)
    else
        TOAST(info.m_reason)
    end
end
function ChatLayer:Friend_SendMessage_Nty(_id,info) 
    self:updateChatList() -- 聊天列表 
    if not self.panel_chat:isVisible() then
        self.btns[2]:getChildByName("image_dot2"):setVisible(true)
    end
    local items = self.listview_chat:getItems()
    for k,v in pairs(items) do  
        if v.m_userid ==info.m_from_UserInfo.m_userid then
            local image_dot = v:getChildByName("image_dot") 
            if not self.panel_chat:isVisible() then
                
                image_dot:setVisible(true) 
            else
                if info.m_from_UserInfo.m_userid ~=self.m_curSelectId then
                     image_dot:setVisible(true) 
                end
            end
        end
        if v.m_userid ==self.m_curSelectId then
              local image_select = v:getChildByName("select") 
            image_select:setVisible(true) 
        end
    end  
    if self.m_curSelectId then 
        if self.m_curSelectId == info.m_from_UserInfo.m_userid and self.scrollview_chat:isVisible() and self.panel_chat:isVisible() then
            local item = ChatMgr:getInstance():getLastFriendMsg(info.m_from_UserInfo.m_userid) 
            self:showMessage(item)   
        end
         if self.m_curSelectId == info.m_from_UserInfo.m_userid and self.scrollview_friendchat:isVisible() and self.panel_friend:isVisible() then
            local item = ChatMgr:getInstance():getLastFriendMsg(info.m_from_UserInfo.m_userid) 
            self:showMessage(item)   
        end
    end
   
end

function ChatLayer:Group_SendMessage_Nty(_id,info) 
    self:updateChatList() -- 聊天列表 
    if not self.panel_chat:isVisible() then
        self.btns[2]:getChildByName("image_dot2"):setVisible(true)
    end
    local items = self.listview_chat:getItems()
    for k,v in pairs(items) do    
        if tonumber(v.m_userid) ==tonumber(info.m_GroupID) then
            local image_dot = v:getChildByName("image_dot")  
            if not self.panel_chat:isVisible() then 
                image_dot:setVisible(true) 
            else
                if info.m_GroupID ~=self.m_curGroupId then
                     image_dot:setVisible(true) 
                end
            end
        end
        if tonumber(v.m_userid) ==self.m_curGroupId then
              local image_select = v:getChildByName("select") 
            image_select:setVisible(true) 
        end
    end  
    
    if self.m_curGroupId then
        if self.m_curGroupId == info.m_GroupID and self.scrollview_chat:isVisible() and self.panel_chat:isVisible() then
            local item = ChatMgr:getInstance():getLastFriendMsg(info.m_GroupID) 
            self:showMessage(item)   
        end
        if self.m_curGroupId == info.m_GroupID and self.scrollview_friendchat:isVisible() and self.panel_friend:isVisible() then 
            local item = ChatMgr:getInstance():getLastFriendMsg(info.m_GroupID) 
            self:showMessage(item)   
        end
    end
end
function ChatLayer:Friend_AddFriend_Ack(_id,info) 
    if info.m_result == 0 then
        TOAST("申请成功")
        self.textField_search:setText("") 
        self.textField_search:setPlaceHolder("请输入玩家ID")
    else
        TOAST(info.m_reason)
    end
end

function ChatLayer:Friend_AddFriend_Nty(_id,info) 
    --显示红点
    if not self.panel_addFriendList:isVisible() then
        self.image_dot1:setVisible(true)
    end
    self:updateApplyFriendList()
end

function ChatLayer:Friend_AddFriend_Final_Nty(_id,info) 
    self:addFriend(info.m_user_info2)
end
function ChatLayer:Friend_AddFriend_Final_Ack(_id,info) 
dump(info)
    if info.m_result == 0 then
        local player =ChatMgr:getInstance():getFriendById(info.m_userid)
        self:addFriend(player) 
    end
    self:removeApplyFriend(info.m_userid)
end
function ChatLayer:removeApplyFriend(userId)
    ChatMgr:getInstance():removeApplyFriend(userId) 
    for k,v in pairs( self.listview_addFriend:getItems()) do 
        if v.m_userid == userId then 
            
            self.listview_addFriend:removeItem(k-1)
        end
    end
end 
function ChatLayer:ChatRoom_Login_Nty(_id,info) 
    local player = ChatMgr:getInstance():getLastRoomPlayer(info.m_RoomID)
    self:addPlayer(player)
end
function ChatLayer:ChatRoom_Logout_Nty(_id,info) 
    self:removePlayer(info.m_RoomID,info.m_userid)
end
function ChatLayer:ChatRoom_SendMessage_Ack(_id,info)
  --  self:removePlayer(info.roomID,info.userid)
    if info.m_status == 0 then
        -- ChatMgr:getInstance():addRoomMsg(info.m_RoomID,Player:getAccountID(),self.textField_name:getText())
        local item = ChatMgr:getInstance():getLastMsgItem(info.m_RoomID)
        dump(item)
        -- self:showMessage(item)
        sendMsg(M2C_ChatRoom_NewMessage,self.textField_name:getText())
        self.textField_name:setText("") 
        
    else
        TOAST(info.m_reason)
    end
end
function ChatLayer:ChatRoom_SendMessage_Nty(_id,info)
    local item = ChatMgr:getInstance():getLastMsgItem(info.m_RoomID)
    if self.panel_chatroom:isVisible() then
        self:showMessage(item)
    end
end
function ChatLayer:showMessage(item)
    local isSelf = false 
    local info = item.m_from_UserInfo or item.m_Member
    if info.m_userid == Player:getAccountID() then
        isSelf = true
    end
    local chatContent =self:createWorldType(item.m_message, isSelf);   
    dump(item)
    local str =ToolKit:getHead(info.m_head) 
    --    //加载头像
       
    local image_head = ccui.ImageView:create()
    image_head:loadTexture(str,ccui.TextureResType.plistType);  
    
    local stencilWidth = image_head:getContentSize().width/2
    local image_frame = ccui.ImageView:create()
    local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", info.m_vip)
    image_frame:loadTexture(framePath, ccui.TextureResType.plistType)
   -- image_frame:setScale(0.6);    
    image_head:setScale(0.6); 
    image_head:addChild(image_frame)
    image_frame:setPosition(image_head:getContentSize().width/2,image_head:getContentSize().height/2)
    local textName = ccui.Text:create(info.m_name,"ttf/yt.TTF", 20)
    textName:setAnchorPoint(cc.p(0,0))
     local time = os.date("%H:%M",item.m_time)  
    local textTime = ccui.Text:create(time,"ttf/yt.TTF", 20) 
    local size =textName:getContentSize() 
    textTime:setAnchorPoint(cc.p(0,0))
    if isSelf then
        textTime:setPosition(chatContent:getContentSize().width+stencilWidth-100,chatContent:getContentSize().height+10)
        textName:setPosition(textTime:getPositionX()-size.width-30,chatContent:getContentSize().height+10)
        chatContent:setPositionX( self.curSpeakingContent:getContentSize().width-200 )
        image_head:setPosition(chatContent:getContentSize().width+stencilWidth+15,chatContent:getContentSize().height - 22)
    else
        textName:setPosition( -stencilWidth+50,chatContent:getContentSize().height+10)
        textTime:setPosition(textName:getPositionX()+size.width+30,chatContent:getContentSize().height+10)
        
        chatContent:setPositionX( 160)
        image_head:setPosition(-stencilWidth,chatContent:getContentSize().height - 22)
    end 
    chatContent:addChild(textTime)
    chatContent:addChild(textName)
    chatContent:addChild(image_head)
    self.curSpeakingContent:setVisible(true)
    self.curSpeakingContent:addChild(chatContent);
    self:updateScrollView(self.curSpeakingContent); 
    
end
 function ChatLayer:onTouchCallback(sender)
    local name = sender:getName()
      self.button_newFriend:loadTextureNormal("hall/image/chat/xdpy_n.png", ccui.TextureResType.localType)
        self.button_group:loadTextureNormal("hall/image/chat/ql_n.png", ccui.TextureResType.localType)
    if name == "button_close" then
        if self.closeExtra then
            self:closeExtra()
            return 
        end
        if self:getParent().image_dot1 then 
            if  ChatMgr:getInstance():getAddFriendFlag() or ChatMgr:getInstance():getNewMessageFlag() or (not ChatMgr:getInstance():isAllReadMsg()) then 
                self:getParent().image_dot1:setVisible(true) 
            else
                self:getParent().image_dot1:setVisible(false) 
            end
        end
        if self.mExitCall then
            self.mExitCall()
        end
        self:close() 
    elseif name == "btn_send" then 
        local chatStr = self.textField_name:getText()
        self.m_SessionID= self.m_SessionID+1
        if chatStr == "" then
            TOAST("请输入文字")
            return
        elseif string.utf8len(chatStr)>150 then
            TOAST("输入不能超过150字")
            return
        end
        self:ButtonShake(sender, 0.5)
        if self:isSendFriendState() then
            if self.m_curGroupId then
                GlobalIMController:CS_C2M_Group_SendMessage_Req(self.m_curGroupId,self.m_SessionID,chatStr, 1)
            else
                GlobalIMController:Friend_SendMessage_Req(self.m_curSelectId,chatStr,self.m_SessionID, 1)
            end
        else 
            GlobalIMController:ChatRoom_SendMessage_Req(self.m_curRoomId,self.m_SessionID,chatStr, 1)
        end
     elseif name == "btn_send" then
        local chatStr = self.textField_name:getText()
        self.m_SessionID= self.m_SessionID+1
        if chatStr == "" then
            TOAST("请输入文字")
            return
        end
        self:ButtonShake(sender, 0.5)
        GlobalIMController:ChatRoom_SendMessage_Req(self.m_curRoomId,self.m_SessionID,chatStr, 1)
    elseif name == "btn_face" then
        self.Image_Emote:setVisible(not self.Image_Emote:isVisible())
    elseif name == "btn_add" then
       
    elseif name == "button_search" then
         local chatStr = self.textField_search:getText() 
        if chatStr == "" then
            TOAST("请输入玩家ID")
            return
        end
        GlobalIMController:Friend_AddFriend_Req(tonumber(chatStr))
    elseif name == "button_newFriend" then
         self.panel_selectFriend:setVisible(false)
        self.panel_group:setVisible(false)
        self.panel_addFriendList:setVisible(true)
        self.panel_serchFriend:setVisible(false)
        self.panel_friendchat:setVisible(false) 
        self.image_dot1:setVisible(false)
        self.panel_block:setVisible(true)
        self:cleanSelect(self.listview_friend)
        self.button_newFriend:loadTextureNormal("hall/image/chat/xdpy.png", ccui.TextureResType.localType)
        self.button_group:loadTextureNormal("hall/image/chat/ql_n.png", ccui.TextureResType.localType)
        ChatMgr:getInstance():setAddFriendFlag(false)
        self.btns[3]:getChildByName("image_dot2"):setVisible(false)
    elseif name == "button_group" then
         
        self.panel_selectFriend:setVisible(false)
        self.panel_group:setVisible(true)
        self.panel_addFriendList:setVisible(false)
        self.panel_serchFriend:setVisible(false)
        self.panel_friendchat:setVisible(false) 
        self.panel_block:setVisible(true)
        self.panel_addView:setVisible(false)   
        self:cleanSelect(self.listview_friend)
        self.button_group:loadTextureNormal("hall/image/chat/ql.png", ccui.TextureResType.localType)
        self.button_newFriend:loadTextureNormal("hall/image/chat/xdpy_n.png", ccui.TextureResType.localType) 
         ChatMgr:getInstance():setAddGroupFlag(false)
         self.image_dot0:setVisible(false)
        self.btns[3]:getChildByName("image_dot2"):setVisible(false)
        GlobalIMController:Group_ReadAllGroup_Req()
    elseif name =="btn_reserch" then
        self.panel_addView:setVisible(true)
    elseif name == "button_createGroup" then
         self.panel_creategroup1:setVisible(true) 
         self.panel_addView:setVisible(false)
    elseif name == "button_addFriend" then 
         self.panel_selectFriend:setVisible(false)
        self.panel_group:setVisible(false)
        self.panel_addFriendList:setVisible(false)
        self.panel_serchFriend:setVisible(true)
        self.panel_friendchat:setVisible(false)
        self.panel_block:setVisible(true)
        self.panel_addView:setVisible(false)   
        self:cleanSelect(self.listview_friend)
        
    elseif name == "button_msg" then
      --  self:onShowPanel(self.btns[2],ccui.TouchEventType.ended)
       self.panel_selectFriend:setVisible(false)
        self.panel_group:setVisible(false)
        self.panel_addFriendList:setVisible(false)
        self.panel_serchFriend:setVisible(false)
        self.panel_friendchat:setVisible(true) 
        self.panel_block:setVisible(false)
        local data = ChatMgr:getInstance():getMsgFriendById(self.m_curSelectId)
        local name = ChatMgr:getInstance():getFriendName(self.m_curSelectId)
        self.text_friendname:setString(name)
        self.button_more:setVisible(false) 
   --     if data then 
            self:showFriendMsg(self.m_curSelectId) 
      --  end
    elseif name == "button_del" then
        local params = { 
            message = "是否确定删除该好友？" -- todo: 换行 
        }
        if self.m_DlgAlertDlg == nil then     
            self.m_DlgAlertDlg = require("app.hall.base.ui.MessageBox").new()
            local _leftCallback = function ()   
                GlobalIMController:Friend_DelOneFriend_Req(self.m_curUserId) 
                self.m_DlgAlertDlg = nil
            end

            local _rightcallback = function () 
                -- self.m_DlgAlertDlg:closeDialog()
                self.m_DlgAlertDlg = nil
            end

            self.m_DlgAlertDlg:TowSubmitAlert(params, _leftCallback, _rightcallback)
            self.m_DlgAlertDlg:showDialog()
        end
    elseif name == "btn_vip1" then 
        if self.m_bMenuMoving then
            return
        end
        self.m_bMenuMoving = true

        if self.image_vipList:isVisible() then
            --菜单栏收起 
         --   self.m_pBtnMenuPush:setVisible(false)
            local callBack = cc.CallFunc:create(function()
           --     self.btn_vip1:setFlipX(true)
                self.btn_vip1:loadTextureNormal("hall/image/chat/5.png")
            end)
            local callBack2 = cc.CallFunc:create(function()
                self.m_bMenuMoving = false
            end)
            showMenuPush(self.image_vipList, callBack, callBack2, 1245, 380 )
            showMenuPop(self.btn_vip1, callBack, callBack2, 1206, 417 )
        
        else 
            --菜单栏放下
          --  self.m_pBtnMenuPush:setVisible(true)
           -- self.m_pNodeMenu:setPosition(cc.p(2, 780))
            local callBack = cc.CallFunc:create(function()
             --   self.btn_vip1:setFlipX(true)
                self.btn_vip1:loadTextureNormal("hall/image/chat/4.png")
            end)
            local callBack2 = cc.CallFunc:create(function()
                self.m_bMenuMoving = false
            end)
            showMenuPop(self.image_vipList, callBack, callBack2, 993, 380 )
            showMenuPop(self.btn_vip1, callBack, callBack2, 943, 417 )
        end
    elseif name == "button_groupclose" then
        self.panel_creategroup1:setVisible(false)
    elseif name == "button_groupnext" then
        if self.m_groupNameText:getText() == "" then
            TOAST("请输入群组名称")
        else
             GlobalIMController:Group_CreateGroup_Req(self.m_groupNameText:getText())
        end
    elseif name =="button_groupback" then
          self.panel_creategroup1:setVisible(false)
        self.panel_creategroup2:setVisible(false)
    elseif name == "button_finish" then
        local groupList = {}
        for k,v in pairs(self.listview_groupfriend:getItems()) do
            local image_select = v:getChildByName("select")
            if image_select:isVisible() then
                table.insert(groupList,v.m_userid)
            end
        end
        GlobalIMController:Group_AddFriendToGroup_Req(self.m_curGroupId,self.m_SessionID,groupList)
    elseif name == "button_more" then
        GlobalIMController:Group_ReadGroup_Req(self.m_curGroupId)
    elseif name == "panel_addgroup1" then
        self.panel_creategroup2:setVisible(true) 
        self:updateGroupFriendList()
    elseif name == "button_release" then
        if self.m_flag then
             GlobalIMController:Group_DeleteGroup_Req(self.m_curGroupId)
        else
             GlobalIMController:Group_ExitGroup_Req(self.m_curGroupId) 
        end
    elseif name == "button_groupmsgclose" then
        self.panel_groupmsg:setVisible(false)
    elseif name == "btn_clear" then 
        local params = { 
            message = "是否确定清楚聊天记录？" -- todo: 换行 
        }
        if self.m_DlgAlertDlg == nil then     
            self.m_DlgAlertDlg = require("app.hall.base.ui.MessageBox").new()
            local _leftCallback = function ()   
               self.curSpeakingContent:removeAllChildren() 
                ChatMgr:getInstance():removeFriendMsg(self.m_curGroupId or self.m_curSelectId)
                self:updateChatList()
                self:onShowPanel(self.btns[2],ccui.TouchEventType.ended)
                self.btn_clear:setVisible(false)
                self.m_DlgAlertDlg = nil
            end

            local _rightcallback = function () 
                self.m_DlgAlertDlg:closeDialog()
                self.m_DlgAlertDlg = nil
            end

            self.m_DlgAlertDlg:TowSubmitAlert(params, _leftCallback, _rightcallback)
            self.m_DlgAlertDlg:showDialog()
        end
       
    else
        self.panel_addView:setVisible(false)   
--        self.panel_creategroup1:setVisible(false)
--        self.panel_creategroup2:setVisible(false)
--        self.panel_groupmsg:setVisible(false)
    end
 end
 function showMenuPop(node, callback, callback2, x, y)
    
    if node==nil or callback==nil or callback2==nil then 
        return
    end
    AudioManager.getInstance():playSound("res/public/sound/sound-button.mp3")
    node:stopAllActions()
    local aTime = 0.3
    local desX = x or 0
    local desY = y or 0
    node:setOpacity(0)
    local moveTo = cc.MoveTo:create(aTime, cc.p(desX, desY))
    local show = cc.Show:create()
    local sp = cc.Spawn:create(cc.EaseBackOut:create(moveTo), cc.FadeIn:create(aTime))
    local seq = cc.Sequence:create(show,callback, sp,  cc.DelayTime:create(0.1), callback2)
    node:runAction(seq)
end

function showMenuPush(node, callback, callback2, x, y)

    if node==nil or callback==nil or callback2==nil then 
        return
    end
    AudioManager.getInstance():playSound("res/public/sound/sound-close.mp3")
    node:stopAllActions()
    local aTime = 0.25
    local desX = x or 0
    local desY = y or 0
    local moveTo = cc.MoveTo:create(aTime, cc.p(desX, desY))
    local sp = cc.Spawn:create(cc.EaseBackIn:create(moveTo), cc.FadeOut:create(aTime))
    local hide = cc.Hide:create()
    local seq = cc.Sequence:create(callback,sp,  hide, cc.DelayTime:create(0.1), callback2)
    node:runAction(seq)
end
function ChatLayer:setupViews() 
    
	local node = UIAdapter:createNode(CHAT_UI_PATH);
   
    UIAdapter:adapter(node, handler(self, self.onTouchCallback))
	self:addChild(node);
    local center =  node:getChildByName("center");  
    -- local diffY = (display.size.height - 750) / 2
    node:setPosition(cc.p(0,0)) 
    local centerSize = center:getContentSize()
    local diffX = ((display.width - centerSize.width) / 2) --145 - (1624-display.size.width)/2
    center:setPositionX(diffX) 
    UIAdapter:praseNode(node,self) 
    --self.panel_vipCell:clone() 
    local image_head = self.panel_vipCell:getChildByName("image_head")
    local image_frame = self.panel_vipCell:getChildByName("image_frame")
    local image_vip = self.panel_vipCell:getChildByName("image_vip")
    -- image_vip:setPositionX(21)
    image_frame:setPositionX(35)
    image_head:setPositionX(35)

    image_head = self.panel_friendCell:getChildByName("image_head") 
    image_frame = self.panel_friendCell:getChildByName("image_frame")
    image_head:setPositionX(50)
    image_frame:setPositionX(50)

    image_head = self.panel_addCell:getChildByName("image_head")
    image_frame = self.panel_addCell:getChildByName("image_frame")
    image_head:setPositionX(50)
    image_frame:setPositionX(50)

    image_head = self.panel_groupCell:getChildByName("image_head")
    image_frame = self.panel_groupCell:getChildByName("image_frame")
    image_head:setPositionX(50)
    image_frame:setPositionX(50)
    self.image_vipList:setScale9Enabled(true)
    self.image_vipList:setCapInsets(cc.rect(180, 100, 10, 460))
    self.image_vipList:setContentSize({width = 263, height = 573})
    self.listview_player:setContentSize({width = 253, height = 518})
    self.listview_player:setPosition(cc.p(5, 5))
    self.image_vipList:setVisible(false)
    local editTxt = nil
    if UpdateFunctions.platform == "ios" then
        editTxt = ccui.EditBox:create(cc.size(425,45), "hall/image/bank/zcm_zc1_2.png")
    else
        editTxt = ccui.EditBox:create(cc.size(425,70), "hall/image/bank/zcm_zc1_2.png")
    end
    self.textField_name = editTxt
    editTxt:setName("inputTxt")  
    editTxt:setAnchorPoint(0.5,0.5)  
    editTxt:setPosition(cc.p(self.image_input:getPositionX() / 2+5  ,self.image_input:getPositionY() / 2+5 ))
    editTxt:setFontSize(26)
    --editTxt:setMaxLength(12)
    editTxt:setFontColor(cc.c4b(255,255,255,255))
    editTxt:setFontName("ttf/jcy.ttf")
    if UpdateFunctions.platform == "ios" then
        editTxt:setPlaceholderFontSize(16)
    else
        editTxt:setPlaceholderFontSize(24)
    end
	editTxt:setPlaceholderFontColor(cc.c4b(184,187,182,255))  
    
	editTxt:setPlaceHolder("请输入文字")
     if ChatMgr:getInstance():getSpeakFlag() == 1 then
        self.textField_name:setPlaceHolder("全体禁言中")
         self.panel_block:setVisible(true)
    end
    editTxt:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE)
    editTxt:setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE)
    editTxt:registerScriptEditBoxHandler(function(eventname,sender) self:editboxHandle(eventname,sender) end)
    self.image_input:addChild(editTxt,5)  
    

   local editTxt = nil
   if UpdateFunctions.platform == "ios" then
        editTxt = ccui.EditBox:create(cc.size(280,45), "hall/image/bank/zcm_zc1_2.png")
    else
        editTxt = ccui.EditBox:create(cc.size(280,70), "hall/image/bank/zcm_zc1_2.png")
    end
    editTxt:setName("inputTxt1")  
    editTxt:setAnchorPoint(0.5,0.5)  
    editTxt:setPosition(cc.p(self.image_input1:getPositionX() / 2+5  ,30 ))
    editTxt:setFontSize(26)
  --  editTxt:setMaxLength(12)
    editTxt:setFontColor(cc.c4b(255,255,255,255))
    editTxt:setFontName("ttf/jcy.ttf")
    if UpdateFunctions.platform == "ios" then
        editTxt:setPlaceholderFontSize(16)
    else
        editTxt:setPlaceholderFontSize(24)
    end
	editTxt:setPlaceholderFontColor(cc.c4b(184,187,182,255))  
	editTxt:setPlaceHolder("请输入玩家ID")
    editTxt:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE)
    editTxt:setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE)
    editTxt:registerScriptEditBoxHandler(function(eventname,sender) self:editboxHandle1(eventname,sender) end)
    self.image_input1:addChild(editTxt,5)  
    self.textField_search = editTxt

    local editTxt = nil
   if UpdateFunctions.platform == "ios" then
        editTxt = ccui.EditBox:create(cc.size(120,45), "hall/image/bank/zcm_zc1_2.png")
    else
        editTxt = ccui.EditBox:create(cc.size(120,70), "hall/image/bank/zcm_zc1_2.png")
    end
    editTxt:setName("inputTxt2")  
    editTxt:setAnchorPoint(0.5,0.5)  
    editTxt:setPosition(cc.p(self.image_input2:getPositionX() / 2+30  ,30 ))
    editTxt:setFontSize(26)
  --  editTxt:setMaxLength(12)
    editTxt:setFontColor(cc.c4b(255,255,255,255))
    editTxt:setFontName("ttf/jcy.ttf")
    if UpdateFunctions.platform == "ios" then
        editTxt:setPlaceholderFontSize(16)
    else
        editTxt:setPlaceholderFontSize(24)
    end
	editTxt:setPlaceholderFontColor(cc.c4b(184,187,182,255))  
	editTxt:setPlaceHolder("搜索")
    editTxt:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE)
    editTxt:setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE)
    editTxt:registerScriptEditBoxHandler(function(eventname,sender) self:editboxHandle2(eventname,sender) end)
    self.image_input2:addChild(editTxt,5)  
    self.textField_search1 = editTxt

        local editTxt = nil
   if UpdateFunctions.platform == "ios" then
        editTxt = ccui.EditBox:create(cc.size(120,45), "hall/image/bank/zcm_zc1_2.png")
    else
        editTxt = ccui.EditBox:create(cc.size(120,70), "hall/image/bank/zcm_zc1_2.png")
    end
    editTxt:setName("inputTxt2")  
    editTxt:setAnchorPoint(0.5,0.5)  
    editTxt:setPosition(cc.p(self.image_input2:getPositionX() / 2+20  ,10 ))
    editTxt:setFontSize(26)
  --  editTxt:setMaxLength(12)
    editTxt:setFontColor(cc.c4b(255,255,255,255))
    editTxt:setFontName("ttf/jcy.ttf")
    if UpdateFunctions.platform == "ios" then
        editTxt:setPlaceholderFontSize(16)
    else
        editTxt:setPlaceholderFontSize(24)
    end
	editTxt:setPlaceholderFontColor(cc.c4b(255,255,255,255))  
	editTxt:setPlaceHolder("创建群组")
    editTxt:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE)
    editTxt:setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE)
    editTxt:registerScriptEditBoxHandler(function(eventname,sender) self:editboxHandle4(eventname,sender) end)
    self.image_input4:addChild(editTxt,5)  
    self.m_groupNameText = editTxt


          local editTxt = nil
   if UpdateFunctions.platform == "ios" then
        editTxt = ccui.EditBox:create(cc.size(120,45), "hall/image/bank/zcm_zc1_2.png")
    else
        editTxt = ccui.EditBox:create(cc.size(120,70), "hall/image/bank/zcm_zc1_2.png")
    end
    editTxt:setName("inputTxt2")  
    editTxt:setAnchorPoint(0.5,0.5)  
    editTxt:setPosition(cc.p(self.image_input2:getPositionX() / 2  ,20 ))
    editTxt:setFontSize(26)
  --  editTxt:setMaxLength(12)
    editTxt:setFontColor(cc.c4b(255,255,255,255))
    editTxt:setFontName("ttf/jcy.ttf")
    if UpdateFunctions.platform == "ios" then
        editTxt:setPlaceholderFontSize(16)
    else
        editTxt:setPlaceholderFontSize(24)
    end
	editTxt:setPlaceholderFontColor(cc.c4b(255,255,255,255))  
	editTxt:setPlaceHolder("搜索联系人")
    editTxt:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE)
    editTxt:setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE)
    editTxt:registerScriptEditBoxHandler(function(eventname,sender) self:editboxHandle3(eventname,sender) end)
    self.panel_input:addChild(editTxt,5)  
    self.m_searchText = editTxt

    self.curSpeakingContent = self.scrollview_chatRoom
    self:updateEmoteList()
    self:updateMessageList(self.m_curRoomId) -- 大厅消息
    self:updatePlayerList(self.m_curRoomId) -- 大厅成员
    self.m_panelList ={self.panel_chatroom,self.panel_chat,self.panel_friend}
    self.m_scrollViewList ={self.scrollview_chatRoom,self.scrollview_chat,self.scrollview_friendchat}
    self:showBtnList()
    self:updateApplyFriendList() -- 好友申请列表
    self:updateChatList() -- 聊天列表
    self:updateFriendList()  -- 好友列表

    if ChatMgr:getInstance():getAddFriendFlag()  then
        self.image_dot1:setVisible(true)
        self.btns[3]:getChildByName("image_dot2"):setVisible(true)
    end
    if  ChatMgr:getInstance():getAddGroupFlag() then
        self.image_dot0:setVisible(true)
        self.btns[3]:getChildByName("image_dot2"):setVisible(true)
    end
    if ChatMgr:getInstance():getNewMessageFlag() or (not ChatMgr:getInstance():isAllReadMsg()) then 
        self.btns[2]:getChildByName("image_dot2"):setVisible(true)
    end


end
function ChatLayer:showFriendMsg(userId)
    self.curSpeakingContent:removeAllChildren()
    local msgList = ChatMgr:getInstance():getFriendMsg(userId)
    if msgList and #msgList>0 then  
        for k, item in pairs(msgList) do
            self:showMessage(item)
        end
    end
end
function ChatLayer:updateChatList()
    local msgList = ChatMgr:getInstance():getFriendMsgList() 
    if msgList == nil then
        return
    end
    self.listview_chat:removeAllItems()
    print("updateChatList")
    --dump(msgList)
    for k,v in pairs(msgList) do
        self:addMsgItem(v) 
    end
end
function ChatLayer:addMsgItem(player)  
    local t = self.panel_friendCell:clone() 
    local image_head = t:getChildByName("image_head") 
    local image_frame = t:getChildByName("image_frame")
    local image_vip = t:getChildByName("image_vip")
    local text_name = t:getChildByName("text_name")
    if player.m_bGroup then
        image_head:loadTexture("hall/image/chat/group/qun.png");  
        image_head:setScale(1.5)
        image_frame:setVisible(false) 
        image_vip:setVisible(false)  
        
    else
        local str =ToolKit:getHead(player.m_head)   
         image_head:loadTexture(str,ccui.TextureResType.plistType);  
        local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", player.m_vip)
        image_frame:loadTexture(framePath, ccui.TextureResType.plistType)
        local vip_path = string.format("hall/plist/vip/img-vip-v%d.png", player.m_vip)
        image_vip:loadTexture(vip_path, ccui.TextureResType.plistType)
        image_vip:ignoreContentAdaptWithSize(true)
        image_head:setScale(0.6)
        image_frame:setScale(0.6)
        image_vip:setScale(0.6)  
        t.m_head = player.m_head
        t.m_vip =  player.m_vip 
        
    end
    local image_dot = t:getChildByName("image_dot")
    image_dot:setVisible(not player.m_bRead)
    text_name:setString(player.m_name) 
     t.m_userid = player.m_userid
     t.m_bGroup = player.m_bGroup 
     t.m_name = player.m_name
     t:addTouchEventListener(handler(self, self.onShowMsg))
    self.listview_chat:pushBackCustomItem(t)
 end

 function ChatLayer:onShowMsg(sender,eventType) 
    if eventType== ccui.TouchEventType.ended then   
        if sender.m_bGroup then
            self.m_curSelectId  = nil
            self.m_curGroupId = tonumber(sender.m_userid)
            ChatMgr:getInstance():setIsReadMsg(self.m_curGroupId)
        else
            
            self.m_curSelectId  = tonumber(sender.m_userid)
            self.m_curGroupId = nil
            ChatMgr:getInstance():setIsReadMsg(self.m_curSelectId)
        end
        self:cleanSelect(self.listview_chat)
        local items = self.listview_chat:getItems()
        local flag =false
        for k,v in pairs(items) do 
             local image_select = v:getChildByName("select")
            image_select:setVisible(false)
            local image_unselect = v:getChildByName("unselect")
            image_unselect:setVisible(true) 
             local image_dot = sender:getChildByName("image_dot")  
        end
       
        local image_select = sender:getChildByName("select")
        image_select:setVisible(true)
        local image_unselect = sender:getChildByName("unselect")
        image_unselect:setVisible(false)
        local image_dot = sender:getChildByName("image_dot") 
        image_dot:setVisible(false)
        
         for k,v in pairs(items) do  
            if image_dot:isVisible() then
                flag = true
            end
        end
         ChatMgr:getInstance():setNewMessageFlag(flag)
        if not flag then
            self.btns[2]:getChildByName("image_dot2"):setVisible(false)
        end
        
        self.panel_block:setVisible(false)
        self.textField_name:setPlaceHolder("请输入文字")
        self:showFriendMsg(sender.m_userid) 
        self.btn_clear:setVisible(true)
    end
end
function ChatLayer:updateMessageList(roomID)
     local msgList = ChatMgr:getInstance():getRoomMsgList(roomID) 
     if msgList == nil then
        return
    end
    self.curSpeakingContent:removeAllChildren()
     for k, item in pairs(msgList) do
        self:showMessage(item)
    end
end
 function ChatLayer:updatePlayerList(roomID)
    local playerList = ChatMgr:getInstance():getRoomPlayerList(roomID) or {}
    -- if playerList == nil then
    --     return
    -- end
    -- table.insert(playerList, {m_name = Player:getNickName(), m_userid = Player:getAccountID(), m_vip = 1, m_head = Player:getFaceID()})
    self.listview_player:removeAllItems()
    for k,v in pairs(playerList) do
        self:addPlayer(v) 
    end
 end

 function ChatLayer:addPlayer(player)
    if player == nil then
        return
    end
    local t = self.panel_vipCell:clone() 
    local image_head = t:getChildByName("image_head")
    local str =ToolKit:getHead(player.m_head)   
    image_head:loadTexture(str,ccui.TextureResType.plistType);  
    
    local image_frame = t:getChildByName("image_frame")
    local image_vip = t:getChildByName("image_vip")
    local text_name = t:getChildByName("text_name")
    local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", player.m_vip)
    image_frame:loadTexture(framePath, ccui.TextureResType.plistType)
    local vip_path = string.format("hall/plist/vip/img-vip-v%d.png", player.m_vip)
    image_vip:loadTexture(vip_path, ccui.TextureResType.plistType)
    image_vip:ignoreContentAdaptWithSize(true)
    image_head:setScale(0.3)
    image_frame:setScale(0.3)
    image_vip:setScale(0.3)
    text_name:setString(player.m_name)
    t.m_userid = player.m_userid
    self.listview_player:pushBackCustomItem(t)
 end
 function ChatLayer:removePlayer(roomID,userid)  
     for k,v in pairs( self.listview_player:getItems()) do  
        if v.m_userid == userid then  
            self.listview_player:removeItem(k-1)
        end
    end
 end
 function ChatLayer:updateEmoteList()
  
    self.ListView_Emote:removeAllItems();
    self.orderList ={}
    local orderNum = math.floor(table.nums(EmoteList)/4)
    local leftNum = table.nums(EmoteList) % 4
    for k=1,orderNum do
        self.orderList[k]={}
        for i=1,4 do
            table.insert(self.orderList[k],EmoteList[4*(k-1)+i])
        end
    end 
    if leftNum~=0 then
        self.orderList[orderNum+1]={}
        for k=1,leftNum do 
            table.insert(self.orderList[orderNum+1],EmoteList[4*orderNum+k])
        end
    end  
	for k,v in pairs(self.orderList) do 
		local panel = self:getOneItem(v,k); 
		self.ListView_Emote:pushBackCustomItem(panel);
	end
end 
function ChatLayer:getOneItem(userInfo,index)
    local func = function(sender, eventType)
         if eventType == ccui.TouchEventType.ended then
            local tag = sender:getTag()
            local str= self.textField_name:getText()
            str =str..EmoteList[tag]
            self.textField_name:setText(str)
             self.Image_Emote:setVisible(false)
        end
     end
    local csbNode = self.panel_emotCell:clone()
    for k=1,4 do 
        local emote = csbNode:getChildByName("emote_"..k)   
        emote:loadTexture(string.format("hall/image/chat/emotion/chat_face_%d.png",4*(index-1)+k))
        emote:setTag(4*(index-1)+k)
        emote:addTouchEventListener(func)
	end
    return csbNode
end 
 function ChatLayer:editboxHandle(strEventName,sender)  
    if strEventName == "began" then  
    	sender:setPlaceHolder("")                                    --光标进入，清空内容/选择全部  
    elseif strEventName == "ended" then  
        if sender:getText() then
        	sender:setPlaceHolder("请输入文字") 
        end                                                        --当编辑框失去焦点并且键盘消失的时候被调用  
    elseif strEventName == "return" then  
         print(sender:getText(), "sender")                         --当用户点击编辑框的键盘以外的区域，或者键盘的Return按钮被点击时所调用  
    elseif strEventName == "changed" then
    	 print(sender:getText(), "sender")                         --输入内容改变时调用   
    end  
end  
 function ChatLayer:editboxHandle1(strEventName,sender)  
    if strEventName == "began" then  
    	sender:setPlaceHolder("")                                    --光标进入，清空内容/选择全部  
    elseif strEventName == "ended" then  
        if sender:getText() then
        	sender:setPlaceHolder("请输入玩家ID") 
        end                                                        --当编辑框失去焦点并且键盘消失的时候被调用  
    elseif strEventName == "return" then  
         print(sender:getText(), "sender")                         --当用户点击编辑框的键盘以外的区域，或者键盘的Return按钮被点击时所调用  
    elseif strEventName == "changed" then
    	 print(sender:getText(), "sender")                         --输入内容改变时调用   
    end  
end  
 function ChatLayer:editboxHandle2(strEventName,sender)  
    if strEventName == "began" then  
    	sender:setPlaceHolder("")                                    --光标进入，清空内容/选择全部  
    elseif strEventName == "ended" then  
        if sender:getText() then
        	sender:setPlaceHolder("搜索") 
        end                                                        --当编辑框失去焦点并且键盘消失的时候被调用  
    elseif strEventName == "return" then  
         print(sender:getText(), "sender")                         --当用户点击编辑框的键盘以外的区域，或者键盘的Return按钮被点击时所调用  
    elseif strEventName == "changed" then
        local playerList = ChatMgr:getInstance():getFriendList() 
        local findList = {}
         for k,v in pairs(playerList) do
            if string.find(v.m_name,sender:getText()) then
                table.insert(findList,v)
            end
        end  
        if #findList ==0 then 
            self:updateFriendList() 
        else
             self.listview_friend:removeAllItems()
            for k,v in pairs(findList) do
                self:addFriend(v) 
            end 
        end 
    	 print(sender:getText(), "sender")                         --输入内容改变时调用   
    end  
end  
 function ChatLayer:editboxHandle3(strEventName,sender)  
    if strEventName == "began" then  
    	sender:setPlaceHolder("")                                    --光标进入，清空内容/选择全部  
    elseif strEventName == "ended" then  
        if sender:getText() then
        	sender:setPlaceHolder("搜索联系人") 
        end                                                        --当编辑框失去焦点并且键盘消失的时候被调用  
    elseif strEventName == "return" then  
         print(sender:getText(), "sender")                         --当用户点击编辑框的键盘以外的区域，或者键盘的Return按钮被点击时所调用  
    elseif strEventName == "changed" then
        local playerList = ChatMgr:getInstance():getFriendList() 
        local findList = {}
         for k,v in pairs(playerList) do
            if string.find(v.m_name,sender:getText()) then
                table.insert(findList,v)
            end
        end  
        if #findList ==0 then 
            self:updateGroupFriendList() 
        else
             self.listview_groupfriend:removeAllItems()
            for k,v in pairs(findList) do
                self:addGroupFriend(v) 
            end 
        end 
    	 print(sender:getText(), "sender")                         --输入内容改变时调用   
    end  
end  
 function ChatLayer:editboxHandle4(strEventName,sender)  
    if strEventName == "began" then  
    	sender:setPlaceHolder("")                                    --光标进入，清空内容/选择全部  
    elseif strEventName == "ended" then  
        if sender:getText() then
        	sender:setPlaceHolder("创建群组") 
        end                                                        --当编辑框失去焦点并且键盘消失的时候被调用  
    elseif strEventName == "return" then  
         print(sender:getText(), "sender")                         --当用户点击编辑框的键盘以外的区域，或者键盘的Return按钮被点击时所调用  
    elseif strEventName == "changed" then
    	 print(sender:getText(), "sender")                         --输入内容改变时调用   
    end  
end  
  function ChatLayer:updateFriendList()
    local playerList = ChatMgr:getInstance():getFriendList() 
    if playerList == nil then
        return
    end
    self.listview_friend:removeAllItems()
    for k,v in pairs(playerList) do
        self:addFriend(v) 
    end
 end
function ChatLayer:updateGroupFriendList()
    local playerList = ChatMgr:getInstance():getFriendList() 
    if playerList == nil then
        return
    end
    self.listview_groupfriend:removeAllItems()
    for k,v in pairs(playerList) do
        self:addGroupFriend(v) 
    end
 end
  function ChatLayer:addGroupFriend(player)   
    local t = self.panel_groupCell:clone() 
    local image_head = t:getChildByName("image_head")
    local str =ToolKit:getHead(player.m_head)   
    image_head:loadTexture(str,ccui.TextureResType.plistType);  
    
    local image_frame = t:getChildByName("image_frame")
    local image_vip = t:getChildByName("image_vip")
    local text_name = t:getChildByName("text_name")
    local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", player.m_vip)
    image_frame:loadTexture(framePath, ccui.TextureResType.plistType)
    local vip_path = string.format("hall/plist/vip/img-vip-v%d.png", player.m_vip)
    image_vip:loadTexture(vip_path, ccui.TextureResType.plistType)
    image_vip:ignoreContentAdaptWithSize(true)
    image_head:setScale(0.5)
    image_frame:setScale(0.5)
    image_vip:setScale(0.5)
    text_name:setString(player.m_name)
    t.m_userid = player.m_userid
    t.m_head = player.m_head
    t.m_vip = player.m_vip
    t.m_name = player.m_name
    if self.m_curGroupMember then
        for k,v in pairs(self.m_curGroupMember) do
            if v.m_userid == player.m_userid then
                local image_select = t:getChildByName("select")
                local image_unselect = t:getChildByName("unselect")
                image_select:setVisible(true)
                image_unselect:setVisible(false)
                t:setEnabled(false)
            end
        end
    end
     t:addTouchEventListener(handler(self, self.onSelectEvent))
    self.listview_groupfriend:pushBackCustomItem(t)
 end

  function ChatLayer:onSelectEvent(sender,eventType)
    if eventType== ccui.TouchEventType.ended then   
        local image_select = sender:getChildByName("select")
        local image_unselect = sender:getChildByName("unselect")
        image_select:setVisible(not image_select:isVisible())
        image_unselect:setVisible(not image_unselect:isVisible())
    end
  end
 function ChatLayer:addFriend(player)   
    local t = self.panel_friendCell:clone() 
    local image_head = t:getChildByName("image_head")
    local str =ToolKit:getHead(player.m_head)   
    image_head:loadTexture(str,ccui.TextureResType.plistType);  
    
    local image_frame = t:getChildByName("image_frame")
    local image_vip = t:getChildByName("image_vip")
    local text_name = t:getChildByName("text_name")
    local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", player.m_vip)
    image_frame:loadTexture(framePath, ccui.TextureResType.plistType)
    local vip_path = string.format("hall/plist/vip/img-vip-v%d.png", player.m_vip)
    image_vip:loadTexture(vip_path, ccui.TextureResType.plistType)
    image_vip:ignoreContentAdaptWithSize(true)
    image_head:setScale(0.5)
    image_frame:setScale(0.5)
    image_vip:setScale(0.5)
    text_name:setString(player.m_name)
    t.m_userid = player.m_userid
    t.m_head = player.m_head
    t.m_vip = player.m_vip
    t.m_name = player.m_name   
    t.m_StatusString = player.m_StatusString
   
    t:addTouchEventListener(handler(self, self.onBtnTouchEvent))
    local image_line = ccui.ImageView:create()
    image_line:loadTexture("hall/image/chat/line2.png",ccui.TextureResType.localType);  
    
    self.listview_friend:pushBackCustomItem(t)
    local playerList = ChatMgr:getInstance():getFriendList() 
    for k,v in pairs(playerList) do
        if player.m_userid == v.m_userid then
            if playerList[k+1] then
                if player.m_StatusString ~=playerList[k+1].m_StatusString then
                     image_line:setScaleY(10)
                end
            else
                if player.m_StatusString == "1121111111" then
                    image_line:setScaleY(10)
                end
            end
        end
    end
    self.listview_friend:pushBackCustomItem(image_line)
 end

function ChatLayer:updateApplyFriendList()
    local playerList = ChatMgr:getInstance():getApplyFriendList()  
    if playerList == nil then
        return
    end
    self.listview_addFriend:removeAllItems()
    for k,v in pairs(playerList) do
        self:addApplyFriend(v.m_user_info1) 
    end
end

 function ChatLayer:addApplyFriend(player)
    local t = self.panel_addCell:clone() 
    local image_head = t:getChildByName("image_head")
    local str =ToolKit:getHead(player.m_head)   
    image_head:loadTexture(str,ccui.TextureResType.plistType);  
    
    local image_frame = t:getChildByName("image_frame")
    local image_vip = t:getChildByName("image_vip")
    local text_name = t:getChildByName("text_name")
    local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", player.m_vip)
    image_frame:loadTexture(framePath, ccui.TextureResType.plistType)
    local vip_path = string.format("hall/plist/vip/img-vip-v%d.png", player.m_vip)
    image_vip:loadTexture(vip_path, ccui.TextureResType.plistType)
    image_vip:ignoreContentAdaptWithSize(true)
    image_head:setScale(0.6)
    image_frame:setScale(0.6)
    image_vip:setScale(0.6)
    text_name:setString(player.m_name)
    t.m_userid = player.m_userid
    t.m_head = player.m_head
    t.m_vip = player.m_vip
    t.m_name = player.m_name 
 --    t:addTouchEventListener(handler(self, self.onBtnTouchEvent))
    local button_addFriend =  t:getChildByName("button_addFriend")
    button_addFriend:addTouchEventListener(handler(self, self.onAgree))
    button_addFriend.m_userid = player.m_userid
    
    local button_delFriend =  t:getChildByName("button_delFriend")
    button_delFriend.m_userid = player.m_userid
    button_delFriend:addTouchEventListener(handler(self, self.onRefuse))
    self.listview_addFriend:pushBackCustomItem(t)
 end
function ChatLayer:onAgree(sender,eventType) 
    if eventType== ccui.TouchEventType.ended then   
        GlobalIMController:AddFriend_Final_Req(sender.m_userid,0)
    end
end
function ChatLayer:onRefuse(sender,eventType) 
    if eventType== ccui.TouchEventType.ended then  
        GlobalIMController:AddFriend_Final_Req(sender.m_userid,1)
    end
end
function ChatLayer:cleanSelect(view)
    local items = view:getItems()
        
    for k,v in pairs(items) do 
        
        local image_select = v:getChildByName("select")
        if image_select then
            image_select:setVisible(false)
            local image_unselect = v:getChildByName("unselect")
            image_unselect:setVisible(true)
        end
            
    end
end
 function ChatLayer:onBtnTouchEvent(sender,eventType) 
    
    if eventType== ccui.TouchEventType.ended then  
        local userId = sender.m_userid
         self.m_curUserId = userId
         self.m_curSelectId  = tonumber(userId)
           self.m_curGroupId = nil
            self:cleanSelect(self.listview_friend)
            local image_select = sender:getChildByName("select")
            image_select:setVisible(true)
            local image_unselect = sender:getChildByName("unselect")
            image_unselect:setVisible(false)
            self.button_newFriend:loadTextureNormal("hall/image/chat/xdpy_n.png", ccui.TextureResType.localType)
            self.button_group:loadTextureNormal("hall/image/chat/ql_n.png", ccui.TextureResType.localType)
         if sender.m_StatusString == "1121111111" then
           
            self:onTouchCallback(self.button_msg)
        else
            
            self.panel_selectFriend:setVisible(true)
            self.panel_group:setVisible(false)
            self.panel_addFriendList:setVisible(false)
            self.panel_serchFriend:setVisible(false)
            self.panel_friendchat:setVisible(false) 
            self.panel_block:setVisible(true)
            self.textField_name:setPlaceHolder("请输入文字")
            
            local image_head = self.panel_selectFriend:getChildByName("image_head")
            local str =ToolKit:getHead(sender.m_head)   
            image_head:loadTexture(str,ccui.TextureResType.plistType);  
    
            local image_frame = self.panel_selectFriend:getChildByName("image_frame")
            local image_vip = self.panel_selectFriend:getChildByName("image_vip")
            local text_name = self.panel_selectFriend:getChildByName("text_name") 
            local framePath = string.format("hall/plist/userinfo/gui-frame-v%d.png", sender.m_vip)
            image_frame:loadTexture(framePath, ccui.TextureResType.plistType)
            local vip_path = string.format("hall/plist/vip/img-vip-v%d.png", sender.m_vip)
            image_vip:loadTexture(vip_path, ccui.TextureResType.plistType)
            image_vip:ignoreContentAdaptWithSize(true)
            text_name:setString(sender.m_name)  

          
        end
       --  ConnectManager:send2Server(Protocol.ChatServer, "CS_C2M_Friend_DelOneFriend_Req", {userId}) 
    end
end
 
 function ChatLayer:removeFriend(userid)  
     for k,v in pairs( self.listview_friend:getItems()) do 
        if v.m_userid == userid then 
            self.listview_friend:removeItem(k-1)
        end
    end
 end
function ChatLayer:pushBackItemInScrollView(chatResponse, isChatRecord) 
    local userId = chatResponse["userId"] 
    local chatType = chatResponse["chatType"] 
    local chatStr = chatResponse["chatStr"] 
    local isSelf = chatResponse["isSelf"] 
    local dialogTime = chatResponse["dialogTime"] 
    local rawUrl = chatResponse["rawUrl"] 
    local msgSec = chatResponse["msgSec"] 
    local gamename = chatResponse["gameName"] 
    local gameRoomId = chatResponse["gameRoomId"] 
    local timeStamp =  chatResponse["index"] 
    local faceUrl =  chatResponse["faceUrl"] 

    --  //非聊天记录阶段才缓存 聊天集录

    local chatContent = nil; 

    if(not isChatRecord) then 
            self.chatRecordData={}
           
        self.chatRecordData.pushback(chatResponse);
    end
         
          
    if chatType == 1 then
        chatContent = self:createWorldType(chatStr, isSelf); 
    elseif chatType == 2 then
        chatContent = self:createEmotionType(chatStr, isSelf); 
    elseif chatType == 3 then
        chatContent = self:createImageType(chatStr, isSelf); 
    elseif chatType == 4 then
        chatContent = self:createImageMenuType(chatStr, isSelf, gamename, gameRoomId, userId, timeStamp); 
    elseif chatType == 100 then 
        chatContent = self:createStartTimeImage(dialogTime); 
    elseif chatType == 6 then
        chatContent = self:createVoiceType(chatStr, isSelf, msgSec, userId); 
    elseif chatType == 101 then 
        chatContent = self:createSpeekDialog();
    end 

--        // 加载网络图片比较特别因为加载远程图片需要 加载完毕初始化完成才能设置位置
--        if(chatType == 5){
--                local remoteUrl = chatStr ? chatStr : rawUrl;
--                //如果自己 直接从本地获取图片 其他人只能从远端下载
--                ngc.pubUtils.loadImgFromUrl(remoteUrl, function (chatContent) {
--                    chatContent.setAnchorPoint(isSelf ? 1 : 0, 1);
--                    chatContent.x = isSelf ? curSpeakingContent.width - 100 :  59/2 + 70;
--                    chatContent.rawUrl = rawUrl;
--                    curSpeakingContent.addChild(chatContent);
--                    //加载玩家头像
--                    ngc.pubUtils.loadImgFromUrl(ngc.curUser.baseInfo.faceUrl , function (iconSprite) {
--                        iconSprite.setScale(0.6);
--                        local clipNode = ngc.uiUtils.cliperNative("res/Common/clipBg.png");
--                        clipNode.addChild(iconSprite);
--                        local stencilWidth = iconSprite.width/2;
--                        clipNode.attr({
--                            x:isSelf ? chatContent.width + stencilWidth - 15 : -stencilWidth + 15 ,
--                            y:chatContent.height - 22,
--                        });
--                        clipNode.setScale(0.8);
--                        chatContent.addChild(clipNode);
--                    }.bind(this));

--                    self:updateScrollView(curSpeakingContent);
--                    local that = this;
--                    //精灵创建 按钮监听
--                    ngc.pubUtils.addTouchClickInSprite(chatContent, false, function (target) {
--                        that.lookRawImage(target.rawUrl);
--                    });
--                    //更新控件布局
--                }.bind(this));
--        }else{
--            if(!chatContent && !curSpeakingContent) return;
        self.curSpeakingContent:addChild(chatContent);
        local child = self.curSpeakingContent:getChildren() 
        if(chatType == 100 or chatType == 101)then
            chatContent.x = self.curSpeakingContent.width/2 - 5;
            self:updateScrollView(self.curSpeakingContent);
            return;
        end
            local x = 0
        if isSelf then
            chatContent.x = self.curSpeakingContent.width - 100 
            x=  chatContent.width + stencilWidth - 15
        else
                chatContent.x =59/2 + 70
                x=  -stencilWidth + 15
        end

        local str =ToolKit:getHead(headID) 
        --    //加载头像
        local image_head = ccui.ImageView:create()
        image_head:loadTexture(str,ccui.TextureResType.plistType);  
        image_head:setPosition(x,chatContent.height - 22)
        image_head:setScale(0.8);
        chatContent:addChild(image_head)
--            ngc.pubUtils.loadImgFromUrl(isSelf ? ngc.curUser.baseInfo.faceUrl : faceUrl, function (iconSprite) {
--                iconSprite.setScale(0.6);
--                local clipNode = ngc.uiUtils.cliperNative("res/Common/clipBg.png");
--                clipNode.addChild(iconSprite);
--                local stencilWidth = iconSprite.width/2;
--                clipNode.attr({
--                    x:isSelf ? chatContent.width + stencilWidth - 15 : -stencilWidth + 15 ,
--                    y:chatContent.height - 22,
--                });
--                clipNode.setScale(0.8);
--                chatContent.addChild(clipNode);
--            }.bind(this));
        self:updateScrollView(self.curSpeakingContent); 
end
function    ChatLayer:updateScrollView (curSpeakingContent) 
        local offSetHeight = self:resetAndGetScrollViewItemPos(curSpeakingContent);
        curSpeakingContent:setInnerContainerSize(cc.size(curSpeakingContent:getInnerContainerSize().width,offSetHeight));
        self:resetAndGetScrollViewItemPos(curSpeakingContent);
 end

function    ChatLayer:resetAndGetScrollViewItemPos(curSpeakingContent) 
    local offSetHeight = 50;
    local curScroll = curSpeakingContent;
    local allChildren = curScroll:getChildren(); 
    for k=#allChildren,1,-1 do
        local chatContent = allChildren[k];
        chatContent:setPositionY(offSetHeight+ chatContent:getContentSize().height);
        offSetHeight =offSetHeight+ (chatContent:getContentSize().height +  50);
    end
    curScroll:jumpToBottom(); 
    return offSetHeight;
 end
function ChatLayer:createWorldType(chatStr, isSelf)  
    local chatWorldType = ChatWorldType.new(chatStr, isSelf)
    return chatWorldType
end
 function ChatLayer:showBtnList()  
    local list = {"综\n合","聊\n天","通\n讯\n录"}
    for k,v in pairs(list) do
        self:creatGameBtn(k,v)
    end
    self:onShowPanel(self.btns[1],ccui.TouchEventType.ended)
end 
function ChatLayer:creatGameBtn(index,name)
    
    local custom_button = self.panel_itemCell:clone() 
    custom_button:getChildByName("text_title"):setString(name)    
    custom_button.light =  custom_button:getChildByName("light")
    custom_button.unlight =  custom_button:getChildByName("unlight")
    custom_button.name = custom_button:getChildByName("text_title") 
    custom_button:addTouchEventListener(handler(self, self.onShowPanel))
    custom_button:setTag(index) 
     self.listview_btn:pushBackCustomItem(custom_button)
     table.insert(self.btns,custom_button) 
end

function ChatLayer:showPanel(index)
    self.Image_Emote:setVisible(false)
   
   -- self:onTouchCallback(self.btn_vip1)
    for k,v in pairs(self.m_panelList) do
        v:setVisible(false)
        if index == k then
            self.m_panelList[index]:setVisible(true)
            self.curSpeakingContent = self.m_scrollViewList[index]
        end
    end
end
function ChatLayer:isSendFriendState()
    if self.m_panelList[2]:isVisible() or self.m_panelList[3]:isVisible() then
        return true
    else
        return false
    end
end
function ChatLayer:onShowPanel(sender,eventType)  
    if eventType== ccui.TouchEventType.ended then  
        local roomTab = {GlobalConf.CHANNEL_ID,1,2}
        local index = sender:getTag()
                
        for k,v in pairs(self.btns) do
            v.light:setVisible(false)
            v.unlight:setVisible(true)
            v.name:setColor(cc.c3b(145,145,145))
        end
        sender.light:setVisible(true)
        sender.unlight:setVisible(false)
        sender.name:setColor(cc.c3b(255,255,255))  
        self:showPanel(index) 
        
         if roomTab[index] == self.m_curRoomId then
            return
        else
            self.m_curRoomId = roomTab[index]
        end 
        self.textField_name:setText("")  
        self.btn_clear:setVisible(false)
         if index == 2 then
            self:updateChatList()
            self.panel_block:setVisible(true)
            self.textField_name:setPlaceHolder("请输入文字")
            self.scrollview_chat:setVisible(false)
            self.btn_vip1:setVisible(false)
             self.btn_vip1:loadTextureNormal("hall/image/chat/5.png")
            self.m_bMenuMoving = false
            self.image_vipList:setVisible(false)
            self.image_vipList:setPosition(1230, 380)
             self.btn_vip1:setPosition(1206, 417)  
             self:cleanSelect(self.listview_chat)
        elseif index == 3 then
            self.panel_block:setVisible(true)
            self.textField_name:setPlaceHolder("请输入文字")
            self.panel_selectFriend:setVisible(false)
            self.panel_group:setVisible(false)
            self.panel_addFriendList:setVisible(false)
            self.panel_serchFriend:setVisible(false)
            self.panel_friendchat:setVisible(false) 
            self:cleanSelect(self.listview_friend)
            self.btn_vip1:setVisible(false)
               self.btn_vip1:loadTextureNormal("hall/image/chat/5.png")
            self.m_bMenuMoving = false
            self.image_vipList:setVisible(false)
            self.image_vipList:setPosition(1230, 380)
             self.btn_vip1:setPosition(1206, 417)  
        else
            self:updateMessageList(self.m_curRoomId)
            self.panel_block:setVisible(false)
             self.textField_name:setPlaceHolder("请输入文字")
            if ChatMgr:getInstance():getSpeakFlag() == 1 then
                self.textField_name:setPlaceHolder("全体禁言中")
                 self.panel_block:setVisible(true)
            end
            
            self.btn_vip1:setVisible(true)
        end
    end
end  

function ChatLayer:ButtonShake(button, delay)
    button:stopAllActions()
    button:setEnabled(false)
    local _delay = delay or 0.5
    button:runAction(cc.Sequence:create(cc.DelayTime:create(_delay), cc.CallFunc:create(function()
        button:setEnabled(true)
    end), nil))
end

return ChatLayer
