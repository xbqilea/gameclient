
require("config")
require("cocos.init")
require("framework.init")
require("app.hall.base.util.InterFaceUtil")
require("app.newHall.data.Player")


local MyApp = class("MyApp", cc.mvc.AppBase)

function MyApp:ctor()
    MyApp.super.ctor(self)
end

function MyApp:run()
    ToolKit:init()
    local LoginScene = require("app.hall.login.view.LoginScene")
    local scene = LoginScene.new()
    cc.Director:getInstance():replaceScene(scene)
   
end

function MyApp:onEnterBackground()
    print("---------------MyApp:onEnterBackground-------------")
    MyApp.super.onEnterBackground(self)
    TotalController:onEnterBackground()
end

function MyApp:onEnterForeground()
    print("************MyApp:onEnterForeground*************")
    MyApp.super.onEnterForeground(self)
    TotalController:onEnterForeground()
end
function MyApp:onCloseWindow()
    print("************MyApp:onEnterForeground*************")
    MyApp.super.onCloseWindow(self)
    TotalController:onExitApp()
end
return MyApp
