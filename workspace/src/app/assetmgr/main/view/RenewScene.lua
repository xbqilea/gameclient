--
-- Author: 
-- Date: 2018-07-27 11:42:15
--
local UpdateController = require("app.assetmgr.main.control.RenewController")
local UpdateScene = class("UpdateScene", function()
    return cc.Scene:create()
end)

function UpdateScene:ctor()
    self:myInit()
end

function UpdateScene:myInit()
    self:showLayer() -- 显示界面
    self.updateController = UpdateController.new(self)
	-- self.updateController:start(false)
    self.updateController:startPointUpdate(false)
end

function UpdateScene:UpdateSelf()
    package.loaded["main"] = nil
    require("main")
end


function UpdateScene:showLayer()
    self.updateLayer = require("app.assetmgr.main.view.RenewLayer").new()
    self:addChild(self.updateLayer, 10)
end

function UpdateScene:showLoadingBar( __params )
    self.updateLayer:showLoading(__params)
end

function UpdateScene:updateLoadingBar(__percent, __content)
    self.updateLayer:updateLoadingBar(__percent, __content)
end

function UpdateScene:showAlertDlg( __params )
    self.updateLayer:showAlertDlg(__params)
end

return UpdateScene