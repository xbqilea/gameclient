--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 更新显示界面

require("app.assetmgr.util.RenewUtil")
local UpdateLayer = class("UpdateLayer", function ()
    return cc.Layer:create()
end)

function UpdateLayer:ctor()
    self:myInit()
    self:setupViews()
end

function UpdateLayer:myInit()
    self.animationCompleteCallback = nil  
    self.winSize = cc.Director:getInstance():getWinSize()
    self.callback1 = nil 
    self.callback2 = nil
end

function UpdateLayer:setupViews()
    -- loading 界面 
    self.loginLayer = require("app.hall.login.view.LoginLayer").new()
    self:addChild(self.loginLayer, 100)
    self.loginLayer.root:getChildByName("layout_progress"):setVisible(false)
    --self.loginLayer:showPakcageInfo(false)
end

-- 显示进度条
function UpdateLayer:showLoading( __params )
    self:closeAlerDlg()
    if __params.files and #__params.files == 1 and type(__params.files[1]) == "table" and tostring(__params.files[1].name) == "src/app/hall/version.luac" then
    else
        self.loginLayer:setBottomVisible(true)
        self.loginLayer:showTipsStr(__params.content)
    end
end

-- 刷新进度条
function UpdateLayer:updateLoadingBar( __percent, __content )
    -- print(debug.traceback())
    self.loginLayer:showProgress(string.format("%2d", __percent))
    self.loginLayer:showTipsStr(__content)
end

-- 显示正在加载提示
function UpdateLayer:showZipContent( __content )
    self.loginLayer:showProgress(100)
    self.loginLayer:showTipsStr(__content)
end

-- 隐藏弹出提示框
function UpdateLayer:closeAlerDlg()
    if self.dlg then
        self.dlg:removeFromParent()
    end
end

-- 显示弹出提示框
function UpdateLayer:showAlertDlg( __params )
    self.loginLayer:setBottomVisible(false)

    self.dlg = cc.CSLoader:createNode("csb/common/update_tips_layer.csb") 
    self.dlg:setScale(self.loginLayer:getSizeScale())
    self:addChild(self.dlg, 100)
    -- self.dlg:setScale(self.loginLayer:getSizeScale())

    self.dlg:setPosition(self.winSize.width / 2, self.winSize.height / 2)

    local title = __params.title or "提示"
    local content = __params.content or ""
    local btnText1 = __params.btnText1 or "确定"
    local btnText2 = __params.btnText2 or "取消"
    self.callback1 = __params.callback1 or nil 
    self.callback2 = __params.callback2 or nil

    local labelTitle = self.dlg:getChildByName("txt_title")
    labelTitle:enableOutline({r = 255, g = 255, b = 0}, 10)
    local labelContent = self.dlg:getChildByName("txt_content")
    local btn1 = self.dlg:getChildByName("btn_1")
    local btn2 = self.dlg:getChildByName("btn_2")
    btn1:enableOutline({r = 7, g = 131, b = 13, a = 255}, 2)
    btn2:enableOutline({r = 179, g = 94, b = 11, a = 255}, 2)
    labelTitle:setString(title)
    labelContent:setString(content)
    btn1:setTitleText(btnText1)

    local layer1 = cc.LayerColor:create({r=0, g=0, b=0, a=100}, self.winSize.width, self.winSize.height)
    layer1:setPosition(-self.winSize.width/2, -self.winSize.height/2)
    layer1:setLocalZOrder(-1000)
    self.dlg:addChild(layer1)
    labelTitle:setOpacity(255)
    labelTitle:setLocalZOrder(btn1:getLocalZOrder() + 1)
    labelTitle:setFontSize(35)
    labelContent:enableOutline({r = 0, g = 0, b = 0}, 2)
    labelContent:setColor({r = 255, g = 255, b = 255})
    labelContent:setLocalZOrder(btn1:getLocalZOrder() + 1)

    local func = function ( __sender, __event )
        if __event == 2 then -- ccui.TouchEventType.ended
            if __sender:getName() == "btn_1" then
                if self.callback1 then
                    self.callback1()
                end
            elseif __sender:getName() == "btn_2" then
                if self.callback2 then
                    self.callback2()
                end
            end
        end
    end
    btn1:addTouchEventListener(func)
    if self.callback2 then
        btn2:setVisible(true)
        btn2:setTitleText(btnText2)
        btn2:addTouchEventListener(func)
    else
        btn2:setVisible(false)
        btn1:setPositionX(self.dlg:getChildByName("layout_dlg_tips"):getContentSize().width / 2)
    end

    -- run action
    self.dlg:setAnchorPoint({x = 0.5, y = 0.5})
    self.dlg:setScale(0.0)
    local sc = self.loginLayer:getSizeScale()
    local action1 = cc.ScaleTo:create(0.1, sc)
    local action2 = cc.ScaleTo:create(0.05, 0.9 * sc)
    local action3 = cc.ScaleTo:create(0.02, sc)
    self.dlg:runAction(cc.Sequence:create(action1, action2, action3))
end

return UpdateLayer
