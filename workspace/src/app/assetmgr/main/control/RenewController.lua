--
-- Author: 
-- Date: 2018-07-27 11:42:15
--
-- 检查更新的管理类，下载更新
local scheduler = require("framework.scheduler")
package.loaded["app.assetmgr.util.RenewUtil"] = nil
require("app.assetmgr.util.RenewUtil")
local UpdateController = UpdateFunctions.class("UpdateController")
local Updater = require("app.assetmgr.util.AssetDownload")

local PointUpdateUtil = require("app.hall.base.util.Silence")
local HttpUtil = require("app.hall.base.network.HttpWrap")

UpdateController.updatePath = cc.FileUtils:getInstance():getWritablePath() .. UpdateConfig.updateDirName -- 更新路径

function enter_game()
    require("app.hall.base.network.HttpWrap")
    require("app.MyApp").new():run()
    print("enter game**********************************************************")
end

local ADDRESS_TYPE = {
    ["MAIN"] = 1, 
    ["RESERVE"] = 2
}

local publicInfo = require("src.app.hall.config.design.MilCommonText")
local ServiceNumber = publicInfo["phonenub"].value
package.loaded["src.app.hall.config.design.MilCommonText"] = nil


function UpdateController:ctor(scene)
    self.scene = scene
    self:myInit()
end

function UpdateController:myInit()
    self.addressType = ADDRESS_TYPE.MAIN -- 初始化请求地址: 主地址开始
    self.reqApkType = ADDRESS_TYPE.MAIN -- 初始化安卓下载类型

    self.needPackages = {} -- 将要下载的模块
    self.flistDownCount = 0 -- 已下载的flist数量

    self.needDownloadFiles = {} -- 需要下载的列表
    self.needRemoveFiles = {} -- 需要下载列表
    self.needDownloadSize = 0 -- 需要下载的总大小
    self.hasDownloadSize = 0 -- 已下载的总大小
    self.currentDownloadIndex = 1 -- 当前下载文件下标

    self.isIntime = false -- 实时更新
    self.updateLobby = true -- 更新大厅
    self.updateGameId = nil -- 更新游戏时, 游戏id
end

function UpdateController:pointUpdateCallback( __client, __tag, event )
    if event.name == "completed" then
        local code = event.request:getResponseStatusCode()
        if code ~= 200 then
            print("failed with code: " .. code)
            if GlobalConf.POINT_URL_MAIN and __client.url == GlobalConf.POINT_URL_MAIN then -- 启用备用地址
				self:doStart(GlobalConf.POINT_URL)
				return
            end
            --qka.BuglyUtil:reportException("checkUpdateFail", "ec: " .. ec .. "\nip:" .. QkaPhoneInfoUtil:getPhoneIP())
            local params = {
                --content =  "检查更新失败(RC:" .. code .. "), 如有问题请联系客服,电话:"..ServiceNumber,
                content = "检查更新失败(RC:" .. code .. "), 如有问题请联系客服",
                btnText1 =  "确定",
                callback1 =  function ()
                    os.exit()
                end
            }
            self.scene:showAlertDlg(params)
            return
        end
        local response =  event.request:getResponseString() 
        print("response =", response)
          -- 先下载 版本管理文件
        if response then
            local info = require("cjson").decode(response)
            if info.ret == 0 then -- 成功
                self.updateInfoServer =  info.return_info
                -- self.updateInfoServer = "http://192.168.0.90:8080/mobile/20171108170623/UpdateInfo_U0001.lua"
                self.updateInfoServer = "http://testresources.xbqpthe.com/mobile/UpdateInfo.lua"
		        --self.updateInfoServer = "http://182.16.53.98:8085/mobile/UpdateInfo.lua"
                print("UpdateInfo = " .. self.updateInfoServer)
                local clear = info.return_clear 
                if clear and clear == 1 then
                    print("update need clear local data!!!")
                    self:removeCache()
                end

                self:updatePlatform() -- 更新平台
            else
                print("failed with code: " .. info.ret)
                -- enter_game()
                local params = {
                    content =  "由于您目前网络不稳定，更新失败，请改善网络状况，重新更新。",
                    btnText1 =  "确定",
                    callback1 =  function ()
                        cc.Director:getInstance():replaceScene(require("app.assetmgr.main.view.RenewScene").new())
                    end
                }
                self.scene:showAlertDlg(params)
            end
        end
    elseif event.name == "progress" then
        --print("progress", __tag)
    else
        if GlobalConf.POINT_URL_MAIN and __client.url == GlobalConf.POINT_URL_MAIN then -- 启用备用地址
			self:doStart(GlobalConf.POINT_URL)
			return
        end
        local ec = -1 
        if __client.getErrorCode then
            ec = tostring(__client:getErrorCode())
        end
        print("failed!! EC: ", ec)
        -- TOAST("检查更新失败!")
        local params = {
            --content =  "检查更新失败(EC:" .. ec .. "), 如有问题请联系客服,电话:"..ServiceNumber,
            content =  "检查更新失败(EC:" .. ec .. "), 如有问题请联系客服",
            btnText1 =  "确定",
            callback1 =  function ()
                os.exit()
            end
        }
        self.scene:showAlertDlg(params)
    end
end

------------------------ 定点更新获取更新信息地址 start -----------------------------------
function UpdateController:startPointUpdate( __isIntime )
    self.updateLobby = true
    self.isIntime = __isIntime
    
    local url = GlobalConf.POINT_URL
    if GlobalConf.POINT_URL_MAIN then
		url = GlobalConf.POINT_URL_MAIN
	end
    self:doStart(url)
end

function UpdateController:doStart( __url )
    self.updateInfoServer = "http://testresources.xbqpthe.com/mobile/UpdateInfo.lua"
    print("UpdateInfo = " .. self.updateInfoServer)

    self:updatePlatform() -- 更新平台
end

------------------------ 获取更新信息地址 start -----------------------------------
-- 开始执行
function UpdateController:start( __inTime )
    self.updateLobby = true
    self.isIntime = __inTime
    self:_requestAddressFromServer(UpdateConfig.UpdateHttpServer.mainurl, ADDRESS_TYPE.MAIN)
end

-- 请求更新地址
-- __requestUrl: 请求地址
-- __addressType: 请求类型(main, reserve)
function UpdateController:_requestAddressFromServer( __requestUrl, __addressType )
    print("_requestAddressFromServer: ", __requestUrl)
    self.addressType = __addressType
    local request = cc.HTTPRequest:createWithUrl(function(event) 
        self:_onAddressResponse(event, requestType)
        end, __requestUrl, cc.kCCHTTPRequestMethodGET) 

    request:setTimeout(3)--重试时间，不设默认为10
    request:start()
end

--2拿到更新文件地址
function UpdateController:_onAddressResponse(event, requestType)
    if event.name == "completed" then
        local code = event.request:getResponseStatusCode()
        if code ~= 200 then
            if self.addressType == ADDRESS_TYPE.MAIN then
                self:_requestAddressFromServer(UpdateConfig.UpdateHttpServer.reserveUrl, ADDRESS_TYPE.RESERVE)
            else 
                self:loadAddressFail()
            end
        end
        local response =  event.request:getResponseString() 
        print("response =",response)
          -- 先下载 版本管理文件
        if response then
            self.updateInfoServer =  UpdateFunctions.trim(response)
            -- self.updateInfoServer = "http://192.168.1.22:8028/mobile/20161112/UpdateInfo.lua" -- todo: test
            print("UpdateInfo = " .. self.updateInfoServer)
            self:updatePlatform() -- 更新平台
        end
    elseif event.name == "progress" then

    else
        print(event.request:getErrorCode(), event.request:getErrorMessage())
        if self.addressType == ADDRESS_TYPE.MAIN then
            self:_requestAddressFromServer(UpdateConfig.UpdateHttpServer.reserveUrl, ADDRESS_TYPE.RESERVE)
        else
            self:loadAddressFail()
        end
    end
end

function UpdateController:loadAddressFail()
    -- if self.forceUpdate then
    --     enter_game()
    -- else
        local publicInfo = require("src.app.hall.config.design.MilCommonText.lua")
        local params = {
            --content =  "您的网络不正常, 更新失败, 如有问题请联系客服,电话:"..ServiceNumber,
            content = "您的网络不正常, 更新失败, 如有问题请联系客服",
            btnText1 =  "确定",
            callback1 =  function ()
                os.exit()
            end
        }
        self.scene:showAlertDlg(params)
    -- end
end

------------------------ 获取更新信息地址 end -----------------------------------

------------------------ 更新平台 start -----------------------------------
-- 更新平台
function UpdateController:updatePlatform()
    print("Update start")
    UpdateFunctions.mkDir(UpdateFunctions.writablePath .. UpdateConfig.updateDirName)
    UpdateFunctions.mkDir(UpdateFunctions.writablePath .. UpdateConfig.updateDirName.."res/")
    UpdateFunctions.mkDir(UpdateFunctions.writablePath .. UpdateConfig.updateDirName.."src/")
    -- UpdateFunctions.mkDir(UpdateFunctions.writablePath .. UpdateConfig.updateDirName.."src/app/Updater/Flists")
    self.oldVersion =  UpdateFunctions.getAppVersionCode() -- 读取老版本号(Android)
    print("self.oldVersion: ", self.oldVersion)
    self:initOldAppVersion() -- 读取老版本信息
    local params = {
        url = self.updateInfoServer,
        path = UpdateFunctions.writablePath .. UpdateConfig.updateDirName .. UpdateConfig.gameVersionName,
        resType = UpdateConfig.RequestType.UPDATEINFO,
        timeout = 3,
        callback = function ( updater, status, progress )
            if status == UpdateConfig.UpdateStatus.SUCCESSED then
                self:onUpdateInfoFinished(updater.path)
            elseif status == UpdateConfig.UpdateStatus.FAILED then
                -- if cc.FileUtils:getInstance():isFileExist("res/UpdateInfo.lua") then
                --     self:onUpdateInfoFinished("res/UpdateInfo.lua")
                -- else
                    self._updateRetType = UpdateConfig.UpdateRetType.UPDATEINFO
                    self:updateFail()
                -- end
            end
        end,
    }
    local ud = Updater.new(params)
    if not ud:doUpdate() then
        self._updateRetType = UpdateConfig.UpdateRetType.UPDATEINFO
        self:updateFail()
    end
end

-- 初始化已有的版本信息
function UpdateController:initOldAppVersion()
    -- 先读upd 下
    local updfileName = UpdateFunctions.writablePath .. UpdateConfig.updateDirName .. UpdateConfig.gameVersionName
    if UpdateFunctions.isFileExist(updfileName) then
        self.updateInfo = UpdateFunctions.doFile(updfileName)
        if self.updateInfo and self.updateInfo.forceUpdate == 0 then -- 先重命名上次下载的文件内容
            self:renameAllFiles(1)
            self:unzipAllFiles(1)
        end
    else 
        self.updateInfo = nil
    end
end

-- 游戏更新信息下载完成
function UpdateController:onUpdateInfoFinished( __path )
    if cc.FileUtils:getInstance():isFileExist(__path) then -- 文件存在
        self.updateInfo = UpdateFunctions.doFile(__path) -- 加载新的UpdateInfo
        if not self.updateInfo then
            self._updateRetType = UpdateConfig.UpdateRetType.OTHER_ERROR 
            self:updateFail() -- 结束
            return
        else
            if not GlobalConf.UPDATELOBBY then --不更新大厅
                -- self._updateRetType = UpdateConfig.UpdateRetType.SUCCESSED
                if self.updateLobby then
                    if not self.isIntime then
                        enter_game()
                    else
                        -- todo
                    end
                end
                return
            end

            local channelInfo = self:getChannelInfo()
            if self.updateLobby then 
                if not self.isIntime then
                    if not self.forceUpdate then
                        enter_game() -- 后台更新, 直接进入游戏
                    else
                        -- self.scene.updateLayer:showAnimation()
                    end
                else
                    -- todo
                end
            end

            -- 更新版本文件下载成功
            if self.oldVersion  then
                -- 老版本与新的版本号不同，判断为新安装覆盖
                print("----- 新安装覆盖 -------")
                if UpdateFunctions.platform ~= "windows" and self.oldVersion < channelInfo.version then
                    UpdateFunctions.removePath(UpdateFunctions.writablePath .. "upd/res")
                    UpdateFunctions.removePath(UpdateFunctions.writablePath .. "upd/src")
                end
            end
            
            print("UpdateFunctions.platform = ", UpdateFunctions.platform)
            if UpdateFunctions.platform == "android" and GlobalConf.UPDATE_APK then -- Android
                print("android channelInfo.version = ", channelInfo.version)
                if self.oldVersion < channelInfo.version then
                    --判断SDCard里的apk版本是不是跟目标版本一致
                    local apkPath = "/sdcard/" .. UpdateConfig.apkName
                    if UpdateFunctions.isFileExist(apkPath) and UpdateFunctions.getApkVersion(apkPath) == channelInfo.version then
                        --一致则进入安装SDCard里的apk
                        self:showInstallApkDlg(function() self:onApkFinished(apkPath) end)
                        return
                    end
                    --下载apk包
                    --  判断SD卡剩余容量
                    local sdAvailableBytes = self:getAndroidSDAvailableBytes()
                    if sdAvailableBytes <  100*1024*1024 then -- 如果小于安装包大小，提示不能安装
                        if self._updateSDCardSizeSmallCallBack then
                            self._updateSDCardSizeSmallCallBack()
                            return
                        end
                    end
                    print("下载apk包 channelInfo.mainUrl = ", channelInfo.mainUrl)
                    local funcDownload = function ()
                        if self.forceUpdate then
                            self.scene:showLoadingBar({content = "正在更新,请稍后..."})
                        end
                        -- 下载apk
                        local params = {
                            url = self.updateInfo.downloadapkUrl.mainUrl,
                            backurl = self.updateInfo.downloadapkUrl.reserveUrl,
                            path = "sdcard/" .. UpdateConfig.apkName,
                            resType = UpdateConfig.RequestType.APK,
                            timeout = 3,
                            callback = function ( updater, status, progress )
                                self:onDownloadApk(updater, status, progress)
                            end,
                        }
                        local ud = Updater.new(params)
                        if not ud:doUpdate() then
                            self._updateRetType = UpdateConfig.UpdateRetType.NETWORK_ERROR
                            self:updateFail()
                        end
                    end
                    if self.forceUpdate then
                        self:showDownloadApkDlg(funcDownload, self.updateInfo.apkSize)
                    else
                        if UpdateFunctions.getNetType() == "wifi" then -- wifi 下直接下载
                            funcDownload()
                        end
                    end
                    
                else 
                    self:getAllFlist(1)
                end
            else-- 其他平台处理
                self:getAllFlist(1)
            end
        end
        if self.isIntime then
            self:updateAllGames()
        end
    else
        self._updateRetType = UpdateConfig.UpdateRetType.OTHER_ERROR 
        self:updateFail()
    end
end

function UpdateController:showDownloadApkDlg( __downloadFunc, __apkSize )
    local _content = "检测到有新版本, 是否更新?\n"
    if __apkSize ~= nil then
        _content = string.format("检测到有新版本, 是否更新?\n(更新大小: %dM)", __apkSize / 1024 / 1024)
    end
    local params = {
        content =  _content,
        btnText1 =  "下载安装",
        btnText2 =  "取消",
        callback1 =  function ()
            __downloadFunc()
        end,
        callback2 =  function ()
            if self.forceUpdate then
                os.exit()
            else
                self:getAllFlist(1)
            end
        end
    }
    self.scene:showAlertDlg(params)
end

function UpdateController:showInstallApkDlg(__installFuc)
    local _content = "新版本已经下好了, 是否安装?"
    local params = {
        content =  _content,
        btnText1 =  "安装",
        btnText2 =  "取消",
        callback1 =  function ()
            __installFuc()
        end,
        callback2 =  function ()
            if self.forceUpdate then
                os.exit()
            else
                self:getAllFlist(1)
            end
        end
    }
    self.scene:showAlertDlg(params)
end

-- 更新所有游戏
function UpdateController:updateAllGames()
    for k, v in pairs(self.updateInfo.package) do
        local packageName =v.flistPath
        local flistPath = UpdateFunctions.writablePath .. UpdateConfig.updateDirName .. packageName
        if k >= 100000 and cc.FileUtils:getInstance():isFileExist(flistPath) then
            self:updateGame( k )
        end
    end
end

-- 下载所有flist
function UpdateController:getAllFlist( __packageid )
    local packageid = __packageid
    self.flistDownCount = 0
    self.needDownloadFiles = {} -- 需要下载的列表
    self.needRemoveFiles = {} -- 需要下载列表
    self.needDownloadSize = 0 -- 需要下载的总大小
    self.hasDownloadSize = 0 -- 已下载的总大小
    while packageid ~= 0 do -- todo: package is not exist
        print("getAllFlist: with current id: " .. packageid)
        local packageName = self.updateInfo.package[packageid].flistPath
        local flistPath = UpdateFunctions.writablePath .. UpdateConfig.updateDirName .. packageName
        if not cc.FileUtils:getInstance():isFileExist(flistPath) then -- 本地文件不存在, 则读取UpdateInfo
            flistPath = self.updateInfo.package[packageid].flistPath
        end

        if self.updateLobby then -- 更新大厅
            if not cc.FileUtils:getInstance():isFileExist(flistPath) then -- 文件不存在
                if self.updateLobby then -- 大厅必须有flist
                    self._updateRetType = UpdateConfig.UpdateRetType.OTHER_ERROR
                    print("[ERROR]: local flist file not exist! ", flistPath)
                    self:updateFail()
                    return
                end
            end
            local oldFlist = UpdateFunctions.doFile(flistPath)
            if not oldFlist then
                self._updateRetType = UpdateConfig.UpdateRetType.OTHER_ERROR
                print("[ERROR]: load flist failed!")
                self:updateFail()
                return
            end

            if oldFlist.version < self.updateInfo.package[packageid].version then
                self:downLoadFlist(packageid)

                self.needPackages[#self.needPackages + 1] = packageid
            end
            packageid = self.updateInfo.package[packageid].nextPackageId
        else -- 更新游戏
            if not cc.FileUtils:getInstance():isFileExist(flistPath) then -- 文件不存在 下载整个游戏
                self:downLoadFlist(packageid)
                self.needPackages[#self.needPackages + 1] = packageid
                packageid = self.updateInfo.package[packageid].nextPackageId
            else -- flist文件存在, 对比版本下载
                local oldFlist = UpdateFunctions.doFile(flistPath)
                if not oldFlist then
                    self._updateRetType = UpdateConfig.UpdateRetType.OTHER_ERROR
                    print("[ERROR]: load flist falied!")
                    self:updateFail()
                    return
                end

                if oldFlist.version < self.updateInfo.package[packageid].version then
                    self:downLoadFlist(packageid)
                    self.needPackages[#self.needPackages + 1] = packageid
                end
                packageid = self.updateInfo.package[packageid].nextPackageId
            end
            
        end
    end 
    print("all package sum: ", #self.needPackages)
    if self.updateLobby then
        if #self.needPackages == 0 and self.forceUpdate then
            if not self.isIntime then
                enter_game()
            else
                -- todo
            end
        end
    else
        return #self.needPackages
    end
end

function UpdateController:downLoadFlist( __packageid )
    local packageName = self.updateInfo.package[__packageid].flistPath
    local flistPath = UpdateFunctions.writablePath .. UpdateConfig.updateDirName .. self.updateInfo.package[__packageid].flistPath
    local _, dirPos = string.find(flistPath, "(.+)/")
    local dirPath = string.sub(flistPath, 1, dirPos-1)
    UpdateFunctions.mkDirs(dirPath)
    local params = {
        tag = 100 + __packageid,
        url = self.updateInfo.updateUrl.mainUrl .. self.updateInfo.package[__packageid].flistPath,
        backurl = self.updateInfo.updateUrl.reserveUrl .. self.updateInfo.package[__packageid].flistPath,
        path = UpdateFunctions.writablePath .. UpdateConfig.updateDirName .. packageName .. UpdateConfig.updateFilePostfix,
        resType = UpdateConfig.RequestType.FLIST,
        timeout = 3,
        callback = function ( updater, status, progress )
            if status == UpdateConfig.UpdateStatus.SUCCESSED then
                self:onDownLoadFlist(updater, status, progress)
            elseif status == UpdateConfig.UpdateStatus.FAILED then
                self._updateRetType = UpdateConfig.UpdateRetType.NETWORK_ERROR
                self:updateFail()
            end
        end,
    }
    local ud = Updater.new(params)
    if not ud:doUpdate() then
        self._updateRetType = UpdateConfig.UpdateRetType.NETWORK_ERROR
        self:updateFail()
    end
end

function UpdateController:onDownloadApk( __updater, __status, __progress )
    if __status == UpdateConfig.UpdateStatus.PROGRESS then
        if self.forceUpdate then
            self.scene:updateLoadingBar(__progress, "正在更新,请稍后......")
        end
    elseif __status == UpdateConfig.UpdateStatus.SUCCESSED then
        self:onApkFinished(__updater.path)
    else 
        self._updateRetType = UpdateConfig.UpdateRetType.NETWORK_ERROR
        self:updateFail()
    end
end

-- 下载完一个flist
function UpdateController:onDownLoadFlist( __updater, __status, __progress )
    local path = __updater.path -- 本地路径
    self.flistDownCount = self.flistDownCount + 1
    if not cc.FileUtils:getInstance():isFileExist(__updater.path) then
        self._updateRetType = UpdateConfig.UpdateRetType.OTHER_ERROR
        self:updateFail()
        return
    end

    -- 加载刚刚保存的flist
    local flist = UpdateFunctions.doFile(__updater.path)
    if not flist then
        print("Load flist fail|path = ", __updater.path)
        self._updateRetType = UpdateConfig.UpdateRetType.OTHER_ERROR
        self:updateFail()
        return
    end

    --根据文件路径计算子目录
    local filePaths = flist.fileInfoList
    local dirPaths = {}
    for i, v_filePath in ipairs(filePaths) do
        print(i)
        print(v_filePath)
        print(v_filePath.name)
        local _, dirPos = string.find(v_filePath.name, "(.+)/")
        local dirPath = string.sub(v_filePath.name, 1, dirPos-1)
        dirPaths[#dirPaths+1]={name = dirPath}
    end
    --创建模块所有子目录old
    for i = 1, #(dirPaths) do
        UpdateFunctions.mkDir(UpdateFunctions.writablePath .. UpdateConfig.updateDirName .. dirPaths[i].name)
    end

    self:updateNeedDownloadFiles(__updater.tag - 100, flist)
    if self.flistDownCount == #self.needPackages then -- 所有flist加载完成
        for i = 1, #self.needDownloadFiles do
            -- print("self.needDownloadFiles: " .. tostring(self.needDownloadFiles[i]))
        end

        self.currentDownloadIndex = 1 -- 初始化资源下标

        if self.forceUpdate then
            if not self.isIntime and self.scene then
                -- self.scene.updateLayer:showAnimation() -- 播放主页动画
                self.scene:showLoadingBar({content = "正在更新,请稍后",files = self.needDownloadFiles})
            end
        end
        if #self.needDownloadFiles > 0 then
            self:updateResource() -- 开始下载资源
        else

            if self.updateLobby then
                if not self.isIntime then
                    if self.forceUpdate then
                        self:renameAllFiles( 1 )
                        enter_game()
                    end
                else
                    -- todo
                end
            else
                if sendMsg and not self.isIntime and self.forceUpdate then
                    self:renameAllFiles( self.updateGameId )
                    sendMsg(MSG_GAME_UPDATE_PROGRESS, self.updateGameId, 1000)
                end
            end
        end
    end
end

function UpdateController:updateResource()
    local item = self.needDownloadFiles[self.currentDownloadIndex]
    if item then
        if type(item) == "table" then -- 下载单个文件
            local params = {
                tag = item.md5, -- 此处以md5为tag
                url = self.updateInfo.updateUrl.mainUrl .. item.name,
                backurl = self.updateInfo.updateUrl.reserveUrl .. item.name,
                path = UpdateFunctions.writablePath .. UpdateConfig.updateDirName .. item.name .. UpdateConfig.updateFilePostfix,
                resType = UpdateConfig.RequestType.RES,
                timeout = 3,
                callback = function ( updater, status, progress )
                    self:onResourceUpdate(updater, status, progress)
                end,
            }
            local ud = Updater.new(params)
            if not ud:doUpdate() then
                self._updateRetType = UpdateConfig.UpdateRetType.NETWORK_ERROR
                self:updateFail()
            end
        elseif type(item) == "number" then -- 下载zip
            local lpath = self.updateInfo.package[item].zipPath .. self.updateInfo.package[item].packageName .. ".zip"
            local params = {
                tag = item + 1000,
                url = self.updateInfo.downloadUrl.mainUrl .. lpath,
                backurl = self.updateInfo.downloadUrl.reserveUrl .. lpath,
                path = UpdateFunctions.writablePath .. UpdateConfig.updateDirName .. lpath,
                resType = UpdateConfig.RequestType.ZIP,
                timeout = 3,
                callback = function ( updater, status, progress )
                    self:onResourceUpdate(updater, status, progress)
                end,
            }
            local ud = Updater.new(params)
            if not ud:doUpdate() then
                self._updateRetType = UpdateConfig.UpdateRetType.NETWORK_ERROR
                self:updateFail()
            end
        end
    else
        print("[ERROR]: get nil when attempt to update resources by index: ", self.currentDownloadIndex)
        print(debug.traceback())
    end
end

-- 下载资源回调
function UpdateController:onResourceUpdate( updater, status, progress )
    if status == UpdateConfig.UpdateStatus.SUCCESSED then
        if updater.resType == UpdateConfig.RequestType.RES then -- 下载单个文件
            self.hasDownloadSize = self.hasDownloadSize + 1
            self:onDownloadOneFile(updater.path, updater.tag, UpdateConfig.RequestType.RES)
        elseif updater.resType == UpdateConfig.RequestType.ZIP then -- 下载压缩包
            self.hasDownloadSize = self.hasDownloadSize + self.updateInfo.package[updater.tag - 1000].zipSize
            self:onDownloadOneFile(updater.path, updater.tag, UpdateConfig.RequestType.ZIP)
        end
        if self.updateLobby then
            if not self.isIntime then
                if self.forceUpdate then
                    local percent = self.hasDownloadSize / self.needDownloadSize * 100
                    self.scene:updateLoadingBar(percent, "正在更新,请稍后......")
                end
            else
                -- todo
            end
        else
            if sendMsg and not self.isIntime and self.forceUpdate then
                local percent = self.hasDownloadSize / self.needDownloadSize * 100
                sendMsg(MSG_GAME_UPDATE_PROGRESS, self.updateGameId, percent)
            end
        end
        self.currentDownloadIndex = self.currentDownloadIndex + 1 -- 设置下载下标
        if self.currentDownloadIndex > #self.needDownloadFiles then
            scheduler.performWithDelayGlobal(function ()
                self:onDownloadCompleted() -- 下载完成
            end, 0.1)
        else
            self:updateResource() -- 继续下载资源
        end
        
    elseif status == UpdateConfig.UpdateStatus.PROGRESS then
        if updater.resType == UpdateConfig.RequestType.ZIP then -- 下载压缩包
            local currentSize = self.hasDownloadSize + progress / 100 * self.updateInfo.package[updater.tag - 1000].zipSize
            if self.updateLobby then
                if not self.isIntime then
                    if self.forceUpdate then
                        local percent = currentSize / self.needDownloadSize * 100
                        self.scene:updateLoadingBar(percent, "正在更新,请稍后......")
                    end
                else
                    -- todo
                end
            else
                if sendMsg and not self.isIntime and self.forceUpdate then
                    local percent = currentSize / self.needDownloadSize * 100
                    sendMsg(MSG_GAME_UPDATE_PROGRESS, self.updateGameId, percent)
                end
            end
        end
    else
        self._updateRetType = UpdateConfig.UpdateRetType.NETWORK_ERROR
        self:updateFail()
    end
end

-- 下载完一个文件
function UpdateController:onDownloadOneFile( __path, __md5, __resType )
    print("Save res|path= ", __path)
    if __resType == UpdateConfig.RequestType.RES then
        if not cc.FileUtils:getInstance():isFileExist(__path) then
            self._updateRetType = UpdateConfig.UpdateRetType.OTHER_ERROR
            self:updateFail()
            return
        end

        if UpdateFunctions.fileMd5(__path) ~= __md5 then -- md5校验
            --print("文件验证失败", crypto.md5(__path), __md5)
            self._updateRetType = UpdateConfig.UpdateRetType.MD5_ERROR
            self:updateFail()
        end
    elseif __resType == UpdateConfig.RequestType.ZIP then
        print("UpdateController:onDownloadOneFile: ", __path)
    end   
end

-- 所有文件下载完成
function UpdateController:onDownloadCompleted()
    if self.updateLobby then -- 更新大厅
        if not self.isIntime then
            if self.forceUpdate then -- 重命名文件, 解压文件
                self.scene.updateLayer:showZipContent("加载资源中，不会消耗流量")
                self:renameAllFiles(1)
                self:unzipAllFiles(1)  
                enter_game()
            end
        else
            -- todo
        end
        -- todo: 重写本地定点更新信息
    else -- 更新游戏
        self:renameAllFiles(self.updateGameId)
        self:unzipAllFiles(self.updateGameId)  
        if sendMsg and not self.isIntime and self.forceUpdate then
            sendMsg(MSG_GAME_UPDATE_PROGRESS, self.updateGameId, 1000) -- 游戏更新结束, 发送1000
        end
    end
end

-- 重命名所有文件
function UpdateController:renameAllFiles( __packageid )
    local updateInfoName = UpdateFunctions.writablePath .. UpdateConfig.updateDirName .. UpdateConfig.gameVersionName
    self.updateInfo = UpdateFunctions.doFile(updateInfoName)

    -- by package id
    local packageid = __packageid
    while packageid ~= 0 do
        self:renameByPackageId(packageid)
        packageid = self.updateInfo.package[packageid].nextPackageId
    end
end

function UpdateController:renameByPackageId( __packageid )
    local flistName = self.updateInfo.package[__packageid].flistPath
    local flistPath = UpdateFunctions.writablePath .. UpdateConfig.updateDirName .. flistName
    if cc.FileUtils:getInstance():isFileExist(flistPath .. UpdateConfig.updateFilePostfix) then -- 本地文件存在, 则需要重命名此模块
        local flist = UpdateFunctions.doFile(flistPath .. UpdateConfig.updateFilePostfix)
        if not flist then
            self._updateRetType = UpdateConfig.UpdateRetType.OTHER_ERROR
            print("[ERROR]: load flist falied when rename file by package id !")
            self:updateFail()
            return
        end

        for i = 1, #flist.fileInfoList do
            local name = flist.fileInfoList[i].name 
            local path = UpdateFunctions.writablePath .. UpdateConfig.updateDirName
            local pathUpd = UpdateFunctions.writablePath .. UpdateConfig.updateDirName .. name .. UpdateConfig.updateFilePostfix
            if cc.FileUtils:getInstance():isFileExist(pathUpd) then -- 文件存在
                if not UpdateFunctions.checkFileWithMd5(pathUpd, flist.fileInfoList[i].md5) then 
                    self._updateRetType = UpdateConfig.UpdateRetType.MD5_ERROR -- md5  校验失败
                    self:updateFail()
                    return
                end
                self:renameFile(path, name .. UpdateConfig.updateFilePostfix, name)
            end
        end

        self:renameFile(UpdateFunctions.writablePath .. UpdateConfig.updateDirName, flistName .. UpdateConfig.updateFilePostfix, flistName) -- 最后重命名flist
    end
end

-- 解压所有压缩包
function UpdateController:unzipAllFiles( __packageid )
    local updateInfoName = UpdateFunctions.writablePath .. UpdateConfig.updateDirName .. UpdateConfig.gameVersionName
    self.updateInfo = UpdateFunctions.doFile(updateInfoName)

    -- by package id
    local packageid = __packageid
    while packageid ~= 0 do
        self:unzipByPackageId(packageid)
        packageid = self.updateInfo.package[packageid].nextPackageId
    end
end

function UpdateController:unzipByPackageId( __packageid )
    print("UpdateController:unzipByPackageId: " .. __packageid)
    local packageName = self.updateInfo.package[__packageid].packageName
    local lpath = self.updateInfo.package[__packageid].zipPath
    local uncompressPath = UpdateFunctions.writablePath .. UpdateConfig.updateDirName .. lpath 
    local zipPath = uncompressPath .. packageName .. ".zip"
    print("zipPath: ", zipPath)
    if cc.FileUtils:getInstance():isFileExist(zipPath) then
        -- 删除原来目录
        UpdateFunctions.removePath(uncompressPath .. packageName)
        print("unzip: " .. zipPath .. " " .. uncompressPath .. packageName .. "/")
        UpdateFunctions.mkDirs(uncompressPath .. packageName)
        if qka.QFileUtil:uncompressDir(zipPath, uncompressPath .. packageName .. "/", nil) then -- 解压zip 
            UpdateFunctions.removePath(zipPath)
        else
            print("Uncompress zip fail")
            self._updateRetType = UpdateConfig.UpdateRetType.UNZIP_ERROR
            self:updateFail()
            return
        end
    end
end

-- 重命名一个文件
function UpdateController:renameFile( __path, __srcName, __dstName )
    if not cc.FileUtils:getInstance():isFileExist(__path .. __srcName) then
        print("[ERROR]: get nil when attempt to rename file with src path: ", __srcPath)
        return false
    end

    if not UpdateFunctions.renameFile(__path, __srcName, __dstName) then
        print("rename file fail|path=", __path, __dstName)
        self._updateRetType = UpdateConfig.UpdateRetType.OTHER_ERROR
        self:updateFail()
        return false
    end 
    return true
end

-- 检查须要更新的文件
function UpdateController:updateNeedDownloadFiles( __packageid, __newFlist )
    local oldPath = UpdateFunctions.writablePath .. UpdateConfig.updateDirName .. self.updateInfo.package[__packageid].flistPath
    if not cc.FileUtils:getInstance():isFileExist(oldPath) then
        oldPath = self.updateInfo.package[__packageid].flistPath
    end
    if not cc.FileUtils:getInstance():isFileExist(oldPath) then
        print("[ERROR]: " .. oldPath .. " is not exist!")
        if not self.updateLobby then
            -- 更新游戏, 若没有flist, 则整个zip下载
            table.insert(self.needDownloadFiles, __packageid)
            self.needDownloadSize = self.needDownloadSize + self.updateInfo.package[__packageid].zipSize
            return 
        end
    end
    local oldFlist = UpdateFunctions.doFile(oldPath)
    if not oldFlist then
        self._updateRetType = UpdateConfig.UpdateRetType.OTHER_ERROR
        self:updateFail()
        return 
    end

    local hasChanged = false
    local needDownloadFiles = {}
    if __newFlist.version > oldFlist.version then
        for i=1, #(__newFlist.fileInfoList) do
            hasChanged = false
            for k=1, #(oldFlist.fileInfoList) do
                if __newFlist.fileInfoList[i].name == oldFlist.fileInfoList[k].name then
                    if UpdateFunctions.isFileExist(oldFlist.fileInfoList[k].name) then

                        hasChanged = true
                        if __newFlist.fileInfoList[i].md5 ~= UpdateFunctions.fileMd5(oldFlist.fileInfoList[k].name) then
                            local fn = __newFlist.fileInfoList[i].name
                            local path = UpdateFunctions.writablePath .. UpdateConfig.updateDirName .. fn
                            if UpdateFunctions.isFileExist(path) and not UpdateFunctions.checkFileWithMd5(path, __newFlist.fileInfoList[i].md5) then -- 存在/upd目录文件且md5不同, 则需要下载
                                print("need download file | path1=", fn, __newFlist.fileInfoList[i].md5)
                                table.insert(needDownloadFiles, __newFlist.fileInfoList[i])
                            elseif UpdateFunctions.isFileExist(fn) and not UpdateFunctions.checkFileWithMd5(fn, __newFlist.fileInfoList[i].md5) then -- 存在assets文件且md5不同, 则需要下载
                                print("need download file | path2=", fn)
                                table.insert(needDownloadFiles, __newFlist.fileInfoList[i])
                            end
                        end
                    end
                    table.remove(oldFlist.fileInfoList, k) -- 清除数据, 缩短下次遍历时间
                    break
                end
            end
            
            if hasChanged == false then -- 新增的
                -- print("new file add to download list", __newFlist.fileInfoList[i].name)
                table.insert(needDownloadFiles, __newFlist.fileInfoList[i])
            end
        end
        print("-------------------------------------------")
        print("packageid and files sum: " .. __packageid .. " , ", #needDownloadFiles)
        -- for k, v in ipairs(needDownloadFiles) do
        --     print(k, v.name)
        -- end
        print("-------------------------------------------")
        self.needRemoveFiles = oldFlist.fileInfoList
        if #needDownloadFiles >= UpdateConfig.updateFileCount then -- 文件数量太多. 则下载zip
            table.insert(self.needDownloadFiles, __packageid)
            self.needDownloadSize = self.needDownloadSize + self.updateInfo.package[__packageid].zipSize
        else
            for k, v in ipairs(needDownloadFiles) do
                table.insert(self.needDownloadFiles, v)
                self.needDownloadSize = self.needDownloadSize + 1
            end
        end
    end
    return true
end

-- 下载apk 成功， 大厅更新成功
function UpdateController:onApkFinished(__path)
    if self.forceUpdate then
        self.scene:updateLoadingBar(100)
        self:removeCache()
        self:installApk(__path)
    else
        if sendMsg and not self.isIntime then
            if not sendMsg("UPDATE_APK_FINISHED", self, __path) then
                UpdateConfig.showInstallDlg = true 
            end
        end
    end
    UpdateConfig.hasDoneUpdate = true
end

-- 清理缓存
function UpdateController:removeCache()
    UpdateFunctions.removePath(UpdateFunctions.writablePath .. UpdateConfig.updateDirName)
end

function UpdateController:updateFail()
    local errorType = -1 
    for k, v in pairs(UpdateConfig.UpdateRetType) do
        if v == self._updateRetType then
            errorType = k
            break
        end
    end
    print("[ERROR]: 更新失败: " .. self._updateRetType .. " : " .. errorType)
    print(debug.traceback())

    -- remove the flist those downloaded!
    for i = 1, #self.needPackages do
        local path = UpdateFunctions.writablePath .. UpdateConfig.updateDirName .. self.updateInfo.package[self.needPackages[i]].flistPath .. ".upd"
        -- print('aaaaaaaaaaaaaa: ', self.needPackages[i], path)
        -- print("aaaaaaaaaaaaaaaaaaaaaaaaa:  ", cc.FileUtils:getInstance():isFileExist(path))
        if cc.FileUtils:getInstance():isFileExist(path) then
            print("remove file: ", path)
            os.remove(path)
        end
    end
    if (self.forceUpdate or UpdateConfig.UpdateRetType.UPDATEINFO == self._updateRetType) and self.scene then
        if not self.isIntime then
            local params = {
                --content =  "更新失败(UE:" .. self._updateRetType .. "), 如有问题请联系客服,电话:"..ServiceNumber,
                -- content =  "更新失败(UE:" .. self._updateRetType .. "), 如有问题请联系客服",
                content =  "由于您目前网络不稳定，更新失败，请改善网络状况，重新更新",
                btnText1 =  "确定",
                callback1 =  function ()
                    -- os.exit()
                    cc.Director:getInstance():replaceScene(require("app.assetmgr.main.view.RenewScene").new())
                end
            }
            if not cc.FileUtils:getInstance():isFileExist("src/app/assetmgr/flist.lua") then
                enter_game()
            else
                self.scene:showAlertDlg(params)
            end
        else

        end
        return 
    end
    if self.updateLobby then
        if not self.isIntime and self.forceUpdate then
            enter_game()
        else
            -- todo
        end
    else
        if sendMsg and not self.isIntime and self.forceUpdate then
            sendMsg(MSG_GAME_UPDATE_PROGRESS, self.updateGameId, -1)
        end
    end
end

function UpdateController:getAndroidSDAvailableBytes()
    local androidSDSize = 0
    if UpdateFunctions.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "getSDFreeSize"
        local javaParams = {}
        local javaMethodSig = "()Ljava/lang/String;"
        local ok, ret = UpdateFunctions.luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            androidSDSize = ret
        else
            
        end
        print("UpdateController:getAndroidSDAvailableBytes androidSDSize = ", androidSDSize)
        local sdAvailableBytes = tonumber(androidSDSize)
        return sdAvailableBytes
    end
    return 1000*1024*1024
end

-- 开始安装新的apk
function UpdateController:installApk( __apkPath )
    print("install new apk!")
    if UpdateFunctions.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "installApk"
        local javaParams = {
            __apkPath,
            function(event)
                local str = "Java method callback value is [" .. event .. "]"
                print("str =",str)
                os.exit()
            end
        }

        local javaMethodSig = "(Ljava/lang/String;I)V"
        local ok, ret = UpdateFunctions.luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then     
        end
    end
end


function UpdateController:getChannelInfo()
    local info = self.updateInfo.downloadapkUrl

    info.version = self.updateInfo.apkVersion

    self.forceUpdate = (self.updateInfo.forceUpdate == 1)
    -- self.forceUpdate = false -- todo: test code
  --   if info.forceUpdate ~= nil then
		-- self.forceUpdate = UpdateConfig.forceUpdate
  --   end

    -- self.forceUpdate = false

    if self.updateInfo.pointInfo then -- 定点更新内容
        local channelvs = require("cjson").decode(self.updateInfo.pointInfo.channelv)
        local setvs = require("cjson").decode(self.updateInfo.pointInfo.setv)

        local uidvs = nil 
        if self.updateInfo.pointInfo.uidv then
            uidvs = require("cjson").decode(self.updateInfo.pointInfo.uidv)
        end
         

        local updateUtil = PointUpdateUtil.new()
        updateUtil:setImei(UpdateFunctions.getDeviceId())
        updateUtil:read()

        -- 对比总版本
        if self.updateInfo.pointInfo.totalv > updateUtil:getTotalV() then
            updateUtil:setTotalV(self.updateInfo.pointInfo.totalv)
        end

        -- 对比渠道号版本
        local data = UpdateFunctions.Lua_string_split(updateUtil:getData().channelversion, ":")
        for k, v in pairs(channelvs) do
            print(k, v)
            if k == data[1] then
                if v > tonumber(data[2]) then
                    -- updateUtil:getData().channelversion = k .. ":" .. v
                    updateUtil:setChannelV(v)
                    break
                end
            end
        end

        -- 对比设备号版本
        data = UpdateFunctions.Lua_string_split(updateUtil:getData().setversion, ":")
        for k, v in pairs(setvs) do
            print(k, v)
            if k == data[1] then
                if v > tonumber(data[2]) then
                    -- updateUtil:getData().setversion = k .. ":" .. v
                    updateUtil:setImeiV(v)
                    break
                end
            end
        end

        -- 对比用户id版本
        data = UpdateFunctions.Lua_string_split(updateUtil:getData().useridversion, ":")
        if uidvs and data then
            for k, v in pairs(uidvs) do
                print(k, v)
                if k == data[1] then
                    if v > tonumber(data[2]) then
                        -- updateUtil:getData().useridversion = k .. ":" .. v
                        updateUtil:setUidV(v)
                        break
                    end
                end
            end
        end

        updateUtil:write()
    end

    return info
end

-- 更新游戏
function UpdateController:updateGame( __packageid, __forceUpdate ) 
    self.updateLobby = false -- 更新游戏
	
	self.forceUpdate = __forceUpdate  --强制更新
	
    self.isIntime = false
    if not GlobalConf.UPDATE_GAME then
        return false -- 未开启更新, 则直接返回失败
    end

    local updateInfoName = UpdateFunctions.writablePath .. UpdateConfig.updateDirName .. UpdateConfig.gameVersionName
    local updateInfoNameUpd = updateInfoName .. UpdateConfig.updateFilePostfix
    if cc.FileUtils:getInstance():isFileExist(updateInfoNameUpd) then
        self.updateInfo = UpdateFunctions.doFile(updateInfoNameUpd)
    elseif cc.FileUtils:getInstance():isFileExist(updateInfoName) then
        self.updateInfo = UpdateFunctions.doFile(updateInfoName)
    end
    if self.updateInfo == nil then
        return false
    end
    self.updateGameId = __packageid
    if 0 == self:getAllFlist(__packageid) then
        return false
    end

    return true
end

return UpdateController
