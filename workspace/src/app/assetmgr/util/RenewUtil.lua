--
-- Author: 
-- Date: 2018-07-27 11:42:15
--
--存放一些加载组件前更新就要用到的函数


package.loaded["UpdateConfig"] = nil
require("app.assetmgr.RenewDef")
require("lfs") 

UpdateFunctions = {}

function UpdateFunctions.class(classname, super)
    local superType = type(super)
    local cls

    if superType ~= "function" and superType ~= "table" then
        superType = nil
        super = nil
    end

    if superType == "function" or (super and super.__ctype == 1) then
        -- inherited from native C++ Object
        cls = {}

        if superType == "table" then
            -- copy fields from super
            for k,v in pairs(super) do cls[k] = v end
            cls.__create = super.__create
            cls.super    = super
        else
            cls.__create = super
            cls.ctor = function() end
        end

        cls.__cname = classname
        cls.__ctype = 1

        function cls.new(...)
            local instance = cls.__create(...)
            -- copy fields from class to native object
            for k,v in pairs(cls) do instance[k] = v end
            instance.class = cls
            instance:ctor(...)
            return instance
        end

    else
        -- inherited from Lua Object
        if super then
            cls = {}
            setmetatable(cls, {__index = super})
            cls.super = super
        else
            cls = {ctor = function() end}
        end

        cls.__cname = classname
        cls.__ctype = 2 -- lua
        cls.__index = cls

        function cls.new(...)
            local instance = setmetatable({}, cls)
            instance.class = cls
            instance:ctor(...)
            return instance
        end
    end

    return cls
end

function UpdateFunctions.isFileExist(path)
    return cc.FileUtils:getInstance():isFileExist(path)
end

function UpdateFunctions.isDirectoryExist(path)
    return cc.FileUtils:getInstance():isDirectoryExist(path)
end

function UpdateFunctions.readFileString(path)
    return cc.FileUtils:getInstance():getStringFromFile(path)
end

function UpdateFunctions.readFileData(path)
    return cc.HelperFunc:getFileData(path)
end

function UpdateFunctions.renameFile(path, oldname, name)
    return cc.FileUtils:getInstance():renameFile(path, oldname, name)
end

function UpdateFunctions.writefile(path, content, mode)
    mode = mode or "w+b"
    print("UpdateFunctions.writefile",path)
    local file = io.open(path, mode)
    if file then
        print("writefile file = ",file)
        if file:write(content) == nil then return false end
        io.close(file)
        print("writefile file1 = ",file)
        return true
    else
        print("writefile false file = ",file)
        return false
    end
end



function UpdateFunctions.removePath(path)
    local mode = lfs.attributes(path, "mode")
    if mode == "directory" then
        local dirPath = path.."/"
        for file in lfs.dir(dirPath) do
            if file ~= "." and file ~= ".." then 
                local f = dirPath..file 
                UpdateFunctions.removePath(f)
            end 
        end
        os.remove(path)
    else
        os.remove(path)
    end
end

function UpdateFunctions.mkDir(path)
    if not UpdateFunctions.isDirectoryExist(path) then
        --先检查父文件夹
        local dirPos
        _, dirPos = string.find(path, "(.+)/")
        local parentPath = string.sub(path, 1,dirPos-1)
        if not UpdateFunctions.isDirectoryExist(parentPath) then
            UpdateFunctions.mkDir(parentPath)
        end
        return lfs.mkdir(path)
    end
    return true
end

-- 创建多层文件夹
function UpdateFunctions.mkDirs( __path )
    local t = UpdateFunctions.Lua_string_split(__path, "/")
    local path = t[1]
    for i = 2, #t do
        path = path .. "/" .. t[i]
        UpdateFunctions.mkDir(path)
    end
end

-- TODO : 以下一堆代码是否可省去
local sharedApplication = cc.Application:getInstance()
local sharedDirector = cc.Director:getInstance()
local target = sharedApplication:getTargetPlatform()
UpdateFunctions.platform    = "unknown"
UpdateFunctions.model       = "unknown"
UpdateFunctions.PLATFORM_OS_WINDOWS = 0
UpdateFunctions.PLATFORM_OS_LINUX   = 1
UpdateFunctions.PLATFORM_OS_MAC     = 2
UpdateFunctions.PLATFORM_OS_ANDROID = 3
UpdateFunctions.PLATFORM_OS_IPHONE  = 4
UpdateFunctions.PLATFORM_OS_IPAD    = 5
UpdateFunctions.PLATFORM_OS_BLACKBERRY = 6
UpdateFunctions.PLATFORM_OS_NACL    = 7
UpdateFunctions.PLATFORM_OS_EMSCRIPTEN = 8
UpdateFunctions.PLATFORM_OS_TIZEN   = 9
UpdateFunctions.PLATFORM_OS_WINRT   = 10
UpdateFunctions.PLATFORM_OS_WP8     = 11
if target == UpdateFunctions.PLATFORM_OS_WINDOWS then
    UpdateFunctions.platform = "windows"
elseif target == UpdateFunctions.PLATFORM_OS_MAC then
    UpdateFunctions.platform = "mac"
elseif target == UpdateFunctions.PLATFORM_OS_ANDROID then
    UpdateFunctions.platform = "android"
elseif target == UpdateFunctions.PLATFORM_OS_IPHONE or target == UpdateFunctions.PLATFORM_OS_IPAD then
    UpdateFunctions.platform = "ios"
    if target == UpdateFunctions.PLATFORM_OS_IPHONE then
        UpdateFunctions.model = "iphone"
    else
        UpdateFunctions.model = "ipad"
    end
elseif target == UpdateFunctions.PLATFORM_OS_WINRT then
    UpdateFunctions.platform = "winrt"
elseif target == UpdateFunctions.PLATFORM_OS_WP8 then
    UpdateFunctions.platform = "wp8"
end

UpdateFunctions.writablePath = cc.FileUtils:getInstance():getWritablePath()
cc.FileUtils:getInstance():addSearchPath(UpdateFunctions.writablePath)
if UpdateFunctions.platform == "android" then
    --TODO:这里需要修改成自己项目中java文件，这个文件实现lua调用java的方法
    UpdateFunctions.javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
    UpdateFunctions.luaj = {}
    function UpdateFunctions.luaj.callStaticMethod(className, methodName, args, sig)
        return LuaJavaBridge.callStaticMethod(className, methodName, args, sig)
    end
elseif UpdateFunctions.platform == "ios" then
    --TODO:这里LuaObjcFun 是.mm 文件，实现lua调用oc方法
    iosCurPhoneInfoClass = "CurPhoneInfo"
    UpdateFunctions.ocClassName = "AppController"
    UpdateFunctions.luaoc = {}
    function UpdateFunctions.luaoc.callStaticMethod(className, methodName, args)
        local ok, ret = LuaObjcBridge.callStaticMethod(className, methodName, args)
        if not ok then
            local msg = string.format("luaoc.callStaticMethod(\"%s\", \"%s\", \"%s\") - error: [%s] ",
                    className, methodName, tostring(args), tostring(ret))
            if ret == -1 then
                print(msg .. "INVALID PARAMETERS")
            elseif ret == -2 then
                print(msg .. "CLASS NOT FOUND")
            elseif ret == -3 then
                print(msg .. "METHOD NOT FOUND")
            elseif ret == -4 then
                print(msg .. "EXCEPTION OCCURRED")
            elseif ret == -5 then
                print(msg .. "INVALID METHOD SIGNATURE")
            else
                print(msg .. "UNKNOWN")
            end
        end
        return ok, ret
    end
end


UpdateFunctions.tData = "||"  --记录http传过来的数据

function class(classname, super)
    local superType = type(super)
    local cls

    if superType ~= "function" and superType ~= "table" then
        superType = nil
        super = nil
    end

    if superType == "function" or (super and super.__ctype == 1) then
        -- inherited from native C++ Object
        cls = {}

        if superType == "table" then
            -- copy fields from super
            for k,v in pairs(super) do cls[k] = v end
            cls.__create = super.__create
            cls.super    = super
        else
            cls.__create = super
            cls.ctor = function() end
        end

        cls.__cname = classname
        cls.__ctype = 1

        function cls.new(...)
            local instance = cls.__create(...)
            -- copy fields from class to native object
            for k,v in pairs(cls) do instance[k] = v end
            instance.class = cls
            instance:ctor(...)
            return instance
        end

    else
        -- inherited from Lua Object
        if super then
            cls = {}
            setmetatable(cls, {__index = super})
            cls.super = super
        else
            cls = {ctor = function() end}
        end

        cls.__cname = classname
        cls.__ctype = 2 -- lua
        cls.__index = cls

        function cls.new(...)
            local instance = setmetatable({}, cls)
            instance.class = cls
            instance:ctor(...)
            return instance
        end
    end

    return cls
end


--解析的第一个是空字符 
function UpdateFunctions.CheckData(t1,t2)
    local iTmep = 0
    local b = false
    print("len1:",#t1)
    print("len2:",#t2)
    if table.getn(t1) ~= table.getn(t2) then 
        b = true
    else
        for i=1,#t1 do                          --比较两个表的值
            local iCurr = t1[i]
            for j=1,#t2 do
                local jCurr = t2[j]
                if iCurr == jCurr and #t1> 0 then
                    iTmep = iTmep+1
                end     
            end
        end
        print("iTmep:",iTmep)
        print("t2:",#t2)
        if iTmep ~= #t1 and  iTmep > 0 then  --所有的数据都匹配 就不用重复执行刷新
            b = true
        end
    end
    return b,t2
end


function UpdateFunctions.getNetType()
    if UpdateFunctions.platform == "android" then
        local javaClassName = "com/milai/util/PhoneInfoUtil"
        local javaMethodName = nil
        local javaParams = nil
        local javaMethodSig = nil
        javaMethodName = "getNetType"
        javaParams = {
            }
        javaMethodSig = "()Ljava/lang/String;"
        local result,net_type = UpdateFunctions.luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        return net_type
    elseif UpdateFunctions.platform == "ios" then
        local ok, net_type = UpdateFunctions.luaoc.callStaticMethod(iosCurPhoneInfoClass , "getNetworkTypeFromStatusBar")
            if ok then
                print("ios  getNetworkTypeFromStatusBar:" ..net_type)
            end
        return net_type
    end
    return "wifi"
end

function UpdateFunctions.getNetIp()
    if UpdateFunctions.platform == "android" then
        local javaClassName = "com/milai/util/PhoneInfoUtil"
        local javaMethodName = nil
        local javaParams = nil
        local javaMethodSig = nil
        javaMethodName = "getPhoneIP"
        javaParams = {
            }
        javaMethodSig = "()Ljava/lang/String;"
        local result,ip = UpdateFunctions.luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        return ip
    elseif UpdateFunctions.platform == "ios" then
        local ok, ip = UpdateFunctions.luaoc.callStaticMethod(iosCurPhoneInfoClass , "deviceIPAdress")
            if ok then
                print("ios  deviceIPAdress:" ..ip)
            end
        return ip
    end
    return "0.0.0.0"
end


--拆分字符串
function UpdateFunctions.Lua_string_split(str, split_char)
    if split_char == "*" and (str == "" or str == nil) then str = UpdateFunctions.CombinationLobbyData() end
    if not str then return end
    local sub_str_tab = {};
    while (true) do
        local pos = string.find(str, split_char);
        if (not pos) then
            sub_str_tab[#sub_str_tab + 1] = str;
            break;
        end
        local sub_str = string.sub(str, 1, pos - 1);
        sub_str_tab[#sub_str_tab + 1] = sub_str;
        str = string.sub(str, pos + 1, #str);
    end

    return sub_str_tab;
end



function UpdateFunctions.hex(s)
    s = string.gsub(s,"(.)",function (x) return string.format("%02X",string.byte(x)) end)
    return s
end

function UpdateFunctions.doFile(path)
    local fileData = cc.HelperFunc:getFileData(path)
    local fun = loadstring(fileData)
    local ret, flist = pcall(fun)
    if ret then
        return flist
    end

    return flist
end


function UpdateFunctions.fileDataMd5(fileData)
    if fileData ~= nil then
        return cc.Crypto:MD5(fileData, false)
    else
        return nil
    end
end

function UpdateFunctions.fileMd5(filePath)
    return cc.Crypto:MD5File(cc.FileUtils:getInstance():fullPathForFilename(filePath))
    -- local data = UpdateFunctions.readFileData(filePath)
    -- return UpdateFunctions.fileDataMd5(data)
end

function UpdateFunctions.checkFileDataWithMd5(data, cryptoCode)
    if cryptoCode == nil then
        return true
    end

    local fMd5 = cc.Crypto:MD5(data, false)
    if fMd5 == cryptoCode then
        return true
    end

    return false
end

function UpdateFunctions.checkFileWithMd5(filePath, cryptoCode)
    return UpdateFunctions.fileMd5(filePath) == cryptoCode --md5file不返回正常字符串
end

--获取app编译版本号
function UpdateFunctions.getAppVersionCode()
    if UpdateFunctions.platform == "android" then
        local javaMethodName = "getAppVersionCode"
        local javaParams = { }
        local javaMethodSig = "()I"
        local ok, ret = UpdateFunctions.luaj.callStaticMethod(UpdateFunctions.javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            appVersion = ret
        end
    elseif UpdateFunctions.platform == "ios" then
        print("ios getAppVersionCode")
        local ok, ret = UpdateFunctions.luaoc.callStaticMethod(iosCurPhoneInfoClass, "getAppVersionCode")
        if ok then
            appVersion = tonumber(ret)
        end
        appVersion = 5
    else
        appVersion = 1
    end
    return appVersion
end

function UpdateFunctions.handler(obj, method)
    return function(...)
        return method(obj, ...)
    end
end

function UpdateFunctions.split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
     table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end


function UpdateFunctions.trim(s)
    return (string.gsub(s, "^%s*(.-)%s*$", "%1"))
end

function UpdateFunctions.getChannelID()
    if cc.FileUtils:getInstance():isFileExist("src/launcher/AnySdkChannel.lua") then
        AnySdkChannel = require("UpdateFunctions.AnySdkChannel")
    else
        AnySdkChannel = {}
        if UpdateFunctions.platform == "ios" or UpdateFunctions.platform == "mac" then
            AnySdkChannel._channelID = "QKAqipai"
        elseif UpdateFunctions.platform == "android" then
            AnySdkChannel._channelID = "qkagame"
        else
            AnySdkChannel._channelID = "qkagame"
        end
    end
    -- print("AnySdkChannel._channelID = ",AnySdkChannel._channelID)
    return AnySdkChannel._channelID
end

function UpdateFunctions.getDeviceId()
    local Launcher_machineKey = 0
    if UpdateFunctions.getLauncher_machineKey() == nil then
        Launcher_machineKey = UpdateFunctions.getIMEI() 
    else
        Launcher_machineKey = UpdateFunctions.getLauncher_machineKey()
    end

    if Launcher_machineKey == "" or Launcher_machineKey == nil then
        local code = {"0","1","2","3","4","5","6","7","8","9","a",
        "b","c","d","e","f","g","h","i","j","k","l","m","o","p","q","r",
            "s","t","u","v","w","x","y","z"} 
        local count = #code
        math.randomseed(os.time())
        local mimaStr = ""
        for i=1, 12 do
            local mima = math.random(1,count)
            
            mimaStr = mimaStr .. code[mima]
        end
        Launcher_machineKey = mimaStr
        UpdateFunctions.writeLauncher_machineKey(Launcher_machineKey)
    end
    -- body
    return Launcher_machineKey
end
-- 通知android，lua初始化成功
function UpdateFunctions.setInit()
    if UpdateFunctions.platform == "android" then
        local javaMethodName = "setInit"
        local javaParams = { }
        local javaMethodSig = "()V"
        UpdateFunctions.luaj.callStaticMethod(UpdateFunctions.javaClassName, javaMethodName, javaParams, javaMethodSig)
    elseif UpdateFunctions.platform == "ios" then
        print("ios UpdateFunctions.setLuaInit")
        local ocClassName = UpdateFunctions.ocClassName
        local ok, ret = UpdateFunctions.luaoc.callStaticMethod(ocClassName, "setLuaInit")
        if ok then
            paramsStr = ret
        end
    end
    return appVersion
end

-- 获取android 平台，第三方玩家id
function UpdateFunctions.getPlatformUserId()
    local platformUserId = nil
    if UpdateFunctions.platform == "android" then
        local javaMethodName = "getPlatformUserId"
        local javaParams = { }
        local javaMethodSig = "()Ljava/lang/String;"
        local ok, ret = UpdateFunctions.luaj.callStaticMethod(UpdateFunctions.javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            platformUserId = ret
        end
    elseif UpdateFunctions.platform == "ios" then
        print("ios platform ")
        local ocClassName = UpdateFunctions.ocClassName
        local ok, ret = UpdateFunctions.luaoc.callStaticMethod(ocClassName, "getPlatformUserId")
        print("ios getParamsStr ",ok,ret)
        if ok then
            platformUserId = ret
        end
    end
    return platformUserId
end

-- 获取推广id
function UpdateFunctions.getSPID()
    
    local spid = UpdateFunctions.getParamsStr()
    print("===Launcher:getSPID===",spid)
    --兼容老模式
    if spid == "" then
       if UpdateFunctions.platform == "android" then
            local javaParams = {} 
            local CLASS_NAME = "org/cocos2dx/lua/MilaiPublicUtil"
            local methodName = "getChannel"

            local ok, ret = UpdateFunctions.luaj.callStaticMethod(CLASS_NAME,methodName,nil,"()Ljava/lang/String;")
            if ok then
                spid = ret
            else
                spid = ""
            end
        end
    end
    return spid 
end

function UpdateFunctions.getParamsStr()
    print("===UpdateFunctions.getParamsStr==",UpdateFunctions.platform)
    local spid = ""
    local paramsStr = ""
    if UpdateFunctions.platform == "android" then
        local javaParams = {}
        local CLASS_NAME = "org/cocos2dx/lua/AppActivity"
        local methodName = "getParamsStr"
        local ok, ret = UpdateFunctions.luaj.callStaticMethod(UpdateFunctions.javaClassName,methodName,javaParams,"()Ljava/lang/String;")
        print("===UpdateFunctions.ret==",ret)
        print("===UpdateFunctions.ok==",ok)
        if ok then
            paramsStr = ret
        else
            paramsStr = ""
        end
    elseif UpdateFunctions.platform == "ios" then
        print("ios getParamsStr")
        local ocClassName = "CurPhoneInfo"
        local ok, ret = UpdateFunctions.luaoc.callStaticMethod(ocClassName, "getParamsStr")
        if ok then
            paramsStr = ret
        end
    end
    local params = UpdateFunctions.split(paramsStr, "_")
    print("===UpdateFunctions.paramsStr==",paramsStr)
    if params[1] then
        spid = params[1]
    else
        if paramsStr and #paramsStr then
            spid = paramsStr
        end
    end
    return spid
end

function UpdateFunctions.isNewInstall()
    local is_newsteup = 1
    local updfileName = UpdateFunctions.writablePath .. UpdateConfig.updateDirName ..UpdateFunctions.gameVersionName
    if  UpdateFunctions.isFileExist(updfileName) then
        is_newsteup = 0
    end  
    if is_newsteup == 0 then
        return false
    else
        return true
    end
end

function UpdateFunctions.juageIsNoSDKChannel()  -- 非SDK渠道
    if UpdateFunctions.getChannelID() == "qkagame" or UpdateFunctions.getChannelID() == "1100031" then  -- 官网渠道
        local  spid = UpdateFunctions.getSPID() 
        print("juageIsNoSDKChannel spid = ",spid)
        if spid ~= "" and spid ~= nil then
            print("juageIsNoSDKChannel = true")
            return true
        end
    end 
   return false
end

function UpdateFunctions.cryptoMd5(input, isRawOutput)
    input = tostring(input)
    if type(isRawOutput) ~= "boolean" then isRawOutput = false end
    return cc.Crypto:MD5(input, isRawOutput)
end

-- -- 获取手机的wifi名字
function UpdateFunctions.getIMEI()
    if UpdateFunctions.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = nil
        local javaParams = nil
        local javaMethodSig = nil
        javaMethodName = "uniqueGlobalDeviceIdentifier"
        javaParams = {
            }
        javaMethodSig = "()Ljava/lang/String;"
        local result,type = UpdateFunctions.luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        return type
    elseif UpdateFunctions.platform == "ios" then
        local ok, ret = UpdateFunctions.luaoc.callStaticMethod(iosCurPhoneInfoClass , "getUUIDName")
            if ok then
                print("ios  getUUIDName:" ..ret)   
            end
        return ret
       
    end
    return ""
end

function UpdateFunctions.getPhoneType()
    if UpdateFunctions.platform == "android" then
        local javaClassName = "com/milai/util/PhoneInfoUtil"
        local javaMethodName = nil
        local javaParams = nil
        local javaMethodSig = nil
        javaMethodName = "getPhoneType"
        javaParams = {
        }
        javaMethodSig = "()Ljava/lang/String;"
        local result,type = UpdateFunctions.luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        return type
    elseif UpdateFunctions.platform == "ios" then
        local ok, ret = UpdateFunctions.luaoc.callStaticMethod(iosCurPhoneInfoClass , "getCurrentDeviceModel")
        if ok then
            print("ios  getCurrentDeviceModel:" ..ret)  
        end
        return ret
    end
    return nil
end

--获取输入路径上的apk的版本号
function UpdateFunctions.getApkVersion(_path)
    if UpdateFunctions.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = nil
        local javaParams = nil
        local javaMethodSig = nil
        javaMethodName = "getApkVersion"
        javaParams = {
            _path,
        }
        javaMethodSig = "(Ljava/lang/String;)I"
        local ok, ret = UpdateFunctions.luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        end
    end
    return 0
end

function UpdateFunctions.writeLauncher_machineKey( machineKey )
    local content =  "machineKey = " .. machineKey --"DanTiGameKind = {}" .. "\n\r".. "DanTiGameKind.danTiGameKind = " .. danTiGameKind2 .. "\n\r" .."return DanTiGameKind"
    local result = UpdateFunctions.writefile(UpdateFunctions.writablePath .. "machineKey.txt",content)
    print("writeLauncher_machineKey machineKey =  ",result)
end

function UpdateFunctions.getLauncher_machineKey()
    if cc.FileUtils:getInstance():isFileExist(UpdateFunctions.writablePath .. "machineKey.txt") then
        local str = cc.FileUtils:getInstance():getStringFromFile(UpdateFunctions.writablePath .. "machineKey.txt")
        local a = UpdateFunctions.split(str,"=")
        print("a[1] = ",a[1])
        print("a[2] = ",a[2])
        local  DanTiGameKind = a[2] -- danTiGameKind.danTiGameKind
        return UpdateFunctions.trim(DanTiGameKind)
    else
        return nil
    end
end

function UpdateFunctions.writeTime( Time )
    local content =  "Time = " .. Time --"DanTiGameKind = {}" .. "\n\r".. "DanTiGameKind.danTiGameKind = " .. danTiGameKind2 .. "\n\r" .."return DanTiGameKind"
    local result = UpdateFunctions.writefile(UpdateFunctions.writablePath .. "Time.txt",content)
    print("writeLauncher_machineKey Time =  ",result)
end

