--
-- Author: 
-- Date: 2018-07-27 11:42:15
--
UpdateConfig={}
-- UpdateConfig.PointUrl = "http://192.168.0.192:8888/api/ConfigManage/Find" -- 定点更新
-- UpdateConfig.UpdateHttpServer = {mainurl = "http://newhttp.qkatest.com/",reserveUrl = "http://192.168.1.22"}
-- UpdateConfig.GetServerTimeIP = "http://nsystcl.qkagame.cn/"

-- 更新状态
UpdateConfig.UpdateStatus = {SUCCESSED = 0, PROGRESS = 1, FAILED = 2}
--请求类型
UpdateConfig.RequestType = { LAUNCHER = 0, FLIST = 1, RES = 2, UPDATEINFO = 3, ZIP = 4, APK = 5, LOGINTIMENODE = 6, GETSERVERTIME = 7, GETDATA = 8 }

--更新结果
UpdateConfig.UpdateRetType = { SUCCESSED = 0, NETWORK_ERROR = 1, MD5_ERROR = 2, OTHER_ERROR = 3, UNZIP_ERROR = 4, ADDRESS_ERROR = 5, UPDATEINFO = 6 }

-- 更新类型,类型不同，提示不同
UpdateConfig.UpdateType = { LOBBY = 0, GAME = 1, Launcher = 2, GAMEVERSION = 3 }
-- 更新方式,非wifi点击，wifi自动
UpdateConfig.LoadType = { SHOUDONG = 0, AUTO = 1, }

UpdateConfig.ServerTime = 0
UpdateConfig.PackageId  = 0

UpdateConfig.gameVersionName = "UpdateInfo.lua"
UpdateConfig.hasDoneUpdate = false
UpdateConfig.apkName = "MilaiGame.apk"
UpdateConfig.updateFilePostfix = ".upd"
UpdateConfig.updateDirName = "upd/"
UpdateConfig.kindId = 0
UpdateConfig.updateFileCount = 30 -- 20个以上，判断为整包下载
UpdateConfig.forceUpdate = true -- 是否强制更新
UpdateConfig.direction = 2 -- 屏幕方向(1: 竖版, 2: 横版)
UpdateConfig.showInstallDlg = false -- 下载完成, 进入大厅后弹出安装提示(用于非强制更新)

UpdateConfig.DEFAULT_TIMEOUT = 120

UpdateConfig.GAMEPUBLIC_PACKAGE_ID = 99
