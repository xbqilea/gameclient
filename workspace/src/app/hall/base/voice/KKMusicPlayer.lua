-- KKMusicPlayer.lua
-- 音乐音效封装、状态管理
-- Create by @2017-7-3
-- 说明：接口名字与AudioEngine.lua保持一致

local SOUND_DIR = "sound/"

local state = {
    ERROR  = -1,
    INITIALIZING = 0,
    PLAYING = 1,
    PAUSED = 2,
}

local KKMusicPlayer = class("KKMusicPlayer")

function KKMusicPlayer:ctor( ... )
    print('music init.....')
    self.musicPath_ = ""
    self.musicVolume_ = 1
    self.musicID_ = nil

    self.effectPath_ = ""
    self.effectVolume_ = 1
    self.effectID_ = {}

    -- 加载配置
    self:setMusicVolume(tonumber(cc.UserDefault:getInstance():getStringForKey("ONOFF_MUSIC", "1")))
    self:setEffectsVolume(tonumber(cc.UserDefault:getInstance():getStringForKey("ONOFF_VOICE", "1")))
    addMsgCallBack(self, MSG_ENTER_BACKGROUND, function ()
		 self:pauseMusic()
	end)

	addMsgCallBack(self, MSG_ENTER_FOREGROUND, function ()
		  self:resumeMusic()
	end)
end

-------------------------------------------------------------------
--音乐
function KKMusicPlayer:getMusicVolume()
    return self.musicVolume_
end
 function KKMusicPlayer:preloadEffect(filename)
   audio.preloadSound(filename)
end
function KKMusicPlayer:unloadEffect(filename)
   audio.unloadSound(filename)
end
function KKMusicPlayer:setMusicVolume(v) 
        audio.setMusicVolume(v) 
    cc.UserDefault:getInstance():setStringForKey("ONOFF_MUSIC", v)
    cc.UserDefault:getInstance():setStringForKey("MUSIC_VOL", v)
    cc.UserDefault:getInstance():flush()
    self.musicVolume_ = v
end

function KKMusicPlayer:setMusicVolumeValue(v)
    self:setMusicVolume(v)
end

-- 播放背景音乐
-- filename可为文件名或路径，未指定路径则使用默认路径
-- 为了可以设置进度 使用ccexp.AudioEngine
function KKMusicPlayer:playMusic(filename, isLoop, node, giveTime)
    print('playMusic', filename)
    isLoop = isLoop or false
    
    audio.playMusic(filename, isLoop)
end
  

function KKMusicPlayer:preloadMusic(filename)
    local path = self:_getPath(filename)
    ccexp.AudioEngine.preload(path)
end

function KKMusicPlayer:pauseMusic()
   audio.pauseMusic()
end

function KKMusicPlayer:resumeMusic()
   audio.resumeMusic()
end

function KKMusicPlayer:stopMusic()
   audio.stopMusic(true)
end


-------------------------------------------------------------------
--音效
function KKMusicPlayer:playEffect(filename, isLoop)
   audio.playSound(filename, isLoop)
end
 function KKMusicPlayer:stopAllSounds()
   audio.stopAllSounds()
end 
 function KKMusicPlayer:resumeAllSounds()
   audio.resumeAllSounds()
end
function KKMusicPlayer:setEffectsVolume(v)
    audio.setSoundsVolume(v)
    self.effectVolume_ = v
    cc.UserDefault:getInstance():setStringForKey("ONOFF_VOICE", v)
    cc.UserDefault:getInstance():setStringForKey("EFFECT_VOL", v)
    cc.UserDefault:getInstance():flush()
end

function KKMusicPlayer:setEffectsVolumeValue( v )
    -- body
    self:setEffectsVolume(v)
end

function KKMusicPlayer:getEffectsVolume()
    return self.effectVolume_
end
 
  

return KKMusicPlayer