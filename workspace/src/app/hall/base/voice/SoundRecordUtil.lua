--
-- Author: 
-- Date: 2018-07-27 11:42:15
--
-- 录音工具
VoiceRecordUtil = {}
local scheduler = require("framework.scheduler")
VoiceRecordUtil.TALK_PATH = UpdateFunctions.writablePath .. "phrase/"
UpdateFunctions.mkDirs(VoiceRecordUtil.TALK_PATH)
VoiceRecordUtil.SERVER_TALK_PATH = AliYunUtil.voicePath
VoiceRecordUtil.MIN_RECORD_TIME = 0.5 --最短录音时间要求
VoiceRecordUtil.MAX_RECORD_TIME = 30.0 --最长录音时间限制
VoiceRecordUtil.voiceTime = 0.0
VoiceRecordUtil.recordCallback = function(isUploadSuccess,uuid,spxfile,voiceTime)
    print("isUploadSuccess:",isUploadSuccess)
    print("uuid:",uuid)
    print("spxfile:",spxfile)
    print("voiceTime:",voiceTime)
    -- 播放本地录音测试
    -- VoiceRecordUtil:playRecord(spxfile, function() end )
    -- 播放本地录音测试end
    --播放阿里云录音测试
        AliYunUtil:downloadFile(AliYunUtil.voicePath..spxfile, VoiceRecordUtil.TALK_PATH..spxfile, function(success)
            print(type(success))
            print(success)
            if success == 1 then
                print("下载成功")
                VoiceRecordUtil:playRecord(spxfile, function() end )
            else
                print("下载失败")
            end
        end)
    --播放阿里云录音测试end
end
------------------------------speex-------------------------
--Speex是一套主要针对语音的开源免费，无专利保护的音频压缩格式。
--窄带（8kHz），宽带（16kHz）和超宽带（32kHz）压缩于同一位流。
--强化立体编码
--数据包丢失隐蔽
--可变比特率（VBR）
--语音捕捉（VAD）
--非连续传输（DTX）
--定点运算
--感官回声消除（AEC）
--噪音屏蔽
--add by chenzhanming
-------------------------------------------------------------
--对外接口
--设置一次即可，会在录音成功并上传后回调
--@param callback function(isUploadSuccess:0/1,uuid,spxfile,voiceTime)
function VoiceRecordUtil:setCallback(callback)
    VoiceRecordUtil.recordCallback = callback
end
--开始录音，可以不带参数，不带参数则使用之前设置好的callback，会自动静音
--@param callback function(isUploadSuccess:0/1,uuid,spxfile,voiceTime)
function VoiceRecordUtil:startRecord(callback)
    local recordCallback = callback or VoiceRecordUtil.recordCallback
    if not QkaPhoneInfoUtil:getRecordPermisson() then
        ToolKit:showErrorTip(234)--无法录音，请检查应用的录音权限
        recordCallback(0)
        return
    end
	local uuid = QkaPhoneInfoUtil:getIMEI()..math.floor(ToolKit:getTimeMs())..math.random(999)
	local directory = VoiceRecordUtil.TALK_PATH
	local spxfilePath = uuid ..".spx"
	local ret = spxfilePath
	local finishCallback = function(args)
		g_GameMusicUtil:resumeAll()

		-- local strlist = string.split(agrs, ';')
	 --    print("agrs=",agrs)
	 --    local uuid = strlist[1]
	 --    local speexFile = strlist[2]
	    if self:isRecordTimeOK() and self.recordOK then
          	-- 更新到第三方服务器(阿里巴巴)
			AliYunUtil:uploadFileEx(VoiceRecordUtil.SERVER_TALK_PATH..spxfilePath, VoiceRecordUtil.TALK_PATH..spxfilePath, function(success) 
                    recordCallback(success,uuid,spxfilePath,VoiceRecordUtil.voiceTime)
                    if success == 0 then
                        AliYunUtil:askAliyunKey()
                    end
                end)
	    end
	end
	--静音
	g_GameMusicUtil:museAll()
	--开始录音
	self.beginTime = ToolKit:getTimeMs()
	self:startSpeexRecord(uuid,directory, spxfilePath , finishCallback)
	--开始计时
	self.scheduler2stop = scheduler.performWithDelayGlobal(handler(self, self.stopRecord), VoiceRecordUtil.MAX_RECORD_TIME-1)
	return ret
end
--完成录音，会自动开始上传录音文件到阿里云
function VoiceRecordUtil:stopRecord()
    self.recordOK = true
    self:stopScheduler()
	self:stopSpeexRecord()
end
--中断并取消录音
function VoiceRecordUtil:cancelRecord()
    self.recordOK = false
    self:stopScheduler()
    self:stopSpeexRecord()
end
--播放录音
function VoiceRecordUtil:playRecord(_spxfileName, callback)
    local spxFileFullPath = VoiceRecordUtil.TALK_PATH.._spxfileName
    VoiceRecordUtil:playSpeexVoice(spxFileFullPath, 
        function() 
            print("playRecord:",spxFileFullPath)
            if callback and type(callback) == "function" then
                callback()
            end
        end )
end
--注册按钮函数，一键集成所有功能
function VoiceRecordUtil:addVoiceRecordTouchCallback(btn,myTouchCallback)
    self.myTouchCallback = myTouchCallback
    btn:addTouchEventListener(handler(self,self.onVoiceBtnPressButtons))
end

--注册按钮函数
function VoiceRecordUtil:onVoiceBtnPressButtons(sender,eventType)
    if  sender then 
        if eventType == ccui.TouchEventType.began then
            self:startRecord()
        elseif eventType == ccui.TouchEventType.moved then

        elseif eventType == ccui.TouchEventType.ended then
            self:stopRecord()
        elseif eventType == ccui.TouchEventType.canceled then
            self:cancelRecord()
        end
        if self.myTouchCallback then
            self.myTouchCallback(sender,eventType)
        end
    end
end
-------------------------------------------------------------
--内部方法
--开始录音
function VoiceRecordUtil:startSpeexRecord(_uuid,_directory, _spxfilePath , _finishCallback)
    if device.platform == "android" then
        local javaClassName = "com/milai/milaigame/voice/VoiceHelper"
        local javaMethodName = "startRecordingWriteFile"
        local javaParams = {_uuid,_directory, _spxfilePath, _finishCallback}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V"
        local ok = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
    elseif device.platform == "ios" then
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "VoiceManager"
        local _spxFile = _directory .. _spxfilePath
        local args = { spxFile = _spxFile, uuid = _uuid, callBackFutionId = _finishCallback }
        local ok = luaoc.callStaticMethod(className,"startRecordingByLua",args)
        if not ok then
        end
    elseif device.platform == "windows" then
         ShowSmallMsg("对不起，请用真机录音！")
    end
end

--停止录音
function VoiceRecordUtil:stopSpeexRecord()
    if device.platform == "android" then
        local javaClassName = "com/milai/milaigame/voice/VoiceHelper"
        local javaMethodName = "stopRecording"
        local javaParams = {}
        local javaMethodSig = "()V"
        local ok = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
    elseif device.platform == "ios" then
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "VoiceManager"
        local ok = luaoc.callStaticMethod(className,"stopRecordingByLua")
        if not ok then
        end
    end
end

--播放录音
function VoiceRecordUtil:playSpeexVoice(_spxfileName, playfunction )
    if device.platform == "android" then
        local javaClassName = "com/milai/milaigame/voice/VoiceHelper"
        local javaMethodName = "playVoiceFile"
        local javaParams = { _spxfileName ,playfunction}
        local javaMethodSig = "(Ljava/lang/String;I)V"
        local ok = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
    elseif device.platform == "ios" then
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "VoiceManager"
        local args = { spxFile = _spxfileName, callBackFutionId = function ( retCode )
            ToolKit:getCurrentScene():performWithDelay(function ()
                playfunction(retCode)
            end, 0.1)
            -- playfunction(retCode)
        end }
        local ok = luaoc.callStaticMethod(className,"playRecordingByLua",args)
        if not ok then
        end
    end
end

--停止播放录音
function VoiceRecordUtil:stopSpeexVoice()
    if device.platform == "android" then
        local javaClassName = "com/milai/milaigame/voice/VoiceHelper"
        local javaMethodName = "stopPlayRecording"
        local javaParams = {}
        local javaMethodSig = "()V"
        local ok = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
    elseif device.platform == "ios" then
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "VoiceManager"
        local ok = luaoc.callStaticMethod(className,"stopPlayRecordingByLua")
        if not ok then
        end
    end
end

function VoiceRecordUtil:isRecordTimeOK()
    if not self:isRecordTimeTooShort() and not self:isRecordTimeTooLong() then
        return true
    else
    	return false
    end
end

function VoiceRecordUtil:isRecordTimeTooShort()
    self.voiceTime = ToolKit:getTimeMs() - self.beginTime
    if self.voiceTime < VoiceRecordUtil.MIN_RECORD_TIME then
        return true
    else
        return false
    end
end

function VoiceRecordUtil:isRecordTimeTooLong()
    self.voiceTime = ToolKit:getTimeMs() - self.beginTime
    if self.voiceTime > VoiceRecordUtil.MAX_RECORD_TIME then
        return true
    else
        return false
    end
end

function VoiceRecordUtil:stopScheduler()
    if self.scheduler2stop then
        scheduler.unscheduleGlobal(self.scheduler2stop)
        self.scheduler2stop = nil
    end
end