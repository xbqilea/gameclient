--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- Http 类

local HttpUtil = class("HttpUtil")

HttpUrl = {}
 
if GlobalConf.CurServerType == GlobalConf.ServerType.NC then
    HttpUrl.CHECK_SERVER = "http://wkefu.milaichess.com/api/NoticeMaintenance/GetMaintainingStateByGameId"
    HttpUrl.GET_ACCESS_TOKEN = "http://woauth.milaichess.com/api/oauth/getaccesstoken"    --获取微信token
    HttpUrl.GET_USER_INFOR = "http://woauth.milaichess.com/api/oauth/getuserinfo"  --获取微信玩家信息
elseif GlobalConf.CurServerType == GlobalConf.ServerType.WW then
     HttpUrl.CHECK_SERVER = "http://kefu.milaichess.com/api/NoticeMaintenance/GetMaintainingStateByGameId"
    HttpUrl.GET_ACCESS_TOKEN = "http://oauth.milaichess.com/api/oauth/getaccesstoken"  --获取微信token
    HttpUrl.GET_USER_INFOR = "http://oauth.milaichess.com/api/oauth/getuserinfo"   --获取微信玩家信息
end 


function HttpUtil:ctor( __url )
    self:myInit()
    self.url = __url
end

function HttpUtil:myInit()
    self.url = ""
    self.tag = 0
    self.request = nil 
end


function HttpUtil:setTag( __tag )
    self.tag = __tag
end

function HttpUtil:getErrorCode()
    print(tostring(self.request))
    if self.request then
        return self.request:getErrorCode()
    end
    return nil
end

-- post数据
-- @param __params(table): 参数
-- @param __callback(function): 回调函数
-- @param __rsa(bool): 是否需要rsa加密
function HttpUtil:post( __params, __callback, __rsa )
    local params = require("cjson").encode(__params)
    if __rsa then
        params = '"'..U2R(params)..'"'
    end
    print("params: ", self.url, params)
	self.request = cc.HTTPRequest:createWithUrl(function ( event )
        __callback(self.tag, event)
    end, self.url, cc.kCCHTTPRequestMethodPOST) 
    self.request:setPOSTData(params)
    if cc.Application:getInstance():getTargetPlatform() == 3 then -- Android
        self.request:addRequestHeader("Content-Type=application/json")
    else
        self.request:addRequestHeader("Content-Type:application/json")
    end
    -- self.request:setTimeout(5) --重试时间，不设默认为10
    self.request:start()
end

-- post数据
-- @param __paramName(string): 字段名
-- @param __params(table): 参数
-- @param __callback(function): 回调函数
-- @param __rsa(bool): 是否需要rsa加密
function HttpUtil:get( __paramName, __params, __callback, __rsa )
    local params = require("cjson").encode(__params)
    print("params: ", params)
    if __rsa then
        params = U2R(params)
    end
    print("params: ", params)
   -- self.url = self.url .. "?" .. __paramName .. "=" .. string.urlencode(params)
 --   print("self.url=",self.url)
--    self.request = cc.HTTPRequest:createWithUrl(function ( event )
--        __callback(self.tag, event)
--    end, self.url, cc.kCCHTTPRequestMethodGET) 
    -- self.request:setTimeout(5) --重试时间，不设默认为10
    -- print("get url: ", self.url)
  --  self.request:start()
end

return HttpUtil
