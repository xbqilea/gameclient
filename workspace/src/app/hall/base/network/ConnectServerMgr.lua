--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 服务器链接管理

cc.net = require("framework.cc.net.init")
local SocketUtil = require("app.hall.base.network.SocketWrap")

ConnectManager = {}

PSU = -- 平台连接(platform socket util)
{
	[Protocol.LoginServer] = nil, -- 登录服
	[Protocol.LobbyServer] = nil, -- 大厅
    [Protocol.ChatServer] = nil, -- 聊天
}

GSU = {} -- 游戏连接(game socket util)

-------------------------------------------------------- Lobby ------------------------------------------------------------

-- 连接服务器
-- @params __protocol (string) 连接标记
-- @params __ip (string) IP地址
-- @params __port (string) 端口
-- @return (bool) 是否成功
function ConnectManager:connect2Server(__protocol, __ip, __port)
	if PSU[__protocol] and PSU[__protocol]:getState() then
		print("[WARNING:] reconnect the socket !!")
		return false
	end

	if not PSU[__protocol] then
		PSU[__protocol] = SocketUtil.new(__protocol, __ip, __port, false)
	else
		if not PSU[__protocol]:getSocket() then
			PSU[__protocol] = SocketUtil.new(__protocol, __ip, __port, false)
		end
	end
	PSU[__protocol]:connect()
    return true
end

-- 重连服务器
function ConnectManager:reconnect2Server(__protocol)
	if PSU[__protocol] then
		if PSU[__protocol]:getSocket() == nil then
			PSU[__protocol] = SocketUtil.new(__protocol, PSU[__protocol]:getIp(), PSU[__protocol]:getPort(), false)
		end
		PSU[__protocol]:reconnect()
 
	end
end

-- disconnect to the server
-- __protocol: login, lobby, game 
function ConnectManager:disConnectSvr( __protocol )
	if PSU[__protocol] then
		print("ConnectManager:disConnectSvr: ", __protocol) 
		PSU[__protocol]:disconnect()
	end
end

-- close the socket 
function ConnectManager:closeConnect( __protocol )
	if PSU[__protocol] then
		print("ConnectManager:closeConnect: ", __protocol)
		PSU[__protocol]:close()
		PSU[__protocol] = nil
	end
end

function ConnectManager:isConnectSvr( __protocol )
	return PSU[__protocol] and PSU[__protocol]:getState()
end

-- 发送到大厅服
-- __protocol: 服务器id
-- __cmdId: 协议id
-- __dataTable: 数据包
-- return:bool 是否成功发送
function ConnectManager:send2Server( __protocol, __cmdId, __dataTable )
	dump(__dataTable, __cmdId)
	if not PSU[__protocol] then
        print("------------------------------------------")
        print("PSU[__protocol] is nil!!!!1")
		return false
	end
	local ret = PSU[__protocol]:send(__cmdId, __dataTable)
		--if ret == -2 and  (g_LoginController._ischangeAccount == nil or g_LoginController._ischangeAccount == false ) then
			--ConnectManager:reconnect()
            --print("------------------------------------------")
            --print("ConnectManager:reconnect()")
		--end
	return ret
end

-- 重连
function ConnectManager:reconnect()
	--重连超过5次,不再重连
	--if self.m_reconnectTimes and self.m_reconnectTimes > 5 then
	--	print("ConnectManager:reconnect 超过5次未连上, 终止")
		--ToolKit:returnToLoginScene()
	--	return
	--end
	
	--if g_LoginController._isReconnect == true then
	--	print("已经在重连了")
	--	return
	--end
	ConnectManager:disConnectSvr(Protocol.LobbyServer)
    ConnectManager:disConnectSvr(Protocol.ChatServer)
	for i,v in ipairs(GSU) do
		ConnectManager:disconnectGameSvr(v)
	end
	
	print("ConnectManager:reconnect 重连")
	--g_LoginController._isReconnect = true -- 标志重连
	ConnectManager:reconnect2Server(Protocol.LobbyServer)
    ConnectManager:reconnect2Server(Protocol.ChatServer)
	sendMsg(MSG_RECONNECT_LOBBY_SERVER, "start") -- 开始重连
	--if not self.m_reconnectTimes then
	--	self.m_reconnectTimes = 1
	--else
	--	self.m_reconnectTimes = self.m_reconnectTimes + 1
	--end
end

function ConnectManager:resetReconnectTimes()
	--self.m_reconnectTimes = nil
end

-- 发送到场景服
-- @params __gameAtomTypeId(number) 游戏服务ID
-- @params __cmdId(nunber) 协议ID
-- @params __dataTable(table) 协议提数据
-- @params __type(number) 游戏服务类型(默认0)
function ConnectManager:send2SceneServer( __gameAtomTypeId, __cmdId, __dataTable, __type )
	print("send2SceneServer: ", __cmdId)
	dump(__dataTable)
	local ltype = __type or 0
	if PSU[Protocol.LobbyServer] and PSU[Protocol.LobbyServer]:getSocket() then
		local __str = PSU[Protocol.LobbyServer]:makePacket(__cmdId, __dataTable, false)
		ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_HandleMsg_Req", {__gameAtomTypeId, ltype, __str})
	else
		print("大厅断开了，场景服协议转发失败")
	end
end

function ConnectManager:parseByteStringPackets( __protocol, __data )
	return PSU[__protocol]:parseByteStringPackets(__data)
end


-------------------------------------------------------- Game ------------------------------------------------------------
-- connect to the game server 
-- __protocol: game protocol Id 
-- __ip: ip address
-- __port: port
function ConnectManager:connect2GameServer(__protocol, __ip, __port)
	if GSU[__protocol] and GSU[__protocol]:getState() then
		print("[WARNING:] reconnect the socket !!")
		return false
	end

	if not GSU[__protocol] then
		GSU[__protocol] = SocketUtil.new(__protocol, __ip, __port, false)
	else
		if not GSU[__protocol]:getSocket() then
			GSU[__protocol] = SocketUtil.new(__protocol, __ip, __port, false)
		end
	end
	GSU[__protocol]:connect()
    return true
end

-- disconnect to the game server
-- __protocol: game protocol id
function ConnectManager:disconnectGameSvr( __protocol )
	if GSU[__protocol] then
		print("ConnectManager:disconnectGameSvr")
		GSU[__protocol]:disconnect()
	end
end

-- close the game socket 
function ConnectManager:closeGameConnect( __protocol )
	if GSU[__protocol] then
		print("ConnectManager:closeConnect")
		GSU[__protocol]:close()
		GSU[__protocol] = nil
	end
end

-- 发送到登录服
-- __protocol: 服务器id
-- __cmdId: 协议id
-- __dataTable: 数据包
function ConnectManager:send2GameServer( __protocol, __cmdId, __dataTable )
	dump(__dataTable, __cmdId)
	if nil == GSU[__protocol] then
		print("ERROR]: Get nil when attempt to send2Gameserver by protocol: ", __protocol) 
		--sendMsg(MSG_SEND2GAME_FAIL, __cmdId, __dataTable, __protocol)
		return 
	end
	local ret = GSU[__protocol]:send(__cmdId, __dataTable)
	--if ret == -2 then 
		--sendMsg(MSG_SEND2GAME_FAIL, __cmdId, __dataTable, __protocol)
	--end
	return ret
end

function ConnectManager:isConnectGameSvr( __protocol )
	return GSU[__protocol] and GSU[__protocol]:getState()
end

function ConnectManager:getGameClosePassiveness( __protocol )
	return GSU[__protocol] and GSU[__protocol]:getClosePassiveness()
end

function ConnectManager:touchLobbyMsgTag()
	if PSU[Protocol.LobbyServer] then
		PSU[Protocol.LobbyServer]:touchMsgTag()
	end
end

return ConnectManager

