--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 跨平台接口

local pf = nil 
if device.platform == "android" then
    pf = require("framework.luaj")
elseif device.platform == "ios" or device.platform == "mac" then
    pf = require("framework.luaoc")
end

platform = {}


-- 拍照获得头像
function platform.openCamara( __callback )
    if device.platform == "android" then
        print("openCamara")
        local javaClassName = "com/qka/image/MilaiImageUtil"
        local javaMethodName = "openCamara"
        local javaParams = {function (_path)
                local scene = cc.Director:getInstance():getRunningScene()
                if scene then
                    scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                        __callback(_path)
                    end)))
                end
        end}
        local javaMethodSig = "(I)V"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return false
        end
    elseif device.platform == "ios" then 
            local luaoc = require "cocos.cocos2d.luaoc"
            local className = "AppController"
            -- local className1 = "LuaObjectCBridgeTest"
            -- luaoc.callStaticMethod(className1,"registerScriptHandler", {scriptHandler = __callback } )
            local args = { callback = function (_path)
                    local scene = cc.Director:getInstance():getRunningScene()
                    if scene then
                        scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                            dump(_path)
                            __callback(_path)
                        end)))
                    end
            end}
            local ok  = luaoc.callStaticMethod(className,"openCamera",args)
            if not ok then
                return false
            else
                print("ios openCamara ")
                return true
            end
    else
        return true
    end
end

-- 本地选择头像 imageType=1(裁剪)头像, imageType=2原图
function platform.openLocalPhoto( __callback,imageType)
    imageType = imageType or 1
    if device.platform == "android" then
        print("openLocalPhoto")
        local javaClassName = "com/qka/image/MilaiImageUtil"
        local javaMethodName = "openLocalPhoto"
        local javaParams = {function (_path)
        
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                        __callback(_path)
                end)))
            end
        end,imageType}
        
        local javaMethodSig = "(II)V"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return false
        end
    elseif device.platform == "ios" then
        -- TOAST({text = "功能暂未开放"})
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AppController"
        local args = { callback = function (_path)
                local scene = cc.Director:getInstance():getRunningScene()
                if scene then
                    scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                        dump(_path)
                        __callback(_path)
                    end)))
                end
        end, imageType = imageType}
        local ok  = luaoc.callStaticMethod(className,"openPhotoLibrary",args)
        if not ok then
            return false
        else
            print("ios openPhotoLibrary ")
            return true
        end
    else
        return true
    end
end

-- 是否已安装应用
-- _packName: 包名
function platform.isAppInstalled( _packName )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "isAppInstalled"
        local javaParams = {_packName}
        local javaMethodSig = "(Ljava/lang/String;)Z"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return false
        end
    elseif device.platform == "windows" then
        return false
    else
        return true
    end
end


-- 判断是否有sdcard 
function platform.isExistSDCard()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "existSDCard"
        local javaParams = {}
        local javaMethodSig = "()Z"
        local ok ,ret  = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            print("******判断是否存在sd卡失败，错误码：".. ret .. "********" )
            return ret
        end
    end
end


-- 获取sd卡的路径
function platform.getSDcardPath()

    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "getSDcardPath"
        local javaParams = {}
        local javaMethodSig = "()Ljava/lang/String;"
        local ok ,ret  = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret

        else
            print("******获取sd卡路径失败，错误码：".. ret .. "********" )
            return ""
        end
    else
        return ""
    end
end

-- 复制文件到sdcard
function platform.copyFileToSDcard( src, dst )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "copyFile"
        local javaParams = {src, dst}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;)V"
        local ok = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
        end
    end
end

-- 从assets复制到路径
function platform.assetToPath( assetPath ,path )
    if assetPath == nil or #assetPath == 0 
    or path == nil or #path == 0 then
        return
    end 
    if device.platform == "android" then
        local result,path = pf.callStaticMethod(
            "org/cocos2dx/lua/MilaiPublicUtil",
            "assetToPath", 
            {assetPath,path}, 
            "(Ljava/lang/String;Ljava/lang/String;)V"
            )
    end
end


-- 判断文件是否存在
function platform.fileIsExists( fileName)
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "fileIsExists"
        local javaParams = {fileName}
        local javaMethodSig = "(Ljava/lang/String;)Z"
        local ok,ret  = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
            return ret
        end
    end
end


--保存文件到sd卡
function platform.writeToSDFile( fileName, text )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "writeToSDFile"
        local javaParams = {fileName,text}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;)V"
        local ok,ret  = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
            return ok
        end
    end
end

--获取玩家的登陆信息
function platform.getUserLoginInfor()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "getUserLoginInfor"
        local javaParams = {}
        local javaMethodSig = "()Ljava/lang/String;"
        local ok,ret  = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
            return ret
        end
    end
end

--打开其他apk
-- _value  附带的信息，必须为json格式
function platform.openOtherApk( _apkName ,_className )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "openOtherAppWithString"
        local javaParams = {_apkName,_className }
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;)V"
        local ok,ret  = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
           -- return ret
        end
    end
end

-- 重启游戏
function platform.restartApp()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "restartApplication"
        local javaParams = {}
        local javaMethodSig = "()V"
        local ok,ret  = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
           -- return ret
        end
    end
end

-- 获取app包名
function platform.getAppPackageName()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "getMyPackageName"
        local javaParams = {}
        local javaMethodSig = "()Ljava/lang/String;"
        local ok, ret  = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
           return ret
        else
            return ""
        end
        
    elseif device.platform == "ios" then
        return "com.milai.milaigame"
    elseif device.platform == "windows" then
        return "com.milai.milaigamewin"
    elseif device.platform == "mac" then
        return "com.milai.milaigamemac"
    end
end

-- 充值接口
-- __type: (string)充值类型
-- __params: 充值参数(json)
-- __callback: 回调函数
function platform.qkapay( __type, __params, __callback )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "qkapay"
        local javaParams = {__type, __params, function ( __result )
             -- scheduler.performWithDelayGlobal(__callback, 0.1)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)V"
        local ok,ret  = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
           -- return ret
        end
    elseif device.platform == "ios" then
        local ocClassName = "MilaiPublicUtil"
        local ocMethodName = "qkapay"
        local args = {m_type = __type, m_params = __params, m_callback = function ( __result )
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}

        local ok = pf.callStaticMethod(ocClassName, ocMethodName, args)
        if ok then
            -- return ret
        end
    end
end

-- umeng 推送
function platform.getUMengPushMessageToken()
    print("getUMengPushMessageToken ")
    if device.platform == "android" then
        print("getUMengPushMessageToken")
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "getUMengPushMessageToken"
        local javaParams = {}
        local javaMethodSig = "()Ljava/lang/String;"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then 
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AppController"
        local args = {}
        local ok, ret  = luaoc.callStaticMethod(className,"getUMengPushMessageToken",args)
        if not ok then
            return ""
        else
            print("ios getUMengPushMessageToken " .. ret)
            return ret
        end
        return ret
    else
        return ""
    end
end

--  
function platform.startWebService(__url, __params, __callback )
    if device.platform == "android" then
        print("startWebService")
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "startWebService"
        local javaParams = {__url}
        local javaMethodSig = "(Ljava/lang/String;)V"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then 
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AppController"
        local args = {m_type = __url}
        local ok, ret = luaoc.callStaticMethod(className,"startWebService",args)
        if not ok then
            return ""
        else
            print("ios startWebService ")
            return ret
        end
        return ret
    else
        return ""
    end
end

--
function platform.showBGUrl(__url, __params, __callback )
    -- alter
    -- local idx = string.find(__url, "%?")
    -- __url = "http://localhost:8088" .. string.sub(__url, idx)
    --
    if device.platform == "android" then
        print("getUMengPushMessageToken")
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "showBGUrl"
        local javaParams = {__url, __params, function ()
             -- scheduler.performWithDelayGlobal(__callback, 0.1)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback()
                end)))
            end
        end}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)V"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then 
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AppController"
        local args = {m_type = __url, m_params = __params, m_callback = function (__result)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local ok, ret  = luaoc.callStaticMethod(className,"getShowBGURL",args)
        if not ok then
            return ""
        else
            print("ios getShowBGURL ")
            return ret
        end
        return ret
    else
        return ""
    end
end

--
function platform.showPurchaseWebViewUrl(__url, __params, __callback )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "showPurchaseWebViewUrl"
        local javaParams = {__url, __params, function ()
             -- scheduler.performWithDelayGlobal(__callback, 0.1)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback()
                end)))
            end
        end}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)V"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then 
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AppController"
        local args = {m_type = __url, m_params = __params, m_callback = function (__result)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local ok, ret  = luaoc.callStaticMethod(className,"showPurchaseWebViewUrl",args)
        if not ok then
            return ""
        else
            return ret
        end
        return ret
    else
        return ""
    end
end

--
function platform.closePurchaseWebViewUrl(__url, __params, __callback )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "closePurchaseWebViewUrl"
        local javaParams = {__url, __params, function ()
             -- scheduler.performWithDelayGlobal(__callback, 0.1)
            local scene = cc.Director:getInstance():getRunningScene()
            if __callback and scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback()
                end)))
            end
        end}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)V"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then 
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AppController"
        local args = {m_type = __url, m_params = __params, m_callback = function (__result)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local ok, ret  = luaoc.callStaticMethod(className,"closePurchaseWebViewUrl",args)
        if not ok then
            return ""
        else
            return ret
        end
        return ret
    else
        return ""
    end
end

--
function platform.showXXViewUrl(__url, __params, __callback )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "showXXViewUrl"
        local javaParams = {__url, __params, function (__result)
             -- scheduler.performWithDelayGlobal(__callback, 0.1)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)V"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then 
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AppController"
        local args = {m_type = __url, m_params = __params, m_callback = function (__result)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local ok, ret  = luaoc.callStaticMethod(className,"showXXViewUrl",args)
        if not ok then
            return ""
        else
            return ret
        end
        return ret
    else
        return ""
    end
end

--
function platform.closeXXViewUrl(__url, __params, __callback )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "closeXXViewUrl"
        local javaParams = {__url, __params, function ()
             -- scheduler.performWithDelayGlobal(__callback, 0.1)
            local scene = cc.Director:getInstance():getRunningScene()
            if __callback and scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback()
                end)))
            end
        end}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)V"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then 
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AppController"
        local args = {m_type = __url, m_params = __params, m_callback = function (__result)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local ok, ret  = luaoc.callStaticMethod(className,"closeXXViewUrl",args)
        if not ok then
            return ""
        else
            return ret
        end
        return ret
    else
        return ""
    end
end

function platform.saveImageToGallery(__url, __params, __callback )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "saveImageToGallery"
        local javaParams = {__url, __params, function ()
             -- scheduler.performWithDelayGlobal(__callback, 0.1)
            local scene = cc.Director:getInstance():getRunningScene()
            if __callback and scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback()
                end)))
            end
        end}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)Z"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then 
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AppController"
        local args = {m_type = __url, m_params = __params, m_callback = function (__result)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local ok, ret  = luaoc.callStaticMethod(className,"saveImageToGallery",args)
        if not ok then
            return ""
        else
            return ret
        end
        return ret
    else
        return ""
    end
end

--
function platform.setJPushAlias(__url, __params, __callback )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "setJPushAlias"
        local javaParams = {__url, __params, function (__result)
             -- scheduler.performWithDelayGlobal(__callback, 0.1)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)V"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then 
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AppController"
        local args = {m_type = __url, m_params = __params, m_callback = function (__result)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local ok, ret  = luaoc.callStaticMethod(className,"setJPushAlias",args)
        if not ok then
            return ""
        else
            return ret
        end
        return ret
    else
        return ""
    end
end

--
function platform.hideChatView(__url, __params, __callback )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "hideChatView"
        local javaParams = {__url, __params, function (__result)
             -- scheduler.performWithDelayGlobal(__callback, 0.1)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;I)V"
        local ok, ret = pf.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then 
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "AppController"
        local args = {m_type = __url, m_params = __params, m_callback = function (__result)
            local scene = cc.Director:getInstance():getRunningScene()
            if scene then
                scene:runAction(cc.Sequence:create(cc.DelayTime:create(0.1), cc.CallFunc:create(function ()
                    __callback(tonumber(__result))
                end)))
            end
        end}
        local ok, ret  = luaoc.callStaticMethod(className,"hideChatView",args)
        if not ok then
            return ""
        else
            return ret
        end
        return ret
    else
        return ""
    end
end

function platform.openWebViewFillUp(_url, _direction)
    if GlobalConf.APPVERSION < 9 then
        device.openURL(_url)
    else
        local _dir = _direction or 2
        if device.platform == "android" then
            local javaClassName = "org/cocos2dx/lua/AppActivity"
            local javaMethodName = "openKefuView"
            local javaParams = {_url, _direction} -- 0:横屏 1:竖屏 2:物理旋转
            local javaMethodSig = "(Ljava/lang/String;I)V"
            local ok = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        elseif device.platform == "ios" then
            local luaoc = require "cocos.cocos2d.luaoc"
            local className = "MilaiPublicUtil"
            local args = {url = _url, direction = _dir}
            local ok = luaoc.callStaticMethod(className,"openKefuView",args)
            if not ok then
            end
        end
    end
end

function platform.setGLViewTouchEnabled(isEnabled)
    local nIsTouch = 1
    if isEnabled then
        nIsTouch = 1
    else
        nIsTouch = 0
    end
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaParams = { nIsTouch }
        local javaMethodSig = "(I)V"
        local ok, ret = luaj.callStaticMethod(javaClassName, "setGameViewGLIsTouch", javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then
        -- platformUtils.showXXViewUrl(_url, _params, handler(self, self.onGameExit))
    end
end

-- _direction 0:横屏 1:竖屏
function platform.openWebView(_url, _params, _isCookie, _callback, _direction)
    print(string.format("platform.openWebView [url: %s, params:%s]", _url, _params))
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "openGameWebView"
        local __isCookie = _isCookie
        if _params == "" then
            __isCookie = 0
        end
        local javaParams = { _url, _params, __isCookie, _direction or 0, function(__result)
            -- if (not tolua.isnull(self)) then
            _callback(tonumber(__result))
            -- self:runAction(cc.Sequence:create(cc.DelayTime:create(0.05), cc.CallFunc:create(function()
            -- end)))
            -- end
        end }
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;III)V"
        local ok, ret = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then
        -- platformUtils.showXXViewUrl(_url, _params, _callback)
    end
end

function platform.closeGameWebView()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodSig = "()V"
        local ok, ret = luaj.callStaticMethod(javaClassName, "closeGameWebView", nil, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then
        -- platformUtils.showXXViewUrl(_url, _params, handler(self, self.onGameExit))
    end
end

function platform.hideGameChatView()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodSig = "()V"
        local ok, ret = luaj.callStaticMethod(javaClassName, "hideGameChatView", nil, javaMethodSig)
        if ok then
            return ret
        else
            return ""
        end
    elseif device.platform == "ios" then
        -- platformUtils.showXXViewUrl(_url, _params, handler(self, self.onGameExit))
    end
end

return platform
