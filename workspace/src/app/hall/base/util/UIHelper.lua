--UIHelper.lua
--Create by . 2017-05-09
--遍历node结点接口

--程序使用的节点名
local ParseNodeTable = {
    "image",
    "button",
    "sprite",
    "text",
    "atlas",
    "listView",
    "slider",
    "panel",
    "bmfont",
    "node",
    "fileNode",
    "checkBox",
    "loadingBar",
    "textField",
    "pageView",
    "scrollView",
    "particle",
    "audio",
    "map",
}

local function seekNodeByTag(parent, tag)
    if not parent then
        return
    end

    if tag == parent:getTag() then
        return parent
    end

    local findNode
    local children = parent:getChildren()
    local childCount = parent:getChildrenCount()
    if childCount < 1 then
        return
    end
    for i=1, childCount do
        if "table" == type(children) then
            parent = children[i]
        elseif "userdata" == type(children) then
            parent = children:objectAtIndex(i - 1)
        end

        if parent then
            findNode = seekNodeByTag(parent, tag)
            if findNode then
                return findNode
            end
        end
    end

    return
end

local function seekNodeByName(parent, name)
    --print("seekNodeByName", parent, name)
    if not parent or parent.getChildren == nil then
        return
    end
    --print("seekNodeByName", name, parent:getName())
    if name == parent:getName() then
        return parent
    end

    local findNode
    local children = parent:getChildren()
    local childCount = parent:getChildrenCount()
    if childCount < 1 then
        --print(parent:getName() .. " childCount < 1")
        return
    end
    for i=1, childCount do
        if "table" == type(children) then
            parent = children[i]
        elseif "userdata" == type(children) then
            parent = children:objectAtIndex(i - 1)
        end

        if parent then
            findNode = seekNodeByName(parent, name)
            if findNode then
                return findNode
            end
        end
    end

    return
end

local function seekWidgetByName(parent, name)
    return ccui.Helper:seekWidgetByName(tolua.cast(parent, "Widget"), name)
end

local function seekWidgetByTag(parent, name)
    return ccui.Helper:seekWidgetByTag(tolua.cast(parent, "Widget"), name)
end

--解析界面节点
local function parseCSDNode(self, root, t)
    if not root or root.getChildren == nil then
        return
    end
    self.t_parseNode = t or {}
    local name = root:getName()
    local function checkName(name)
        for k,v in pairs(ParseNodeTable) do
            local ret = string.find(name, v .. "_")
            if ret then
                return true
            end
        end
    end
    if checkName(name) then
        local _, _, name = string.find(name, "_(.+)")
        if self.t_parseNode[name] or self[name] then
            print('----------- warning -----------')
            print(name .. ' already existed in ' .. (self.getName and self:getName() or '') .. '!')
            print('----------- warning -----------')
        end
        self.t_parseNode[name] = root
        self[name] = root
        -- print(name, root)
    end
    local function isFileNode(name)
        if string.find(name, "FileNode") or string.find(name, "fileNode") then
            return true
        end
    end
    if isFileNode(name) then
        return
    end

    local children = root:getChildren()
    local childCount = root:getChildrenCount()
    if childCount < 1 then
        return
    end
    for i=1, childCount do
        local child
        if "table" == type(children) then
            -- print("table", name)
            child = children[i]
        elseif "userdata" == type(children) then
            -- print("userdata", name)
            child = children:objectAtIndex(i - 1)
        end

        if child then
            parseCSDNode(self, child, self.t_parseNode)
        end
    end
end
--解析一个fileNode
local function parseFileNode(self, root, t)
    local children = root:getChildren()
    local childCount = root:getChildrenCount()
    if childCount < 1 then
        return
    end
    self.t_parseNode = t or {}
    for i=1, childCount do
        local child
        if "table" == type(children) then
            --print("table", name)
            child = children[i]
        elseif "userdata" == type(children) then
            --print("userdata", name)
            child = children:objectAtIndex(i - 1)
        end

        if child then
            parseCSDNode(self, child, self.t_parseNode)
        end
    end
end
--释放已解析的节点
local function freeCSDParse(self)
    if type(self.t_parseNode) ~= "table" then
        return
    end
    for k,v in pairs(self.t_parseNode) do
        self.t_parseNode[k] = nil
        self[k] = nil
    end
    self.t_parseNode = nil
end

return {
    ["seekNodeByTag"] = seekNodeByTag,
    ["seekNodeByName"] = seekNodeByName,
    ["seekWidgetByTag"] = seekWidgetByTag,
    ["seekWidgetByName"] = seekWidgetByName,
    ["parseCSDNode"] = parseCSDNode,
    ["parseFileNode"] = parseFileNode,
    ["freeCSDParse"] = freeCSDParse,
}