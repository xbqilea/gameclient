--
-- Author: 
-- Date: 2018-07-27 11:42:15
--

local function receivePhoneIP(event)
    print("receivePhoneIP",event.name)
    if event.name == "completed" then
        local request = event.request
        if request:getResponseStatusCode() == 200 then
            --dataRecv = "var returnCitySN = {"cip": "61.140.222.161", "cid": "440100", "cname": "广东省广州市"};"
            local dataRecv = request:getResponseData()
            g_PhoneIP = dataRecv 
            print("g_PhoneIP:",g_PhoneIP) 
            if g_aready_login_ip and QkaPhoneInfoUtil then
                local __dataTable = {QkaPhoneInfoUtil:getPhoneIP()}
                ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_UpdateIp_Req", __dataTable)
            end
        end
    elseif event.name == "progress" then
        
    end
end
-- 联网获取手机的IP,保存在Launcher.phoneIP里
local function requestPhoneIP()
    local url = "http://ip.myhostadmin.net/"
    local request = cc.HTTPRequest:createWithUrl(function(event) 
        if receivePhoneIP then
            receivePhoneIP(event)
        end
    end, url,cc.kCCHTTPRequestMethodGET )
    if request then
        request:start()
    end
end
requestPhoneIP()