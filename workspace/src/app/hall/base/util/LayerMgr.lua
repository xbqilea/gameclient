--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- StackLayer管理类

local StackLayerManager = class("StackLayerManager")
local PUSH_DURATION = 0.15 -- 层移动持续时间
local POP_DURATION = 0.15 -- 层移动持续时间
local SCALE_DURATION = 0.15 -- 层放大持续时间

GLOBAL_ZORDER_START = 100000
CUR_GLOBAL_ZORDER = CUR_GLOBAL_ZORDER or GLOBAL_ZORDER_START -- 全局的zorder, 用于控制所有对话框的顺序

function StackLayerManager:ctor( _scene )
    self:myInit()

    self.scene = _scene
end

function StackLayerManager:myInit()
    self.scene = nil -- 场景
    self.isAnimating = false -- 用来防止同时进行两个页面切换动画的变量
    self.stackLayerList = {} -- 栈列表
end

-- 推出到栈底
function StackLayerManager:pop2RootLayer()
    if #self.stackLayerList == 1 then
        return 
    elseif #self.stackLayerList == 2 then
        self:popStackLayer()
    else
        for i = #self.stackLayerList - 1, 2, -1 do
            local stackLayer = self.stackLayerList[i]
            stackLayer:removeFromParent()
            table.remove(self.stackLayerList, i)
            for j=#GlobalViewBaseStack,1,-1 do
                if GlobalViewBaseStack[j]:getType() == "StackLayer" then
                    table.remove(GlobalViewBaseStack, j)
                    break
                end
            end
        end
        self:popStackLayer()
    end
end


-- StackLayer入栈
-- @param _stackLayerName(string) 入盏StackLayer的完整路径
-- @param __params(table) 需要传递的参数
-- @return (StackLayer) 当前压栈的层
function StackLayerManager:pushStackLayer( _stackLayerName, __params )
    if self.isAnimating then
        return
    end
    local __params = __params or {}

    self.isAnimating = true
    local _direction = __params._direction or GlobalDefine.INIT_DIRECTION
    local animation = __params._animation or STACKANI.BOTTOM_TO_TOP
    local showPreLayer = __params._showPreLayer or false -- 显示前一个layer

    
    if animation == nil then
        animation = STACKANI.RIGHT_TO_LEFT
    end

    if #self.stackLayerList == 0 then
        animation = STACKANI.NONE
    end

    local stackLayer 
	if type(_stackLayerName) == "string"  then 
	   --如果发过来的是界面的路径
        local stackLayerClass = require(_stackLayerName)
        stackLayer = stackLayerClass.new(__params)
	else 
        stackLayer = _stackLayerName
	end

    stackLayer:setShowPreLayer(showPreLayer)

    table.insert(self.stackLayerList, stackLayer)
    CUR_GLOBAL_ZORDER = CUR_GLOBAL_ZORDER + 1
    self.scene:addChild(stackLayer, CUR_GLOBAL_ZORDER)
    stackLayer:setScene(self.scene)

    self:runAnimation(stackLayer, animation, true)
    stackLayer:setPushAnimation(animation)

    GlobalViewBaseStack[#GlobalViewBaseStack+1] = stackLayer
    return stackLayer
end

-- StackLayer出栈
-- @return (StackLayer) 出栈后栈定的StackLayer
function StackLayerManager:popStackLayer()
    if #self.stackLayerList <= 1 then
        return
    end
    if self.isAnimating then
        return
    end

    self:delExitRemoveLayer()
    self.isAnimating = true
    local stackLayer = self:getCurrentLayer()
    local layer = table.remove(self.stackLayerList, #self.stackLayerList) -- 列表中删除

    for i,v in ipairs(GlobalViewBaseStack) do
        if v == layer then
            table.remove(GlobalViewBaseStack,i)
            break
        end
    end
    self.stackLayerList[#self.stackLayerList]:setVisible(true)

    local animation = stackLayer:getPushAnimation()
    self:runAnimation( stackLayer, animation, false )

    return self.stackLayerList[#self.stackLayerList]
end

function StackLayerManager:runAnimation( __layer, __animation, __isPush )
    dump(__animation)
    if __isPush then
        if __layer.onPushAnimation then
            showTransParentDlg()
            __layer:onPushAnimation(function ()
                hideTransParentDlg()
                self:onEndAnimation(__layer,__isPush)
            end)
        elseif __animation == STACKANI.RIGHT_TO_LEFT then
            showTransParentDlg()
            __layer:setPositionX(display.width)
            local lMove = cc.MoveTo:create(PUSH_DURATION, cc.p(0, 0))
            __layer:runAction(cc.Sequence:create(lMove, cc.CallFunc:create(function ()
                hideTransParentDlg()
                self:onEndAnimation(__layer,__isPush)
            end)))

            local preLayer = self.stackLayerList[#self.stackLayerList - 1]
            local pMove = cc.MoveTo:create(PUSH_DURATION, cc.p(-preLayer:getContentSize().width, 0))
            preLayer:runAction(pMove)
        elseif __animation == STACKANI.ZERO_TO_WHOLE then
            showTransParentDlg()
            __layer:setAnchorPoint(0.5, 0.5)
            __layer:setPosition(0, 0)
            __layer:setScale(0.2)
            __layer:runAction(cc.ScaleTo:create(SCALE_DURATION, 1))
            local move = cc.MoveTo:create(SCALE_DURATION, cc.p(0, 0))
            __layer:runAction(cc.Sequence:create(move, cc.CallFunc:create(function ()
                hideTransParentDlg()
                self:onEndAnimation(__layer,__isPush)
            end)))
        elseif __animation == STACKANI.BOTTOM_TO_TOP then
            showTransParentDlg()
            __layer:setPositionY(0 - display.height)
            local lMove = cc.MoveTo:create(PUSH_DURATION, cc.p(0, 0))
            __layer:runAction(cc.Sequence:create(lMove, cc.CallFunc:create(function ()
                hideTransParentDlg()
                self:onEndAnimation(__layer,__isPush)
            end)))
        else
            self:onEndAnimation(__layer,__isPush)
        end
    else
        if __layer.onPopAnimation then
            showTransParentDlg()
            __layer:onPopAnimation(function ()
                __layer:removeFromParent()
                hideTransParentDlg()
                self:onEndAnimation()
            end)
        elseif __animation == STACKANI.RIGHT_TO_LEFT then
            showTransParentDlg()
            __layer:runAction(cc.Sequence:create(cc.MoveTo:create(POP_DURATION, cc.p(display.width, 0)), cc.CallFunc:create(function ()
                __layer:removeFromParent()
                hideTransParentDlg()
                self:onEndAnimation()
            end)))
            local layer = self:getCurrentLayer()
            layer:setPosition(-layer:getContentSize().width, 0)
            layer:runAction(cc.MoveTo:create(POP_DURATION, cc.p(0, 0)))
        elseif __animation == STACKANI.ZERO_TO_WHOLE then
            showTransParentDlg()
            __layer:runAction(cc.Sequence:create(cc.ScaleTo:create(SCALE_DURATION, 0.2), cc.CallFunc:create(function ()
                __layer:removeFromParent()
                hideTransParentDlg()
                self:onEndAnimation()
            end)))
            local layer = self:getCurrentLayer()
            layer:setPosition(0, 0)
        elseif __animation == STACKANI.BOTTOM_TO_TOP then
            showTransParentDlg()
            __layer:runAction(cc.Sequence:create(cc.MoveTo:create(POP_DURATION, cc.p(0, 0 - display.height)), cc.CallFunc:create(function ()
                __layer:removeFromParent()

                hideTransParentDlg()
                self:onEndAnimation()
            end)))
        else
            __layer:removeFromParent()
            self:onEndAnimation()
        end
    end
end

function StackLayerManager:onEndAnimation(_layer,_isPush)
    self.isAnimating = false
    
    if _layer and _isPush then
        if #self.stackLayerList > 1 then
            if not _layer:getShowPreLayer() then
                self.stackLayerList[#self.stackLayerList - 1]:setVisible(false)
            end
        end

        --当动画结束之后，通知layer的onEndAnimation，一般是进入动画结束后，用来通知layer加载内容
        if _layer.onEndAnimation then
            _layer:onEndAnimation(_isPush)
        end 
    end
end

-- delete StackLayer when needed
function StackLayerManager:delExitRemoveLayer()
    local layer = self.stackLayerList[#self.stackLayerList - 1]
    if layer and layer:getExitRemove() then
        print("dele the stackLayer which name is: ", layer.class.__cname)
        -- dump(layer.class)
        layer:removeFromParent()
        -- dump(self.stackLayerList)
        table.remove(self.stackLayerList, #self.stackLayerList - 1)
        for i,v in ipairs(GlobalViewBaseStack) do
            if v == layer then
                table.remove(GlobalViewBaseStack,i)
                break
            end
        end
    end
end

function StackLayerManager:getAnimating()
    return self.isAnimating
end

-- 获取当前栈顶layer
function StackLayerManager:getCurrentLayer()
    return self.stackLayerList[#self.stackLayerList]
end
return StackLayerManager