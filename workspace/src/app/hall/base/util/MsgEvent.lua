--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 本地广播

g_callbackMsgList = {} -- 消息队列
g_onlyOneMsgList = {} -- 唯一消息队列

MSG_GAME_UPDATE_PROGRESS = "MSG_GAME_UPDATE_PROGRESS" -- 游戏更新进度
UPDATE_APK_FINISHED = "UPDATE_APK_FINISHED" -- 更新apk完成
-- MSG_DELETE_GAME = "MSG_DELETE_GAME" -- 删除游戏
MSG_ENTER_GAME_PROTAL = "MSG_ENTER_GAME_PROTAL" -- 进入游戏节点
MSG_ENTER_GAME_PROTAL_NTY = "MSG_ENTER_GAME_PROTAL_NTY" -- 当前游戏所在节点刷新
MSG_PUSH_GAME_RECONNECT = "MSG_PUSH_GAME_RECONNECT" -- 是否有游戏需要重连
MSG_GAME_SHUT_DOWN = "MSG_GAME_SHUT_DOWN" -- 游戏关闭通知

MSG_LOGIN_LOBBY_ASK = "MSG_LOGIN_LOBBY_ASK" -- 登录大厅结果
MSG_LOGIN_FAIL = "MSG_LOGIN_FAIL" -- 登录大厅失败
MSG_RECONNECT_LOBBY_SERVER = "MSG_RECONNECT_LOBBY_SERVER" -- 重连大厅
MSG_SHOW_ERROR_TIPS = "MSG_SHOW_ERROR_TIPS" --controller发送错误信息id
MSG_SEND2GAME_FAIL = "MSG_SEND2GAME_FAIL" -- 游戏服发送消息失败
MSG_ENTER_FOREGROUND = "MSG_ENTER_FOREGROUND" -- 切换前台
MSG_ENTER_BACKGROUND = "MSG_ENTER_BACKGROUND" -- 切换后台
MSG_CAPTURE_4_DIALOG = "MSG_CAPTURE_4_DIALOG" -- 弹出框截屏完成
MSG_RECHARGE_INPUT_COMPLETE = "MSG_RECHARGE_INPUT_COMPLETE" -- 充值键盘输入完成
MSG_RECHARGE_OPEN_H5_WEBVIEW = "MSG_RECHARGE_OPEN_H5_WEBVIEW" -- 打开H5支付页面
MSG_RECHARGE_RESULT = "MSG_RECHARGE_RESULT" -- 充值结果
MSG_GOLD_SHOP_INPUT_COMPLETE = "MSG_GOLD_SHOP_INPUT_COMPLETE" -- 金币商城数量输入完成
MSG_MAINTANENCE_INFO = "MSG_MAINTANENCE_INFO" -- 显示维护开始结束时间提示框

MSG_SOCKET_CONNECTION_EVENT = "MSG_SOCKET_CONNECTION_EVENT" -- sock状态改变
MSG_ENTER_SOME_LAYER = "MSG_ENTER_SOME_LAYER" -- 跳转界面
MSG_RUNTIME_ERROR = "MSG_RUNTIME_ERROR" -- 程序运行出错
MSG_BAG_UPDATE_SUCCESS = "MSG_BAG_UPDATE_SUCCESS" -- 背包道具内容更新成功
MSG_BAG_COMBINE_SUCCESS = "MSG_BAG_COMBINE_SUCCESS" -- 背包碎片合成成功
MSG_EXCHANGE_LEFT_TIMES_UPDATE_SUCCESS = "MSG_EXCHANGE_LEFT_TIMES_UPDATE_SUCCESS" -- 每日话费剩余兑换次数更新成功
MSG_EXCHANGE_SUCCESS = "MSG_EXCHANGE_SUCCESS" -- 兑换（话费、实物）成功
MSG_EXCHANGE_ADDRESS_UPDATE_SUCCESS = "MSG_EXCHANGE_ADDRESS_UPDATE_SUCCESS" --兑换的玩家收货地址更新成功
MSG_PLAYER_UPDATE_SUCCESS = "MSG_PLAYER_UPDATE_SUCCESS" -- 玩家（财富、账号）属性更新成功
MSG_MAIL_UPDATE_SUCCESS = "MSG_MAIL_UPDATE_SUCCESS" -- 邮件列表更新成功
MSG_MAIL_REWARD_SUCCESS = "MSG_MAIL_REWARD_SUCCESS" --领取邮件奖励
MSG_GOTO_STACK_LAYER = "MSG_GOTO_STACK_LAYER" -- 切换页面
MSG_BULLETIN_UPDATE_SUCCESS = "MSG_BULLETIN_UPDATE_SUCCESS" -- 公告更新成功
MSG_NO_POP_UP_BULLETIN = "MSG_NO_POP_UP_BULLETIN" -- 没有需要弹出的公告
MSG_ON_MUSIC_STATUS_CHANGED = "MSG_ON_MUSIC_STATUS_CHANGED" -- 大厅音效设置变化(大厅已监听，不要重复监听)
MSG_ROOM_CONTROLLER_CREATED = "MSG_ROOM_CONTROLLER_CREATED" -- 游戏controller创建
MSG_NEW_COME_MAIL = "MSG_NEW_COME_MAIL" --有新的邮件
MSG_RED_DOT = "MSG_RED_DOT" -- 红点操作
MSG_BECOME_REGULAR_ACCOUNT = "MSG_BECOME_REGULAR_ACCOUNT" --成功转正
MSG_CREAT_NAME_ACCOUNT = "MSG_CREAT_NAME_ACCOUNT" -- 注册用户名账号成功
MSG_CREAT_PHONE_ACCOUNT = "MSG_CREAT_PHONE_ACCOUNT" -- 注册手机号账号成功
MSG_SHOW_LOBBY_BULLETIN_DLG = "MSG_SHOW_LOBBY_BULLETIN_DLG" -- 大厅弹窗公告
MSG_SHOW_HORSELAMP_DLG = "MSG_SHOW_HORSELAMP_DLG" -- 跑马灯
MSG_FEETYPE_MATCH_CHANGED = "MSG_FEETYPE_MATCH_CHANGED" -- 报名方式修改
MSG_SET_SHARE_CD = "MSG_SET_SHARE_CD" -- 更新分享时间
MSG_BANK_UPDATE_SUCCESS = "MSG_BANK_UPDATE_SUCCESS" --银行更新成功
MSG_AVTIVITY_UPDATE = "MSG_AVTIVITY_UPDATE" --活动界面更新
MSG_ACCOUNT_UNNORMAL = "MSG_ACCOUNT_UNNORMAL" --账号异常
MSG_ACCOUNT_FROZEN = "MSG_ACCOUNT_FROZEN" --账号冻结
MSG_FUNCSWITCH_UPDATE = "MSG_FUNCSWITCH_UPDATE"   --功能开关数据更新
MSG_GAMELIST_UPDATE = "MSG_GAMELIST_UPDATE"    --游戏列表数据更新
MSG_HEADICON_CLICKED = "MSG_HEADICON_CLICKED"   --点击头像icon
MSG_FREEZE_CLOSE = "MSG_FREEZE_CLOSE" --冻结状态更新
MSG_ACT_SHARE_SUCCESS = "MSG_ACT_SHARE_SUCCESS" --游戏分享成功
MSG_SHARE_PHONE_CHARGE = "MSG_SHARE_PHONE_CHARGE" --赚话费数据回调
MSG_SHARE_FOR_GOLD = "MSG_SHARE_FOR_GOLD" --分享赚金币数据回调
MSG_ACTIVITY_DATA_UPDATE = "MSG_ACTIVITY_DATA_UPDATE"  --服务器活动数据返回
MSG_SHOW_LOBBY_ACTIVITY_DLG = "MSG_SHOW_LOBBY_ACTIVITY_DLG" -- 大厅活动弹窗
MSG_BIND_PHONE_SUCCESS = "MSG_BIND_PHONE_SUCCESS"   --绑定手机成功
MSG_CHECKOUT_REWARDS_SUCCESS = "MSG_CHECKOUT_REWARDS_SUCCESS"    --查询可兑换的奖励成功
MSG_APPLY_EXCHANG_REWARDS_SUCCESS = "MSG_APPLY_EXCHANG_REWARDS_SUCCESS"   -- 申请兑换奖励成功
MSG_QUERY_REWARDS_STATUS_SUCCES = "MSG_QUERY_REWARDS_STATUS_SUCCES"     --查询奖品兑换情况
MSG_BINDING_ID_SUCCES = "MSG_BINDING_ID_SUCCES"     --绑定身份证成功
MSG_CHANGE_NICKNAME_SUCCES = "MSG_CHANGE_NICKNAME_SUCCES"   --修改昵称成功
MSG_CHANGE_LOGIN_PWD_SUCCES = "MSG_CHANGE_LOGIN_PWD_SUCCES"  -- 修改登录密码成功
MSG_SEND_YZM_ASK   = "MSG_SEND_YZM_ASK"   --发送验证码结果
MSG_CHANGE_HEADICON_ASK  = "MSG_CHANGE_HEADICON_ASK"   --更换头像结果
MSG_CHANGE_HEADFRAME_ASK  = "MSG_CHANGE_HEADFRAME_ASK"   --更换头像框结果
MSG_KICK_NOTIVICE = "MSG_KICK_NOTIVICE" --踢人通知
MSG_SHOW_REGULARTIPS_DLG = "MSG_SHOW_REGULARTIPS_DLG"  -- 显示提醒玩家转正的对话框
MSG_SETBANK_PWD_SUCCESS = "MSG_SETBANK_PWD_SUCCESS" --设置银行密码成功
MSG_VERIFY_NOTICE = "MSG_VERIFY_NOTICE" --异常登录验证通知
MSG_RESET_PASSWD_NOTICE = "MSG_RESET_PASSWD_NOTICE" --重置密码通知
MSG_TIME_OUT_NOTICE = "MSG_TIME_OUT_NOTICE" --操作超时通知
MSG_SEND_PHONENUM = "MSG_SEND_PHONENUM"   --发送手机号码
MSG_PHONE_BINGDING_TPIS = "MSG_PHONE_BINGDING_TPIS"  --绑定手机提示
MSG_KICK_OTHER_TIPS = "MSG_KICK_OTHER_TIPS"  --顶号方提示
MSG_SHOW_HIDE_LOBBY = "MSG_SHOW_HIDE_LOBBY"    --隐藏或者显示大厅界面按钮
MSG_RE_BIND_WX  = "MSG_RE_BIND_WX"      -- 微信已解绑，需重新登录
MSG_ITEM_PRESENTED  = "MSG_ITEM_PRESENTED"  --赠送道具响应
MSG_ITEM_PRESENTED_CONFIRM = "MSG_ITEM_PRESENTED_CONFIRM"    --赠送道具确认响应 
MSG_USEBROADCASTPROP    = "MSG_USEBROADCASTPROP"        --传音入密
MSG_KICKPLAY_MAINTANENCE = "MSG_KICKPLAY_MAINTANENCE"   -- 服务器维护   踢人通知  CS_H2C_KickNotice_Nty
MSG_CHECK_SERVER_STATUS = "MSG_CHECK_SERVER_STATUS"    --服务器状态信息
MSG_UPDATE_DIAMOND_SHOP_ITEM_SUCCESS = "MSG_UPDATE_DIAMOND_SHOP_ITEM_SUCCESS"   --钻石商城商品列表更新成功
MSG_UPDATE_DIAMOND_SHOP_RECORD_SUCCESS = "MSG_UPDATE_DIAMOND_SHOP_RECORD_SUCCESS" --钻石商城记录列表更新成功
MSG_UPDATE_OR_DOWNLOAD_GAME  = "MSG_UPDATE_OR_DOWNLOAD_GAME"   --下载或者更新游戏
MSG_GAME_SHOW_MESSAGE = "MSG_GAME_SHOW_MESSAGE" -- 游戏提示内容(主要CS_M2C_ShowMessage_Nty协议内容)
MSG_ADD_LOADING = "MSG_ADD_LOADING"  --如果有加载界面则去掉加载界面，如果没有则添加一个加载界面
MSG_ROMOVE_LOADING = "MSG_ROMOVE_LOADING"
MSG_MALL_ITEM_AVAILABLE = "MSG_MALL_ITEM_AVAILABLE" -- 道具可购买剩余数量足够
MSG_BACK_HALL_VIEW_CLASSIFY = "MSG_BACK_HALL_VIEW_CLASSIFY" -- 返回大厅


MSG_FINDPWD_CHECK_PHONE_ASK = "MSG_FINDPWD_CHECK_PHONE_ASK" -- 找回密码 检查手机号
MSG_FINDPWD_RESET_ASK = "MSG_FINDPWD_RESET_ASK" --找回密码下一步
MSG_STOP_DOWNLOAD = "MSG_STOP_DOWNLOAD" --断网时停止下载
MSG_RE_CREATE_VISITOR = "MSG_RE_CREATE_VISITOR" --密码错误时弹出游客登录选项
MSG_LOGIN_ERROR_CLOSE_GAME = "MSG_LOGIN_ERROR_CLOSE_GAME"  --第三方登录失败，关闭游戏

MSG_TODAY_RANK_ASK  =  "MSG_TODAY_RANK_ASK" --获取排行榜请求
MSG_YDY_RANK_ASK  = "MSG_YDY_RANK_ASK"  --获取昨日排行榜请求
MSG_RANK_PLAYER_DETAIL = "MSG_RANK_PLAYER_DETAIL" --排行榜玩家具体排行信息

MSG_REBATE_DATA_ASK = "MSG_REBATE_DATA_ASK"  --推广返利请求
MSG_REBATE_BALANCE_DATA_ASK = "MSG_REBATE_BALANCE_DATA_ASK" --推广返利结算请求
MSG_REBATE_DETAIL_ASK = "MSG_REBATE_DETAIL_ASK" --推广返利详情请求
MSG_REBATE_RECORDE_ASK = "MSG_REBATE_RECORDE_ASK" -- 推广返利奖励记录
MSG_REBATE_WEEKRANK_ASK = "MSG_REBATE_WEEKRAN_ASK" -- 返利排行榜
MSG_SHOW_MAIL_RED = "MSG_SHOW_MAIL_RED"  --邮件小红点
MSG_SHOW_SHARE_RED = "MSG_SHOW_SHARE_RED"  --分享小红点

MSG_GETTASKLIST_ACK = "MSG_GETTASKLIST_ACK" --任务列表
MSG_ACCEPTTASKREWARD_ACK = "MSG_ACCEPTTASKREWARD_ACK" --领取任务

MSG_REQ_DOWANLOADSTATE = "MSG_REQ_DOWANLOADSTATE" --请求对应游戏下载状态
MSG_GET_DOWANLOADSTATE = "MSG_GET_DOWANLOADSTATE"  --获得对应游戏下载状态

MSG_ALIPAYBIND_ACK      ="MSG_ALIPAYBIND_ACK"      --响应绑定支付宝
MSG_BANKBIND_ACK        ="MSG_BANKBIND_ACK"        --响应绑定银行卡
MSG_EXCHANGE2MONEY_ACK  ="MSG_EXCHANGE2MONEY_ACK"  --兑换返回
MSG_EXCHANGECOINOUTHIST ="MSG_EXCHANGECOINOUTHIST" --获取兑换历史记录

MSG_BJL_RANK_ASK ="MSG_BJL_RANK_ASK" --百家乐人数
MSG_OPENSHOP ="MSG_OPENSHOP" --打开商城


LOADROULETTEHIST_ACK ="LOADROULETTEHIST_ACK"  -- 幸运转盘历史信息
LOADALLROULETTEHIST_ACK ="LOADALLROULETTEHIST_ACK"  -- 幸运转盘大奖信息
DRAWLOTTERY_ACK ="DRAWLOTTERY_ACK"  -- 抽奖
NEWALLROULETTEHIST_ACK ="NEWALLROULETTEHIST_ACK"  -- 添加信息
SHOWSIGNINCHANNEL_ACK="SHOWSIGNINCHANNEL_ACK"
PLAYERSIGNIN_ACK="PLAYERSIGNIN_ACK"
SHOWDEPOSITCHANNEL_ACK="SHOWDEPOSITCHANNEL_ACK"
GAMERECORD_ACK="GAMERECORD_ACK"
GAMERECORDIST_ACK="GAMERECORDIST_ACK"
POPSCENE_ACK ="POPSCENE_ACK"
SHOWSAFEGOLD_ACK ="SHOWSAFEGOLD_ACK"
RECEIVESAFEGOLD_ACK ="RECEIVESAFEGOLD_ACK"
UPDATE_GAME_RESOURCE = "UPDATE_GAME_RESOURCE"
REFRESH_QRCODE = "REFRESH_QRCODE"

MSG_REBATE_DAY_SHARE_ASK = "MSG_REBATE_DAY_SHARE_ASK"

MSG_REBATE_DAY_SHAREGOLD_ASK = "MSG_REBATE_DAY_SHAREGOLD_ASK"

MSG_VIP_DETAILS = "MSG_VIP_DETAILS"
MSG_TASKLIST = "MSG_TASKLIST"
MSG_TASKREWARD = "MSG_TASKREWARD"

--即时通讯
M2C_ChatRoom_Login_Ack ="M2C_ChatRoom_Login_Ack"
M2C_ChatRoom_Login_Nty ="M2C_ChatRoom_Login_Nty"
M2C_ChatRoom_Logout_Ack ="M2C_ChatRoom_Logout_Ack"
M2C_ChatRoom_Logout_Nty ="M2C_ChatRoom_Logout_Nty"
M2C_ChatRoom_SendMessage_Ack ="M2C_ChatRoom_SendMessage_Ack"
M2C_ChatRoom_SendMessage_Nty ="M2C_ChatRoom_SendMessage_Nty"
CMC_IM_Heartbeat_Ack ="CMC_IM_Heartbeat_Ack"
M2C_IM_InvalidTag_Nty ="M2C_IM_InvalidTag_Nty"
M2C_IM_Router_Nty ="M2C_IM_Router_Nty"
M2C_IM_kick_Nty ="M2C_IM_kick_Nty"
M2C_IM_disconnecting_Nty ="M2C_IM_disconnecting_Nty"
M2C_IM_NoSuch_Nty ="M2C_IM_NoSuch_Nty"
M2C_ChatRoom_NewMessage = "M2C_ChatRoom_NewMessage"
M2C_Friend_AddFriend_Ack = "M2C_Friend_AddFriend_Ack"
M2C_Friend_AddFriend_Nty = "M2C_Friend_AddFriend_Nty"
M2C_Friend_AddFriend_Final_Ack = "M2C_Friend_AddFriend_Final_Ack"
M2C_Friend_AddFriend_Final_Nty = "M2C_Friend_AddFriend_Final_Nty"
M2C_Friend_GetAllFriendDetails_Ack = "M2C_Friend_GetAllFriendDetails_Ack"
M2C_Friend_DelOneFriend_Ack = "M2C_Friend_DelOneFriend_Ack"
M2C_Friend_SendMessage_Nty = "M2C_Friend_SendMessage_Nty"
M2C_Friend_SendMessage_Ack = "M2C_Friend_SendMessage_Ack"
M2C_Friend_EmptyMessage = "M2C_Friend_EmptyMessage"
--群聊
M2C_Group_CreateGroup_Ack = "M2C_Group_CreateGroup_Ack"
M2C_Group_AddFriendToGroup_1_Nty = "M2C_Group_AddFriendToGroup_1_Nty"
M2C_Group_AddFriendToGroup_2_Nty = "M2C_Group_AddFriendToGroup_2_Nty"
M2C_Group_AddFriendToGroup_Ack = "M2C_Group_AddFriendToGroup_Ack"
M2C_Group_ExitGroup_Ack = "M2C_Group_ExitGroup_Ack"
M2C_Group_ExitGroup_Nty = "M2C_Group_ExitGroup_Nty"
M2C_Group_ReadGroup_Ack = "M2C_Group_ReadGroup_Ack"
M2C_Group_ReadAllGroup_Ack = "M2C_Group_ReadAllGroup_Ack"
M2C_Group_DelMemberFromGroup_Ack= "M2C_Group_DelMemberFromGroup_Ack"
M2C_Group_DelMemberFromGroup_1_Nty = "M2C_Group_DelMemberFromGroup_1_Nty"
M2C_Group_DelMemberFromGroup_2_Nty = "M2C_Group_DelMemberFromGroup_2_Nty"
M2C_Group_SendMessage_Ack = "M2C_Group_SendMessage_Ack"
M2C_Group_SendMessage_Nty = "M2C_Group_SendMessage_Nty"
M2C_Group_DeleteGroup_Ack = "M2C_Group_DeleteGroup_Ack"
M2C_Group_DeleteGroup_Nty = "M2C_Group_DeleteGroup_Nty"
M2C_Group_EmptyMessage = "M2C_Group_EmptyMessage"
-- 消息机制

--- 全局唯一消息需在列表中说明
g_MsgOnlyOne = {
    MSG_KICK_NOTIVICE ,
    MSG_TIME_OUT_NOTICE ,
    MSG_GOTO_STACK_LAYER ,
    MSG_SHOW_ERROR_TIPS ,
    MSG_RECONNECT_LOBBY_SERVER,
    MSG_MAINTANENCE_INFO,
    MSG_KICKPLAY_MAINTANENCE,
    MSG_BACK_HALL_VIEW_CLASSIFY, -- 返回主界面
}

-- 注册消息
-- @param __ref(ref, class, etc): 参数
-- @param __msgName(string): 消息名称
-- @param __callback(function): 回调函数
function addMsgCallBack( __ref, __msgName, __callback )
    if __ref == nil or __msgName == nil or __callback == nil then
        print("addMsgCallBack error,msgName might be nil")
        return
    end
    local msgObj = 
    {
        ref = __ref, 
        callback = __callback
    }

    if g_callbackMsgList[__msgName] == nil then
        g_callbackMsgList[__msgName] = {}
    end
    if isOnlyOneMsg(__msgName) then
        g_callbackMsgList[__msgName] = {}
        if g_onlyOneMsgList[__msgName] == nil then
            g_onlyOneMsgList[__msgName] = {}
        end
        g_onlyOneMsgList[__msgName][#g_onlyOneMsgList[__msgName] + 1] = msgObj
    end
    g_callbackMsgList[__msgName][#g_callbackMsgList[__msgName] + 1] = msgObj
end

-- 发送消息
-- @param __msgName(string): 消息名称
-- @param ...(...): 参数
-- @return (bool) success or not
function sendMsg( __msgName, ... )
    if g_callbackMsgList[__msgName] == nil then
        print("[WARNING]: can not find msg callback with name: ", __msgName)
        return false
    end

    -- local index = 1
    -- while g_callbackMsgList[__msgName][index] ~= nil do
    --     g_callbackMsgList[__msgName][index].callback(__msgName, ...)
    --     index = index + 1
    -- end

    local t = clone(g_callbackMsgList[__msgName])
    local index = 1
    while t[index] ~= nil do
        t[index].callback(__msgName, ...)
        index = index + 1
    end

    return true
end

-- 注销消息
-- @param __ref(ref, class, etc): 参数
-- @param __msgName(string): 消息名称
function removeMsgCallBack( __ref, __msgName )
    if g_callbackMsgList[__msgName] == nil then
        print("[ERROR]: can not find msg callback with name: ", __msgName)
        return
    end

    for i, v in ipairs(g_callbackMsgList[__msgName]) do
        if v.ref == __ref then
            table.remove(g_callbackMsgList[__msgName], i)
            if g_onlyOneMsgList[__msgName] then
                -- dump(g_onlyOneMsgList[__msgName],"g_onlyOneMsgList["..__msgName)
                for i2 = #g_onlyOneMsgList[__msgName],1,-1 do
                    --如果移除自定义唯一消息默认监听本身，则从唯一列表里也去除
                    if __ref == g_onlyOneMsgList[__msgName][i2].ref then
                        table.remove(g_onlyOneMsgList[__msgName],i2)
                    end
                end
                -- dump(g_onlyOneMsgList[__msgName],"g_onlyOneMsgList["..__msgName.."after")
            end
            -- 移除了自定义唯一消息后需恢复默认监听
            if isOnlyOneMsg(__msgName) and hasOnlyOneMsgValue(__msgName) and #g_onlyOneMsgList[__msgName] >= 1 then
                local msgObj = g_onlyOneMsgList[__msgName][#g_onlyOneMsgList[__msgName]]
                addMsgCallBack(msgObj.ref, __msgName, msgObj.callback)
            end
            return true
        end
    end
    return false
end

-- 是否全局唯一消息
-- @param __msgName(string): 消息名称
-- @param table(table): 自定义table
-- @return (bool)
function isOnlyOneMsg(__msgName, table)
    local tab = table or g_MsgOnlyOne
    for k,v in pairs(tab) do
        if v == __msgName then
            return true
        end
    end
    return false
end

-- 唯一消息默认结构体是否存在
-- @param __msgName(string): 消息名称
-- @return (bool)
function hasOnlyOneMsgValue(__msgName)
    for k,v in pairs(g_onlyOneMsgList) do
        if k == __msgName then
            return true
        end
    end
    return false
end