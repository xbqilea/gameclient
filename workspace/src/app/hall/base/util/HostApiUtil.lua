--[[
手机信息类
@author chenxiongzhou

--使用方法，获得参数都是 字符串类型
local phoneType = QkaPhoneInfoUtil:getPhoneType()   -- 手机机型
local netType = QkaPhoneInfoUtil:getNetType()       -- 网络类型 （"wifi","2g","3g","4g",""）
local wifiName = QkaPhoneInfoUtil:getWifiName()     -- wifi名称
local phoneIP = QkaPhoneInfoUtil:getPhoneIP()       -- 手机ip
local imei = QkaPhoneInfoUtil:getIMEI()             -- 手机设备号

print("手机机型：" .. phoneType)
print("网络类型：" .. netType)
print("wifi名称：" .. wifiName)
print("手机ip：" .. phoneIP)
print("手机设备号：" .. imei)
--]]

QkaPhoneInfoUtil = {}
local iosCurPhoneInfoClass = "CurPhoneInfo" -- ios
--获取手机机型
function QkaPhoneInfoUtil:getPhoneType()
    if self:isAndroid() then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = nil
        local javaParams = nil
        local javaMethodSig = nil
        javaMethodName = "getPhoneType"
        javaParams = {
        }
        javaMethodSig = "()Ljava/lang/String;"
        local result,type = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        return type
    elseif device.platform == "ios" then
        local ok, ret = luaoc.callStaticMethod(iosCurPhoneInfoClass , "getCurrentDeviceModel")
        if ok then
            print("ios  getCurrentDeviceModel:" ..ret)
            dump(ret)   
        end
        return ret
    elseif device.platform == "windows" then
        return "windows"
    end
    return ""
end

-- 获取手机网络类型
function QkaPhoneInfoUtil:getNetType()
    if self:isAndroid() then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = nil
        local javaParams = nil
        local javaMethodSig = nil
        javaMethodName = "getNetType"
        javaParams = {
            }
        javaMethodSig = "()Ljava/lang/String;"
        local result,type = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        return type
    elseif device.platform == "ios" then
        local ok, ret = luaoc.callStaticMethod(iosCurPhoneInfoClass , "getNetworkTypeFromStatusBar")
            if ok then
                print("ios  getNetworkTypeFromStatusBar:" ..ret)
                dump(ret)   
            end
        return ret
    elseif device.platform == "windows" then
        return "windows"
    end
    return ""
end

-- 获取手机的wifi名字
function QkaPhoneInfoUtil:getWifiName()
    if self:isAndroid() then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = nil
        local javaParams = nil
        local javaMethodSig = nil
        javaMethodName = "getWifiName"
        javaParams = {
            }
        javaMethodSig = "()Ljava/lang/String;"
        local result,type = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        return type
    elseif device.platform == "ios" then
        local ok, ret = luaoc.callStaticMethod(iosCurPhoneInfoClass , "getWifiName")
            if ok then
                print("ios  getWifiName:" ..ret)
                dump(ret)   
            end
        return ret
    elseif device.platform == "windows" then
        return "windows"
    end
    return ""
end

-- 获取手机的IP
function QkaPhoneInfoUtil:getPhoneIP()
    --g_PhoneIP = "var returnCitySN = {"cip": "61.140.222.161", "cid": "440100", "cname": "广东省广州市"};"     
    if g_PhoneIP then 
        return g_PhoneIP
    else 
        g_aready_login_ip = true
         if device.platform == "android"then
--             local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
--             local javaMethodName = nil
--             local javaParams = nil
--             local javaMethodSig = nil
--             javaMethodName = "getPhoneIP"
--             javaParams = {
--                 }
--             javaMethodSig = "()Ljava/lang/String;"
--             local result,type = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)


            return "0.0.0.0"
            
         --    return type
         elseif device.platform == "ios" then
             local ok, ret = luaoc.callStaticMethod(iosCurPhoneInfoClass , "deviceIPAdress")
                 if ok then
                     print("ios  deviceIPAdress:" ..ret)
                     dump(ret)   
                 end
             return ret
         elseif device.platform == "windows" then
            
             return "0.0.0.0"
         end 
        return ""
    end
end

-- 获取手机的UUID
function QkaPhoneInfoUtil:getIMEI()
    if self:isAndroid() then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = nil
        local javaParams = nil
        local javaMethodSig = nil
        javaMethodName = "uniqueGlobalDeviceIdentifier"
        javaParams = {
            }
        javaMethodSig = "()Ljava/lang/String;"
        local result,type = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        return type
    elseif device.platform == "ios" then
        local ok, ret = luaoc.callStaticMethod(iosCurPhoneInfoClass , "getUUIDName")
            if ok then
                print("ios  getUUIDName:" ..ret)
                dump(ret)   
            end
        return ret
    elseif device.platform == "windows" then
        local Launcher = {}
        Launcher.writablePath = cc.FileUtils:getInstance():getWritablePath()
        function Launcher.writefile(path, content, mode)
            mode = mode or "w+b"
            local file = io.open(path, mode)
            if file then
                print("writefile file = ",file)
                if file:write(content) == nil then return false end
                io.close(file)
                print("writefile file1 = ",file)
                return true
            else
                print("writefile false file = ",file)
                return false
            end
        end
        function Launcher.writeLauncher_machineKey( machineKey )
            local content =  "machineKey = " .. machineKey --"DanTiGameKind = {}" .. "\n\r".. "DanTiGameKind.danTiGameKind = " .. danTiGameKind2 .. "\n\r" .."return DanTiGameKind"
            local result = Launcher.writefile(Launcher.writablePath .. "machineKey.txt",content)
            print("writeLauncher_machineKey machineKey =  ",result)
        end

        function Launcher.getLauncher_machineKey()
            if cc.FileUtils:getInstance():isFileExist(Launcher.writablePath .. "machineKey.txt") then
                local str = cc.FileUtils:getInstance():getStringFromFile(Launcher.writablePath .. "machineKey.txt")
                local a = string.split(str,"=")
                print("a[1] = ",a[1])
                print("a[2] = ",a[2])
                local  DanTiGameKind = a[2] -- danTiGameKind.danTiGameKind
                return string.trim(DanTiGameKind)
            else
                return nil
            end
        end
        local Launcher_machineKey = Launcher.getLauncher_machineKey()
        if Launcher_machineKey == "" or Launcher_machineKey == nil then
            local code = {"0","1","2","3","4","5","6","7","8","9","a",
            "b","c","d","e","f","g","h","i","j","k","l","m","o","p","q","r",
                "s","t","u","v","w","x","y","z"} 
            local count = #code
            math.randomseed(os.time())
            local mimaStr = ""
            for i=1,12 do
                local mima = math.random(1,count)
                
                mimaStr = mimaStr .. code[mima]
            end
            Launcher_machineKey = mimaStr
            Launcher.writeLauncher_machineKey(Launcher_machineKey)
        end
        return Launcher_machineKey
    end
    return ""
end

function QkaPhoneInfoUtil:isAndroid()
    if device.platform == "android" then
        return true
    else 
        return false
    end
end

function QkaPhoneInfoUtil:getParamsStr()
    print("===QkaPhoneInfoUtil:getParamsStr==")
    self.spid = ""
    self.shareType = ""
    self.spreadType = ""
    local paramsStr = ""
    if device.platform == "android" then
        local javaParams = {} 
        local luaj=import("framework.luaj")
        local CLASS_NAME = "org/cocos2dx/lua/AppActivity"
        local methodName = "getParamsStr"

        local ok, ret = luaj.callStaticMethod(CLASS_NAME,methodName,nil,"()Ljava/lang/String;")
        if ok then
            print("-----------ok=true")
            print("----------ret = " .. ret)
            paramsStr = ret
        else
            print("-----------ok=false")
            paramsStr = ""
        end
    elseif device.platform == "ios" then
        print("ios getParamsStr")
        -- local ocClassName = "CurPhoneInfo"
        -- local ok, ret = Launcher.luaoc.callStaticMethod(ocClassName, "getParamsStr")
        -- if ok then
        --     paramsStr = ret
        -- end
    end
    print("self.paramsStr = " .. paramsStr)



    local params = string.split(paramsStr, "_")
    if params[1] then
        self.spid = params[1]
    else
        if paramsStr and #paramsStr then
            self.spid = paramsStr
        end
    end
    if params[2] then 
        self.shareType = params[2]
    end
    if params[3] then 
        self.spreadType = params[3]
    end

end

-- 获取推广id
function QkaPhoneInfoUtil:getSPID()
    print("===QkaPhoneInfoUtil:getSPID===")
    local spid = ""
    if spid == "" then
        if self.spid == nil then
            self:getParamsStr()
        end
    else
        self.spid = spid 
    end
    return self.spid
end

-- 获取渠道号
function QkaPhoneInfoUtil:getChannel()
    --兼容老模式
    if self.channel == "" then
        local sharedApplication = cc.Application:getInstance()
        local target = sharedApplication:getTargetPlatform()
        if (cc.PLATFORM_OS_ANDROID  == target) then 
            local javaParams = {} 

            local luaj=import("framework.luaj")
            local CLASS_NAME = "org/cocos2dx/lua/MilaiPublicUtil"
            local methodName = "getChannel"

            local ok, ret = luaj.callStaticMethod(CLASS_NAME,methodName,nil,"()Ljava/lang/String;")
            if ok then
                print("-----------ok=true")
                print("----------ret = " .. ret)
                self.channel = ret
            else
                print("-----------ok=false")
                self.channel = ""
            end
        end
    end
    print("--------self.channel = ",self.channel)
    return self.channel 
end

-- 获取分享类型
function QkaPhoneInfoUtil:getShareType()
    print("===QkaPhoneInfoUtil:getShareType===")
    if self.shareType == nil then
        self:getParamsStr()
    end
    if string.len(self.shareType) == 0  then
        --兼容老模式
        local sharedApplication = cc.Application:getInstance()
        local target = sharedApplication:getTargetPlatform()
        if (cc.PLATFORM_OS_ANDROID  == target) then 
            local javaParams = {} 

            local luaj=import("framework.luaj")
            local CLASS_NAME = "org/cocos2dx/lua/AppActivity"
            local methodName = "getShareType"

            local ok, ret = luaj.callStaticMethod(CLASS_NAME,methodName,nil,"()Ljava/lang/String;")
            if ok then
                print("-----------ok=true")
                print("----------ret = " .. ret)
                self.shareType = ret
            end
        end
        if string.len(self.shareType) == 0  then
            self.shareType = 99
        end
    end
    return self.shareType
end

-- 获取推广类型
function QkaPhoneInfoUtil:getSpreadType()
    print("===QkaPhoneInfoUtil:getSpreadType===")
    if self.spreadType == nil then
        self:getParamsStr()
    end
    if string.len(self.spreadType) == 0  then
        self.spreadType = -1
    end
    return self.spreadType
end

function QkaPhoneInfoUtil:getIOSChannelID()  -- 判断是否越狱包,充值时用到
    print("ios getParamsStr")
    local ocClassName = "CurPhoneInfo"
    local ok, ret = luaoc.callStaticMethod(ocClassName, "getParamsStr")
    if ok then
        return ret
    end
    return nil
end



-- 获取电池电量 100为满，0为没电，-1为获取失败
function QkaPhoneInfoUtil:getBatteryLevel()
    local batteryLevel
    if device.getBatteryLeve() then
        batteryLevel = tonumber(device.getBatteryLeve())
    end
    if not batteryLevel then
        batteryLevel = -1
    end
    return batteryLevel
end

-- 获取wifi信号量 50~100为 信号好，30到50为 信号一般，30以下为 信号差, 小于0表示wifi断开
function QkaPhoneInfoUtil:getWifiStrength()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "getWifiStrength"
        local javaParams = {}
        local javaMethodSig = "()I"
        local ok,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
            return ret
        end
    elseif device.platform == "ios" then -- IOS的强度是0-5个圆点表示，5个最强，0个没有，返回值0-5
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "PhoneInfo"
        local args = nil
        local ok,ret  = luaoc.callStaticMethod(className,"getWifiStrength",args)
        if ok then
            print("ret = ",ret)
            return ret*20  -- 一个圆点是20，5个100
        else
            return 0
        end

    else
        return 100
    end
end

-- 返回旧平台
function QkaPhoneInfoUtil:returnToOldPlatform()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "returnToOldPlatform"
        local javaParams = {}
        local javaMethodSig = "()V"
        local ok,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
            return ret
        end
    end
end

-- 获取系统版本
-- 获取手机的UUID
function QkaPhoneInfoUtil:getSystemVersion()
    local ret = "0.0"
    if self:isAndroid() then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = nil
        local javaParams = nil
        local javaMethodSig = nil
        javaMethodName = "getSystemVersion"
        javaParams = {
            }
        javaMethodSig = "()Ljava/lang/String;"
        local result,ret = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        return ret
    elseif device.platform == "ios" then
        local luaoc = require "cocos.cocos2d.luaoc"
        local ok, ret = luaoc.callStaticMethod("PhoneInfo" , "getSystemVersion")
            if ok then
                print("ios  getSystemVersion:" ..ret)
                dump(ret)   
            end
        return ret
    end
    return ret
end

-- 判断录音权限
function QkaPhoneInfoUtil:getRecordPermisson()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = nil
        local javaParams = nil
        local javaMethodSig = nil
        javaMethodName = "getRecordPermisson"
        javaParams = {
            }
        javaMethodSig = "()Z"
        local result,ret = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        return ret
    else
        return true
    end
end