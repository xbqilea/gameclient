--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 工具类
require("app.hall.base.util.SLFacade")
require("app.hall.base.util.MsgEvent")
require("app.hall.base.ui.UIAdapter")
require("app.hall.base.define.Macro")
require("app.hall.base.network.ProtoResolve")
require("app.hall.base.util.ConfigUtil")
require("app.hall.base.network.ConnectServerMgr")
require("app.hall.main.control.MainlController")
require("app.hall.main.model.GameRoomData")
require("app.hall.base.util.platform")
--require("app.hall.base.third.GameShare")
require("app.hall.base.network.ConnectStateCb")
require("app.hall.base.util.LuaUtils")
require("app.newHall.define.gameConstants")
require("app.newHall.public.helper")
require("app.newHall.manager.CacheManager") 
 local FloatMessage = require("src.app.newHall.layer.FloatMessage")
local GameMusicUtil = require("app.hall.base.voice.MusicUtil")
local GameSwitchUtil = require("app.hall.base.util.PhoneSwitchUtil")
local XbDialog = require("app.hall.base.ui.CommonView")

local TableViewEx = require("app.hall.base.ui.TableViewEx")
local KKMusicPlayer = require("app/hall/base/voice/KKMusicPlayer")
require("app.newHall.manager.AudioManager")
ToolKit = class("ToolKit")


-- 初始化环境(所有ToolKit类的变量放到这里)
function ToolKit:init()
    self.normalSpriteGLProgram = {}
        math.randomseed(os.time())
    if g_GameMusicUtil == nil then -- 音效类单例
        g_GameMusicUtil = GameMusicUtil.new()
    end
    if g_GameSwitchUtil == nil then
        g_GameSwitchUtil = GameSwitchUtil.new()
    end
     if g_AudioPlayer == nil then
        g_AudioPlayer = KKMusicPlayer.new()
    end
    ToolKit:addSearchPath("src/app/hall/config/code")
end


-- 将字符串转成字符数组
-- @param __str(string): 字符串
-- @return arr(table): 单个字符组成的table
function ToolKit:char2Array( __str )
    local arr = {}    i = 1 
    while true do 
        c = string.sub(__str,i,i)
        b = string.byte(c)
        if b >= 239 then
            table.insert(arr, string.sub(__str,i,i+3))
            i = i + 4
        elseif b > 223 then
            table.insert(arr, string.sub(__str,i,i+2))
            i = i + 3
        elseif b > 128 then
            table.insert(arr, string.sub(__str,i,i+1))
            i = i + 2
        else
            table.insert(arr, c)
            i = i + 1
        end
        if i > #__str then
            break
        end
    end
    return arr
end


-- 将字符数组转成字符串
-- @param array(table): 字符数组
-- @return str(string): 组成的字符串
function ToolKit:array2Char( array )
	local str = ""
	for i = 1, #array do
		str = str .. array[i]
	end
	return str
end

-- 添加搜索路径
-- @param __path(string): 需添加的路径
function ToolKit:addSearchPath( __path )
    if device.platform == "windows" then
        cc.FileUtils:getInstance():addSearchPath(cc.FileUtils:getInstance():getWritablePath() .. UpdateConfig.updateDirName..__path)
    else
        cc.FileUtils:getInstance():addSearchPath(cc.FileUtils:getInstance():getWritablePath()..UpdateConfig.updateDirName..__path)
    end
    cc.FileUtils:getInstance():addSearchPath(__path)
end

-- 添加搜索路径
-- @param __path(string): 需添加的路径
function ToolKit:removeSearchPath( __path )
    local t_finalsearchpath = {}
    local s_pathtemp = cc.FileUtils:getInstance():getWritablePath() .. UpdateConfig.updateDirName..__path
    local t_searchpath = cc.FileUtils:getInstance():getSearchPaths()
    for k,v in pairs(t_searchpath) do
        if not string.find(v, __path) then
            table.insert(t_finalsearchpath, v)
        end
    end
    cc.FileUtils:getInstance():setSearchPaths(t_finalsearchpath)
end

-- 创建动画，
-- @param __path(string) 所在路径(以"/"结尾)
-- @param __fileName(string) json资源的名字（不包括后缀名)
-- @param __fileCount(int)  json需要的plist资源数量  可选(当需要多plist时,必须填写)
function ToolKit:createArmatureAnimation( __path, __fileName, __fileCount )
    local manager = ccs.ArmatureDataManager:getInstance()

    if __fileCount and __fileCount > 1 then
        for i = 0, __fileCount - 1 do
            local png = __path .. __fileName .. i .. ".png"
            local plist = __path .. __fileName .. i .. ".plist"
            local json = __path .. __fileName .. ".ExportJson"
            manager:addArmatureFileInfo(png, plist, json)
        end
    else
        local png = __path .. __fileName .. "0.png"
        local plist = __path .. __fileName .. "0.plist"
        local json = __path .. __fileName .. ".ExportJson"
        manager:addArmatureFileInfo(png, plist, json)
    end
    return ccs.Armature:create(__fileName) 
end

-- 设置游戏帧率
-- @param fps(number) 帧率
function ToolKit:setGameFPS( fps )
    if fps == cc.Director:getInstance():getAnimationInterval() then
        print("fps is already ",fps)
        return
    end
    local function delay()
        local sharedDirector = cc.Director:getInstance()
    	if fps then
    		cc.Director:getInstance():setAnimationInterval(fps)
    	else
    		cc.Director:getInstance():setAnimationInterval(1/30.0)
    	end
    end
    ToolKit:delayDoSomething(delay, 2)
end
 function ToolKit:getHead(id)  
    cc.SpriteFrameCache:getInstance():addSpriteFrames("common/head/head.plist","common/head/head.png")
    local str = "" 
    --  id = id%20
    if id == 21 then
        return "head_super_100.png"
    elseif id>10 then 
        local i = id -10
	    if i == 10 then
           str = "head_mman_10.png"
         elseif id ==0 then
             str = "head_mman_01.png"
        else
            str = "head_mman_0"..i..".png"
        end 
    else
         if id == 10 then
           str = "head_mwoman_10.png"
        elseif id ==0 then
             str = "head_mwoman_01.png"
        else
            str = "head_mwoman_0"..id..".png"
        end 
    end
    print(str)
    return str
 end

-- 精灵置灰
-- @param _sprite(cc.Sprite) 精灵
function ToolKit:addGrayToSprite( _sprite )
    if self.normalSpriteGLProgram[tostring(_sprite)] == nil then
        self.normalSpriteGLProgram[tostring(_sprite)] = _sprite:getGLProgram()
    end
    local p = cc.GLProgram:createWithFilenames("shader/gray.vsh", "shader/gray.fsh")
    p:bindAttribLocation("a_position", 0)
    p:bindAttribLocation("a_color", 1)
    p:bindAttribLocation("a_texCoord", 2)
    p:link()
    p:updateUniforms()
    _sprite:setGLProgram(p)
end

-- 清除精灵置灰
-- @param _sprite(cc.Sprite) 精灵
function ToolKit:removeGrayFromSprite( _sprite )
    _sprite:setGLProgram(self.normalSpriteGLProgram[tostring(_sprite)])
end

-- 注册析构
function ToolKit:registDistructor( _node, _handler )
    _node:addNodeEventListener(cc.NODE_EVENT, function(event)
        if event.name == "cleanup" then
            _handler()
        end
    end)
end

-- 获得一个c3b
-- @params str(string) 颜色值字符串
-- @return (cc.c3b) 转换之后的c3b值
function ToolKit:getCCC3( str )
    local r, g, b = string.match(str, "(%x%x)(%x%x)(%x%x)")
    return cc.c3b(tonumber("0x"..r), tonumber("0x"..g), tonumber("0x"..b))
end

-- 获得一个c4b(a = 255)
-- @params str(string) 颜色值字符串
-- @return (cc.c4b) 转换之后的c4b值
function ToolKit:getCCC4A( str )
    local r, g, b = string.match(str, "(%x%x)(%x%x)(%x%x)")
    return cc.c4b(tonumber("0x"..r), tonumber("0x"..g), tonumber("0x"..b), 255)
end




-- 错误提示公用接口
function ToolKit:showErrorTip( __errorId, __callback )
    local data = getErrorTipById(__errorId)
    if data then
        local dlg = nil
        -- TOAST
        if data.type == 1 then
            if data.tip then
                TOAST(data.tip, __callback)
            end
        -- 正确弹窗
        elseif data.type == 2 then
            local DlgAlert = require("app.hall.base.ui.MessageBox")
            dlg = DlgAlert.showRightAlert(data, __callback)
            return dlg

        -- 错误弹窗
        elseif data.type == 3 then
            local DlgAlert = require("app.hall.base.ui.MessageBox")
            dlg = DlgAlert.showErrorAlert(data, __callback)
            return dlg

        -- 提示弹窗
        elseif data.type == 4 then
            local DlgAlert = require("app.hall.base.ui.MessageBox")
            dlg = DlgAlert.showTipsAlert(data, __callback)
            return dlg

        end
    else
        TOAST("错误码: " .. __errorId, __callback)
    end
end

-- 错误提示公用接口
function ToolKit:showErrorTip4( __errorId, __callback )
    local data = getErrorTipById(__errorId)
    if data then
        local DlgAlert = require("app.hall.base.ui.MessageBox")
        local dlg = DlgAlert.showTipsAlert(data, __callback)
        return dlg
    else
        TOAST("错误码: " .. __errorId, __callback)
    end
end

-- 去掉loading加载页面
function ToolKit:removeLoadingDialog()
   
    local loadingDialog = g_loadingDialog--cc.Director:getInstance():getNotificationNode()
    if  loadingDialog and loadingDialog.closeDialog and type(loadingDialog.closeDialog) == "function" then
        loadingDialog:closeDialog()
    end
    --cc.Director:getInstance():setNotificationNode(nil)
    g_loadingDialog = nil
end

-- 增加loading加载页面
-- @param durTime(number): 持续时间
-- @param text(string): 提示文本
-- @param scene(SceneBase): 父节点场景
function ToolKit:addLoadingDialog(durTime, text, scene)
    ToolKit:removeLoadingDialog() 
     
    local LoadingDialog = require("app.hall.base.ui.LoadingView")
    local loadingDialog = LoadingDialog.new(durTime, text)
    loadingDialog:showDialog(scene)
--    if loadingDialog._delayShowTime then
--        loadingDialog:setDelayShow()
--    end
    g_loadingDialog = loadingDialog
    -- cc.Director:getInstance():setNotificationNode(loadingDialog)
end

-- loading加载页面是否存在
-- @return (bool): 
function ToolKit:hasLoadingDialog()
    local loadingDialog = g_loadingDialog--cc.Director:getInstance():getNotificationNode()
    if loadingDialog then
        return true
    end
    return false
end


--返回两点之间的距离
function ToolKit:distance(x1,y1,x2,y2)
    if type(x1) == "table" and type(y1) == "table" then
        return math.sqrt(math.pow((y1.y-x1.y),2)+math.pow((y1.x-x1.x),2))
    else
        return math.sqrt(math.pow((y2-y1),2)+math.pow((x2-x1),2))
    end
end

--时间戳转时间
function ToolKit:changeTime(seconds,dateformat)
    --http://wiki.interfaceware.com/569.html
    seconds = tonumber(seconds)
    dateformat = dateformat or "%Y-%m-%d %H:%M:%S"
    return os.date(dateformat, seconds)
end

-- 获取文件名以及后缀
-- @param __fileName(string) 文件名
-- @reurn prefix(string) 文件名 
-- @reurn suffix(string) 后缀名
function ToolKit:getPrefixAndSuffix( __fileName )
    local prefix = ""
    local suffix = ""
    
    -- 前缀
    local idx = __fileName:match(".+()%.%w+$")
    if(idx) then
        prefix = __fileName:sub(1, idx-1)
    else
        prefix = __fileName
    end
    -- 扩展名
    suffix = __fileName:match(".+%.(%w+)$")

    return prefix, suffix
end

-- lua(luac)脚本文件是否存在
-- @param __path(string) 文件路径
-- @reurn (bool) true or false 
function ToolKit:isLuaExist( __path )
    -- __path = "app.game/common.main.RoomController"
    local path = string.gsub(__path, "[.]+", "/")
    local luaExist = cc.FileUtils:getInstance():isFileExist(path .. ".lua")
    local luacExist = cc.FileUtils:getInstance():isFileExist(path .. ".luac")
    -- print(path, tostring(luaExist), tostring(luacExist))
    if not luacExist and not luaExist then
        return false
    end
    return true
end

-- 获取当前场景的界面盏管理器
-- @return (StackLayerManager) 当前场景的界面盏管理器
function ToolKit:getCurStackManager()
    local scene = cc.Director:getInstance():getRunningScene()
    return scene:getStackLayerManager()
end

-- 获取当前场景
-- @return (Scene) 当前场景
function ToolKit:getCurrentScene()
    return cc.Director:getInstance():getRunningScene()
end

-- 弹出提示框
-- @param _str(string) : 内容
-- @param __callback: 完成后回调函数
function TOAST( __str, __callback,flag )  
 --   ShowSmallMsg(__str , __callback,flag)
    -- local FloatMessage = FloatMessage.new()
    -- local scene = cc.Director:getInstance():getRunningScene()
    -- scene:addChild(FloatMessage,100000)
    -- FloatMessage:pushMessageDebug(__str)
    FloatMessage.TOAST(__str)
end

-- 保存本地日志
function TLOG( ... )
    LogUtil = require("app.hall.base.log.Log")
    LogUtil:getInstance():writeLog(...)
end

-- 弹出小型提示框
function ShowSmallMsg( __str , __callback,flag )
    local dialog = require("app.hall.base.ui.TextTips")
    dialog.new(__str, __callback,flag)
end

-- 公用用户协议界面
-- @param parent : 父节点，如果不传该参数会取当前层加入
function ShowUserAgreement(parent, skinNode)
    local layer = parent
    if parent == nil then
        layer = ToolKit:getCurStackManager():getCurrentLayer()
    end

   local _dlg =  skinNode or require("src.app.hall.userinfo.view.UserProtoLayer").new()
   -- _dlg:setParentStacklayer(layer)
    _dlg:showDialog()
end

-- 显示透明页面
function showTransParentDlg()
    if g_TransParentDlg == nil then
        g_TransParentDlg = XbDialog.new()
        g_TransParentDlg:setColorAndOpacity(cc.c3b(0, 0, 0), 0)
        g_TransParentDlg:setMountMode(DIALOGMOUNT.NONE)
        g_TransParentDlg:showDialog()
        g_TransParentDlg:enableTouch(false)
    end
end

-- 隐藏透明页面
function hideTransParentDlg()
    if g_TransParentDlg and not tolua.isnull(g_TransParentDlg) then
        g_TransParentDlg:closeDialog()
        g_TransParentDlg = nil 
    end
end


-- 检查输入的字符串是否是数字
-- @param text(string) 源字符串
-- @return (bool) true or false
function ToolKit:CheckNum(text)
    if text == nil then
        return false
    end
    local len = string.len(text)

    if len == 0 then
        return false
    end

    for var = 1, len do
        repeat
            local num = string.byte(text,var)

            if num >= 48 and num <= 57 then

                if var == 1 and num == 48 then
                    return false
                end
                --数字
                break
            else
                return false
            end

        until true
    end
    return true
end



-- 获取验证码(生成验证码)
-- @return capcha(string) 验证码
function ToolKit:getCapcha()
    local code = 
        {
            "0","1","2","3","4","5","6","7","8","9",
            "a","b","c","d","e","f","g","h","i","j",
            "k","l","m","n","o","p","q","r","s","t",
            "u","v","w","x","y","z",
            "A","B","C","D","D","F","G","H","I","J","K","L","M","N", "O","P","Q","R",
            "S","T","U","V","W","X","Y","Z"}
    local len = #code
    local capcha = ""
    for i=1,4 do
        local index = math.random(1,len)
        capcha = capcha .. code[index]
    end
    return capcha
end


--判断账号是否合法
function ToolKit:onEditIsAccountOK(textStr)

    local text = textStr
    local lenth = string.len(text)
    if lenth == 0 then
        -- local msg = message or "账户不能为空，请输入"
        -- AssertDialog.new(nil,"提示信息", msg, {{"确定",nil}});
        --ToolKit:showErrorTip( 1101 )
        sendMsg(MSG_SHOW_ERROR_TIPS, 1101 ) 
        return false
    end

    if lenth < GlobalDefine.USER_NAME_LEN[1] or lenth > GlobalDefine.USER_NAME_LEN[2] then
        -- local msg = message or "账户长度6~12字符，请重新输入"
        -- AssertDialog.new(nil,"提示信息", msg, {{"确定",nil}})
        --ToolKit:showErrorTip( 1102 )
        sendMsg(MSG_SHOW_ERROR_TIPS, 1102 ) 
        -- print("2event",event)
        return false
    end
    local t = ""
    for text in string.gmatch(text,"[%w_]") do
        t = t .. text
    end
    if text ~= t then
        -- local msg = message or "账号格式错误，只能英文和数字或下划线组合，请重新输入"
        -- AssertDialog.new(nil,"提示信息", msg,{{"确定",nil}})
        --ToolKit:showErrorTip( 1103 )
        sendMsg(MSG_SHOW_ERROR_TIPS, 1103 ) 
        return false
    end
    print("t = ",t)
    return true
end


--判断密码是否合法
--合法返回true , 不合法返回false  返回false之前先返回错误提示的序号和callback
function ToolKit:onEditIsPasswordOK(textStr, _callback)

    local text = textStr
    local lenth = string.len(text)
    if lenth == 0 then
        -- local msg = message or "密码不能为空，请输入"
        -- AssertDialog.new(nil,"提示信息", msg, {{"确定",nil}});
        -- print("1event",event)
        --  ToolKit:showErrorTip( 1104, _callback )
        
        sendMsg(MSG_SHOW_ERROR_TIPS, 1104, _callback ) 
        return false
    end

    if lenth < GlobalDefine.USER_PWD_LEN[1] or lenth > GlobalDefine.USER_PWD_LEN[2] then
        -- local msg = message or "密码长度6~12字符，请重新输入"
        -- AssertDialog.new(nil,"提示信息", msg, {{"确定",nil}})
        -- print("2event",event)
        --ToolKit:showErrorTip( 1105 , _callback)
        sendMsg(MSG_SHOW_ERROR_TIPS, 1105, _callback ) 
        return false
    end
    
    local t = ""
    for text in string.gmatch(text,"[%w]") do
        t = t .. text
    end
    if text ~= t then
        -- local msg = message or "密码格式错误，只能英文和数字，请重新输入"
        -- AssertDialog.new(nil,"提示信息", msg,{{"确定",nil}});
        --ToolKit:showErrorTip( 1106, _callback )
        sendMsg(MSG_SHOW_ERROR_TIPS, 1106, _callback )
        return false
    end

    --连续数字
    local seriesNum = "01234567890"
    local seriesNum2 = "09876543210"
    local i = string.find(seriesNum,text)
    local i2 = string.find(seriesNum2,text)

    --相同数字
    local isSameChar = true
    for i=1,lenth-1 do
        local str1 = string.sub(text,i,i)
        local str2 = string.sub(text,i+1,i+1)
        if str1 ~= str2 then
            isSameChar = false
            break
        end
    end

    if i or i2 or isSameChar then
        -- local msg = "密码：不能是连续或者相同的字母或数字"
        -- AssertDialog.new(nil,"提示信息", msg,{{"确定",nil}})
        --ToolKit:showErrorTip( 1107, _callback )
        sendMsg(MSG_SHOW_ERROR_TIPS, 1107, _callback ) 
        return false
    end

    if string.find(text,"^[+-]?%d+$") then
        -- local msg = "密码：不能是单纯的数字"
        -- AssertDialog.new(nil,"密码过于简单", msg,{{"确定",nil}})
       -- ToolKit:showErrorTip( 1108, _callback )
        sendMsg(MSG_SHOW_ERROR_TIPS, 1108, _callback ) 
        return false
    end


    print("t = ",t)
    return true
end

-- 判断是否是手机号码
-- @param _phone(string) : 电话号码 
-- @reurn (bool) true or false 
function ToolKit:isPhoneNumber( _phone )

    if not _phone or string.match(_phone,"[1][3,4,5,6,7,8,9]%d%d%d%d%d%d%d%d%d") == _phone then
        return true
    else
        sendMsg(MSG_SHOW_ERROR_TIPS, 228 )
        return false
    end
end


function ToolKit:SubUTF8String(s, n)    
  local dropping = string.byte(s, n+1)  
  if not dropping then return s end 
  if dropping >= 128 and dropping < 192 then    
    return self:SubUTF8String(s, n+2)    
  end   
  return string.sub(s, 1, n)    
end 

--筛选去除非UTF8的字，比如emoji表情
function ToolKit:safeUtf8(str)
    local tab = ToolKit:char2Array(str)
    local res = ""
    for i,v in ipairs(tab) do
        if not ToolKit:isEmoji(v) then
            res = res .. v
        end
    end
    return res
end

function ToolKit:isEmoji(str)
    local byteLen = string.len(str)--编码占多少字节
    if byteLen > 3 then--超过三个字节的必须是emoji字符啊
        return true
    end

    if byteLen == 3 then
        if string.find(str, "[\226][\132-\173]") or string.find(str, "[\227][\128\138]") then
            return true--过滤部分三个字节表示的emoji字符，可能是早期的符号，用的还是三字节，坑。。。这里不保证完全正确，可能会过滤部分中文字。。。
        end
    end

    if byteLen == 1 then
        local ox = string.byte(str)
        if (33 <= ox and 47 >= ox) or (58 <= ox and 64 >= ox) or (91 <= ox and 96 >= ox) or (123 <= ox and 126 >= ox) or (str == "　") then
            return true--过滤ASCII字符中的部分标点，这里排除了空格，用编码来过滤有很好的扩展性，如果是标点可以直接用%p匹配。
        end
    end
    return false
end


--截取不超过 _len个字节的字符串， 一个汉字占3个字节 
--比如  ToolKit:getMaxUtf8String("发生大a幅个", 11)  返回  "发生大a"
function ToolKit:getMaxUtf8String(_str, _len)
    local _length = string.len(_str)
    
    if _length <= _len then
        return  _str
    end
    
	local n = 0

    while 1 do	
	    if n < _len then
            if string.byte(_str, n+1) >= 128  then
                n = n+ 3
            else
                n = n+ 1
            end
	    else
	       break
	    end		
	end
	
	if n > _len then
		n = n -3
	end
	
    return  string.sub(_str,1,n)
end


function ToolKit:getUtf8StringCount(str)
    local tmpStr=str
    local _,sum=string.gsub(str,"[^\128-\193]","")
    local _,countEn=string.gsub(tmpStr,"[%z\1-\127]","")
    return sum,countEn,sum-countEn
end


--数值处理
--number 数值
--isQian 是否取千,默认不取
--decimal 保留小数点位,默认2
function ToolKit:getNumberFormat(number, isQian,decimalCom)
    local decimal = "%0."..(decimalCom or 2).."f"
    local str = ""
    local bool = false
    local function isCurTain(number, format)
        if number % format == 0 then
            bool = true
        else
            bool = false
        end

        return bool
    end
    if number >= 1000000000000 then--万亿
        if isCurTain(number, 1000000000000) then
             str = string.format("%d万亿", number/1000000000000)
        else
            str = string.format(decimal,number/1000000000000).."万亿"
        end
    elseif number >= 100000000000 then--千亿
        if isCurTain(number, 100000000) then
             str = string.format("%d亿", number/100000000)
        else
            if(not decimalCom)then decimal = "%0.0f"end
            str = string.format(decimal,number/100000000).."亿"
        end
    elseif number >= 10000000000 then--百亿
        if isCurTain(number, 100000000) then
             str = string.format("%d亿", number/100000000)
        else
            if(not decimalCom)then decimal = "%0.1f"end
            str = string.format(decimal,number/100000000).."亿"
        end
    elseif number >= 100000000 then--十亿，亿
        if isCurTain(number, 100000000) then
             str = string.format("%d亿", number/100000000)
        else
            str = string.format(decimal,number/100000000).."亿"
        end
    elseif number >= 10000000 then--千万
        if isCurTain(number, 10000) then
             str = string.format("%d万", number/10000)
        else
            if(not decimalCom)then decimal = "%0.0f"end
            local numTmp = string.format(decimal,number/10000)
            if(tonumber(numTmp) >= 10000)then str = (numTmp/10000).."亿"
            else str = numTmp.."万" end
        end
    elseif number >= 1000000 then--百万
        if isCurTain(number, 10000) then
             str = string.format("%d万", number/10000)
        else
            if(not decimalCom)then decimal = "%0.1f"end
            str = string.format(decimal,number/10000).."万"
        end
    elseif number >= 10000 then--十万，万
        if isCurTain(number, 10000) then
             str = string.format("%d万", number/10000)
        else
            str = string.format(decimal,number/10000).."万"
        end
    else
        if isQian == true then

            if isCurTain(number, 1000) then
                str = string.format("%d千", number/1000)
            else
                str = string.format(decimal, number/1000).."千"
            end
        else
            str = string.format("%d", number)
        end
    end

    return str
end


--funciton --处理滑动敏感度
--layer   --当前层
--node    --当前节点
--slideDis --敏感度距离设置
--touchEndedBack  -- 回调函数

--添加点对点触摸事件监听处理
function ToolKit:listenerNodeTouch(layer, node, touchEndedBack, slideDis)

    local slideDisAbs = slideDis == nil and 5 or slideDis

    function onTouchBegan(touch, event)
        if node:getCascadeBoundingBox():containsPoint(touch:getLocation()) then
            node.beginPos = touch:getLocation()
            node.instance = 0
            return true
        end
        return false
    end

    function onTouchMoved(touch, event)
        if node.beginPos ~= nil then
            local pos = touch:getLocation()
            node.instance = self:distance(node.beginPos, pos)
        end
    end

    function onTouchEnded(touch, event)
        if node.beginPos ~= nil and node.instance < slideDisAbs then
           local target = event:getCurrentTarget()
           touchEndedBack(target)
        end
    end
    local listener = cc.EventListenerTouchOneByOne:create()
    layer.listener = listener
    listener:setSwallowTouches(false)

    layer.listener:registerScriptHandler(onTouchBegan,cc.Handler.EVENT_TOUCH_BEGAN )
    layer.listener:registerScriptHandler(onTouchMoved,cc.Handler.EVENT_TOUCH_MOVED )
    layer.listener:registerScriptHandler(onTouchEnded,cc.Handler.EVENT_TOUCH_ENDED )

    local eventDispatcher = cc.Director:getInstance():getEventDispatcher()
    eventDispatcher:addEventListenerWithSceneGraphPriority(listener, node)

end

--@ size                tableView大小
--@ cellsize            cell大小 (支持不同大小 在data中设置单独cell大小)
--@ data                tableView数据列表
--@ createCellEvent     创建cell函数
--@ direction           滚动方向 默认 cc.SCROLLVIEW_DIRECTION_VERTICAL  cc.SCROLLVIEW_DIRECTION_HORIZONTAL
--@ fillOrder           数据显示方向 默认 cc.TABLEVIEW_FILL_TOPDOWN  cc.TABLEVIEW_FILL_BOTTOMUP
function ToolKit:createTableView(size,cellsize,data,createCellEvent,direction,fillOrder)
    local dir   = direction or cc.SCROLLVIEW_DIRECTION_VERTICAL
    local fill  = fillOrder or cc.TABLEVIEW_FILL_TOPDOWN
    local tableView = TableViewEx.new(size)
    tableView:setDirection(dir)
    tableView:setVerticalFillOrder(fill)
    tableView:setViewData(data)
    tableView:setCellSize(cellsize)
    tableView:setCreateCellHandler(createCellEvent)
    tableView:reloadData()
    return tableView
end

function ToolKit:setResNode(resNode,parent)
    local nodeList = resNode:getChildren()
    parent[resNode:getName()] = {}
    parent[resNode:getName()].root = resNode
    for _,nodeObj in pairs(nodeList) do
        ToolKit:setResNode(nodeObj,parent[resNode:getName()])
    end
end

--utf8字符串，包括中英文混合，中文一个算2个
function ToolKit:onEditTextLenth(textStr)
    local width = 0
    local len  = string.len(textStr)  
    local left = len  
    local cnt  = 2  -- 中文算2个
    local arr  = {0, 0xc0, 0xe0, 0xf0, 0xf8, 0xfc}  --{0，192，224，240，248，252}
    while left ~= 0 do  
        local tmp = string.byte(textStr, -left)  
        local i   = #arr  
        while arr[i] do  
            if tmp >= arr[i] then  --（224，240）汉字
                left = left - i  
                if tmp >= arr[3] and tmp <= arr[4] then
                    width = width + cnt
                    print("汉字长度+2")
                else
                    width = width + 1 -- 普通字符加1
                end
                break  
            end  
            i = i - 1  
        end  
    end  
    print("width = ",width)
    return width
end

-- 字符串是否可转为数字
function  ToolKit:juageRecordEditBoxOk(numberStr)
    if string.find(numberStr,"^[+-]?%d+$") then
        local result = tonumber(numberStr)
        if result then
            return result
        end
    end
    -- if numberStr then
    --   local result = tonumber(numberStr)
    --   if result then
    --     return result
    --   end
    -- end
    return nil
end




-- 判断是否有sdcard 
function ToolKit:isExistSDCard()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "existSDCard"
        local javaParams = {}
        local javaMethodSig = "()Z"
        local ok ,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret

        else
            print("******判断是否存在sd卡失败，错误码：".. ret .. "********" )
            return ret
        end
    end
end


-- 获取sd卡的路径

function ToolKit:getSDcardPath()

    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "getSDcardPath"
        local javaParams = {}
        local javaMethodSig = "()Ljava/lang/String;"
        local ok ,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret

        else
            print("******获取sd卡路径失败，错误码：".. ret .. "********" )
            return ""
        end
    end
end

-- 复制文件到sdcard
function ToolKit:copyFileToSDcard( src, dst )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "copyFile"
        local javaParams = {src, dst}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;)V"
        local ok = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
        end
    end
end

-- 从assets复制到路径
function ToolKit:assetToPath( assetPath ,path )
    if assetPath == nil or #assetPath == 0 
    or path == nil or #path == 0 then
        return
    end 
    if device.platform == "android" then
        local result,path = luaj.callStaticMethod(
            "org/cocos2dx/lua/MilaiPublicUtil",
            "assetToPath", 
            {assetPath,path}, 
            "(Ljava/lang/String;Ljava/lang/String;)V"
            )
    end
end


-- 判断文件是否存在
function ToolKit:fileIsExists( fileName)
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "fileIsExists"
        local javaParams = {fileName}
        local javaMethodSig = "(Ljava/lang/String;)Z"
        local ok,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
            return ret
        end
    end
end


--保存文件到sd卡
function ToolKit:writeToSDFile( fileName, text )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "writeToSDFile"
        local javaParams = {fileName,text}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;)V"
        local ok,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
            return ok
        end
    end
end

--获取玩家的登陆信息
function ToolKit:getUserLoginInfor()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "getUserLoginInfor"
        local javaParams = {}
        local javaMethodSig = "()Ljava/lang/String;"
        local ok,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
            return ret
        end
    end
end

-- 拨打电话
function ToolKit:MakePhoneCall( _phoneNumber )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "makeTelePhoneCall"
        local javaParams = {_phoneNumber}
        local javaMethodSig = "(Ljava/lang/String;)I"
        local ok, ret = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok and ret == -1 then
            -- TOAST({text = "对不起，您还没有插入手机卡，可以输入问题反馈给我们，我们会及时跟您联系。"})
            device.openURL("tel:" .. _phoneNumber)
        end
    elseif device.platform == "ios" then
        print("ios 拨打电话 callPhoneNumber:" .._phoneNumber)
        local args = {
            phoneNumber = _phoneNumber,
            
            }
        local ok, ret = luaoc.callStaticMethod("AppController", "callPhoneNumber",args)
        if ok then
            print("ios 拨打电话调用成功 callPhoneNumber:" .._phoneNumber)
            dump(ret)
            if ret and ret.can == 0 then
               TOAST(STR(18,2))
            end
        end
    else
        TOAST( STR(19,2))
    end
end

-- 根据开关状态显示按钮
function ToolKit:setBtnStatusByFuncStatus( __btn, __id )
    local status = getFuncOpenStatus(__id)
    if status == 1 then -- 隐藏
        __btn:setVisible(false)
        if __btn:getDescription() == "Button" then
            __btn:setEnable(false)
        else
            __btn:setTouchEnabled(false)
        end
    elseif status == 2 then -- 置灰不可点击
        __btn:setVisible(true)
        if __btn:getDescription() == "Button" then
            __btn:setEnable(false)
        else
            __btn:setTouchEnabled(false)
        end
    elseif status == 0 then -- 不处理
        __btn:setVisible(true)
        if __btn:getDescription() == "Button" then
            __btn:setEnable(true)
        else
            __btn:setTouchEnabled(true)
        end
    end
end

--打开其他apk
-- _value  附带的信息，必须为json格式
function ToolKit:openOtherApk( _apkName ,_className )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "openOtherAppWithString"
        local javaParams = {_apkName,_className }
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;)V"
        local ok,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
           -- return ret
        end
    end
end

function ToolKit:doFile(path)
    local fileData = cc.HelperFunc:getFileData(path)
    local fun = loadstring(fileData)
    local ret, flist = pcall(fun)
    if ret then
        return flist
    end

    return flist
end

-- function ToolKit:afterCaptured4Dlg( __successed )
--     if __successed == 1 then
--         sendMsg(MSG_CAPTURE_4_DIALOG)
--     end
-- end

function ToolKit:delayDoSomething(func, time)
    local scheduler = require("framework.scheduler")
    scheduler.performWithDelayGlobal(func, time)
end


-- time 以秒为单位的时间
-- return 一个字符串  xx:xx:xx  天:时:分
function ToolKit:FormatTime(_time) 
    --  60*60*24
    --
    local time = _time
    local day = STR(48,1)
    local hour = STR(49,1)
    local minute = STR(50,1)

    if time > 86400  then
        day = (time - time%86400)/86400
        time =  time - 86400*day
        day = day .. STR(48,1)
    end

    if time > 3600 then
        hour = (time - time%3600)/3600
        time =  time - 3600*hour
        hour = hour .. STR(49,1)
    end

    if time > 0 then
        minute = (time - time%60)/60 + 1
        minute = minute .. STR(50,1)
    end

    return day .. hour .. minute
end

-- time 以秒为单位的时间
-- return 一个字符串  xx:xx:xx  天:时:分:秒
function ToolKit:FormatTimeSecond(_time) 
    --  60*60*24
    --
    local time = _time
    local day = ""
    local hour = ""
    local minute = ""
    local second = ""

    if time > 86400  then
        day = (time - time%86400)/86400
        time =  time - 86400*day
        day = day .. STR(48,1)
    end

    if time > 3600 then
        hour = (time - time%3600)/3600
        time =  time - 3600*hour
        hour = hour .. STR(49,1)
    end

    if time > 60 then
        minute = (time - time%60)/60
        minute = minute .. STR(50,1)
    end

    if time > 0 then
        second = time%60
        second = second .. STR(93,1)
    end

    return day .. hour .. minute .. second
end

--[[-- 
创建动画，
@param _path(string) 所在路径
@param _fileName(string) json资源的名字（不包括后缀名)
@param _fileCount(int)  json需要的plist资源数量  可选(当需要多plist时,必须填写)
@param _isNeedRemove(bool)  是否需要加入清除列表  可选
]]
function ToolKit:createFrameAnimation( _path, _fileName, _fileCount, _isNeedRemove )
    local manager = ccs.ArmatureDataManager:getInstance()
    if _fileCount and _fileCount > 1 then
        for i = 0, _fileCount - 1 do
            local png = _fileName .. i .. ".png"
            local plist = _fileName .. i .. ".plist"
            local json = _fileName .. ".ExportJson"
            manager:addArmatureFileInfo(png, plist, json)
        end
    else
        local png = _fileName .. "0.png"
        local plist = _fileName .. "0.plist"
        local json = _fileName .. ".ExportJson"
        manager:addArmatureFileInfo(png, plist, json)
    end

    local armature = ccs.Armature:create(_fileName) 
    return armature
end

--[[-- 
创建动画，
@param _path(string) 所在路径
@param _fileName(string) json资源的名字（不包括后缀名)
@param _fileCount(int)  json需要的plist资源数量  可选(当需要多plist时,必须填写)
@param _isNeedRemove(bool)  是否需要加入清除列表  可选
]]
function ToolKit:createFrameAnimationEx( _path, _fileName, _fileCount, _isNeedRemove )
    local manager = ccs.ArmatureDataManager:getInstance()
    if _fileCount and _fileCount > 1 then
        for i = 0, _fileCount - 1 do
            local png = _path .. _fileName .. i .. ".png"
            local plist = _path .. _fileName .. i .. ".plist"
            local json = _path .. _fileName .. ".ExportJson"
            manager:addArmatureFileInfo(png, plist, json)
        end
    else
        local png = _path .. _fileName .. "0.png"
        local plist = _path .. _fileName .. "0.plist"
        local json = _path .. _fileName .. ".ExportJson"
        manager:addArmatureFileInfo(png, plist, json)
    end

    local armature = ccs.Armature:create(_fileName) 
    return armature
end

--[[-- 
显示或隐藏密码
@param _tf(obj) 输入框
@param _btn(obj) 显示或隐藏按钮
]]
function ToolKit:setPwdshowHide(_tf, _btn) 
    local _str  = _tf:getString()
    if _tf:isPasswordEnabled() == true then
        _tf:setPasswordEnabled(false)
        _btn:setTitleText(STR(16, 1))
    else
        _btn:setTitleText(STR(17, 1))
        _tf:setPasswordEnabled(true)
    end

    _tf:setString(_str)
end


--[[-- 
设置密码输入框的一些属性
@param _tf(obj) 密码输入框
]]
function ToolKit:setPwdTextfield(_tf) 
    if _tf then 
        _tf:setMaxLengthEnabled(true)    
        _tf:setMaxLength(GlobalDefine.USER_PWD_LEN[2]) 

        local _ph =  "8-18位字母、数字组合" --getPublicInfor("pwdTfPlaceHold")

        if _ph then
            _tf:setPlaceHolder(_ph) 
        end       
    else
        print("[ERROR]: 输 入 框 获 取 失 败 !")
    end
end



-- 判断是否是敏感字符  有敏感字符返回true  没有返回false
-- @param _string(string) : 需要检查的字符
-- @reurn (bool) true or false 
function ToolKit:checkIsSensitive(_string)

    if g_nickNameTable == nil  then

        ToolKit:addLoadingDialog(20, "正在检查中，请稍后...")

        local _nameFile = cc.FileUtils:getInstance():getStringFromFile("src/app/hall/config/design/SensitiveWord.txt")
        g_nickNameTable = {}  
        g_nickNameTable = string.split(_nameFile, "\n")

        for k,v in pairs(g_nickNameTable) do
            --print("*********k:" ..v .."************")
            g_nickNameTable[k] = string.lower(string.trim(g_nickNameTable[k]))
            -- print("*********" .. g_nameTable[k] .. "************")
        end

        ToolKit:removeLoadingDialog()  
    end


    for k,v in pairs(g_nickNameTable) do
        --print(k,v)

        if string.find (_string, v) ~= nil  then
            print("find sensitive string: ", v)
            return true
        end

    end

    return false
end

--- 过滤敏感字
function ToolKit:splitSensitiveStr(str)
    if g_nickNameTable == nil  then
        ToolKit:addLoadingDialog(20, "正在检查中，请稍后...")
        local _nameFile = cc.FileUtils:getInstance():getStringFromFile("src/app/hall/config/design/SensitiveWord.txt")
        g_nickNameTable = {}
        g_nickNameTable = string.split(_nameFile, "\n")
        for k,v in pairs(g_nickNameTable) do
            g_nickNameTable[k] = string.lower(string.trim(g_nickNameTable[k]))
        end
        ToolKit:removeLoadingDialog()
    end

    local fixStr = str
    for k,v in pairs(g_nickNameTable) do
        if string.find (fixStr, v) ~= nil  then
            fixStr = ToolKit:reomveSomeOneStr(fixStr, v)
        end
    end
    return fixStr
end

--- 去掉字符串中某段字符
function ToolKit:reomveSomeOneStr(str, remove)
    local len = string.len(remove)
    local lcSubStrTab = {}
    while true do
        local lcPos = string.find(str, remove)
        if not lcPos then
            lcSubStrTab[#lcSubStrTab+1] =  str
            break
        end
        local lcSubStr  = string.sub(str,1,lcPos-1)
        lcSubStrTab[#lcSubStrTab+1] = lcSubStr
        str = string.sub(str,lcPos+len,#str)
    end
    local lcMergeStr =""
    local lci = 1
    while true do
        if lcSubStrTab[lci] then
            lcMergeStr = lcMergeStr .. lcSubStrTab[lci]
            lci = lci + 1
        else
            break
        end
    end
    return lcMergeStr
end

-- 把敏感词替换成*
function ToolKit:replaceSensitiveStr(str)
    if g_nickNameTable == nil  then
        ToolKit:addLoadingDialog(20, "正在检查中，请稍后...")
        local _nameFile = cc.FileUtils:getInstance():getStringFromFile("src/app/hall/config/design/SensitiveWord.txt")
        g_nickNameTable = {}
        g_nickNameTable = string.split(_nameFile, "\n")
        for k,v in pairs(g_nickNameTable) do
            g_nickNameTable[k] = string.lower(string.trim(g_nickNameTable[k]))
        end
        ToolKit:removeLoadingDialog()
    end

    local fixStr = str
    for k,v in pairs(g_nickNameTable) do
        if string.find (fixStr, v) ~= nil  then
            local destinStr = ""
            local length = ToolKit:getUtf8StringCount(str)
            for i=1,length do
                destinStr = destinStr .. "*"
            end
            fixStr = string.gsub(fixStr, v, destinStr)
        end
    end
    return fixStr
end

--拨打客服电话
function ToolKit:showPhoneCall()
    local DlgAlert = require("app.hall.base.ui.MessageBox")
    local dialog = DlgAlert.new()
    local number = getPublicInfor("phonenub")
    dialog:TowSubmitAlert({title = STR(25, 2),leftStr = STR(23, 2),rightStr = STR(24, 2),message = STR(23, 5)..number }, function()
        ToolKit:MakePhoneCall(number) end)
    dialog:showDialog()
end

--- 获取下划线文字按钮
function ToolKit:getLabelBtn(data, callback)
    local layer = UIAdapter:createNode("csb/common/label_btn_model.csb")
    local label = layer:getChildByName("text")
    local line = label:getChildByName("line")
    label:setString(data.str)
    label:setColor(data.color or cc.c3b(242,107.19))
    label:setFontSize(data.size or 30)
    line:setFontSize(data.size or 30)
    line:setString("_")
    local getAbs = function ()
        return math.abs(line:getContentSize().width - label:getContentSize().width)
    end
    local str = line:getString()
    local abs = getAbs()
    line:setString(line:getString().."_")
    local absMin = getAbs()
    while(absMin < abs)
    do
        abs = absMin
        str = line:getString()
        line:setString(line:getString().."_")
        absMin = getAbs()
    end
    line:setString(str)
    if callback then
        UIAdapter:bindingLabelBtn(label, callback)
    end
    return layer
end

--- 去掉字符串两侧的空格
function ToolKit:trimString( str )
    return (string.gsub(str, "^%s*(.-)%s*$", "%1"))
end

--获取本机时间毫秒
function ToolKit:getTimeMs()
    return socket.gettime()
end



-- 缩写数字
-- 缩写规则：
--[[
1、当数量小于万时，显示全部数量
2、当数量大于等于万，小于百万时，显示万位，并显示1位小数，显示格式为“XX.X万”
3、当数量大于等于百万，小于亿时，显示万位，不显示小数
4、当数量大于等于亿时，显示亿位，并显示1位小数
5、小数往下取整
6、如果小数是.0结尾则省略
]]
-- @praram number
-- @return string
function ToolKit:shorterNumber(number)
    local ret = ""
    local originNumber = tonumber(number)
    if type(originNumber) == "number" then
        if originNumber < 10^4 then
            ret = tostring(originNumber)
        elseif originNumber < 10^6 then --大于一万,小于百万,单位取万
            local shortNumber = math.floor(originNumber/10^3)
            ret = string.format("%.1f",shortNumber/10).."万"
            ret = string.gsub(ret, "%.0", "")
        elseif originNumber < 10^8 then --大于百万,小于一亿 单位取万
            local shortNumber = math.floor(originNumber/10^4)
            ret = tostring(shortNumber).."万"
        elseif originNumber >= 10^8 then --大于一亿,单位取亿
            local shortNumber = math.floor(originNumber/10^7)
            ret = string.format("%.1f",shortNumber/10).."亿"
            ret = string.gsub(ret, "%.0", "")
        end
    end
    return ret
end

-- @praram string 要截取的string
-- @praram string 截取的字数
-- @return string 带省略号的缩略string
function ToolKit:shorterString(_str,_len)
    if _str == "" then
        return _str
    end
    
    local srtTab = ToolKit:char2Array(_str)
    local ret = ""
    dump(srtTab,"srtTab")
    if #srtTab > _len then
        for i=1,_len-1 do
            ret = ret .. srtTab[i]
        end
        ret = ret .. "..."
    else
        ret = _str     
    end
    return ret
end


-- 跳转到微信
function ToolKit:openWX()
    if not platform.isAppInstalled("com.tencent.mm") then
        TOAST("没有安装微信")
        return false
    end
    
    if device.platform == "android" or device.platform == "ios" then
        device.openURL("weixin://")
    else
        print("打开了微信")
    end
end
function ToolKit:openZFB()
    if not platform.isAppInstalled("com.eg.android.AlipayGphone") then
        TOAST("没有安装支付宝")
        return false
    end
    
    if device.platform == "android" or device.platform == "ios" then
        device.openURL("alipays://platformapi/startapp?appiId=20000221")
    else
        print("打开了支付宝")
    end
end

--检查是否绑定了微信账号
--返回值   true-绑定了微信   false-没有绑定微信
function ToolKit:checkBindWx()
    if GlobalConf.DEBUG_MODE == true then
        return true
    end

   
        local _data =  fromJson(Player:getBindThirdAccList())

        print("*************判断有没有绑定**************")
        dump(_data)

        if _data and ( _data[1].type == 0  or _data[1].type == 1 or _data[1].type == 2 or _data[1].type == 3 or _data[1].type == 4 )then
            return true
        else
            return false
        end
end


--绑定微信
-- 绑定成功 _callback(_code )
-- 0- 成功 1-当前微信号已绑定其他账号
function ToolKit:bindingWx( _callback )
    g_UserCenterController:bingdingWx(_callback)
end


--使用当前微信号登陆
-- 0-登录成功  1-登录失败
--[[
function ToolKit:loginByWx( _callback )
    g_LoginController:loginByWx( _callback )
end
--]]


--获取微信授权
--result  true-获取授权成功  false-获取失败
function ToolKit:getWechatAuthorizeCode(_callback) 
   
    if not g_coldTime then
        --冷却时间
        g_coldTime = true
        local function resetColdTime()
            if g_coldTime then
                g_coldTime = false
            end
        end

        ToolKit:delayDoSomething(resetColdTime, 5)
        
        package.loaded["app.hall.base.third.WechatLogin"] = nil
        local WechatAuthorize = require("app.hall.base.third.WechatLogin")
        WechatAuthorize.new(_callback)
    else
       -- TOAST("正在申请授权，请勿频繁点击！")
    end
end

--获取最近一次登录类型
--状态：0.无登录账号      1.官方账号密码登录      2.微信登录
function ToolKit:getLoginType()
       
    local _loginType = cc.UserDefault:getInstance():getIntegerForKey("loginType",0)
    print("***********获得登录方式："  .. _loginType .. "*************")
    return _loginType
    
end



--设置下一次的登录类型
-- @param: _value(int) 登录类型 0-无登录账号  1-官方账号密码登录 2-微信登录 3-使用旧平台的信息登录
function ToolKit:setLoginType( _value )

    if _value then   
        cc.UserDefault:getInstance():setIntegerForKey("loginType",tonumber(_value))
        print("***********设置登录方式："  .. _value .. "*************")
    else
        print("[ERROR]: get nil when attempt to set login type !")
    end
end



--判断是否是从旧平台跳过来
-- true-从旧平台跳过来  false-不是
function ToolKit:checkFromOldPlatform() 
    local _loginInfor =  platform.getUserLoginInfor()

    if _loginInfor ~= nil and  string.len(_loginInfor) > 1 then
        return true
    else
        return false
    end
end

--[[比较两个时间，返回相差多少时间，前者减后者]]
-- return table:{year=2014,month=6,day=10,hour=16,min=0,sec=0}
--例子：
-- local n_long_time = os.date(os.time{year=2014,month=6,day=10,hour=16,min=0,sec=0});  
-- local n_short_time = os.date(os.time{year=2013,month=5,day=11,hour=16,min=0,sec=0});
-- local t_time = timediff(n_long_time,n_short_time); 
function timediff(long_time,short_time)  
    local n_short_time,n_long_time,carry,diff = os.date('*t',short_time),os.date('*t',long_time),false,{}  
    local colMax = {60,60,24,os.date('*t',os.time{year=n_short_time.year,month=n_short_time.month+1,day=0}).day,12,0}  
    n_long_time.hour = n_long_time.hour - (n_long_time.isdst and 1 or 0) + (n_short_time.isdst and 1 or 0) -- handle dst  
    for i,v in ipairs({'sec','min','hour','day','month','year'}) do  
        diff[v] = n_long_time[v] - n_short_time[v] + (carry and -1 or 0)  
        carry = diff[v] < 0  
        if carry then  
            diff[v] = diff[v] + colMax[i]  
        end  
    end  
    return diff  
end

SERVER_TIMESTAMP_DIFF = 0
function ToolKit:getServerTimeStamp()
    SERVER_TIMESTAMP_DIFF = SERVER_TIMESTAMP_DIFF or 0
    return os.time()+SERVER_TIMESTAMP_DIFF
end


--查询服务器状态
-- @param _roomID(int): 房间号，如果是大厅则为0
-- 接收消息 ：MSG_CHECK_SERVER_STATUS   
--如果收到的消息体是nil，则表明查询失败，
--如果收到一个table，则自行判断，如下
--{
--  "CurrentStatus": -1,//-1:房间id配置不存在,0:正常,1:维护
--  "StartMaintainingTime": 0,//开始维护时间(时间戳)
--  "EndMaintainingTime": 0//结束维护时间(时间戳)
--}

--[[
function ToolKit:checkServerStatus( _roomID )
    g_LoginController:checkServerStatus( _roomID )
end
--]]



--添加开赛通知
-- @praram _noticeNode ( node )  消息节点
-- @praram _time ( int )  显示时间，以秒记
function ToolKit:addBeginGameNotice(  _noticeNode, _time  )  
    if not  g_RollNoticeController then
        g_RollNoticeController = require("app.hall.horseLamp.control.RNoticeController").new()   
    end

    g_RollNoticeController:addGameNotice( _noticeNode  , _time   )  
end



--获取渠道类型
-- GlobalDefine.ChannelType.qka  官网渠道
-- GlobalDefine.ChannelType.third  第三方渠道

function ToolKit:getChannelType()
    --local channelId = GlobalConf.CHANNEL_ID
    local channel = string.sub(GlobalConf.CHANNEL_ID,4,7)  
    
    if channel == "0001" or GlobalConf.DEBUG_MODE then
        return GlobalDefine.ChannelType.qka
    else
        return GlobalDefine.ChannelType.third
    end
        
end


-- 获取第三方玩家id
function ToolKit:getThirdpartyUserID()
 
    local platformUserId = nil
    
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "getThirdpartyUserID"
        local javaParams = {}
        local javaMethodSig = "()Ljava/lang/String;"
        local ok,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
            platformUserId = ret
        end
        
    elseif device.platform == "ios"  then 
        
    end
      
    return platformUserId
end


--获取第三方accessToken 
function ToolKit:getThirdpartyAccesstoken() 
    local platformUserId = nil

    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "getThirdpartyAccesstoken"
        local javaParams = {}
        local javaMethodSig = "()Ljava/lang/String;"
        local ok,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
            platformUserId = ret
        end

    elseif device.platform == "ios"  then 

    end

    return platformUserId
end


function ToolKit:setLuaInit()

    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "setLuaInit"
        local javaParams = {}
        local javaMethodSig = "()V"
        local ok,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
           
        end

    elseif device.platform == "ios"  then 

    end
end

function ToolKit:getThirdExitType()

    local _exitType = 1

    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "getThirdExitType"
        local javaParams = {}
        local javaMethodSig = "()I"
        local ok,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
            _exitType = ret
        end

    elseif device.platform == "ios"  then 

    end

    return _exitType    
end


function ToolKit:sendNotification(time,title,content)
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "sendNotification"
        local javaParams = {time,title,content}
        local javaMethodSig = "(L;Ljava/lang/String;Ljava/lang/String;)Z"
        local ok,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
            return ret
        end
    end
end

--检查是否转正的提示框
function ToolKit:toRegularAccount(dlg,funcId,isAutoShown)
--[[    local params = {
        title = "温馨提示",
        message = "你的账号还没转正，无法使用此功能，是否前往账号转正?" , 
        leftStr = "确定",
        rightStr = "取消",
    }
    local callback = function ()
        local _funcId = funcId or GlobalDefine.FUNC_ID.USER_ACC_REGULAR
        local data = fromFunction(_funcId)
        if data then
            local stackLayer = data.mobileFunction

            if stackLayer then
                sendMsg(MSG_GOTO_STACK_LAYER, {layer = stackLayer, name = data.name, direction = DIRECTION.HORIZONTAL})
            else
                TOAST("尚未发布, 敬请期待")
            end
        end
    end
    dlg = dlg or require("app.hall.base.ui.MessageBox").new()
    dlg:TowSubmitAlert(params, callback)
    if isAutoShown == nil or isAutoShown == true then
        dlg:showDialog()
    end
    return dlg--]]
end
--检查是否绑定手机的提示框
function ToolKit:toPhoneBunding(dlg,funcId,isAutoShown)
    local params = {
        title = "温馨提示",
        message = "你的账号还没绑定手机，无法使用此功能，是否前往手机绑定?", -- todo: 换行
        leftStr = "确定",
        rightStr = "取消",
    }
    local callback = function ()
        local _funcId = funcId or GlobalDefine.FUNC_ID.USER_PHONE_PWD
        local data = fromFunction(_funcId)
        if data then
            local stackLayer = data.mobileFunction

            if stackLayer then
                sendMsg(MSG_GOTO_STACK_LAYER, {layer = stackLayer, name = data.name, direction = DIRECTION.HORIZONTAL, funcId = _funcid})

            else
                TOAST("尚未发布, 敬请期待")
            end
        end
    end
    dlg = dlg or require("app.hall.base.ui.MessageBox").new()
    dlg:TowSubmitAlert(params, callback)
    if isAutoShown == nil or isAutoShown == true then
        dlg:showDialog()
    end
    return dlg
end


--[[
参数：
{
"platformType": --1,订单来源  1：平台，2：U3D
"orderNO": "1008610086",--订单号由游戏服务器生成
"userID": 302,用户ID
"account": "000000000282",--用户账号
"channelNO": "1020001001",--渠道号
"payType": 9,--支付类型策划提供http://192.168.0.199:443/svn/产品部管理/产品1部/内部/策划文档/策划配置表/充值配置表.xlsx  取 充值ID作为参数
"payAmount": 3,充值金额单位：元
"itemID": 10000,--道具ID
"itemCount": 30000,--道具数量
"subject": "30000金币",--商品名称，可空
"body": "30000金币"--商品详情，可空
}
]]


--第三方充值接口
-- @praram _userID     ( string )  玩家id
-- @praram _account    ( string )  玩家账号名
-- @praram _orderNO    ( string )  订单号
-- @praram _payAmount  ( string )  充值金额 （单位  元）
-- @praram _itemCount  ( string )  商品数量
-- @praram _itemID     ( string )  商品id
-- @praram _subject    ( string )  商品名称
-- @praram _body       ( string )  商品详情
-- @praram _extInfor   ( string )  附加信息
function ToolKit:thirdPartyRechaege(_userID,_account,_orderNO,_payAmount,_itemCount,_itemID , _subject,_body,_extInfor)

    print("****************启动第三方充值****************") 
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "reqPay"
        local javaParams = {_userID,_account,_orderNO,_payAmount,_itemCount,_itemID ,_subject,_body,_extInfor}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V"
        local ok,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
           
        end

    elseif device.platform == "ios"  then 

    end


end


--发送玩家信息到渠道方
-- @praram _id        ( string )  玩家id
-- @praram _account   ( string )  玩家账号名
-- @praram _vip       ( string )  玩家vip等级
-- @praram _level     ( string )  玩家账号等级
function ToolKit:sendUserInforToSdk(_id, _account, _viplevel, _level)
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "sendPlyerInforToSdk"
        local javaParams = {_id,_account,_viplevel,_level}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V"
        local ok,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then

        end

    elseif device.platform == "ios"  then 

    end
end

-- 获取第三方充值需要的额外信息（发送给网站）
-- @praram _itemId         ( string )  商品id，（金币或者房卡）
-- @praram _price          ( string )  商品价格，单位 元
-- @praram _goodsName      ( string )  商品名称
function ToolKit:getThirdRechargeExtInfor(_itemId, _price, _goodsName )

    if device.platform == "android" then

    local rechInfor =   require("app.hall.base.third.OtherPayAssist").new(_itemId, _price, _goodsName)
    
        local infor = rechInfor:getExtPayInfor()
        return infor
        
    elseif device.platform == "ios"  then 

    end
   
   return ""
    
end


--将user id 发给android层
function g_GetUserId(...)
    local _userId =  Player:getAccountID()


    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "setGameId"
        local javaParams = {_userId}
        local javaMethodSig = "(Ljava/lang/String;)V"
        local ok,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then

        end

    elseif device.platform == "ios"  then 

    end

end


--从网站获取登录信息时传递的sdk类型
function ToolKit:getSdkType()
    
    if GlobalConf.SDK_TYPE then
        return GlobalConf.SDK_TYPE
	end 
end




--获取第三方登录类型
-- 包括第三方登录类型和官网的微信登录
function ToolKit:getThirdLoginType()
   
    if GlobalConf.LOGIN_TYPE then
        return GlobalConf.LOGIN_TYPE
    else
        if device.platform == "android" then

            return 1

        elseif device.platform == "ios"  then

            return 0

        end
    end
           
    print("[ERROR]: 获取第三方登录类型失败!")
    
end

--获取第三方登录的扩展字段

function ToolKit:getThirdLoginExt() 

    local _ret = toJson("")
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "getThirdLoginExt"
        local javaParams = {}
        local javaMethodSig = "()Ljava/lang/String;"
        local ok,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then      
            _ret = ret
        end

    elseif device.platform == "ios"  then 
    end
    
    return _ret   
end

--检查是否是官网微信登录包
-- false  不是
-- true  是
function ToolKit:checkOfficialWxLogin()  

    local porId = string.sub(GlobalConf.CHANNEL_ID,8,10)  
    if tonumber(porId) >= 401 then
        return true
    end
    
    return false

end

--根据一个节点的节点id查找它的父节点id
function ToolKit:getParentFuncid(_id)

    if not _id or type(_id) ~= "number"  then
        print("ERROR: 传  入  的  节  点  id  无  效  ！")
        return nil
    end

    local data = fromFunction(_id)
    if not data then
        print("********读取节点信息失败*********")
        return nil
    end

    return data.father

end

--获取一个功能节点的根节点
function ToolKit:getRootFuncid(_id)
    if not _id or type(_id) ~= "number"  then
        print("ERROR: 传  入  的  节  点  id  无  效  ！")
    end

    local _parentid = ToolKit:getParentFuncid(_id)

    if _parentid == 0 then
        return _id
    elseif _parentid == nil   then
        print("********获取父节点信息失败*********")
        return nil
    else
        return  ToolKit:getRootFuncid(_parentid)
    end

end

--获取房卡的赠送比率
--返回购买的数量和赠送的数量
function ToolKit:getRoomCardGiveRate()

    local _Jvalue =    getPublicInfor("rebateRoomCard")   
    
    local _tab =   fromJson(_Jvalue)
    
    if _tab then
        return  _tab.roomCard, _tab.rebate
    end
end


--检查是不是代币房包
--是代币房返回 1
--不是代币房返回 0 
function ToolKit:checkRoomType()

    local _Jvalue =    getPublicInfor("mjCoinChannelNum")

    local _tab =   fromJson(_Jvalue)

    if _tab then
        for key, var in pairs(_tab) do
            if GlobalConf.CHANNEL_ID == tostring(var)  then
                return 1
            end
        end
    end

    return 0
end

--截图
function ToolKit:captureScreen(callback, path)
    local glView = cc.Director:getInstance():getOpenGLView()
    local frameSize = glView:getFrameSize()
    local tempWidth = frameSize.width
    local tempHeight = frameSize.height
    local scaleX = glView:getScaleX()
    local scaleY = glView:getScaleY()
    glView:setFrameSize(display.width*scaleX,display.height*scaleY)
    cc.utils:captureScreen(callback, path)
    local scheduler = require("framework.scheduler")
    local function resize()
        glView:setFrameSize(tempWidth,tempHeight)
    end
    scheduler.performWithDelayGlobal(resize, 0.0)
end

function ToolKit:captureScreenEx(callback, path)
    
    local size = cc.Director:getInstance():getWinSize()
    --定义一个屏幕大小的渲染纹理
    local renderTexture = cc.RenderTexture:create(size.width,size.height, cc.TEXTURE2_D_PIXEL_FORMAT_RGB_A8888,gl.DEPTH24_STENCIL8_OES)
    
    renderTexture:beginWithClear(0.0,0.0,0.0,0.0)
    renderTexture:ignoreAnchorPointForPosition(true)
    renderTexture:setAnchorPoint(cc.p(0,0))
    --获得当前的场景指针
    local scene = cc.Director:getInstance():getRunningScene()
    scene:visit()
    renderTexture:endToLua()
    renderTexture:saveToFile(path, cc.IMAGE_FORMAT_JPEG)
    
    local fullPath = cc.FileUtils:getInstance():getWritablePath()..path
    local scheduler = require("framework.scheduler")
    local function saveback()
        callback(true,fullPath)
    end
    scheduler.performWithDelayGlobal(saveback,0.3)
end


--关闭游戏，包括退出提示框
function ToolKit:ShowExitGame()
	ToolKit:QkaExitGame() 
end


--官网退出
function ToolKit:QkaExitGame()
    local _leftStr = ""
    local _leftCallback = nil 

    if Player:getIsRegularAccount() == 0  then

        -- if getFuncOpenStatus(GlobalDefine.FUNC_ID.USER_ACC_REGULAR) == 0 then
        --     _leftStr = STR(65, 1)
        --     _leftCallback = function ()
        --         local data = fromFunction(GlobalDefine.FUNC_ID.USER_ACC_REGULAR)
        --         if data then
        --             local stackLayer = data.mobileFunction
        --             if stackLayer then
        --                 sendMsg(MSG_GOTO_STACK_LAYER, {layer = stackLayer, name = data.name, direction = DIRECTION.HORIZONTAL})
        --             else
        --                 TOAST(STR(62, 1))
        --             end
        --         end
        --     end
        -- else
            _leftStr = "取消"
            _leftCallback = function ()                   
            end
        --end
    else
        if  getFuncOpenStatus(GlobalDefine.FUNC_ID.USER_SWITCH_ACC) == 0 then
            _leftStr = STR(75, 1)
            _leftCallback = function ()
                -- 切换账号
                local data = fromFunction(GlobalDefine.FUNC_ID.USER_SWITCH_ACC)
                if data then
                    local stackLayer = data.mobileFunction
                    if stackLayer then
                        sendMsg(MSG_GOTO_STACK_LAYER, {layer = "src.app.hall.userinfo.view.UserAccountLoginLayer",  dataTable = { name = "切换账号" } })
                    else
                        TOAST(STR(62, 1))
                    end
                end
            end
        else
            _leftStr = "取消"
            _leftCallback = function ()
            end
        end
    end

    local params = {
        title = "提示",
        message = STR(56, 4), -- todo: 换行
        leftStr = _leftStr ,
        rightStr = STR(76, 1),
    }

    local _rightcallback = function ()    
        TotalController:onExitApp()
    end

    local dlg = require("app.hall.base.ui.MessageBox").new()
    dlg:TowSubmitAlert(params, _leftCallback, _rightcallback)
    dlg:showDialog()
end



--第三方退出
function ToolKit:ThirdpartyExitGame()
    if ToolKit:getThirdExitType() == GlobalDefine.ThirdExitType.qka then
        --使用官网的弹出框

        local params = {
            title = "提示",
            message = "您要退出游戏吗？", -- todo: 换行
            leftStr = "取消" ,
            rightStr = "退出",

        }

        local _leftCallback = function ()       
        end

        local _rightcallback = function ()

            ToolKit:reqThirdExitGame()            
        end

        local dlg = require("app.hall.base.ui.MessageBox").new()
        dlg:TowSubmitAlert(params, _leftCallback, _rightcallback)
        dlg:showDialog()

    elseif ToolKit:getThirdExitType() == GlobalDefine.ThirdExitType.third then

        -- 使用sdk的弹出框
        ToolKit:reqThirdExitGame()

    end
end

function ToolKit:reqThirdExitGame()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "ExitGame" 
        local javaParams = nil
        local javaMethodSig = nil
        local m_callback = function() ToolKit:exitGame() end
        javaParams = {
            m_callback,
        }
        javaMethodSig = "(I)V"
        luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
    elseif device.platform == "ios" then
    --        local ocClassName = "AppController"
    --        local className1 = "LuaObjectCBridgeTest"
    --        local m_callback = function() self:exitGame() end
    --        luaoc.callStaticMethod(className1,"registerScriptHandler", {scriptHandler = m_callback } )
    --
    --        local ok, ret = Launcher.luaoc.callStaticMethod(ocClassName, "reqExitGame")
    --        if ok then
    --            paramsStr = ret
    --        end
    end
end


function ToolKit:exitGame()
    print("TotalScene:exitGame()")
    TotalController:onExitApp()
end

--退回到登陆界面
function ToolKit:returnToLoginScene()
    --TotalController:stopToSendHallPing()
    ConnectManager:closeConnect(Protocol.LoginServer) 
    ConnectManager:closeConnect(Protocol.LobbyServer)
    GlobalDefine.ISLOGOUT = true
	g_isAllowReconnect = false
    UIAdapter:pop2RootScene()
    UIAdapter:replaceScene("app.hall.login.view.LoginScene")
end

-- 获取位置信息，经纬度 如果用户选择不允许获取地理信息，则返回经纬度0,0
function ToolKit:getLocation()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "getLocation"
        local javaParams = {}
        local javaMethodSig = "()Ljava/lang/String;"
        local ok,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok and string.len(ret)>3  then
            return json.decode(ret)
        else
            return {longitude=0.0,latitude=0.0}
        end
    elseif device.platform == "ios" then
        local luaoc = require "cocos.cocos2d.luaoc"
            local className = "PhoneInfo"
            local args = nil
            local ok,ret  = luaoc.callStaticMethod(className,"getLocation",args)
            if ok then
                dump(ret)
                print("getLocation ret = ",ret.latitude)
                print("getLocation ret = ",ret.longitude)
                return ret
            else
                return {longitude=0.0,latitude=0.0}
            end
    else
        return {longitude=113.0,latitude=23.0} --给windows调试用
    end
end

-- 获取gps开关，windows固定返回true
function ToolKit:getGpsEnabled()
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "getGpsEnabled"
        local javaParams = {}
        local javaMethodSig = "()Z"
        local ok,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret
        else
            return false
        end
    elseif device.platform == "ios" then
        -- local luaoc = require "cocos.cocos2d.luaoc"
        --     local className = "PhoneInfo"
        --     local args = nil
        --     local ok,ret  = luaoc.callStaticMethod(className,"getLocation",args)
        --     if ok then
        --         dump(ret)
        --         print("getLocation ret = ",ret.latitude)
        --         print("getLocation ret = ",ret.longitude)
        --         return ret
        --     else
        --         return {longitude=0.0,latitude=0.0}
        --     end
    else
        return true --给windows调试用
    end
end

-- 获取当前是否debug版本
-- @params nil
-- @return (bool) debug -> true, release -> false
function ToolKit:isDebug()
    return true
end

--检查一个userdata对象是否存在，我们在将 C++ 对象保存到 Lua 值后。只要还有 Lua 代码在使用这些值，那么即使 C++ 对象已经被删除了，但 Lua 值仍然会存在。如果此时调用 Lua 值的方法就会出错。
-- @praram _obj userdata对象
-- @return bool 存在返回true，不存在返回false
function ToolKit:checkUserData( _obj)

    if _obj == nil  then
        print("错误，传入的对象是空！！！")
        return false
    else
        if tolua.isnull(_obj) then
            return false

        else
            return true
        end
    end
end

-- 复制文本到粘贴板，只支持IOS，安卓
--intentionType 用于区分功能
-- return bool 复制是否成功
function ToolKit:setClipboardText(text)
    return g_ClipboardTextController:setClipboardText(text)
end
-- 获取粘贴板文本，只支持IOS，安卓
-- return callback(str) 粘贴板文本,获取失败就是""空字符串，仅安卓有效
function ToolKit:getClipboardText(callback)
    g_ClipboardTextController:getClipboardText(callback)
end

function ToolKit:captureScreenExSample(callback, path, layerToVisit)
    
    local size = layerToVisit:getContentSize()
    --定义一个输入layer大小的渲染纹理
    local renderTexture = cc.RenderTexture:create(size.width,size.height, cc.TEXTURE2_D_PIXEL_FORMAT_RGB_A8888,gl.DEPTH24_STENCIL8_OES)
    
    renderTexture:beginWithClear(0.0,0.0,0.0,0.0)
    renderTexture:ignoreAnchorPointForPosition(true)
    renderTexture:setAnchorPoint(cc.p(0,0))

    layerToVisit:visit()
    renderTexture:endToLua()
    renderTexture:saveToFile(path, cc.IMAGE_FORMAT_JPEG)
    
    local fullPath = cc.FileUtils:getInstance():getWritablePath()..path
    local scheduler = require("framework.scheduler")
    local function saveback()
        callback(true,fullPath)
    end
    scheduler.performWithDelayGlobal(saveback,0.3)
end

-- 获取sd卡的路径

function ToolKit:getSDcardPath()

    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "getSDcardPath"
        local javaParams = {}
        local javaMethodSig = "()Ljava/lang/String;"
        local ok ,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            return ret

        else
            print("******获取sd卡路径失败，错误码：".. ret .. "********" )
            return ""
        end
    end
end

-- 复制文件到sdcard
function ToolKit:copyFileToSDcard( src, dst )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "copyFile"
        local javaParams = {src, dst}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;)V"
        local ok = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
        end
    end
end

-- 从assets复制到路径
function ToolKit:assetToPath( assetPath ,path )
    if assetPath == nil or #assetPath == 0
        or path == nil or #path == 0 then
        return
    end
    if device.platform == "android" then
        local result,path = luaj.callStaticMethod(
            "org/cocos2dx/lua/MilaiPublicUtil",
            "assetToPath",
            {assetPath,path},
            "(Ljava/lang/String;Ljava/lang/String;)V"
        )
    end
end

-- 判断文件是否存在
function ToolKit:fileIsExists( fileName)
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "fileIsExists"
        local javaParams = {fileName}
        local javaMethodSig = "(Ljava/lang/String;)Z"
        local ok,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
            return ret
        end
    end
end


--保存文件到sd卡
function ToolKit:writeToSDFile( fileName, text )
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "writeToSDFile"
        local javaParams = {fileName,text}
        local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;)V"
        local ok,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok  then
            return ok
        end
    end
end

local function receivePhoneIP(event)
    print("receivePhoneIP",event.name)
    if event.name == "completed" then
        local request = event.request
        if request:getResponseStatusCode() == 200 then
            --dataRecv = "var returnCitySN = {"cip": "61.140.222.161", "cid": "440100", "cname": "广东省广州市"};"
            local dataRecv = request:getResponseData()
            g_PhoneIP = dataRecv 
            print("g_PhoneIP:",g_PhoneIP) 
            if g_aready_login_ip and QkaPhoneInfoUtil then
                local __dataTable = {QkaPhoneInfoUtil:getPhoneIP()}
                ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_UpdateIp_Req", __dataTable)
            end
        end
    elseif event.name == "progress" then
        
    end
end
-- 联网获取手机的IP,保存在Launcher.phoneIP里
local function requestPhoneIP()
    local url = "http://ip.myhostadmin.net/"
    local request = cc.HTTPRequest:createWithUrl(function(event) 
        if receivePhoneIP then
            receivePhoneIP(event)
        end
    end, url,cc.kCCHTTPRequestMethodGET )
    if request then
        request:start()
    end
end
requestPhoneIP()