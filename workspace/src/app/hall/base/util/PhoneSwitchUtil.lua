--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- To change this template use File | Settings | File Templates.
-- 开关管理

local GameSwitchUtil = class("GameSwitchUtil")

local swStr = {
    vib = 1,
    voi = 2
}
local sw_vibrate = "qka_switch_vibrate"
local sw_voice = "qka_switch_voice"

function GameSwitchUtil:ctor()
    self:myInit()
end

function GameSwitchUtil:myInit()
    self.userDefault = cc.UserDefault:getInstance()
    self.switchEnable = {}
--    self.switchEnable[swStr.vib] = GlobalDefine.SWITCHTYPE.OPEN
--    self.switchEnable[swStr.voi] = GlobalDefine.SWITCHTYPE.OPEN
end

function GameSwitchUtil:setVibrateStatus(status)
    if self.switchEnable[swStr.vib] ~= status then
        self.switchEnable[swStr.vib] = status
    end
    self.userDefault:setIntegerForKey(sw_vibrate, status)
    self.userDefault:flush()
end

function GameSwitchUtil:getVibrateStatus()
    if self.switchEnable[swStr.vib] == nil then
        self.switchEnable[swStr.vib] = self.userDefault:getIntegerForKey(sw_vibrate, GlobalDefine.SWITCHTYPE.OPEN)
    end
    return self.switchEnable[swStr.vib]
end

function GameSwitchUtil:setVoiceStatus(status)
    if self.switchEnable[swStr.voi] ~= status then
        self.switchEnable[swStr.voi] = status
    end
    self.userDefault:setIntegerForKey(sw_voice, status)
    self.userDefault:flush()
end

function GameSwitchUtil:getVoiceStatus()
    if self.switchEnable[swStr.voi] == nil then
        self.switchEnable[swStr.voi] = self.userDefault:getIntegerForKey(sw_voice, GlobalDefine.SWITCHTYPE.OPEN)
    end
    return self.switchEnable[swStr.voi]
end


return GameSwitchUtil