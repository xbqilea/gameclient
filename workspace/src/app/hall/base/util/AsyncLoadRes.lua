--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 异步加载资源

local scheduler = require("framework.scheduler")
local CCAsyncData = class("CCAsyncData")


local __ExportJson = "ExportJson"
local __plist = "plist"
local __png = "png"
local __jpg = "jpg"
local __mp3 = "mp3"


local __load = "load"
local __notLoad = "notLoad"

function CCAsyncData:ctor()
	self:myInit()
end

function CCAsyncData:myInit()
	self._fileList = {} -- 提取的资源列表

	self._resourceList = {} -- 资源列表
	
	self._loadIndex = 1 -- 当前加载的下标
	
	self._currentSum = 0 -- 当前加载总数
	
	self.__currentLoadKey = "" -- 当前加载的路径 -- TODO: 可能会被修改, 在update里面
	
	self.loadCompleteCallback = nil -- 加载完成回调

	self.loadingCallback = nil -- 加载过程回调
end

-- 阻塞式加载
function CCAsyncData:blockLoadResources( __resourcesTemp )
	self.__currentLoadKey = __resourcesTemp.TableName
	self._resourceList[self.__currentLoadKey] = __resourcesTemp[self.__currentLoadKey]
	dump(self._resourceList[self.__currentLoadKey])
	for k, v in ipairs(self._resourceList[self.__currentLoadKey]) do
		self:loadBlock(v)
	end
end

-- 是否已经加载过(已加载过就不需要再加载)
-- return (bool) 已加载过就是true, 否则false
function CCAsyncData:isLoadedCompleted()
	return self._loadIndex == self._currentSum
end

-- 预加载资源
-- @params __resourcesTemp (table) 资源temp
-- @params __completeCallback (function) 完成回调
-- @params __loadingCallback (function) 过程回调
function CCAsyncData:preloadResources( __resourcesTemp, __completeCallback, __loadingCallback )
	local key = __resourcesTemp.TableName
	local resList = __resourcesTemp[key]

	self.loadCompleteCallback = __completeCallback

	self.loadingCallback = __loadingCallback

	self:loadResources(key, resList)
end

-- 释放路径里的资源
-- @params __resourcesTemp (table) 资源temp
function CCAsyncData:removeResources( __resourcesTemp )
	local key = __resourcesTemp.TableName
	if self._resourceList[key] then
		for k, v in pairs(self._resourceList[key]) do
			self:remove(key, v)
		end
		self._resourceList[key] = nil
	end
end

-- 加载资源
function CCAsyncData:loadResources( __key, __resList )
	-- self:sort(__key, __resList)
	self._resourceList[__key] = __resList
	if #self._resourceList[__key] == 0 then
		-- print("There is no resource in directory: ", __key)
		if self.loadCompleteCallback then
			self.loadCompleteCallback()
		end
		return
	end
	self.__currentLoadKey = __key
	self._loadIndex = 0
	self._currentSum = #self._resourceList[__key]

	for i = 1, self._currentSum do
		local res = self._resourceList[self.__currentLoadKey][i]
		self:load(res)
	end

end

function CCAsyncData:loadBlock( __res )
	if self:isWithSuffix(__res, __plist) then -- 加载大图(.plist, .png)
		local pre, suf = ToolKit:getPrefixAndSuffix(__res)
		cc.SpriteFrameCache:getInstance():addSpriteFrames(pre .. ".plist", pre .. ".png")
	elseif self:isWithSuffix(__res, __png) then -- 加载碎图(png)
		cc.Director:getInstance():getTextureCache():addImage(__res)
	elseif self:isWithSuffix(__res, __jpg) then -- 加载碎图(jpg)
		cc.Director:getInstance():getTextureCache():addImage(__res)
	elseif self:isWithSuffix(__res, __mp3) then -- 加载音效(.mp3)
		g_AudioPlayer:preloadEffect(__res)
	elseif self:isWithSuffix(__res, __ExportJson) then -- 加载动画(.ExportJson)
		ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(__res)
	end
end


function CCAsyncData:load( __res )
	if self:isWithSuffix(__res, __plist) then -- 加载大图(.plist, .png)
		local pre, suf = ToolKit:getPrefixAndSuffix(__res)
		cc.Director:getInstance():getTextureCache():addImageAsync(pre .. ".png", function ( __texture )
			cc.SpriteFrameCache:getInstance():addSpriteFrames(__res)
			self:onAsyncData(__res)
		end)
	elseif self:isWithSuffix(__res, __png) then -- 加载碎图(png)
		cc.Director:getInstance():getTextureCache():addImageAsync(__res, function ( __texture )
			self:onAsyncData(__res)
		end)
	elseif self:isWithSuffix(__res, __jpg) then -- 加载碎图(jpg)
		cc.Director:getInstance():getTextureCache():addImageAsync(__res, function ( __texture )
			self:onAsyncData(__res)
		end)
	elseif self:isWithSuffix(__res, __mp3) then -- 加载音效(.mp3)
		g_AudioPlayer:preloadEffect(__res)
		self:onAsyncData( __res )
	elseif self:isWithSuffix(__res, __ExportJson) then -- 加载动画(.ExportJson)
		ccs.ArmatureDataManager:getInstance():addArmatureFileInfoAsync(__res, function ( __texture )
			self:onAsyncData(__res)
		end)
	end
end

function CCAsyncData:remove( __key, __res )
	print("remove: ", __key, __res)
	if self:isWithSuffix(__res, __plist) then -- 清除大图(.plist, .png)
		local pre, suf = ToolKit:getPrefixAndSuffix(__res)
		display.removeSpriteFramesWithFile(pre .. ".plist", pre .. ".png")
		-- cc.SpriteFrameCache:getInstance():removeSpriteFramesFromFile(__res)
	elseif self:isWithSuffix(__res, __png) then -- 清除碎图(png)
		cc.Director:getInstance():getTextureCache():removeTextureForKey(__res)
	elseif self:isWithSuffix(__res, __jpg) then -- 清除碎图(jpg)
		cc.Director:getInstance():getTextureCache():removeTextureForKey(__res)
	elseif self:isWithSuffix(__res, __mp3) then -- 清除音效(.mp3)
		g_AudioPlayer:unloadEffect(__res)
	elseif self:isWithSuffix(__res, __ExportJson) then -- 清除动画(.ExportJson)
		ccs.ArmatureDataManager:getInstance():removeArmatureFileInfo(__res)
	end
end

-- 判断是否以此为后缀
function CCAsyncData:isWithSuffix( __name, __suffix )
	local prefix, suffix = ToolKit:getPrefixAndSuffix(__name)
	return (suffix == __suffix)
end

function CCAsyncData:onDestory()
	self.loadCompleteCallback = nil
	self.loadingCallback = nil 
end

function CCAsyncData:onAsyncData( __res )
	self._loadIndex = self._loadIndex + 1
	if self._loadIndex == self._currentSum then
		if self.loadCompleteCallback then
			self.loadCompleteCallback()
		end		
	else
		if self.loadingCallback then
			self.loadingCallback(self._currentSum, self._loadIndex, __res)
		end
	end
end


return CCAsyncData