--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 定点更新实例

local PointUpdateUtil = class("PointUpdateUtil")

function PointUpdateUtil:ctor()
	self:myInit()
	-- self:read()
end

function PointUpdateUtil:myInit()

	self.keyName = "PointUpdateUtil"

	self.data = {}

	-- self.ip = ""
	
	self.imei = ""
	self.uid = ""
	self.channel = GlobalConf.CHANNEL_ID
	self.accountid = ""

	self.totalv = 0
	self.channelv = 0
	self.imeiv = 0
	self.uidv = 0
end

-- 设置ip
-- @param __ip(string): ip地址
-- function PointUpdateUtil:setIp( __ip )
-- 	self.ip = __ip
-- end

-- 设置总版本号
-- @params _version(string): 总版本号
function PointUpdateUtil:setTotalV( _version )
	self.totalv = _version
end
function PointUpdateUtil:getTotalV()
	return self.totalv
end

-- 设置渠道号版本
-- @params __channel(string): 渠道号
function PointUpdateUtil:setChannel( __channel )
	self.channel = __channel
end

-- 设置渠道号版版本
-- @params __channelv(number): 渠道号版本
function PointUpdateUtil:setChannelV( __channelv )
	self.channelv = __channelv
end

-- 设置设备号
-- @params __imei(string): 设备号
function PointUpdateUtil:setImei( __imei )
	self.imei = __imei
end

-- 设置设备号版本
-- @params __version(number): 渠道号版本
function PointUpdateUtil:setImeiV( __version )
	self.imeiv = __version
end

-- 设置uid
-- @params __imei(string): 用户id
function PointUpdateUtil:setUid( __uid )
	self.uid = __uid
end

-- 设置UID版本
-- @params __version(number): 渠道号版本
function PointUpdateUtil:setUidV( __version )
	self.uidv = __version
end


-- 设置数据
-- @param __data(table): 数据
function PointUpdateUtil:setData( __data )
	self.data = __data
end
-- function PointUpdateUtil:getData()
-- 	return self.data
-- end

-- 获取数据
-- @return self.data
function PointUpdateUtil:getData()
	self.data = self:read()

	if (self.data.useridversion == nil or self.data.useridversion == "") and Player and Player.getAccountID then
		self.data["useridversion"] = Player:getAccountID() .. ":1"
	end
	print("totalversion", self.data.totalversion)
	print("channelversion", self.data.channelversion)
	print("ipversion", self.data.ipversion)
	print("setversion", self.data.setversion)
	print("useridversion", self.data.useridversion)
	return self.data
end

-- 写入本地数据
function PointUpdateUtil:write()
	self.data = {
		["totalversion"] = self.totalv,
        ["channelversion"] = self.channel .. ":" .. self.channelv,
        ["setversion"] = self.imei .. ":" .. self.imeiv,
	}

	if Player and Player.getAccountID and self.uidv > 0 then
		data["useridversion"] = Player:getAccountID() .. ":" .. self.uidv
	end

	cc.UserDefault:getInstance():setStringForKey(self.keyName, require("cjson").encode(self.data))
end

-- 读取本地数据
-- @return 本地数据
function PointUpdateUtil:read()
	self.data = cc.UserDefault:getInstance():getStringForKey(self.keyName, require("cjson").encode(self:getDefaultDate()))
	print("self.data: ", self.data)
	return require("cjson").decode(self.data)
end

-- 获取默认数据
-- @return (table)默认数据(所有版本都是1)
function PointUpdateUtil:getDefaultDate()
	local data = {}
	local imei = self.imei or QkaPhoneInfoUtil:getIMEI()

	local data = {
		["totalversion"] = 0,
        ["channelversion"] = "{" .. self.channel .. ":1}",
        ["setversion"] = "{" .. self.imei .. ":1}",
	}	
	if Player and Player.getAccountID then
		data["useridversion"] = "{" .. Player:getAccountID() .. "	:1}"
	end

	return data
end

return PointUpdateUtil