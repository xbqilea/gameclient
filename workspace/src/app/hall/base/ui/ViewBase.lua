--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- Layer基类, 所有显示的界面的基类


local ViewBase = class("ViewBase", function ()
    return cc.Layer:create()
end)

function ViewBase:ctor()
    self._backBtnEnable = true
end

function ViewBase:setBackBtnEnable(_value)
	self._backBtnEnable = _value
end

function ViewBase:getBackBtnEnable()
	return self._backBtnEnable
end

return ViewBase