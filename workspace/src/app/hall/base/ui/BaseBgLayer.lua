--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 背景基类，包括一张背景图，一个按钮，通过setContentView设置页面内容
-- 福袋背景，功能背景都可以

local StackLayer = require("app.hall.base.ui.StackLayer")

local BaseBgLayer = class("BaseBgLayer", function ()
    return StackLayer.new()
end)

function BaseBgLayer:ctor()
    self:myInit()

    self:setupViews()
end

-- 初始化成员变量
function BaseBgLayer:myInit()
	-- ToolKit:addSearchPath( "src/app/game/common/res" )
end

-- 初始化界面
function BaseBgLayer:setupViews()
	local root = nil
	if self:getDirection() == DIRECTION.VERTICAL then
	    root = UIAdapter:createNode("csb/vertical/base_bg_layer.csb")
	    self:addChild(root)
	else
		root = UIAdapter:createNode("csb/horizontal/base_bg_layer.csb")
	    self:addChild(root)
	end

    UIAdapter:adapter(root, handler(self, self.onBaseTouchCallback))

    -- 内容层
    self.layoutContent = root:getChildByName("layout_content")

    -- 按钮
    self.btn = root:getChildByName("btn")

    -- 背景层
    self.layoutBg = root:getChildByName("layout_bg")
end


-- 设置内容
-- layer 大小为 720*1170
function BaseBgLayer:setContentView( layer, center )
    if layer then
        self.layoutContent:addChild(layer)
        if center then
        	local size = self.layoutContent:getContentSize()
        	layer:setPosition(size.width / 2, size.height / 2)
        	layer:setAnchorPoint(0.5, 0.5)
        end
    end
end

-- 设置按钮文本
function BaseBgLayer:setButtonText( str )
	if self:getDirection() == DIRECTION.VERTICAL then
		self.btn:setTitleText(str)
	else
		local tStr = ToolKit:char2Array( str )
		local hStr = tStr[1]
		for i = 2, #tStr do
			hStr = hStr .. "\n" .. tStr[i]
		end
		self.btn:setTitleText(hStr)
	end
end

-- 点击事件回调
function BaseBgLayer:onBaseTouchCallback( sender )
	local name = sender:getName()
    
	if name == "btn" then
		sendMsg(MSG_ENTER_SOME_LAYER, { name = "back" } )
	end
end

-- 设置背景
function BaseBgLayer:setBG( imageName )
	-- body
	if imageName then
		self.layoutBg:setBackGroundImage(imageName)
		self.layoutBg:setBackGroundImageCapInsets(cc.rect(0, 0, 0, 0))
	end
end

function BaseBgLayer:onExit()
	--清理msg
end

return BaseBgLayer
