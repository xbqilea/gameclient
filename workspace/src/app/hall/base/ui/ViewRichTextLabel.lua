--
-- Author: lhj
-- Date: 2017-7-10 
-- 对RichText的封装,加入正文[color=#FFFFFF]等等的颜色支持
--

local RichLabel = class("RichLabel", function()
	return display.newLayer()
end)

local DefaultColor = cc.c3b(255, 255, 255)

function RichLabel:ctor(_text,_width,_height,_font,_fontSize,_defaultColor,_lineNum)
	self.text = _text or ""
	local sum,en = ToolKit:getUtf8StringCount(self.text)
	self.fontSize = _fontSize or 30
	self.width = _width or display.width + en * self.fontSize/2	--对于中英文混排自动换行bug的尺寸修正
	self.height = _height or 1
	self:setTouchEnabled(false)
	self.font = _font or "ttf/jcy.TTF"
	self.defaultColor = _defaultColor or DefaultColor
	self.lineNum = _lineNum or 1
	self.maxI = 0

	self:setContentSize(self.width,self.height)
	self.richText = ccui.RichText:create():addTo(self)
	self.richText:ignoreContentAdaptWithSize(false)
	self.richText:setContentSize(self.width, self.height)
	self:myInit()
end

function RichLabel:myInit()
	local trimText = string.gsub(self.text, "%[.-%]", "");
	local total,en,cn = ToolKit:getUtf8StringCount(trimText)
	self.textWidth = self.fontSize* (en*0.6 + cn)

	self.textTable = self:_transformText(self.text)
	-- dump(self.textTable)
	self:_showTextWithTable(self.textTable)
end

function RichLabel:_transformText(text)
	local result = {}
	local firstTrans = string.split(text, "[color=#")


	-- dump(firstTrans, "firstTrans:", 10)

	local secondTrans = {}
	if #firstTrans>0 then
		for firstI,firstV in ipairs(firstTrans) do
			local temp = string.split(firstV,"[/color]")
			for tempI,tempV in ipairs(temp) do
				table.insert(secondTrans, tempV)
			end
		end
		-- dump(secondTrans, "secondTrans:::::", 10)
		for secondI,secondV in ipairs(secondTrans) do
			local temp = string.split(secondV,"]")
			local color = self.defaultColor
			local text = ""
			if #temp ==2 then
                color = self:_transformColor(temp[1])
                text = temp[2]
            else
                text = temp[1]
            end
            table.insert(result,{["color"]=color,["text"]=text})
        end
	end

	return result
end

function RichLabel:_transformColor(text)
	if string.len(text) < 6 then
		return self.defaultColor
	else
		local color1Text = string.sub(text, -6,-5)
		local color1 = tonumber("0x"..color1Text)

		local color2Text = string.sub(text, -4,-3)
		local color2 = tonumber("0x"..color2Text)

		local color3Text = string.sub(text, -2,-1)
		local color3 = tonumber("0x"..color3Text)
		return cc.c3b(color1, color2, color3)
	end
end

function RichLabel:_showTextWithTable(textTable)
	if self.maxI>0 then
		for i=1,self.maxI do
			self.richText:removeElement(i)
		end
	end
	for i,v in ipairs(textTable) do

		local text = ""
		for i=1, self.lineNum-1 do
			text = text.."\n"
		end
		text = text..v.text
		local textLabel = ccui.RichElementText:create(i, v.color, 255, text, self.font, self.fontSize)

		self.richText:pushBackElement(textLabel)
		self.maxI = i
	end
end

function RichLabel:setText(_text)
	self.text = _text
end

function RichLabel:setFontSize(_size)
	self.fontSize = _size
end

function RichLabel:getTextWidth()
	return self.textWidth
end

function RichLabel.setRichTextString(richText, _str, _fontSize, _font)
	if (richText) then
		local elements = RichLabel.getRichElements(_str, _fontSize, _font)
		for index = 1, #elements do
			richText:pushBackElement(elements[index])
		end
	end
end

function RichLabel.getRichElements(_str, _fontSize, _font)
	local texts = RichLabel.getTransformText(_str)
	local font = _font or "ttf/jcy.TTF"
	local fontSize = _fontSize or 24

	local elements = {}

	for i,v in ipairs(texts) do
		local element = ccui.RichElementText:create(i, v.color, 255, v.text, font, fontSize)
		table.insert(elements, element)
	end
	return elements
end

function RichLabel.getTransformText(text)
	local result = {}
	local firstTrans = string.split(text, "<color=#")


	-- dump(firstTrans, "firstTrans:", 10)

	local secondTrans = {}
	if #firstTrans>0 then
		for firstI,firstV in ipairs(firstTrans) do
			local temp = string.split(firstV,"</color>")
			for tempI,tempV in ipairs(temp) do
				table.insert(secondTrans, tempV)
			end
		end
		-- dump(secondTrans, "secondTrans:::::", 10)
		for secondI,secondV in ipairs(secondTrans) do
			local temp = string.split(secondV,">")
			local color = DefaultColor
			local text = ""
			if #temp ==2 then
				color = RichLabel.getTransformColor(temp[1])
				text = temp[2]
			else
				text = temp[1]
			end
			table.insert(result,{["color"]=color,["text"]=text})
		end
	end

	return result
end

function RichLabel.getTransformColor(text)
	if string.len(text) < 6 then
		return DefaultColor
	else
		local color1Text = string.sub(text, -6,-5)
		local color1 = tonumber("0x"..color1Text)

		local color2Text = string.sub(text, -4,-3)
		local color2 = tonumber("0x"..color2Text)

		local color3Text = string.sub(text, -2,-1)
		local color3 = tonumber("0x"..color3Text)
		return cc.c3b(color1, color2, color3)
	end
end



return RichLabel