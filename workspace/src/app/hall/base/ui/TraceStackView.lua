--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 运行错误提示框(debug时显示)

local XbDialog = import("app.hall.base.ui.CommonView")

local ErrorDialog = class("ErrorDialog", function()
    return XbDialog.new()
end)


function ErrorDialog:ctor( item )
    self.node = UIAdapter:createNode("csb/common/layer_hunter_error.csb")
    UIAdapter:adapter(self.node)

    self:addChild(self.node, 100)

    self:updateViews(item)
end

function ErrorDialog:updateViews( item )

    self.layoutMain = self.node:getChildByName("layout_main")

    local titleLabel = self.layoutMain:getChildByName("text_title")
    local scrollview = self.layoutMain:getChildByName("scrollview")
    local contentLabel = scrollview:getChildByName("text")

    titleLabel:setString(item.title)
    contentLabel:setString(item.content)
    contentLabel:disableEffect()
    titleLabel:disableEffect()
end

return ErrorDialog