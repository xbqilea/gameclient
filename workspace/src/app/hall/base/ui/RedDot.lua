--
-- Author: 
-- Date: 2018-07-27 11:42:15
--
-- 记录未读或可领邮件数目的红点
local RedDot = class("RedDot",function() return display.newNode() end)

function RedDot:ctor(redDotType)
	self.redDotType = redDotType
	self:myInit()
	addMsgCallBack(self, MSG_RED_DOT, handler(self, self.setCount))
end

function RedDot:myInit()
	self.redDotCount = 0
	local radius = 25
	self.redDot = display.newSolidCircle(radius, {x = 0, y = 0, color = cc.c4f(255/255,75/255,75/255,255/255)})
	self.redDot:addTo(self)
	self.redDotLabel = display.newTTFLabel(
	{
		text = "1",
	    size = 25,
	    color = cc.c3b(255, 255, 255),
	})
	:addTo(self.redDot)
	:align(display.CENTER, 0, 0)
	self:setCount(1)
end

function RedDot:setCount(msg,redDotType,count)
	print("RedDot:setCount(",msg,redDotType,count)
	if redDotType ~= self.redDotType then
		return
	end
	if count == -1 then
		self.redDotCount = self.redDotCount -1
		self:setCount(self.redDotCount)
	elseif count == 0 or count == nil then
		self.redDotCount = 0
		self.redDot:setVisible(false)
	elseif count > 0 then
		self.redDotCount = count
		self.redDot:setVisible(true)
		self.redDotLabel:setString(tostring(self.redDotCount))
	end
end

return RedDot