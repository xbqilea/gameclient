--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 通用栈Layer

local ViewBase = require("app.hall.base.ui.ViewBase")

local StackLayer = class("StackLayer", function ()
    return ViewBase.new()
end)


function StackLayer:ctor( __params )
    self:myInit()
    self.params = __params
end

function StackLayer:myInit()
    self.isStackLayer = true -- 标志通用栈Layer

    self.parentScene = nil -- 所在场景

    self.__currentFuncId = 0 -- 当前功能节点id
    
    self.pushAnimation = STACKANI.NONE -- push动画

    self.exitRemove = false -- onExit 删除

    self.showPreLayer = false -- 显示前一个layer

    self.params = nil -- 参数
end

-- 设置出盏时析构掉
function StackLayer:setExitRemove( __value )
	self.exitRemove = __value
end
function StackLayer:getExitRemove()
	return self.exitRemove
end

-- 设置push动画类型
function StackLayer:setPushAnimation( __animation )
	self.pushAnimation = __animation
end
function StackLayer:getPushAnimation()
	return self.pushAnimation
end

-- 设置所在场景
function StackLayer:setScene( __scene )
    self.parentScene = __scene
end
function StackLayer:getScene()
    return self.parentScene
end

-- 设置获取节点id
function StackLayer:setCurrentFuncId( __id )
    self.__currentFuncId = __id
end
function StackLayer:getCurrentFuncId()
    return self.__currentFuncId
end

function StackLayer:setShowPreLayer( __showPreLayer )
    self.showPreLayer = __showPreLayer
end

function StackLayer:getShowPreLayer()
    return self.showPreLayer
end

-- 获取传递进来的参数
-- @return (table): StackLayer创建时的结构体
function StackLayer:getParams()
    return self.params
end

function StackLayer:getType()
    return "StackLayer"
end


return StackLayer