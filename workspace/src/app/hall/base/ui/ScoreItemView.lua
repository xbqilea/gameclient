--
-- Author: 
-- Date: 2018-07-27 11:42:15
--
-- 金币条

local GoldBar = class("GoldBar",function() return display.newNode() end)

function GoldBar:ctor(params)
	self.params = params
	self:myInit()
	self:setupViews()
end

function GoldBar:myInit()
	addMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS, handler(self, self.refreshViews))
	ToolKit:registDistructor( self, handler(self, self.onDestory))
end

function GoldBar:setupViews()
	self.node = UIAdapter:createNode("csb/bag/gold_bar.csb"):addTo(self)
	UIAdapter:adapter(self.node)
	self.node:getChildByName("gold_txt"):setString(Player:getGoldCoin())

    if getFuncOpenStatus(GlobalDefine.FUNC_ID.HALL_GOLD_BAR) == 1 then
        self:setVisible(false)
    end
end

function GoldBar:refreshViews()
	if not self.node then
        removeMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS)
        return
    end
	self.node:getChildByName("gold_txt"):setString(Player:getGoldCoin())
end

function GoldBar:onDestory()
    removeMsgCallBack(self, MSG_PLAYER_UPDATE_SUCCESS)
end

return GoldBar