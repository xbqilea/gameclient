--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 封装UIpageView

--local UIPageView = import("..framework.cc.ui.UIPageView")

local QkaPageView = class("QkaPageView", function()
	return display.newLayer()
end)

--构造函数
function QkaPageView:ctor(params)
	self.listener_ = nil
	self.pageIndexNode_Space = 50
	self.pageCountOld_ = 0
	--self.pageIndexPosY = {}

	self.viewRect_ = params.viewRect or cc.rect(0, 0, display.width, display.height)
	self.column_ = params.column or 1
	self.row_ = params.row or 1
	self.columnSpace_ = params.columnSpace or 0
	self.rowSpace_ = params.rowSpace or 0
	self.padding_ = params.padding or {left = 0, right = 0, top = 0, bottom = 0}
	self.bCirc = params.bCirc or false
    
    --这里设置layer大小防止挡住，吞噬下层的事件
 	--self:setAnchorPoint(cc.p(0,0))
    self:setContentSize(self.viewRect_.width, self.viewRect_.height)
	--self:setTouchEnabled(true)


 	----------------cc.ui.UIPageView
	self.pageView_ = cc.ui.UIPageView.new(params):addTo(self)
		:onTouch(handler(self, self.touchListener_))
	-- --平板适配
	-- if g_hasXuNiKey then
	-- 	if display.width<display.height then --竖屏
	-- 		self.pageView_:setClippingRegion(cc.rect(self.viewRect_.x, self.viewRect_.y, self.viewRect_.width, self.viewRect_.height + g_xuNiHeight))
	-- 	else --横屏
	-- 		self.pageView_:setClippingRegion(cc.rect(self.viewRect_.x, self.viewRect_.y, self.viewRect_.width + g_xuNiHeight, self.viewRect_.height))
	-- 	end
	-- end

	-- touchOnContent true:当触摸在滚动内容上才有效 false:当触摸在显示区域(viewRect_)就有效
	-- 当内容小于显示区域时，两者就有区别了
    self:setTouchType(params.touchOnContent)
    self.pageView_:setTouchEnabled(true)
    self.pageView_:setTouchSwallowEnabled(false)
	--背景
	self:addBgIf(params)

	--页面指示园点
	self.pageIndexNode_ = display.newNode()
	self:addChild(self.pageIndexNode_)
	self:addPageIndexIf(params)


	self:setTouchEnabled(true)
	UIAdapter:transNode(self.pageView_)
end


--获取cc.ui.UIPageView
function QkaPageView:getSuper()
	return self.pageView_
end

--触摸响应
function QkaPageView:onTouch(listener)
	self.listener_ = listener

	return self
end


--private
function QkaPageView:touchListener_(__event)

	
	if __event.name == "reload" then		
		print("event,reload")
		self.pageCountOld_ = __event.pageCount

		--通过页数渲染页面指示圆点
		self.pageIndexNode_:removeAllChildren()		

		--计算起点位置
		local startx = self.viewRect_.x + ((self.viewRect_.width - ((__event.pageCount-1)*self.pageIndexNode_Space))/2)
		local starty --= self.viewRect_.y + (self.padding_.bottom/2)
		if self.pageIndexPosY == nil then
			starty = self.viewRect_.y + (self.padding_.bottom/2)
		else
			starty = self.pageIndexPosY 
		end

		for i=1,__event.pageCount do
			local sprite
			if self.normalImg_ then
				sprite = display.newSprite(self.normalImg_) 
			else
				sprite = display.newSolidCircle(12, {x = 0, y = 0, color = cc.c4f(11/255,77/255,108/255,255/255)})
				--display.newCircle(10, {fillColor = cc.c4b(11,77,108,255), borderColor = cc.c4b(11,77,108,255), borderWidth = 1})
			end
			sprite:setPosition( startx + (i-1)*self.pageIndexNode_Space , starty )
			self.pageIndexNode_:addChild(sprite,5)

			--设置指示标坐标
			if i == __event.curPageIdx then
				self.selectImg_:setPosition( startx + (i-1)*self.pageIndexNode_Space , starty )
			end
		end
		
	elseif __event.name == "pageChange" then
		
		--计算起点位置
		local startx = self.viewRect_.x + ((self.viewRect_.width - ((self.pageCountOld_-1)*self.pageIndexNode_Space))/2)		
		local starty --= self.viewRect_.y + (self.padding_.bottom/2)
		if self.pageIndexPosY == nil then
			starty = self.viewRect_.y + (self.padding_.bottom/2)
		else
			starty = self.pageIndexPosY 
		end

--循环移动指示点改为直接移动到,不跨屏幕
		-- --移动指示点
		-- if __event.bLeftToRight == true then -- 左出 右进
		-- 	local leftOut = cc.MoveTo:create(0.1, cc.p(-20, starty))
  --           local setPlace = cc.Place:create(cc.p(CONFIG_SCREEN_HEIGHT+20,starty)) 
		-- 	local rightIn = cc.MoveTo:create(0.1, cc.p((startx + (__event.curPageIdx-1)*self.pageIndexNode_Space) , starty))
			
		-- 	local action = cc.Sequence:create(leftOut,setPlace,rightIn)
		-- 	self.selectImg_:runAction(action)

		-- elseif __event.bRightToLeft == true then -- 右出 左进
  --           local rightOut = cc.MoveTo:create(0.1,cc.p(CONFIG_SCREEN_HEIGHT+20,starty))
		-- 	local setPlace = cc.Place:create(cc.p(-20, starty))
		-- 	local leftIn = cc.MoveTo:create(0.1, cc.p(startx + (__event.curPageIdx-1)*self.pageIndexNode_Space , starty))
			
		-- 	local action = cc.Sequence:create(rightOut,setPlace,leftIn)
		-- 	self.selectImg_:runAction(action)
		-- else
			self.selectImg_:moveTo(0.2, startx + (__event.curPageIdx-1)*self.pageIndexNode_Space , starty)
		-- end
		
	end


	-- 客户监听
	if self.listener_ then
		self.listener_(__event)
	end

end


--设置页面指示原点之间的间距
function QkaPageView:setSpace( space )
	self.pageIndexNode_Space = space
end

--设置页面指示原点的坐标
function QkaPageView:setPageIndexPosY( posy )
	self.pageIndexPosY = self.viewRect_.y + posy ;
	--self.selectImg_:setPosition( self.viewRect_.x + self.viewRect_.width/2 , self.viewRect_.y + self.padding_.bottom/2 )
end

--设置是否显示页面指示原点
function QkaPageView:setPageIndexVisible( bVisible )
	self.selectImg_:setVisible(bVisible)
	self.pageIndexNode_:setVisible(bVisible)
end

--------------------------------
-- 设置触摸响应方式
-- true:当触摸在滚动内容上才有效 false:当触摸在显示区域(viewRect_)就有效
-- 内容大于显示区域时，两者无差别
-- 内容小于显示区域时，true:在空白区域触摸无效,false:在空白区域触摸也可滚动内容
-- @function [parent=#QkaPageView] setTouchType
-- @param boolean bTouchOnContent 是否触控到滚动内容上才有效
-- @return QkaPageView#QkaPageView 

-- end --
function QkaPageView:setTouchType(bTouchOnContent)
	self.touchOnContent = bTouchOnContent

	if self.touchOnContent == true then
		self.pageView_:setContentSize(0 , 0)
	else

    	--这里一定要调用setContentSize设置大小，否则当页面元素比较少的时候拖动不了
		self.pageView_:setContentSize(self.viewRect_.width , self.viewRect_.height)
	end

	return self
end


--添加背景
function QkaPageView:addBgIf(params)
	if not params.bg then
		return
	end

	local bg
	if params.bgScale9 then
		bg = display.newScale9Sprite(params.bg, nil, nil, nil, params.capInsets)
	else
		bg = display.newSprite(params.bg)
	end

	bg:size(params.viewRect.width, params.viewRect.height)
		:pos(params.viewRect.x + params.viewRect.width/2,
			params.viewRect.y + params.viewRect.height/2)
		:addTo(self, -1)
		:setTouchEnabled(false)

	return self
end

--添加页面指示原点
function QkaPageView:addPageIndexIf(params)
	if params.normal ~= nil and params.selected ~= nil then			

		self.normalImg_ = params.normal

		self.selectImg_ = display.newSprite(params.selected) 
		self.selectImg_:setAnchorPoint(cc.p(0.5, 0.5))
		self.selectImg_:setPosition( self.viewRect_.x + self.viewRect_.width/2 , self.viewRect_.y + self.padding_.bottom/2 )

		--[[
		--页面指示原点
		self.normalImg_ = display.newSprite(params.normal) 
		self.selectImg_ = display.newSprite(params.selected) 
		]]
	else
		-- self.normalImg_ = display.newCircle(10, {fillColor = cc.c4b(11,77,108,255), borderColor = cc.c4b(11,77,108,255), borderWidth = 1})
		self.selectImg_ = display.newSolidCircle(12, {x = 0, y = 0, color = cc.c4f(5/255,173/255,167/255,255/255)})
		--display.newCircle(10, {fillColor = cc.c4b(5,173,167,255), borderColor = cc.c4b(5,173,167,255), borderWidth = 1})
	end

	if self.selectImg_ ~= nil then
		-- self.pageIndexNode_:addChild(self.normalImg_)
		self:addChild(self.selectImg_,10)

		-- self.normalImg_:setAnchorPoint(cc.p(0.5, 0.5))
		-- self.normalImg_:setPosition( self.viewRect_.x + self.viewRect_.width/2 , self.viewRect_.y + self.padding_.bottom/2 )		
		
		self.selectImg_:setAnchorPoint(cc.p(0.5, 0.5))
		self.selectImg_:setPosition( self.viewRect_.x + self.viewRect_.width/2 , self.viewRect_.y + self.padding_.bottom/2 )
	end

	return self
end

return QkaPageView

