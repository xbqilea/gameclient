--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 场景基类
local StackLayerManager = require("app.hall.base.util.LayerMgr")
local DlgAlert = require("app.hall.base.ui.MessageBox")

--账号验证
local Ver_Reason = {
    none = 0,    --不需验证
    freeze = 1,  --临时冻结
    unnormal = 2 --异常登录
}

--验证类型
local Ver_Type = {
    phone = 1,  --手机验证码
    id = 2,     --身份证验证
    que = 3     --问题验证
}

local SceneBase = class("SceneBase", function()
    return display.newScene("SceneBase")
end)

function SceneBase:ctor()
    self:myInit()
    self:setTouchSwallowEnabled(true)
    self:addBackClick()
end

function SceneBase:myInit()
    self._dialogList = {}
    self._isGameScene = false
    self._backEvent = true
    self._topDialog = nil

    self.sceneMusicPath = "hall/sound/audio-hall.mp3" -- 背景音效地址(默认大厅背景音乐)
    self.sceneSoundPath = "audio/click_audio.mp3" -- 默认点击音效地址

    self._backClickHandler = nil -- 点击返回按钮回调

    self._direction = DIRECTION.HORIZONTAL -- 屏幕方向(默认竖屏)

    self.stackLayerManager = StackLayerManager.new(self)

    ConnectionUtil:setCallback(function ( network_state )
        if self.onConnectUtilCallback then
            self:onConnectUtilCallback(network_state)
        end
    end)
end

-- 音乐开关变化
function SceneBase:onMusicStatusChanged()
--    if g_GameMusicUtil:getMusicStatus() then
--        g_GameMusicUtil:setMusicVolume(1)
--        g_GameMusicUtil:playBGMusic(self:getMusicPath(), true)
--    else
--        g_GameMusicUtil:stopBGMusic()
--    end
end

-- 设置屏幕方向
function SceneBase:setSceneDirection( __direction )
    self._direction = __direction
end

-- 获取屏幕方向
function SceneBase:getSceneDirection()
    return self._direction
end

-- 设置游戏场景标志
function SceneBase:setIsGameScene( __value )
    self._isGameScene = __value
end

-- 获取游戏场景标志
function SceneBase:getIsGameScene()
    return self._isGameScene
end


function SceneBase:setSoundPath( __soundPath )
    self.sceneSoundPath = __soundPath
end
function SceneBase:getSoundPath( ... )
    return self.sceneSoundPath
end

function SceneBase:setMusicPath( __musicPath )
    self.sceneMusicPath = __musicPath
end
function SceneBase:getMusicPath()
    return self.sceneMusicPath
end

function SceneBase:onEnter()
  
end

function SceneBase:onExit()
    
end

function SceneBase:registBackClickHandler( __handler )
    self._backClickHandler = __handler
end

-- 屏蔽
function SceneBase:setBackEventFlag(flag)
    self._backEvent = flag
end

--监听手机返回键
function SceneBase:addBackClick()
    -- 监听手机返回键
    
    if device.platform == "android" or device.platform == "windows" or device.platform == "mac" then
        self:setKeypadEnabled(true)
        self:addNodeEventListener(cc.KEYPAD_EVENT, function (event)             
            if event.key == "back" and self._backEvent == true then
                print("-----------SceneBase:back----------------")
                if #self._dialogList > 0 or #self:getStackLayerManager().stackLayerList>2 then
                    self:onSceneBaseBackButtonClicked()
                else
                    if self._backClickHandler then
                        self._backClickHandler()
                    else
                        print("error: 没有注册返回键方法!!!")
                    end
                end
            end  
        end)
    end
end

-- 返回键
function SceneBase:onSceneBaseBackButtonClicked()
    if not ToolKit:hasLoadingDialog() and g_TransParentDlg == nil then
        if GlobalViewBaseStack[#GlobalViewBaseStack] and GlobalViewBaseStack[#GlobalViewBaseStack].getType then
            if not GlobalViewBaseStack[#GlobalViewBaseStack]:getBackBtnEnable() then
                return
            end
            if GlobalViewBaseStack[#GlobalViewBaseStack]:getType() == "dialog" then
                self:popDialog()
            elseif GlobalViewBaseStack[#GlobalViewBaseStack]:getType() == "StackLayer" then
                if not self:getStackLayerManager():getAnimating() then
                    self:getStackLayerManager():popStackLayer()
                end
            end
        end
    end
end

-- 获取对话框列表
function SceneBase:getDialogs()
    return self._dialogList
end

-- 设置最高对话框
function SceneBase:addTopDialog( dlg )
    self._topDialog = dlg
end

-- 获取最高对话框
function SceneBase:getTopDialog()
    return self._topDialog
end

-- 对话框入栈
function SceneBase:pushDialog( pDlg )
    if self._topDialog and self._dialogList[#self._dialogList] == self._topDialog then
        self._dialogList[#self._dialogList] = pDlg
        self._dialogList[#self._dialogList + 1] = self._topDialog
    else
        self._dialogList[#self._dialogList + 1] = pDlg
    end
end

-- 对话框出栈
function SceneBase:popDialog()
    if #self._dialogList>0 then
        local pDig = table.remove(self._dialogList,#self._dialogList)
        pDig:closeDialog()
    end
end

-- 根据tag获取对话框
function SceneBase:getDialogByTag( _tag )
    for k, v in pairs(self._dialogList) do
        if v:getTag() == _tag then
            return v
        end
    end
end

-- 关闭此场景下所有对话框
function SceneBase:closeAllDialog()
    for i = #self._dialogList, 1, -1 do
        self._dialogList[i]:closeDialog()
    end
end

function SceneBase:getStackLayerManager()
    return self.stackLayerManager
end

function SceneBase:getType()
    return "scene"
end

function SceneBase:getSceneName()
    return "SceneBase"
end

-- 异常登录处理
function SceneBase:accountUnnormalCallback( msgName, __info )
    GlobalVerifyController:updateOpenView(true)
    local func = function(node)
        node:setCloseCallback( function ()
            GlobalVerifyController:updateOpenView(false)
        end )
    end
    if __info.m_nVerifyReason == Ver_Reason.freeze then
        local freeze_dlg = require(GlobalVerifyController:getFreePath()).new(__info.m_strPhone)
        freeze_dlg:showDialog()
        func(freeze_dlg)
    elseif __info.m_nVerifyReason == Ver_Reason.unnormal then
        local Verify_Type = {
            phone = 1,
            id = 3
        }
        if __info.m_nVerifyType == Ver_Type.phone then
            local phone_dlg = require(GlobalVerifyController:getPhonePath()).new({type = Verify_Type.phone, phone = __info.m_strPhone})
            phone_dlg:showDialog()
            func(phone_dlg)
        elseif __info.m_nVerifyType == Ver_Type.id then
            local id_dlg = require(GlobalVerifyController:getPhonePath()).new({type = Verify_Type.id})
            id_dlg:showDialog()
            func(id_dlg)
        else
            local data = {id = __info.m_nQuestID, par = __info.m_strQuestPar, result = __info.m_strAnswerHistory}
            local que_dlg = require(GlobalVerifyController:getQuestionPath()).new(data)
            que_dlg:showDialog()
            func(que_dlg)
        end
    end
end

return SceneBase