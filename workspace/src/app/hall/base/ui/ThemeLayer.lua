-- Author: 
-- Date: 2018-07-27 11:42:15
--自动切换的背景
-- AutoSwitchBgLayer


local ViewBase = require("app.hall.base.ui.ViewBase")

local AutoSwitchBgLayer = class("AutoSwitchBgLayer", function ()
    return ViewBase.new()
end)


local scheduler = require("framework.scheduler")

local lobbyBgImg =
    {
        [1] = "res/ui/dt_bg3.jpg"
        -- [1] = "res/ui/dt_bg.jpg",  -- 早上
        -- [2] = "res/ui/dt_bg02.jpg",     --傍晚
        -- [3] = "res/ui/dt_bg01.jpg",    --晚上
    }



function AutoSwitchBgLayer:ctor()
    self:myInit()
    self:setupViews()
end


function AutoSwitchBgLayer:myInit(_parent)

    ToolKit:registDistructor( self, handler(self, self.onDestory) )
end

function AutoSwitchBgLayer:setupViews()

    self:setupBg()

end


function AutoSwitchBgLayer:onDestory()

    if self._sch_changeLB then
        scheduler.unscheduleGlobal(self._sch_changeLB)
        self._sch_changeLB = nil
    end

    if self._sch_setupBg then
        scheduler.unscheduleGlobal(self._sch_setupBg)
        self._sch_setupBg = nil
    end
end


--设置背景
function AutoSwitchBgLayer:setupBg()

    if self._sch_setupBg then
        scheduler.unscheduleGlobal(self._sch_setupBg)
        self._sch_setupBg = nil
    end
    --修改为单背景，不分日夜
    self:setLobbyBg(1)

    -- local _year    = os.date("%Y", os.time())
    -- local _month   = os.date("%m", os.time())
    -- local _day     = os.date("%d", os.time())
    -- local _hour    = os.date("%H", os.time())
    -- local _minute  = os.date("%M", os.time())
    -- local _second  = os.date("%S", os.time())

    -- --目标时间   5.01   13.01  19.01
    -- local _currentTime = os.time()
    -- local _step1 =  os.time({year=tonumber(_year),  month=tonumber(_month),  day=tonumber(_day), hour= 5, min=1, sec=0})
    -- local _step2 =  os.time({year=tonumber(_year),  month=tonumber(_month),  day=tonumber(_day), hour= 13, min=1, sec=0})
    -- local _step3 =  os.time({year=tonumber(_year),  month=tonumber(_month),  day=tonumber(_day), hour= 19, min=1, sec=0})

    -- if _currentTime <  _step1 then

    --     self:setLobbyBg( 3 )
    --     self._sch_changeLB = scheduler.scheduleGlobal(handler(self, self.onChangeLobbyBg), _step1 - _currentTime)

    -- elseif _currentTime >=  _step1 and _currentTime < _step2 then

    --     self:setLobbyBg( 1 )
    --     self._sch_changeLB = scheduler.scheduleGlobal(handler(self, self.onChangeLobbyBg),  _step2 - _currentTime)

    -- elseif  _currentTime >=  _step2 and _currentTime < _step3 then

    --     self:setLobbyBg( 2 )
    --     self._sch_changeLB = scheduler.scheduleGlobal(handler(self, self.onChangeLobbyBg),  _step3 - _currentTime)

    -- elseif _currentTime >=  _step3  then

    --     self:setLobbyBg( 3 )
    --     self._sch_changeLB = scheduler.scheduleGlobal(handler(self, self.onChangeLobbyBg),  _step1 + 24*3600 - _currentTime )

    -- end
end

--设置背景
function AutoSwitchBgLayer:setLobbyBg( _index )
    if not self.lobbyBg then
        self.lobbyBg = self:getBgByIndex(_index)
        self:addChild(self.lobbyBg)
    end
end


--通过背景序号获得背景
function AutoSwitchBgLayer:getBgByIndex(_index)
    self.currentBgIndex = _index

    local _lobbyBg  =  ccui.ImageView:create(lobbyBgImg[_index])
    _lobbyBg:setScaleX(display.scaleX )
    _lobbyBg:setScaleY(display.scaleY )
    _lobbyBg:setAnchorPoint(0,0)
    _lobbyBg:setPosition(0,0)
    _lobbyBg:setLocalZOrder(-10)
    
    
    -- --添加波纹动画
    -- local _node = cc.CSLoader:createNode("res/csb/lobby_function/layer_wave.csb")
    -- local _action = cc.CSLoader:createTimeline("res/csb/lobby_function/layer_wave.csb")
    -- _node:runAction(_action)
    -- _action:gotoFrameAndPlay(0,true)
    -- _lobbyBg:addChild(_node)    
    -- _node:setPosition(0,0)
    
   
    local _bgHeight =_lobbyBg:getContentSize().height
    local _bgWidth = _lobbyBg:getContentSize().width

    -- if _index == 1  then
    --     --加海鸥和云动画  dt_ani_cloud.png
    --     local _x = display.scaleX
    --     local _y = display.scaleY
    --     local _cloud  = display.newSprite("#dt_ani_cloud.png"):align(display.LEFT_BOTTOM, _bgWidth, _bgHeight *0.17 )
    --     _lobbyBg:addChild(_cloud)
    --     local function _resetPos()
    --         _cloud:setPosition(_bgWidth, _bgHeight*0.17)
    --     end
    --     local _action =  cc.RepeatForever:create( cc.Sequence:create(cc.MoveTo:create(140, cc.p( - _cloud:getContentSize().width, _bgHeight*0.17)  ),cc.CallFunc:create(_resetPos)))
    --     _cloud:runAction(_action)

    --     local _mountain = display.newSprite("ui/dt_img_mountain.png"):align(display.LEFT_BOTTOM, 0 , _bgHeight *0.17 )
    --     _lobbyBg:addChild(_mountain)

    --     local _haiou = self:getHaiOu(_lobbyBg)
    --     _haiou:setScale(0.8)
    --     _lobbyBg:addChild(_haiou)

    -- elseif _index == 2   then
    --     -- 加海鸥 动画
    --     local _haiou = self:getHaiOu(_lobbyBg)
    --     _haiou:setScale(0.8)
    --     _lobbyBg:addChild(_haiou)

    -- elseif _index == 3   then
    --     -- 加流星动画

    --     local armature = ToolKit:createFrameAnimationEx("res/tx/", "liuxing", nil ,nil )
    --     _lobbyBg:addChild(armature, 100)

    --     armature:setPosition(display.height*0.5  , display.width*0.5)
    --     armature:getAnimation():play("liuxing", -1, -1)
    -- end
    return _lobbyBg
end

-- --获得海鸥动画
-- function AutoSwitchBgLayer:getHaiOu(_lobbyBg)

--     local _width = _lobbyBg:getContentSize().width
--     local _height = _lobbyBg:getContentSize().height*0.1

--     local array = {
--         cc.p(0, _height*1),
--         cc.p(_width*0.3, _height*0.2),
--         cc.p(_width*0.7, _height*0.9),
--         cc.p(_width + 100, _height*0.2),
--     }

--     local _haiou = ToolKit:createFrameAnimationEx("res/tx/", "dt_ani_seanew", nil ,nil )
--     _haiou:getAnimation():play("Animation1", -1, -1)

--     local function resetpos()
--         _haiou:setPosition(0  , _height)
--     end

--     local _action3 = cc.Sequence:create(cc.CardinalSplineBy:create(50, array, 0.5),cc.DelayTime:create(5), cc.CallFunc:create(resetpos)  )
--     local _action4 = cc.RepeatForever:create(_action3)

--     _haiou:runAction(_action4)
--     _haiou:setPosition(0 , _height)
--     return _haiou
-- end


-- --变换背景
-- function AutoSwitchBgLayer:onChangeLobbyBg()

--     if self._sch_changeLB then
--         scheduler.unscheduleGlobal(self._sch_changeLB)
--         self._sch_changeLB = nil
--     end



--     local _nextbgIndex = 0
--     if self.currentBgIndex == 3 then
--         _nextbgIndex = 1
--     else
--         _nextbgIndex = self.currentBgIndex + 1
--     end

--     self.currentBgIndex = _nextbgIndex

--     local _nextbg = self:getBgByIndex(_nextbgIndex)
--     self:addChild(_nextbg)

--     _nextbg:setCascadeOpacityEnabled(true)
--     _nextbg:setOpacity(0)

--     local _ac1 =   cc.FadeIn:create(5) --渐渐显现
--     _nextbg:runAction(_ac1)

--     local function callback1()
--         self.lobbyBg:removeFromParent()
--         self.lobbyBg = _nextbg
--     end

--     self.lobbyBg:runAction( cc.Sequence:create(cc.FadeOut:create(5),cc.CallFunc:create(callback1)))

--     --  scheduler.performWithDelayGlobal(handler(self, self.setupBg), 10)

--     self._sch_setupBg = scheduler.scheduleGlobal(handler(self, self.setupBg),  10 )
-- end






return  AutoSwitchBgLayer

