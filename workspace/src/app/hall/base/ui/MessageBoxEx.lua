local MessageBoxEx = class("MessageBoxEx", function ()
    return display.newLayer()
end)

function MessageBoxEx:setTitleText(text)
    self.mTextTitle:setString(text)
end

function MessageBoxEx:setMsgText(text)
    self.mTextMsg:setString(text)
end

function MessageBoxEx:addButton(text, imagePath, buttonSize, fontSize)
    local button = self.mButtonTmp:clone()
    button:setVisible(true)
    self.mPanel:addChild(button)
    self.mButtons[#self.mButtons + 1] = button
    button.index = #self.mButtons
    if text then
        button:setTitleText(text)
    end
    if imagePath then
        button:loadTextures(imagePath, imagePath, imagePath)
    end

    button:ignoreContentAdaptWithSize(false)
    button:setContentSize(buttonSize or self.mButtonTmp:getContentSize())

    if fontSize then
        button:getTitleFontSize(fontSize)
    end

    local panelSize = self.mPanel:getContentSize()
    local w = panelSize.width / (button.index + 1)
    local x = w
    local y = self.mButtonTmp:getPositionY()
    for index = 1, #self.mButtons do
        local item = self.mButtons[index]
        item:setPosition(cc.p(x, y))
        x = x + w
    end

    UIAdapter:registClickCallBack(button, handler(self, self.onButtonClick))
    return button.index
end

function MessageBoxEx:setCloseEnabled(isEnabled)
    self.mIsCloseEnabled = isEnabled
    self.mButtonClose:setVisible(isEnabled)
end

function MessageBoxEx:setButtonCall(callBack)
    self.mCallBack = callBack
end

function MessageBoxEx:setBackOpacity(opacity)
    self.mMaskBack:setOpacity(opacity)
end

function MessageBoxEx:ctor(titleText, text, callBack)
    self.mButtons = {}
    self.mCallBack = callBack
    self.mIsCloseEnabled = true

    local layer = UIAdapter:createNode("hall/MessageBoxExUI.csb")
    self:addChild(layer)
    layer:setContentSize(display.size)
    self.mMaskBack = layer:getChildByName("mask")
    self.mMaskBack:setContentSize(display.size)

    if isPortraitView() then
        self.mMaskBack:setOpacity(0)
    end

    self.mPanel = layer:getChildByName("Panel")
    self.mPanel:setPosition(cc.p(display.width * 0.5, display.height * 0.5))

    self.mTextTitle = self.mPanel:getChildByName("TextTitle")
    self.mTextMsg = self.mPanel:getChildByName("TextMsg")
    self.mButtonClose = self.mPanel:getChildByName("Button_3")
    self.mButtonTmp = self.mPanel:getChildByName("ButtonTmp")
    
    self.mButtonTmp:setVisible(false)
    if titleText then
        self.mTextTitle:setString(titleText)
    end
    self.mTextMsg:setString(text or "")
    self.mMaskBack:addTouchEventListener(handler(self, self.onCloseClick))
    self.mButtonClose:addTouchEventListener(handler(self, self.onCloseClick))
end

function MessageBoxEx:onButtonClick(sender)
    local callBack = self.mCallBack
    if callBack then
        callBack(sender.index, self)
    end
end

function MessageBoxEx:onCloseClick(sender, eventType)
    if eventType == ccui.TouchEventType.ended and self.mIsCloseEnabled then
        local callBack = self.mCallBack
        self:removeFromParent()
        if callBack then
            callBack(0)
        end
    end
end


return MessageBoxEx