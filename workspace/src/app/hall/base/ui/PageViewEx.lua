local PageViewEx = class("PageViewEx", function()
    return display.newLayer()
end)

function PageViewEx:ctor()
    self.m_nSelect = 0
    self.m_nInterval = 0
    self.m_pAllItem = {}
    self.m_pItemList = {}
    self.m_sDotSel = ""
    self.m_sDotUn = ""
    self.m_sPlist = ""
    self.m_nDotType = nil --ccui.TextureResType.localType
    self.m_pDotSize = {width = 32, height = 32}
    self.m_dotOffset = cc.p(0, 0)

    self.m_bIsTouchBegan = false
    self.m_nMoveType = 0
    self.m_nCurrentTime = 0
    self.m_nTime = 0
    self.m_nSpeed = 500

    self.m_pDotNode = display.newNode()
    self:addChild(self.m_pDotNode, 1)
    self.m_pDotNode:setAnchorPoint(cc.p(0.5, 0))

    self.m_pContent = ccui.Layout:create()
    self:addChild(self.m_pContent, 0)
    self.m_pContent:setAnchorPoint(cc.p(0, 0))
    self.m_pContent:setPosition(cc.p(0,0))
    self.m_pContent:setTouchEnabled(false)
    self.m_pContent:setClippingEnabled(true)
	self.m_pContent:setBackGroundColorOpacity(0)

    local listener = cc.EventListenerTouchOneByOne:create()
    listener:registerScriptHandler(handler(self,self._OnTouchMoved),cc.Handler.EVENT_TOUCH_MOVED)
    listener:registerScriptHandler(handler(self,self._OnTouchBegan),cc.Handler.EVENT_TOUCH_BEGAN)
    listener:registerScriptHandler(handler(self,self._OnTouchEnded),cc.Handler.EVENT_TOUCH_ENDED)
    listener:registerScriptHandler(handler(self,self._OnTouchCancelled),cc.Handler.EVENT_TOUCH_CANCELLED)
    self:getEventDispatcher():addEventListenerWithSceneGraphPriority(listener, self)
    self:setTouchEnabled(true)

    self:addNodeEventListener(cc.NODE_ENTER_FRAME_EVENT,handler(self,self._Update))
    self:scheduleUpdate()
    self:setNodeEventEnabled( true )
end

function PageViewEx:setSize(width, height)
    self:setContentSize(width, height)
    self.m_pContent:setContentSize(self:getContentSize())
    self.m_pContent:setPosition(cc.p(0,0))
end

function PageViewEx:setDot(_sel, _un, _plist, _width, _height)
    self.m_sDotSel = _sel
    self.m_sDotUn = _un
    self.m_sPlist = _plist
    self.m_pDotSize = {width = _width or 32, height = _height or 32}
    if (self.m_sPlist) then
        self.m_nDotType = ccui.TextureResType.plistType
    else
        self.m_nDotType = ccui.TextureResType.localType
    end

    if (self.m_sDotSel == "" or self.m_sDotUn == "") then
        return
    end
    self.m_pDotNode:removeAllChildren()

    for index = 1, #self.m_pAllItem do
        self:_AddDot()
    end
end

function PageViewEx:setDotOffset(_x, _y)
    self.m_dotOffset = cc.p(_x or 0, _y or 0)
    self.m_pDotNode:setPosition(cc.p( self:getContentSize().width / 2 + self.m_dotOffset.x, self.m_dotOffset.y) )
end

function PageViewEx:addItem(item)
    self.m_pContent:addChild(item)
    item:setSwallowTouches(false)
    item:setAnchorPoint(cc.p(0, 0))
    local count = #self.m_pItemList
    if (count ~= 0) then
        local endItem = self.m_pAllItem[count]
        item:setPosition(cc.p(endItem:getPositionX() + endItem:getContentSize().width + self.m_nInterval ,0))
    end
    table.insert(self.m_pItemList, item)
    table.insert(self.m_pAllItem, item)
    self:_AddDot()
    if (#self.m_pAllItem == 1) then
        self:setSelect(1)
    end
end

function PageViewEx:setInterval(interval)
    self.m_nInterval = interval
    self:_BeganToEnd()
    local lastItem = nil
    for index = 1, #self.m_pItemList do
        local item = self.m_pItemList[index]
        if (nil ~= lastItem) then
            item:setPositionX(lastItem:getPositionX() + lastItem:getContentSize().width + self.m_nInterval)
        end
        lastItem = item;
    end
end

function PageViewEx:setSelect(index)
    if (self.m_bIsTouchBegan or self.m_nMoveType ~= 0) then
        return
    end

    if (index < 1 or index > #self.m_pAllItem) then
        return
    end
    self:_BeganToEnd()
    self:_SetSelectDot(index)
    self.m_nSelect = index
    self:_MoveToSelect(true)
end

function PageViewEx:getSelectItem()
    if (#self.m_pAllItem == 0) then
        return nil
    end
    if (self.m_nSelect > 0 and self.m_nSelect <= #self.m_pAllItem) then
        return self.m_pAllItem[self.m_nSelect]
    end
    return nil
end

function PageViewEx:getSelectIndex()
    return self.m_nSelect
end

function PageViewEx:getLastIndex()
    local ret = (self.m_nSelect + #self.m_pAllItem - 2) % #self.m_pAllItem + 1
    return ret
end

function PageViewEx:getNextIndex()
    return self.m_nSelect % #self.m_pAllItem + 1
end

function PageViewEx:_ItemToIndex(item)
    for index = 1, #self.m_pAllItem do
        if (item == self.m_pAllItem[index]) then
            return index
        end
    end
    return 0
end

function PageViewEx:_AddDot()
    if self.m_nDotType == nil then
        return
    end
    local image = ccui.ImageView:create()
    self.m_pDotNode:addChild(image)
    local count = self.m_pDotNode:getChildrenCount()
    if (self.m_nDotType == ccui.TextureResType.plistType) then
        display.loadSpriteFrames(self.m_sPlist..".plist", self.m_sPlist..".png")
    end
    if (self.m_nSelect == count) then
        image:loadTexture(self.m_sDotSel, self.m_nDotType)
    else
        image:loadTexture(self.m_sDotUn, self.m_nDotType)
    end
    image:setPosition(cc.p(count * self.m_pDotSize.width - self.m_pDotSize.width / 2, self.m_pDotSize.height / 2))
    image:setTag(count)
    self.m_pDotNode:setContentSize({width = count * self.m_pDotSize.width, height = self.m_pDotSize.height})
    --self.m_pDotNode:setPosition(cc.p( self:getContentSize().width / 2, 0) )
    self.m_pDotNode:setPosition(cc.p( self:getContentSize().width / 2 + self.m_dotOffset.x, self.m_dotOffset.y) )
    image:setTouchEnabled(true)
    image:addTouchEventListener(handler(self, self._OnDotClick))
end

function PageViewEx:_OnDotClick(touch, eventType)
    if (eventType == ccui.TouchEventType.ended) then
        self:setSelect(touch:getTag())
    end
end

function PageViewEx:_SetSelectDot(index)
    if (self.m_nSelect == index) then
        return
    end
    if (0 == self.m_pDotNode:getChildrenCount()) then
        return
    end
    if (self.m_nDotType == ccui.TextureResType.plistType) then
        display.loadSpriteFrames(self.m_sPlist..".plist", self.m_sPlist..".png")
    end
    if (0 ~= self.m_nSelect and self.m_nSelect ~= index) then
        local item = self.m_pDotNode:getChildByTag(self.m_nSelect)
        item:loadTexture(self.m_sDotUn, self.m_nDotType)
    end
    local item = self.m_pDotNode:getChildByTag(index)
    item:loadTexture(self.m_sDotSel, self.m_nDotType)
end

function PageViewEx:_MoveToSelect(isAction)
    local item = self:getSelectItem()
    self.m_nMoveType = 0
    local viewSize = self:getContentSize()
    local pointX = item:getPositionX() + item:getContentSize().width / 2
    local offsetX = viewSize.width / 2 - pointX
    if (isAction) then
        self.m_nSpeed = (1.0 / 0.2) * offsetX
        if (math.abs( self.m_nSpeed ) < 500) then
            if (self.m_nSpeed > 0) then
                self.m_nSpeed = 500
            else
                self.m_nSpeed = -500
            end
        end

        if (offsetX > 0) then
            self.m_nMoveType = 1
        elseif (offsetX < 0) then
            self.m_nMoveType = 2
        else
            self:_MoveBy(offsetX, 0)
        end
    else
        self:_MoveBy(offsetX, 0)
    end
end

function PageViewEx:_MoveBy(x, y)
    if (#self.m_pItemList == 0 ) then
        return
    end
    local lastItem = nil
    local viewSize = self:getContentSize()
    for index = 1, #self.m_pItemList do
        local item = self.m_pItemList[index]
        if (nil == lastItem) then
            item:setPositionX(x + item:getPositionX())
        else
            item:setPositionX(lastItem:getPositionX() + lastItem:getContentSize().width + self.m_nInterval)
        end
        lastItem = item;
        local fx = item:getPositionX() + item:getContentSize().width / 2
        local offsetX = math.abs( viewSize.width / 2 - fx )
        if (offsetX < viewSize.width / 2) then
            if (1 ~= self.m_nMoveType and 2 ~= self.m_nMoveType and self:getSelectItem() ~= item) then
                local selIndex = self:_ItemToIndex(item)
                self:_SetSelectDot(selIndex)
                self.m_nSelect = selIndex
            end
        end
    end
    if (x < 0) then
        self:_BeganToEnd()
    elseif (x > 0) then
        self:_EndToBegan()
    end
end

function PageViewEx:_BeganToEnd()
    if (#self.m_pItemList < 2) then
        return
    end
    local lastItem = self.m_pItemList[1]
    while (lastItem:getPositionX() + lastItem:getContentSize().width <= 0) do
        local endItem = self.m_pItemList[#self.m_pItemList]
        lastItem:setPositionX(endItem:getPositionX() + endItem:getContentSize().width + self.m_nInterval)
        table.remove(self.m_pItemList, 1)
        table.insert(self.m_pItemList, lastItem)
        lastItem = self.m_pItemList[1]
    end
end

function PageViewEx:_EndToBegan()
    if (#self.m_pItemList < 2) then
        return
    end
    local endItem = self.m_pItemList[#self.m_pItemList]
    while (endItem:getPositionX() >= self:getContentSize().width) do
        table.remove(self.m_pItemList)
        local lastItem = self.m_pItemList[1]
        endItem:setPositionX(lastItem:getPositionX() - endItem:getContentSize().width - self.m_nInterval)
        table.insert(self.m_pItemList, 1, endItem)
        endItem = self.m_pItemList[#self.m_pItemList]

    end
end

function PageViewEx:_OnTouchBegan(touch, eventType)
    local point = self:convertToNodeSpace(touch:getLocation())
    local viewSize = self:getContentSize()
    if ( point.x < 0 or point.y < 0
        or point.x > viewSize.width or point.y > viewSize.height ) then
        return false
    end

    self.m_bIsTouchBegan = true
    self.m_pBeganPoint = point
    self.m_pLastPoint = point
    self.m_nMoveType = 0
    self.m_nStartTime = os.clock()
    return true
end

function PageViewEx:_OnTouchMoved(touch, eventType)
    local point = self:convertToNodeSpace(touch:getLocation())
    local offsetX = point.x - self.m_pLastPoint.x
    self.m_pLastPoint = point
    self:_MoveBy(offsetX, 0)
    -- for index = 1, #self.m_pAllItem do
    --     local item = self.m_pAllItem[index]
    --     item:setTouchEnabled(false)
    -- end
end

function PageViewEx:_OnTouchEnded(touch, eventType)
    self.m_bIsTouchBegan = false
    local endPoint = self:convertToNodeSpace(touch:getLocation())
    self.m_nMoveType = 0
    local minTime = 0.2
    local viewSize = self:getContentSize()
    local clock = os.clock() - self.m_nStartTime
    if (clock < minTime) then
        local distance = endPoint.x - self.m_pBeganPoint.x
        if (math.abs(distance) <= viewSize.width * 0.5) then
            if (endPoint.x > self.m_pBeganPoint.x) then
                self:setSelect(self:getLastIndex())
            elseif (endPoint.x < self.m_pBeganPoint.x) then
                self:setSelect(self:getNextIndex())
            else
                self:_MoveToSelect(true)
            end
        elseif (math.abs(distance) < viewSize.width) then
            self:_MoveToSelect(true)
        else
            self:_MoveToSelect(true)
            -- self.m_nMoveType = 3
            -- self.m_nTime = 0.1 / clock
            -- self.m_nCurrentTime = self.m_nTime
            -- self.m_nSpeed = (distance * 3) * self.m_nTime
        end
    else
        self:_MoveToSelect(true)
    end
    
end

function PageViewEx:_OnTouchCancelled(touch, eventType)

end

function PageViewEx:_Update(dt)
    if (1 == self.m_nMoveType) then
        local offsetX = self.m_nSpeed * dt
        local viewOffsetX = self:getContentSize().width / 2
        local item = self:getSelectItem()
        local pointX = item:getPositionX() + item:getContentSize().width / 2
        if (pointX + offsetX > viewOffsetX) then
            self.m_nMoveType = 0
            self:_MoveBy(viewOffsetX - pointX)
            -- for index = 1, #self.m_pAllItem do
            --     local item = self.m_pAllItem[index]
            --     item:setTouchEnabled(true)
            --     item:setSwallowTouches(false)
            -- end
        else
            self:_MoveBy(offsetX)
        end
    elseif (2 == self.m_nMoveType) then
        local offsetX = self.m_nSpeed * dt
        local viewOffsetX = self:getContentSize().width / 2
        local item = self:getSelectItem()
        local pointX = item:getPositionX() + item:getContentSize().width / 2
        if (pointX + offsetX < viewOffsetX) then
            self.m_nMoveType = 0
            -- for index = 1, #self.m_pAllItem do
            --     local item = self.m_pAllItem[index]
            --     item:setTouchEnabled(true)
            --     item:setSwallowTouches(false)
            -- end
            self:_MoveBy(viewOffsetX - pointX)
        else
            self:_MoveBy(offsetX)
        end
    elseif (3 == self.m_nMoveType) then
        self.m_nCurrentTime = math.max( self.m_nCurrentTime - dt, 0 )
        local pi2 = self.m_nCurrentTime / self.m_nTime * (math.pi / 2.0)
        local speed = (1.0 - math.cos( pi2 )) * self.m_nSpeed
        local offsetX = speed * dt
        if (self.m_nCurrentTime == 0) then
            self:_MoveToSelect(true)
        else
            self:_MoveBy(offsetX, 0)
        end
    end
end


return PageViewEx