--网上找的NetSprite http://blog.csdn.net/dd135373/article/details/46468505
--图片名字为图片网络位置的md5(创建时输入的url)
--Modify by ReST
--[[用法:
	local netSprite = NetSprite.new(url,isForceUpdate,callBack)
	netSprite:setScaleSize(width,height)

	callBack(isDownloadSuccess)
]]
--[[修改的地方有:
--修改图片保存路径在安卓的缓存目录,可以通过安卓的清除缓存清除掉
--安卓图片保存目录在\data\data\(appPackagaName)\cache\netSprite
--PC模拟器的话图片保存目录在C:\Users\Administrator\AppData\Local\QkaGameMobile\netSprite
--修改了检查图片是否已经下载的判断条件,改为检查文件是否存在
--下载如果超时,删除那个创建的图片文件
--增加了本次启动下载过的文件列表g_NetSpriteList,即使强制更新也只会下载一次,而不是每次创建NetSprite都下载
--增加了getScaleSize跟setScaleSize(width,height),用来调整图片下载完之后的大小(会改变scale)
]]
require "lfs"

local NetSprite = class("NetSprite",function()
	return display.newSprite()
end)

g_NetSpriteList = g_NetSpriteList or {}

--url:图片网络地址 isForceUpdate:即使图片存在,是否强制重新下载(每次游戏启动只会下载一次)
function NetSprite:ctor(url,isForceUpdate,callBack, jpg)--创建NetSprite
	if device.platform == "android" then
		self.path = device.writablePath.."../cache/netSprite/" --获取本地缓存目录
	else
		self.path = device.writablePath.."netSprite/" --获取本地存储目录
	end 
	if not io.exists(self.path) then
		lfs.mkdir(self.path) --目录不存在，创建此目录
	end
	self.url  = url
	self.isForceUpdate = isForceUpdate
	self.callBack = callBack

    self.suffix = ".png"
    if jpg then
        self.suffix = ".jpg"
    end
    self.finishCallback = nil -- 下载图片之后的回调  
	if url and type(url) == "string" and string.len(url) then 
		self:createSprite()
	end
end

function NetSprite:getUrlMd5()
	local tempMd5 = crypto.md5(self.url)
	local alreadyDownloadThisTime = false
	local g_NetSpriteList = g_NetSpriteList or {}
	for i,v in ipairs(g_NetSpriteList) do
	   if tempMd5 == v then
	       alreadyDownloadThisTime = true
	       break
	   end
    end  
    if (alreadyDownloadThisTime or not self.isForceUpdate) and cc.FileUtils:getInstance():isFileExist(self.path..tempMd5..self.suffix) then --判断本地保存数据是否存在
      
        return true,self.path..tempMd5..self.suffix --存在，返回本地存储文件完整路径
	else
      
		return false,self.path..tempMd5..self.suffix --不存在，返回将要存储的文件路径备用
	end
end

function NetSprite:setUrlMd5(isOvertime)
	if isOvertime then --如果超时,删掉那个文件
		local picPath = self.path..crypto.md5(self.url)..self.suffix
		if cc.FileUtils:getInstance():isFileExist(picPath) then
			cc.FileUtils:getInstance():removeFile(picPath)
		end
	else   --下载成功,加入到NetSpriteManager里
        table.insert(g_NetSpriteList,crypto.md5(self.url))
	end
	if self.callBack then
		local picPath = self.path..crypto.md5(self.url)..self.suffix
		self.callBack(not isOvertime,picPath, self)
	end
end

function NetSprite:createSprite()
	local isExist,fileName = self:getUrlMd5()

	if isExist then --如果存在，直接更新纹理
		self:updateTexture(fileName) 
		if self.callBack then
			self.callBack(true,fileName, self)
		end

	else --如果不存在，启动http下载
		if device.platform ~= "windows" and network.getInternetConnectionStatus() == cc.kCCNetworkStatusNotReachable then
			print(self.class.__cname,"no net")
			return
		end

		local request = network.createHTTPRequest(function(event)  
			if self and self.onRequestFinished then
                
				self:onRequestFinished(event,fileName)
			end
			end,self.url, "GET")
		request:start()
	end
end

function NetSprite:onRequestFinished(event,fileName)
   
    local ok = (event.name == "completed")
    local request = event.request
    if not ok then
        -- 请求失败，显示错误代码和错误消息  
        print(self.class.__cname,request:getErrorCode(), request:getErrorMessage())
        if (event.name == "failed" or event.name == "unknown") and self.callBack then
        	self.callBack(false,fileName, self)
    	end 
        
        return
    end

    local code = request:getResponseStatusCode()
    if code ~= 200 then
        -- 请求结束，但没有返回 200 响应代码 
        --print(self.class.__cname,code)
		if self.callBack then
        	self.callBack(false,fileName, self)
    	end 
        return
    end 
    -- 请求成功，显示服务端返回的内容
    local response = request:getResponseString()
--    print(self.class.__cname,response)
    
    --保存下载数据到本地文件，如果不成功，重试30次。
    local times = 1 
    while (not request:saveResponseData(fileName)) and times < 30 do
    	times = times + 1
    end
    local isOvertime = (times == 30) --是否超时
    self:setUrlMd5(isOvertime) --保存md5 
    self:updateTexture(fileName) --更新纹理
    
    if self.finishCallback then
        self.finishCallback(fileName)
    end
end

function NetSprite:updateTexture(fileName)
	local sprite = display.newSprite(fileName) --创建下载成功的sprite  
	if not sprite then return end
	local texture = sprite:getTexture()--获取纹理
	local size = texture:getContentSize()
	self:setTexture(texture)--更新自身纹理
	self:setContentSize(size)
	self:setTextureRect(cc.rect(0,0,size.width,size.height))
	
	if self._scaleSize then
		self:setScaleX(self._scaleSize.width/size.width)
		self:setScaleY(self._scaleSize.height/size.height)
	end
end

function NetSprite:setScaleSize(width,height)
	local size = self:getContentSize()
	if size.width >0 then
		self._scaleSize = cc.size(width, height)
		self:setScaleX(width/size.width)
		self:setScaleY(height/size.height)
	else
		self._scaleSize = cc.size(width, height)
	end
end

function NetSprite:getScaleSize()
	return self._scaleSize or self:getContentSize()
end

function NetSprite:setFinishCallback( _callback )
    self.finishCallback = _callback
end

return NetSprite