--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 对话框
-- 增加最高对话框功能，设置后层次是最高层，且后面再弹出对话框也在其后面，按返回键也会首先关闭最高对话框，用法：self:setTopDialog()
local ViewBase = require("app.hall.base.ui.ViewBase")

local scheduler = require("framework.scheduler")

local XbDialog = class("XbDialog", function ()
    return ViewBase.new()
end)

GLOBAL_ZORDER_START = 100000
GLOBAL_DIALOG_TAG = 1
CUR_GLOBAL_ZORDER = CUR_GLOBAL_ZORDER or GLOBAL_ZORDER_START -- 全局的zorder, 用于控制所有对话框的顺序

CUR_TAG_DIALOG = GLOBAL_DIALOG_TAG
local DLG_CAP_TEXTURE_NAME = "DLG_CAP_TEXTURE_NAME"


function XbDialog:ctor()
    self:myInit()
    self.root = UIAdapter:createNode("csb/common/milai_dialog.csb")
    self.root:setName("dialog_root")
    self:addChild(self.root, -1)
    UIAdapter:adapter( self.root, handler(self, self.onTouchCallback) )

    self.dlgLayoutMain = self.root:getChildByName("layout_main")

    self:isShowHorseLamp(false)
    self:createScheduler()
    
end

function XbDialog:myInit()
    self.closeCallBack = nil -- 关闭对话框回调
    self.DIALOGANI = DIALOGANI.NONE -- 动画类型

    self.zorder = 0 
    self.isSetZorder = false
    self.isTopDialog = false
    self:setContentSize(display.size)
    
    self.parentScene = nil
    self.parentStackLayer = nil 

    self.dlgEnableTouch = true -- 开启触摸(点击退出)
    self.isAnimationEnable = true -- 是否激活动画(默认激活)
    self.dlgAnimation = DIALOGANI.ZERO_TO_WHOLE -- 动画类型(默认由小变大)
    self.bgMountMode = DIALOGMOUNT.SHADOW -- 蒙版样式(默认半透明背景)

    self.dialogTag = CUR_TAG_DIALOG

    CUR_TAG_DIALOG = CUR_TAG_DIALOG + 1

    print("create a dialog with tag: ", self.dialogTag)
    -- addMsgCallBack(self, MSG_CAPTURE_4_DIALOG, handler(self, self.onCapture4Dialog))
end

--创建定时器
function XbDialog:createScheduler()
    self.shcedeuler = scheduler.scheduleGlobal(handler(self, self.schedulerUpdate), 0.1)
end

function XbDialog:schedulerUpdate(t)
    if self.isShowHorseLamp then
        self:isShowHorseLamp(false)
    end
    
end

--跑马灯隐藏
function XbDialog:isShowHorseLamp(_isShow)
    local scene = cc.Director:getInstance():getRunningScene()
    if scene:getChildByTag(555555) then
        scene:getChildByTag(555555):setVisible(_isShow)
    end
end

-- 设置是否显示动画
-- params __isEnable(bool) 是否启动动画
function XbDialog:enableAnimation( __isEnable )
    self.isAnimationEnable = __isEnable
end

-- 设置动画类型
-- params __digAnimation(DIALOGANI) 动画类型
function XbDialog:setAnimationType( __digAnimation )
    
end

-- 设置背景蒙版样式
-- @params __mode(DIALOGMOUNT) 蒙版样式
-- DIALOGMOUNT = 
-- {
--     NONE        = 0, -- 无蒙版
--     SHADOW      = 1, -- 黑色半透明
--     BLUR        = 2, -- 模糊背景
-- }
function XbDialog:setMountMode( __mode )
    self.bgMountMode = __mode
end

-- 设置背景颜色以及透明度
-- params __color(cc.c3b) 背景颜色
-- params __opacity(number) 背景透明度
function XbDialog:setColorAndOpacity( __color, __opacity )
    self.dlgLayoutMain:setColor(__color)
    if __opacity then
        self.dlgLayoutMain:setBackGroundColorOpacity(__opacity)
    end
end


--设置背景透明度
-- params __opacity(number) 背景透明度
function XbDialog:setBgOpacity( __opacity ) 
    if __opacity then
        self.dlgLayoutMain:setBackGroundColorOpacity(__opacity)
    end
end

-- 设置父节点StackLayer
-- params __stackLayer(StackLayer) 服节点StackLayer
function XbDialog:setParentStacklayer( __stackLayer )
    self.parentStackLayer = __stackLayer
end

-- 获取父节点StackLayer
function XbDialog:getParentStacklayer()
    return self.parentStackLayer
end

-- 将对话框设置为最高对话框
function XbDialog:setTopDialog()
    self.isTopDialog = true
end

-- 显示对话框
-- params __scene(SceneBase) 所在场景
function XbDialog:showDialog( __scene )
    local scene = __scene or display.getRunningScene()

    CUR_GLOBAL_ZORDER = CUR_GLOBAL_ZORDER + 1
    local zOr = CUR_GLOBAL_ZORDER
    if self.isTopDialog == true then
        zOr = GlobalDefine.SO_BIG_NUMBER
    end
    self:setLocalZOrder(zOr)
    if self.isSetZorder then
        self:setLocalZOrder(self.zorder)
    end

    if CUR_GLOBAL_ZORDER > 0x00ffffff then
        CUR_GLOBAL_ZORDER = 0x00ffffff 
    end
    
    if self.parentStackLayer then
        self.parentStackLayer:addChild(self, zOr)
    else
        scene:addChild(self, zOr)
    end
    if self.isTopDialog == true and scene.addTopDialog then
        scene:addTopDialog(self)
        GlobalViewBaseStack[#GlobalViewBaseStack+1] = self
    elseif scene.addTopDialog then
        if scene:getTopDialog() then
            GlobalViewBaseStack[#GlobalViewBaseStack] = self
            GlobalViewBaseStack[#GlobalViewBaseStack+1] = scene:getTopDialog()
        else
            GlobalViewBaseStack[#GlobalViewBaseStack+1] = self
        end
    else
        return
    end
    scene:pushDialog(self)
    self.parentScene = scene

    if self.bgMountMode == DIALOGMOUNT.SHADOW then
        if self.isAnimationEnable then
            self:showEffect()
        end
    else
        self:setBgOpacity(0)
        if self.isAnimationEnable then
            self:showEffect()
        end
    end

end


-- 设置对话框zorder
-- params __zorder(number) zorder
function XbDialog:setDialogZorder( __zorder )
    self.zorder = __zorder
    self.isSetZorder = true
end

-- 获取动画节点
-- return (Node) 用户添加进来的第一个节点, 所以如果需要动画, 折只允许添加一个节点进来
function XbDialog:getAnimationNode()
    local children = self:getChildren()
    for i = 1, #children do
        if children[i]:getName() ~= "dialog_root" then
            return children[i]
        end
    end
    return nil
end

-- 显示动画
function XbDialog:showEffect()
    local node = self:getAnimationNode()
    if nil == node then
        print("[ERROR]: get nil when attempt to get Animation node for showing dialog")
        return
    end

    node:setAnchorPoint(cc.p(0.5, 0.5))
    node:setPosition(display.width / 2, display.height / 2)
    if self.dlgAnimation == DIALOGANI.ZERO_TO_WHOLE then
        node:setScale(0)
        
        -- local action1 = cc.ScaleTo:create(0.1, 1)
        -- local action2 = cc.ScaleTo:create(0.05, 0.9)
        -- local action3 = cc.ScaleTo:create(0.02, 1)
        -- node:runAction(cc.Sequence:create(action1, action2, action3))
        node:runAction(cc.ScaleTo:create(0.1, 1))
    end
end

-- 设置关闭回调
-- params __callback(function) 对话框关闭时的回调函数
function XbDialog:setCloseCallback( __callback )
    self.closeCallBack = __callback
end

-- 关闭对话框
function XbDialog:closeDialog()
    print("close a dialog which tag is: ", self.dialogTag)

    self:isShowHorseLamp(true)

    if self.closeCallBack then
        self:closeCallBack()
    end

    if self.shcedeuler then
        scheduler.unscheduleGlobal(self.shcedeuler)
        self.shcedeuler = nil
    end

    local scene = self.parentScene
    if scene and scene.getDialogs and #scene:getDialogs() > 0 then
        for i = 1, #scene:getDialogs() do
            if scene:getDialogs()[i] == self then
                table.remove(scene:getDialogs(), i)
            end
        end
    end
    if scene and scene.getTopDialog and  scene:getTopDialog() then
        scene:addTopDialog(nil)
    end

    self:removeFromParent()

    for i,v in ipairs(GlobalViewBaseStack) do
        if v == self then
            table.remove(GlobalViewBaseStack,i)
            break
        end
    end
end

-- 设置背景是否可点击
-- params __isEnable(bool) 是否可点击
function XbDialog:enableTouch( __isEnable )
    self.dlgEnableTouch = __isEnable
end

-- 获取控件类型
-- return (string) 对话框控件类型(dialog)
function XbDialog:getType()
    return "dialog"
end

function XbDialog:onTouchCallback( __sender )
    if __sender:getName() == "layout_main" then
        cc.Director:getInstance():getOpenGLView():setIMEKeyboardState(false, 0) -- close keyboard !
        if self.dlgEnableTouch then
            self:closeDialog()
        end
    end
end

return XbDialog


