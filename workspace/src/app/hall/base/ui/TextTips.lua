--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- To change this template use File | Settings | File Templates.
-- 小型文字提示弹出框

local SmallTipsDialog = class("SmallTipsDialog", function ()
    return display.newNode()
end)

QKATipsList = {} -- 显示列表
currentScene = nil -- 当前场景, 若不是同一场景, 则清空列表

function SmallTipsDialog:ctor( __str ,__callback,flag)
    self:myInit(__str,__callback,flag)
end

function SmallTipsDialog:myInit( str ,__callback,flag)
    self._str = str
    self._finishCallback = __callback
    self.second = 1.5
    self:setupViews(flag)
end

function SmallTipsDialog:setupViews(flag)
    self.node = self:getTipsNode(self._str)
    self:addChild(self.node)
    self._height = self.node._height
    ToolKit:registDistructor(self, function ()
        for i = 1, #QKATipsList do
            if QKATipsList[i] == self then
                table.remove(QKATipsList, i)
            end
        end
    end)
    showTipsDlg(self,flag)
end

function SmallTipsDialog:getTipsNode(str)
    local node = UIAdapter:createNode("csb/common/small_tips_layer.csb")
    node:setName("tipsNode")
    local label = node:getChildByName("content")
    local bg = node:getChildByName("img_bg")
    label:setString(str)
    label:enableOutline(cc.c4b(0,0,0,200), 3)
    local width = label:getContentSize().width
    local fixWidth = width + 150
    bg:setContentSize(cc.size(fixWidth, bg:getContentSize().height))
    node._height = bg:getContentSize().height
    if width > (display.width - 200) then
        label:setVisible(false)
        local fix_label = cc.ui.UILabel.new({
            text = str,
            font = "ttf/jcy.ttf",
            size = 36,
            dimensions = cc.size(display.width - 200, 100),
        })
        fix_label:setAnchorPoint(cc.p(0.5, 0.58))
        fix_label:setPosition(cc.p(0, 0))
        node:addChild(fix_label)
        bg:setContentSize(cc.size(display.width - 150, 120))
        fix_label:enableOutline(cc.c4b(0,0,0,200), 3)
        node._height = 120
    end
    return node
end

function SmallTipsDialog:autoClose()
    if self._callback then
        self._callback()
    end
    self:removeFromParent()
end

function SmallTipsDialog:updateLabel()
    local label = self.node:getChildByName("content")
    local bg = self.node:getChildByName("img_bg")
    label:setString(self._str)
    local width = label:getContentSize().width
    bg:setContentSize(cc.size(width + 140, bg:getContentSize().height))
end

function RemoveTipsDlg( pNode )
    -- 删掉第一个
    table.remove(QKATipsList, 1)
    --[[local pMove = pNode:getActionByTag(100)
    if pMove then
        pMove:update(1);
        pNode:stopActionByTag(100)
    end

    pNode:stopActionByTag(101)

    local h = pNode._height--]]

    --local moveBy = cc.MoveBy:create(0.15, cc.p(0, h))

    pMove = cc.Sequence:create(cc.Spawn:create(cc.FadeOut:create(2)), cc.CallFunc:create(function ()
        pNode:setVisible(false)
        if pNode._finishCallback then
            pNode._finishCallback()
        end
        pNode:removeFromParent()
    end))

    --[[pMove:setTag(100)--]]

    local tipsNode = pNode:getChildByName("tipsNode")

    --pNode:runAction(pMove)
    tipsNode:runAction(pMove)

    -- 把剩下的往上移动
    --[[for i = 1, #QKATipsList do
        local node = QKATipsList[i]
        pMove = node:getActionByTag(100)
        if pMove then
            pMove:update(1)
            node:stopActionByTag(100)
        end
        pMove = cc.MoveBy:create(0.15, cc.p(0, h))
        pMove:setTag(100)
        node:runAction(pMove)
    end--]]
end

function clearAllDlg()
    for i = #QKATipsList, 1, -1 do
        if not tolua.isnull(QKATipsList[i]) then
            QKATipsList[i]:removeFromParent()
        end
        table.remove(QKATipsList, i)
    end
end

function showTipsDlg( _pNode,flag )
    local scene = cc.Director:getInstance():getRunningScene()
    local y = display.height * 0.7 -- Y轴的位置
    if currentScene == nil then
        currentScene = scene
    elseif currentScene ~= nil and currentScene ~= scene then
        for i = #QKATipsList, 1, -1 do
            QKATipsList[i]:stopAllActions()
            QKATipsList[i]:removeFromParent()
            table.remove(QKATipsList, i)
        end
        QKATipsList = {}
        currentScene = scene
    end

   --[[ for i = 1, #QKATipsList do
        y = y - QKATipsList[i]._height
    end--]]

    _pNode:setPosition(display.width / 2, display.height / 2)

    scene:addChild(_pNode, 0x00ffffff)

    for i=1, #QKATipsList do
        QKATipsList[i]:stopAllActions()
        QKATipsList[i]:removeFromParent()
        table.remove(QKATipsList, i)
    end

    QKATipsList[#QKATipsList + 1] = _pNode

    --[[_pNode:setPositionY(y -  2*_pNode._height)--]]

    --[[print("_pNode.second = ", _pNode.second)--]]

    local pWait = cc.Sequence:create(cc.DelayTime:create(_pNode.second), cc.CallFunc:create(function ()
        if QKATipsList[1] then
            RemoveTipsDlg(QKATipsList[1])
        end
    end))




   --[[ pWait:setTag(101)

    local pMove = cc.Spawn:create(cc.FadeIn:create(0.15), cc.MoveBy:create(0.15, cc.p(0, _pNode._height)))
    pMove:setTag(100)

    _pNode:runAction(pMove)
    _pNode:runAction(pWait)

    if #QKATipsList > 5 then
        RemoveTipsDlg(QKATipsList[1])
    end--]]

    _pNode:setScale(0.8)
    local pShow = cc.Sequence:create(cc.ScaleTo:create(0.1, 1.1), cc.ScaleTo:create(0.1, 1))
    _pNode:runAction(pShow)

    _pNode:runAction(pWait)
    if flag then
        _pNode:setRotation(-90)
    end
end

return SmallTipsDialog
