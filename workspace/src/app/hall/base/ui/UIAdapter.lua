--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 适配方案 
local ParseNodeTable = {
    "image",
    "button",
    "sprite",
    "text",
    "atlas",
    "listView",
    "slider",
    "panel",
    "bmfont",
    "node",
    "fileNode",
    "checkBox",
    "loadingBar",
    "textField",
    "pageView",
    "scrollView",
    "particle",
    "audio",
    "map",
}
UIAdapter = class("UIAdapter")

---方正粗圆简体
UIAdapter.TTF_FZCYJ = "hall/font/fzft.ttf"

---经典中圆简体
-- UIAdapter.TTF_YT = "ttf/yt.TTF"

UIAdapter.sceneStack = {}

-- 创建csb节点
-- @param __csbFileName(string) csb文件路径
function UIAdapter:createNode( __csbFileName )
    print("UIAdapter:createNode: ", __csbFileName)
    return cc.CSLoader:createNode(__csbFileName)
end

-- UI适配
-- @param _node(CCNode) 根节点
-- @param _touchCallBack(function) 触摸回调地址
-- @param _isRoot(bool) 是否根节点
function UIAdapter:praseNode( _node, Root )
     local children = _node:getChildren()

    for i=1, #children do  
        local child = children[i]
        local name = child:getName()
        if Root then
            Root[name] =child 
        end
        UIAdapter:praseNode( child, Root )
    end
end
--解析界面节点
function UIAdapter:parseCSDNode(_node, Root)
      local children = _node:getChildren()

    for i=1, #children do  
        local child = children[i]
        local name = child:getName()
         local function checkName(name)
            for k,v in pairs(ParseNodeTable) do
                local ret = string.find(name, v .. "_")
                if ret then
                    return true
                end
            end
        end
        if checkName(name) then
            local _, _, name = string.find(name, "_(.+)")
           
            if Root then
                 if name ~= "close" then
                    
                    Root[name] =child 
                end
            end 
        end
       
        UIAdapter:parseCSDNode( child, Root )
    end
      
end
function UIAdapter:adapter( _node, _touchCallBack,_isRoot )
    if _node == nil then
        return false
    end

    if _isRoot == nil then
        _isRoot = true
    end

    if _isRoot then
        _node:setContentSize(display.size)
    end 
    local children = _node:getChildren()

    for i=1, #children do  
        local child = children[i]
        local name = child:getName()
       
        -- print("child:", name, child:getDescription())
        if _touchCallBack then
            UIAdapter:registClickCallBack(child, _touchCallBack)
        end

        if child:getDescription() == "Label" then
            child:setFontName(UIAdapter.TTF_FZCYJ)
            -- child:enableShadow()
        elseif child:getDescription() == "Button" then
            child:setTitleFontName(UIAdapter.TTF_FZCYJ)
        end

        local _, p = string.find(name, "@")

        if p ~= nil then 
            
            local parentSize = display.size
            local parent = child:getParent()
            -- print("size: display=================")
            if parent then
                -- print("size: parent=================")
                parentSize = parent:getContentSize()
            end
            
            local size = child:getContentSize()
            
            local suffix = string.sub(name, p + 1)
            local array = string.split(suffix, "@")
            -- dump(array)
            for i = 1, #array do
                local subSuffix = array[i]
                -- print("subSuffix:", name, subSuffix)
                if subSuffix == "bg" then -- 背景
                    child:setAnchorPoint(0.5, 0.5)
                    child:setPosition(display.cx, display.cy)
                    child:setContentSize(display.size)
                elseif subSuffix == "sfull" then -- 最大拉伸
                    child:setScale(parentSize.width / size.width, parentSize.height / size.height)
                elseif subSuffix == "cfull" then -- 最大铺满
                    child:setContentSize(parentSize)
                elseif subSuffix == "sx" then -- X拉伸
                    child:setScaleX(parentSize.width / size.width)
                elseif subSuffix == "sy" then -- Y拉伸
                    child:setScaleY(parentSize.height / size.height)
                elseif subSuffix == "cx" then -- X轴铺满
                    child:setContentSize(cc.size(parentSize.width, size.height))
                elseif subSuffix == "cy" then -- Y轴铺满
                    child:setContentSize(cc.size(size.width, parentSize.height))
                elseif subSuffix == "1" then
                    child:setAnchorPoint(cc.p(0, 1))
                    child:setPosition(0, parentSize.height)
                elseif subSuffix == "2" then
                    child:setAnchorPoint(cc.p(0.5, 1))
                    child:setPosition(parentSize.width / 2, parentSize.height)
                elseif subSuffix == "3" then
                    child:setAnchorPoint(cc.p(1, 1))
                    child:setPosition(parentSize.width, parentSize.height)
                elseif subSuffix == "4" then
                    child:setAnchorPoint(cc.p(0, 0.5))
                    child:setPosition(0, parentSize.height / 2)
                elseif subSuffix == "5" then
                    child:setAnchorPoint(cc.p(0.5, 0.5))
                    child:setPosition(parentSize.width / 2, parentSize.height / 2)
                elseif subSuffix == "6" then
                    child:setAnchorPoint(cc.p(1, 0.5))
                    child:setPosition(parentSize.width, parentSize.height / 2)
                elseif subSuffix == "7" then
                    child:setAnchorPoint(cc.p(0, 0))
                    child:setPosition(0, 0)
                elseif subSuffix == "8" then
                    child:setAnchorPoint(cc.p(0.5, 0))
                    child:setPosition(parentSize.width / 2, 0)
                elseif subSuffix == "9" then
                    child:setAnchorPoint(cc.p(1, 0))
                    child:setPosition(parentSize.width, 0)
                else
                    print("UIAdapter:adapter-----> unknown suffix: ", subSuffix)
                end
            end
            

            local finalName = string.sub(name, 1, p - 1)
            -- print(finalName)
            child:setName(finalName)
           
        end

        UIAdapter:adapter(child, _touchCallBack,false)
    end
end


-- 注册按钮点击时间(CCUI)
-- @param _btn(CCNode) 节点
-- @param _handler(function) 触摸回调地址
-- @param _isNeedScale(bool) 是否需要缩放
function UIAdapter:registClickCallBack( _btn, _handler, _isNeedScale )
    if _btn == nil or _handler == nil then
        print("get nil btn or handler when registClickCallBack")
        return 
    end
    local isNeedScale = _isNeedScale or false
    if _btn.addTouchEventListener == nil then
        -- print("btn is not click type ui !")
        return 
    end
	
    _btn:addTouchEventListener(function ( sender, eventType )
		if sender then
			if eventType == ccui.TouchEventType.began then 
        
			elseif eventType == ccui.TouchEventType.canceled then
		
			elseif eventType == ccui.TouchEventType.ended then
--                if g_LastSenderName then 
--                    if g_LastSenderName == sender:getName() then 
--                        sender:setEnabled(false)
--                        performWithDelay(sender, function()
--                            sender:setEnabled(true)
--                        end, 1)
--                        return
--                    end 
--                end
--                 g_LastSenderName =sender:getName() 
				if sender:getDescription() == "Button" then
					g_AudioPlayer:playEffect(ToolKit:getCurrentScene():getSoundPath()) -- play effect sound
				end
				_handler(sender)
            end
        end
    end)
end

-- push场景
-- @param __scenePath: 场景类路径
-- @param __direction: 方向
-- @param __params: 创建场景时需要的参数
function UIAdapter:pushScene( __scenePath, __direction, __params )
   -- local direction = ToolKit:getCurrentScene():getSceneDirection()

   -- if direction ~= __direction then -- 屏幕不一样, 需要转屏
     --   qka.CCGameOrientation:changOrientationDir(__direction == DIRECTION.HORIZONTAL)
     --   display.resetDisplay(__direction == DIRECTION.HORIZONTAL)
     ---   qka.CCGameOrientation:chanOrientationDirOver()
    --end
     local name = UIAdapter.sceneStack[#UIAdapter.sceneStack]:getSceneName()
    if name == "TotalScene" then  
        cc.AnimationCache:destroyInstance()
        ccs.ArmatureDataManager:destroyInstance()
        cc.SpriteFrameCache:getInstance():removeSpriteFrames()
            cc.SpriteFrameCache:getInstance():removeUnusedSpriteFrames()
        cc.Director:getInstance():getTextureCache():removeAllTextures() 
            cc.Director:getInstance():getTextureCache():removeUnusedTextures()  
            cc.Director:getInstance():purgeCachedData()
        local num =   cc.Director:getInstance():getTextureCache():getCachedTextureInfo() 
    end
    local Scene = require(__scenePath)
    local scene = Scene.new(__params)
    cc.Director:getInstance():pushScene(scene)

    table.insert(UIAdapter.sceneStack, scene)
    --更新背景音乐
    scene:onMusicStatusChanged()
    if QKATipsList then
        clearAllDlg()
    end
    return scene
end

-- replace场景
-- @param __scenePath: 场景类路径
-- @param __direction: 方向
-- @param __params: 创建场景时需要的参数
function UIAdapter:replaceScene( __scenePath, __direction, __params )
    local Scene = require(__scenePath)
    local scene = Scene.new(__params)
    cc.Director:getInstance():replaceScene(scene)
    table.remove(UIAdapter.sceneStack, #UIAdapter.sceneStack)
    table.insert(UIAdapter.sceneStack, scene)
    --更新背景音乐
 --   scene:onMusicStatusChanged()
    if QKATipsList then
        clearAllDlg()
    end
    return scene
end


-- pop场景
function UIAdapter:popScene()
    if #UIAdapter.sceneStack <= 1 then
        print("[ERROR] Scene stack size minus when attempt to pop scene!")
        return
    end


    local name = UIAdapter.sceneStack[#UIAdapter.sceneStack - 1]:getSceneName()
    if name == "TotalScene" then
        --大厅场景需重载资源
        UIAdapter:reloadAllResources()
      --  ProtalController:getInstance():back2ProtalId( 0 )
    end

    cc.Director:getInstance():popScene()
    table.remove(UIAdapter.sceneStack, #UIAdapter.sceneStack)
    if #UIAdapter.sceneStack > 0 then
        local scene = UIAdapter.sceneStack[#UIAdapter.sceneStack]
        -- local name = scene:getSceneName()
        -- if name == "TotalScene" then
        --     --大厅场景需重载资源
        --     UIAdapter:reloadAllResources()
        -- end
        if #scene:getStackLayerManager().stackLayerList == 1 then
            local layer = scene:getStackLayerManager():getCurrentLayer()
            if layer:getExitRemove() then
                cc.Director:getInstance():pushScene(cc.Scene:create())

                cc.Director:getInstance():popToSceneStackLevel(cc.Director:getInstance():getSceneStackSize() - 2)
                table.remove(UIAdapter.sceneStack, #UIAdapter.sceneStack)
                return 
            end
        end
        scene:onMusicStatusChanged()
    end
    --优化断线重连体验
    --g_LoginController._isReconnect = false
end

-- 退出到根场景
function UIAdapter:pop2RootScene()
    if #UIAdapter.sceneStack <= 1 then
        print("pop2RootScene already rootScene")
        return
    end
    while #UIAdapter.sceneStack > 1 do
        local name = UIAdapter.sceneStack[#UIAdapter.sceneStack - 1]:getSceneName()
        if name == "TotalScene" then
            --大厅场景需重载资源
            UIAdapter:reloadAllResources()
        end
        table.remove(UIAdapter.sceneStack, #UIAdapter.sceneStack)
    end
    local scene = UIAdapter.sceneStack[#UIAdapter.sceneStack]
    scene:onMusicStatusChanged()
    -- cc.Director:getInstance():popToRootScene()
    cc.Director:getInstance():pushScene(cc.Scene:create())
    cc.Director:getInstance():popToSceneStackLevel(1)
end

-- 重载大厅资源，删除其余资源
function UIAdapter:reloadAllResources()
    local function delay()
        
        local name = UIAdapter.sceneStack[#UIAdapter.sceneStack]:getSceneName()
        if name == "TotalScene" then  
            cc.AnimationCache:destroyInstance()
            ccs.ArmatureDataManager:destroyInstance()
            cc.SpriteFrameCache:getInstance():removeSpriteFrames()
             cc.SpriteFrameCache:getInstance():removeUnusedSpriteFrames()
            cc.Director:getInstance():getTextureCache():removeAllTextures() 
             cc.Director:getInstance():getTextureCache():removeUnusedTextures()  
             cc.Director:getInstance():purgeCachedData()
            local num =   cc.Director:getInstance():getTextureCache():getCachedTextureInfo() 
        end
    end
    ToolKit:delayDoSomething(delay, 1)
end

-- 获取上一个场景
-- @return (SceneBase) 场景栈的上一个
function UIAdapter:getPreScene()
    local scene = UIAdapter.sceneStack[#UIAdapter.sceneStack - 1]
    if nil == scene then
        print("[ERROR]: Get nil when attempt to get pre scene!")
    end
    return scene
end


--将普通触摸优先级的node升级成Widget触摸优先级
function UIAdapter:transNode(luaNode,isSwallowTouches)
    local function checkVisible(node)
        if node:isVisible() == false then
            return false
        elseif node.getParent and node:getParent() then
            return checkVisible(node:getParent())
        end
        return true
    end
    local isSwallowTouches = isSwallowTouches
    if isSwallowTouches == nil and luaNode.isTouchSwallowEnabled then
        isSwallowTouches = luaNode:isTouchSwallowEnabled()
    end
    if isSwallowTouches == nil then
        isSwallowTouches = true
    end
    if luaNode._scriptEventListeners_==nil or luaNode._scriptEventListeners_[cc.NODE_TOUCH_EVENT]==nil then
        return
    end
    luaNode:setTouchEnabled(false)
    luaNode._touchEnabled = false
    function luaNode.onTouchBegan (touch, unusedEvent)
        local event = {name="began",
            x=touch:getLocation().x,
            y=touch:getLocation().y,
            prevX=touch:getPreviousLocation().x,
            prevY=touch:getPreviousLocation().y,
            }
        local inArea = luaNode._touchEnabled and checkVisible(luaNode) and luaNode:getCascadeBoundingBox():containsPoint(touch:getLocation())
        -- print(luaNode:convertToWorldSpace(cc.p(luaNode:getPosition())).x)
        -- print(touch:getLocation().x)
        -- print(luaNode:convertToWorldSpace(cc.p(luaNode:getPosition())).y)
        -- print(touch:getLocation().y)
        if inArea then
            luaNode:EventDispatcher(cc.NODE_TOUCH_EVENT,event)
        end
        return inArea
    end
    function luaNode.onTouchMoved (touch, unusedEvent)
        local event = {name="moved",
            x=touch:getLocation().x,
            y=touch:getLocation().y,
            prevX=touch:getPreviousLocation().x,
            prevY=touch:getPreviousLocation().y,
            }
        local inArea = luaNode._touchEnabled --and luaNode:getCascadeBoundingBox():containsPoint(touch:getLocation())
        if inArea then
            luaNode:EventDispatcher(cc.NODE_TOUCH_EVENT,event)
        end
    end
    function luaNode.onTouchEnded (touch, unusedEvent)
        local event = {name="ended",
            x=touch:getLocation().x,
            y=touch:getLocation().y,
            prevX=touch:getPreviousLocation().x,
            prevY=touch:getPreviousLocation().y,
            }
        local inArea = luaNode._touchEnabled --and luaNode:getCascadeBoundingBox():containsPoint(touch:getLocation())
        if inArea then
            luaNode:EventDispatcher(cc.NODE_TOUCH_EVENT,event)
        -- else
        --  event.name="cancelled"
        --  luaNode:EventDispatcher(cc.NODE_TOUCH_EVENT,event)
        end
    end
    function luaNode.onTouchCancelled (touch, unusedEvent)
        local event = {name="cancelled",
            x=touch:getLocation().x,
            y=touch:getLocation().y,
            prevX=touch:getPreviousLocation().x,
            prevY=touch:getPreviousLocation().y,
            }
        if luaNode._touchEnabled then
            luaNode:EventDispatcher(cc.NODE_TOUCH_EVENT,event)
        end
    end
    function luaNode:setTouchEnabled(enable)
        if enable == self._touchEnabled then
            return
        end
        local _eventDispatcher = cc.Director:getInstance():getEventDispatcher()
        self._touchEnabled = enable
        if (self._touchEnabled) then
            self._touchListener = cc.EventListenerTouchOneByOne:create()
            self._touchListener:setSwallowTouches(isSwallowTouches)
            self._touchListener:registerScriptHandler(self.onTouchBegan,cc.Handler.EVENT_TOUCH_BEGAN)
            self._touchListener:registerScriptHandler(self.onTouchMoved,cc.Handler.EVENT_TOUCH_MOVED)
            self._touchListener:registerScriptHandler(self.onTouchEnded,cc.Handler.EVENT_TOUCH_ENDED)
            self._touchListener:registerScriptHandler(self.onTouchCancelled,cc.Handler.EVENT_TOUCH_CANCELLED)
            _eventDispatcher:addEventListenerWithSceneGraphPriority(self._touchListener,self)
        else
            _eventDispatcher:removeEventListener(self._touchListener)
        end
    end
    luaNode:setTouchEnabled(true)
    return luaNode
end

--- 绑定文字按钮
function UIAdapter:bindingLabelBtn(btn, callback)
    btn._color = btn:getColor()
    local function onTouchBegan(touch, event)
        if btn:getCascadeBoundingBox():containsPoint(touch:getLocation()) then
            btn:setColor(cc.c3b(255,255,255))
            btn._touch_flag = true
            return true
        end
        return false
    end
    local function onTouchMoved(touch, event)
        if not btn:getCascadeBoundingBox():containsPoint(touch:getLocation()) then
            btn._touch_flag = false
            btn:setColor(btn._color)
        end
    end
    local function onTouchEdned(touch, event)
        if btn._touch_flag then
            btn:setColor(btn._color)
            btn._touch_flag = false
            callback()
        end
    end
    local listener = cc.EventListenerTouchOneByOne:create()
    listener:registerScriptHandler(onTouchBegan,cc.Handler.EVENT_TOUCH_BEGAN )
    listener:registerScriptHandler(onTouchMoved,cc.Handler.EVENT_TOUCH_MOVED )
    listener:registerScriptHandler(onTouchEdned,cc.Handler.EVENT_TOUCH_ENDED )
    local eventDispatcher = cc.Director:getInstance():getEventDispatcher()
    eventDispatcher:addEventListenerWithSceneGraphPriority(listener, btn)
end

--获取TotalScene
function UIAdapter:getTotalScene()
    for i,v in ipairs(UIAdapter.sceneStack) do
        if v:getSceneName() == "TotalScene" then
            return v
        end
    end
end

--获取当前场景名称
function UIAdapter:isLoginScene()
	if #UIAdapter.sceneStack == 0 then
		return true
	else
		return UIAdapter.sceneStack[#UIAdapter.sceneStack]:getSceneName() == "LoginScene"
	end
end

function UIAdapter:showMenuPop(node, callback, callback2, x, y)
    
    if node==nil or callback==nil or callback2==nil then 
        return
    end
    g_AudioPlayer:playEffect("res/public/sound/sound-button.mp3")
    node:stopAllActions()
    local aTime = 0.3
    local desX = x or 0
    local desY = y or 0
    node:setOpacity(0)
    local moveTo = cc.MoveTo:create(aTime, cc.p(desX, desY))
    local show = cc.Show:create()
    local sp = cc.Spawn:create(cc.EaseBackOut:create(moveTo), cc.FadeIn:create(aTime))
    local seq = cc.Sequence:create(show, sp, callback, cc.DelayTime:create(0.1), callback2)
    node:runAction(seq)
end

function UIAdapter:showMenuPush(node, callback, callback2, x, y)

    if node==nil or callback==nil or callback2==nil then 
        return
    end
    g_AudioPlayer:playEffect("res/public/sound/sound-close.mp3")
    node:stopAllActions()
    local aTime = 0.25
    local desX = x or 0
    local desY = y or 0
    local moveTo = cc.MoveTo:create(aTime, cc.p(desX, desY))
    local sp = cc.Spawn:create(cc.EaseBackIn:create(moveTo), cc.FadeOut:create(aTime))
    local hide = cc.Hide:create()
    local seq = cc.Sequence:create(sp, callback, hide, cc.DelayTime:create(0.1), callback2)
    node:runAction(seq)
end

function UIAdapter:CreateRecord(_font, _fontSize, _color, _outLineColor, _outLineColorSize)
    local font = _font or UIAdapter.TTF_FZCYJ
    local fontSize = _fontSize or 22
    local color = _color or cc.c3b(255, 255, 255)
    local outLineColor = _outLineColor or cc.c4b(0, 0, 0, 255) --cc.c4b(96, 55, 33, 255)
    local outLineColorSize = _outLineColorSize or 1
    local textRecord = display.newTTFLabel({
        text = "",
        font = font,
        size = fontSize,
        color = color,
    })
    textRecord:enableOutline(outLineColor, outLineColorSize)
    textRecord:setAnchorPoint(cc.p(0, 0.5))
    return textRecord
end

function UIAdapter:replaceParent(node, parent, zorder)
    node:retain()
    if (node:getParent()) then
        node:removeFromParent()
    end
    parent:addChild(node, zorder or 0)
    node:release()
    return node
end

--设置是否多点触控
function UIAdapter:setIsTouches(isTouchs)
    if device.platform == "android" then
        -- local javaClassName = "org/cocos2dx/lib/Cocos2dxGLSurfaceView"
        -- local javaMethodName = "setMaxTouchCount"
        -- local javaMethodSig = "(ILjava/lang/String)V"
        -- if isTouchs then
        --     local ok, ret = luaj.callStaticMethod(javaClassName, javaMethodName, 10, javaMethodSig)
        -- else
        --     local ok, ret = luaj.callStaticMethod(javaClassName, javaMethodName, 1, javaMethodSig)
        -- end
    elseif device.platform == "ios" then
    end
end

function UIAdapter:setChildrensFontTTF(_node, _ttf)
    local ttf = _ttf or UIAdapter.TTF_FZCYJ

    local children = _node:getChildren()
    for i=1, #children do  
        local child = children[i]
        if child:getDescription() == "Label" then
            child:setFontName(ttf)
        elseif child:getDescription() == "Button" then
            child:setTitleFontName(ttf)
        end
        UIAdapter:setChildrensFontTTF(child, ttf)
    end
end

function UIAdapter:setResDownloadProgress(item, progress, proImgFileName, _point)
    if (nil == item.m_Progress) then
        item.__downloadStatus = 1
        -- 创建精灵时，如果图片资源在大图里，可用createWithSpriteFrameName创建
        cc.SpriteFrameCache:getInstance():addSpriteFrames("hall/plist/gui-hall.plist", "hall/plist/gui-hall.png")

        local sliderName = "hall/plist/hall/image-download-res-pro.png"
        local sliderBgName = "hall/plist/hall/image-download-res-bg.png"

        local itemSize = item:getContentSize()

        local point = _point or cc.p(itemSize.width/2, itemSize.height/2)


        --图片进度
        --local fileName = "hall/image/match/freebm.png";--string.format("hall/image/gamekind/gamekind-%d.png",item.m_nKindId)
        local progressImg = cc.Sprite:create(proImgFileName)
        item.m_pImageTimer = cc.ProgressTimer:create(progressImg)
        item.m_pImageTimer:setAnchorPoint(cc.p(0.5, 0.5))
        item.m_pImageTimer:setPosition(cc.p(point))
        item.m_pImageTimer:setType(cc.PROGRESS_TIMER_TYPE_BAR) -- 设置为条
        item.m_pImageTimer:setMidpoint(cc.p(0, 0))             -- 设置起点为条形左下方
        item.m_pImageTimer:setBarChangeRate(cc.p(0, 1))        -- 设置为竖直方向
        item.m_pImageTimer:setOpacity(80)
        item.m_pImageTimer:setColor(cc.c3b(0, 0, 0))
        item.m_pImageTimer:setPercentage(100)
        item.m_pImageTimer:setCascadeOpacityEnabled(true)
        item:addChild(item.m_pImageTimer)

        local sprite = cc.Sprite:createWithSpriteFrameName(sliderName)
        item.m_ProgressBg = cc.Sprite:createWithSpriteFrameName(sliderBgName)
        item.m_ProgressBg:setAnchorPoint(cc.p(0.5, 0.5))
        item.m_ProgressBg:setPosition(cc.p(point))
        item:addChild(item.m_ProgressBg)
        item.m_Progress = cc.ProgressTimer:create(sprite)
        item.m_Progress:setType(cc.PROGRESS_TIMER_TYPE_RADIAL)
        item.m_Progress:setBarChangeRate(cc.p(1,0))
        item.m_Progress:setMidpoint(cc.p(0.5,0.5)) 
        item.m_Progress:setAnchorPoint(cc.p(0.5,0.5))
        item.m_Progress:setPosition(cc.p(point))
        item.m_ProLabel = display.newTTFLabel({
            text = "0%",
            size = 22,
            align = cc.TEXT_ALIGNMENT_CENTER,
            color = cc.c3b(255, 255, 255),
        }):addTo(item.m_Progress):align(display.CENTER, sprite:getContentSize().width*0.5, 30)
        item:addChild(item.m_Progress)
    end
    item.m_ProgressBg:setVisible(true)
    item.m_Progress:setVisible(true)

    if progress == -1 then -- 游戏更新失败
        TOAST(STR(63,1))
        item.m_Progress:setVisible(false)
        item.m_ProgressBg:setVisible(false)
        item.m_pImageTimer:setVisible(false)

        item.__downloadStatus = 0
        item.m_ProLabel:setString("0%")
        return -1
    elseif progress <= 100 and progress >= 0 then
        item.m_Progress:setPercentage(progress)
        item.m_ProLabel:setString(string.format("%2d",progress) .. "%")
        item.m_pImageTimer:setPercentage(100-progress)
        item.__downloadStatus = 1
        return 1
    elseif progress == 1000 then -- 下载完成
        item.m_Progress:setVisible(false)
        item.m_ProgressBg:setVisible(false)
        item.m_pImageTimer:setVisible(false)
        item.__downloadStatus = 0
        return 0
    end

end

function UIAdapter:createEditBox(config)

    local maxLength = config.maxLength
    local returnType   = config.returnType or cc.KEYBOARD_RETURNTYPE_DONE
    local inputMode     = config.inputMode or cc.EDITBOX_INPUT_MODE_SINGLELINE
    local inputFlag     = config.inputFlag or cc.EDITBOX_INPUT_FLAG_SENSITIVE
    local tag    = config.tag or -1
    local size   = config.size or {width = 200, height = 36}
    local placestr = config.placestr or ""
    local fontSize      = config.fontSize or 24
    local parent = config.parent
    local alignRect = config.alignRect or {left = 0, right = 0, top = 0, down = 0}

    if not config.size and parent then
        local parentSize = parent:getContentSize()
        size = {width = parentSize.width - alignRect.left - alignRect.right, height = parentSize.height - alignRect.top - alignRect.down}
    end


    local sprite1 = ccui.Scale9Sprite:create("hall/image/image_opacity.png")
    local sprite2 = ccui.Scale9Sprite:create("hall/image/image_opacity.png")
    local sprite3 = ccui.Scale9Sprite:create("hall/image/image_opacity.png")

    local editBox = ccui.EditBox:create(size, sprite1, sprite2, sprite3)
    if maxLength then
        editBox:setMaxLength(maxLength)
    end
    editBox:setReturnType(returnType)
    editBox:setInputMode(inputMode)
    editBox:setInputFlag(inputFlag)
    editBox:setTag(tag)
    editBox:setFont("Helvetica", fontSize)

    editBox:setFontColor(cc.c3b(0xFF, 0xFF, 0xFF))
    editBox:setPlaceHolder(placestr)
    editBox:setPlaceholderFontSize(fontSize)
    editBox:setPlaceholderFontColor(cc.c3b(0x7A,0x7B,0x7D))

    editBox:setText("")
    editBox:setAnchorPoint(cc.p(0, 0))
    if parent then
        editBox:addTo(parent)
        editBox:setPosition(cc.p(alignRect.left, alignRect.down))
    end
    return editBox
end

function UIAdapter:adapterEx(_node)
    if nil == _node then
        return false
    end
    -- "@{"NAME":"","X":0,"Y":0,"WIDTH":0,"HEIGHT":0,"LEFT":0,"UP":0,"RIGHT":0,"DOWN":0}"
    -- @{"X":0,"Y":0,"WIDTH":0,"HEIGHT":0,"LEFT":0,"RIGHT":0,"UP":0,"DOWN":0}
    local viewSize = _node:getContentSize()

    local list = _node:getChildren()
    for index = 1, #list do
        local pNode = list[index]
        local nameStr = pNode:getName()
        local findIndex = string.find(nameStr, '@')
        local pJson = nil
        if findIndex then
            if findIndex == 1 then
                local sJson = string.sub( nameStr, 2 )
                pJson = json.decode(sJson)
                pNode:setName(pJson.NAME)
            elseif findIndex > 1 then
                local name = string.sub(nameStr, 1, findIndex - 1)
                pNode:setName(name)
                local sJson = string.sub(nameStr, findIndex + 1)
                pJson = json.decode(sJson)
            end
        end
        if pJson then
            local nodeSize = pNode:getContentSize()
            local anchorPoint = pNode:getAnchorPoint()
            local point = cc.p(pNode:getPosition())
            local isRefret = false

            if pJson.WIDTH and (nil == pJson.LEFT or nil == pJson.RIGHT) then
                nodeSize.width = pJson.WIDTH * viewSize.width / 100
                isRefret = true
            end

            if pJson.HEIGHT and (nil == pJson.UP and nil == pJson.DOWN) then
                nodeSize.height = pJson.HEIGHT * viewSize.height / 100
                isRefret = true
            end

            if pJson.X and nil == pJson.LEFT and nil == pJson.RIGHT then
                point.x = pJson.X * viewSize.width / 100
                isRefret = true
            end

            if pJson.Y and nil == pJson.UP and nil == pJson.DOWN then
                point.y = pJson.Y * viewSize.height / 100
                isRefret = true
            end

            if pJson.LEFT and nil == pJson.RIGHT then
                point.x = pJson.LEFT + nodeSize.width * anchorPoint.x
                isRefret = true
            end

            if pJson.RIGHT and nil == pJson.LEFT then
                point.x = viewSize.width - pJson.RIGHT - nodeSize.width + nodeSize.width * anchorPoint.x
                isRefret = true
            end

            if pJson.UP and nil == pJson.DOWN then
                point.y = viewSize.height - pJson.UP - nodeSize.height + nodeSize.height * anchorPoint.y
                isRefret = true
            end

            if pJson.DOWN and nil == pJson.UP then
                point.y = pJson.DOWN + nodeSize.height * anchorPoint.y
                isRefret = true
            end

            if pJson.LEFT and pJson.RIGHT then
                nodeSize.width = viewSize.width - pJson.LEFT - pJson.RIGHT
                point.x = pJson.LEFT + nodeSize.width * anchorPoint.x
                isRefret = true
            end

            if pJson.UP and pJson.DOWN then
                nodeSize.height = viewSize.height - pJson.UP - pJson.DOWN
                point.y = pJson.DOWN + nodeSize.height * anchorPoint.y
                isRefret = true
            end
            if isRefret then
                local description = pNode:getDescription()
                if "ImageView" == description then
                    pNode:ignoreContentAdaptWithSize(false)
                end
                pNode:setContentSize(nodeSize)
                pNode:setPosition(point)
            end
        end

        self:adapterEx(pNode)
    end

    return true
end

function UIAdapter:getChildByName(node, childPath)
    local resultStrList = {}
    string.gsub(childPath,'[^/]+',function ( w )
        table.insert(resultStrList,w)
    end)
    local child = node
    for index = 1, #resultStrList do
        local name = resultStrList[index]
        child = child:getChildByName(name)
        if child == nil then
            return nil
        end
    end
    if #resultStrList == 0 then
        return nil
    end
    return child
end