--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 功能选择界面

local XbDialog = import("app.hall.base.ui.CommonView")

local FunctionLayer = class("FunctionLayer", function ()
    return XbDialog.new()
end )


function FunctionLayer:ctor( __params )
	self:myInit(__params)
	
	ToolKit:registDistructor( self, handler(self, self.onDestory) )
	
	self:setupViews()
	--self:enableAnimation(false)
	self:setMountMode(DIALOGMOUNT.NONE)
	
	sendMsg(MSG_SHOW_HIDE_LOBBY)
   
end

-- 初始化成员变量
function FunctionLayer:myInit( __params )
    if __params and __params.funcId then
        self.parentId = __params.funcId   -- 父节点id
	end
	  
   
end

function FunctionLayer:setupViews()
	self.root = UIAdapter:createNode("csb/common/layer_function.csb")
    self.root:setAnchorPoint(0,0)
	self:addChild(self.root)
	UIAdapter:adapter(self.root)
	

    self.img_line = self.root:getChildByName("img_line")
    self.img_bg = self.root:getChildByName("img_bg")
    self.ListView_1 = self.root:getChildByName("ListView_1")
    self.ListView_2 = self.root:getChildByName("ListView_2")

	
    self.ListView_1:setInertiaScrollEnabled(false)
    self.ListView_1:setBounceEnabled(false)        
    self.ListView_1:setInnerContainerSize(self.ListView_1:getContentSize())   
    
    self.ListView_2:setInertiaScrollEnabled(false)
    self.ListView_2:setBounceEnabled(false)        
    self.ListView_2:setInnerContainerSize(self.ListView_2:getContentSize())  
   
   -- self:setAnchorPoint(0,0)
    --self.root:setPositionY( - self.root:getContentSize().height )

	--self:setPosition(0,0)
    
     self:updataView()
	
	
end


function FunctionLayer:onDestory()
	g_functionDlg = nil 
    sendMsg(MSG_SHOW_HIDE_LOBBY)
end



function FunctionLayer:updataView()

    local funcData =  getChildrenById(self.parentId)

    local _openNum = 0
    local _maxRowNum = 8
    local _data = {}
    for key, var in ipairs(funcData) do
        if getFuncOpenStatus(var.id) ~= 1 then
            _openNum =  _openNum + 1
            _data[_openNum] = var
        end
    end
    if _openNum > _maxRowNum  then
        self.img_line:setVisible(true)
        self.ListView_1:setVisible(true)

        self.root:setContentSize(display.width,368)
        self.img_bg:setContentSize(display.width,368)

        self.ListView_1:setPositionY(self.img_bg:getContentSize().height*0.55)
        self.ListView_2:setPositionY(self.img_bg:getContentSize().height*0.05)
        self.img_line:setPositionY(self.img_bg:getContentSize().height*0.5)

        local _width = self.ListView_1:getContentSize().width
        local _num = _openNum - _maxRowNum  
        
        _width  =  _width/_maxRowNum*( 1 + (_maxRowNum - _num ) /_maxRowNum *0.8  ) * _num
        
        self.ListView_1:setContentSize(_width  ,self.ListView_1:getContentSize().height ) 
        self.ListView_1:setAnchorPoint(0.5,0)
        self.ListView_1:setPositionX(640)
         
         
        for key, var in ipairs(_data) do 
            if key <= _maxRowNum then
                self:addCellToListview(self.ListView_2 , _maxRowNum , var )
            else
                self:addCellToListview(self.ListView_1 , _openNum - _maxRowNum , var )
            end
        end
    else
        -- <= 8
        self.img_line:setVisible(false)
        self.ListView_1:setVisible(false)

        self.root:setContentSize(display.width,262)
        self.img_bg:setContentSize(display.width,262)

        self.ListView_2:setPositionY(self.img_bg:getContentSize().height*0.2)

        for key, var in ipairs(_data) do
            self:addCellToListview(self.ListView_2 , _openNum , var )
        end
    end

end

function FunctionLayer:_showDialog()
	self:showDialog() 
end



function FunctionLayer:closeDialog()
    print("close a dialog which tag is: ", self.dialogTag)

    local function func() 
       
        local scene = self:getParent()
        if scene and scene._dialogList then
            for i = 1, #scene._dialogList do
                if scene._dialogList[i] == self then
                    table.remove(scene._dialogList, i)
                end
            end
        end
        -- removeMsgCallBack( self, MSG_CAPTURE_4_DIALOG )
        self:removeFromParent()

        for i,v in ipairs(GlobalViewBaseStack) do
            if v == self then
                table.remove(GlobalViewBaseStack,i)
                break
            end
        end
    end

    local _action = cc.Sequence:create( cc.MoveTo:create(0.1,cc.p(0, - self.root:getContentSize().height )) , cc.CallFunc:create(func))   

    self:runAction(_action)

end


function FunctionLayer:showEffect()
   
    self.root:setPositionY( - self.root:getContentSize().height )  
    local _action =   cc.MoveTo:create(0.1,cc.p(0,0))    
    self.root:runAction(_action)

end


function FunctionLayer:btnRegistClick(_btn,  _id )
    UIAdapter:registClickCallBack(_btn, function ()
        local data = fromFunction(_id)
        if data then
            local stackLayer = data.mobileFunction
            if stackLayer and string.len(stackLayer)  > 0  then
                sendMsg(MSG_GOTO_STACK_LAYER, {layer = stackLayer, name = data.name, funcId = _id})
                self:closeDialog()
            else
                TOAST("稍等,功能未开通,功能id:" .. _id )
            end
        end
    end)
	
end

function FunctionLayer:LayoutRegistClick(default_item,_btn,  _id )
    default_item:addTouchEventListener(function ( sender, eventType )
        if sender then
            if eventType == ccui.TouchEventType.began then
                _btn:setHighlighted(true)
            elseif eventType == ccui.TouchEventType.canceled then
                _btn:setHighlighted(false)
            elseif eventType == ccui.TouchEventType.ended then
                local data = fromFunction(_id)
                if data then
                    local stackLayer = data.mobileFunction

                    if stackLayer and string.len(stackLayer)  > 0  then
                        sendMsg(MSG_GOTO_STACK_LAYER, {layer = stackLayer, name = data.name, funcId = _id})
                        self:closeDialog()
                    else
                        TOAST("稍等,功能未开通,功能id:" .. _id )
                    end
                end

            end
        end
    end)
end

function FunctionLayer:addCellToListview(_listview , _num , _var )

    local default_item = ccui.Layout:create()
    default_item:setTouchEnabled(true)
    default_item:setContentSize(  _listview:getContentSize().width /_num,  _listview:getContentSize().height  )
    
   -- default_item:setColor(cc.c3b(200,55,55))
    --default_item:setOpacity(100) 
    
    

    local  _funcBtn = ccui.Button:create()

    _funcBtn.funId = _var.id

    if _var.iconName then
		print(_var.iconName)
        _funcBtn:loadTextures(_var.iconName, _var.iconName, _var.iconName, 1)
    end
    if getFuncOpenStatus(_var.id) == 2 then
        _funcBtn:setEnable(false)
    end
    _funcBtn:setPosition(cc.p(default_item:getContentSize().width / 2.0, default_item:getContentSize().height / 2.0))
    default_item:addChild(_funcBtn)

    self:btnRegistClick(_funcBtn,  _var.id )
    
    self:LayoutRegistClick(default_item, _funcBtn,  _var.id )
    
    _listview:addChild(default_item)
end



return FunctionLayer