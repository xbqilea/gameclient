--ThirdpartyPayExtInfor 



local ThirdpartyPayExtInfor = class("ThirdpartyPayExtInfor")


function ThirdpartyPayExtInfor:ctor(_itemId, _price, _goodsName )
 
    self:myInit(_itemId, _price, _goodsName )
end

function ThirdpartyPayExtInfor:myInit(_itemId, _price, _goodsName )
    self.itemId = _itemId
    self.price = _price
    self.goodsName = _goodsName

end

function ThirdpartyPayExtInfor:getExtPayInfor()

    local channel = GlobalConf.CHANNEL_ID

    local _infor = ""

    if GlobalConf.CHANNEL_ID == "1020009001" then
        --魅族
        _infor = self:makeMzPayExt()

    elseif GlobalConf.CHANNEL_ID == "1020010001"  then
        --应用汇
        _infor = self:makeYyhPayExt()

    end

    return _infor
end

function ThirdpartyPayExtInfor:makeMzPayExt()

    local infor = {}

    infor["uid"] = ToolKit:getThirdpartyUserID()
    infor["user_info"] = ""
    infor["pay_type"] = 0
    
    return infor

end

function ThirdpartyPayExtInfor:makeYyhPayExt()

    local _payExt = {}
    
    local cointab = {
        [3] = 9,
        [5] = 1,
        [10] = 2,
        [20] = 3,
        [30] = 4,
        [50] = 5,
        [100] = 6,
        [200] = 7,
        [500] = 8,  
    }
    
    local cardTab = {
        [3] = 10,
        [15] = 11,
        [12] = 12,
    }
    
    local _goodsId = 0
    if self.itemId == GlobalDefine.ITEM_ID.RoomCard then
     
         _goodsId =    cardTab[tonumber(self.price ) ]
    	
    elseif self.itemId == GlobalDefine.ITEM_ID.GoldCoin then
    
        _goodsId =    cointab[tonumber(self.price ) ]
    end
    
    if _goodsId == nil  then
    	print("ERROR:获取商品号失败！！！")
    	return
    end
    
    _payExt["waresid"] = _goodsId
    _payExt["currency"] = "RMB"
    _payExt["appuserid"] = Player:getAccountID() .. "#1"
    _payExt["waresname"] = tostring(self.goodsName)
    _payExt["price"] = tonumber(self.price)
    _payExt["cpprivateinfo"] = ""

    return _payExt

end

return  ThirdpartyPayExtInfor