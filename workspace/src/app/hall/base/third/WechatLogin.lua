--微信授权
-- Author: 
-- Date: 2018-07-27 11:42:15



local WechatAuthorize = class("WechatAuthorize")

local scheduler = require("framework.scheduler")


function WechatAuthorize:ctor(_callback)
    self._callback = _callback  
    self:getWxAuthorization() 
end


--获取微信授权code
function WechatAuthorize:getWxAuthorization()

    --请求微信授权
    print("******开始请求微信授权*******")
        
    g_wxUserInfor = {}
    
    -- '登陆类型 1-授权 2-本地token'
    g_wxUserInfor["wx_loginType"] = GlobalDefine.wxLoginType.authorize
    
    self:getWechatLoginCode(handler(self, self.getWXParam))
end

function WechatAuthorize:getWXParam(_info)
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaParams = {}
        local javaMethodSig = "()Ljava/lang/String;"
        local ok,ret1  = luaj.callStaticMethod(javaClassName, "getaccessToken", javaParams, javaMethodSig)
        if ok then
            print("token",ret1)
            g_wxUserInfor["token"] = ret1
        end
        local ok,ret2  = luaj.callStaticMethod(javaClassName, "getopenID", javaParams, javaMethodSig)
        if ok then
            print("openID",ret2)
            g_wxUserInfor["openid"] = ret2
        end
        local ok,ret3  = luaj.callStaticMethod(javaClassName, "getrefreshToken", javaParams, javaMethodSig)
        if ok then
            print("refresh_token",ret3)
            g_wxUserInfor["refresh_token"] = ret3
        end
        local ok,ret4  = luaj.callStaticMethod(javaClassName, "getnickName", javaParams, javaMethodSig)
        if ok then
            print("nickName",ret4)
            g_wxUserInfor["nickName"] = ret4
        end
        local ok,ret5  = luaj.callStaticMethod(javaClassName, "getheadimgurl", javaParams, javaMethodSig)
        if ok then
            print("headImgUrl",ret5)
            g_wxUserInfor["headImgUrl"] = ret5
        end
        self._callback(true)
    elseif device.platform == "ios" then
        dump(_info, "info")
        local info2 = require("framework.json").decode(_info)
        local weixinCode = info2["result"]
        local GetCodeRequest = string.format("https://api.weixin.qq.com/sns/oauth2/access_token?appid=wx3b5e1d7620570971&secret=6e7b9fb1f28380ef8aef2785851dc803&code="..weixinCode.."&grant_type=authorization_code")
        print(GetCodeRequest)

        local xhr1 = cc.XMLHttpRequest:new()
        xhr1.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
        xhr1:open("GET",GetCodeRequest)

        local function onGetOpenId()
            local response = xhr1.response
            if response == "" then
                print("response = nil")
            else
                local output = require("framework.json").decode(response,1)
                dump(output)
                accessToken = output["access_token"]
                openID = output["openid"]
                refreshToken = output["refresh_token"]
                g_wxUserInfor["token"] = accessToken
                g_wxUserInfor["openid"] = openID
                g_wxUserInfor["refresh_token"] = refreshToken

                local getUserRequest = string.format("https://api.weixin.qq.com/sns/userinfo?access_token="..accessToken.."&openid="..openID)
                local xhr2 = cc.XMLHttpRequest:new()
                xhr2.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
                xhr2:open("GET",getUserRequest)
                local function getUserInfo()
                    local response = xhr2.response
                    if response == "" then
                        print("response = nil")
                    else
                        local output = require("framework.json").decode(response,1)
                        dump(output)
                        local nickname = output["nickname"]
                        local headimgurl = output["headimgurl"]
                        g_wxUserInfor["nickName"] = nickname
                        g_wxUserInfor["headImgUrl"] = headimgurl
                        self._callback(true)
                    end
                end
                xhr2:registerScriptHandler(getUserInfo)
                xhr2:send()
            end
        end
        xhr1:registerScriptHandler(onGetOpenId)
        xhr1:send()
    end
end

-- 获取微信code， 获取后给网站然后获取其他信息
--通过回调返回登录结果，result  0-未安装微信，1-取消授权 2-授权出错  其他-code
function WechatAuthorize:getWechatLoginCode(_callback)
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/AppActivity"
        local javaMethodName = "LoginWX"
        local javaParams = { _callback}
        local javaMethodSig = "(I)V"
        local ok,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok then
            print("安卓登录授权")
        end
    elseif device.platform == "ios" then
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "PhoneInfo"
        local num1 = "num1"
        local args = {num1 = _callback}
        local ok ,ret = luaoc.callStaticMethod(className,"wechatLogin",args)

    else --测试用
        _callback( "{\"result\":\"0\",\"openid\":\"0\"}" )
    end

end

--获取微信授权code 
--0 没有安装微信
--1 取消授权
--2 授权出错
function WechatAuthorize:getWxCode( _infor )
    print("__infor: ", _infor)
    local _data =  fromJson( _infor )
    dump(_data)
    local resultCode = _data.result

    if resultCode and type(resultCode) == "string" then
        -- 获得code
        if resultCode == "0" or resultCode == "1" or resultCode == "2"   then
            --没有安装微信

            self:getWxAuthorizeFail( resultCode )

        else
            -- 开始 请求access_token openid  refresh_token,
       
            local function onAskServer()
                ToolKit:addLoadingDialog(30, "正在连接服务器...")    

                local HttpUtil = require("app.hall.base.network.HttpWrap")
                local client = HttpUtil.new(HttpUrl.GET_ACCESS_TOKEN)  
                local _authType = ToolKit:getSdkType()
--                if device.platform == "ios" then
--                    _authType = 1
--                end
                
                local params = {authType = tonumber(_authType), authInfo =  { code = resultCode }, channelKey = GlobalConf.CHANNEL_ID,    }  
                
                print("***********打印params************")
                dump(params)

                --请求服务器获取 openid acces_token 等
                client:post(params, handler(self, self.httpCallback_access_token), false)  
               
            end
            
            scheduler.performWithDelayGlobal(onAskServer, 1)
            
        end
    else
        -- 未知错误
        print("******请求code发生未知错误*******")
        self:getWxAuthorizeFail( "2" )
    end
end

--获取微信信息失败
function WechatAuthorize:getWxAuthorizeFail( _code )
    if _code == "0" then
        ToolKit:delayDoSomething( function()
            TOAST("您的手机没有安装微信，请下载安装后重试！")
        end  , 0.1)
        
        if self._callback then
            self._callback(false)
        end
    elseif _code == "1" then
        print("******取消授权*******")
        ToolKit:delayDoSomething( function()
            TOAST("微信授权已取消！")
        end  , 0.1)
        if self._callback then
            self._callback(false)
        end
    elseif _code == "2"   then
       
        print("******授权出错*******")
        ToolKit:delayDoSomething( function()
            TOAST("微信授权出错！")
        end  , 0.1)
        if self._callback then
            self._callback(false)
        end
        
    elseif _code == "3"   then
        print("******连接网站失败*******")
        
        ToolKit:delayDoSomething( function()
            TOAST("连接服务器失败，请稍后重试！")
        end  , 0.1)
        if self._callback then
            self._callback(false)
        end
    end

end


function WechatAuthorize:httpCallback_access_token( __tag, event   )
    if event.name == "completed" then
    
        ToolKit:removeLoadingDialog()
        local code = event.request:getResponseStatusCode()
        if code ~= 200 then
            print("failed with code: " .. code)
            self:getWxAuthorizeFail( "3" )
            return 
        end
        
        local response =  event.request:getResponseString() 
        
        if response then
            local info = fromJson(response)
            dump(info)
            if info ~= nil then
                if tonumber(info.code) == 0 then
                    --返回成功
                    print("******请求token成功*******")
                    if info.authRet then
                        print("************打印返回的token信息*************")
                        dump(info.authRet)
                        -- access_token  openid  refresh_token 
                        
                        local access_token  = info.authRet.access_token
                        local openid  = info.authRet.openid
                        
                        if  access_token == nil or access_token == "" or openid == nil or openid == ""  then
                            print("获取access_token失败")
                            
                            self:getWxAuthorizeFail( "2" )   
                                            
                            return
                        end


                        print("**************token：" .. info.authRet.access_token .. "******************")
                        print("**************openid：" ..info.authRet.openid .. "******************")

                        g_wxUserInfor["openid"] = openid
                        g_wxUserInfor["token"] = access_token

                        local refresh_token = info.authRet.refresh_token
						
						print("************服务器返回的token***************")
						print(refresh_token)
												
						g_wxUserInfor["refresh_token"] = refresh_token

                        --请求服务器获取玩家信息
                        local _authType = ToolKit:getSdkType() 
--                        if device.platform == "ios" then
--                            _authType = 1
--                        end
                        self:getUserInfor( tonumber(_authType), openid, access_token, ""  ) 

                    else
                        print("获取info.authRet失败")
                        self:getWxAuthorizeFail( "2" )   
                    end

                else
                    --失败
                    self:getWxAuthorizeFail( "2" )   
                    print("获取信息失败，请稍后重试！错误id：" .. info.code)
                end
            else
                self:getWxAuthorizeFail( "2" ) 
                print("服务器返回信息失败，请稍后重试！")
            end
        end
    elseif event.name == "progress" then
  
    else
        ToolKit:removeLoadingDialog()
        print("failed!!! 超时，请求token超时")      
        self:getWxAuthorizeFail( "3" )
    end
end

--获取玩家信息
function WechatAuthorize:getUserInfor( _authType, _authId, _authToken, _extData  )

    ToolKit:addLoadingDialog(30, "正在请求数据...")   

    local HttpUtil = require("app.hall.base.network.HttpWrap")
    local client = HttpUtil.new(HttpUrl.GET_USER_INFOR) 
    local params = {authType = _authType, authID = _authId , authToken = _authToken , channelKey = GlobalConf.CHANNEL_ID,  extData = _extData   }  

    client:post(params, handler(self, self.httpCallback_userInfor), false)   
end

--获取玩家信息回调
function WechatAuthorize:httpCallback_userInfor( __tag, event   )

    if event.name == "completed" then
        ToolKit:removeLoadingDialog()
        local code = event.request:getResponseStatusCode()
        if code ~= 200 then
            print("****连接失败: " .. code)
            self:getWxAuthorizeFail( "3" )
            return 
        end
        local response =  event.request:getResponseString() 
        if response then
            local info = fromJson(response)
            dump(info)
            if info ~= nil then
                if tonumber(info.code) == 0 then
                    print("*************请求玩家信息成功******************")
                    --返回成功
                    
                    if info.nickName then
                        g_wxUserInfor["nickName"] = info.nickName
                    end

                    if info.headImgUrl then
                        g_wxUserInfor["headImgUrl"] = info.headImgUrl     
                    end

                    if info.extData then 
                        g_wxUserInfor["extData"] = info.extData
                    end

                    print("**************昵称：" ..info.nickName .. "******************")
                    print("**************头像地址：" ..info.headImgUrl .. "******************")

                    --开始登录服务器
                    print("***********获取信息完毕，开始登录***************")
                                       
                    if self._callback then
                        self._callback(true)
                    end
                    
                elseif  tonumber(info.code) == 500 then
                   
                    self:getWxAuthorizeFail( "2" )   
                    --鉴权失败
                    
                else
                    self:getWxAuthorizeFail( "2" )   
                   
                end
            else
                print("**************获取玩家信息失败******************")
                self:getWxAuthorizeFail( "3" )
             
            
            end
        end
    elseif event.name == "progress" then

    else
        ToolKit:removeLoadingDialog()
 
        print("failed!!! 超时,请求玩家信息超时")
        self:getWxAuthorizeFail( "3" )

    end

end

return  WechatAuthorize