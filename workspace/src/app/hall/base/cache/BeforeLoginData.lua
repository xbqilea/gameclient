--
-- Author: 
-- Date: 2018-07-27 11:42:15
--
-- 玩家的基本信息



--local OldPlateLoginInfor = {} --全局对象

local OldPlateLoginInfor = class("OldPlateLoginInfor")

local GameState = require("app.hall.base.cache.BeforeGameData") --本地保存用户数据


OldPlateLoginInfor.GameSaveData_={
    --[[ 目前，保存一下变量
        soundState  -- 音乐开关
        accountList = {{account = "qka23",passWord = "sdasdasds",loginType = 1,register = true},{}}
        lastAccount = ""
        lastPassWord = ""  -- md5password
        lastRegisterAccount    -- 注册新帐号 （转正，切换）
        firstLogin -- 初次登陆
        logonType  -- 正式号， 体验号 0 ，1 ，2，3
        updateGameList = {{kindId = 1007,fileName = "src/app/landGame",size = 10000,gameName="斗地主"},{}}
        danTiGameKind = {}
        appStoreReceiptDataList -- 苹果充值订单
    ]]
} -- 本地保存对象
-- 保存的文件名
OldPlateLoginInfor.GAME_STATE_FILENAME = "gameState"
OldPlateLoginInfor.GAME_STATE_ENCRYPTION_XXTEA_KEY =  "abcd"  -- 加密密钥
OldPlateLoginInfor.GAME_STATE_SECREY_KEY =  "xiaoYanMa"  --校验码
OldPlateLoginInfor.GAME_STATE_ENCRYPTION_OPERATION_SAVE="save" --保存
OldPlateLoginInfor.GAME_STATE_ENCRYPTION_OPERATION_LOAD="load" --加载
-- 保存end


function OldPlateLoginInfor:ctor()

    self:init()

end

function OldPlateLoginInfor:init()
    self:initGameState()
    -- test
    OldPlateLoginInfor.GameSaveData_ = GameState.load()

    print("GameSaveData.account GameSaveData.logonType",OldPlateLoginInfor.GameSaveData_.account,OldPlateLoginInfor.GameSaveData_.logonType)
    -- end
end

--初始化本地数据
function OldPlateLoginInfor:initGameState()
    GameState.init(function(param)
        local returnValue=nil
        if param.errorCode then
            printError("errorCode",param.errorCode)
        else
            if param.name==OldPlateLoginInfor.GAME_STATE_ENCRYPTION_OPERATION_SAVE then --保存
                local str=json.encode(param.values)
                
                str=crypto.encryptXXTEA(str,OldPlateLoginInfor.GAME_STATE_ENCRYPTION_XXTEA_KEY)
                returnValue={data=str}
            elseif param.name==OldPlateLoginInfor.GAME_STATE_ENCRYPTION_OPERATION_LOAD then --加载
            
                local str=crypto.decryptXXTEA(param.values.data,OldPlateLoginInfor.GAME_STATE_ENCRYPTION_XXTEA_KEY)
                returnValue=json.decode(str)
            end  
        end
        return returnValue
    end,OldPlateLoginInfor.GAME_STATE_FILENAME,OldPlateLoginInfor.GAME_STATE_SECREY_KEY)
    
    if io.exists(GameState.getGameStatePath()) then
        OldPlateLoginInfor.GameSaveData_=GameState.load()
        print("savePath:"..GameState.getGameStatePath()) 
    end
end


function OldPlateLoginInfor:getLastAccountAndLastPassWord()
    if OldPlateLoginInfor.GameSaveData_.lastAccount == "" then
        print("************获取最近的登陆账号失败*************")
        return
    end

    if OldPlateLoginInfor.GameSaveData_.lastPassWord == "" then
        -- 生成随机密码 3pwu7j11t0qs 3cwazga52lfv
        print("************获取最近的登陆密码失败*************")
        return
    end
    
    print("************最近登陆的账号*************")
    print(OldPlateLoginInfor.GameSaveData_.lastAccount)
    
    print("************最近登陆的密码*************")
    print(OldPlateLoginInfor.GameSaveData_.lastPassWord)

   return OldPlateLoginInfor.GameSaveData_.lastAccount,OldPlateLoginInfor.GameSaveData_.lastPassWord

end



return OldPlateLoginInfor