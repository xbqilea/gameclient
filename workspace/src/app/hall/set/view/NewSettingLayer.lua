--
-- User: lhj
-- Date: 2018/7/9 15:30
-- 大厅设置

local XbDialog = require("app.hall.base.ui.CommonView")
local NewSettingLayer = class("NewSettingLayer", function ()
    return XbDialog.new()
end)

function NewSettingLayer:ctor()
    self:myInit()
    self:setupViews()
end

-------  初始化开关数据 ----------
-- music_type 音乐
-- sound_type 音效
-- vib_type 震动
-- voice_type 语音(语音暂时没效果)
function NewSettingLayer:myInit()
    self.music_type = g_GameMusicUtil:getMusicStatus()
    self.sound_type = g_GameMusicUtil:getSoundStatus()
    self.vib_type = g_GameSwitchUtil:getVibrateStatus()
    self.voice_type = g_GameSwitchUtil:getVoiceStatus()

    if self.music_type == false then
        self.music_type = ccui.CheckBoxEventType.unselected
    else
        self.music_type = ccui.CheckBoxEventType.selected
    end

    if self.sound_type == false then
        self.sound_type = ccui.CheckBoxEventType.unselected
    else
        self.sound_type = ccui.CheckBoxEventType.selected
    end
end

function NewSettingLayer:setupViews()
    self.node = UIAdapter:createNode("csb/setting/setting_layer.csb"):addTo(self)
    UIAdapter:adapter(self.node, handler(self, self.onTouchCallback))
    --self:initAgreementBtn()
    self:updateViews()

    --self:showVersion()
end

-- 显示版本信息
function NewSettingLayer:showVersion()
    -- 显示版本号
    -- package.loaded["app.hall.idnum"] = nil
    -- local v = require("app.hall.idnum")
    -- local version = "1." .. v.version .. "." .. v.sub
    -- self.node:getChildByName("txt_version"):setString("版本号: " .. version)

    -- local versionUpdName = cc.FileUtils:getInstance():getWritablePath() .. UpdateConfig.updateDirName .. "src/app/hall/version.luac.upd"
    -- -- dump(versionUpdName)
    -- if cc.FileUtils:getInstance():isFileExist(versionUpdName) then
    --     UIAdapter:bindingLabelBtn(self.node:getChildByName("txt_version"), function ()
    --         local v = UpdateFunctions.doFile(versionUpdName)
    --         local version = "1." .. v.version .. "." .. v.sub
    --         TOAST("已离线更新到"  .. version .. ", 重启生效!")
    --     end)
    -- end
end

function NewSettingLayer:onEnter()
--    TOAST("------onEnter--------" )
end

----------- 更新开关状态 -------------
function NewSettingLayer:updateViews()
    self:updateBtnViews("btn_music", "img_music", self.music_type)
    self:updateBtnViews("btn_sound", "img_sound", self.sound_type)
    self:updateBtnViews("btn_vib", "img_vib", self.vib_type)
    self:updateBtnViews("btn_voice", "img_voice", self.voice_type)

    self:updateViewForIpad()
end

--- Ipad屏蔽震动
function NewSettingLayer:updateViewForIpad()
    local func = function (name, bool)
        self.node:getChildByName(name):setVisible(bool)
    end
    local boo = (device.platform == "ios" and device.model == "ipad")
    func("text4", not boo);
    func("btn_vib", not boo);
    func("img_vib", not boo);
    local y1 = 180
    local y2 = 182
    if boo == true then
        y1 = 230
        y2 = 232
    end
    self.node:getChildByName("text6"):setPositionY(y1)
    self.node:getChildByName("img_voice"):setPositionY(y2)
    self.node:getChildByName("btn_voice"):setPositionY(y2)
end

function NewSettingLayer:updateBtnViews(btn, bg, type)
    local _btn = self.node:getChildByName(btn)
    local _bg = self.node:getChildByName(bg)
    local children = _btn:getChildren()
    local _open = children[1];
    local _close = children[2];
    local bg_name = "settings_di_1.png";
    _open:setVisible(type == ccui.CheckBoxEventType.selected);
    _close:setVisible(type == ccui.CheckBoxEventType.unselected);
    local _x = 50
    if type == ccui.CheckBoxEventType.selected then
        _x = -50
        bg_name = "settings_di_2.png";
    end
    _btn:loadTextures(bg_name, bg_name, bg_name, 1)
    _bg:setPositionX(_btn:getPositionX() + _x);
end

---------- 重写按钮触碰事件 -----------
function NewSettingLayer:initAgreementBtn()
    local btn = self.node:getChildByName("text_agr")
    UIAdapter:bindingLabelBtn(btn, handler(self, self.agreementBtnCallback))
end

function NewSettingLayer:agreementBtnCallback()
    ShowUserAgreement(self:getParent())
    self:closeDialog()
end

----------- 获取变化后开关的状态 -------------
function NewSettingLayer:getExBtnType(_type)
    if _type == ccui.CheckBoxEventType.selected then
        return ccui.CheckBoxEventType.unselected
    end
    return ccui.CheckBoxEventType.selected
end

----------- 按钮回调 -------------
function NewSettingLayer:onTouchCallback( sender )
    local name = sender:getName()

    if name == "btn_close" then
        self:closeDialog()
    else
        if name == "btn_music" then
            self.music_type = self:getExBtnType(self.music_type)
            local function de()
                g_GameMusicUtil:setMusicStatus(self.music_type)
                g_GameMusicUtil:setMusicVolume(1 - self.music_type)
                self.musicAction = nil
            end
            if self.musicAction then
                self:stopAction(self.musicAction)
            end
            self.musicAction = self:performWithDelay(de, 0.1)
        elseif name == "btn_sound" then
            self.sound_type = self:getExBtnType(self.sound_type)
            g_GameMusicUtil:setSoundStatus(self.sound_type)
            g_GameMusicUtil:setSoundsVolume(1 - self.sound_type)
        elseif name == "btn_vib" then
            self.vib_type = self:getExBtnType(self.vib_type)
            g_GameSwitchUtil:setVibrateStatus(self.vib_type)
        elseif name == "btn_voice" then
            self.voice_type = self:getExBtnType(self.voice_type)
            g_GameSwitchUtil:setVoiceStatus(self.voice_type)
        end
        self:updateViews()
    end
end

return NewSettingLayer