--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 选择的头像

--使用示例
--   local UserCenterHeadIcon = require("app.hall.userinfo.view.UserHeadIcon")
--   local img_head = UserCenterHeadIcon.new(  {_size =_sizeT, _id = _iconid ,  _accountID = accid,   _clip = true }   ) 
--    @param _size(table)     :  你期望的头像的大小
--    @param _id(int)         :  头像id
--    @param _accountID(int)  :  账号  --  如果需要其他玩家的头像则要传入其他玩家的账号，并且把该玩家的头像id传过来
--    @param _clip(bool)      :  是否进行裁剪，如果传入true，则自动进行裁剪，裁剪成圆形，大小按照传入的_size参数
--   获得一个  Node 对象， 对象包含一个 ccui.ImageView子节点， 如果想要的是玩家目前使用的头像，则不需要传入 _id这个参数， _size 为你期望的这个头像的尺寸， 如果不传这个参数，则不对返回的头像做缩放处理

--   注意！！ ：现在系统头像只提供20个 ，id为1-20

 
local writePath =  cc.FileUtils:getInstance():getWritablePath()


local UserCenterHeadIcon = class("UserCenterHeadIcon",function()
    return display.newNode()
end)

function UserCenterHeadIcon:ctor( _data  )
    self:myInit(_data)

    self:setupViews()
end

function UserCenterHeadIcon:myInit(_data)

    -- 注册析构函数
	print("**********头像myInit**********")
	--dump(_data)
    ToolKit:registDistructor( self, handler(self, self.onDestory) )

    self._showhead_img_head = nil      --头像
    self._headImgId = nil              --头像图片id
    self._headImgName = nil            --头像图片名字                  
    self._needSize = nil               --设置的头像大小
    
    
    if _data and _data._id  then
        self._headImgId = _data._id
        print("******头像初始化，头像id:" .. _data._id .. "*********") 
    else
        self._headImgId = Player:getFaceID()   
        print("******头像初始化，没有头像id *********") 
    end
    if self._headImgId == 21 then
        
	elseif self._headImgId > 20 then
		self._headImgId = 20
	elseif self._headImgId < 1 then
		self._headImgId = 1
	end
   
    if _data and _data._accountID and _data._id  then
        print("******玩家id:" .. _data._accountID .. "*********") 
        
        self.accountID =  _data._accountID  
    else
        self.accountID = Player:getAccountID()
    end
    
    if _data and _data._size then
        self._needSize = _data._size   
    end               
    
    if _data and _data._clip and  _data._clip == true  then
        self._clip = true 
    end 
    
end

function UserCenterHeadIcon:setupViews ()

    --先生成一个系统头像
    self._showhead_img_head =  ccui.ImageView:create("res/platform/userData/res/head/icon_0.png",0)
    self._showhead_img_head:setAnchorPoint(cc.p(0.5,0.5))

    self._showhead_img_head:setTouchEnabled(true)
    --self._showhead_img_head:setSwallowTouches(true)
    
    self._showhead_img_head:setVisible(false)

    UIAdapter:registClickCallBack( self._showhead_img_head, handler(self,self.onTouchCallback) )

    self:addChild(self._showhead_img_head)
    
    self:checkHeadIconID()
end

--检查头像id
function UserCenterHeadIcon:checkHeadIconID () 
        
      self._headImgName = ToolKit:getHead(self._headImgId) 
      self:img_headloadTexture()
end

function UserCenterHeadIcon:onTouchCallback (sender)
    sendMsg(MSG_HEADICON_CLICKED,   { id = self._headImgId,  btn = sender   })
end

-- 检查是否是AI的 id 
function UserCenterHeadIcon:checkAIuserid(_userId)

    if not _userId   then
        print("没有输入uid")
        return
    end

    if tonumber(_userId ) >= GlobalDefine.AI_User_id[1]  and tonumber(_userId ) <= GlobalDefine.AI_User_id[2]  then
        return true
    end

    return false
   
end

--    @param headId(int)            :  头像id
--    @param accountId(int)         :  账号id
function UserCenterHeadIcon:updateTexture(headId , accountId  )
	
   
   if headId then
        print("*************** 刷新头像 headId= " .. headId .. "*********************")
   else
        print("*************** 刷新头像  没有发送headId *********************")
   end
   
    if accountId then
        print("*************** 刷新头像 accountId= " .. accountId .. "*********************")
    else
        print("*************** 刷新头像  没有发送accountId *********************")
    end
    
   
    self._showhead_img_head:setVisible(false)
    
    self._headImgId = headId or Player:getFaceID()  
    
    self.accountID = accountId or Player:getAccountID()

    self:checkHeadIconID()
           
end


function UserCenterHeadIcon:win32NetSpriteCallBack(result, fileName )
   
    if (not tolua.isnull(self))  then
        if result then
            print("**********NetSprite 下载成功**********")
            print(fileName)
            self._headImgName = fileName
            self:img_headloadTexture()
        else
            print("**********NetSprite 下载失败**********")
            print(fileName)
            self._headImgId  = 1
            self._headImgName = g_SystemAvatarData[1].pngFileName
            self:img_headloadTexture()
           -- self._showhead_img_head:setVisible(true)
        end
    end
   
end

function UserCenterHeadIcon:downloadHead()

--    local headPath = writePath .. self.accountID .. "/" .. self._headImgId .. ".png"
--    print("判断图片是否存在 headPath = ",headPath)
--    -- dump(headPath)
--
--    --判断图片是否存在，存在则不去下载
--    if UpdateFunctions.isFileExist(headPath) then
--        print("存在")
--        -- self._txt_reviewing:setString("" .. self._showHeadState.headId)
--        self._headImgName = headPath
--        self:img_headloadTexture()   
--        return
--    end
--
--    -- 下载图片
--    UpdateFunctions.mkDir(writePath ..  self.accountID .."/".. self._headImgId) -- 创建文件夹
    
    if true then--device.platform == "windows" then -- windows平台, 用NetSprite下载
        local NetSprite = require("app.hall.base.ui.UrlImage")
        -- print("self._showHeadState.headId = ",self._showHeadState.headId)
        
--        local url = "" 
--        -- "http://image.milaichess.com/" .. AliYunUtil.headPath .. self.accountID .. "/" .. self._headImgId .. ".png"

--        if self:checkAIuserid(self.accountID) then
--        	-- ai 
--            url =  "http://image.milaichess.com/newPlatform/image/head/AI/" .. self._headImgId .. ".png"
--        else
--            url = "http://image.milaichess.com/" .. AliYunUtil.headPath .. self.accountID .. "/" .. self._headImgId .. ".jpg"
--        end

--        url = url .. "?x-oss-process=image/format,jpg"
--        print("**************NetSprite下载，url: ", url)

--        local imgSuffix = ".jpg"
--        if self:checkAIuserid(self.accountID) then
--            imgSuffix = ".png"
--        end 
        
         local url =  ToolKit:getHead(self._headImgId)
        local picPath

        if device.platform == "android" then
            picPath = device.writablePath.."../cache/netSprite/".. crypto.md5(url) .. imgSuffix
        else
            picPath = device.writablePath.."netSprite/" .. crypto.md5(url) .. imgSuffix
        end
                  
        print("**********头像保存路径**********")
        print(picPath)
                
                
              --  cc.FileUtils:isFileExist(filename)
        if UpdateFunctions.isFileExist(picPath)  then      
            print("******NetSprite,头像已存在，" .. picPath .. "*******")
            self._headImgName = picPath
            self:img_headloadTexture()   
            return
        else
            print("******NetSprite,头像不存在,开始下载*******", url)

            if self:checkAIuserid(self.accountID )then
                --ai
                local netSprite = NetSprite.new(url, true, handler(self, self.win32NetSpriteCallBack), false)
                netSprite:addTo(self):setVisible(false)
            else
                --todo
                local netSprite = NetSprite.new(url, true, handler(self, self.win32NetSpriteCallBack), true)
                 netSprite:addTo(self):setVisible(false)
            end
            
            return
        end
        
        
    end

--    if not AliYunUtil:getAliyunState(false) then  -- 更新状态失败，使用系统头像
--        print("**********阿里云状态更新失败*************") 
--        
--        self._headImgId  = 1
--        self._headImgName = g_SystemAvatarData[1].pngFileName
--        -- self._txt_reviewing:setString("" .. self._showHeadState.headId)
--        self:img_headloadTexture()
--        print("getAliyunState(false)")
--        return
--    end
--    
--    print("beginDownload2")
--    
--    local _alyUrl = ""
--    if self:checkAIuserid(self.accountID) then
--        _alyUrl =  "newPlatform/image/head/AI/" .. self._headImgId .. ".png"
--    else
--        _alyUrl = AliYunUtil.headPath .. self.accountID .. "/" .. self._headImgId .. ".png"
--    end
--    
--    print("从阿里云下载的地址")
--    print(_alyUrl)
--    
--    
--    AliYunUtil:downloadFile(_alyUrl, headPath, function ( result )
--        dump(result,"beginDownload2")
--        -- dump(cc.FileUtils:getInstance():isFileExist(headPath))
--        if tonumber(result) == 1 then -- 成功
--           -- TOAST("从阿里云下载成功")
--            print("下载成功headPath = ",headPath)
--            self._headImgName = headPath
--                               
--        else  -- 下载失败
--           -- TOAST("从阿里云下载失败")
--            print("下载失败headPath = ",headPath)
--            
--            self._headImgId  = 1
--            self._headImgName = g_SystemAvatarData[1].pngFileName
--        end
--        if device.platform == "ios" then
--            self:performWithDelay(function ()
--               self:img_headloadTexture()    
--            end, 0.1)
--        else
--            self:img_headloadTexture()    
--        end
--    end)
end



function UserCenterHeadIcon:getHeadImg()
    return self._showhead_img_head
end

function UserCenterHeadIcon:getHeadBgSize()
    return self._showhead_img_head:getContentSize()
end

--将头像变灰或者恢复正常
-- true - 变灰     false - 恢复正常
function UserCenterHeadIcon:setHeadGray( _isGray)
    self._showhead_img_head:setGray(_isGray)
end

-- 设置头像颜色  颜色格式 cc.c3b(byte,byte,byte)
function UserCenterHeadIcon:setHeadColor( _color )
    if _color and type(_color) == "table" then
        self._showhead_img_head:setColor(_color)
    else
       print("颜色格式错误")
    end
    
end

--头像加载纹理
function UserCenterHeadIcon:img_headloadTexture()

   
        self._showhead_img_head:loadTexture(self._headImgName, 1 )  
    if self._needSize then
        local _size =  self._showhead_img_head:getContentSize()
        self._showhead_img_head:setScale(self._needSize.width/_size.width)
    end
     
    if self._clip and self._needSize then
    
        local ClipperRound = require("app.hall.base.ui.HeadIconClipperArea")
          
        --重新加载模板精灵的图片
        cc.SpriteFrameCache:getInstance():addSpriteFrames("ui/p_lobby.plist", "ui/p_lobby.png")
        --模板精灵
        local _StencilSpr =  cc.Sprite:createWithSpriteFrameName("dt_tx.png")
        local _size =  _StencilSpr:getContentSize()
        
        _StencilSpr:setScale(self._needSize.width/_size.width)
        
        self._showhead_img_head:removeFromParent()
        
        self._showhead_img_head = ccui.ImageView:create()
         
            self._showhead_img_head:loadTexture(self._headImgName, 1)   
        local _size =  self._showhead_img_head:getContentSize()
        self._showhead_img_head:setScale(self._needSize.width/_size.width)

        local _clipNode  = ClipperRound:clipperHead(_StencilSpr,  self._showhead_img_head)
        self:addChild(_clipNode)           	
     end  	
       
    self._showhead_img_head:setVisible(true)
end

function UserCenterHeadIcon:onDestory()
    print("UserCenterHeadIcon:onDestory")  
end

return UserCenterHeadIcon
