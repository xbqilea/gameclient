--
-- User: lhj
-- Date: 2018/8/17 
-- 返利控制类

local RebateController = class("RebateController")

--推广返利信息
RebateDatas = RebateDatas or {}
RebateBalanceDatas = RebateBalanceDatas or {}
RebateDetailDatas = RebateDetailDatas or {}
RebateRecordDatas = RebateRecordDatas  or {}
RebateWeekRankDatas = RebateWeekRankDatas  or {}
function RebateController:ctor()
	self:myInit()
    self.m_totalNum= 0
end

function RebateController:myInit()
	-- 注册网络监听
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GetRebateInfo_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_RebateBalance_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GetRebateDetail_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_LoadRebateBalHist_Ack", handler(self, self.netMsgHandler))
     TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GetRebateWeekRank_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_DayShare_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_ShowDayShare_Ack", handler(self, self.netMsgHandler))
end

function RebateController:netMsgHandler(__idStr, __info)
	dump(__info,__idStr,6)
	if __idStr == "CS_H2C_GetRebateInfo_Ack" then
		RebateDatas = __info
		sendMsg(MSG_REBATE_DATA_ASK) 
	elseif __idStr == "CS_H2C_RebateBalance_Ack" then
		RebateBalanceDatas = __info
		sendMsg(MSG_REBATE_BALANCE_DATA_ASK)
	elseif __idStr == "CS_H2C_GetRebateDetail_Ack" then
--		for i=1, #__info.m_rebateDetails do
--			RebateDetailDatas[#RebateDetailDatas + 1] = __info.m_rebateDetails[i]
--		end  
        RebateDetailDatas = __info.m_rebateDetails
        self.m_totalNum = __info.m_totalNum
		sendMsg(MSG_REBATE_DETAIL_ASK)
	elseif __idStr == "CS_H2C_LoadRebateBalHist_Ack" then
		RebateRecordDatas = __info.m_vHistory
		sendMsg(MSG_REBATE_RECORDE_ASK)
    elseif __idStr == "CS_H2C_GetRebateWeekRank_Ack" then
        RebateWeekRankDatas = __info.m_rankList
        sendMsg(MSG_REBATE_WEEKRANK_ASK)
    elseif __idStr == "CS_H2C_DayShare_Ack" then
        RebateDayShareDatas = __info.m_result
        sendMsg(MSG_REBATE_DAY_SHARE_ASK)
    elseif __idStr == "CS_H2C_ShowDayShare_Ack" then 
        self.m_dayAward = __info.m_dayAward 
        if __info.m_result == -1242 then
            TOAST("每日分享功能暂未开放")
            return
        elseif __info.m_result == -1243 then
            TOAST("今日分享次数已超过有奖励最大次数")
            return
         elseif __info.m_result == -1244 then
            TOAST("本次分享无奖励,继续分享有累计奖励哦")
            return
        end
        
       
        sendMsg(MSG_REBATE_DAY_SHAREGOLD_ASK,__info.m_dayAward)
	end
end
function RebateController:getTotalPage()
   return self.m_totalNum%8+1
end
--请求推广返利
function RebateController:reqRebateWeekRankInfo()
   ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetRebateWeekRank_Req")
end
function RebateController:reqRebateInfo()
   ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetRebateInfo_Req")
end
--请求推广返利结算
function RebateController:reqRebateBalanceInfo()
   ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_RebateBalance_Req")
end

--请求推广返利详情
function RebateController:reqRebateDetailInfo(_page)
   ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetRebateDetail_Req", {_page})
end

--请求推广奖励记录
function RebateController:reqRebateRecordInfo()
	print("1111111111111111111111111111")
	ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_LoadRebateBalHist_Req")
end

--获取推广返利
function RebateController:getRebateDatas()
	return RebateDatas
end

--获取推广返利结算
function RebateController:getRebateBalanceDatas()
	return RebateBalanceDatas
end

--获取推广返利详情
function RebateController:getRebateDetailDatas()
	return RebateDetailDatas
end

--获取推广奖励记录
function RebateController:getRebateRecordDatas()
	return RebateRecordDatas
end

function RebateController:getRebateWeekRankDatas()
	return RebateWeekRankDatas
end

--获取推广分享结果
function RebateController:getRebateDayShareDatas()
	return RebateDayShareDatas
end

--清除推广返利
function RebateController:removeRebateDatas()
	RebateDatas = {}
end

--清除推广返利结算
function RebateController:removeRebateBalanceDatas()
	RebateBalanceDatas = {}
end

--清除推广返利详情
function RebateController:removeRebateDetailDatas()
	RebateDetailDatas = {}
end

--清楚推广奖励记录
function RebateController:removeRebateRecordDatas()
	RebateRecordDatas = {}
end
function RebateController:removeRebateWeekRankDatas()
	RebateWeekRankDatas = {}
end

function RebateController:onDestory()
	print("RebateController:onDestory")
	TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GetRebateInfo_Ack")
	TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_RebateBalance_Ack")
	TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GetRebateDetail_Ack")
	TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_LoadRebateBalHist_Ack")
    TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_DayShare_Ack")
end

return RebateController