--
-- Author: lhj
-- Date: 2018-11-5 
--


local NoticeCell = class("NoticeCell", function() return display.newNode() end)

function NoticeCell:ctor(params)
	self.m_Data =params.data
	self.m_Layer = params.layer
	self:myInit()
	self:setupViews()
	self:showContent()
end

function NoticeCell:myInit()
end

function NoticeCell:setupViews()
	self.node = UIAdapter:createNode("csb/mail_notice/layer_notice_cell.csb"):addTo(self)
	UIAdapter:adapter(self.node, handler(self, self.onTouchCallback))
	self.bg = self.node:getChildByName("bg")

	ToolKit:listenerNodeTouch(self, self.bg, handler(self, self.onTouchBack))

	self.bg:setSwallowTouches(false)
	self.img_spr = self.node:getChildByName("img_spr")
	self.name = self.node:getChildByName("name")
end

function NoticeCell:showContent()
	self.name:setString(self.m_Data:getTitle())
end

function NoticeCell:refreshContent(_isSelect)
	if _isSelect then
		self.img_spr:setSpriteFrame(display.newSpriteFrame("sc_button_yellow.png"))
	else
		self.img_spr:setSpriteFrame(display.newSpriteFrame("sc_button_blue.png"))
	end
end

function NoticeCell:onTouchCallback(sender)
	
end

function NoticeCell:onTouchBack(sender, eventType)
	self.m_Layer:updateContent(self:getTag())
end

return NoticeCell