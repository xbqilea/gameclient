--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 以textlabelTTF为基础的ColorLabel
--

local ColorLabel = class("ColorLabel", function()
	local label = display.newTTFLabel(
	   {text = "",font = "黑体",size=20}
	)
	return label
end)

local DefaultColor = cc.c3b(255, 255, 255)
local AndroidEnWidth = 0.5
local IOSEnWidth = 0.5625

--功能：统计字符串中字符的个数
--返回：总字符个数、英文字符数、中文字符数
local function getUtf8StringCount(str)
    local tmpStr=str
    local _,sum=string.gsub(str,"[^\128-\193]","")
    local _,countEn=string.gsub(tmpStr,"[%z\1-\127]","")
    return sum,countEn,sum-countEn
end

function ColorLabel:ctor(_text,_width,_height,_font,_fontSize,_defaultColor,_lineNum,_forceDefaultColor)
	self.text = _text or ""
	local sum,en = getUtf8StringCount(self.text)
	self.fontSize = _fontSize or 20
	self.width = _width or display.width
	self.height = _height or 1
	self:setTouchEnabled(false)
	self.font = _font or "黑体"
	self.defaultColor = _defaultColor or DefaultColor
	self.lineNum = _lineNum or 1
	self.forceDefaultColor = _forceDefaultColor
	self.maxI = 0

	self:setDimensions(self.width,self.height)
	self:myInit()
end

function ColorLabel:myInit()
	local trimText = string.gsub(self.text, "<.->", "");
	local total,en,cn = getUtf8StringCount(trimText)
	-- local enWidth = AndroidEnWidth
	-- if device.platform == "ios" then
	-- 	enWidth = IOSEnWidth
	-- end
	-- self.textWidth = (self.fontSize+2)* (en*enWidth + cn)

    local hideLabel = display.newTTFLabel({
    	text = trimText,font = self.font,size=self.fontSize
    })
    self.textWidth = hideLabel:getContentSize().width

	self.textTable = self:_transformText(self.text)
	-- dump(self.textTable)
	self:_showTextWithTable(self.textTable)
	self:setSystemFontSize(self.fontSize)
end

function ColorLabel:_transformText(text)
	local result = {}
	local firstTrans = string.split(text, "color=\"#")
	if #firstTrans>=2 and not self.forceDefaultColor then
		local colorString = string.split(firstTrans[2], "\"")
		if #colorString>0 then
			color = self:_transformColor(colorString[1])
			--<划掉>去除color标签</划掉>去除所有<font>标签
			local trimText = string.gsub(text,"<font(.-)>","")
			trimText = string.gsub(trimText,"</font>","")
			trimText = string.gsub(trimText,"&nbsp;"," ")
            table.insert(result,{["color"]=color,["text"]=trimText})
		end
	else
		--<划掉>去除color标签</划掉>去除所有<font>标签
		local trimText = string.gsub(text,"<font(.-)>","")
		trimText = string.gsub(trimText,"</font>","")
		trimText = string.gsub(trimText,"&nbsp;"," ")
		table.insert(result,{["color"]=self.defaultColor,["text"]=trimText})
	end
	-- local secondTrans = {}
	-- if #firstTrans>0 then
	-- 	for firstI,firstV in ipairs(firstTrans) do
	-- 		local temp = string.split(firstV,"[/color]")
	-- 		for tempI,tempV in ipairs(temp) do
	-- 			table.insert(secondTrans, tempV)
	-- 		end
	-- 	end
	-- 	for secondI,secondV in ipairs(secondTrans) do
	-- 		temp = string.split(secondV,"]")
	-- 		local color = self.defaultColor
	-- 		local text = ""
	-- 		if #temp ==2 then
 --                color = self:_transformColor(temp[1])
 --                text = temp[2]
 --            else
 --                text = temp[1]
 --            end
 --            table.insert(result,{["color"]=color,["text"]=text})
 --        end
	-- end

	return result
end

function ColorLabel:_transformColor(text)
	if string.len(text) < 6 then
		return self.defaultColor
	else
		local color1Text = string.sub(text, -6,-5)
		local color1 = tonumber("0x"..color1Text)

		local color2Text = string.sub(text, -4,-3)
		local color2 = tonumber("0x"..color2Text)

		local color3Text = string.sub(text, -2,-1)
		local color3 = tonumber("0x"..color3Text)
		return cc.c3b(color1, color2, color3)
	end
end

function ColorLabel:_showTextWithTable(textTable)
	for i,v in ipairs(textTable) do

		local text = ""
		for i=1, self.lineNum-1 do
			text = text.."\n"
		end
		text = text..v.text
		self:setString(text)
		self:setColor(v.color)
	end
end

function ColorLabel:getTextWidth()
	return self.textWidth
end

return ColorLabel
