--
-- Author: 
-- Date: 2018-07-27 11:42:15
--
local XbDialog = require("app.hall.base.ui.CommonView")
local GlobalItemInfoMgr = GlobalItemInfoMgr or require("app.hall.bag.model.GoodsData").new()
local MailBoxDialog = class("MailBoxDialog", function() return XbDialog.new() end)
local MyColorTextArea = require("app.hall.mail.view.ReadOnlyTextEdit")

function MailBoxDialog:ctor(params)
	self.params = params
    -- self._funcId = params._funcId
    self:myInit()
end
--初始化数据
function MailBoxDialog:myInit()
	if not GlobalMailController then
	   GlobalMailController = require("app.hall.mail.control.MailController").new()
	end
	self.csb = "csb/mail_notice/mail_box_dialog_layer.csb"
    ToolKit:registDistructor( self, handler(self, self.onDestory) )
	addMsgCallBack(self, MSG_MAIL_UPDATE_SUCCESS, handler(self, self.updateViews))

	

	self.mailController = GlobalMailController
	self:setMailList(GlobalMailController:getMailList())
	self.currentSelectedIndex = 1
	self:setupViews()
	self:updateViews()
	self:performWithDelay(handler(self, self.onEndAnimation), 0.2)
	ToolKit:addLoadingDialog(10)
end

function MailBoxDialog:onEndAnimation()
	GlobalMailController:requestUpdateMail(1)
end

function MailBoxDialog:setMailList(mailList)
	self.mailList = mailList
end

function MailBoxDialog:setCurrentSelectedIndex(index)
	self.currentSelectedIndex = index
end

--生成时调用一次，以后不再调用
function MailBoxDialog:setupViews()
	self.node = UIAdapter:createNode(self.csb):addTo(self)
	UIAdapter:adapter(self.node, handler(self, self.onTouchCallback))

	do
		--初始化邮件列表（清空）
		local listView = self.node:getChildByName("ListView_1")
		local itemModel = listView:getChildByName("tab")
		if itemModel then
			listView:setItemModel(itemModel)
		end
		listView:removeAllItems()
		listView:addScrollViewEventListener( handler(self, self.listViewListener) )
	end

	do
		--初始化邮件详情（清空）
		self.mailDetailUI = {}
		self.mailDetailUI.title = self.node:getChildByName("mail_title")
		self.mailDetailUI.content = self.node:getChildByName("mail_content")
		local defaultColor = self.mailDetailUI.content:getColor()

		--content使用MyColorTextArea
		self.mailDetailUI.content:setString("")
		local myColorTextArea = MyColorTextArea.new({
			text = "",
			width = self.mailDetailUI.content:getContentSize().width,
			height = self.mailDetailUI.content:getContentSize().height,
			-- font = "黑体",
			fontSize = self.mailDetailUI.content:getFontSize(),
			defaultColor = self.mailDetailUI.content:getColor(),
			lineHeight = self.mailDetailUI.content:getFontSize(),
			isWebSize = true,
		}):addTo(self.mailDetailUI.content)
		:align(display.LEFT_TOP, 0, self.mailDetailUI.content:getContentSize().height)
		self.mailDetailUI.content = myColorTextArea

		self.mailDetailUI.time = self.node:getChildByName("mail_time")
		self.mailDetailUI.rewardPanel = self.node:getChildByName("reward_panel")
		self.mailDetailUI.reward1 = self.node:getChildByName("reward_1")
		self.mailDetailUI.reward2 = self.node:getChildByName("reward_2")
		self.mailDetailUI.reward3 = self.node:getChildByName("reward_3")
		self.mailDetailUI.reward4 = self.node:getChildByName("reward_4")
		self.mailDetailUI.reward5 = self.node:getChildByName("reward_5")
		self.mailDetailUI.rewardIcon1 = self.node:getChildByName("reward_icon_1")
		self.mailDetailUI.rewardIcon2 = self.node:getChildByName("reward_icon_2")
		self.mailDetailUI.rewardIcon3 = self.node:getChildByName("reward_icon_3")
		self.mailDetailUI.rewardIcon4 = self.node:getChildByName("reward_icon_4")
		self.mailDetailUI.rewardIcon5 = self.node:getChildByName("reward_icon_5")
		self.mailDetailUI.count1 = self.node:getChildByName("count_1")
		self.mailDetailUI.count2 = self.node:getChildByName("count_2")
		self.mailDetailUI.count3 = self.node:getChildByName("count_3")
		self.mailDetailUI.count4 = self.node:getChildByName("count_4")
		self.mailDetailUI.count5 = self.node:getChildByName("count_5")
		self.mailDetailUI.count1:setLocalZOrder(2)
		self.mailDetailUI.count2:setLocalZOrder(2)
		self.mailDetailUI.count3:setLocalZOrder(2)
		self.mailDetailUI.count4:setLocalZOrder(2)
		self.mailDetailUI.count5:setLocalZOrder(2)
		self.mailDetailUI.rewardBtn = self.node:getChildByName("reward_btn")
		self.mailDetailUI.deleteBtn = self.node:getChildByName("delete_btn")
		self.mailDetailUI.rewardBtn:enableOutline(cc.c4b(25, 118, 12, 255), 3)
		 self.node:getChildByName("reward_btn_disabled"):enableOutline(cc.c4b(110, 110, 110, 255), 3)
		self.bg1 = self.node:getChildByName("bg1")
		self.bg1:setVisible(false)
		
		self.mailDetailUI.title:disableEffect()
		-- self.mailDetailUI.content:disableEffect()
		self.mailDetailUI.time:disableEffect()
		self.mailDetailUI.title:enableOutline(cc.c4b(163, 112, 29, 255),2)
		self.mailDetailUI.count1:disableEffect()
		self.mailDetailUI.count1:enableOutline(cc.c4b(163, 112, 29, 255),2)
		self.mailDetailUI.count2:disableEffect()
		self.mailDetailUI.count2:enableOutline(cc.c4b(163, 112, 29, 255),2)
		self.mailDetailUI.count3:disableEffect()
		self.mailDetailUI.count3:enableOutline(cc.c4b(163, 112, 29, 255),2)
		self.mailDetailUI.count4:disableEffect()
		self.mailDetailUI.count4:enableOutline(cc.c4b(163, 112, 29, 255),2)
		self.mailDetailUI.count5:disableEffect()
		self.mailDetailUI.count5:enableOutline(cc.c4b(163, 112, 29, 255),2)
		--开始隐藏
		self:clearContent()
	end
	self:runEmptyAnimation()
end

function MailBoxDialog:trimText(text)
	-- local trimText = string.gsub(text,"<br>","\n")
	-- trimText = string.gsub(trimText,"<(.-)>","")
	-- trimText = string.gsub(trimText,"&nbsp;"," ")
	-- return trimText
	return text
end

--设置好maillist和mail之后调用
function MailBoxDialog:updateViews()
	print("MailBoxDialog:updateViews():",self.currentSelectedIndex)
	ToolKit:removeLoadingDialog()
	--更新列表+
	local listView = self.node:getChildByName("ListView_1")
	listView:removeAllItems()
	if self.currentSelectedIndex > #self.mailList and #self.mailList>0 then
		self.currentSelectedIndex = #self.mailList
	end
	for i,mail in ipairs(self.mailList) do
		listView:insertDefaultItem(i-1)
		local item = listView:getItem(i-1)
		if self.currentSelectedIndex == i then
			GlobalMailController:readMail(self.currentSelectedIndex)
			item:getChildByName("tab_bg_unselected"):setVisible(false)
			item:getChildByName("tab_bg_selected"):setVisible(true)
			item:getChildByName("tab_text_title"):disableEffect()
			item:getChildByName("tab_text_title"):enableOutline(cc.c4b(197, 115, 9, 255),2)
			item:getChildByName("tab_text_time"):disableEffect()
			item:getChildByName("tab_text_time"):enableOutline(cc.c4b(197, 115, 9, 255),2)
		else
			item:getChildByName("tab_bg_selected"):setVisible(false)
			item:getChildByName("tab_bg_unselected"):setVisible(true)
			item:getChildByName("tab_text_title"):disableEffect()
			item:getChildByName("tab_text_title"):enableOutline(cc.c4b(0, 110, 130, 255),2)
			item:getChildByName("tab_text_time"):disableEffect()
			item:getChildByName("tab_text_time"):enableOutline(cc.c4b(0, 110, 130, 255),2)
		end
		item:getChildByName("tab_text_title"):setString(ToolKit:shorterString(mail:getTitle(),7))
		local date = ToolKit:changeTime(mail:getSendDate(),"%Y/%m/%d")
		item:getChildByName("tab_text_time"):setString(date)
		if mail:getIsTextMail() == 1 or mail:getState() == 3 then
			item:getChildByName("tab_reward"):setVisible(false)
		end
		if mail:getState() ~= 1 then
			item:getChildByName("red_dot"):setVisible(false)
		end
		item:setTag(i)
	end

	--更新正文
	if self.currentSelectedIndex >0 and self.mailList[self.currentSelectedIndex] then
		local mail = self.mailList[self.currentSelectedIndex]
		local title = mail:getTitle()
		self.mailDetailUI.title:setString(title)
		local content = mail:getContent()
		self.mailDetailUI.content:setString("    "..self:trimText(content))
		local date = ToolKit:changeTime(mail:getSendDate(),"%Y年%m月%d日 %H:%M:%S")
		self.mailDetailUI.time:setString(date)
		self.mailDetailUI.deleteBtn:setVisible(true)
		self.mailDetailUI.deleteBtn:setEnabled(true)
		if mail:getIsTextMail()==0 then
			self.mailDetailUI.rewardPanel:setVisible(true)
			local rewardList = mail:getRewardList()
			--给reward_node1~5加上图标和数量
			for i=1,5 do
				self.mailDetailUI["reward"..i]:setVisible(false)
				self.mailDetailUI["rewardIcon"..i]:removeAllChildren()
			end
			for i,v_reward in ipairs(rewardList) do
				if i<=5 then
					-- if v_reward.m_type == 1 then --金币
					-- 	display.newSprite("icon/public_img_jinbi_03.png")
					-- 	:addTo(self.node:getChildByName("reward_icon_"..i))
					-- 	:align(display.CENTER, 0, 0):setScale(0.67)
					-- 	local text = ToolKit:shorterNumber(v_reward.m_num)
					-- 	self.node:getChildByName("count_"..i):setString(text)
					-- elseif v_reward.m_type == 2 then --钻石
					-- 	display.newSprite("icon/public_img_diamond.png")
					-- 	:addTo(self.node:getChildByName("reward_icon_"..i))
					-- 	:align(display.CENTER, 0, 0):setScale(0.67)
					-- 	local text = ToolKit:shorterNumber(v_reward.m_num)
					-- 	self.node:getChildByName("count_"..i):setString(text)
					-- elseif v_reward.m_type == 3 then --道具
						local itemInfo = GlobalItemInfoMgr:getItemInfoByID(v_reward.m_id)
						if itemInfo then
							itemInfo:getItemIconSprite()
							:addTo(self.node:getChildByName("reward_icon_"..i))
							:align(display.CENTER, 0, 0):setScale(0.67)
							local text = ToolKit:shorterNumber(v_reward.m_num)
							self.node:getChildByName("count_"..i):setString(text)
						end
					-- end
					self.mailDetailUI["reward"..i]:setVisible(true)
				end
			end -- end of for
			--领取按钮状态
			if mail:getState() == 3 then
				self.mailDetailUI.rewardBtn:setVisible(false)
			else
				self.mailDetailUI.rewardBtn:setVisible(true)
			end
		else
			self.mailDetailUI.rewardPanel:setVisible(false)
		end --end of rewardIf
	else
		self:clearContent()
	end

	--空邮件
	local isMailListEmpty = false
	if #self.mailList == 0 then
		isMailListEmpty = true
	end
	self.bg1:setVisible(isMailListEmpty)
	self.node:getChildByName("content_bg"):setVisible(not isMailListEmpty)
	self.node:getChildByName("listview_clip"):setVisible(not isMailListEmpty)
	self.node:getChildByName("no_mail_tips"):setVisible(isMailListEmpty)
	self.node:getChildByName("animation_node"):setVisible(isMailListEmpty)
end

--隐藏正文
function MailBoxDialog:clearContent()
	self.mailDetailUI.title:setString("")
	self.mailDetailUI.content:setString("")
	self.mailDetailUI.time:setString("")
	self.mailDetailUI.rewardPanel:setVisible(false)
	self.mailDetailUI.reward1:setVisible(false)
	self.mailDetailUI.reward2:setVisible(false)
	self.mailDetailUI.reward3:setVisible(false)
	self.mailDetailUI.reward4:setVisible(false)
	self.mailDetailUI.reward5:setVisible(false)
	self.mailDetailUI.count1:setString("")
	self.mailDetailUI.count2:setString("")
	self.mailDetailUI.count3:setString("")
	self.mailDetailUI.count4:setString("")
	self.mailDetailUI.count5:setString("")
	self.mailDetailUI.deleteBtn:setVisible(false)
end

--按钮绑定
function MailBoxDialog:onTouchCallback(sender)
	local name = sender:getName()
	if name == "close_btn" then
		self:closeDialog()
	elseif name == "tab" then
		self.currentSelectedIndex = sender:getTag()
		self:updateViews()
	elseif name == "reward_btn" then
		self.mailDetailUI.rewardBtn:setVisible(false)
		GlobalMailController:getMailReward (self.currentSelectedIndex)
	elseif name == "delete_btn" then
		local mail = self.mailList[self.currentSelectedIndex]
		if mail:getIsTextMail() == 1 then
			self.mailDetailUI.deleteBtn:setEnabled(false)
			GlobalMailController:deleteMail(self.currentSelectedIndex)
		else
			local params = {
		        title = "删除邮件",
		        message = "是否删除本邮件?", -- todo: 换行
		        leftStr = "确认",
		        rightStr = "取消",
		    }
		    local callback = function ()
				GlobalMailController:deleteMail(self.currentSelectedIndex)
		        end
		    local dlg = require("app.hall.base.ui.MessageBox").new()
		    dlg:TowSubmitAlert(params, callback)
		    dlg:showDialog()
	    end
	end
end

--列表拖到底之后的绑定
function MailBoxDialog:listViewListener(sender,eventType)
	if eventType == ccui.ScrollviewEventType.scrollToBottom then
        print("SCROLL_TO_BOTTOM")
        GlobalMailController:requestMoreMail()
    end
end

function MailBoxDialog:runEmptyAnimation()
    self.armature = ToolKit:createArmatureAnimation("res/tx/", "youjiankongzhuangtai", nil )
    self.armature:addTo(self.node:getChildByName("animation_node")):align(display.CENTER, 0, 0)
    self.armature:getAnimation():playWithIndex(0,-1,1)
end

--析构函数
function MailBoxDialog:onDestory()
	removeMsgCallBack(self, MSG_MAIL_UPDATE_SUCCESS)
end

return MailBoxDialog