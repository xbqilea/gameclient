--
-- Author: lhj
-- Date: 2018-11-5 
--

local Dialog = require("app.hall.base.ui.CommonView")
local NoticeCell = require("app.hall.mail.view.NoticeCell")
local NoticeLayer = class("NoticeLayer", function() return Dialog.new() end)

function NoticeLayer:ctor()
	self:myInit()
	self:setupViews()
	self:showContent()
end

function NoticeLayer:myInit()
	self.m_SelectIndex = 1
end

function NoticeLayer:setupViews()
	self.node = UIAdapter:createNode("csb/mail_notice/layer_notice.csb"):addTo(self)
	UIAdapter:adapter(self.node, handler(self, self.onTouchCallback))
	self.noticeScrol = self.node:getChildByName("notice_scroll")
	self.notice_desc = self.node:getChildByName("notice_desc")
	self.notice_desc:setString("")
end

function NoticeLayer:showContent()
	self.m_NoticeList = GlobalBulletinController:getBulletinList()

	if #self.m_NoticeList == 0 then 
		self.node:getChildByName("btn_bg"):setVisible(false)
		self.node:getChildByName("tips"):setVisible(true)
		return 
	else
		self.node:getChildByName("btn_bg"):setVisible(true)
		self.node:getChildByName("tips"):setVisible(false)
	end

	self.m_Noticeheight = table.nums(self.m_NoticeList) * 100 > 500 and table.nums(self.m_NoticeList) * 100 or 500
	self.noticeScrol:setInnerContainerSize(cc.size(232, self.m_Noticeheight))
	self:createNoticeScrol()
end

function NoticeLayer:createNoticeScrol()
	for i=1, #self.m_NoticeList do
		local node = NoticeCell.new({data = self.m_NoticeList[i], layer = self})
		node:setTag(i)
		node:addTo(self.noticeScrol)
		node:setPosition(cc.p(3, self.m_Noticeheight - 100*i))
	end

	self:updateContent(self.m_SelectIndex)
end

function NoticeLayer:updateContent(tag)
	self.m_SelectIndex = tag
	for i=1, #self.m_NoticeList do
		local node = self.noticeScrol:getChildByTag(i)
		if self.m_SelectIndex == i then
			node:refreshContent(true)
		else
			node:refreshContent(false)
		end
	end

	self.notice_desc:setString(self.m_NoticeList[self.m_SelectIndex]:getContent())
end

function NoticeLayer:onTouchCallback(sender)
	local name = sender:getName()
	if name == "exitBtn" then
		self:closeDialog()
	end
end

return NoticeLayer