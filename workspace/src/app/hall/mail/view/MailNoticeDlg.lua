--
-- Author: 
-- Date: 2018-07-27 11:42:15
--

local XbDialog = require("app.hall.base.ui.CommonView")
local DlgAlert = require("app.hall.base.ui.MessageBox")
local GlobalItemInfoMgr = GlobalItemInfoMgr or require("app.hall.bag.model.GoodsData").new()
local MailNoticeDlg = class("MailNoticeDlg", function() return XbDialog.new() end)
local MyColorTextArea = import(".ReadOnlyTextEdit")
local MailDetail = import(".MailContent")

local FUNC_TYPE = {
	MAIL                 =              1,  --邮件
	NOTICE               =              2 , --公告
}

local FUNC_IMAGE = {
	{x = 108, y = 24},
	{x = 320, y = 24}, 
}

local TAG_INDEX = 100000

function MailNoticeDlg:ctor(params)
	self.params = params
	self.m_FuncType  = 1
    self:myInit()

    self.m_deleteMailLock = false
end

function MailNoticeDlg:myInit()
	-- 注册析构函数
	ToolKit:registDistructor(self, handler(self, self.onDestroy))

	addMsgCallBack(self, MSG_MAIL_UPDATE_SUCCESS, handler(self, self.updateMailViews))

	self.node = UIAdapter:createNode("csb/mail_notice/mail_box_dialog_layer.csb"):addTo(self)
	UIAdapter:adapter(self.node, handler(self, self.onTouchCallback))
	self:showFuncType()
end

function MailNoticeDlg:showFuncType()

	self.node:getChildByName("mailNode"):setVisible(self.m_FuncType == FUNC_TYPE.MAIL)
	self.node:getChildByName("noticeNode"):setVisible(self.m_FuncType == FUNC_TYPE.NOTICE)

--[[	self.node:getChildByName("light_spr"):setPosition(cc.p(FUNC_IMAGE[self.m_FuncType].x, FUNC_IMAGE[self.m_FuncType].y))--]]

	
	

	if self.m_FuncType == FUNC_TYPE.MAIL then
		self:showMailView()
	else
		self:showNoticeView()
	end
end

function MailNoticeDlg:showMailView()
	if not GlobalMailController then
	   GlobalMailController = require("app.hall.mail.control.MailController").new()
	end
	self:performWithDelay(handler(self, self.onEndAnimation), 0.2)
end

function MailNoticeDlg:onEndAnimation()
	GlobalMailController:requestUpdateMail(1)
end

function MailNoticeDlg:showNoticeView()

	self.m_NoticeList = GlobalBulletinController:getBulletinList()

	dump(self.m_NoticeList, "公告列表:", 10)

	self.node:getChildByName("no_notice"):setVisible(table.nums(self.m_NoticeList) == 0)

	self.node:getChildByName("mailScrol"):removeAllChildren()

	self.noticeScrol = self.node:getChildByName("noticeScrol")
	if self.noticeScrol then
		self.noticeScrol:removeAllChildren()
	end

	if table.nums(self.m_NoticeList) == 0 then
		return
	end

	self.m_Noticeheight = table.nums(self.m_NoticeList) * 114 > 500 and table.nums(self.m_NoticeList) * 114 or 500
	self.noticeScrol:setInnerContainerSize(cc.size(1074, self.m_Noticeheight))
	self:createNoticeScrol()
end

function MailNoticeDlg:createNoticeScrol()
	for key, value in ipairs(self.m_NoticeList) do
		local noticeCell = UIAdapter:createNode("csb/mail_notice/mail_box_dialog_cell.csb")
		local bg = noticeCell:getChildByName("bg")

		noticeCell:getChildByName("del_btn"):setVisible(false)

		bg.index = key
		ToolKit:listenerNodeTouch(self, bg, handler(self, self.mailCellBtnBack))

		--noticeCell:getChildByName("title"):setString(ToolKit:shorterString(value:getTitle(),7))
		noticeCell:getChildByName("title"):setString(value:getTitle())

		noticeCell:getChildByName("content"):setString(self:trimText(value:getContent()))
		noticeCell:getChildByName("img"):setVisible(false)
		bg:setSwallowTouches(false)
		noticeCell:addTo(self.noticeScrol)
		noticeCell:setPosition(cc.p(20, self.m_Noticeheight - 114* key))
	end
end

function MailNoticeDlg:updateMailViews()

	self.m_deleteMailLock = false

	self.m_MailList = GlobalMailController:getMailList()

	self.node:getChildByName("no_mail"):setVisible(table.nums(self.m_MailList) == 0)

	self.node:getChildByName("noticeScrol"):removeAllChildren()


	self.mailScrol = self.node:getChildByName("mailScrol")
	if self.mailScrol then
		self.mailScrol:removeAllChildren()
	end

	if table.nums(self.m_MailList) == 0 then
		return
	end

	self.m_Mailheight = table.nums(self.m_MailList) *114 > 460 and table.nums(self.m_MailList) * 114 or 460
	
	self.mailScrol:setInnerContainerSize(cc.size(1074, self.m_Mailheight))
	self:createMailScrol()
end

function MailNoticeDlg:createMailScrol()
	for key, value in ipairs(self.m_MailList) do
		local mailCell  = UIAdapter:createNode("csb/mail_notice/mail_box_dialog_cell.csb")
		local bg = mailCell:getChildByName("bg")
		bg.index = key
		ToolKit:listenerNodeTouch(self, bg, handler(self, self.mailCellBtnBack))

		--mailCell:getChildByName("title"):setString(ToolKit:shorterString(value:getTitle(),7))


		mailCell:getChildByName("title"):setString(value:getTitle())

		local content = string.split(value:getContent(), "\n")

		if content and content[1] ~= "" then
			mailCell:getChildByName("content"):setString(ToolKit:shorterString(self:trimText(content[1]), 20))
		end

		if value:getState() == 1  then
			mailCell:getChildByName("img"):setSpriteFrame(display.newSpriteFrame("email_tubiao_yj.png"))
		else
			mailCell:getChildByName("img"):setSpriteFrame(display.newSpriteFrame("email_tubiao_yidu.png"))
		end

		local del_btn = mailCell:getChildByName("del_btn")
		del_btn:setTag(10000 + key)
		del_btn:addTouchEventListener(handler(self,self.onTouchCallback)) 

		bg:setSwallowTouches(false)
		mailCell:addTo(self.mailScrol)
		mailCell:setPosition(cc.p(20, self.m_Mailheight - 114* key))
	end
end

function MailNoticeDlg:mailCellBtnBack(sender)
	g_GameMusicUtil:playSound("audio/click_audio.mp3")
	if self:getChildByTag(TAG_INDEX) then
		return
	end
	if self.m_FuncType == FUNC_TYPE.MAIL then
		GlobalMailController:readMail(sender.index)
		sender:getChildByName("img"):setSpriteFrame(display.newSpriteFrame("email_tubiao_yidu.png"))
		MailDetail.new({data = self.m_MailList[sender.index], index = sender.index, funType = self.m_FuncType}):addTo(self):setTag(TAG_INDEX)
	else
		MailDetail.new({data = self.m_NoticeList[sender.index], index = sender.index, funType = self.m_FuncType}):addTo(self):setTag(TAG_INDEX)
	end
end

function MailNoticeDlg:trimText(text)
	--local trimText = string.gsub(text,"<br>","\n")
	local trimText = string.gsub(text,"<br>","")
	trimText = string.gsub(trimText,"<(.-)>","")
	trimText = string.gsub(trimText,"&nbsp;"," ")
	return trimText
end

function MailNoticeDlg:onTouchCallback(sender, eventType)

	local name = sender:getName()
	print("name =========", name)
	if name == "exitBtn" then
		self:closeDialog()
	elseif name == "func_mail" then
		self.m_FuncType = FUNC_TYPE.MAIL
		self:showFuncType()
	elseif name == "func_notice" then
		self.m_FuncType = FUNC_TYPE.NOTICE
		self:showFuncType()
	elseif name == "del_btn" then
		if eventType == ccui.TouchEventType.ended then

			local tag = sender:getTag()
			print("TAG 值", tag)
			local index = tag % 10000

			local data = self.m_MailList[index]

			if self.m_deleteMailLock then
				return
			end
			
			local params = {tip = "是否确认删除", tip_size = 28, areaSize = cc.size(540, 250)}
				
			local kickDialog = DlgAlert.new()	

    		local dlg = kickDialog.customTipsAlert(params)

    		dlg:setBtnAndCallBack( STR(5, 4), STR(6, 4), 
		    function ()
		        if data.BaseInfo.IsTextMail == 0 then
		        	if data.BaseInfo.State ~= 3 then
		        		--未领取
		        		TOAST("您有附件未领取，无法删除！")
		        	else
		        		--已领取
		        		self.m_deleteMailLock = true
		        		GlobalMailController:deleteMail(index)
		        		dlg:closeDialog()
		        	end
		        else
		        	self.m_deleteMailLock = true
		        	GlobalMailController:deleteMail(index)
		        	dlg:closeDialog()
		        end
		    end, 

		    function ()
		        dlg:closeDialog()
		    end )
		end
	end
end

function MailNoticeDlg:onDestroy()
	removeMsgCallBack(self, MSG_MAIL_UPDATE_SUCCESS)
end

return MailNoticeDlg


