--
-- Author: 
-- Date: 2018-07-27 11:42:15
--对多个MyColorLabel的封装, 以实现使用[n]换行的功能(测试,比MyRichLabel换行效果好,但是只能一段话同一种颜色
-- [n]换行要放在[color]标签后
-- 对齐的是左上角
--
--[[
	用法示例
	local myColorTextArea = MyColorTextArea:create("[color=#FF00FF]这只是一条简单的小文本CC[n]",width,height,fontName,fontSize)
	或者
	local myColorTextArea = MyColorTextArea.new({
		text = "[color=#FF00FF]这只是一条简单的小文本[n]",
		width = 400,
		height = 400,
		font = "黑体",
		fontSize = 20,
		defaultColor = cc.c3b(0,0,0),
		lineHeight = 20,
		isWebSize = true,
		forceDefaultColor = false,
	})
]]
local MyColorLabel = require("app.hall.mail.view.StaticLabel")

local ColorTextArea = class("ColorTextArea", function()
	local node = display.newNode()
	return node
end)

local defaultColor = cc.c3b(255, 255, 255)
local defaultLineHeight = 1.65
local defaultLinePadding = 0
local WebSize = {	--蛋疼地与网站一一对应
	[1] = 12,
	[2] = 13,
	[3] = 16,
	[4] = 18,
	[5] = 24,
	[6] = 32,
	[7] = 48,
}

function ColorTextArea:create(_text,_width,_height,_font,_fontSize,_defaultColor,_linePadding,_forceDefaultColor)
	return ColorTextArea.new(
	{
		text = _text,
		width = _width,
		height = _height,
		font = _font,
		fontSize = _fontSize,
		defaultColor = _defaultColor,
		linePadding = _linePadding,
		forceDefaultColor = _forceDefaultColor,
	})
end

function ColorTextArea:ctor(params)
	self.text = params.text or ""
	self.width = params.width or display.width
	self.height = params.height or 1
	self.font = params.font or "黑体"
	self.fontSize = params.fontSize or 20
	self.isWebSize = params.isWebSize or false --如果是webSize，字号/12*fontSize
	self.defaultColor = params.defaultColor or defaultColor
	self.linePadding = params.linePadding or defaultLinePadding
	self.forceDefaultColor = params.forceDefaultColor
	self.myColorLabel = {}
	self:myInit()
	self:setContentSize(self.width,self.height)
end

function ColorTextArea:myInit()
	self.lineNum = 0
	for ti,tv in ipairs(self.myColorLabel) do
		tv:removeFromParent()
	end
	self.myColorLabel = {}
	self.textTable = self:_splitText(self.text)
	for ti,tv in ipairs(self.textTable) do
		if string.len(tv) >= 0 then
			--记录检查有没有url标签<url></url>
			local url = nil
			if string.find(tv, "<a href=") then
				local temp = string.split(tv,"<a href=\"")
				if temp and #temp>=2 then
					temp = string.split(temp[2],"\"")
					if temp and temp[1] then
						url = temp[1]
					end
				end
				--去除url标签
				tv = string.gsub(tv,"<a href=(.-)>","")
				tv = string.gsub(tv,"</a>","")
			end
			--记录检查有没有size标签[size=19]
			local fontSize
			if string.find(tv, "size=\"") then
				local temp = string.split(tv,"size=\"")
				if temp and #temp>=2 then
					temp = string.split(temp[2],"\"")
					if temp and temp[1] then
						fontSize = tonumber(temp[1])
						if self.isWebSize then
							fontSize = WebSize[tonumber(fontSize)]/12*self.fontSize
						end
					end
				end
				--去除size标签
				tv = string.gsub(tv,"size=\"%d+\"","")
			end
			if not fontSize then
				fontSize = self.fontSize
			end
			--生成文字组件
			self.myColorLabel[ti] = MyColorLabel.new(tv,self.width,(self.height - self.lineNum*0.8),self.font,fontSize,self.defaultColor,1,self.forceDefaultColor):addTo(self)
			self.myColorLabel[ti]:setAnchorPoint(0,1.0)
			self.myColorLabel[ti]:setPosition(0,self.height - self.lineNum)
			-- self.myColorLabel[ti]:setPosition(-self.width/2,self.height/2)

				local addLineNum = 0
                if string.len(tv)>10 then
                	print(tv)
					addLineNum = math.floor(self.myColorLabel[ti]:getTextWidth()/self.width) + 1
                	print(addLineNum)
				else
					addLineNum = 1
				end
				self.lineNum = self.lineNum + addLineNum * (fontSize * defaultLineHeight + defaultLinePadding)
			-- end
			if url then

				local function distance(x1,y1,x2,y2)
					return math.sqrt(math.pow((y2-y1),2)+math.pow((x2-x1),2))
				end
				self.myColorLabel[ti]:addNodeEventListener(cc.NODE_TOUCH_EVENT, function (event)
					local x, y, prevX, prevY = event.x, event.y, event.prevX, event.prevY
					if event.name == "began" then
						self.startX = x
						self.startY = y
						return true
					elseif event.name == "moved" then
					elseif event.name == "ended" then
                        if distance(self.startX,self.startY,x,y)<30 then
							print(url)
							device.openURL(url)
						end
					end
				end)
				UIAdapter:transNode(self.myColorLabel[ti])
			end
		end
		-- print(self.lineNum,self.height - (self.lineNum-1)*self.linePadding,tv)
	end
	dump(self.textTable)
end

function ColorTextArea:setString(_text)
	self:setText(_text)
end

function ColorTextArea:setText(_text)
	self.text = _text
	self:myInit()
end
--根据</p>和<br>分行, 一行一个ColorLabel
function ColorTextArea:_splitText(text)
	local result = {}
	local p = string.split(text, "</p>")
	for i,v in ipairs(p) do
		local br = string.split(v, "<br>")
		for ib,vb in ipairs(br) do
			vb = string.gsub(vb, "<p>", "") --去掉<p>
			table.insert(result, vb)
		end
	end
	return result
end

--改版,linenum现在记录具体偏移高度值
function ColorTextArea:getLineNum()
	return self.lineNum
end

function ColorTextArea:getSizeHeight()
	return self.lineNum --* (self.fontSize*defaultLineHeight + self.linePadding)
end

return ColorTextArea