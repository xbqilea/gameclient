--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 邮件控制类

local MailController = class("MailController")
local Mail = require("app.hall.mail.model.Mail")

-- 道具列表全局变量
GlobalMails = GlobalMails or {}

function MailController:ctor()
	self:myInit()
end

function MailController:myInit()

	addMsgCallBack(self, MSG_MAIL_UPDATE_SUCCESS, handler(self, self.onMailUpdateSuccess))

    -- 注册网络监听
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GetMailList_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_AcceptMail_Ack", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_DeleteMail_Ack", handler(self, self.netMsgHandler))
    	
 	-- self:requestUpdateMail(1)
end

--邮件接受的服务器信息处理
--param：
--__idStr:协议名
--__info: 内容
function MailController:netMsgHandler( __idStr, __info ) 
	-- print("MailController:netMsgHandler", __idStr)
	dump(__info,__idStr,6)
	-- 新邮件提醒
	if __idStr == "CS_H2C_GetMailList_Ack" then
		if __info.m_optCode == 0 then--刷新邮件列表成功
			self:_updateMail(GlobalMails,__info.m_list,__info.m_curPage)
			sendMsg(MSG_MAIL_UPDATE_SUCCESS)
		else
			sendMsg(MSG_SHOW_ERROR_TIPS, __info.m_optCode )
		end
	elseif __idStr == "CS_H2C_AcceptMail_Ack" then
		if __info.m_optCode == 0 then 		-- 邮件领取成功
			-- print("领取成功")
--			 local layer = scene:getStackLayerManager():getCurrentLayer()
--             layer:showMailAward()
			sendMsg(MSG_MAIL_UPDATE_SUCCESS)
            sendMsg(MSG_MAIL_REWARD_SUCCESS)
		else --其他原因
			self.sending = false
			sendMsg(MSG_SHOW_ERROR_TIPS, __info.m_optCode )
		end
	elseif __idStr == "CS_H2C_DeleteMail_Ack" then
		if __info.m_optCode == 0 then 		-- 邮件删除成功
			print("删除成功")
			sendMsg(MSG_MAIL_UPDATE_SUCCESS)
		else --其他原因
			self.sending = false
			sendMsg(MSG_MAIL_UPDATE_SUCCESS)
			sendMsg(MSG_SHOW_ERROR_TIPS, __info.m_optCode )
		end
	end
end

--获取正在领取的道具列表
function MailController:getReceivingRewardList()
	return self.receivingRewardList
end
-- 更新邮件列表数据，非手动调用，处理服务器返回的邮件列表
function MailController:_updateMail(mailList,mails,page)
	self.page = page
	if page == 1 then
		for i=1,#mailList do
			table.remove(mailList,1)
		end
	end
	for i,v_mail in ipairs(mails) do
		local detail = v_mail
		if detail.m_ID ~= 0 then
			local flag = false
			for j,v_localMail in ipairs(mailList) do
				if v_localMail:getID() == detail.m_ID then
					print("_updateMail,detail.ID",detail.m_ID)
					v_localMail:setInfo(detail)
					-- v_localMail:setState(v_mail.m_state)
					flag = true
				end
			end
			if flag == false then
				mailList[#mailList+1] = Mail.new(detail)
				-- mailList[#mailList]:setState(v_mail.m_state)
			end
		end
	end 
	-- self:sortMail()
end
--排序邮件
function MailController:sortMail()
	table.sort(GlobalMails,function(a,b) 
		local ret = false
		if a:getState()<b:getState() then
			ret = true
		elseif a:getID()>b:getID() then
			ret = true
		end
		return ret
	end)
end
--删除邮件的请求
--params:
--index:number邮件在列表里的索引,第几封
function MailController:deleteMail(index)
	local mailList = self:getMailList()
	if index then
		local mail = mailList[index]
		-- state = 3已领取
		if mail:getIsTextMail()==1 or mail:getState() == 3 then
			table.remove(mailList,index)
		end
		if mail then
			self:requestDeleteMail(mail:getID())
			self:requestUpdateMailByIndex(#mailList+1)
		else
			print("查无此件")
		end
	else
		-- --删除所有前，先检查一遍是不是有带附件的
		-- local hasReward = false
		-- for i=1,#mailList do
		-- 	if mailList[i]:getIsTextMail() == 0 then
		-- 		hasReward = true
		-- 		break
		-- 	end
		-- end
		-- if hasReward == false then
			--删除所有, 那就只删除所有不带附件的邮件，即IsTextMail为1的
			for i=#mailList,1,-1 do
				if mailList[i]:getIsTextMail() == 1 or mailList[i]:getState() == 3 then
					table.remove(mailList,i)
				end
			end
			self:requestDeleteMail(nil)--不填参数代表删除所有
			sendMsg(MSG_NEW_COME_MAIL,#mailList)
		-- else
		-- 	sendMsg(MSG_SHOW_ERROR_TIPS,-601)
		-- end
	end
end
-- 根据邮件id删除邮件，绑定在列表的单独删除按钮上
--params:
--id:number邮件的id
function MailController:deleteMailByID(id)
	local mailList = self:getMailList()
	local index = 0
	for i,v_mail in ipairs(mailList) do
		if v_mail:getID() == id then
			index = i
		end
	end
	self:deleteMail(index)
end
--删除邮件的请求，非手动调用
--id:number邮件的id
function MailController:requestDeleteMail(id)
	if self.sending then
		return
	end
	self.sending = true
	local params = {2,{}}
	local delType = 1
	if id then
		delType = 2
		params = {1,{id}}
	end
	ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_DeleteMail_Req", params)
end
-- 收到邮件数据更新成功消息
function MailController:onMailUpdateSuccess()
	self.sending = false
	self.sendingMorePage = false
	print("邮件数据已更新")
end
-- 请求更新邮件数据
--page:number第几页，每页邮件数在GlobalDefine里定义
function MailController:requestUpdateMail(page)
--	if self.sending then
--		return
--	end
	self.sending = true
	-- self.page = page
	ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetMailList_Req", {page, GlobalDefine.MAIL_PAGE_SIZE})
end
-- 请求更新邮件数据（单条）
--index:number第几条，用于删除邮件时刷最后一条出来
function MailController:requestUpdateMailByIndex(index)
	ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GetMailList_Req", {index, 1})
end
-- 下拉时调用，请求加载更多邮件数据
function MailController:requestMoreMail()
	if not self.sendingMorePage then
		self:requestUpdateMail(self.page+1)
		self.sendingMorePage = true
	end
end
--领取邮件
--id:number邮件的id
function MailController:getMailRewardByID(id)
	local mailList = self:getMailList()
	local index = 0
	for i,v_mail in ipairs(mailList) do
		if v_mail:getID() == id then
			index = i
		end
	end
	self:getMailReward(index)
end
--领取邮件
--index:number邮件的索引
function MailController:getMailReward(index)
	local mailList = self:getMailList()
	if index then
		local mail = mailList[index]
		if mail then
			self:requestGetMailReward(mail:getID())
			-- table.remove(mailList,index)
			mail:setState(3)
		else
			print("查无此件")
		end
	else
		--领取所有邮件
        self:requestGetMailReward(nil)
		for i=#mailList,1,-1 do
			if mailList[i]:getIsTextMail() == 0 then
				table.remove(mailList,i)
			end
		end
	end
end
-- 请求领取邮件附件 id为nil时领取全部，非手动调用
function MailController:requestGetMailReward(id)
	if self.sending then
		return
	end
	self.sending = true
	local params = {2,{}}
	local delType = 1
	if id then
		delType = 2
		params = {1,{id}}
		self.receivingRewardList = {}
		local mail
		local mailList = self:getMailList()
		for i,v_mail in ipairs(mailList) do
			if v_mail:getID() == id then
				mail = v_mail
			end
		end
		local rewardList = mail:getRewardList()
		for i,v_reward in ipairs(rewardList) do
			self.receivingRewardList[#self.receivingRewardList+1] = {m_itemId = v_reward.m_id,m_count = v_reward.m_num}
		end
	else

		self.receivingRewardList = {}
		for j,v_mail in ipairs(self:getMailList()) do
			local rewardList = v_mail:getRewardList()
			for i,v_reward in ipairs(rewardList) do
				if v_reward.m_type == 1 then --金币
					self.receivingRewardList[#self.receivingRewardList+1] = {m_itemId = -1,m_count = v_reward.m_num}
				elseif v_reward.m_type == 2 then --钻石
					self.receivingRewardList[#self.receivingRewardList+1] = {m_itemId = -2,m_count = v_reward.m_num}
				elseif v_reward.m_type == 3 then --道具
					self.receivingRewardList[#self.receivingRewardList+1] = {m_itemId = v_reward.m_id,m_count = v_reward.m_num}
				end
			end
		end
	end
	ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_AcceptMail_Req", params)
end

--发送邮件读取的请求
--index:number邮件的索引
function MailController:readMail(index)
	local mailList = self:getMailList()
	local mail = mailList[index]
	if mail and mail:getState()==1 then
		self:requestReadMail(mail:getID())
		sendMsg(MSG_NEW_COME_MAIL,-1)
		mail:setState(2)
	elseif mail == nil then
		print("查无此件")
	elseif mail and mail:getState()==2 then
		print("邮件已读")
	end
end
-- 邮件设为已阅读 非手动调用
--id:number邮件的id
function MailController:requestReadMail(id)
	ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_ReadMail_Rep", {id})
end

-- 打印邮件数据
--_tag：打印的tag
function MailController:dumpMail(_tag)
	local tag = _tag or "MailController:dumpMail"
	for i,v_mail in ipairs(GlobalMails) do
		v_mail:dump("MailController:dumpMail["..i.."]")
	end
end
-- 获取本地的邮件列表
-- return:table 包含model/mail 的table
function MailController:getMailList()
	return GlobalMails
end

function MailController:onDestory()
	print("MailController:onDestory")
	removeMsgCallBack(self, MSG_MAIL_UPDATE_SUCCESS)

	TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_GetMailList_Ack")
	TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_AcceptMail_Ack")
	TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_DeleteMail_Ack")

	
end

return MailController

