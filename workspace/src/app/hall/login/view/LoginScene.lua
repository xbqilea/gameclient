--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 登录场景


local SceneBase = require("app.hall.base.ui.SceneBase")
--local LoginController = require("app.hall.login.control.LoginController")
local LoginController = require("app.newHall.message.LoginController")
local CCLoadResAsync = require("app.hall.base.util.AsyncLoadRes")
local TotalResources = require("app.hall.config.code.MainLoadResConfig")
local DlgAlert = require("app.hall.base.ui.MessageBox")
require("app.hall.main.model.GameMsgUtil")
require("app.hall.main.control.RoomTotalController")
local scheduler = require("framework.scheduler")
local LoginScene = class("LoginScene", function ()
    return SceneBase.new()
end)

function LoginScene:ctor()	 
    self:myInit()
end

function LoginScene:myInit()
    package.loaded["app.hall.config.code.HallConfig"] = nil
    package.loaded["app.hall.base.network.HttpWrap"] = nil
    require("app.hall.config.code.HallConfig")
    package.loaded["app.hall.config.design.MilAppShowNodeCfg"] = nil

    g_LoginController = LoginController:getInstance()
	g_LoginController:bindLoginScene(self)
   
    ToolKit:registDistructor(self, handler(self, self.onDestory))
   
    self.loadResAsync = CCLoadResAsync.new()
end

function LoginScene:onEnter()
	print("--------LoginScene:onEnter--------------")
    
    addMsgCallBack(self, MSG_SHOW_ERROR_TIPS, handler(self,self.onReceiveErrorTips))
    addMsgCallBack(self, MSG_ACCOUNT_UNNORMAL, handler(self, self.unnormalCallback))
    addMsgCallBack(self, MSG_ACCOUNT_FROZEN, handler(self, self.frozenCallback))
    addMsgCallBack(self, MSG_MAINTANENCE_INFO, handler(self, self.showMaintanence))
    --addMsgCallBack(self, MSG_CHECK_SERVER_STATUS, handler(self, self.onCheckServerStatus))
    addMsgCallBack(self, MSG_ADD_LOADING , handler(self, self.onAddLoading))  
    addMsgCallBack(self, MSG_ROMOVE_LOADING , handler(self, self.onRemoveLoading))
      
    --g_LoginController:checkServerStatus(0)

   -- display.removeSpriteFramesWithFile("ui/p_service_center.plist", "ui/p_service_center.png")
--    package.loaded["app.hall.login.view.LoginLayer"] = nil
--    self.loginLayer = require("app.hall.login.view.LoginLayer").new(self)
   -- if  GlobalDefine.ISLOGOUT then
    package.loaded["app.newHall.rootLayer.LoginLayer"] = nil
    self.loginLayer = require("app.newHall.rootLayer.LoginLayer").new(self) 
    self:addChild(self.loginLayer, 100)
    --self.loginLayer:showLoginType()
    --self.loginLayer:showPakcageInfo(true)
    self.loginLayer:setMsgListener()
--    else
--        local logo = display.newSprite("platform/logo.png"):addTo(self)
--    end
     --   scheduler.performWithDelayGlobal(handler(self,self.doLogin), 1)    
    self:doLogin()

    -- if GlobalConf.LOCAL_LOG then
    --     local LogUtil = require("app.hall.base.log.Log")
    --     self:performWithDelay(function ()
    --         LogUtil:getInstance():copyLogs2SDCard()
    --     end, 1) 
    -- end
end

function LoginScene:isLoginScene()
    return true
end

-- 登录
function LoginScene:doLogin()
    -- 加载大厅资源
    self.loginLayer:setBottomVisible(true)
    self.loginLayer:setProgressLayoutVisible(false)
    self.loadResAsync:preloadResources(TotalResources, handler(self, self.onLoadComplete), handler(self, self.onLoading))
end

-- 登录成功, 进入主场景
function LoginScene:loginSuccess()
--    if (true) then
--        cc.Director:getInstance():replaceScene(require("app.test.MainScene").new())
--        return 
--    end

    if display.getRunningScene() == self and self.isLoadResCompleted then
		--UIAdapter:pop()
		--UIAdapter:pushScene("app.hall.main.view.MainScene")
		UIAdapter:replaceScene("app.hall.main.view.MainScene")
    end
end

-- 登录失败, 弹出提示
function LoginScene:onLoginFail()
    if not self.failDlg then
        self.failDlg = DlgAlert.showTipsAlert({title = "提示", tip = "登录服务器失败，请检查您的网络"})
        self.failDlg:setSingleBtn("确定", function ()
            TotalController:onExitApp()
        end)
        self.failDlg:enableTouch(false)
    end
end

-- 重新登录
function LoginScene:relogin()
    g_LoginController:start()
end

function LoginScene:onExit()
	print("------------LoginScene:onExit------------------")
    removeMsgCallBack(self, MSG_SHOW_ERROR_TIPS)
    removeMsgCallBack(self, MSG_ACCOUNT_UNNORMAL)
    removeMsgCallBack(self, MSG_MAINTANENCE_INFO)
    --removeMsgCallBack(self, MSG_CHECK_SERVER_STATUS)
    removeMsgCallBack(self, MSG_ADD_LOADING)
    removeMsgCallBack(self, MSG_ROMOVE_LOADING)
end

function LoginScene:onDestory()
    -- self.loadResAsync:removeResources(TotalResources) --pushScene改成replaceScene所以注释掉这行
end

function LoginScene:onReceiveErrorTips(msgName,_errorId)
    ToolKit:showErrorTip(_errorId)
end

--[[
function LoginScene:onCheckServerStatus(_msgName , _infor  )
    ToolKit:removeLoadingDialog()
    if _infor == nil  then
    --  
        TOAST("获取服务器信息失败！")
        g_LoginController.webConnectFailed = true
    elseif   _infor.CurrentStatus == 1 then
    --维护中       
        local _timeformat = "%H:%M"
        local _statTime = os.date(_timeformat ,  tonumber(_infor.StartMaintainingTime)  )  --"%m月%d日%H:%M"
        local _endTime = os.date(_timeformat ,  tonumber(_infor.EndMaintainingTime)  )
        local dlg = DlgAlert.showTipsAlert({title = "维护通知", tip = "游戏正在更新维护，预计维护时间：" .. _statTime .. "-" .. _endTime  , tip_size = 34})
        dlg:setSingleBtn(STR(37, 5), function ()
            TotalController:onExitApp()
        end)
        dlg:setBackBtnEnable(false)
        dlg:enableTouch(false)
        g_LoginController.webConnectFailed = true
    elseif _infor.CurrentStatus == -1   then
        print("***********维护配置错误*******")
        TOAST("维护配置错误")
        
    elseif _infor.CurrentStatus == 0  then
        --服务器正常  但是连接失败
        if g_LoginController.serverConnectFailed == true then
            self:onLoginFail()
        end
        g_LoginController.webConnectFailed = false
    end

end
--]]


-- 加载资源完成回调
function LoginScene:onLoadComplete()
    print("LoginScene:onLoadComplete()")

    self.loginLayer:showProgress(100)
    self.loginLayer:showBigTipsStr("")
   -- self.loginLayer:showBigTipsStr("加载中，请稍候......")
    self.isLoadResCompleted = true
    --local flag = cc.UserDefault:getInstance():getStringForKey("First_Login")
	--if  flag == "" then 
	--	cc.UserDefault:getInstance():setStringForKey("First_Login",1)
	--	cc.UserDefault:getInstance():flush() 
	--	return
    --end
	
	local loginType = ToolKit:getLoginType()
	if loginType == GlobalDefine.LoginType.none then
		return
	end
	
    if not GlobalDefine.ISLOGOUT then
         self:beginLogin()
    end
end

function LoginScene:beginLogin()
    print("ToolKit:getLoginType():", ToolKit:getLoginType())
    if ToolKit:getLoginType() == GlobalDefine.LoginType.none then
        g_LoginController:login()
    elseif ToolKit:getLoginType() == GlobalDefine.LoginType.wx  then
		g_LoginController:startByWx()
    elseif ToolKit:getLoginType() == GlobalDefine.LoginType.qka  and  not GlobalConf.SHOW_DEBUG_SERVER then
        g_LoginController:login()
    end
end

--增加loading界面
function LoginScene:onAddLoading(msgName,durTime, text)  
    if durTime and text then
        ToolKit:addLoadingDialog(durTime, text)
    else
        ToolKit:addLoadingDialog(15)
    end
end

function LoginScene:onRemoveLoading()  
    ToolKit:removeLoadingDialog()   
end


-- 加载资源进度回调
function LoginScene:onLoading( __total, __current, __name )
    print("加载资源进度回调----------->>>>")
    print("全部" .. __total .. "个文件需加载, 当前正在加载第" .. __current .. "个文件: " .. __name)
    self.loginLayer:showProgress(string.format("%2d", 100 * __current / __total))
    --self.loginLayer:showBigTipsStr("加载中，请稍候......")
end

function LoginScene:addSelectServerDlg()
    local dlg = require("app.hall.login.view.TestLogin").new(self)
    dlg:showDialog()
end

function LoginScene:unnormalCallback( msgName, __info )
    self:accountUnnormalCallback(msgName, __info)
end

function LoginScene:frozenCallback(msgName,__info)
    local frozenReason = -305 --platformErrCode -305是账号冻结
    local number = getPublicInfor("phonenub")
    --local str = STR(86,4) .. number
    local str = STR(86,4)
    local kickDialog = DlgAlert.new()
    local data = {tip = str, tip_size = 28, areaSize = cc.size(540, 250)}
    local dlg = kickDialog.customTipsAlert(data)
    dlg:enableTouch(nil)
  --[[  dlg:setBtnAndCallBack( STR(76, 1), STR(87, 4), 
    function ()
        TotalController:onExitApp()
    end, 
    function ()
        ToolKit:showPhoneCall()
    end )--]]

    dlg:setSingleBtn(STR(5, 4), function()
        TotalController:onExitApp()
    end)

--    dlg:setTitle(STR(88, 4))
    -- local btn = ToolKit:getLabelBtn({str = number}, ToolKit.showPhoneCall)
    -- btn:setPosition(cc.p(-15, -10))
    -- dlg:setContent(btn)

    self:setBackEventFlag(false)
end

function LoginScene:showMaintanence(msgName,__info)
    local startTime = os.date("%m月%d日%H:%M",__info.m_nStart)
    local finishTime = os.date("%m月%d日%H:%M",__info.m_nFinish)
    local str = STR(85, 4)..startTime.."~"..finishTime
    local dlg = DlgAlert.showTipsAlert({title = "", tip = str, tip_size = 34})
        dlg:setSingleBtn(STR(37, 5), function ()
            TotalController:onExitApp()
        end)
    dlg:setBackBtnEnable(false)
    dlg:enableTouch(false)
	g_isAllowReconnect = false		--不允许重连
    --被顶号后关闭断线检测
    ConnectionUtil:setCallback(function ( network_state )
        
    end)
    TotalController:stopToSendHallPing()
end

--[[
function g_exitGame()
    print("TotalScene:exitGame()")
    if device.platform == "ios" then
        os.exit()
    elseif  device.platform == "android"  then
        cc.Director:getInstance():endToLua()
    end
end
--]]

return LoginScene
