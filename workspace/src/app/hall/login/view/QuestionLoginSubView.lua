-- Author: 
-- Date: 2018-07-27 11:42:15
-- To change this template use File | Settings | File Templates.
-- 问题验证选项上拉框

local XbDialog = require("app.hall.base.ui.CommonView")
local QuestionPickUpDialog = class("QuestionPickUpDialog", function()
    return XbDialog.new()
end)

local Que_Type = {
    one = 1,
    two = 2,
    tir = 3,
    four = 4
}

function QuestionPickUpDialog:ctor(params)
    self:myInit(params)
    self:setupViews()
end

function QuestionPickUpDialog:myInit(params)
    ToolKit:registDistructor( self, handler(self, self.onDestroy))
    self._data = params
    self._list = params.list
    self._type = params.type
    self._callback = params.callback
    self._closeCallback = params.closeCallback
    if self._type == nil then
        self._type = Que_Type.one
    end
    self:setCloseCallback( function ()
        if self._closeCallback then
            self._closeCallback()
        end
    end )
end

function QuestionPickUpDialog:onDestroy()

end

function QuestionPickUpDialog:setupViews()
    self.node = UIAdapter:createNode("csb/bag/area_pick_up.csb"):addTo(self)
    UIAdapter:adapter(self.node, handler(self, self.onTouchCallback))
    if self._type == Que_Type.one then
        self:updateViewsForQueOne()
    else
        self:updateViewsOther()
    end
    if self._data.pageData then
        self:updatePage(self._data.pageData)
    end
end

function QuestionPickUpDialog:updateViewsForQueOne()
    self.pageViewMon = self.node:getChildByName("PageView_1")
    self.pageViewMon:removeAllPages()
    for i=1, 12 do
        local page = ccui.Layout:create()
        page:setContentSize(self.pageViewMon:getContentSize())
        local areaNameLabel = display.newTTFLabel(
            {
                text=i,
                fontSize = 36,
                color = cc.c3b(0, 0, 0),
                font = "ttf/jcy.TTF",
            }):addTo(page)
        :align(display.CENTER, page:getContentSize().width*0.5, page:getContentSize().height*0.5)
        areaNameLabel:setRotation(-90)
        self.pageViewMon:addPage(page)
    end

    self.pageViewDay = self.node:getChildByName("PageView_2")
    self.pageViewDay:removeAllPages()
    for i=1, 31 do
        local page = ccui.Layout:create()
        page:setContentSize(self.pageViewMon:getContentSize().width,self.pageViewMon:getContentSize().height)
        local areaNameLabel = display.newTTFLabel(
            {
                text=i,
                fontSize = 36,
                color = cc.c3b(0, 0, 0),
                font = "ttf/jcy.TTF",
            }):addTo(page)
        :align(display.CENTER, page:getContentSize().width*0.5, page:getContentSize().height*0.5)
        areaNameLabel:setRotation(-90)
        self.pageViewDay:addPage(page)
    end

    self.pageViewHour = self.node:getChildByName("PageView_3")
    self.pageViewHour:removeAllPages()
    for i=1,#self._list do
        local page = ccui.Layout:create()
        page:setContentSize(self.pageViewHour:getContentSize().width,self.pageViewMon:getContentSize().height)
        local areaNameLabel = display.newTTFLabel(
            {
                text=self._list[i],
                fontSize = 36,
                color = cc.c3b(0, 0, 0),
                font = "ttf/jcy.TTF",
            }):addTo(page)
        :align(display.CENTER, page:getContentSize().width*0.5, page:getContentSize().height*0.5)
        areaNameLabel:setRotation(-90)
        self.pageViewHour:addPage(page)
    end

    local function delay()
        self.pageViewMon:addEventListener(handler(self, self.onMonSelected))
    end
    self:performWithDelay(delay, 0.5)
end

function QuestionPickUpDialog:onMonSelected()
    local index = self.pageViewMon:getCurPageIndex() + 1
    self.pageViewDay:removeAllPages()
    local max = 31
    if index == 2 then
        max = 28
    elseif index == 4 or index == 9 or index == 6 or index == 11 then
        max = 30
    end
    for i=1, max do
        local page = ccui.Layout:create()
        page:setContentSize(self.pageViewMon:getContentSize())
        local areaNameLabel = display.newTTFLabel(
            {
                text=i,
                fontSize = 36,
                color = cc.c3b(0, 0, 0),
                font = "ttf/jcy.TTF",
            }):addTo(page)
        :align(display.CENTER, page:getContentSize().width*0.5, page:getContentSize().height*0.5)
        areaNameLabel:setRotation(-90)
        self.pageViewDay:addPage(page)
    end
end

function QuestionPickUpDialog:updateViewsOther()
    self.pageView1 = self.node:getChildByName("PageView_1")
    self.pageView2 = self.node:getChildByName("PageView_2")
    self.pageView3 = self.node:getChildByName("PageView_3")
    self.pageView1:setVisible(false)
    self.pageView3:setVisible(false)
    self.pageView2:removeAllPages()
    for i=1,#self._list do
        local page = ccui.Layout:create()
        page:setContentSize(self.pageView2:getContentSize())
        local areaNameLabel = display.newTTFLabel(
            {
                text=self._list[i],
                fontSize = 36,
                color = cc.c3b(0, 0, 0),
                font = "ttf/jcy.TTF",
            }):addTo(page)
        :align(display.CENTER, page:getContentSize().width*0.5, page:getContentSize().height*0.5)
        areaNameLabel:setRotation(-90)
        self.pageView2:addPage(page)
    end
end

function QuestionPickUpDialog:updatePage(data)
    if self._type == Que_Type.one then
        self.pageViewMon:scrollToPage(data.pageList[1] or 0)
        self.pageViewDay:scrollToPage(data.pageList[2] or 0)
        self.pageViewHour:scrollToPage(data.pageList[3] or 0)
    else
        self.pageView2:scrollToPage(data.pageIdx or 0)
    end
end

function QuestionPickUpDialog:onTouchCallback(sender)
    local name = sender:getName()
    if name == "close_btn" then
        self:closeDialog()
    elseif name == "confirm_btn" then
        if self._callback then
            if self._type == Que_Type.one then
                local data = {}
                data.mon = self.pageViewMon:getCurPageIndex() + 1
                data.day = self.pageViewDay:getCurPageIndex() + 1
                local idx = self.pageViewHour:getCurPageIndex() + 1
                data.hour = self._list[idx]
                local data1 = {}
                data1.data = data
                data1.pageList = {
                    self.pageViewMon:getCurPageIndex(),
                    self.pageViewDay:getCurPageIndex(),
                    self.pageViewHour:getCurPageIndex()
                }
                self._callback(data1)
            else
                local index = self.pageView2:getCurPageIndex() + 1
                local str = self._list[index]
                local data = {}
                data.str = str
                data.page = self.pageView2:getCurPageIndex()
                self._callback(data)
            end
        end
        self:closeDialog()
    end
end

function QuestionPickUpDialog:closePickUpDialog()
    if self._closeCallback then
        self._closeCallback()
    end
    self:closeDialog()
end

return QuestionPickUpDialog
