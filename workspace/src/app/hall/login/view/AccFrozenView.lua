--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- To change this template use File | Settings | File Templates.
-- 账号冻结提示弹出框

local XbDialog = require("app.hall.base.ui.CommonView")

local FreezeTipsDialog = class("FreezeTipsDialog", function ()
    return XbDialog.new()
end)

function FreezeTipsDialog:ctor( phone )
    self:myInit(phone)
    self:setupViews()
end

function FreezeTipsDialog:myInit( phone )
    self._phone_number = phone
    self._hasPhone = phone ~= ""

    ToolKit:registDistructor(self, handler(self, self.onDestory))
    addMsgCallBack(self, MSG_FREEZE_CLOSE, handler(self, self.closeDialog))

end

function FreezeTipsDialog:onDestory()
    removeMsgCallBack(self, MSG_FREEZE_CLOSE)
end

function FreezeTipsDialog:setupViews()
    self.node = UIAdapter:createNode("csb/common/freeze_tips_layer.csb")
    self:addChild(self.node)
    UIAdapter:adapter(self.node, handler(self, self.onTouchCallback))

    self:updateViewForPhone()
    self:enableTouch(false)
end

function FreezeTipsDialog:updateViewForPhone()
    local call = self.node:getChildByName("phone_call")
    local ver = self.node:getChildByName("phone_ver")
    local num = 2
    local bool = false
    local y = 155
    if self._hasPhone == true then
        num = 3
        y = 108
        bool = true
        local label = ver:getChildByName("txt_btn")
        UIAdapter:bindingLabelBtn(label, handler(self, self.phoneVerTouchCallback))
    end
    local call_txt = call:getChildByName("txt_3")
    local number_txt = call:getChildByName("txt_2")
    local number = getPublicInfor("phonenub")
    number_txt:setString(number)
    local str = num.. "." .. STR(2, 5)
    call_txt:setString(str)
    ver:setVisible(bool)
    call:setPositionY(y)
    local call_btn = call:getChildByName("txt_2")
    UIAdapter:bindingLabelBtn(call_btn, ToolKit.showPhoneCall)
end

function FreezeTipsDialog:phoneVerTouchCallback()
    local phoneVerDialog = require("app.hall.login.view.PhoneNumLoginView")
    phoneVerDialog.new({type = 2, phone = self._phone_number}):showDialog();
end

-- 点击事件回调
function FreezeTipsDialog:onTouchCallback( sender )
    local name = sender:getName()
    if name == "btn_close" then
        self:closeDialog()
        TotalController:onExitApp()
    end
end

return FreezeTipsDialog