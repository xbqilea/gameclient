--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- To change this template use File | Settings | File Templates.
-- 冻结异常登录控制类

local FreezeVerifyController = class("FreezeVerifyController")

function FreezeVerifyController:ctor()
    self:myInit()
    self.isOpenView = false

    --- 相关界面路径
    self.freezeTipsPath = "app.hall.login.view.AccFrozenView"
    self.phoVerPath = "app.hall.login.view.PhoneNumLoginView"
    self.queVerPath = "app.hall.login.view.QuestionLoginView"
end

function FreezeVerifyController:myInit()
    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_ExceptionVerifyAck", handler(self, self.netMsgHandler))
    TotalController:registerNetMsgCallback(self, Protocol.LoginServer, "CS_L2C_ResetPassword_Ack", handler(self, self.netMsgHandler))
end

--- 发送验证
--- 包括验证码、问题、身份证验证通用接口
function FreezeVerifyController:sendVerifyStr( str )
    ConnectManager:send2Server(Protocol.LoginServer, "CS_C2L_ExceptionVerifyReq", {str})
end

--- 重置密码
function FreezeVerifyController:resetPassword( pwd )
    ConnectManager:send2Server(Protocol.LoginServer, "CS_C2L_ResetPassword_Req", {pwd})
end

function FreezeVerifyController:netMsgHandler( __idStr, __info )
    if __idStr == "CS_L2C_ExceptionVerifyAck" then
        sendMsg(MSG_VERIFY_NOTICE, __info)

    elseif __idStr == "CS_L2C_ResetPassword_Ack" then
        sendMsg(MSG_RESET_PASSWD_NOTICE, __info)
    end
end

function FreezeVerifyController:updateOpenView(flag)
    self.isOpenView = flag
end

function FreezeVerifyController:getOpenViewFlag()
    return self.isOpenView
end

--- 设置相关界面路径
function FreezeVerifyController:setFreezeVerifyPath( freezeTipsPath, phoVerPath, queVerPath )
    self.freezeTipsPath = freezeTipsPath or "app.hall.login.view.AccFrozenView"
    self.phoVerPath = phoVerPath or "app.hall.login.view.PhoneNumLoginView"
    self.queVerPath = queVerPath or "app.hall.login.view.QuestionLoginView"
end

--- 获取界面路径
function FreezeVerifyController:getFreePath()
    return self.freezeTipsPath;
end

function FreezeVerifyController:getPhonePath()
    return self.phoVerPath;
end

function FreezeVerifyController:getQuestionPath()
    return self.queVerPath;
end

return FreezeVerifyController