--
--author lhj
--date 2018/8/15
--

local HorseLampUI = class("HorseLampUI", function()
	return display.newLayer()
end)

local ViewRichTextLabel = require("src.app.hall.base.ui.ViewRichTextLabel")

local Layer_Width = 520
local Layer_Height = 40

local playSpeed = 130

function HorseLampUI:ctor(data)
	self.m_Data  = data
    self:initDatas()
end

function HorseLampUI:initDatas()

	self.node = UIAdapter:createNode("csb/horse_lamp/horse_lamp_ui.csb")

	self.bg = self.node:getChildByName("bg")
    self.bg:setContentSize(Layer_Width + 100, Layer_Height)

	self.clippingNode = cc.ClippingRectangleNode:create()

	self.clippingNode:setClippingRegion(cc.size(Layer_Width, Layer_Height))
	self.clippingNode:setPositionX(60)

	if self.m_Data then
		local desc = self.m_Data:getm_content()
		self.content_Width = ccui.Text:create(desc, "sfzht.ttf", 26):getContentSize().width + 30
		self.content_Node  = ViewRichTextLabel.new(desc, self.content_Width, Layer_Height /2, nil , 26)
		self.content_Node:setPosition(cc.p(self.content_Width / 2 + Layer_Width, Layer_Height/2 +4))
		self.content_Node:addTo(self.clippingNode)
	end

	self.bg:addChild(self.clippingNode, 1)

	self:addChild(self.node)
	self.node:setAnchorPoint(cc.p(0.5, 0.5))

	self.node:setPosition(cc.p(display.cx - 190, display.top - 78))

	self:playBeginBgAction()
end

function HorseLampUI:playBeginBgAction()
	self.bg:setScaleY(0.1)
	local scale = cc.ScaleTo:create(0.3, 1, 1)
	local sequence = cc.Sequence:create(scale, cc.CallFunc:create(function()
		self:playAction()
	end))
	self.bg:runAction(sequence)
end

function HorseLampUI:playAction()

	local delay1 = cc.DelayTime:create(0.5)

	local distanceDis = 0

	if self.content_Width >= Layer_Width then
		distanceDis = self.content_Width + 40
	else
		distanceDis = Layer_Width +self.content_Width + 10
	end

	local time = distanceDis / playSpeed

	local action1 = cc.MoveTo:create(time, cc.p(self.content_Node:getPositionX() - distanceDis, self.content_Node:getPositionY()))
	local hide = cc.Hide:create()
	local delay2 = cc.DelayTime:create(0.5)
	local sequence = cc.Sequence:create(delay1, action1, hide, delay2, cc.CallFunc:create(function()
		self:playEndBgAction()
	end))

	self.content_Node:runAction(sequence)

end

function HorseLampUI:playEndBgAction()
	local scale = cc.ScaleTo:create(0.3, 1, 0.1)
	local sequence = cc.Sequence:create(scale, cc.CallFunc:create(function()
		local rollListDatas = GlobalBulletinController:getRollNoticeList()
		if #rollListDatas > 0 then
			performWithDelay(self, function()
				sendMsg(MSG_SHOW_HORSELAMP_DLG)
			end, 1)
		end
		self:removeSelf()
	end))
	self.bg:runAction(sequence) 
end

return HorseLampUI