-- Author: lhj
-- Date: 2018-08-07 18:17:10
-- RollNoticeController

local RollNoticeController = class("RollNoticeController")

local horseLampUI = require("src.app.hall.horseLamp.view.horseLampUI")

local scheduler = require("framework.scheduler")

--跑马灯---游戏 映射表
local Open_Scene_Type_Map = {
    {scene = "TotalScene", gameSceneId = 0},
    {scene = "CrazyCowEntrance", gameSceneId = 500000},
    {scene = "HappyCowGameScene", gameSceneId = 500001},
    {scene = "CowGameScene", gameSceneId = 500001},
    {scene = "LandLord", gameSceneId = 100000},
    {scene = "LandGameMainScene", gameSceneId = 100001},
    {scene = "FishingEntranceScene", gameSceneId = 200000},
    {scene = "FishingScene", gameSceneId = 200001},
    {scene = "FivepieceEntranceScene", gameSceneId = 700000},
    {scene = "FivepieceScene", gameSceneId = 700001},
    {scene = "MJEntranceScene", gameSceneId = 411000},
    {scene = "CCMJMainScene", gameSceneId = 400001},
}

function RollNoticeController:ctor()
    self:myInit()

    --当前场景
    self.currentscene = nil

    self.rootNode = nil

    self.bgColor = nil
    self.fontColor = nil
    self.fontSize = nil

end

function RollNoticeController:myInit()
    self.shcedeuler = scheduler.scheduleGlobal(handler(self, self.onCheckRollNotice), 2)
    addMsgCallBack(self, MSG_SHOW_HORSELAMP_DLG, handler(self, self.onCheckRollNotice))
end

--添加一个开赛提醒
-- @praram _noticeNode ( node )  消息节点
function RollNoticeController:addGameNotice( _noticeNode , _showTime   )
    self:setRootNode()
    self:addToRootNode ( _noticeNode,_showTime )
end

--检测跑马灯在不同场景(大厅，游戏)中播放
function RollNoticeController:isShowHorseLamp(_sceneName)
    --print("_sceneName", _sceneName)
    local isPlay = false
    local gameSceneId = nil
    for key, value in pairs(Open_Scene_Type_Map) do
        if value.scene == _sceneName then
            gameSceneId = value.gameSceneId
            break
        end
    end

   -- print("gameSceneId = ", gameSceneId)

    local g_rollNoticeList =   GlobalBulletinController:getRollNoticeList()
    local listData = g_rollNoticeList[1]:getGameList()

    --dump(listData, "listData", 10)

    for i=1, #listData do
        if listData[i] == gameSceneId then
            isPlay = true
            break
        end
    end

    return isPlay
end

--rollNoticeList[ #rollNoticeList +1  ] = { id = v_bulletin.m_id, content = detail.content, frequence = detail.frequence*60, cooltime = detail.frequence*60   } 
function RollNoticeController:onCheckRollNotice()

    local g_rollNoticeList =   GlobalBulletinController:getRollNoticeList()

    --print("跑马灯列表数量", #g_rollNoticeList)

    if table.nums(g_rollNoticeList) == 0 then return end

    local scene = cc.Director:getInstance():getRunningScene()

    --local isPlay = self:isShowHorseLamp(scene.__cname)
    local isPlay = true

    if isPlay == false then
        --print("---------------不播放")
        if g_rollNoticeList[1]:getPlayFrequence() <= 1 then
            
            table.remove(g_rollNoticeList, 1)
        else
            --调整播放顺序
            local list = g_rollNoticeList[1]
            --list:setPlayFrequence(list:getPlayFrequence() - 1)
            table.remove(g_rollNoticeList, 1)
            table.insert(g_rollNoticeList, list)
        end

        return
    end

    --if g_rollNoticeList[1]:getPlayFrequence() > 1 then
        --多次播放
        self:isPlayHorseEffect(false)

--[[        local localHorseLampInfo = cc.UserDefault:getInstance():getStringForKey(g_rollNoticeList[1]:getGuid(), "")

        print("localHorseLampInfo", localHorseLampInfo)--]]

        --[[if localHorseLampInfo == "" then
            --第一次保存
            cc.UserDefault:getInstance():setStringForKey(g_rollNoticeList[1]:getGuid(), "1")
            cc.UserDefault:getInstance():flush()  --flush保存

            self:isPlayHorseEffect(false)
        else
            if tonumber(localHorseLampInfo) == g_rollNoticeList[1]:getPlayFrequence() then
                cc.UserDefault:getInstance():setStringForKey(g_rollNoticeList[1]:getGuid(), "")
                cc.UserDefault:getInstance():flush()  --flush保存
                table.remove(g_rollNoticeList, 1)
                self:isPlayHorseEffect(false)
            else
                local count = tonumber(localHorseLampInfo) + 1
                cc.UserDefault:getInstance():setStringForKey(g_rollNoticeList[1]:getGuid(), tostring(count))
                cc.UserDefault:getInstance():flush()  --flush保存

                local list = g_rollNoticeList[1]

                table.remove(g_rollNoticeList, 1)

                table.insert(g_rollNoticeList, list)

                self:isPlayHorseEffect(false)
            end
        end--]]
    --else
        --一次性播放
       --self:isPlayHorseEffect(true)
    --end

end

function RollNoticeController:isPlayHorseEffect(_isRemove)

    local g_rollNoticeList =   GlobalBulletinController:getRollNoticeList()

    local scene = cc.Director:getInstance():getRunningScene()
    if scene:getChildByTag(555555) then
        return
    end

    local horseLampUI = horseLampUI.new(g_rollNoticeList[1])
    horseLampUI:setTag(555555)
    horseLampUI:setLocalZOrder(101)
    if scene:getName()=="TotalScene" then
        scene:addChild(horseLampUI)
    end
    

    if _isRemove then
        table.remove(g_rollNoticeList, 1)
    else
        local g_rollNoticeList =   GlobalBulletinController:getRollNoticeList()
        local list = g_rollNoticeList[1]
        --list:setPlayFrequence(list:getPlayFrequence() - 1)
        table.remove(g_rollNoticeList, 1)
        table.insert(g_rollNoticeList, list)
    end

end

--显示一个notice
function RollNoticeController:setRootNode()

    if self.currentscene == nil  then
        self.currentscene =  cc.Director:getInstance():getRunningScene()

        self.rootNode = cc.Node:create()

        self.currentscene:addChild(self.rootNode,0x00ffffff)

        self.rootNode:setPosition(0,display.height)


    elseif self.currentscene  and  self.currentscene ~= cc.Director:getInstance():getRunningScene()  then

        --        if self.currentscene and self.currentscene.isRunning and self.currentscene:isRunning() == false  then
        --            self.rootNode:removeFromParent()
        --            self.rootNode = nil
        --        end
        
        if self.rootNode and not tolua.isnull(self.rootNode)  then

            self.rootNode:removeFromParent()
            self.rootNode = nil
        end

        self.currentscene =  cc.Director:getInstance():getRunningScene()


        self.rootNode = cc.Node:create()
        self.currentscene:addChild(self.rootNode,0x00ffffff)
        self.rootNode:setPosition(0,display.height)

    elseif self.currentscene  and  self.currentscene == cc.Director:getInstance():getRunningScene() then
        --在当前场景
        if self.rootNode and self.rootNode.getParent   then
        --rootnode 还存在
        else
            self.rootNode = cc.Node:create()
            self.currentscene:addChild(self.rootNode,0x00ffffff)
            self.rootNode:setPosition(0,display.height)
        end
    end

end

function RollNoticeController:addToRootNode ( _notice , _showTime )

    local _childNums =  self.rootNode:getChildrenCount()

    local _height = 60 *_childNums


    self.rootNode:addChild(_notice)

    _notice:setPosition(0,_height)

    local function noticeSetHide()
        _notice:setVisible(false)
    end

    local _moveLenght = _notice:getNoticeSize().height

    local _action1 =  cc.MoveBy:create(0.5,cc.p(0, - _moveLenght ))

    self.rootNode:runAction(_action1)

    local _actionSpaw = cc.Spawn:create(  cc.MoveBy:create( 1,cc.p(0, _moveLenght ))  , cc.FadeOut:create( 1 )   )

    local _time = _showTime or 5
    local _action2 =  cc.Sequence:create( cc.DelayTime:create( _time) ,  _actionSpaw , cc.CallFunc:create(noticeSetHide ) )

    --  _notice:setCascadeOpacityEnabled(true)
    _notice:runAction(_action2)
end



--生成一个notice

--{  id = 11, content = "ssss" , frequence = 1, cooltime = 1   , bgColor = cc.c3b(byte,byte,byte) ,  fontColor = cc.c3b(byte,byte,byte) , fontsize = 32 }
function RollNoticeController:makeNotice( _noticeInfor )

    --    _loyout:setBackGroundColorType(1)
    --    _loyout:setBackGroundColorOpacity(100)

    local _notice =  require("app.hall.base.ui.RollNotice").new( _noticeInfor.content )

    if _noticeInfor.bgColor then
        _notice:setBgColor( self.bgColor )
    end

    if _noticeInfor.fontColor then
        _notice:setFontColor( self.fontColor )
    end

    if _noticeInfor.fontsize then
        _notice:setFontSize( self.fontSize )
    end

    _notice:setAnchorPoint(0,0)
    return _notice

end

function RollNoticeController:onDestory()
    print("RollNoticeController:onDestory")
    removeMsgCallBack(self, MSG_SHOW_HORSELAMP_DLG)
end

return RollNoticeController