local PromoteConfig = {}
local ImagePath = "hall/image/promote/"
local ImageButtonPath = "hall/image/promote/button/"


-- PromoteConfig.DOMAIN = "http://api.testxbqpthe.com/api/itf/proxyMember/"
PromoteConfig.DOMAIN = GlobalConf.DOMAIN.."api/itf/proxyMember/"

PromoteConfig.UICsb = "hall/PromoteUI.csb"

PromoteConfig.LabelList = {
    "LabelTGZQ",
    "LabelMyCommission",
    -- "LabelCommissionRecording",
    "LabelSubordinateReports",
    "LabelMember",
    "LabelGameHistory",
    "LabelAccountChange",
    "LabelPromoteHelp"
}
PromoteConfig.Labels = {
    -- {name="", normal = ImageButtonPath.."", disable = ImageButtonPath.."", view = "", script = ""},
    LabelTGZQ = {
        name="推广挣钱",
        normal = ImageButtonPath.."label_tgzq_f.png",
        disable = ImageButtonPath.."label_tgzq.png",
        view = "PanelTGZQ",
        script = "src.app.hall.PromoteUI.Script.PromoteTGZQ"
    },
    LabelMyCommission = {
        name="我的佣金",
        normal = ImageButtonPath.."label_wdyj_f.png",
        disable = ImageButtonPath.."label_wdyj.png",
        view = "PanelMyCommission",
        script = "src.app.hall.PromoteUI.Script.PromoteMyCommission"
    },
    LabelCommissionRecording = {
        name="佣金记录",
        normal = ImageButtonPath.."label_jxjl_f.png",
        disable = ImageButtonPath.."label_jxjl.png",
        view = "PanelCommissionRecording",
        script = "src.app.hall.PromoteUI.Script.PromoteCommissionRecording"
    },
    LabelSubordinateReports = {
        name="下级报表",
        normal = ImageButtonPath.."label_xjbb_f.png",
        disable = ImageButtonPath.."label_xjbb.png",
        view = "PanelSubordinateReports",
        script = "src.app.hall.PromoteUI.Script.PromoteSubordinateReports"
    },
    LabelMember = {
        name="直属成员",
        normal = ImageButtonPath.."label_zscy_f.png",
        disable = ImageButtonPath.."label_zscy.png",
        view = "PanelMember",
        script = "src.app.hall.PromoteUI.Script.PromoteMember"
    },
    LabelGameHistory = {
        name="游戏记录",
        normal = ImageButtonPath.."label_yxjl_f.png",
        disable = ImageButtonPath.."label_yxjl.png",
        view = "PanelGameHistory",
        script = "src.app.hall.PromoteUI.Script.PromoteGameHistory"
    },
    LabelAccountChange = {
        name="账变记录",
        normal = ImageButtonPath.."label_zbjl_f.png",
        disable = ImageButtonPath.."label_zbjl.png",
        view = "PanelAccountChange",
        script = "src.app.hall.PromoteUI.Script.PromoteAccountChange"
    },
    LabelPromoteHelp = {
        name="代理介绍",
        normal = ImageButtonPath.."label_dljs_f.png",
        disable = ImageButtonPath.."label_dljs.png",
        view = "PanelPromoteHelp",
        script = "src.app.hall.PromoteUI.Script.PromotePromoteHelp"
    },
    
}


PromoteConfig.classify =
{
    {id = 0, name = "全部", doc = ""},
    {id = 1, name = "保险箱", doc = "保险箱"},
    {id = 2, name = "充值提现", doc = "充值提现"},
    {id = 3, name = "活动奖励", doc = "活动奖励"},
    {id = 4, name = "邮件", doc = "邮件领取"},
    {id = 5, name = "BG", doc = "BG转入转出"},
    {id = 6, name = "彩票", doc = "彩票转入转出"},
    {id = 7, name = "棋牌", doc = "游戏输赢"},
}
PromoteConfig.detail =
{
    [0]= {name = '全部',classId = 0},

    [1801001]= {name = '转入保险箱',classId = 1},
    [1801002]= {name = '转出保险箱',classId = 1},
    
    [1501001]= {name = '平台充值',classId = 2},
    [2001016]= {name = '手动赠送',classId = 2},
    [2001008]= {name = '充值返利',classId = 2},
    [2001013]= {name = '首充奖励1',classId = 2},
    [2001014]= {name = '首充奖励2',classId = 2},
    [2001009]= {name = '撤销返利',classId = 2},
    [1501002]= {name = '充值异常1',classId = 2},
    [1501003]= {name = '充值异常2',classId = 2},
    [1601004]= {name = '兑换金币',classId = 2},
    [1601006]= {name = '直接兑换金币',classId = 2},
    [1601005]= {name = '兑换失败',classId = 2},
    
    [1308002]= {name = '转正奖励',classId = 3},
    [2001005]= {name = '转盘抽奖',classId = 3},
    [2001007]= {name = '签到奖励',classId = 3},
    [2001017]= {name = '昵称奖励',classId = 3},
    [2001030]= {name = '每日任务',classId = 3},
    [2001031]= {name = 'VIP晋级彩金',classId = 3},
    [2001032]= {name = 'VIP周礼金',classId = 3},
    [2001033]= {name = 'VIP月礼金',classId = 3},
    [2001034]= {name = 'VIP签到奖励加成',classId = 3},
    [2001035]= {name = '救济金',classId = 3},
    [2001036]= {name = '每日分享',classId = 3},
    [1310005]= {name = '推广返利',classId = 3},
    
    [1303001]= {name = '运营邮件',classId = 4},
    [1303002]= {name = '系统邮件',classId = 4},
    [1101001]= {name = '比赛奖励',classId = 4},

    [2001018]= {name = '从BG转出',classId = 5},
    [2001019]= {name = '向BG转入',classId = 5},
    [2001020]= {name = '撤销从BG转出',classId = 5},
    [2001021]= {name = '撤销向BG转入',classId = 5},
    
    [2001022]= {name = '转入彩票',classId = 6},
    [2001023]= {name = '转入彩票失败',classId = 6},
    [2001024]= {name = '转出彩票',classId = 6},
        
    [1001001]= {name = '游戏输赢',classId = 7},
    [1001003]= {name = '发红包',classId = 7},
    [1001004]= {name = '红包退还',classId = 7},
    [1101002]= {name = '取消报名',classId = 7},
    [1101006]= {name = '奖金池派奖',classId = 7},
    [1201001]= {name = '报名',classId = 7},	
    [1801006]= {name = '强退扣除',classId = 7},
    [1801007]= {name = '强退转出',classId = 7},
    [1801008]= {name = '转入游戏',classId = 7},
    [1801010]= {name = '转入游戏失败',classId = 7},
    [1801011]= {name = '游戏结束转出',classId = 7},
    --[1201002]= {name = '游戏房费',classId = 7},
    --[1201003]= {name = '玩家抽水',classId = 7},
    --[2001015]= {name = '扎金花特殊',classId = 7},
}



return PromoteConfig