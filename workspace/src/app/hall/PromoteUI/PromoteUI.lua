local PromoteConfig = require("src.app.hall.PromoteUI.PromoteConfig")
local PromoteUI = class("PromoteUI", function()
    return display.newLayer()
end)

function PromoteUI:onDestory()

end

function PromoteUI:ctor()
    self:setName("PromoteUI")
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    local node = UIAdapter:createNode(PromoteConfig.UICsb)
    self:addChild(node)
    node:setContentSize(display.size)
    node:setPosition(cc.p(0,0))
    self:adapterEx(node)

    self._nCurrentIndex = -1
    self.m_pRoot = node:getChildByName("ImageBackground")
    self.m_pPanelView = self.m_pRoot:getChildByName("PanelView")
    self.m_pListViewMenu = self.m_pPanelView:getChildByName("ListViewMenu")
    self.m_pButtonClose = self.m_pRoot:getChildByName("ButtonClose")

    local pImageTitle = self.m_pRoot:getChildByName("ImageTitle")
    local pTitle = CacheManager:addSpineTo(self.m_pRoot, "hall/image/promote/effect/tgzq/tgzq", 0, ".json", 1)
    pTitle:setAnimation(0, "animation", true)
    pTitle:setPosition(cc.p(pImageTitle:getPositionX(),pImageTitle:getPositionY() + 5))
    pImageTitle:setVisible(false)

    for index = 1, #PromoteConfig.LabelList do
        local _k = PromoteConfig.LabelList[index]
        local _v = PromoteConfig.Labels[_k]
        local button = ccui.ImageView:create(_v.normal, ccui.TextureResType.localType)
        button:setName(_k)
        button:setTag(index)
        button.pInfo = _v
        self.m_pListViewMenu:pushBackCustomItem(button)
        button:setTouchEnabled(true)
        button:addTouchEventListener(handler(self, self.onLabelClick))
    end
    UIAdapter:registClickCallBack(self.m_pButtonClose, handler(self, self.onCloseClick))
    self:_showPanelView(1)
end

function PromoteUI:onCloseClick()
    self:removeFromParent()
end

function PromoteUI:onLabelClick(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        local _index = sender:getTag()
        self:_showPanelView(_index)
    end
end

function PromoteUI:_showPanelView(_index)
    if _index ~= self._nCurrentIndex then
        if self._nCurrentIndex ~= -1 then
            local button = self.m_pListViewMenu:getChildByTag(self._nCurrentIndex)
            button:loadTexture(button.pInfo.normal, ccui.TextureResType.localType)
            button:setEnabled(true)
            button:setTouchEnabled(true)
            button.pView:hideView()
        end
        self._nCurrentIndex = _index
        local button = self.m_pListViewMenu:getChildByTag(_index)
        button:loadTexture(button.pInfo.disable, ccui.TextureResType.localType)
        button:setEnabled(false)
        button:setTouchEnabled(false)
        if nil == button.pView then
            local pNode = UIAdapter:createNode("res/hall/csb/promoteUI/PromoteUI_"..button.pInfo.view..".csb")
            pNode:setContentSize(display.size)
            self:adapterEx(pNode)
            local view = pNode:getChildByName("Panel")
            button.pView = require(button.pInfo.script).new(view)
            -- view:addChild(button.pView)
            self:addChild(button.pView)
            self.m_pPanelView:addChild(pNode)
        end
        button.pView:showView()
    end
end

function PromoteUI:adapterEx(_node)
    if nil == _node then
        return false
    end
    -- "@{"NAME":"","X":0,"Y":0,"WIDTH":0,"HEIGHT":0,"LEFT":0,"UP":0,"RIGHT":0,"DOWN":0}"
    local viewSize = _node:getContentSize()

    local list = _node:getChildren()
    for index = 1, #list do
        local pNode = list[index]
        local name = pNode:getName()
        local ca = string.sub( name, 1, 1 )
        if ca == '@' then
            local sJson = string.sub( name, 2 )
            local pJson = json.decode(sJson)
            pNode:setName(pJson.NAME)
            local nodeSize = pNode:getContentSize()
            local anchorPoint = pNode:getAnchorPoint()
            local point = cc.p(pNode:getPosition())
            local isRefret = false

            if pJson.WIDTH and (nil == pJson.LEFT or nil == pJson.RIGHT) then
                nodeSize.width = pJson.WIDTH * viewSize.width / 100
                isRefret = true
            end

            if pJson.HEIGHT and (nil == pJson.UP and nil == pJson.DOWN) then
                nodeSize.height = pJson.HEIGHT * viewSize.height / 100
                isRefret = true
            end

            if pJson.X and nil == pJson.LEFT and nil == pJson.RIGHT then
                point.x = pJson.X * viewSize.width / 100
                isRefret = true
            end

            if pJson.Y and nil == pJson.UP and nil == pJson.DOWN then
                point.y = pJson.Y * viewSize.height / 100
                isRefret = true
            end

            if pJson.LEFT and nil == pJson.RIGHT then
                point.x = pJson.LEFT + nodeSize.width * anchorPoint.x
                isRefret = true
            end

            if pJson.RIGHT and nil == pJson.LEFT then
                point.x = viewSize.width - pJson.RIGHT - nodeSize.width + nodeSize.width * anchorPoint.x
                isRefret = true
            end

            if pJson.UP and nil == pJson.DOWN then
                point.y = viewSize.height - pJson.UP - nodeSize.height + nodeSize.height * anchorPoint.y
                isRefret = true
            end

            if pJson.DOWN and nil == pJson.UP then
                point.y = pJson.DOWN + nodeSize.height * anchorPoint.y
                isRefret = true
            end

            if pJson.LEFT and pJson.RIGHT then
                nodeSize.width = viewSize.width - pJson.LEFT - pJson.RIGHT
                point.x = pJson.LEFT + nodeSize.width * anchorPoint.x
                isRefret = true
            end

            if pJson.UP and pJson.DOWN then
                nodeSize.height = viewSize.height - pJson.UP - pJson.DOWN
                point.y = pJson.DOWN + nodeSize.height * anchorPoint.y
                isRefret = true
            end
            if isRefret then
                local description = pNode:getDescription()
                if "ImageView" == description then
                    pNode:ignoreContentAdaptWithSize(false)
                end
                pNode:setContentSize(nodeSize)
                pNode:setPosition(point)
            end
        end

        self:adapterEx(pNode)
    end

    return true
end

function PromoteUI:close()
    self:removeFromParent()
end
return PromoteUI