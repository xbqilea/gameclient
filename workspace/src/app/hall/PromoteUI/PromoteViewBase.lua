local PromoteViewBase = class("PromoteViewBase", function()
    return display.newNode()
end)

function PromoteViewBase:ctor(node)
    self.m_pPanel = node
end

function PromoteViewBase:showView()
    -- if self.m_pPanel then
        self.m_pPanel:setVisible(true)
    -- end
end

function PromoteViewBase:hideView()
    -- if self.m_pPanel then
        self.m_pPanel:setVisible(false)
    -- end
    -- self:getParent():setVisible(false)
end

function PromoteViewBase:isVisibleView()
    return self.m_pPanel:isVisible()
end

function PromoteViewBase:showLoading(_parent, _time, _point, _handler)
    local pArmature = nil
    if nil == self.m_pLoadingLayer then
        self.m_pLoadingLayer =  ccui.Layout:create()
        self.m_pPanel:addChild(self.m_pLoadingLayer, 1000)
        self.m_pLoadingLayer:setBackGroundColorType(ccui.LayoutBackGroundColorType.solid)
        self.m_pLoadingLayer:setBackGroundColor(cc.c3b(0, 0, 0))
        self.m_pLoadingLayer:setBackGroundColorOpacity(120)
        self.m_pLoadingLayer:setTouchEnabled(true)
        self.m_pLoadingLayer:setContentSize(self.m_pPanel:getContentSize())
        self.m_pLoadingLayer:setAnchorPoint(cc.p(0, 0))
        self.m_pLoadingLayer:setPosition(cc.p(0, 0))

        pArmature = ToolKit:createArmatureAnimation("res/tx/", "hall_loading", nil )
        self.m_pLoadingLayer:addChild(pArmature)
        pArmature:setAnchorPoint(cc.p(0.5, 0.5))
        pArmature:setPosition(_point)
        self.m_pLoadingLayer.m_pArmature = pArmature
        pArmature:getAnimation():playWithIndex(0,-1,1)
    end

    self.m_pLoadingLayer:stopAllActions()
    self.m_pLoadingLayer:runAction(cc.Sequence:create(cc.DelayTime:create(_time), cc.CallFunc:create(function()
        self.m_pLoadingLayer:removeFromParent()
        self.m_pLoadingLayer = nil
        if _handler then
            _handler()
        end
    end), nil))
end

function PromoteViewBase:hideLoading()
    if self.m_pLoadingLayer then
        self.m_pLoadingLayer:stopAllActions()
        self.m_pLoadingLayer:removeFromParent()
        self.m_pLoadingLayer = nil
    end
end
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
function PromoteViewBase:initDateNode(_handler)
    self._nSelectDateIndex = 1
    self._hFindDateCall = _handler
    local pPanelDateNode = self.m_pPanel:getChildByName("PanelDate")

    local pPanelFindDateNode = pPanelDateNode:getChildByName("PanelDateBack")
    self._pTextDate = pPanelFindDateNode:getChildByName("TextDate")

    self._pPanelDateListNode = pPanelDateNode:getChildByName("PanelListNode")
    self._pPanelDateListView = self._pPanelDateListNode:getChildByName("PanelListView")
    
    -- self.m_pButtonFindDate = pPanelFindDateNode:getChildByName("ButtonFindDate")
    
    UIAdapter:registClickCallBack(pPanelFindDateNode, function () self._pPanelDateListNode:setVisible(true) end)
    UIAdapter:registClickCallBack(self._pPanelDateListNode, function () self._pPanelDateListNode:setVisible(false) end)
    local listTextName = {"全部", "今天", "昨天", "本周", "本月"}
    local viewSize = self._pPanelDateListView:getContentSize()
    for index = 1, 5 do
        local item = self._pPanelDateListView:getChildByName("LabelDate"..index)
        item:setTag(index)
        item:setTouchEnabled(true)
        item:addTouchEventListener(handler(self, self._OnFindDateItemClick))
        viewSize.height = viewSize.height - item:getContentSize().height
        item:setPosition(cc.p(0, viewSize.height))
        viewSize.height = viewSize.height - 2
        item.pText = item:getChildByName("Text")
        item.pText:setString(listTextName[index])
    end

    self._pPanelDateListNode:setVisible(false)
end

function PromoteViewBase:setCurrentTime(_index, _isCall)
    local pChild = self._pPanelDateListView:getChildByTag(_index)
    if pChild then
        if self._nSelectDateIndex == _index then
            return
        end
        local item = self._pPanelDateListView:getChildByTag(self._nSelectDateIndex)
        item:setBackGroundColor(cc.c3b(0x4D, 0x4D, 0x4D))
    
        self._nSelectDateIndex = _index
        self._pPanelDateListNode:setVisible(false)
        pChild:setBackGroundColor(cc.c3b(0xE5, 0xE5, 0xE5))
        self._pTextDate:setString(pChild.pText:getString())
        if _isCall and self._hFindDateCall then
            self._hFindDateCall(_index)
        end
    end
end

function PromoteViewBase:_OnFindDateItemClick(sender, eventType)
    if ccui.TouchEventType.ended ~= eventType then
        return
    end
    local index = sender:getTag()
    self:setCurrentTime(index, true)
end

function PromoteViewBase:getCurrentTime()
    local pTime = nil
    local endTime = nil
    if 2 == self._nSelectDateIndex then
        -- 今天
        pTime = os.date("*t", os.time())
    elseif 3 == self._nSelectDateIndex then
        -- 昨天
        pTime = os.date("*t", os.time() - 24*60*60)
        endTime = clone(pTime)
        endTime.hour = 23
        endTime.min = 59
        endTime.sec = 59
    elseif 4 == self._nSelectDateIndex then
        -- 本周
        local timeTmp = os.date("*t", os.time())
        if timeTmp.wday == 1 then
            pTime = os.date("*t", os.time() - 7*24*60*60)
        else
            pTime = os.date("*t", os.time() - (timeTmp.wday - 1)*24*60*60)
        end
    elseif 5 == self._nSelectDateIndex then
        -- 本月
        pTime = os.date("*t", os.time())
        pTime.day = 1
    end
    if pTime then
        -- local dates = self:split(sTime ,"-")
        local nTime = os.time({year=pTime.year, month=pTime.month, day=pTime.day, hour=0, min=0, sec=1})
        if endTime then
            local nEndTime = os.time({year=endTime.year, month=endTime.month, day=endTime.day, hour=endTime.hour, min=endTime.min, sec=endTime.sec})
            return nTime, nEndTime 
        else
            return nTime
        end
    else
        return 0
    end
end
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
function PromoteViewBase:initFindMemeber(_handler)
    self.m_nUserID = -1
    self._hFindMemberCall = _handler
    local pFindBack = self.m_pPanel:getChildByName("PanelFindMember")
    local pTextFieldFindID = pFindBack:getChildByName("TextFieldFindID")
    local pButtonFindID = pFindBack:getChildByName("ButtonFindID")

    self._pEditBoxFindID = self:createFindMemberEditBox(pFindBack, pTextFieldFindID)

    UIAdapter:registClickCallBack(pButtonFindID, handler(self, self._OnFindMemeberIDClick))
end

function PromoteViewBase:setFindUserID(_memberId)
    self.m_nUserID = _memberId
    if _memberId > 0 then
        self._pEditBoxFindID:setText(self.m_nUserID.."")
    else
        self._pEditBoxFindID:setText("")
    end    
end

function PromoteViewBase:_OnFindMemeberIDClick(sender)
    local sFindID = self._pEditBoxFindID:getText()
    local nId = -1
    if sFindID == nil or sFindID == "" then
        nId = -1
    else
        local id = tonumber(sFindID)
        if id ~= nil then
            nId = id
        else
            TOAST("请输入数字ID")
            return
        end
    end
    if nId ~= self.m_nUserID then
        self.m_nUserID = nId
        self._hFindMemberCall(self.nId)
    end
end
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
function PromoteViewBase:sendPost(_url, _json, _handler)
    local xhr = cc.XMLHttpRequest:new()
	-- xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
	xhr:setRequestHeader("Content-Type", "application/json")
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_JSON
    xhr:open("POST", _url)
    xhr:registerScriptHandler(function()
        if tolua.isnull(self) then return end
        if nil == xhr.response or "" == xhr.response then
            _handler(xhr.response, nil, false)
            return
        end
        print("PromoteViewBase:sendPost:::" .. xhr.response.."\n")
        local _json = json.decode(xhr.response)
        if _json["code"] ~= 0 then
            _handler(xhr.response, _json, false)
            return
        end
        _handler(xhr.response, _json, true)
    end)
    xhr:send(json.encode(_json))
end

function PromoteViewBase:sendGet(_url, _handler)
    local xhr = cc.XMLHttpRequest:new()
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
    xhr:open("GET", _url)
    xhr:registerScriptHandler(function()
        if tolua.isnull(self) then return end
        if nil == xhr.response or "" == xhr.response then
            _handler(xhr.response, nil, false)
            return
        end
        print("PromoteViewBase:sendGet:::" .. xhr.response.."\n")
        local _json = json.decode(xhr.response)
        if _json["code"] ~= 0 then
            _handler(xhr.response, _json, false)
            return
        end
        _handler(xhr.response, _json, true)
    end)
    xhr:send()
end

function PromoteViewBase:split(str,reps)
    local resultStrList = {}
    string.gsub(str,'[^'..reps..']+',function ( w )
        table.insert(resultStrList,w)
    end)
    return resultStrList
end

function PromoteViewBase:array_concat(t1, ...)
    local retArrar = t1
    for index = 1, arg.n do
        local tmp = arg[index]
        if (type(tmp) == "table") then
            for i = 1, #tmp do
                table.insert(retArrar, tmp[i])
            end
        else
            table.insert(retArrar, tmp)
        end
    end
    return retArrar
end

function PromoteViewBase:createEditBox(maxLength, keyboardReturnType, inputMode, inputFlag, tag, size, placestr)
    -- local sprite1 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    -- local sprite2 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")
    -- local sprite3 = ccui.Scale9Sprite:createWithSpriteFrameName("hall/plist/hall/gui-texture-null.png")

    local sprite1 = ccui.Scale9Sprite:create("hall/image/image_opacity.png")
    local sprite2 = ccui.Scale9Sprite:create("hall/image/image_opacity.png")
    local sprite3 = ccui.Scale9Sprite:create("hall/image/image_opacity.png")

    size = cc.size(size.width, size.height)
    local editBox = ccui.EditBox:create(size, sprite1, sprite2, sprite3)
    editBox:setMaxLength(maxLength)
    editBox:setReturnType(keyboardReturnType)
    editBox:setInputMode(inputMode)
    editBox:setInputFlag(inputFlag)
    editBox:setTag(tag)
    editBox:setFont("Helvetica", 24)
    -- editBox:setFont(UIAdapter.TTF_FZCYJ, 26)
    
    editBox:setFontColor(cc.c3b(0xFF, 0xFF, 0xFF))
    editBox:setPlaceHolder(placestr)
    editBox:setPlaceholderFontSize(24)
    editBox:setPlaceholderFontColor(cc.c3b(0x7A,0x7B,0x7D))
    -- editBox:setAnchorPoint(cc.p(0, 0))
    -- editBox:setPositionY(5)
    -- editBox:registerScriptEditBoxHandler(handler(self, self.onEditBoxClicked))
    return editBox
end

function PromoteViewBase:createFindMemberEditBox(_parent, _inputNode)
    local pBackSize = _parent:getContentSize()
    pBackSize.width = pBackSize.width - 111

    local default_length = 15
    local input_return   = cc.KEYBOARD_RETURNTYPE_DONE
    local input_mode     = cc.EDITBOX_INPUT_MODE_SINGLELINE
    local input_flag     = cc.EDITBOX_INPUT_FLAG_SENSITIVE
    local default_tag    = -1
    local default_size   = pBackSize
    local default_string = "请输入会员ID"

    local pEditBox = self:createEditBox(default_length, input_return, input_mode, input_flag, default_tag, default_size, default_string)
    pEditBox:setText("")
    pEditBox:setAnchorPoint(cc.p(_inputNode:getAnchorPoint()))
    pEditBox:setPosition(_inputNode:getPosition())
    pEditBox:addTo(_parent)

    _inputNode:setVisible(false)

    return pEditBox
end

function PromoteViewBase:numberToString(_number)
    local iN, fN = math.modf(_number * 100)

    return iN * 0.01
end

return PromoteViewBase