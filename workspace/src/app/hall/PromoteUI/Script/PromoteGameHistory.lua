-- 游戏记录
local PromoteUI = require("src.app.hall.PromoteUI.PromoteUI")
local PromoteConfig = require("src.app.hall.PromoteUI.PromoteConfig")
local PromoteViewBase = require("src.app.hall.PromoteUI.PromoteViewBase")

-- 游戏总记录
-------------------------------------------------------------------------
local GameListView = class("GameListView", function(node)
    return PromoteViewBase.new(node)
end)

function GameListView:ctor(node)
    self.m_nLoadingType = 0
    self:initDateNode(function()
        self:resetData()
        self:loadData()
    end)

    self:initFindMemeber(function()
        self:resetData()
        self:loadData()
    end)
    
    self.m_pImageNoData = node:getChildByName("ImageNoData")
    self.m_pItemTmp = node:getChildByName("ImageLabelTmp")
    self.m_pListView = node:getChildByName("ListView")
    local pImageTitleBack = node:getChildByName("ImageTitleBack")
    for index = 1, 8 do
        local pButtonTitle = pImageTitleBack:getChildByName("ButtonTile"..index)
        if index > 6 then
            local button = self.m_pItemTmp:getChildByName("Image"..index)
            button:setPosition(cc.p(pButtonTitle:getPosition()))
        else
            local pText = self.m_pItemTmp:getChildByName("Text"..index)
            pText:setPosition(cc.p(pButtonTitle:getPosition()))
        end
    end
    
    self.m_pItemTmp:setVisible(false)
    node:setVisible(true)
    self:setCurrentTime(2, false)
end

function GameListView:showView()
    self.m_pPanel:setVisible(true)
    self:loadData()
end

function GameListView:loadData(jumpType)
    if self.m_nLoadingType == 0 then
        self.m_nLoadingType = 1
        self.m_pListView:removeAllItems()
        self.m_pImageNoData:setVisible(false)
        self:showLoading(self.m_pPanel, 10, cc.p(self.m_pImageNoData:getPosition()), function() self.m_pImageNoData:setVisible(true) end)
        
        local postData = {}
        if self.m_nUserID ~= -1 then
            postData.memberId = self.m_nUserID
        end
    
        local nTime, nEndTime = self:getCurrentTime()
        if nTime > 0 then
            postData.start = nTime
            if nEndTime then
                postData["end"] = nEndTime
            end
        end
        
        self:sendPost( PromoteConfig.DOMAIN.."getGameScoreStats/"..Player:getAccountID(), postData, function(_response, _json, _state)
            self:hideLoading()
            self.m_nLoadingType = 0
            if not _state then
                -- self.m_nLoadingType = 0
                -- self.m_pImageNoData:setVisible(true)
                self.m_pInfos = {}
            else
                self.m_pInfos = _json.data
                if 0 == #self.m_pInfos then
                    -- self.m_nLoadingType = 0
                end
            end
            self:updateList()
        end)
    end
end

function GameListView:updateList()
    -- "game": "141", //游戏编码 
    -- "boards": 349, //总局数
    -- "scores": -3120460.0, //总得分 
    -- "players": 2, //玩家数量 
    -- "winBoards": 169, //获胜局数 
    -- "loseBoards": 180, //失败局数 
    -- "minScore": -1400000.0, //最小得分 
    -- "maxScore": 3600000.0, //最大得分 
    -- "avgScore": -8941.15 //平均得分
    self.m_pImageNoData:setVisible(#self.m_pInfos == 0)
    -- self.m_pListView:removeAllItems()
    for index = 1, #self.m_pInfos do
        local info = self.m_pInfos[index]
        local item = self:createItem(info)
        self.m_pListView:pushBackCustomItem(item)
    end

    self.m_pListView:refreshView()
    self.m_pListView:jumpToTop()
end

function GameListView:createItem(info)
    local item = self.m_pItemTmp:clone()
    item:setVisible(true)
    item:ignoreContentAdaptWithSize(false)
    item:setContentSize(self.m_pItemTmp:getContentSize())
    if type(info.game) ~= "number" then
        info.game = tonumber(info.game)
    end
    local name = getGameName(info.game)
    item:getChildByName("Text1"):setString(name)    -- 名称
    item:getChildByName("Text2"):setString(info.boards)    -- 玩家数
    item:getChildByName("Text3"):setString(info.winBoards)    -- 胜局
    item:getChildByName("Text4"):setString(info.loseBoards)    -- 败局
    item:getChildByName("Text5"):setString(self:numberToString(info.avgScore/100))    -- 平均分
    item:getChildByName("Text6"):setString(self:numberToString(info.scores/100))    -- 总分

    local buttonStartGame = item:getChildByName("Image7")   -- 进入游戏
    buttonStartGame:ignoreContentAdaptWithSize(false)
    buttonStartGame:setContentSize({width=120, height=44})
    buttonStartGame:addTouchEventListener(handler(self, self.onStartGameClick))
    buttonStartGame.nGameID = info.game

    local button = item:getChildByName("Image8")
    button:ignoreContentAdaptWithSize(false)
    button:setContentSize({width=84, height=44})
    button:addTouchEventListener(handler(self, self.onDetailClick))
    item.nGameID = info.game
    button.nGameID = info.game
    return item
end

function GameListView:onDetailClick(sender, eventType)
    if ccui.TouchEventType.ended ~= eventType then
        return
    end
    if self.mClickDetailCall then
        self.mClickDetailCall(sender.nGameID, self._nSelectDateIndex, self.m_nUserID)
    end
end

function GameListView:onStartGameClick(sender, eventType)
    if ccui.TouchEventType.ended ~= eventType then
        return
    end
    -- 进入游戏
    local nGameTypeID = sender.nGameID
    jumpToGame(nGameTypeID, self:getParent():getParent())
    -- local nClassify = getGameClassifyType(nGameTypeID)
    -- local nKindId = getGameSwitchDataKindId(nGameTypeID)
    -- local _IsUpdateGame = function(kindId)
    --     if RoomData:isGameExist(RoomData:getGameByBtnId(kindId)) then
    --         if RoomData:isGameUp2Date(RoomData:getGameByBtnId(kindId)) then -- 游戏已经是最新
    --             return true
    --         end
    --     end
    --     return false
    -- end

    -- if nClassify == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_CARD or nClassify == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_FISH then
    --     -- 视讯和彩票
    --     sendMsg(Hall_Events.MSG_OPNE_CLASSIFY_GAME_VIEW, nClassify)
    -- elseif nClassify == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_ARCADE or nClassify == G_CONSTANTS.GameClassifyType.GAME_CLASSIFY_TIGER then
    --     if _IsUpdateGame(nKindId) then
    --         local tab = getRoomList(nKindId)
    --         if #tab > 1 then
    --             sendMsg(Hall_Events.MSG_OPNE_CLASSIFY_GAME_VIEW, nClassify)
    --         else
    --             if isOpenGame(nKindId) then
    --                 enterSelectGame(nKindId, false)
    --             else
    --                 TOAST("游戏暂未开放")
    --             end
    --         end
    --     else
    --         sendMsg(Hall_Events.MSG_OPNE_CLASSIFY_GAME_VIEW, nClassify)
    --     end
    -- else
    --     sendMsg(Hall_Events.MSG_OPNE_CLASSIFY_GAME_VIEW, nClassify)
    -- end
end

function GameListView:resetData()
    self.m_pInfos = {}
end

-- 游戏详细记录
-------------------------------------------------------------------------
local GameHistoryView = class("GameHistoryView", function(node)
    return PromoteViewBase.new(node)
end)

function GameHistoryView:ctor(node)
    self.m_nGameID = 0
    self.m_nLoadingType = 0
    self.m_nDateIndex = 1
    self.m_nUserID = -1
    self:resetData()

    self:initDateNode(function()
        self:resetData()
        self:loadData(1)
    end)

    self:initFindMemeber(function()
        self:resetData()
        self:loadData(1)
    end)

    self.m_pImageNoData = node:getChildByName("ImageNoData")
    self.m_pItemTmp = node:getChildByName("ImageLabelTmp")
    self.m_pListView = node:getChildByName("ListView")
    local pTitleBack = node:getChildByName("ImageTitleBack")
    self.m_pButtonReturn = node:getChildByName("ButtonReturn")

    self.m_pItemTmp:setVisible(false)

    for index = 1, 7 do
        local button = pTitleBack:getChildByName("ButtonTile"..index)
        button:setTag(index)
        local pText = self.m_pItemTmp:getChildByName("Text".. index)
        pText:setPosition(cc.p(button:getPosition()))
    end

    UIAdapter:registClickCallBack(self.m_pButtonReturn, handler(self, self.onReturnClick))
end

function GameHistoryView:showView(nGameID, nDateIndex, nUserID)
    self.m_pPanel:setVisible(true)
    local _nGameID = nGameID or self.m_nGameID
    local _nDateIndex = nDateIndex or self._nSelectDateIndex
    local _nUserID = nUserID or self.m_nUserID
    if self.m_nGameID ~= _nGameID or _nDateIndex ~= self._nSelectDateIndex or _nUserID ~= self.m_nUserID or self.m_nCount == 0 then
        self.m_nGameID = _nGameID
        self:setCurrentTime(_nDateIndex, false)
        self:setFindUserID(_nUserID or self.m_nUserID)
        self:resetData()
        self.m_pListView:removeAllItems()
        self:loadData(1)
    end
end

function GameHistoryView:onReturnClick()
    if self.mClickReturnCall then
        self.mClickReturnCall()
    end
end

function GameHistoryView:updateList(jumpType)
    self.m_pImageNoData:setVisible(#self.m_pInfos == 0)
    self.m_pListView:removeAllItems()
    for index = 1, #self.m_pInfos do
        local info = self.m_pInfos[index]
        info.index = index
        local item = self:createItem(info)
        self.m_pListView:pushBackCustomItem(item)
    end

    if #self.m_pInfos > 0 and not self.m_bIsEnd then
        local item = self:createItem(nil, true)
        self.m_pListView:pushBackCustomItem(item)
    end

    self.m_pListView:refreshView()
    if 1 == jumpType then
        self.m_pListView:jumpToTop()
    end
end

function GameHistoryView:createItem(info, isEnd)
    -- "createAt": "2020-05-05 16:51:35", 
    -- "id": "158426335810258244438", 
    -- "userID": 1025824, //玩家id 
    -- "nickName": "1025824", //玩家昵称 
    -- "score": 1900, //本局得分 
    -- "game": "201", //游戏编码 
    -- "room": "001", //房间 
    -- "gameTypeID": 201001, //游戏最小类型id 
    -- "startTime": 1584263330, //开始时间 
    -- "endTime": 1584263358, //结束时间 
    -- "recordID": "158426333010029", //牌局id 
    -- "channelNO": "1020001001", //渠道号 
    -- "terminalType": 1, //终端类别 
    -- "beforeScore": 2182716, //之前金币 
    -- "afterScore": 2184616, //之后金币 
    -- "m_path": [//父级队列 "1025742", "1025824" ],
    -- "deep": 1, //父级深度 
    -- "pid": 1025742 //上级id
    local itemSize = self.m_pItemTmp:getContentSize()
    local item = self.m_pItemTmp:clone()
    item:setVisible(true)
    item:ignoreContentAdaptWithSize(false)
    item:setContentSize(itemSize)
    if isEnd then
        for index = 2, 7 do
            local pText = item:getChildByName("Text"..index)
            pText:setVisible(false)
        end
        local pText = item:getChildByName("Text1")
        pText:setString("显示更多")
        pText:setPosition(cc.p(itemSize.width/2, itemSize.height/2))
        pText:setColor(cc.c3b(0xE5,0xE5, 0xE5))
        item:setTouchEnabled(true)
        item:addTouchEventListener(function(sender, eventType)
            if ccui.TouchEventType.ended == eventType then
                self:loadData()
            end
        end)
    else
        item:getChildByName("Text1"):setString(info.index)
        item:getChildByName("Text2"):setString(info.userID)
        item:getChildByName("Text3"):setString(info.deep)
        item:getChildByName("Text4"):setString(info.recordID)
        item:getChildByName("Text5"):setString(info.room)
        item:getChildByName("Text6"):setString(self:numberToString( (info.afterScore - info.beforeScore) * 0.01 ) )
        item:getChildByName("Text7"):setString(os.date("%Y/%m/%d\n%H:%M:%S", info.endTime))
    end
    return item
end

function GameHistoryView:resetData()
    self.m_nCount = 0
    self.m_bIsEnd = false
    self.m_pInfos = {}
end

function GameHistoryView:loadData(jumpType)
    -- if self.m_nLoadingType ~= 0 then
    --     return
    -- end
    -- self.m_nLoadingType = 1
    self:showLoading(self.m_pPanel, 10, cc.p(self.m_pImageNoData:getPosition()), function() self.m_pImageNoData:setVisible(true) end)
    local postData = {game = self.m_nGameID, offset = self.m_nCount, limit = 10}

    if self.m_nUserID ~= -1 then
        postData.memberId = self.m_nUserID
    end

    local nTime, nEndTime = self:getCurrentTime()
    if nTime > 0 then
        postData.start = nTime
        if nEndTime then
            postData["end"] = nEndTime
        end
    end

    self:sendPost(PromoteConfig.DOMAIN.."getGameScoreDetails/"..Player:getAccountID(), postData, function(_response, _json, _state)
        -- self.m_nLoadingType = 0
        self:hideLoading()
        if not _state then
            self:updateList(jumpType)
        else
            self.m_nCount = self.m_nCount + #_json.data.records
            if self.m_nCount ~= 0 and self.m_nCount >= _json.data.total then
                self.m_bIsEnd = true
            end
            self:array_concat(self.m_pInfos, _json.data.records)
            table.sort(self.m_pInfos, function(table1, table2)
                local t1 = os.date("%Y%m%d%H%M%S", table1.endTime)
                local t2 = os.date("%Y%m%d%H%M%S", table2.endTime)
                return t1 > t2
            end)
            self:updateList(jumpType)
        end
    end)
end


-------------------------------------------------------------------------
local PromoteGameHistory = class("PromoteGameHistory", function(node)
    return PromoteViewBase.new(node)
end)

function PromoteGameHistory:onDestory()

end

function PromoteGameHistory:ctor(node)
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    self.m_pGameListView = GameListView.new(node:getChildByName("PanelGameListBack"))
    self.m_pGameHistoryView = GameHistoryView.new(node:getChildByName("PanelMemberListBack"))
    self:addChild(self.m_pGameListView)
    self:addChild(self.m_pGameHistoryView)

    self.m_pGameHistoryView:hideView()
    self.m_pGameListView:hideView()

    self.m_pGameHistoryView.mClickReturnCall = function()
        self.m_pGameListView:showView()
        self.m_pGameHistoryView:hideView()
    end
    self.m_pGameListView.mClickDetailCall = function(nGameID, nDateIndex, nUserID)
        self.m_pGameListView:hideView()
        self.m_pGameHistoryView:showView(nGameID, nDateIndex, nUserID)
    end

    self.m_pButtonOffList = node:getChildByName("ButtonOffList")
    self.m_pButtonOffList:setVisible(false)
end

function PromoteGameHistory:showView()
    self.m_pPanel:setVisible(true)
    if self.m_pGameListView:isVisibleView() then
        self.m_pGameListView:showView()
    elseif self.m_pGameHistoryView:isVisibleView() then
        self.m_pGameHistoryView:showView()
    else
        self.m_pGameListView:showView()
    end
end



return PromoteGameHistory