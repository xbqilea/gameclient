-- 下级报表
-- local PromoteUI = require("src.app.hall.PromoteUI.PromoteUI")
local PromoteConfig = require("src.app.hall.PromoteUI.PromoteConfig")
local PromoteViewBase = require("src.app.hall.PromoteUI.PromoteViewBase")
local PromoteSubordinateReports = class("PromoteSubordinateReports", function(node)
    return PromoteViewBase.new(node)
end)

function PromoteSubordinateReports:onDestory()

end

function PromoteSubordinateReports:ctor(node)
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    self.m_bIsInit = false
    self.m_nReqDataStatus = 0
    self.m_nReqDataListStatus = 0
    self.m_nDateIndex = 1
    self.m_nUserID = -1
    self:resetData()

    self:initDateNode(function()
        self:resetData()
        self:loadData(1)
    end)

    self:initFindMemeber(function()
        self:resetData()
        self:loadData(1)
    end)

    self.m_pPanelInfo = node:getChildByName("PanelInfo")
    self.m_pImageNoData = node:getChildByName("ImageNoData")

    self.m_pTitleBack = node:getChildByName("ImageTitleBack")
    self.m_pItemTmp = node:getChildByName("ImageLabelTmp")
    self.m_pListView = node:getChildByName("ListView")

    self.m_pButtonOffList = node:getChildByName("ButtonOffList")

    self.m_pItemTmp:setVisible(false)
    self.m_pButtonOffList:setVisible(false)
    self.m_pImageNoData:setVisible(false)

    for index = 1, 7 do
        local button = self.m_pTitleBack:getChildByName("ButtonTile"..index)
        button.m_pSortImage = button:getChildByName("TextTile"):getChildByName("ImageSort")
        button.m_nSortType = 0
        button:setEnabled(false)
        button.m_pSortImage:setVisible(false)
        local item = self.m_pItemTmp:getChildByName("Text"..index)
        item:setPosition(cc.p(button:getPosition()))
    end
end

function PromoteSubordinateReports:showView()
    self.m_pPanel:setVisible(true)
    self:loadData(1)
end

function PromoteSubordinateReports:updateList(jumpType)
    self.m_pImageNoData:setVisible(#self.m_pInfos == 0)
    self.m_pListView:removeAllItems()
    table.sort(self.m_pInfos, function(info1, info2)
        return info1.deep < info2.deep
    end)

    for _index = 1, #self.m_pInfos do
        local item = self:createItem(self.m_pInfos[_index])
        self.m_pListView:pushBackCustomItem(item)
    end

    if #self.m_pInfos > 0 and not self.m_bIsEnd then
        local item = self:createItem(nil, true)
        self.m_pListView:pushBackCustomItem(item)
    end

    self.m_pListView:refreshView()
    if jumpType and 1 == jumpType then
        self.m_pListView:jumpToTop()
    end
end

function PromoteSubordinateReports:createItem(_info, _isEnd)
    -- "captainId": 1025824, //队长id 
    -- "captainNickName": "1025824",//队长昵称 
    -- "immediateMembers": 1, //直属成员 
    -- "allMembers": 2, //所有成员 
    -- "totalRecharge": 0.0, //团队充值 
    -- "totalCashFee": 600.0, //团队提现 
    -- "totalStake": 2488065.0, //团队打码 
    -- "totalRebate": 0.0, //团队返佣 
    -- "deep": 1 //父级深度
    local itemSize = self.m_pItemTmp:getContentSize()
    local _item = self.m_pItemTmp:clone()
    _item:setVisible(true)
    _item:ignoreContentAdaptWithSize(false)
    _item:setContentSize(itemSize)
    if _isEnd then
        if self.m_bIsEnd then
            for index = 2, 7 do
                _item:getChildByName("Text1"):setVisible(false)
            end
            local pText = _item:getChildByName("Text1")
            pText:setString("显示更多")
            pText:setPosition(cc.p(itemSize.width/2, itemSize.height/2))
            pText:setColor(cc.c3b(0xE5,0xE5, 0xE5))
            _item:setTouchEnabled(true)
            _item:addTouchEventListener(function(sender, eventType)
                if ccui.TouchEventType.ended == eventType then
                    self:loadData()
                end
            end)
        end
    else
        _item:getChildByName("Text1"):setString(_info.captainId) -- 成员账号
        _item:getChildByName("Text2"):setString(_info.deep) -- 级数d
        _item:getChildByName("Text3"):setString(_info.allMembers) -- 人数
        _item:getChildByName("Text4"):setString(self:numberToString( _info.totalStake/100 )) -- 团队投注
        _item:getChildByName("Text5"):setString(self:numberToString( _info.totalRebate/100 )) -- 团队返佣
        _item:getChildByName("Text6"):setString( _info.totalRecharge ) -- 团队充值
        _item:getChildByName("Text7"):setString( _info.totalCashFee ) -- 团队提现
    end

    return _item
end

function PromoteSubordinateReports:resetData()
    self.m_nCount = 0
    self.m_bIsEnd = false
    self.m_pInfos = {}
end

function PromoteSubordinateReports:loadData(jumpType)
    local postData1 = {}

    local postData2 = { offset = self.m_nCount, limit = 10 }

    if self.m_nUserID and self.m_nUserID ~= -1 then
        postData2.memberId = self.m_nUserID
    end

    local nStartTime, nEndTime = self:getCurrentTime()
    if nStartTime > 0 then
        postData2.start = nStartTime
        postData1.start = nStartTime
        if nEndTime then
            postData1["end"] = nEndTime
            postData2["end"] = nEndTime
        end
    end

    if self.m_nReqDataStatus == 0 or self.m_nReqDataListStatus == 0 then
        self.m_pImageNoData:setVisible(false)
        self:showLoading(self.m_pPanel, 10, cc.p(self.m_pImageNoData:getPosition()),function()
            self.m_pImageNoData:setVisible(true)
        end)
    end

    if self.m_nReqDataStatus == 0 then
        self.m_nReqDataStatus = 1
        self:sendPost(PromoteConfig.DOMAIN.."getTeamMetrics/"..Player:getAccountID(), postData1, function(_response, _json, _state)
            self.m_nReqDataStatus = 0
            if self.m_nReqDataListStatus == 0 then
                self:hideLoading()
            end
            if _state then
                self.m_pTeamMetricsInfo = _json["data"]
                -- "immediateMembers": 1, //直属成员 
                -- "allMembers": 3, //所有成员 
                -- "totalRecharge": 0.0, //总充值 
                -- "totalCashFee": 600.0, //总提现 
                -- "totalStake": 1.2715386E7, //总打码 
                -- "totalRebate": 1144340.0 //总返佣
                local _setText = function(_parentName, _value)
                    local pText = self.m_pPanelInfo:getChildByName(_parentName):getChildByName("TextValue")
                    pText:setString(_value)
                end
                _setText("Text1", _json["data"].immediateMembers)
                _setText("Text2", self:numberToString( _json["data"].totalRecharge))
                _setText("Text3", self:numberToString( _json["data"].totalCashFee))
                _setText("Text4", _json["data"].allMembers)
                _setText("Text5", self:numberToString( _json["data"].totalStake/100))
                _setText("Text6", self:numberToString( _json["data"].totalRebate/100))
            end
        end)
    end
    if self.m_nCount == 0 then
        if self.m_nReqDataListStatus == 0 then
            self.m_nReqDataListStatus = 1
            self:sendPost(PromoteConfig.DOMAIN.."getSubordinateTeamMetrics/"..Player:getAccountID(), postData2, function(_response, _json, _state)
                self.m_nReqDataListStatus = 0
                if self.m_nReqDataStatus == 0 then
                    self:hideLoading()
                end
                if not _state then
                    self.m_bIsInit = false
                    self:updateList(jumpType)
                else
                    if _json.data.total == 0 then
                        self.m_bIsInit = false
                    end
                    self.m_nCount = self.m_nCount + #_json.data.records
                    if self.m_nCount >= _json.data.total then
                        self.m_bIsEnd = true
                    end
                    self:array_concat(self.m_pInfos, _json.data.records)
                    self:updateList(jumpType)
                end
            end)
        end
    end
end

return PromoteSubordinateReports