-- 账变记录
local PromoteConfig = require("src.app.hall.PromoteUI.PromoteConfig")
local PromoteViewBase = require("src.app.hall.PromoteUI.PromoteViewBase")
local PromoteAccountChange = class("PromoteAccountChange", function(node)
    return PromoteViewBase.new(node)
end)

function PromoteAccountChange:onDestory()
    
end

function PromoteAccountChange:ctor(node)
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    -- self.m_pPanel = node
    self.m_nLoadingType = 0
    self.m_nFindTypeID = 0
    self:resetData()

    self:initDateNode(function()
        self:resetData()
        self:loadData(1)
    end)

    self:initFindMemeber(function()
        self:resetData()
        self:loadData(1)
    end)

    self.m_pItemTmp = node:getChildByName("PaneItemTmp")
    self.m_pPanelTitleBack = node:getChildByName("PanelTitleBack")
    self.m_pListView = node:getChildByName("ListView")
    self.m_pImageNoData = node:getChildByName("ImageNoData")
    self.m_pButtonOffList = node:getChildByName("ButtonOffList")
    self.m_pPanelFindType = node:getChildByName("ImageType")
    self.m_pPanleTypeListBack = node:getChildByName("PanelFindType")

    self.m_pImageNoData:setVisible(false)
    self.m_pItemTmp:setVisible(false)
    self.m_pButtonOffList:setVisible(false)
    self.m_pPanleTypeListBack:setVisible(false)

    for index = 1, 7 do
        local button = self.m_pPanelTitleBack:getChildByName("Button"..index)
        button:setTag(index)
        button.mNSortType = 0
        button.mImageSort = button:getChildByName("Text1"):getChildByName("ImageSort")
        button:setEnabled(false)
        -- button:addTouchEventListener(handler(self, self.onSortClick))
        button.mImageSort:setVisible(false)
        local pText = self.m_pItemTmp:getChildByName("Text"..index)
        pText:setPosition(cc.p(button:getPosition()))

    end

    local listView =  self.m_pPanleTypeListBack:getChildByName("ListView")
    local itemTmp = listView:getChildByName("Label1")
    local createClassifyItem = function (info, tag)
        local item = itemTmp:clone()
        item:setTag(tag)
        item.mInfo = info
        local pText = item:getChildByName("Text")
        item:setBackGroundColor(cc.c3b(0x4D, 0x4D, 0x4D))
        -- if tag == (#PromoteConfig.classify + 1) then
        --     pText:setString("其它")
        -- else
        if 0 == tag then
            pText:setString("全部")
            item:setBackGroundColor(cc.c3b(0xE5, 0xE5, 0xE5))
        else
            pText:setString(info.name)
        end
        item:setTouchEnabled(true)
        item:addTouchEventListener(handler(self, self.onItemFindTypeClick))
        return item
    end

    -- listView:pushBackCustomItem(createClassifyItem(nil, 0))
    for index = 1, #PromoteConfig.classify do
        local info = PromoteConfig.classify[index]
        local item = createClassifyItem(info, info.id)
        listView:pushBackCustomItem(item)
    end
    -- listView:pushBackCustomItem(createClassifyItem(nil, #PromoteConfig.classify + 1))
    listView:removeChild(itemTmp)
    UIAdapter:registClickCallBack(self.m_pPanelFindType, handler(self, self.onFindTypeClick))
    self.m_pPanleTypeListBack:setTouchEnabled(true)
    UIAdapter:registClickCallBack(self.m_pPanleTypeListBack, function()
        self.m_pPanleTypeListBack:setVisible(false)
    end)
    
end

function PromoteAccountChange:onFindTypeClick()
    self.m_pPanleTypeListBack:setVisible(true)
end

function PromoteAccountChange:onItemFindTypeClick(sender, eventType)
    if ccui.TouchEventType.ended ~= eventType then
        return
    end

    local tag = sender:getTag()
    if self.m_nFindTypeID == tag then
        return
    end
    self.m_pPanleTypeListBack:setVisible(false)
    local listView =  self.m_pPanleTypeListBack:getChildByName("ListView")
    if self.m_nFindTypeID ~= -1 then
        local item = listView:getChildByTag(self.m_nFindTypeID)
        item:setBackGroundColor(cc.c3b(0x4D, 0x4D, 0x4D))
    end

    self.m_nFindTypeID = tag
    sender:setBackGroundColor(cc.c3b(0xE5, 0xE5, 0xE5))
    self.m_pPanelFindType:getChildByName("Text"):setString(sender:getChildByName("Text"):getString())

    self:resetData()
    self:loadData(1)
end

function PromoteAccountChange:updateList(jumpType)
    local infos = self.m_pInfos
    -- if self.m_nUserID == -1  then
    -- if self.m_sFindID == "" then
    --     infos = self.m_pInfos
    -- else
    --     local nFindID = tonumber(self.m_sFindID)
    --     for index = 1, #self.m_pInfos do
    --         local info = self.m_pInfos[index]
    --         if info.userID == nFindID then
    --             table.insert(self.infos, info)
    --         end
    --     end
    -- end

    self.m_pImageNoData:setVisible(#infos == 0)

    table.sort(infos, function(info1, info2)
        local nTime1 = tonumber(os.date( "%Y%m%d%H%M%S", info1.recordTime))
        local nTime2 = tonumber(os.date( "%Y%m%d%H%M%S", info2.recordTime))
        return nTime1 > nTime2
    end)


    self.m_pListView:removeAllItems()
    for index = 1, #infos do
        local info = infos[index]
        local item = self:createItem(info)
        self.m_pListView:pushBackCustomItem(item)
    end
    if #infos > 0 and not self:isLoadEnd() then
        local item = self:createItem(nil, true)
        self.m_pListView:pushBackCustomItem(item)
    end
    self.m_pListView:refreshView()
    if jumpType and 1 == jumpType then
        self.m_pListView:jumpToTop()
    elseif jumpType and 2 == jumpType then
        self.m_pListView:jumpToBottom()
    end

end

function PromoteAccountChange:createItem(info, isEnd)
    local item = self.m_pItemTmp:clone()
    local itemSize = self.m_pItemTmp:getContentSize()
    item:setVisible(true)
    item:setContentSize(itemSize)
    if isEnd then
        for index = 2, 7 do
            local pText = item:getChildByName("Text"..index)
            pText:setVisible(false)
        end
        local pText = item:getChildByName("Text1")
        pText:setString("显示更多")
        pText:setPosition(cc.p(itemSize.width/2, itemSize.height/2))
        pText:setColor(cc.c3b(0xE5,0xE5, 0xE5))
        item:setTouchEnabled(true)
        item:addTouchEventListener(function(sender, eventType)
            if ccui.TouchEventType.ended == eventType then
                self:loadData()
            end
        end)
    else
        -- item:getChildByName("Text1"):setString(info.index)  -- 序号
        item:getChildByName("Text1"):setString(info.userID)  -- 会员ID
        item:getChildByName("Text2"):setString(info.deep)  -- 下线等级
        item:getChildByName("Text3"):setString(self:numberToString( info.beforeCount / 100) )  -- 变前金额
        item:getChildByName("Text4"):setString(self:numberToString( info.gainCount / 100 ))  -- 变动金额
        item:getChildByName("Text5"):setString(self:numberToString( info.afterCount / 100 ))  -- 变后金额

        local sDate = os.date( "%Y/%m/%d\n%H:%M:%S", info.recordTime)
        item:getChildByName("Text7"):setString(sDate)  -- 时间
        local pBusinessInfo = PromoteConfig.detail[info.businessType]
        if pBusinessInfo then
            if info.gameTypeID ~= 0 then
                local gameData = getGameSwitchData(info.gameTypeID)
                if gameData then
                    item:getChildByName("Text6"):setString("《"..gameData.name.."》\n"..pBusinessInfo.name)
                else
                    item:getChildByName("Text6"):setString(pBusinessInfo.name)
                end
            else
                item:getChildByName("Text6"):setString(pBusinessInfo.name)
            end
        else
            item:getChildByName("Text6"):setString("其它")
        end
    end
    return item
end


function PromoteAccountChange:showView()
    self.m_pPanel:setVisible(true)
    if self.m_isInit == nil then
        self.m_isInit = true
        self:loadData(1)
    end
end

function PromoteAccountChange:loadData(jumpType)
    if self:isLoadEnd() then
        -- self:updateList()
        return
    end

    if self:isLoadEnd(self.m_nCurrentIndex) then
        self:loadNext(jumpType)
        return
    end

    local ids = self.m_pDetailList[self.m_nFindTypeID]
    local pID = ids[self.m_nCurrentIndex]
    local postData = {deep = 0, offset = pID.nCount, limit = 10}
    if #ids > 4 then
        postData.limit = 5
    end

    if pID.nId ~= 0 then
        postData.businessType = pID.nId
    end

    if self.m_nUserID ~= -1 then
        postData.memberId = self.m_nUserID
    end

    local nTime, nEndTime = self:getCurrentTime()
    if nTime > 0 then
        postData.start = nTime
        if nEndTime then
            postData["end"] = nEndTime
        end
    end

    self.m_nLoadingType = 1
    self.m_pImageNoData:setVisible(false)
    self:showLoading(self.m_pPanel, 30, cc.p(self.m_pImageNoData:getPosition()), function() self.m_pImageNoData:setVisible(true) end)

    self:sendPost(PromoteConfig.DOMAIN.."getTreasureChanges/"..Player:getAccountID(), postData, function(_response, _json, _state)
        self.m_nLoadingType = 0
        local ids = self.m_pDetailList[self.m_nFindTypeID]
        if not _state then
            self:loadNext(jumpType)
        else
            self:array_concat(self.m_pInfos, _json.data.records)
            local pID = ids[self.m_nCurrentIndex]
            pID.nCount = pID.nCount + #_json.data.records
            if 0 == _json.data.total or pID.nCount >= _json.data.total then
                pID.bIsLoadEnd = true
            end
            self:loadNext(jumpType)
        end
    end)
end

function PromoteAccountChange:resetData()
    self.m_nCurrentIndex = 1
    self.m_pDetailList = {}
    self.m_pInfos = {}
    for _k, info in pairs(PromoteConfig.detail) do
        if self.m_pDetailList[info.classId] == nil then
            self.m_pDetailList[info.classId] = {}
        end
        table.insert(self.m_pDetailList[info.classId], {
            nId = _k,
            nCount = 0,
            bIsLoadEnd = false
        })
    end
end

function PromoteAccountChange:isLoadEnd(index)
    local ids = self.m_pDetailList[self.m_nFindTypeID]
    if index then
        local pID = ids[index]
        return pID.bIsLoadEnd
    else
        for _k, _item in pairs(ids) do
            if not _item.bIsLoadEnd then
                return false
            end
        end
        return true
    end
end

function PromoteAccountChange:loadNext(jumpType)
    local _ids = self.m_pDetailList[self.m_nFindTypeID]
    local n = self.m_nCurrentIndex + 1
    for index = n, #_ids do
        local info = _ids[index]
        if not info.bIsLoadEnd then
            self.m_nCurrentIndex = index
            self:loadData(jumpType)
            return
        end
    end

    self.m_nCurrentIndex = 1
    self:hideLoading()
    self:updateList(jumpType)
end


return PromoteAccountChange