-- 佣金记录
local PromoteConfig = require("src.app.hall.PromoteUI.PromoteConfig")
local PromoteViewBase = require("src.app.hall.PromoteUI.PromoteViewBase")
local PromoteCommissionRecording = class("PromoteCommissionRecording", function()
    return PromoteViewBase.new()
end)

function PromoteCommissionRecording:onDestory()

end

function PromoteCommissionRecording:ctor(node)
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    self.m_pPanel = node
    self.m_nLoadingType = 0
    self.m_nSortIndex = 0
    self.m_nDateIndex = 1
    self.m_pInfos = {}
    self.m_pCurrentInfos = {}

    self.m_pImageNoData = node:getChildByName("ImageNoData")
    self.m_pTextCommission = node:getChildByName("TextCommission"):getChildByName("TextCommissionValue")
    self.m_pItemTmp = node:getChildByName("PanelLabelTmp")
    self.m_pListView = node:getChildByName("ListView")
    self.m_pDatePanel = node:getChildByName("PanelListViewDateBack")
    self.m_pButtonFindDate = node:getChildByName("ImageDateBack")
    self.m_pButtonOffList = node:getChildByName("ButtonOffList")
    self.m_pTextData = self.m_pButtonFindDate:getChildByName("TextDate")
    self.m_pTitleBack = node:getChildByName("PanelTitleBack")

    self.m_pItemTmp:setVisible(false)
    self.m_pButtonOffList:setVisible(false)
    self.m_pImageNoData:setVisible(false)
    self.m_pDatePanel:setVisible(false)

    self.m_pButtonFindDate:addTouchEventListener(handler(self, self.onShowDateListClick))
    self.m_pDatePanel:addTouchEventListener(handler(self, self.onHideDateListClick))

    self.m_pDateListView = self.m_pDatePanel:getChildByName("PanelListViewDate")
    for index = 1, 5 do
        local button = self.m_pDateListView:getChildByName("LabelDate"..index)
        button:setTag(index)
        button:addTouchEventListener(handler(self, self.onDateClick))
    end

    for index = 1, 3 do
        local button = self.m_pTitleBack:getChildByName("ButtonTile"..index)
        button:setTag(index)
        if index ~= 1 then
            button.m_pSortImage = button:getChildByName("TextTile"):getChildByName("ImageSort")
            button.m_nSortType = 0
            button:addTouchEventListener(handler(self, self.onSortListClick))
        end

    end
end

function PromoteCommissionRecording:onHideDateListClick(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        self.m_pDatePanel:setVisible(false)
    end
end

function PromoteCommissionRecording:onShowDateListClick(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        self.m_pDatePanel:setVisible(true)
    end
end

function PromoteCommissionRecording:onDateClick(sender, eventType)
    if eventType == ccui.TouchEventType.ended then
        local index = sender:getTag()
        if self.m_nDateIndex == index then
            return
        end
        local item = self.m_pDateListView:getChildByTag(self.m_nDateIndex)
        item:setBackGroundColor(cc.c3b(0x4D, 0x4D, 0x4D))

        self.m_nDateIndex = index
        self.m_pDatePanel:setVisible(false)
        sender:setBackGroundColor(cc.c3b(0xE5, 0xE5, 0xE5))
        self.m_pTextData:setString(sender:getChildByName("Text_1"):getString())

        self.m_pCurrentInfos = {}
        if 1 == self.m_nDateIndex then
            self.m_pCurrentInfos = self.m_pInfos
        elseif 2 == self.m_nDateIndex then
            -- 今天
            local sTime = os.date("%Y%m%d", os.time())
            for index = 1, #self.m_pInfos do
                local info = self.m_pInfos[index]
                if os.date("%Y%m%d", info.recordTime) == sTime then
                    table.insert(self.m_pCurrentInfos, info)
                end
            end
        elseif 3 == self.m_nDateIndex then
            -- 昨天
            local nTime = tonumber(os.date("%Y%m%d", os.time() - 24*60*60))
            for index = 1, #self.m_pInfos do
                local info = self.m_pInfos[index]
                local infoTime = tonumber(os.date("%Y%m%d", info.recordTime))
                if infoTime >= nTime  then
                    table.insert(self.m_pCurrentInfos, info)
                end
            end
        elseif 4 == self.m_nDateIndex then
            -- 一周
            local nTime = tonumber(os.date("%Y%m%d", os.time() - 7*24*60*60))
            for index = 1, #self.m_pInfos do
                local info = self.m_pInfos[index]
                local infoTime = tonumber(os.date("%Y%m%d", info.recordTime))
                if infoTime >= nTime  then
                    table.insert(self.m_pCurrentInfos, info)
                end
            end
        elseif 5 == self.m_nDateIndex then
            -- 本月
            local nTime = tonumber(os.date("%Y%m", os.time()))
            for index = 1, #self.m_pInfos do
                local info = self.m_pInfos[index]
                local infoTime = tonumber(os.date("%Y%m", info.recordTime))
                if infoTime == nTime  then
                    table.insert(self.m_pCurrentInfos, info)
                end
            end
        end
        self:updateList()
    end
end

function PromoteCommissionRecording:onSortListClick(sender, eventType)
    if eventType ~= ccui.TouchEventType.ended then
        return
    end
    local index = sender:getTag()
    if self.m_nSortIndex == index then
        sender.m_nSortType = (sender.m_nSortType + 1) % 2
        if sender.m_nSortType == 0 then
            sender.m_pSortImage:setRotation(0)
        else
            sender.m_pSortImage:setRotation(180)
        end
    else
        if self.m_nSortIndex ~= 0 then
            local button = self.m_pTitleBack:getChildByTag(self.m_nSortIndex)
            button.m_pSortImage:loadTexture("hall/image/promote/image_3.png")
        end
        self.m_nSortIndex = index
        sender.m_pSortImage:loadTexture("hall/image/promote/image_4.png")
    end
    self:updateList()
end

function PromoteCommissionRecording:showView()
    self.m_pPanel:setVisible(true)
    if self.m_nLoadingType ~= 0 then
        return
    end
    self.m_nLoadingType = 1
    self.m_pImageNoData:setVisible(false)
    self:showLoading(self.m_pPanel, 10, cc.p(self.m_pImageNoData:getPosition()),function()
        self.m_pImageNoData:setVisible(true)
    end)

    self:sendPost(
        PromoteConfig.DOMAIN.."getRebateBalances/"..Player:getAccountID(),
        {},
        function(respones)
            if tolua.isnull(self) then
                return
            end
            self:hideLoading()
            if nil == respones or "" == respones then
                self.m_nLoadingType = 0
                self.m_pImageNoData:setVisible(true)
                return
            end

            local _pJson = json.decode(respones)
            if _pJson.code ~= 0 then
                self.m_nLoadingType = 0
                self.m_pImageNoData:setVisible(true)
                -- TOAST("请求数据失败！")
                return
            end
            if #_pJson.data.records == 0 then
                self.m_nLoadingType = 0
            end
            self.m_pTextCommission:setString( string.format( "%.02f", _pJson.data.total / 100 ) )
            self.m_pInfos = _pJson.data.records
            -- self.m_pInfos = {
            --     { 
            --         count = 0, --//提取金额 
            --         recordTime = os.time(), --//提取时间(unix_timestamp) 
            --         id = "relFA6CE5EA128EF1112DBB6", --//订单号 
            --         channelNO = "1020001001" --//渠道号 
            --     },
            --     { 
            --         count = 1, --//提取金额 
            --         recordTime = os.time() - 24*60*60, --//提取时间(unix_timestamp) 
            --         id = "relFA6CE5EA128EF1112DBB6", --//订单号 
            --         channelNO = "1020001001" --//渠道号 
            --     },
            --     { 
            --         count = 2, --//提取金额 
            --         recordTime = os.time() - 2*24*60*60, --//提取时间(unix_timestamp) 
            --         id = "relFA6CE5EA128EF1112DBB6", --//订单号 
            --         channelNO = "1020001001" --//渠道号 
            --     },
            --     { 
            --         count = 8, --//提取金额 
            --         recordTime = os.time() - 8*24*60*60, --//提取时间(unix_timestamp) 
            --         id = "relFA6CE5EA128EF1112DBB6", --//订单号 
            --         channelNO = "1020001001" --//渠道号 
            --     }
            -- }
            self.m_pCurrentInfos = self.m_pInfos
            self:updateList()
        end
    )

    


end

function PromoteCommissionRecording:updateList()
    self.m_pImageNoData:setVisible(0 ~= #self.m_pCurrentInfos)
    if self.m_nSortIndex == 2 then
        local button = self.m_pTitleBack:getChildByTag(self.m_nSortIndex)
        table.sort(self.m_pCurrentInfos, function (info1, info2)
            if button.m_nSortType == 0 then
                return info1.count > info2.count
            else
                return info1.count < info2.count
            end
        end)
    elseif self.m_nSortIndex == 3 then
        local button = self.m_pTitleBack:getChildByTag(self.m_nSortIndex)
        table.sort(self.m_pCurrentInfos, function (info1, info2)
            local nTime1 = tonumber(os.date("%Y%m%d%H%M%S", info1.recordTime))
            local nTime2 = tonumber(os.date("%Y%m%d%H%M%S", info2.recordTime))
            if button.m_nSortType == 0 then
                return nTime1 > nTime2
            else
                return nTime1 < nTime2
            end
        end)
    end

    self.m_pListView:removeAllItems()
    for index = 1, #self.m_pCurrentInfos do
        local info = self.m_pCurrentInfos[index]
        local item = self:createItem(info)
        self.m_pListView:pushBackCustomItem(item)
    end
    self.m_pListView:refreshView()
    self.m_pListView:jumpToTop()
end

function PromoteCommissionRecording:createItem(info)
    -- "count": 13440, //提取金额 
    -- "recordTime": 1587620079, //提取时间(unix_timestamp) 
    -- "id": "relFA6CE5EA128EF1112DBB6", //订单号 
    -- "channelNO": "1020001001" //渠道号

    local item = self.m_pItemTmp:clone()
    item:setVisible(true)
    item:setContentSize(self.m_pItemTmp:getContentSize())
    item:getChildByName("Text1"):setString(info.id) -- 订单
    item:getChildByName("Text2"):setString(string.format( "%.02f", info.count / 100 )) -- 佣金
    item:getChildByName("Text3"):setString(os.date("%Y/%m/%d %H:%M:%S", info.recordTime)) -- 交易时间
    return item
end

return PromoteCommissionRecording