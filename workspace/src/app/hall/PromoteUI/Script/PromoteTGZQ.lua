-- 推广挣钱
local PromoteConfig = require("src.app.hall.PromoteUI.PromoteConfig")
local PromoteViewBase = require("src.app.hall.PromoteUI.PromoteViewBase")
local PromoteTGZQ = class("PromoteTGZQ", function(node)
    return PromoteViewBase.new(node)
end)
function PromoteTGZQ:onDestory()
    removeMsgCallBack(self, REFRESH_QRCODE)
    removeMsgCallBack(self, MSG_REBATE_BALANCE_DATA_ASK)
    removeMsgCallBack(self, MSG_REBATE_DATA_ASK)
end

function PromoteTGZQ:ctor(node)
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    self.m_nLoadingQRcodeType = 0
    self.m_nLoadingType = 0

    self.m_pTextUrl = node:getChildByName("ImageUrl"):getChildByName("TextUrl")
    self.m_pImageQRCodeBack = node:getChildByName("ImageQRCodeBack")
    self.m_pPanelQRCodeBack = node:getChildByName("PanelQRCode")
    self.m_pImageQRCode = self.m_pImageQRCodeBack:getChildByName("ImageQRCode")
    self.m_pButtonQRCode = self.m_pImageQRCodeBack:getChildByName("ButtonQRCode")

    self.m_pButtonReceiveCommission = node:getChildByName("ButtonReceiveCommission")
    self.m_pButtonCopyUrl = node:getChildByName("ImageUrl"):getChildByName("ButtonCopyUrl")
    self.m_pButtonShareWeChat = node:getChildByName("ImageShare"):getChildByName("ButtonShareWeChat")
    self.m_pButtonShareWeChatFriend = node:getChildByName("ImageShare"):getChildByName("ButtonShareWeChatFriend")
    self.m_pButtonSaveQRCode = self.m_pImageQRCodeBack:getChildByName("ButtonSaveQRCode")

    self.m_pPanelQRCodeBack:setVisible(false)

    self.m_pTextUrl:setString(g_ShareQRcodeController.qrcodeUrl or "")
    self:setTextValue("Image1", Player:getAccountID())
    -- self.m_pTextReferrerID:getParent():setVisible(false)
    -- self.m_pTextMyID:getParent():setContentSize({width=760.50, height=53.00})
    -- self.m_pTextMyID:setPositionX(760.5 - 18)
    self:setButtonEnabled(self.m_pButtonReceiveCommission, false)

    UIAdapter:registClickCallBack(self.m_pButtonReceiveCommission, handler(self, self.onReceiveCommissionClick))
    UIAdapter:registClickCallBack(self.m_pButtonCopyUrl, handler(self, self.onCopyUrlClick))
    UIAdapter:registClickCallBack(self.m_pButtonShareWeChat, handler(self, self.onShareWeChatClick))
    UIAdapter:registClickCallBack(self.m_pButtonShareWeChatFriend, handler(self, self.onShareWeChatFriendClick))
    UIAdapter:registClickCallBack(self.m_pButtonSaveQRCode, handler(self, self.onSaveQRCodeClick))
    UIAdapter:registClickCallBack(self.m_pPanelQRCodeBack, function() self.m_pPanelQRCodeBack:setVisible(false) end)
    UIAdapter:registClickCallBack(self.m_pButtonQRCode, function() self.m_pPanelQRCodeBack:setVisible(true) end)

    addMsgCallBack(self, REFRESH_QRCODE, handler(self, self.onREFRESH_QRCODE)) -- 二维码
    addMsgCallBack(self, MSG_REBATE_BALANCE_DATA_ASK, handler(self, self.onMSG_REBATE_BALANCE_DATA_ASK)) -- 领取佣金
    addMsgCallBack(self, MSG_REBATE_DATA_ASK, function()
        local info = GlobalRebateController:getRebateDatas()
        self:setTextValue("ImageHistoryCommission", string.format( "%.02f", (info.m_historyAward or 0) / 100 ))
        self:setTextValue("ImageExtractCommission", string.format( "%.02f", (info.m_takeAward or 0 ) / 100 ))
        -- self:setTextValue("Image8", string.format( "%.02f", (info.m_ydTotalAward or 0)/100))
        self:setButtonEnabled(self.m_pButtonReceiveCommission, (info.m_takeAward or 0 ) > 0)
    end)

    self:onREFRESH_QRCODE()
    g_ShareQRcodeController:checkQRCode()
end

function PromoteTGZQ:onReceiveCommissionClick(sender, eventType)
    -- 领取佣金
    local num = tonumber(self:getTextValue("ImageExtractCommission"))
    if num == 0 then
        TOAST("奖励已领取")
        return
    end
    GlobalRebateController:reqRebateBalanceInfo()
end

function PromoteTGZQ:onCopyUrlClick(sender, eventType)
    -- 复制链接地址
    if self.m_pTextUrl:getString() ~= nil and self.m_pTextUrl:getString() ~= "" then
        ToolKit:setClipboardText(self.m_pTextUrl:getString())
        TOAST("复制成功")
    else
        TOAST("复制失败")
    end
end

function PromoteTGZQ:onShareWeChatClick(sender, eventType)
    self:disableQuickClick(sender, 2)
    -- 分享到微信
    print("button_wxhaoyou")
    local picUrl = cc.UserDefault:getInstance():getStringForKey( "erweima__url" ,"http://www.baidu.com")
    local m_callback = function(success) if (success) then self:sendShareWechatReq() end end
    local picPath1 = "res/ui/img.jpg"
    local picPath2 = cc.UserDefault:getInstance():getStringForKey("erweima")
    if cc.FileUtils:getInstance():isFileExist(picPath2) then
    else
        picPath2 = "res/ui/white.jpg"
    end
    local picPath = self:createImageFileWithTwoImage(picPath1,picPath2,1093,613)
    print(picPath)
    if device.platform == "android" then
        local javaClassName = "com/XinBo/LoveLottery/wxapi/WXEntryActivity"
        local javaMethodName = "OnShare"
        local javaParams = {
            m_callback,
            "",
            picPath,
            "",
            "",
            false,
        }
        local javaMethodSig = "(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V"
        luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
    elseif device.platform == "ios" then
        local args = { num1 = "", num2 = "", num3 = picPath, num4 = picUrl, num5 = m_callback }
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "MilaiPublicUtil"
        local ok, ret  = luaoc.callStaticMethod(className,"shareWX",args)
        if not ok then
            cc.Director:getInstance():resume()
        else
            print("The ret is:",tostring(ret))
            --callback(tostring(ret))
        end

        local function callback_ios2lua(param)
            if "success" == param then
                print("object c call back success")
            end
        end
        luaoc.callStaticMethod(className,"registerScriptHandler", {scriptHandler = callback_ios2lua } )
        luaoc.callStaticMethod(className,"callbackScriptHandler")
    end
end

function PromoteTGZQ:onShareWeChatFriendClick(sender, eventType)
    -- 分享到微信朋友圈
    self:disableQuickClick(sender, 2)
    print("button_pyquan")
    local picUrl = cc.UserDefault:getInstance():getStringForKey( "erweima__url" ,"http://www.baidu.com")
    local m_callback = function(success) if (success) then self:sendShareWechatReq() end end
    local picPath1 = "res/ui/img.jpg"
    local picPath2 = cc.UserDefault:getInstance():getStringForKey("erweima")
    if cc.FileUtils:getInstance():isFileExist(picPath2) then
    else
        picPath2 = "res/ui/white.jpg"
    end
    local picPath = self:createImageFileWithTwoImage(picPath1,picPath2,1093,613)
    print(picPath)
    if device.platform == "android" then
        local javaClassName = "com/XinBo/LoveLottery/wxapi/WXEntryActivity"
        local javaMethodName = "OnShare"
        local javaParams = {
            m_callback,
            "",
            picPath,
            "",
            "",
            true,
        }
        local javaMethodSig = "(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V"
        luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
    elseif device.platform == "ios" then
        local args = { num1 = "", num2 = "", num3 = picPath, num4 = picUrl, num5 = m_callback }
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "MilaiPublicUtil"
        local ok, ret = luaoc.callStaticMethod(className,"shareWXTimeLine",args)
        if not ok then
            cc.Director:getInstance():resume()
        else
            print("The ret is:",tostring(ret))
            --callback(tostring(ret))
        end

        local function callback_ios2lua(param)
            if "success" == param then
                print("object c call back success")
            end
        end
        luaoc.callStaticMethod(className,"registerScriptHandler", {scriptHandler = callback_ios2lua } )
        luaoc.callStaticMethod(className,"callbackScriptHandler")
    end
end

function PromoteTGZQ:onSaveQRCodeClick(sender, eventType)
    self:disableQuickClick(sender, 2)
    -- 保存二维码
    local picPath1 = "res/ui/img.jpg"
    local picPath2 = cc.UserDefault:getInstance():getStringForKey("erweima")
    if not cc.FileUtils:getInstance():isFileExist(picPath2) then
        TOAST("保存失败")
        return
    end
    local picPath = self:createImageFileWithTwoImage(picPath2,nil,300,300)
    if picPath then
        platform.saveImageToGallery(picPath,"MyQRCode"..os.date("%Y-%m-%d_%H:%M:%S", os.time())..".png",function()
            TOAST("保存成功")
        end)
    else
        TOAST("保存失败")
    end
end

function PromoteTGZQ:onREFRESH_QRCODE(_msg)
    self.m_nLoadingQRcodeType = 0

    local qrcode = g_ShareQRcodeController:getQRcode()
    if qrcode then
        self.m_pImageQRCode:removeAllChildren()
        self.m_pImageQRCode:addChild(qrcode)

        qrcode:setAnchorPoint(0.5,0.5)
        local size = qrcode:getContentSize()
        local imageSize = self.m_pImageQRCode:getContentSize()
        qrcode:setScale(0.5)
        qrcode:setPosition(cc.p(imageSize.width / 2, imageSize.height / 2))
    end

    -- local imageQRCode = self.m_pPanelQRCodeBack:getChildByName("ImageQRCode")
    local qrcode = g_ShareQRcodeController:getQRcode1()
    if qrcode then
        self.m_pPanelQRCodeBack:removeAllChildren()
        self.m_pPanelQRCodeBack:addChild(qrcode)
        qrcode:setAnchorPoint(0.5,0.5)
        -- local size = qrcode:getContentSize()
        -- qrcode:setScale(size.width / display.width)
        qrcode:setPosition(cc.p(display.width/2, display.height/2))
    end
end


function PromoteTGZQ:showView()
    self.m_pPanel:setVisible(true)
    if self.m_nLoadingQRcodeType ~= 1 then
        self.m_nLoadingQRcodeType = 1
        self.m_pImageQRCode:runAction(cc.Sequence:create(cc.DelayTime:create(10), cc.CallFunc:create(function()
            self.m_nLoadingQRcodeType = 0
        end), nil))
        g_ShareQRcodeController:checkQRCode()
    end

    if 0 ~= self.m_nLoadingType then
        return
    end
    self.m_nLoadingType = 1
    local viewSize = self.m_pPanel:getContentSize()
    self:showLoading(self.m_pPanel, 10, cc.p(viewSize.width / 2, viewSize.height / 2))
    self:sendGet( PromoteConfig.DOMAIN.."getMemberProfile/"..Player:getAccountID(), function(_respones, _json, _state)
        self.m_nLoadingType = 0
        self:hideLoading()
        GlobalRebateController:reqRebateInfo()
        if _state then
            -- "myId": 1025742, //我的id 
            -- "pid": 0, //上级id 
            -- "immediateMembers": 1, //直属会员 
            -- "allMembers": 3, //所有会员 
            -- "todayMembers": 0, //今日新增会员 
            -- "todayImmMembers": 0, //今日新增直属 
            -- "todayRebate": 208.0, //今日佣金 
            -- "yesterdayRebate": 208.0, //昨日佣金 
            -- "totalRebate": 572368.0, //历史总佣金 
            -- "withdrawableRebate": 549808.0 //可提取佣金
            local info = _json.data
            self:setTextValue("Image2", "无")
            if info then
                if info.pid =="" or info.pid == 0 then
                    self:setTextValue("Image2", "无")
                else
                    self:setTextValue("Image2", info.pid)
                end
                self:setTextValue("Image3", info.todayImmMembers or 0)
                self:setTextValue("Image4", info.immediateMembers or 0)
                self:setTextValue("Image5", info.todayMembers or 0)
                self:setTextValue("Image6", info.allMembers or 0)
                self:setTextValue("Image7", self:numberToString( (info.todayRebate or 0)/100 ))
                self:setTextValue("Image8", self:numberToString( (info.yesterdayRebate or 0)/100))

                -- self:setTextValue("ImageHistoryCommission", self:numberToString( (info.totalRebate or 0) / 100 ))
                -- self:setTextValue("ImageExtractCommission", self:numberToString( (info.withdrawableRebate or 0 ) / 100 ))
                self:setButtonEnabled(self.m_pButtonReceiveCommission, (info.withdrawableRebate or 0 ) > 0)
            end
        end
    end)
end

function PromoteTGZQ:onMSG_REBATE_BALANCE_DATA_ASK()
    -- 领取佣金
    local info =GlobalRebateController:getRebateBalanceDatas()
    self:setTextValue("ImageHistoryCommission", self:numberToString( (info.m_historyAward or 0) / 100 ))
    self:setTextValue("ImageExtractCommission", self:numberToString( (info.m_takeAward or 0 ) / 100 ))
    self:setButtonEnabled(self.m_pButtonReceiveCommission, (info.m_takeAward or 0 ) > 0)
end


function PromoteTGZQ:setTextValue(parentName, value)
    local pNode = self.m_pPanel:getChildByName(parentName)
    local pTextValue = pNode:getChildByName("TextValue")
    pTextValue:setString(value)
end

function PromoteTGZQ:getTextValue(parentName)
    local pNode = self.m_pPanel:getChildByName(parentName)
    local pTextValue = pNode:getChildByName("TextValue")
    return pTextValue:getString()
end

function PromoteTGZQ:setButtonEnabled(button, isEnabled)
    if button:getDescription() == "Button" then
        button:setEnable(isEnabled)
    else
        button:setEnabled(isEnabled)
        if isEnabled then
            button:setColor(cc.c3b(0xFF,0xFF,0xFF))
            button:setOpacity(160)
        else
            button:setColor(cc.c3b(160,160,160))
            button:setOpacity(160)
        end
    end
    -- button:setColor(cc.c3b(160,160,160))
    -- button:setOpacity(160)
end


function PromoteTGZQ:createImageFileWithTwoImage(firstImagePath, secondImagePath, width, height)
    local firstNode  =  self:createRenderNodeWithPath(firstImagePath, 0, 0, 1)
    local secondNode = self:createRenderNodeWithPath(secondImagePath, 680, 95, 1)
    local toFileName = "pandaWXSharePic.png"
    local renderTexture = self:createRenderTextureWithNodes(firstNode, secondNode, width, height)
    if renderTexture then
        local saveRet = renderTexture:saveToFile(toFileName, cc.IMAGE_FORMAT_JPEG, false)
        cc.Director:getInstance():getTextureCache():removeTextureForKey(cc.FileUtils:getInstance():getWritablePath()..toFileName)
        if saveRet then
           return  cc.FileUtils:getInstance():getWritablePath() .. toFileName
        else
            print("保存图片失败")
            return nil
        end
    end
end

function PromoteTGZQ:createRenderNodeWithPath(path, posX, posY, scale)
        local sprite = nil
        if path then
            sprite = cc.Sprite:create(path)
            sprite:setAnchorPoint(cc.p(0,0))
            sprite:setPosition(cc.p(posX,posY))
            sprite:setScale(scale)
        end
        return sprite
end

-- 两张图片纹理二合一
function PromoteTGZQ:createRenderTextureWithNodes(firstRenderNode, secondRenderNode, width, height)
    local renderTexture = cc.RenderTexture:create(width, height)
    renderTexture:beginWithClear(0,0,0,0)

    if firstRenderNode then
        firstRenderNode:getTexture():setTexParameters(0x2601, 0x2601, 0x812f, 0x812f)
    end

    if firstRenderNode then
        firstRenderNode:visit()
    end
   
    if secondRenderNode then
        secondRenderNode:visit()
    end

    renderTexture:endToLua()
    return renderTexture
end

-- 防止频繁点击
function PromoteTGZQ:disableQuickClick(button, _delay)
    local delay = _delay or 2
    button:stopAllActions()
    self:setButtonEnabled(button, false)
    button:runAction(cc.Sequence:create(
        cc.DelayTime:create(delay),
        cc.CallFunc:create(function()
            self:setButtonEnabled(button, true)
        end),
    nil))
end

return PromoteTGZQ