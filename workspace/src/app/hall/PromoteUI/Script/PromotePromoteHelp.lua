-- 代理介绍
local PromoteViewBase = require("src.app.hall.PromoteUI.PromoteViewBase")
local PromotePromoteHelp = class("PromotePromoteHelp", function()
    return PromoteViewBase.new()
end)

function PromotePromoteHelp:onDestory()
    GlobalRebateController:removeRebateDatas()
   removeMsgCallBack(self, MSG_REBATE_DATA_ASK)
end

function PromotePromoteHelp:ctor(node)
    ToolKit:registDistructor(self, handler(self, self.onDestory))
    self.m_pPanel = node
    self.m_pImageHelp = node:getChildByName("ImageHelp")
    self.m_pPanelHelpBack = node:getChildByName("PanelHelpBack")
    self.m_pRichText = ccui.RichText:create()
    self.m_pPanelHelpBack:addChild(self.m_pRichText)
    self.m_pRichText:ignoreContentAdaptWithSize(false)
    
    self.m_pTextList = {
        {
            text ="您的返利=1级返利+2级返利+..N级返利。\n1级返利=对应下线*80%   其它级返利=对应下线*30%\n ",
            color = cc.c3b(0xFB, 0xE3, 0x8D),
            fontSize = 32
        },
        {
            text = "举例说明：\n",
            color = cc.c3b(0xFC, 0xF0, 0x30),
            fontSize = 32
        },
        {
            text = "B=你的1级下线，税收为10000\nB1=B的1级下线，你的2级下线，税收为20000\n你的返利=10000*80%=（20000*80%）*30%",
            color = cc.c3b(0xBF,0xBF,0xBF),
            fontSize = 32
        },
    }

    if g_promoteType == 2 then
        self.m_pImageHelp:loadTexture("hall/image/promote/image_help0.png")
    end

    self:updateHelpText()
    addMsgCallBack(self, MSG_REBATE_DATA_ASK, handler(self, self.on_REBATE_DATA_ASK))

    GlobalRebateController:reqRebateInfo()
end

function PromotePromoteHelp:createText(_text, _color, _fontSize)
    local label = display.newTTFLabel({
        text = _text,
        font = UIAdapter.TTF_FZCYJ,
        size = _fontSize or 32,
        color = _color or cc.c3b(0xFF, 0xFF, 0xFF),
    })
    label:setAnchorPoint(cc.p(0, 1))
    return label, label:getContentSize()
end

function PromotePromoteHelp:updateHelpText()
    local panelSize = self.m_pPanelHelpBack:getContentSize()
    self.m_pPanelHelpBack:removeAllChildren()
    for index = 1, #self.m_pTextList do
        local item = self.m_pTextList[index]
        local label, labelSize = self:createText(item.text, item.color, item.fontSize)
        self.m_pPanelHelpBack:addChild(label)
        label:setPosition(cc.p(0, panelSize.height))
        panelSize.height = panelSize.height - labelSize.height
    end
end

function PromotePromoteHelp:on_REBATE_DATA_ASK(_msg, _cmd)
    local info = GlobalRebateController:getRebateDatas()
    local first =info.m_firstLevelRate*0.01
    local other =info.m_otherLevelRate*0.01

    self.m_pTextList = {}

    if g_promoteType == 2 then
        self.m_pTextList[#self.m_pTextList+1] = {
            text = "您的返利=1级返利。\n1级返利=对应下线*"..first.."%",
            color = cc.c3b(0xFB, 0xE3, 0x8D),
            fontSize = 32
        }
    else
        self.m_pTextList[#self.m_pTextList+1] = {
            text = "您的返利=1级返利+2级返利+..N级返利。\n1级返利=对应下线*"..first.."%   其它级返利=对应下线*"..other.."%\n ",
            color = cc.c3b(0xFB, 0xE3, 0x8D),
            fontSize = 32
        }
    end
    self.m_pTextList[#self.m_pTextList+1] = {
        text = "举例说明：",
        color = cc.c3b(0xFC, 0xF0, 0x30),
        fontSize = 32
    }
    self.m_pTextList[#self.m_pTextList+1] = {
        text = "B=你的1级下线，税收为10000\nB1=B的1级下线，你的2级下线，税收为20000\n您的返利=10000*"..first.."%+（20000*"..first.."%）*"..other.."%",
        color = cc.c3b(0xBF,0xBF,0xBF),
        fontSize = 32
    }
    -- self.m_pTextList[#self.m_pTextList+1] = {
    --     text = "您的返利=10000*"..first.."%+（20000*"..first.."%）*"..other.."%",
    --     color = cc.c3b(0xFF,0xFF,0xFF),
    --     fontSize = 32
    -- }
    self:updateHelpText()
end


return PromotePromoteHelp