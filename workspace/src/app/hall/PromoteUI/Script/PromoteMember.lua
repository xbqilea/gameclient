-- 直属成员
-- local PromoteUI = require("src.app.hall.PromoteUI.PromoteUI")
local PromoteConfig = require("src.app.hall.PromoteUI.PromoteConfig")
local PromoteViewBase = require("src.app.hall.PromoteUI.PromoteViewBase")

-- 全部成员
--------------------------------------------------------------------------------
local ALLMemberView = class("ALLMemberView", function (node) return PromoteViewBase.new(node) end)

function ALLMemberView:ctor(node)
    self.m_nLoadingType = 0
    self.m_nSortIndex = 0
    self.m_pInfos = {}
    self.m_nUserID = -1
    self:resetData()

    self:initFindMemeber(function()
        self:resetData()
        self:loadData(1)
    end)

    self.m_pItemTmp = node:getChildByName("PanelLabelTmp")
    self.m_pListView = node:getChildByName("ListView")
    self.m_pTextMemberCount = node:getChildByName("TextMember"):getChildByName("TextMemberCount")

    self.m_pImageNoData = node:getChildByName("ImageNoData")
    self.m_pTileBack = node:getChildByName("PanelTitleBack")

    self.m_pItemTmp:setVisible(false)

    for index = 1, 6 do
        local button = self.m_pTileBack:getChildByName("ButtonTile"..index)
        button:setTag(index)
        if index > 1 then
            local textTile = button:getChildByName("TextTile")
            button.m_nSortType = 0
            button.m_pSortImage = textTile:getChildByName("ImageSort")
            button:addTouchEventListener(handler(self, self.onSortClick))
            if index == 5 then
                textTile:setString("总提现")
            end
        end

        local text = self.m_pItemTmp:getChildByName("Text"..index)
        text:setPosition(cc.p(button:getPosition()))
    end

end

function ALLMemberView:showView()
    self.m_pPanel:setVisible(true)
    if self.m_nCount == 0 then
        self:loadData(1)
    end
end

function ALLMemberView:onSortClick(sender, eventType)
    if eventType ~= ccui.TouchEventType.ended then
        return
    end
    local index = sender:getTag()
    if self.m_nSortIndex == index then
        sender.m_nSortType = (sender.m_nSortType + 1) % 2
        if sender.m_nSortType == 0 then
            sender.m_pSortImage:setRotation(0)
        else
            sender.m_pSortImage:setRotation(180)
        end
    else
        if self.m_nSortIndex ~= 0 then
            local button = self.m_pTileBack:getChildByTag(self.m_nSortIndex)
            button.m_pSortImage:loadTexture("hall/image/promote/image_3.png")
        end
        self.m_nSortIndex = index
        sender.m_pSortImage:loadTexture("hall/image/promote/image_4.png")
    end
    self:updateList(1)
end

function ALLMemberView:updateList(jumpType)
    self.m_pImageNoData:setVisible(0 == #self.m_pInfos)
    local _getDate = function (dateStr)
        local date = self:getParent():split(dateStr, "- :")
        local str = ""
        for index = 1, #date do
            str = string.format( str.."%02d", tonumber(date[index]) )
        end
        return tonumber(str)
    end

    if 2 == self.m_nSortIndex then
        -- 时间
        local button = self.m_pTileBack:getChildByTag(self.m_nSortIndex)
        table.sort(self.m_pInfos, function(info1, info2)
            -- local time1 = _getDate(info1.createAt)
            -- local time2 = _getDate(info2.createAt)
            local time1 = tonumber(os.date("%Y%m%d%H%M%S", info1.t_regTime))
            local time2 = tonumber(os.date("%Y%m%d%H%M%S", info2.t_regTime))
            if button.m_nSortType == 0 then
                return time1 > time2
            else
                return time1 < time2
            end
        end)
    elseif 3 == self.m_nSortIndex then
        -- 总充值
        local button = self.m_pTileBack:getChildByTag(self.m_nSortIndex)
        table.sort(self.m_pInfos, function(info1, info2)
            if button.m_nSortType == 0 then
                return info1.teamMetrics.totalRecharge > info2.teamMetrics.totalRecharge
            else
                return info1.teamMetrics.totalRecharge < info2.teamMetrics.totalRecharge
            end
        end)
    elseif 4 == self.m_nSortIndex then
        -- 总打码
        local button = self.m_pTileBack:getChildByTag(self.m_nSortIndex)
        table.sort(self.m_pInfos, function(info1, info2)
            if button.m_nSortType == 0 then
                return info1.teamMetrics.totalStake > info2.teamMetrics.totalStake
            else
                return info1.teamMetrics.totalStake < info2.teamMetrics.totalStake
            end
        end)
    elseif 5 == self.m_nSortIndex then
        -- 总转账
        local button = self.m_pTileBack:getChildByTag(self.m_nSortIndex)
        table.sort(self.m_pInfos, function(info1, info2)
            if button.m_nSortType == 0 then
                return info1.teamMetrics.totalCashFee > info2.teamMetrics.totalCashFee
            else
                return info1.teamMetrics.totalCashFee < info2.teamMetrics.totalCashFee
            end
        end)
    elseif 6 == self.m_nSortIndex then
        -- 总佣金
        local button = self.m_pTileBack:getChildByTag(self.m_nSortIndex)
        table.sort(self.m_pInfos, function(info1, info2)
            if button.m_nSortType == 0 then
                return info1.teamMetrics.totalRebate > info2.teamMetrics.totalRebate
            else
                return info1.teamMetrics.totalRebate < info2.teamMetrics.totalRebate
            end
        end)
    end

    self.m_pListView:removeAllItems()
    for index = 1, #self.m_pInfos do
        local info = self.m_pInfos[index]
        local item = self:createItem(info)
        self.m_pListView:pushBackCustomItem(item)
    end
    if #self.m_pInfos > 0 and not self.m_bIsEnd then
        local item = self:createItem(nil, true)
        self.m_pListView:pushBackCustomItem(item)
    end

    self.m_pListView:refreshView()
    if jumpType and jumpType == 1 then
        self.m_pListView:jumpToTop()
    end
end

function ALLMemberView:createItem(info, isEnd)
    local itemSize = self.m_pItemTmp:getContentSize()
    local _item = self.m_pItemTmp:clone()
    _item:setVisible(true)
    _item:setContentSize(self.m_pItemTmp:getContentSize())
    if isEnd then
        if self.m_bIsEnd then
            for index = 2, 7 do
                _item:getChildByName("Text1"):setVisible(false)
            end
            local pText = _item:getChildByName("Text1")
            pText:setString("显示更多")
            pText:setPosition(cc.p(itemSize.width/2, itemSize.height/2))
            pText:setColor(cc.c3b(0xE5,0xE5, 0xE5))
            _item:setTouchEnabled(true)
            _item:addTouchEventListener(function(sender, eventType)
                if ccui.TouchEventType.ended == eventType then
                    self:loadData()
                end
            end)
        end
    else
        _item:getChildByName("Text1"):setString(info.id) -- 成员ID
        -- local date = self:getParent():split(info.createAt, "- :")
        -- _item:getChildByName("Text2"):setString(string.format( "%s/%s/%s\n%s:%s:%s",date[1],date[2],date[3],date[4],date[5],date[6] )) -- 注册时间
        _item:getChildByName("Text2"):setString(os.date("%Y/%m/%d\n%H:%M:%S", info.t_regTime))  -- 注册时间
        _item:getChildByName("Text3"):setString(self:numberToString( info.teamMetrics.totalRecharge ))    -- 总充值
        _item:getChildByName("Text4"):setString(self:numberToString( info.teamMetrics.totalStake/100 ))    -- 总打码
        _item:getChildByName("Text5"):setString(self:numberToString( info.teamMetrics.totalCashFee ))    -- 总转账
        _item:getChildByName("Text6"):setString(self:numberToString( info.teamMetrics.totalRebate/100 ))    -- 总佣金
    end
    return _item
end

function ALLMemberView:resetData()
    self.m_nCount = 0
    self.m_bIsEnd = false
    self.m_pInfos = {}
end

function ALLMemberView:loadData(jumpType)
    if self.m_nLoadingType == 0 then
        self.m_nLoadingType = 1
        self.m_pImageNoData:setVisible(false)
        self:showLoading(self.m_pPanel, 10, cc.p(self.m_pImageNoData:getPosition()))
        local postData = {offset = self.m_nCount, limit = 10}
        if self.m_nUserID ~= -1 then
            postData.memberId = self.m_nUserID
        end
        self:sendPost(PromoteConfig.DOMAIN.."getDirectSubordinates/"..Player:getAccountID(), postData, function(_response, _json, _state)
            self:hideLoading()
            self.m_nLoadingType = 0
            if not _state then
                self:updateList(jumpType)
            else
                self.m_pTextMemberCount:setString(_json.data.total)
                self.m_nCount = self.m_nCount + #_json.data.records
                if self.m_nCount ~= 0 and self.m_nCount >= _json.data.total then
                    self.m_bIsEnd = true
                end
                self:array_concat(self.m_pInfos, _json.data.records)
                self:updateList(jumpType)
            end
        end)
    end
end


-- 成员详情
--------------------------------------------------------------------------------
-- local MemberView = class("MemberView", function () return display.newNode() end)
-- function MemberView:ctor(node)
--     self.m_pPanel = node
-- end

-- function MemberView:setVisibleView(isVisible)
--     self.m_pPanel:setVisible(isVisible)
--     self:setVisible(isVisible)
-- end


--------------------------------------------------------------------------------
local PromoteMember = class("PromoteMember", function(node)
    return PromoteViewBase.new(node)
end)

function PromoteMember:onDestory()
    
end

function PromoteMember:ctor(node)
    ToolKit:registDistructor(self, handler(self, self.onDestory))

    self.m_pALLMemberView = ALLMemberView.new(node:getChildByName("PanelALLMember"))
    self:addChild(self.m_pALLMemberView)

    -- self.m_pMemberView = MemberView:new(node:getChildByName("PanelMember"))
    -- self:addChild(self.m_pMemberView)
    -- self.m_pMemberView:setVisible(false)

    self.m_pButtonOffList = node:getChildByName("ButtonOffList")
    self.m_pButtonOffList:setVisible(false)
end

function PromoteMember:showView()
    self.m_pPanel:setVisible(true)
    self.m_pALLMemberView:showView()
end

function PromoteMember:hideView()
    self.m_pPanel:setVisible(false)
    self.m_pALLMemberView:hideView()
end


return PromoteMember