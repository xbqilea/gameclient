
--
-- Author: 
-- Date: 2018-08-07 18:17:10
-- 公告控制类

local BulletinController = class("BulletinController")
local Bulletin = require("app.hall.notice.model.Notice")

-- 公告列表全局变量
GlobalBulletins = GlobalBulletins or {}

--滚动公告列表table
g_RollNotices = g_RollNotices or {}

--弹窗公告数据
g_AutoBulletins = g_AutoBulletins or {}

function BulletinController:ctor()
	self:myInit()
end

function BulletinController:myInit()
    self._Datas = {
        [1] = {
            m_bulletinType = 2,
            m_content      = 'VIP充值,支持多种充值方式,更有VIP充值回馈,秒冲到账,单场返利最高可达28888元,请认准官方唯一网【www.983750.com/】                                            ',
            m_finish       = 1559318400,
            m_hyperLink    = '',
            m_id           = 'd0ae996ca69b4781a858253f410ba201',
            m_start        = 1554134400,
            m_title        = '新玩家必读',
            m_type         = 0,
        },
        [2] = {
            m_bulletinType = 2,
            m_content      = '无限推广,加入全民代理,支持无限奖励额度,月入百万不是梦。                                           ',
            m_finish       = 1559318400,
            m_hyperLink    = '',
            m_id           = 'd0ae996ca69b4781a858253f410ba202',
            m_start        = 1554134400,
            m_title        = '反作弊公告',
            m_type         = 0,
        },
    }

	addMsgCallBack(self, MSG_BULLETIN_UPDATE_SUCCESS, handler(self, self.onBulletinUpdateSuccess))

    -- 注册网络监听
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_Bulletin_Nty", handler(self, self.netMsgHandler))
	TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_ShowFristTotalDeposit_Ack", handler(self, self.netMsgHandler))
 	-- self:requestUpdateBulletin(1)
	self.loginPopUp = true
end


function BulletinController:netMsgHandler( __idStr, __info ) 
	-- print("BulletinController:netMsgHandler", __idStr)
	--dump(__info,__idStr)
	-- 新公告提醒
	if __idStr == "CS_H2C_Bulletin_Nty" then
		--刷新公告列表成功
        if __info.m_bulletinList and type(__info.m_bulletinList) == "table" then
    		self:_updateBulletin(__info.m_bulletinList)
			sendMsg(MSG_BULLETIN_UPDATE_SUCCESS)
		end
    elseif __idStr == "CS_H2C_ShowFristTotalDeposit_Ack" then
        self.m_cfgInfo = __info.m_cfgInfo
	end
end

function BulletinController:getCfgInfo()
    return self.m_cfgInfo
end
function BulletinController:CfgInfoReq()
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_ShowFristTotalDeposit_Req", {})
end
--PstBulletinInfo = 
--{
--    {1, 1, 'm_strMsg', "STRING", 1, '公告信息'},
--    {2, 1, "m_type", "UINT", 1, "0:新增 1:删除 2:修改"},
--    {3, 1, "m_pop", "UINT", 1, "0:不需要弹窗 1:弹窗"},
--    {4, 1, "m_id", "UINT", 1, "公告ID"}
--}

-- 更新公告列表数据
function BulletinController:_updateBulletin(bulletins)
	local bulletinList = self:getBulletinList()
	local needPopUp = false
	--local needPopUpBulletin = {}
	--[[local popNoticeId = 0--]]
	local rollNoticeS = {} 
	
	print("*********打印公告信息***********")
    local rollNoticeList = self:getRollNoticeList()
    rollNoticeList[#rollNoticeList + 1] = Bulletin.new(self._Datas[1])
    rollNoticeList[#rollNoticeList + 1] = Bulletin.new(self._Datas[2])

	for i,v_bulletin in ipairs(bulletins) do

	    if v_bulletin.m_type == 1 then
	    
	        print("*********开始删除公告*************")  

            self:deleteBulletinByID(v_bulletin.m_id)
	        self:removeRollNotice(v_bulletin.m_id)

	    elseif v_bulletin.m_type == 2 then
    		if v_bulletin.m_id then

                if v_bulletin.m_bulletinType == 2 then
                   --跑马灯
                    --{ id = 11, content = "ssss" , frequence = 1, cooltime = 1  }

                    local rollNoticeList = self:getRollNoticeList()
                    
                    local flag = false

                    for key, var in ipairs(rollNoticeList) do
                        if var.m_id == v_bulletin.m_id then
                            flag = true
                    	end
                    end

                    if flag == false then   
                    	local data  = Bulletin.new(v_bulletin)
                    	table.insert(rollNoticeList, 1, data)   --新来的跑马灯数据插入前面
                    end
                    
                elseif v_bulletin.m_bulletinType == 1 then
                		needPopUp = true
			    		local flag = false
			    		for k, value in ipairs(g_AutoBulletins) do
			    			if value.m_id == v_bulletin.m_id  then
			    				flag = true
			    			end
			    		end
			    		if flag == false then
			    			g_AutoBulletins[#g_AutoBulletins + 1] = v_bulletin
			    		end
                else

                    local flag = false
                    for j,v_localBulletin in ipairs(bulletinList) do
                        if v_localBulletin.m_id == v_bulletin.m_id then
                            print("_updateBulletin,detail.ID",v_bulletin.m_id)
                            v_localBulletin:setInfo(v_bulletin)
                            flag = true
                        end
                    end

                    if flag == false then
                        bulletinList[#bulletinList+1] = Bulletin.new(v_bulletin)
                    end

                end
    		end
		else
            if v_bulletin.m_id then

                if v_bulletin.m_bulletinType == 2 then
                   --跑马灯

                    local rollNoticeList = self:getRollNoticeList()
                    
                    local flag = false

                    for key, var in ipairs(rollNoticeList) do
                        if var.m_id == v_bulletin.m_id then
                            flag = true
                    	end
                    end

                    if flag == false then   
                    	local data  = Bulletin.new(v_bulletin)
                    	table.insert(rollNoticeList, 1, data)   --新来的跑马灯数据插入前面
                    end
                    
                elseif v_bulletin.m_bulletinType == 1 then
                		needPopUp = true
			    		local flag = false
			    		for k, value in ipairs(g_AutoBulletins) do
			    			if value.m_id == v_bulletin.m_id  then
			    				flag = true
			    			end
			    		end
			    		if flag == false then
			    			g_AutoBulletins[#g_AutoBulletins + 1] = v_bulletin
			    		end
                else

                    local flag = false
                    for j,v_localBulletin in ipairs(bulletinList) do
                        if v_localBulletin.m_id == v_bulletin.m_id then
                            print("_updateBulletin,detail.ID",v_bulletin.m_id)
                            v_localBulletin:setInfo(v_bulletin)
                            flag = true
                        end
                    end

                    if flag == false then
                        bulletinList[#bulletinList+1] = Bulletin.new(v_bulletin)
                    end

                end
    		end
        end
	end
	self:sortBulletin()
	--弹窗公告
	if needPopUp then
       -- sendMsg(MSG_SHOW_LOBBY_BULLETIN_DLG, popNoticeId)
       self:sendAutoBulletin()
	elseif self.loginPopUp then
	   self:sendAutoBulletin()
	end
		
    g_RollNoticeController =  g_RollNoticeController or  require("app.hall.horseLamp.control.RNoticeController").new()
		
end
--排列公告
function BulletinController:sortBulletin()
	table.sort(GlobalBulletins,function(a,b) 
		return a:getPriority()>b:getPriority()
	end)
end

--推送自动弹窗公告
function BulletinController:sendAutoBulletin()
	table.sort(g_AutoBulletins, function(a, b)

	end)
    for k,v in pairs(g_AutoBulletins) do
        if v.m_title=="天上掉馅饼" then
            table.remove(g_AutoBulletins,k)
        end
    end
    for k,v in pairs(g_AutoBulletins) do
        if v.m_title=="每日豪礼" then
            table.remove(g_AutoBulletins,k)
        end
    end
    for k,v in pairs(g_AutoBulletins) do
        if v.m_title=="新用户首存彩金" then
            table.remove(g_AutoBulletins,k)
        end
    end
    for k,v in pairs(g_AutoBulletins) do
        if v.m_title=="邀请好友赚彩金" then
            table.remove(g_AutoBulletins,k)
        end
    end
    for k,v in pairs(g_AutoBulletins) do
        if v.m_title=="全民赚金" then
            table.remove(g_AutoBulletins,k)
        end
    end
    for k,v in pairs(g_AutoBulletins) do
        if v.m_title=="劲爆游戏" then
            table.remove(g_AutoBulletins,k)
        end
    end
    for k,v in pairs(g_AutoBulletins) do
        if v.m_title=="幸运夺宝" then
            table.remove(g_AutoBulletins,k)
        end
    end
    for k,v in pairs(g_AutoBulletins) do
        if v.m_title=="签到领红包" then
            table.remove(g_AutoBulletins,k)
        end
    end
    for k,v in pairs(g_AutoBulletins) do
        if v.m_title=="女神回访" then
            table.remove(g_AutoBulletins,k)
        end
    end
    local temp0 = {
            m_bulletinType = 1,
            m_content      = '',
            m_finish       = 1559318400,
            m_hyperLink    = 'hall/image/gonggao/0.jpg',
            m_id           = 'd0ae996ca69b4781a858253f410ba201',
            m_start        = 1554134400,
            m_title        = '天上掉馅饼',
            m_type         = 20,
    }
    table.insert(g_AutoBulletins, temp0)
    local temp1 = {
            m_bulletinType = 1,
            m_content      = '',
            m_finish       = 1559318400,
            m_hyperLink    = 'hall/image/gonggao/1.jpg',
            m_id           = 'd0ae996ca69b4781a858253f410ba201',
            m_start        = 1554134400,
            m_title        = '每日豪礼',
            m_type         = 3,
    }
    table.insert(g_AutoBulletins, temp1)
    local temp7 = {
            m_bulletinType = 1,
            m_content      = '',
            m_finish       = 1559318400,
            m_hyperLink    = 'hall/image/gonggao/9.jpg',
            m_id           = 'd0ae996ca69b4781a858253f410ba201',
            m_start        = 1554134400,
            m_title        = '新用户首存彩金',
            m_type         = 3,
    }
    table.insert(g_AutoBulletins, temp7)
    local temp8 = {
            m_bulletinType = 1,
            m_content      = '',
            m_finish       = 1559318400,
            m_hyperLink    = 'hall/image/gonggao/10.jpg',
            m_id           = 'd0ae996ca69b4781a858253f410ba201',
            m_start        = 1554134400,
            m_title        = '邀请好友赚彩金',
            m_type         = 6,
    }
    table.insert(g_AutoBulletins, temp8)
    local temp2 = {
            m_bulletinType = 1,
            m_content      = '',
            m_finish       = 1559318400,
            m_hyperLink    = 'hall/image/gonggao/2.jpg',
            m_id           = 'd0ae996ca69b4781a858253f410ba201',
            m_start        = 1554134400,
            m_title        = '全民赚金',
            m_type         = 2,
    }
    table.insert(g_AutoBulletins, temp2)
    local temp3 = {
            m_bulletinType = 1,
            m_content      = '',
            m_finish       = 1559318400,
            m_hyperLink    = 'hall/image/gonggao/3.jpg',
            m_id           = 'd0ae996ca69b4781a858253f410ba201',
            m_start        = 1554134400,
            m_title        = '劲爆游戏',
            m_type         = 1,
    }
    table.insert(g_AutoBulletins, temp3)
    local temp4 = {
            m_bulletinType = 1,
            m_content      = '',
            m_finish       = 1559318400,
            m_hyperLink    = 'hall/image/gonggao/5.jpg',
            m_id           = 'd0ae996ca69b4781a858253f410ba201',
            m_start        = 1554134400,
            m_title        = '幸运夺宝',
            m_type         = 4,
    }
    table.insert(g_AutoBulletins, temp4)
    local temp5 = {
            m_bulletinType = 1,
            m_content      = '',
            m_finish       = 1559318400,
            m_hyperLink    = 'hall/image/gonggao/8.jpg',
            m_id           = 'd0ae996ca69b4781a858253f410ba201',
            m_start        = 1554134400,
            m_title        = '签到领红包',
            m_type         = 5,
    }
    table.insert(g_AutoBulletins, temp5)
    local temp6 = {
            m_bulletinType = 1,
            m_content      = '',
            m_finish       = 1559318400,
            m_hyperLink    = 'hall/image/gonggao/4.jpg',
            m_id           = 'd0ae996ca69b4781a858253f410ba201',
            m_start        = 1554134400,
            m_title        = '女神回访',
            m_type         = 1,
    }
    table.insert(g_AutoBulletins, temp6)
    
	sendMsg(MSG_SHOW_LOBBY_BULLETIN_DLG, g_AutoBulletins)
end

--检查过期时间，过期的删掉
function BulletinController:deleteExpiredBulletin()
	local bulletinList = self:getBulletinList()
	local idsToBeDeleted = {}
	for i,v in ipairs(bulletinList) do
		
	end
end
--根据列表indexs删除公告indexs={index1,index2}
function BulletinController:deleteBulletinByIndexes(indexes)
	local bulletinList = self:getBulletinList()
	local indexToBeDeleted = {}
	for i=1,#bulletinList do
		indexToBeDeleted[i] = false
	end
	for i,index in ipairs(indexes) do
		indexToBeDeleted[i] = true
	end
	for i=#indexToBeDeleted,1,-1 do
		if indexToBeDeleted[i] == true then
			local bulletin = bulletinList[i]
			if bulletin then
				table.remove(bulletinList,i)
			else
				print("查无此件")
			end
		end
	end
end
--根据index删除公告
function BulletinController:deleteBulletin(index)
	local bulletinList = self:getBulletinList()
	if index then
		local bulletin = bulletinList[index]
		if bulletin then
			table.remove(bulletinList,index)
		else
			print("查无此件")
		end
	end
end
-- 根据公告ids删除公告
function BulletinController:deleteBulletinByIDs(ids)
	local bulletinList = self:getBulletinList()
	local indexToBeDeleted = {}
	for i=1,#bulletinList do
		indexToBeDeleted[i] = false
	end
	for i,id in ipairs(ids) do
		for i,v_Bulletin in ipairs(bulletinList) do
			if v_Bulletin:getm_id() == id then
				indexToBeDeleted[i] = true
			end
		end
	end
	for i=#indexToBeDeleted,1,-1 do
		if indexToBeDeleted[i] == true then
			local bulletin = bulletinList[i]
			if bulletin then
				table.remove(bulletinList,i)
			else
				print("查无此件")
			end
		end
	end
end
-- 根据公告id删除公告
function BulletinController:deleteBulletinByID(id)
	local bulletinList = self:getBulletinList()
	local index = 0
	for i,v_Bulletin in ipairs(bulletinList) do
		if v_Bulletin:getm_id() == id then
			index = i
		end
	end
	self:deleteBulletin(index)
end

-- 收到公告数据更新成功消息
function BulletinController:onBulletinUpdateSuccess()
	self.sending = false
	print("公告数据已更新")
end

function BulletinController:getBulletinByID(id)
	local BulletinList = self:getBulletinList()
	local index = 0
	for i,v_Bulletin in ipairs(BulletinList) do
		if v_Bulletin:getm_id() == id then
			index = i
		end
	end
	self:getBulletin(index)
end
function BulletinController:getBulletin(index)
	local BulletinList = self:getBulletinList()
	if index then
		local Bulletin = BulletinList[index]
		if Bulletin then
			return Bulletin
		else
			print("查无此件")
		end
	end
end
-- 打印公告数据
function BulletinController:dumpBulletin(_tag)
	local tag = _tag or "BulletinController:dumpBulletin"
	for i,v_Bulletin in ipairs(GlobalBulletins) do
		v_Bulletin:dump("BulletinController:dumpBulletin["..i.."]")
	end
end

function BulletinController:getBulletinList()
	return GlobalBulletins
end

--删掉滚动公告
function BulletinController:removeRollNotice( _id )

    local _rollNoticeList = self:getRollNoticeList()
    
    local _index = 0 
    
    for key, var in ipairs(_rollNoticeList) do 
        if var:getm_id() == _id  then
            _index = key         
        end	
    end   
    
    if _index ~= 0 then
        table.remove(_rollNoticeList,_index)
    end   
end

--获取滚动公告
function BulletinController:getRollNoticeList()

    return g_RollNotices
end


--获取自动弹窗公告
function BulletinController:getAutoBulletinList()
	return g_AutoBulletins
end

--删除自动弹窗公告
function BulletinController:removeAutoBulletin(_id)
	local _autoBulletinList = self:getAutoBulletinList()
	local index = 0

	for key, value in ipairs(_autoBulletinList) do
		if value:getm_id() == _id  then
			index = key
		end
	end
	if index ~= 0 then
		table.remove(_autoBulletinList, index)
	end
end

function BulletinController:onDestory()
	print("BulletinController:onDestory")
	removeMsgCallBack(self, MSG_BULLETIN_UPDATE_SUCCESS)

	TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_Bulletin_Nty")
    TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_ShowFristTotalDeposit_Ack")
end

return BulletinController

