--
-- Author: lhj
-- Date: 2018-07-05 12：43:53
--

local XbDialog = require("app.hall.base.ui.CommonView")
local PopBulletinDlg = class("PopBulletinDlg", function() return XbDialog.new() end)

function PopBulletinDlg:ctor(params)
	self.m_Datas = params
	self:myInit()
	self:setupViews()
end

function PopBulletinDlg:myInit()
	self.node = UIAdapter:createNode("csb/lobby_function/notice/layer_notice_auto_bulletin.csb"):addTo(self)
	UIAdapter:adapter(self.node, handler(self, self.onTouchCallback))
	self:setCloseCallback(handler(self, self.CloseCallBack))
end

function PopBulletinDlg:setupViews()
	self.node:getChildByName("title"):setString(self:trimText(self.m_Datas:getm_title()))
	self.node:getChildByName("content"):setString(self:trimText(self.m_Datas:getm_content()))
end

function PopBulletinDlg:trimText(text)
	local trimText = string.gsub(text,"<br>","\n")
	trimText = string.gsub(trimText,"<(.-)>","")
	trimText = string.gsub(trimText,"&nbsp;"," ")
	return trimText
end

function PopBulletinDlg:onTouchCallback(sender)
	local name = sender:getName()
	if name == "btn_close" then
		self:closeDialog()
	end
end

function PopBulletinDlg:CloseCallBack()
	local autoBulletins = GlobalBulletinController:getAutoBulletinList()
	GlobalBulletinController:removeAutoBulletin(self.m_Datas:getm_id())
	if table.nums(autoBulletins) > 0  then
		sendMsg(MSG_SHOW_LOBBY_BULLETIN_DLG, autoBulletins[1])
	end
end

return PopBulletinDlg