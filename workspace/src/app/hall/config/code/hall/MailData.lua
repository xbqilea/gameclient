--{字段tag（协议内唯一）,是否必须传输的字段，字段名称，字段类型，字段长度，字段描述 }
module(..., package.seeall)

-- 《客户端-服务器》

-- 大厅服务器通知客户端邮箱概要信息
CS_H2C_MailBrief_Nty = {
	{ 1		, 1		, 'm_unDoneNum'				, 'UINT'				, 1		, '待操作邮件数量 未读文本邮件以及未读奖励邮件'},	
}

-- 邮件服发送玩家新邮件到达消息至客户端
CS_Y2C_NewMailCome_Nty = {
	{ 1		, 1		, 'm_ntf'				, 'PstNewMailNtf'				, 1		, '通知内容'},
	
}

-- 玩家请求从数据库或邮件服务器领取新邮件至玩家 服务端无需返回
CS_C2H_CheckNewMail_Req = {
	{ 1		, 1		, 'm_verifyInfo'				, 'PstNewMailNtf'			, 1		, '获取新邮件校验信息'},
}

-- 玩家请求邮件列表
CS_C2H_GetMailList_Req = {	
	{ 1		, 1		, 'm_curPage'			, 'SHORT'		, 1	, '请求第几页'},
	{ 2		, 1		, 'm_pageSize'			, 'SHORT'		, 1	, '页容量'},		
}

-- 玩家请求邮件列表返回
CS_H2C_GetMailList_Ack = {
	{ 1		, 1		, 'm_curPage'		, 'SHORT'				, 1		, '请求第几页'},
	{ 2		, 1		, 'm_list'			, 'PstMailDetail'			, 30	, '邮件数组'},
	{ 3		, 1		, 'm_totalPage'		, 'UINT'				, 1		, '总共页数'},	
	{ 4		, 1		, 'm_optCode'		, 'SHORT'				, 1		, '参见错误码表'},
}

-- 玩家读邮件
CS_C2H_ReadMail_Rep = {
	{ 1		, 1		, 'm_ID'			, 'UINT'				, 1	, '读取邮件ID'},	
}

-- 玩家领取邮件请求
CS_C2H_AcceptMail_Req = {
	{ 1		, 1		, 'm_acceptType'			, 'SHORT'				, 1		, '1-列表领取 2-一键领取全部可领取邮件'},	
	{ 2		, 1		, 'm_acceptIDList'			, 'UINT'				, 20	, '领取邮件ID列表，若为空则默认是一键领取所有可领取邮件'},	
}

-- 玩家领取邮件响应
CS_H2C_AcceptMail_Ack = {	
	{ 1		, 1		, 'm_optCode'		, 'SHORT'				, 1	, '操作码 0：成功，-1：未知异常'},
}

-- 玩家删除邮件请求
CS_C2H_DeleteMail_Req = {
	{ 1		, 1		, 'm_delType'			, 'SHORT'				, 1		, '1-列表删除 2-一键删除全部邮件'},	
	{ 2		, 1		, 'm_delIDList'				, 'UINT'				, 20	, '删除邮件ID列表，若为空则默认是一键删除所有邮件'},	
}

-- 玩家删除邮件响应
CS_H2C_DeleteMail_Ack = {
	{ 1		, 1		, 'm_optCode'		, 'SHORT'				, 1	, '操作码 0：成功，-1：未知异常'},	
}

