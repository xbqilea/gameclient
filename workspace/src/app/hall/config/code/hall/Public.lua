module(..., package.seeall)

-- 玩家设备信息
PstDeviceInfo = {
	{ 1		, 1		, 'm_nTerminalType'		, 'SHORT'	, 1	, '1-PC,2-mobile,3-h5'},	
	{ 2		, 1		, 'm_strDeviceCode'		, 'STRING'	, 1	, '机器设备码'},
	{ 3		, 1		, 'm_strIP'				, 'STRING'	, 1	, 'IP地址'},
	{ 4		, 1		, 'm_nOSType'			, 'SHORT'	, 1	, '终端操作系统类型 0-windows 1-ios 2-android 4-windows phone'},
	{ 5		, 1		, 'm_strOSVersion'		, 'STRING'	, 1	, '终端操作系统版本'},
	{ 6		, 1		, 'm_nNetworkType'		, 'SHORT'	, 1	, '网络类型 0—WIFI；1—2G；2—3G；3—4G'},
	{ 7		, 1		, 'm_nOperatorType'		, 'SHORT'	, 1	, '运营商类型 0—网络；1—移动；2—联通；3—电信；4—虚拟'},
	{ 8		, 1		, 'm_strWifiName'		, 'STRING'	, 1	, '无线网的ssid名称'},
	{ 9		, 1		, 'm_strDeviceModel'	, 'STRING'	, 1	, '设备型号 PC/手机型号'},
	{ 10	, 1		, 'm_strChannelKey'		, 'STRING'	, 1	, '渠道号'},
	{ 11	, 1		, 'm_strCltVersion'		, 'STRING'	, 1	, '客户端包版本号'},
	{ 12	, 1		, 'm_shareType'						, 'SHORT'	, 1	, '分享类型 0:无  >0:具体分享类型 '},
	{ 13	, 1		, 'm_shareTriggerFuncId'			, 'SHORT'	, 1	, '触发分享功能id 0:无 >0 具体触发功能id'},
	{ 14	, 1		, 'm_spreaderId'					, 'UINT'	, 1	, '推广人id 0:无 >0 具体推广者id'},	
	{ 15	, 1		, 'm_strOrigin'			, 'STRING'	, 1	, '客户端来源'},
	{ 16	, 1		, 'm_strDeviceToken'	, 'STRING'	, 1	, '客户端设备token, 离线消息'},	
}

-- 服务器转发玩家消息拓展头部信息
PstLoginAccState = {
	{ 1		, 1		, 'm_strLoginIp'	, 'STRING'	, 1	, '登录ip'},
	{ 2		, 1		, 'm_strDv'			, 'STRING'	, 1	, '登录设备号'},
	{ 3		, 1		, 'm_nTerminalType'	, 'SHORT'	, 1	, '登录终端类型 1-pc 2-手机'},
}

-- 一件基本背包物品单元信息 --
PstBagItem =
{
	{1, 1, 'm_id', 'UINT', 1, '编号ID唯一标识'},
	{2, 1, 'm_itemId', 'UINT', 1, '物品id'},
	{3, 1, 'm_count', 'UINT', 1, '对应数量'},
	{4, 1, 'm_extendParams', 'STRING', 1, '扩展参数，比如添加m_golde: "\"m_gold\":\"100\""'},
	--[[
	------m_extendParams statement-------
	m_getTime:100, 获取服务端时间，用于检查物品是否过期,UTC时间戳，1970.01.01的秒数，0：无限制,-1:忽略
	m_vaildTime:200, 该物品的截止时间，UTC时间戳，自1970.01.01的秒数，0：无限制,-1:忽略
	]]--
}

--物品请求精简量 
PstBagDelta = {
	{1, 1, 'm_id', 'UINT', 1, '物品唯一id,不是itemId'},
	{2, 1, 'm_count', 'UINT', 1, '物品数量'},
}

--道具赠送
PstDonateItem = {
	{1, 1, 'm_accountId', 'UINT', 1, '账户id'},
	{2, 1, 'm_lastDonateTime', 'UINT', 1, '上一次赠送时间'},
	{3, 1, 'm_donateCnt', 'UINT', 1, '当天赠送次数'},
	{4, 1, 'm_donateErrCnt', 'UINT', 1, '当天赠送出错次数'},
	{5, 1, 'm_donateMaxErrCntTime', 'UINT', 1, '当天赠送最大出错次数的时间'},
}

---------------------------------------------------------房间列表信息--------------------------------------------------------------

-- 入口节点属性
PstPortalNodeAttr1 =
{
	{ 1		, 1		, 'm_pid'				, 'USHORT'				, 1	   	  , '入口属性ID 1:人数 2开始时间 3结束时间'},
	{ 2		, 1		, 'm_value'				, 'UINT'				, 1	   	  , '属性值'},
}

PstPortalNodeAttr2 =
{
	{ 1		, 1		, 'm_pid'				, 'USHORT'				, 1	   	  , '入口属性ID  1:人数 2开始时间 3结束时间'},
	{ 2		, 1		, 'm_value'				, 'STRING'				, 1	   	  , '属性值'},
}

-- 新功能入口节点信息
PstPortalNode =
{
	{ 1		, 1		, 'm_portalId'				, 'UINT'				, 1	   , '功能入口ID'},
	{ 2		, 1		, 'm_sort'					, 'UINT'				, 1	   , '排序'},
	{ 3		, 1		, 'm_attrList1'				, 'PstPortalNodeAttr1'	, 4096	   , '入口节点属性'},
	{ 4		, 1		, 'm_attrList2'				, 'PstPortalNodeAttr2'	, 4096	   , '入口节点属性'},
}

-----------------------------------------------------------------------------------------------------------------------------------

-- 角色基础属性更改同步单元结构
PstSyncBaseAttrItem = {
	{ 1		, 1		, 'm_syncKeyID'				, 'SHORT'				, 1		, '更新字段ID 根据字段ID读取不同类型值'},
	{ 2		, 1		, 'm_numberVal'				, 'UINT'				, 1		, '数字类型值'},
	{ 3		, 1		, 'm_strVal'				, 'STRING'				, 1		, '字符串类型值'},	
}


-- 新邮件到达通知消息
PstNewMailNtf = {
	{ 1		, 1		, 'm_mailType'				, 'SHORT'				, 1		, '邮件类型:1-群发 2-私人'},
	{ 2		, 1		, 'm_onlineMailVoucher'		, 'STRING'				, 1		, '收到新全服在线邮件凭证'},	
}

-- 获取新群发邮件摘要
PstMailBrief = {
	{ 1		, 1		, 'm_ID'			, 'UINT'				, 1	   , '邮件id'},
	{ 2		, 1		, 'm_isTextMail'	, 'SHORT'				, 1    , '是否文本邮件'},
	{ 3		, 1		, 'm_isTemplateMail', 'SHORT'				, 1    , '是否模板邮件'},
	{ 4		, 1		, 'm_isGroupMail'	, 'SHORT'				, 1    , '是否群发邮件'},
	{ 5		, 1		, 'm_state'			, 'SHORT'				, 1    , '邮件状态'},	
	{ 6		, 1		, 'm_sendDate'		, 'UINT'				, 1    , '发送日期'},
	{ 7		, 1		, 'm_businessGuid'	, 'STRING'				, 1    , '业务关联标识'},
}
----------------------------------------------------------------------------------------------------------------


-- 加载公告结构
PstSendBulletin =
{
	{1, 1, "m_id", "STRING", 1, "公告ID"},
	{2, 1, "m_platform", "UINT", 1, "公告推送平台"},
	{3, 1, "m_type", "UINT", 1, "0:新增 1:删除 2:修改"},
	{4, 1, "m_pop", "UINT", 1, "0:不弹窗显示 1:弹窗显示"},
	{5, 1, "m_productList", "UINT", 1024, "公告推送产品"},
	{6, 1, "m_strMsg", "STRING", 1, "公告信息"}
}

PstBulletinInfo = 
{
	{1, 1, "m_id", "STRING", 1, "公告ID"},
	{2, 1, "m_title", "STRING", 1, "公告title"},
	{3, 1, "m_bulletinType", "SHORT", 1, "公告类型 1-公告栏 2-跑马灯"},
	{4, 1, "m_content", "STRING", 1, "公告内容"},
	{5, 1, "m_hyperLink", "STRING", 1, "公告超链接"},
	{6, 1, "m_start", "UINT", 1, "公告开始时间"},
	{7, 1, "m_finish", "UINT", 1, "公告结束时间"},
	{8, 1, "m_type", "UINT", 1, "0:新增 1:删除 2:修改"},
	
	--{1, 1, 'm_strMsg', "STRING", 1, '公告信息'},
	--{2, 1, "m_type", "UINT", 1, "0:新增 1:删除 2:修改"},
	--{3, 1, "m_pop", "UINT", 1, "0:不需要弹窗 1:弹窗"},
	--{4, 1, "m_id", "STRING", 1, "公告ID"}
}

-- 玩家公告信息
PstUserBulletin = 
{
	{1, 1, "m_accountID", "UINT", 1, "玩家ID"},
	{2, 1, "m_timestamp", "UINT", 1, "时间戳"},
	{3, 1, "m_idList", "STRING", 2048, "玩家当天弹出的所有公告"}
}

-- 新旧平台兑换物品信息
PstExChange2OldItem = {
	{1, 1, "m_nType", "SHORT", 1, "交易物品类型 1-金币 2-钻石 3-道具"},
	{2, 1, "m_nOldPlatformId", "UINT", 1, "旧平台道具id"},
	{3, 1, "m_nOldPlatformNum", "UINT", 1, "旧平台兑换所得数量"},
	{4, 1, "m_nNewPlatformId", "UINT", 1, "新平台道具id"},
	{5, 1, "m_nNewPlatformNum", "UINT", 1, "新平台用于兑换数量"}
}

-- 新旧平台资源兑换订单信息
PstExchangeRes2OldPlatformOrder = 
{
	{1, 1, "m_orderID", "STRING", 1, "兑换订单号"},
	{2, 1, "m_exchangeTime", "UINT", 1, "兑换时间戳"},
	{3, 1, "m_state", "UINT", 1,"状态 【1】审核中【2】审核不通过【3】拒绝【4】兑换中【5】成功"},	
	{4, 1, "m_exchangeDetail", "PstExChange2OldItem", 30, "兑换资源列表"}
}

-- DB新旧平台资源兑换订单信息
PstExchangeRes2OldStateInfo = {	
	{1, 1, "m_nUsedExchangedTimes", "SHORT", 1, "今日已兑换次数"},
	{2, 1, "m_nResetTimesTimestamp", "UINT", 1,"今日已兑换次数重置utc时间"},	
	{3, 1, "m_nTodayExchangedMoney", "UINT", 1, "今日已兑换金额"},
	{5, 1, "m_nTotalExchangedMoney", "UINT", 1, "累计兑换金额"},	
	{6, 1, "m_nTotalExchangedGoldCoin", "UINT", 1, "累计兑换金币"},
	{7, 1, "m_nTotalExchangedDiamond", "UINT", 1, "累计兑换钻石"},
	{8, 1, "m_strTotalExchangedProp", "STRING", 1, "累计兑换道具"},
}

PstOpenGiftInfo = {
	{1, 1, "m_gridId", "UINT", 1, "背包格子"},
	{2, 1, "m_count", "UINT", 1, "开启次数"}
}

PstOrderInfo = 
{
	{1, 1, "m_strOderID", 			"STRING", 	1, "订单ID"},
	{2, 1, "m_nAccountID", 			"UINT", 	1, "玩家ID"},
	{3, 1, "m_nTime", 				"UINT", 	1, "订单发起时间"},
	{4, 1, "m_nStatus", 			"UINT", 	1, "订单状态"},
	{5, 1, "m_nType", 				"UINT", 	1, "订单类型"},
	{6, 1, "m_nNum", 				"UINT", 	1, "兑换的数量"},
	{7, 1, "m_strCost", 			"STRING", 	1, "消耗物品"},
}


PstExchangeInfo = 
{
	{1, 1, "m_nAccountID", 			"UINT", 1, "玩家ID"},
	{2, 1, "m_nDailyExchangeGold", 	"UINT", 1, "当日累计兑换金币"},
	{3, 1, "m_nExchangeGoldTotal", 	"UINT", 1, "累计兑换金币"},
	{4, 1, "m_nExchangeGoldTime", 	"UINT", 1, "上次兑换金币时间"},
	{5, 1, "m_nDailyExchangeNum", 	"UINT", 1, "当日累计兑换流量"},
	{6, 1, "m_nTotalExchangeNum", 	"UINT", 1, "历史累计兑换流量"},
	{7, 1, "m_nExchangeTime", 		"UINT", 1, "上次兑换流量时间"},
	{8, 1, "m_nDailyExchangeCount", "UINT", 1, "当日累计兑换话费次數"},
	{9, 1, "m_nIndex", 				"UINT", 1, "索引"},
}

----------------------------分享数据----------------------------
PstShareItemInfo =
{
	{1, 1, "m_shareId", "UINT", 1, "分享ID（唯一）"},
	{2, 1, "m_shareName", "STRING", 1, "分享ID名字"},
	{3, 1, "m_shareScheme", "UINT", 10, "方案ＩＤ"},
	{4, 1, "m_shareCondition", "UINT", 10, "条件数组，与参数一一对应"},
	{5, 1, "m_shareParam", "UINT", 10, "条件参数数组，与条件一一对应"}, 
	{6, 1, "m_shareReward", "UINT", 10, "奖品"}, 
	{7, 1, "m_completeCount", "UINT", 1, "已经完成过的次数"},
	{8, 1, "m_lastUpdateTime", "UINT", 1, "最近一次完成的时间, 0 为从未完成或已经重置"},
}

PstShare4GoldItemInfo =  {
	{1, 1, "m_accountId", "UINT", 1, "用户ID"},
	{2, 1, "m_nickName", "STRING", 1, "昵称"},
	{3, 1, "m_registerTime", "UINT", 1, "用户注册时间"},
	{4, 1, "m_gold", "UINT", 1, "贡献金币"},
}

PstShare4PhoneCardItemInfo = {
	{1, 1, "m_accountId", "UINT", 1, "用户ID"},
	{2, 1, "m_nickName", "STRING", 1, "昵称"},
	{3, 1, "m_winGold", "INT", 1, "赢取金币数"},
	{4, 1, "m_gameTime", "INT", 1, "游戏时长"},
	{5, 1, "m_bindPhone", "INT", 1, "绑定手机"},
	{6, 1, "m_midOnly", "INT", 1, "机器码唯一"},
}

----------------------------充值详情----------------------------
PstRechargeItemInfo = 
{
	{1, 1, "m_nTime", "UINT", 1, "充值时间"},
	{2, 1, "m_sOrder", "STRING", 1, "充值订单"},
	{3, 1, "m_nAmount", "UINT", 1, "充值金额"},
	{4, 1, "m_nStatus", "UINT", 1, "充值状态"},
	{5, 1, "m_nType", "UINT", 1, "类型"},
}

-- 任务进度
PstTaskProgress = 
{
	{1, 1, "m_nCur", "UINT", 1, "当前进度"},
	{2, 1, "m_nTarget", "UINT", 1, "目标参数"},
	{3, 1, "m_strDesc", "STRING", 1, "进度描述"}
}

-- 任务项
PstTask = 
{
	{1, 1, "m_taskId"          , "UINT"   , 1, "任务id"},
	{2, 1, "m_targetNum"       , "UINT"   , 1, "目标数量"},
	{3, 1, "m_reward"          , "UINT"   , 1, "任务奖励(服务器金币数量)"},
	{4, 1, "m_curProcess"      , "UINT"   , 1, "当前进度"},
	{5, 1, "m_isFinsh"		   , "SHORT"  , 1, "是否已完成任务 0:否，1：是"},	
	{6, 1, "m_isAcceptedReward", "SHORT"  , 1, "是否已领取奖励 0:否，1：是"},	
    {7, 1, "m_continueTaskCoinLimit"	, "UINT"   , 1, "连胜局数任务的下注额限制"},
}


-- db任务项
PstDbTask = 
{
	{1, 1, "m_nTaskId", "UINT", 1, "任务id"},
	{2, 1, "m_strJsonTask", "STRING", 1, "json详细任务内容"}
}
PstOnlineItem = {
	{1		, 1			, "m_strChannelKey"			, "STRING"		, 1		, "渠道号"},
	{2		, 1			, 'm_nPcOnline'				, 'UINT'	, 1	       	, 'pc在线'},
	{3		, 1			, 'm_nPhoneOnline'			, 'UINT'	, 1	       , 'phone在线'},
	{4		, 1			, 'm_nH5Online'				, 'UINT'	, 1	       , 'h5在线'},
}

PstGameMaintenanceInfo = 
{
	{1, 1, "m_gameId", "UINT", 1, "游戏ID"},
	{2, 1, "m_state", "UINT", 1, "1--维护中 2--正常"},
	{3, 1, "m_start", "UINT", 1, ""},
	{4, 1, "m_finish", "UINT", 1, ""}
}

PstOfflineDataInfo = 
{
	{1, 1, "m_strID", "STRING", 1, "ID"},
	--{2, 1, "m_nAccountID", "UINT", 1, "玩家ID"},
	{3, 1, "m_strFuncName", "STRING", 1, "功能名"},
	{4, 1, "m_strData", "STRING", 1, "数据内容"},
	--{5, 1, "m_nState", "UINT", 1, "0:未使用 1:已使用"}
}

PstTokenHistory = 
{
	{1		, 1			, "m_strId"				, "STRING"		, 1, "唯一ID"},
	{2		, 1			, "m_nType"				, "SHORT"		, 1, "代币获取类型 推广人充值房卡（1）， 下载（2）， 游戏返点（3）， 完成局数（4），分享（5）"},
	{3		, 1			, "m_nTime"				, "UINT"		, 1, "记录时间"},
	{4		, 1			, "m_nNum"				, "UINT"		, 1, "获得代币数量"},
	{5		, 1			, "m_strJsonParam"		, "STRING"		, 1, "记录扩展内容, userName-游戏玩家名称（1, 3）, serviceCharge-服务费（3） round1-已完成局数（4） round2-还需要完成局数（4） newGet-再获得代币数量（4）"},
}

PstTokenData = 
{
	{1		, 1			, "m_nDayTokenNum"		, "UINT"		, 1, "今天获得代币"},
	{2		, 1			, "m_nCalcTokenNum"		, "UINT"		, 1, "累计获得代币"},
	{3		, 1			, "m_nLastTime"			, "UINT"		, 1, "累计获得代币"},
}

PstOfflineWealth = 
{
	{1		, 1			, "m_nType"				, "UINT"		, 1, "0-赠送 1-游戏返点"},
	{2		, 1			, "m_nItemId"			, "UINT"		, 1, "道具ID"},
	{3		, 1			, "m_nNum"				, "UINT"		, 1, "道具数量"},
	{4		, 1			, "m_bMerge"			, "SHORT"		, 1, "是否合并大奥原纪录数据中"},
	{5		, 1			, "m_strParam"			, "STRING"		, 1, "扩展参数, 暂不使用"},
}

PstPurchaseInfo = 
{
	{1, 1, "m_itemId", "UINT", 1, "道具ID"},
	{2, 1, "m_count", "UINT", 1, "数量"}
}

PstRelationInfo = 
{
	{1, 1, "m_accountId", "UINT", 1, "玩家ID"},
	{2, 1, "m_promoterId", "UINT", 1, "推广人ID"},
	{3, 1, "m_promoteLv", "UINT", 1, "玩家推广层级"},
	{4, 1, "m_agentId", "UINT", 1, "代理ID"},
	{5, 1, "m_extend", "STRING", 1, "玩家推广返利数据"},
}

PstRebateDetail =
{
	{1, 1, "m_accountId", "UINT", 1,  "玩家ID"},
	{2, 1, "m_nickName", "STRING", 1, "玩家昵称"},
	{3, 1, "m_histContribute", "UINT", 1, "总贡献奖励"},
	{4, 1, "m_yesterdayContribute", "UINT", 1, "昨天贡献奖励"},
	{5, 1, "m_num", "UINT", 1, "挂在该玩家下面有多少人"},
	{6, 1, "m_otherHistContrib", "UINT", 1, "挂在该玩家下面所有玩家总返利"},
	{7, 1, "m_otherYestdayOtherContrib", "UINT", 1, "挂在该玩家下面昨天返利"},
	--{3, 1, "m_goldCoin", "UINT", 1, "玩家持有金币"},
	--{4, 1, "m_onlineState", "UBYTE", 1, "状态 1-在线 0-离线"},
}

PstGameRebate =
{
	{1, 1, "m_gameHeadId", "UINT", 1, "游戏大类"},
	{2, 1, "m_rebateDirect", "UINT", 1, "直接推广玩家成绩，今天0-24点的游戏数据"},
	{3, 1, "m_rebateIndirect", "UINT", 1, "间接推广玩家成绩，今天0-24点的游戏数据"},
	{4, 1, "m_award", "UINT", 1, "今天的奖励"},
}

PstWeekRebateItem =
{
	{1, 1, "m_rank", "UINT", 1, "排名"},
	{2, 1, "m_nickName", "STRING", 1, "玩家昵称"},
	{3, 1, "m_rebateCoin", "UINT", 1, "返利金币"},
}

--  排行榜类型
PstRankType	={
	{ 1		, 1		, 'm_nKindType'			, 'UINT'				, 1		, '排行榜类型 platform_rank.csv表kindType字段'},
	{ 2		, 1		, 'm_nRankType'			, 'SHORT'				, 1		, '1-日战利榜 2-日在线时间 3-日斗地主比赛积分榜 4-周战力榜 5-周在线时间 6-周斗地主比赛积分榜'},
	{ 3		, 1		, 'm_nDateType'			, 'SHORT'				, 1		, '1-日 2-月'},
}

PstRebateBalHist =
{
	{ 1		, 1		, 'm_order'				, 'STRING'					, 1		, '单号'},
	{ 2		, 1		, 'm_time'				, 'UINT'				, 1		, '推广返利结算时间'},
	{ 3		, 1		, 'm_rebateCoin'		, 'UINT'				, 1		, '结算金额'},
}

PstExchangeCoinOutHist =
{
	{ 1		, 1		, 'm_order'				, 'STRING'				, 1		, '单号'},
	{ 2		, 1		, 'm_type'				, 'UBYTE'				, 1		, '下分方式 1-支付宝 2-银行卡'},
	{ 3		, 1		, 'm_exchangeCoin'		, 'UINT'				, 1		, '兑换金币数, 只能整数兑换'},
	{ 4		, 1		, 'm_money'				, 'UINT'				, 1		, '下分金额'},
	{ 5		, 1		, 'm_time'				, 'UINT'				, 1		, '下分时间'},
	{ 6		, 1		, 'm_status'			, 'UINT'				, 1		, '下分状态 1-审核 2-成功 3-失败'},
}

PstExchangeCoinOutHistStatus = 
{
	{ 1		, 1		, 'm_order'				, 'STRING'					, 1		, '单号'},
	{ 2		, 1		, 'm_status'			, 'UBYTE'					, 1		, '下分状态 1-审核 2-成功 3-失败'},
}

PstCltLuckyRoulette =
{
	{ 1		, 1		, 'm_pos'		, 'UBYTE'		, 1		, '白银转盘信息设置'},
	{ 2		, 1		, 'm_award'		, 'UINT'		, 1		, '白银准盘一次抽奖消耗'},
	{ 3		, 1		, 'm_res'		, 'UBYTE'		, 1		, '黄金转盘信息设置'},
}

PstLuckyRouletteHist = 
{
	{1		, 1			, "m_strId"				, "STRING"		, 1, "唯一ID"},
	{2		, 1			, "m_nTime"				, "UINT"		, 1, "记录时间"},
	{3		, 1			, "m_type"				, "UINT"		, 1, "夺宝类型 1-白银 2-黄金 3-钻石"},
	{4		, 1			, "m_award"				, "UINT"		, 1, "获奖金额"},
}

PstAllLuckyRouletteHist = 
{
	{1		, 1			, "m_strId"				, "STRING"		, 1, "唯一ID"},
	{2		, 1			, "m_accountId"			, "UINT"		, 1, "玩家ID"},
	{3		, 1			, "m_nick"				, "STRING"		, 1, "昵称"},
	{4		, 1			, "m_nTime"				, "UINT"		, 1, "记录时间"},
	{5		, 1			, "m_type"				, "UINT"		, 1, "夺宝类型 1-白银 2-黄金 3-钻石"},
	{6		, 1			, "m_award"				, "UINT"		, 1, "获奖金额"},
}

PstSignInChannelInfo =
{
	{ 1		, 1		, 'm_channel'			, 'STRING'		, 1		, '渠道号'},
	{ 2		, 1		, 'm_cfgInfo'			, 'STRING'		, 1		, '签到配置'},
	{ 3		, 1		, 'm_start'				, 'UINT'		, 1		, '活动开始时间'},
	{ 4		, 1		, 'm_finish'			, 'UINT'		, 1		, '活动结束时间'},
	{ 5		, 1		, 'm_betLimit'			, 'UINT'		, 1		, '每日下注限制(客户端金币,服务器需要乘以100)'},
	{ 6		, 1		, 'm_openType'			, 'UINT'		, 1		, '开关类型：1开 0关'},
}

PstSignInChannel2Clt =
{
	{1		, 1			, "m_day"			, "UINT"		, 1, "第几天"},
	{2		, 1			, "m_award"			, "UINT"		, 1, "奖励"},
	{3		, 1			, "m_additionalAward"		, "UINT"		, 1, "额外奖励"},
	{4		, 1			, "m_betLimit"		, "UINT"		, 1, "下注限制"},
}

PstDepositChannelInfo =
{
	{ 1		, 1		, 'm_channel'			, 'STRING'		, 1		, '渠道号'},
	{ 2		, 1		, 'm_cfgInfo'			, 'STRING'		, 1		, '充值返利配置'},
	{ 3		, 1		, 'm_start'				, 'UINT'		, 1		, '活动开始时间'},
	{ 4		, 1		, 'm_finish'			, 'UINT'		, 1		, '活动结束时间'},
	{ 5		, 1		, 'm_openType'			, 'UINT'		, 1		, '开关类型：1开 0关'},
}

PstDepositChannel2Clt = 
{
	{1		, 1			, "m_minCoin"			, "UINT"		, 1, "充值下限"},
	{2		, 1			, "m_maxCoin"			, "UINT"		, 1, "充值上限"},
	{3		, 1			, "m_rebateType"		, "UINT"		, 1, "返利类型 1: 百分比  2：固定数值"},
	{4		, 1			, "m_rebateContent"		, "UINT"		, 1, "返利内容  如果返利类型是 1，返利内容为百分比的分子。 如果返利类型是2， 返利内容是固定数值(单位分)"},
}

PstFirstTotalDepositCfg2Clt =
{
	{ 1		, 1		, 'm_openType'			, 'UINT'		, 1		, '开关类型, 1 开放, 0 关闭'},
	{ 2		, 1		, 'm_timeType'			, 'UINT'		, 1		, '时间类型, 1  活动起始-结束时间内的充值才有效   2.永久'},
	{ 3		, 1		, 'm_beginTime'			, 'UINT'		, 1		, '活动开始时间'},
	{ 4		, 1		, 'm_endTime'			, 'UINT'		, 1		, '活动结束时间'},
	{ 5		, 1		, 'm_firstTotalDeposit'	, 'UINT'		, 1		, '首次累计充值达标金额'},
	{ 6		, 1		, 'm_selfReward'		, 'UINT'		, 1		, '给自己奖励金币值(服务器金币)'},
	{ 7		, 1		, 'm_upperReward'		, 'UINT'		, 1		, '给直接上级(直接推广人)奖励金币值'},
}

PstGameResult2Clt =
{
	{ 1		, 1		, 'm_orderId'			, 'UINT'		, 1		, '序号'},
	{ 2		, 1		, 'm_recordId'			, 'STRING'		, 1		, '牌局编号'},
	{ 3		, 1		, 'm_roomName'			, 'STRING'		, 1		, '房间'},
	{ 4		, 1		, 'm_profit'			, 'INT'			, 1		, '盈利'},
	{ 5		, 1		, 'm_balanceTime'		, 'UINT'		, 1		, '结算时间'},
}

PstGameResult_2Db =
{
	{ 1		, 1		, 'm_key'				, 'STRING'		, 1		, '唯一key'},
	{ 2		, 1		, 'm_gameResultInfo'	, 'STRING'		, 1		, '战绩'},
}

PstMatchInfo = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'			, 'UINT'				, 1	, '游戏最小类型ID'},
	{ 2		, 1		, 'm_enrollNum'					, 'UINT'				, 1 , '当前报名人数'},
	{ 3		, 1		, 'm_extra'						, 'STRING'				, 1	, '额外信息'},
	-- { 1		, 1		, 'm_gameAtomTypeId'			, 'UINT'				, 1	, '游戏最小类型ID'},
	-- { 2		, 1		, 'm_matchStartNum'				, 'UINT'				, 1	, '比赛开赛总人数'},
	-- { 3		, 1		, 'm_enrollNum'					, 'UINT'				, 1 , '报名人数'},
	-- { 4		, 1		, 'm_freePlayNum'				, 'UINT'				, 1 , '免费次数 0-不免费'},
	-- { 5		, 1		, 'm_signCost'					, 'UINT'				, 1 , '报名费, 不免费时所需报名费'},
	-- { 6		, 1		, 'm_awards'					, 'PstMatchAward'		, 10, '排名奖励'},
	-- { 7		, 1		, 'm_extra'						, 'STRING'				, 1	, '额外信息'},
}

PstMatchCfg = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'			, 'UINT'				, 1	, '游戏最小类型ID'},
	{ 2		, 1		, 'm_matchStartNum'				, 'UINT'				, 1	, '比赛开赛总人数'},
	{ 3		, 1		, 'm_freePlayNum'				, 'UINT'				, 1 , '免费次数 0-不免费'},
	{ 4		, 1		, 'm_signCost'					, 'UINT'				, 1 , '报名费, 不免费时所需报名费'},
	{ 5		, 1		, 'm_awards'					, 'PstMatchAward'		, 10, '排名奖励'},
	{ 6		, 1		, 'm_openBeginTime'				, 'INT'					, 1 , '开放开始时间 -1表示无限制 1600表示16点0分'},
	{ 7		, 1		, 'm_openEndTime'				, 'INT'					, 1 , '开放结束时间 -1表示无限制 1600表示16点0分'},
}
PstVipInfoClt =
{
	{ 1		, 1		, 'm_vipLevel'						, 'UINT'				, 1	, 'VIP等级'},
	{ 2		, 1		, 'm_targetTotalDeposit'			, 'UINT'				, 1	, '累计充值金额(客户端单位换算)'},
	{ 3		, 1		, 'm_upgradeAward'					, 'UINT'				, 1	, '晋级彩金(客户端单位换算)'},
	{ 4		, 1		, 'm_addSignInPercent'				, 'UINT'				, 1	, '签到奖励加成百分比(分子,客户端需要除以100)'},
	{ 5		, 1		, 'm_weekAward'						, 'UINT'				, 1	, '周礼金(客户端单位换算)'},
	{ 6		, 1		, 'm_monthAward'					, 'UINT'				, 1	, '月礼金(客户端单位换算)'},
	{ 7		, 1		, 'm_succourCnt'					, 'UINT'				, 1	, '每日破产救济金次数(客户端单位换算)'},
}
PstDayShareAddition_2Clt =
{
	{ 1		, 1		, 'm_totalDay'						, 'UINT'				, 1	, '累计天数'},
	{ 2		, 1		, 'm_additionAward'					, 'UINT'				, 1	, '额外奖励'},
}
PstMemberInfo =
{
	{1,		1, 'm_userid'				,  'UINT'						, 1		, '用户唯一标识,每个用户不能重复,可以整数(类似于QQ号),也可以是email,也可以是字符串'},
	{2,		1, 'm_name'					,  'STRING'						, 1		, '用户名称,可重复'},
	{3,		1, 'm_head'					,  'UBYTE'						, 1		, '头像'},
	{4,		1, 'm_vip'					,  'UBYTE'						, 1		, 'VIP等级'},
}

PstMessageData =
{
	{1,		1, 'm_Member'				,  'PstMemberInfo'				, 1		, '用户唯一标识,每个用户不能重复,可以整数(类似于QQ号),也可以是email,也可以是字符串'},
	{2,		1, 'm_message'				,  'STRING'						, 1		, '消息内容'},
	{3,		1, 'm_time'					,  'UINT'						, 1		, '消息发送时间，通过系统函数time(0)得到的整数'},
	{4,		1, 'm_MessageID'			,  'STRING'						, 1		, '消息ID'},
	{5,		1, 'm_MessageTag'			,  'UINT'						, 1		, '消息类型,45000+49:普通消息,45000+59:注单分享、抢红包、跟投'},
	{6,		1, 'm_Type'					,  'UINT'						, 1		, '消息类型,45000+49:普通消息,45000+59:注单分享、抢红包、跟投'},
}

PstClassFriendDetailed =
{
	{1,		1, 'm_userid'				,  'UINT'						, 1		, '好友userid'},
	{2,		1, 'm_ClassID'				,  'UINT'						, 1		, '分类ID'},
	{3,		1, 'm_StatusString'			,  'STRING'						, 1		, '好友状态串--第3位--1正常--2超级用户'},
	{4,		1, 'm_ClassName'			,  'STRING'						, 1		, '分类名称'},
	{5,		1, 'm_name'					,  'STRING'						, 1		, '好友名称'},
	{6,		1, 'm_head'					,  'UBYTE'						, 1		, '好友头像'},
	{7,		1, 'm_vip'					,  'UBYTE'						, 1		, '好友vip等级'},
	{8,		1, 'm_note'					,  'STRING'						, 1		, '好友备注信息'},
}

PstOneGroup =
{
	{1,		1, 'm_GroupID'					,  'UINT'					, 1		, '群组ID'},
	{2,		1, 'm_GroupName'				,  'STRING'					, 1		, '群组名称'},
	{3,		1, 'm_GroupNote'				,  'STRING'					, 1		, '群组备注'},
	{4,		1, 'm_CreatedTime'				,  'UINT'					, 1		, '群组创建时间'},	
	{5, 	1, 'm_userid'					,  'UINT'					, 1 	, '群主userid' },
----{6, 	1, 'm_AllUserid'				,  'UINT'					, 1024 	, '群内所有成员ID,包括群主'},
}

PstAiMemberInfo =
{
	{1,		1, 'm_userid'				,  'UINT'						, 1		, '用户唯一标识,每个用户不能重复,可以整数(类似于QQ号),也可以是email,也可以是字符串'},
	{2,		1, 'm_name'					,  'STRING'						, 1		, '用户名称,可重复'},
	{3,		1, 'm_head'					,  'UBYTE'						, 1		, '头像'},
	{4,		1, 'm_vip'					,  'UBYTE'						, 1		, 'VIP等级'},
	{5,		1, 'm_GameID'				,  'UINT'						, 1		, '游戏ID,不同的游戏可能组成不同的聊天室'},
}