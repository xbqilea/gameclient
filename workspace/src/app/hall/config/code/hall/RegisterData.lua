module(..., package.seeall)


-- 注册帐号协议<客户端-服务器>


-- 注册用户名账号
CS_C2L_CreateAccountReq	=
{
	{ 1		, 1		, 'm_strAccountName'	, 'STRING'	, 1	, '账号名'},
	{ 2		, 1		, 'm_strNickName'		, 'STRING'	, 1	, '昵称'},	
	{ 3		, 1		, 'm_strKEY'			, 'STRING'	, 1	, '密码'},
	{ 4		, 1		, 'm_deviceInfo'		, 'PstDeviceInfo'	, 1	, '设备信息'},		
}

CS_L2C_CreateAccountAck	=
{
	{ 1		, 1		, 'm_nResult'	, 'INT'	, 1	, '参见错误码表'},
}

-- 注册手机号账号
CS_C2L_CreatePhoneNumAccountReq	=
{
	{ 1		, 1		, 'm_strPhoneNumber'	, 'STRING'	, 1	, '注册手机号'},
	{ 2		, 1		, 'm_strKEY'			, 'STRING'	, 1	, '密码'},
	{ 3		, 1		, 'm_strNickName'		, 'STRING'	, 1	, '昵称'},
	{ 4		, 1		, 'm_strVerifyCode'		, 'STRING'	, 1	, '手机验证码'},
	{ 5		, 1		, 'm_deviceInfo'		, 'PstDeviceInfo'	, 1	, '设备信息'},		
}

 CS_L2C_CreatePhoneNumAccountAck	=
{
	{ 1		, 1		, 'm_nResult'	, 'INT'	, 1	, '参见错误码表'},
}

-- 注册游客账号请求
CS_C2L_CreateVisitorAccountReq	=
{	
	{ 1		, 1		, 'm_strKEY'			, 'STRING'	, 1	, '密码'},
	{ 2		, 1		, 'm_deviceInfo'		, 'PstDeviceInfo'	, 1	, '设备信息'},		
}

CS_L2C_CreateVisitorAccountAck	=
{
	{ 1		, 1		, 'm_nResult'			, 'INT'		, 1	, '[0]成功[-1]密码不合法[-x]其他原因'},
	{ 2		, 1		, 'm_strAccountName'	, 'STRING'	, 1	, '账号名'},
}

-- 游客账号转正
CS_C2H_BecomeRegularAccountReq	=
{	
	{ 1		, 1		, 'm_nAccountType'		, 'SHORT'	, 1	, '1:账号名，2：手机号'},
	{ 2		, 1		, 'm_strAccountName'	, 'STRING'	, 1	, '新账号名'},
	{ 3		, 1		, 'm_strPhoneNumber'	, 'STRING'	, 1	, '新注册手机号'},
	{ 4		, 1		, 'm_strKEY'			, 'STRING'	, 1	, '账号密码'},
	{ 5		, 1		, 'm_strVerifyCode'		, 'STRING'	, 1	, '手机验证码'},
}

CS_H2C_BecomeRegularAccountAck	=
{
	{ 1		, 1		, 'm_nRetCode'	, 'SHORT'	, 1	,  '参见错误码表'},
}


-- 绑定手机号请求
CS_C2H_BindPhoneNumReq	=
{		
	{ 1		, 1		, 'm_strPhoneNumber'	, 'STRING'	, 1	, '绑定手机号'},
	{ 2		, 1		, 'm_strVerifyCode'		, 'STRING'	, 1	, '手机验证码'},
}

CS_H2C_BindPhoneNumAck	=
{
	{ 1		, 1		, 'm_nRetCode'	, 'SHORT'	, 1	, '参见错误码表'},
}


-- 导入旧平台账号请求
CS_C2L_RegisteOldPlatformAccountReq = 
{
	{ 1		, 1		, 'm_strAccount'	, 'STRING'	, 1	, '旧平台账号名'},
	{ 2		, 1		, 'm_strNickName'	, 'STRING'	, 1	, '旧平台昵称'},
	{ 3		, 1		, 'm_strKey'		, 'STRING'	, 1	, '旧平台登录密码'},
	{ 4		, 1		, 'm_deviceInfo'		, 'PstDeviceInfo'	, 1	, '设备信息'},			
}

CS_L2C_RegisteOldPlatformAccountAck = 
{
	{ 1		, 1		, 'm_nResult'	, 'SHORT'	, 1	, '参见错误码表'},
}

