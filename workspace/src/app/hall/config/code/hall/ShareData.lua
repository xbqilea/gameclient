--{字段tag（协议内唯一）,是否必须传输的字段，字段名称，字段类型，字段长度，字段描述 }
module(..., package.seeall)

-- 客户端向服务器请求分享内容
CS_LoadShareDetail_Req =
{
	{ 1		, 1		, 'm_nShareId'	, 'UINT', 1		, '分享ID'},
}

CS_LoadShareDetail_Ack = 
{
	{ 1		, 1		, 'm_nShareId'	, 'UINT', 1		, '分享ID'},
	{ 2		, 1		, 'm_result'	, 'SHORT', 1		, '结果：0 成功， <0 失败'},
	{ 3		, 1		, 'm_shareInfo'	, 'PstShareItemInfo', 1		, '分享详情数据'},
}

-- 提交分享
CS_ShareSubmit_Req = 
{
	{ 1		, 1		, 'm_nShareId'	, 'UINT', 1		, '分享索引ID'},
	{ 2		, 1		, 'm_nShareChannel'	, 'UINT', 1		, '分享渠道（用户数据埋点）'},
	{ 3		, 1		, 'm_nShareContentID'	, 'UINT', 1		, '分享内容ID（用户数据埋点）'},
}

CS_ShareSubmit_Ack = 
{
	{ 1		, 1		, 'm_nShareId'	, 'UINT', 1		, '分享索引ID'}, 
	{ 2		, 1		, 'm_result'	, 'SHORT', 1		, '提交结果：0 成功， -1 失败（如次数满等）'},
}

CS_ShareClientLog_Req = {
	{ 1		, 1		, 'm_sShareLog'	, 'STRING', 1		, '分享LOG内容'}, 
}

CS_Share4GoldLoad_Req = {
	
}

CS_Share4GoldLoad_Ack = {
	{ 1		, 1		, 'm_shareList'	, 'PstShare4GoldItemInfo', 100	, '列表数据'},
}

CS_Share4PhoneCardLoad_Req = {
	
}

CS_Share4PhoneCardLoad_Ack = {
	{ 1		, 1		, 'm_nGameTime'	, 'UINT', 1		, '我的游戏时长'}, 
	{ 2		, 1		, 'm_nBindPhone', 'UINT', 1		, '是否绑定手机'}, 
	{ 3		, 1		, 'm_nCurState',  'UINT', 1		, '当前状态 0:正行中 1:已完成 2:审核中 3:已领奖 4:已拒绝'}, 
	{ 4		, 1		, 'm_nComplete',  'UINT', 1		, '当前完成数'}, 
	{ 5		, 1		, 'm_shareList'	, 'PstShare4PhoneCardItemInfo', 100	, '列表数据'},
}

CS_Share4PhoneCardSubmit_Req = {
}

CS_Share4PhoneCardSubmit_Ack = {
	{ 1		, 1		, 'm_result'	, 'SHORT', 1		, '提交结果：0 成功， 非0 失败'},
}

-- 暂时添加位置
-- 每日分享
CS_C2H_DayShare_Req =
{
}

CS_H2C_DayShare_Ack = 
{
	{ 1		, 1		, 'm_result'					, 'INT'				, 1		, '0:分享成功 <0: 错误码'},
}

CS_C2H_ShowDayShare_Req =
{
}

CS_H2C_ShowDayShare_Ack =
{
	{ 1		, 1		, 'm_result'					, 'INT'										, 1		, '0:成功, <0: 失败'},
	{ 2		, 1		, 'm_awardDayShareCnt'			, 'UINT'									, 1		, '每日有奖励分享次数'},
	{ 3		, 1		, 'm_dayAward'					, 'UINT'									, 1		, '每日分享奖励金币'},
	{ 4		, 1		, 'm_additionAwardArr'			, 'PstDayShareAddition_2Clt'				, 1024  , '额外奖励'},
}
-- 视讯进入退出游戏
CS_C2H_EnterBg_Req	=
{
}

CS_H2C_EnterBg_Ack	=
{
	{ 1		, 1		, 'm_result'					, 'INT'										, 1		, '0:成功, <0: 失败'},
    { 2		, 1		, 'm_webErrCode'				, 'INT'										, 1		, '后台错误码(1.m_result = 0 成功 2. m_result != 0 && m_webErrCode= 0 棋牌错误码 3.m_result != 0 && m_webErrCode != 0 后台报错)'},
	{ 3		, 1		, 'm_webErrMsg'					, 'STRING'									, 1		, '后台错误信息(m_result !=0  && m_webErrCode != 0时有效)'},
}

CS_C2H_ExitBg_Req = 
{
}

CS_H2C_ExitBg_Ack	=
{
	{ 1		, 1		, 'm_result'					, 'INT'										, 1		, '0:成功, <0: 失败'},
    { 2		, 1		, 'm_webErrCode'				, 'INT'										, 1		, '后台错误码(1.m_result = 0 成功 2. m_result != 0 && m_webErrCode= 0 棋牌错误码 3.m_result != 0 && m_webErrCode != 0 后台报错)'},
	{ 3		, 1		, 'm_webErrMsg'					, 'STRING'									, 1		, '后台错误信息(m_result!=0  && m_webErrCode != 0时有效)'},
}
