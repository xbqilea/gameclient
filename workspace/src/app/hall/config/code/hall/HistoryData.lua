module(..., package.seeall)

CS_C2H_LoadTokenHistory_Req = 
{
	
}

CS_H2C_LoadTokenHistory_Ack = 
{
	{ 1		, 1		, 'm_vTokenHistory'			, 'PstTokenHistory'			, 4096	   , '历史数据'},
}

----------------------------------------推广返利，结算历史记录----------------------------------------------

CS_C2H_LoadRebateBalHist_Req = 
{
	
}

CS_H2C_LoadRebateBalHist_Ack = 
{
	{ 1		, 1		, 'm_vHistory'				, 'PstRebateBalHist'			, 4096	   , '历史数据'},
}

---------------------------------------下分历史纪录信息---------------------------------------------
CS_C2H_LoadExchangeCoinOutHist_Req = 
{
	
}

CS_H2C_LoadExchangeCoinOutHist_Ack = 
{
	{ 1		, 1		, 'm_vHistory'				, 'PstExchangeCoinOutHist'			, 4096	   , '历史数据'},
}

---------------------------幸运夺宝------------------
CS_C2H_LoadRouletteHist_Req = 
{
	--{ 1		, 1		, 'm_type'				, 'SHORT'			, 1	   , '请求类型 1-大奖信息和历史信息 2-个人信息'},
}

CS_H2C_LoadRouletteHist_Ack = 
{
	{ 1		, 1		, 'm_vHistory'			, 'PstLuckyRouletteHist'			, 20	   , '历史数据'},
}

CS_H2C_NewAllRouletteHist_Nty = 
{
	{ 1		, 1		, 'm_newHistory'			, 'PstAllLuckyRouletteHist'			, 1	   , '中奖纪录'},	
    { 2		, 1		, 'm_isBigAward'			, 'UBYTE'							, 1	   , '是否大奖记录'},	
}

CS_H2C_LoadAllRouletteHist_Ack = 
{
	{ 1		, 1		, 'm_vBigHistory'			, 'PstAllLuckyRouletteHist'			, 30	   , '大奖记录'},
	{ 2		, 1		, 'm_vNormalHistory'		, 'PstAllLuckyRouletteHist'			, 20	   , '滚动历史记录'},
}