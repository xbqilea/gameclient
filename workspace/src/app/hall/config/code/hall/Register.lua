if not LUA_VERSION or LUA_VERSION ~= "5.3" then
	module(..., package.seeall)
end
require(g_protocolPath.."hall/Define")
-- 公共结构定义文件注册
netLoaderCfg_Templates_common = {
	g_protocolPath.."hall/Public",
}

----------------------------------配置所有模板定义文件-------------------
netLoaderCfg_Templates	=	{	
	g_protocolPath.."hall/TransferData",
	g_protocolPath.."hall/RegisterData",
	g_protocolPath.."hall/LoginData",
	g_protocolPath.."hall/BagData",
	g_protocolPath.."hall/MailData",
	g_protocolPath.."hall/UserInfoData",
	g_protocolPath.."hall/MatchData",
	g_protocolPath.."hall/BankData",
	g_protocolPath.."hall/MallData",
	g_protocolPath.."hall/NoticeData",
	g_protocolPath.."hall/CommonData",
	g_protocolPath.."hall/ShareData",
	g_protocolPath.."hall/PayData",
	g_protocolPath.."hall/TaskData",
	g_protocolPath.."hall/HistoryData", --代币
	g_protocolPath.."hall/RankData", --离线
    g_protocolPath.."hall/IMData", --离线
    g_protocolPath.."hall/pushTemp", --推送
	g_protocolPath.."hall/cpTemp", --cp
}

-- 公共结构协议注册
netLoaderCfg_Regs_common = {
	
-- 公共结构协议
	PstBagItem			=	PSTID_BAGITEM,
	PstBagDelta 		=	PSTID_BAGDELTA,
	PstSyncBaseAttrItem			= PSTID_SYNCBASEATTRITEM,
	PstNewMailNtf				= PSTID_NEWMAILNTF,
	PstSendBulletin 			= PSTID_SENDBULLETIN,
	PstUserBulletin 			= PSTID_USERBULLETIN,
	PstDeviceInfo				= PSTID_DEVICEINFO,
	PstMailBrief				= PSTID_USERNEWGROUPMAILBRIEF,
	PstExchangeRes2OldPlatformOrder = PSTID_EXCHANGERES_2OLDPLATFORMORDER,
	PstExChange2OldItem	= PSTID_EXCHANGE2OLDITEM,
	PstExchangeRes2OldStateInfo = PSTID_EXCHANGERES2OLDSTATEINFO,
	
	PstOpenGiftInfo 			= PSTID_OPENGIFTINFO,
	PstBulletinInfo 			= PSTID_BULLETININFO,
	
	PstShareItemInfo 			= PSTID_SHARE_ITEM_INFO,
	PstShare4GoldItemInfo		= PSTID_SHARE_4GOLD_ITEM_INFO,
	PstShare4PhoneCardItemInfo  = PSTID_SHARE_4PHONECARD_ITEM_INFO,

	PstRechargeItemInfo         = PSTID_RECHARGE_ITEM_INFO,
	PstOrderInfo 				= PSTID_ORDERINFO,
	
	PstPortalNodeAttr1			= PSTID_PORTALNODEATTR1,
	PstPortalNodeAttr2			= PSTID_PORTALNODEATTR2,
	PstPortalNode				= PSTID_PORTALNODE,
	PstTaskProgress				= PSTID_PSTTASKPROGRESS,
	PstTask 					= PSTID_PSTTASK, 
	
	PstExchangeInfo 			= PSTID_EXCHANGEINFO,
	PstOnlineItem				= PSTID_ONLINEITEM,
	PstLoginAccState			= PSTID_LOGINACCSTATE,
	PstDonateItem				= PSTID_DONATEITEM,
	PstGameMaintenanceInfo 		= PSTID_GAMEMAINTENANCEINFO,
	PstOfflineDataInfo			= PSTID_PSTOFFLINEDATAINFO,
	PstTokenHistory				= PSTID_TOKENHISTORY,
	
	PstOfflineWealth			= PSTID_OFFLINEWEALTH,
	PstTokenData				= PSTID_TOKENDATA,
	PstPurchaseInfo				= PSTID_PURCHASEINFO,
	PstRelationInfo				= PSTID_RELATIONINFO,
	PstRankType					= PSTID_RANKTYPE,
	PstRebateDetail				= PSTID_REBATEDETAIL,
	PstGameRebate				= PSTID_GAMEREBATE,
	PstRebateBalHist			= PSTID_REBATEBALHIST,
	PstExchangeCoinOutHist		= PSTID_EXCHANGECOINOUTHIST,
	PstExchangeCoinOutHistStatus = PSTID_EXCHANGECOINOUTHISTSTATUS,
	PstWeekRebateItem			= PSTID_WEEKREBATEITEM,
	
	PstCltLuckyRoulette			= PSTID_CLTLUCKYROULETTE,
	PstLuckyRouletteHist		= PSTID_LUCKYROULETTEHIST,
	PstAllLuckyRouletteHist		= PSTID_ALLLUCKYROULETTEHIST,
    PstSignInChannelInfo 		= PSTID_SIGNIN_CHANNEL_INFO,
	PstSignInChannel2Clt		= PSTID_SIGNIN_CHANNEL_2CLT,
	PstDepositChannelInfo		= PSTID_DEPOSIT_CHANNEL_INFO,
	PstDepositChannel2Clt 		= PSTID_DEPOSIT_CHANNEL_2CLT,
    PstFirstTotalDepositCfg2Clt = PSTID_FIRST_TOTAL_DEPOSIT_CFG_2CLT,
	PstGameResult2Clt 			= PSTID_GAME_RESULT_2CLT, 
	PstMatchInfo				= PSTID_MATCHINFO,
	PstDayShareAddition_2Clt 	= PSTID_DAY_SHARE_ADDITION_2CLT,
	PstMatchCfg					= PSTID_MATCHCFG,
    PstVipInfoClt				= PSTID_VIP_INFO_CLT,
    PstMemberInfo				=	PSTID_MEMBER_INFO,
	PstMessageData		 		= 	PSTID_MESSAGE_DATA,
    PstClassFriendDetailed      =   PSTID_CLASS_FRIEND_DETAILED,
    PstAiMemberInfo				=   PSTAIMEMBERINFO,
	PstOneGroup					=   PSTONEGROUP,
}
-------------------------注册协议-----------------------------
netLoaderCfg_Regs	=	{
	

-- 转发消息<服务器-服务器>
	CS_C2H_TransMsg_Req	= CS_C2H_TRANSMSG_REQ,
	CS_H2C_TransMsg_Ack	= CS_H2C_TRANSMSG_ACK,

-- 注册帐号协议<客户端-服务器>
	CS_C2L_CreateAccountReq				=	CS_C2L_CREATE_ACCOUNT_REQ,
	CS_L2C_CreateAccountAck				=	CS_L2C_CREATE_ACCOUNT_ACK,
	CS_C2L_CreatePhoneNumAccountReq		=	CS_C2L_CREATE_PHONENUM_ACCOUNT_REQ,
	CS_L2C_CreatePhoneNumAccountAck		=	CS_L2C_CREATE_PHONENUM_ACCOUNT_ACK,
	CS_C2L_CreateVisitorAccountReq		=	CS_C2L_CREATE_VISITOR_ACCOUNT_REQ,
	CS_L2C_CreateVisitorAccountAck		=	CS_L2C_CREATE_VISITOR_ACCOUNT_ACK,
	CS_C2H_BecomeRegularAccountReq		=	CS_C2H_BECOME_REGULAR_ACCOUNT_REQ,
	CS_H2C_BecomeRegularAccountAck		=	CS_H2C_BECOME_REGULAR_ACCOUNT_ACK,
	CS_C2H_BindPhoneNumReq				=	CS_C2H_BIND_PHONENUM_REQ,
	CS_H2C_BindPhoneNumAck				=	CS_H2C_BIND_PHONENUM_ACK,
	CS_C2L_RegisteOldPlatformAccountReq	= CS_C2L_REGISTE_OLDPLATFORM_ACCOUNT_REQ,
	CS_L2C_RegisteOldPlatformAccountAck	= CS_L2C_REGISTE_OLDPLATFORM_ACCOUNT_ACK,
	
-- 公共协议<客戶端-服務器>
	CS_GetNoticeSMSReq					=	CS_GET_NOTICE_SMS_REQ,
	CS_GetNoticeSMSAck					=	CS_GET_NOTICE_SMS_ACK,	
	CS_C2H_GetAliAccessKey_Req			= 	CS_C2H_GETALI_ACCESSKEY_REQ,
	CS_H2C_GetAliAccessKey_Ack			= 	CS_H2C_GETALI_ACCESSKEY_ACK,
	CS_C2A_GetApplyToken_Req			= 	CS_C2A_GETAPPLYTOKEN_REQ,
	CS_A2C_GetApplyToken_Ack			= 	CS_A2C_GETAPPLYTOKEN_ACK,
	CS_H2C_ThirdAccAvatarURLChange_Nty	= CS_H2C_THIRDACCAVATARURLCHANGE_NTY,

	
-- 账号登录协议<客户端-服务器>
	CS_C2L_LoginReq						= 	CS_C2L_LOGIN_REQ,
	CS_L2C_SRP6_1_Ack					= 	CS_L2C_SRP6_1_ACK,
	CS_C2L_SRP6_2_Req					= 	CS_C2L_SRP6_2_REQ,
	CS_L2C_SRP6_3_Ack					= 	CS_L2C_SRP6_3_ACK,
			
	CS_L2C_LoginAck						= 	CS_L2C_LOGIN_ACK,
	CS_C2H_LoginReq						= 	CS_C2H_LOGIN_REQ,
	CS_H2C_LoginAck						= 	CS_H2C_LOGIN_ACK,
			
	CS_C2L_NoLoginPingReq				= 	CS_C2L_NOLOGINPING_REQ, 
	CS_L2C_NoLoginPingAck 				= 	CS_L2C_NOLOGINPING_ACK, 
			
	CS_H2C_AccountDataNty				= 	CS_H2C_ACCOUNT_DATA_NTY,
				
	CS_C2H_PingReq						= 	CS_C2H_PING_REQ,
	CS_H2C_PingAck						= 	CS_H2C_PING_ACK,

	CS_H2C_KickNty						= 	CS_H2C_KICK_NTY,
	CS_C2H_LogoutRep 					= 	CS_C2H_LOGOUT_REP,
				
	CS_C2L_ExceptionVerifyReq 			= 	CS_C2L_EXCEPTION_VERIFY_REQ,
	CS_L2C_ExceptionVerifyAck 			= 	CS_L2C_EXCEPTION_VERIFY_ACK,
	CS_C2L_ThirdPartyAccLogin_Req 		= 	CS_C2L_THIRDPARTY_ACC_LOGIN_REQ,
	CS_L2C_ThirdPartyAccLogin_Ack 		= 	CS_L2C_THIRDPARTY_ACC_LOGIN_ACK,
	CS_C2H_BindThirdPartyAcc_Req 		= 	CS_C2H_BIND_THIRDPARTYACC_REQ,
	CS_H2C_BindThirdPartyAcc_Ack 		= 	CS_H2C_BIND_THIRDPARTYACC_ACK,
	
	CS_H2C_FuncSwitchNty				= 	CS_H2C_FUNC_SWITCH_NTY,
	CS_C2H_UpdateIp_Req					= 	CS_C2H_UPDATEIP_REQ,
	CS_H2C_UpdateIp_Ack					= 	CS_H2C_UPDATEIP_ACK,

----------------------商城功能-------------------------------------
	CS_C2H_mall_buy_Req = CS_C2H_MALL_BUY_REQ,
	CS_H2C_mall_buy_Ack = CS_H2C_MALL_BUY_ACK,

----------------------分享功能-------------------------------------
	CS_LoadShareDetail_Req   = CS_LOAD_SHARE_DETAIL_REQ,
	CS_LoadShareDetail_Ack   = CS_LOAD_SHARE_DETAIL_ACK,
	CS_ShareSubmit_Req     = CS_SHARE_SUBMIT_REQ,
	CS_ShareSubmit_Ack     = CS_SHARE_SUBMIT_ACK, 
	CS_ShareClientLog_Req  = CS_SHARE_CLIENT_LOG_REQ,

	CS_Share4GoldLoad_Req	    = CS_LOAD_SHARE4GOLD_REQ,
	CS_Share4GoldLoad_Ack       = CS_LOAD_SHARE4GOLD_ACK,

	CS_Share4PhoneCardLoad_Req 	   = CS_SHARE4PHONECARD_LOAD_REQ,
	CS_Share4PhoneCardLoad_Ack 	   = CS_SHARE4PHONECARD_LOAD_ACK,
	CS_Share4PhoneCardSubmit_Req   = CS_SHARE4PHONECARD_SUBMIT_REQ,
	CS_Share4PhoneCardSubmit_Ack   = CS_SHARE4PHONECARD_SUBMIT_ACK,

-----------------------充值功能-------------------------------------
	CS_Recharge_Order_Req 				=   CS_RECHARGE_ORDER_REQ,
	CS_Recharge_Order_Ack 				=   CS_RECHARGE_ORDER_ACK,
	CS_Recharge_List_Req 				=   CS_RECHARGE_LIST_REQ,
	CS_Recharge_List_Ack 				=   CS_RECHARGE_LIST_ACK,

----------------------背包功能--------------------------------------
-- 背包-查询 
	CS_H2C_GetBagInfo_Ack		=	CS_H2C_BAG_QUERY_ACK,
	
-- 背包-变化推送 
	CS_H2C_BagChange_Nty		=	CS_H2C_BAG_CHANGE_NTY,
	
-- 背包-合成 
	CS_C2H_BagCombine_Req		=	CS_C2H_BAG_COMBINE_REQ,
	CS_H2C_BagCombine_Ack		=	CS_H2C_BAG_COMBINE_ACK,

-- 背包-领奖中心 
	CS_C2H_BagExchange_Req 				= 	CS_C2H_BAGEXCHANGE_REQ,
	CS_H2C_BagExchange_Ack 				= 	CS_H2C_BAGEXCHANGE_ACK,
	CS_C2H_OpenGift_Req					=	CS_C2H_OPENGIFT_REQ,
	CS_H2C_OpenGift_Ack					=	CS_H2C_OPENGIFT_ACK,
	CS_C2H_BagDelItem_Req 				= 	CS_C2H_BAGDELITEM_REQ,
	CS_H2C_BagDelItem_Ack 				= 	CS_H2C_BAGDELITEM_ACK,
	CS_C2H_GetAddress_Req				= 	CS_C2H_GETADDRESS_REQ,
	CS_H2C_GetAddress_Ack				= 	CS_H2C_GETADDRESS_ACK,
	CS_C2H_ModAddress_Req 				= 	CS_C2H_MODADDRESS_REQ,
	CS_H2C_ModAddress_Ack 				= 	CS_H2C_MODADDRESS_ACK,
	CS_C2H_GetExchangeLeftTimes_Req 	= 	CS_C2H_GETEXCHANGELEFTTIMES_REQ,
	CS_H2C_GetExchangeLeftTimes_Ack 	= 	CS_H2C_GETEXCHANGELEFTTIMES_ACK,
	CS_C2H_TariffeCombine_Req 			= 	CS_C2H_TARIFFECOMBINE_REQ,
	CS_H2C_TariffeCombine_Ack 			= 	CS_H2C_TARIFFECOMBINE_ACK,
	CS_C2H_ActiveItem_Req 				= 	CS_C2H_ACTIVEITEM_REQ,
	CS_H2C_ActiveItem_Ack 				= 	CS_H2C_ACTIVEITEM_ACK,
	CS_C2H_AddItem_Req 					= 	CS_C2H_ADDITEM_REQ,
	CS_H2C_AddItem_Ack 					= 	CS_H2C_ADDITEM_ACK,
	CS_H2C_OrderID_Ack 					= 	CS_H2C_ORDERID_ACK,
	CS_C2H_DonateItem_Req				=	CS_C2H_DONATEITEM_REQ,
	CS_H2C_DonateItem_Ack				=	CS_H2C_DONATEITEM_ACK,
	CS_C2H_DonateItemConfirm_Req		=	CS_C2H_DONATEITEM_CONFIRM_REQ,
	CS_H2C_DonateItemConfirm_Ack		=	CS_H2C_DONATEITEM_CONFIRM_ACK,
	CS_H2C_UpdatePurchase_Nty			=	CS_H2C_UPDATEPURCHASE_NTY,
	CS_C2H_mall_buy_check_Req			=	CS_C2H_MALL_BUY_CHECK_REQ,
	CS_H2C_mall_buy_check_Ack			=	CS_H2C_MALL_BUY_CHECK_ACK,
	CS_C2H_UseBroadcastProp_Req			=	CS_C2H_USEBROADCASTPROP_REQ,
	CS_H2C_UseBroadcastProp_Ack			=	CS_H2C_USEBROADCASTPROP_ACK,
------------------------房间相关------------------------------------
	
	CS_C2H_HandleMsg_Req				=	CS_C2H_HANDLEMSG_REQ,
	CS_H2C_HandleMsg_Ack				=	CS_H2C_HANDLEMSG_ACK,
	CS_H2C_LastGame_Nty					=	CS_H2C_LASTGAME_NTY,
	CS_C2H_GetGameStatus_Req			=	CS_C2H_GETGAMESTATUS_REQ,
	CS_H2C_GetGameStatus_Ack			=	CS_H2C_GETGAMESTATUS_ACK,
	CS_H2C_KickNotice_Nty				=	CS_H2C_KICKNOTICE_NTY,
	CS_H2C_GameShutdown_Nty				=	CS_H2C_GAMESHUTDOWN_NTY,
	CS_C2H_MatchSubscribe_Req			=	CS_C2H_MATCHSUBSCRIBE_REQ,
	CS_C2H_CancelMatchSubscribe_Req		=	CS_C2H_CANCELMATCHSUBSCRIBE_REQ,
	CS_H2C_MatchSubscribe_Nty			=	CS_H2C_MATCHSUBSCRIBE_NTY,
	CS_H2C_MatchInfo_Nty				=	CS_H2C_MATCHINFO_NTY,
	
	CS_X2C_GameData_Nty					=	CS_X2C_GAMEDATA_NTY,
	CS_X2C_ExitGame_Nty					=	CS_X2C_EXITGAME_NTY,
		
	CS_S2C_MaintanenceInfo_Nty 						= 	CS_S2C_MAINTANENCEINFO_NTY,

	-- 排行榜相关
	CS_C2H_GetRankList_Req					= 	CS_C2H_GETRANKLIST_REQ,
	CS_H2C_GetRankList_Ack					=	CS_H2C_GETRANKLIST_ACK,
	CS_C2H_GetYTDRank_Req					=	CS_C2H_GETYTDRANK_REQ,
	CS_H2C_GetYTDRank_Ack					=	CS_H2C_GETYTDRANK_ACK,
	CS_C2H_QueryRankDetail_Req				=	CS_C2H_QUERYRANKDETAIL_REQ,
	CS_H2C_QueryRankDetail_Ack				=	CS_H2C_QUERYRANKDETAIL_ACK,
    CS_H2C_RankChange_Nty					=	CS_H2C_RANKCHANGE_NTY,
-----------------------------------------------------------------------邮件模块-------------------------------------------------------------------------		
	
CS_S2C_MaintanenceInfo_Nty 						= 	CS_S2C_MAINTANENCEINFO_NTY,
-- 排行榜相关
CS_C2H_GetRankList_Req					= 	CS_C2H_GETRANKLIST_REQ,
CS_H2C_GetRankList_Ack					=	CS_H2C_GETRANKLIST_ACK,
CS_C2H_GetYTDRank_Req					=	CS_C2H_GETYTDRANK_REQ,
CS_H2C_GetYTDRank_Ack					=	CS_H2C_GETYTDRANK_ACK,
CS_C2H_QueryRankDetail_Req				=	CS_C2H_QUERYRANKDETAIL_REQ,
CS_H2C_QueryRankDetail_Ack				=	CS_H2C_QUERYRANKDETAIL_ACK,
--<客户端-服务器>
	CS_C2H_GetMailList_Req 				= 	CS_C2H_GET_MAILLIST_REQ,
	CS_H2C_GetMailList_Ack 				= 	CS_H2C_GET_MAILLIST_ACK,
	CS_C2H_ReadMail_Rep 				= 	CS_C2H_READ_MAIL_REP,
	CS_C2H_AcceptMail_Req 				= 	CS_C2H_ACCEP_TMAIL_REQ,
	CS_H2C_AcceptMail_Ack 				= 	CS_H2C_ACCEPT_MAIL_ACK,
	CS_C2H_DeleteMail_Req				= 	CS_C2H_DELETEMAIL_REQ,
	CS_H2C_DeleteMail_Ack				= 	CS_H2C_DELETEMAIL_ACK,
	CS_Y2C_NewMailCome_Nty 				= 	CS_Y2C_NEWMAIL_COME_NTY,	
	CS_C2H_CheckNewMail_Req				= 	CS_C2H_CHECKNEWMAIL_REQ,
	CS_H2C_MailBrief_Nty				= 	CS_H2C_MAILBRIEF_NTY,
	
	
-----------------------------------------------------------------------交易模块-------------------------------------------------------------------------
--<客户端-服务器>
	CS_C2H_ExchangeRes2OldPlatform_1_Req	=	CS_C2H_EXCHANGERES_2OLDPLATFORM_1_REQ,
	CS_H2C_ExchangeRes2OldPlatform_1_Ack	=	CS_H2C_EXCHANGERES_2OLDPLATFORM_1_ACK,
	CS_C2H_ExchangeRes2OldPlatform_2_Req	=	CS_C2H_EXCHANGERES_2OLDPLATFORM_2_REQ,
	CS_H2C_ExchangeRes2OldPlatform_2_Ack	=	CS_H2C_EXCHANGERES_2OLDPLATFORM_2_ACK,
	CS_C2H_GetExchangeRes2OldPlatformOrder_Req	=	CS_C2H_GETEXCHANGERES_2OLDPLATFORMORDER_REQ,
	CS_H2C_GetExchangeRes2OldPlatformOrder_Ack	=	CS_H2C_GETEXCHANGERES_2OLDPLATFORMORDER_ACK,
	
	
--------------------------------------------------  角色基础属性模块 -----------------------
	CS_H2C_SyncBaseAttr_Nty				= CS_H2C_SYNCBASEATTR_NTY,
	CS_C2H_ModifyNickName_Req			= CS_C2H_MODIFYNICKNAME_REQ,
	CS_H2C_ModifyNickName_Ack			= CS_H2C_MODIFYNICKNAME_ACK,
	CS_C2H_ChangeAvatar_Req				= CS_C2H_CHANGEAVATAR_REQ,
	CS_H2C_ChangeAvatar_Ack				= CS_H2C_CHANGEAVATAR_ACK,
	CS_C2H_BindIDCard_Req				= CS_C2H_BIND_IDCARD_REQ,
	CS_H2C_BindIDCard_Ack				= CS_H2C_BIND_IDCARD_ACK,
	CS_C2L_ResetPassword_Req 			= CS_C2L_RESETPASSWORD_REQ,
	CS_L2C_ResetPassword_Ack 			= CS_L2C_RESETPASSWORD_ACK,
	CS_C2L_FindPassword_1_Req 			= CS_C2L_FIND_PASSWORD_1_REQ,
	CS_L2C_FindPassword_1_Ack 			= CS_L2C_FIND_PASSWORD_1_ACK,
	CS_C2L_FindPassword_2_Req 			= CS_C2L_FIND_PASSWORD_2_REQ,
	CS_L2C_FindPassword_2_Ack 			= CS_L2C_FIND_PASSWORD_2_ACK,	
	CS_C2H_ModifyPassword_Req			= CS_C2H_MODIFY_PASSWORD_REQ,
	CS_H2C_ModifyPassword_Ack			= CS_H2C_MODIFY_PASSWORD_ACK,
	CS_C2H_LockLocalDevice_Req			= CS_C2H_LOCK_LOCALDEVICE_REQ,
	CS_H2C_LockLocalDevice_Ack			= CS_H2C_LOCK_LOCALDEVICE_ACK,
	CS_C2H_UnlockLocalDevice_Req		= CS_C2H_UNLOCK_LOCALDEVICE_REQ,
	CS_H2C_UnlockLocalDevice_Ack		= CS_H2C_UNLOCK_LOCALDEVICE_ACK,
	CS_C2H_AliPayBind_Req				= CS_C2H_ALIPAYBIND_REQ,
	CS_H2C_AliPayBind_Ack				= CS_H2C_ALIPAYBIND_ACK,
	CS_C2H_Exchange2Money_Req			= CS_C2H_EXCHANGE2MONEY_REQ,
	CS_H2C_Exchange2Money_Ack			= CS_H2C_EXCHANGE2MONEY_ACK,
	CS_C2H_BankBind_Req					= CS_C2H_BANKBIND_REQ,
	CS_H2C_BankBind_Ack					= CS_H2C_BANKBIND_ACK,
	CS_C2H_LoadExchangeCoinOutHist_Req	= CS_C2H_LOADEXCHANGECOINOUTHIST_REQ,
	CS_H2C_LoadExchangeCoinOutHist_Ack	= CS_H2C_LOADEXCHANGECOINOUTHIST_ACK,
	CS_C2H_ChangeAvatarFame_Req   = CS_C2H_CHANGEAVATARFAME_REQ,
    CS_C2H_ChangeAvatarFame_Req   = CS_C2H_CHANGEAVATARFAME_REQ,
    CS_H2C_ChangeAvatarFame_Ack   = CS_H2C_CHANGEAVATARFAME_ACK,
	CS_C2H_DrawLottery_Req				=	CS_C2H_DRAWLOTTERY_REQ,
	CS_H2C_DrawLottery_Ack				=	CS_H2C_DRAWLOTTERY_ACK,
	CS_H2C_LuckyRoulette_Nty			=	CS_H2C_LUCKYROULETTE_NTY,
	CS_C2H_LoadRouletteHist_Req			=	CS_C2H_LOADROULETTEHIST_REQ,
	CS_H2C_LoadRouletteHist_Ack			=	CS_H2C_LOADROULETTEHIST_ACK,
	CS_H2C_LoadAllRouletteHist_Ack		=	CS_H2C_LOADALLROULETTEHIST_ACK,
	CS_H2C_NewAllRouletteHist_Nty		=	CS_H2C_NEWALLROULETTEHIST_NTY,
	CS_C2H_ShowSignInChannel_Req		=	CS_C2H_SHOW_SIGNIN_CHANNEL_REQ,	
	CS_H2C_ShowSignInChannel_Ack		=	CS_H2C_SHOW_SIGNIN_CHANNEL_ACK,
	CS_C2H_PlayerSignIn_Req				=	CS_C2H_PLAYER_SIGNIN_REQ,	
	CS_H2C_PlayerSignIn_Ack				=	CS_H2C_PLAYER_SIGNIN_ACK,
	CS_C2H_ShowDepositChannel_Req		=	CS_C2H_SHOW_DEPOSIT_CHANNEL_REQ,
	CS_H2C_ShowDepositChannel_Ack		=	CS_H2C_SHOW_DEPOSIT_CHANNEL_ACK,
    CS_C2H_ShowFristTotalDeposit_Req	=	CS_C2H_SHOW_FRIST_TOTAL_DEPOSIT_REQ,
	CS_H2C_ShowFristTotalDeposit_Ack	=	CS_H2C_SHOW_FRIST_TOTAL_DEPOSIT_ACK,
    CS_C2H_GetGameResultList_Req		=	CS_C2H_GET_GAME_RESULT_LIST_REQ,
	CS_H2C_GetGameResultList_Ack		=	CS_H2C_GET_GAME_RESULT_LIST_ACK,
	CS_C2H_GetGameResult_Req			=	CS_C2H_GET_GAME_RESULT_REQ,
	CS_H2C_GetGameResult_Ack			=	CS_H2C_GET_GAME_RESULT_ACK,
    CS_C2H_ShowVipDetail_Req			=	CS_C2H_SHOW_VIP_DETAIL_REQ,			
	CS_H2C_ShowVipDetail_Ack			=	CS_H2C_SHOW_VIP_DETAIL_ACK,	
    CS_C2H_ReciveSuccour_Req			=	CS_C2H_RECIVE_SUCCOUR_REQ,
	CS_H2C_ReciveSuccour_Ack			=	CS_H2C_RECIVE_SUCCOUR_ACK,
	CS_C2H_ShowSuccour_Req 				=   CS_C2H_SHOW_SUCCOUR_REQ,
	CS_H2C_ShowSuccour_Ack				=   CS_H2C_SHOW_SUCCOUR_ACK,	

	--------------------------------------------------  任务模块 -----------------------
	CS_C2H_GetTaskList_Req = CS_C2H_GET_TASKLIST_REQ,
	CS_H2C_GetTaskList_Ack = CS_H2C_GET_TASKLIST_ACK,
	CS_C2H_AcceptTaskReward_Req = CS_C2H_ACCEPT_TASKREWARD_REQ,
	CS_H2C_AcceptTaskReward_Ack = CS_H2C_ACCEPT_TASKREWARD_ACK,
	

----------------------------------------银行-------------------------------------
	CS_C2H_GetBankInfo_Req				= 	CS_C2H_GETBANKINFO_REQ,
	CS_H2C_GetBankInfo_Ack				= 	CS_H2C_GETBANKINFO_ACK,
	CS_C2H_OperateBank_Req				= 	CS_C2H_OPERATEBANK_REQ,
	CS_H2C_OperateBank_Ack				=	CS_H2C_OPERATEBANK_ACK,
	CS_C2H_SetBankPasswd_Req 			= 	CS_C2H_SETBANKPASSWD_REQ,
	CS_H2C_SetBankPasswd_Ack			= 	CS_H2C_SETBANKPASSWD_ACK,

-- 公告
	CS_H2C_SendCheckVersion_Nty 				=	CS_H2C_SENDCHECKVERSION_NTY,
	CS_H2C_Bulletin_Nty 						= 	CS_H2C_BULLETIN_NTY,
	
	CS_C2H_LoadTokenHistory_Req					=	CS_C2H_LOADTOKENHISTORY_REQ,	
	CS_H2C_LoadTokenHistory_Ack					=	CS_H2C_LOADTOKENHISTORY_ACK,
	CS_S2C_TokenInfo_Nty						=	CS_S2C_TOKENINFO_NTY,	
	CS_S2C_PromoteNum_Nty						=	CS_S2C_PROMOTENUM_NTY,	
-- 推广关系
	CS_H2C_WebGM_Nty							=	CS_H2C_WEBGM_NTY,
	CS_C2H_GetRebateInfo_Req					=	CS_C2H_GETREBATEINFO_REQ,
	CS_H2C_GetRebateInfo_Ack					=	CS_H2C_GETREBATEINFO_ACK,
	CS_C2H_RebateBalance_Req					=	CS_C2H_REBATEBALANCE_REQ,
	CS_H2C_RebateBalance_Ack					=	CS_H2C_REBATEBALANCE_ACK,
	CS_C2H_GetRebateDetail_Req					=	CS_C2H_GETREBATEDETAIL_REQ,
	CS_H2C_GetRebateDetail_Ack					=	CS_H2C_GETREBATEDETAIL_ACK,
	
--二维码分享
	CS_C2H_GenQRCode_Req	=	CS_C2H_GENQRCODE_REQ,
	CS_H2C_GenQRCode_Ack	=	CS_H2C_GENQRCODE_ACK,
	
	CS_C2H_LoadRebateBalHist_Req				=	CS_C2H_LOADREBATEBALHIST_REQ,	
	CS_H2C_LoadRebateBalHist_Ack				=	CS_H2C_LOADREBATEBALHIST_ACK,	
	CS_C2H_GetRebateWeekRank_Req				=	CS_C2H_GETREBATEWEEKRANK_REQ,
	CS_H2C_GetRebateWeekRank_Ack				=	CS_H2C_GETREBATEWEEKRANK_ACK,

    CS_C2H_DayShare_Req					=	CS_C2H_DAYSHARE_REQ,
	CS_H2C_DayShare_Ack					=	CS_H2C_DAYSHARE_ACK,
	CS_C2H_ShowDayShare_Req				=	CS_C2H_SHOW_DAYSHARE_REQ,
	CS_H2C_ShowDayShare_Ack				=	CS_H2C_SHOW_DAYSHARE_ACK,

    CS_C2H_EnterBg_Req					=	CS_C2H_ENTER_BG_REQ,
	CS_H2C_EnterBg_Ack					=	CS_H2C_ENTER_BG_ACK,
	CS_C2H_ExitBg_Req					=	CS_C2H_EXIT_BG_REQ,
	CS_H2C_ExitBg_Ack					=	CS_H2C_EXIT_BG_ACK,

--
	CS_C2H_EnterCP_Req 							=	CS_C2H_ENTERCP_REQ,
	CS_H2C_EnterCP_Ack							=	CS_H2C_ENTERCP_ACK,
	CS_C2H_ExitCP_Req 							=	CS_C2H_EXITCP_REQ,
	CS_H2C_ExitCP_Ack 							=	CS_H2C_EXITCP_ACK,
	CS_C2H_EnterThirdPart_Req					=	CS_C2H_ENTERTHIRDPART_REQ,
	CS_H2C_EnterThirdPart_Ack					=	CS_H2C_ENTERTHIRDPART_ACK,
	CS_C2H_ExitThirdPart_Req					=	CS_C2H_EXITTHIRDPART_REQ,
	CS_H2C_ExitThirdPart_Ack					=	CS_H2C_EXITTHIRDPART_ACK,

-- IM
    CS_C2M_ChatRoom_Login_Req				=	CS_C2M_CHATROOM_LOGIN_REQ,
	CS_M2C_ChatRoom_Login_Ack				=	CS_M2C_CHATROOM_LOGIN_ACK,
	CS_M2C_ChatRoom_Login_Nty				=	CS_M2C_CHATROOM_LOGIN_NTY,
	CS_C2M_ChatRoom_Logout_Req				=	CS_C2M_CHATROOM_LOGOUT_REQ,
	CS_M2C_ChatRoom_Logout_Ack				=	CS_M2C_CHATROOM_LOGOUT_ACK,
	CS_M2C_ChatRoom_Logout_Nty				=	CS_M2C_CHATROOM_LOGOUT_NTY,
	CS_C2M_ChatRoom_SendMessage_Req			=	CS_C2M_CHATROOM_SENDMESSAGE_REQ,
	CS_M2C_ChatRoom_SendMessage_Ack			=	CS_M2C_CHATROOM_SENDMESSAGE_ACK,
	CS_M2C_ChatRoom_SendMessage_Nty			=	CS_M2C_CHATROOM_SENDMESSAGE_NTY,
    CS_M2C_ChatRoom_IsEnableMessage_Nty		=	CS_M2C_CHATROOM_ISENABLEMESSAGE_NTY,
	CS_C2M_ChatRoom_SendMessage2_Req		=	CS_C2M_CHATROOM_SENDMESSAGE2_REQ,
	CS_M2C_ChatRoom_SendMessage2_Ack		=	CS_M2C_CHATROOM_SENDMESSAGE2_ACK,
	CS_M2C_ChatRoom_SendMessage2_Nty		=	CS_M2C_CHATROOM_SENDMESSAGE2_NTY,
	
	CS_C2M_IM_Message_CancelDEL_Req			=	CS_C2M_IM_MESSAGE_CANCELDEL_REQ,
	CS_M2C_IM_Message_CancelDEL_Ack			=	CS_M2C_IM_MESSAGE_CANCELDEL_ACK,
	CS_M2C_IM_Message_CancelDEL_Nty			=	CS_M2C_IM_MESSAGE_CANCELDEL_NTY,
	
	CS_CMC_IM_Heartbeat_Req					=	CS_CMC_IM_HEARTBEAT_REQ,
	CS_CMC_IM_Heartbeat_Ack					=	CS_CMC_IM_HEARTBEAT_ACK,
	CS_M2C_IM_InvalidTag_Nty				=	CS_M2C_IM_INVALIDTAG_NTY,
	CS_M2C_IM_Router_Nty					=	CS_M2C_IM_ROUTER_NTY,
	CS_M2C_IM_kick_Nty						=	CS_M2C_IM_KICK_NTY,
	CS_M2C_IM_disconnecting_Nty				=	CS_M2C_IM_DISCONNECTING_NTY,
	CS_M2C_IM_NoSuch_Nty					=	CS_M2C_IM_NOSUCH_NTY,
	
	CS_C2M_ChatRoom_Ai_Login_Req			=   CS_C2M_CHATROOM_AI_LOGIN_REQ,
	CS_C2M_ChatRoom_Ai_GetAllAiInfo_Req		=   CS_C2M_CHATROOM_AI_GETALLAIINFO_REQ,
	CS_M2C_ChatRoom_Ai_GetAllAiInfo_Ack		=   CS_M2C_CHATROOM_AI_GETALLAIINFO_ACK,

    ---------------------------------------------------------------------------------------------通讯录
	
	CS_C2M_Friend_login_Req					=   CS_C2M_FRIEND_LOGIN_REQ,
	CS_M2C_Friend_login_Ack					=	CS_M2C_FRIEND_LOGIN_ACK,
	CS_C2M_Friend_logout_Req				=	CS_C2M_FRIEND_LOGOUT_REQ,
	CS_M2C_Friend_logout_Ack				=	CS_M2C_FRIEND_LOGOUT_ACK,
	CS_C2M_Friend_SendMessage_Req			=	CS_C2M_FRIEND_SENDMESSAGE_REQ,
	CS_C2M_Friend_SendMessage_Ack			=	CS_C2M_FRIEND_SENDMESSAGE_ACK,
	CS_M2C_Friend_SendMessage_Nty			=	CS_M2C_FRIEND_SENDMESSAGE_NTY,
	CS_C2M_Friend_GetAllFriendDetails_Req	=	CS_C2M_FRIEND_GETALLFRIENDDETAILS_REQ,
	CS_M2C_Friend_GetAllFriendDetails_Ack	=	CS_M2C_FRIEND_GETALLFRIENDDETAILS_ACK,
	
	CS_C2M_Friend_ModifyFriendNote_Req		=	CS_C2M_FRIEND_MODIFYFRIENDNOTE_REQ,
	CS_M2C_Friend_ModifyFriendNote_Ack		=	CS_M2C_FRIEND_MODIFYFRIENDNOTE_ACK,
	CS_C2M_Friend_SearchUser_Req			=	CS_C2M_FRIEND_SEARCHUSER_REQ,
	CS_M2C_Friend_SearchUser_Ack			=	CS_M2C_FRIEND_SEARCHUSER_ACK,
	
	CS_C2M_Friend_AddFriend_Req				=	CS_C2M_FRIEND_ADDFRIEND_REQ,
	CS_M2C_Friend_AddFriend_Ack				=	CS_M2C_FRIEND_ADDFRIEND_ACK,
	CS_M2C_Friend_AddFriend_Nty				=	CS_M2C_FRIEND_ADDFRIEND_NTY,
	CS_C2M_Friend_AddFriend_Final_Req		=	CS_C2M_FRIEND_ADDFRIEND_FINAL_REQ,
	CS_M2C_Friend_AddFriend_Final_Ack		=	CS_M2C_FRIEND_ADDFRIEND_FINAL_ACK,
	CS_M2C_Friend_AddFriend_Final_Nty		=	CS_M2C_FRIEND_ADDFRIEND_FINAL_NTY,
	CS_C2M_Friend_DelOneFriend_Req			=	CS_C2M_FRIEND_DELONEFRIEND_REQ,
	CS_M2C_Friend_DelOneFriend_Ack			=	CS_M2C_FRIEND_DELONEFRIEND_ACK,

	CS_C2M_Group_CreateGroup_Req			=   CS_C2M_GROUP_CREATEGROUP_REQ,
	CS_M2C_Group_CreateGroup_Ack			=   CS_M2C_GROUP_CREATEGROUP_ACK,
    CS_C2M_Group_DeleteGroup_Req			=   CS_C2M_GROUP_DELETEGROUP_REQ,
	CS_M2C_Group_DeleteGroup_Ack			=   CS_M2C_GROUP_DELETEGROUP_ACK,
	CS_M2C_Group_DeleteGroup_Nty			=   CS_M2C_GROUP_DELETEGROUP_NTY,
	
	CS_C2M_Group_AddFriendToGroup_Req		=   CS_C2M_GROUP_ADDFRIENDTOGROUP_REQ,
	CS_M2C_Group_AddFriendToGroup_Ack		=   CS_M2C_GROUP_ADDFRIENDTOGROUP_ACK,
	CS_M2C_Group_AddFriendToGroup_1_Nty		=   CS_M2C_GROUP_ADDFRIENDTOGROUP_1_NTY,
	CS_M2C_Group_AddFriendToGroup_2_Nty		=   CS_M2C_GROUP_ADDFRIENDTOGROUP_2_NTY,
	CS_C2M_Group_ExitGroup_Req				=   CS_C2M_GROUP_EXITGROUP_REQ,
	CS_M2C_Group_ExitGroup_Ack				=   CS_M2C_GROUP_EXITGROUP_ACK,
	CS_M2C_Group_ExitGroup_Nty				=   CS_M2C_GROUP_EXITGROUP_NTY,
	CS_C2M_Group_ReadGroup_Req				=   CS_C2M_GROUP_READGROUP_REQ,
	CS_M2C_Group_ReadGroup_Ack				=   CS_M2C_GROUP_READGROUP_ACK,
	CS_C2M_Group_ReadAllGroup_Req			=   CS_C2M_GROUP_READALLGROUP_REQ,
	CS_M2C_Group_ReadAllGroup_Ack			=   CS_M2C_GROUP_READALLGROUP_ACK,
	CS_C2M_Group_DelMemberFromGroup_Req		=   CS_C2M_GROUP_DELMEMBERFROMGROUP_REQ,
	CS_M2C_Group_DelMemberFromGroup_Ack		=   CS_M2C_GROUP_DELMEMBERFROMGROUP_ACK,
	CS_M2C_Group_DelMemberFromGroup_1_Nty	=   CS_M2C_GROUP_DELMEMBERFROMGROUP_1_NTY,
	CS_M2C_Group_DelMemberFromGroup_2_Nty	=   CS_M2C_GROUP_DELMEMBERFROMGROUP_2_NTY,
	CS_C2M_Group_SendMessage_Req			=   CS_C2M_GROUP_SENDMESSAGE_REQ,
	CS_M2C_Group_SendMessage_Ack			=   CS_M2C_GROUP_SENDMESSAGE_ACK,
	CS_M2C_Group_SendMessage_Nty			=   CS_M2C_GROUP_SENDMESSAGE_NTY,
}
if LUA_VERSION and LUA_VERSION == "5.3" then
	return netLoaderCfg_Regs
end
