--{字段tag（协议内唯一）,是否必须传输的字段，字段名称，字段类型，字段长度，字段描述 }
module(..., package.seeall)

--前端生成订单号
CS_Recharge_Order_Req = {
	{ 1		, 1		, 'm_nType'	, 'UINT'	, 1	, '充值类型'},
	{ 2		, 1		, 'm_nAmount'	, 'UINT'	, 1	, '充值金额'},
}

CS_Recharge_Order_Ack = {
	{ 1		, 1		, 'm_nResult'	, 'SHORT'	, 1	, '操作结果'},
	{ 2		, 1		, 'm_sOrder'	, 'STRING'	, 1	, '订单号'},
}


--前端加载列表
CS_Recharge_List_Req = {
	{ 1		, 1		, 'm_nType'	, 'UINT'	, 1	, '充值类型'},
}

CS_Recharge_List_Ack = {
	{ 1		, 1		, 'm_nType'	, 'UINT'	, 1	, '充值类型'},
	{ 2		, 1		, 'm_lItemList'	, 'PstRechargeItemInfo'	, 100	, '充值列表'},
}