module(..., package.seeall)

-- 通用协议<客户端-服务器>

-- 请求大厅转发请求到其它服务
CS_C2H_TransMsg_Req = {
	{ 1		, 1		, 'm_nToServiceType'		, 'UINT'						, 1		, '目标服务类型 1-token Auth'},
	{ 2		, 1		, 'm_strMsg'				, 'STRING'						, 1		, '转发消息'},
}

-- 转发处理响应
CS_H2C_TransMsg_Ack = {
	{ 1		, 1		, 'm_nRetCode'				, 'INT'							, 1		, '转发结果 具体参见错误码表'},
	{ 2		, 1		, 'm_strMsg'				, 'STRING'						, 1		, '转发消息处理回包'},
}

