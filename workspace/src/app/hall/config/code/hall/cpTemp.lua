module(..., package.seeall)


-- 账号登录协议<客户端-服务器>
CS_C2H_EnterCP_Req = 
{
	{ 1		, 1		, 'm_nThirdType'				, 'INT'										, 1		, '第三方类型 彩票：1，真人:2，电子：3， 体育:4， 捕鱼：5'},
}

CS_H2C_EnterCP_Ack = 
{
	{ 1		, 1		, 'm_nThirdType'				, 'INT'										, 1		, '第三方类型 彩票：1，真人:2，电子：3， 体育:4， 捕鱼：5'},
	{ 2		, 1		, 'm_nRet'						, 'INT'										, 1		, '错误码'},	
	{ 3		, 1		, 'm_webErrMsg'					, 'STRING'									, 1		, '后台错误信息(m_result !=0  && m_webErrCode != 0时有效)'},
	{ 4		, 1		, 'm_accessToken'				, 'STRING'									, 1		, 'accessToken'},
	{ 5		, 1		, 'm_domain'					, 'STRING'									, 1		, '跳转链接'},
}

CS_C2H_ExitCP_Req = 
{

}

CS_H2C_ExitCP_Ack = 
{
	{ 1		, 1		, 'm_nRet'						, 'INT'										, 1		, '错误码'},	
	{ 2		, 1		, 'm_webErrMsg'					, 'STRING'									, 1		, '后台错误信息(m_result !=0  && m_webErrCode != 0时有效)'},
}

-- 进出第三方
CS_C2H_EnterThirdPart_Req = 
{
	{ 1		, 1		, 'm_nThirdType'				, 'INT'									, 1		, '第三方类型 彩票：1，真人:2，电子：3， 体育:4， 捕鱼：5'},
	{ 2		, 1		, 'm_nPlayCode'					, 'STRING'								, 1		, '真人code'},
	{ 3		, 1		, 'm_nPlatType'					, 'INT'									, 1		, '真人平台type'},
	{ 4		, 1		, 'm_gameId'					, 'STRING'								, 1		, '游戏ID'},
}

CS_H2C_EnterThirdPart_Ack = 
{
	{ 1		, 1		, 'm_nThirdType'				, 'INT'										, 1		, '第三方类型 彩票：1，真人:2，电子：3， 体育:4， 捕鱼：5'},
	{ 2		, 1		, 'm_nPlayCode'					, 'STRING'									, 1		, '真人code'},
	{ 3		, 1		, 'm_nPlatType'					, 'INT'										, 1		, '真人平台type'},
	{ 4		, 1		, 'm_gameId'					, 'STRING'								, 1		, '游戏ID'},
	{ 5		, 1		, 'm_nRet'						, 'INT'										, 1		, '错误码'},	
	{ 6		, 1		, 'm_webErrMsg'					, 'STRING'									, 1		, '后台错误信息(m_result !=0  && m_webErrCode != 0时有效)'},
	{ 7		, 1		, 'm_accessToken'				, 'STRING'									, 1		, 'accessToken'},
	{ 8		, 1		, 'm_domain'					, 'STRING'									, 1		, '跳转链接'},
}

CS_C2H_ExitThirdPart_Req = 
{
	{ 1		, 1		, 'm_nThirdType'				, 'INT'									, 1		, '第三方类型 彩票：1，真人:2，电子：3， 体育:4， 捕鱼：5'},
	{ 2		, 1		, 'm_nPlayCode'					, 'STRING'								, 1		, '真人code'},
	{ 3		, 1		, 'm_nPlatType'					, 'INT'									, 1		, '真人平台type'},
}

CS_H2C_ExitThirdPart_Ack = 
{
	{ 1		, 1		, 'm_nRet'						, 'INT'										, 1		, '错误码'},	
	{ 2		, 1		, 'm_webErrMsg'					, 'STRING'									, 1		, '后台错误信息(m_result !=0  && m_webErrCode != 0时有效)'},
	{ 3		, 1		, 'm_domain'					, 'STRING'									, 1		, '跳转链接'},
}


