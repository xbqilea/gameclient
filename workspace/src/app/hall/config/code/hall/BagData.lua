--{字段tag（协议内唯一）,是否必须传输的字段，字段名称，字段类型，字段长度，字段描述 }
module(..., package.seeall)

----------------------------玩家背包查询请求Request------------------------------
-- CS_C2H_GetBagInfo_Req =
-- {
	-- {1, 1, 'm_bagType', 'SHORT', 1, '背包类型（泛型），1：背包，2：福袋,...'},
	-- {2, 1, 'm_itemType', 'UINT', 1, '背包内物品类型，0：查询所有背包物品，1：碎片，2：道具，3：炮台，4：礼包'}

-- }
------------------------------玩家背包查询响应Acknowledge(reply)------------------------------
CS_H2C_GetBagInfo_Ack =
{
	{1, 1, 'm_optCode', 'SHORT',  1, '返回码，0：成功，-1：未知异常'},

	-- {2, 1, 'm_itemType', 'UINT', 1, '返回客户端背包内物品类型，0：所有，1：碎片，2：道具，3：炮台，4：礼包'},
	{2, 1, 'm_items', 'PstBagItem',  2048, '物品列表'},
}


------------------------------背包增量变化(服务端主动推送)玩家背包变化通知Notify------------------------------
CS_H2C_BagChange_Nty =
{
	{1, 1, 'm_changeList', 'PstBagItem', 2048, '物品变化列表'},
}


------------------------------玩家背包获得(增加)------------------------------
--无，背包物品获得是服务端内部完成 --

------------------------------玩家背包合成请求Request------------------------------
CS_C2H_BagCombine_Req =
{
	{1, 1, 'm_combineId', 'UINT', 1, '合成配置表中的合成序号Id'},
	{2, 1, 'm_num', 'UINT', 1, '合成目标数量'},
}

------------------------------玩家背包合成响应Reply------------------------------
CS_H2C_BagCombine_Ack =
{
	{1, 1, 'm_optCode', 'SHORT',  1, '返回码，0：成功，-1：未知异常'},
	{2, 1, 'm_combineId', 'UINT', 1, '合成配置表中的合成序号Id'}
}

--开礼包
CS_C2H_OpenGift_Req = 
{
	{1, 1, "m_openInfo", "PstOpenGiftInfo", 1024, "开启信息"}
}

CS_H2C_OpenGift_Ack = 
{
	{1, 1, "m_optCode", "SHORT", 1, "返回码，0：成功，-1：未知异常"},
	{2, 1, 'm_items', "PstBagItem", 1024, '开启获得物品'}
}

CS_C2H_BagExchange_Req = 
{
	{1, 1, "m_id", "UINT", 1, "背包格子ID"},
	{2, 1, "m_count", "UINT", 1, "使用数量"},
	{3, 1, "m_type", "UINT", 1, "使用类型"},
	{4, 1, "m_extern", "STRING", 1, "其他参数(json串)"}
}

CS_H2C_BagExchange_Ack = 
{
	{1, 1, "m_optCode", "SHORT", 1, "状态码"}
}

CS_C2H_BagDelItem_Req = 
{
	{1, 1, "m_itemList", "PstBagItem", 1024, "要删除的物品"}
}

CS_H2C_BagDelItem_Ack = 
{
	{1, 1, "m_itemList", "UINT", 1024, '删除失败的格子ID'}
}

CS_C2H_GetAddress_Req = {
	{1, 1, "m_accountId", "UINT", 1, "用户ID"}
}

CS_H2C_GetAddress_Ack = {
	{1, 1, "m_addr", "STRING", 1, "用户地址"}
}

CS_C2H_ModAddress_Req = {
	{1, 1, "m_addr", "STRING", 1, "用户地址"}
}

CS_H2C_ModAddress_Ack = {
	{1, 1, "m_optCode", "SHORT", 1, "0：成功 1失败"}
}

CS_C2H_GetExchangeLeftTimes_Req = {
	{1, 1, "m_accountId", "UINT", 1, "用户ID"}
}

CS_H2C_GetExchangeLeftTimes_Ack = {
	{1, 1, "m_ExchangeGoldTimes", "UINT", 1, "兑换金币剩余次数"},
	{2, 1, "m_ExchangeTimes", "UINT", 1, "兑换话费/流量剩余次数"}
}

CS_C2H_TariffeCombine_Req = 
{
	{1, 1, "m_itemId", "UINT", 1, "合成器Id"},
	{2, 1, "m_targetId", "UINT", 1, "合成序号"},
	{3, 1, "m_materials", "PstBagItem", 1024, "合成材料"}
}

CS_H2C_TariffeCombine_Ack = 
{
	{1, 1, "m_optCode", "SHORT", 1, "0:成功，-1：未知异常"}
}

CS_C2H_ActiveItem_Req = 
{
	{1, 1, "m_gridId", "UINT", 1, "格子ID"}
}

CS_H2C_ActiveItem_Ack = {
	{1, 1, "m_optCode", "SHORT", 1, "0:成功, -1:未知错误"}
}

CS_C2H_AddItem_Req = 
{
	{1, 1, "m_items", "PstBagItem", 1024, "物品ID"}
}

CS_H2C_AddItem_Ack = 
{
	{1, 1, "m_optCode", "SHORT", 1, "0:成功, -1:未知错误"}
}


CS_H2C_OrderID_Ack =
{
	{1, 1, "m_orderId", "STRING", 1, "订单ID"},
}

-------------------------------------------------背包道具赠送------------------------------
CS_C2H_DonateItem_Req =
{
	{1, 1, "m_toPlayer", "UINT", 1, "受赠玩家"},
	{2, 1, "m_itemId", "UINT", 1, "物品ID"},
	{3, 1, "m_itemNum", "UINT", 1, "物品数量"},
}

CS_H2C_DonateItem_Ack =
{
	{1, 1, "m_toPlayer", "UINT", 1, "受赠玩家"},
	{2, 1, "m_itemId", "UINT", 1, "物品ID"},
	{3, 1, "m_itemNum", "UINT", 1, "物品数量"},
	{4, 1, "m_toAvatarId", "UINT", 1, "受赠人头像ID"},
	{5, 1, "m_toNickname", "STRING", 1, "受赠人昵称"},
	{6, 1, "m_result", "INT", 1, "0:成功 -X：查找平台错误码csv"},
}

CS_C2H_DonateItemConfirm_Req =
{
	{1, 1, "m_toPlayer", "UINT", 1, "受赠玩家"},
	{2, 1, "m_itemId", "UINT", 1, "物品ID"},
	{3, 1, "m_itemNum", "UINT", 1, "物品数量"},
}

CS_H2C_DonateItemConfirm_Ack =
{
	{1, 1, "m_toPlayer", "UINT", 1, "受赠玩家"},
	{2, 1, "m_itemId", "UINT", 1, "物品ID"},
	{3, 1, "m_itemNum", "UINT", 1, "物品数量"},	
	{4, 1, "m_result", "INT", 1, "0:成功 -X：查找平台错误码csv"},
}

CS_H2C_UpdatePurchase_Nty = 
{
	{1, 1, "m_purchaseList", 'PstPurchaseInfo', 1024, "购买列表"}
}

--   使用传音入密道具请求
CS_C2H_UseBroadcastProp_Req	={
	{ 1		, 1		, 'm_strContent'				, 'STRING'			, 1			, '广播内容'},
	{ 2		, 1		, 'm_nBroadcastType'			, 'UINT'			, 1			, '广播类型 1-全服 '},
	{ 3		, 1		, 'm_nCostPid'					, 'UINT'				, 1			, '消耗道具ID '},

}
--   响应使用传音入密道具请求
CS_H2C_UseBroadcastProp_Ack	={
	{ 1		, 1		, 'm_nRetCode'			, 'INT'						, 1		, '操作返回码 0-成功 <0错误 详情见错误码表'},			
}