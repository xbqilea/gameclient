--{字段tag（协议内唯一）,是否必须传输的字段，字段名称，字段类型，字段长度，字段描述 }
module(..., package.seeall)

--银行数据
CS_C2H_GetBankInfo_Req = 
{
	{1, 1, "m_type", "UINT", 1, "填写0就好了"}
}

CS_H2C_GetBankInfo_Ack = 
{
	{1, 1, 'm_optCode', 'SHORT', 1, "0：成功，-1：未知异常"},
	{2, 1, 'm_gold', 'UINT', 1, '金币数量'},
	{3, 1, 'm_diamond', 'UINT', 1, '钻石数量'}
}

--存款/取款
CS_C2H_OperateBank_Req =
{
	{1, 1, 'm_optType', 'UINT', 1, "0:取款 1:存款"},
	{2, 1, "m_nGold", 'UINT', 1, "金币数量"},
	{3, 1, "m_nDiamond", 'UINT', 1, "钻石数量"},
	{4, 1, "m_passwd", "STRING", 1, "密码(取款的时候需要，存款的时候可以直接传空字符串"},
	{5, 1, "m_syn", "UINT", 1, ""}
}

CS_H2C_OperateBank_Ack =
{
	{1, 1, 'm_optCode', 'SHORT', 1, '0：成功，-1：未知异常'},
	{2, 1, "m_syn", "UINT", 1, ""},
	{3, 1, 'm_optType', 'UINT', 1, "0:取款 1:存款"},
	{4, 1, "m_nGold", 'UINT', 1, "金币数量"},
	{5, 1, "m_nDiamond", 'UINT', 1, "钻石数量"},
}

-- 修改/设置密码
CS_C2H_SetBankPasswd_Req = 
{
	{1, 1, "m_newPwd", "STRING", 1, "新密码"},
	{2, 1, "m_verify", "STRING", 1, "旧密码或验证码（第一次设置的时候填空）"}
}

CS_H2C_SetBankPasswd_Ack = 
{
	{1, 1, 'm_optCode', 'SHORT', 1, '0：成功，-1：未知异常'}
}