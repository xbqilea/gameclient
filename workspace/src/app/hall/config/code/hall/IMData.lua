module(..., package.seeall)


--------------------------------------------------------------------------------------------------------------------------------------------AI相关

--[51]AI，申请登录聊天室
CS_C2M_ChatRoom_Ai_Login_Req =
{
	{1,		1, 'm_userid'				,  'UINT'						, 1		, '用户唯一标识,每个用户不能重复,可以整数(类似于QQ号),也可以是email,也可以是字符串'},
	{2,		1, 'm_RoomID'				,  'UINT'						, 1		, '聊天室ID'},
	{3,		1, 'm_ClientInfo'			,  'STRING'						, 1		, 'APP版本号'},
	{4,		1, 'm_name'					,  'STRING'						, 1		, '用户名称,可重复'},
	{5,		1, 'm_head'					,  'UBYTE'						, 1		, '头像'},
	{6,		1, 'm_vip'					,  'UBYTE'						, 1		, 'VIP等级'},
}

--[53]AI，获得所有的在线AI信息   【备注：某client socket，连接到AI服务器，获得所有的在线AI信息】
CS_C2M_ChatRoom_Ai_GetAllAiInfo_Req =
{
}

--[54]AI，获得所有的在线AI信息--应答
CS_M2C_ChatRoom_Ai_GetAllAiInfo_Ack =
{
	{1, 	1, 'm_AiMember'				,  'PstAiMemberInfo'			, 1024 	, '所有AI在线成员信息'},
}

--------------------------------------------------------------------------------------------------------------------------------------------AI相关


--------------------------------------------------------------------------------------------------------------------------------------------聊天室相关

--[41]某成员，申请登录聊天室    			[修改]
CS_C2M_ChatRoom_Login_Req =
{
	{1,		1, 'm_userid'				,  'UINT'						, 1		, '用户唯一标识,每个用户不能重复,可以整数(类似于QQ号),也可以是email,也可以是字符串'},
	{2,		1, 'm_RoomID'				,  'UINT'						, 1		, '聊天室ID'},
	{3,		1, 'm_ClientInfo'			,  'STRING'						, 1		, 'APP版本号'},
}

--[42]某成员，申请登录聊天室--应答   	[修改]
CS_M2C_ChatRoom_Login_Ack =
{
	{1,		1, 'm_RoomID'				,  'UINT'						, 1		, '聊天室ID'},
	{2,		1, 'm_status'				,  'UBYTE'						, 1		, '0--登录成功，非0--登录失败  	【登录失败，1--重复登录，2--RoomID错误，3--41消息包格式错 等】'},
	{3,		1, 'm_reason'				,  'STRING'						, 1		, '一个字符串或整数，成功时意义不大，失败时 简单描述失败原因，以便于APP调试'},
	{4,		1, 'm_UserCount'			,  'SHORT'						, 1		, '房间内成员数量			【包括自己】'},
	{5,		1, 'm_MessageCount'			,  'UBYTE'						, 1		, '房间内最近的消息数量【一般情况返回 最近100条，除非总共不足100条】'},
	{6,		1, 'm_CompressFlag'			,  'UBYTE'						, 1		, '0--不压缩，非0--压缩  【暂时都不压缩，如果压缩，则对m_Member_AND_Message进行压缩】'},
	{7,		1, 'm_Member_AND_Message'	,  'STRING'						, 1		, '如果压缩，才会在此字段内填值，否则此字段为空。如果有内容: 约2000个成员资料+100条最新的即时消息'},
	{8, 	1, 'm_Member'				,  'PstMemberInfo'				, 2048 	, '聊天室所有成员' },
	{9, 	1, 'm_Message'				,  'PstMessageData'				, 1024 	, '聊天室最近100条消息' },
}

--m_Member_AND_Message格式说明
--<userid1> <name1> <\n>
--<userid2> <name2> <\n>
--<userid3> <name3> <\n>
--......
--<userid3> <name3> <message3> <time> <\n>
--<userid6> <name6> <message6> <time> <\n>
--<userid8> <name8> <message8> <time> <\n>							备注：time，消息发送时间，通过系统函数time(0)得到的整数。
--......

--[43]通告all：某成员进入聊天室			[修改]
CS_M2C_ChatRoom_Login_Nty =
{
	{1,		1, 'm_RoomID'				,  'UINT'						, 1		, '聊天室ID'},
	{2, 	1, 'm_Member'				,  'PstMemberInfo'				, 1 	, '聊天室所有成员'},
}

--------------------------------------------------------------------------------------------------------------------------------------------聊天室相关

--[44]某成员，申请退出某聊天室
CS_C2M_ChatRoom_Logout_Req =
{
	{1,		1, 'm_RoomID'				,  'UINT'						, 1		, '聊天室ID'},
}

--[45]某成员，申请退出某聊天室--应答
CS_M2C_ChatRoom_Logout_Ack =
{
	{1,		1, 'm_RoomID'				,  'UINT'						, 1		, '聊天室ID'},
	{2,		1, 'm_status'				,  'UBYTE'						, 1		, '0--退出成功   1--你没有登录此聊天室,无需退出   2--RoomID不存在'},
	{3,		1, 'm_reason'				,  'STRING'						, 1		, '一个字符串或整数，成功时意义不大，失败时 简单描述失败原因，以便于APP调试'},
}

--[46]通告all：某成员离开某聊天室
CS_M2C_ChatRoom_Logout_Nty =
{
	{1,		1, 'm_RoomID'				,  'UINT'						, 1		, '聊天室ID'},
	{2,		1, 'm_userid'				,  'UINT'						, 1		, '用户唯一标识,每个用户不能重复,可以整数(类似于QQ号),也可以是email,也可以是字符串'},
	{3,		1, 'm_reason'				,  'UBYTE'						, 1		, '1--自行离开[44号消息]    2--异常掉线[socket断了]'},
}

--------------------------------------------------------------------------------------------------------------------------------------------聊天室相关

------------------------------------------------
--[47]某成员，申请聊天室发消息
CS_C2M_ChatRoom_SendMessage_Req =
{
	{1,		1, 'm_RoomID'				,  'UINT'						, 1		, '聊天室ID'},
	{2,		1, 'm_SessionID'			,  'UINT'						, 1		, '自定义唯一标识,取值不能重复'},
	{3,		1, 'm_message'				,  'STRING'						, 1		, '消息内容'},
	{4,     1, 'm_Type'					,  'UINT'						, 1		, '1:普通消息--2:图片消息'}
}

--[48]某成员，申请聊天室发消息--应答
CS_M2C_ChatRoom_SendMessage_Ack =
{
	{1,		1, 'm_RoomID'				,  'UINT'						, 1		, '聊天室ID'},
	{2,		1, 'm_SessionID'			,  'UINT'						, 1		, '取值，跟上面的消息包对应，一一匹配'},
	{3,		1, 'm_status'				,  'UBYTE'						, 1		, '0--发消息成功    非0--发消息失败【可能还没登录，或其他原因】'},
	{4,		1, 'm_reason'				,  'STRING'						, 1		, '一个字符串或整数，成功时意义不大，失败时 简单描述失败原因，以便于APP调试'},
}

--[49]通告all：某成员在聊天室发消息了
CS_M2C_ChatRoom_SendMessage_Nty =
{
	{1,		1, 'm_RoomID'				,  'UINT'						, 1		, '聊天室ID'},
	{2,		1, 'm_userid'				,  'UINT'						, 1		, '用户唯一标识,每个用户不能重复,可以整数(类似于QQ号),也可以是email,也可以是字符串'},
	{3,		1, 'm_message'				,  'STRING'						, 1		, '消息内容'},
	{4,		1, 'm_MessageID'			,  'STRING'						, 1		, '消息ID'},
	{5,     1, 'm_Type'					,  'UINT'						, 1		, '1:普通消息--2:图片消息'}
}
------------------------------------------------

--[50]全体禁言、全体不禁言
CS_M2C_ChatRoom_IsEnableMessage_Nty =
{
	{1,		1, 'm_RoomID'				,  'UINT'						, 1		, '聊天室ID'},
	{2,		1, 'm_Flags'				,  'UBYTE'						, 1		, '0--全体不禁言  1--全体禁言'},
}

------------------------------------------------
--[57]某成员，申请聊天室发消息			【注单分享、抢红包、跟投】
CS_C2M_ChatRoom_SendMessage2_Req =
{
	{1,		1, 'm_RoomID'				,  'UINT'						, 1		, '聊天室ID'},
	{2,		1, 'm_SessionID'			,  'UINT'						, 1		, '自定义唯一标识,取值不能重复'},
	{3,		1, 'm_message'				,  'STRING'						, 1		, '消息内容'},
	{4,     1, 'm_Type'					,  'UINT'						, 1		, '1:普通消息--2:图片消息'}
}

--[58]某成员，申请聊天室发消息--应答	【注单分享、抢红包、跟投】
CS_M2C_ChatRoom_SendMessage2_Ack =
{
	{1,		1, 'm_RoomID'				,  'UINT'						, 1		, '聊天室ID'},
	{2,		1, 'm_SessionID'			,  'UINT'						, 1		, '取值，跟上面的消息包对应，一一匹配'},
	{3,		1, 'm_status'				,  'UBYTE'						, 1		, '0--发消息成功    非0--发消息失败【可能还没登录，或其他原因】'},
	{4,		1, 'm_reason'				,  'STRING'						, 1		, '一个字符串或整数，成功时意义不大，失败时 简单描述失败原因，以便于APP调试'},
}

--[59]通告all：某成员在聊天室发消息了	【注单分享、抢红包、跟投】
CS_M2C_ChatRoom_SendMessage2_Nty =
{
	{1,		1, 'm_RoomID'				,  'UINT'						, 1		, '聊天室ID'},
	{2,		1, 'm_userid'				,  'UINT'						, 1		, '用户唯一标识,每个用户不能重复,可以整数(类似于QQ号),也可以是email,也可以是字符串'},
	{3,		1, 'm_message'				,  'STRING'						, 1		, '消息内容'},
	{4,		1, 'm_MessageID'			,  'STRING'						, 1		, '消息ID'},
	{5,     1, 'm_Type'					,  'UINT'						, 1		, '1:普通消息--2:图片消息'}
}
------------------------------------------------

--[65]消息撤销[删除]
CS_C2M_IM_Message_CancelDEL_Req =
{
	{1,		1, 'm_MessageID'			,  'STRING'						, 1		, '消息ID,主键唯一'},
	{2,		1, 'm_userid'				,  'UINT'						, 1		, '用户ID, 如果是聊天室消息/群组消息,则表示消息发送者; 如果是好友间消息,则表示消息接收者'},
	{3,		1, 'm_RoomID'				,  'UINT'						, 1		, '聊天室ID'},
	{4,		1, 'm_GroupID'				,  'INT'						, 1		, '群组ID'},
}

--备注：主键---MessageID  [如果RoomID>0，则某聊天室消息； 如果GroupID>0，则某群内消息； 否则 普通好友消息]


--[66]消息撤销[删除]--应答
CS_M2C_IM_Message_CancelDEL_Ack =
{
	{1,		1, 'm_MessageID'			,  'STRING'						, 1		, '消息ID,主键唯一'},
	{2,		1, 'm_status'				,  'UBYTE'						, 1		, '0：找到消息，提交成功   非0：错误代码'},
	{3,		1, 'm_reason'				,  'STRING'						, 1		, '一个字符串或整数，成功时意义不大，失败时 简单描述失败原因，以便于APP调试'},
}

--[67]消息撤销[删除]--通告
CS_M2C_IM_Message_CancelDEL_Nty =
{
	{1,		1, 'm_MessageID'			,  'STRING'						, 1		, '消息ID,主键唯一'},
	{2,		1, 'm_from_userid'			,  'UINT'						, 1		, '用户ID,消息原发送者'},
	{3,		1, 'm_RoomID'				,  'UINT'						, 1		, '聊天室ID'},
	{4,		1, 'm_GroupID'				,  'INT'						, 1		, '群组ID'},
	{5,		1, 'm_apply_userid'			,  'UINT'						, 1		, '用户ID,消息撤销者(删除者)'},
}

--------------------------------------------------------------------------------------------------------------------------------------------聊天室相关

--[4801]心跳包在线监测
CS_CMC_IM_Heartbeat_Req =
{
}


--[4802]心跳包在线监测--应答
CS_CMC_IM_Heartbeat_Ack =
{
	{1,		1, 'm_interval'				,  'UBYTE'						, 1		, '字段interval，可为0，也可填写一个值。例如：填写120，则表示server通知client，心跳包发送间隔改为120秒'},
}

--------------------------------------------------------------------------------------------------------------------------------------------聊天室相关

--[111]错误消息包  [当服务端收到无法识别的ID，则发送此消息包]
CS_M2C_IM_InvalidTag_Nty =
{
	{1,		1, 'm_reason'				,  'STRING'						, 1		, '一个字符串,简述错误原因,以便于APP调试'},
}


--[112]服务器重定向  [负载均衡]
CS_M2C_IM_Router_Nty =
{
	{1,		1, 'm_ip'					,  'STRING'						, 1		, '另一台聊天室服务器的 ip'},
	{2,		1, 'm_port'					,  'UINT'						, 1		, '另一台聊天室服务器的 port'},
}


--同一个用户，使用新socket登录，考虑到以下2种情况，因此server需要踢掉旧socket
--1：旧socket已经存在异常，但这个异常1.5分钟之后才能被服务端侦测到 	【旧socket断，client急需重新连接\登录】
--2：在公司电脑登录后，回家想在家里电脑登录，这个时候必须踢掉以前的
--
--[748]客户端重复登录，server踢掉旧客户端，向旧客户端发送748消息	   		【备注：当客户端收到这条消息之后，如果再发现socket中断，则不要再 自动断线重连，除非 手动重连】
CS_M2C_IM_kick_Nty =
{
}


--[316]客户端socket disconnecting			【当server主动断开某个client的socket之前，会先发送这个消息。】
CS_M2C_IM_disconnecting_Nty =
{
}


--[404]server端NOSUCH通知消息
CS_M2C_IM_NoSuch_Nty =
{
	{1,		1, 'm_reason'				,  'STRING'						, 1		, '一个字符串,简述错误原因,以便于APP调试'},
}


--------------------------------------------------------------------------------------------------------------------------------------------好友相关 【登录、登出、好友间互发消息】

------------------------------------------------

--[61]用户登录

CS_C2M_Friend_login_Req =
{
	{1,		1, 'm_userid'				,  'UINT'						, 1		, 'userid'},
	{2,		1, 'm_password'				,  'STRING'						, 1		, '密码：填0'},
	{3,		1, 'm_ClientInfo'			,  'STRING'						, 1		, 'APP版本号：填1'},
}

--[62]用户登录--应答

CS_M2C_Friend_login_Ack =
{
	{1,		1, 'm_result'				,  'SHORT'						, 1		, '0：成功		非0：错误代码'},
	{2,		1, 'm_reason'				,  'STRING'						, 1		, '备注信息'},
}

------------------------------------------------

--[63]用户登出

CS_C2M_Friend_logout_Req =
{
}

--[64]用户登出--应答

CS_M2C_Friend_logout_Ack =
{
	{1,		1, 'm_result'				,  'SHORT'						, 1		, '0：成功		非0：错误代码'},
	{2,		1, 'm_reason'				,  'STRING'						, 1		, '备注信息'},
}

------------------------------------------------

--[4000]好友间发送消息

CS_C2M_Friend_SendMessage_Req =
{
	{1,		1, 'm_to_userid'			,  'UINT'						, 1		, '接收者userid'},
	{2,		1, 'm_message'				,  'STRING'						, 1		, '消息内容'},
	{3,		1, 'm_SessionID'			,  'UINT'						, 1		, '自定义唯一标识,取值不能重复'},
	{4,     1, 'm_Type'					,  'UINT'						, 1		, '1:普通消息--2:图片消息'}
}

--[4002]好友间发送消息--应答

CS_C2M_Friend_SendMessage_Ack =
{
	{1,		1, 'm_to_userid'			,  'UINT'						, 1		, '接收者userid'},
	{2,		1, 'm_SessionID'			,  'UINT'						, 1		, '自定义唯一标识,取值不能重复'},
	{3,		1, 'm_result'				,  'SHORT'						, 1		, '0：成功		非0：错误代码'},
	{4,		1, 'm_reason'				,  'STRING'						, 1		, '备注信息'},
	{5,		1, 'm_MessageID'			,  'STRING'						, 1		, '消息ID'},
}

--[4001]好友间发送消息--转发

CS_M2C_Friend_SendMessage_Nty =
{
	{1,		1, 'm_from_UserInfo'		,  'PstMemberInfo'				, 1		, '发送者userid'},
	{2,		1, 'm_time'					,  'UINT'						, 1		, '通过系统函数time(0)得到的整数'},
	{3,		1, 'm_message'				,  'STRING'						, 1		, '消息内容'},
	{4,		1, 'm_MessageID'			,  'STRING'						, 1		, '消息ID'},
	{5,     1, 'm_Type'					,  'UINT'						, 1		, '1:普通消息--2:图片消息'}
}

------------------------------------------------

--[4739]读取所有的好友详细内容

CS_C2M_Friend_GetAllFriendDetails_Req =
{
}

--[4740]读取所有的好友详细内容--应答

CS_M2C_Friend_GetAllFriendDetails_Ack =
{
	{1,		1, 'm_AllFriend'			,  'PstClassFriendDetailed'		, 1024	, '好友信息列表'},
}

--备注1：用户登录时，由服务器 推送给用户[4740号消息]
--备注2：用户发送4739号消息后，服务器返回4740号消息
--备注3：获得3种信息  A: 分类非0的记录  B: 分类0的记录  C: 分类中没有好友的那些分类信息  【信息比较全面】
--备注4：那是服务端的 "多表关联查询"，得到的好友资料 非常全面，应有尽有。

--[4717]修改好友备注信息

CS_C2M_Friend_ModifyFriendNote_Req =
{
	{1,		1, 'm_userid'				,  'UINT'						, 1		, '好友userid'},
	{2,		1, 'm_note'					,  'STRING'						, 1		, '好友备注信息'},
}

--[4718]修改好友备注信息--应答

CS_M2C_Friend_ModifyFriendNote_Ack =
{
	{1,		1, 'm_userid'				,  'UINT'						, 1		, '好友userid'},
	{2,		1, 'm_result'				,  'SHORT'						, 1		, '0：成功		非0：失败'},
	{3,		1, 'm_reason'				,  'STRING'						, 1		, '备注信息'},
}

--[4719]搜索用户资料

CS_C2M_Friend_SearchUser_Req =
{
	{1,		1, 'm_userid'				,  'UINT'						, 1		, '用户userid'},
}

--[4720]搜索用户资料--应答

CS_M2C_Friend_SearchUser_Ack =
{
	{1,		1, 'm_userid'				,  'UINT'						, 1		, '用户userid'},
	{2,		1, 'm_result'				,  'SHORT'						, 1		, '0：成功		非0：失败'},
	{3,		1, 'm_reason'				,  'STRING'						, 1		, '备注信息'},

	{4,		1, 'm_name'					,  'STRING'						, 1		, '用户名称,可重复'},
	{5,		1, 'm_head'					,  'UBYTE'						, 1		, '头像'},
	{6,		1, 'm_vip'					,  'UBYTE'						, 1		, 'VIP等级'},
	{7,		1, 'm_OldAccount'			,  'STRING'						, 1		, '用户旧账号'},
}

--------------------------------------------------------------------------------------------------------------------------------------------好友相关 【添加好友、删除好友、转移好友到其他分类、好友间互发消息】

------------------------------------------------[以下6条，为添加好友]

--[4741]用户添加好友第一次请求-----由添加者发起

CS_C2M_Friend_AddFriend_Req =
{
	{1,		1, 'm_userid'				,  'UINT'						, 1		, '被添加者的userid'},
	{2,		1, 'm_ClassID'				,  'UINT'						, 1		, '分类ID'},
}

--[4742]用户添加好友第一次应答

CS_M2C_Friend_AddFriend_Ack =
{
	{1,		1, 'm_result'				,  'SHORT'						, 1		, '0：提交成功		非0：提交失败     	[备注：是提交成功，不是添加成功]'},
	{2,		1, 'm_reason'				,  'STRING'						, 1		, '备注信息'},
	{3,		1, 'm_userid'				,  'UINT'						, 1		, '被添加者的userid'},
	{4,		1, 'm_ClassID'				,  'UINT'						, 1		, '分类ID'},
}

--[4710]用户添加好友第一次通知-----把消息转发给“被添加者”

CS_M2C_Friend_AddFriend_Nty =
{
	{1,		1, 'm_user_info1'			,  'PstMemberInfo'				, 1		, '添加者的userid等信息'},
	{2,		1, 'm_ClassID'				,  'UINT'						, 1		, '分类ID'},
	{3,		1, 'm_EncryptData'		    ,  'STRING'					    , 1		, '[添加者userid+被添加者userid]，经服务端加密后获得EncryptData'},
	{4,		1, 'm_time'					,  'UINT'						, 1		, 'time，消息发送时间，通过系统函数time(0)得到的整数'},
}

--[4711]用户添加好友第二次请求-----由被添加者发起

CS_C2M_Friend_AddFriend_Final_Req =
{
	{1,		1, 'm_userid'				,  'UINT'						, 1		, '添加者的userid等信息'},
	{2,		1, 'm_ClassID'				,  'UINT'						, 1		, '分类ID1,添加者的分类ID,保持原值'},
	{3,		1, 'm_EncryptData'		    ,  'STRING'					    , 1		, '同上,其值 必须跟上一条的内容相同，否则会导致 添加好友失败'},
	{4,		1, 'm_result'				,  'SHORT'						, 1		, '0：同意添加		非0：拒绝添加'},
	{5,		1, 'mClassID2'				,  'UINT'						, 1		, '分类ID2,被添加者的分类ID,填0'},
}

--[4712]用户添加好友第二次应答

CS_M2C_Friend_AddFriend_Final_Ack =
{
	{1,		1, 'm_userid'				,  'UINT'						, 1		, '添加者的userid等信息'},
	{2,		1, 'm_ClassID'				,  'UINT'						, 1		, '分类ID'},
	{3,		1, 'm_result'			    ,  'SHORT'					    , 1		, '0：同意添加		非0：拒绝添加'},
	{4,		1, 'm_time'					,  'UINT'						, 1		, 'time，被添加者的操作时间，通过系统函数time(0)得到的整数'},
	{5,		1, 'm_reason'				,  'STRING'						, 1		, '备注信息'},
}

--[4743]用户添加好友第二次通知-----把消息最后转发给“添加者”

CS_M2C_Friend_AddFriend_Final_Nty =
{
	{1,		1, 'm_user_info2'			,  'PstMemberInfo'				, 1		, '被添加者的userid等信息'},
	{2,		1, 'm_ClassID'				,  'UINT'						, 1		, '分类ID'},
	{3,		1, 'm_result'			    ,  'SHORT'					    , 1		, '0：同意添加		非0：拒绝添加'},
	{4,		1, 'm_time'					,  'UINT'						, 1		, 'time，被添加者的操作时间，通过系统函数time(0)得到的整数'},
	{5,		1, 'm_reason'				,  'STRING'						, 1		, '备注信息'},
}

------------------------------------------------[以上6条，为添加好友]

--[4715]用户删除好友

CS_C2M_Friend_DelOneFriend_Req =
{
	{1, 	1, 'm_userid'					,  'UINT'					, 1 	, '好友的userid' },
}

--[4716]用户删除好友--应答

CS_M2C_Friend_DelOneFriend_Ack =
{
	{1,		1, 'm_result'					,  'SHORT'					, 1		, '0：成功		否则：错误代码'},
	{2,		1, 'm_reason'					,  'STRING'					, 1		, '备注信息'},
	{3, 	1, 'm_userid'					,  'UINT'					, 1 	, '好友的userid' },
}

------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------------------------群组相关 start

------------------------------------------------

--[4731]用户新建群组

CS_C2M_Group_CreateGroup_Req =
{
	{1,		1, 'm_ClassID'					,  'UINT'					, 1		, '分类ID'},
	{2,		1, 'm_GroupName'				,  'STRING'					, 1		, '群组名称'},
	{3,		1, 'm_GroupNote'				,  'STRING'					, 1		, '群组备注'},
}

--[4732]用户新建群组--应答

CS_M2C_Group_CreateGroup_Ack =
{
	{1,		1, 'm_GroupID'					,  'INT'					, 1		, '群组ID, >0：成功，返回GroupID, <0：错误代码'},
	{2,		1, 'm_reason'					,  'STRING'					, 1		, '备注信息'},
}

------------------------------------------------

--[4735]用户删除群组

CS_C2M_Group_DeleteGroup_Req =
{
	{1,		1, 'm_GroupID'					,  'INT'					, 1		, '群组ID'},
}

--[4736]用户删除群组--应答

CS_M2C_Group_DeleteGroup_Ack =
{
	{1,		1, 'm_result'					,  'SHORT'					, 1		, '0：删除成功	 非0：删除失败'},
	{2,		1, 'm_reason'					,  'STRING'					, 1		, '备注信息'},
	{3,		1, 'm_GroupID'					,  'INT'					, 1		, '群组ID'},
}

--[4737]用户删除群组--通告群内所有人
CS_M2C_Group_DeleteGroup_Nty =
{
	{1,		1, 'm_GroupID'					,  'INT'					, 1		, '群组ID'},
}

------------------------------------------------

--[4760]用户添加1-N个好友到某个群

CS_C2M_Group_AddFriendToGroup_Req =
{
	{1,		1, 'm_GroupID'					,  'UINT'					, 1		, '群组ID'},
	{2,		1, 'm_SessionID'				,  'UINT'					, 1		, 'SessionID'},
	{3, 	1, 'm_UseridList'				,  'UINT'					, 1024 	, '成员userid' },
}

--[4761]用户添加1-N个好友到某个群--应答

CS_M2C_Group_AddFriendToGroup_Ack =
{
	{1,		1, 'm_GroupID'					,  'UINT'					, 1		, '群组ID'},
	{2,		1, 'm_SessionID'				,  'UINT'					, 1		, 'SessionID'},
	{3,		1, 'm_result'					,  'SHORT'					, 1		, '0：提交成功	 非0：提交失败   [备注：提交成功，不是添加成功]'},
	{4,		1, 'm_reason'					,  'STRING'					, 1		, '备注信息'},
}

--[4762]用户添加1-N个好友到某个群--转发给“在线的被添加者”

CS_M2C_Group_AddFriendToGroup_1_Nty =
{
	{1,		1, 'm_Group'					,  'PstOneGroup'			, 1		, '群组简要信息,不包括群成员'},
}

--[4763]用户添加1-N个好友到某个群--转发给“群内在线的所有成员”

CS_M2C_Group_AddFriendToGroup_2_Nty =
{
	{1,		1, 'm_GroupID'					,  'UINT'					, 1		, '群组ID'},
	{2, 	1, 'm_Member'					,  'PstMemberInfo'			, 1024 	, '刚才的被添加者们'},
}

------------------------------------------------

--[4723]用户退出某群组

CS_C2M_Group_ExitGroup_Req =
{
	{1,		1, 'm_GroupID'					,  'UINT'					, 1		, '群组ID'},
}

--[4724]用户退出某群组--应答

CS_M2C_Group_ExitGroup_Ack =
{
	{1,		1, 'm_result'					,  'SHORT'					, 1		, '0：成功		否则：错误代码'},
	{2,		1, 'm_reason'					,  'STRING'					, 1		, '备注信息'},
	{3,		1, 'm_GroupID'					,  'UINT'					, 1		, '群组ID'},
}

--[4759]用户退出某群组--转发给“群内在线的所有成员”

CS_M2C_Group_ExitGroup_Nty =
{
	{1,		1, 'm_GroupID'					,  'UINT'					, 1		, '群组ID'},
	{2, 	1, 'm_userid'					,  'UINT'					, 1 	, '用户userid' },
}

------------------------------------------------

--[4727]用户读取--某个群组成员信息

CS_C2M_Group_ReadGroup_Req =
{
	{1,		1, 'm_GroupID'					,  'UINT'					, 1		, '群组ID'},
}

--[4728]用户读取--某个群组成员信息--应答

CS_M2C_Group_ReadGroup_Ack =
{
	{1,		1, 'm_result'					,  'SHORT'					, 1		, '0：成功		否则：错误代码'},
	{2,		1, 'm_reason'					,  'STRING'					, 1		, '备注信息'},
	{3,		1, 'm_GroupID'					,  'UINT'					, 1		, '群组ID'},
	{4, 	1, 'm_Member'					,  'PstMemberInfo'			, 1024 	, '群内所有成员,包括群主'},
}

--[4729]用户读取--所有群组成员信息

CS_C2M_Group_ReadAllGroup_Req =
{
}

--[4730]用户读取--所有群组成员信息--应答

CS_M2C_Group_ReadAllGroup_Ack =
{
	{1,		1, 'm_result'					,  'SHORT'					, 1		, '0：成功		否则：错误代码'},
	{2,		1, 'm_reason'					,  'STRING'					, 1		, '备注信息'},
	{3, 	1, 'm_AllGroup'					,  'PstOneGroup'			, 1024 	, '所有的群内所有成员'},
}

--备注：假设平均每个群100人，10个群共1000人，但m_AllMember可能只存放500人，因为：可能存在重复数据。
------------------------------------------------

--[4765]群主删除1-N个群内成员

CS_C2M_Group_DelMemberFromGroup_Req =
{
	{1,		1, 'm_GroupID'					,  'UINT'					, 1		, '群组ID'},
	{2,		1, 'm_SessionID'				,  'UINT'					, 1		, 'SessionID'},
	{3, 	1, 'm_UseridList'				,  'UINT'					, 1024 	, '成员userid' },
}

--[4766]群主删除1-N个群内成员--应答

CS_M2C_Group_DelMemberFromGroup_Ack =
{
	{1,		1, 'm_GroupID'					,  'UINT'					, 1		, '群组ID'},
	{2,		1, 'm_SessionID'				,  'UINT'					, 1		, 'SessionID'},
	{3,		1, 'm_result'					,  'SHORT'					, 1		, '0：提交成功	 非0：提交失败   [备注：提交成功，不是添加成功]'},
	{4,		1, 'm_reason'					,  'STRING'					, 1		, '备注信息'},
}

--[4767]群主删除1-N个群内成员--转发给“被删除者--包括离线用户”

CS_M2C_Group_DelMemberFromGroup_1_Nty =
{
	{1,		1, 'm_GroupID'					,  'UINT'					, 1		, '群组ID'},
	{2, 	1, 'm_userid'					,  'UINT'					, 1 	, '好友的userid' },
	{3,		1, 'm_name'						,  'STRING'					, 1		, '用户名称,可重复'},
	{4,		1, 'm_head'						,  'UBYTE'					, 1		, '头像'},
	{5,		1, 'm_vip'						,  'UBYTE'					, 1		, 'VIP等级'},
}

----[4768]群主删除1-N个群内成员--转发给“群内在线的所有成员”

CS_M2C_Group_DelMemberFromGroup_2_Nty =
{
	{1,		1, 'm_GroupID'					,  'UINT'					, 1		, '群组ID'},
	{2, 	1, 'm_UseridList'				,  'UINT'					, 1024 	, '刚才的被添加者们'},
}

------------------------------------------------

--[4752]群组内群发—请求

CS_C2M_Group_SendMessage_Req =
{
	{1,		1, 'm_GroupID'					,  'UINT'					, 1		, '群组ID'},
	{2,		1, 'm_SessionID'				,  'UINT'					, 1		, 'SessionID'},
	{3,		1, 'm_message'					,  'STRING'					, 1		, '消息内容'},
	{4,     1, 'm_Type'						,  'UINT'					, 1		, '1:普通消息--2:图片消息'}
}	

--[4758]群组内群发—应答

CS_M2C_Group_SendMessage_Ack =
{
	{1,		1, 'm_GroupID'					,  'UINT'					, 1		, '群组ID'},
	{2,		1, 'm_SessionID'				,  'UINT'					, 1		, 'SessionID'},
	{3,		1, 'm_result'					,  'SHORT'					, 1		, '0：提交成功	 非0：提交失败   [备注：提交成功，不是添加成功]'},
	{4,		1, 'm_reason'					,  'STRING'					, 1		, '备注信息'},
}	

--[4757]群组内群发--发送给所有在线用户

--modify
CS_M2C_Group_SendMessage_Nty =
{
	{1,		1, 'm_GroupID'					,  'UINT'					, 1		, '群组ID'},
	{2,		1, 'm_from_UserInfo'			,  'PstMemberInfo'			, 1		, '发送者userid'},
	{3,		1, 'm_time'						,  'UINT'					, 1		, '通过系统函数time(0)得到的整数'},
	{4,		1, 'm_message'					,  'STRING'					, 1		, '消息内容'},
	{5,		1, 'm_GroupName'				,  'STRING'					, 1		, '群组名称'},
	{6,		1, 'm_MessageID'				,  'STRING'					, 1		, '消息ID'},
	{7,     1, 'm_Type'						,  'UINT'					, 1		, '1:普通消息--2:图片消息'}
}	

------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------------------------群组相关 end
