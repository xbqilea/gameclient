module(..., package.seeall)


-- 通用协议<客户端-服务器>
-- 获取验证码
CS_GetNoticeSMSReq = {	
	{ 1		, 1		, 'm_nReqType'				, 'SHORT'						, 1		, '请求类型 1-注册手机账号，2-绑定手机号 3-找回密码 4-解冻临时冻结 5-异常设备验证 6-修改银行密码 7-修改登录密码 8-锁定本机 9-解锁锁定本机，10-动态密码登录'},
	{ 2		, 1		, 'm_strPhoneNumber'		, 'STRING'						, 1		, '注册手机号或者绑定的手机号'},
	{ 3		, 1		, 'm_nTerminalType'			, 'SHORT'						, 1		, '1-PC,2-mobile'},
   { 4		, 1		, 'm_strChannelID'		, 'STRING'							, 1		, '渠道号'},
}

CS_GetNoticeSMSAck = {
	{ 1		, 1		, 'm_nRetCode'				, 'SHORT'						, 1		, '详情参见错误码表'},
}

--版本提示更新
CS_H2C_SendCheckVersion_Nty = 
{
	{1, 1, "m_version", "STRING", 1, "服务器当前版本"}
}

-- 请求获取访问阿里云空间临时凭证
CS_C2H_GetAliAccessKey_Req = {
	{ 1		, 1		, 'm_nReqType'					, 'SHORT'		, 1	, '请求类型【1】请求上传 【2】请求下载'},	
}

-- 请求获取访问阿里云空间临时凭证响应
CS_H2C_GetAliAccessKey_Ack = {
	{ 1		, 1		, 'm_nReqType'					, 'SHORT'		, 1	, '请求类型【1】请求上传 【2】请求下载'},	
	{ 2		, 1		, 'm_strRequestId'				, 'STRING'		, 1	, '阿里云请求id'},		
	{ 3		, 1		, 'm_strRequestKey'				, 'STRING'		, 1	, '阿里云请求key'},
	{ 4		, 1		, 'm_strRequestToken'			, 'STRING'		, 1	, '阿里云临时访问凭证'},
	{ 5		, 1		, 'm_nExpireSecond'				, 'UINT'		, 1	, '有效时长（秒）'},
	{ 6		, 1		, 'm_strRequestAddr'			, 'STRING'		, 1	, '上传下载资源地址'},
	{ 7		, 1		, 'm_nRetCode'					, 'SHORT'		, 1	, '参见错误码表'},
}


-- 向Auth服务请求应用token请求 例如商城购买token
CS_C2A_GetApplyToken_Req = {
}


-- 获取应用token响应
CS_A2C_GetApplyToken_Ack = {
	{ 1		, 1		, 'm_nRetCode'				, 'INT'							, 1		, '操作返回码 具体参见错误码表'},
	{ 2		, 1		, 'm_strToken'			, 'STRING'						, 1		, '应用token'},	
}


-- 第三方账号头像变化通知
CS_H2C_ThirdAccAvatarURLChange_Nty = 
{
	{1, 1, "m_strAvatarURL", "STRING", 1, "第三方账号新头像地址"}
}


-- 通用协议<服务器-服务器>


-- rpc协议请求
SS_rpc_Req = {
	{ 1		, 1		, 'm_nReqId'			, 'UINT'						, 1		, '请求id'},
	{ 2		, 1		, 'm_strMsg'			, 'STRING'						, 1		, '具体请求协议消息'},
}

-- 请求auth服务验证应用token响应
SS_rpc_Ack = {
	{ 1		, 1		, 'm_nReqId'			, 'UINT'						, 1		, 'rpc请求id'},
	{ 2		, 1		, 'm_strMsg'			, 'STRING'						, 1		, '具体回应协议消息'},
	{ 3		, 1		, 'm_nRetCode'			, 'INT'							, 1		, 'rpc调用结果 0:成功 -1:超时'},	
	
}



-- 查询当前手机号是否能发送手机短信
SS_QueryIfCanGetSMSReq = {
	{ 1		, 1		, 'm_strPhoneNumber'		, 'STRING'						, 1		, '查询手机号'},
}

SS_QueryIfCanGetSMSAck = {
	{ 1		, 1		, 'm_strPhoneNumber'		, 'STRING'						, 1		, '查询手机号'},
	{ 2		, 1		, 'm_nRetCode'				, 'SHORT'						, 1		, '错误码参见错误码表'},
}


-- 累加当前手机号获取手机短信次数
SS_AddUpGetSMSRecodeNty = {
	{ 1		, 1		, 'm_strPhoneNumber'		, 'STRING'						, 1		, '累计发送短信手机号'},	
}


-- 请求auth服务生成应用token
SS_GenApplyToken_Req = {
	{ 1		, 1		, 'm_nAccountID'		, 'UINT'						, 1		, '需要生成token玩家id'},
	{ 2		, 1		, 'm_nSessId'			, 'UINT'						, 1		, '需要生成token玩家连接id'},
}

-- 请求auth服务生成应用token响应
SS_GenApplyToken_Ack = {
	{ 1		, 1		, 'm_nAccountID'		, 'UINT'						, 1		, '需要生成token玩家id'},
	{ 2		, 1		, 'm_nSessId'			, 'UINT'						, 1		, '需要生成token玩家连接id'},
	{ 3		, 1		, 'm_nRetCode'			, 'INT'							, 1		, '错误码参见错误码表'},
	{ 4		, 1		, 'm_strToken'			, 'STRING'						, 1		, '生成token'},
}


-- 请求auth服务验证应用token
SS_VerifyApplyToken_Req = {
	{ 1		, 1		, 'm_nAccountID'		, 'UINT'						, 1		, '需要校验token玩家id'},
	{ 2		, 1		, 'm_strToken'			, 'STRING'						, 1		, '生成token'},
}

-- 请求auth服务验证应用token响应
SS_VerifyApplyToken_Ack = {
	{ 1		, 1		, 'm_nAccountID'		, 'UINT'						, 1		, '需要校验token玩家id'},
	{ 2		, 1		, 'm_nRetCode'			, 'INT'							, 1		, '错误码参见错误码表'},	
}


SS_H2B_QuerySummary_Req = {
	{ 1		, 1		, 'm_nAccountId'		, 'UINT'						, 1		, '需要查询摘要信息的玩家id'},
	{ 2		, 1		, 'm_vecSummary'		, 'UINT'						, 1024	, '需要查询的属性id数组 (属性id参考 public/CommonDef.h SyncAttrName枚举)'},
	{ 3		, 1		, 'm_nType'				, 'UINT'						, 1		, '查询业务类型(参考  public/CommonDef.h QuerySummaryType)'},
	{ 4		, 1		, 'm_strExtend'			, 'STRING'						, 1		, '自定义扩展字段'},
}

SS_B2H_QuerySummary_Ack = {
	{ 1		, 1		, 'm_iResult'			, 'INT'							, 1		, '0 ： 成功， -X: 错误码,参见错误码表'},
	{ 2		, 1		, 'm_nAccountId'		, 'UINT'						, 1		, '需要查询摘要信息的玩家id'},
	{ 3		, 1		, 'm_vecSummary'		, 'PstSyncBaseAttrItem'			, 1024	, '需要查询的属性数组 (属性id参考 public/CommonDef.h SyncAttrName枚举)'},
	{ 4		, 1		, 'm_nType'				, 'UINT'						, 1		, '查询业务类型(参考  public/CommonDef.h QuerySummaryType)'},
	{ 5		, 1		, 'm_strExtend'			, 'STRING'						, 1		, '自定义扩展字段'},
}

-- 通知归还ai消息到其它ai服
SS_Trans_DeallocAI_Nty = {
	{ 1		, 1		, 'm_nGameId'		, 'UINT'						, 1		, '归还ai的游戏id'},
	{ 2		, 1		, 'm_nOptType'		, 'UINT'						, 1		, '归还方式 0-列表归还 1-全部归还'},
	{ 3		, 1		, 'm_idList'		, 'UINT'						, 1024		, '归还id列表'},
}

SS_H2S_QueryPlayerHallId_Req = 
{
	{1, 1, "m_nAccountID", "UINT", 1, ""},
}

SS_S2H_QueryPlayerHallId_Ack = 
{
	{1, 1, "m_nAccountID", "UINT", 1, ""},
	{2, 1, "m_nHallID", "UINT", 1, "0:玩家不在线"}
}

SS_H2H_SendDonateItem_Req = 
{
	{1, 1, "m_nDoneeID", 		"UINT", 		1, 		"受赠者ID"},
	{2, 1, "m_nDonorID", 		"UINT", 		1, 		"捐赠者ID"},
	{3, 1, "m_strDonorName", 	"STRING", 		1, 		"捐赠者名字"},
	{4, 1, "m_donateList", 		"PstBagItem", 	1024, 	"捐赠物"},
	{5, 1, "m_strParam", 		"STRING", 		1, 		"扩展参数"},
	{6, 1, "m_strUID", 			"STRING", 		1, 		"流水号"},
	{7, 1, "m_nRecordType", 	"UINT", 		1, 		"业务类型"}
}

SS_H2R_AddRelation_Req = 
{
	{1, 1, "m_relationInfo", "PstRelationInfo", 1, "关系信息"}
}

SS_R2H_AddRelation_Ack = 
{
	{1, 1, "m_relationInfo", "PstRelationInfo", 1, "关系信息"}
}

SS_R2H_UpdateRelation_Nty = 
{
	{1, 1, "m_relationInfo", "PstRelationInfo", 1024, "关系信息"}
}

SS_H2R_LoadRelation_Req = 
{
	{1, 1, "m_accountId", "UINT", 1, ""}
}

CS_H2C_WebGM_Nty = 
{
	{1, 1, "m_strMsg", "STRING", 1, ""}
}

---- 推广返利 -------
SS_H2R_RelationChannel_Nty = 
{
	{1, 1, "m_accountId", "UINT", 1, "玩家ID"},
	{2, 1, "m_goldCoin", "UINT", 1, "玩家金币"},
	{3, 1, "m_nickName", "STRING", 1, "玩家金币"},
	{4, 1, "m_avatarId", "UINT", 1, "玩家头像"},
	{5, 1, "m_msgId", "UINT", 1, "协议ID"},
	{6, 1, "m_strMsg", "STRING", 1, "包装协议体"},
}

-- 请求推广返利信息
CS_C2H_GetRebateInfo_Req = 
{	
}

CS_H2C_GetRebateInfo_Ack = 
{
	{1, 1, "m_accountId", "UINT", 1, "玩家ID"},
	{2, 1, "m_ydTotalAward", "UINT", 1, "昨日总奖励"},
	{3, 1, "m_directNum", "UINT", 1, "直属玩家人数"},
	{4, 1, "m_indirectNum", "UINT", 1, "其他玩家人数"},
	{5, 1, "m_ydDirectAward", "UINT", 1, "昨日直属玩家奖励"},
	{6, 1, "m_ydIndirectAward", "UINT", 1, "昨日其他玩家奖励"},
	{7, 1, "m_historyAward", "UINT", 1, "历史已领取奖励"},
	{8, 1, "m_takeAward", "UINT", 1, "可领取奖励， 只能领取今天之前的未领取的奖励"},
	{9, 1, "m_firstLevelRate", "INT", 1, "第一级返利比例"},
	{10, 1, "m_otherLevelRate", "INT", 1, "第N级返利比例, 除第一级外"},
	{11, 1, "m_result", "INT", 1, "结果 0-表示成功 其它表示失败"},
    {12, 1, "m_promoteType", "INT", 1, "推广类型, 1-无限推广模式 2-单级模式"},
}

-- 请求推广返利信息
CS_C2H_RebateBalance_Req = 
{
	
}

CS_H2C_RebateBalance_Ack = 
{
	{1, 1, "m_accountId", "UINT", 1, "玩家ID"},
	{2, 1, "m_award", "UINT", 1, "可领取奖励， 只能领取今天之前的未领取的奖励, 由大厅发放"},
	{3, 1, "m_historyAward", "UINT", 1, "结算后历史已领取奖励"},
	{4, 1, "m_takeAward", "UINT", 1, "结算后后的可领取值"},
	{5, 1, "m_strBusId", "STRING", 1, "业务关联标识"},
	{6, 1, "m_result", "INT", 1, "结果 0-表示成功 其它表示失败"},
}

-- 请求推广返利信息详细信息
CS_C2H_GetRebateDetail_Req = 
{
	{1, 1, "m_pageIdx"			, "UINT"		, 1, "请求页数，从1开始"},
}

CS_H2C_GetRebateDetail_Ack = 
{
	{1, 1, "m_accountId"		, "UINT"				, 1, "玩家ID"},
	{2, 1, "m_pageIdx"			, "UINT"		, 1, "推广返利详细信息当前请求页数"},
	{3, 1, "m_rebateDetails"	, "PstRebateDetail"		, 15, "推广返利详细信息"},
	{4, 1, "m_totalNum"			, "UINT"		, 1, "推广返利详细信息条数，一页暂定为8条"},
	{5, 1, "m_result", "INT", 1, "结果 0-表示成功 其它表示失败"},
}

CS_C2H_GetRebateWeekRank_Req = 
{
	
}

CS_H2C_GetRebateWeekRank_Ack = 
{
	{1, 1, "m_accountId"		, "UINT"				, 1, "玩家ID"},
	{2, 1, "m_rankList"			, "PstWeekRebateItem"	, 50, "周返利排行榜"},
}


SS_H2R_UpdateNode_Nty = 
{
	{1, 1, "m_state"			, "UBYTE"				, 1, "玩家状态 1-表示在线 0-表示不在线"},
}


--  推广人获取分享朋友圈二维码请求
CS_C2H_GenQRCode_Req	=
{
	{ 1		, 1		, 'm_strGenParam'			, 'STRING'						, 1		, '	生成二维码所需参数 序列化json字符串 内容客户端和网站定义 '},
}

--  响应推广人获取分享朋友圈二维码请求
CS_H2C_GenQRCode_Ack	=
{
	{ 1		, 1		, 'm_nResult'			, 'INT'							, 1		, '0 ： 成功， -X: 错误码,参见错误码表'},
	{ 2		, 1		, 'm_strQRCode'			, 'STRING'			, 1	, '返回二维码信息 序列化json字符串  网站返回生成二维码内容'},
}

-- 幸运转盘抽奖
CS_C2H_DrawLottery_Req = 
{
	{ 1		, 1		, 'm_type'				, 'UINT'		, 1	, '抽奖方式 1-白银转盘 2-黄金转盘 3-钻石转盘'},	
}

CS_H2C_DrawLottery_Ack = 
{
	{ 1		, 1		, 'm_type'				, 'UINT'		, 1	, '抽奖方式 1-白银转盘 2-黄金转盘 3-钻石转盘'},	
	{ 2		, 1		, 'm_index'				, 'UINT'		, 1	, '中奖pos, 0表示未中奖'},	
	{ 3		, 1		, 'm_award'				, 'UINT'		, 1	, '中将金额'},	
	{ 4		, 1		, 'm_ret'				, 'INT'			, 1	, '错误码 0-表示正常,负数表示失败'},			
}

CS_H2C_LuckyRoulette_Nty = 
{
	{ 1		, 1		, 'm_silverSetting'		, 'PstCltLuckyRoulette'		, 13	, '白银转盘配置'},	
	{ 2		, 1		, 'm_silverCost'		, 'UINT'		, 1		, '白银转盘一次抽奖消耗积分数'},	
	{ 3		, 1		, 'm_goldSetting'		, 'PstCltLuckyRoulette'		, 13	, '黄金转盘配置'},	
	{ 4		, 1		, 'm_goldCost'			, 'UINT'		, 1		, '黄金转盘一次抽奖消耗积分数'},	
	{ 5		, 1		, 'm_diamondSetting'	, 'PstCltLuckyRoulette'		, 13	, '钻石转盘配置'},	
	{ 6		, 1		, 'm_diamondCost'		, 'UINT'		, 1	, '钻石转盘一次抽奖消耗积分数'},	
    { 7		, 1		, 'm_openType'			, 'UINT'		, 1	, '开关类型, 0关, 1:开'},	
}

CS_C2H_ShowSignInChannel_Req =
{
}

CS_H2C_ShowSignInChannel_Ack =
{
	{ 1		, 1		, 'm_reciveArr'			, 'UINT'		, 1024	, '已签到的天数'},
	{ 2		, 1		, 'm_start'				, 'UINT'		, 1		, '活动开始时间'},
	{ 3		, 1		, 'm_finish'			, 'UINT'		, 1		, '活动结束时间'},
	{ 4		, 1		, 'm_channelInfo'		, 'PstSignInChannel2Clt'		, 1024	, '渠道签到配置'},
	{ 5		, 1		, 'm_ret'				, 'INT'			, 1	, '错误码 0-表示成功,负数表示失败'},
	{ 6		, 1		, 'm_curDay'			, 'UINT'		, 1		, '今天是第几天签到'},
	{ 7		, 1		, 'm_totalAward'		, 'UINT'		, 1		, '签到活动总奖励'},
	{ 8		, 1		, 'm_isCirculate'		, 'UINT'		, 1		, '是否循环'},
}

CS_C2H_PlayerSignIn_Req =
{
}

CS_H2C_PlayerSignIn_Ack =
{
	{ 1		, 1		, 'm_ret'				, 'INT'			, 1	, '错误码 0-表示成功,负数表示失败'},
	{ 2		, 1		, 'm_day'				, 'UINT'		, 1		, '本次签到第几天'},
	{ 3		, 1		, 'm_award'				, 'UINT'		, 1		, '本次领取天数奖励'},
	{ 4		, 1		, 'm_additionAward'		, 'UINT'		, 1		, '本次领取额外奖励'},
	{ 5		, 1		, 'm_reciveArr'			, 'UINT'		, 1024	, '已签到的天数'},
}

CS_C2H_ShowDepositChannel_Req =
{
}

CS_H2C_ShowDepositChannel_Ack =
{
	{1		, 1			, 'm_ret'				, 'INT'			, 1	, '错误码 0-表示成功,负数表示失败'},
	{2		, 1			, 'm_beginTime'			, 'UINT'		, 1, '活动开始时间'},
	{3		, 1			, 'm_endTime'			, 'UINT'		, 1, '活动结束时间'},
	{4		, 1			, 'm_cfgInfo'			, 'PstDepositChannel2Clt'	, 1024, '充值返利配置'},
}
CS_C2H_ShowFristTotalDeposit_Req =
{
}

CS_H2C_ShowFristTotalDeposit_Ack = 
{
	{ 1		, 1		, 'm_ret'			, 'INT'								, 1		, '错误码 0-表示成功,负数表示失败'},
	{ 2		, 1		, 'm_cfgInfo'		, 'PstFirstTotalDepositCfg2Clt'		, 1		, '推广首次累计充值奖励配置'},
}

CS_C2H_GetGameResult_Req =
{
	{ 1		, 1		, 'm_gameType'				, 'UINT'			, 1		, '游戏类型,101:经典斗地主, 105:百人牛牛, 106:欢乐捕鱼, 141：花色嘉年华, 151：龙虎斗, 161:百家乐, 171:炸金花, 181:财神到, 191:红黑大战, 200:红包接龙'},
}

CS_H2C_GetGameResult_Ack =
{
	{ 1		, 1		, 'm_gameType'				, 'UINT'			, 1		, '游戏类型'},
	{ 2		, 1		, 'm_gameResultArr'			, 'PstGameResult2Clt'		, 1024	, '游戏战绩'},
}
CS_C2H_GetGameResultList_Req = 
{

}

CS_H2C_GetGameResultList_Ack =
{
	{ 1		, 1		, 'm_gameTypeArr'				, 'UINT'			, 1024		, '游戏类型,101:经典斗地主, 105:百人牛牛, 106:欢乐捕鱼, 141：花色嘉年华, 151：龙虎斗, 161:百家乐, 171:炸金花, 181:财神到, 191:红黑大战, 200:红包接龙,201:抢庄牛牛, 202: 9线水果机, 203：飞禽走兽, 204:奔驰宝马'},
}
CS_C2H_ShowVipDetail_Req =
{
}

CS_H2C_ShowVipDetail_Ack =
{	
	{ 1		, 1		, 'm_result'					, 'INT'				, 1		, '0:成功, <0: 失败'},
	{ 2		, 1		, 'm_curVipLevel'				, 'UINT'			, 1		, '当前VIP等级(0代表当前不是VIP)'},
	{ 3		, 1		, 'm_nextVipLevel'				, 'UINT'			, 1		, '下一个VIP等级(100代表当前VIP等级已是系统最大,没有下一个VIP等级)'},
	{ 4		, 1		, 'm_curTotalDeposit'			, 'UINT'			, 1		, '当前累计充值(客户端换算单位)'},
	{ 5		, 1		, 'm_nextTotalDeposit'			, 'UINT'			, 1		, '下一个VIP等级需要满足的累计充值(客户端换算单位)(仅在m_nextVipLevel != 100时有效)'},
	{ 6		, 1		, 'm_vipDetail'					, 'PstVipInfoClt'	, 1024	, 'VIP详情'},
}
CS_C2H_ReciveSuccour_Req =
{
}

CS_H2C_ReciveSuccour_Ack =
{
	{ 1		, 1		, 'm_result'					, 'INT'				, 1		, '0:成功, <0: 失败'},
	{ 2		, 1		, 'm_reciveCnt'					, 'INT'				, 1		, '今日已领取次数'},
	{ 3		, 1		, 'm_totalCnt'					, 'INT'				, 1		, '今日可以领取的总次数(普通用户、VIP用户)'},
	{ 4		, 1		, 'm_succourAward'				, 'INT'				, 1		, '本次救济金(客户端换算单位)'},
	{ 5		, 1		, 'm_isCanRecive'				, 'INT'				, 1		, '是否可以领取,0:否, 1:是'},
	{ 6		, 1		, 'm_leftCnt'					, 'INT'				, 1		, '剩余可领取次数'},
}
CS_C2H_ShowSuccour_Req =
{
}

CS_H2C_ShowSuccour_Ack =
{
	{ 1		, 1		, 'm_result'					, 'INT'				, 1		, '0:成功, <0: 失败'},
	{ 2		, 1		, 'm_reciveCnt'					, 'INT'				, 1		, '今日已领取次数'},
	{ 3		, 1		, 'm_totalCnt'					, 'INT'				, 1		, '今日可以领取的总次数(普通用户、VIP用户)'},
	{ 4		, 1		, 'm_shareConditionCnt'			, 'INT'				, 1		, '第几次开始普通用户需要分享才能领取(VIP用户无此限制)'},
	{ 5		, 1		, 'm_succourAward'				, 'INT'				, 1		, '救济金金额(客户端换算单位)'},
	{ 6		, 1		, 'm_coinLimit'					, 'INT'				, 1		, '领取救济金金币限制(客户端换算单位)(玩家金币 <= coinLimit时，才可以领取救济金)'},
	{ 7		, 1		, 'm_vipLevel'					, 'INT'				, 1		, 'VIP等级(0代表当前不是VIP)'},
	{ 8		, 1		, 'm_isCanRecive'				, 'INT'				, 1		, '是否可以领取,0:否, 1:是'},
	{ 9		, 1		, 'm_todayTotalDeposit'			, 'INT'				, 1		, '今日累计充值金额(客户端换算单位)'},
	{ 10	, 1		, 'm_todayTotalExchange'		, 'INT'				, 1		, '今日累计提现金额(客户端换算单位)'},
	{ 11	, 1		, 'm_leftCnt'					, 'INT'				, 1		, '剩余可领取次数'},
}