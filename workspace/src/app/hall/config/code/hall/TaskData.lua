--{字段tag（协议内唯一）,是否必须传输的字段，字段名称，字段类型，字段长度，字段描述 }
module(..., package.seeall)


-- 客户端请求获取任务列表
CS_C2H_GetTaskList_Req = {
}

-- 服务器返回任务列表
CS_H2C_GetTaskList_Ack = {
	{ 1		, 1		, 'm_retCode'				, 'INT'					, 1   , '0:成功, -x:错误码'},	
	{ 2		, 1		, 'm_taskList'				, 'PstTask'			    , 1024, '任务列表'},
}

-- 客户端请求领取任务奖励
CS_C2H_AcceptTaskReward_Req = {	
	{ 1		, 1		, 'm_taskId'				, 'UINT'				, 1	, '领取任务id'},	
}

-- 服务器返回领取任务奖励
CS_H2C_AcceptTaskReward_Ack = {	
	{ 1		, 1		, 'm_taskId'				, 'UINT'				, 1	, '领取任务id'},
	{ 2		, 1		, 'm_retCode'				, 'INT'				    , 1	, '0:成功, -x:错误码'},		
	{ 3		, 1		, "m_reward"				, 'UINT'				, 1 , '任务奖励(服务器金币)'},	
}


