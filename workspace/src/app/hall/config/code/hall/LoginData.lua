module(..., package.seeall)


-- 账号登录协议<客户端-服务器>

CS_C2L_NoLoginPingReq = {
	{ 1		, 1		, 'm_nClinetStamp'	, 'UINT'	, 1	, '客户端时间戳'},	
}

CS_L2C_NoLoginPingAck = {
	{ 1		, 1		, 'm_nServerStamp'	, 'UINT'	, 1	, '服务器时间戳'},	
	{ 2		, 1		, 'm_nClinetStamp'	, 'UINT'	, 1	, '客户端时间戳'},	
}

CS_C2L_LoginReq		=
{
	{ 1		, 1		, 'm_strAccountName'	, 'STRING'	, 1	, '账号名'},	
	{ 2		, 1		, 'm_strSRPA'			, 'STRING'	, 1	, '一次性随机Key'},
	{ 3		, 1		, 'm_deviceInfo'		, 'PstDeviceInfo'	, 1	, '设备信息'},		
	{ 4		, 1		, 'm_strVerifyCode'		, 'STRING'	, 1	, '仅用于登录类型为动态验证码登录时需要  手机验证码'},
	{ 5		, 1		, 'm_loginType'			, 'SHORT'	, 1	, '1-密码登录 2-动态验证码登录'},
}

CS_L2C_SRP6_1_Ack	=
{
	{ 1		, 1		, 'm_strSalt'		, 'STRING'	, 1	, '一次性随机数'},
	{ 2		, 1		, 'm_strSRPB'	, 'STRING'	, 1	, '一次性随机Key'},
}

CS_C2L_SRP6_2_Req	=
{
	{ 1		, 1		, 'm_strCltSessionKey'		, 'STRING'	, 1	, '客户端生成sessionKey'},

}

CS_L2C_SRP6_3_Ack	=
{
	{ 1		, 1		, 'm_strSessionKey'		, 'STRING'	, 1	, '服务端生成sessionKey'},
}

--  问题验证参数说明
-- id	问题描述						参数

--	1	最近一次登录时间				序列化json字符串 格式为：{"month":1,"day":2,"option":1} “option"按照时间段早晚定例如【0点-6点】为第一个段以此类推
--  2	最近一次登录地区				序列化json int数组字符串 格式为:[1,2] 数组元素为两位 第一个是省级(或直辖市) 第二个是市级
--	3	最后一次玩什么游戏				字符串游戏id 0:没有游戏 >0 具体游戏id
--  4	最经常玩的游戏 					字符串游戏id 0:没有游戏 >0 具体游戏id
		
CS_L2C_LoginAck	=
{
	{ 1		, 1		, 'm_nResult'			, 'INT'			, 1	, '参见错误码表'},
	{ 2		, 1		, 'm_nAccountID'		, 'UINT'		, 1	, '账号ID'},
	{ 3		, 1		, 'm_strDomainName'		, 'STRING'		, 1	, '大厅域名'},
	{ 4		, 1		, 'm_nPort'				, 'INT'			, 1	, '大厅端口'},
	{ 5		, 1		, 'm_nLoginKey'			, 'UINT'		, 1	, '登陆密钥'},
	{ 6		, 1		, 'm_nVerifyReason'		, 'SHORT'		, 1	, '验证原因 【0】无需验证【1】临时冻结【2】异常设备'},
	{ 7		, 1		, 'm_nVerifyType'		, 'SHORT'		, 1	, '验证类型【1】手机验证码验证【2】绑定身份证验证【3】问题验证'},
	{ 8		, 1		, 'm_nQuestID'			, 'SHORT'		, 1	, '验证问题id '},
	{ 9		, 1		, 'm_strQuestPar'		, 'STRING'		, 1	, '问题参数 暂时只有问题2需要用到  返回地区选项参数：序列化json二级数组例如 [[1,2],[3,4]]地区id数组 '},
	{ 10	, 1		, 'm_strPhone'			, 'STRING'		, 1	, '绑定的手机号'},
	{ 11	, 1		, 'm_strAnswerHistory'			, 'STRING'		, 1	, '历史答题记录 json int数组字符串例如[0,-1] 0:成功 -1:失败'},
	{ 12	, 1		, 'm_strAccountName'	, 'STRING'		, 1	, '玩家账号'},	
}

CS_C2H_LoginReq	=
{
	{ 1		, 1		, 'm_nAccountID'		, 'UINT'		, 1	, '账号ID'},
	{ 2		, 1		, 'm_nLoginKey'			, 'UINT'		, 1	, '登陆密钥'},
	{ 3		, 1		, 'm_deviceInfo'		, 'PstDeviceInfo'	, 1	, '设备信息'},	
	{ 4		, 1		, 'm_loginType'			, 'SHORT'		, 1	, '0:正常登陆 1:重连'},
}

CS_H2C_LoginAck	=
{
	{ 1		, 1		, 'm_nResult'			, 'INT'			, 1	, '[0]成功[-1]密钥错误[-x]其他原因'},
	{ 2		, 1		, 'm_nOtherBeforeTermType'			, 'INT'			, 1	, '被顶号方登录设备 0:无被顶号 1: pc 2:phone'},	

	--{ 2		, 1		, 'm_strAccount'		, 'STRING'		, 1	, '登陆的账号'},
}

CS_H2C_AccountDataNty	=
{
	{ 1		, 1		, 'm_nAccountID'		, 'UINT'	, 1	, '账号ID'},
	{ 2		, 1		, 'm_strNickName'		, 'STRING'	, 1	, '昵称'},
	{ 3		, 1		, 'm_strPhoneNumber'	, 'STRING'	, 1	, '手机号'},
	{ 4		, 1		, 'm_nFaceID'			, 'UINT'	, 1	, '头像ID'},	
	{ 6		, 1		, 'm_nSex'				, 'SHORT'	, 1	, '0保密 1男 2女'},
	--{ 7		, 1		, 'm_nMemberLevel'		, 'SHORT'	, 1	, '会员等级'},
	{ 8     , 1     , 'm_nRechargeMoney'    , 'UINT'    , 1 , '充值金额'},
	--{ 10	, 1		, 'm_nExp'				, 'UINT'	, 1	, '经验'},
	--{ 11	, 1		, 'm_nLevel'			, 'UINT'	, 1	, '等级'},
	
	--{ 12	, 1		, 'm_nDiamond'			, 'UINT'	, 1	, '钻石'},
	--{ 13	, 1		, 'm_nABean'			, 'UINT'	, 1	, 'A豆'},
	{ 14	, 1		, 'm_nGoldCoin'			, 'UINT'	, 1	, '金币'},
	--{ 15	, 1		, 'm_nBankDiamond'		, 'UINT'	, 1	, '银行钻石'},
	--{ 16	, 1		, 'm_nBankABean'		, 'UINT'	, 1	, '银行A豆'},
	{ 17	, 1		, 'm_nBankGoldCoin'		, 'UINT'	, 1	, '银行金币'},
	{ 18	, 1		, 'm_strIDCard'			, 'STRING'	, 1	, '身份证号'},	
	{ 19	, 1		, 'm_nIsModifiedNick'	, 'SHORT'	, 1	, '昵称是否修改过【0】未修改 【1】修改'},
	{ 20	, 1		, 'm_nAuditingAvatarID'	, 'UINT'	, 1	, '正在审核头像【0】当前未有审核头像【>0】当前审核对象id'},
	{ 21	, 1		, 'm_customAvatarList'	, 'UINT'	, 10, '自定义头像id列表'},
	{ 22	, 1		, 'm_nIsRegularAccount'	, 'SHORT'	, 1, '是否正式账号【0】游客 【1】正式'},
	{ 23	, 1		, 'm_nIsModifiedBankPwd', 'SHORT'	, 1, '银行密码是否修改过【0】未修改 【1】修改 '},
	{ 24	, 1		, 'm_unlockAvatarList'	, 'UINT'	, 10, '解锁系统头像列表'},
	{ 25	, 1		, 'm_nextUploadAvatarID', 'UINT'	, 1, '下次上传头像可用头像id'},
    { 26    , 1     , 'm_nAvatarFrameID'    , 'UINT'    , 1, '头像框'},
	{ 27	, 1		, 'm_strLockDevice'	, 'STRING'	, 1, '锁定机器信息json格式字符串 "terminalType":1(1-PC,2-mobile);"deviceCode":"xx"'},
	{ 28	, 1		, 'm_smsCoolTimeList'	, 'SHORT'	, 30, '功能获取短信验证冷却时间列表 以请求类型id作为下标索引'},
	{ 29	, 1		, 'm_strBindThirdAccList'	, 'STRING'	, 1, 'json对象数组字符串 格式：[{"type":1,"thirdAcc":"aaaa"}] type:platform_sdk表主键 thirdAcc:第三方账号唯一标识 比如微信openid'},
	--{ 29	, 1		, 'm_nTokenRoundInning'	, 'UINT'	, 1, '代币房局数'},

	{ 30	,1		,'m_strRealName'			,'STRING'	, 1, '实名'},
    { 31	, 1		, 'm_strAlipayID'			, 'STRING'								, 1					, '支付宝账号'},
	--{ 32	,1		, 'm_strBankRealName'			,'STRING'	, 1, '实名'},
	{ 33	, 1		, 'm_strBankID'			, 'STRING'								, 1					, '银行账号'},
    { 34	, 1		, 'm_regularAward'			, 'UINT'								, 1					, '转正赠送金币'},
    { 35	, 1		, 'm_curBetScore'			, 'UINT'								, 1					, '当前抽奖积分'},
	{ 36	, 1		, 'm_tomorrowBetScore'		, 'UINT'								, 1					, '明日有效下注:明日'},
	{ 37	, 1		, 'm_damaNeed'				, 'UINT'								, 1					, '出款需达投注量'},
	{ 38	, 1		, 'm_damaAmount'			, 'UINT'								, 1					, '当前打码量:当前有效投注金额'},
	{ 39	, 1		, 'm_vipLevel'				, 'UINT'								, 1					, 'VIP等级(0代表不是VIP)'},

}

-- 渠道大厅功能开关服务器推送 当登陆成功或者后台有推送新的渠道配置时服务器进行推送 
CS_H2C_FuncSwitchNty = {
	{ 1		, 1		, 'm_OpenNodeIDList'		, 'UINT'	, 300	, '开放大厅功能節點id列表'},
    { 2		, 1		, 'm_promoteType'			, 'UINT'	, 1	, '推广类型, 1-无限推广模式 2-单级模式'},
}

CS_C2H_PingReq =
{
}

CS_H2C_PingAck =
{	
	{1, 1, "m_nTimestamp", "UINT", 1, "服务器当前时间"}
}

--[[
CS_C2G_PingReq =
{
}

CS_G2C_PingAck =
{
}
--]]

-- 登出
CS_C2H_LogoutRep = {

}

-- 服务器主动踢玩家下线
CS_H2C_KickNty =
{
	{ 1		, 1		, 'm_nReason'			, 'SHORT'	, 1	, 'Kick原因 详情参见错误码表'},
	{ 2		, 1		, 'm_nOtherTerminalType', 'SHORT'	, 1	, '当当前被踢原因是顶号时  此值指抢号设备类型 具体设备类型见上'},
}

-- 异常验证请求
CS_C2L_ExceptionVerifyReq =
{
	{ 1		, 1		, 'm_strAnswer'			, 'STRING'	, 1	, '验证答案'},
}

-- 异常验证响应
CS_L2C_ExceptionVerifyAck = 
{
	{ 1		, 1		, 'm_nRetCode'			, 'SHORT'	, 1	, '验证返回码'},
	{ 2		, 1		, 'm_nHasNextVerify'	, 'SHORT'	, 1	, '是否还有下一步验证 【1】还有进一步验证【0】验证至此结束'},
	{ 3		, 1		, 'm_nVerifyType'		, 'SHORT'	, 1	, '验证类型'},
	{ 4		, 1		, 'm_nQuestID'			, 'SHORT'	, 1	, '问题id'},
	{ 5		, 1		, 'm_strQuestPar'		, 'STRING'	, 1	, '问题参数'},
}


-- 重置密码
CS_C2L_ResetPassword_Req = 
{
	{ 1		, 1		, 'm_strNewPassword'		, 'STRING'		, 1	, '新密码'},	

}

-- 重置密码响应
CS_L2C_ResetPassword_Ack = 
{	
	{ 1		, 1		, 'm_nRetCode'					, 'SHORT'		, 1	, '操作码详情参见错误码'},	
}

-- 找回密码
CS_C2L_FindPassword_1_Req = 
{
	{ 1		, 1		, 'm_strAccount'			, 'STRING'		, 1	, '手机号'},	
	{ 2		, 1		, 'm_strChannelKey'			, 'STRING'		, 1	, '渠道号'},
}

-- 找回密码响应
CS_L2C_FindPassword_1_Ack = 
{
	{ 1		, 1		, 'm_strAccountName'			, 'STRING'		, 1	, '账号名'},	
	{ 2		, 1		, 'm_strPhoneNum'				, 'STRING'		, 1	, '手机号'},	
	{ 3		, 1		, 'm_nRetCode'					, 'SHORT'		, 1	, '操作码详情参见错误码'},	
}

-- 找回密码
CS_C2L_FindPassword_2_Req = 
{
	{ 1		, 1		, 'm_strPhoneNum'			, 'STRING'		, 1	, '手机号'},	
	{ 2		, 1		, 'm_strNewPassword'		, 'STRING'		, 1	, '新密码'},
	{ 3		, 1		, 'm_strVerifyCode'			, 'STRING'		, 1	, '手机验证码'},	
}

-- 找回密码响应
CS_L2C_FindPassword_2_Ack = 
{
	{ 1		, 1		, 'm_nRetCode'			, 'SHORT'		, 1	, '参见错误码表'},	

}

-- 锁定本机
CS_C2H_LockLocalDevice_Req = 
{
	{ 1		, 1		, 'm_strPhoneNum'			, 'STRING'		, 1	, '手机号'},	
	{ 2		, 1		, 'm_strVerifyCode'			, 'STRING'		, 1	, '手机验证码'},	
}

-- 锁定本机响应
CS_H2C_LockLocalDevice_Ack = 
{
	{ 1		, 1		, 'm_nRetCode'			, 'SHORT'		, 1	, '参见错误码表'},	

}

-- 解锁锁定本机
CS_C2H_UnlockLocalDevice_Req = 
{	
	{ 1		, 1		, 'm_strVerifyCode'			, 'STRING'		, 1	, '手机验证码'},	
}

-- 解锁锁定本机响应
CS_H2C_UnlockLocalDevice_Ack = 
{
	{ 1		, 1		, 'm_nRetCode'			, 'SHORT'		, 1	, '参见错误码表'},	

}


-- 第三方账号登陆
CS_C2L_ThirdPartyAccLogin_Req = 
{
	{ 1		, 1		, 'm_strThirdAcc'			, 'STRING'			, 1	, '第三方账号'},	
	{ 2		, 1		, 'm_nThirdPartyType'		, 'SHORT'			, 1	, '第三方类型 platform_sdk.csv key值'},
	{ 3		, 1		, 'm_nLoginType'			, 'SHORT'			, 1	, '登陆类型 1-授权 2-本地token'},
	{ 4		, 1		, 'm_strToken'				, 'STRING'			, 1	, '匹配登录类型  1-第三方授权验证token 2-本地token'},
	{ 5		, 1		, 'm_deviceInfo'			, 'PstDeviceInfo'	, 1	, '设备信息'},
	{ 6		, 1		, 'm_strExtInfo'			, 'STRING'			, 1	, '备用字段 json拓展信息'},
}

-- 第三方账号登陆响应
CS_L2C_ThirdPartyAccLogin_Ack = 
{
	{ 1		, 1		, 'm_nResult'			, 'INT'			, 1	, '操作返回码 详情参见错误码表'},	
	{ 2		, 1		, 'm_strToken'			, 'STRING'		, 1	, '下次登陆本地token'},
	{ 3		, 1		, 'm_nAccountID'		, 'UINT'		, 1	, '账号ID'},
	{ 4		, 1		, 'm_strDomainName'		, 'STRING'		, 1	, '大厅域名'},
	{ 5		, 1		, 'm_nPort'				, 'INT'			, 1	, '大厅端口'},
	{ 6		, 1		, 'm_nLoginKey'			, 'UINT'		, 1	, '登陆密钥'},
	{ 7		, 1		, 'm_nVerifyReason'		, 'SHORT'		, 1	, '验证原因 【0】无需验证【1】临时冻结【2】异常设备'},
	{ 8		, 1		, 'm_nVerifyType'		, 'SHORT'		, 1	, '验证类型【1】手机验证码验证【2】绑定身份证验证【3】问题验证'},
	{ 9		, 1		, 'm_nQuestID'			, 'SHORT'		, 1	, '验证问题id '},
	{ 10	, 1		, 'm_strQuestPar'		, 'STRING'		, 1	, '问题参数 暂时只有问题2需要用到  返回地区选项参数：序列化json二级数组例如 [[1,2],[3,4]]地区id数组 '},
	{ 11	, 1		, 'm_strPhone'			, 'STRING'		, 1	, '绑定的手机号'},
	{ 12	, 1		, 'm_strAnswerHistory'	, 'STRING'		, 1	, '历史答题记录 json int数组字符串例如[0,-1] 0:成功 -1:失败'},

	{ 13	, 1		, 'm_strAvatarURL'		, 'STRING'		, 1	, '第三方头像url 仅当为授权验证登陆成功此值才有意义 其它情况此值无意义'},
	{ 14	, 1		, 'm_strAccount'		, 'STRING'				, 1	, '本地账号名'},

}

-- 绑定第三方账号
CS_C2H_BindThirdPartyAcc_Req = 
{
	{ 1		, 1		, 'm_strThirdAcc'			, 'STRING'			, 1	, '第三方账号'},	
	{ 2		, 1		, 'm_nThirdPartyType'		, 'SHORT'			, 1	, '第三方类型 1-微信'},
	{ 3		, 1		, 'm_strToken'				, 'STRING'			, 1	, '第三方授权验证token'},
	{ 4		, 1		, 'm_deviceInfo'			, 'PstDeviceInfo'	, 1	, '设备信息'},
	{ 5		, 1		, 'm_strExtInfo'			, 'STRING'			, 1	, '拓展信息'},
}

-- 绑定第三方账号响应
CS_H2C_BindThirdPartyAcc_Ack = 
{
	{ 1		, 1		, 'm_nResult'			, 'INT'			, 1	, '操作返回码 详情参见错误码表'},	
	{ 2		, 1		, 'm_strToken'			, 'STRING'			, 1	, '绑定账号成功时 此值为下次登陆token 其它情况此值无意义'},
	{ 3		, 1		, 'm_strAvatarURL'		, 'STRING'			, 1	, '第三方头像url'},
}

-- 此协议为补充协议 用于当登录时出现网络延迟没有及时将ip发至服务器 后续再向服务器更新数据
CS_C2H_UpdateIp_Req = 
{
	{ 1		, 1		, 'm_strIp'			, 'STRING'			, 1	, '当前登录ip'},
}

CS_H2C_UpdateIp_Ack = 
{
	{ 1		, 1		, 'm_nResult'			, 'INT'			, 1	, '操作返回码 详情参见错误码表'},
}

CS_S2C_MaintanenceInfo_Nty = 
{
	{1, 1, "m_nStart", "UINT", 1, "维护开始时间"},
	{2, 1, "m_nFinish", "UINT", 1, "维护结束时间"}
}

CS_S2C_TokenInfo_Nty = 
{
	{ 1		, 1		, 'm_nDayToken'				, 'UINT'			, 1	, '今日获得token数据'},
	{ 3		, 1		, 'm_nCalcToken'			, 'UINT'			, 1	, '累计获得代币数'},
}

CS_S2C_PromoteNum_Nty = 
{
	{ 1		, 1		, 'm_nPromoteNum'			, 'UINT'			, 1	, '推广人数量'},
}

