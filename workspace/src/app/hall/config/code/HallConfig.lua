--
-- Author: 
-- Date: 2018-07-27 11:42:15
--
-- print = release_print

local path = "app/MyAppID.luac"
if not cc.FileUtils:getInstance():isFileExist(path) then
    path = "app/MyAppID.lua"
end
local channelinfor  = require(path)
local channelId = 0

if type(channelinfor) == "table" then
    channelId = channelinfor.channel
    GlobalConf.LOGIN_TYPE = channelinfor.loginType
    GlobalConf.SDK_TYPE = channelinfor.sdkType  
    GlobalConf.EXCHANGE_URL =channelinfor.exchangeUrl
    GlobalConf.CHARGE_URL =channelinfor.chargeUrl
    GlobalConf.KEFU_URL =channelinfor.kefuUrl
    GlobalConf.RESOURCE_PATH =channelinfor.resourcePath
else
    channelId = channelinfor
end

  
GlobalConf.CHANNEL_ID = 1020001001 --1005 -- 渠道id(产品号（3）+ 渠道号（4）+ 版本号（3）)
GlobalConf.LOCAL_LOG = true -- 本地写log
GlobalConf.CHANNEL_QQ = '11111111111'
GlobalConf.DOMAIN = "https://api.xbqpthe.com/"
GlobalConf.URL = "983750.com/"

--非官网包不需要更新apk
if string.sub(channelId, 4, 7)~="0001" then
    GlobalConf.UPDATE_APK = false -- 开启更新apk
end

local LogUtil = nil 
if GlobalConf.LOCAL_LOG then
    local tmp_print = print
    function print(...)
        tmp_print(...)
        if LogUtil then
            LogUtil:getInstance():writeLog(...)
        end
    end
end

function is_need_update()
    local path = "app/MyAppConfig.luac"
    if not cc.FileUtils:getInstance():isFileExist(path) then
        path = "app/MyAppConfig.lua"
    end
    if not cc.FileUtils:getInstance():isFileExist(path) then
        return false
    end
    require(path)
    return GlobalConf.TOTAL_UPDATE 
end
GlobalConf.INIT_FUNCTION = function()
    local target = cc.Application:getInstance():getTargetPlatform()
    if GlobalConf.LOCAL_LOG and (target == 4 or target == 5) then
        LogUtil = require("app.hall.base.log.Log")
    end
    if not is_need_update() then
        require("app.MyApp").new():run()
    else
        cc.Director:getInstance():replaceScene(require("app.assetmgr.main.view.RenewScene").new())
    end
end
 
if GlobalConf.CurServerType == GlobalConf.ServerType.NC then
    GlobalConf.LOGIN_SERVER_IP = "182.16.19.226"
    GlobalConf.IM_SERVER_IP = "45.112.205.118"
     GlobalConf.IM_SERVER_PORT = "8818" -- IM服端口 
  --  GlobalConf.LOGIN_SERVER_IP = "172.16.11.32"
--    GlobalConf.LOGIN_SERVER_IP = "172.16.11.98"
--    GlobalConf.LOGIN_SERVER_IP = "172.16.11.116"
    GlobalConf.LOGIN_SERVER_PORT = "9000" -- 登录服端口 
    GlobalConf.BUGLY_APPID = "16c8825b88" -- buglyid
    GlobalConf.DOMAIN = "http://27.50.48.40:9999/"

    local target = cc.Application:getInstance():getTargetPlatform()
    --[[if target == 4 or target == 5 then
        GlobalConf.BUGLY_APPID = "5d28963e85" -- buglyid
    end]]--
elseif GlobalConf.CurServerType == GlobalConf.ServerType.WW then
    GlobalConf.LOGIN_SERVER_IP = "156.224.3.220"
    GlobalConf.LOGIN_SERVER_PORT = "9000" -- 登录服端口 
     GlobalConf.IM_SERVER_IP = "45.112.205.118"
     GlobalConf.IM_SERVER_PORT = "8818" -- IM服端口 
    GlobalConf.BUGLY_APPID = "75c875318f" -- buglyid
    GlobalConf.DOMAIN = "https://api.xbqpthe.com/"
    local target = cc.Application:getInstance():getTargetPlatform()
    --[[if target == 4 or target == 5 then
        GlobalConf.BUGLY_APPID = "ae0f84b4e5" -- buglyid
    end]]--
    
    -- new service
    GlobalConf.LOGIN_SERVER_IP = "appclient.xbqpthe.com" 
    GlobalConf.DOMAIN = "https://api.xbqpthe.com/"
end 