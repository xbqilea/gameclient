module(..., package.seeall)

--------------------------------------报名，取消报名----------------------------------------------
CS_C2M_SignUp_Req = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1	   , '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_nOption'			, 'UBYTE'				, 1	   , '报名种类[0]免费[1|2|3]收费'},
	{ 3		, 1		, 'm_strParam'			, 'STRING'				, 1	   , '扩展参数, json'},
}

CS_M2C_SignUp_Ack = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1	   , '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_ret'				, 'INT'				, 1	   , '进入场景结果'},
	{ 3		, 1		, 'm_strParam'			, 'STRING'				, 1	   , '扩展参数, json'},
}

CS_C2M_CancelSignUp_Req = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1	   , '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_strParam'			, 'STRING'				, 1	   , '扩展参数, json'},
}

CS_M2C_CancelSignUp_Ack = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1	   , '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_ret'				, 'INT'				, 1	   , '报名比赛结果'},
	{ 3		, 1		, 'm_strParam'			, 'STRING'				, 1	   , '扩展参数, json'},
}

CS_C2M_Enter_Req =
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1	   , '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_strParam'			, 'STRING'				, 1	   , '扩展参数, json'},
}

CS_M2C_Enter_Ack =
{
	{ 1, 	1, 'm_gameAtomTypeId',  'UINT'						, 1	   , '游戏最小配置类型ID'},
	{ 2,	1, 'm_ret'			 ,  'INT'						, 1		, '进入结果'},
	{ 3,	1, 'm_retParam'		 ,  'STRING'					, 1		, '进入结果附加信息'},
}

CS_M2C_GameStart_Nty =
{
    { 1  , 1  , 'm_gameAtomTypeId'   , 'UINT'    , 1 , '游戏最小类型ID'},
    { 2  , 1  , 'm_key'      , 'STRING'    , 1 , '密钥'},
    { 3  , 1  , 'm_domainName'    , 'STRING'    , 1 , 'GameServer域名'},
    { 4  , 1  , 'm_port'      , 'UINT'    , 1 , 'GameServer端口'},
}

--------------------------------------进入，退出协议----------------------------------------------
--以下四条协议暂时不用接入
CS_C2M_SEnterScene_Req = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1	   , '游戏最小配置类型ID'},
}

CS_M2C_SEnterScene_Ack = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1	   , '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_ret'				, 'INT'					, 1	   , '进入场景结果'},
}

CS_C2M_SExitScene_Req = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1	   , '游戏最小配置类型ID'},
}

CS_M2C_SExitScene_Ack = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1	   , '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_ret'				, 'INT'					, 1	   , '进入场景结果'},
}

CS_M2C_ShowMessage_Nty = 
{
	{ 1		, 1		, 'm_msgTipId'				, 'UINT'			, 1	    , '消息内容ID, csv配置, csv文件包含大类信息'},
	{ 2		, 1		, 'm_type'					, 'UINT'			, 1	    , '显示方式 1-顶部， 2-提示框'},
	{ 3		, 1		, 'm_params'				, 'STRING'			, 1		, '消息内容参数(包块功能参数)，不能发送带有颜色字体，客户端需要根据这个进行组合,json格式传递'},
}

