require(g_protocolPath.."DataUtil")
-----------------------------------协议ID定义----------------------------
BEGIN_MODULEID("公共结构ID定义",80000) --[80000 -- 80500)
PSTID_INTKVDATA								=   GENPID(0, 'INT KV值')
PSTID_UINTKVDATA							=   GENPID(1, 'UINT KV值')
PSTID_STRKVDATA								=   GENPID(2, 'STRING KV值')

PSTID_SCENESTATE							=	GENPID(3, '场景信息')
PSTID_SCENEVIEW								=	GENPID(4, '场景列表信息')
--PSTID_SERVICECHANGE							=	GENPID(5, "服务变化统计")
PSTID_SCENEREG								=	GENPID(6, "场景注册")

PSTID_USERSTATUSREQ							=	GENPID(7, "玩家场景状态")
PSTID_USERSTATUSACK							=	GENPID(8, "玩家场景状态")
PSTID_GAMEUSERSTATUS						=	GENPID(9, "游戏状态数据")

PSTID_HALLUSER								=	GENPID(10, '大厅玩家')

PSTID_WEALTHNEED							=   GENPID(11, '需要加載屬性')
PSTID_ITEMNEED								=   GENPID(12, '需要加載道具')
PSTID_WEALTHRET								=   GENPID(13, '带回财富数据')

PSTID_ATTRSUPPLY							=   GENPID(20, '补充屬性')
PSTID_ITEMSUPPLY							=   GENPID(21, '补充道具')
PSTID_ATTRCHANGE							=	GENPID(22, '人物属性变化结构')
PSTID_ITEMCHANGE							=	GENPID(23, '道具变化')
PSTID_INTATTRDATA							=	GENPID(24, '属性数值类型携带数据')
PSTID_STRATTRDATA							=	GENPID(25, '属性字符串类型携带数据')
PSTID_ITEMDATA								=	GENPID(26, '道具携带数据')
PSTID_ATTRDATA								=	GENPID(27, '玩家属性数据')

PSTID_GAMEDATA								=   GENPID(42, '进入游戏携带数据')
PSTID_BASEDATA								=	GENPID(43, '属性背包数据')
PSTID_USERDATA								=	GENPID(44, '游戏需要同步到场景数据')

PSTID_GAMEWEALTH							=   GENPID(50, '游戏报名或者其他需要大厅消耗')
PSTID_GAMEWEALTHCHECKRES					=   GENPID(51, '大厅财富数据检测')
PSTID_EXITGAMENTY							=	GENPID(52, '场景列表信息')
PSTID_ENTERGAMENTY							=	GENPID(53, '进入游戏信息')
PSTID_OPERATERES							=	GENPID(54, '玩家操作结果')
PSTID_KICKUSERNTY							=	GENPID(55, '到游戏服踢人')


PSTID_REWARDITEM							=	GENPID(80, '奖励单元信息')
PSTID_TEMPLATEMAILINFO						=	GENPID(81, '模板邮件相关信息')
PSTID_MAILDETAIL							=	GENPID(82, '协议邮件详情')
PSTID_MAILSENDADDITION 						=	GENPID(83, '邮件发送条件')
PSTID_SENDMAIL								=	GENPID(84, '发送邮件信息')
PSTID_AIITEM								=	GENPID(85, 'AI服返回ai结构')
PSTID_RANKITEM			= 	GENPID(86, '玩家排行数据')
PSTID_TASKITEM			= 	GENPID(87, '任务项')
PSTID_REDENVELOPEITEM	= 	GENPID(88, '红包项')
PSTID_MATCHAWARD							=	GENPID(95, "比赛排名奖励配置")

	
-- 游戏基础公用模块-------------------------------------------------------------------------
BEGIN_MODULEID("游戏基础模块<游戏基础协议>",84500) --[84500 -- 85000]
CS_C2G_PING_REQ								=   GENPID(5	, 'Ping游戏')
CS_G2C_PING_ACK								=   GENPID(6	, 'Ping游戏响应')


BEGIN_MODULEID("游戏基础模块<游戏基础协议>",32000) --[32000 -- 32099]
CS_C2G_ENTER_GAME_REQ			=	GENPID(50	, '请求进入游戏')
CS_G2C_ENTER_GAME_ACK			=   GENPID(51	, '响应请求进入游戏')
CS_C2G_USERLEFT_REQ				=   GENPID(52	, '请求退出游戏')
CS_G2C_USERLEFT_ACK				=   GENPID(53	, '响应请求退出游戏')

BEGIN_MODULEID("玩家游戏状态<游戏基础协议>",32200) --[32200 -- 32299]
CS_C2M_SIGNUP_REQ			=   GENPID(1	, '比赛报名')
CS_M2C_SIGNUP_ACK			=   GENPID(2	, '比赛报名响应')
CS_C2M_CANCELSIGNUP_REQ		=   GENPID(3	, '取消报名')
CS_M2C_CANCELSIGNUP_ACK		=   GENPID(4	, '取消报名响应')
CS_C2M_ENTER_REQ			=	GENPID(5	, '进入游戏')
CS_M2C_ENTER_ACK			=	GENPID(6	, '进入游戏响应')
CS_M2C_GAMESTART_NTY		=	GENPID(7	, '通知进入游戏')
CS_M2C_SHOWMESSAGE_NTY		=	GENPID(9	, '提示内容')