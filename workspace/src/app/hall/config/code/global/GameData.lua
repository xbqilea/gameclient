module(..., package.seeall)

-----------------------------------客户端到服务器(流程)------------------------------------------
CS_C2G_EnterGame_Req =
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1	, '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_accountId'			, 'UINT'				, 1 , '玩家ID'},
	{ 4		, 1		, 'm_key'				, 'STRING'				, 1 , '密钥'},	
}

CS_G2C_EnterGame_Ack =
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'		, 1		, '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_result'			, 'SHORT'		, 1		, '0:进入成功, -1:密钥不对'},	
}

CS_C2G_UserLeft_Req = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'		, 1		, '游戏最小配置类型ID'},
}

CS_G2C_UserLeft_Ack = 
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'		, 1		, '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_ret'				, 'SHORT'		, 1		, '玩家强退结果'},
}

CS_C2G_PingReq = 
{
	
}

CS_G2C_PingAck = 
{
	
}