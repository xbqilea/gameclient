module(..., package.seeall)

-- 人物属性信息 --
PstAttrChange = 
{
	{1, 1, 'm_pid', 'UINT',   1, '属性ID'},
	{2, 1, 'm_delta', 'INT',  1, '属性变化量'},
}

-- 玩家道具消耗后者增加		
PstItemChange = 		
{		
	{ 1		, 1		, 'm_itemId'					, 'UINT'				, 1	, '道具ID'},
	{ 2		, 1		, 'm_num'						, 'INT'					, 1 , '数量'},
}

--玩家携带属性数据
PstIntAttrData = 
{
	{ 1		, 1		, 'm_pid'						, 'UINT'	, 1 , '属性id'},
	{ 2		, 1		, 'm_value'						, 'UINT'	, 1	, '属性值'},
	{ 3		, 1		, 'm_delta'						, 'INT'		, 1	, '属性改变值'},
}		
		
--玩家携带属性数据		
PstStrAttrData = 		
{		
	{ 1		, 1		, 'm_pid'						, 'UINT'	, 1 , '属性id'},
	{ 2		, 1		, 'm_value'						, 'STRING'	, 1	, '属性值'},	
}		

----------------------------------------------------补充----------------------------------------------------------
-- 需要人物属性信息 --
PstItemSupply = 
{
	{1		, 1		, 'm_itemId'					, 'UINT',  	1, '属性ID'},
	{2		, 1		, 'm_type'						, 'SHORT',  1, '需要属性类型，0表示默认携带m_num，不够则失败,1表示补充至m_num，不够则全部带入'},
	{3		, 1		, 'm_num'						, 'UINT',  	1, '需要数量'},
}

PstAttrSupply = 
{
	{1		, 1		, 'm_pid'						, 'UINT',  	1, '属性ID'},
	{2		, 1		, 'm_type'						, 'SHORT',  1, '需要属性类型，0表示默认携带m_value，不够则失败,1表示补充至m_value，不够则全部带入'},
	{3		, 1		, 'm_value'						, 'UINT',  	1, '需要数量'},
}
-----------------------------------------------------场景公共---------------------------------------------------------
--玩家携带道具数据
PstItemData = 
{
	{ 1		, 1		, 'm_gridId'					, 'UINT'	, 1 , '唯一ID, 暂未使用'},
	{ 2		, 1		, 'm_type'						, 'UINT'	, 1 , '道具类型'},
	{ 3		, 1		, 'm_itemId'					, 'UINT'	, 1	, '道具ItemId'},	
	{ 4		, 1		, 'm_count'						, 'UINT'	, 1	, '道具数量'},
	{ 5		, 1		, 'm_param'						, 'UINT'	, 1	, '道具其他属性'},
	{ 6		, 1		, 'm_extend'					, 'STRING'	, 1	, '道具扩展属性'},
}		

--玩家携带/补充属性数据
PstAttrData = 
{
	{ 1		, 1		, 'm_pid'						, 'UINT'	, 1 , '属性id'},
	{ 2		, 1		, 'm_type'						, 'SHORT'	,  1, '需要属性类型，0表示默认携带m_num，不够则失败,1表示补充至m_num，不够则全部带入'},
	{ 3		, 1		, 'm_value1'					, 'UINT'	, 1	, '属性值1'},
	{ 4		, 1		, 'm_value2'					, 'STRING'	, 1	, '属性值2'},
}


-- 需要道具信息（携带或者补充使用)
PstItemNeed = 
{
	{1		, 1		, 'm_itemId'					, 'UINT',  	1, '属性ID'},
	{2		, 1		, 'm_type'						, 'SHORT',  1, '需要属性类型，0表示默认携带m_num，不够则失败,1表示补充至m_num，不够则全部带入'},
	{3		, 1		, 'm_num'						, 'UINT',  	1, '需要数量'},
}

-- 需要属性信息（携带或者补充使用)
PstWealthNeed = 
{
	{1		, 1		, 'm_pid'						, 'UINT',  	1, '财富属性ID'},
	{2		, 1		, 'm_type'						, 'SHORT',  1, '需要财富属性类型，0表示默认携带m_value，不够则失败,1表示补充至m_value, 不够则全部带入'},
	{3		, 1		, 'm_value'						, 'UINT',  	1, '需要数量'},
}

-- 带回财富数据
PstWealthRet = 
{
	{ 1		, 1		, 'm_pid'						, 'UINT'	, 1 , '财富属性id， 金币，钻石，a豆，经验， 游戏赢取金币数，游戏时长'},
	{ 2		, 1		, 'm_value'						, 'UINT'	, 1	, '财富属性值'},
}	

-----------------------------------------------------场景公共---------------------------------------------------------
		
--玩家数据同步		
PstBaseData =		
{		
	{ 1		, 1		, 'm_intProps'					, 'PstIntAttrData'		, 100	, '玩家数值属性'},
	{ 2		, 1		, 'm_strProps'					, 'PstStrAttrData'		, 100	, '玩家字符串属性'},
	{ 3		, 1		, 'm_items'						, 'PstItemData'			, 4096	, '玩家基础道具'},
}		
		
--玩家数据同步		
PstUserData = 		
{		
	{ 1		, 1		, 'm_accountId'					, 'UINT'				, 1	, '玩家账户ID'},
	{ 2		, 1		, 'm_intProps'					, 'PstIntAttrData'		, 100	, '玩家数值属性'},
	{ 3		, 1		, 'm_strProps'					, 'PstStrAttrData'		, 100	, '玩家字符串属性'},
	{ 4		, 1		, 'm_items'						, 'PstItemData'			, 4096	, '玩家基础道具'},
}		
		
--进入游戏携带数据
PstGameData = 		
{		
	{ 2		, 1		, 'm_accountId'					, 'UINT'	, 1	, '玩家账户ID'},
	{ 5		, 1		, 'm_hallServiceId'				, 'UINT'	, 1	, '玩家对应大厅服务ID'},
	{ 6		, 1		, 'm_verifyKey'					, 'STRING'	, 1	, '密钥'},
	{ 9		, 1		, 'm_intProps'					, 'PstIntAttrData'		, 100	, '玩家数值属性'},
	{ 10	, 1		, 'm_strProps'					, 'PstStrAttrData'		, 100	, '玩家字符串属性'},
	{ 11	, 1		, 'm_items'						, 'PstItemData'			, 4096	, '玩家基础道具'},
}		
		
--通知进入房间		
PstEnterGameNty =		
{		
	{ 1		, 1		, 'm_gameAtomTypeId'			, 'UINT'				, 1	, '游戏最小类型ID'},
	{ 2		, 1		, 'm_accountId'					, 'UINT'				, 1	, '玩家账户ID'},
	{ 3		, 1		, 'm_roomId'					, 'UINT'				, 1 , '房间编号(自由房填服务器生成的编号，其他填0)'},
	{ 4		, 1		, 'm_key'						, 'STRING'				, 1 , '密钥'},
	{ 5		, 1		, 'm_domainName'				, 'STRING'				, 1	, 'GameServer域名'},
	{ 6		, 1		, 'm_port'						, 'UINT'				, 1	, 'GameServer端口'},
	{ 7		, 1		, 'm_gameServiceId'				, 'UINT'				, 1	, '游戏服服务Id'},
}		
		
--踢出玩家		
PstKickUserNty =		
{		
	{ 1		, 1		, 'm_accountId'					, 'UINT'				, 1	, '玩家账户ID'},
	{ 2		, 1		, 'm_reason'					, 'USHORT'				, 1 , '原因参考枚举UserExitType'},
	{ 3		, 1		, 'm_bForceKick'				, 'USHORT'				, 1 , '是否强踢, 1：强制踢人, 0:弱踢人，标记用，游戏可能优先淘汰, 托管, 或者强行解散'},
}		
		
--玩家退出		
PstExitGameNty =		
{		
	{ 1		, 1		, 'm_gameAtomTypeId'			, 'UINT'				, 1	, '游戏最小类型ID'},
	{ 2		, 1		, 'm_accountId'					, 'UINT'				, 1	, '玩家账户ID'},
}		
		
--进入游戏结果		
PstOperateRes =		
{		
	{ 1		, 1		, 'm_accountId'					, 'UINT'				, 1	, '玩家账户ID'},
	{ 2		, 1		, 'm_param'						, 'SHORT'				, 1 , '操作结果 0表示成功 -1表示失败'},
}		
		
--供大厅检查数据		
PstGameWealth =		
{		
	{ 1		, 1		, 'm_gameAtomTypeId'			, 'UINT'				, 1	    , '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_accountId'					, 'UINT'				, 1	    , '玩家账户ID'},
	{ 3		, 1		, 'm_type'						, 'UINT'				, 1		, '业务类型'},
	{ 4		, 1		, 'm_attrs'						, 'PstAttrChange'		, 4096	, '玩家财富数据'},
	{ 5		, 1		, 'm_items'						, 'PstItemChange'		, 4096	, '玩家基础道具'},
	{ 6		, 1		, 'm_params'					, 'STRING'				, 1		, '扩展属性，如服务费返点需要填充推广人昵称， 服务费总数'},
	{ 7		, 1		, 'm_businessGuid'				, 'STRING'				, 1		, '业务关联标识  必填项'},
}

--供大厅检查数据
PstGameWealthCheckRes =
{
	{ 1		, 1		, 'm_gameAtomTypeId'			, 'UINT'				, 1	    , '游戏最小配置类型ID'},
	{ 2		, 1		, 'm_accountId'					, 'UINT'				, 1	    , '玩家账户ID'},
	{ 3		, 1		, 'm_type'						, 'UINT'				, 1		, '业务类型'},
	{ 4		, 1		, 'm_result'					, 'INT'					, 1		, '操作结果'},
}

------------------------------------------------------------------------------------------------------------------------

PstIntKVData =
{
	{ 1		, 1		, 'm_key'						, 'STRING'					, 1 , 'key值'},
	{ 2		, 1		, 'm_value'						, 'INT'						, 1	, 'value值'},	
}		
		
PstUIntKVData =		
{		
	{ 1		, 1		, 'm_key'						, 'STRING'					, 1 , 'key值'},
	{ 2		, 1		, 'm_value'						, 'UINT'					, 1	, 'value值'},	
}		
		
PstStrKVData =		
{		
	{ 1		, 1		, 'm_key'						, 'STRING'					, 1 , 'key值'},
	{ 2		, 1		, 'm_value'						, 'STRING'					, 1	, 'value值'},	
}

PstSceneState =
{
	{ 1		, 1		, 'm_gameAtomTypeId'			, 'UINT'				, 1	   , '游戏最小配置类型ID'},
	{ 4		, 1		, 'm_intProps'					, 'PstIntKVData'		, 4096	   , 'int数据'},
	{ 5		, 1		, 'm_uintProps'					, 'PstUIntKVData'		, 4096	   , 'uint数据'},
	{ 6		, 1		, 'm_strProps'					, 'PstStrKVData'		, 4096	   , 'string数据'},
}

--------------------------------------------------协议包装-----------------------------------------------------
PstHallUser =
{
	{ 1		, 1		, 'm_gameAtomTypeId'	, 'UINT'				, 1	   , '游戏服务ID'},
	{ 2		, 1		, 'm_accountId'			, 'UINT'				, 1	   , '玩家ID'},
	{ 3     , 1     , 'm_serviceType'       , 'UINT'                , 1    , '游戏服务类型'},
	--{ 4     , 1     , 'm_protoType'       	, 'SHORT'               , 1    , '协议类型 0-产品独立协议 1-产拼公用协议，平台客户端可能需要关心'},
}

---------------------------------------------------邮件--------------------------------------------------------

-- 通用奖励类型
PstRewardItem =
{
	{ 1		, 1		, 'm_id'			, 'UINT'				, 1	, '道具ID 10000-金币 10001-钻石 10002-A币 其它正常道具id'},
	{ 2		, 1		, 'm_num'			, 'UINT'				, 1 , '奖励数量'},	
}

-- 模板邮件参数信息
PstTemplateMailInfo = 
{
	{ 1		, 1		, 'm_templateID'								, 'UINT'									, 1	, '模板ID'},
	{ 2		, 1		, 'm_strCustomParam'							, 'STRING'									, 1, '自定义参数JSON格式 字段名csv配置表中@..@的变量名，字段类型爲字段名前綴str爲字符串，n爲int'},	
}

-- Proto邮件详情
PstMailDetail = 
{
	{ 1		, 1		, 'm_ID'				, 'UINT'				, 1	, '邮件ID'},
	{ 2		, 1		, 'm_title'				, 'STRING'				, 1	, '邮件标题'},	
	{ 3		, 1		, 'm_content'			, 'STRING'				, 1	, '邮件内容'},
	{ 4		, 1		, 'm_rewardList'		, 'PstRewardItem'		, 20 , '邮件附件奖励'},
	{ 5		, 1		, 'm_templateInfo'		, 'PstTemplateMailInfo'	, 1 , '模板邮件相关参数'},
	{ 6		, 1		, 'm_sendDate'			, 'UINT'				, 1 , '发送日期'},
	{ 7		, 1		, 'm_sender'			, 'STRING'				, 1 , '发件人'},	
	{ 8		, 1		, 'm_isTextMail'		, 'SHORT'				, 1 , '是否文本邮件'},
	{ 9		, 1		, 'm_isTemplateMail'	, 'SHORT'				, 1 , '是否模板邮件'},
	{ 10	, 1		, 'm_isGroupMail'		, 'SHORT'				, 1 , '是否群发邮件'},
	{ 11	, 1		, 'm_state'				, 'SHORT'				, 1 , '邮件状态'},	
}

-- Proto邮件发送限制条件
PstMailSendAddition = 
{
	{ 1		, 1		, 'm_sendType'			, 'SHORT'				, 1 , '发送类型，1-全服，2-全服在线,3-列表，4-个人,5-按产品发送'},
	{ 2		, 1		, 'm_toUserList'		, 'UINT'				, 1024, '发送目标玩家ID数组列表'},
	{ 3		, 1		, 'm_productIDList'		, 'UINT'				, 100, '产品号列表'},	
}

-- Proto发送邮件
PstSendMail = 
{
	{ 1		, 1		, 'm_mail'			, 'PstMailDetail'					, 1, '发送邮件内容'},	
	{ 2		, 1		, 'm_sendCondition'	, 'PstMailSendAddition'				, 1 , '发送范围'},
	{ 3		, 1		, 'm_nBusinessType'	, 'UINT'							, 1 , '业务类型 0:未定义类型 >0:具体业务类型'},
	{ 4		, 1		, 'm_nGameType'		, 'UINT'							, 1 , '触发游戏类型 '},
	{ 5		, 1		, 'm_businessGuid'	, 'STRING'							, 1 , '业务关联标识  必填项'},	
}

-- Proto发送邮件
PstSceneReg = 
{
	{ 1		, 1		, 'm_gameAtomTypeID'	, 'UINT'				, 1, '游戏ID'},	
	{ 2		, 1		, 'm_serviceId'			, 'UINT'				, 1, '对应serviceId'},	
}

PstUserStatusReq =
{
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1	   , '玩家ID'},
	{ 2     , 1     , 'm_status'       		, 'UINT'                , 1    , '玩家状态，1-报名 2-进入游戏'},
	{ 3     , 1     , 'm_extend1'       	, 'UINT'                , 1    , '定点赛为开赛时间, 其他为0'},
}

PstUserStatusAck =
{
	{ 1		, 1		, 'm_accountId'			, 'UINT'				, 1	   , '玩家ID'},
	{ 2     , 1     , 'm_ret'       		, 'INT'                	, 1    , '返回值 0-成功 -1-注册时已经存在 -2-注销时已经删除 -3修改时找不到对应游戏数据'},
}

PstGameUserStatus =
{
	{ 1		, 1		, 'm_gameAtomTypeId'		, 'UINT'				, 1	   , '玩家ID'},
	{ 2     , 1     , 'm_status'       			, 'UINT'                , 1    , '玩家状态，1-报名 2-进入游戏'},
	{ 3     , 1     , 'm_extend1'       		, 'UINT'            	, 1    , '定点赛为开赛时间, 其他为0'},
}

--  AI服返回AI结构
PstAIItem = 
{
	{1, 1, 'm_nId', 		'UINT',  	1, 'AI账号id'},
	{2, 1, 'm_strNick', 	'STRING'	,  1, 'ai昵称'},
	{3, 1, 'm_nAvatatId',	'UINT'	,  1, 'ai头像'},
}	

-- 排行项
PstRankItem = 
{
	{ 1		, 1		, 'm_nAccountId'		, 'UINT'						, 1		, '玩家id'},
	{ 2		, 1		, 'm_nAvatarId'			, 'UINT'						, 1		, '玩家头像id'},
	{ 3		, 1		, 'm_nFrameId'			, 'UINT'						, 1		, '玩家头像框id'},
	{ 4		, 1		, 'm_nVipLevel'			, 'UINT'						, 1		, 'vip等級'},
	{ 5		, 1		, 'm_strNick'			, 'STRING'						, 1		, '昵称'},
	{ 6		, 1		, 'm_nVal'				, 'INT'							, 1		, '数值， 金币或者在线时间'},
	{ 7		, 1		, 'm_nAward'			, 'INT'							, 1		, '奖励, 只有斗地主排行榜有, 若未0, 不显示'},
}

-- 任务数据
PstTaskItem = 
{
	{ 1		, 1		, 'm_nTaskId'			, 'UINT'						, 1		, '任务id(配置表中任务id)'},
	{ 2		, 1		, 'm_nCur'				, 'UINT'						, 1		, '当前进度'},
	{ 3		, 1		, 'm_nTarget'			, 'UINT'						, 1		, '任务目标数'},
	{ 4		, 1		, 'm_nState'			, 'UINT'						, 1		, '任务状态 1-未完成 2-已完成 3-已领取'},
}

-- 红包数据
PstRedENVItem = 
{
	{ 1		, 1		, 'm_nCountdown'		, 'UINT'						, 1		, '红包可领取倒计时(以秒为单位)'},
	{ 2		, 1		, 'm_nRemain'			, 'UINT'						, 1		, '当前红包剩余可领取个数'},
	{ 3		, 1		, 'm_nPerLimit'			, 'UINT'						, 1		, '单个红包上限'},
}

PstMatchAward =
{
	{ 1		, 1		, 'm_rankBegin'	, 'UBYTE'		, 1		, '名次范围起始'},
	{ 2		, 1		, 'm_rankEnd'	, 'UBYTE'		, 1		, '名次范围结束'},
	{ 3		, 1		, 'm_award'		, 'UINT'		, 1		, '奖励'},
}







