
local MatchDetailsView = require("src.app.hall.MatchGameList.view.MatchDetailsView")
local MatchController = require("src.app.hall.MatchGameList.control.MatchController")
local MatchEvent = require("src.app.hall.MatchGameList.control.MatchEvent")
local MatchSignView = require("src.app.hall.MatchGameList.view.MatchSignView")

local HNlayer = import("src.app.newHall.HNLayer")
local MatchGameListView = class("MatchGameListView", HNlayer)

function MatchGameListView:ctor()
    self:initData()
    self:initCSB()
    self:initMsgEvent()
    MatchController.getInstance():sendSubscribe()
    ToolKit:registDistructor(self, handler(self, self.onDestory))
end

function MatchGameListView:onDestory()
    self:unMsgEvent()
    MatchController.getInstance():sendCancelSubscribe();
end

function MatchGameListView:initData() 
    self.m_pMatchInfos = nil
    self.m_pItemTables = {}
    self.m_bIsBMButton = true;
end

function MatchGameListView:initCSB()
    self:setName("MatchGameListView")
    local layer = UIAdapter:createNode("hall/MatchGameListView.csb")
    --UIAdapter:adapter(layer);
    self.m_pPanle = layer:getChildByName("panle")
    self:addChild(layer)
    layer:setPosition(0, 0);
    local size = layer:getContentSize();
    self.m_pPanle:setPosition(cc.p(size.width / 2, size.height/2));
    self.m_pPanle:setAnchorPoint(cc.p(0.5, 0.5));

    --UIAdapter:adapter(layer, handler(self, self.onTouchCallback))
    --UIAdapter:praseNode(layer,self)

    self.m_pGameListView = self.m_pPanle:getChildByName("ListView_1")
    self.m_pTmpTable = self.m_pPanle:getChildByName("tmp_item")
    --self.m_pTmpTable:setVisible(false)
    self.m_pTmpTable:setPositionY(-600)

    -- local pCloseButton = self.m_pPanle:getChildByName("close_button")
    -- UIAdapter:registClickCallBack(pCloseButton, handler(self, self.onCloseClick), true);
    self:on_MSG_SUBSCRIBE(MatchEvent.MSG_SUBSCRIBE, MatchController.getInstance():getSubscribeInfos());

    self.m_pImageTitle = self.m_pPanle:getChildByName("image_game_title");
    self.m_pImageTitle:setPositionX(992);
    self.m_pImageTitle:setPositionY(657);
    local point = cc.p(self.m_pImageTitle:getPosition());
    self.m_pImageTitle:setPositionY(900);
    self.m_pImageTitle:setOpacity(0);
    local moveAct = cc.EaseBackOut:create( cc.MoveTo:create(0.4, point) )
    local spawnAct = cc.Spawn:create(cc.FadeIn:create(0.4), moveAct)
    self.m_pImageTitle:runAction(spawnAct);
    self.m_pImageTitle:setContentSize(cc.size(560, 128))

    point = cc.p(self.m_pGameListView:getPosition());
    self.m_pGameListView:setPositionX(display.width);
    self.m_pGameListView:setOpacity(0);
    moveAct = cc.EaseBackOut:create( cc.MoveTo:create(0.4, point) );
    spawnAct = cc.Spawn:create(cc.FadeIn:create(0.4), moveAct)
    self.m_pGameListView:runAction(spawnAct);

end

function MatchGameListView:initMsgEvent()
    addMsgCallBack(self, MatchEvent.MSG_START_UPDATE, handler(self, self.on_MSG_START_UPDATE));
    addMsgCallBack(self, MatchEvent.MSG_GAMERES_UPDATE, handler(self, self.on_MSG_GAMERES_UPDATE));
    addMsgCallBack(self, MatchEvent.MSG_SUBSCRIBE, handler(self, self.on_MSG_SUBSCRIBE));
end

function MatchGameListView:unMsgEvent()
    removeMsgCallBack(self, MatchEvent.MSG_START_UPDATE)
    removeMsgCallBack(self, MatchEvent.MSG_GAMERES_UPDATE)
    removeMsgCallBack(self, MatchEvent.MSG_SUBSCRIBE)
end

function MatchGameListView:onClear()
    
end

function MatchGameListView:addItem(info)
    local item = self.m_pTmpTable:clone();
    -- UIAdapter:adapter(item);
    --item:setVisible(true)
    self.m_pGameListView:pushBackCustomItem(item);
    
    local button_detail = item:getChildByName("button_detail");
    item.m_pBaoMingBtn = item:getChildByName("button_baoming");
    item.m_pFreeBtn = item:getChildByName("button_free");
    
    -- UIAdapter:registClickCallBack(button_detail, handler(self, self.onMatchItemClick), true);
    -- UIAdapter:registClickCallBack(item.m_pBaoMingBtn, handler(self, self.onMatchItemClick), true);
    -- UIAdapter:registClickCallBack(item.m_pFreeBtn, handler(self, self.onMatchItemClick), true);

    button_detail:addTouchEventListener(handler(self, self.onMatchItemClick));
    item.m_pBaoMingBtn:addTouchEventListener(handler(self, self.onMatchItemClick));
    item.m_pFreeBtn:addTouchEventListener(handler(self, self.onMatchItemClick));

    -- local image_title = item:getChildByName("image_title");
    -- image_title:addTouchEventListener(handler(self, self.onMatchItemClick1));

    self:setItemData(item, info);
    if self.m_pItemTables == nil then
        self.m_pItemTables = {};
    end
    self.m_pItemTables[item.m_pInfo.m_gameAtomTypeId] = item
    --self:showDianJi(item)
end

-- function MatchGameListView:onMatchItemClick1(pSender, event)
--     local pItem = pSender:getParent();
--     local btnName = pSender:getName();
-- end

function MatchGameListView:setItemData(item, info)
    local pItemBg = item:getChildByName("item_bg")
    --{ 1		, 1		, 'm_gameAtomTypeId'			, 'UINT'				, 1	, '游戏最小类型ID'},
	--{ 2		, 1		, 'm_matchStartNum'				, 'UINT'				, 1	, '比赛开赛总人数'},
	--{ 3		, 1		, 'm_enrollNum'					, 'UINT'				, 1 , '报名人数'},
	--{ 4		, 1		, 'm_freePlayNum'				, 'UINT'				, 1 , '免费次数 0-不免费'},
	--{ 5		, 1		, 'm_signCost'					, 'UINT'				, 1 , '报名费, 不免费时所需报名费'},
	--{ 6		, 1		, 'm_awards'					, 'PstMatchAward'		, 10, '排名奖励'},
    --{ 7		, 1		, 'm_extra'						, 'STRING'				, 1	, '额外信息'},
    -- { 6		, 1		, 'm_openBeginTime'				, 'INT'					, 1 , '开放开始时间 -1表示无限制 1600表示16点0分'},
	-- { 7		, 1		, 'm_openEndTime'				, 'INT'					, 1 , '开放结束时间 -1表示无限制 1600表示16点0分'},
    item.m_pInfo = info

    local data = getGameSwitchData(item.m_pInfo.m_gameAtomTypeId);
    if data == nil then
        item.m_nKindId = -1;
    else
        local nKindId = data.kindId;
        if nKindId == 25 then
            nKindId = 15;
        end
        if nKindId == 28 then
            nKindId = 206;
        end
        item.m_nKindId = nKindId;
    end

    local res_path = string.format("hall/image/match/Kind_%d/",item.m_nKindId);

    local text_enroll_title = item:getChildByName("text_enroll_title");
    local image_icon = item:getChildByName("image_icon");
    local image_title = item:getChildByName("image_title");
    local image_rank = item:getChildByName("image_rank");
    local image_free_title = item:getChildByName("image_free_title");
    local text_enroll_num = item:getChildByName("text_enroll_num");
    local text_free_title = image_free_title:getChildByName("text_free_title");
    local button_baoming = item:getChildByName("button_baoming");
    local button_free = item:getChildByName("button_free");
    
    text_enroll_title:setSkewX(11.0);
    text_enroll_num:setSkewX(11.0);
    text_free_title:setSkewX(11.0);

    image_title:loadTexture(res_path.."title_"..info.m_gameAtomTypeId..".png");
    image_icon:loadTexture(res_path.."avatar_"..info.m_gameAtomTypeId..".png");
    image_rank:loadTexture(res_path.."rank_"..info.m_gameAtomTypeId..".png");

    text_enroll_title:setScale(0.85)
    text_enroll_title:setPositionX(image_icon:getPositionX() + 115)
    text_enroll_title:setPositionY(image_icon:getPositionY() - 47)
    text_enroll_num:setScale(0.85)
    text_enroll_num:setPositionX(image_icon:getPositionX() + 195)
    text_enroll_num:setPositionY(image_icon:getPositionY() - 47)
    local image_game = item:getChildByName("image_game")
    if not image_game then 
        image_game = image_icon:clone()
        image_icon:getParent():addChild(image_game)
        -- image_game:loadTexture(res_path .. "game.png")
        image_game:loadTexture(res_path.."game_"..info.m_gameAtomTypeId..".png")
        image_game:setPosition(image_icon:getPositionX() + 150, 68)
        image_game:setLocalZOrder(0)
    end

    local pButton = nil

    if info.m_signCost > 0 then
        button_free:setVisible(false);
        button_baoming:setVisible(true);
        pButton = button_baoming
    else
        button_free:setVisible(true);
        button_baoming:setVisible(false);
        pButton = button_free
    end

    if info.m_freePlayNum > 0 then
        image_free_title:setVisible(true);
        local playNum = 0;
        if info.m_extra then
            if string.find( info.m_extra, "playNum" ) then
                local pJson = json.decode(info.m_extra);
                playNum = pJson["playNum"];
            end
        end
        -- text_free_title:setString(string.format( "每日%d次免费%d/%d", info.m_freePlayNum, playNum, info.m_freePlayNum));
        local number = info.m_freePlayNum - playNum;
        if number < 0 then 
            number = 0;
        end
        text_free_title:setString(string.format( "每日免费次数剩余%d次", number));
        if playNum >= info.m_freePlayNum then
            button_free:setVisible(false);
            button_baoming:setVisible(true);
            pButton = button_baoming
        else
            button_free:setVisible(true);
            button_baoming:setVisible(false);
            pButton = button_free
        end
    else
        image_free_title:setVisible(false);
    end
    local text_open_time = item:getChildByName("textOpenTime")
    if text_open_time == nil then
        text_open_time = text_enroll_num:clone()
        item:addChild(text_open_time)
        text_open_time:setPosition(cc.p(button_free:getPosition()))
        text_open_time:setName("textOpenTime")
        text_open_time:setVisible(false)
        text_open_time:setSkewX(11.0);
        text_open_time:setScale(0.75)
    end
    text_enroll_num:setString(string.format( "%d/%d", info.m_enrollNum, info.m_matchStartNum));
    text_enroll_title:setString("参加中...")
    if info.m_openBeginTime == -1 or info.m_openEndTime == -1 then
        text_enroll_num:setVisible(true)    
        text_open_time:setVisible(false)
    else
        local localTime = tonumber(os.date("%H%M", os.time()))
        if info.m_openBeginTime > localTime or info.m_openEndTime < localTime then
            -- 未开赛
            text_enroll_title:setString("未开赛")
            text_enroll_title:setPositionX(image_game:getPositionX())
            text_open_time:setVisible(true)
            pButton:setVisible(false)
            text_enroll_num:setVisible(false)
            local getOpenTime = function (_nTime, _isb)
                local _str = string.format( "%.02f", _nTime / 100 )
                local resultStrList = {}
                string.gsub(_str,'[^.]+',function ( w )
                    table.insert(resultStrList,w)
                end)
                local _h = tonumber(resultStrList[1])
                local _m = tonumber(resultStrList[2])
                local _ret = resultStrList[1].."点"
                if _isb then
                    if _h <= 12 then
                        _ret = "上午"..resultStrList[1].."点"
                    else
                        _ret = "下午"..resultStrList[1].."点"
                    end
                end

                if _m ~= 0 then
                    _ret = _ret..tonumber(resultStrList[2]).."分"
                end

                return _ret
            end
            text_open_time:setString("开赛时间：\n每日"..getOpenTime(info.m_openBeginTime, true).."—"..getOpenTime(info.m_openEndTime))
        else
            -- 开赛中
            text_open_time:setVisible(false)
            text_enroll_num:setVisible(true)
        end
    end
    


    
    --
    

    -- local enroll_num_back = pItemBg:getChildByName("enroll_num_back")
    -- local rank_num_back = pItemBg:getChildByName("rank_num_back")
    -- local text_title = pItemBg:getChildByName("text_title")
    -- local doc_button = pItemBg:getChildByName("doc_button")
    -- local icon = pItemBg:getChildByName("icon")
    -- local freenumber = pItemBg:getChildByName("number")
    -- local baoming_button = pItemBg:getChildByName("baoming_button")
    -- local free_button = pItemBg:getChildByName("free_button")

    -- enroll_num_back:getChildByName("text_title"):setString(string.format( "满人%d人",info.m_matchStartNum ))
    -- enroll_num_back:getChildByName("text_number"):setString(string.format( "%d",info.m_enrollNum ))

    -- icon:loadTexture("hall/image/match/match_coin.png");
    
    -- text_title:setString("")
    -- freenumber:setString("")
    -- if item.m_pTextTile ~= nil then
    --     item:removeChild(item.m_pTextTile)
    --     item.m_pTextTile = nil
    -- end

    -- item.m_pTextTile = self:createRichText(item, text_title)
    
    -- if info.m_signCost > 0 then
    --     baoming_button:setVisible(true)
    --     free_button:setVisible(false)
    --     baoming_button:getChildByName("text_number"):setString(string.format( "%d", info.m_signCost /100 ))

    --     item.m_pTextTile:pushBackElement(ccui.RichElementText:create(1, text_title:getColor(), 255, "比赛场·", "", 28))
    -- else 
    --     baoming_button:setVisible(false)
    --     free_button:setVisible(true)
    --     --local pFreeNumber = self:createRichText(item, freenumber) 
    --     item.m_pTextTile:pushBackElement(ccui.RichElementText:create(1, text_title:getColor(), 255, "免费赛·", "", 28))
    -- end

    -- if info.m_freePlayNum == 0 then
    --     freenumber:setString("");
    -- else
    --     local playNum = 0;
    --     if info.m_extra then
    --         if string.find( info.m_extra, "playNum" ) then
    --             local pJson = json.decode(info.m_extra);
    --             playNum = pJson["playNum"];
    --         end
    --     end
    --     -- if info.m_signCost > 0 then
    --     --     freenumber:setString(string.format( "每日次数(%d/%d)", playNum, info.m_freePlayNum ));
    --     -- else
    --         freenumber:setString(string.format( "每日免费次数(%d/%d)", playNum, info.m_freePlayNum ));
    --     -- end
    --     if info.m_signCost > 0 then
            
    --         if playNum >= info.m_freePlayNum then
    --             baoming_button:setVisible(true)
    --             free_button:setVisible(false)
    --         else
    --             baoming_button:setVisible(false)
    --             free_button:setVisible(true)
    --         end
    --     end
        
    -- end

    -- local pRoomData = RoomData:getRoomDataById(info.m_gameAtomTypeId)

    -- item.m_pTextTile:pushBackElement(ccui.RichElementText:create(2, cc.c3b(165,42,42), 255, pRoomData.gameKindName, "", 22))
    -- item.m_pTextTile:setPositionX(text_title:getPositionX() + 80)
    -- --{
    -- --    { 1		, 1		, 'm_rankBegin'	, 'UBYTE'		, 1		, '名次范围起始'},
    -- --    { 2		, 1		, 'm_rankEnd'	, 'UBYTE'		, 1		, '名次范围结束'},
    -- --    { 3		, 1		, 'm_award'		, 'UBYTE'		, 1		, '奖励'},
    -- --}   
    -- --item.m_pAwards = info.m_awards
    -- local rank_text_number = rank_num_back:getChildByName("text_number")
    -- local rank_text_title = rank_num_back:getChildByName("text_title")
    -- rank_text_title:setString("第1名")
    -- if 0 < #info.m_awards then 
    --     rank_num_back:setVisible(true)
    --     rank_text_number:setString(string.format( "%.02f",info.m_awards[1].m_award *0.01 ))
    -- else
    --     rank_num_back:setVisible(false)
    -- end
end

function MatchGameListView:onMatchItemClick(pSender, eventType)
    if eventType == ccui.TouchEventType.began then
        pSender:setScale(1.1);
    elseif eventType == ccui.TouchEventType.canceled then
        pSender:setScale(1.0);
    elseif eventType == ccui.TouchEventType.ended then
        g_AudioPlayer:playEffect(ToolKit:getCurrentScene():getSoundPath())
        pSender:setScale(1.0);
        local pItem = pSender:getParent();
        local btnName = pSender:getName();
        local pInfo = pItem.m_pInfo
        if "button_detail" == btnName then
            -- self:onClear();
            -- self:close()
            MatchController.getInstance():openView("MatchDetailsView", pInfo)
        elseif "button_baoming" == btnName or "button_free" == btnName then
            if self.m_bIsBMButton ~= true then
                return;
            end
            self.m_bIsBMButton = false;
            self:doSomethingLater(function()
                self.m_bIsBMButton = true;
            end, 0.5);
            --报名
            if 2 ~= MatchController.getInstance():enterGame(pInfo.m_gameAtomTypeId) then
                local pItem = pSender:getParent();
                local free_button = pItem:getChildByName("button_free")
                local _state = UIAdapter:setResDownloadProgress(pItem, 0, "hall/image/match/freebm.png", cc.p(free_button:getPosition()))
            end
        end
    end
end

function MatchGameListView:onCloseClick()
    -- if MatchController.getInstance():getViewClassify() then
    --     MatchController.getInstance():getViewClassify():backClassify();
    -- end
    --sendMsg(MSG_BACK_HALL_VIEW_CLASSIFY);
    self:close();
    -- local pCloseButton = self.m_pPanle:getChildByName("close_button");
    -- pCloseButton:setEnabled(false);
    -- self.m_pPanle:runAction(
    --     cc.Sequence:create(
    --         cc.Spawn:create(
    --             cc.ScaleTo:create(0.1, 0),
    --             cc.FadeOut:create(0.1)
    --         ),
    --         cc.CallFunc:create(function()
                
    --         end),
    --     nil)
    -- );

end

function MatchGameListView:createRichText(parent, label)
    local pRichText = ccui.RichText:create()
    parent:addChild(pRichText)
    local ancher = label:getAnchorPoint()
    local pos = label:getPosition()
    pRichText:setPosition(cc.p(label:getPosition()))
    --pRichText:setAnchorPoint(cc.p(0, 0.5))
    return pRichText
end

function MatchGameListView:findTable(gameId)
    if self.m_pItemTables[gameId] ~= nil then
        return self.m_pItemTables[gameId]
    end
    return nil
end

function MatchGameListView:getTablesByKindId(kindId)
    local tables = {}
    for k, v in pairs(self.m_pItemTables) do
        if v.m_nKindId == kindId then
            table.insert(tables, v);
        end
    end
    return tables;
end


--msg 消息
function MatchGameListView:on_MSG_SUBSCRIBE(__idStr, __cmd)
    if __cmd.m_vecMatch ~= nil and 0 < #__cmd.m_vecMatch then
        local pItemTables = {}
        for key, item in pairs(self.m_pItemTables) do
            local isRemove = true
            for __, matchItem in pairs(__cmd.m_vecMatch) do
                if item.m_pInfo.m_gameAtomTypeId == matchItem.m_gameAtomTypeId then
                    isRemove = false
                    break
                end
            end    
            if isRemove == true then 
                item:removeFromParent()
            else
                pItemTables[key] = item
            end
        end
        
        self.m_pItemTables = pItemTables;
        for __, matchItem in pairs(__cmd.m_vecMatch) do
            local item = self:findTable(matchItem.m_gameAtomTypeId)
            if item ~= nil then
                self:setItemData(item, matchItem)
            else
                self:addItem(matchItem)
            end
            
        end
    end
end

function MatchGameListView:on_MSG_START_UPDATE(__msgId, __cmd)
    local kindId = RoomData:getBtnIdByGameId(__cmd.m_gameId);
    local items = self:getTablesByKindId(kindId);
    
    for k, v in pairs(items) do
        self:createProgress(v);
        v.__downloadStatus = 1;
    end

    -- local item = self:findTable(__cmd.m_gameId)
    -- if nil == item then
    --     return
    -- end
    -- if nil == item.progress then
    --     self:createProgress(item)
    --     item.__downloadStatus = 1
    -- end
end


function MatchGameListView:on_MSG_GAMERES_UPDATE(__msgId, __cmd)
    
    if __cmd == nil then
        return;
    end

    local kindId = RoomData:getBtnIdByGameId(__cmd.m_gameId);
    
    local items = self:getTablesByKindId(kindId);

    local setItemPro = function(item, cmd)
        local free_button = item:getChildByName("button_free")
        local _state = UIAdapter:setResDownloadProgress(item, cmd.m_progress, "hall/image/match/freebm.png", cc.p(free_button:getPosition()))
        if (-1 == _state) then
            item.m_pFreeBtn:setEnabled(true)
            item.m_pBaoMingBtn:setEnabled(true)
        elseif (1 == _state) then

        elseif (0 == _state) then
            item.m_pFreeBtn:setEnabled(true)
            item.m_pBaoMingBtn:setEnabled(true)
            self:showDianJi(item)
        end
    end

    for k, item in pairs(items) do
        setItemPro(item, __cmd);
    end
end

function MatchGameListView:doSomethingLater(call, delay)
    local node = cc.Node:create();
    self:addChild(node);
    node:runAction(cc.Sequence:create(cc.DelayTime:create(delay), cc.CallFunc:create(call), nil));
end

function MatchGameListView:showDianJi(item)
    local pDianJi = item.m_pDianJi;
    if nil == pDianJi and self:isVisible() then
        local free_button = item:getChildByName("button_free")
        local btnSize = item:getContentSize()
        local button_point = cc.p(free_button:getPosition())
        pDianJi = CacheManager:addSpineTo(item, "res/hall/effect/dianyidian/dianyidian",1,".json", 0.7)
        item.m_pDianJi = pDianJi
        pDianJi:setTimeScale(1.5)
        pDianJi:setAnimation(0, "animation", true);
        pDianJi:setPosition(cc.p(button_point.x + 70, button_point.y - 5))
        pDianJi:runAction(cca.seq({cca.delay(5), cca.fadeOut(0.1), cca.callFunc(function()
            item:removeChild(item.m_pDianJi)
            item.m_pDianJi = nil
        end)}))

        local pLabel = display.newTTFLabel({text = "游戏下载完成咯!\n再次点击即可报名!",size = 18, font = UIAdapter.TTF_FZCYJ})
        pLabel:setSkewX(8)
        pDianJi:addChild(pLabel)
        pLabel:setPosition(cc.p(-70, -50))
        pLabel:enableOutline(cc.c4b(0x2f, 0x76, 0xa0, 255), 2)
    end
end


return MatchGameListView

