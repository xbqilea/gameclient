local MatchEvent = require("src.app.hall.MatchGameList.control.MatchEvent")
local MatchController = require("src.app.hall.MatchGameList.control.MatchController")


local HNLayer = import("src.app.newHall.HNLayer")
local MatchSignView = class("MatchSignView", HNLayer)

function MatchSignView:ctor(data)
    self:initData(data.m_gameId, data.m_info)
    self:initCSB()
    self:initMsgEvent()
    MatchController.getInstance():sendSubscribe();
    ToolKit:registDistructor(self, handler(self, self.onDestory))
end

function MatchSignView:onDestory()
    self:unMsgEvent();
    -- if MatchController.getInstance():getView("MatchGameListView") == nil then
    --     MatchController.getInstance():sendCancelSubscribe();
    -- end
end


function MatchSignView:setSignNumber(signNumber)
    for __, element in pairs(self.m_pRichElements) do
        self.m_pSignNumText:removeElement(element);
    end
    self.m_pRichElements = {}
    self.m_pRichElements[1] = ccui.RichElementText:create(1, cc.c3b(255,255,255), 255, "已报名", self.m_sFontName, self.m_nFontSize)
    self.m_pRichElements[2] = ccui.RichElementText:create(1, cc.c3b(245,244,128), 255, string.format( "%d", signNumber), self.m_sFontName, self.m_nFontSize)
    self.m_pRichElements[3] = ccui.RichElementText:create(1, cc.c3b(255,255,255), 255, "人，还需", self.m_sFontName, self.m_nFontSize)
    self.m_pRichElements[4] = ccui.RichElementText:create(1, cc.c3b(245,244,128), 255, string.format( "%d", math.max(self.m_pInfo.m_matchStartNum - signNumber, 0)), self.m_sFontName, self.m_nFontSize)
    self.m_pRichElements[5] = ccui.RichElementText:create(1, cc.c3b(255,255,255), 255, "人即可开始,请耐心等待", self.m_sFontName, self.m_nFontSize)

    for __, element in pairs(self.m_pRichElements) do
        self.m_pSignNumText:pushBackElement(element);
    end

end

--初始化函数
function MatchSignView:initData(gameId, info)
    self.mRoomData  = RoomData:getRoomDataById( gameId )
    self.m_nGameId = gameId
    self.m_pInfo = info
    self.m_nFontSize = 32
    self.m_sFontName = 32
    self.m_pRichElements = {}
end

function MatchSignView:initCSB()
    local layer = UIAdapter:createNode("hall/MatchSignView.csb")
    self.m_pPanle = layer:getChildByName("panle")
    self:addChild(layer)
    -- UIAdapter:adapter(self.m_pPanle);
    self.m_pPanle:setPosition(cc.p(display.size.width / 2, display.size.height/2));
    self.m_pPanle:setAnchorPoint(cc.p(0.5, 0.5));
    
    local pCancelButton = self.m_pPanle:getChildByName("cancel_button")
    UIAdapter:registClickCallBack(pCancelButton, handler(self, self.onCancelClick), true);

    self.m_pTitleText = self.m_pPanle:getChildByName("title_text")
    -- self.m_pAwardText = self.m_pPanle:getChildByName("award_text")
    self.m_pListView = self.m_pPanle:getChildByName("ListView")
    self.m_pItemTmp = self.m_pPanle:getChildByName("ImageViewItemTmp")
    self.m_pItemTmp:setVisible(false)

    local pSignNumText = self.m_pPanle:getChildByName("sign_num_text")
    pSignNumText:setVisible(false)
    self.m_nFontSize = pSignNumText:getFontSize()
    self.m_sFontName = pSignNumText:getFontName()

    self.m_pSignNumText = ccui.RichText:create()
    self.m_pPanle:addChild(self.m_pSignNumText)
    self.m_pSignNumText:setPosition(cc.p(pSignNumText:getPosition()))

    self:refreshView();

    self.m_pPanle:setOpacity(0);
    self.m_pPanle:setScale(0.0);
    self.m_pPanle:runAction(
        cc.Sequence:create(
            cc.Spawn:create(
                cc.ScaleTo:create(0.15, 1.05),
                cc.FadeIn:create(0.15)
            ),
            cc.ScaleTo:create(0.05, 1),
        nil)
    );
    

end

function MatchSignView:refreshView()
    if self.m_pInfo == nil then
        return;
    end
    self:setSignNumber(self.m_pInfo.m_enrollNum, self.m_pInfo.m_matchStartNum);
    if table.nums(self.m_pListView:getItems()) == 0 then
        self.m_pListView:removeAllItems()
        for index = 1, #self.m_pInfo.m_awards do
            local item =  self.m_pItemTmp:clone()
            item:setVisible(true)
            local imageIcon = item:getChildByName("rank_icon")
            local textRank = item:getChildByName("TextBMFontRank")
            local textCoin = item:getChildByName("award_text")
            if 1 == index then
                textRank:setVisible(false)
                imageIcon:loadTexture("hall/image/rank/tjylc_phb_hg1.png")
            elseif 2 == index then
                textRank:setVisible(false)
                imageIcon:loadTexture("hall/image/rank/tjylc_phb_hg2.png")
            elseif 3 == index then
                textRank:setVisible(false)
                imageIcon:loadTexture("hall/image/rank/tjylc_phb_hg3.png")
            else
                imageIcon:setVisible(false)
                textRank:setString(index)
            end
            textCoin:setString("金币    "..(self.m_pInfo.m_awards[index].m_award * 0.01).."元")
            self.m_pListView:pushBackCustomItem(item)
            -- self.m_pListView:jumpToTop()
        end
    end
    
    -- if 0 < #self.m_pInfo.m_awards then 
    --     self.m_pAwardText:setString(string.format( "金币    %.02f元", self.m_pInfo.m_awards[1].m_award * 0.01));
    -- else
    --     self.m_pAwardText:setString("");
    -- end
    local names = {[101201]="初级场", [101202]="中级场", [101203]="高级场"}
    self.m_pTitleText:setString( string.format( "%s（%s）", names[self.m_nGameId], self.mRoomData.gameKindName ));
    -- if self.m_pInfo.m_freePlayNum == 0 then
    --     self.m_pTitleText:setString( string.format( "金币场（%s）", self.mRoomData.gameKindName ));
    -- else
    --     self.m_pTitleText:setString( string.format( "免费场（%s）", self.mRoomData.gameKindName ));
    -- end
end

function MatchSignView:initMsgEvent()
    --addMsgCallBack(self, MatchEvent.MSG_SIGN_INFO, handler(self, self.onSignInfos));
    addMsgCallBack(self, MatchEvent.MSG_SUBSCRIBE, handler(self, self.on_MSG_SUBSCRIBE));
end 
     
function MatchSignView:unMsgEvent()
    --removeMsgCallBack(self, MatchEvent.MSG_SIGN_INFO);
    removeMsgCallBack(self, MatchEvent.MSG_SUBSCRIBE);
end

function MatchSignView:onClear()
    
end



function MatchSignView:onCancelClick()
    --取消报名
    if g_GameController.reqSignCancel ~= nil and g_GameController:getGameAtomTypeId() == self.m_nGameId then
        g_GameController:reqSignCancel();
    end

end

-- function MatchSignView:onSignInfos(__cmd)
--     self:setSignNumber(#__cmd.m_signers)
-- end

function MatchSignView:on_MSG_SUBSCRIBE(__msgId, __cmd)
    for __, info in pairs(__cmd.m_vecMatch) do
        if info.m_gameAtomTypeId == self.m_nGameId then
            self.m_pInfo = info;
            self:setSignNumber(info.m_enrollNum)
            self:refreshView();
        end
    end
end



return MatchSignView