local MatchController = require("src.app.hall.MatchGameList.control.MatchController")
local MatchEvent = require("src.app.hall.MatchGameList.control.MatchEvent")
local MatchSignView = require("src.app.hall.MatchGameList.view.MatchSignView")

local HNlayer = import("src.app.newHall.HNLayer")
local MatchDetailsView = class("MatchDetailsView", HNlayer)

function MatchDetailsView:ctor(info)
    self:initData(info)
    self:initCSB()
    self:initMsgEvent()
    ToolKit:registDistructor(self, handler(self, self.onDestory))
end

function MatchDetailsView:onDestory()
    self:unMsgEvent();
end

function MatchDetailsView:initData(info)
    self.mRoomData  = RoomData:getRoomDataById( info.m_gameAtomTypeId )
    self.m_pInfo = info
    self.m_bIsBMButton = true;
    local data = getGameSwitchData(info.m_gameAtomTypeId);
    if data == nil then
        self.m_nKindId = -1;
    else
        local nKindId = data.kindId;
        if nKindId == 25 then
            nKindId = 15;
        end
        self.m_nKindId = nKindId;
    end
end

function MatchDetailsView:initCSB()
    local layer = UIAdapter:createNode("hall/MatchDetailsView.csb")
    
    self.m_pPanle = layer:getChildByName("panle")
    self:addChild(layer)
    -- UIAdapter:adapter(self.m_pPanle);

    self.m_pPanle:setPosition(cc.p(display.size.width / 2, display.size.height/2));
    self.m_pPanle:setAnchorPoint(cc.p(0.5, 0.5));
    self.m_ImageModel = self.m_pPanle:getChildByName("image_model");

    -- self.m_pCheck1 =  self.m_pPanle:getChildByName("Button_5")
    -- self.m_pCheck2 =  self.m_pPanle:getChildByName("Button_6")
    -- UIAdapter:registClickCallBack(self.m_pCheck1, handler(self, self.onCheckClick), true);
    -- UIAdapter:registClickCallBack(self.m_pCheck2, handler(self, self.onCheckClick), true);

    -- self.m_pCheckText1 = self.m_pCheck1:getChildByName("Text_2")
    -- self.m_pCheckText2 = self.m_pCheck2:getChildByName("Text_3")

    -- self.m_pJianTou = self.m_pPanle:getChildByName("ImageView_10")


    local pCloseButton = self.m_pPanle:getChildByName("close_button")
    local button_free = self.m_pPanle:getChildByName("button_free");
    local button_baoming = self.m_pPanle:getChildByName("button_baoming");

    UIAdapter:registClickCallBack(pCloseButton, handler(self, self.onCloseClick), true);
    UIAdapter:registClickCallBack(button_free, handler(self, self.onBaoMingClick), true);
    UIAdapter:registClickCallBack(button_baoming, handler(self, self.onBaoMingClick), true);
    
    -- self.m_pAwardView = self.m_pPanle:getChildByName("ListView_1")
    self.m_pHelpView = self.m_pPanle:getChildByName("ListView_2")
    
    -- self.m_pCheck1:setBright(false)
    -- self.m_pHelpView:setVisible(false)

    -- self.m_pTmpAwardItem = self.m_pPanle:getChildByName("award_item")
    -- self.m_pTmpAwardItem:setVisible(false)

    -- for k,award in pairs(self.m_pInfo.m_awards) do
    --     self:addItem(award, k)
    -- end

    local pControl = self.m_pHelpView:getChildByName("control");
    -- local tipText = ccui.RichText:create();
    -- tipText:pushBackElement(ccui.RichElementText:create(1, cc.c3b(0,0,0), 255, self.mRoomData.matchReward, "hall/font/fzft.ttf", 26));
    -- --tipText:setAnchorPoint(cc.p(0, 0));
    -- pControl:addChild(tipText)
    -- pControl:setContentSize(cc.size(self.m_pHelpView:getContentSize().width, tipText:getContentSize().height))
    local text = pControl:getChildByName("text");
    text:setString(self.mRoomData.matchReward);
    -- if self.m_pInfo.m_openBeginTime == -1 or self.m_pInfo.m_openEndTime == -1 then
    --     text:setString(string.format( self.mRoomData.matchReward, "全天开放" ) );
    -- else
    --     local getOpenTime = function (_str)
    --         local resultStrList = {}
    --         string.gsub(_str,'[^.]+',function ( w )
    --             table.insert(resultStrList,w)
    --         end)
    --         local _number = tonumber(resultStrList[2])
    --         if _number == 0 then
    --             return resultStrList[1].."点"
    --         else
    --             return resultStrList[1].."点"..tonumber(resultStrList[2]).."分"
    --         end
            
    --     end
    --     -- local beginTime = insertChar(self.m_pInfo.m_openBeginTime.."", "点", 3).."分"
    --     -- local endTime = insertChar(self.m_pInfo.m_openEndTime.."", "点", 3).."分"
    --     local beginTime = getOpenTime(string.format( "%.02f",self.m_pInfo.m_openBeginTime/100 ))
    --     local endTime = getOpenTime(string.format( "%.02f",self.m_pInfo.m_openEndTime/100 ))
    --     text:setString(string.format( self.mRoomData.matchReward, beginTime.."—"..endTime ) );
    -- end
    
    local text_size = text:getVirtualRendererSize();
    text:setContentSize(text_size);
    pControl:setContentSize(cc.size(pControl:getContentSize().width, text_size.height));
    text:setPosition(0, 0);
   
    if self.m_pInfo.m_signCost > 0 then
        button_free:setVisible(false);
        button_baoming:setVisible(true);
    else
        button_free:setVisible(true);
        button_baoming:setVisible(false);
    end

    if self.m_pInfo.m_freePlayNum > 0 then
        local playNum = 0;
        if self.m_pInfo.m_extra then
            if string.find( self.m_pInfo.m_extra, "playNum" ) then
                local pJson = json.decode(self.m_pInfo.m_extra);
                playNum = pJson["playNum"];
            end
        end
        if playNum >= self.m_pInfo.m_freePlayNum then
            button_free:setVisible(false);
            button_baoming:setVisible(true);
        else
            button_free:setVisible(true);
            button_baoming:setVisible(false);
        end
    end

end

function MatchDetailsView:initMsgEvent()
    addMsgCallBack(self, MatchEvent.MSG_START_UPDATE, handler(self, self.on_MSG_START_UPDATE))
    addMsgCallBack(self, MatchEvent.MSG_GAMERES_UPDATE, handler(self, self.on_GAMERES_UPDATE))
end

function MatchDetailsView:unMsgEvent()
    removeMsgCallBack(self, MatchEvent.MSG_START_UPDATE)
    removeMsgCallBack(self, MatchEvent.MSG_GAMERES_UPDATE)
end

function MatchDetailsView:onClear()
end

-- function MatchDetailsView:onCheckClick (sender,event)
--     local btnName = sender:getName()
--     if btnName == "Button_5" then
--         self.m_pCheck1:setBright(false)
--         self.m_pCheck2:setBright(true)
--         self.m_pAwardView:setVisible(true)
--         self.m_pHelpView:setVisible(false)
--         self.m_pCheckText1:setColor(cc.c3b(255, 255, 255))
--         self.m_pCheckText2:setColor(cc.c3b(0, 0, 0))
--         self.m_pJianTou:setPositionX(494)
--     elseif btnName == "Button_6" then
--         self.m_pCheck1:setBright(true)
--         self.m_pCheck2:setBright(false)
--         self.m_pAwardView:setVisible(false)
--         self.m_pHelpView:setVisible(true)
--         self.m_pCheckText2:setColor(cc.c3b(255, 255, 255))
--         self.m_pCheckText1:setColor(cc.c3b(0, 0, 0))
--         self.m_pJianTou:setPositionX(self.m_pPanle:getContentSize().width - 494)
--     end
-- end

function MatchDetailsView:onCloseClick(sender, event)
    self:onClear();
    self:close();
    --MatchController.getInstance():openView("MatchGameListView");
end

function MatchDetailsView:onBaoMingClick(sender, event)
    if self.m_bIsBMButton ~= true then
        return;
    end
    self.m_bIsBMButton = false;
    self:doSomethingLater(function () 
        self.m_bIsBMButton = true;
    end, 0.5);
    MatchController.getInstance():enterGame(self.m_pInfo.m_gameAtomTypeId)
end

-- function MatchDetailsView:addItem(award, index)
--     local item = self.m_pTmpAwardItem:clone()
--     self.m_pAwardView:pushBackCustomItem(item)
--     item:setVisible(true)
--     local pBg = item:getChildByName("bg")
--     local pRankIcon = item:getChildByName("rank_icon")
--     local pRankNumber = item:getChildByName("rank_number")
--     local pAwardIcon = item:getChildByName("award_icon")
--     local pRankText = item:getChildByName("rank_text")
--     local pCoinText = item:getChildByName("coin_text")

--     --    { 1		, 1		, 'm_rankBegin'	, 'UBYTE'		, 1		, '名次范围起始'},
--     --    { 2		, 1		, 'm_rankEnd'	, 'UBYTE'		, 1		, '名次范围结束'},
--     --    { 3		, 1		, 'm_award'		, 'UBYTE'		, 1		, '奖励'},

--     if index % 2 == 1 then
--         pBg:setVisible(false)
--     else
--         pBg:setVisible(true)
--     end

--     if award.m_rankBegin > 3 then
--         pRankIcon:setVisible(false)
--         pRankNumber:setVisible(true)
--         pRankNumber:setString(""..award.m_rankBegin)
--     else
--         pRankIcon:setVisible(true)
--         pRankNumber:setVisible(false)
--         pRankIcon:loadTexture(string.format("hall/image/rank/tjylc_phb_hg%d.png",award.m_rankBegin))
--     end
--     local rankTexts = {"第一名：","第二名：","第三名：","第四名：","第五名：","第六名：","第七名：","第八名：","第九名：","第十名：",}
--     pRankText:setString(rankTexts[award.m_rankBegin])
--     pCoinText:setString(string.format( "%.02f 金币", award.m_award *0.01))
-- end

function MatchDetailsView:doSomethingLater(call, delay)
    local node = cc.Node:create();
    self:addChild(node);
    node:runAction(cc.Sequence:create(cc.DelayTime:create(delay), cc.CallFunc:create(call), nil));
end

function MatchDetailsView:on_MSG_START_UPDATE(__msgId, __cmd)
    if __cmd.m_gameId == RoomData:getGameByBtnId(self.m_nKindId) then
        local button_free = self.m_pPanle:getChildByName("button_free");
        local button_baoming = self.m_pPanle:getChildByName("button_baoming");
        button_free:setEnabled(false);
        button_baoming:setEnabled(false);
    end
end

function MatchDetailsView:on_GAMERES_UPDATE(__msgId, __cmd)
    if __cmd.m_gameId == RoomData:getGameByBtnId(self.m_nKindId) then
        if -1 == __cmd.m_progress or 1000 == __cmd.m_progress then 
            local button_free = self.m_pPanle:getChildByName("button_free");
            local button_baoming = self.m_pPanle:getChildByName("button_baoming");
            button_free:setEnabled(false);
            button_baoming:setEnabled(false);
        end
    end
end




return MatchDetailsView