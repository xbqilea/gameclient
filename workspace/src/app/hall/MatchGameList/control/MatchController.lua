local MatchEvent = require("src.app.hall.MatchGameList.control.MatchEvent")
-- local MatchSignView = require("src.app.hall.MatchGameList.view.MatchSignView")
-- local MatchGameListView = require("src.app.hall.MatchGameList.view.MatchGameListView")
-- local MatchDetailsView = require("src.app.hall.MatchGameList.view.MatchDetailsView")
local DlgAlert = require("app.hall.base.ui.MessageBox")
local scheduler  =  require("framework.scheduler")

local MatchController = class("MatchController")


GlobalMatchController = GlobalMatchController or {}

GlobalMatchController._instance = nil

function MatchController.getInstance()
    if GlobalMatchController._instance == nil then
        GlobalMatchController._instance = MatchController.new()
    end
    return GlobalMatchController._instance
end

function MatchController.releseInstance()
    if GlobalMatchController._instance ~= nil then
        GlobalMatchController._instance:clear()
    --else
        --ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_CancelMatchSubscribe_Req", {});
    end
    GlobalMatchController._instance = nil
end

-- function MatchController:setViewClassify(viewClassify)
--     self.m_pViewClassify = viewClassify;
-- end

-- function MatchController:getViewClassify()
--     return self.m_pViewClassify;
-- end


function MatchController:openView(viewName, data, delayTime)
    local funcOpenView = function()
        local pScene = display.getRunningScene()
        local pUI = pScene:getChildByName(viewName);
        if pUI == nil then
            local resPath = string.format( "src.app.hall.MatchGameList.view.%s", viewName );
            local uiViewReq = require(resPath);
            pUI = uiViewReq.new(data);
            pUI:setName(viewName);
            pScene:addChild(pUI);
        end
        return pUI
    end

    if delayTime ~= nil then
        scheduler.performWithDelayGlobal(funcOpenView, delayTime);
        return nil;
    else
        return funcOpenView();
    end
end

function MatchController:getView(viewName)
    local pScene = display.getRunningScene();
    local pUI = pScene:getChildByName(viewName);
    return pUI;
end


function MatchController:closeView(viewName)
    local pScene = display.getRunningScene();
    local pUI = pScene:getChildByName(viewName);
    if pUI ~= nil then
        pUI:onClear();
        pUI:close();
    end
end

function MatchController:getSubscribeInfo(gameId)
    local getInfo = function()
        for __, info in pairs(self.m_pMatchInfo) do
            if info.m_gameAtomTypeId == gameId then
                return info;
            end
        end
    end

    local getConfig = function()
        for __, cfg in pairs(self.m_pMatchConfig) do
            if cfg.m_gameAtomTypeId == gameId then
                return cfg;
            end
        end
    end

    local info = getInfo();
    local cfg = getConfig();

    if cfg then
        local ret = clone(cfg);
        if info then
            ret.m_enrollNum = info.m_enrollNum;
            ret.m_extra = info.m_extra;
        else
            ret.m_enrollNum = 0;
            ret.m_extra = nil;
        end
        return ret;
    end
    return nil;
end

function MatchController:getSubscribeInfos()

    local getInfo = function(gameId)
        for __, info in pairs(self.m_pMatchInfo) do
            if info.m_gameAtomTypeId == gameId then
                return info;
            end
        end
    end

    local ret = {};
    for __, cfg in pairs(self.m_pMatchConfig) do
        local info = getInfo(cfg.m_gameAtomTypeId);
        local info_clone = clone(cfg);
        if info then
            info_clone.m_enrollNum = info.m_enrollNum;
            info_clone.m_extra = info.m_extra;
        else
            info_clone.m_enrollNum = 0;
            info_clone.m_extra = nil;
        end
        if isGameAtomOpen(info_clone.m_gameAtomTypeId) then
            table.insert(ret, info_clone);
        end
    end
    return {m_vecMatch = ret};
end


function MatchController:sendSubscribe()
    if self.m_bIsSubscribe ~= true then
        ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_MatchSubscribe_Req", {});
    end
end

function MatchController:sendCancelSubscribe()
    self.m_bIsSubscribe = false;
    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_CancelMatchSubscribe_Req", {});
end

function MatchController:enterGame(gameAtomTypeId)
    local data = getGameSwitchData(gameAtomTypeId);
    if data == nil then
        TOAST("游戏不存在");
        return;
    end
    local nGameId = RoomData:getGameByBtnId(data.kindId);

    if RoomData:isGameExist(nGameId) then
        if RoomData:isGameUp2Date(nGameId) then -- 游戏已经是最新
            self.__downloadStatus = 2
        else
            self.__downloadStatus = 0
        end
    else
        self.__downloadStatus = 0
    end

    if self.__downloadStatus == 0 then
        self:doDownLoad(nGameId, gameAtomTypeId)
    elseif self.__downloadStatus == 1 then
        print("下载中")
    elseif self.__downloadStatus == 2 then
        print("已下载")
        self:startGame(gameAtomTypeId);
    end

    return self.__downloadStatus;
end

function MatchController:doDownLoad(nGameId, gameAtomTypeId)
    -- 每次只读取UpdateInfo即可判断是否需要下载
    self.updateControlllers = self.updateControlllers or {}
    -- local data = getGameSwitchData(gameAtomTypeId);
    -- if data == nil then
    --     TOAST("游戏不存在");
    --     return;
    -- end
    -- local nGameId = RoomData:getGameByBtnId(data.kindId);
    local packageId = RoomData:getPackageIdByPortalId(nGameId);

    if self.updateControlllers[nGameId] == nil then
        local UpdateController = require("src.app.assetmgr.main.control.RenewController");
        local pUpdateController = UpdateController.new();
        self.updateControlllers[nGameId] = pUpdateController;
        self.m_selectUpdateId = nGameId;
        if not pUpdateController:updateGame(packageId, true) then
            self:startGame(gameAtomTypeId)
        else
            sendMsg(MatchEvent.MSG_START_UPDATE, {m_gameId = nGameId});
        end
    end
end

function MatchController:startGame(gameAtomTypeId, delayTime)
    -- if g_GameController then
    --     if g_GameController:getGameAtomTypeId() == gameId then
    --         if g_GameController:isMatchEnter() then 
    --             g_GameController:reqJoinMatch()
    --         else
    --             ConnectManager:send2SceneServer(gameId, "CS_C2M_Enter_Req", {gameId, "" })
    --         end
    --     else
    --         local controllerPath = getControllerPath(gameId) --RoomData:getRoomDataById( gameId )
    --         g_GameController:releaseInstance()
    --         g_GameController = require(controllerPath):getInstance() 
    --         g_GameController:bindGameTypeId(gameId)
    --         ConnectManager:send2SceneServer(gameId, "CS_C2M_Enter_Req", {gameId, "" })
    --     end
    -- else
    --     local controllerPath = getControllerPath(gameId)
    --     local req = require(controllerPath)
    --     g_GameController = require(controllerPath):getInstance() 
    --     g_GameController:bindGameTypeId(gameId)
    --     ConnectManager:send2SceneServer(gameId, "CS_C2M_Enter_Req", {gameId, "" })
    -- end
    local funcStart = function()
        if isMatchGameByAtomTypeId(gameAtomTypeId) then
            ToolKit:addLoadingDialog(5, "正在报名中,请稍等...");
        else
            ToolKit:addLoadingDialog(5, "正在进入游戏，请稍等...");
        end
        
        if g_GameController then
            g_GameController:releaseInstance();
        end
    
        local controllerPath = getControllerPath(gameAtomTypeId);
        LuaUtils.removeRequiredByName(controllerPath);
        LuaUtils.removeRequiredByFind("app.game.landlords");
        --LuaUtils.removeAllRequired();
        local req = require(controllerPath);
        g_GameController = require(controllerPath):getInstance();
        g_GameController:bindGameTypeId(gameAtomTypeId);
        ConnectManager:send2SceneServer(gameAtomTypeId, "CS_C2M_Enter_Req", {gameAtomTypeId, "" });
    end

    if nil == delayTime or 0 == delayTime then
        funcStart();
    else
        if self.m_pFuncStart then
            scheduler.unscheduleGlobal(self.m_pFuncStart);
        end
        self.m_pFuncStart = scheduler.performWithDelayGlobal(funcStart, delayTime);

    end
end



function MatchController:ctor()
    self:initData();
    self:initEvent();
    --self:sendSubscribe();
end

function MatchController:clear()
    if self.m_pFuncStart then
        scheduler.unscheduleGlobal(self.m_pFuncStart);
    end
    self.m_pFuncStart = nil;
    self:unEvent();
    self:sendCancelSubscribe();
end

function MatchController:initData()
    self.m_bIsSubscribe = false;
    self.m_pMatchInfo = {};
    self.m_pMatchConfig = {};
    self.m_pViewClassify = nil;
    self.updateControlllers = {};
    --self.m_pScene = display:getRunningScene();
end

function MatchController:initEvent()
    addMsgCallBack(self, MSG_GAME_UPDATE_PROGRESS, handler(self, self.on_MSG_GAME_UPDATE_PROGRESS))
    addMsgCallBack(self, MatchEvent.MSG_SIGNUP, handler(self, self.on_MSG_SIGNUP));
    addMsgCallBack(self, MatchEvent.MSG_SIGNUP_TIME_OUT, handler(self, self.on_MSG_SIGNUP_TIME_OUT));
    addMsgCallBack(self, MatchEvent.MSG_START_UPDATE, handler(self, self.on_MSG_START_UPDATE));
    addMsgCallBack(self, MatchEvent.MSG_SIGN_STATE, handler(self, self.on_MSG_SIGN_STATE));
    addMsgCallBack(self, MatchEvent.MSG_CANCEL_SIGNUP, handler(self, self.on_MSG_CANCEL_SIGNUP));

    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_MatchInfo_Nty", handler(self, self.on_NET_MATCHINFO_CONFIG));
    TotalController:registerNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_MatchSubscribe_Nty", handler(self, self.on_NET_SUBSCRIBE));
    
end

function MatchController:unEvent()
    TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_MatchSubscribe_Nty");
    TotalController:removeNetMsgCallback(self, Protocol.LobbyServer, "CS_H2C_MatchInfo_Nty");
    removeMsgCallBack(self, MSG_GAME_UPDATE_PROGRESS);
    removeMsgCallBack(self, MatchEvent.MSG_SIGNUP);
    removeMsgCallBack(self, MatchEvent.MSG_SIGNUP_TIME_OUT);
    removeMsgCallBack(self, MatchEvent.MSG_START_UPDATE);
    removeMsgCallBack(self, MatchEvent.MSG_SIGN_STATE);
    removeMsgCallBack(self, MatchEvent.MSG_CANCEL_SIGNUP);
end

function MatchController:_getScene()
    if self.m_pScene == nil then
        self.m_pScene = display:getRunningScene();
    end
    return self.m_pScene;
end


--region msg 消息

--游戏资源更新
function MatchController:on_MSG_GAME_UPDATE_PROGRESS(msgName, gameKindType, progress)
    sendMsg(MatchEvent.MSG_GAMERES_UPDATE, {m_gameId = gameKindType, m_progress = progress});
    if self.updateControlllers == nil then
        return;
    end
    self.updateControlllers = self.updateControlllers or {};
    if progress == -1 then -- 游戏更新失败
        self.updateControlllers[gameKindType] = nil;
    elseif progress == 1000 then -- 下载完成
        self.updateControlllers[gameKindType] = nil;
        -- if gameKindType == self.m_selectUpdateId then
        --     self:startGame(self.m_selectUpdateId);
        --     self.m_selectUpdateId = nil;
        -- end
    end
end

--报名
function MatchController:on_MSG_SIGNUP(__msgId, __cmd)
    -- if __info.m_ret == 0 then
    --     --self.gameScene.m_MatchJoinLayer:SignUp()
    -- elseif __info.m_ret == -10 then
    --     TOAST("玩家不存在")
    -- elseif __info.m_ret == -25005 then
    --     TOAST("玩家已报名")
    -- elseif __info.m_ret == -25006 then
    --     TOAST("报名中, 请稍后")
    -- elseif __info.m_ret == -25007 then
    --     TOAST("金币不足, 无法报名")
    -- elseif __info.m_ret == -25008 then
    --     TOAST("人数已满, 报名下一场比赛")
    -- end
    local pInfo = self:getSubscribeInfo(__cmd.m_gameId);
    --if pInfo ~= nil then
        if 0 == __cmd.m_code then
            local pUI = self:openView("MatchSignView", {m_gameId = __cmd.m_gameId, m_info = pInfo})
        elseif -25005 == __cmd.m_code then
            local pUI = self:openView("MatchSignView", {m_gameId = __cmd.m_gameId, m_info = pInfo})
        elseif -25013 == __cmd.m_code then
            if pInfo then
                local getOpenTime = function (_str)
                    local resultStrList = {}
                    string.gsub(_str,'[^.]+',function ( w )
                        table.insert(resultStrList,w)
                    end)
                    local _number = tonumber(resultStrList[2])
                    if _number == 0 then
                        return resultStrList[1].."点"
                    else
                        return resultStrList[1].."点"..tonumber(resultStrList[2]).."分"
                    end
                    
                end
                local beginTime = getOpenTime(string.format( "%.02f",pInfo.m_openBeginTime/100 ))
                local endTime = getOpenTime(string.format( "%.02f",pInfo.m_openEndTime/100 ))
                local dlg = DlgAlert.showTipsAlert({title = "提示", tip = string.format("比赛场开赛时间%s", beginTime.."—"..endTime )})
                dlg:setBackBtnEnable(false)
                dlg:enableTouch(false)
                dlg:setSingleBtn("确定", function ()
                    dlg:closeDialog()
                end)
            else
                TOAST("比赛场未到开放时间")
            end
        else
        end
    --end
end

--报名超时
function MatchController:on_MSG_SIGNUP_TIME_OUT(__msgId, __cmd)

end

--报名玩家数据
function MatchController:on_MSG_START_UPDATE(__msgId, __cmd)
end

--报名状态
function MatchController:on_MSG_SIGN_STATE(__msgId, __cmd)
    --{m_gameId = self.m_gameAtomTypeId, m_state = info.m_state, m_enrollNum = info.m_signCnt, m_beginNum = info.m_beginNum}
    --0：未报名(按钮状态：未报名)
    --1: 报名扣费中(显示为报名, 暂时不可点击),
    --2：已报名(报名成功，按钮显示为退赛) 
    --3: 报名人员已满, 分配房间中(不可退赛) 
    --4: 已开赛, 正在游戏中(不显示报名页面) 
    --5: 当前轮比赛结束, 显示等待中 
    --6: 晋级界面,显示名次'
    local pInfo = self:getSubscribeInfo(__cmd.m_gameId);
    if 2 == __cmd.m_state then 
        local pUI = self:openView("MatchSignView", {m_gameId = __cmd.m_gameId, m_info = pInfo});
        --pUI:setSignNumber(pInfo.m_enrollNum);
    elseif 1 == __cmd.m_state then
        
    else 
        -- self:closeView("MatchSignView");
        -- self:closeView("MatchGameListView");
        -- self:closeView("MatchDetailsView");
    end
    ToolKit:removeLoadingDialog();
end

--取消报名
function MatchController:on_MSG_CANCEL_SIGNUP(__msgId, __cmd)
    if __cmd.m_code == 0 then
        --self:closeView("MatchSignView");
    elseif __cmd.m_code == -10 then
        --self:closeView("MatchSignView");
    elseif __cmd.m_code == -25008  then
        --self:closeView("MatchSignView");
    end
    self:closeView("MatchSignView");
end

function MatchController:on_NET_SUBSCRIBE(__msgId, __cmd)
    if __msgId ~= "CS_H2C_MatchSubscribe_Nty" then
        return;
    end
    self.m_bIsSubscribe = true;

    -- {1		, 1		, "m_vecMatch"			, "PstMatchInfo"				, 128			, "比赛信息"},
    -- {2		, 1		, "m_type"				, "UBYTE"						, 1			, "发送类型 1-全发 2-部分下发，这里只发有变化的数据"},

    -- { 1		, 1		, 'm_gameAtomTypeId'			, 'UINT'				, 1	, '游戏最小类型ID'},
	-- { 2		, 1		, 'm_enrollNum'					, 'UINT'				, 1 , '当前报名人数'},
	-- { 3		, 1		, 'm_extra'						, 'STRING'				, 1	, '额外信息'},
    
    if __cmd.m_type == 1 then
        --self.m_pMatchInfo = __cmd.m_vecMatch;
        self.m_pMatchInfo = {}
        for __, item in pairs(__cmd.m_vecMatch) do
            if isGameAtomOpen(item.m_gameAtomTypeId) then
                table.insert(self.m_pMatchInfo, item)
            end
        end
    else
        local getInfo = function(gameId)
            for __, info in pairs(self.m_pMatchInfo) do
                if info.m_gameAtomTypeId == gameId then
                    return info;
                end
            end
        end

        for index = 1, #__cmd.m_vecMatch do
            local item = __cmd.m_vecMatch[index];
            local info = getInfo(item.m_gameAtomTypeId);
            if info == nil then
                if isGameAtomOpen(item.m_gameAtomTypeId) then
                    table.insert(self.m_pMatchInfo, item);
                end
            else
                info.m_enrollNum = item.m_enrollNum;
                info.m_extra = item.m_extra;
            end
        end
    end
    
    sendMsg(MatchEvent.MSG_SUBSCRIBE, self:getSubscribeInfos());
    print("收到别比赛订阅消息\n");
end


function MatchController:on_NET_MATCHINFO_CONFIG(__msgId, __cmd)
    if __msgId ~= "CS_H2C_MatchInfo_Nty" then
        return;
    end
    self.m_bIsSubscribe = true;
    -- {1		, 1		, "m_vecMatch"			, "PstMatchCfg"				, 128			, "比赛信息"},

    -- { 1		, 1		, 'm_gameAtomTypeId'			, 'UINT'				, 1	, '游戏最小类型ID'},
	-- { 2		, 1		, 'm_matchStartNum'				, 'UINT'				, 1	, '比赛开赛总人数'},
	-- { 3		, 1		, 'm_freePlayNum'				, 'UINT'				, 1 , '免费次数 0-不免费'},
	-- { 4		, 1		, 'm_signCost'					, 'UINT'				, 1 , '报名费, 不免费时所需报名费'},
    -- { 5		, 1		, 'm_awards'					, 'PstMatchAward'		, 10, '排名奖励'},
    -- { 6		, 1		, 'm_openBeginTime'				, 'INT'					, 1 , '开放开始时间 -1表示无限制 1600表示16点0分'},
	-- { 7		, 1		, 'm_openEndTime'				, 'INT'					, 1 , '开放结束时间 -1表示无限制 1600表示16点0分'},
    --self.m_pMatchConfig = __cmd.m_vecMatch;
    self.m_pMatchConfig = {}
    for __, item in pairs(__cmd.m_vecMatch) do
        if isGameAtomOpen(item.m_gameAtomTypeId) then
            table.insert(self.m_pMatchConfig, item)
        end
    end
end

--endregion msg 消息


return MatchController
