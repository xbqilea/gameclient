--
-- Author: lhj
-- Date: 2018-8-24 
-- 复制粘贴板管理

local ClipboardTextController = class("ClipboardTextController")

function ClipboardTextController:getInstance()
    if g_ClipboardTextController == nil then
        g_ClipboardTextController = ClipboardTextController.new()
    end
    return g_ClipboardTextController
end

function ClipboardTextController:ctor()
    self.m_sCopycontent = ""
end

-- 复制文本到粘贴板，只支持IOS，安卓
-- return bool 复制是否成功
function ClipboardTextController:setClipboardText(text)

    print("text:", text)

    local ret = self:callSetClipboardText(text)
    if ret then
        self.m_sCopycontent = text
    end
    return ret
end


-- 获取粘贴板文本，只支持IOS，安卓
-- return callback(str) 粘贴板文本,获取失败就是""空字符串，仅安卓有效
function ClipboardTextController:getClipboardText(callback)
    local ret = self:callGetClipboardText(callback)
    return ret
end

-- 清理粘贴板文本，只支持IOS，安卓. --设置为""
function ClipboardTextController:cleanupClipboardText()
    local ret = self:callSetClipboardText("")
    if ret then
        self.m_sCopycontent = ""
    end
end

-- 复制文本到粘贴板，只支持IOS，安卓
-- return bool 复制是否成功
function ClipboardTextController:callSetClipboardText(text)
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "setClipboardText"
        local javaParams = {text}
        local javaMethodSig = "(Ljava/lang/String;)Z"
        local ok,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok and ret  then
            return true
        else
            return false
        end
    elseif device.platform == "ios" then
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "PhoneInfo"
        local args = {text = text}
        local ok,ret  = luaoc.callStaticMethod(className, "setClipboardText", args)
        if ok then
            return true
        else
            return false
        end
    else
        return true --给windows调试用
    end
end

-- 获取粘贴板文本，只支持IOS，安卓
-- return callback(str) 粘贴板文本,获取失败就是""空字符串，仅安卓有效
function ClipboardTextController:callGetClipboardText(callback)
    if device.platform == "android" then
        local javaClassName = "org/cocos2dx/lua/MilaiPublicUtil"
        local javaMethodName = "getClipboardTextNew"
        local javaParams = {callback}
        local javaMethodSig = "(I)V"
        local ok,ret  = luaj.callStaticMethod(javaClassName, javaMethodName, javaParams, javaMethodSig)
        if ok and ret  then
            return true
        else
            return callback("")
        end
    elseif device.platform == "ios" then
        local luaoc = require "cocos.cocos2d.luaoc"
        local className = "PhoneInfo"
        local args = nil
        local ok,ret  = luaoc.callStaticMethod(className,"getClipboardText",args)
        if ok and ret then
            return callback(ret)
        else
            return callback("")
        end
    else
        return callback(self.m_sCopycontent) --给windows调试用
    end
end

return ClipboardTextController