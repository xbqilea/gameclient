--
-- User: lhj
-- Date: 2018/7/11 17:26
-- 推荐有礼

local XbDialog = require("app.hall.base.ui.CommonView")
local GetTeleFareLayer = class("GetTeleFareLayer", function ()
    return XbDialog.new()
end)

function GetTeleFareLayer:ctor()
    self:myInit()
    self:setupViews()
end

function GetTeleFareLayer:myInit()
    self._share_type = 2
    addMsgCallBack(self, MSG_ACT_SHARE_SUCCESS, handler(self, self.showSuccessTips))
    ToolKit:registDistructor(self, handler(self, self.onDestory) )
end

function GetTeleFareLayer:setupViews()
   	self.node = UIAdapter:createNode("csb/share/make_tele_fare_layer.csb"):addTo(self)
    UIAdapter:adapter(self.node, handler(self, self.onTouchCallback))

    local node = self.node:getChildByName("Node_3")
    if self.price_Txt == nil then
        self.price_Txt = ccui.TextAtlas:create("1", "ui/bankruptcy_shuzi_lse.png", 102, 102, "0")
        self.price_Txt:setPosition(cc.p(12, 5))
        self.price_Txt:addTo(node)
    else
        self.price_Txt:setString("1")
    end

    if self.price_Txt_1 == nil then
        self.price_Txt = ccui.TextAtlas:create("8", "ui/bankruptcy_shuzi_lse.png", 102, 102, "0")
        self.price_Txt:setPosition(cc.p(72, 10))
        self.price_Txt:addTo(node)
    else
        self.price_Txt_1:setString("8")
    end
end

function GetTeleFareLayer:showSuccessTips(msg, data)
    local name = data.name
    local type = XbShareUtil.SHARE_SUCCESS_NO_REWARD_DAILY
    if data.success == true then
        type = XbShareUtil.SHARE_SUCCESS_REWARD
        self:showDropGoldAminataion()
    end
    XbShareUtil:showResultMsg(type, name)
end

function GetTeleFareLayer:showDropGoldAminataion()
    XbShareUtil:showDropGoldAminataion(XbShareUtil.gameType.share_gift)
end

function GetTeleFareLayer:shareInfo()
    XbShareUtil:share({gameType = XbShareUtil.gameType.share_gift, tag = 1, callback = handler(self, self.onShareResultCallback)})
end

function GetTeleFareLayer:shareCircleInfo()
    QkaShareUtil:share({gameType = QkaShareUtil.gameType.share_gift, tag = 2, callback = handler(self, self.onShareResultCallback)})
end

function GetTeleFareLayer:onShareResultCallback(result)
    performWithDelay(self, function()
        if result == QkaShareUtil.WX_SHARE_OK then
            QkaShareUtil:requestShareReward(QkaShareUtil.gameType.share_gift)
        else
            QkaShareUtil:showResultMsg(result)
        end
    end, 0.5)
end

function GetTeleFareLayer:onTouchCallback(sender)
	local name = sender:getName()
	if name == "close_btn" then
		self:closeDialog()
	elseif name == "share_btn_f" then
        self:shareInfo()
    elseif name == "share_btn_c" then
        self:shareCircleInfo()
	end
end

function GetTeleFareLayer:onDestory()
    removeMsgCallBack(self, MSG_ACT_SHARE_SUCCESS)
end

return GetTeleFareLayer