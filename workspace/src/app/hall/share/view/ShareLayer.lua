--
-- User: lhj
-- Date: 2018/7/9 15:30
-- 分享

local XbDialog = require("app.hall.base.ui.CommonView")
local ShareLayer = class("ShareLayer", function ()
    return XbDialog.new()
end)

function ShareLayer:ctor()
    self:myInit()
    self:setupViews()
end

function ShareLayer:myInit()
    self._share_type = 1 
    self.leftTime = 0
--[[	addMsgCallBack(self, MSG_SET_SHARE_CD, handler(self, self.setShareTime))--]]
	addMsgCallBack(self, MSG_ACT_SHARE_SUCCESS, handler(self, self.showSuccessTips))
	ToolKit:registDistructor(self, handler(self, self.onDestory) )
end

--[[function ShareLayer:setShareTime()
    if XbShareUtil.shareInfo[XbShareUtil.gameType.daily_share] then
        local time = XbShareUtil.shareInfo[XbShareUtil.gameType.daily_share].m_lastUpdateTime
        if time == 0 or time == nil then
            self.leftTime = 0
        else
            self.leftTime = time
        end
    end
end--]]

function ShareLayer:showSuccessTips(msg, data)
    local name = data.name
    local type = XbShareUtil.SHARE_SUCCESS_NO_REWARD_DAILY
    if data.success == true then
        type = XbShareUtil.SHARE_SUCCESS_REWARD
        self:showDropGoldAminataion()
    end
    XbShareUtil:showResultMsg(type, name)
end


function ShareLayer:showDropGoldAminataion()
    XbShareUtil:showDropGoldAminataion(XbShareUtil.gameType.daily_share)
end

function ShareLayer:setupViews()
	self.node = UIAdapter:createNode("csb/share/sharelayer.csb"):addTo(self)
    UIAdapter:adapter(self.node, handler(self, self.onTouchCallback))

    if XbShareUtil.shareInfo[XbShareUtil.gameType.daily_share] then
        local data = XbShareUtil.shareInfo[XbShareUtil.gameType.daily_share]
        if table.nums(data.m_shareRewardList) > 0  then
            self.node:getChildByName("num_txt"):setString(data.m_shareRewardList[1].m_count)
        end

        self.node:getChildByName("icon"):setPositionX(self.node:getChildByName("num_txt"):getPositionX() + self.node:getChildByName("num_txt"):getContentSize().width + 4)

    end

end

function ShareLayer:updateViews()
	--刷新ui
end

function ShareLayer:shareInfo()
	XbShareUtil:share({gameType = XbShareUtil.gameType.daily_share, tag = self._share_type, callback = handler(self, self.onShareResultCallback)})
end

function ShareLayer:onShareResultCallback( result )
    
  --[[  self.node:runAction(cc.Sequence:create(cc.DelayTime:create(0.5), cc.CallFunc:create(function ()
        if result == XbShareUtil.WX_SHARE_OK then
            XbShareUtil:requestShareReward(XbShareUtil.gameType.daily_share)
        else
            XbShareUtil:showResultMsg(result)
        end

    end)))--]]

    performWithDelay(self, function()
        if result == XbShareUtil.WX_SHARE_OK then
            XbShareUtil:requestShareReward(XbShareUtil.gameType.daily_share)
        else
            XbShareUtil:showResultMsg(result)
        end
    end, 0.5)
end

function ShareLayer:onTouchCallback(sender)
	local name = sender:getName()
	if name == "exitBtn" then
		self:closeDialog()
	elseif name == "share_btn" then
		self:shareInfo()
	end
end

function ShareLayer:onDestory()
--[[	removeMsgCallBack(self, MSG_SET_SHARE_CD)--]]
    removeMsgCallBack(self, MSG_ACT_SHARE_SUCCESS)
end

return ShareLayer