--
-- User: lhj
-- Date: 2018/10/11 
-- 分享二维码控制器

local ShareQRcodeController = class("ShareQRcodeController")
local HttpUtil = require("app.hall.base.network.HttpWrap")
require "lfs"

function ShareQRcodeController:ctor()
	self:myInit()
    self:initNetMsgHandlerSwitch()
end

function ShareQRcodeController:myInit()
    self.m_tNetMsgHandlerSwitch = {}
end


function ShareQRcodeController:initNetMsgHandlerSwitch()
    --self:addNetMsgHandler(self, "CS_H2C_GenQRCode_Ack"                , handler(self, self.getQRCodeAck))                  --获取二维码
end

function ShareQRcodeController:addNetMsgHandler(cClass, sKey, fCallback)
    print("ElectricalCityController:setNetMsgHandlerSwitch() = ",cClass, " |", sKey, " |", fCallback)
    if sKey and fCallback then
        local sign = false
        for key, var in pairs(self.m_tNetMsgHandlerSwitch) do
            if var.sKey == sKey and var.cClass == cClass and  var.fCallback == fCallback then
                sign = true
            end
        end
        if not sign then
            TotalController:registerNetMsgCallback(cClass, Protocol.LobbyServer, sKey, fCallback)
            local data = {}
            data.cClass = cClass
            data.sKey = sKey
            data.fCallback = fCallback
            table.insert(self.m_tNetMsgHandlerSwitch, data)
        end

    end
end

local function urlEncode(s)
    s = string.gsub(s, "([^%w%.%- ])", function(c) return string.format("%%%02X", string.byte(c)) end)
    return string.gsub(s, " ", "+")
end

local function urlDecode(s)
    s = string.gsub(s, '%%(%x%x)', function(h) return string.char(tonumber(h, 16)) end)
    return s
end

function ShareQRcodeController:checkQRCode() 
	local _imgpath = cc.UserDefault:getInstance():getStringForKey( "QRshareImg" .. Player:getAccountID(), "")

    local shareBigImgPath, bigImgSavePath

    if device.platform == "android" then
        shareBigImgPath = device.writablePath.."../cache/netSprite/shareBigImgPath/"
    else
        shareBigImgPath = device.writablePath.."netSprite/shareBigImgPath/"
    end


    print("ShareQRcodeController:checkQRCode->", _imgpath, _imgpath == "", cc.FileUtils:getInstance():isFileExist(_imgpath) == false, self:checkFileMd5() == false, not cc.FileUtils:getInstance():isFileExist(shareBigImgPath), not self:isQRcodeExist())
 
    if  _imgpath == "" or cc.FileUtils:getInstance():isFileExist(_imgpath) == false  or self:checkFileMd5() == false  or not self:isQRcodeExist() then
        print("self:ReqShareQRCode()")
        cc.UserDefault:getInstance():setStringForKey( "QRshareImg" .. Player:getAccountID(),"")
        self:getShareLink()
    else
        self:creatShareImg(_imgpath)
    end


end

-- 判断二维码是否生成
function ShareQRcodeController:isQRcodeExist()
   local qRcodeImgPath = cc.UserDefault:getInstance():getStringForKey( "QRcodeImgPath" .. Player:getAccountID(), "" )
   local isExist  = false
   if qRcodeImgPath ~="" and cc.FileUtils:getInstance():isFileExist( qRcodeImgPath  ) then
      isExist = true
   end
   return isExist
end

function ShareQRcodeController:onHttpGet(url, fCallback, index)
    local xhr = cc.XMLHttpRequest:new()
    xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
    xhr:open("GET", url)
    print("onHttpGet() index = ", index)
    local function onReadyStateChange()
        print("onHttpGet() onReadyStateChange() index = ", index)
        print("xhr.status ", xhr.status)
        print("xhr.responseText ", xhr.responseText)
        if fCallback and _G.type(fCallback) == "function" then
            fCallback(xhr, index)
        end
    end
    xhr:registerScriptHandler(onReadyStateChange)
    xhr:send()
end

--获取短链接
function ShareQRcodeController:onUrlShortGet(tindex, fCallback)

    local shareCfg = fromLua("MilShareCfg2")
    self._linkAddress1 = string.gsub(shareCfg[1].linkAddress, "<userid>", Player:getAccountID())
    self._linkAddress1 = string.gsub(self._linkAddress1, "<appNum>", GlobalConf.CHANNEL_ID)

    self._linkAddress = string.gsub(shareCfg[8].linkAddress, "<userid>", Player:getAccountID())
    self._linkAddress = string.gsub(self._linkAddress, "<appNum>", GlobalConf.CHANNEL_ID)

--    print("ShareQRcodeController:onUrlShortGet()linkAddress = ",linkAddress)
--    self.m_sLocalLinkAddress = linkAddress
--    local function onReadyStateChange(client, __tag, event) 
--        if event.name == "completed" then
--            local code = event.request:getResponseStatusCode()

--            local response =  event.request:getResponseString() 
--            print("response =", response)
--            if response then
--                local info = require("cjson").decode(response)
--                if info["code"] == 0 then -- 成功
--                    local strQRCode = info["orCodeUrl"]

--                    self:downloadQRCode(strQRCode)
--                else
--                    print("failed with code: " .. info["code"])
--                end
--            end
--        elseif event.name == "progress" then
--            --print("progress", __tag)
--        end
--    end

--    local __url = "https://api.xbqpthe.com/api/itf/qrcode/create"
--    local __data = {
--        ["url"] = linkAddress
--    }

--    local client = HttpUtil.new(__url)
--    client:post(__data, function ( __tag, event )
--        onReadyStateChange(client, __tag, event)
--    end, false)

                
    local __url = shareCfg[2].linkAddress
    local __data = {
        ["channelId"] = tostring(GlobalConf.CHANNEL_ID),
        ["url"] = string.urlencode(self._linkAddress), 
        ["isRefresh"] = 1
    } 
    local client = HttpUtil.new(__url)
    --if GlobalConf.CurServerType == GlobalConf.ServerType.WW then
        client:post(__data, function ( __tag, event )
            self:getShortUrlBack(client, __tag, event)
        end, false) 
    --end
end

local HttpUrlPath = "/res/qrcode/" 
local localHttpUrlPath = "res/qrcode/"
local htmlFile = "qrcode.html"

function ShareQRcodeController:getShortUrlBack(__client, __tag, event)
    if event.name == "completed" then
        local code = event.request:getResponseStatusCode()

        local response =  event.request:getResponseString() 
        print("response =", response)
        if response then
            local info = require("cjson").decode(response)
            if info and info["code"] == 0 then -- 成功
                local LinkAddress = info["shortUrl"] 
           --     local url = string.format("https://api.xbqpthe.com/api/itf/qrcode/doCreate?url="..LinkAddress)
                local url = LinkAddress.shortUrl  
                self.qrcodeUrl = url
                --    print("ShareQRcodeController:onUrlShortGet()linkAddress = ",linkAddress) 
                local function onReadyStateChange(client, __tag, event) 
                    if event.name == "completed" then
                        local code = event.request:getResponseStatusCode()

                        local response =  event.request:getResponseString() 
                        print("response =", response)
                        if response then
                            local info = require("cjson").decode(response)
                            if info["code"] == 0 then -- 成功
                                local strQRCode = info["orCodeUrl"]

                                self:downloadQRCode(strQRCode)
                            else
                                print("failed with code: " .. info["code"])
                                self:downloadQRCode(self._linkAddress)
                            end
                        end
                    elseif event.name == "progress" then
                        --print("progress", __tag)
                    end
                end

                local __url = GlobalConf.DOMAIN.."api/itf/qrcode/create"
                local __data = {
                    ["url"] = url
                } 
                local client = HttpUtil.new(__url)
                client:post(__data, function ( __tag, event )
                    onReadyStateChange(client, __tag, event)
                end, false)
--                print(url)
--                local xhr = cc.XMLHttpRequest:new()
--                xhr.responseType = cc.XMLHTTPREQUEST_RESPONSE_STRING
--                xhr:open("GET",url) 

--                local function onReadyStateChange() 
--                    local response = xhr.response
--                    if response == "" then
--                        print("response = nil")
--                    else
--                        local output = json.decode(response,1) 
--                        dump(output)
--                        if output["code"] == 0 then
--                            print("******获取二维码下载地址成功******") 
--                            local strQRCode = output["orCodeUrl"]
--                            self:downloadQRCode(strQRCode)
--                        else
--                            print("******获取二维码下载地址失败******") 
--                        end
--                    end
--                end
--                xhr:registerScriptHandler(onReadyStateChange)
--                xhr:send()
            else
                --print("failed with code: " .. info["code"]) 
                local url = self._linkAddress
                --    print("ShareQRcodeController:onUrlShortGet()linkAddress = ",linkAddress) 
                local function onReadyStateChange(client, __tag, event) 
                    if event.name == "completed" then
                        local code = event.request:getResponseStatusCode()

                        local response =  event.request:getResponseString() 
                        print("response =", response)
                        if response then
                            local info = require("cjson").decode(response)
                            if info and info["code"] == 0 then -- 成功
                                local strQRCode = info["orCodeUrl"]

                                self:downloadQRCode(strQRCode)
                            else
                                --print("failed with code: " .. info["code"])
                                self:downloadQRCode(self._linkAddress)
                            end
                        end
                    elseif event.name == "progress" then
                        --print("progress", __tag)
                    end
                end

                local __url = GlobalConf.DOMAIN.."api/itf/qrcode/create"
                local __data = {
                    ["url"] = url
                }

                local client = HttpUtil.new(__url)
                client:post(__data, function ( __tag, event )
                    onReadyStateChange(client, __tag, event)
                end, false)
            end
        end
    elseif event.name == "progress" then
        --print("progress", __tag)
    end
end
--获取分享链接
function ShareQRcodeController:getShareLink()
    
    local function getshortUrl ( _url )
        if _url and string.len(_url)> 3 then

            if device.platform == "android" or  device.platform == "ios" then 

                local Url = "file://"..localHttpUrlPath..htmlFile

                if device.platform == "android" then
                    Url = "file://"..ToolKit:getSDcardPath()..HttpUrlPath..htmlFile
                end

                local url = Url .. "?sidelen=150&url=" .. string.urlencode( _url ) .. "&w=230&h=230"

                print("********最终 url :",url)

                self._webView = ccexp.WebView:create()
                self._webView:setAnchorPoint(0.5, 0.5)
                self._webView:setPosition(500000, 500000)
                self._webView:setContentSize(230, 230)
                self._webView:loadURL(url)
                self._webView:setScalesPageToFit(true)
                self._webView:retain()
                self._webView:setVisible(false) 
                print("开始生成二维码") 
                ToolKit:delayDoSomething( function ()
                    local _str = self._webView:getHtmlData()
                    print("aaaaaaaaaa", _str)

                    local _begin1, _end1 =  string.find(_str,"data:image/png;base64,")

                    local _begin2, _end2 = string.find(_str,"</body></html>")

                    local _source = string.sub(_str, _end1+1,_begin2 - 1 )
                    print(_source)

                    local data = crypto.decodeBase64(_source)
                    print("bbbbbbbbbb")

                    --二维码图片保存路径
                    local QrImg = ""
                    if device.platform == "android" then
                        QrImg  = cc.FileUtils:getInstance():getWritablePath().."../cache/netSprite/".. crypto.md5(_url) .. ".png"
                    else
                        QrImg  = cc.FileUtils:getInstance():getWritablePath().."netSprite/" .. crypto.md5(_url) .. ".png"
                    end

                    print("二维码路径")
                    print(QrImg )

                    --如果没有这个文件夹，则创建这个文件夹
                    if device.platform == "android" then
                        if not cc.FileUtils:getInstance():isDirectoryExist(cc.FileUtils:getInstance():getWritablePath().."../cache/netSprite/")  then
                            UpdateFunctions.mkDir(cc.FileUtils:getInstance():getWritablePath().."../cache/netSprite/")
                        end
                    else
                        if not cc.FileUtils:getInstance():isDirectoryExist(cc.FileUtils:getInstance():getWritablePath().."netSprite/")  then
                            UpdateFunctions.mkDir(cc.FileUtils:getInstance():getWritablePath().."netSprite/")
                        end
                    end
                  
                    local file = io.open(QrImg, "w + b")
                    file:write(data)
                    file:close()
                    print("生成二维码成功")

                    self:creatShareImg( QrImg)

                    self._webView:release()

                end , 4 )

            end

        else
            print("ERROR:获取分享链接失败")
        end
    end

    self:onUrlShortGet(1, getshortUrl)

end

function ShareQRcodeController:creatShareImg( path)
    print("ElectricalCityController:creatShareImg:",path)

    if cc.FileUtils:getInstance():isFileExist(path) then
        if self.QRcode == nil then
            self.QRcode =  cc.Sprite:create(path) 
            self.QRcode:retain()
             self.QRcode1 =  cc.Sprite:create(path) 
            self.QRcode1:retain()
            if self.QRcode then
                print("二维码精灵创建成功")
                if cc.FileUtils:getInstance():isFileExist(path) then
                    cc.UserDefault:getInstance():setStringForKey( "QRcodeImgPath" .. Player:getAccountID(), path)
                end
            else
                print("二维码精灵创建失败")
                return
            end 
        else
            self.QRcode:setTexture(path)
            self.QRcode1:setTexture(path)
        end
    end
end


--获取分享二维码
function ShareQRcodeController:ReqShareQRCode()
    --[[if device.platform == "android" then
        self:copyFile()
        self:getShareLink()
    else
        if self.isDownload == nil  then

            self.isDownload = true

            local function getshortUrl ( _url )
                if _url and string.len(_url)> 3 then

                    local _para = toJson({sidelen = 150, UserId = Player:getAccountID(), url =  _url })
                    print("******向服务器请求二维码下载地址*******")
                    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GenQRCode_Req", { _para}   )
                else
                    print("ERROR:获取分享链接失败")
                end
            end
            self:onUrlShortGet(1, getshortUrl)
        end
    end--]]   
    if self.isDownload == nil  then

        self.isDownload = true

        local function getshortUrl ( _url )
            if _url and string.len(_url)> 3 then

                local _para = toJson({sidelen = 150, UserId = Player:getAccountID(), url =  _url })
                print("******向服务器请求二维码下载地址*******")
                ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_GenQRCode_Req", {_para})
            else
                print("ERROR:获取分享链接失败")
            end
        end

        self:onUrlShortGet(1, getshortUrl)
    end

end

function ShareQRcodeController:getQRCodeAck(__idStr, __info)
    print("ShareQRcodeController:getQRCodeAck")  
    dump(__info, __idStr)
    if __info.m_nResult == 0 then
        print("******获取二维码下载地址成功******") 
        self:downloadQRCode( __info.m_strQRCode )
    else
        print("******获取二维码下载地址失败******")
    end
end


function ShareQRcodeController:downloadQRCode( _url )
    cc.UserDefault:getInstance():setStringForKey( "erweima__url" , _url)
    local originPath = cc.UserDefault:getInstance():getStringForKey( "erweima" , "")
    if originPath~="" then
        UpdateFunctions.removePath(originPath)
    end
    
    local url = _url
    print("**************二维码下载，url: ", url)

    local dic

    if device.platform == "android" then
        dic = device.writablePath.."../cache/netSprite/"
    else
        dic = device.writablePath.."netSprite/"
    end

    if not io.exists(dic) then
        lfs.mkdir(dic) --目录不存在，创建此目录
    end

    local picPath = dic .. crypto.md5(url) .. ".png"

    print("**********二维码保存路径**********")
    print(picPath)

    --self.picPath = picPath

    cc.UserDefault:getInstance():setStringForKey( "erweima" , picPath)
    cc.UserDefault:getInstance():flush()  --flush保存

    local NetSprite = require("app.hall.mail.view.NetSprite")
    if self.QRcode == nil then
           self.QRcode = NetSprite.new(url, true, handler(self, self.downloadQRcodeCallBack), false)   
        self.QRcode:retain() 
        self.QRcode1 = NetSprite.new(url, true, handler(self, self.downloadQRcodeCallBack), false)   
        self.QRcode1:retain() 
            
    else 
       
        self.QRcode:removeFromParent()
        self.QRcode1:removeFromParent()
         self.QRcode:release()
        self.QRcode1:release()
         self.QRcode = NetSprite.new(url, true, handler(self, self.downloadQRcodeCallBack), false)   
        self.QRcode:retain() 
        self.QRcode1 = NetSprite.new(url, true, handler(self, self.downloadQRcodeCallBack), false)   
        self.QRcode1:retain() 
        sendMsg(REFRESH_QRCODE)
    end
--    self.QRcode = NetSprite.new(url, true, handler(self, self.downloadQRcodeCallBack), false)   
--    self.QRcode:retain() 
--    self.QRcode1 = NetSprite.new(url, true, handler(self, self.downloadQRcodeCallBack), false)   
--    self.QRcode1:retain() 
    return

end
function ShareQRcodeController:getQRcode()
    return self.QRcode
end
function ShareQRcodeController:getQRcode1()
    return self.QRcode1
end
function ShareQRcodeController:downloadQRcodeCallBack(isSuccess, path)
    print("ShareQRcodeController:downloadQRcodeCallBack:",path)

--    if isSuccess and self.QRcode then
--        self:creatShareImg(path)
--    end
end 

--检查分享大图的md5值和保存的大图的的md5值是不是一样的
-- 如果不一样说明图片更新了
function ShareQRcodeController:checkFileMd5()

    local _md5 = cc.UserDefault:getInstance():getStringForKey( "ylc_share_img_18yuan", "")

    if _md5 == tostring(UpdateFunctions.fileMd5("res/ui/share/share_img_18yuan.jpg")) then
        --已有的和记录的一样，图片没有改变
        return true
    else
        return false
    end
end



--webview 不能加载游戏目录下的文件，要先把网页文件拷贝到sd卡
function ShareQRcodeController:copyFile()

    print("开始拷贝文件")
    UpdateFunctions.mkDir(ToolKit:getSDcardPath()..HttpUrlPath)

    local sdPath = ToolKit:getSDcardPath()..HttpUrlPath

    print("目标路径：" .. sdPath)

    local filepath = UpdateFunctions.writablePath .. "upd/"..localHttpUrlPath..htmlFile

    if ToolKit:fileIsExists( filepath)  then
        print("源文件路径：" .. filepath)
        ToolKit:copyFileToSDcard( filepath, sdPath..htmlFile )
    else
        print("源文件路径：" .. localHttpUrlPath..htmlFile)
        ToolKit:assetToPath( localHttpUrlPath..htmlFile, sdPath..htmlFile )
    end
end

return ShareQRcodeController