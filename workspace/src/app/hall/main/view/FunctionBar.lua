--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 主界面菜单项

local ViewBase = require("app.hall.base.ui.ViewBase")

local ItemMenu = class("ItemMenu", function ()
    return ViewBase.new()
end)

function ItemMenu:ctor( __funcId )
    self:myInit(__funcId)

    self:setupViews()
end

-- 初始化成员变量
function ItemMenu:myInit( __funcId )
	self.funcId = __funcId
end

-- 初始化界面
function ItemMenu:setupViews()
    if self.funcId ~= nil then
        local data = fromFunction(self.funcId)
        
        if data then
            self.btn = ccui.Button:create(data.iconName,data.iconName,data.iconName,1)
            self:addChild(self.btn)
            
            self.btn:setName("btn_top_menu")

            UIAdapter:registClickCallBack( self.btn, function ()
                self:onTouchCallback(self.btn)
            end )
        end
        ToolKit:setBtnStatusByFuncStatus( self.btn, self.funcId )
    end
end

function ItemMenu:getMenuSize() 
    local _size  = self.btn:getContentSize()
    return _size 
end

-- 点击事件回调
function ItemMenu:onTouchCallback( sender )
    local name = sender:getName()

    -- TOAST("尚未发布, 敬请期待")
    if name == "btn_top_menu" then
        if self.funcId == GlobalDefine.FUNC_ID.SWITCH_PLATFORM then  --返回原平台

            local params = {
                title = STR(74, 1),
                message = STR(73, 1), -- todo: 换行
                leftStr = STR(71, 1),
                rightStr = STR(72, 1),
            }
            local callback1 = function ()
                TotalController:onExitApp()
            end
   
            local dlg = require("app.hall.base.ui.MessageBox").new()
            dlg:TowSubmitAlert(params, callback1)
            dlg:showDialog()
        elseif self.funcId == GlobalDefine.FUNC_ID.MORE_FUNC then
        --更多 
            
          if not g_functionDlg then         
                g_functionDlg = require("app.hall.base.ui.FunctionLayer").new( {funcId = self.funcId} )
                g_functionDlg:showDialog() 
          end
        
        else
            local data = fromFunction(self.funcId)
            if data then
                local stackLayer = data.mobileFunction

                print("stackLayer------------>", stackLayer)

                if stackLayer and string.len(stackLayer) > 0 then
                    sendMsg(MSG_GOTO_STACK_LAYER, {layer = stackLayer, name = data.name, direction = DIRECTION.HORIZONTAL, funcId = self.funcId})

                else
                    --TOAST(STR(62, 1) .. self.funcId )
                    TOAST(data.name .. "敬请期待")
                    -- TOAST("尚未发布, 敬请期待  Id:" .. self.funcId)
                end
            end
        end

    end

end

return ItemMenu
