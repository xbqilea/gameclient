--
-- Author: 
-- Date: 2018-07-27 11:42:15
--

local StackLayer = require("app.hall.base.ui.StackLayer")
local DlgAlert = require("app.hall.base.ui.MessageBox")
local ItemGameIconTwo = require("app.hall.main.view.GameSubIcon")

local HomeTwoLayer = class("HomeTwoLayer", function ()
    return StackLayer.new()
end)

function HomeTwoLayer:ctor( __params )
    self:myInit()
    
    dump(__params)
    self.portalId = __params._info.m_portalId
    for k, v in pairs(__params._info.m_portalList) do
        local data = RoomData:getGameDataById(v.m_portalId)
        data.setSort = v.m_sort
        table.insert(self.games, data)
    end
    table.sort(self.games, function ( a, b )
        return a.setSort < b.setSort
    end)

    -- 初始化界面
    self:setupViews()
    self:updateViews()
    ToolKit:registDistructor( self, handler(self, self.onDestory) )
end

function HomeTwoLayer:onDestory()
    -- 注销消息
end

-- 初始化数据
function HomeTwoLayer:myInit()
    self.portalId = 0 -- 功能id
    self.games = {}
end

-- 初始化界面
function HomeTwoLayer:setupViews()
    self.root = UIAdapter:createNode("csb/vertical/total_layer.csb")
    self:addChild(self.root)

    UIAdapter:adapter(self.root, handler(self, self.onTouchCallback))

    -- 大厅游戏列表
    self.layout5 = self.root:getChildByName("layout_5")

    self.layoutBg = self.root:getChildByName("layout_bg")


    -- 设置菜单栏
    local layoutMenu = self.root:getChildByName("layout_menu")
    layoutMenu:setVisible(false)

    -- 玩家信息栏
    self.layoutHeadIcon = self.root:getChildByName("layout_head_icon")         --头像
    self.labelGold = self.root:getChildByName("label_gold")                    --金币数
end

-- 设置大厅游戏列表
function HomeTwoLayer:updateViews()
    local layoutGameList = UIAdapter:createNode("csb/vertical/lobby/layout_game_two.csb")
    self.layout5:addChild(layoutGameList)
    
    -- local children = RoomData:getChildrenByGameId(self.portalId)
    dump(self.games)
    for i = 1, 9 do
        local layout = self.root:getChildByName("item_" .. i)
        if #self.games >= i then
            layout:setVisible(true)
            layout:addChild(ItemGameIconTwo.new(self.games[i].id))
        else
            layout:setVisible(false)
        end
    end

    -- 下移
    -- local disY = (3 - math.ceil( (#gameList) / 3 )) / 2 * 204
    -- layoutGameList:setPositionY(-disY)
end

-- 点击事件回调
function HomeTwoLayer:onTouchCallback( sender )
    local name = sender:getName()

    print(name)
    if name == "btn_gold" then

    elseif name == "layout_bg" then
        sendMsg(MSG_ENTER_SOME_LAYER, { name = "back" })
    end
end

function HomeTwoLayer:adapter()
    UIAdapter:adapter(self.root)
end


return HomeTwoLayer