--
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 游戏数据(账号数据及保存在客户端的其他数据)
-- 为方便处理，通过GameState保存到本地的游戏数据类需要继承自本类，

local GameData = class("GameData")

function GameData:ctor()
    self:myInit()
end

-- 定义成员变量
function GameData:myInit()
	self.data = {}
end

-- 更新游戏数据
function GameData:updateData( data )
	self.data = data or self.data
end

function GameData:getData()
	return self.data
end

-- function GameData:getAccountList()
-- 	return self._accountList
-- end

-- function GameData:setAccount( __account )
	
-- end

return GameData
