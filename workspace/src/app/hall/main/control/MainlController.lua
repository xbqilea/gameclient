
-- Author: 
-- Date: 2018-07-27 11:42:15
-- 网络消息总控制类

local scheduler = require("framework.scheduler")
local ChatMgr = require("app.newHall.chat.ChatMgr")
TotalController = {}

TotalController.messageCallbakes = {}

TotalController.hallOfflineCount = 0	--大厅心跳离线计数器

-- 注册网络信息回调函数
function TotalController:registerNetMsgCallback( __ref, __protocol, __cmdId, __handler, __tag )
	local tag = __tag or 0
	local callback = {}
	callback.node = __ref
	callback.handler = __handler
	callback.tag = tag
	if TotalController.messageCallbakes[__protocol] == nil then
		TotalController.messageCallbakes[__protocol] = {}
	end
	if TotalController.messageCallbakes[__protocol][__cmdId] == nil then
		TotalController.messageCallbakes[__protocol][__cmdId] = {}
	end
	table.insert(TotalController.messageCallbakes[__protocol][__cmdId], callback)
	-- TotalController.messageCallbakes[__protocol][__cmdId] = callback
end

-- 注销网络信息回调函数
function TotalController:removeNetMsgCallback( __ref, __protocol, __cmdId, __tag )
	if TotalController.messageCallbakes[__protocol] and TotalController.messageCallbakes[__protocol][__cmdId] then
		for i = 1, #TotalController.messageCallbakes[__protocol][__cmdId] do
			local t = TotalController.messageCallbakes[__protocol][__cmdId][i]
			local tag = __tag or 0
			if t.node == __ref and t.tag == tag then
				table.remove(TotalController.messageCallbakes[__protocol][__cmdId], i)
				return true
			end
		end
	end
	return false
end

function TotalController:getMsgCallbacks()
	return TotalController.messageCallbakes
end

-- -- 游戏服socket连接状态改变
-- function TotalController:onGameSocketEventChanged(__protocolId, __connectName) 
-- 	sendMsg(MSG_SOCKET_CONNECTION_EVENT, __protocolId, __connectName)
-- end

-- 网路消息总来源入口
function TotalController:onSvrData( __protocol, __idStr, __msgs )
	if TotalController.messageCallbakes[__protocol] == nil then
		print("[ERROR]: get message callback error by protocal: ", __protocol)
		return
	end
	--处理心跳包
	if __idStr == "CS_H2C_PingAck" then
		self:onReceiveHallPing(__msgs.m_nTimestamp)
		return
	elseif __idStr == "CS_Y2C_NewMailCome_Nty" then
		-- 有新邮件通知
	--	if getFuncOpenStatus(GlobalDefine.FUNC_ID.LOBBY_MAIL) == 0 then
        sendMsg(MSG_MAIL_UPDATE_SUCCESS)
		TOAST("收到新邮件")
	    sendMsg(MSG_NEW_COME_MAIL)
	    ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_CheckNewMail_Req", {{__msgs.m_ntf.m_mailType,__msgs.m_ntf.m_onlineMailVoucher}})
     --   end
        return
	elseif __idStr == "CS_H2C_SendCheckVersion_Nty" then -- 版本更新通知
		-- TOAST("版本更新通知")
	    local UpdateController = require("app.assetmgr.main.control.RenewController")
	    local updateController = UpdateController.new(nil)
		updateController:startPointUpdate(true) -- 开始定点更新呢
		return
	elseif __idStr == "CS_H2C_WebGM_Nty" then -- 后台指令(扣日志)
		self:readWebGmMsg(__msgs)
		return 
	elseif __idStr == "CS_H2C_LastGame_Nty" then -- 广播出去
	--	sendMsg(MSG_PUSH_GAME_RECONNECT, __msgs)
		dump(__msgs, "---------CS_H2C_LastGame_Nty--------")
		if __msgs.m_gameAtomTypeId > 0 then
			local controller = getControllerPath(__msgs.m_gameAtomTypeId)
			if controller then          
				if g_GameController then
					if g_GameController.gameScene and g_GameController.m_gameAtomTypeId == __msgs.m_gameAtomTypeId then
						g_GameController:reqEnterScene()
						return
					end
					g_GameController:releaseInstance()
				end
				if not cc.FileUtils:getInstance():isFileExist(controller) then return end
				g_GameController = require(controller):getInstance()
				g_GameController:bindGameTypeId(__msgs.m_gameAtomTypeId)
				ConnectManager:send2SceneServer(__msgs.m_gameAtomTypeId, "CS_C2M_Enter_Req", { __msgs.m_gameAtomTypeId, "" } )
			end
		elseif __msgs.m_thirdpart > 0 then
			if __msgs.m_thirdpart == 1 then
				if g_CPController == nil then
					
				end
			else
				if g_ThirdPartyContntroller == nil then
					g_ThirdPartyContntroller = require("app.game.caipiao.src.thirdPartyGameContntroller"):getInstance()
				end
				g_ThirdPartyContntroller:setThirdType(__msgs.m_thirdpart)
				g_ThirdPartyContntroller:setEventHandler(function (_id, _cmd)
					if _id == "EnterCP_Ack" then

						if _cmd.m_nRet == 0 then
							g_ThirdPartyContntroller:setEventHandler(nil)
							g_ThirdPartyContntroller.mEnterCPInfo = nil
							g_ThirdPartyContntroller:sendEnterThirdPartReq(__msgs.m_nPlayCode, __msgs.m_nPlatType, __msgs.m_gameId)
						end
					end
				end)
				g_ThirdPartyContntroller:sendEnterReq()
			end
		else
			sendMsg(POPSCENE_ACK)
		end
		return
	elseif __idStr == "CS_H2C_GameShutdown_Nty" then -- 游戏关闭广播(游戏服务已关闭, 无法通过场景转发, 由大厅通知)
		sendMsg(MSG_GAME_SHUT_DOWN, __msgs)
		return
	elseif __idStr == "CS_H2C_HandleMsg_Ack" then -- 场景协议, 转发即可 
		self:transmitSceneMsg( __protocol, __idStr, __msgs )
		return
	elseif __idStr == "CS_H2C_EnterBg_Ack" then
        ToolKit:removeLoadingDialog()
        if (g_BGController) then
            g_BGController:ackEnterBgScene(__msgs)
        end
    elseif __idStr == "CS_H2C_ExitBg_Ack" then
    	ToolKit:removeLoadingDialog()
        if (g_BGController) then
            g_BGController:ackExitBgScene(__msgs)
        end
	elseif __idStr == "CS_H2C_EnterCP_Ack" then
        ToolKit:removeLoadingDialog()
        if (g_CPController) then
            g_CPController:ackEnterScene(__msgs)
		end
		if g_ThirdPartyContntroller then
			g_ThirdPartyContntroller:callNetMsgHandler(__idStr, __msgs)
		end
    elseif __idStr == "CS_H2C_ExitCP_Ack" then
    	ToolKit:removeLoadingDialog()
        if (g_CPController) then
            g_CPController:ackExitScene(__msgs)
		end
		if g_ThirdPartyContntroller then
			g_ThirdPartyContntroller:callNetMsgHandler(__idStr, __msgs)
		end
	elseif __idStr == "CS_H2C_EnterThirdPart_Ack" then
		ToolKit:removeLoadingDialog()
		if g_ThirdPartyContntroller then
			g_ThirdPartyContntroller:callNetMsgHandler(__idStr, __msgs)
		end
	elseif __idStr == "CS_H2C_ExitThirdPart_Ack" then
		ToolKit:removeLoadingDialog()
		if g_ThirdPartyContntroller then
			g_ThirdPartyContntroller:callNetMsgHandler(__idStr, __msgs)
		end
	end

	--dump(TotalController.messageCallbakes, "消息回掉:", 10)

	local callbacks = TotalController.messageCallbakes[__protocol][__idStr]
	if callbacks == nil then
		print("[WARNING]: get message callbacks error by protocal and id: ", __protocol, __idStr)
		return
	end

	local t = clone(callbacks)
	for i = 1, #t do
		t[i].handler(__idStr, __msgs, t[i].tag) 
	end
end

function TotalController:readWebGmMsg( __msgs )
	local table = fromJson(__msgs.m_strMsg)
	if table.type == nil then
		print("[ERROR]: read web gm message fail, because type is nil !")
		return
	end
	if table.type == 1 then -- 扣日志
		local date = table.date 
		local LogUtil = require("app.hall.base.log.Log")
		LogUtil:getInstance():updateLogsByDate(date)
	end
end

-- 转发场景协议-- TODO: 若场景未加载, 则不转发. 
function TotalController:transmitSceneMsg( __protocol, __idStr, __msgs )
	--print("场景协议, 转发!!")
	--dump(__msgs)
	if __msgs.m_result ~= 0 then
		print("TotalController:transmitSceneMsg")
		--TOAST("暂未开放")
		--ToolKit:removeLoadingDialog()
		dump(__msgs)
		if g_GameController then
			g_GameController.releaseInstance()
		end
		return
	end
	
	local msgs = {
		m_type = __msgs.m_type or 0, 
		m_gameAtomTypeId = __msgs.m_gameAtomTypeId,
		m_result = __msgs.m_result,
		m_message = ConnectManager:parseByteStringPackets( __protocol, __msgs.m_message )
	}
	--print("场景协议, 解析!!")
	--dump(msgs)
	if __msgs.m_protoType == 1 then -- 大厅处理
		print("场景处理类型的场景转发协议.")
		--dump(msgs.m_message)
	end
	if TotalController.messageCallbakes[Protocol.SceneServer] == nil then
		print("[ERROR]: get message callback error when attempt to get SceneServer!!")
		return
	end
	local callbacks = TotalController.messageCallbakes[Protocol.SceneServer][__idStr]

	if callbacks == nil then
		print("[ERROR]: get message callbacks error when attempt to get callbacks by id: in SceneServer!!", Protocol.SceneServer, __idStr)
		return
	end

	local t = clone(callbacks)
	for i = 1, #t do
		t[i].handler(__idStr, msgs, t[i].tag) 
	end
end


-- 大厅心跳包
function TotalController:beginToSendHallPing()
	self.hallOfflineCount = 0
	--每过GlobalDefine.HALL_PING_CD秒发一次心跳包
	if g_hall_ping_schedule then
		scheduler.unscheduleGlobal(g_hall_ping_schedule)
	end
	g_hall_ping_schedule = scheduler.scheduleGlobal(handler(self, self.sendPing),GlobalDefine.HALL_PING_CD)
end

function TotalController:stopToSendHallPing() -- 断开心跳
    print("TotalController断开心跳")
	if g_hall_ping_schedule then
		scheduler.unscheduleGlobal(g_hall_ping_schedule)
		g_hall_ping_schedule = nil
	end
    if g_hall_im_schedule then
		scheduler.unscheduleGlobal(g_hall_im_schedule)
		g_hall_im_schedule = nil
	end
end

--发往大厅的心跳包
function TotalController:sendPing()
	self.hallOfflineCount = self.hallOfflineCount + 1
	
	--如果发送失败， 则直接走重连
	if self.hallOfflineCount <= 2 then
		if ConnectManager:send2Server(Protocol.LobbyServer, "CS_C2H_PingReq") ~= 0 then-- 心跳
			self.hallOfflineCount = 3
		end
	end
	
	print("PingHall:",self.hallOfflineCount)
	if self.hallOfflineCount > 2 then --离线心跳包次数
		print("ping timeout")
		ConnectManager:disConnectSvr(Protocol.LobbyServer)
		--self:stopToSendHallPing()
		--g_LoginController._isReconnect = false
		sendMsg(MSG_RECONNECT_LOBBY_SERVER,"fail")		
		if g_isAllowReconnect then
			ConnectManager:reconnect()
		end
	end
end

function TotalController:onReceiveHallPing(m_nTimestamp)
	--大厅心跳包，离线计数清零
	self.hallOfflineCount = 0
	SERVER_TIMESTAMP_DIFF = m_nTimestamp-os.time()
end


-- 退出应用
function TotalController:onExitApp() 
    removeMsgCallBack(self, MSG_GOTO_STACK_LAYER)
	removeMsgCallBack(self, MSG_ENTER_SOME_LAYER)
    if ChatMgr.instance then
	    ChatMgr:releaseInstance() 
    end
	if device.platform == "ios" then
		os.exit()
	elseif device.platform == "windows" then
		cc.Director:getInstance():endToLua()
	else
		
        cc.Director:getInstance():endToLua()   
		--QkaPhoneInfoUtil:returnToOldPlatform()
	end
end

function TotalController:onEnterBackground()
	print("进入后台 TotalController:onEnterBackground")
	sendMsg(MSG_ENTER_BACKGROUND)
end

function TotalController:onEnterForeground()
    --if Player:getAccountID() > 0  then
    --    AliYunUtil:updateAliyunExpireSecond()
    --end
	
	print("进入前台 TotalController:onEnterForeground")
	--g_IsSwitchToBack = false
	ConnectManager:touchLobbyMsgTag()
	sendMsg(MSG_ENTER_FOREGROUND)
end

function TotalController:onGotoCurSceneStackLayer( msgName, msgObj )
	if msgObj.layer and string.len(msgObj.layer) > 0 then
		local scene = ToolKit:getCurrentScene()
		local _node  = require(msgObj.layer).new({ _funcId = msgObj.funcId, _dataTable = msgObj.dataTable})
		if _node.getType and _node.getType() == "dialog" then
			_node:showDialog()
		else
			scene:getStackLayerManager():pushStackLayer(_node, {_funcId = msgObj.funcId, _dataTable = msgObj.dataTable})
		end
	else
		if msgObj.name then
			TOAST(msgObj.name .. "暂未开放")
		end
	end
end
-- 跳转界面的回调
function TotalController:onEnterSomeLayer( msgName, msgObj )
	-- 一级游戏房间
    if msgObj.name == "GameRoomLayer" then
        ToolKit:getCurStackManager():pushStackLayer( msgObj.layer, {_direction = msgObj.direction, _info = msgObj.info} )
	-- 大厅二级菜单
	elseif msgObj.name == "HomeTwoLayer" then
        local view = ToolKit:getCurStackManager():pushStackLayer( "app.hall.main.view.MainSubLayer", {_direction = DIRECTION.HORIZONTAL, _animation = STACKANI.NONE, _info = msgObj.data} )
	elseif msgObj.name == "back" then
		ToolKit:getCurStackManager():popStackLayer()
        -- self:onSceneBaseBackButtonClicked()
    elseif msgObj.name == "root" then
        UIAdapter:getTotalScene():getStackLayerManager():pop2RootLayer()
    elseif msgObj.name == "enterGame" then 
	end
end

addMsgCallBack(TotalController, MSG_GOTO_STACK_LAYER, handler(TotalController, TotalController.onGotoCurSceneStackLayer))
addMsgCallBack(TotalController, MSG_ENTER_SOME_LAYER, handler(TotalController, TotalController.onEnterSomeLayer))

return TotalController
